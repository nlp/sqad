<s>
Rock	rock	k1gInSc1	rock
je	být	k5eAaImIp3nS	být
žánr	žánr	k1gInSc4	žánr
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
proudu	proud	k1gInSc2	proud
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
v	v	k7c4	v
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
rollu	rollo	k1gNnSc6	rollo
<g/>
,	,	kIx,	,
rhythm	rhythm	k6eAd1	rhythm
and	and	k?	and
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
country	country	k2eAgFnSc6d1	country
hudbě	hudba	k1gFnSc6	hudba
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
také	také	k9	také
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
folkem	folk	k1gInSc7	folk
<g/>
,	,	kIx,	,
jazzem	jazz	k1gInSc7	jazz
a	a	k8xC	a
vážnou	vážný	k2eAgFnSc7d1	vážná
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
jako	jako	k8xS	jako
hammondovy	hammondův	k2eAgFnPc4d1	hammondův
varhany	varhany	k1gFnPc4	varhany
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc4	klavír
nebo	nebo	k8xC	nebo
od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rocku	rock	k1gInSc6	rock
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
kytarová	kytarový	k2eAgNnPc1d1	kytarové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
klávesová	klávesový	k2eAgNnPc1d1	klávesové
<g/>
,	,	kIx,	,
saxofonová	saxofonový	k2eAgNnPc1d1	saxofonové
nebo	nebo	k8xC	nebo
harmoniková	harmonikový	k2eAgNnPc1d1	harmonikové
sóla	sólo	k1gNnPc1	sólo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
rocku	rock	k1gInSc2	rock
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
několik	několik	k4yIc1	několik
podžánrů	podžánr	k1gMnPc2	podžánr
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
s	s	k7c7	s
folkem	folk	k1gInSc7	folk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
folk	folk	k1gInSc1	folk
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
smícháním	smíchání	k1gNnSc7	smíchání
s	s	k7c7	s
blues	blues	k1gNnSc7	blues
blues	blues	k1gNnSc2	blues
rock	rock	k1gInSc1	rock
a	a	k8xC	a
rock	rock	k1gInSc1	rock
s	s	k7c7	s
jazzem	jazz	k1gInSc7	jazz
stvořily	stvořit	k5eAaPmAgInP	stvořit
jazz	jazz	k1gInSc1	jazz
fusion	fusion	k1gInSc1	fusion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rock	rock	k1gInSc4	rock
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
Soul	Soul	k1gInSc4	Soul
<g/>
,	,	kIx,	,
funk	funk	k1gInSc4	funk
a	a	k8xC	a
latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
také	také	k9	také
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
další	další	k2eAgFnPc1d1	další
podžánry	podžánra	k1gFnPc1	podžánra
jako	jako	k8xS	jako
soft	soft	k?	soft
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
glam	glam	k1gInSc1	glam
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
heavy	heava	k1gFnPc1	heava
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgInSc4d1	progresivní
rock	rock	k1gInSc4	rock
a	a	k8xC	a
punk	punk	k1gInSc4	punk
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
syntézou	syntéza	k1gFnSc7	syntéza
punk	punk	k1gInSc1	punk
rocku	rock	k1gInSc2	rock
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgInPc7d1	další
styly	styl	k1gInPc7	styl
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tzv.	tzv.	kA	tzv.
Nová	Nová	k1gFnSc1	Nová
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
punk	punk	k1gMnSc1	punk
rock	rock	k1gInSc4	rock
zradikalizoval	zradikalizovat	k5eAaPmAgMnS	zradikalizovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hardcore	hardcor	k1gMnSc5	hardcor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
položilo	položit	k5eAaPmAgNnS	položit
základy	základ	k1gInPc1	základ
alternativnímu	alternativní	k2eAgInSc3d1	alternativní
rocku	rock	k1gInSc3	rock
a	a	k8xC	a
grunge	grunge	k1gFnSc3	grunge
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
britský	britský	k2eAgInSc1d1	britský
alternativní	alternativní	k2eAgInSc4d1	alternativní
rock	rock	k1gInSc4	rock
vykristalizoval	vykristalizovat	k5eAaPmAgMnS	vykristalizovat
v	v	k7c4	v
britpop	britpop	k1gInSc4	britpop
<g/>
,	,	kIx,	,
indie	indie	k1gFnSc1	indie
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americe	amerika	k1gFnSc6	amerika
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nu	nu	k9	nu
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
hudebníků	hudebník	k1gMnPc2	hudebník
soustřeďujících	soustřeďující	k2eAgMnPc2d1	soustřeďující
se	se	k3xPyFc4	se
na	na	k7c4	na
rockovou	rockový	k2eAgFnSc4d1	rocková
muziku	muzika	k1gFnSc4	muzika
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
rockových	rockový	k2eAgFnPc2d1	rocková
kapel	kapela	k1gFnPc2	kapela
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kytaristy	kytarista	k1gMnSc2	kytarista
(	(	kIx(	(
<g/>
hrajícího	hrající	k2eAgMnSc2d1	hrající
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgMnSc2d1	hlavní
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
,	,	kIx,	,
basáka	basák	k1gMnSc2	basák
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc2	bubeník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tvoří	tvořit	k5eAaImIp3nS	tvořit
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
rolí	role	k1gFnPc2	role
vynechají	vynechat	k5eAaPmIp3nP	vynechat
nebo	nebo	k8xC	nebo
využijí	využít	k5eAaPmIp3nP	využít
zpěváka	zpěvák	k1gMnSc2	zpěvák
i	i	k8xC	i
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
nějakého	nějaký	k3yIgInSc2	nějaký
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tvoří	tvořit	k5eAaImIp3nS	tvořit
trio	trio	k1gNnSc1	trio
nebo	nebo	k8xC	nebo
duo	duo	k1gNnSc1	duo
<g/>
;	;	kIx,	;
některé	některý	k3yIgNnSc4	některý
naopak	naopak	k6eAd1	naopak
nějakého	nějaký	k3yIgMnSc4	nějaký
hudebníka	hudebník	k1gMnSc4	hudebník
přidají	přidat	k5eAaPmIp3nP	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Rockové	rockový	k2eAgFnPc1d1	rocková
kapely	kapela	k1gFnPc1	kapela
nějakých	nějaký	k3yIgInPc2	nějaký
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
těch	ten	k3xDgMnPc2	ten
založených	založený	k2eAgMnPc2d1	založený
na	na	k7c4	na
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
rollu	roll	k1gMnSc6	roll
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
saxofon	saxofon	k1gInSc4	saxofon
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
strunné	strunný	k2eAgInPc1d1	strunný
smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
nástroje	nástroj	k1gInPc1	nástroj
jako	jako	k8xC	jako
housle	housle	k1gFnPc1	housle
nebo	nebo	k8xC	nebo
violoncello	violoncello	k1gNnSc1	violoncello
a	a	k8xC	a
žesťové	žesťový	k2eAgInPc1d1	žesťový
nástroje	nástroj	k1gInPc1	nástroj
jako	jako	k8xS	jako
trubka	trubka	k1gFnSc1	trubka
nebo	nebo	k8xC	nebo
pozoun	pozoun	k1gInSc1	pozoun
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rock	rock	k1gInSc1	rock
and	and	k?	and
roll	roll	k1gInSc1	roll
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
roll	roll	k1gInSc1	roll
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
během	během	k7c2	během
konce	konec	k1gInSc2	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
smícháním	smíchání	k1gNnSc7	smíchání
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
černošské	černošský	k2eAgFnSc2d1	černošská
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rhythm	rhythm	k1gInSc1	rhythm
and	and	k?	and
blues	blues	k1gInSc1	blues
a	a	k8xC	a
gospel	gospel	k1gInSc1	gospel
<g/>
)	)	kIx)	)
s	s	k7c7	s
country	country	k2eAgFnSc7d1	country
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
začal	začít	k5eAaPmAgMnS	začít
diskžokej	diskžokej	k1gMnSc1	diskžokej
Alan	Alan	k1gMnSc1	Alan
Freed	Freed	k1gMnSc1	Freed
hrát	hrát	k5eAaImF	hrát
rhythm	rhythm	k1gInSc4	rhythm
and	and	k?	and
blues	blues	k1gNnSc2	blues
pro	pro	k7c4	pro
multirasové	multirasový	k2eAgNnSc4d1	multirasový
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
rock	rock	k1gInSc1	rock
and	and	k?	and
roll	roll	k1gInSc1	roll
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rock	rock	k1gInSc1	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
roll	roll	k1gMnSc1	roll
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
americké	americký	k2eAgFnSc2d1	americká
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
ze	z	k7c2	z
žebříčků	žebříček	k1gInPc2	žebříček
populární	populární	k2eAgFnSc2d1	populární
zpěváky	zpěvák	k1gMnPc7	zpěvák
předchozího	předchozí	k2eAgNnSc2d1	předchozí
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
vznik	vznik	k1gInSc4	vznik
několika	několik	k4yIc3	několik
podžánrům	podžánr	k1gMnPc3	podžánr
včetně	včetně	k7c2	včetně
rockabilly	rockabilla	k1gFnSc2	rockabilla
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
populární	populární	k2eAgMnSc1d1	populární
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
díky	díky	k7c3	díky
Elvisovi	Elvis	k1gMnSc3	Elvis
Presleymu	Presleym	k1gInSc2	Presleym
<g/>
,	,	kIx,	,
Buddy	Budda	k1gFnSc2	Budda
Hollymu	Hollym	k1gInSc2	Hollym
a	a	k8xC	a
Carlu	Carl	k1gMnSc3	Carl
Perkinsovi	Perkins	k1gMnSc3	Perkins
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
používat	používat	k5eAaImF	používat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Buddy	Budda	k1gFnPc4	Budda
Holly	Holla	k1gFnSc2	Holla
a	a	k8xC	a
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
narukoval	narukovat	k5eAaPmAgMnS	narukovat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
sláva	sláva	k1gFnSc1	sláva
rock	rock	k1gInSc1	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
rollu	rollo	k1gNnSc6	rollo
ustupovat	ustupovat	k5eAaImF	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
mezi	mezi	k7c7	mezi
koncem	konec	k1gInSc7	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
roll	roll	k1gInSc1	roll
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
zlomový	zlomový	k2eAgInSc1d1	zlomový
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
začátcích	začátek	k1gInPc6	začátek
měli	mít	k5eAaImAgMnP	mít
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
stáli	stát	k5eAaImAgMnP	stát
černoši	černoch	k1gMnPc1	černoch
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
rock	rock	k1gInSc1	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
roll	roll	k1gInSc1	roll
nezmizel	zmizet	k5eNaPmAgInS	zmizet
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
energie	energie	k1gFnSc2	energie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
viděna	vidět	k5eAaImNgFnS	vidět
např.	např.	kA	např.
ve	v	k7c6	v
vlně	vlna	k1gFnSc6	vlna
šílenství	šílenství	k1gNnSc2	šílenství
okolo	okolo	k7c2	okolo
Twistu	twist	k1gInSc2	twist
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proslavil	proslavit	k5eAaPmAgInS	proslavit
Chubby	Chubb	k1gInPc4	Chubb
Checker	Checkra	k1gFnPc2	Checkra
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
britský	britský	k2eAgInSc1d1	britský
rock	rock	k1gInSc1	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
rollový	rollový	k2eAgInSc1d1	rollový
hit	hit	k1gInSc1	hit
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
Move	Move	k1gNnPc1	Move
It	It	k1gFnPc2	It
<g/>
"	"	kIx"	"
od	od	k7c2	od
Cliffa	Cliff	k1gMnSc2	Cliff
Richarda	Richard	k1gMnSc2	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kapela	kapela	k1gFnSc1	kapela
The	The	k1gFnSc2	The
Shadows	Shadows	k1gInSc1	Shadows
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
instrumentální	instrumentální	k2eAgFnSc7d1	instrumentální
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
rock	rock	k1gInSc1	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
roll	roll	k1gInSc1	roll
ustupoval	ustupovat	k5eAaImAgInS	ustupovat
k	k	k7c3	k
lehkému	lehký	k2eAgInSc3d1	lehký
popu	pop	k1gInSc3	pop
a	a	k8xC	a
baladám	balada	k1gFnPc3	balada
<g/>
,	,	kIx,	,
britské	britský	k2eAgFnPc1d1	britská
rockové	rockový	k2eAgFnPc1d1	rocková
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
začaly	začít	k5eAaPmAgFnP	začít
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
prudkostí	prudkost	k1gFnSc7	prudkost
a	a	k8xC	a
elánem	elán	k1gInSc7	elán
málokdy	málokdy	k6eAd1	málokdy
viděným	viděný	k2eAgInSc7d1	viděný
ve	v	k7c6	v
vystoupeních	vystoupení	k1gNnPc6	vystoupení
amerických	americký	k2eAgMnPc2d1	americký
bělochů	běloch	k1gMnPc2	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
byl	být	k5eAaImAgInS	být
také	také	k9	také
příchod	příchod	k1gInSc1	příchod
soulu	soul	k1gInSc2	soul
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
velký	velký	k2eAgInSc1d1	velký
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Ray	Ray	k1gMnSc1	Ray
Charles	Charles	k1gMnSc1	Charles
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
Cooke	Cook	k1gFnSc2	Cook
<g/>
,	,	kIx,	,
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Marvin	Marvina	k1gFnPc2	Marvina
Gaye	gay	k1gMnSc2	gay
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
Aretha	Aretha	k1gFnSc1	Aretha
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
<g/>
,	,	kIx,	,
Curtis	Curtis	k1gFnSc1	Curtis
Mayfield	Mayfield	k1gMnSc1	Mayfield
a	a	k8xC	a
Stevie	Stevie	k1gFnSc1	Stevie
Wonder	Wondra	k1gFnPc2	Wondra
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tak	tak	k6eAd1	tak
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
zrychlit	zrychlit	k5eAaPmF	zrychlit
včlenění	včlenění	k1gNnSc4	včlenění
černochů	černoch	k1gMnPc2	černoch
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Surf	surf	k1gInSc1	surf
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
spustila	spustit	k5eAaPmAgFnS	spustit
vlna	vlna	k1gFnSc1	vlna
instrumentálního	instrumentální	k2eAgInSc2d1	instrumentální
surf	surf	k1gInSc4	surf
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jehož	k3xOyRp3gMnPc4	jehož
zakladatele	zakladatel	k1gMnPc4	zakladatel
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Dick	Dick	k1gInSc1	Dick
Dale	Dal	k1gInSc2	Dal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
typický	typický	k2eAgInSc1d1	typický
"	"	kIx"	"
<g/>
mokrý	mokrý	k2eAgInSc1d1	mokrý
<g/>
"	"	kIx"	"
reverb	reverb	k1gInSc1	reverb
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
surf	surf	k1gInSc1	surf
rock	rock	k1gInSc1	rock
jako	jako	k8xS	jako
vokální	vokální	k2eAgFnSc1d1	vokální
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	dík	k1gInPc1	dík
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
dali	dát	k5eAaPmAgMnP	dát
dohromady	dohromady	k6eAd1	dohromady
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
první	první	k4xOgInSc1	první
hit	hit	k1gInSc1	hit
"	"	kIx"	"
<g/>
Surfin	Surfin	k1gInSc1	Surfin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
udělal	udělat	k5eAaPmAgMnS	udělat
ze	z	k7c2	z
surf	surf	k1gInSc1	surf
rocku	rock	k1gInSc2	rock
národní	národní	k2eAgInSc1d1	národní
fenomén	fenomén	k1gInSc1	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
kapela	kapela	k1gFnSc1	kapela
pomalu	pomalu	k6eAd1	pomalu
opouštěla	opouštět	k5eAaImAgFnS	opouštět
surfování	surfování	k1gNnSc4	surfování
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
téma	téma	k1gNnSc4	téma
jejich	jejich	k3xOp3gFnPc2	jejich
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
přesunula	přesunout	k5eAaPmAgFnS	přesunout
se	se	k3xPyFc4	se
k	k	k7c3	k
obecnějším	obecní	k2eAgNnPc3d2	obecní
tématům	téma	k1gNnPc3	téma
chlapeckého	chlapecký	k2eAgNnSc2d1	chlapecké
mládí	mládí	k1gNnSc2	mládí
-	-	kIx~	-
např.	např.	kA	např.
autům	auto	k1gNnPc3	auto
a	a	k8xC	a
dívkám	dívka	k1gFnPc3	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
surfové	surfový	k2eAgFnSc2d1	surfová
hudby	hudba	k1gFnSc2	hudba
byla	být	k5eAaImAgFnS	být
rázně	rázně	k6eAd1	rázně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
britskou	britský	k2eAgFnSc7d1	britská
invazí	invaze	k1gFnSc7	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
rocková	rockový	k2eAgFnSc1d1	rocková
nebo	nebo	k8xC	nebo
popová	popový	k2eAgFnSc1d1	popová
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
konkurovat	konkurovat	k5eAaImF	konkurovat
Beatles	Beatles	k1gFnSc4	Beatles
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
albem	album	k1gNnSc7	album
Pet	Pet	k1gFnPc2	Pet
Sounds	Sounds	k1gInSc4	Sounds
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
právě	právě	k9	právě
Beach	Beach	k1gInSc1	Beach
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Britská	britský	k2eAgFnSc1d1	britská
invaze	invaze	k1gFnSc1	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
rockové	rockový	k2eAgFnSc6d1	rocková
scéně	scéna	k1gFnSc6	scéna
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
big	big	k?	big
beatové	beatový	k2eAgFnSc2d1	beatová
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
velká	velký	k2eAgFnSc1d1	velká
škála	škála	k1gFnSc1	škála
amerických	americký	k2eAgMnPc2d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
stylů	styl	k1gInPc2	styl
-	-	kIx~	-
např.	např.	kA	např.
soul	soul	k1gInSc1	soul
<g/>
,	,	kIx,	,
rhythm	rhythm	k1gInSc1	rhythm
and	and	k?	and
blues	blues	k1gInSc1	blues
a	a	k8xC	a
surf	surf	k1gInSc1	surf
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
pouze	pouze	k6eAd1	pouze
přebíraly	přebírat	k5eAaImAgFnP	přebírat
klasické	klasický	k2eAgFnPc1d1	klasická
americké	americký	k2eAgFnPc1d1	americká
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přidali	přidat	k5eAaPmAgMnP	přidat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skladby	skladba	k1gFnPc4	skladba
se	s	k7c7	s
stále	stále	k6eAd1	stále
složitějšími	složitý	k2eAgFnPc7d2	složitější
hudebními	hudební	k2eAgFnPc7d1	hudební
myšlenkami	myšlenka	k1gFnPc7	myšlenka
a	a	k8xC	a
osobitým	osobitý	k2eAgInSc7d1	osobitý
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
The	The	k1gFnSc4	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
kapel	kapela	k1gFnPc2	kapela
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
The	The	k1gFnPc7	The
Animals	Animalsa	k1gFnPc2	Animalsa
a	a	k8xC	a
The	The	k1gFnPc2	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
)	)	kIx)	)
ovlivněných	ovlivněný	k2eAgMnPc2d1	ovlivněný
bluesem	blues	k1gInSc7	blues
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
beatové	beatový	k2eAgFnSc2d1	beatová
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xC	jako
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Searchers	Searchers	k1gInSc1	Searchers
a	a	k8xC	a
The	The	k1gMnSc1	The
Hollies	Hollies	k1gMnSc1	Hollies
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
rock	rock	k1gInSc1	rock
se	se	k3xPyFc4	se
populárním	populární	k2eAgMnSc6d1	populární
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1964	[number]	k4	1964
díky	díky	k7c3	díky
The	The	k1gFnSc3	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
hit	hit	k1gInSc1	hit
"	"	kIx"	"
<g/>
I	i	k8xC	i
Want	Want	k1gMnSc1	Want
to	ten	k3xDgNnSc4	ten
Hold	hold	k1gInSc1	hold
Your	Your	k1gMnSc1	Your
Hand	Hand	k1gMnSc1	Hand
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
tak	tak	k6eAd1	tak
Britskou	britský	k2eAgFnSc4d1	britská
invazi	invaze	k1gFnSc4	invaze
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
dostala	dostat	k5eAaPmAgFnS	dostat
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
45	[number]	k4	45
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
7	[number]	k4	7
týdnů	týden	k1gInPc2	týden
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
vydržela	vydržet	k5eAaPmAgFnS	vydržet
15	[number]	k4	15
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
The	The	k1gMnPc1	The
Beatles	Beatles	k1gFnPc2	Beatles
poprvé	poprvé	k6eAd1	poprvé
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
The	The	k1gFnSc6	The
Ed	Ed	k1gMnSc1	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc1	show
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
mezník	mezník	k1gInSc4	mezník
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
popkultuře	popkultuře	k?	popkultuře
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
přilákalo	přilákat	k5eAaPmAgNnS	přilákat
zhruba	zhruba	k6eAd1	zhruba
73	[number]	k4	73
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rekordní	rekordní	k2eAgInSc4d1	rekordní
počet	počet	k1gInSc4	počet
pro	pro	k7c4	pro
televizní	televizní	k2eAgInSc4d1	televizní
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Beatles	Beatles	k1gFnPc2	Beatles
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
rockovou	rockový	k2eAgFnSc7d1	rocková
kapelou	kapela	k1gFnSc7	kapela
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
následovalo	následovat	k5eAaImAgNnS	následovat
je	on	k3xPp3gMnPc4	on
mnoho	mnoho	k6eAd1	mnoho
britských	britský	k2eAgFnPc2d1	britská
kapel	kapela	k1gFnPc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
umístily	umístit	k5eAaPmAgInP	umístit
např.	např.	kA	např.
kapely	kapela	k1gFnSc2	kapela
The	The	k1gFnSc2	The
Animals	Animals	k1gInSc1	Animals
<g/>
,	,	kIx,	,
Manfred	Manfred	k1gMnSc1	Manfred
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Petula	Petula	k1gFnSc1	Petula
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
and	and	k?	and
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Troggs	Troggs	k1gInSc1	Troggs
<g/>
,	,	kIx,	,
Freddie	Freddie	k1gFnSc1	Freddie
and	and	k?	and
the	the	k?	the
Dreamers	Dreamers	k1gInSc1	Dreamers
<g/>
,	,	kIx,	,
Herman	Herman	k1gMnSc1	Herman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hermits	Hermits	k1gInSc1	Hermits
nebo	nebo	k8xC	nebo
Donovan	Donovan	k1gMnSc1	Donovan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
kapely	kapela	k1gFnPc4	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
invaze	invaze	k1gFnSc1	invaze
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
The	The	k1gFnSc4	The
Kinks	Kinksa	k1gFnPc2	Kinksa
a	a	k8xC	a
The	The	k1gFnPc2	The
Dave	Dav	k1gInSc5	Dav
Clark	Clark	k1gInSc4	Clark
Five	Five	k1gNnSc3	Five
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
Britské	britský	k2eAgFnSc2d1	britská
invaze	invaze	k1gFnSc2	invaze
dominovaly	dominovat	k5eAaImAgInP	dominovat
žebříčkům	žebříček	k1gInPc3	žebříček
i	i	k9	i
doma	doma	k6eAd1	doma
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Ameriku	Amerika	k1gFnSc4	Amerika
znamenala	znamenat	k5eAaImAgFnS	znamenat
Britská	britský	k2eAgFnSc1d1	britská
invaze	invaze	k1gFnSc1	invaze
konec	konec	k1gInSc1	konec
surfové	surfový	k2eAgFnSc2d1	surfová
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
dívčích	dívčí	k2eAgFnPc2d1	dívčí
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hrála	hrát	k5eAaImAgFnS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
při	při	k7c6	při
vzestupu	vzestup	k1gInSc6	vzestup
odlišného	odlišný	k2eAgInSc2d1	odlišný
žánru	žánr	k1gInSc2	žánr
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Garage	Garage	k1gFnSc1	Garage
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Garage	Garage	k6eAd1	Garage
rock	rock	k1gInSc1	rock
byl	být	k5eAaImAgInS	být
formou	forma	k1gFnSc7	forma
amatérského	amatérský	k2eAgInSc2d1	amatérský
rocku	rock	k1gInSc2	rock
převážně	převážně	k6eAd1	převážně
se	se	k3xPyFc4	se
vyskytujícího	vyskytující	k2eAgMnSc4d1	vyskytující
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nazýván	nazýván	k2eAgInSc1d1	nazýván
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nacvičovala	nacvičovat	k5eAaImAgFnS	nacvičovat
v	v	k7c6	v
rodinných	rodinný	k2eAgFnPc6d1	rodinná
garážích	garáž	k1gFnPc6	garáž
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
se	se	k3xPyFc4	se
točily	točit	k5eAaImAgInP	točit
okolo	okolo	k7c2	okolo
problémů	problém	k1gInPc2	problém
středoškolského	středoškolský	k2eAgInSc2d1	středoškolský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
a	a	k8xC	a
podání	podání	k1gNnSc4	podání
byly	být	k5eAaImAgFnP	být
agresivnější	agresivní	k2eAgFnPc1d2	agresivnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
protopunk	protopunk	k1gInSc1	protopunk
Garage	Garage	k1gNnSc1	Garage
rock	rock	k1gInSc1	rock
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
písně	píseň	k1gFnPc4	píseň
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
počtech	počet	k1gInPc6	počet
umisťovaly	umisťovat	k5eAaImAgFnP	umisťovat
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
žebříčcích	žebříček	k1gInPc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
byl	být	k5eAaImAgInS	být
garage	garage	k6eAd1	garage
rock	rock	k1gInSc1	rock
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
zmizel	zmizet	k5eAaPmAgMnS	zmizet
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
podžánry	podžánr	k1gInPc1	podžánr
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ho	on	k3xPp3gInSc4	on
nahradily	nahradit	k5eAaPmAgFnP	nahradit
(	(	kIx(	(
<g/>
blues	blues	k1gInSc1	blues
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgInSc1d1	progresivní
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
country	country	k2eAgInSc1d1	country
rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
zůstal	zůstat	k5eAaPmAgInS	zůstat
garage	garage	k6eAd1	garage
rock	rock	k1gInSc1	rock
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
skupinami	skupina	k1gFnPc7	skupina
MC5	MC5	k1gFnSc2	MC5
a	a	k8xC	a
The	The	k1gFnSc2	The
Stooges	Stoogesa	k1gFnPc2	Stoogesa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
punkové	punkový	k2eAgFnSc3d1	punková
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
Pop	pop	k1gMnSc1	pop
rock	rock	k1gInSc1	rock
a	a	k8xC	a
Power	Power	k1gMnSc1	Power
pop	pop	k1gMnSc1	pop
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
pop	pop	k1gInSc1	pop
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
padesátých	padesátý	k4xOgInPc2	padesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
rozlišný	rozlišný	k2eAgInSc4d1	rozlišný
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
na	na	k7c4	na
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
Britské	britský	k2eAgFnSc2d1	britská
invaze	invaze	k1gFnSc2	invaze
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
častěji	často	k6eAd2	často
užívat	užívat	k5eAaImF	užívat
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
rocku	rock	k1gInSc3	rock
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
více	hodně	k6eAd2	hodně
komerčního	komerční	k2eAgInSc2d1	komerční
<g/>
,	,	kIx,	,
pomíjivého	pomíjivý	k2eAgInSc2d1	pomíjivý
a	a	k8xC	a
dostupného	dostupný	k2eAgInSc2d1	dostupný
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rock	rock	k1gInSc4	rock
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
pohlíželo	pohlížet	k5eAaImAgNnS	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
umělečtější	umělecký	k2eAgInSc4d2	umělečtější
<g/>
,	,	kIx,	,
autentičtější	autentický	k2eAgInSc4d2	autentičtější
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
mnohé	mnohý	k2eAgFnPc4d1	mnohá
popové	popový	k2eAgFnPc4d1	popová
a	a	k8xC	a
rockové	rockový	k2eAgFnPc4d1	rocková
skladby	skladba	k1gFnPc4	skladba
podobné	podobný	k2eAgFnPc4d1	podobná
zvukově	zvukově	k6eAd1	zvukově
<g/>
,	,	kIx,	,
instrumentálně	instrumentálně	k6eAd1	instrumentálně
i	i	k9	i
textově	textově	k6eAd1	textově
<g/>
.	.	kIx.	.
</s>
<s>
Termíny	termín	k1gInPc1	termín
"	"	kIx"	"
<g/>
pop	pop	k1gInSc1	pop
rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
power	powrat	k5eAaPmRp2nS	powrat
pop	pop	k1gInSc1	pop
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
komerčně	komerčně	k6eAd1	komerčně
úspěšnější	úspěšný	k2eAgFnSc4d2	úspěšnější
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
elementy	element	k1gInPc7	element
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Výraznými	výrazný	k2eAgMnPc7d1	výrazný
představiteli	představitel	k1gMnPc7	představitel
pop	pop	k1gInSc4	pop
rocku	rock	k1gInSc2	rock
jsou	být	k5eAaImIp3nP	být
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gMnSc2	McCartnea
<g/>
,	,	kIx,	,
The	The	k1gMnSc2	The
Everly	Everla	k1gMnSc2	Everla
Brothers	Brothers	k1gInSc4	Brothers
<g/>
,	,	kIx,	,
Rod	rod	k1gInSc4	rod
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Frampton	Frampton	k1gInSc1	Frampton
a	a	k8xC	a
Queen	Queen	k1gInSc1	Queen
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Blues	blues	k1gNnSc1	blues
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
první	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
Britské	britský	k2eAgFnSc2d1	britská
invaze	invaze	k1gFnSc2	invaze
byla	být	k5eAaImAgFnS	být
skrze	skrze	k?	skrze
big	big	k?	big
beat	beat	k1gInSc1	beat
a	a	k8xC	a
rhythm	rhythm	k1gInSc1	rhythm
and	and	k?	and
blues	blues	k1gInSc1	blues
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
)	)	kIx)	)
čerpala	čerpat	k5eAaImAgFnS	čerpat
inspiraci	inspirace	k1gFnSc4	inspirace
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
amerického	americký	k2eAgNnSc2d1	americké
blues	blues	k1gNnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnSc2d1	britská
skupiny	skupina	k1gFnSc2	skupina
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
hlavně	hlavně	k6eAd1	hlavně
Leadbelly	Leadbella	k1gFnSc2	Leadbella
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
akustické	akustický	k2eAgInPc4d1	akustický
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
zvuk	zvuk	k1gInSc4	zvuk
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
postavou	postava	k1gFnSc7	postava
britského	britský	k2eAgNnSc2d1	Britské
blues	blues	k1gNnSc2	blues
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Mayall	Mayall	k1gMnSc1	Mayall
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
kapelu	kapela	k1gFnSc4	kapela
Bluesbreakers	Bluesbreakersa	k1gFnPc2	Bluesbreakersa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc4	jejíž
členy	člen	k1gMnPc4	člen
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Eric	Eric	k1gFnSc4	Eric
Clapton	Clapton	k1gInSc4	Clapton
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Peter	Peter	k1gMnSc1	Peter
Green	Grena	k1gFnPc2	Grena
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
Clapton	Clapton	k1gInSc1	Clapton
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgInS	založit
superskupiny	superskupina	k1gFnSc2	superskupina
Cream	Cream	k1gInSc1	Cream
<g/>
,	,	kIx,	,
Blind	Blind	k1gMnSc1	Blind
Faith	Faith	k1gMnSc1	Faith
a	a	k8xC	a
Derek	Derek	k1gMnSc1	Derek
and	and	k?	and
the	the	k?	the
Dominos	Dominosa	k1gFnPc2	Dominosa
a	a	k8xC	a
poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
sólové	sólový	k2eAgFnSc6d1	sólová
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přinesla	přinést	k5eAaPmAgFnS	přinést
blues	blues	k1gNnSc4	blues
rock	rock	k1gInSc1	rock
do	do	k7c2	do
mainstreamu	mainstream	k1gInSc2	mainstream
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Green	Green	k2eAgMnSc1d1	Green
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mickem	Micek	k1gMnSc7	Micek
Fleetwoodem	Fleetwood	k1gMnSc7	Fleetwood
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
McViem	McVius	k1gMnSc7	McVius
z	z	k7c2	z
Bluesbreakers	Bluesbreakersa	k1gFnPc2	Bluesbreakersa
založili	založit	k5eAaPmAgMnP	založit
Fleetwood	Fleetwood	k1gInSc4	Fleetwood
Mac	Mac	kA	Mac
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
kapel	kapela	k1gFnPc2	kapela
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Jimmy	Jimma	k1gFnPc1	Jimma
Page	Pag	k1gFnSc2	Pag
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
The	The	k1gFnSc2	The
Yardbirds	Yardbirds	k1gInSc1	Yardbirds
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
kapelu	kapela	k1gFnSc4	kapela
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
první	první	k4xOgFnSc1	první
alba	alba	k1gFnSc1	alba
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
bluesovou	bluesový	k2eAgFnSc7d1	bluesová
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
blues	blues	k1gFnSc2	blues
rocku	rock	k1gInSc2	rock
v	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
kytarista	kytarista	k1gMnSc1	kytarista
Lonnie	Lonnie	k1gFnSc2	Lonnie
Mack	Mack	k1gMnSc1	Mack
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
hudba	hudba	k1gFnSc1	hudba
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
začala	začít	k5eAaPmAgFnS	začít
zvukově	zvukově	k6eAd1	zvukově
podobat	podobat	k5eAaImF	podobat
hudebníkům	hudebník	k1gMnPc3	hudebník
britského	britský	k2eAgInSc2d1	britský
bluesu	blues	k1gInSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
hudebníku	hudebník	k1gMnSc6	hudebník
patří	patřit	k5eAaImIp3nP	patřit
Canned	Canned	k1gMnSc1	Canned
Heat	Heat	k1gMnSc1	Heat
<g/>
,	,	kIx,	,
Janis	Janis	k1gFnSc1	Janis
Joplin	Joplin	k1gInSc1	Joplin
<g/>
,	,	kIx,	,
Johnny	Johnna	k1gFnPc1	Johnna
Winter	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
J.	J.	kA	J.
Geils	Geils	k1gInSc1	Geils
Band	band	k1gInSc1	band
a	a	k8xC	a
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
založil	založit	k5eAaPmAgMnS	založit
power	power	k1gMnSc1	power
tria	trio	k1gNnSc2	trio
The	The	k1gMnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
a	a	k8xC	a
Band	banda	k1gFnPc2	banda
of	of	k?	of
Gypsys	Gypsysa	k1gFnPc2	Gypsysa
<g/>
.	.	kIx.	.
</s>
<s>
Blues	blues	k1gNnSc1	blues
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xS	jako
The	The	k1gFnSc2	The
Allman	Allman	k1gMnSc1	Allman
Brothers	Brothersa	k1gFnPc2	Brothersa
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
Lynyrd	Lynyrd	k1gMnSc1	Lynyrd
Skynyrd	Skynyrd	k1gMnSc1	Skynyrd
a	a	k8xC	a
ZZ	ZZ	kA	ZZ
Top	topit	k5eAaImRp2nS	topit
z	z	k7c2	z
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
stylu	styl	k1gInSc2	styl
vložily	vložit	k5eAaPmAgInP	vložit
prvky	prvek	k1gInPc1	prvek
country	country	k2eAgInPc1d1	country
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
tak	tak	k9	tak
southern	southern	k1gNnSc4	southern
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
blues	blues	k1gFnSc7	blues
rockové	rockový	k2eAgFnPc1d1	rocková
kapely	kapela	k1gFnPc1	kapela
často	často	k6eAd1	často
napodobovaly	napodobovat	k5eAaImAgFnP	napodobovat
jazz	jazz	k1gInSc4	jazz
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrály	hrát	k5eAaImAgFnP	hrát
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
složité	složitý	k2eAgFnPc1d1	složitá
improvizace	improvizace	k1gFnPc1	improvizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
staly	stát	k5eAaPmAgInP	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
prvkem	prvek	k1gInSc7	prvek
progresivního	progresivní	k2eAgInSc2d1	progresivní
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
Cream	Cream	k1gInSc1	Cream
a	a	k8xC	a
The	The	k1gMnPc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experience	k1gFnPc1	Experience
začaly	začít	k5eAaPmAgFnP	začít
oddalovat	oddalovat	k5eAaImF	oddalovat
od	od	k7c2	od
blues	blues	k1gNnSc2	blues
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
psychedelické	psychedelický	k2eAgInPc4d1	psychedelický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
blues	blues	k1gInSc1	blues
rock	rock	k1gInSc4	rock
stával	stávat	k5eAaImAgInS	stávat
těžším	těžký	k2eAgMnSc7d2	těžší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
založeným	založený	k2eAgFnPc3d1	založená
na	na	k7c6	na
riffech	riff	k1gInPc6	riff
a	a	k8xC	a
hranice	hranice	k1gFnPc1	hranice
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
hard	hard	k6eAd1	hard
rockem	rock	k1gInSc7	rock
byla	být	k5eAaImAgFnS	být
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
soustředit	soustředit	k5eAaPmF	soustředit
více	hodně	k6eAd2	hodně
na	na	k7c4	na
heavy	heava	k1gFnPc4	heava
metal	metat	k5eAaImAgMnS	metat
a	a	k8xC	a
blues	blues	k1gFnSc4	blues
rock	rock	k1gInSc4	rock
pomalu	pomalu	k6eAd1	pomalu
odstupoval	odstupovat	k5eAaImAgMnS	odstupovat
z	z	k7c2	z
mainstreamu	mainstream	k1gInSc2	mainstream
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
a	a	k8xC	a
Folk	folk	k1gInSc1	folk
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
silně	silně	k6eAd1	silně
projevovat	projevovat	k5eAaImF	projevovat
skupina	skupina	k1gFnSc1	skupina
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
oživit	oživit	k5eAaPmF	oživit
folkovou	folkový	k2eAgFnSc4d1	folková
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
byli	být	k5eAaImAgMnP	být
průkopníky	průkopník	k1gMnPc7	průkopník
tohoto	tento	k3xDgNnSc2	tento
žánru	žánr	k1gInSc3	žánr
např.	např.	kA	např.
Woody	Wooda	k1gFnSc2	Wooda
Guthrie	Guthrie	k1gFnSc2	Guthrie
a	a	k8xC	a
Pete	Pet	k1gFnSc2	Pet
Seeger	Seegra	k1gFnPc2	Seegra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
tohoto	tento	k3xDgInSc2	tento
proudu	proud	k1gInSc2	proud
postavili	postavit	k5eAaPmAgMnP	postavit
Joan	Joan	k1gMnSc1	Joan
Baez	Baez	k1gMnSc1	Baez
a	a	k8xC	a
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
<g/>
.	.	kIx.	.
</s>
<s>
Dylan	Dylan	k1gInSc1	Dylan
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
hity	hit	k1gInPc7	hit
"	"	kIx"	"
<g/>
Blowin	Blowin	k1gInSc1	Blowin
<g/>
'	'	kIx"	'
in	in	k?	in
the	the	k?	the
Wind	Wind	k1gInSc1	Wind
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Masters	Masters	k1gInSc1	Masters
of	of	k?	of
War	War	k1gFnSc1	War
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
mainstreamu	mainstream	k1gInSc2	mainstream
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ovlivňovaly	ovlivňovat	k5eAaImAgInP	ovlivňovat
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
rock	rock	k1gInSc4	rock
a	a	k8xC	a
folk	folk	k1gInSc4	folk
oddělenými	oddělený	k2eAgInPc7d1	oddělený
žánry	žánr	k1gInPc7	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
pokusy	pokus	k1gInPc4	pokus
zkombinovat	zkombinovat	k5eAaPmF	zkombinovat
folk	folk	k1gInSc4	folk
a	a	k8xC	a
rock	rock	k1gInSc4	rock
patří	patřit	k5eAaImIp3nP	patřit
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
House	house	k1gNnSc1	house
of	of	k?	of
the	the	k?	the
Rising	Rising	k1gInSc1	Rising
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Animals	Animalsa	k1gFnPc2	Animalsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
komerčně	komerčně	k6eAd1	komerčně
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
folkovou	folkový	k2eAgFnSc7d1	folková
písní	píseň	k1gFnSc7	píseň
nahranou	nahraný	k2eAgFnSc7d1	nahraná
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
rollovými	rollův	k2eAgInPc7d1	rollův
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
a	a	k8xC	a
Loser	Loser	k1gInSc1	Loser
<g/>
"	"	kIx"	"
skupiny	skupina	k1gFnPc1	skupina
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
za	za	k7c4	za
start	start	k1gInSc4	start
folk	folk	k1gInSc4	folk
rockové	rockový	k2eAgFnSc2d1	rocková
vlny	vlna	k1gFnSc2	vlna
považuje	považovat	k5eAaImIp3nS	považovat
nahrávka	nahrávka	k1gFnSc1	nahrávka
Dylanovy	Dylanův	k2eAgFnSc2d1	Dylanova
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Tambourine	Tambourin	k1gInSc5	Tambourin
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnPc2	The
Byrds	Byrdsa	k1gFnPc2	Byrdsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
přední	přední	k2eAgNnPc4d1	přední
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
nástroje	nástroj	k1gInPc4	nástroj
přešel	přejít	k5eAaPmAgInS	přejít
Dylan	Dylan	k1gInSc1	Dylan
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Like	Like	k1gInSc1	Like
a	a	k8xC	a
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
americkým	americký	k2eAgInSc7d1	americký
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Folk	folk	k1gInSc1	folk
rock	rock	k1gInSc1	rock
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působily	působit	k5eAaImAgFnP	působit
The	The	k1gFnPc1	The
Mamas	Mamas	k1gInSc1	Mamas
&	&	k?	&
the	the	k?	the
Papas	Papas	k1gInSc1	Papas
a	a	k8xC	a
Crosby	Crosba	k1gFnPc1	Crosba
<g/>
,	,	kIx,	,
Stills	Stills	k1gInSc1	Stills
<g/>
,	,	kIx,	,
Nash	Nash	k1gMnSc1	Nash
and	and	k?	and
Young	Young	k1gMnSc1	Young
<g/>
,	,	kIx,	,
a	a	k8xC	a
New	New	k1gFnSc1	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xS	jako
The	The	k1gFnPc1	The
Lovin	Lovin	k1gInSc1	Lovin
<g/>
'	'	kIx"	'
Spoonful	Spoonful	k1gInSc1	Spoonful
nebo	nebo	k8xC	nebo
Simon	Simon	k1gMnSc1	Simon
&	&	k?	&
Garfunkel	Garfunkel	k1gMnSc1	Garfunkel
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
hudebníci	hudebník	k1gMnPc1	hudebník
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
britské	britský	k2eAgMnPc4d1	britský
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Fairport	Fairport	k1gInSc1	Fairport
Convention	Convention	k1gInSc1	Convention
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
tato	tento	k3xDgFnSc1	tento
kapela	kapela	k1gFnSc1	kapela
přestala	přestat	k5eAaPmAgFnS	přestat
hrát	hrát	k5eAaImF	hrát
vlastní	vlastní	k2eAgFnSc1d1	vlastní
verze	verze	k1gFnSc1	verze
amerických	americký	k2eAgFnPc2d1	americká
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
tradiční	tradiční	k2eAgInSc4d1	tradiční
anglický	anglický	k2eAgInSc4d1	anglický
folk	folk	k1gInSc4	folk
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
elektrický	elektrický	k2eAgInSc1d1	elektrický
folk	folk	k1gInSc1	folk
začaly	začít	k5eAaPmAgInP	začít
hrát	hrát	k5eAaImF	hrát
např.	např.	kA	např.
kapely	kapela	k1gFnSc2	kapela
Pentangle	Pentangle	k1gFnSc2	Pentangle
a	a	k8xC	a
Steeleye	Steeleye	k1gNnSc2	Steeleye
Span	Spana	k1gFnPc2	Spana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přimělo	přimět	k5eAaPmAgNnS	přimět
irské	irský	k2eAgFnPc4d1	irská
a	a	k8xC	a
skotské	skotský	k2eAgFnPc4d1	skotská
hudební	hudební	k2eAgFnPc4d1	hudební
skupiny	skupina	k1gFnPc4	skupina
jako	jako	k8xC	jako
Horslips	Horslips	k1gInSc4	Horslips
vytvořit	vytvořit	k5eAaPmF	vytvořit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
keltský	keltský	k2eAgInSc4d1	keltský
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Folk	folk	k1gInSc1	folk
rock	rock	k1gInSc1	rock
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
svého	svůj	k3xOyFgInSc2	svůj
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mnohé	mnohý	k2eAgFnPc1d1	mnohá
kapely	kapela	k1gFnPc1	kapela
posouvaly	posouvat	k5eAaImAgFnP	posouvat
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dylan	Dylan	k1gInSc1	Dylan
a	a	k8xC	a
The	The	k1gMnPc1	The
Byrds	Byrdsa	k1gFnPc2	Byrdsa
začali	začít	k5eAaPmAgMnP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
country	country	k2eAgInSc4d1	country
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
ve	v	k7c6	v
folku	folk	k1gInSc6	folk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
o	o	k7c6	o
sobě	se	k3xPyFc3	se
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hraje	hrát	k5eAaImIp3nS	hrát
psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
13	[number]	k4	13
<g/>
th	th	k?	th
Floor	Floor	k1gInSc1	Floor
Elevators	Elevatorsa	k1gFnPc2	Elevatorsa
z	z	k7c2	z
Texasu	Texas	k1gInSc2	Texas
.	.	kIx.	.
</s>
<s>
Dali	dát	k5eAaPmAgMnP	dát
to	ten	k3xDgNnSc4	ten
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
-	-	kIx~	-
The	The	k1gFnSc4	The
Psychedelic	Psychedelice	k1gFnPc2	Psychedelice
Sounds	Sounds	k1gInSc4	Sounds
of	of	k?	of
the	the	k?	the
13	[number]	k4	13
<g/>
th	th	k?	th
Floor	Floor	k1gInSc1	Floor
Elevators	Elevators	k1gInSc1	Elevators
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Beatles	Beatles	k1gFnPc1	Beatles
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
představili	představit	k5eAaPmAgMnP	představit
hlavní	hlavní	k2eAgInPc4d1	hlavní
prvky	prvek	k1gInPc4	prvek
psychedelického	psychedelický	k2eAgInSc2d1	psychedelický
zvuku	zvuk	k1gInSc2	zvuk
širšímu	široký	k2eAgNnSc3d2	širší
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Psychedelický	psychedelický	k2eAgInSc1d1	psychedelický
rock	rock	k1gInSc1	rock
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
ujal	ujmout	k5eAaPmAgInS	ujmout
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
přední	přední	k2eAgMnPc4d1	přední
představitele	představitel	k1gMnPc4	představitel
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
patří	patřit	k5eAaImIp3nS	patřit
Grateful	Grateful	k1gInSc1	Grateful
Dead	Deado	k1gNnPc2	Deado
<g/>
,	,	kIx,	,
Country	country	k2eAgFnSc1d1	country
Joe	Joe	k1gFnSc1	Joe
and	and	k?	and
the	the	k?	the
Fish	Fish	k1gMnSc1	Fish
a	a	k8xC	a
Jefferson	Jefferson	k1gMnSc1	Jefferson
Airplane	Airplan	k1gMnSc5	Airplan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Byrds	Byrds	k1gInSc1	Byrds
se	se	k3xPyFc4	se
od	od	k7c2	od
čistého	čistý	k2eAgInSc2d1	čistý
folk	folk	k1gInSc4	folk
rocku	rock	k1gInSc2	rock
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Eight	Eight	k2eAgMnSc1d1	Eight
Miles	Miles	k1gMnSc1	Miles
High	High	k1gMnSc1	High
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
byli	být	k5eAaImAgMnP	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
kapel	kapela	k1gFnPc2	kapela
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
The	The	k1gFnSc2	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
britský	britský	k2eAgInSc1d1	britský
underground	underground	k1gInSc1	underground
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
patřily	patřit	k5eAaImAgFnP	patřit
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
<g/>
,	,	kIx,	,
Traffic	Traffic	k1gMnSc1	Traffic
a	a	k8xC	a
Soft	Soft	k?	Soft
Machine	Machin	k1gInSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k9	také
vydaly	vydat	k5eAaPmAgFnP	vydat
své	svůj	k3xOyFgInPc4	svůj
debuty	debut	k1gInPc4	debut
kapely	kapela	k1gFnSc2	kapela
Cream	Cream	k1gInSc1	Cream
a	a	k8xC	a
The	The	k1gMnPc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
kytarové	kytarový	k2eAgInPc1d1	kytarový
jamy	jam	k1gInPc1	jam
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
znakem	znak	k1gInSc7	znak
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vydali	vydat	k5eAaPmAgMnP	vydat
The	The	k1gMnSc4	The
Beatles	beatles	k1gMnSc4	beatles
psychedelické	psychedelický	k2eAgNnSc1d1	psychedelické
album	album	k1gNnSc1	album
Sgt	Sgt	k1gFnSc2	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc4	club
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Lucy	Luca	k1gFnSc2	Luca
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
with	with	k1gMnSc1	with
Diamonds	Diamonds	k1gInSc1	Diamonds
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
později	pozdě	k6eAd2	pozdě
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
vydali	vydat	k5eAaPmAgMnP	vydat
Their	Their	k1gInSc4	Their
Satanic	satanice	k1gFnPc2	satanice
Majesties	Majesties	k1gMnSc1	Majesties
Request	Request	k1gMnSc1	Request
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k6eAd1	pink
Floyd	Floyda	k1gFnPc2	Floyda
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
The	The	k1gMnSc1	The
Piper	Piper	k1gMnSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
jako	jako	k9	jako
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
nejlepším	dobrý	k2eAgNnSc6d3	nejlepší
psychedelickém	psychedelický	k2eAgNnSc6d1	psychedelické
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgMnPc7d1	hlavní
alby	alba	k1gFnSc2	alba
Surrealistic	Surrealistice	k1gFnPc2	Surrealistice
Pillow	Pillow	k1gMnSc1	Pillow
kapely	kapela	k1gFnSc2	kapela
Jefferson	Jefferson	k1gMnSc1	Jefferson
Airplane	Airplan	k1gMnSc5	Airplan
a	a	k8xC	a
Strange	Strange	k1gFnPc6	Strange
Days	Days	k1gInSc1	Days
od	od	k7c2	od
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žánr	žánr	k1gInSc1	žánr
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
na	na	k7c6	na
hudebním	hudební	k2eAgInSc6d1	hudební
festivalu	festival	k1gInSc6	festival
Woodstock	Woodstocka	k1gFnPc2	Woodstocka
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgMnPc2d1	hlavní
psychedelických	psychedelický	k2eAgMnPc2d1	psychedelický
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
úplném	úplný	k2eAgInSc6d1	úplný
konci	konec	k1gInSc6	konec
dekády	dekáda	k1gFnSc2	dekáda
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
psychedelický	psychedelický	k2eAgInSc1d1	psychedelický
rock	rock	k1gInSc1	rock
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Brian	Brian	k1gMnSc1	Brian
Jones	Jones	k1gMnSc1	Jones
z	z	k7c2	z
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stones	k1gMnSc1	Stones
a	a	k8xC	a
Syd	Syd	k1gMnSc1	Syd
Barrett	Barrett	k1gMnSc1	Barrett
z	z	k7c2	z
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
přestali	přestat	k5eAaPmAgMnP	přestat
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
kapely	kapela	k1gFnSc2	kapela
The	The	k1gFnSc2	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
a	a	k8xC	a
Cream	Cream	k1gInSc1	Cream
se	se	k3xPyFc4	se
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
ostatních	ostatní	k2eAgFnPc2d1	ostatní
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
k	k	k7c3	k
roots	roots	k6eAd1	roots
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgInS	vracet
více	hodně	k6eAd2	hodně
ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Progresivní	progresivní	k2eAgInSc4d1	progresivní
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Progresivní	progresivní	k2eAgInSc1d1	progresivní
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
s	s	k7c7	s
art	art	k?	art
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pokusem	pokus	k1gInSc7	pokus
odpoutat	odpoutat	k5eAaPmF	odpoutat
se	se	k3xPyFc4	se
od	od	k7c2	od
zavedených	zavedený	k2eAgInPc2d1	zavedený
hudebních	hudební	k2eAgInPc2d1	hudební
zvyků	zvyk	k1gInPc2	zvyk
experimentováním	experimentování	k1gNnSc7	experimentování
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
typy	typ	k1gInPc1	typ
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
podobami	podoba	k1gFnPc7	podoba
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
začaly	začít	k5eAaPmAgFnP	začít
skupiny	skupina	k1gFnPc4	skupina
jako	jako	k8xS	jako
The	The	k1gFnPc4	The
Beatles	beatles	k1gMnPc2	beatles
<g/>
,	,	kIx,	,
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
a	a	k8xC	a
The	The	k1gFnPc2	The
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
zařazovat	zařazovat	k5eAaImF	zařazovat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
písní	píseň	k1gFnPc2	píseň
cembalo	cembalo	k1gNnSc1	cembalo
a	a	k8xC	a
dechové	dechový	k2eAgInPc1d1	dechový
a	a	k8xC	a
strunné	strunný	k2eAgInPc4d1	strunný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
barokní	barokní	k2eAgInSc4d1	barokní
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
The	The	k1gFnSc2	The
Moody	Mooda	k1gFnSc2	Mooda
Blues	blues	k1gFnSc2	blues
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
Days	Daysa	k1gFnPc2	Daysa
of	of	k?	of
Future	Futur	k1gMnSc5	Futur
Passed	Passed	k1gMnSc1	Passed
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
použila	použít	k5eAaPmAgFnS	použít
celý	celý	k2eAgInSc4d1	celý
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
orchestrální	orchestrální	k2eAgInPc4d1	orchestrální
zvuky	zvuk	k1gInPc4	zvuk
syntezátory	syntezátor	k1gInPc4	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
orchestrace	orchestrace	k1gFnSc1	orchestrace
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
a	a	k8xC	a
syntezátory	syntezátor	k1gInPc1	syntezátor
se	se	k3xPyFc4	se
v	v	k7c6	v
progresivním	progresivní	k2eAgInSc6d1	progresivní
rocku	rock	k1gInSc6	rock
staly	stát	k5eAaPmAgInP	stát
častým	častý	k2eAgInSc7d1	častý
přídavkem	přídavek	k1gInSc7	přídavek
k	k	k7c3	k
běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgFnSc3d1	používaná
formě	forma	k1gFnSc3	forma
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
basy	basa	k1gFnSc2	basa
a	a	k8xC	a
bubnů	buben	k1gInPc2	buben
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
SF	SF	kA	SF
Sorrow	Sorrow	k1gFnSc1	Sorrow
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Pretty	Pretta	k1gFnSc2	Pretta
Things	Things	k1gInSc1	Things
<g/>
,	,	kIx,	,
Tommy	Tomm	k1gInPc1	Tomm
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
od	od	k7c2	od
The	The	k1gFnSc2	The
Who	Who	k1gFnPc2	Who
a	a	k8xC	a
Arthur	Arthura	k1gFnPc2	Arthura
(	(	kIx(	(
<g/>
Or	Or	k1gFnSc1	Or
the	the	k?	the
Decline	Declin	k1gInSc5	Declin
and	and	k?	and
Fall	Fall	k1gInSc1	Fall
of	of	k?	of
the	the	k?	the
British	British	k1gInSc1	British
Empire	empir	k1gInSc5	empir
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
od	od	k7c2	od
The	The	k1gFnSc2	The
Kinks	Kinksa	k1gFnPc2	Kinksa
představila	představit	k5eAaPmAgFnS	představit
rockovou	rockový	k2eAgFnSc4d1	rocková
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
otevřela	otevřít	k5eAaPmAgFnS	otevřít
dveře	dveře	k1gFnPc4	dveře
konceptuálním	konceptuální	k2eAgNnPc3d1	konceptuální
albům	album	k1gNnPc3	album
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnPc1	skupina
King	King	k1gMnSc1	King
Crimson	Crimson	k1gMnSc1	Crimson
In	In	k1gMnSc1	In
the	the	k?	the
Court	Court	k1gInSc1	Court
of	of	k?	of
the	the	k?	the
Crimson	Crimson	k1gMnSc1	Crimson
King	King	k1gMnSc1	King
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
smíchalo	smíchat	k5eAaPmAgNnS	smíchat
rázné	rázný	k2eAgNnSc1d1	rázné
kytarové	kytarový	k2eAgInPc1d1	kytarový
riffy	riff	k1gInPc1	riff
a	a	k8xC	a
mellotron	mellotron	k1gInSc1	mellotron
s	s	k7c7	s
jazzem	jazz	k1gInSc7	jazz
a	a	k8xC	a
symfonickou	symfonický	k2eAgFnSc7d1	symfonická
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pomohla	pomoct	k5eAaPmAgFnS	pomoct
rozšíření	rozšíření	k1gNnSc3	rozšíření
progresivního	progresivní	k2eAgInSc2d1	progresivní
rocku	rock	k1gInSc2	rock
mezi	mezi	k7c7	mezi
blues	blues	k1gNnSc7	blues
rockové	rockový	k2eAgFnSc2d1	rocková
a	a	k8xC	a
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
kapely	kapela	k1gFnSc2	kapela
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
měli	mít	k5eAaImAgMnP	mít
Pink	pink	k6eAd1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Syda	Syd	k2eAgFnSc1d1	Syda
Barretta	Barretta	k1gFnSc1	Barretta
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
z	z	k7c2	z
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
virtuozitu	virtuozita	k1gFnSc4	virtuozita
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
Yes	Yes	k1gFnSc1	Yes
předváděla	předvádět	k5eAaImAgFnS	předvádět
umění	umění	k1gNnSc4	umění
kytaristů	kytarista	k1gMnPc2	kytarista
Steva	Steve	k1gMnSc2	Steve
Howea	Howeus	k1gMnSc2	Howeus
a	a	k8xC	a
Ricka	Ricek	k1gMnSc2	Ricek
Wakemana	Wakeman	k1gMnSc2	Wakeman
a	a	k8xC	a
superskupina	superskupina	k1gFnSc1	superskupina
Emerson	Emerson	k1gInSc1	Emerson
<g/>
,	,	kIx,	,
Lake	Lake	k1gInSc1	Lake
&	&	k?	&
Palmer	Palmer	k1gInSc1	Palmer
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
některá	některý	k3yIgNnPc4	některý
technicky	technicky	k6eAd1	technicky
nejnáročnější	náročný	k2eAgNnPc4d3	nejnáročnější
díla	dílo	k1gNnPc4	dílo
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
prog	prog	k1gInSc1	prog
rock	rock	k1gInSc1	rock
sahal	sahat	k5eAaImAgInS	sahat
od	od	k7c2	od
průkopnického	průkopnický	k2eAgMnSc2d1	průkopnický
Franka	Frank	k1gMnSc2	Frank
Zappy	Zappa	k1gFnSc2	Zappa
<g/>
,	,	kIx,	,
Captaina	Captain	k1gMnSc2	Captain
Beefhearta	Beefheart	k1gMnSc2	Beefheart
a	a	k8xC	a
Blood	Blood	k1gInSc4	Blood
<g/>
,	,	kIx,	,
Sweat	Sweat	k2eAgInSc4d1	Sweat
&	&	k?	&
Tears	Tears	k1gInSc4	Tears
k	k	k7c3	k
více	hodně	k6eAd2	hodně
popovým	popový	k2eAgFnPc3d1	popová
kapelám	kapela	k1gFnPc3	kapela
Boston	Boston	k1gInSc4	Boston
<g/>
,	,	kIx,	,
Foreigner	Foreigner	k1gInSc4	Foreigner
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc4	Kansas
<g/>
,	,	kIx,	,
Journey	Journea	k1gFnPc4	Journea
a	a	k8xC	a
Styx	Styx	k1gInSc4	Styx
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Británii	Británie	k1gFnSc4	Británie
působily	působit	k5eAaImAgInP	působit
např.	např.	kA	např.
kapely	kapela	k1gFnSc2	kapela
Supertramp	Supertramp	k1gInSc1	Supertramp
a	a	k8xC	a
ELO	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
část	část	k1gFnSc1	část
žánru	žánr	k1gInSc2	žánr
vyústila	vyústit	k5eAaPmAgFnS	vyústit
např.	např.	kA	např.
v	v	k7c4	v
album	album	k1gNnSc4	album
Tubular	Tubulara	k1gFnPc2	Tubulara
Bells	Bellsa	k1gFnPc2	Bellsa
Mika	Mik	k1gMnSc2	Mik
Oldfielda	Oldfieldo	k1gNnSc2	Oldfieldo
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
a	a	k8xC	a
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
hit	hit	k1gInSc1	hit
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Virgin	Virgin	k2eAgInSc1d1	Virgin
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Instrumentální	instrumentální	k2eAgInSc4d1	instrumentální
rock	rock	k1gInSc4	rock
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgInS	pomoct
kapelám	kapela	k1gFnPc3	kapela
jako	jako	k9	jako
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
,	,	kIx,	,
Tangerine	Tangerin	k1gInSc5	Tangerin
Dream	Dream	k1gInSc1	Dream
<g/>
,	,	kIx,	,
Can	Can	k1gFnSc1	Can
nebo	nebo	k8xC	nebo
Faust	Faust	k1gFnSc1	Faust
překonat	překonat	k5eAaPmF	překonat
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
bariéru	bariéra	k1gFnSc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
krautrock	krautrock	k1gInSc1	krautrock
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
Briana	Brian	k1gMnSc2	Brian
Ena	Ena	k1gMnSc2	Ena
(	(	kIx(	(
<g/>
který	který	k3yIgInSc4	který
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Roxy	Roxa	k1gFnSc2	Roxa
Music	Musice	k1gFnPc2	Musice
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgInS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
synth	synth	k1gInSc4	synth
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
punk	punk	k1gInSc4	punk
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
technologických	technologický	k2eAgFnPc2d1	technologická
změn	změna	k1gFnPc2	změna
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začali	začít	k5eAaPmAgMnP	začít
lidé	člověk	k1gMnPc1	člověk
postupně	postupně	k6eAd1	postupně
progresivní	progresivní	k2eAgInSc4d1	progresivní
rock	rock	k1gInSc4	rock
odmítat	odmítat	k5eAaImF	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pár	pár	k4xCyI	pár
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Genesis	Genesis	k1gFnSc4	Genesis
<g/>
,	,	kIx,	,
ELP	ELP	kA	ELP
<g/>
,	,	kIx,	,
Yes	Yes	k1gFnSc2	Yes
a	a	k8xC	a
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
vydávali	vydávat	k5eAaPmAgMnP	vydávat
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
alba	alba	k1gFnSc1	alba
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
světovými	světový	k2eAgFnPc7d1	světová
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Glam	Glam	k1gMnSc1	Glam
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Glam	Glam	k6eAd1	Glam
rock	rock	k1gInSc1	rock
vyvstal	vyvstat	k5eAaPmAgInS	vyvstat
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
psychedelického	psychedelický	k2eAgInSc2d1	psychedelický
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
art	art	k?	art
rocku	rock	k1gInSc3	rock
konce	konec	k1gInSc2	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
jako	jako	k9	jako
rozšíření	rozšíření	k1gNnSc1	rozšíření
těchto	tento	k3xDgMnPc2	tento
žánrů	žánr	k1gInPc2	žánr
a	a	k8xC	a
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
<g/>
,	,	kIx,	,
sahal	sahat	k5eAaImAgInS	sahat
od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollových	rollův	k2eAgInPc2d1	rollův
revivalů	revival	k1gInPc2	revival
až	až	k9	až
ke	k	k7c3	k
složitosti	složitost	k1gFnSc3	složitost
skladeb	skladba	k1gFnPc2	skladba
kapely	kapela	k1gFnSc2	kapela
Roxy	Roxa	k1gFnSc2	Roxa
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
módu	móda	k1gFnSc4	móda
i	i	k9	i
jako	jako	k9	jako
na	na	k7c4	na
podžánr	podžánr	k1gInSc4	podžánr
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
směs	směs	k1gFnSc1	směs
různých	různý	k2eAgInPc2d1	různý
stylů	styl	k1gInPc2	styl
inspirovaných	inspirovaný	k2eAgInPc2d1	inspirovaný
např.	např.	kA	např.
hollywoodskou	hollywoodský	k2eAgFnSc7d1	hollywoodská
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc7	sci-fi
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
pobuřujícím	pobuřující	k2eAgNnSc7d1	pobuřující
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
make-upy	makep	k1gInPc7	make-up
a	a	k8xC	a
účesy	účes	k1gInPc7	účes
<g/>
.	.	kIx.	.
</s>
<s>
Glam	Glam	k1gInSc1	Glam
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
sexuální	sexuální	k2eAgFnSc4d1	sexuální
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
nejasnost	nejasnost	k1gFnSc4	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
představitele	představitel	k1gMnPc4	představitel
glam	glama	k1gFnPc2	glama
rocku	rock	k1gInSc2	rock
patří	patřit	k5eAaImIp3nS	patřit
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
glam	glama	k1gFnPc2	glama
rocku	rock	k1gInSc2	rock
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
Marcem	Marce	k1gMnSc7	Marce
Bolanem	Bolan	k1gMnSc7	Bolan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
svoje	svůj	k3xOyFgNnSc4	svůj
folkové	folkový	k2eAgNnSc1d1	folkové
duo	duo	k1gNnSc1	duo
na	na	k7c6	na
T.	T.	kA	T.
Rex	Rex	k1gMnPc7	Rex
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
elektrické	elektrický	k2eAgInPc4d1	elektrický
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
počáteční	počáteční	k2eAgInSc1d1	počáteční
moment	moment	k1gInSc1	moment
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
jeho	jeho	k3xOp3gNnSc4	jeho
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
televizním	televizní	k2eAgInSc6d1	televizní
programu	program	k1gInSc6	program
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Popsa	k1gFnPc2	Popsa
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
třpytivé	třpytivý	k2eAgNnSc4d1	třpytivé
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
zahrál	zahrát	k5eAaPmAgMnS	zahrát
svůj	svůj	k3xOyFgInSc4	svůj
později	pozdě	k6eAd2	pozdě
první	první	k4xOgInSc4	první
veleúspěšný	veleúspěšný	k2eAgInSc4d1	veleúspěšný
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Ride	Ridus	k1gMnSc5	Ridus
a	a	k8xC	a
White	Whit	k1gMnSc5	Whit
Swan	Swano	k1gNnPc2	Swano
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
si	se	k3xPyFc3	se
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
alter	alter	k1gMnSc1	alter
ego	ego	k1gNnSc2	ego
Ziggyho	Ziggy	k1gMnSc2	Ziggy
Stardusta	Stardust	k1gMnSc2	Stardust
a	a	k8xC	a
přinesl	přinést	k5eAaPmAgInS	přinést
tím	ten	k3xDgMnSc7	ten
do	do	k7c2	do
svých	svůj	k3xOyFgMnPc2	svůj
vystoupení	vystoupení	k1gNnSc2	vystoupení
něco	něco	k3yInSc1	něco
nového	nový	k2eAgNnSc2d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Podobného	podobný	k2eAgInSc2d1	podobný
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ujaly	ujmout	k5eAaPmAgFnP	ujmout
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
Roxy	Roxa	k1gFnPc1	Roxa
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
Sweet	Sweeta	k1gFnPc2	Sweeta
<g/>
,	,	kIx,	,
Slade	slad	k1gInSc5	slad
<g/>
,	,	kIx,	,
Mott	motto	k1gNnPc2	motto
the	the	k?	the
Hoople	Hoople	k1gFnSc2	Hoople
a	a	k8xC	a
Mud	Mud	k1gFnSc2	Mud
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
britských	britský	k2eAgInPc6d1	britský
žebříčcích	žebříček	k1gInPc6	žebříček
měly	mít	k5eAaImAgFnP	mít
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
málokterá	málokterý	k3yIgFnSc1	málokterý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokázala	dokázat	k5eAaPmAgFnS	dokázat
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
;	;	kIx,	;
výraznou	výrazný	k2eAgFnSc7d1	výrazná
výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgMnS	být
Bowie	Bowius	k1gMnPc4	Bowius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
hvězdou	hvězda	k1gFnSc7	hvězda
a	a	k8xC	a
navedl	navést	k5eAaPmAgInS	navést
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
některých	některý	k3yIgInPc2	některý
prvků	prvek	k1gInPc2	prvek
glam	glam	k6eAd1	glam
rocku	rock	k1gInSc2	rock
kapely	kapela	k1gFnSc2	kapela
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
<g/>
,	,	kIx,	,
Iggy	Iggy	k1gInPc1	Iggy
Pop	pop	k1gInSc1	pop
nebo	nebo	k8xC	nebo
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Dolls	Dolls	k1gInSc1	Dolls
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
glam	glama	k1gFnPc2	glama
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jejíž	jejíž	k3xOyRp3gMnPc4	jejíž
představitele	představitel	k1gMnPc4	představitel
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Suzi	Suzi	k1gNnSc1	Suzi
Quatro	Quatro	k1gNnSc1	Quatro
nebo	nebo	k8xC	nebo
Sparks	Sparks	k1gInSc1	Sparks
<g/>
,	,	kIx,	,
dominovala	dominovat	k5eAaImAgFnS	dominovat
britským	britský	k2eAgFnPc3d1	britská
hitparádám	hitparáda	k1gFnPc3	hitparáda
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1974	[number]	k4	1974
a	a	k8xC	a
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
již	již	k6eAd1	již
existující	existující	k2eAgFnPc4d1	existující
kapely	kapela	k1gFnPc4	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
žánrem	žánr	k1gInSc7	žánr
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Queen	Queen	k2eAgMnSc1d1	Queen
a	a	k8xC	a
na	na	k7c4	na
čas	čas	k1gInSc4	čas
i	i	k8xC	i
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kapelu	kapela	k1gFnSc4	kapela
Kiss	Kissa	k1gFnPc2	Kissa
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
gothic	gothice	k1gFnPc2	gothice
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
glam	glam	k1gMnSc1	glam
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
také	také	k9	také
punk	punk	k1gInSc4	punk
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pomohl	pomoct	k5eAaPmAgInS	pomoct
ukončit	ukončit	k5eAaPmF	ukončit
slávu	sláva	k1gFnSc4	sláva
glam	gla	k1gNnSc7	gla
rocku	rock	k1gInSc2	rock
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
glam	glam	k6eAd1	glam
rock	rock	k1gInSc1	rock
dočkal	dočkat	k5eAaPmAgInS	dočkat
občasných	občasný	k2eAgInPc2d1	občasný
revivalů	revival	k1gInPc2	revival
např.	např.	kA	např.
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
The	The	k1gMnPc2	The
Darkness	Darknessa	k1gFnPc2	Darknessa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Punk	punk	k1gMnSc1	punk
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Punk	punk	k1gInSc1	punk
rock	rock	k1gInSc1	rock
jako	jako	k8xS	jako
takový	takový	k3xDgMnSc1	takový
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžeme	moct	k5eAaImIp1nP	moct
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
tzv.	tzv.	kA	tzv.
protopunkové	protopunkový	k2eAgFnSc2d1	protopunkový
kapely	kapela	k1gFnSc2	kapela
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
<g/>
,	,	kIx,	,
MC	MC	kA	MC
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Punk	punk	k1gMnSc1	punk
rock	rock	k1gInSc4	rock
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jako	jako	k8xC	jako
reakce	reakce	k1gFnSc1	reakce
mládeže	mládež	k1gFnSc2	mládež
na	na	k7c4	na
utahaná	utahaný	k2eAgNnPc4d1	utahané
a	a	k8xC	a
nabubřelá	nabubřelý	k2eAgNnPc4d1	nabubřelé
art	art	k?	art
rocková	rockový	k2eAgNnPc1d1	rockové
sóla	sólo	k1gNnPc1	sólo
<g/>
,	,	kIx,	,
na	na	k7c4	na
samolibost	samolibost	k1gFnSc4	samolibost
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
mainstreamového	mainstreamový	k2eAgInSc2d1	mainstreamový
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
především	především	k9	především
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
socio-ekonomickou	sociokonomický	k2eAgFnSc4d1	socio-ekonomická
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejznámější	známý	k2eAgFnSc2d3	nejznámější
kapely	kapela	k1gFnSc2	kapela
první	první	k4xOgFnSc2	první
punkové	punkový	k2eAgFnSc2d1	punková
vlny	vlna	k1gFnSc2	vlna
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaBmF	jmenovat
Ramones	Ramones	k1gMnSc1	Ramones
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Damned	Damned	k1gMnSc1	Damned
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Clash	Clasha	k1gFnPc2	Clasha
či	či	k8xC	či
Patti	Patť	k1gFnSc2	Patť
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
významné	významný	k2eAgInPc1d1	významný
-	-	kIx~	-
UK	UK	kA	UK
Subs	Subs	k1gInSc1	Subs
<g/>
,	,	kIx,	,
Social	Social	k1gInSc1	Social
Distortion	Distortion	k1gInSc1	Distortion
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
klasický	klasický	k2eAgInSc4d1	klasický
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
roll	roll	k1gInSc1	roll
a	a	k8xC	a
rock	rock	k1gInSc1	rock
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
povětšinou	povětšinou	k6eAd1	povětšinou
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
a	a	k8xC	a
plné	plný	k2eAgFnPc1d1	plná
emocí	emoce	k1gFnSc7	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
punk	punk	k1gMnSc1	punk
rock	rock	k1gInSc4	rock
radikalizoval	radikalizovat	k5eAaBmAgMnS	radikalizovat
a	a	k8xC	a
vykrystalizoval	vykrystalizovat	k5eAaPmAgMnS	vykrystalizovat
v	v	k7c6	v
Oi	Oi	k1gFnSc6	Oi
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Hardcore	Hardcor	k1gMnSc5	Hardcor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
žánry	žánr	k1gInPc1	žánr
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
míchat	míchat	k5eAaImF	míchat
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
rockovými	rockový	k2eAgInPc7d1	rockový
žánry	žánr	k1gInPc7	žánr
(	(	kIx(	(
<g/>
Psychedelic	Psychedelice	k1gFnPc2	Psychedelice
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
Blues	blues	k1gFnSc7	blues
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
Jazz	jazz	k1gInSc1	jazz
rockem	rock	k1gInSc7	rock
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
svět	svět	k1gInSc4	svět
punk	punk	k1gInSc1	punk
rocku	rock	k1gInSc2	rock
v	v	k7c6	v
ještě	ještě	k6eAd1	ještě
větším	veliký	k2eAgInSc6d2	veliký
světě	svět	k1gInSc6	svět
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Soft	Soft	k?	Soft
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
Hard	Hard	k1gInSc1	Hard
rock	rock	k1gInSc1	rock
a	a	k8xC	a
Heavy	Heava	k1gFnSc2	Heava
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
běžným	běžný	k2eAgInSc7d1	běžný
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
mainstreamový	mainstreamový	k2eAgInSc4d1	mainstreamový
rock	rock	k1gInSc4	rock
na	na	k7c4	na
soft	soft	k?	soft
rock	rock	k1gInSc4	rock
a	a	k8xC	a
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Soft	Soft	k?	Soft
rock	rock	k1gInSc1	rock
většinou	většinou	k6eAd1	většinou
čerpal	čerpat	k5eAaImAgInS	čerpat
z	z	k7c2	z
folk	folk	k1gInSc4	folk
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgInS	používat
akustické	akustický	k2eAgInPc4d1	akustický
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
melodii	melodie	k1gFnSc4	melodie
a	a	k8xC	a
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgMnPc4d1	hlavní
umělce	umělec	k1gMnPc4	umělec
patřili	patřit	k5eAaImAgMnP	patřit
Carole	Carole	k1gFnPc1	Carole
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Cat	Cat	k1gMnSc1	Cat
Stevens	Stevens	k1gInSc1	Stevens
a	a	k8xC	a
James	James	k1gMnSc1	James
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
působili	působit	k5eAaImAgMnP	působit
Billy	Bill	k1gMnPc4	Bill
Joel	Joel	k1gInSc1	Joel
<g/>
,	,	kIx,	,
America	America	k1gFnSc1	America
a	a	k8xC	a
předělaní	předělaný	k2eAgMnPc1d1	předělaný
Fleetwood	Fleetwood	k1gInSc4	Fleetwood
Mac	Mac	kA	Mac
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
album	album	k1gNnSc4	album
Rumours	Rumoursa	k1gFnPc2	Rumoursa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějších	prodávaný	k2eAgInPc2d3	nejprodávanější
albem	album	k1gNnSc7	album
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
hard	hard	k6eAd1	hard
rock	rock	k1gInSc1	rock
častěji	často	k6eAd2	často
čerpal	čerpat	k5eAaImAgInS	čerpat
z	z	k7c2	z
blues	blues	k1gNnSc2	blues
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
se	se	k3xPyFc4	se
hlasitěji	hlasitě	k6eAd2	hlasitě
a	a	k8xC	a
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
na	na	k7c4	na
rytmus	rytmus	k1gInSc4	rytmus
i	i	k9	i
jako	jako	k8xC	jako
sólový	sólový	k2eAgInSc4d1	sólový
nástroj	nástroj	k1gInSc4	nástroj
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
s	s	k7c7	s
overdrivem	overdriv	k1gInSc7	overdriv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgMnPc4d1	hlavní
umělce	umělec	k1gMnPc4	umělec
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc3	rock
patřily	patřit	k5eAaImAgFnP	patřit
kapely	kapela	k1gFnPc1	kapela
Britské	britský	k2eAgFnSc2d1	britská
invaze	invaze	k1gFnSc2	invaze
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
a	a	k8xC	a
The	The	k1gFnSc1	The
Kinks	Kinksa	k1gFnPc2	Kinksa
a	a	k8xC	a
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
skupiny	skupina	k1gFnSc2	skupina
Cream	Cream	k1gInSc1	Cream
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gFnPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
a	a	k8xC	a
The	The	k1gMnSc1	The
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hardrockové	hardrockový	k2eAgFnPc4d1	hardrocková
kapely	kapela	k1gFnPc4	kapela
konce	konec	k1gInSc2	konec
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
patří	patřit	k5eAaImIp3nS	patřit
Queen	Queen	k1gInSc1	Queen
<g/>
,	,	kIx,	,
Thin	Thin	k1gInSc1	Thin
Lizzy	Lizza	k1gFnSc2	Lizza
<g/>
,	,	kIx,	,
Aerosmith	Aerosmith	k1gInSc4	Aerosmith
a	a	k8xC	a
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
hard	hard	k1gInSc4	hard
rock	rock	k1gInSc1	rock
hraný	hraný	k2eAgInSc1d1	hraný
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc7d2	veliký
intenzitou	intenzita	k1gFnSc7	intenzita
nejdříve	dříve	k6eAd3	dříve
jako	jako	k9	jako
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
jako	jako	k9	jako
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
a	a	k8xC	a
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Blue	Blue	k1gNnSc1	Blue
Cheer	Cheer	k1gMnSc1	Cheer
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Funk	funk	k1gInSc4	funk
Railroad	Railroad	k1gInSc1	Railroad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
působily	působit	k5eAaImAgFnP	působit
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
tři	tři	k4xCgFnPc1	tři
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pomohly	pomoct	k5eAaPmAgFnP	pomoct
utvářet	utvářet	k5eAaImF	utvářet
tento	tento	k3xDgInSc1	tento
podžánr	podžánr	k1gInSc1	podžánr
-	-	kIx~	-
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
vlně	vlna	k1gFnSc6	vlna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přišla	přijít	k5eAaPmAgFnS	přijít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xS	jako
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
UFO	UFO	kA	UFO
<g/>
,	,	kIx,	,
Motörhead	Motörhead	k1gInSc1	Motörhead
<g/>
,	,	kIx,	,
Rainbow	Rainbow	k1gFnSc1	Rainbow
<g/>
,	,	kIx,	,
Kiss	Kiss	k1gInSc1	Kiss
<g/>
,	,	kIx,	,
Ted	Ted	k1gMnSc1	Ted
Nugent	Nugent	k1gMnSc1	Nugent
<g/>
,	,	kIx,	,
Blue	Blue	k1gFnSc1	Blue
Öyster	Öyster	k1gMnSc1	Öyster
Cult	Cult	k1gMnSc1	Cult
<g/>
,	,	kIx,	,
Rush	Rush	k1gMnSc1	Rush
a	a	k8xC	a
Scorpions	Scorpions	k1gInSc1	Scorpions
<g/>
.	.	kIx.	.
</s>
<s>
Heavy	Heava	k1gFnPc1	Heava
metal	metal	k1gInSc1	metal
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
značné	značný	k2eAgNnSc4d1	značné
publikum	publikum	k1gNnSc4	publikum
hlavně	hlavně	k9	hlavně
mezi	mezi	k7c7	mezi
dospívajícími	dospívající	k2eAgMnPc7d1	dospívající
muži	muž	k1gMnPc7	muž
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
vůdci	vůdce	k1gMnPc1	vůdce
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nemorální	morální	k2eNgInSc4d1	nemorální
<g/>
,	,	kIx,	,
protikřesťanský	protikřesťanský	k2eAgInSc4d1	protikřesťanský
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
démonický	démonický	k2eAgMnSc1d1	démonický
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
Ježíšovu	Ježíšův	k2eAgNnSc3d1	Ježíšovo
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
získalo	získat	k5eAaPmAgNnS	získat
několik	několik	k4yIc1	několik
umělců	umělec	k1gMnPc2	umělec
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
rocku	rock	k1gInSc2	rock
světový	světový	k2eAgInSc4d1	světový
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
Mark	Mark	k1gMnSc1	Mark
Farner	Farner	k1gMnSc1	Farner
<g/>
,	,	kIx,	,
Amy	Amy	k1gMnSc1	Amy
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Cliff	Cliff	k1gMnSc1	Cliff
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
