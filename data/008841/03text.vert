<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
stával	stávat	k5eAaImAgInS	stávat
dříve	dříve	k6eAd2	dříve
hrad	hrad	k1gInSc1	hrad
postavený	postavený	k2eAgInSc1d1	postavený
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
zemské	zemský	k2eAgFnSc2d1	zemská
obchodním	obchodní	k2eAgMnSc7d1	obchodní
stezky	stezka	k1gFnSc2	stezka
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Moravské	moravský	k2eAgFnSc2d1	Moravská
Sázavy	Sázava	k1gFnSc2	Sázava
k	k	k7c3	k
Tatenici	Tatenice	k1gFnSc3	Tatenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
zábřežská	zábřežský	k2eAgFnSc1d1	Zábřežská
tvrz	tvrz	k1gFnSc1	tvrz
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
držení	držení	k1gNnSc4	držení
Šternberků	Šternberk	k1gInPc2	Šternberk
a	a	k8xC	a
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
podstatně	podstatně	k6eAd1	podstatně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
a	a	k8xC	a
opevněna	opevnit	k5eAaPmNgFnS	opevnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
zábřežský	zábřežský	k2eAgInSc4d1	zábřežský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
příkrém	příkrý	k2eAgInSc6d1	příkrý
svahu	svah	k1gInSc6	svah
spadajícím	spadající	k2eAgInSc6d1	spadající
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Havlíčkovy	Havlíčkův	k2eAgFnSc2d1	Havlíčkova
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
chráněn	chránit	k5eAaImNgInS	chránit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kamennou	kamenný	k2eAgFnSc7d1	kamenná
zdí	zeď	k1gFnSc7	zeď
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
zbytky	zbytek	k1gInPc1	zbytek
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
zachovány	zachovat	k5eAaPmNgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgInS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
hlubokým	hluboký	k2eAgInSc7d1	hluboký
příkopem	příkop	k1gInSc7	příkop
a	a	k8xC	a
vcházelo	vcházet	k5eAaImAgNnS	vcházet
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
branou	braný	k2eAgFnSc4d1	braná
s	s	k7c7	s
předsazeným	předsazený	k2eAgInSc7d1	předsazený
vytahovacím	vytahovací	k2eAgInSc7d1	vytahovací
mostem	most	k1gInSc7	most
nacházejícím	nacházející	k2eAgInSc7d1	nacházející
se	se	k3xPyFc4	se
u	u	k7c2	u
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
části	část	k1gFnSc2	část
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
vedly	vést	k5eAaImAgFnP	vést
podzemní	podzemní	k2eAgFnPc1d1	podzemní
chodby	chodba	k1gFnPc1	chodba
–	–	k?	–
podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
údajně	údajně	k6eAd1	údajně
až	až	k9	až
na	na	k7c4	na
sousední	sousední	k2eAgInPc4d1	sousední
hrady	hrad	k1gInPc4	hrad
Brníčko	Brníčko	k1gNnSc4	Brníčko
a	a	k8xC	a
Hoštejn	Hoštejn	k1gNnSc4	Hoštejn
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
to	ten	k3xDgNnSc1	ten
však	však	k9	však
byly	být	k5eAaImAgInP	být
pouze	pouze	k6eAd1	pouze
únikové	únikový	k2eAgInPc1d1	únikový
východy	východ	k1gInPc1	východ
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
dostat	dostat	k5eAaPmF	dostat
za	za	k7c4	za
hradby	hradba	k1gFnPc4	hradba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
hluboká	hluboký	k2eAgFnSc1d1	hluboká
sklepení	sklepení	k1gNnSc6	sklepení
(	(	kIx(	(
<g/>
zčásti	zčásti	k6eAd1	zčásti
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
<g/>
)	)	kIx)	)
a	a	k8xC	a
část	část	k1gFnSc1	část
chodby	chodba	k1gFnSc2	chodba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
směřovala	směřovat	k5eAaImAgFnS	směřovat
přes	přes	k7c4	přes
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
k	k	k7c3	k
domu	dům	k1gInSc3	dům
"	"	kIx"	"
<g/>
Pod	pod	k7c7	pod
podloubím	podloubí	k1gNnSc7	podloubí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
parčíku	parčík	k1gInSc2	parčík
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
propadla	propadnout	k5eAaPmAgFnS	propadnout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
zasypána	zasypán	k2eAgFnSc1d1	zasypána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přešlo	přejít	k5eAaPmAgNnS	přejít
již	již	k6eAd1	již
zformované	zformovaný	k2eAgNnSc1d1	zformované
zábřežské	zábřežský	k2eAgNnSc1d1	zábřežské
panství	panství	k1gNnSc1	panství
na	na	k7c4	na
pány	pan	k1gMnPc4	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
prodal	prodat	k5eAaPmAgMnS	prodat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1442	[number]	k4	1442
panství	panství	k1gNnSc2	panství
Janu	Jan	k1gMnSc3	Jan
staršímu	starší	k1gMnSc3	starší
Tunklovi	Tunkl	k1gMnSc3	Tunkl
z	z	k7c2	z
Drahanovic	Drahanovice	k1gFnPc2	Drahanovice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
držení	držení	k1gNnSc4	držení
Tunklů	Tunkl	k1gInPc2	Tunkl
nastal	nastat	k5eAaPmAgInS	nastat
rozmach	rozmach	k1gInSc1	rozmach
zábřežského	zábřežský	k2eAgNnSc2d1	zábřežské
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
se	se	k3xPyFc4	se
o	o	k7c6	o
panství	panství	k1gNnSc6	panství
brníčské	brníčský	k2eAgFnPc1d1	brníčský
a	a	k8xC	a
hoštejnské	hoštejnské	k2eAgInSc1d1	hoštejnské
a	a	k8xC	a
zábřežský	zábřežský	k2eAgInSc1d1	zábřežský
hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rodovým	rodový	k2eAgNnSc7d1	rodové
sídlem	sídlo	k1gNnSc7	sídlo
(	(	kIx(	(
<g/>
Brníčko	Brníčko	k1gNnSc1	Brníčko
bylo	být	k5eAaImAgNnS	být
poničeno	poničit	k5eAaPmNgNnS	poničit
za	za	k7c2	za
česko-uherských	českoherský	k2eAgInPc2d1	česko-uherský
a	a	k8xC	a
Hoštejn	Hoštejna	k1gFnPc2	Hoštejna
za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zábřežské	zábřežský	k2eAgNnSc1d1	zábřežské
sídlo	sídlo	k1gNnSc1	sídlo
tak	tak	k6eAd1	tak
honosně	honosně	k6eAd1	honosně
vyzdobeno	vyzdoben	k2eAgNnSc1d1	vyzdobeno
a	a	k8xC	a
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Několikerou	několikerý	k4xRyIgFnSc7	několikerý
přestavbou	přestavba	k1gFnSc7	přestavba
objekt	objekt	k1gInSc1	objekt
úplně	úplně	k6eAd1	úplně
ztratil	ztratit	k5eAaPmAgInS	ztratit
charakter	charakter	k1gInSc1	charakter
opevněného	opevněný	k2eAgInSc2d1	opevněný
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
úpravy	úprava	k1gFnSc2	úprava
mu	on	k3xPp3gMnSc3	on
vtiskly	vtisknout	k5eAaPmAgInP	vtisknout
rysy	rys	k1gInPc1	rys
zámecké	zámecký	k2eAgFnSc2d1	zámecká
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
vzácná	vzácný	k2eAgFnSc1d1	vzácná
památka	památka	k1gFnSc1	památka
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
průjezdu	průjezd	k1gInSc6	průjezd
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
umístěná	umístěný	k2eAgFnSc1d1	umístěná
nad	nad	k7c7	nad
hradní	hradní	k2eAgFnSc7d1	hradní
branou	brána	k1gFnSc7	brána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
štít	štít	k1gInSc4	štít
vytesaný	vytesaný	k2eAgInSc4d1	vytesaný
z	z	k7c2	z
maletínského	maletínský	k2eAgInSc2d1	maletínský
pískovce	pískovec	k1gInSc2	pískovec
s	s	k7c7	s
bohatými	bohatý	k2eAgInPc7d1	bohatý
ornamenty	ornament	k1gInPc7	ornament
<g/>
,	,	kIx,	,
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
Tunklů	Tunkl	k1gInPc2	Tunkl
(	(	kIx(	(
<g/>
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Georgius	Georgius	k1gInSc1	Georgius
Tunkl	Tunkl	k1gInSc1	Tunkl
senior	senior	k1gMnSc1	senior
de	de	k?	de
Brniczko	Brniczka	k1gFnSc5	Brniczka
et	et	k?	et
in	in	k?	in
Zabrzeh	Zabrzeh	k1gInSc1	Zabrzeh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
letopočtem	letopočet	k1gInSc7	letopočet
1478	[number]	k4	1478
<g/>
.	.	kIx.	.
</s>
<s>
Nedostupnost	nedostupnost	k1gFnSc1	nedostupnost
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
následných	následný	k2eAgFnPc6d1	následná
úpravách	úprava	k1gFnPc6	úprava
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
podrobněji	podrobně	k6eAd2	podrobně
popsat	popsat	k5eAaPmF	popsat
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
charakter	charakter	k1gInSc4	charakter
stavebních	stavební	k2eAgFnPc2d1	stavební
etap	etapa	k1gFnPc2	etapa
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c4	vyjma
pozdně	pozdně	k6eAd1	pozdně
renesanční	renesanční	k2eAgFnSc4d1	renesanční
přístavbu	přístavba	k1gFnSc4	přístavba
hranaté	hranatý	k2eAgFnSc2d1	hranatá
úzké	úzký	k2eAgFnSc2d1	úzká
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
pouze	pouze	k6eAd1	pouze
objekt	objekt	k1gInSc4	objekt
kostelíka	kostelík	k1gInSc2	kostelík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
součástí	součást	k1gFnPc2	součást
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
Boskoviců	Boskoviec	k1gInPc2	Boskoviec
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
)	)	kIx)	)
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
komnaty	komnata	k1gFnPc4	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Zábřežská	zábřežský	k2eAgFnSc1d1	Zábřežská
farní	farní	k2eAgFnSc1d1	farní
kronika	kronika	k1gFnSc1	kronika
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kostelíku	kostelík	k1gInSc6	kostelík
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
probošt	probošt	k1gMnSc1	probošt
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
kapituly	kapitula	k1gFnSc2	kapitula
a	a	k8xC	a
světící	světící	k2eAgMnSc1d1	světící
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
Benedikt	Benedikt	k1gMnSc1	Benedikt
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
náhrobní	náhrobní	k2eAgInSc1d1	náhrobní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
kaple	kaple	k1gFnSc2	kaple
zasazen	zasadit	k5eAaPmNgInS	zasadit
v	v	k7c6	v
zámecké	zámecký	k2eAgFnSc6d1	zámecká
zdi	zeď	k1gFnSc6	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
památka	památka	k1gFnSc1	památka
se	se	k3xPyFc4	se
však	však	k9	však
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prodejem	prodej	k1gInSc7	prodej
zábřežského	zábřežský	k2eAgNnSc2d1	zábřežské
panství	panství	k1gNnSc2	panství
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Tunklem	Tunkl	k1gInSc7	Tunkl
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1510	[number]	k4	1510
<g/>
)	)	kIx)	)
přešel	přejít	k5eAaPmAgInS	přejít
majetek	majetek	k1gInSc1	majetek
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Trčky	Trčka	k1gMnSc2	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
a	a	k8xC	a
na	na	k7c6	na
Lichtenburce	Lichtenburka	k1gFnSc6	Lichtenburka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
nejen	nejen	k6eAd1	nejen
období	období	k1gNnSc1	období
budování	budování	k1gNnSc2	budování
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
období	období	k1gNnSc4	období
zábřežského	zábřežský	k2eAgNnSc2d1	zábřežské
panského	panské	k1gNnSc2	panské
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
majetky	majetek	k1gInPc1	majetek
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
mocných	mocný	k2eAgInPc2d1	mocný
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
bylo	být	k5eAaImAgNnS	být
zdejší	zdejší	k2eAgNnSc1d1	zdejší
sídlo	sídlo	k1gNnSc1	sídlo
jen	jen	k6eAd1	jen
místem	místo	k1gNnSc7	místo
dočasným	dočasný	k2eAgNnSc7d1	dočasné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
(	(	kIx(	(
<g/>
sídlící	sídlící	k2eAgFnSc1d1	sídlící
na	na	k7c6	na
Opočně	Opočno	k1gNnSc6	Opočno
<g/>
)	)	kIx)	)
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
zábřežské	zábřežský	k2eAgNnSc4d1	zábřežské
panství	panství	k1gNnSc4	panství
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
vybral	vybrat	k5eAaPmAgMnS	vybrat
Moravskou	moravský	k2eAgFnSc4d1	Moravská
Třebovou	Třebová	k1gFnSc4	Třebová
a	a	k8xC	a
na	na	k7c6	na
zábřežském	zábřežský	k2eAgInSc6d1	zábřežský
zámku	zámek	k1gInSc6	zámek
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sídlili	sídlit	k5eAaImAgMnP	sídlit
jen	jen	k9	jen
hejtmané	hejtmaný	k2eAgMnPc4d1	hejtmaný
a	a	k8xC	a
hospodářští	hospodářský	k2eAgMnPc1d1	hospodářský
správci	správce	k1gMnPc1	správce
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
(	(	kIx(	(
<g/>
pravnuk	pravnuk	k1gMnSc1	pravnuk
Ladislava	Ladislav	k1gMnSc2	Ladislav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1569	[number]	k4	1569
převzal	převzít	k5eAaPmAgMnS	převzít
správu	správa	k1gFnSc4	správa
obrovského	obrovský	k2eAgInSc2d1	obrovský
majetku	majetek	k1gInSc2	majetek
Boskoviců	Boskoviec	k1gInPc2	Boskoviec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
zábřežské	zábřežský	k2eAgNnSc4d1	zábřežské
sídlo	sídlo	k1gNnSc4	sídlo
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
a	a	k8xC	a
často	často	k6eAd1	často
zde	zde	k6eAd1	zde
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
dal	dát	k5eAaPmAgMnS	dát
přebudovat	přebudovat	k5eAaPmF	přebudovat
zámek	zámek	k1gInSc4	zámek
v	v	k7c4	v
renesanční	renesanční	k2eAgNnSc4d1	renesanční
sídlo	sídlo	k1gNnSc4	sídlo
–	–	k?	–
objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
patro	patro	k1gNnSc4	patro
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
sály	sál	k1gInPc7	sál
a	a	k8xC	a
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
do	do	k7c2	do
náměstí	náměstí	k1gNnSc2	náměstí
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
renesanční	renesanční	k2eAgFnSc4d1	renesanční
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
pivovar	pivovar	k1gInSc1	pivovar
a	a	k8xC	a
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
panský	panský	k2eAgInSc1d1	panský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
zde	zde	k6eAd1	zde
také	také	k9	také
sídlila	sídlit	k5eAaImAgFnS	sídlit
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
sestra	sestra	k1gFnSc1	sestra
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Žerotínová	Žerotínová	k1gFnSc1	Žerotínová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Velen	velen	k2eAgMnSc1d1	velen
ze	z	k7c2	z
Žerotína	Žerotína	k1gFnSc1	Žerotína
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
stal	stát	k5eAaPmAgMnS	stát
dědicem	dědic	k1gMnSc7	dědic
veškerého	veškerý	k3xTgInSc2	veškerý
majetku	majetek	k1gInSc2	majetek
Boskoviců	Boskoviec	k1gInPc2	Boskoviec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
si	se	k3xPyFc3	se
však	však	k9	však
vybral	vybrat	k5eAaPmAgMnS	vybrat
Moravskou	moravský	k2eAgFnSc7d1	Moravská
Třebovou	Třebová	k1gFnSc7	Třebová
a	a	k8xC	a
v	v	k7c6	v
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
prodléval	prodlévat	k5eAaImAgMnS	prodlévat
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
Boskoviců	Boskovice	k1gMnPc2	Boskovice
a	a	k8xC	a
Žerotínů	Žerotín	k1gMnPc2	Žerotín
prožívalo	prožívat	k5eAaImAgNnS	prožívat
léta	léto	k1gNnPc4	léto
blahobytu	blahobyt	k1gInSc2	blahobyt
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Častým	častý	k2eAgMnSc7d1	častý
obyvatelem	obyvatel	k1gMnSc7	obyvatel
zámku	zámek	k1gInSc2	zámek
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
jediný	jediný	k2eAgMnSc1d1	jediný
Velenův	Velenův	k2eAgMnSc1d1	Velenův
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgMnS	být
Ladislav	Ladislav	k1gMnSc1	Ladislav
Velen	velen	k2eAgMnSc1d1	velen
<g/>
,	,	kIx,	,
coby	coby	k?	coby
vojenský	vojenský	k2eAgMnSc1d1	vojenský
vůdce	vůdce	k1gMnSc1	vůdce
vzbouřených	vzbouřený	k2eAgInPc2d1	vzbouřený
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
nucen	nucen	k2eAgMnSc1d1	nucen
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
zkonfiskován	zkonfiskován	k2eAgInSc1d1	zkonfiskován
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
dostal	dostat	k5eAaPmAgMnS	dostat
zábřežské	zábřežský	k2eAgNnSc4d1	zábřežské
panství	panství	k1gNnSc4	panství
kníže	kníže	k1gMnSc1	kníže
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lichtenštejnských	lichtenštejnský	k2eAgNnPc2d1	Lichtenštejnské
panství	panství	k1gNnSc2	panství
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
zábřežský	zábřežský	k2eAgInSc1d1	zábřežský
zámek	zámek	k1gInSc1	zámek
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c4	na
pouhé	pouhý	k2eAgNnSc4d1	pouhé
sídlo	sídlo	k1gNnSc4	sídlo
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
zábřežské	zábřežský	k2eAgNnSc4d1	zábřežské
panství	panství	k1gNnSc4	panství
období	období	k1gNnSc2	období
úpadku	úpadek	k1gInSc2	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
ubytováni	ubytován	k2eAgMnPc1d1	ubytován
důstojníci	důstojník	k1gMnPc1	důstojník
valdštejnského	valdštejnský	k2eAgInSc2d1	valdštejnský
regimentu	regiment	k1gInSc2	regiment
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
zde	zde	k6eAd1	zde
značné	značný	k2eAgInPc4d1	značný
šrámy	šrám	k1gInPc4	šrám
způsobili	způsobit	k5eAaPmAgMnP	způsobit
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
částečně	částečně	k6eAd1	částečně
zbarokizován	zbarokizovat	k5eAaPmNgInS	zbarokizovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
kteréžto	kteréžto	k?	kteréžto
úpravy	úprava	k1gFnSc2	úprava
se	se	k3xPyFc4	se
památkou	památka	k1gFnSc7	památka
dochoval	dochovat	k5eAaPmAgInS	dochovat
pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
znak	znak	k1gInSc1	znak
Lichtenštejnů	Lichtenštejn	k1gMnPc2	Lichtenštejn
(	(	kIx(	(
<g/>
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
majitele	majitel	k1gMnSc2	majitel
Karla	Karel	k1gMnSc2	Karel
Eusebia	Eusebius	k1gMnSc2	Eusebius
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1727	[number]	k4	1727
<g/>
–	–	k?	–
<g/>
1736	[number]	k4	1736
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
větší	veliký	k2eAgFnSc3d2	veliký
přestavbě	přestavba	k1gFnSc3	přestavba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
potřeby	potřeba	k1gFnPc4	potřeba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
přistavěno	přistavěn	k2eAgNnSc1d1	přistavěno
severní	severní	k2eAgNnSc1d1	severní
zámecké	zámecký	k2eAgNnSc1d1	zámecké
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
období	období	k1gNnSc4	období
slezských	slezský	k2eAgFnPc2d1	Slezská
a	a	k8xC	a
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
procházely	procházet	k5eAaImAgFnP	procházet
Zábřehem	Zábřeh	k1gInSc7	Zábřeh
nejrůznější	různý	k2eAgNnPc1d3	nejrůznější
vojska	vojsko	k1gNnPc1	vojsko
(	(	kIx(	(
<g/>
pruská	pruský	k2eAgFnSc1d1	pruská
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
i	i	k8xC	i
francouzská	francouzský	k2eAgFnSc1d1	francouzská
–	–	k?	–
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
častým	častý	k2eAgNnSc7d1	časté
pleněním	plenění	k1gNnSc7	plenění
zámku	zámek	k1gInSc2	zámek
i	i	k8xC	i
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
i	i	k8xC	i
město	město	k1gNnSc1	město
neušetřily	ušetřit	k5eNaPmAgInP	ušetřit
ani	ani	k8xC	ani
živelní	živelní	k2eAgFnSc2d1	živelní
pohromy	pohroma	k1gFnSc2	pohroma
–	–	k?	–
největší	veliký	k2eAgInSc1d3	veliký
byl	být	k5eAaImAgInS	být
požár	požár	k1gInSc1	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zničil	zničit	k5eAaPmAgInS	zničit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
i	i	k8xC	i
část	část	k1gFnSc4	část
zámku	zámek	k1gInSc2	zámek
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
obnovena	obnoven	k2eAgFnSc1d1	obnovena
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgFnSc1d1	nynější
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Alois	Alois	k1gMnSc1	Alois
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
prodal	prodat	k5eAaPmAgMnS	prodat
městu	město	k1gNnSc3	město
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
pronajalo	pronajmout	k5eAaPmAgNnS	pronajmout
okresnímu	okresní	k2eAgNnSc3d1	okresní
hejtmanství	hejtmanství	k1gNnSc3	hejtmanství
a	a	k8xC	a
okresnímu	okresní	k2eAgInSc3d1	okresní
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
úřadů	úřad	k1gInPc2	úřad
slouží	sloužit	k5eAaImIp3nP	sloužit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zámek	zámek	k1gInSc1	zámek
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Tunklové	Tunklové	k2eAgInSc2d1	Tunklové
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
MěÚ	MěÚ	k1gFnSc1	MěÚ
</s>
</p>
