<s>
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ottová	Ottová	k1gFnSc1
</s>
<s>
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ottová	Ottová	k1gFnSc1
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ottová	Ottová	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
koncertu	koncert	k1gInSc6
v	v	k7c6
Cáchách	Cáchy	k1gFnPc6
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2011	#num#	k4
<g/>
Rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Alice	Alice	k1gFnSc1
Sara	Sar	k2eAgFnSc1d1
Ott	Ott	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1988	#num#	k4
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
SRN	SRN	kA
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Mozarteum	Mozarteum	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Mozarteum	Mozarteum	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
klasická	klasický	k2eAgFnSc1d1
klavíristka	klavíristka	k1gFnSc1
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
www.alicesaraott.com	www.alicesaraott.com	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ottová	Ottová	k1gFnSc1
<g/>
,	,	kIx,
nepřechýleně	přechýleně	k6eNd1
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ott	Ott	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pianistka	pianistka	k1gFnSc1
německo-japonského	německo-japonský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
interpretka	interpretka	k1gFnSc1
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
jako	jako	k9
dcera	dcera	k1gFnSc1
německého	německý	k2eAgMnSc2d1
otce	otec	k1gMnSc2
a	a	k8xC
japonské	japonský	k2eAgFnSc2d1
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klavírní	klavírní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Ve	v	k7c6
třech	tři	k4xCgInPc6
letech	let	k1gInPc6
navštívila	navštívit	k5eAaPmAgFnS
s	s	k7c7
rodiči	rodič	k1gMnPc7
hudební	hudební	k2eAgInSc4d1
recitál	recitál	k1gInSc4
a	a	k8xC
projevila	projevit	k5eAaPmAgFnS
velký	velký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
ji	on	k3xPp3gFnSc4
ve	v	k7c6
hře	hra	k1gFnSc6
na	na	k7c4
klavír	klavír	k1gInSc4
vedla	vést	k5eAaImAgFnS
maďarská	maďarský	k2eAgFnSc1d1
učitelka	učitelka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třinácti	třináct	k4xCc2
letech	léto	k1gNnPc6
si	se	k3xPyFc3
odnesla	odnést	k5eAaPmAgFnS
z	z	k7c2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
klavírní	klavírní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
akademie	akademie	k1gFnSc2
v	v	k7c6
japonském	japonský	k2eAgInSc6d1
Hamamacu	Hamamacus	k1gInSc6
titul	titul	k1gInSc4
„	„	k?
<g/>
nejslibnější	slibní	k2eAgMnSc1d3
umělec	umělec	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
pak	pak	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
klavírní	klavírní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
Silvia	Silvia	k1gFnSc1
Bengaliho	Bengali	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvítězila	zvítězit	k5eAaPmAgFnS
na	na	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Pianello	Pianello	k1gNnSc1
Val	val	k1gInSc4
Tidone	Tidon	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Studium	studium	k1gNnSc1
hry	hra	k1gFnSc2
na	na	k7c4
klavír	klavír	k1gInSc4
absolvovala	absolvovat	k5eAaPmAgFnS
v	v	k7c6
salzburském	salzburský	k2eAgInSc6d1
Mozarteu	Mozarteus	k1gInSc6
u	u	k7c2
profesora	profesor	k1gMnSc2
Karla-Heinze	Karla-Heinze	k1gFnSc2
Kämmerlinga	Kämmerling	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
německé	německý	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
vydavatelství	vydavatelství	k1gNnSc4
Deutsche	Deutsch	k1gFnSc2
Grammophon	Grammophona	k1gFnPc2
nahrála	nahrát	k5eAaBmAgFnS,k5eAaPmAgFnS
s	s	k7c7
Mnichovskými	mnichovský	k2eAgMnPc7d1
filharmoniky	filharmonik	k1gMnPc7
Koncert	koncert	k1gInSc4
pro	pro	k7c4
klavír	klavír	k1gInSc4
a	a	k8xC
orchestr	orchestr	k1gInSc4
č.	č.	k?
1	#num#	k4
b	b	k?
moll	moll	k1gNnSc6
od	od	k7c2
Čajkovského	Čajkovský	k2eAgNnSc2d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
Lisztovy	Lisztův	k2eAgFnPc1d1
Transcendentální	transcendentální	k2eAgFnPc1d1
etudy	etuda	k1gFnPc1
<g/>
,	,	kIx,
Chopinovy	Chopinův	k2eAgInPc1d1
Valčíky	valčík	k1gInPc1
či	či	k8xC
Beethovenovy	Beethovenův	k2eAgInPc1d1
klavírní	klavírní	k2eAgInPc1d1
koncerty	koncert	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Koncertuje	koncertovat	k5eAaImIp3nS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
2011	#num#	k4
účinkovala	účinkovat	k5eAaImAgFnS
spolu	spolu	k6eAd1
s	s	k7c7
Českou	český	k2eAgFnSc7d1
filharmonií	filharmonie	k1gFnSc7
jako	jako	k8xS,k8xC
sólistka	sólistka	k1gFnSc1
na	na	k7c6
zahajovacím	zahajovací	k2eAgInSc6d1
koncertu	koncert	k1gInSc6
festivalu	festival	k1gInSc2
Dvořákova	Dvořákův	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Alice	Alice	k1gFnSc2
Sara	Sara	k1gMnSc1
Ott	Ott	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Životopis	životopis	k1gInSc1
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
Sarah	Sarah	k1gFnSc1
Ott	Ott	k1gFnSc1
<g/>
;	;	kIx,
www.klassikakzente.de	www.klassikakzente.de	k6eAd1
<g/>
↑	↑	k?
Praha	Praha	k1gFnSc1
patří	patřit	k5eAaImIp3nS
Antonínu	Antonín	k1gMnSc3
Dvořákovi	Dvořák	k1gMnSc3
<g/>
,	,	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
8.9	8.9	k4
<g/>
.2011	.2011	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alice	Alice	k1gFnSc1
Sara	Sar	k2eAgFnSc1d1
Ottová	Ottová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ottová	Ottová	k1gFnSc1
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Alice	Alice	k1gFnSc1
Sara	Sara	k1gFnSc1
Ottová	Ottová	k1gFnSc1
na	na	k7c6
Deutsche	Deutsche	k1gNnSc6
Grammophon	Grammophona	k1gFnPc2
</s>
<s>
Pianist	Pianist	k1gMnSc1
Alice	Alice	k1gFnSc2
Sara	Sara	k1gMnSc1
Ott	Ott	k1gMnSc1
makes	makes	k1gMnSc1
her	hra	k1gFnPc2
first	first	k1gMnSc1
ever	ever	k1gMnSc1
concert	concert	k1gMnSc1
appearance	appearance	k1gFnSc2
in	in	k?
Turkey	Turkea	k1gFnSc2
<g/>
,	,	kIx,
Today	Todaa	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
zaman	zaman	k1gInSc1
<g/>
,	,	kIx,
17.3	17.3	k4
<g/>
.2010	.2010	k4
</s>
<s>
Die	Die	k?
Geheimnisse	Geheimnisse	k1gFnSc1
der	drát	k5eAaImRp2nS
Pianistin	Pianistin	k1gMnSc1
Alice	Alice	k1gFnSc2
Sara	Sara	k1gMnSc1
Ott	Ott	k1gMnSc1
<g/>
,	,	kIx,
BR-Online	BR-Onlin	k1gInSc5
</s>
<s>
Sagen	Sagen	k1gInSc1
Sie	Sie	k1gFnSc2
Jetzt	Jetzt	k2eAgInSc1d1
nichts	nichts	k1gInSc1
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
Sara	Sarus	k1gMnSc2
Ott	Ott	k1gMnSc2
<g/>
,	,	kIx,
Süd	Süd	k1gMnSc2
Deutsche	Deutsch	k1gMnSc2
<g/>
,	,	kIx,
13	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
142999	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
134292170	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7853	#num#	k4
7159	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2010029261	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
60294981	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2010029261	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
