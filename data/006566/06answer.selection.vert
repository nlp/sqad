<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
(	(	kIx(	(
<g/>
ER	ER	kA	ER
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgFnPc2d1	propojená
miniaturních	miniaturní	k2eAgFnPc2d1	miniaturní
membránových	membránový	k2eAgFnPc2d1	membránová
cisteren	cisterna	k1gFnPc2	cisterna
a	a	k8xC	a
kanálků	kanálek	k1gInPc2	kanálek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
