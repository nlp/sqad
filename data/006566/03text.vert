<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
(	(	kIx(	(
<g/>
ER	ER	kA	ER
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgFnPc2d1	propojená
miniaturních	miniaturní	k2eAgFnPc2d1	miniaturní
membránových	membránový	k2eAgFnPc2d1	membránová
cisteren	cisterna	k1gFnPc2	cisterna
a	a	k8xC	a
kanálků	kanálek	k1gInPc2	kanálek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Napojuje	napojovat	k5eAaImIp3nS	napojovat
se	se	k3xPyFc4	se
na	na	k7c4	na
buněčné	buněčný	k2eAgNnSc4d1	buněčné
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
na	na	k7c4	na
Golgiho	Golgi	k1gMnSc4	Golgi
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
povrch	povrch	k1gInSc4	povrch
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
metabolické	metabolický	k2eAgInPc4d1	metabolický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
drsné	drsný	k2eAgInPc1d1	drsný
ER	ER	kA	ER
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vnějším	vnější	k2eAgInSc6d1	vnější
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
ribozómy	ribozóma	k1gFnPc1	ribozóma
<g/>
,	,	kIx,	,
a	a	k8xC	a
hladké	hladký	k2eAgNnSc1d1	hladké
ER	ER	kA	ER
bez	bez	k7c2	bez
přisedlých	přisedlý	k2eAgInPc2d1	přisedlý
ribozómů	ribozóm	k1gInPc2	ribozóm
<g/>
.	.	kIx.	.
</s>
<s>
Drsná	drsný	k2eAgFnSc1d1	drsná
část	část	k1gFnSc1	část
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
syntézu	syntéza	k1gFnSc4	syntéza
některých	některý	k3yIgFnPc2	některý
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
procesy	proces	k1gInPc4	proces
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnSc1d1	související
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
skládání	skládání	k1gNnSc4	skládání
těchto	tento	k3xDgInPc2	tento
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
oligomerizace	oligomerizace	k1gFnSc2	oligomerizace
či	či	k8xC	či
navěšování	navěšování	k1gNnSc2	navěšování
jistých	jistý	k2eAgInPc2d1	jistý
cukerných	cukerný	k2eAgInPc2d1	cukerný
zbytků	zbytek	k1gInPc2	zbytek
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drsném	drsný	k2eAgInSc6d1	drsný
ER	ER	kA	ER
také	také	k6eAd1	také
probíhá	probíhat	k5eAaImIp3nS	probíhat
rozklad	rozklad	k1gInSc4	rozklad
špatně	špatně	k6eAd1	špatně
sbalených	sbalený	k2eAgFnPc2d1	sbalená
či	či	k8xC	či
poškozených	poškozený	k2eAgFnPc2d1	poškozená
bílkovin	bílkovina	k1gFnPc2	bílkovina
–	–	k?	–
mechanismus	mechanismus	k1gInSc1	mechanismus
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
ER-asociovaná	ERsociovaný	k2eAgFnSc1d1	ER-asociovaný
degradace	degradace	k1gFnSc1	degradace
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hladkém	hladký	k2eAgNnSc6d1	hladké
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgInPc1d1	odlišný
procesy	proces	k1gInPc1	proces
–	–	k?	–
odstraňování	odstraňování	k1gNnSc4	odstraňování
toxických	toxický	k2eAgFnPc2d1	toxická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
metabolismu	metabolismus	k1gInSc2	metabolismus
lipidů	lipid	k1gInPc2	lipid
a	a	k8xC	a
metabolismu	metabolismus	k1gInSc2	metabolismus
hemu	hem	k1gInSc2	hem
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
z	z	k7c2	z
hladkého	hladký	k2eAgInSc2d1	hladký
ER	ER	kA	ER
regulovaně	regulovaně	k6eAd1	regulovaně
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
pozorování	pozorování	k1gNnSc4	pozorování
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
již	již	k6eAd1	již
světelným	světelný	k2eAgInSc7d1	světelný
mikroskopem	mikroskop	k1gInSc7	mikroskop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výzkumníci	výzkumník	k1gMnPc1	výzkumník
neměli	mít	k5eNaImAgMnP	mít
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vlastně	vlastně	k9	vlastně
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
Nisslova	Nisslův	k2eAgNnPc4d1	Nisslův
tělíska	tělísko	k1gNnPc4	tělísko
objevená	objevený	k2eAgFnSc1d1	objevená
Franzem	Franz	k1gMnSc7	Franz
Nisslem	Nissl	k1gMnSc7	Nissl
v	v	k7c6	v
nervových	nervový	k2eAgFnPc6d1	nervová
buňkách	buňka	k1gFnPc6	buňka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
jako	jako	k8xC	jako
takové	takový	k3xDgFnPc4	takový
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
organelami	organela	k1gFnPc7	organela
objeveno	objevit	k5eAaPmNgNnS	objevit
až	až	k6eAd1	až
poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
elektronového	elektronový	k2eAgInSc2d1	elektronový
mikroskopu	mikroskop	k1gInSc2	mikroskop
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
A	a	k9	a
Study	stud	k1gInPc1	stud
of	of	k?	of
Tissue	Tissuus	k1gMnSc5	Tissuus
Culture	Cultur	k1gMnSc5	Cultur
Cells	Cells	k1gInSc1	Cells
by	by	kYmCp3nS	by
Electron	Electron	k1gInSc4	Electron
Microscopy	Microscopa	k1gFnSc2	Microscopa
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
Keith	Keith	k1gInSc4	Keith
R.	R.	kA	R.
Porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Claude	Claud	k1gInSc5	Claud
a	a	k8xC	a
Ernest	Ernest	k1gMnSc1	Ernest
F.	F.	kA	F.
Fullam	Fullam	k1gInSc1	Fullam
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
ve	v	k7c6	v
fixovaných	fixovaný	k2eAgFnPc6d1	fixovaná
ptačích	ptačí	k2eAgFnPc6d1	ptačí
buňkách	buňka	k1gFnPc6	buňka
jako	jako	k8xC	jako
systém	systém	k1gInSc4	systém
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgInPc2d1	propojený
váčků	váček	k1gInPc2	váček
a	a	k8xC	a
vláken	vlákno	k1gNnPc2	vlákno
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
kolem	kolem	k7c2	kolem
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
popisu	popis	k1gInSc6	popis
této	tento	k3xDgFnSc2	tento
struktury	struktura	k1gFnSc2	struktura
věnovali	věnovat	k5eAaPmAgMnP	věnovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
studie	studie	k1gFnPc4	studie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Keith	Keith	k1gInSc1	Keith
R.	R.	kA	R.
Porter	porter	k1gInSc1	porter
poprvé	poprvé	k6eAd1	poprvé
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
<g/>
"	"	kIx"	"
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
(	(	kIx(	(
<g/>
v	v	k7c6	v
endoplazmě	endoplazma	k1gFnSc6	endoplazma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
komplikované	komplikovaný	k2eAgFnSc3d1	komplikovaná
větvené	větvený	k2eAgFnSc3d1	větvená
struktuře	struktura	k1gFnSc3	struktura
–	–	k?	–
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
reticulum	reticulum	k1gInSc1	reticulum
<g/>
"	"	kIx"	"
=	=	kIx~	=
síť	síť	k1gFnSc1	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
všech	všecek	k3xTgNnPc2	všecek
eukaryotických	eukaryotický	k2eAgNnPc2d1	eukaryotické
(	(	kIx(	(
<g/>
jaderných	jaderný	k2eAgNnPc2d1	jaderné
<g/>
)	)	kIx)	)
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
velmi	velmi	k6eAd1	velmi
redukované	redukovaný	k2eAgInPc1d1	redukovaný
a	a	k8xC	a
sotva	sotva	k6eAd1	sotva
znatelné	znatelný	k2eAgNnSc1d1	znatelné
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc1d1	společný
předek	předek	k1gInSc1	předek
všech	všecek	k3xTgFnPc2	všecek
eukaryot	eukaryota	k1gFnPc2	eukaryota
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
funkční	funkční	k2eAgInSc1d1	funkční
systém	systém	k1gInSc1	systém
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
membrán	membrána	k1gFnPc2	membrána
včetně	včetně	k7c2	včetně
ER	ER	kA	ER
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
i	i	k9	i
rozšíření	rozšíření	k1gNnSc1	rozšíření
proteinových	proteinový	k2eAgFnPc2d1	proteinová
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
důležitých	důležitý	k2eAgFnPc2d1	důležitá
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
ER	ER	kA	ER
<g/>
,	,	kIx,	,
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgInPc7	všecek
eukaryotickými	eukaryotický	k2eAgInPc7d1	eukaryotický
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
univerzálně	univerzálně	k6eAd1	univerzálně
se	se	k3xPyFc4	se
vyskytujícím	vyskytující	k2eAgFnPc3d1	vyskytující
bílkovinám	bílkovina	k1gFnPc3	bílkovina
patří	patřit	k5eAaImIp3nP	patřit
coat	coat	k5eAaPmF	coat
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
SNARE	SNARE	kA	SNARE
<g/>
,	,	kIx,	,
GAPy	GAPa	k1gFnSc2	GAPa
a	a	k8xC	a
GEFy	GEFa	k1gFnSc2	GEFa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
vzniku	vznik	k1gInSc2	vznik
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
několik	několik	k4yIc1	několik
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Cavalier-Smith	Cavalier-Smith	k1gMnSc1	Cavalier-Smith
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ER	ER	kA	ER
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kontinuální	kontinuální	k2eAgInPc1d1	kontinuální
s	s	k7c7	s
membránou	membrána	k1gFnSc7	membrána
jaderného	jaderný	k2eAgInSc2d1	jaderný
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
a	a	k8xC	a
domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ER	ER	kA	ER
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
vlastně	vlastně	k9	vlastně
současně	současně	k6eAd1	současně
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
předka	předek	k1gMnSc2	předek
eukaryot	eukaryot	k1gInSc4	eukaryot
byla	být	k5eAaImAgFnS	být
DNA	dna	k1gFnSc1	dna
navázána	navázán	k2eAgFnSc1d1	navázána
u	u	k7c2	u
cytoplazmatické	cytoplazmatický	k2eAgFnSc2d1	cytoplazmatická
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
však	však	k9	však
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
navázána	navázán	k2eAgFnSc1d1	navázána
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odškrcení	odškrcení	k1gNnSc3	odškrcení
této	tento	k3xDgFnSc2	tento
cytoplasmatické	cytoplasmatický	k2eAgFnSc2d1	cytoplasmatická
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
váčku	váček	k1gInSc2	váček
s	s	k7c7	s
DNA	DNA	kA	DNA
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Membránový	membránový	k2eAgInSc1d1	membránový
váček	váček	k1gInSc1	váček
se	se	k3xPyFc4	se
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
postupně	postupně	k6eAd1	postupně
nařasil	nařasit	k5eAaPmAgInS	nařasit
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
systém	systém	k1gInSc4	systém
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
specializaci	specializace	k1gFnSc3	specializace
systému	systém	k1gInSc2	systém
jaderné	jaderný	k2eAgFnSc2d1	jaderná
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
membrán	membrána	k1gFnPc2	membrána
ER	ER	kA	ER
–	–	k?	–
tyto	tento	k3xDgFnPc4	tento
membrány	membrána	k1gFnPc4	membrána
totiž	totiž	k9	totiž
začaly	začít	k5eAaPmAgFnP	začít
využívat	využívat	k5eAaPmF	využívat
kotranslační	kotranslační	k2eAgInSc4d1	kotranslační
transport	transport	k1gInSc4	transport
pomocí	pomocí	k7c2	pomocí
částice	částice	k1gFnSc2	částice
SRP	srp	k1gInSc1	srp
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
neumí	umět	k5eNaImIp3nS	umět
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vypracovány	vypracovat	k5eAaPmNgFnP	vypracovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohl	moct	k5eAaImAgMnS	moct
tento	tento	k3xDgInSc4	tento
kotranslační	kotranslační	k2eAgInSc4d1	kotranslační
transport	transport	k1gInSc4	transport
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
proteiny	protein	k1gInPc1	protein
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
potřebné	potřebný	k2eAgInPc1d1	potřebný
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
právě	právě	k9	právě
jen	jen	k9	jen
díky	díky	k7c3	díky
kotranslačnímu	kotranslační	k2eAgInSc3d1	kotranslační
transportu	transport	k1gInSc3	transport
<g/>
.	.	kIx.	.
</s>
<s>
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
dynamický	dynamický	k2eAgInSc1d1	dynamický
systém	systém	k1gInSc1	systém
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgInPc2d1	propojený
váčků	váček	k1gInPc2	váček
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yRgNnSc2	který
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
čilému	čilý	k2eAgInSc3d1	čilý
transportu	transport	k1gInSc3	transport
lipidů	lipid	k1gInPc2	lipid
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
projde	projít	k5eAaPmIp3nS	projít
endoplazmatickým	endoplazmatický	k2eAgNnSc7d1	endoplazmatické
retikulem	retikulum	k1gNnSc7	retikulum
mezi	mezi	k7c4	mezi
2	[number]	k4	2
a	a	k8xC	a
10	[number]	k4	10
miliony	milion	k4xCgInPc4	milion
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
jen	jen	k9	jen
málokterá	málokterý	k3yIgFnSc1	málokterý
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
měřítku	měřítko	k1gNnSc6	měřítko
udržována	udržovat	k5eAaImNgFnS	udržovat
přibližně	přibližně	k6eAd1	přibližně
konstantní	konstantní	k2eAgFnSc1d1	konstantní
velikost	velikost	k1gFnSc1	velikost
ER	ER	kA	ER
a	a	k8xC	a
rozložení	rozložení	k1gNnSc1	rozložení
jeho	jeho	k3xOp3gInPc2	jeho
váčků	váček	k1gInPc2	váček
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
udržování	udržování	k1gNnSc6	udržování
rovnováhy	rovnováha	k1gFnSc2	rovnováha
v	v	k7c6	v
ER	ER	kA	ER
hrají	hrát	k5eAaImIp3nP	hrát
molekulární	molekulární	k2eAgInPc4d1	molekulární
motory	motor	k1gInPc4	motor
asociované	asociovaný	k2eAgInPc4d1	asociovaný
s	s	k7c7	s
mikrotubuly	mikrotubul	k1gMnPc7	mikrotubul
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
roztahovat	roztahovat	k5eAaImF	roztahovat
váčky	váček	k1gInPc4	váček
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
do	do	k7c2	do
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
sektoru	sektor	k1gInSc2	sektor
cytosolu	cytosol	k1gInSc2	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
představuje	představovat	k5eAaImIp3nS	představovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozsahem	rozsah	k1gInSc7	rozsah
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
část	část	k1gFnSc1	část
systému	systém	k1gInSc2	systém
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
membrán	membrána	k1gFnPc2	membrána
v	v	k7c6	v
eukaryotické	eukaryotický	k2eAgFnSc6d1	eukaryotická
buňce	buňka	k1gFnSc6	buňka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
endomembránového	endomembránový	k2eAgInSc2d1	endomembránový
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
zaujímat	zaujímat	k5eAaImF	zaujímat
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
trojrozměrným	trojrozměrný	k2eAgInSc7d1	trojrozměrný
systémem	systém	k1gInSc7	systém
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgFnPc2d1	propojená
trubic	trubice	k1gFnPc2	trubice
či	či	k8xC	či
plochých	plochý	k2eAgInPc2d1	plochý
váčků	váček	k1gInPc2	váček
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cisteren	cisterna	k1gFnPc2	cisterna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
sahat	sahat	k5eAaImF	sahat
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
až	až	k6eAd1	až
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
povrch	povrch	k1gInSc1	povrch
membrán	membrána	k1gFnPc2	membrána
ER	ER	kA	ER
např.	např.	kA	např.
u	u	k7c2	u
jaterních	jaterní	k2eAgFnPc2d1	jaterní
buněk	buňka	k1gFnPc2	buňka
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
povrchu	povrch	k1gInSc2	povrch
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
(	(	kIx(	(
<g/>
u	u	k7c2	u
exokrinních	exokrinní	k2eAgFnPc2d1	exokrinní
buněk	buňka	k1gFnPc2	buňka
slinivky	slinivka	k1gFnSc2	slinivka
dokonce	dokonce	k9	dokonce
60	[number]	k4	60
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
Leydigových	Leydigův	k2eAgFnPc6d1	Leydigův
buňkách	buňka	k1gFnPc6	buňka
dokonce	dokonce	k9	dokonce
71	[number]	k4	71
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
povrch	povrch	k1gInSc1	povrch
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
v	v	k7c6	v
jaterních	jaterní	k2eAgFnPc6d1	jaterní
buňkách	buňka	k1gFnPc6	buňka
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
56	[number]	k4	56
000	[number]	k4	000
μ	μ	k?	μ
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
v	v	k7c6	v
(	(	kIx(	(
<g/>
objemově	objemově	k6eAd1	objemově
mnohem	mnohem	k6eAd1	mnohem
menších	malý	k2eAgFnPc2d2	menší
<g/>
)	)	kIx)	)
exokrinních	exokrinní	k2eAgFnPc6d1	exokrinní
buňkách	buňka	k1gFnPc6	buňka
slinivky	slinivka	k1gFnSc2	slinivka
asi	asi	k9	asi
7	[number]	k4	7
800	[number]	k4	800
μ	μ	k?	μ
<g/>
2	[number]	k4	2
a	a	k8xC	a
v	v	k7c6	v
Leydigových	Leydigův	k2eAgFnPc6d1	Leydigův
buňkách	buňka	k1gFnPc6	buňka
asi	asi	k9	asi
32	[number]	k4	32
000	[number]	k4	000
μ	μ	k?	μ
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
opačné	opačný	k2eAgInPc4d1	opačný
extrémy	extrém	k1gInPc4	extrém
<g/>
:	:	kIx,	:
ve	v	k7c6	v
zralých	zralý	k2eAgFnPc6d1	zralá
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
se	se	k3xPyFc4	se
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vypuzeno	vypudit	k5eAaPmNgNnS	vypudit
společně	společně	k6eAd1	společně
s	s	k7c7	s
jádrem	jádro	k1gNnSc7	jádro
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zrání	zrání	k1gNnSc2	zrání
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
udávalo	udávat	k5eAaImAgNnS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ER	ER	kA	ER
chybí	chybět	k5eAaImIp3nS	chybět
také	také	k9	také
u	u	k7c2	u
prvoka	prvok	k1gMnSc2	prvok
E.	E.	kA	E.
histolytica	histolyticus	k1gMnSc2	histolyticus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkci	funkce	k1gFnSc4	funkce
ER	ER	kA	ER
zastává	zastávat	k5eAaImIp3nS	zastávat
systém	systém	k1gInSc1	systém
izolovaných	izolovaný	k2eAgInPc2d1	izolovaný
váčků	váček	k1gInPc2	váček
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
fluorescenčních	fluorescenční	k2eAgFnPc2d1	fluorescenční
technik	technika	k1gFnPc2	technika
(	(	kIx(	(
<g/>
GFP	GFP	kA	GFP
fúzní	fúzní	k2eAgInSc1d1	fúzní
protein	protein	k1gInSc1	protein
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
FLIP	FLIP	kA	FLIP
<g/>
)	)	kIx)	)
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
tohoto	tento	k3xDgMnSc4	tento
prvoka	prvok	k1gMnSc4	prvok
nakonec	nakonec	k6eAd1	nakonec
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
i	i	k8xC	i
opravdové	opravdový	k2eAgNnSc1d1	opravdové
membránové	membránový	k2eAgNnSc1d1	membránové
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
při	při	k7c6	při
srovnávání	srovnávání	k1gNnSc6	srovnávání
různých	různý	k2eAgInPc2d1	různý
buněčných	buněčný	k2eAgInPc2d1	buněčný
typů	typ	k1gInPc2	typ
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
pankreatické	pankreatický	k2eAgFnPc1d1	pankreatická
acinární	acinární	k2eAgFnPc1d1	acinární
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
fibroblasty	fibroblast	k1gInPc1	fibroblast
a	a	k8xC	a
plazmatické	plazmatický	k2eAgFnPc1d1	plazmatická
buňky	buňka	k1gFnPc1	buňka
mají	mít	k5eAaImIp3nP	mít
obrovské	obrovský	k2eAgNnSc4d1	obrovské
drsné	drsný	k2eAgNnSc4d1	drsné
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
vysokou	vysoký	k2eAgFnSc7d1	vysoká
proteosyntetickou	proteosyntetický	k2eAgFnSc7d1	proteosyntetický
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
spoustu	spousta	k1gFnSc4	spousta
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
trávicí	trávicí	k2eAgInPc1d1	trávicí
enzymy	enzym	k1gInPc1	enzym
v	v	k7c6	v
případě	případ	k1gInSc6	případ
acinárních	acinární	k2eAgFnPc2d1	acinární
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
kolagen	kolagen	k1gInSc1	kolagen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
fibroblastů	fibroblast	k1gInPc2	fibroblast
či	či	k8xC	či
protilátky	protilátka	k1gFnSc2	protilátka
v	v	k7c6	v
případě	případ	k1gInSc6	případ
plazmatických	plazmatický	k2eAgFnPc2d1	plazmatická
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
hladkého	hladký	k2eAgNnSc2d1	hladké
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
steroidy	steroid	k1gInPc4	steroid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
kůry	kůra	k1gFnSc2	kůra
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c6	v
hepatocytech	hepatocyt	k1gInPc6	hepatocyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
svalových	svalový	k2eAgFnPc6d1	svalová
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
sarkoplazmatické	sarkoplazmatický	k2eAgNnSc1d1	sarkoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Cisterny	cisterna	k1gFnPc1	cisterna
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
jsou	být	k5eAaImIp3nP	být
obaleny	obalen	k2eAgInPc1d1	obalen
buněčnou	buněčný	k2eAgFnSc7d1	buněčná
membránou	membrána	k1gFnSc7	membrána
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
složením	složení	k1gNnSc7	složení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
membrána	membrána	k1gFnSc1	membrána
ER	ER	kA	ER
v	v	k7c6	v
potkaních	potkaní	k2eAgInPc6d1	potkaní
hepatocytech	hepatocyt	k1gInPc6	hepatocyt
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
z	z	k7c2	z
53	[number]	k4	53
%	%	kIx~	%
fosfatidylcholin	fosfatidylcholina	k1gFnPc2	fosfatidylcholina
<g/>
,	,	kIx,	,
z	z	k7c2	z
20,2	[number]	k4	20,2
%	%	kIx~	%
fosfatidylethanolamin	fosfatidylethanolamin	k1gInSc1	fosfatidylethanolamin
<g/>
,	,	kIx,	,
z	z	k7c2	z
3,7	[number]	k4	3,7
%	%	kIx~	%
sfingomyelin	sfingomyelina	k1gFnPc2	sfingomyelina
a	a	k8xC	a
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
14,1	[number]	k4	14,1
%	%	kIx~	%
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
fosfatidylserin	fosfatidylserin	k1gInSc4	fosfatidylserin
a	a	k8xC	a
fosfatidylinositol	fosfatidylinositol	k1gInSc4	fosfatidylinositol
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
odchylkami	odchylka	k1gFnPc7	odchylka
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
i	i	k8xC	i
např.	např.	kA	např.
pro	pro	k7c4	pro
kořenové	kořenový	k2eAgFnPc4d1	kořenová
buňky	buňka	k1gFnPc4	buňka
ječmene	ječmen	k1gInSc2	ječmen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kvasinky	kvasinka	k1gFnSc2	kvasinka
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisiae	k6eAd1	cerevisiae
je	být	k5eAaImIp3nS	být
složení	složení	k1gNnSc4	složení
rovněž	rovněž	k9	rovněž
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
jen	jen	k9	jen
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
fosfatidylethanolaminu	fosfatidylethanolamin	k1gInSc2	fosfatidylethanolamin
(	(	kIx(	(
<g/>
33,4	[number]	k4	33,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
fosfatidylcholinu	fosfatidylcholin	k1gInSc2	fosfatidylcholin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
membráně	membrána	k1gFnSc6	membrána
ER	ER	kA	ER
se	se	k3xPyFc4	se
udávají	udávat	k5eAaImIp3nP	udávat
i	i	k9	i
stopová	stopový	k2eAgNnPc4d1	stopové
množství	množství	k1gNnPc4	množství
signálních	signální	k2eAgInPc2d1	signální
fosfolipidů	fosfolipid	k1gInPc2	fosfolipid
(	(	kIx(	(
<g/>
DAG	dag	kA	dag
<g/>
,	,	kIx,	,
CDP-DAG	CDP-DAG	k1gFnSc1	CDP-DAG
<g/>
,	,	kIx,	,
PA	Pa	kA	Pa
a	a	k8xC	a
lysofosfolipidů	lysofosfolipid	k1gMnPc2	lysofosfolipid
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
dolicholu	dolichola	k1gFnSc4	dolichola
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
membránami	membrána	k1gFnPc7	membrána
extrémně	extrémně	k6eAd1	extrémně
chudá	chudý	k2eAgFnSc1d1	chudá
na	na	k7c4	na
steroly	sterol	k1gInPc4	sterol
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
)	)	kIx)	)
a	a	k8xC	a
složitější	složitý	k2eAgInPc4d2	složitější
sfingolipidy	sfingolipid	k1gInPc4	sfingolipid
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
cholesterolu	cholesterol	k1gInSc2	cholesterol
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
sterol	sterol	k1gInSc1	sterol
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
syntetizován	syntetizován	k2eAgMnSc1d1	syntetizován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
transportován	transportován	k2eAgInSc1d1	transportován
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
organel	organela	k1gFnPc2	organela
<g/>
.	.	kIx.	.
</s>
<s>
Lumen	lumen	k1gNnSc1	lumen
(	(	kIx(	(
<g/>
dutina	dutina	k1gFnSc1	dutina
<g/>
)	)	kIx)	)
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
má	mít	k5eAaImIp3nS	mít
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
toto	tento	k3xDgNnSc1	tento
prostředí	prostředí	k1gNnSc1	prostředí
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
cytosolu	cytosol	k1gInSc2	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
proteomu	proteom	k1gInSc6	proteom
a	a	k8xC	a
metabolomu	metabolom	k1gInSc3	metabolom
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Panují	panovat	k5eAaImIp3nP	panovat
zde	zde	k6eAd1	zde
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
redoxní	redoxní	k2eAgFnPc1d1	redoxní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yRgInPc2	který
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
disulfidické	disulfidický	k2eAgInPc1d1	disulfidický
můstky	můstek	k1gInPc1	můstek
(	(	kIx(	(
<g/>
oxidací	oxidace	k1gFnSc7	oxidace
SH	SH	kA	SH
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
pyridinové	pyridinový	k2eAgInPc1d1	pyridinový
nukleotidy	nukleotid	k1gInPc1	nukleotid
(	(	kIx(	(
<g/>
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
metabolických	metabolický	k2eAgFnPc2d1	metabolická
drah	draha	k1gFnPc2	draha
<g/>
)	)	kIx)	)
udržují	udržovat	k5eAaImIp3nP	udržovat
v	v	k7c6	v
redukovaném	redukovaný	k2eAgInSc6d1	redukovaný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
anomálií	anomálie	k1gFnSc7	anomálie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
<g/>
:	:	kIx,	:
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
buňkách	buňka	k1gFnPc6	buňka
až	až	k9	až
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
vyšší	vysoký	k2eAgFnSc2d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1	[number]	k4	1
mM	mm	kA	mm
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
0,1	[number]	k4	0,1
μ	μ	k?	μ
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pH	ph	kA	ph
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neutrální	neutrální	k2eAgFnSc6d1	neutrální
<g/>
,	,	kIx,	,
srovnatelné	srovnatelný	k2eAgFnSc6d1	srovnatelná
s	s	k7c7	s
cytosolem	cytosol	k1gInSc7	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
morfologicky	morfologicky	k6eAd1	morfologicky
odlišitelných	odlišitelný	k2eAgFnPc2d1	odlišitelná
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
volně	volně	k6eAd1	volně
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
a	a	k8xC	a
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
např.	např.	kA	např.
zamezit	zamezit	k5eAaPmF	zamezit
difuzi	difuze	k1gFnSc3	difuze
proteinů	protein	k1gInPc2	protein
přítomných	přítomný	k1gMnPc2	přítomný
uvnitř	uvnitř	k7c2	uvnitř
ER	ER	kA	ER
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
celý	celý	k2eAgInSc4d1	celý
prostor	prostor	k1gInSc4	prostor
ER	ER	kA	ER
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
uváděnými	uváděný	k2eAgFnPc7d1	uváděná
částmi	část	k1gFnPc7	část
ER	ER	kA	ER
jsou	být	k5eAaImIp3nP	být
drsné	drsný	k2eAgNnSc4d1	drsné
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
(	(	kIx(	(
<g/>
RER	RER	kA	RER
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
rough	rougha	k1gFnPc2	rougha
<g/>
)	)	kIx)	)
a	a	k8xC	a
hladké	hladký	k2eAgNnSc1d1	hladké
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
(	(	kIx(	(
<g/>
SER	srát	k5eAaImRp2nS	srát
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
smooth	smooth	k1gInSc1	smooth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
drsným	drsný	k2eAgMnSc7d1	drsný
a	a	k8xC	a
hladkým	hladký	k2eAgMnSc7d1	hladký
ER	ER	kA	ER
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
ribozomů	ribozom	k1gInPc2	ribozom
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
drsného	drsný	k2eAgNnSc2d1	drsné
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
hladkém	hladký	k2eAgInSc6d1	hladký
tyto	tento	k3xDgFnPc4	tento
struktury	struktura	k1gFnPc1	struktura
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
uvádět	uvádět	k5eAaImF	uvádět
jako	jako	k9	jako
třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
ER	ER	kA	ER
ještě	ještě	k6eAd1	ještě
jaderný	jaderný	k2eAgInSc4d1	jaderný
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
ER	ER	kA	ER
liší	lišit	k5eAaImIp3nP	lišit
přítomností	přítomnost	k1gFnSc7	přítomnost
jaderných	jaderný	k2eAgInPc2d1	jaderný
pórů	pór	k1gInPc2	pór
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
na	na	k7c4	na
chromatin	chromatin	k1gInSc4	chromatin
a	a	k8xC	a
jadernou	jaderný	k2eAgFnSc4d1	jaderná
laminu	lamin	k2eAgFnSc4d1	lamina
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
část	část	k1gFnSc4	část
ER	ER	kA	ER
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
představovat	představovat	k5eAaImF	představovat
desmotubuly	desmotubul	k1gInPc4	desmotubul
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgMnPc1d1	procházející
skrz	skrz	k7c4	skrz
plazmodezmata	plazmodezma	k1gNnPc4	plazmodezma
mezi	mezi	k7c7	mezi
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
C.	C.	kA	C.
Pollard	Pollard	k1gMnSc1	Pollard
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc4	část
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
subdomény	subdomén	k1gInPc4	subdomén
a	a	k8xC	a
přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
ještě	ještě	k6eAd1	ještě
"	"	kIx"	"
<g/>
exportní	exportní	k2eAgFnSc4d1	exportní
zónu	zóna	k1gFnSc4	zóna
ER	ER	kA	ER
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
váčky	váček	k1gInPc1	váček
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
do	do	k7c2	do
sekretorické	sekretorický	k2eAgFnSc2d1	sekretorická
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
kontaktní	kontaktní	k2eAgFnSc4d1	kontaktní
zónu	zóna	k1gFnSc4	zóna
ER	ER	kA	ER
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
lipidů	lipid	k1gInPc2	lipid
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
membránovými	membránový	k2eAgFnPc7d1	membránová
organelami	organela	k1gFnPc7	organela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
drsného	drsný	k2eAgNnSc2d1	drsné
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
úpravě	úprava	k1gFnSc6	úprava
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc3	jejich
transportu	transport	k1gInSc3	transport
a	a	k8xC	a
rozkladu	rozklad	k1gInSc3	rozklad
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kotranslační	kotranslační	k2eAgFnSc2d1	kotranslační
translokace	translokace	k1gFnSc2	translokace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drsném	drsný	k2eAgNnSc6d1	drsné
ER	ER	kA	ER
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
(	(	kIx(	(
<g/>
translaci	translace	k1gFnSc3	translace
<g/>
)	)	kIx)	)
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgFnPc2	všecek
transmembránových	transmembránův	k2eAgFnPc2d1	transmembránův
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
trvale	trvale	k6eAd1	trvale
zabudovány	zabudovat	k5eAaPmNgInP	zabudovat
do	do	k7c2	do
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
proteiny	protein	k1gInPc1	protein
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
cytoplazmatickou	cytoplazmatický	k2eAgFnSc4d1	cytoplazmatická
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k8xC	i
např.	např.	kA	např.
pro	pro	k7c4	pro
membránu	membrána	k1gFnSc4	membrána
Golgiho	Golgi	k1gMnSc2	Golgi
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
buněčného	buněčný	k2eAgNnSc2d1	buněčné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
endozomů	endozom	k1gInPc2	endozom
<g/>
,	,	kIx,	,
lyzozomů	lyzozom	k1gInPc2	lyzozom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
membránu	membrána	k1gFnSc4	membrána
samotného	samotný	k2eAgNnSc2d1	samotné
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
uvnitř	uvnitř	k7c2	uvnitř
ER	ER	kA	ER
<g/>
,	,	kIx,	,
Golgiho	Golgi	k1gMnSc2	Golgi
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
endozomů	endozom	k1gInPc2	endozom
<g/>
,	,	kIx,	,
lysozomů	lysozom	k1gInPc2	lysozom
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
složek	složka	k1gFnPc2	složka
sekreční	sekreční	k2eAgFnSc2d1	sekreční
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
většiny	většina	k1gFnSc2	většina
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
následně	následně	k6eAd1	následně
uvolňovány	uvolňovat	k5eAaImNgFnP	uvolňovat
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
do	do	k7c2	do
mimobuněčného	mimobuněčný	k2eAgInSc2d1	mimobuněčný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Translace	translace	k1gFnSc1	translace
na	na	k7c6	na
drsném	drsný	k2eAgNnSc6d1	drsné
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
bílkovin	bílkovina	k1gFnPc2	bílkovina
docházet	docházet	k5eAaImF	docházet
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
translace	translace	k1gFnPc4	translace
na	na	k7c6	na
volných	volný	k2eAgInPc6d1	volný
ribozomech	ribozom	k1gInPc6	ribozom
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Translace	translace	k1gFnSc1	translace
proteinů	protein	k1gInPc2	protein
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
ER	ER	kA	ER
začíná	začínat	k5eAaImIp3nS	začínat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
rozeznány	rozeznat	k5eAaPmNgFnP	rozeznat
jisté	jistý	k2eAgFnPc1d1	jistá
signální	signální	k2eAgFnPc1d1	signální
sekvence	sekvence	k1gFnPc1	sekvence
nacházející	nacházející	k2eAgFnPc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vyráběného	vyráběný	k2eAgInSc2d1	vyráběný
proteinu	protein	k1gInSc2	protein
(	(	kIx(	(
<g/>
k	k	k7c3	k
rozeznání	rozeznání	k1gNnSc3	rozeznání
dojde	dojít	k5eAaPmIp3nS	dojít
pomocí	pomocí	k7c2	pomocí
signál	signál	k1gInSc4	signál
rozpoznávající	rozpoznávající	k2eAgFnSc2d1	rozpoznávající
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
SRP	srp	k1gInSc1	srp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	být	k5eAaImIp3nS	být
protein	protein	k1gInSc1	protein
naveden	navést	k5eAaPmNgInS	navést
na	na	k7c4	na
SRP	srp	k1gInSc4	srp
receptor	receptor	k1gInSc4	receptor
na	na	k7c6	na
membráně	membrána	k1gFnSc6	membrána
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
a	a	k8xC	a
translace	translace	k1gFnSc2	translace
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
spuštěna	spustit	k5eAaPmNgFnS	spustit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
SRP	srp	k1gInSc1	srp
receptoru	receptor	k1gInSc2	receptor
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vazbě	vazba	k1gFnSc3	vazba
na	na	k7c4	na
speciální	speciální	k2eAgInSc4d1	speciální
membránový	membránový	k2eAgInSc4d1	membránový
kanál	kanál	k1gInSc4	kanál
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
který	který	k3yRgInSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
polypeptid	polypeptid	k1gInSc1	polypeptid
soukán	soukat	k5eAaImNgInS	soukat
do	do	k7c2	do
lumen	lumen	k1gInSc1	lumen
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
speciální	speciální	k2eAgInPc4d1	speciální
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
například	například	k6eAd1	například
vyrobit	vyrobit	k5eAaPmF	vyrobit
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
membránu	membrána	k1gFnSc4	membrána
překračují	překračovat	k5eAaImIp3nP	překračovat
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
skládání	skládání	k1gNnSc1	skládání
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
posttranslační	posttranslační	k2eAgFnSc2d1	posttranslační
modifikace	modifikace	k1gFnSc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
skládání	skládání	k1gNnSc4	skládání
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
translokovány	translokovat	k5eAaBmNgFnP	translokovat
do	do	k7c2	do
retikula	retikulum	k1gNnSc2	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
enzymů	enzym	k1gInPc2	enzym
je	být	k5eAaImIp3nS	být
proteindisulfidizomeráza	proteindisulfidizomeráza	k1gFnSc1	proteindisulfidizomeráza
(	(	kIx(	(
<g/>
PDI	PDI	kA	PDI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
vznik	vznik	k1gInSc4	vznik
SS	SS	kA	SS
můstků	můstek	k1gInPc2	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
v	v	k7c6	v
ER	ER	kA	ER
se	se	k3xPyFc4	se
od	od	k7c2	od
těch	ten	k3xDgMnPc2	ten
cytosolických	cytosolický	k2eAgMnPc2d1	cytosolický
liší	lišit	k5eAaImIp3nS	lišit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
SS	SS	kA	SS
můstky	můstek	k1gInPc1	můstek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c4	v
lumen	lumen	k1gInSc4	lumen
ER	ER	kA	ER
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
dostatečně	dostatečně	k6eAd1	dostatečně
oxidativní	oxidativní	k2eAgFnPc1d1	oxidativní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
bílkovinou	bílkovina	k1gFnSc7	bílkovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
skládání	skládání	k1gNnSc1	skládání
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
BiP	BiP	k1gMnSc1	BiP
protein	protein	k1gInSc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
chaperon	chaperon	k1gInSc1	chaperon
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
hydrofobní	hydrofobní	k2eAgFnPc4d1	hydrofobní
oblasti	oblast	k1gFnPc4	oblast
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
špatně	špatně	k6eAd1	špatně
složených	složený	k2eAgInPc2d1	složený
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
přimět	přimět	k5eAaPmF	přimět
tyto	tento	k3xDgInPc4	tento
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
složily	složit	k5eAaPmAgFnP	složit
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
část	část	k1gFnSc1	část
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
posttranslačních	posttranslační	k2eAgFnPc2d1	posttranslační
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
glykosylace	glykosylace	k1gFnSc1	glykosylace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
výroba	výroba	k1gFnSc1	výroba
glykoproteinů	glykoprotein	k1gMnPc2	glykoprotein
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
proteinů	protein	k1gInPc2	protein
přítomných	přítomný	k2eAgInPc2d1	přítomný
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
i	i	k8xC	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
sekreční	sekreční	k2eAgFnSc2d1	sekreční
dráhy	dráha	k1gFnSc2	dráha
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
(	(	kIx(	(
<g/>
N-	N-	k1gFnSc6	N-
<g/>
)	)	kIx)	)
<g/>
glykoproteiny	glykoproteina	k1gFnSc2	glykoproteina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
se	se	k3xPyFc4	se
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
přidává	přidávat	k5eAaImIp3nS	přidávat
poměrně	poměrně	k6eAd1	poměrně
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
větvený	větvený	k2eAgInSc1d1	větvený
oligosacharid	oligosacharid	k1gInSc1	oligosacharid
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prekurzorový	prekurzorový	k2eAgInSc1d1	prekurzorový
oligosacharid	oligosacharid	k1gInSc1	oligosacharid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
předpřipraven	předpřipravit	k5eAaPmNgInS	předpřipravit
v	v	k7c6	v
membráně	membrána	k1gFnSc6	membrána
(	(	kIx(	(
<g/>
navázaný	navázaný	k2eAgMnSc1d1	navázaný
na	na	k7c4	na
dolichol	dolichol	k1gInSc4	dolichol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
monosacharidů	monosacharid	k1gInPc2	monosacharid
promptně	promptně	k6eAd1	promptně
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
většina	většina	k1gFnSc1	většina
glykosylační	glykosylační	k2eAgFnSc2d1	glykosylační
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
vzniká	vznikat	k5eAaImIp3nS	vznikat
až	až	k9	až
v	v	k7c4	v
Golgiho	Golgi	k1gMnSc4	Golgi
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
i	i	k8xC	i
glykosylace	glykosylace	k1gFnSc1	glykosylace
v	v	k7c6	v
ER	ER	kA	ER
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
důležitou	důležitý	k2eAgFnSc4d1	důležitá
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
glykosylované	glykosylovaný	k2eAgInPc4d1	glykosylovaný
proteiny	protein	k1gInPc4	protein
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vážou	vázat	k5eAaImIp3nP	vázat
chaperony	chaperon	k1gInPc1	chaperon
kalnexin	kalnexin	k1gInSc1	kalnexin
a	a	k8xC	a
kalretikulin	kalretikulin	k2eAgInSc1d1	kalretikulin
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
drží	držet	k5eAaImIp3nP	držet
špatně	špatně	k6eAd1	špatně
sbalené	sbalený	k2eAgInPc1d1	sbalený
proteiny	protein	k1gInPc1	protein
v	v	k7c6	v
ER	ER	kA	ER
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
uspořádají	uspořádat	k5eAaPmIp3nP	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připevňování	připevňování	k1gNnSc1	připevňování
vybraných	vybraný	k2eAgInPc2d1	vybraný
membránových	membránový	k2eAgInPc2d1	membránový
proteinů	protein	k1gInPc2	protein
na	na	k7c6	na
GPI	GPI	kA	GPI
kotvu	kotva	k1gFnSc4	kotva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
periferní	periferní	k2eAgNnSc1d1	periferní
připevnění	připevnění	k1gNnSc1	připevnění
bílkovin	bílkovina	k1gFnPc2	bílkovina
na	na	k7c4	na
vnější	vnější	k2eAgInSc4d1	vnější
list	list	k1gInSc4	list
cytoplazmatické	cytoplazmatický	k2eAgFnSc2d1	cytoplazmatická
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
ERAD	ERAD	kA	ERAD
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
sbalit	sbalit	k5eAaPmF	sbalit
protein	protein	k1gInSc4	protein
do	do	k7c2	do
prostorového	prostorový	k2eAgNnSc2d1	prostorové
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
přísluší	příslušet	k5eAaImIp3nS	příslušet
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
možnost	možnost	k1gFnSc1	možnost
tento	tento	k3xDgInSc4	tento
protein	protein	k1gInSc4	protein
degradovat	degradovat	k5eAaBmF	degradovat
(	(	kIx(	(
<g/>
rozložit	rozložit	k5eAaPmF	rozložit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
nepřekážel	překážet	k5eNaImAgMnS	překážet
či	či	k8xC	či
neškodil	škodit	k5eNaImAgMnS	škodit
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
toto	tento	k3xDgNnSc4	tento
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
"	"	kIx"	"
<g/>
ERAD	ERAD	kA	ERAD
<g/>
"	"	kIx"	"
–	–	k?	–
z	z	k7c2	z
angl.	angl.	k?	angl.
ER-associated	ERssociated	k1gInSc1	ER-associated
degradation	degradation	k1gInSc1	degradation
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
degradace	degradace	k1gFnSc1	degradace
asociovaná	asociovaný	k2eAgFnSc1d1	asociovaná
s	s	k7c7	s
endoplazmatickým	endoplazmatický	k2eAgNnSc7d1	endoplazmatické
retikulem	retikulum	k1gNnSc7	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
začíná	začínat	k5eAaImIp3nS	začínat
rozpoznáním	rozpoznání	k1gNnSc7	rozpoznání
špatně	špatně	k6eAd1	špatně
složeného	složený	k2eAgInSc2d1	složený
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
protein	protein	k1gInSc1	protein
transportován	transportován	k2eAgInSc1d1	transportován
(	(	kIx(	(
<g/>
retrotranslokován	retrotranslokován	k2eAgInSc1d1	retrotranslokován
<g/>
)	)	kIx)	)
do	do	k7c2	do
cytosolu	cytosol	k1gInSc2	cytosol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ubikvitinován	ubikvitinován	k2eAgInSc1d1	ubikvitinován
<g/>
.	.	kIx.	.
</s>
<s>
Ubikvitinace	Ubikvitinace	k1gFnSc1	Ubikvitinace
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
daný	daný	k2eAgInSc1d1	daný
protein	protein	k1gInSc1	protein
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
proteazomu	proteazom	k1gInSc6	proteazom
rozložen	rozložit	k5eAaPmNgMnS	rozložit
na	na	k7c4	na
základní	základní	k2eAgInPc4d1	základní
stavební	stavební	k2eAgInPc4d1	stavební
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
špatně	špatně	k6eAd1	špatně
složených	složený	k2eAgFnPc2d1	složená
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
v	v	k7c6	v
nepořádku	nepořádek	k1gInSc6	nepořádek
s	s	k7c7	s
enzymatickou	enzymatický	k2eAgFnSc7d1	enzymatická
mašinérií	mašinérie	k1gFnSc7	mašinérie
chaperonů	chaperon	k1gInPc2	chaperon
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
proteinů	protein	k1gInPc2	protein
podílejících	podílející	k2eAgInPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c4	na
skládání	skládání	k1gNnSc4	skládání
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
spustí	spustit	k5eAaPmIp3nS	spustit
tzv.	tzv.	kA	tzv.
unfolded	unfolded	k1gInSc1	unfolded
protein	protein	k1gInSc1	protein
odpověď	odpověď	k1gFnSc1	odpověď
(	(	kIx(	(
<g/>
UPR	UPR	kA	UPR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mění	měnit	k5eAaImIp3nS	měnit
expresi	exprese	k1gFnSc4	exprese
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
genů	gen	k1gInPc2	gen
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
ER	ER	kA	ER
výkonnější	výkonný	k2eAgMnSc1d2	výkonnější
a	a	k8xC	a
schopnější	schopný	k2eAgMnSc1d2	schopnější
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
skládání	skládání	k1gNnSc1	skládání
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hladkém	hladký	k2eAgNnSc6d1	hladké
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
širokému	široký	k2eAgNnSc3d1	široké
spektru	spektrum	k1gNnSc3	spektrum
různých	různý	k2eAgFnPc2d1	různá
anabolických	anabolický	k2eAgFnPc2d1	anabolická
či	či	k8xC	či
katabolických	katabolický	k2eAgFnPc2d1	katabolická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k8xC	i
enzymy	enzym	k1gInPc4	enzym
schopné	schopný	k2eAgNnSc1d1	schopné
zneškodňovat	zneškodňovat	k5eAaImF	zneškodňovat
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Hladké	Hladká	k1gFnSc3	Hladká
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
mimoto	mimoto	k6eAd1	mimoto
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
vnitrobuněčným	vnitrobuněčný	k2eAgInSc7d1	vnitrobuněčný
skladem	sklad	k1gInSc7	sklad
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
také	také	k9	také
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
hladkém	hladký	k2eAgInSc6d1	hladký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
syntézu	syntéza	k1gFnSc4	syntéza
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
řada	řada	k1gFnSc1	řada
poměrně	poměrně	k6eAd1	poměrně
významných	významný	k2eAgInPc2d1	významný
metabolických	metabolický	k2eAgInPc2d1	metabolický
pochodů	pochod	k1gInPc2	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
uvnitř	uvnitř	k6eAd1	uvnitř
(	(	kIx(	(
<g/>
v	v	k7c4	v
lumen	lumen	k1gNnSc4	lumen
<g/>
)	)	kIx)	)
hladkého	hladký	k2eAgNnSc2d1	hladké
ER	ER	kA	ER
dochází	docházet	k5eAaImIp3nS	docházet
například	například	k6eAd1	například
k	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
glukóza-	glukóza-	k?	glukóza-
<g/>
6	[number]	k4	6
<g/>
-fosfátu	osfát	k1gInSc2	-fosfát
<g/>
,	,	kIx,	,
oxidaci	oxidace	k1gFnSc4	oxidace
a	a	k8xC	a
redukci	redukce	k1gFnSc4	redukce
různých	různý	k2eAgInPc2d1	různý
steroidních	steroidní	k2eAgInPc2d1	steroidní
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
syntéze	syntéza	k1gFnSc3	syntéza
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
a	a	k8xC	a
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
krokům	krok	k1gInPc3	krok
rozkladu	rozklást	k5eAaPmIp1nS	rozklást
hemu	hem	k1gInSc2	hem
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
zde	zde	k6eAd1	zde
i	i	k9	i
desaturace	desaturace	k1gFnSc1	desaturace
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
elongace	elongace	k1gFnSc2	elongace
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
či	či	k8xC	či
třeba	třeba	k9	třeba
jejich	jejich	k3xOp3gFnSc1	jejich
ω	ω	k?	ω
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
membrány	membrána	k1gFnSc2	membrána
ER	ER	kA	ER
probíhá	probíhat	k5eAaImIp3nS	probíhat
např.	např.	kA	např.
část	část	k1gFnSc1	část
syntézy	syntéza	k1gFnSc2	syntéza
cholesterolu	cholesterol	k1gInSc2	cholesterol
nebo	nebo	k8xC	nebo
ceramidu	ceramid	k1gInSc2	ceramid
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
pochodů	pochod	k1gInPc2	pochod
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
membrány	membrána	k1gFnSc2	membrána
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
,	,	kIx,	,
namátkou	namátkou	k6eAd1	namátkou
syntéza	syntéza	k1gFnSc1	syntéza
fosfolipidů	fosfolipid	k1gMnPc2	fosfolipid
či	či	k8xC	či
výroba	výroba	k1gFnSc1	výroba
acyl-CoA	acyl-CoA	k?	acyl-CoA
pomocí	pomocí	k7c2	pomocí
acyl-CoA	acyl-CoA	k?	acyl-CoA
syntetázy	syntetáza	k1gFnSc2	syntetáza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
membráně	membrána	k1gFnSc6	membrána
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
skupina	skupina	k1gFnSc1	skupina
cytochromů	cytochrom	k1gInPc2	cytochrom
P	P	kA	P
<g/>
450	[number]	k4	450
<g/>
,	,	kIx,	,
proteinů	protein	k1gInPc2	protein
schopných	schopný	k2eAgInPc2d1	schopný
přidávat	přidávat	k5eAaImF	přidávat
různé	různý	k2eAgFnPc4d1	různá
funkční	funkční	k2eAgFnPc4d1	funkční
skupiny	skupina	k1gFnPc4	skupina
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
mastné	mastný	k2eAgFnPc4d1	mastná
kyseliny	kyselina	k1gFnPc4	kyselina
<g/>
,	,	kIx,	,
steroidy	steroid	k1gInPc4	steroid
a	a	k8xC	a
prostaglandiny	prostaglandin	k2eAgFnPc4d1	prostaglandin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
cizorodé	cizorodý	k2eAgFnPc4d1	cizorodá
látky	látka	k1gFnPc4	látka
včetně	včetně	k7c2	včetně
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
,	,	kIx,	,
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
karcinogenů	karcinogen	k1gInPc2	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
biotransformaci	biotransformace	k1gFnSc3	biotransformace
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
léky	lék	k1gInPc4	lék
následně	následně	k6eAd1	následně
vyloučeny	vyloučen	k2eAgInPc1d1	vyloučen
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cytochromů	cytochrom	k1gInPc2	cytochrom
P450	P450	k1gFnPc2	P450
přítomných	přítomný	k2eAgFnPc2d1	přítomná
v	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
je	být	k5eAaImIp3nS	být
cytochrom	cytochrom	k1gInSc1	cytochrom
P450	P450	k1gFnSc2	P450
3A4	[number]	k4	3A4
(	(	kIx(	(
<g/>
CYP	CYP	kA	CYP
<g/>
3	[number]	k4	3
<g/>
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
metabolizuje	metabolizovat	k5eAaImIp3nS	metabolizovat
celou	celý	k2eAgFnSc4d1	celá
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
namátkou	namátkou	k6eAd1	namátkou
nifedipin	nifedipin	k2eAgInSc1d1	nifedipin
<g/>
,	,	kIx,	,
cyklosporin	cyklosporin	k1gInSc1	cyklosporin
<g/>
,	,	kIx,	,
erytromycin	erytromycina	k1gFnPc2	erytromycina
<g/>
,	,	kIx,	,
gestoden	gestodna	k1gFnPc2	gestodna
či	či	k8xC	či
aflatoxiny	aflatoxin	k1gInPc4	aflatoxin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgFnPc2d1	důležitá
rolí	role	k1gFnPc2	role
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
je	být	k5eAaImIp3nS	být
skladovat	skladovat	k5eAaImF	skladovat
vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
ionty	ion	k1gInPc4	ion
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
je	být	k5eAaImIp3nS	být
řízeně	řízeně	k6eAd1	řízeně
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
do	do	k7c2	do
cytosolu	cytosol	k1gInSc2	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ji	on	k3xPp3gFnSc4	on
udržovat	udržovat	k5eAaImF	udržovat
pod	pod	k7c7	pod
striktní	striktní	k2eAgFnSc7d1	striktní
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
endoplazmatickém	endoplazmatický	k2eAgNnSc6d1	endoplazmatické
retikulu	retikulum	k1gNnSc6	retikulum
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
koncentrace	koncentrace	k1gFnSc1	koncentrace
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
mM	mm	kA	mm
(	(	kIx(	(
<g/>
cca	cca	kA	cca
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vně	vně	k6eAd1	vně
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mnohokrát	mnohokrát	k6eAd1	mnohokrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
vazebné	vazebný	k2eAgInPc1d1	vazebný
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
kalretikulin	kalretikulin	k2eAgMnSc1d1	kalretikulin
<g/>
,	,	kIx,	,
GRP	GRP	kA	GRP
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
BiP	BiP	k1gFnSc1	BiP
či	či	k8xC	či
kalsekvestrin	kalsekvestrin	k1gInSc1	kalsekvestrin
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
asi	asi	k9	asi
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
0,4	[number]	k4	0,4
mM	mm	kA	mm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
v	v	k7c4	v
lumen	lumen	k1gNnSc4	lumen
ER	ER	kA	ER
(	(	kIx(	(
<g/>
nenavázána	navázat	k5eNaPmNgFnS	navázat
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
ionty	ion	k1gInPc4	ion
se	se	k3xPyFc4	se
do	do	k7c2	do
ER	ER	kA	ER
pumpují	pumpovat	k5eAaImIp3nP	pumpovat
pomocí	pomocí	k7c2	pomocí
SERCA	SERCA	kA	SERCA
ATPázy	ATPáza	k1gFnSc2	ATPáza
a	a	k8xC	a
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
urč	urč	k?	urč
<g/>
.	.	kIx.	.
signálu	signál	k1gInSc2	signál
<g/>
)	)	kIx)	)
skrz	skrz	k7c4	skrz
kanály	kanál	k1gInPc4	kanál
v	v	k7c6	v
inositoltrifosfátových	inositoltrifosfátův	k2eAgInPc6d1	inositoltrifosfátův
receptorech	receptor	k1gInPc6	receptor
či	či	k8xC	či
v	v	k7c6	v
ryanodinových	ryanodinový	k2eAgInPc6d1	ryanodinový
receptorech	receptor	k1gInPc6	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Hladké	Hladké	k2eAgNnSc4d1	Hladké
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
ve	v	k7c6	v
svalových	svalový	k2eAgFnPc6d1	svalová
buňkách	buňka	k1gFnPc6	buňka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sarkoplazmatické	sarkoplazmatický	k2eAgNnSc1d1	sarkoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
právě	právě	k9	právě
na	na	k7c4	na
uvolňování	uvolňování	k1gNnSc4	uvolňování
vápenatých	vápenatý	k2eAgInPc2d1	vápenatý
iontů	ion	k1gInPc2	ion
<g/>
:	:	kIx,	:
bez	bez	k7c2	bez
otevírání	otevírání	k1gNnSc2	otevírání
vápníkových	vápníkový	k2eAgInPc2d1	vápníkový
kanálů	kanál	k1gInPc2	kanál
by	by	kYmCp3nP	by
nemohlo	moct	k5eNaImAgNnS	moct
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
svalovému	svalový	k2eAgInSc3d1	svalový
stahu	stah	k1gInSc3	stah
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ER	ER	kA	ER
vychází	vycházet	k5eAaImIp3nS	vycházet
množství	množství	k1gNnSc1	množství
váčků	váček	k1gInPc2	váček
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
organel	organela	k1gFnPc2	organela
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
ER	ER	kA	ER
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
<s>
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
zastávkou	zastávka	k1gFnSc7	zastávka
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
sekrečním	sekreční	k2eAgInPc3d1	sekreční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
transport	transport	k1gInSc4	transport
látek	látka	k1gFnPc2	látka
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
plazmatické	plazmatický	k2eAgFnSc3d1	plazmatická
membráně	membrána	k1gFnSc3	membrána
a	a	k8xC	a
ven	ven	k6eAd1	ven
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
ER	ER	kA	ER
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
opatřeny	opatřit	k5eAaPmNgInP	opatřit
značkou	značka	k1gFnSc7	značka
KDEL	KDEL	kA	KDEL
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
bílkovin	bílkovina	k1gFnPc2	bílkovina
přítomných	přítomný	k2eAgFnPc2d1	přítomná
v	v	k7c6	v
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc7	první
zastávkou	zastávka	k1gFnSc7	zastávka
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
pouti	pouť	k1gFnSc6	pouť
buňkou	buňka	k1gFnSc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
bílkoviny	bílkovina	k1gFnPc1	bílkovina
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
ER	ER	kA	ER
exportních	exportní	k2eAgFnPc6d1	exportní
doménách	doména	k1gFnPc6	doména
<g/>
"	"	kIx"	"
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
signálních	signální	k2eAgInPc2d1	signální
motivů	motiv	k1gInPc2	motiv
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nS	vážit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
pomocných	pomocný	k2eAgInPc2d1	pomocný
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
pučí	pučet	k5eAaImIp3nS	pučet
ven	ven	k6eAd1	ven
ve	v	k7c6	v
váčcích	váček	k1gInPc6	váček
obalených	obalený	k2eAgInPc6d1	obalený
pláštěm	plášť	k1gInSc7	plášť
z	z	k7c2	z
COPII	COPII	kA	COPII
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Váčky	váček	k1gInPc1	váček
směřují	směřovat	k5eAaImIp3nP	směřovat
ke	k	k7c3	k
Golgiho	Golgi	k1gMnSc4	Golgi
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podél	podél	k7c2	podél
speciálních	speciální	k2eAgFnPc2d1	speciální
"	"	kIx"	"
<g/>
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
"	"	kIx"	"
tvořených	tvořený	k2eAgFnPc2d1	tvořená
mikrotubuly	mikrotubula	k1gFnSc2	mikrotubula
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
však	však	k9	však
prokázány	prokázán	k2eAgInPc1d1	prokázán
i	i	k8xC	i
váčky	váček	k1gInPc1	váček
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
Golgiho	Golgi	k1gMnSc2	Golgi
aparátu	aparát	k1gInSc2	aparát
vrací	vracet	k5eAaImIp3nS	vracet
zpátky	zpátky	k6eAd1	zpátky
k	k	k7c3	k
endoplazmatickému	endoplazmatický	k2eAgNnSc3d1	endoplazmatické
retikulu	retikulum	k1gNnSc3	retikulum
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
retrográdní	retrográdní	k2eAgFnSc4d1	retrográdní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
např.	např.	kA	např.
pro	pro	k7c4	pro
recyklaci	recyklace	k1gFnSc4	recyklace
"	"	kIx"	"
<g/>
omylem	omylem	k6eAd1	omylem
<g/>
"	"	kIx"	"
odeslaných	odeslaný	k2eAgFnPc2d1	odeslaná
bílkovin	bílkovina	k1gFnPc2	bílkovina
z	z	k7c2	z
Golgiho	Golgi	k1gMnSc2	Golgi
aparátu	aparát	k1gInSc2	aparát
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
ER	ER	kA	ER
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
váčky	váček	k1gInPc1	váček
pučí	pučet	k5eAaImIp3nP	pučet
díky	díky	k7c3	díky
plášťovému	plášťový	k2eAgInSc3d1	plášťový
proteinu	protein	k1gInSc3	protein
COPI	COPI	kA	COPI
<g/>
.	.	kIx.	.
</s>
