<s>
Pohřbena	pohřben	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
v	v	k7c4
Sceaux	Sceaux	k1gInSc4
po	po	k7c6
boku	bok	k1gInSc6
svého	svůj	k3xOyFgMnSc2
manžela	manžel	k1gMnSc2
Pierra	Pierr	k1gMnSc2
a	a	k8xC
po	po	k7c6
60	#num#	k4
letech	let	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
byly	být	k5eAaImAgInP
jejich	jejich	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
společně	společně	k6eAd1
převezeny	převézt	k5eAaPmNgInP
a	a	k8xC
uloženy	uložit	k5eAaPmNgInP
do	do	k7c2
Panthéonu	Panthéon	k1gInSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
pohřbenou	pohřbený	k2eAgFnSc7d1
v	v	k7c4
Panthéonu	Panthéona	k1gFnSc4
za	za	k7c4
vlastní	vlastní	k2eAgFnPc4d1
zásluhy	zásluha	k1gFnPc4
<g/>
.	.	kIx.
</s>