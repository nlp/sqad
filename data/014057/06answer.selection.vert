<s>
Francium	francium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Fr	fr	k0
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Francium	francium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejtěžší	těžký	k2eAgInSc4d3
známý	známý	k2eAgInSc4d1
chemický	chemický	k2eAgInSc4d1
prvek	prvek	k1gInSc4
z	z	k7c2
řady	řada	k1gFnSc2
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nestabilní	stabilní	k2eNgInSc4d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
</s>
