<s>
Francium	francium	k1gNnSc1
</s>
<s>
Francium	francium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
7	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
223	#num#	k4
</s>
<s>
Fr	fr	kA
</s>
<s>
87	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Atom	atom	k1gInSc1
francia	francium	k1gNnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Francium	francium	k1gNnSc1
<g/>
,	,	kIx,
Fr	fr	k0
<g/>
,	,	kIx,
87	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Francium	francium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
s	s	k7c7
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
ppm	ppm	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrný	stříbrný	k2eAgInSc1d1
radioaktivní	radioaktivní	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-73-5	7440-73-5	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
233,019	233,019	k4
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
280	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Fr	fr	k0
<g/>
+	+	kIx~
<g/>
)	)	kIx)
180	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
7	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
I	i	k9
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,79	0,79	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
3,98	3,98	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
krychlová	krychlový	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
2,29	2,29	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
100	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
69,08	69,08	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
94,20	94,20	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
27	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
300,15	300,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
677	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
950,15	950,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
9,34	9,34	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
310	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
221	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
přirozený	přirozený	k2eAgInSc1d1
</s>
<s>
4,8	4,8	k4
min	mina	k1gFnPc2
</s>
<s>
α	α	k?
</s>
<s>
6,457	6,457	k4
</s>
<s>
217	#num#	k4
<g/>
At	At	k1gFnSc1
</s>
<s>
223	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
přirozený	přirozený	k2eAgInSc1d1
</s>
<s>
22	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
1,149	1,149	k4
</s>
<s>
223	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
α	α	k?
</s>
<s>
5,430	5,430	k4
</s>
<s>
219	#num#	k4
<g/>
At	At	k1gFnSc1
</s>
<s>
224	#num#	k4
<g/>
Fr	fr	k0
</s>
<s>
přirozený	přirozený	k2eAgInSc1d1
</s>
<s>
2	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
224	#num#	k4
<g/>
Ra	ra	k0
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Cs	Cs	k?
<g/>
⋏	⋏	k?
</s>
<s>
Fr	fr	k0
<g/>
≻	≻	k?
Radium	radium	k1gNnSc4
</s>
<s>
Francium	francium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Fr	fr	k0
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Francium	francium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejtěžší	těžký	k2eAgInSc4d3
známý	známý	k2eAgInSc4d1
chemický	chemický	k2eAgInSc4d1
prvek	prvek	k1gInSc4
z	z	k7c2
řady	řada	k1gFnSc2
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nestabilní	stabilní	k2eNgInSc4d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Nejstabilnější	stabilní	k2eAgInSc4d3
izotop	izotop	k1gInSc4
francia	francium	k1gNnSc2
223	#num#	k4
<g/>
Fr	fr	k0
má	mít	k5eAaImIp3nS
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
22	#num#	k4
minut	minuta	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podléhá	podléhat	k5eAaImIp3nS
β	β	k?
<g/>
−	−	k?
rozpadu	rozpad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
izotop	izotop	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
přírodě	příroda	k1gFnSc6
a	a	k8xC
to	ten	k3xDgNnSc1
α	α	k?
přeměnou	přeměna	k1gFnSc7
aktinia	aktinium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francium	francium	k1gNnSc1
se	se	k3xPyFc4
přeměňuje	přeměňovat	k5eAaImIp3nS
α	α	k?
rozpadem	rozpad	k1gInSc7
na	na	k7c4
astat	astat	k1gInSc4
<g/>
,	,	kIx,
β	β	k?
<g/>
+	+	kIx~
rozpadem	rozpad	k1gInSc7
na	na	k7c4
radon	radon	k1gInSc4
nebo	nebo	k8xC
β	β	k?
<g/>
−	−	k?
rozpadem	rozpad	k1gInSc7
na	na	k7c4
radium	radium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
pokojové	pokojový	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
je	být	k5eAaImIp3nS
francium	francium	k1gNnSc4
pevný	pevný	k2eAgInSc1d1
kov	kov	k1gInSc1
s	s	k7c7
nejnižší	nízký	k2eAgFnSc7d3
hodnotou	hodnota	k1gFnSc7
elektronegativity	elektronegativita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
reaktivní	reaktivní	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
sloučeniny	sloučenina	k1gFnPc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
podobají	podobat	k5eAaImIp3nP
sloučeninám	sloučenina	k1gFnPc3
cesia	cesium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc1
soli	sůl	k1gFnPc1
francia	francium	k1gNnSc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
vodě	voda	k1gFnSc6
rozpustné	rozpustný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Fluorid	fluorid	k1gInSc1
francia	francium	k1gNnSc2
(	(	kIx(
<g/>
FrF	FrF	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sloučeninou	sloučenina	k1gFnSc7
s	s	k7c7
největším	veliký	k2eAgInSc7d3
rozdílem	rozdíl	k1gInSc7
elektronegativity	elektronegativita	k1gFnSc2
mezi	mezi	k7c7
vázanými	vázaný	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Francium	francium	k1gNnSc1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
až	až	k9
roku	rok	k1gInSc2
1939	#num#	k4
Margueritou	Marguerita	k1gFnSc7
Pereyovou	Pereyový	k2eAgFnSc7d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
a	a	k8xC
pojmenováno	pojmenován	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
podle	podle	k7c2
země	zem	k1gFnSc2
svého	svůj	k3xOyFgInSc2
objevu	objev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Francium	francium	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c6
Zemi	zem	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
množství	množství	k1gNnSc6
7	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
19	#num#	k4
ppm	ppm	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
parts	parts	k1gInSc1
per	pero	k1gNnPc2
milion	milion	k4xCgInSc4
=	=	kIx~
počet	počet	k1gInSc4
částic	částice	k1gFnPc2
na	na	k7c4
1	#num#	k4
milion	milion	k4xCgInSc4
částic	částice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
by	by	kYmCp3nS
odpovídalo	odpovídat	k5eAaImAgNnS
obsahu	obsah	k1gInSc2
15	#num#	k4
g	g	kA
francia	francium	k1gNnSc2
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
o	o	k7c6
tloušťce	tloušťka	k1gFnSc6
1	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
izotopy	izotop	k1gInPc1
francia	francium	k1gNnSc2
221	#num#	k4
<g/>
Fr	fr	k0
(	(	kIx(
<g/>
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
neptuniové	ptuniový	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
223	#num#	k4
<g/>
Fr	fr	k0
(	(	kIx(
<g/>
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
aktiniové	aktiniový	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
224	#num#	k4
<g/>
Fr	fr	k0
(	(	kIx(
<g/>
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
thoriové	thoriový	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
stopy	stopa	k1gFnPc1
nalézáme	nalézat	k5eAaImIp1nP
v	v	k7c6
uranových	uranový	k2eAgFnPc6d1
a	a	k8xC
thoriových	thoriový	k2eAgFnPc6d1
rudách	ruda	k1gFnPc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
34	#num#	k4
izotopů	izotop	k1gInPc2
francia	francium	k1gNnSc2
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc1
jsou	být	k5eAaImIp3nP
značně	značně	k6eAd1
nestálé	stálý	k2eNgFnPc1d1
a	a	k8xC
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
podléhají	podléhat	k5eAaImIp3nP
radioaktivní	radioaktivní	k2eAgFnSc3d1
přeměně	přeměna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uměle	uměle	k6eAd1
se	se	k3xPyFc4
francium	francium	k1gNnSc1
připravuje	připravovat	k5eAaImIp3nS
protonovým	protonový	k2eAgNnSc7d1
bombardováním	bombardování	k1gNnSc7
thoria	thorium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Jursík	Jursík	k1gMnSc1
F.	F.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
nekovů	nekov	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7080-504-8	80-7080-504-8	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Chemické	chemický	k2eAgFnSc2d1
a	a	k8xC
analytické	analytický	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
literatura	literatura	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
42	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
část	část	k1gFnSc1
o	o	k7c6
franciu	francium	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
francium	francium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
francium	francium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Anorganické	anorganický	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
francné	francný	k2eAgFnSc2d1
Halogenidy	Halogenida	k1gFnSc2
</s>
<s>
Fluorid	fluorid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
FrF	FrF	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bromid	bromid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
FrBr	FrBr	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Chlorid	chlorid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
FrCl	FrCl	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Jodid	jodid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
FrI	FrI	k1gFnPc1
<g/>
)	)	kIx)
Soli	sůl	k1gFnPc1
tvořené	tvořený	k2eAgFnPc1d1
záměnou	záměna	k1gFnSc7
vodíkuze	vodíkuze	k1gFnSc2
sloučenin	sloučenina	k1gFnPc2
typu	typ	k1gInSc2
prvekx	prvekx	k1gInSc4
–	–	k?
vodíky	vodík	k1gInPc4
</s>
<s>
Hydrid	hydrid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
FrH	FrH	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Hydroxid	hydroxid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
FrOH	FrOH	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
francný	francný	k2eAgInSc1d1
(	(	kIx(
<g/>
Fr	fr	k0
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
Protože	protože	k8xS
je	být	k5eAaImIp3nS
francium	francium	k1gNnSc4
radioaktivní	radioaktivní	k2eAgInPc4d1
a	a	k8xC
velmi	velmi	k6eAd1
nestálý	stálý	k2eNgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
nestálé	stálý	k2eNgFnPc1d1
i	i	k9
jeho	jeho	k3xOp3gFnPc1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4155153-9	4155153-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5749	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85051541	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85051541	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
