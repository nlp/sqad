<s>
Série	série	k1gFnSc1	série
light	lighta	k1gFnPc2	lighta
novel	novela	k1gFnPc2	novela
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
již	již	k9	již
osmi	osm	k4xCc2	osm
mang	mango	k1gNnPc2	mango
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
anime	animat	k5eAaPmIp3nS	animat
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
motivy	motiv	k1gInPc4	motiv
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
a	a	k8xC	a
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
hry	hra	k1gFnSc2	hra
Sword	Sworda	k1gFnPc2	Sworda
Art	Art	k1gMnSc5	Art
Online	Onlin	k1gMnSc5	Onlin
<g/>
:	:	kIx,	:
Infinity	Infinita	k1gFnSc2	Infinita
Moment	moment	k1gInSc1	moment
pro	pro	k7c4	pro
PSP	PSP	kA	PSP
a	a	k8xC	a
Sword	Sword	k1gMnSc1	Sword
Art	Art	k1gMnSc1	Art
Online	Onlin	k1gInSc5	Onlin
<g/>
:	:	kIx,	:
Hollow	Hollow	k1gFnSc1	Hollow
Fragment	fragment	k1gInSc4	fragment
pro	pro	k7c4	pro
PS	PS	kA	PS
Vita	vít	k5eAaImNgFnS	vít
<g/>
.	.	kIx.	.
</s>
