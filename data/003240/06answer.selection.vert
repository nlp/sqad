<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
kapely	kapela	k1gFnPc4	kapela
tohoto	tento	k3xDgInSc2	tento
subžánru	subžánr	k1gInSc2	subžánr
patří	patřit	k5eAaImIp3nS	patřit
Emperor	Emperor	k1gMnSc1	Emperor
<g/>
,	,	kIx,	,
Cradle	Cradle	k1gMnSc1	Cradle
of	of	k?	of
Filth	Filth	k1gMnSc1	Filth
<g/>
,	,	kIx,	,
Dimmu	Dimma	k1gFnSc4	Dimma
Borgir	Borgir	k1gInSc4	Borgir
<g/>
,	,	kIx,	,
Carach	Carach	k1gInSc4	Carach
Angren	Angrna	k1gFnPc2	Angrna
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
známá	známý	k2eAgFnSc1d1	známá
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
CHtHoniC	CHtHoniC	k1gFnSc2	CHtHoniC
<g/>
.	.	kIx.	.
</s>
