<s>
Šepseskare	Šepseskar	k1gMnSc5
</s>
<s>
ŠepseskareDoba	ŠepseskareDoba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
~	~	kIx~
<g/>
2403	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
let	let	k1gInSc1
(	(	kIx(
<g/>
Beckerath	Beckerath	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
let	let	k1gInSc1
(	(	kIx(
<g/>
Turínský	turínský	k2eAgInSc1d1
královský	královský	k2eAgInSc1d1
papyrus	papyrus	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
let	let	k1gInSc1
(	(	kIx(
<g/>
Manehto	Manehto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Netjeruser	Netjeruser	k1gMnSc1
(	(	kIx(
<g/>
Netjer	Netjer	k1gMnSc1
user	usrat	k5eAaPmRp2nS
<g/>
)	)	kIx)
Trůnní	trůnní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Šepses-ka-Re	Šepses-ka-Re	k1gFnSc1
(	(	kIx(
<g/>
Špss-k	Špss-k	k1gInSc1
<g/>
3	#num#	k4
<g/>
-Rˁ	-Rˁ	k?
<g/>
)	)	kIx)
Horovo	Horův	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Sechem-chem	Sechem-ch	k1gInSc7
Otec	otec	k1gMnSc1
</s>
<s>
Neferirkare	Neferirkar	k1gMnSc5
Matka	matka	k1gFnSc1
</s>
<s>
Chentkaus	Chentkaus	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narození	narození	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
2473	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šepseskare	Šepseskare	k1gMnSc1
byl	být	k5eAaImAgInS
egyptským	egyptský	k2eAgMnSc7d1
faraonem	faraon	k1gMnSc7
5	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgMnS
přibližně	přibližně	k6eAd1
v	v	k7c6
~	~	kIx~
<g/>
2403	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Přesná	přesný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
<g/>
,	,	kIx,
Manehto	Manehto	k1gNnSc1
a	a	k8xC
Turínský	turínský	k2eAgInSc1d1
královský	královský	k2eAgInSc1d1
papyrus	papyrus	k1gInSc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
vládl	vládnout	k5eAaImAgInS
7	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgInPc2d1
královských	královský	k2eAgInPc2d1
seznamů	seznam	k1gInPc2
ho	on	k3xPp3gInSc4
uvádí	uvádět	k5eAaImIp3nS
ještě	ještě	k6eAd1
Ebozevská	Ebozevský	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Sakkárský	Sakkárský	k2eAgInSc4d1
královský	královský	k2eAgInSc4d1
seznam	seznam	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
je	být	k5eAaImIp3nS
o	o	k7c6
něm	on	k3xPp3gMnSc6
málo	málo	k4c1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
ani	ani	k8xC
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
jaké	jaký	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
pořadí	pořadí	k1gNnSc1
v	v	k7c6
seznamů	seznam	k1gInPc2
faraonů	faraon	k1gInPc2
5	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
mu	on	k3xPp3gMnSc3
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgMnSc1d1
egyptolog	egyptolog	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Verner	Verner	k1gMnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
ho	on	k3xPp3gNnSc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
vlády	vláda	k1gFnPc4
faraonů	faraon	k1gMnPc2
Neferirkarea	Neferirkareum	k1gNnSc2
a	a	k8xC
Raneferefa	Raneferef	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
jinde	jinde	k6eAd1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
Raneferefova	Raneferefův	k2eAgMnSc4d1
následníka	následník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
také	také	k9
vedou	vést	k5eAaImIp3nP
diskuze	diskuze	k1gFnPc1
o	o	k7c6
jeho	jeho	k3xOp3gMnPc6
rodičích	rodič	k1gMnPc6
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
Neferirkare	Neferirkar	k1gMnSc5
a	a	k8xC
královna	královna	k1gFnSc1
Chentkaus	Chentkaus	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
jeho	jeho	k3xOp3gFnSc2
bratrem	bratr	k1gMnSc7
by	by	kYmCp3nS
pak	pak	k9
byl	být	k5eAaImAgInS
Raneferef	Raneferef	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
vysvětlení	vysvětlení	k1gNnPc4
naznačují	naznačovat	k5eAaImIp3nP
poslední	poslední	k2eAgInPc1d1
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
analýza	analýza	k1gFnSc1
v	v	k7c6
celém	celý	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
dat	datum	k1gNnPc2
získaných	získaný	k2eAgNnPc2d1
na	na	k7c6
Abúsírské	Abúsírský	k2eAgFnSc6d1
lokalitě	lokalita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
jen	jen	k6eAd1
několk	několk	k6eAd1
několika	několik	k4yIc2
zlomků	zlomek	k1gInPc2
hliněných	hliněný	k2eAgFnPc2d1
pečetí	pečeť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
jeho	jeho	k3xOp3gFnSc6
vládě	vláda	k1gFnSc6
a	a	k8xC
činech	čin	k1gInPc6
se	se	k3xPyFc4
nic	nic	k3yNnSc1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
ani	ani	k8xC
jeho	jeho	k3xOp3gFnSc1
hrobka	hrobka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
faraonem	faraon	k1gMnSc7
Menkauhorem	Menkauhor	k1gMnSc7
ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejméně	málo	k6eAd3
známým	známý	k2eAgInPc3d1
faraonům	faraon	k1gInPc3
starého	starý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
jižně	jižně	k6eAd1
od	od	k7c2
pyramidy	pyramida	k1gFnSc2
krále	král	k1gMnSc2
Sahurea	Sahureus	k1gMnSc2
v	v	k7c6
Abúsíru	Abúsír	k1gInSc6
objevena	objevit	k5eAaPmNgFnS
nedokončená	dokončený	k2eNgFnSc1d1
pyramida	pyramida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
pyramida	pyramida	k1gFnSc1
je	být	k5eAaImIp3nS
právě	právě	k9
Šepseskareovou	Šepseskareův	k2eAgFnSc7d1
hrobkou	hrobka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
HORNUG	HORNUG	kA
<g/>
,	,	kIx,
Erik	Erik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ancient	Ancient	k1gMnSc1
Egyptian	Egyptian	k1gMnSc1
Chronology	chronolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leiden	Leidna	k1gFnPc2
<g/>
,	,	kIx,
Boston	Boston	k1gInSc1
<g/>
:	:	kIx,
<g/>
Brill	Brill	k1gMnSc1
<g/>
:	:	kIx,
Briil	Briil	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
491	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11385	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
VERNER	Verner	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
BAREŠ	Bareš	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
;	;	kIx,
VACHALA	VACHALA	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
starověkého	starověký	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
517	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohové	bůh	k1gMnPc1
a	a	k8xC
králové	král	k1gMnPc1
starého	starý	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
S.	S.	kA
330	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VERNER	Verner	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
BAREŠ	Bareš	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
;	;	kIx,
VACHALA	VACHALA	kA
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
starověkého	starověký	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
460	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VERNER	Verner	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abúsír	Abúsír	k1gInSc1
<g/>
.	.	kIx.
<g/>
V	v	k7c6
srdci	srdce	k1gNnSc6
pyramidových	pyramidový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2700	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
65	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Pharoah	Pharoaha	k1gFnPc2
Otline	Otlin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tři	tři	k4xCgFnPc4
královny	královna	k1gFnPc1
se	s	k7c7
stejným	stejný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
-	-	kIx~
Chentkaus	Chentkaus	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Felgr	Felgr	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Neferirkare	Neferirkar	k1gMnSc5
</s>
<s>
Raneferef	Raneferef	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Neferirkare	Neferirkar	k1gMnSc5
</s>
<s>
Egyptský	egyptský	k2eAgMnSc1d1
král	král	k1gMnSc1
~	~	kIx~
<g/>
2403	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Raneferef	Raneferef	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Faraoni	faraon	k1gMnPc1
staré	starý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
2707	#num#	k4
<g/>
/	/	kIx~
<g/>
2657	#num#	k4
<g/>
–	–	k?
<g/>
2639	#num#	k4
<g/>
/	/	kIx~
<g/>
2589	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nebka	Nebka	k1gFnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
•	•	k?
Necerichet	Necerichet	k1gInSc1
(	(	kIx(
<g/>
Džoser	Džoser	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Sechemchet	Sechemchet	k1gInSc1
•	•	k?
Sanacht	Sanacht	k1gInSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
•	•	k?
Chaba	Chaba	k1gFnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
•	•	k?
Hunej	Hunej	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
2639	#num#	k4
<g/>
/	/	kIx~
<g/>
2589	#num#	k4
<g/>
–	–	k?
<g/>
2504	#num#	k4
<g/>
/	/	kIx~
<g/>
2454	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Snofru	Snofra	k1gFnSc4
•	•	k?
Chufu	Chuf	k1gInSc2
(	(	kIx(
<g/>
Cheops	Cheops	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Radžedef	Radžedef	k1gInSc1
•	•	k?
Rachef	Rachef	k1gInSc1
(	(	kIx(
<g/>
Chefrén	Chefrén	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Baufre	Baufr	k1gInSc5
•	•	k?
Menkaure	Menkaur	k1gMnSc5
(	(	kIx(
<g/>
Mykerinos	Mykerinosa	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Šepseskaf	Šepseskaf	k1gInSc1
•	•	k?
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
Thamphthis	Thamphthis	k1gInSc1
5	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
2504	#num#	k4
<g/>
/	/	kIx~
<g/>
2454	#num#	k4
<g/>
–	–	k?
<g/>
2347	#num#	k4
<g/>
/	/	kIx~
<g/>
2297	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Veserkaf	Veserkaf	k1gInSc1
•	•	k?
Sahure	Sahur	k1gMnSc5
•	•	k?
Neferirkare	Neferirkar	k1gMnSc5
•	•	k?
Šepseskare	Šepseskar	k1gMnSc5
•	•	k?
Raneferef	Raneferef	k1gInSc1
•	•	k?
Niuserre	Niuserr	k1gInSc5
•	•	k?
Menkauhor	Menkauhor	k1gInSc1
•	•	k?
Džedkare	Džedkar	k1gMnSc5
•	•	k?
Venis	Venis	k1gFnSc7
6	#num#	k4
<g/>
.	.	kIx.
dynastie	dynastie	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
2347	#num#	k4
<g/>
/	/	kIx~
<g/>
2297	#num#	k4
<g/>
–	–	k?
<g/>
2216	#num#	k4
<g/>
/	/	kIx~
<g/>
2166	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Teti	teti	k1gFnSc1
•	•	k?
Veserkare	Veserkar	k1gMnSc5
•	•	k?
Pepi	Pepi	k1gMnSc1
I.	I.	kA
•	•	k?
Merenre	Merenr	k1gInSc5
I.	I.	kA
•	•	k?
Pepi	Pepi	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Merenre	Merenr	k1gInSc5
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Neitokret	Neitokret	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
