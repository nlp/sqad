<s>
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Čung-chua	Čunghua	k1gFnSc1	Čung-chua
žen-min	ženin	k1gInSc1	žen-min
kung-che-kuo	kungheuo	k6eAd1	kung-che-kuo
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Zhō	Zhō	k1gFnSc2	Zhō
rénmín	rénmín	k1gInSc1	rénmín
gò	gò	k?	gò
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
中	中	k?	中
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
