<s>
Fed	Fed	k?	Fed
Cup	cup	k1gInSc1	cup
je	být	k5eAaImIp3nS	být
ženská	ženský	k2eAgFnSc1d1	ženská
týmová	týmový	k2eAgFnSc1d1	týmová
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Pohár	pohár	k1gInSc1	pohár
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
Federation	Federation	k1gInSc1	Federation
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
