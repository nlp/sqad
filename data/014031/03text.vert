<s>
Aš	Aš	k1gFnSc1
</s>
<s>
Aš	Aš	k1gFnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
město	město	k1gNnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0411	CZ0411	k4
554499	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Aš	Aš	k1gInSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cheb	Cheb	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
411	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karlovarský	karlovarský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
41	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
13	#num#	k4
182	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
55,86	55,86	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
666	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
352	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
9	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
9	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
19	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
městského	městský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
473	#num#	k4
<g/>
/	/	kIx~
<g/>
52352	#num#	k4
01	#num#	k4
Aš	Aš	k1gFnSc1
posta@muas.cz	posta@muas.cza	k1gFnPc2
Starosta	Starosta	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Dalibor	Dalibor	k1gMnSc1
Blažek	Blažek	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.muas.cz	www.muas.cz	k1gInSc1
</s>
<s>
Aš	Aš	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
554499	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aš	Aš	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Asch	Asch	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Cheb	Cheb	k1gInSc1
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
20	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Chebu	Cheb	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
Ašském	ašský	k2eAgInSc6d1
výběžku	výběžek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
obklopeno	obklopit	k5eAaPmNgNnS
ze	z	k7c2
tří	tři	k4xCgFnPc2
stran	strana	k1gFnPc2
Německem	Německo	k1gNnSc7
(	(	kIx(
<g/>
Bavorskem	Bavorsko	k1gNnSc7
a	a	k8xC
Saskem	Sasko	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
13	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
V	v	k7c6
historických	historický	k2eAgInPc6d1
dokumentech	dokument	k1gInPc6
je	být	k5eAaImIp3nS
Aš	Aš	k1gInSc1
nazýván	nazývat	k5eAaImNgInS
také	také	k9
Ascha	Asch	k1gMnSc4
<g/>
,	,	kIx,
Asche	Asch	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
první	první	k4xOgNnSc1
zaznamenané	zaznamenaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
Aska	Aska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
města	město	k1gNnSc2
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
německého	německý	k2eAgInSc2d1
názvu	název	k1gInSc2
ryby	ryba	k1gFnSc2
–	–	k?
lipana	lipan	k1gMnSc2
(	(	kIx(
<g/>
die	die	k?
Äsche	Äsche	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
ve	v	k7c6
znaku	znak	k1gInSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
jméno	jméno	k1gNnSc1
mylně	mylně	k6eAd1
odvozováno	odvozovat	k5eAaImNgNnS
od	od	k7c2
německého	německý	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
Asche	Asche	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
popel	popel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
němčině	němčina	k1gFnSc6
má	mít	k5eAaImIp3nS
název	název	k1gInSc1
podobu	podoba	k1gFnSc4
Asch	Asch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
název	název	k1gInSc1
byl	být	k5eAaImAgInS
jako	jako	k9
oficiální	oficiální	k2eAgInSc1d1
používán	používán	k2eAgInSc1d1
až	až	k8xS
do	do	k7c2
konce	konec	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
paralelně	paralelně	k6eAd1
s	s	k7c7
českou	český	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
Aš	Aš	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Německu	Německo	k1gNnSc6
se	se	k3xPyFc4
varianty	varianta	k1gFnSc2
Asch	Ascha	k1gFnPc2
užívá	užívat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
na	na	k7c6
dopravních	dopravní	k2eAgFnPc6d1
informačních	informační	k2eAgFnPc6d1
značkách	značka	k1gFnPc6
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vysídlení	vysídlení	k1gNnSc6
Němců	Němec	k1gMnPc2
z	z	k7c2
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
obce	obec	k1gFnPc1
s	s	k7c7
německým	německý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
budou	být	k5eAaImBp3nP
přejmenovány	přejmenován	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gFnSc1
byla	být	k5eAaImAgFnS
jen	jen	k9
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
tří	tři	k4xCgFnPc2
obcí	obec	k1gFnPc2
na	na	k7c4
Ašsku	Ašska	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
kromě	kromě	k7c2
německého	německý	k2eAgInSc2d1
názvu	název	k1gInSc2
i	i	k9
český	český	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
uvažovalo	uvažovat	k5eAaImAgNnS
o	o	k7c6
přejmenování	přejmenování	k1gNnSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
například	například	k6eAd1
u	u	k7c2
města	město	k1gNnSc2
Sokolov	Sokolovo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
česky	česky	k6eAd1
dříve	dříve	k6eAd2
nazývalo	nazývat	k5eAaImAgNnS
Falknov	Falknov	k1gInSc4
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
návrhů	návrh	k1gInPc2
byl	být	k5eAaImAgInS
název	název	k1gInSc1
Dukla	Dukla	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
realizován	realizovat	k5eAaBmNgInS
<g/>
,	,	kIx,
a	a	k8xC
nakonec	nakonec	k6eAd1
městu	město	k1gNnSc3
zůstal	zůstat	k5eAaPmAgInS
název	název	k1gInSc1
Aš	Aš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1000	#num#	k4
patřilo	patřit	k5eAaImAgNnS
ašské	ašský	k2eAgNnSc1d1
území	území	k1gNnSc1
do	do	k7c2
takzvaného	takzvaný	k2eAgNnSc2d1
regia	regium	k1gNnSc2
Slavorum	Slavorum	k1gNnSc4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
z	z	k7c2
latiny	latina	k1gFnSc2
<g/>
:	:	kIx,
země	země	k1gFnSc1
Slovanů	Slovan	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gFnSc1
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
založena	založit	k5eAaPmNgFnS
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1270	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
získal	získat	k5eAaPmAgInS
ašský	ašský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
řád	řád	k1gInSc1
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
první	první	k4xOgFnSc1
zaznamenaná	zaznamenaný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
Aši	Aš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
samotné	samotný	k2eAgInPc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
historických	historický	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
zmíněno	zmíněn	k2eAgNnSc1d1
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1281	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
zastaveno	zastavit	k5eAaPmNgNnS
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Rudolfem	Rudolf	k1gMnSc7
Habsburským	habsburský	k2eAgMnSc7d1
fojtu	fojt	k1gMnSc6
Jindřichovi	Jindřich	k1gMnSc6
z	z	k7c2
Plavna	Plavno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1331	#num#	k4
byla	být	k5eAaImAgFnS
Aš	Aš	k1gFnSc1
a	a	k8xC
město	město	k1gNnSc1
Selb	Selba	k1gFnPc2
zastaveno	zastaven	k2eAgNnSc1d1
králem	král	k1gMnSc7
Janem	Jan	k1gMnSc7
Lucemburským	lucemburský	k2eAgMnSc7d1
znovu	znovu	k6eAd1
pánům	pan	k1gMnPc3
z	z	k7c2
Plavna	Plavno	k1gNnSc2
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc4
Ašska	Ašsek	k1gInSc2
potom	potom	k8xC
pánům	pan	k1gMnPc3
z	z	k7c2
Neuberga	Neuberg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plavenští	Plavenský	k2eAgMnPc1d1
si	se	k3xPyFc3
na	na	k7c6
Mikulově	Mikulov	k1gInSc6
vybudovali	vybudovat	k5eAaPmAgMnP
vedlejší	vedlejší	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neubergové	Neuberg	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
pravděpodobně	pravděpodobně	k6eAd1
mezi	mezi	k7c7
lety	let	k1gInPc7
1335	#num#	k4
až	až	k9
1355	#num#	k4
oddělit	oddělit	k5eAaPmF
Aš	Aš	k1gFnSc4
od	od	k7c2
Chebska	Chebsko	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
protože	protože	k8xS
Chebští	chebský	k2eAgMnPc1d1
páni	pan	k1gMnPc1
se	se	k3xPyFc4
dožadovali	dožadovat	k5eAaImAgMnP
krále	král	k1gMnSc2
Jana	Jan	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
privilegií	privilegium	k1gNnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
nichž	jenž	k3xRgFnPc2
by	by	kYmCp3nS
k	k	k7c3
tomuto	tento	k3xDgNnSc3
dojít	dojít	k5eAaPmF
nemohlo	moct	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1400	#num#	k4
odkoupili	odkoupit	k5eAaPmAgMnP
celé	celý	k2eAgNnSc4d1
Ašsko	Ašsko	k1gNnSc4
od	od	k7c2
pánů	pan	k1gMnPc2
z	z	k7c2
Plavna	Plavno	k1gNnSc2
Zedtwitzové	Zedtwitzová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1422	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
čeho	co	k3yInSc2,k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
Chebští	chebský	k2eAgMnPc1d1
obávali	obávat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Zikmund	Zikmund	k1gMnSc1
předal	předat	k5eAaPmAgMnS
celé	celý	k2eAgNnSc4d1
Ašsko	Ašsko	k1gNnSc4
Zedtwitzům	Zedtwitz	k1gMnPc3
jako	jako	k8xS,k8xC
mužské	mužský	k2eAgNnSc1d1
léno	léno	k1gNnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
bylo	být	k5eAaImAgNnS
definitivně	definitivně	k6eAd1
Ašsko	Ašsko	k1gNnSc1
odděleno	oddělit	k5eAaPmNgNnS
od	od	k7c2
Chebska	Chebsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
fakticky	fakticky	k6eAd1
autonomií	autonomie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zedtwitzové	Zedtwitz	k1gMnPc1
poté	poté	k6eAd1
ovládali	ovládat	k5eAaImAgMnP
Ašsko	Ašsko	k1gNnSc4
po	po	k7c6
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
dalších	další	k2eAgNnPc2d1
500	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
provedli	provést	k5eAaPmAgMnP
Zedtwitzové	Zedtwitz	k1gMnPc1
na	na	k7c4
Ašsku	Ašska	k1gFnSc4
reformaci	reformace	k1gFnSc4
<g/>
,	,	kIx,
luteránství	luteránství	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
dominantní	dominantní	k2eAgFnSc7d1
konfesí	konfese	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1629	#num#	k4
bylo	být	k5eAaImAgNnS
Ašsko	Ašsko	k1gNnSc1
obsazeno	obsadit	k5eAaPmNgNnS
císařskými	císařský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
a	a	k8xC
zdejší	zdejší	k2eAgMnPc1d1
evangeličtí	evangelický	k2eAgMnPc1d1
duchovní	duchovní	k1gMnPc1
byli	být	k5eAaImAgMnP
vyhnáni	vyhnat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
zvláštnímu	zvláštní	k2eAgNnSc3d1
postavení	postavení	k1gNnSc3
Ašska	Ašsek	k1gMnSc2
potvrdil	potvrdit	k5eAaPmAgMnS
vestfálský	vestfálský	k2eAgInSc4d1
mír	mír	k1gInSc4
Ašsko	Ašsko	k1gNnSc1
jako	jako	k8xS,k8xC
protestantské	protestantský	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
zde	zde	k6eAd1
provedena	proveden	k2eAgFnSc1d1
rekatolizace	rekatolizace	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
Ašsko	Ašsko	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
jediným	jediný	k2eAgNnSc7d1
českým	český	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
protestantismus	protestantismus	k1gInSc1
nejen	nejen	k6eAd1
povoleným	povolený	k2eAgInSc7d1
<g/>
,	,	kIx,
ale	ale	k8xC
dokonce	dokonce	k9
výhradním	výhradní	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
<g/>
,	,	kIx,
zůstal	zůstat	k5eAaPmAgMnS
jím	jíst	k5eAaImIp1nS
i	i	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
část	část	k1gFnSc4
rodiny	rodina	k1gFnSc2
Zedtwitzů	Zedtwitz	k1gMnPc2
přestoupila	přestoupit	k5eAaPmAgFnS
ke	k	k7c3
katolicismu	katolicismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1775	#num#	k4
bylo	být	k5eAaImAgNnS
Ašsko	Ašsko	k1gNnSc1
definitivně	definitivně	k6eAd1
připojeno	připojen	k2eAgNnSc1d1
k	k	k7c3
Čechám	Čechy	k1gFnPc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
resp.	resp.	kA
k	k	k7c3
Rakousku	Rakousko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
díky	díky	k7c3
tzv.	tzv.	kA
Temperamentním	temperamentní	k2eAgInPc3d1
bodům	bod	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vydala	vydat	k5eAaPmAgFnS
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
listina	listina	k1gFnSc1
měla	mít	k5eAaImAgFnS
původně	původně	k6eAd1
usnadnit	usnadnit	k5eAaPmF
politickou	politický	k2eAgFnSc4d1
a	a	k8xC
soudní	soudní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
na	na	k7c4
Ašsku	Ašska	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Zedtwitzům	Zedtwitz	k1gInPc3
také	také	k6eAd1
zaručila	zaručit	k5eAaPmAgFnS
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgInSc4d1
soudní	soudní	k2eAgInSc4d1
aparát	aparát	k1gInSc4
<g/>
,	,	kIx,
osvobození	osvobození	k1gNnSc4
od	od	k7c2
daní	daň	k1gFnPc2
a	a	k8xC
svobodu	svoboda	k1gFnSc4
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Toleranční	toleranční	k2eAgInSc1d1
patent	patent	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1781	#num#	k4
se	se	k3xPyFc4
na	na	k7c4
Ašsko	Ašsko	k1gNnSc4
vztahoval	vztahovat	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgInSc6
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
zaručil	zaručit	k5eAaPmAgMnS
toleranci	tolerance	k1gFnSc4
katolické	katolický	k2eAgFnSc3d1
menšině	menšina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Aš	Aš	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1825	#num#	k4
(	(	kIx(
<g/>
kresba	kresba	k1gFnSc1
W.	W.	kA
Kreizinger	Kreizingra	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Textilní	textilní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
v	v	k7c6
regionu	region	k1gInSc6
rychle	rychle	k6eAd1
sílil	sílit	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
koncem	koncem	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
z	z	k7c2
Aše	Aš	k1gFnSc2
stalo	stát	k5eAaPmAgNnS
průmyslové	průmyslový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1814	#num#	k4
vypukl	vypuknout	k5eAaPmAgInS
v	v	k7c6
Aši	Aš	k1gInSc6
velký	velký	k2eAgInSc1d1
požár	požár	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zasáhl	zasáhnout	k5eAaPmAgInS
oba	dva	k4xCgInPc4
vrchy	vrch	k1gInPc4
podél	podél	k7c2
hlavní	hlavní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
;	;	kIx,
Mikulášský	mikulášský	k2eAgInSc1d1
i	i	k8xC
Radniční	radniční	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požár	požár	k1gInSc1
zničil	zničit	k5eAaPmAgInS
celé	celý	k2eAgNnSc4d1
zámecké	zámecký	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
Zedtwitzů	Zedtwitz	k1gInPc2
<g/>
,	,	kIx,
historickou	historický	k2eAgFnSc4d1
radnici	radnice	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
vzal	vzít	k5eAaPmAgMnS
s	s	k7c7
sebou	se	k3xPyFc7
167	#num#	k4
domů	dům	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
dalších	další	k2eAgFnPc2d1
80	#num#	k4
staveb	stavba	k1gFnPc2
mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
vrchy	vrch	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1834	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
Aši	Aš	k1gInSc6
vyroben	vyrobit	k5eAaPmNgInS
první	první	k4xOgInSc1
Jacquardův	Jacquardův	k2eAgInSc1d1
tkalcovský	tkalcovský	k2eAgInSc1d1
stroj	stroj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Aši	Aš	k1gInSc6
už	už	k9
přes	přes	k7c4
6	#num#	k4
000	#num#	k4
tkalců	tkadlec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
třicet	třicet	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1864	#num#	k4
v	v	k7c6
Aši	Aš	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
první	první	k4xOgFnSc1
mechanická	mechanický	k2eAgFnSc1d1
tkalcovna	tkalcovna	k1gFnSc1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1877	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velké	velký	k2eAgFnSc3d1
stávce	stávka	k1gFnSc3
v	v	k7c6
textilní	textilní	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
skončila	skončit	k5eAaPmAgFnS
střelbou	střelba	k1gFnSc7
a	a	k8xC
smrtí	smrtit	k5eAaImIp3nS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
se	se	k3xPyFc4
Aš	Aš	k1gFnSc1
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
třinácti	třináct	k4xCc7
tisíci	tisíc	k4xCgInPc7
obyvateli	obyvatel	k1gMnPc7
stala	stát	k5eAaPmAgFnS
desátým	desátý	k4xOgNnSc7
největším	veliký	k2eAgNnSc7d3
českým	český	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1872	#num#	k4
byla	být	k5eAaImAgFnS
Aš	Aš	k1gFnSc1
právně	právně	k6eAd1
povýšena	povýšit	k5eAaPmNgFnS
z	z	k7c2
městyse	městys	k1gInSc2
na	na	k7c4
město	město	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Průmyslové	průmyslový	k2eAgNnSc1d1
město	město	k1gNnSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
malba	malba	k1gFnSc1
Huga	Hugo	k1gMnSc2
Charlemonta	Charlemont	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1896	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vpravo	vpravo	k6eAd1
kostel	kostel	k1gInSc4
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
zničený	zničený	k2eAgInSc1d1
roku	rok	k1gInSc2
1960	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Aši	Aš	k1gInSc6
založena	založit	k5eAaPmNgFnS
odborná	odborný	k2eAgFnSc1d1
textilní	textilní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
se	s	k7c7
čtyřletým	čtyřletý	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Textilní	textilní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
nadále	nadále	k6eAd1
velmi	velmi	k6eAd1
sílil	sílit	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
městě	město	k1gNnSc6
již	již	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
120	#num#	k4
tkalcoven	tkalcovna	k1gFnPc2
<g/>
,	,	kIx,
13	#num#	k4
barvíren	barvírna	k1gFnPc2
<g/>
,	,	kIx,
52	#num#	k4
obecných	obecný	k2eAgFnPc2d1
pletáren	pletárna	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
129	#num#	k4
pletáren	pletárna	k1gFnPc2
punčoch	punčocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Aše	Aš	k1gInSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
ještě	ještě	k6eAd1
silnější	silný	k2eAgNnSc4d2
průmyslové	průmyslový	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohatství	bohatství	k1gNnSc1
ašských	ašský	k2eAgMnPc2d1
továrníků	továrník	k1gMnPc2
podporovalo	podporovat	k5eAaImAgNnS
propagandu	propaganda	k1gFnSc4
Konráda	Konrád	k1gMnSc2
Henleina	Henlein	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
posilování	posilování	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
přebírání	přebírání	k1gNnSc2
moci	moc	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejím	její	k3xOp3gInSc6
sjezdu	sjezd	k1gInSc6
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1938	#num#	k4
<g/>
,	,	kIx,
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
21	#num#	k4
tisíc	tisíc	k4xCgInPc2
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
čeští	český	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
město	město	k1gNnSc1
opouštět	opouštět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
síle	síla	k1gFnSc3
ašských	ašský	k2eAgMnPc2d1
nacistů	nacista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
postupně	postupně	k6eAd1
přibírali	přibírat	k5eAaImAgMnP
moc	moc	k6eAd1
v	v	k7c6
celém	celý	k2eAgInSc6d1
ašském	ašský	k2eAgInSc6d1
okrese	okres	k1gInSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Ašsko	Ašsko	k1gNnSc1
obsazeno	obsadit	k5eAaPmNgNnS
jednotkami	jednotka	k1gFnPc7
freikorpsu	freikorps	k1gInSc2
a	a	k8xC
de	de	k?
facto	facto	k1gNnSc1
připojeno	připojit	k5eAaPmNgNnS
k	k	k7c3
německé	německý	k2eAgFnSc3d1
říši	říš	k1gFnSc3
ještě	ještě	k9
před	před	k7c7
podepsáním	podepsání	k1gNnSc7
Mnichovské	mnichovský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
21	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
policisté	policista	k1gMnPc1
byli	být	k5eAaImAgMnP
odzbrojeni	odzbrojit	k5eAaPmNgMnP
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
českými	český	k2eAgMnPc7d1
úředníky	úředník	k1gMnPc7
a	a	k8xC
německými	německý	k2eAgMnPc7d1
antifašisty	antifašista	k1gMnPc7
převezeni	převézt	k5eAaPmNgMnP
do	do	k7c2
Bad	Bad	k1gFnSc2
Elsteru	Elster	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
vstoupily	vstoupit	k5eAaPmAgInP
do	do	k7c2
města	město	k1gNnSc2
jednotky	jednotka	k1gFnPc4
wehrmachtu	wehrmacht	k1gInSc2
a	a	k8xC
o	o	k7c4
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
později	pozdě	k6eAd2
je	být	k5eAaImIp3nS
následoval	následovat	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1938	#num#	k4
vstoupily	vstoupit	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
wehrmachtu	wehrmacht	k1gInSc2
do	do	k7c2
Aše	Aš	k1gInSc2
následovány	následovat	k5eAaImNgFnP
samotným	samotný	k2eAgNnSc7d1
Hitlerem	Hitler	k1gMnSc7
</s>
<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
situace	situace	k1gFnSc1
nejen	nejen	k6eAd1
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
celém	celý	k2eAgInSc6d1
Ašsku	Ašsk	k1gInSc6
<g/>
,	,	kIx,
značně	značně	k6eAd1
zhoršila	zhoršit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Textilní	textilní	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
ztrácela	ztrácet	k5eAaImAgFnS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
síle	síla	k1gFnSc6
kvůli	kvůli	k7c3
úbytku	úbytek	k1gInSc3
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
německé	německý	k2eAgInPc1d1
úřady	úřad	k1gInPc1
postupně	postupně	k6eAd1
zastavovaly	zastavovat	k5eAaImAgInP
novou	nový	k2eAgFnSc4d1
výstavbu	výstavba	k1gFnSc4
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1945	#num#	k4
vstupují	vstupovat	k5eAaImIp3nP
do	do	k7c2
Aše	Aš	k1gFnSc2
americké	americký	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
a	a	k8xC
osvobozují	osvobozovat	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
problémy	problém	k1gInPc4
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s>
Vystěhováním	vystěhování	k1gNnSc7
německého	německý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
z	z	k7c2
Aše	Aš	k1gInSc2
klesl	klesnout	k5eAaPmAgInS
i	i	k9
přes	přes	k7c4
okamžité	okamžitý	k2eAgNnSc4d1
osidlování	osidlování	k1gNnSc4
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
byly	být	k5eAaImAgInP
některé	některý	k3yIgInPc1
závody	závod	k1gInPc1
předány	předán	k2eAgInPc1d1
do	do	k7c2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
nebo	nebo	k8xC
do	do	k7c2
rukou	ruka	k1gFnPc2
nových	nový	k2eAgMnPc2d1
českých	český	k2eAgMnPc2d1
osídlenců	osídlenec	k1gMnPc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
udržet	udržet	k5eAaPmF
všechny	všechen	k3xTgMnPc4
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
továren	továrna	k1gFnPc2
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
uzavřeno	uzavřít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reorganizace	reorganizace	k1gFnSc1
textilních	textilní	k2eAgFnPc2d1
továren	továrna	k1gFnPc2
v	v	k7c6
Aši	Aš	k1gInSc6
pokračovala	pokračovat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
zřízeny	zřízen	k2eAgInPc1d1
n.	n.	k?
p.	p.	k?
Tosta	Tosta	k1gFnSc1
<g/>
,	,	kIx,
Ohara	Ohara	k1gFnSc1
Aš	Aš	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
n.	n.	k?
p.	p.	k?
Textilana	textilana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
n.	n.	k?
p.	p.	k?
Krajka	krajka	k1gFnSc1
<g/>
,	,	kIx,
n.	n.	k?
p.	p.	k?
Kovo	kovo	k1gNnSc1
nebo	nebo	k8xC
Metaz	Metaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
také	také	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
závod	závod	k1gInSc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
Aritma	Aritma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byl	být	k5eAaImAgInS
požárem	požár	k1gInSc7
zničen	zničit	k5eAaPmNgInS
evangelický	evangelický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
nejhodnotnější	hodnotný	k2eAgFnSc1d3
památka	památka	k1gFnSc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Aši	Aš	k1gInSc6
zbouráno	zbourán	k2eAgNnSc4d1
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
starých	starý	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
domy	dům	k1gInPc7
panelovými	panelový	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
tak	tak	k6eAd1
množství	množství	k1gNnSc1
sídlišť	sídliště	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
přinesla	přinést	k5eAaPmAgNnP
přes	přes	k7c4
tisíc	tisíc	k4xCgInSc4
nových	nový	k2eAgInPc2d1
bytů	byt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
také	také	k9
přestal	přestat	k5eAaPmAgInS
klesat	klesat	k5eAaImF
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Aši	Aš	k1gInSc6
již	již	k6eAd1
11	#num#	k4
620	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
obohacováno	obohacovat	k5eAaImNgNnS
o	o	k7c4
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
nových	nový	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
rekonstruovány	rekonstruován	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
i	i	k9
ty	ten	k3xDgFnPc1
starší	starý	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibyly	přibýt	k5eAaPmAgInP
také	také	k6eAd1
obchody	obchod	k1gInPc1
<g/>
,	,	kIx,
místa	místo	k1gNnPc1
ke	k	k7c3
sportu	sport	k1gInSc2
nebo	nebo	k8xC
nové	nový	k2eAgNnSc4d1
vlakové	vlakový	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pádem	Pád	k1gInSc7
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
také	také	k9
skončila	skončit	k5eAaPmAgFnS
textilní	textilní	k2eAgFnSc1d1
éra	éra	k1gFnSc1
města	město	k1gNnSc2
Aše	Aš	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
postupně	postupně	k6eAd1
zanikají	zanikat	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
místní	místní	k2eAgFnPc4d1
továrny	továrna	k1gFnPc4
<g/>
,	,	kIx,
Tosta	Tosta	k1gFnSc1
<g/>
,	,	kIx,
Textilana	textilana	k1gFnSc1
i	i	k8xC
Krajka	krajka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Textilní	textilní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Textilana	textilana	k1gFnSc1
(	(	kIx(
<g/>
závod	závod	k1gInSc1
Ohara	Ohara	k1gMnSc1
Aš	Aš	k1gInSc1
<g/>
)	)	kIx)
zanikl	zaniknout	k5eAaPmAgInS
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vedení	vedení	k1gNnSc1
liberecké	liberecký	k2eAgFnSc2d1
Textilany	textilana	k1gFnSc2
rozhodlo	rozhodnout	k5eAaPmAgNnS
tento	tento	k3xDgInSc4
podnik	podnik	k1gInSc4
zcela	zcela	k6eAd1
uzavřít	uzavřít	k5eAaPmF
z	z	k7c2
důvodu	důvod	k1gInSc2
nerentabilnosti	nerentabilnost	k1gFnSc2
a	a	k8xC
ztrátovosti	ztrátovost	k1gFnSc2
<g/>
,	,	kIx,
závod	závod	k1gInSc4
Ohara	Ohar	k1gInSc2
Aš	Aš	k1gInSc4
až	až	k9
do	do	k7c2
zastavení	zastavení	k1gNnSc2
výroby	výroba	k1gFnSc2
vyráběl	vyrábět	k5eAaImAgMnS
dámské	dámský	k2eAgFnPc4d1
šatovky	šatovka	k1gFnPc4
a	a	k8xC
látky	látka	k1gFnPc4
na	na	k7c4
pánské	pánský	k2eAgInPc4d1
obleky	oblek	k1gInPc4
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
už	už	k6eAd1
i	i	k9
liberecká	liberecký	k2eAgFnSc1d1
Textilana	textilana	k1gFnSc1
potýkala	potýkat	k5eAaImAgFnS
s	s	k7c7
nemalými	malý	k2eNgInPc7d1
finančními	finanční	k2eAgInPc7d1
problémy	problém	k1gInPc7
(	(	kIx(
<g/>
za	za	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
sama	sám	k3xTgFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
závod	závod	k1gInSc1
Tosta	Tosto	k1gNnSc2
Aš	Aš	k1gFnSc1
zanikl	zaniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
propuštěni	propuštěn	k2eAgMnPc1d1
všichni	všechen	k3xTgMnPc1
zaměstnanci	zaměstnanec	k1gMnPc1
a	a	k8xC
výroba	výroba	k1gFnSc1
zcela	zcela	k6eAd1
zastavena	zastavit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
úplným	úplný	k2eAgInSc7d1
úpadkem	úpadek	k1gInSc7
zachránilo	zachránit	k5eAaPmAgNnS
město	město	k1gNnSc1
otevření	otevření	k1gNnSc2
hraničního	hraniční	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
celní	celní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
sídlily	sídlit	k5eAaImAgFnP
jak	jak	k6eAd1
české	český	k2eAgFnPc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
německé	německý	k2eAgInPc4d1
celní	celní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Aše	Aš	k1gFnSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stávalo	stávat	k5eAaImAgNnS
turisticky	turisticky	k6eAd1
zajímavé	zajímavý	k2eAgNnSc1d1
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
jezdilo	jezdit	k5eAaImAgNnS
nakupovat	nakupovat	k5eAaBmF
mnoho	mnoho	k4c4
Němců	Němec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Nová	nový	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
na	na	k7c4
Hlavní	hlavní	k2eAgFnSc4d1
ulici	ulice	k1gFnSc4
</s>
<s>
Vstup	vstup	k1gInSc1
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
znamenal	znamenat	k5eAaImAgMnS
pro	pro	k7c4
město	město	k1gNnSc4
novou	nový	k2eAgFnSc4d1
stavební	stavební	k2eAgFnSc4d1
éru	éra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
přibylo	přibýt	k5eAaPmAgNnS
množství	množství	k1gNnSc4
nových	nový	k2eAgInPc2d1
<g/>
,	,	kIx,
moderních	moderní	k2eAgInPc2d1
polyfunkčních	polyfunkční	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
2004	#num#	k4
a	a	k8xC
2009	#num#	k4
prošlo	projít	k5eAaPmAgNnS
město	město	k1gNnSc1
pěti	pět	k4xCc3
etapami	etapa	k1gFnPc7
rekonstrukce	rekonstrukce	k1gFnSc1
silniční	silniční	k2eAgFnSc2d1
a	a	k8xC
parkovací	parkovací	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
ašský	ašský	k2eAgInSc1d1
obchvat	obchvat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byla	být	k5eAaImAgFnS
kompletně	kompletně	k6eAd1
rekonstruována	rekonstruován	k2eAgFnSc1d1
budova	budova	k1gFnSc1
staré	starý	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
opět	opět	k6eAd1
přemístěn	přemístěn	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sousedství	sousedství	k1gNnSc6
byl	být	k5eAaImAgInS
vysvěcen	vysvěcen	k2eAgInSc1d1
památník	památník	k1gInSc1
vyhořelému	vyhořelý	k2eAgInSc3d1
evangelickému	evangelický	k2eAgInSc3d1
kostelu	kostel	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gFnSc6
blízkosti	blízkost	k1gFnSc6
stojí	stát	k5eAaImIp3nS
také	také	k9
jediný	jediný	k2eAgInSc1d1
památník	památník	k1gInSc1
Martina	Martin	k1gMnSc2
Luthera	Luther	k1gMnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
upraveny	upraven	k2eAgInPc1d1
parky	park	k1gInPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
historické	historický	k2eAgFnSc2d1
rozhledny	rozhledna	k1gFnSc2
na	na	k7c6
vrchu	vrch	k1gInSc6
Háj	háj	k1gInSc1
<g/>
,	,	kIx,
i	i	k8xC
parky	park	k1gInPc1
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInPc1d2
obchody	obchod	k1gInPc1
jsou	být	k5eAaImIp3nP
nahrazovány	nahrazovat	k5eAaImNgInP
obchodními	obchodní	k2eAgInPc7d1
domy	dům	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
navíc	navíc	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
opravám	oprava	k1gFnPc3
velkého	velký	k2eAgInSc2d1
počtu	počet	k1gInSc2
panelových	panelový	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
pokračují	pokračovat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
třetí	třetí	k4xOgInSc1
kruhový	kruhový	k2eAgInSc1d1
objezd	objezd	k1gInSc1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
Goethově	Goethův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
byly	být	k5eAaImAgFnP
také	také	k9
vloženy	vložit	k5eAaPmNgFnP
velké	velký	k2eAgFnPc1d1
investice	investice	k1gFnPc1
do	do	k7c2
revitalizace	revitalizace	k1gFnSc2
vrchu	vrch	k1gInSc2
Háj	háj	k1gInSc1
a	a	k8xC
do	do	k7c2
úpravy	úprava	k1gFnSc2
Mikulášského	mikulášský	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
spolu	spolu	k6eAd1
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
rekonstrukcí	rekonstrukce	k1gFnSc7
ašského	ašský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
došlo	dojít	k5eAaPmAgNnS
i	i	k9
na	na	k7c4
rekonstrukce	rekonstrukce	k1gFnPc4
tak	tak	k6eAd1
dlouho	dlouho	k6eAd1
opomíjených	opomíjený	k2eAgInPc2d1
německých	německý	k2eAgInPc2d1
památníků	památník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byly	být	k5eAaImAgInP
rekonstruovány	rekonstruován	k2eAgInPc1d1
památníky	památník	k1gInPc1
F.	F.	kA
L.	L.	kA
Jahna	Jahn	k1gMnSc4
a	a	k8xC
T.	T.	kA
Körnera	Körner	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
poté	poté	k6eAd1
památník	památník	k1gInSc1
Friedricha	Friedrich	k1gMnSc2
Schillera	Schiller	k1gMnSc2
a	a	k8xC
Gustava	Gustav	k1gMnSc2
Geipela	Geipel	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byla	být	k5eAaImAgFnS
také	také	k9
dokončena	dokončen	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
Goethova	Goethův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
sem	sem	k6eAd1
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
vrácena	vrácen	k2eAgFnSc1d1
restaurovaná	restaurovaný	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
se	s	k7c7
sochou	socha	k1gFnSc7
básníka	básník	k1gMnSc2
J.	J.	kA
W.	W.	kA
Goetheho	Goethe	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pověsti	pověst	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ašské	ašský	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3
ašskou	ašský	k2eAgFnSc7d1
pověstí	pověst	k1gFnSc7
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
ta	ten	k3xDgFnSc1
o	o	k7c6
Nahřbetskočovi	Nahřbetskoč	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
napadal	napadat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
pocestné	pocestný	k1gMnPc4
na	na	k7c6
vrchu	vrch	k1gInSc6
Háj	háj	k1gInSc1
<g/>
,	,	kIx,
u	u	k7c2
obávaného	obávaný	k2eAgInSc2d1
Kočičího	kočičí	k2eAgInSc2d1
smrku	smrk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověsti	pověst	k1gFnSc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
Ašska	Ašsek	k1gInSc2
byly	být	k5eAaImAgInP
sepsány	sepsat	k5eAaPmNgInP
v	v	k7c6
knize	kniha	k1gFnSc6
Ašské	ašský	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
rodákem	rodák	k1gMnSc7
Wilhelmem	Wilhelm	k1gMnSc7
Fisherem	Fisher	k1gMnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
v	v	k7c6
češtině	čeština	k1gFnSc6
vydalo	vydat	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
také	také	k6eAd1
Ašské	ašský	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
pověsti	pověst	k1gFnPc1
vydány	vydat	k5eAaPmNgFnP
na	na	k7c4
CD	CD	kA
jako	jako	k8xC,k8xS
audiokniha	audiokniha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geologická	geologický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
reliéf	reliéf	k1gInSc1
a	a	k8xC
půdy	půda	k1gFnPc1
</s>
<s>
Aš	Aš	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Smrčinách	Smrčiny	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
okresu	okres	k1gInSc2
Cheb	Cheb	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
historickém	historický	k2eAgNnSc6d1
území	území	k1gNnSc6
Ašska	Ašsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozprostírá	rozprostírat	k5eAaImIp3nS
se	se	k3xPyFc4
pod	pod	k7c7
vrchem	vrch	k1gInSc7
Háj	háj	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
Ašské	ašský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
650	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
je	být	k5eAaImIp3nS
městem	město	k1gNnSc7
zeleně	zeleň	k1gFnSc2
<g/>
,	,	kIx,
obklopena	obklopen	k2eAgFnSc1d1
lesy	les	k1gInPc7
a	a	k8xC
s	s	k7c7
množstvím	množství	k1gNnSc7
parků	park	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgInPc1d1
sady	sad	k1gInPc1
Míru	mír	k1gInSc2
jsou	být	k5eAaImIp3nP
klasifikovány	klasifikovat	k5eAaImNgInP
jako	jako	k8xC,k8xS
významný	významný	k2eAgInSc1d1
krajinný	krajinný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc7d1
okrajovou	okrajový	k2eAgFnSc7d1
částí	část	k1gFnSc7
města	město	k1gNnSc2
protéká	protékat	k5eAaImIp3nS
Ašský	ašský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
u	u	k7c2
Podhradí	Podhradí	k1gNnSc2
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
Bílého	bílý	k2eAgInSc2d1
Halštrova	Halštrův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
také	také	k9
pramení	pramenit	k5eAaImIp3nS
Račí	račí	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
Selbbach	Selbbach	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
dále	daleko	k6eAd2
teče	téct	k5eAaImIp3nS
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Aš	Aš	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
jednotný	jednotný	k2eAgInSc1d1
celek	celek	k1gInSc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
místní	místní	k2eAgFnSc7d1
částí	část	k1gFnSc7
Mokřiny	mokřina	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
obcí	obec	k1gFnPc2
Krásná	krásný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západní	západní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
města	město	k1gNnSc2
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
s	s	k7c7
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
hraniční	hraniční	k2eAgInSc4d1
přechod	přechod	k1gInSc4
Aš-Wildenau	Aš-Wildenaus	k1gInSc2
(	(	kIx(
<g/>
Selb	Selb	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Podnebí	podnebí	k1gNnSc1
je	být	k5eAaImIp3nS
drsné	drsný	k2eAgNnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
,	,	kIx,
studené	studený	k2eAgFnPc1d1
zimy	zima	k1gFnPc1
(	(	kIx(
<g/>
trvající	trvající	k2eAgFnSc1d1
4-5	4-5	k4
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
jen	jen	k9
velmi	velmi	k6eAd1
krátká	krátký	k2eAgNnPc1d1
léta	léto	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnPc2
1930	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
v	v	k7c6
1996	#num#	k4
domech	dům	k1gInPc6
22	#num#	k4
930	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
290	#num#	k4
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
československé	československý	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
a	a	k8xC
20	#num#	k4
885	#num#	k4
k	k	k7c3
německé	německý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žilo	žít	k5eAaImAgNnS
zde	zde	k6eAd1
9	#num#	k4
790	#num#	k4
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
12	#num#	k4
241	#num#	k4
evangelíků	evangelík	k1gMnPc2
<g/>
,	,	kIx,
15	#num#	k4
příslušníků	příslušník	k1gMnPc2
Církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
a	a	k8xC
64	#num#	k4
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
domů	dům	k1gInPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
<g/>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
9	#num#	k4
40513	#num#	k4
20915	#num#	k4
55718	#num#	k4
67421	#num#	k4
88019	#num#	k4
52522	#num#	k4
93010	#num#	k4
7849	#num#	k4
66711	#num#	k4
62211	#num#	k4
90511	#num#	k4
41011	#num#	k4
63211	#num#	k4
498	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
762918112513051503159019962208142314331378146315481749	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
města	město	k1gNnSc2
Aše	Aš	k1gFnSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
1850	#num#	k4
až	až	k9
1989	#num#	k4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1850	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1947	#num#	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
1975	#num#	k4
</s>
<s>
1976	#num#	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
1985	#num#	k4
</s>
<s>
1989	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
6	#num#	k4
825	#num#	k4
</s>
<s>
22	#num#	k4
930	#num#	k4
</s>
<s>
11	#num#	k4
378	#num#	k4
</s>
<s>
9	#num#	k4
667	#num#	k4
</s>
<s>
11	#num#	k4
622	#num#	k4
</s>
<s>
11	#num#	k4
963	#num#	k4
</s>
<s>
13	#num#	k4
954	#num#	k4
</s>
<s>
14	#num#	k4
054	#num#	k4
</s>
<s>
13	#num#	k4
419	#num#	k4
</s>
<s>
13	#num#	k4
229	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
města	město	k1gNnSc2
Aše	Aš	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1990	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
1993	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
13	#num#	k4
193	#num#	k4
</s>
<s>
12	#num#	k4
298	#num#	k4
</s>
<s>
12	#num#	k4
314	#num#	k4
</s>
<s>
12	#num#	k4
445	#num#	k4
</s>
<s>
12	#num#	k4
516	#num#	k4
</s>
<s>
12	#num#	k4
486	#num#	k4
</s>
<s>
12	#num#	k4
377	#num#	k4
</s>
<s>
12	#num#	k4
340	#num#	k4
</s>
<s>
12	#num#	k4
302	#num#	k4
</s>
<s>
12	#num#	k4
285	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
města	město	k1gNnSc2
Aše	Aš	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
12	#num#	k4
264	#num#	k4
</s>
<s>
12	#num#	k4
590	#num#	k4
</s>
<s>
12	#num#	k4
582	#num#	k4
</s>
<s>
12	#num#	k4
676	#num#	k4
</s>
<s>
12	#num#	k4
866	#num#	k4
</s>
<s>
12	#num#	k4
814	#num#	k4
</s>
<s>
12	#num#	k4
840	#num#	k4
</s>
<s>
12	#num#	k4
957	#num#	k4
</s>
<s>
13	#num#	k4
420	#num#	k4
</s>
<s>
13	#num#	k4
163	#num#	k4
</s>
<s>
13	#num#	k4
204	#num#	k4
</s>
<s>
13	#num#	k4
190	#num#	k4
</s>
<s>
13	#num#	k4
227	#num#	k4
</s>
<s>
13	#num#	k4
245	#num#	k4
</s>
<s>
Struktura	struktura	k1gFnSc1
populace	populace	k1gFnSc2
</s>
<s>
Věková	věkový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
Aš	Aš	k1gInSc1
roku	rok	k1gInSc2
2011	#num#	k4
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1
stav	stav	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
Aš	Aš	k1gInSc1
roku	rok	k1gInSc2
2011	#num#	k4
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
Aš	Aš	k1gInSc1
roku	rok	k1gInSc2
2011	#num#	k4
</s>
<s>
Náboženský	náboženský	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Katolíci	katolík	k1gMnPc1
z	z	k7c2
Aše	Aš	k1gFnSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
jsou	být	k5eAaImIp3nP
sdruženi	sdružen	k2eAgMnPc1d1
ve	v	k7c6
farnosti	farnost	k1gFnSc6
Aš	Aš	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Plzeňské	plzeňský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protestantismus	protestantismus	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
městě	město	k1gNnSc6
reprezentován	reprezentovat	k5eAaImNgMnS
sborem	sbor	k1gInSc7
Bratrské	bratrský	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
baptistů	baptista	k1gMnPc2
<g/>
,	,	kIx,
sborem	sbor	k1gInSc7
Českobratrské	českobratrský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
evangelické	evangelický	k2eAgFnSc2d1
a	a	k8xC
misijní	misijní	k2eAgFnSc2d1
skupinou	skupina	k1gFnSc7
Křesťanských	křesťanský	k2eAgFnPc2d1
společenství	společenství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
městě	město	k1gNnSc6
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
též	též	k9
pravoslavná	pravoslavný	k2eAgFnSc1d1
církevní	církevní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnPc4
bohoslužby	bohoslužba	k1gFnPc4
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
v	v	k7c6
chrámě	chrám	k1gInSc6
v	v	k7c6
Mokřinách	mokřina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
sbor	sbor	k1gInSc1
svědků	svědek	k1gMnPc2
Jehovových	Jehovův	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Obecní	obecní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
a	a	k8xC
politika	politika	k1gFnSc1
</s>
<s>
Místní	místní	k2eAgFnPc1d1
části	část	k1gFnPc1
</s>
<s>
Aš	Aš	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Paseky	paseka	k1gFnPc1
</s>
<s>
Doubrava	Doubrava	k1gFnSc1
</s>
<s>
Horní	horní	k2eAgFnPc1d1
Paseky	paseka	k1gFnPc1
</s>
<s>
Kopaniny	kopanina	k1gFnPc1
</s>
<s>
Mokřiny	mokřina	k1gFnPc1
</s>
<s>
Nebesa	nebesa	k1gNnPc4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Žďár	Žďár	k1gInSc1
</s>
<s>
Vernéřov	Vernéřov	k1gInSc1
</s>
<s>
Správní	správní	k2eAgNnSc1d1
území	území	k1gNnSc1
</s>
<s>
Správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
obce	obec	k1gFnSc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
Aš	Aš	k1gFnSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
město	město	k1gNnSc1
Hranice	hranice	k1gFnSc2
a	a	k8xC
obce	obec	k1gFnSc2
Hazlov	Hazlovo	k1gNnPc2
<g/>
,	,	kIx,
Krásná	krásný	k2eAgFnSc1d1
a	a	k8xC
Podhradí	Podhradí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Starostové	Starosta	k1gMnPc1
města	město	k1gNnSc2
</s>
<s>
Za	za	k7c2
Habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
a	a	k8xC
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Weiß	Weiß	k1gMnSc1
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
–	–	k?
<g/>
1861	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nikol	nikol	k1gInSc1
Ploß	Ploß	k1gFnSc2
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
–	–	k?
<g/>
1861	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Johann	Johann	k1gNnSc1
Bareuther	Bareuthra	k1gFnPc2
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nikol	nikol	k1gInSc1
Adler	Adler	k1gMnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Kässmann	Kässmann	k1gMnSc1
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nikol	nikol	k1gInSc1
Ploss	Ploss	k1gInSc1
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
–	–	k?
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Emil	Emil	k1gMnSc1
Schindler	Schindler	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
H.G.	H.G.	k?
Künzel	Künzel	k1gFnSc1
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Za	za	k7c4
První	první	k4xOgFnPc4
republiky	republika	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Hofmann	Hofmann	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Christian	Christian	k1gMnSc1
Geipel	Geipel	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Carl	Carl	k1gMnSc1
Tins	Tinsa	k1gFnPc2
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Johann	Johann	k1gMnSc1
Jäger	Jäger	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Za	za	k7c2
nacistické	nacistický	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gustav	Gustav	k1gMnSc1
Geipel	Geipel	k1gMnSc1
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
Dobl	Dobl	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
JUDr.	JUDr.	kA
Břetislav	Břetislav	k1gMnSc1
Hadač	hadač	k1gMnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Mgr.	Mgr.	kA
Antonín	Antonín	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ing.	ing.	kA
Libor	Libor	k1gMnSc1
Syrovátka	Syrovátka	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
-	-	kIx~
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
PaedDr	PaedDr	kA
<g/>
.	.	kIx.
Antonín	Antonín	k1gMnSc1
Veselý	Veselý	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mgr.	Mgr.	kA
Dalibor	Dalibor	k1gMnSc1
Blažek	Blažek	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Znak	znak	k1gInSc1
a	a	k8xC
vlajka	vlajka	k1gFnSc1
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
</s>
<s>
Znakem	znak	k1gInSc7
města	město	k1gNnSc2
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgMnPc1
stříbrní	stříbrný	k2eAgMnPc1d1
lipani	lipan	k1gMnPc1
pod	pod	k7c7
sebou	se	k3xPyFc7
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc6d1
dva	dva	k4xCgMnPc1
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
velcí	velký	k2eAgMnPc1d1
<g/>
,	,	kIx,
nejspodnější	spodní	k2eAgNnSc1d3
je	být	k5eAaImIp3nS
menší	malý	k2eAgNnSc1d2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
umístěni	umístěn	k2eAgMnPc1d1
v	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítě	štít	k1gInSc6
(	(	kIx(
<g/>
ryba	ryba	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
takzvané	takzvaný	k2eAgInPc4d1
mluvící	mluvící	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobrazuje	zobrazovat	k5eAaImIp3nS
název	název	k1gInSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgFnPc1d1
z	z	k7c2
německého	německý	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
pro	pro	k7c4
lipana	lipan	k1gMnSc4
=	=	kIx~
Äsche	Äsch	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
lipani	lipan	k1gMnPc1
jsou	být	k5eAaImIp3nP
s	s	k7c7
Aší	Aš	k1gFnSc7
spjaty	spjat	k2eAgFnPc4d1
již	již	k9
od	od	k7c2
roku	rok	k1gInSc2
1270	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
tvrzení	tvrzení	k1gNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
nikdy	nikdy	k6eAd1
potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
názor	názor	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
významného	významný	k2eAgMnSc2d1
ašského	ašský	k2eAgMnSc2d1
historika	historik	k1gMnSc2
Karla	Karel	k1gMnSc2
Albertiho	Alberti	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
lipani	lipan	k1gMnPc1
ve	v	k7c6
znaku	znak	k1gInSc6
začali	začít	k5eAaPmAgMnP
používat	používat	k5eAaImF
až	až	k9
během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Nejstarší	starý	k2eAgNnPc1d3
vyobrazení	vyobrazení	k1gNnPc1
lipanů	lipan	k1gMnPc2
v	v	k7c6
ašském	ašský	k2eAgInSc6d1
znaku	znak	k1gInSc6
je	být	k5eAaImIp3nS
totiž	totiž	k9
známo	znám	k2eAgNnSc1d1
až	až	k6eAd1
z	z	k7c2
městské	městský	k2eAgFnSc2d1
pečetě	pečeť	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1635	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgMnPc1
lipani	lipan	k1gMnPc1
však	však	k9
zkřížení	zkřížený	k2eAgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
hlavy	hlava	k1gFnPc1
směřují	směřovat	k5eAaImIp3nP
k	k	k7c3
zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnPc4d3
známé	známá	k1gFnPc4
vyobrazení	vyobrazení	k1gNnSc2
tří	tři	k4xCgMnPc2
lipanů	lipan	k1gMnPc2
pod	pod	k7c7
sebou	se	k3xPyFc7
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
dnes	dnes	k6eAd1
<g/>
)	)	kIx)
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1765	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
reliéf	reliéf	k1gInSc4
na	na	k7c6
budově	budova	k1gFnSc6
staronové	staronový	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
vedly	vést	k5eAaImAgInP
spory	spor	k1gInPc1
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
přesně	přesně	k6eAd1
má	mít	k5eAaImIp3nS
ašský	ašský	k2eAgInSc4d1
znak	znak	k1gInSc4
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
podoba	podoba	k1gFnSc1
byla	být	k5eAaImAgFnS
ustanovena	ustanovit	k5eAaPmNgFnS
až	až	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Základem	základ	k1gInSc7
byli	být	k5eAaImAgMnP
vždy	vždy	k6eAd1
tři	tři	k4xCgMnPc1
lipani	lipan	k1gMnPc1
pod	pod	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
ale	ale	k8xC
objevovaly	objevovat	k5eAaImAgFnP
se	se	k3xPyFc4
varianty	varianta	k1gFnSc2
znaku	znak	k1gInSc2
například	například	k6eAd1
s	s	k7c7
královskou	královský	k2eAgFnSc7d1
či	či	k8xC
hradební	hradební	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
nad	nad	k7c7
štítem	štít	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vlajka	vlajka	k1gFnSc1
města	město	k1gNnSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
platná	platný	k2eAgFnSc1d1
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
tím	ten	k3xDgNnSc7
město	město	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
sjednocenou	sjednocený	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
a	a	k8xC
používalo	používat	k5eAaImAgNnS
různé	různý	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současná	současný	k2eAgFnSc1d1
oficiální	oficiální	k2eAgFnSc1d1
ašská	ašský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
třech	tři	k4xCgInPc2
bílých	bílý	k2eAgInPc2d1
vlajících	vlající	k2eAgInPc2d1
klínů	klín	k1gInPc2
s	s	k7c7
vrcholy	vrchol	k1gInPc7
v	v	k7c6
polovině	polovina	k1gFnSc6
délky	délka	k1gFnSc2
modrého	modrý	k2eAgInSc2d1
listu	list	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
žerďové	žerďové	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
tři	tři	k4xCgMnPc1
bílí	bílý	k2eAgMnPc1d1
lipani	lipan	k1gMnPc1
pod	pod	k7c7
sebou	se	k3xPyFc7
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
znaku	znak	k1gInSc2
jsou	být	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc1
stejně	stejně	k6eAd1
velcí	velký	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
šířky	šířka	k1gFnSc2
k	k	k7c3
délce	délka	k1gFnSc3
listu	list	k1gInSc2
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
neoficiálních	neoficiální	k2eAgFnPc2d1,k2eNgFnPc2d1
vlajek	vlajka	k1gFnPc2
vlaje	vlát	k5eAaImIp3nS
na	na	k7c6
náměstí	náměstí	k1gNnSc6
v	v	k7c6
partnerském	partnerský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Rehau	Rehaus	k1gInSc2
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
znak	znak	k1gInSc1
města	město	k1gNnSc2
</s>
<s>
Varianta	varianta	k1gFnSc1
znaku	znak	k1gInSc2
s	s	k7c7
korunou	koruna	k1gFnSc7
nad	nad	k7c7
štítem	štít	k1gInSc7
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
na	na	k7c6
budově	budova	k1gFnSc6
bývalé	bývalý	k2eAgFnSc2d1
hasičské	hasičský	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
na	na	k7c6
Poštovním	poštovní	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
a	a	k8xC
vlajka	vlajka	k1gFnSc1
vlající	vlající	k2eAgFnSc1d1
v	v	k7c6
partnerském	partnerský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Rehau	Rehaus	k1gInSc2
</s>
<s>
Členství	členství	k1gNnSc1
ve	v	k7c6
sdruženích	sdružení	k1gNnPc6
</s>
<s>
Toto	tento	k3xDgNnSc1
druhé	druhý	k4xOgNnSc1
nejzápadnější	západní	k2eAgNnSc1d3
město	město	k1gNnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
14	#num#	k4
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
česko-německého	česko-německý	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
Přátelé	přítel	k1gMnPc1
v	v	k7c6
srdci	srdce	k1gNnSc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
někdy	někdy	k6eAd1
také	také	k6eAd1
označovaného	označovaný	k2eAgInSc2d1
za	za	k7c4
mikroregion	mikroregion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
členem	člen	k1gInSc7
sdružení	sdružení	k1gNnSc2
Ašsko	Ašsko	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
jehož	jehož	k3xOyRp3gInPc4
členy	člen	k1gInPc4
jsou	být	k5eAaImIp3nP
obce	obec	k1gFnPc1
bývalého	bývalý	k2eAgInSc2d1
ašského	ašský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
</s>
<s>
Služby	služba	k1gFnPc1
</s>
<s>
Obchodní	obchodní	k2eAgInPc1d1
řetězce	řetězec	k1gInPc1
</s>
<s>
hypermarket	hypermarket	k1gInSc1
Tesco	Tesco	k6eAd1
</s>
<s>
Lidl	Lidl	k1gMnSc1
</s>
<s>
Penny	penny	k1gInSc1
Market	market	k1gInSc1
</s>
<s>
Billa	Bill	k1gMnSc4
</s>
<s>
Travel	Travel	k1gInSc1
Free	Fre	k1gFnSc2
</s>
<s>
KIK	KIK	kA
</s>
<s>
Expert	expert	k1gMnSc1
Elektro	Elektro	k1gNnSc1
</s>
<s>
Free	Fre	k1gInPc1
One	One	k1gFnSc1
Shop	shop	k1gInSc1
</s>
<s>
Zdravotnictví	zdravotnictví	k1gNnSc1
a	a	k8xC
sociální	sociální	k2eAgFnSc1d1
sféra	sféra	k1gFnSc1
</s>
<s>
V	v	k7c6
Aši	Aš	k1gInSc6
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
nacházela	nacházet	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
velkých	velký	k2eAgInPc2d1
plánů	plán	k1gInPc2
izraelského	izraelský	k2eAgMnSc2d1
investora	investor	k1gMnSc2
je	být	k5eAaImIp3nS
plánována	plánovat	k5eAaImNgFnS
stavba	stavba	k1gFnSc1
nové	nový	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
plánu	plán	k1gInSc2
výstavby	výstavba	k1gFnSc2
však	však	k9
pro	pro	k7c4
nedostatek	nedostatek	k1gInSc4
financí	finance	k1gFnPc2
sešlo	sejít	k5eAaPmAgNnS
a	a	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
žádná	žádný	k3yNgFnSc1
výstavba	výstavba	k1gFnSc1
nekoná	konat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
větší	veliký	k2eAgNnPc4d2
zdravotní	zdravotní	k2eAgNnPc4d1
střediska	středisko	k1gNnPc4
a	a	k8xC
množství	množství	k1gNnSc4
lékařů	lékař	k1gMnPc2
různých	různý	k2eAgFnPc2d1
odborností	odbornost	k1gFnPc2
je	být	k5eAaImIp3nS
roztroušeno	roztroušet	k5eAaImNgNnS,k5eAaPmNgNnS
po	po	k7c6
dalších	další	k2eAgFnPc6d1
budovách	budova	k1gFnPc6
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
areálu	areál	k1gInSc6
bývalé	bývalý	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
v	v	k7c6
Nemocniční	nemocniční	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
dnes	dnes	k6eAd1
provozuje	provozovat	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
služby	služba	k1gFnPc4
soukromá	soukromý	k2eAgFnSc1d1
léčebna	léčebna	k1gFnSc1
dlouhodobě	dlouhodobě	k6eAd1
nemocných	nemocný	k1gMnPc2
(	(	kIx(
<g/>
LDN	LDN	kA
<g/>
)	)	kIx)
Carvac	Carvac	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
provozuje	provozovat	k5eAaImIp3nS
lůžkovou	lůžkový	k2eAgFnSc4d1
ošetřovatelskou	ošetřovatelský	k2eAgFnSc4d1
a	a	k8xC
rehabilitační	rehabilitační	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Aši	Aš	k1gInSc6
funguje	fungovat	k5eAaImIp3nS
také	také	k9
pečovatelská	pečovatelský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
důchodce	důchodce	k1gMnPc4
a	a	k8xC
invalidní	invalidní	k2eAgMnPc4d1
důchodce	důchodce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
s	s	k7c7
pečovatelskou	pečovatelský	k2eAgFnSc7d1
službou	služba	k1gFnSc7
a	a	k8xC
Domov	domov	k1gInSc1
důchodců	důchodce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgNnSc7d1
sociálním	sociální	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
Krajský	krajský	k2eAgInSc4d1
dětský	dětský	k2eAgInSc4d1
domov	domov	k1gInSc4
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
funguje	fungovat	k5eAaImIp3nS
v	v	k7c6
Aši	Aš	k1gInSc6
ve	v	k7c6
třech	tři	k4xCgFnPc6
budovách	budova	k1gFnPc6
(	(	kIx(
<g/>
2	#num#	k4
v	v	k7c6
ulici	ulice	k1gFnSc6
Na	na	k7c6
Vrchu	vrch	k1gInSc6
a	a	k8xC
jedna	jeden	k4xCgFnSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Sadová	sadový	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
zpravidla	zpravidla	k6eAd1
pro	pro	k7c4
děti	dítě	k1gFnPc4
od	od	k7c2
3	#num#	k4
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
pro	pro	k7c4
děti	dítě	k1gFnPc4
do	do	k7c2
3	#num#	k4
let	léto	k1gNnPc2
existoval	existovat	k5eAaImAgMnS
ještě	ještě	k9
Dětský	dětský	k2eAgInSc4d1
domov	domov	k1gInSc4
ve	v	k7c6
Vladivostocké	vladivostocký	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
probíhaly	probíhat	k5eAaImAgInP
kontroverzní	kontroverzní	k2eAgInPc1d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
utajené	utajený	k2eAgInPc4d1
porody	porod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Plzeňská	plzeňský	k2eAgFnSc1d1
diecézní	diecézní	k2eAgFnSc1d1
charita	charita	k1gFnSc1
v	v	k7c6
Aši	Aš	k1gInSc6
provozuje	provozovat	k5eAaImIp3nS
také	také	k9
domov	domov	k1gInSc1
sv.	sv.	kA
Zdislavy	Zdislava	k1gFnSc2
pro	pro	k7c4
matky	matka	k1gFnPc4
s	s	k7c7
dětmi	dítě	k1gFnPc7
v	v	k7c6
tísni	tíseň	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
azylový	azylový	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
žijí	žít	k5eAaImIp3nP
matky	matka	k1gFnPc4
s	s	k7c7
dětmi	dítě	k1gFnPc7
v	v	k7c6
těžké	těžký	k2eAgFnSc6d1
životní	životní	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgFnPc1d1
z	z	k7c2
rozvrácených	rozvrácený	k2eAgFnPc2d1
nebo	nebo	k8xC
týraných	týraný	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
je	být	k5eAaImIp3nS
6	#num#	k4
bytových	bytový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
s	s	k7c7
celkovým	celkový	k2eAgInSc7d1
počtem	počet	k1gInSc7
18	#num#	k4
lůžek	lůžko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zařízení	zařízení	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Klicperově	Klicperův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
</s>
<s>
Školství	školství	k1gNnSc1
</s>
<s>
V	v	k7c6
Aši	Aš	k1gInSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
3	#num#	k4
základní	základní	k2eAgFnPc1d1
školy	škola	k1gFnPc1
<g/>
,	,	kIx,
gymnázium	gymnázium	k1gNnSc1
<g/>
,	,	kIx,
5	#num#	k4
mateřských	mateřský	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
a	a	k8xC
umělecká	umělecký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
existovala	existovat	k5eAaImAgFnS
také	také	k9
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
textilní	textilní	k2eAgFnSc1d1
(	(	kIx(
<g/>
SPŠT	SPŠT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašské	ašský	k2eAgFnPc4d1
školy	škola	k1gFnPc4
pravidelně	pravidelně	k6eAd1
pořádají	pořádat	k5eAaImIp3nP
vzdělávací	vzdělávací	k2eAgNnSc4d1
setkání	setkání	k1gNnSc4
se	s	k7c7
školami	škola	k1gFnPc7
německými	německý	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZŠ	ZŠ	kA
(	(	kIx(
<g/>
Kamenná	kamenný	k2eAgFnSc1d1
ul	ul	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k9
„	„	k?
<g/>
Kamenka	kamenka	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
této	tento	k3xDgFnSc6
škole	škola	k1gFnSc6
registrováno	registrovat	k5eAaBmNgNnS
480	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
žáků	žák	k1gMnPc2
v	v	k7c6
18	#num#	k4
třídách	třída	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
škola	škola	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1890	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
stojí	stát	k5eAaImIp3nS
přímo	přímo	k6eAd1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
vlastní	vlastní	k2eAgFnSc7d1
tělocvičnou	tělocvična	k1gFnSc7
<g/>
,	,	kIx,
školní	školní	k2eAgFnSc7d1
jídelnou	jídelna	k1gFnSc7
<g/>
,	,	kIx,
technickými	technický	k2eAgFnPc7d1
dílnami	dílna	k1gFnPc7
<g/>
,	,	kIx,
učebnou	učebna	k1gFnSc7
informatiky	informatika	k1gFnSc2
nebo	nebo	k8xC
školní	školní	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
svým	svůj	k3xOyFgInSc7
velmi	velmi	k6eAd1
kladným	kladný	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
k	k	k7c3
moderním	moderní	k2eAgFnPc3d1
informačním	informační	k2eAgFnPc3d1
technologiím	technologie	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
protkány	protkat	k5eAaPmNgFnP
celou	celý	k2eAgFnSc7d1
školou	škola	k1gFnSc7
(	(	kIx(
<g/>
vlastní	vlastní	k2eAgFnSc2d1
vnitřní	vnitřní	k2eAgFnSc2d1
počítačové	počítačový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
rozdělené	rozdělený	k2eAgFnSc2d1
mezi	mezi	k7c7
I.	I.	kA
a	a	k8xC
II	II	kA
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
počítač	počítač	k1gInSc1
s	s	k7c7
internetem	internet	k1gInSc7
v	v	k7c6
každé	každý	k3xTgFnSc6
učebně	učebna	k1gFnSc6
<g/>
,	,	kIx,
digitální	digitální	k2eAgFnSc1d1
projektory	projektor	k1gInPc4
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
samostatné	samostatný	k2eAgFnPc4d1
<g/>
,	,	kIx,
moderně	moderně	k6eAd1
vybavené	vybavený	k2eAgFnPc4d1
učebny	učebna	k1gFnPc4
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
či	či	k8xC
vlastní	vlastní	k2eAgFnSc1d1
internetová	internetový	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
TV	TV	kA
Kamenka	kamenka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
škole	škola	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
osvědčení	osvědčení	k1gNnSc4
o	o	k7c6
tzv.	tzv.	kA
počítačové	počítačový	k2eAgFnSc2d1
gramotnosti	gramotnost	k1gFnSc2
<g/>
,	,	kIx,
ECDL	ECDL	kA
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZŠ	ZŠ	kA
(	(	kIx(
<g/>
Hlávkova	Hlávkův	k2eAgFnSc1d1
ul	ul	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
této	tento	k3xDgFnSc6
škole	škola	k1gFnSc6
registrováno	registrovat	k5eAaBmNgNnS
377	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
žáků	žák	k1gMnPc2
v	v	k7c6
17	#num#	k4
třídách	třída	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česky	Česko	k1gNnPc7
se	se	k3xPyFc4
zde	zde	k6eAd1
poprvé	poprvé	k6eAd1
začalo	začít	k5eAaPmAgNnS
vyučovat	vyučovat	k5eAaImF
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZŠ	ZŠ	kA
budovu	budova	k1gFnSc4
staré	starý	k2eAgFnSc2d1
německé	německý	k2eAgFnSc2d1
školy	škola	k1gFnSc2
na	na	k7c4
Hlavní	hlavní	k2eAgFnSc4d1
ulici	ulice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
zbourána	zbourán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
vlastní	vlastní	k2eAgFnSc7d1
velkou	velký	k2eAgFnSc7d1
tělocvičnou	tělocvična	k1gFnSc7
<g/>
,	,	kIx,
hřištěm	hřiště	k1gNnSc7
<g/>
,	,	kIx,
školními	školní	k2eAgFnPc7d1
zahradami	zahrada	k1gFnPc7
<g/>
,	,	kIx,
jídelnou	jídelna	k1gFnSc7
<g/>
,	,	kIx,
školní	školní	k2eAgFnSc7d1
družinou	družina	k1gFnSc7
<g/>
,	,	kIx,
technickými	technický	k2eAgFnPc7d1
dílnami	dílna	k1gFnPc7
nebo	nebo	k8xC
také	také	k9
učebnou	učebna	k1gFnSc7
informatiky	informatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
Jiří	Jiří	k1gMnSc1
Honomichl	Honomichl	k1gMnSc1
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
finálového	finálový	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
soutěže	soutěž	k1gFnSc2
o	o	k7c4
nejoblíbenějšího	oblíbený	k2eAgMnSc4d3
učitele	učitel	k1gMnSc4
Zlatý	zlatý	k2eAgMnSc1d1
Ámos	Ámos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
tak	tak	k9
prvním	první	k4xOgMnSc7
ašským	ašský	k2eAgMnSc7d1
učitelem	učitel	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZŠ	ZŠ	kA
(	(	kIx(
<g/>
Okružní	okružní	k2eAgMnSc1d1
ul	ul	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k9
„	„	k?
<g/>
Gymnázka	Gymnázka	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
navštěvovalo	navštěvovat	k5eAaImAgNnS
tuto	tento	k3xDgFnSc4
školu	škola	k1gFnSc4
216	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
gymnázium	gymnázium	k1gNnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
třídou	třída	k1gFnSc7
vybavenou	vybavený	k2eAgFnSc7d1
pro	pro	k7c4
dyslektické	dyslektický	k2eAgNnSc4d1
<g/>
,	,	kIx,
dysgrafické	dysgrafický	k2eAgFnPc4d1
a	a	k8xC
dysortografické	dysortografický	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učitelé	učitel	k1gMnPc1
též	též	k9
umí	umět	k5eAaImIp3nP
jednat	jednat	k5eAaImF
s	s	k7c7
dětmi	dítě	k1gFnPc7
s	s	k7c7
logopedickými	logopedický	k2eAgFnPc7d1
poruchami	porucha	k1gFnPc7
řeči	řeč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
vlastní	vlastní	k2eAgFnSc7d1
tělocvičnou	tělocvična	k1gFnSc7
<g/>
,	,	kIx,
školním	školní	k2eAgNnSc7d1
hřištěm	hřiště	k1gNnSc7
<g/>
,	,	kIx,
jídelnou	jídelna	k1gFnSc7
<g/>
,	,	kIx,
družinou	družina	k1gFnSc7
a	a	k8xC
učebnou	učebna	k1gFnSc7
informatiky	informatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
škole	škola	k1gFnSc3
je	být	k5eAaImIp3nS
připojena	připojit	k5eAaPmNgFnS
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
budovou	budova	k1gFnSc7
školy	škola	k1gFnSc2
stojí	stát	k5eAaImIp3nS
Benešův	Benešův	k2eAgInSc1d1
památník	památník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZŠ	ZŠ	kA
a	a	k8xC
praktická	praktický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
Studentská	studentský	k2eAgFnSc1d1
ul	ul	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
tuto	tento	k3xDgFnSc4
školu	škola	k1gFnSc4
navštěvovalo	navštěvovat	k5eAaImAgNnS
166	#num#	k4
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
školy	škola	k1gFnSc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
budova	budova	k1gFnSc1
soudu	soud	k1gInSc2
okresu	okres	k1gInSc2
Aš	Aš	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
školní	školní	k2eAgFnSc7d1
jídelnou	jídelna	k1gFnSc7
<g/>
,	,	kIx,
posilovnou	posilovna	k1gFnSc7
<g/>
,	,	kIx,
učebnou	učebna	k1gFnSc7
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
knihovnou	knihovna	k1gFnSc7
a	a	k8xC
speciálními	speciální	k2eAgFnPc7d1
učebnami	učebna	k1gFnPc7
<g/>
:	:	kIx,
keramická	keramický	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
<g/>
,	,	kIx,
šicí	šicí	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
<g/>
,	,	kIx,
kovodílna	kovodílna	k1gFnSc1
<g/>
,	,	kIx,
technické	technický	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
bylo	být	k5eAaImAgNnS
vystavěno	vystavěn	k2eAgNnSc1d1
nové	nový	k2eAgNnSc1d1
školní	školní	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
Aš	Aš	k1gFnSc1
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgFnSc1d1
ul	ul	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
osmileté	osmiletý	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
je	být	k5eAaImIp3nS
zaměřeno	zaměřit	k5eAaPmNgNnS
na	na	k7c4
výuku	výuka	k1gFnSc4
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrozšířenějším	rozšířený	k2eAgInSc7d3
vyučovaným	vyučovaný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
je	být	k5eAaImIp3nS
němčina	němčina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gymnázium	gymnázium	k1gNnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
15	#num#	k4
škol	škola	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
tzv.	tzv.	kA
Sprachdiplom	Sprachdiplom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
zde	zde	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
rekonstrukce	rekonstrukce	k1gFnPc4
s	s	k7c7
cílem	cíl	k1gInSc7
modernizace	modernizace	k1gFnSc2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gymnázium	gymnázium	k1gNnSc1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
kulturních	kulturní	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
Ašlerky	Ašlerk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žáci	Žák	k1gMnPc1
školy	škola	k1gFnSc2
pravidelně	pravidelně	k6eAd1
obsazují	obsazovat	k5eAaImIp3nP
vysoké	vysoký	k2eAgFnPc4d1
příčky	příčka	k1gFnPc4
v	v	k7c6
celostátních	celostátní	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
v	v	k7c6
biologii	biologie	k1gFnSc6
<g/>
,	,	kIx,
německém	německý	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
či	či	k8xC
chemii	chemie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
letní	letní	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Den	den	k1gInSc1
pro	pro	k7c4
rodinu	rodina	k1gFnSc4
nebo	nebo	k8xC
divadelně-hudební	divadelně-hudební	k2eAgInSc4d1
festival	festival	k1gInSc4
Ašlerky	Ašlerka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
působí	působit	k5eAaImIp3nS
divadelní	divadelní	k2eAgInSc1d1
soubor	soubor	k1gInSc1
Podividlo	Podividlo	k1gNnSc1
ažažAš	ažažAš	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
Aši	Aš	k1gInSc6
odehrály	odehrát	k5eAaPmAgFnP
rekonstrukce	rekonstrukce	k1gFnPc1
některých	některý	k3yIgFnPc2
důležitých	důležitý	k2eAgFnPc2d1
historických	historický	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
vstup	vstup	k1gInSc4
amerických	americký	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
do	do	k7c2
města	město	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
poté	poté	k6eAd1
boj	boj	k1gInSc1
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
a	a	k8xC
nacisty	nacista	k1gMnPc7
na	na	k7c6
Goethově	Goethův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
poté	poté	k6eAd1
také	také	k9
proběhla	proběhnout	k5eAaPmAgFnS
rekonstrukce	rekonstrukce	k1gFnSc1
vstupu	vstup	k1gInSc2
vojsk	vojsko	k1gNnPc2
Varšavské	varšavský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
do	do	k7c2
Aše	Aš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
všechno	všechen	k3xTgNnSc1
včetně	včetně	k7c2
historické	historický	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
kostýmů	kostým	k1gInPc2
a	a	k8xC
kulis	kulisa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
také	také	k9
v	v	k7c6
několika	několik	k4yIc6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
zhlédnout	zhlédnout	k5eAaPmF
úspěšné	úspěšný	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
automobilových	automobilový	k2eAgMnPc2d1
veteránů	veterán	k1gMnPc2
či	či	k8xC
historické	historický	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
tanků	tank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ašské	ašský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
pravidelně	pravidelně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
výstavy	výstava	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
Ašska	Ašsek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
v	v	k7c6
Aši	Aš	k1gInSc6
existovala	existovat	k5eAaImAgFnS
i	i	k9
soukromá	soukromý	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
Anny	Anna	k1gFnSc2
Tyrolerové	Tyrolerová	k1gFnSc2
Na	na	k7c6
Háji	háj	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
prezentovala	prezentovat	k5eAaBmAgFnS
umění	umění	k1gNnSc4
ašských	ašský	k2eAgMnPc2d1
tvůrců	tvůrce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
několika	několik	k4yIc6
požárech	požár	k1gInPc6
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
hudební	hudební	k2eAgInSc4d1
zbourán	zbourán	k2eAgInSc4d1
klub	klub	k1gInSc4
Klubíčko	klubíčko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Aš	Aš	k1gInSc1
poté	poté	k6eAd1
nechalo	nechat	k5eAaPmAgNnS
zrekonstruovat	zrekonstruovat	k5eAaPmF
prázdnou	prázdný	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
bývalého	bývalý	k2eAgNnSc2d1
učiliště	učiliště	k1gNnSc2
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
zde	zde	k6eAd1
nové	nový	k2eAgNnSc4d1
kulturní	kulturní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
LaRitma	LaRitmum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
přesídla	přesídnout	k5eAaPmAgFnS
i	i	k9
ZUŠ	ZUŠ	kA
Roberta	Roberta	k1gFnSc1
Schumanna	Schumanna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěchy	úspěch	k1gInPc4
na	na	k7c6
hudební	hudební	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
regionu	region	k1gInSc2
i	i	k9
mimo	mimo	k7c4
něj	on	k3xPp3gMnSc4
sklízeli	sklízet	k5eAaImAgMnP
ašské	ašský	k2eAgFnSc2d1
hudební	hudební	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Mac	Mac	kA
Beth	Beth	k1gMnSc1
(	(	kIx(
<g/>
již	již	k6eAd1
zaniklá	zaniklý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
0609	#num#	k4
a	a	k8xC
Afekt	afekt	k1gInSc1
nebo	nebo	k8xC
zpěvák	zpěvák	k1gMnSc1
Petr	Petr	k1gMnSc1
Sepeši	Sepech	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
k	k	k7c3
populárním	populární	k2eAgMnPc3d1
umělcům	umělec	k1gMnPc3
patří	patřit	k5eAaImIp3nP
Petr	Petr	k1gMnSc1
Bundil	Bundil	k1gMnSc1
(	(	kIx(
<g/>
skupina	skupina	k1gFnSc1
Slza	slza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgMnS
k	k	k7c3
dvoustému	dvoustý	k2eAgNnSc3d1
výročí	výročí	k1gNnSc3
narození	narození	k1gNnSc2
skladatele	skladatel	k1gMnSc2
Roberta	Robert	k1gMnSc2
Schumanna	Schumann	k1gMnSc2
uskutečněn	uskutečněn	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Schumann	Schumann	k1gNnSc1
2010	#num#	k4
–	–	k?
"	"	kIx"
<g/>
Mládí	mládí	k1gNnSc1
<g/>
,	,	kIx,
láska	láska	k1gFnSc1
&	&	k?
hudba	hudba	k1gFnSc1
bez	bez	k7c2
hranic	hranice	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Města	město	k1gNnSc2
Aš	Aš	k1gInSc1
a	a	k8xC
saský	saský	k2eAgMnSc1d1
Bad	Bad	k1gMnPc1
Elster	Elstrum	k1gNnPc2
pátrali	pátrat	k5eAaImAgMnP
po	po	k7c6
stopách	stopa	k1gFnPc6
Roberta	Robert	k1gMnSc2
Schumanna	Schumann	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
Aš	Aš	k1gInSc4
navštěvoval	navštěvovat	k5eAaImAgMnS
a	a	k8xC
uskutečnili	uskutečnit	k5eAaPmAgMnP
několik	několik	k4yIc4
koncertů	koncert	k1gInPc2
v	v	k7c6
obou	dva	k4xCgNnPc6
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
V	v	k7c6
Aši	Aš	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Tyršův	Tyršův	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
nikdo	nikdo	k3yNnSc1
neřekne	říct	k5eNaPmIp3nS
jinak	jinak	k6eAd1
než	než	k8xS
„	„	k?
<g/>
Tyršák	Tyršák	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
fotbalové	fotbalový	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
<g/>
,	,	kIx,
krytý	krytý	k2eAgInSc1d1
plavecký	plavecký	k2eAgInSc1d1
bazén	bazén	k1gInSc1
<g/>
,	,	kIx,
skateboardové	skateboardový	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
<g/>
,	,	kIx,
bowlingová	bowlingový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
tenisové	tenisový	k2eAgInPc4d1
kurty	kurt	k1gInPc4
nebo	nebo	k8xC
střelnice	střelnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
Aš	Aš	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k6eAd1
protkána	protkat	k5eAaPmNgFnS
počtem	počet	k1gInSc7
cyklotras	cyklotrasa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
spojují	spojovat	k5eAaImIp3nP
nejen	nejen	k6eAd1
celé	celý	k2eAgNnSc4d1
Ašsko	Ašsko	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
se	se	k3xPyFc4
připojují	připojovat	k5eAaImIp3nP
na	na	k7c4
cyklotrasy	cyklotrasa	k1gFnPc4
německého	německý	k2eAgNnSc2d1
Bavorska	Bavorsko	k1gNnSc2
a	a	k8xC
Saska	Sasko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
vrchu	vrch	k1gInSc6
Háj	háj	k1gInSc4
otevřen	otevřen	k2eAgInSc1d1
lyžařský	lyžařský	k2eAgInSc1d1
areál	areál	k1gInSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
vleku	vlek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
ašským	ašský	k2eAgInSc7d1
sportovním	sportovní	k2eAgInSc7d1
klubem	klub	k1gInSc7
je	být	k5eAaImIp3nS
bezesporu	bezesporu	k9
TJ	tj	kA
Jiskra	jiskra	k1gFnSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
čítá	čítat	k5eAaImIp3nS
bezmála	bezmála	k6eAd1
800	#num#	k4
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
pracují	pracovat	k5eAaImIp3nP
turistické	turistický	k2eAgInPc1d1
spolky	spolek	k1gInPc1
<g/>
,	,	kIx,
například	například	k6eAd1
Smrčinský	Smrčinský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
KČT	KČT	kA
Aš	Aš	k1gInSc1
nebo	nebo	k8xC
Sdružení	sdružení	k1gNnSc1
rodáků	rodák	k1gMnPc2
<g/>
,	,	kIx,
přátel	přítel	k1gMnPc2
a	a	k8xC
patriotů	patriot	k1gMnPc2
města	město	k1gNnSc2
Aše	Aš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
působí	působit	k5eAaImIp3nP
další	další	k2eAgInPc4d1
sportovní	sportovní	k2eAgInPc4d1
kluby	klub	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Ašští	ašský	k2eAgMnPc1d1
bajkeři	bajker	k1gMnPc1
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Volejbalový	volejbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Aš	Aš	k1gInSc1
<g/>
,	,	kIx,
Tenisový	tenisový	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
LK	LK	kA
Jasan	jasan	k1gInSc1
Aš	Aš	k1gInSc1
(	(	kIx(
<g/>
běh	běh	k1gInSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
,	,	kIx,
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alpin	alpinum	k1gNnPc2
klub	klub	k1gInSc1
Aš	Aš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
získala	získat	k5eAaPmAgFnS
Lenka	Lenka	k1gFnSc1
Marušková	Marušková	k1gFnSc1
z	z	k7c2
Aše	Aš	k1gFnSc2
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Athénách	Athéna	k1gFnPc6
stříbrnou	stříbrný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
„	„	k?
<g/>
Revitalizace	revitalizace	k1gFnSc1
vrchu	vrch	k1gInSc2
Háj	háj	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
pod	pod	k7c7
vrchem	vrch	k1gInSc7
Háj	háj	k1gInSc4
velké	velký	k2eAgNnSc4d1
sportovní	sportovní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
fotbalového	fotbalový	k2eAgNnSc2d1
hřiště	hřiště	k1gNnSc2
<g/>
,	,	kIx,
tenisových	tenisový	k2eAgInPc2d1
kurtů	kurt	k1gInPc2
<g/>
,	,	kIx,
in-line	in-lin	k1gInSc5
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
lezecké	lezecký	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
<g/>
,	,	kIx,
minigolfu	minigolf	k1gInSc6
<g/>
,	,	kIx,
pétanque	pétanque	k1gFnPc7
<g/>
,	,	kIx,
apod.	apod.	kA
Byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
dvě	dva	k4xCgNnPc1
velká	velký	k2eAgNnPc1d1
parkoviště	parkoviště	k1gNnPc1
<g/>
,	,	kIx,
a	a	k8xC
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
zcela	zcela	k6eAd1
nová	nový	k2eAgFnSc1d1
infrastruktura	infrastruktura	k1gFnSc1
k	k	k7c3
rozhledně	rozhledna	k1gFnSc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
osvětlení	osvětlení	k1gNnSc2
a	a	k8xC
chodníků	chodník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgInPc1d1
náklady	náklad	k1gInPc1
na	na	k7c4
tento	tento	k3xDgInSc4
projekt	projekt	k1gInSc4
činili	činit	k5eAaImAgMnP
130,6	130,6	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
115	#num#	k4
milionů	milion	k4xCgInPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
grantového	grantový	k2eAgInSc2d1
systému	systém	k1gInSc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2012	#num#	k4
slavnostně	slavnostně	k6eAd1
zahájil	zahájit	k5eAaPmAgInS
činnost	činnost	k1gFnSc4
ašského	ašský	k2eAgInSc2d1
badmintonového	badmintonový	k2eAgInSc2d1
oddílu	oddíl	k1gInSc2
reprezentant	reprezentant	k1gMnSc1
ČR	ČR	kA
a	a	k8xC
olympionik	olympionik	k1gMnSc1
Petr	Petr	k1gMnSc1
Koukal	Koukal	k1gMnSc1
ve	v	k7c6
víceúčelové	víceúčelový	k2eAgFnSc6d1
sportovní	sportovní	k2eAgFnSc6d1
hale	hala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Do	do	k7c2
Aše	Aš	k1gInSc2
vede	vést	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
číslo	číslo	k1gNnSc1
I	i	k8xC
<g/>
/	/	kIx~
<g/>
64	#num#	k4
z	z	k7c2
Chebu	Cheb	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
na	na	k7c4
ašský	ašský	k2eAgInSc4d1
obchvat	obchvat	k1gInSc4
<g/>
,	,	kIx,
dostavěný	dostavěný	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
64	#num#	k4
se	se	k3xPyFc4
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c6
silnici	silnice	k1gFnSc6
vedoucí	vedoucí	k1gMnSc1
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Aše	Aš	k1gInSc2
poté	poté	k6eAd1
pokračují	pokračovat	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
II	II	kA
<g/>
/	/	kIx~
<g/>
216	#num#	k4
ve	v	k7c6
směru	směr	k1gInSc6
Doubrava	Doubrava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
saskou	saský	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
do	do	k7c2
Bad	Bad	k1gFnSc2
Elsteru	Elster	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
silnice	silnice	k1gFnPc1
II	II	kA
<g/>
/	/	kIx~
<g/>
217	#num#	k4
ve	v	k7c6
směru	směr	k1gInSc6
Hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
saskou	saský	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
v	v	k7c6
obci	obec	k1gFnSc6
Ebmath	Ebmatha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Aši	Aš	k1gFnSc6
zahrnuje	zahrnovat	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
linky	linka	k1gFnPc4
<g/>
,	,	kIx,
integrované	integrovaný	k2eAgFnSc6d1
do	do	k7c2
IDOK	IDOK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
autobusy	autobus	k1gInPc1
značky	značka	k1gFnSc2
SOR	SOR	kA
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
SOR	SOR	kA
C	C	kA
9,5	9,5	k4
<g/>
.	.	kIx.
</s>
<s>
MHD	MHD	kA
Aš	Aš	k1gFnSc1
</s>
<s>
MHD	MHD	kA
Aš	Aš	k1gFnSc1
</s>
<s>
Železnice	železnice	k1gFnSc1
</s>
<s>
Budova	budova	k1gFnSc1
železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
Aš	Aš	k1gFnSc1
</s>
<s>
Aš	Aš	k1gFnSc1
s	s	k7c7
okolními	okolní	k2eAgNnPc7d1
městy	město	k1gNnPc7
spojuje	spojovat	k5eAaImIp3nS
i	i	k9
železnice	železnice	k1gFnSc1
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
trať	trať	k1gFnSc1
148	#num#	k4
Cheb	Cheb	k1gInSc1
–	–	k?
Františkovy	Františkův	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
–	–	k?
Hranice	hranice	k1gFnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelný	pravidelný	k2eAgInSc1d1
spoj	spoj	k1gInSc1
z	z	k7c2
Chebu	Cheb	k1gInSc2
do	do	k7c2
Aše	Aš	k1gFnSc2
jezdí	jezdit	k5eAaImIp3nP
v	v	k7c4
pracovní	pracovní	k2eAgInPc4d1
dny	den	k1gInPc4
každým	každý	k3xTgInSc7
směrem	směr	k1gInSc7
dvanáctkrát	dvanáctkrát	k6eAd1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
spoj	spoj	k1gInSc4
denně	denně	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
města	město	k1gNnSc2
Hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
víkendových	víkendový	k2eAgNnPc2d1
spojení	spojení	k1gNnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
mezi	mezi	k7c7
devíti	devět	k4xCc7
a	a	k8xC
deseti	deset	k4xCc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aši	Aš	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
stanice	stanice	k1gFnSc1
Aš	Aš	k1gFnSc1
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Aš	Aš	k1gInSc1
město	město	k1gNnSc1
(	(	kIx(
<g/>
známé	známý	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
malé	malý	k2eAgNnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
horní	horní	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
zastávka	zastávka	k1gFnSc1
Aš	Aš	k1gFnSc1
předměstí	předměstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
ašské	ašský	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
a	a	k8xC
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
jakýmsi	jakýsi	k3yIgInSc7
menším	malý	k2eAgInSc7d2
modelem	model	k1gInSc7
chebského	chebský	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchozí	předchozí	k2eAgFnPc4d1
velké	velká	k1gFnPc4
nádraží	nádraží	k1gNnSc2
<g/>
,	,	kIx,
nazývané	nazývaný	k2eAgNnSc1d1
bavorské	bavorský	k2eAgNnSc1d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zbouráno	zbourat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
Aše	Aš	k1gInSc2
vedou	vést	k5eAaImIp3nP
koleje	kolej	k1gFnPc1
ještě	ještě	k9
dále	daleko	k6eAd2
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
města	město	k1gNnSc2
Hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
trať	trať	k1gFnSc1
směřovala	směřovat	k5eAaImAgFnS
do	do	k7c2
německého	německý	k2eAgInSc2d1
Selbu	Selb	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
trasa	trasa	k1gFnSc1
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
uzavřena	uzavřít	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
vlak	vlak	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
tudy	tudy	k6eAd1
projel	projet	k5eAaPmAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
tzv.	tzv.	kA
Vlak	vlak	k1gInSc4
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vyjel	vyjet	k5eAaPmAgInS
dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1951	#num#	k4
z	z	k7c2
Hazlova	Hazlův	k2eAgMnSc2d1
<g/>
,	,	kIx,
nezastavil	zastavit	k5eNaPmAgMnS
na	na	k7c6
ašském	ašský	k2eAgNnSc6d1
nádraží	nádraží	k1gNnSc6
<g/>
,	,	kIx,
prolomil	prolomit	k5eAaPmAgInS
dřevěné	dřevěný	k2eAgInPc4d1
zátarasy	zátaras	k1gInPc4
a	a	k8xC
přejel	přejet	k5eAaPmAgMnS
státní	státní	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
do	do	k7c2
tehdejšího	tehdejší	k2eAgNnSc2d1
Západního	západní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
cestující	cestující	k1gMnPc1
se	se	k3xPyFc4
o	o	k7c4
pár	pár	k4xCyI
dnů	den	k1gInPc2
vrátili	vrátit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
do	do	k7c2
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
někteří	některý	k3yIgMnPc1
zůstali	zůstat	k5eAaPmAgMnP
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
trať	trať	k1gFnSc1
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
pouze	pouze	k6eAd1
pro	pro	k7c4
nákladní	nákladní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
i	i	k8xC
německá	německý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
přemýšlela	přemýšlet	k5eAaImAgFnS
o	o	k7c6
obnovení	obnovení	k1gNnSc6
trasy	trasa	k1gFnSc2
Aš-Selb	Aš-Selba	k1gFnPc2
od	od	k7c2
počátku	počátek	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
realizaci	realizace	k1gFnSc3
<g/>
,	,	kIx,
pro	pro	k7c4
nezájem	nezájem	k1gInSc4
z	z	k7c2
bavorské	bavorský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Aš	Aš	k1gInSc1
se	se	k3xPyFc4
i	i	k9
přesto	přesto	k8xC
vždy	vždy	k6eAd1
snažilo	snažit	k5eAaImAgNnS
návrhy	návrh	k1gInPc4
znovu	znovu	k6eAd1
předkládat	předkládat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
SŽDC	SŽDC	kA
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
tento	tento	k3xDgInSc1
úsek	úsek	k1gInSc1
železnice	železnice	k1gFnSc2
ke	k	k7c3
státní	státní	k2eAgFnSc3d1
hranici	hranice	k1gFnSc3
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
rozebrán	rozebrán	k2eAgInSc1d1
<g/>
,	,	kIx,
německá	německý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
obrátila	obrátit	k5eAaPmAgFnS
a	a	k8xC
okamžitě	okamžitě	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
o	o	k7c4
trať	trať	k1gFnSc4
zajímat	zajímat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sousedním	sousední	k2eAgNnSc6d1
městě	město	k1gNnSc6
Selb	Selba	k1gFnPc2
bylo	být	k5eAaImAgNnS
vypsáno	vypsán	k2eAgNnSc1d1
referendum	referendum	k1gNnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měli	mít	k5eAaImAgMnP
občané	občan	k1gMnPc1
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
chtějí	chtít	k5eAaImIp3nP
trať	trať	k1gFnSc4
z	z	k7c2
Aše	Aš	k1gFnSc2
či	či	k8xC
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Referendum	referendum	k1gNnSc1
dopadlo	dopadnout	k5eAaPmAgNnS
kladně	kladně	k6eAd1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Hofu	Hofus	k1gInSc6
podepsána	podepsat	k5eAaPmNgFnS
česko-bavorská	česko-bavorský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
znovuobnovení	znovuobnovení	k1gNnSc6
tratě	trať	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekonstrukce	rekonstrukce	k1gFnSc1
českého	český	k2eAgInSc2d1
úseku	úsek	k1gInSc2
nebyla	být	k5eNaImAgFnS
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
německou	německý	k2eAgFnSc7d1
částí	část	k1gFnSc7
tak	tak	k6eAd1
nákladná	nákladný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
německé	německý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
vybudovat	vybudovat	k5eAaPmF
přemostění	přemostění	k1gNnSc4
na	na	k7c6
dvou	dva	k4xCgNnPc6
místech	místo	k1gNnPc6
u	u	k7c2
obce	obec	k1gFnSc2
Erkersreuth	Erkersreuth	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vybudován	vybudovat	k5eAaPmNgInS
obchvat	obchvat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlaky	vlak	k1gInPc1
měly	mít	k5eAaImAgFnP
z	z	k7c2
Aše	Aš	k1gFnSc2
do	do	k7c2
Selbu	Selb	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
z	z	k7c2
Chebu	Cheb	k1gInSc2
do	do	k7c2
Hofu	Hofus	k1gInSc2
jezdit	jezdit	k5eAaImF
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
provoz	provoz	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
zahájen	zahájit	k5eAaPmNgInS
teprve	teprve	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
se	se	k3xPyFc4
Aš	Aš	k1gFnSc1
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
starší	starší	k1gMnPc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
nezachovalo	zachovat	k5eNaPmAgNnS
se	se	k3xPyFc4
zde	zde	k6eAd1
příliš	příliš	k6eAd1
historických	historický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejcennější	cenný	k2eAgFnSc7d3
památkou	památka	k1gFnSc7
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
zničen	zničit	k5eAaPmNgInS
při	při	k7c6
požáru	požár	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
historické	historický	k2eAgInPc1d1
domy	dům	k1gInPc1
na	na	k7c6
hlavním	hlavní	k2eAgNnSc6d1
<g/>
,	,	kIx,
Goethově	Goethův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
takzvaného	takzvaný	k2eAgInSc2d1
ašského	ašský	k2eAgInSc2d1
špalíčku	špalíček	k1gInSc2
<g/>
,	,	kIx,
strženy	stržen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
místa	místo	k1gNnPc4
zaplnila	zaplnit	k5eAaPmAgFnS
většinou	většinou	k6eAd1
panelová	panelový	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Stavby	stavba	k1gFnPc1
</s>
<s>
Budova	budova	k1gFnSc1
radnice	radnice	k1gFnSc1
byla	být	k5eAaImAgFnS
jako	jako	k9
barokní	barokní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1733	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1814	#num#	k4
však	však	k9
vyhořela	vyhořet	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1816	#num#	k4
byla	být	k5eAaImAgFnS
znovu	znovu	k6eAd1
postavena	postaven	k2eAgFnSc1d1
podle	podle	k7c2
původních	původní	k2eAgInPc2d1
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
přistavěno	přistavěn	k2eAgNnSc1d1
další	další	k2eAgNnSc1d1
patro	patro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
prošla	projít	k5eAaPmAgFnS
kompletní	kompletní	k2eAgFnSc7d1
rekonstrukcí	rekonstrukce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývala	bývat	k5eAaImAgFnS
zde	zde	k6eAd1
knihovna	knihovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
se	se	k3xPyFc4
budova	budova	k1gFnSc1
stala	stát	k5eAaPmAgFnS
opět	opět	k6eAd1
městskou	městský	k2eAgFnSc7d1
radnicí	radnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Ašské	ašský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
nyní	nyní	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
budově	budova	k1gFnSc6
v	v	k7c6
Mikulášské	mikulášský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
zámek	zámek	k1gInSc1
Zedtwitzů	Zedtwitz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
zhlédnout	zhlédnout	k5eAaPmF
v	v	k7c6
Evropě	Evropa	k1gFnSc6
ojedinělou	ojedinělý	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
25	#num#	k4
000	#num#	k4
párů	pár	k1gInPc2
rukavic	rukavice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašské	ašský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
také	také	k9
vede	vést	k5eAaImIp3nS
celostátní	celostátní	k2eAgFnSc4d1
evidenci	evidence	k1gFnSc4
smírčích	smírčí	k1gMnPc2
křížu	křízat	k5eAaPmIp1nS
<g/>
,	,	kIx,
kterých	který	k3yQgNnPc2,k3yRgNnPc2,k3yIgNnPc2
se	se	k3xPyFc4
na	na	k7c6
Ašsku	Ašsk	k1gInSc6
také	také	k9
několik	několik	k4yIc1
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
je	být	k5eAaImIp3nS
pseudorenesanční	pseudorenesanční	k2eAgInSc1d1
římskokatolický	římskokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
dostavěn	dostavět	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradil	nahradit	k5eAaPmAgInS
původní	původní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
postaven	postavit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
zaniklého	zaniklý	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
Zedtwitzů	Zedtwitz	k1gInPc2
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
již	již	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
nevyhovoval	vyhovovat	k5eNaImAgMnS
svou	svůj	k3xOyFgFnSc7
velikostí	velikost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1
na	na	k7c6
vrchu	vrch	k1gInSc6
Háj	háj	k1gInSc1
nebo	nebo	k8xC
také	také	k9
Ašská	ašský	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
Bismarckova	Bismarckův	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
v	v	k7c6
letech	let	k1gInPc6
1902	#num#	k4
<g/>
–	–	k?
<g/>
1903	#num#	k4
podle	podle	k7c2
plánů	plán	k1gInPc2
architekta	architekt	k1gMnSc2
Wilhelma	Wilhelm	k1gMnSc2
Kreise	Kreise	k1gFnSc2
<g/>
,	,	kIx,
slavného	slavný	k2eAgMnSc2d1
autora	autor	k1gMnSc2
Bismarckových	Bismarckův	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhledna	rozhledna	k1gFnSc1
je	být	k5eAaImIp3nS
36	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dominantou	dominanta	k1gFnSc7
a	a	k8xC
symbolem	symbol	k1gInSc7
města	město	k1gNnSc2
a	a	k8xC
hojně	hojně	k6eAd1
turisty	turist	k1gMnPc7
navštěvovanou	navštěvovaný	k2eAgFnSc7d1
destinací	destinace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hasičská	hasičský	k2eAgFnSc1d1
zbrojnice	zbrojnice	k1gFnSc1
s	s	k7c7
knihovnou	knihovna	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
projektovaná	projektovaný	k2eAgFnSc1d1
architektem	architekt	k1gMnSc7
Emilem	Emil	k1gMnSc7
Röslerem	Rösler	k1gMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
unikátní	unikátní	k2eAgMnSc1d1
svými	svůj	k3xOyFgInPc7
interiéry	interiér	k1gInPc7
<g/>
,	,	kIx,
hlavně	hlavně	k6eAd1
dveřními	dveřní	k2eAgFnPc7d1
a	a	k8xC
okenními	okenní	k2eAgFnPc7d1
výplněmi	výplň	k1gFnPc7
<g/>
,	,	kIx,
dlažbou	dlažba	k1gFnSc7
a	a	k8xC
kováním	kování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
sídlo	sídlo	k1gNnSc4
národopisné	národopisný	k2eAgFnSc2d1
sbírky	sbírka	k1gFnSc2
ašského	ašský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
byl	být	k5eAaImAgMnS
asi	asi	k9
nejvýznamnější	významný	k2eAgFnSc7d3
památkou	památka	k1gFnSc7
Aše	Aš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
započata	započat	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1622	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
kompletně	kompletně	k6eAd1
přestavěn	přestavět	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
1747	#num#	k4
<g/>
–	–	k?
<g/>
1749	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašský	ašský	k2eAgMnSc1d1
dobrodinec	dobrodinec	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Geipel	Geipel	k1gMnSc1
do	do	k7c2
kostela	kostel	k1gInSc2
zakoupil	zakoupit	k5eAaPmAgInS
největší	veliký	k2eAgInSc1d3
varhanní	varhanní	k2eAgInSc1d1
systém	systém	k1gInSc1
v	v	k7c6
tehdejších	tehdejší	k2eAgFnPc6d1
západních	západní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
kompletně	kompletně	k6eAd1
vyhořel	vyhořet	k5eAaPmAgInS
při	při	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytky	zbytek	k1gInPc1
byly	být	k5eAaImAgFnP
později	pozdě	k6eAd2
strženy	stržen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
místě	místo	k1gNnSc6
kde	kde	k6eAd1
stál	stát	k5eAaImAgInS
založeno	založen	k2eAgNnSc4d1
pietní	pietní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pomníky	pomník	k1gInPc1
a	a	k8xC
památníky	památník	k1gInPc1
</s>
<s>
Restaurovaný	restaurovaný	k2eAgInSc1d1
Lutherův	Lutherův	k2eAgInSc1d1
památník	památník	k1gInSc1
v	v	k7c6
sousedství	sousedství	k1gNnSc6
ašské	ašský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
<g/>
,	,	kIx,
jediný	jediný	k2eAgMnSc1d1
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Památníky	památník	k1gInPc4
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Pomník	pomník	k1gInSc1
J.	J.	kA
W.	W.	kA
Goetha	Goetha	k1gMnSc1
postavený	postavený	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Johannes	Johannes	k1gMnSc1
Watzal	Watzal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
Goethově	Goethův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Pomník	pomník	k1gInSc1
Dr	dr	kA
<g/>
.	.	kIx.
Martina	Martin	k1gMnSc2
Luthera	Luther	k1gMnSc2
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jediný	jediný	k2eAgInSc1d1
Lutherův	Lutherův	k2eAgInSc1d1
památník	památník	k1gInSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
architekt	architekt	k1gMnSc1
J.	J.	kA
Rössner	Rössner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
válek	válka	k1gFnPc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1848	#num#	k4
a	a	k8xC
1869	#num#	k4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
ašském	ašský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
obelisku	obelisk	k1gInSc2
umístěného	umístěný	k2eAgInSc2d1
na	na	k7c6
podstavci	podstavec	k1gInSc6
se	s	k7c7
jmény	jméno	k1gNnPc7
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
většího	veliký	k2eAgInSc2d2
památníku	památník	k1gInSc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
památníkem	památník	k1gInSc7
obětem	oběť	k1gFnPc3
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
na	na	k7c6
ašském	ašský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
a	a	k8xC
také	také	k9
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
obelisku	obelisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
sovětským	sovětský	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
pomáhali	pomáhat	k5eAaImAgMnP
osvobozovat	osvobozovat	k5eAaImF
ašské	ašský	k2eAgNnSc4d1
pohraničí	pohraničí	k1gNnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
ašském	ašský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Benešův	Benešův	k2eAgInSc1d1
památník	památník	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
Okružní	okružní	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
před	před	k7c7
budovou	budova	k1gFnSc7
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
osvobození	osvobození	k1gNnSc2
Aše	Aš	k1gFnSc2
americkou	americký	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
stojí	stát	k5eAaImIp3nS
na	na	k7c4
hranici	hranice	k1gFnSc4
Masarykova	Masarykův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
a	a	k8xC
Sadů	sad	k1gInPc2
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Geipelův	Geipelův	k2eAgInSc1d1
památník	památník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gustav	Gustav	k1gMnSc1
Geipel	Geipel	k1gMnSc1
byl	být	k5eAaImAgMnS
ašský	ašský	k2eAgMnSc1d1
předválečný	předválečný	k2eAgMnSc1d1
měšťan	měšťan	k1gMnSc1
<g/>
,	,	kIx,
průmyslník	průmyslník	k1gMnSc1
a	a	k8xC
dobrodinec	dobrodinec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
daroval	darovat	k5eAaPmAgMnS
městu	město	k1gNnSc3
velkou	velký	k2eAgFnSc4d1
spoustu	spousta	k1gFnSc4
pozemků	pozemek	k1gInPc2
a	a	k8xC
peněz	peníze	k1gInPc2
na	na	k7c4
jejich	jejich	k3xOp3gFnPc4
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památník	památník	k1gInSc1
byl	být	k5eAaImAgInS
odhalen	odhalit	k5eAaPmNgInS
na	na	k7c6
Okružní	okružní	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
Theodora	Theodor	k1gMnSc2
Körnera	Körner	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1913	#num#	k4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
lesoparku	lesopark	k1gInSc6
pod	pod	k7c7
rozhlednou	rozhledna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Památník	památník	k1gInSc1
Friedricha	Friedrich	k1gMnSc2
Ludwiga	Ludwig	k1gMnSc2
Jahna	Jahn	k1gMnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
lesoparku	lesopark	k1gInSc6
pod	pod	k7c7
rozhlednou	rozhledna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
Robertu	Robert	k1gMnSc3
Schumannovi	Schumann	k1gMnSc3
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c6
ašské	ašský	k2eAgFnSc6d1
hudební	hudební	k2eAgFnSc6d1
škole	škola	k1gFnSc6
</s>
<s>
Salva	salva	k1gFnSc1
Guardia	Guardia	k?
je	být	k5eAaImIp3nS
kamenný	kamenný	k2eAgInSc4d1
reliéf	reliéf	k1gInSc4
s	s	k7c7
císařskými	císařský	k2eAgInPc7d1
symboly	symbol	k1gInPc7
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
císařství	císařství	k1gNnSc1
osvobodilo	osvobodit	k5eAaPmAgNnS
Aš	Aš	k1gInSc4
od	od	k7c2
nutného	nutný	k2eAgNnSc2d1
ubytování	ubytování	k1gNnSc2
vojáků	voják	k1gMnPc2
a	a	k8xC
ničení	ničení	k1gNnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
ašském	ašský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
zaniklé	zaniklý	k2eAgInPc1d1
památníky	památník	k1gInPc1
</s>
<s>
památník	památník	k1gInSc1
se	s	k7c7
sochou	socha	k1gFnSc7
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
stál	stát	k5eAaImAgInS
před	před	k7c7
školou	škola	k1gFnSc7
zbouranou	zbouraný	k2eAgFnSc7d1
v	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
současná	současný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
deska	deska	k1gFnSc1
s	s	k7c7
reliéfem	reliéf	k1gInSc7
Friedricha	Friedrich	k1gMnSc2
Schillera	Schiller	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c6
skále	skála	k1gFnSc6
pod	pod	k7c7
ašskou	ašský	k2eAgFnSc7d1
rozhlednou	rozhledna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
bez	bez	k7c2
reliéfu	reliéf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
současná	současný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
není	být	k5eNaImIp3nS
známa	znám	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
stál	stát	k5eAaImAgInS
v	v	k7c6
parku	park	k1gInSc6
mezi	mezi	k7c7
Chebskou	chebský	k2eAgFnSc7d1
a	a	k8xC
Nádražní	nádražní	k2eAgFnSc7d1
ulicí	ulice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
většího	veliký	k2eAgInSc2d2
památníku	památník	k1gInSc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
památníkem	památník	k1gInSc7
obětem	oběť	k1gFnPc3
předchozích	předchozí	k2eAgFnPc2d1
válek	válka	k1gFnPc2
(	(	kIx(
<g/>
ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgMnS
na	na	k7c6
ašském	ašský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
byl	být	k5eAaImAgInS
památník	památník	k1gInSc1
rozebrán	rozebrán	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
byla	být	k5eAaImAgFnS
vztyčena	vztyčen	k2eAgFnSc1d1
socha	socha	k1gFnSc1
rudoarmějce	rudoarmějec	k1gMnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
nebyl	být	k5eNaImAgInS
nikdy	nikdy	k6eAd1
nikde	nikde	k6eAd1
znovu	znovu	k6eAd1
instalován	instalovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s>
dva	dva	k4xCgInPc4
památníky	památník	k1gInPc4
se	s	k7c7
sochami	socha	k1gFnPc7
rudoarmějců	rudoarmějec	k1gMnPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
postupně	postupně	k6eAd1
umístěny	umístit	k5eAaPmNgFnP
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
parku	park	k1gInSc6
mezi	mezi	k7c7
Chebskou	chebský	k2eAgFnSc7d1
a	a	k8xC
Nádražní	nádražní	k2eAgFnSc7d1
ulicí	ulice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
byl	být	k5eAaImAgInS
zničen	zničit	k5eAaPmNgInS
neznámým	známý	k2eNgMnSc7d1
pachatelem	pachatel	k1gMnSc7
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
stržen	strhnout	k5eAaPmNgInS
po	po	k7c6
pádu	pád	k1gInSc6
komunismu	komunismus	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
depozitáři	depozitář	k1gInSc6
ašského	ašský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Regionální	regionální	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
</s>
<s>
Gustav	Gustav	k1gMnSc1
Geipel	Geipel	k1gMnSc1
(	(	kIx(
<g/>
1853	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ašský	ašský	k2eAgMnSc1d1
občan	občan	k1gMnSc1
pocházející	pocházející	k2eAgMnSc1d1
z	z	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
průmyslových	průmyslový	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
na	na	k7c4
Ašsku	Ašska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
dobrosrdečný	dobrosrdečný	k2eAgMnSc1d1
mecenáš	mecenáš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
své	svůj	k3xOyFgInPc4
peníze	peníz	k1gInPc4
daroval	darovat	k5eAaPmAgMnS
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
domů	dům	k1gInPc2
pro	pro	k7c4
chudé	chudý	k2eAgMnPc4d1
dělníky	dělník	k1gMnPc4
a	a	k8xC
staré	starý	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
dotoval	dotovat	k5eAaBmAgInS
sportoviště	sportoviště	k1gNnSc4
<g/>
,	,	kIx,
opravy	oprava	k1gFnPc4
chodníků	chodník	k1gInPc2
a	a	k8xC
silnic	silnice	k1gFnPc2
<g/>
,	,	kIx,
zakoupil	zakoupit	k5eAaPmAgMnS
pro	pro	k7c4
město	město	k1gNnSc4
pozemky	pozemka	k1gFnSc2
pro	pro	k7c4
stavbu	stavba	k1gFnSc4
i	i	k8xC
zalesnění	zalesnění	k1gNnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
evangelický	evangelický	k2eAgInSc4d1
kostel	kostel	k1gInSc4
pořídil	pořídit	k5eAaPmAgInS
rozsáhlý	rozsáhlý	k2eAgInSc1d1
varhanní	varhanní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
své	svůj	k3xOyFgFnSc6
smrti	smrt	k1gFnSc6
odkázal	odkázat	k5eAaPmAgInS
městu	město	k1gNnSc3
velké	velká	k1gFnSc2
množství	množství	k1gNnSc2
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
mu	on	k3xPp3gNnSc3
byl	být	k5eAaImAgInS
odhalen	odhalit	k5eAaPmNgInS
na	na	k7c6
dnešní	dnešní	k2eAgFnSc6d1
Okružní	okružní	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
památník	památník	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
jakýmsi	jakýsi	k3yIgInSc7
symbolickým	symbolický	k2eAgInSc7d1
vstupem	vstup	k1gInSc7
do	do	k7c2
Geipelova	Geipelův	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
on	on	k3xPp3gMnSc1
nechal	nechat	k5eAaPmAgMnS
založit	založit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
komunikací	komunikace	k1gFnPc2
v	v	k7c6
Aši	Aš	k1gInSc6
nese	nést	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Traugott	Traugott	k2eAgMnSc1d1
Alberti	Albert	k1gMnPc1
byl	být	k5eAaImAgMnS
ašský	ašský	k2eAgMnSc1d1
evangelický	evangelický	k2eAgMnSc1d1
farář	farář	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Carla	Carl	k1gMnSc2
Alberiho	Alberi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namaloval	namalovat	k5eAaPmAgMnS
spoustu	spousta	k1gFnSc4
dnes	dnes	k6eAd1
významných	významný	k2eAgFnPc2d1
kreseb	kresba	k1gFnPc2
<g/>
,	,	kIx,
znázorňujících	znázorňující	k2eAgFnPc6d1
Aš	Aš	k1gFnSc4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
sepsal	sepsat	k5eAaPmAgMnS
její	její	k3xOp3gFnPc4
dějiny	dějiny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1870	#num#	k4
byl	být	k5eAaImAgMnS
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
dvoru	dvůr	k1gInSc3
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
ho	on	k3xPp3gMnSc4
vyznamenal	vyznamenat	k5eAaPmAgInS
Rytířským	rytířský	k2eAgInSc7d1
křížem	kříž	k1gInSc7
Železné	železný	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
a	a	k8xC
Komturním	komturní	k2eAgInSc7d1
křížem	kříž	k1gInSc7
Řádu	řád	k1gInSc2
Františka-Josefa	Františka-Josef	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Vídeňské	vídeňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
poté	poté	k6eAd1
Alberti	Albert	k1gMnPc1
zastával	zastávat	k5eAaImAgMnS
významný	významný	k2eAgInSc4d1
post	post	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Carl	Carl	k1gMnSc1
Alberti	Albert	k1gMnPc1
byl	být	k5eAaImAgInS
synem	syn	k1gMnSc7
faráře	farář	k1gMnSc2
Trugotta	Trugott	k1gMnSc2
Albertiho	Alberti	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svém	svůj	k3xOyFgMnSc6
otci	otec	k1gMnSc6
zdědil	zdědit	k5eAaPmAgInS
vztah	vztah	k1gInSc1
k	k	k7c3
Aši	Aš	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
sepsal	sepsat	k5eAaPmAgMnS
podrobné	podrobný	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
jako	jako	k8xS,k8xC
čtyřsvazková	čtyřsvazkový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Beiträge	Beiträge	k1gFnSc1
zur	zur	k?
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Stadt	Stadt	k1gMnSc1
Asch	Asch	k1gMnSc1
und	und	k?
der	drát	k5eAaImRp2nS
Ascher	Aschra	k1gFnPc2
Bezirkes	Bezirkesa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Knüpfer	Knüpfer	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Aši	Aš	k1gInSc6
</s>
<s>
Horst	Horst	k1gMnSc1
Tomayer	Tomayer	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Aši	Aš	k1gInSc6
</s>
<s>
Oskar	Oskar	k1gMnSc1
Fischer	Fischer	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Aši	Aš	k1gInSc6
</s>
<s>
Andreas	Andreas	k1gMnSc1
Leonhardt	Leonhardt	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
rakouských	rakouský	k2eAgMnPc2d1
vojenských	vojenský	k2eAgMnPc2d1
pochodů	pochod	k1gInPc2
<g/>
,	,	kIx,
narozen	narozen	k2eAgInSc1d1
v	v	k7c6
Aši	Aš	k1gInSc6
</s>
<s>
Petr	Petr	k1gMnSc1
Sepeši	Sepech	k1gMnPc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
Lenka	Lenka	k1gFnSc1
Marušková	Marušková	k1gFnSc1
(	(	kIx(
<g/>
za	za	k7c2
svobodna	svoboden	k2eAgFnSc1d1
Hyková	Hyková	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stříbrná	stříbrný	k2eAgFnSc1d1
medailistka	medailistka	k1gFnSc1
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
na	na	k7c6
Olympiádě	olympiáda	k1gFnSc6
2004	#num#	k4
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
žila	žít	k5eAaImAgFnS
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
Anna	Anna	k1gFnSc1
Šochová	Šochová	k1gFnSc1
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Procházka	Procházka	k1gMnSc1
<g/>
,	,	kIx,
stylista	stylista	k1gMnSc1
a	a	k8xC
vizážista	vizážista	k1gMnSc1
českých	český	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
</s>
<s>
Markéta	Markéta	k1gFnSc1
Zinnerová	Zinnerová	k1gFnSc1
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
knih	kniha	k1gFnPc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
</s>
<s>
Petr	Petr	k1gMnSc1
Zahrádka	Zahrádka	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
narozen	narozen	k2eAgMnSc1d1
v	v	k7c6
Aši	Aš	k1gInSc6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Sekáč	sekáč	k1gMnSc1
<g/>
,	,	kIx,
profesionální	profesionální	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
hrající	hrající	k2eAgMnSc1d1
v	v	k7c6
NHL	NHL	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
žil	žít	k5eAaImAgInS
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Konrad	Konrad	k1gInSc1
Henlein	Henlein	k1gInSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
u	u	k7c2
Deutscher	Deutschra	k1gFnPc2
Turnverband	Turnverbando	k1gNnPc2
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
založil	založit	k5eAaPmAgMnS
„	„	k?
<g/>
Ascher	Aschra	k1gFnPc2
Turnschule	Turnschule	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Chrástka	Chrástka	k1gFnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
skupiny	skupina	k1gFnSc2
Turbo	turba	k1gFnSc5
</s>
<s>
Čestmír	Čestmír	k1gMnSc1
Šíma	Šíma	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
strongman	strongman	k1gMnSc1
a	a	k8xC
nejsilnější	silný	k2eAgMnSc1d3
Čech	Čech	k1gMnSc1
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Bundil	Bundil	k1gMnSc1
<g/>
,	,	kIx,
kytarista	kytarista	k1gMnSc1
<g/>
,	,	kIx,
muzikant	muzikant	k1gMnSc1
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1
města	město	k1gNnSc2
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
římskoněmecký	římskoněmecký	k2eAgMnSc1d1
císař	císař	k1gMnSc1
a	a	k8xC
král	král	k1gMnSc1
český	český	k2eAgMnSc1d1
a	a	k8xC
uherský	uherský	k2eAgMnSc1d1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1779	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Johann	Johann	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
von	von	k1gInSc4
Goethe	Goeth	k1gFnSc2
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
básník	básník	k1gMnSc1
(	(	kIx(
<g/>
1806	#num#	k4
<g/>
,	,	kIx,
1819	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
Schumann	Schumann	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
nacistický	nacistický	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Edvard	Edvard	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
československý	československý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klement	Klement	k1gMnSc1
Gottwald	Gottwald	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
československý	československý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
československý	československý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
</s>
<s>
Gustáv	Gustáva	k1gFnPc2
Husák	Husák	k1gMnSc1
<g/>
,	,	kIx,
československý	československý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
2015	#num#	k4
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Aš	Aš	k1gFnSc1
má	mít	k5eAaImIp3nS
následující	následující	k2eAgNnPc4d1
partnerská	partnerský	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
Fiumefreddo	Fiumefreddo	k1gNnSc1
di	di	k?
Sicilia	Sicilia	k1gFnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Marktbreit	Marktbreit	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olešnice	Olešnice	k1gFnSc1
nad	nad	k7c7
Halštrovem	Halštrov	k1gInSc7
(	(	kIx(
<g/>
Oelsnitz	Oelsnitz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Plavno	Plavno	k1gNnSc1
(	(	kIx(
<g/>
Plauen	Plauen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rehau	Rehau	k6eAd1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
přeshraniční	přeshraniční	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
město	město	k1gNnSc1
Aš	Aš	k1gInSc1
spolupracuje	spolupracovat	k5eAaImIp3nS
a	a	k8xC
žádá	žádat	k5eAaImIp3nS
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
z	z	k7c2
evropských	evropský	k2eAgInPc2d1
fondů	fond	k1gInPc2
také	také	k9
společně	společně	k6eAd1
s	s	k7c7
obcemi	obec	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
Bad	Bad	k?
Elster	Elster	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Fichtelberg	Fichtelberg	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://hamelika.webz.cz/h97-05+06.htm1	http://hamelika.webz.cz/h97-05+06.htm1	k4
2	#num#	k4
ALBERTOVÁ	Albertová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dějin	dějiny	k1gFnPc2
Ašského	ašský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
19591	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
VÍT	Víta	k1gFnPc2
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašsko	Ašsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domažlice	Domažlice	k1gFnPc1
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
20001	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gFnSc1
v	v	k7c6
zrcadle	zrcadlo	k1gNnSc6
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
:	:	kIx,
MěÚ	MěÚ	k1gFnSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
20051	#num#	k4
2	#num#	k4
3	#num#	k4
PEKAŘ	Pekař	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheb	Cheb	k1gInSc4
a	a	k8xC
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
↑	↑	k?
TOUFAR	Toufar	k1gMnSc1
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
<g/>
:	:	kIx,
Hranice	hranice	k1gFnSc1
u	u	k7c2
Aše	Aš	k1gFnSc2
<g/>
,	,	kIx,
historie	historie	k1gFnSc2
obce	obec	k1gFnSc2
<g/>
↑	↑	k?
BOHÁČ	Boháč	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
a	a	k8xC
KUBŮ	Kubů	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
:	:	kIx,
Aš	Aš	k1gInSc1
<g/>
,	,	kIx,
ČTK-Pressfoto	ČTK-Pressfota	k1gFnSc5
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Slavnostní	slavnostní	k2eAgNnSc1d1
znovuodhalení	znovuodhalení	k1gNnSc4
památníků	památník	k1gInPc2
T.	T.	kA
Körnera	Körner	k1gMnSc2
a	a	k8xC
F.	F.	kA
L.	L.	kA
Jahna	Jahn	k1gMnSc2
<g/>
.	.	kIx.
www.asskyweb.cz	www.asskyweb.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Slavnostní	slavnostní	k2eAgNnSc4d1
znovuodhalení	znovuodhalení	k1gNnSc4
památníku	památník	k1gInSc2
Gustava	Gustav	k1gMnSc2
Geipela	Geipel	k1gMnSc2
<g/>
.	.	kIx.
www.asskyweb.cz	www.asskyweb.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Náměstí	náměstí	k1gNnSc1
Goetha	Goetha	k1gMnSc1
je	být	k5eAaImIp3nS
po	po	k7c6
šestiletém	šestiletý	k2eAgNnSc6d1
úsilí	úsilí	k1gNnSc6
hotové	hotová	k1gFnSc2
<g/>
↑	↑	k?
Audioknihu	Audioknih	k1gInSc2
pověstí	pověst	k1gFnPc2
již	již	k6eAd1
osobnosti	osobnost	k1gFnPc1
natočily	natočit	k5eAaBmAgFnP
<g/>
↑	↑	k?
Zelený	zelený	k2eAgMnSc1d1
dědek	dědek	k1gMnSc1
o.s.	o.s.	k?
<g/>
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
Republice	republika	k1gFnSc6
československé	československý	k2eAgFnSc6d1
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
I.	I.	kA
Země	země	k1gFnSc1
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
613	#num#	k4
s.	s.	k?
S.	S.	kA
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1850	#num#	k4
a	a	k8xC
2001	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
publikace	publikace	k1gFnSc2
"	"	kIx"
<g/>
Obce	obec	k1gFnPc1
Ašska	Ašsko	k1gNnSc2
v	v	k7c6
proměnách	proměna	k1gFnPc6
času	čas	k1gInSc2
<g/>
,	,	kIx,
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
"	"	kIx"
<g/>
1	#num#	k4
2	#num#	k4
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgNnPc2d1
sčítání	sčítání	k1gNnPc2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Podle	podle	k7c2
www.asch-boehmen.de	www.asch-boehmen.de	k6eAd1
<g/>
↑	↑	k?
GROSS	Gross	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
1954	#num#	k4
<g/>
↑	↑	k?
ALBERTI	ALBERTI	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
:	:	kIx,
Beiträge	Beiträge	k1gFnSc1
zur	zur	k?
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Stadt	Stadt	k1gMnSc1
Asch	Asch	k1gMnSc1
und	und	k?
der	drát	k5eAaImRp2nS
Ascher	Aschra	k1gFnPc2
Bezirkes	Bezirkes	k1gInSc1
<g/>
,	,	kIx,
Aš	Aš	k1gFnSc1
19341	#num#	k4
2	#num#	k4
http://rekos.psp.cz/detail-symbolu/id/3e440fef-a260-4ebf-b08f-2349b8cc9cf1	http://rekos.psp.cz/detail-symbolu/id/3e440fef-a260-4ebf-b08f-2349b8cc9cf1	k4
<g/>
↑	↑	k?
ObecKrásná	ObecKrásný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Zakládací	zakládací	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
sdružení	sdružení	k1gNnSc2
<g/>
↑	↑	k?
Muas	Muasa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.muas.cz	www.muas.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Čísla	číslo	k1gNnPc1
podle	podle	k7c2
ročenky	ročenka	k1gFnSc2
města	město	k1gNnSc2
Aše	Aš	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
<g/>
↑	↑	k?
Čísla	číslo	k1gNnSc2
podle	podle	k7c2
ročenky	ročenka	k1gFnSc2
města	město	k1gNnSc2
Aše	Aš	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2008	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.asch-boehmen.de	www.asch-boehmen.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
http://muscle-fitness.cz/index.php?option=com_content&	http://muscle-fitness.cz/index.php?option=com_content&	k1gMnSc1
Čestmír	Čestmír	k1gMnSc1
Šíma	Šíma	k1gMnSc1
mistrem	mistr	k1gMnSc7
ČR	ČR	kA
strongmanů	strongman	k1gMnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PELANT	PELANT	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Města	město	k1gNnSc2
a	a	k8xC
městečka	městečko	k1gNnSc2
západočeského	západočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Západočeské	západočeský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOLEKTIV	kolektiv	k1gInSc1
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gFnSc1
v	v	k7c6
zrcadle	zrcadlo	k1gNnSc6
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
<g/>
:	:	kIx,
MěÚ	MěÚ	k1gFnSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOLEKTIV	kolektiv	k1gInSc1
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obce	obec	k1gFnPc4
Ašska	Ašsek	k1gInSc2
v	v	k7c6
proměnách	proměna	k1gFnPc6
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
<g/>
:	:	kIx,
Domovský	domovský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
okresu	okres	k1gInSc2
Aš	Aš	k1gFnSc4
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
Aš	Aš	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PEKAŘ	Pekař	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheb	Cheb	k1gInSc4
a	a	k8xC
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VÍT	Vít	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašsko	Ašsko	k1gNnSc1
a	a	k8xC
Chebsko	Chebsko	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VÍT	Vít	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašsko	Ašsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domažlice	Domažlice	k1gFnPc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Českého	český	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86125	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
FISCHER	Fischer	k1gMnSc1
<g/>
,	,	kIx,
Wilhelm	Wilhelm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ašské	ašský	k2eAgFnPc4d1
pověsti	pověst	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
<g/>
:	:	kIx,
Muzeum	muzeum	k1gNnSc1
Aš	Aš	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BOHÁČ	Boháč	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ČTK-Pressfoto	ČTK-Pressfota	k1gFnSc5
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ALBERTI	ALBERTI	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beiträge	Beiträge	k1gFnSc1
zur	zur	k?
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Stadt	Stadt	k1gMnSc1
Asch	Asch	k1gMnSc1
und	und	k?
der	drát	k5eAaImRp2nS
Ascher	Aschra	k1gFnPc2
Bezirkes	Bezirkesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aš	Aš	k1gInSc1
<g/>
:	:	kIx,
Stadt	Stadt	k2eAgInSc1d1
Asch	Asch	k1gInSc1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TOUFAR	Toufar	k1gMnSc1
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
u	u	k7c2
Aše	Aš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
<g/>
:	:	kIx,
MěÚ	MěÚ	k1gFnSc1
Hranice	hranice	k1gFnSc1
</s>
<s>
ALBERTOVÁ	Albertová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dějin	dějiny	k1gFnPc2
Ašského	ašský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Aši	Aš	k1gInSc6
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Aš	Aš	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Aš	Aš	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnSc1
Aš	Aš	k1gFnSc1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Aš	Aš	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Web	web	k1gInSc1
infocentra	infocentrum	k1gNnSc2
Aš	Aš	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Rozsáhlé	rozsáhlý	k2eAgInPc1d1
stánky	stánek	k1gInPc1
s	s	k7c7
historickými	historický	k2eAgFnPc7d1
fotografiemi	fotografia	k1gFnPc7
Aše	Aš	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
www.asch-boehmen.de/	www.asch-boehmen.de/	k?
Německé	německý	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
věnované	věnovaný	k2eAgFnSc3d1
historii	historie	k1gFnSc3
Aše	Aš	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
města	město	k1gNnSc2
Aš	Aš	k1gFnSc4
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
AšDolní	AšDolní	k2eAgInSc1d1
PasekyDoubravaHorní	PasekyDoubravaHorní	k2eAgInSc1d1
PasekyKopaninyMokřinyNebesaNový	PasekyKopaninyMokřinyNebesaNový	k2eAgInSc1d1
ŽďárVernéřov	ŽďárVernéřov	k1gInSc1
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Cheb	Cheb	k1gInSc1
</s>
<s>
Aš	Aš	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgInSc1d1
Žandov	Žandov	k1gInSc1
•	•	k?
Drmoul	Drmoul	k1gInSc1
•	•	k?
Františkovy	Františkův	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
•	•	k?
Hazlov	Hazlov	k1gInSc1
•	•	k?
Hranice	hranice	k1gFnSc2
•	•	k?
Cheb	Cheb	k1gInSc1
•	•	k?
Krásná	krásný	k2eAgFnSc1d1
•	•	k?
Křižovatka	křižovatka	k1gFnSc1
•	•	k?
Lázně	lázeň	k1gFnSc2
Kynžvart	Kynžvart	k1gInSc1
•	•	k?
Libá	libý	k2eAgFnSc1d1
•	•	k?
Lipová	lipový	k2eAgFnSc1d1
•	•	k?
Luby	lub	k1gInPc4
•	•	k?
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
•	•	k?
Milhostov	Milhostov	k1gInSc1
•	•	k?
Milíkov	Milíkov	k1gInSc1
•	•	k?
Mnichov	Mnichov	k1gInSc1
•	•	k?
Nebanice	Nebanice	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Kostel	kostel	k1gInSc1
•	•	k?
Odrava	Odrava	k1gFnSc1
•	•	k?
Okrouhlá	okrouhlý	k2eAgFnSc1d1
•	•	k?
Ovesné	ovesný	k2eAgInPc4d1
Kladruby	Kladruby	k1gInPc4
•	•	k?
Plesná	plesný	k2eAgFnSc1d1
•	•	k?
Podhradí	Podhradí	k1gNnSc4
•	•	k?
Pomezí	pomezí	k1gNnSc2
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
•	•	k?
Poustka	poustka	k1gFnSc1
•	•	k?
Prameny	pramen	k1gInPc1
•	•	k?
Skalná	skalný	k2eAgFnSc1d1
•	•	k?
Stará	starý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
•	•	k?
Teplá	teplý	k2eAgFnSc1d1
•	•	k?
Trstěnice	trstěnice	k1gFnSc1
•	•	k?
Třebeň	Třebeň	k1gFnSc1
•	•	k?
Tři	tři	k4xCgFnPc1
Sekery	sekera	k1gFnPc1
•	•	k?
Tuřany	Tuřana	k1gFnSc2
•	•	k?
Valy	Vala	k1gMnSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
Hleďsebe	Hleďseb	k1gInSc5
•	•	k?
Velký	velký	k2eAgInSc1d1
Luh	luh	k1gInSc1
•	•	k?
Vlkovice	Vlkovice	k1gFnSc2
•	•	k?
Vojtanov	Vojtanov	k1gInSc1
•	•	k?
Zádub-Závišín	Zádub-Závišín	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128699	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4003190-1	4003190-1	k4
</s>
