<s>
Odehrálo	odehrát	k5eAaPmAgNnS	odehrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Bostonské	bostonský	k2eAgNnSc4d1	Bostonské
pití	pití	k1gNnSc4	pití
čaje	čaj	k1gInSc2	čaj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
a	a	k8xC	a
první	první	k4xOgFnPc4	první
bitvy	bitva	k1gFnPc4	bitva
americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
