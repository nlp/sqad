<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
a	a	k8xC	a
metropolitní	metropolitní	k2eAgInSc1d1	metropolitní
distrikt	distrikt	k1gInSc1	distrikt
v	v	k7c6	v
regionu	region	k1gInSc6	region
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Anglie	Anglie	k1gFnSc2	Anglie
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Merseyské	Merseyský	k2eAgFnSc2d1	Merseyský
zátoky	zátoka	k1gFnSc2	zátoka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristika	k1gFnSc1	charakteristika
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
pásmu	pásmo	k1gNnSc6	pásmo
pahorků	pahorek	k1gInPc2	pahorek
vysokých	vysoký	k2eAgInPc2d1	vysoký
maximálně	maximálně	k6eAd1	maximálně
70	[number]	k4	70
m.	m.	k?	m.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
od	od	k7c2	od
Liverpoolského	liverpoolský	k2eAgInSc2d1	liverpoolský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Irského	irský	k2eAgNnSc2d1	irské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Liverpoolu	Liverpool	k1gInSc2	Liverpool
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
441	[number]	k4	441
477	[number]	k4	477
a	a	k8xC	a
Merseyská	Merseyský	k2eAgFnSc1d1	Merseyský
konurbace	konurbace	k1gFnSc1	konurbace
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
1	[number]	k4	1
362	[number]	k4	362
026	[number]	k4	026
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
je	být	k5eAaImIp3nS	být
známým	známý	k2eAgNnSc7d1	známé
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
především	především	k9	především
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
populární	populární	k2eAgFnSc7d1	populární
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
skupina	skupina	k1gFnSc1	skupina
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
přístav	přístav	k1gInSc4	přístav
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInSc4	jeho
přístav	přístav	k1gInSc4	přístav
procházelo	procházet	k5eAaImAgNnS	procházet
nejvíce	nejvíce	k6eAd1	nejvíce
zboží	zboží	k1gNnSc1	zboží
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
britskými	britský	k2eAgMnPc7d1	britský
městy	město	k1gNnPc7	město
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
významným	významný	k2eAgNnSc7d1	významné
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
ale	ale	k9	ale
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
částí	část	k1gFnPc2	část
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
roku	rok	k1gInSc2	rok
1207	[number]	k4	1207
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
Liverpool	Liverpool	k1gInSc1	Liverpool
jako	jako	k8xC	jako
distrikt	distrikt	k1gInSc1	distrikt
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
jen	jen	k9	jen
asi	asi	k9	asi
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
mírně	mírně	k6eAd1	mírně
stoupal	stoupat	k5eAaImAgInS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
místem	místem	k6eAd1	místem
mnoha	mnoho	k4c2	mnoho
bitev	bitva	k1gFnPc2	bitva
včetně	včetně	k7c2	včetně
18	[number]	k4	18
denního	denní	k2eAgNnSc2d1	denní
obléhání	obléhání	k1gNnSc2	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1699	[number]	k4	1699
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
vypravena	vypravit	k5eAaPmNgFnS	vypravit
první	první	k4xOgFnSc1	první
otrokářská	otrokářský	k2eAgFnSc1d1	otrokářská
loď	loď	k1gFnSc1	loď
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
se	se	k3xPyFc4	se
i	i	k9	i
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
mokrý	mokrý	k2eAgInSc1d1	mokrý
dok	dok	k1gInSc1	dok
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
století	století	k1gNnSc2	století
kontroloval	kontrolovat	k5eAaImAgInS	kontrolovat
Liverpool	Liverpool	k1gInSc1	Liverpool
40	[number]	k4	40
<g/>
%	%	kIx~	%
evropského	evropský	k2eAgInSc2d1	evropský
a	a	k8xC	a
80	[number]	k4	80
<g/>
%	%	kIx~	%
britského	britský	k2eAgInSc2d1	britský
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
probíhalo	probíhat	k5eAaImAgNnS	probíhat
40	[number]	k4	40
<g/>
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
obchodního	obchodní	k2eAgInSc2d1	obchodní
obratu	obrat	k1gInSc2	obrat
Liverpoolem	Liverpool	k1gInSc7	Liverpool
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
mnoha	mnoho	k4c2	mnoho
honosných	honosný	k2eAgFnPc2d1	honosná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Liverpool	Liverpool	k1gInSc1	Liverpool
and	and	k?	and
Manchester	Manchester	k1gInSc1	Manchester
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
prudce	prudko	k6eAd1	prudko
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
přílivem	příliv	k1gInSc7	příliv
imigrantů	imigrant	k1gMnPc2	imigrant
především	především	k9	především
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
tvořili	tvořit	k5eAaImAgMnP	tvořit
Irové	Ir	k1gMnPc1	Ir
asi	asi	k9	asi
25	[number]	k4	25
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
další	další	k2eAgFnSc1d1	další
přistěhovalecká	přistěhovalecký	k2eAgFnSc1d1	přistěhovalecká
vlna	vlna	k1gFnSc1	vlna
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
terčem	terč	k1gInSc7	terč
osmi	osm	k4xCc2	osm
náletů	nálet	k1gInPc2	nálet
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
usmrceno	usmrtit	k5eAaPmNgNnS	usmrtit
asi	asi	k9	asi
2	[number]	k4	2
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
mnoho	mnoho	k4c1	mnoho
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
dok	dok	k1gInSc1	dok
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
-	-	kIx~	-
Seaforth	Seaforth	k1gMnSc1	Seaforth
Dock	Dock	k1gMnSc1	Dock
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
mladé	mladý	k2eAgFnSc2d1	mladá
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
slavná	slavný	k2eAgFnSc1d1	slavná
skupina	skupina	k1gFnSc1	skupina
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
popové	popový	k2eAgFnPc1d1	popová
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
učinily	učinit	k5eAaPmAgFnP	učinit
město	město	k1gNnSc1	město
centrem	centrum	k1gNnSc7	centrum
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
úroveň	úroveň	k1gFnSc1	úroveň
města	město	k1gNnSc2	město
však	však	k9	však
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
Liverpool	Liverpool	k1gInSc1	Liverpool
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
mnoho	mnoho	k4c4	mnoho
pracovních	pracovní	k2eAgFnPc2d1	pracovní
<g/>
,	,	kIx,	,
upadaly	upadat	k5eAaImAgFnP	upadat
postupně	postupně	k6eAd1	postupně
i	i	k8xC	i
místní	místní	k2eAgInPc1d1	místní
doky	dok	k1gInPc1	dok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
kontejnerové	kontejnerový	k2eAgFnSc2d1	kontejnerová
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vybavení	vybavení	k1gNnSc1	vybavení
přístavu	přístav	k1gInSc2	přístav
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
procento	procento	k1gNnSc1	procento
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
úřady	úřad	k1gInPc1	úřad
města	město	k1gNnSc2	město
koncentrovaly	koncentrovat	k5eAaBmAgInP	koncentrovat
na	na	k7c4	na
postupnou	postupný	k2eAgFnSc4d1	postupná
regeneraci	regenerace	k1gFnSc4	regenerace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
snaží	snažit	k5eAaImIp3nS	snažit
využít	využít	k5eAaPmF	využít
popularity	popularita	k1gFnSc2	popularita
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
přilákat	přilákat	k5eAaPmF	přilákat
tak	tak	k9	tak
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
zotavovat	zotavovat	k5eAaImF	zotavovat
z	z	k7c2	z
poválečného	poválečný	k2eAgInSc2d1	poválečný
poklesu	pokles	k1gInSc2	pokles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
až	až	k8xS	až
2001	[number]	k4	2001
se	se	k3xPyFc4	se
úroveň	úroveň	k1gFnSc1	úroveň
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
o	o	k7c4	o
6,3	[number]	k4	6,3
<g/>
%	%	kIx~	%
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
5,8	[number]	k4	5,8
<g/>
%	%	kIx~	%
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
5,7	[number]	k4	5,7
<g/>
%	%	kIx~	%
v	v	k7c6	v
Bristolu	Bristol	k1gInSc6	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
narůstal	narůstat	k5eAaImAgInS	narůstat
ročně	ročně	k6eAd1	ročně
o	o	k7c4	o
9,2	[number]	k4	9,2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
stále	stále	k6eAd1	stále
chudá	chudý	k2eAgFnSc1d1	chudá
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
deset	deset	k4xCc1	deset
nejchudších	chudý	k2eAgInPc2d3	nejchudší
distriktů	distrikt	k1gInPc2	distrikt
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
se	se	k3xPyFc4	se
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
některé	některý	k3yIgInPc1	některý
vládní	vládní	k2eAgInPc1d1	vládní
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
kanceláře	kancelář	k1gFnPc1	kancelář
National	National	k1gFnSc2	National
Health	Healtha	k1gFnPc2	Healtha
Service	Service	k1gFnSc2	Service
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
agentury	agentura	k1gFnPc1	agentura
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
–	–	k?	–
Criminal	Criminal	k1gFnSc1	Criminal
Records	Recordsa	k1gFnPc2	Recordsa
Bureau	Bureaus	k1gInSc2	Bureaus
a	a	k8xC	a
Identity	identita	k1gFnSc2	identita
and	and	k?	and
Passport	Passport	k1gInSc1	Passport
Service	Service	k1gFnSc1	Service
<g/>
.	.	kIx.	.
</s>
<s>
Investice	investice	k1gFnPc1	investice
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
například	například	k6eAd1	například
do	do	k7c2	do
call	calla	k1gFnPc2	calla
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
aktivitou	aktivita	k1gFnSc7	aktivita
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
je	být	k5eAaImIp3nS	být
vývojové	vývojový	k2eAgNnSc1d1	vývojové
středisko	středisko	k1gNnSc1	středisko
společnosti	společnost	k1gFnSc2	společnost
Sony	Sony	kA	Sony
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
PlayStation	PlayStation	k1gInSc4	PlayStation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turistika	turistika	k1gFnSc1	turistika
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
jeho	jeho	k3xOp3gFnSc2	jeho
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
plánován	plánovat	k5eAaImNgInS	plánovat
rozvoj	rozvoj	k1gInSc1	rozvoj
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
zřizování	zřizování	k1gNnSc2	zřizování
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
města	město	k1gNnSc2	město
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
nejen	nejen	k6eAd1	nejen
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
filmaře	filmař	k1gMnPc4	filmař
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejfilmovanějších	filmovaný	k2eAgNnPc2d3	filmovaný
měst	město	k1gNnPc2	město
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
==	==	k?	==
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
je	být	k5eAaImIp3nS	být
spravován	spravovat	k5eAaImNgInS	spravovat
radou	rada	k1gMnSc7	rada
města	město	k1gNnSc2	město
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
rad	rada	k1gFnPc2	rada
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
metropolitního	metropolitní	k2eAgNnSc2d1	metropolitní
hrabství	hrabství	k1gNnSc2	hrabství
Merseyside	Merseysid	k1gInSc5	Merseysid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
30	[number]	k4	30
volebních	volební	k2eAgInPc2d1	volební
okrsků	okrsek	k1gInPc2	okrsek
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
5	[number]	k4	5
obvodů	obvod	k1gInPc2	obvod
–	–	k?	–
Liverpool	Liverpool	k1gInSc1	Liverpool
Garston	Garston	k1gInSc1	Garston
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
Riverside	Riversid	k1gInSc5	Riversid
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
Walton	Walton	k1gInSc1	Walton
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
Wavertree	Wavertree	k1gInSc1	Wavertree
a	a	k8xC	a
Liverpool	Liverpool	k1gInSc1	Liverpool
West	Westa	k1gFnPc2	Westa
Derby	derby	k1gNnSc2	derby
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
řekou	řeka	k1gFnSc7	řeka
Mersey	Mersea	k1gFnSc2	Mersea
prochází	procházet	k5eAaImIp3nP	procházet
tři	tři	k4xCgInPc1	tři
tunely	tunel	k1gInPc1	tunel
–	–	k?	–
jeden	jeden	k4xCgMnSc1	jeden
železniční	železniční	k2eAgFnPc1d1	železniční
–	–	k?	–
Mersey	Merse	k2eAgFnPc1d1	Merse
Railway	Railwaa	k1gFnPc1	Railwaa
Tunnel	Tunnela	k1gFnPc2	Tunnela
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
silniční	silniční	k2eAgInPc1d1	silniční
–	–	k?	–
Queensway	Queensway	k1gInPc1	Queensway
Tunnel	Tunnlo	k1gNnPc2	Tunnlo
a	a	k8xC	a
Kingsway	Kingswaa	k1gFnSc2	Kingswaa
Tunnel	Tunnela	k1gFnPc2	Tunnela
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
Liverpoolem	Liverpool	k1gInSc7	Liverpool
a	a	k8xC	a
Wirralem	Wirral	k1gInSc7	Wirral
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
i	i	k9	i
Merseyským	Merseyský	k2eAgInSc7d1	Merseyský
trajektem	trajekt	k1gInSc7	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Břehy	břeh	k1gInPc1	břeh
řeky	řeka	k1gFnSc2	řeka
Mersey	Mersea	k1gFnSc2	Mersea
spojuje	spojovat	k5eAaImIp3nS	spojovat
u	u	k7c2	u
Runcornu	Runcorn	k1gInSc2	Runcorn
most	most	k1gInSc4	most
Silver	Silvero	k1gNnPc2	Silvero
Jubilee	Jubile	k1gMnSc2	Jubile
Bridge	Bridg	k1gMnSc2	Bridg
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Runcorn	Runcorn	k1gInSc1	Runcorn
Bridge	Bridg	k1gInSc2	Bridg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
Liverpoolské	liverpoolský	k2eAgNnSc1d1	Liverpoolské
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgNnSc1d1	nacházející
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
Speke	Spek	k1gFnSc2	Spek
<g/>
,	,	kIx,	,
přejmenováno	přejmenován	k2eAgNnSc1d1	přejmenováno
na	na	k7c4	na
Liverpoolské	liverpoolský	k2eAgNnSc4d1	Liverpoolské
letiště	letiště	k1gNnSc4	letiště
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
letiště	letiště	k1gNnSc2	letiště
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skicu	skica	k1gFnSc4	skica
namalovanou	namalovaný	k2eAgFnSc4d1	namalovaná
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
Nad	nad	k7c7	nad
námi	my	k3xPp1nPc7	my
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
nebe	nebe	k1gNnSc4	nebe
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
písně	píseň	k1gFnSc2	píseň
Imagine	Imagin	k1gInSc5	Imagin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
využilo	využít	k5eAaPmAgNnS	využít
služeb	služba	k1gFnPc2	služba
Liverpoolského	liverpoolský	k2eAgInSc2d1	liverpoolský
přístavu	přístav	k1gInSc2	přístav
716	[number]	k4	716
000	[number]	k4	000
pasažérů	pasažér	k1gMnPc2	pasažér
cestujících	cestující	k1gFnPc2	cestující
především	především	k9	především
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příměstské	příměstský	k2eAgNnSc1d1	příměstské
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
Merseyskou	Merseyska	k1gFnSc7	Merseyska
železniční	železniční	k2eAgFnSc3d1	železniční
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
vedou	vést	k5eAaImIp3nP	vést
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
většinou	většinou	k6eAd1	většinou
podzemím	podzemí	k1gNnSc7	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
linek	linka	k1gFnPc2	linka
–	–	k?	–
severní	severní	k2eAgFnSc2d1	severní
linky	linka	k1gFnSc2	linka
vedoucí	vedoucí	k1gFnSc2	vedoucí
do	do	k7c2	do
Southportu	Southport	k1gInSc2	Southport
<g/>
,	,	kIx,	,
Ormskirku	Ormskirek	k1gInSc2	Ormskirek
<g/>
,	,	kIx,	,
Kirkby	Kirkba	k1gFnSc2	Kirkba
a	a	k8xC	a
Hunts	Hunts	k1gInSc4	Hunts
Cross	Crossa	k1gFnPc2	Crossa
a	a	k8xC	a
linky	linka	k1gFnSc2	linka
wirral	wirrat	k5eAaImAgInS	wirrat
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Mersey	Merse	k2eAgFnPc4d1	Merse
Railway	Railwaa	k1gFnPc4	Railwaa
Tunnel	Tunnela	k1gFnPc2	Tunnela
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
New	New	k1gFnSc2	New
Brighton	Brighton	k1gInSc1	Brighton
<g/>
,	,	kIx,	,
West	West	k1gInSc1	West
Kirby	Kirba	k1gFnSc2	Kirba
<g/>
,	,	kIx,	,
Chester	Chestra	k1gFnPc2	Chestra
a	a	k8xC	a
Ellesmere	Ellesmer	k1gInSc5	Ellesmer
Port	port	k1gInSc1	port
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
pro	pro	k7c4	pro
dálkové	dálkový	k2eAgInPc4d1	dálkový
vlakové	vlakový	k2eAgInPc4d1	vlakový
spoje	spoj	k1gInPc4	spoj
je	být	k5eAaImIp3nS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
Lime	Lim	k1gFnSc2	Lim
Street	Streeta	k1gFnPc2	Streeta
station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
<g/>
,	,	kIx,	,
Birminghamem	Birmingham	k1gInSc7	Birmingham
<g/>
,	,	kIx,	,
Manchesterem	Manchester	k1gInSc7	Manchester
<g/>
,	,	kIx,	,
Prestonem	Preston	k1gInSc7	Preston
<g/>
,	,	kIx,	,
Leedsem	Leeds	k1gInSc7	Leeds
<g/>
,	,	kIx,	,
Scarborough	Scarborough	k1gMnSc1	Scarborough
<g/>
,	,	kIx,	,
Sheffieldem	Sheffield	k1gInSc7	Sheffield
<g/>
,	,	kIx,	,
Nottinghamem	Nottingham	k1gInSc7	Nottingham
a	a	k8xC	a
Norwichem	Norwich	k1gInSc7	Norwich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznáván	uznáván	k2eAgInSc4d1	uznáván
jako	jako	k8xC	jako
významné	významný	k2eAgNnSc4d1	významné
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
historií	historie	k1gFnSc7	historie
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
známé	známý	k2eAgFnSc2d1	známá
i	i	k8xC	i
svými	svůj	k3xOyFgMnPc7	svůj
básníky	básník	k1gMnPc7	básník
například	například	k6eAd1	například
Rogerem	Roger	k1gInSc7	Roger
McGoughem	McGough	k1gInSc7	McGough
a	a	k8xC	a
Adrianem	Adrian	k1gMnSc7	Adrian
Henri	Henr	k1gFnSc2	Henr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgNnSc7d1	významné
hudebním	hudební	k2eAgNnSc7d1	hudební
tělesem	těleso	k1gNnSc7	těleso
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Královský	královský	k2eAgInSc1d1	královský
liverpoolský	liverpoolský	k2eAgInSc1d1	liverpoolský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
koncerty	koncert	k1gInPc4	koncert
ve	v	k7c6	v
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
Hall	Hallum	k1gNnPc2	Hallum
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
historie	historie	k1gFnSc1	historie
divadelního	divadelní	k2eAgNnSc2d1	divadelní
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
divadlech	divadlo	k1gNnPc6	divadlo
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
-	-	kIx~	-
Empire	empir	k1gInSc5	empir
<g/>
,	,	kIx,	,
Everyman	Everyman	k1gMnSc1	Everyman
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
Playhouse	Playhouse	k1gFnSc2	Playhouse
<g/>
,	,	kIx,	,
Neptune	Neptun	k1gMnSc5	Neptun
<g/>
,	,	kIx,	,
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
Royal	Royal	k1gInSc4	Royal
Court	Courta	k1gFnPc2	Courta
a	a	k8xC	a
Unity	unita	k1gMnPc4	unita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
několik	několik	k4yIc1	několik
významných	významný	k2eAgFnPc2d1	významná
galerií	galerie	k1gFnPc2	galerie
–	–	k?	–
Walker	Walker	k1gMnSc1	Walker
Art	Art	k1gMnSc2	Art
Gallery	Galler	k1gInPc4	Galler
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
obrazy	obraz	k1gInPc4	obraz
prerafaelistů	prerafaelista	k1gMnPc2	prerafaelista
<g/>
,	,	kIx,	,
Sudley	Sudlea	k1gFnSc2	Sudlea
House	house	k1gNnSc1	house
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírky	sbírka	k1gFnPc4	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Tate	Tat	k1gMnSc2	Tat
Liverpool	Liverpool	k1gInSc1	Liverpool
se	s	k7c7	s
sbírkami	sbírka	k1gFnPc7	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
z	z	k7c2	z
kolekce	kolekce	k1gFnSc2	kolekce
Tate	Tat	k1gFnSc2	Tat
Gallery	Galler	k1gInPc1	Galler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
nevysílá	vysílat	k5eNaImIp3nS	vysílat
žádná	žádný	k3yNgFnSc1	žádný
významná	významný	k2eAgFnSc1d1	významná
lokální	lokální	k2eAgFnSc1d1	lokální
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
vysíláním	vysílání	k1gNnSc7	vysílání
místních	místní	k2eAgFnPc2d1	místní
televizí	televize	k1gFnPc2	televize
sídlících	sídlící	k2eAgFnPc2d1	sídlící
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
například	například	k6eAd1	například
ITV	ITV	kA	ITV
Granada	Granada	k1gFnSc1	Granada
a	a	k8xC	a
regionální	regionální	k2eAgNnSc1d1	regionální
studio	studio	k1gNnSc1	studio
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgNnSc1	některý
vysílací	vysílací	k2eAgNnSc1d1	vysílací
studia	studio	k1gNnPc1	studio
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nP	sídlet
některé	některý	k3yIgFnPc1	některý
televizní	televizní	k2eAgFnPc1d1	televizní
produkční	produkční	k2eAgFnPc1d1	produkční
společnosti	společnost	k1gFnPc1	společnost
například	například	k6eAd1	například
Mersey	Merse	k2eAgInPc4d1	Merse
Television	Television	k1gInSc4	Television
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
jsou	být	k5eAaImIp3nP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
některé	některý	k3yIgFnPc1	některý
noviny	novina	k1gFnPc1	novina
s	s	k7c7	s
lokálním	lokální	k2eAgNnSc7d1	lokální
zaměřením	zaměření	k1gNnSc7	zaměření
–	–	k?	–
deník	deník	k1gInSc1	deník
Daily	Daila	k1gFnSc2	Daila
Post	posta	k1gFnPc2	posta
a	a	k8xC	a
večerník	večerník	k1gInSc1	večerník
Echo	echo	k1gNnSc1	echo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
místní	místní	k2eAgFnPc4d1	místní
rádiové	rádiový	k2eAgFnPc4d1	rádiová
stanice	stanice	k1gFnPc4	stanice
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
Merseyside	Merseysid	k1gInSc5	Merseysid
<g/>
,	,	kIx,	,
Juice	Juika	k1gFnSc3	Juika
FM	FM	kA	FM
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
City	city	k1gNnSc1	city
a	a	k8xC	a
Magic	Magic	k1gMnSc1	Magic
1548	[number]	k4	1548
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
státních	státní	k2eAgFnPc2d1	státní
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
je	být	k5eAaImIp3nS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
Blue	Blu	k1gFnSc2	Blu
Coat	Coat	k2eAgInSc1d1	Coat
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
a	a	k8xC	a
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
Liverpool	Liverpool	k1gInSc4	Liverpool
College	Colleg	k1gFnSc2	Colleg
a	a	k8xC	a
St.	st.	kA	st.
Edward	Edward	k1gMnSc1	Edward
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
tři	tři	k4xCgFnPc4	tři
univerzity	univerzita	k1gFnPc4	univerzita
–	–	k?	–
University	universita	k1gFnSc2	universita
of	of	k?	of
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
Hope	Hop	k1gFnSc2	Hop
University	universita	k1gFnSc2	universita
a	a	k8xC	a
Liverpool	Liverpool	k1gInSc1	Liverpool
John	John	k1gMnSc1	John
Moores	Moores	k1gMnSc1	Moores
University	universita	k1gFnSc2	universita
–	–	k?	–
původně	původně	k6eAd1	původně
polytechnika	polytechnikum	k1gNnSc2	polytechnikum
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
statut	statut	k1gInSc4	statut
univerzity	univerzita	k1gFnSc2	univerzita
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
vzdělávací	vzdělávací	k2eAgFnSc7d1	vzdělávací
institucí	instituce	k1gFnSc7	instituce
je	být	k5eAaImIp3nS	být
Liverpoolská	liverpoolský	k2eAgFnSc1d1	liverpoolská
škola	škola	k1gFnSc1	škola
topických	topický	k2eAgFnPc2d1	topická
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
námořním	námořní	k2eAgInSc7d1	námořní
obchodem	obchod	k1gInSc7	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liverpoolský	liverpoolský	k2eAgInSc1d1	liverpoolský
institut	institut	k1gInSc1	institut
divadelního	divadelní	k2eAgNnSc2d1	divadelní
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Paulem	Paul	k1gMnSc7	Paul
McCartneym	McCartneym	k1gInSc4	McCartneym
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
techniků	technik	k1gMnPc2	technik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
institut	institut	k1gInSc1	institut
je	být	k5eAaImIp3nS	být
přidružen	přidružit	k5eAaPmNgInS	přidružit
k	k	k7c3	k
Liverpool	Liverpool	k1gInSc1	Liverpool
John	John	k1gMnSc1	John
Moores	Mooresa	k1gFnPc2	Mooresa
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
sídlí	sídlet	k5eAaImIp3nP	sídlet
dva	dva	k4xCgInPc1	dva
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
hrající	hrající	k2eAgInPc1d1	hrající
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
–	–	k?	–
Liverpool	Liverpool	k1gInSc1	Liverpool
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
Anfield	Anfield	k1gInSc1	Anfield
a	a	k8xC	a
Everton	Everton	k1gInSc1	Everton
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
Goodison	Goodison	k1gMnSc1	Goodison
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
pyšní	pyšnit	k5eAaImIp3nP	pyšnit
bohatou	bohatý	k2eAgFnSc7d1	bohatá
historií	historie	k1gFnSc7	historie
–	–	k?	–
Liverpool	Liverpool	k1gInSc1	Liverpool
je	být	k5eAaImIp3nS	být
osmnáctinásobným	osmnáctinásobný	k2eAgMnSc7d1	osmnáctinásobný
anglickým	anglický	k2eAgMnSc7d1	anglický
mistrem	mistr	k1gMnSc7	mistr
<g/>
,	,	kIx,	,
šestkrát	šestkrát	k6eAd1	šestkrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
sedmkrát	sedmkrát	k6eAd1	sedmkrát
Anglický	anglický	k2eAgInSc1d1	anglický
pohár	pohár	k1gInSc1	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Everton	Everton	k1gInSc1	Everton
je	být	k5eAaImIp3nS	být
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
nejdéle	dlouho	k6eAd3	dlouho
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
devítinásobným	devítinásobný	k2eAgMnSc7d1	devítinásobný
anglickým	anglický	k2eAgMnSc7d1	anglický
mistrem	mistr	k1gMnSc7	mistr
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Anglický	anglický	k2eAgInSc1d1	anglický
pohár	pohár	k1gInSc1	pohár
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
Pohár	pohár	k1gInSc4	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dostihovém	dostihový	k2eAgInSc6d1	dostihový
areálu	areál	k1gInSc6	areál
v	v	k7c6	v
Aintree	Aintree	k1gFnSc6	Aintree
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Sefton	Sefton	k1gInSc4	Sefton
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
každoročně	každoročně	k6eAd1	každoročně
pořádány	pořádán	k2eAgInPc4d1	pořádán
dostihové	dostihový	k2eAgInPc4d1	dostihový
závody	závod	k1gInPc4	závod
Grand	grand	k1gMnSc1	grand
National	National	k1gMnSc1	National
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
dostihových	dostihový	k2eAgFnPc2d1	dostihová
akcí	akce	k1gFnPc2	akce
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
účastí	účast	k1gFnSc7	účast
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pořádána	pořádat	k5eAaImNgFnS	pořádat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Známým	známý	k2eAgInSc7d1	známý
atletickým	atletický	k2eAgInSc7d1	atletický
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
Liverpool	Liverpool	k1gInSc1	Liverpool
Harriers	Harriers	k1gInSc1	Harriers
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
domovským	domovský	k2eAgInSc7d1	domovský
stadiónem	stadión	k1gInSc7	stadión
je	být	k5eAaImIp3nS	být
Wavertree	Wavertree	k1gNnSc1	Wavertree
Athletics	Athleticsa	k1gFnPc2	Athleticsa
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
bohatou	bohatý	k2eAgFnSc4d1	bohatá
boxerskou	boxerský	k2eAgFnSc4d1	boxerská
historii	historie	k1gFnSc4	historie
s	s	k7c7	s
takovými	takový	k3xDgNnPc7	takový
jmény	jméno	k1gNnPc7	jméno
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
John	John	k1gMnSc1	John
Conteh	Conteh	k1gMnSc1	Conteh
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Rudkin	Rudkin	k1gMnSc1	Rudkin
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Hodkinson	Hodkinson	k1gMnSc1	Hodkinson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistické	turistický	k2eAgFnPc1d1	turistická
atrakce	atrakce	k1gFnPc1	atrakce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přístav	přístav	k1gInSc1	přístav
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
Liverpoolské	liverpoolský	k2eAgNnSc1d1	Liverpoolské
pobřeží	pobřeží	k1gNnSc1	pobřeží
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
UNESCO	UNESCO	kA	UNESCO
památkou	památka	k1gFnSc7	památka
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
,	,	kIx,	,
odrážejíce	odrážet	k5eAaImSgMnP	odrážet
tak	tak	k8xC	tak
význam	význam	k1gInSc4	význam
města	město	k1gNnSc2	město
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
technologického	technologický	k2eAgInSc2d1	technologický
rozvoje	rozvoj	k1gInSc2	rozvoj
doků	dok	k1gInPc2	dok
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
dokem	dok	k1gInSc7	dok
je	být	k5eAaImIp3nS	být
Albertův	Albertův	k2eAgInSc1d1	Albertův
dok	dok	k1gInSc1	dok
–	–	k?	–
první	první	k4xOgMnSc1	první
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
dok	dok	k1gInSc1	dok
postavený	postavený	k2eAgInSc1d1	postavený
z	z	k7c2	z
nehořlavého	hořlavý	k2eNgInSc2d1	nehořlavý
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
první	první	k4xOgFnSc1	první
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
litiny	litina	k1gFnSc2	litina
<g/>
,	,	kIx,	,
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgMnS	být
Jesse	Jesse	k1gFnPc4	Jesse
Hartley	Hartlea	k1gFnPc4	Hartlea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvíce	hodně	k6eAd3	hodně
chráněných	chráněný	k2eAgFnPc2d1	chráněná
staveb	stavba	k1gFnPc2	stavba
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
původního	původní	k2eAgInSc2d1	původní
doku	dok	k1gInSc2	dok
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
sídlem	sídlo	k1gNnSc7	sídlo
Merseyside	Merseysid	k1gInSc5	Merseysid
Maritime	Maritim	k1gInSc5	Maritim
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
Liverpoolu	Liverpool	k1gInSc2	Liverpool
a	a	k8xC	a
Tate	Tat	k1gFnSc2	Tat
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Stanley	Stanle	k1gMnPc4	Stanle
Dock	Dock	k1gMnSc1	Dock
Tobacco	Tobacco	k1gMnSc1	Tobacco
Warehouse	Warehouse	k1gFnSc1	Warehouse
<g/>
,	,	kIx,	,
skladiště	skladiště	k1gNnSc1	skladiště
postavené	postavený	k2eAgInPc1d1	postavený
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
zastavěnou	zastavěný	k2eAgFnSc7d1	zastavěná
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
Pier	pier	k1gInSc1	pier
Head	Heada	k1gFnPc2	Heada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc4	tři
grácie	grácie	k1gFnPc4	grácie
–	–	k?	–
tři	tři	k4xCgFnPc4	tři
nejznámější	známý	k2eAgFnPc4d3	nejznámější
Liverpoolské	liverpoolský	k2eAgFnPc4d1	liverpoolská
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Royal	Royal	k1gInSc1	Royal
Liver	livra	k1gFnPc2	livra
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
dvěma	dva	k4xCgFnPc7	dva
vysokými	vysoký	k2eAgFnPc7d1	vysoká
věžemi	věž	k1gFnPc7	věž
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
vrcholech	vrchol	k1gInPc6	vrchol
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
sochy	socha	k1gFnPc1	socha
Liver	livra	k1gFnPc2	livra
Bird	Birda	k1gFnPc2	Birda
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
Liverpoolu	Liverpool	k1gInSc2	Liverpool
–	–	k?	–
pták	pták	k1gMnSc1	pták
vzdáleně	vzdáleně	k6eAd1	vzdáleně
připomínající	připomínající	k2eAgMnSc1d1	připomínající
orla	orel	k1gMnSc4	orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cunard	Cunard	k1gInSc1	Cunard
Building	Building	k1gInSc1	Building
–	–	k?	–
původní	původní	k2eAgNnSc4d1	původní
ústředí	ústředí	k1gNnSc4	ústředí
lodní	lodní	k2eAgFnSc2d1	lodní
společnosti	společnost	k1gFnSc2	společnost
Cunard	Cunarda	k1gFnPc2	Cunarda
<g/>
.	.	kIx.	.
</s>
<s>
Port	port	k1gInSc1	port
of	of	k?	of
Liverpool	Liverpool	k1gInSc1	Liverpool
Building	Building	k1gInSc1	Building
–	–	k?	–
původní	původní	k2eAgNnSc4d1	původní
sídlo	sídlo	k1gNnSc4	sídlo
úřadu	úřad	k1gInSc2	úřad
Mersey	Mersea	k1gFnSc2	Mersea
Docks	Docksa	k1gFnPc2	Docksa
and	and	k?	and
Harbour	Harbour	k1gMnSc1	Harbour
Board	Board	k1gMnSc1	Board
regulujícího	regulující	k2eAgInSc2d1	regulující
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
doku	dok	k1gInSc6	dok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
Světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
bylo	být	k5eAaImAgNnS	být
historické	historický	k2eAgNnSc4d1	historické
liverpoolské	liverpoolský	k2eAgNnSc4d1	Liverpoolské
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
kvůli	kvůli	k7c3	kvůli
připravovanému	připravovaný	k2eAgInSc3d1	připravovaný
projektu	projekt	k1gInSc3	projekt
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
,	,	kIx,	,
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Liverpool	Liverpool	k1gInSc1	Liverpool
Waters	Waters	k1gInSc4	Waters
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světových	světový	k2eAgFnPc2d1	světová
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
stavby	stavba	k1gFnPc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
imigrantů	imigrant	k1gMnPc2	imigrant
a	a	k8xC	a
námořníků	námořník	k1gMnPc2	námořník
procházejících	procházející	k2eAgMnPc2d1	procházející
přístavem	přístav	k1gInSc7	přístav
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgFnP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
různá	různý	k2eAgNnPc1d1	různé
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
je	být	k5eAaImIp3nS	být
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
Our	Our	k1gMnSc1	Our
Lady	Lada	k1gFnSc2	Lada
and	and	k?	and
St	St	kA	St
Nicholas	Nicholas	k1gMnSc1	Nicholas
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
stojí	stát	k5eAaImIp3nS	stát
poblíž	poblíž	k7c2	poblíž
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
chrámy	chrám	k1gInPc7	chrám
jsou	být	k5eAaImIp3nP	být
řecký	řecký	k2eAgMnSc1d1	řecký
ortodoxní	ortodoxní	k2eAgMnSc1d1	ortodoxní
Church	Church	k1gMnSc1	Church
of	of	k?	of
St	St	kA	St
Nicholas	Nicholas	k1gMnSc1	Nicholas
(	(	kIx(	(
<g/>
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
byzantském	byzantský	k2eAgInSc6d1	byzantský
slohu	sloh	k1gInSc6	sloh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gustav	Gustav	k1gMnSc1	Gustav
Adolfus	Adolfus	k1gMnSc1	Adolfus
Kyrka	Kyrka	k1gMnSc1	Kyrka
(	(	kIx(	(
<g/>
kostel	kostel	k1gInSc1	kostel
švédských	švédský	k2eAgMnPc2d1	švédský
námořníků	námořník	k1gMnPc2	námořník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohatství	bohatství	k1gNnSc1	bohatství
Liverpoolu	Liverpool	k1gInSc2	Liverpool
jako	jako	k8xS	jako
přístavního	přístavní	k2eAgNnSc2d1	přístavní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
dvou	dva	k4xCgFnPc2	dva
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
Liverpoolská	liverpoolský	k2eAgFnSc1d1	liverpoolská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
navržená	navržený	k2eAgFnSc1d1	navržená
sirem	sir	k1gMnSc7	sir
Gilesem	Giles	k1gMnSc7	Giles
Gilbertem	Gilbert	k1gMnSc7	Gilbert
Scottem	Scott	k1gMnSc7	Scott
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
chrámových	chrámový	k2eAgFnPc2d1	chrámová
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
největších	veliký	k2eAgFnPc2d3	veliký
varhan	varhany	k1gFnPc2	varhany
a	a	k8xC	a
nejhlasitější	hlasitý	k2eAgNnSc1d3	nejhlasitější
zvonění	zvonění	k1gNnSc1	zvonění
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
Liverpoolská	liverpoolský	k2eAgFnSc1d1	liverpoolská
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
katedrála	katedrála	k1gFnSc1	katedrála
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Liverpoolská	liverpoolský	k2eAgFnSc1d1	liverpoolská
katedrála	katedrála	k1gFnSc1	katedrála
ale	ale	k8xC	ale
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
plánů	plán	k1gInPc2	plán
nakonec	nakonec	k6eAd1	nakonec
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
největším	veliký	k2eAgInSc7d3	veliký
panelem	panel	k1gInSc7	panel
barevného	barevný	k2eAgNnSc2d1	barevné
skla	sklo	k1gNnSc2	sklo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c4	mnoho
synagog	synagoga	k1gFnPc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Princes	princes	k1gInSc4	princes
Road	Roada	k1gFnPc2	Roada
Synagogue	Synagogu	k1gInSc2	Synagogu
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
mešit	mešita	k1gFnPc2	mešita
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
Williamem	William	k1gInSc7	William
Abdullahem	Abdullah	k1gInSc7	Abdullah
Quilliamem	Quilliam	k1gInSc7	Quilliam
<g/>
,	,	kIx,	,
právníkem	právník	k1gMnSc7	právník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
k	k	k7c3	k
Islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
městskou	městský	k2eAgFnSc7d1	městská
mešitou	mešita	k1gFnSc7	mešita
mešita	mešita	k1gFnSc1	mešita
Al-Rahma	Al-Rahma	k1gFnSc1	Al-Rahma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
stavby	stavba	k1gFnPc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
William	William	k1gInSc1	William
Brown	Brown	k1gMnSc1	Brown
Street	Streeta	k1gFnPc2	Streeta
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
městská	městský	k2eAgFnSc1d1	městská
kulturní	kulturní	k2eAgFnSc1d1	kulturní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
některé	některý	k3yIgFnPc1	některý
stavby	stavba	k1gFnPc1	stavba
důležitých	důležitý	k2eAgFnPc2d1	důležitá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
jako	jako	k8xC	jako
William	William	k1gInSc1	William
Brown	Browna	k1gFnPc2	Browna
Library	Librara	k1gFnSc2	Librara
<g/>
,	,	kIx,	,
Walker	Walker	k1gMnSc1	Walker
Art	Art	k1gFnSc2	Art
Gallery	Galler	k1gInPc4	Galler
a	a	k8xC	a
World	World	k1gInSc1	World
Museum	museum	k1gNnSc1	museum
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
Liverpoolských	liverpoolský	k2eAgFnPc2d1	liverpoolská
neoklasicistních	neoklasicistní	k2eAgFnPc2d1	neoklasicistní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
St.	st.	kA	st.
George	George	k1gInSc1	George
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejpůsobivějších	působivý	k2eAgFnPc2d3	nejpůsobivější
neoklasicistních	neoklasicistní	k2eAgFnPc2d1	neoklasicistní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
koncertní	koncertní	k2eAgInSc1d1	koncertní
sál	sál	k1gInSc1	sál
i	i	k8xC	i
soudní	soudní	k2eAgFnSc1d1	soudní
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Liverpoolská	liverpoolský	k2eAgFnSc1d1	liverpoolská
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1754	[number]	k4	1754
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
bohatou	bohatý	k2eAgFnSc7d1	bohatá
výzdobou	výzdoba	k1gFnSc7	výzdoba
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Red	Red	k1gFnSc2	Red
Brick	Bricka	k1gFnPc2	Bricka
University	universita	k1gFnSc2	universita
používaný	používaný	k2eAgInSc4d1	používaný
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
mnoha	mnoho	k4c2	mnoho
britských	britský	k2eAgFnPc2d1	britská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
byl	být	k5eAaImAgInS	být
inspirován	inspirován	k2eAgInSc1d1	inspirován
Victoria	Victorium	k1gNnSc2	Victorium
Building	Building	k1gInSc4	Building
University	universita	k1gFnSc2	universita
od	od	k7c2	od
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc7d1	známá
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Liverpool	Liverpool	k1gInSc1	Liverpool
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Liverpool	Liverpool	k1gInSc4	Liverpool
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Liverpool	Liverpool	k1gInSc1	Liverpool
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Rada	Rada	k1gMnSc1	Rada
města	město	k1gNnSc2	město
Liverpool	Liverpool	k1gInSc1	Liverpool
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
Art	Art	k1gFnSc2	Art
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
:	:	kIx,	:
European	European	k1gMnSc1	European
Capital	Capital	k1gMnSc1	Capital
of	of	k?	of
Culture	Cultur	k1gMnSc5	Cultur
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Pohledy	pohled	k1gInPc1	pohled
na	na	k7c6	na
Liverpoolu	Liverpool	k1gInSc6	Liverpool
</s>
</p>
<p>
<s>
History	Histor	k1gInPc1	Histor
Maritime	Maritim	k1gInSc5	Maritim
Liverpool	Liverpool	k1gInSc1	Liverpool
</s>
</p>
<p>
<s>
Photos	Photos	k1gInSc1	Photos
Liverpool	Liverpool	k1gInSc1	Liverpool
</s>
</p>
<p>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
</s>
</p>
