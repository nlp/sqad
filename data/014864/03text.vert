<s>
Evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Růžové	růžový	k2eAgFnSc6d1
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Růžové	růžový	k2eAgFnSc6d1
Lokalita	lokalita	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
15,41	15,41	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
1,83	1,83	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c4
Růžové	růžový	k2eAgNnSc4d1
na	na	k7c6
Děčínsku	Děčínsko	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
na	na	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
svahu	svah	k1gInSc6
Kovářova	Kovářův	k2eAgInSc2d1
kopce	kopec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
přibližně	přibližně	k6eAd1
2	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západně	západně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gNnSc2
je	být	k5eAaImIp3nS
na	na	k7c4
úbočí	úbočí	k1gNnPc4
Petrova	Petrův	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
katolický	katolický	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Hřbitov	hřbitov	k1gInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1862	#num#	k4
po	po	k7c4
zrušení	zrušení	k1gNnSc4
náboženských	náboženský	k2eAgNnPc2d1
diskriminačních	diskriminační	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
posvěcen	posvětit	k5eAaPmNgMnS
byl	být	k5eAaImAgMnS
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
evangelického	evangelický	k2eAgInSc2d1
kostela	kostel	k1gInSc2
postaveného	postavený	k2eAgInSc2d1
roku	rok	k1gInSc2
1864	#num#	k4
k	k	k7c3
němu	on	k3xPp3gNnSc3
vedla	vést	k5eAaImAgFnS
cesta	cesta	k1gFnSc1
lemovaná	lemovaný	k2eAgFnSc1d1
starou	starý	k2eAgFnSc7d1
alejí	alej	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evangelická	evangelický	k2eAgFnSc1d1
obec	obec	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
hřbitov	hřbitov	k1gInSc4
bez	bez	k7c2
symbolu	symbol	k1gInSc2
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
hřbitovní	hřbitovní	k2eAgInSc4d1
kříž	kříž	k1gInSc4
zde	zde	k6eAd1
prosadil	prosadit	k5eAaPmAgMnS
evangelický	evangelický	k2eAgMnSc1d1
farář	farář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3
náhrobek	náhrobek	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
hřbitova	hřbitov	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
dochovaný	dochovaný	k2eAgInSc1d1
nápis	nápis	k1gInSc1
o	o	k7c6
slavnostním	slavnostní	k2eAgNnSc6d1
pohřbení	pohřbení	k1gNnSc6
dvou	dva	k4xCgFnPc2
malých	malý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
jménem	jméno	k1gNnSc7
Franziska	Franziska	k1gFnSc1
Hille	Hille	k1gNnSc2
z	z	k7c2
čp.	čp.	k?
160	#num#	k4
(	(	kIx(
<g/>
narození	narození	k1gNnSc2
8	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1862	#num#	k4
<g/>
,	,	kIx,
úmrtí	úmrtí	k1gNnSc2
25	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1862	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Emanuel	Emanuel	k1gMnSc1
Richter	Richter	k1gMnSc1
(	(	kIx(
<g/>
úmrtí	úmrtí	k1gNnSc2
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1861	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Emanuel	Emanuel	k1gMnSc1
Richter	Richter	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
protestantským	protestantský	k2eAgMnSc7d1
rodičům	rodič	k1gMnPc3
a	a	k8xC
nesměl	smět	k5eNaImAgMnS
být	být	k5eAaImF
pohřben	pohřbít	k5eAaPmNgMnS
na	na	k7c6
katolickém	katolický	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
roku	rok	k1gInSc2
1861	#num#	k4
na	na	k7c6
louce	louka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
patřila	patřit	k5eAaImAgFnS
evangelíku	evangelík	k1gMnSc3
Johannu	Johann	k1gMnSc3
Guthovi	Guth	k1gMnSc3
a	a	k8xC
na	na	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
části	část	k1gFnSc6
evangelická	evangelický	k2eAgFnSc1d1
obec	obec	k1gFnSc1
později	pozdě	k6eAd2
postavila	postavit	k5eAaPmAgFnS
kostel	kostel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
prvním	první	k4xOgInSc6
pohřbu	pohřeb	k1gInSc6
na	na	k7c6
nově	nově	k6eAd1
založeném	založený	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
byly	být	k5eAaImAgInP
ostatky	ostatek	k1gInPc1
dítěte	dítě	k1gNnSc2
přeneseny	přenést	k5eAaPmNgFnP
na	na	k7c4
hřbitov	hřbitov	k1gInSc4
a	a	k8xC
pohřbeny	pohřben	k2eAgFnPc1d1
s	s	k7c7
druhým	druhý	k4xOgNnSc7
dítětem	dítě	k1gNnSc7
do	do	k7c2
jednoho	jeden	k4xCgInSc2
hrobu	hrob	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgInP
oba	dva	k4xCgInPc1
hřbitovy	hřbitov	k1gInPc1
<g/>
,	,	kIx,
evangelický	evangelický	k2eAgMnSc1d1
i	i	k8xC
sousední	sousední	k2eAgMnSc1d1
katolický	katolický	k2eAgMnSc1d1
<g/>
,	,	kIx,
zbaveny	zbavit	k5eAaPmNgFnP
náletových	náletový	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
a	a	k8xC
vyčištěny	vyčištěn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
podstavec	podstavec	k1gInSc4
hřbitovního	hřbitovní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
nainstalován	nainstalován	k2eAgInSc1d1
železný	železný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
s	s	k7c7
použitím	použití	k1gNnSc7
litinových	litinový	k2eAgFnPc2d1
částí	část	k1gFnPc2
původního	původní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1862	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Václav	Václav	k1gMnSc1
Zeman	Zeman	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
pohřeb	pohřeb	k1gInSc4
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgMnPc6
děti	dítě	k1gFnPc4
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1862	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zeman	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
:	:	kIx,
Nekatolíci	nekatolík	k1gMnPc1
v	v	k7c4
Růžové	růžový	k2eAgNnSc4d1
na	na	k7c6
Děčínsku	Děčínsko	k1gNnSc6
na	na	k7c6
cestě	cesta	k1gFnSc6
mezi	mezi	k7c7
herrnhutskou	herrnhutský	k2eAgFnSc7d1
a	a	k8xC
evangelickou	evangelický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
–	–	k?
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Vydání	vydání	k1gNnSc1
<g/>
:	:	kIx,
11	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc6d1
z	z	k7c2
WWW	WWW	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
objekty	objekt	k1gInPc4
<g/>
:	:	kIx,
Růžová-hřbitovy	Růžová-hřbitův	k2eAgInPc4d1
a	a	k8xC
evangelický	evangelický	k2eAgInSc4d1
kostel	kostel	k1gInSc4
(	(	kIx(
<g/>
Rosendorf	Rosendorf	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
Hřbitovy	hřbitov	k1gInPc1
u	u	k7c2
Růžové	růžový	k2eAgFnSc2d1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
18.01	18.01	k4
<g/>
.2007	.2007	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc6d1
z	z	k7c2
WWW	WWW	kA
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Růžová	růžový	k2eAgFnSc1d1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
protestantských	protestantský	k2eAgInPc2d1
hřbitovů	hřbitov	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
