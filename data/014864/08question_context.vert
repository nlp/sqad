<s desamb="1">
Při	při	k7c6
prvním	první	k4xOgInSc6
pohřbu	pohřeb	k1gInSc6
na	na	k7c6
nově	nově	k6eAd1
založeném	založený	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
byly	být	k5eAaImAgInP
ostatky	ostatek	k1gInPc1
dítěte	dítě	k1gNnSc2
přeneseny	přenést	k5eAaPmNgFnP
na	na	k7c4
hřbitov	hřbitov	k1gInSc4
a	a	k8xC
pohřbeny	pohřben	k2eAgFnPc1d1
s	s	k7c7
druhým	druhý	k4xOgNnSc7
dítětem	dítě	k1gNnSc7
do	do	k7c2
jednoho	jeden	k4xCgInSc2
hrobu	hrob	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
roku	rok	k1gInSc2
1861	#num#	k4
na	na	k7c6
louce	louka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
patřila	patřit	k5eAaImAgFnS
evangelíku	evangelík	k1gMnSc3
Johannu	Johann	k1gMnSc3
Guthovi	Guth	k1gMnSc3
a	a	k8xC
na	na	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
části	část	k1gFnSc6
evangelická	evangelický	k2eAgFnSc1d1
obec	obec	k1gFnSc1
později	pozdě	k6eAd2
postavila	postavit	k5eAaPmAgFnS
kostel	kostel	k1gInSc4
<g/>
.	.	kIx.
</s>