<s>
Nejstarší	starý	k2eAgInSc1d3
náhrobek	náhrobek	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
hřbitova	hřbitov	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
dochovaný	dochovaný	k2eAgInSc1d1
nápis	nápis	k1gInSc1
o	o	k7c6
slavnostním	slavnostní	k2eAgNnSc6d1
pohřbení	pohřbení	k1gNnSc6
dvou	dva	k4xCgFnPc2
malých	malý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
jménem	jméno	k1gNnSc7
Franziska	Franziska	k1gFnSc1
Hille	Hille	k1gNnSc2
z	z	k7c2
čp.	čp.	k?
160	#num#	k4
(	(	kIx(
<g/>
narození	narození	k1gNnSc2
8	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1862	#num#	k4
<g/>
,	,	kIx,
úmrtí	úmrtí	k1gNnSc2
25	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1862	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Emanuel	Emanuel	k1gMnSc1
Richter	Richter	k1gMnSc1
(	(	kIx(
<g/>
úmrtí	úmrtí	k1gNnSc2
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1861	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Emanuel	Emanuel	k1gMnSc1
Richter	Richter	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
protestantským	protestantský	k2eAgMnSc7d1
rodičům	rodič	k1gMnPc3
a	a	k8xC
nesměl	smět	k5eNaImAgMnS
být	být	k5eAaImF
pohřben	pohřbít	k5eAaPmNgMnS
na	na	k7c6
katolickém	katolický	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
u	u	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
.	.	kIx.
</s>