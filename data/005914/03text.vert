<s>
Psycho-Pass	Psycho-Pass	k1gInSc1	Psycho-Pass
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
サ	サ	k?	サ
<g/>
,	,	kIx,	,
Saiko	Saiko	k1gNnSc1	Saiko
Pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
psáno	psát	k5eAaImNgNnS	psát
jako	jako	k8xC	jako
PSYCHO-PASS	PSYCHO-PASS	k1gFnSc1	PSYCHO-PASS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgMnSc1d1	japonský
anime	animat	k5eAaPmIp3nS	animat
seriál	seriál	k1gInSc1	seriál
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
studiem	studio	k1gNnSc7	studio
Production	Production	k1gInSc4	Production
I.G.	I.G.	k1gFnPc1	I.G.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
v	v	k7c6	v
televizním	televizní	k2eAgNnSc6d1	televizní
pásmu	pásmo	k1gNnSc6	pásmo
noitaminA	noitaminA	k?	noitaminA
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
studio	studio	k1gNnSc1	studio
Tacunoko	Tacunoko	k1gNnSc1	Tacunoko
Pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomtéž	týž	k3xTgNnSc6	týž
pásmu	pásmo	k1gNnSc6	pásmo
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
japonských	japonský	k2eAgNnPc6d1	Japonské
kinech	kino	k1gNnPc6	kino
premiéru	premiér	k1gMnSc3	premiér
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
okamžitě	okamžitě	k6eAd1	okamžitě
změřit	změřit	k5eAaPmF	změřit
a	a	k8xC	a
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
duševní	duševní	k2eAgInSc4d1	duševní
stav	stav	k1gInSc4	stav
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
daná	daný	k2eAgFnSc1d1	daná
osoba	osoba	k1gFnSc1	osoba
spáchá	spáchat	k5eAaPmIp3nS	spáchat
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Tyhle	tenhle	k3xDgFnPc4	tenhle
informace	informace	k1gFnPc1	informace
měří	měřit	k5eAaImIp3nP	měřit
zařízení	zařízení	k1gNnPc1	zařízení
zvané	zvaný	k2eAgInPc4d1	zvaný
psychometr	psychometr	k1gInSc4	psychometr
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
tzv.	tzv.	kA	tzv.
součinitele	součinitel	k1gInSc2	součinitel
zločinu	zločin	k1gInSc2	zločin
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
danou	daný	k2eAgFnSc4d1	daná
osobu	osoba	k1gFnSc4	osoba
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
,	,	kIx,	,
v	v	k7c6	v
horších	zlý	k2eAgInPc6d2	horší
případech	případ	k1gInPc6	případ
rovnou	rovnou	k6eAd1	rovnou
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
nebo	nebo	k8xC	nebo
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úkol	úkol	k1gInSc1	úkol
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
speciální	speciální	k2eAgInSc1d1	speciální
tým	tým	k1gInSc1	tým
tzv.	tzv.	kA	tzv.
naháněčů	naháněč	k1gMnPc2	naháněč
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
potenciální	potenciální	k2eAgMnPc1d1	potenciální
kriminálníci	kriminálník	k1gMnPc1	kriminálník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
stíhají	stíhat	k5eAaImIp3nP	stíhat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
kriminálníky	kriminálník	k1gMnPc4	kriminálník
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jim	on	k3xPp3gMnPc3	on
přidělený	přidělený	k2eAgMnSc1d1	přidělený
naháněč	naháněč	k1gMnSc1	naháněč
jednal	jednat	k5eAaImAgMnS	jednat
opravdu	opravdu	k6eAd1	opravdu
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
naháněč	naháněč	k1gMnSc1	naháněč
a	a	k8xC	a
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
tzv.	tzv.	kA	tzv.
Dominátor	Dominátor	k1gInSc1	Dominátor
<g/>
;	;	kIx,	;
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
střílet	střílet	k5eAaImF	střílet
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hodnota	hodnota	k1gFnSc1	hodnota
součinitele	součinitel	k1gInSc2	součinitel
zločinu	zločin	k1gInSc2	zločin
je	být	k5eAaImIp3nS	být
nebezpečně	bezpečně	k6eNd1	bezpečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
seriálu	seriál	k1gInSc2	seriál
jsou	být	k5eAaImIp3nP	být
Šin	Šin	k1gFnPc1	Šin
<g/>
'	'	kIx"	'
<g/>
ja	ja	k?	ja
Kógami	Kóga	k1gFnPc7	Kóga
a	a	k8xC	a
Akane	Akan	k1gMnSc5	Akan
Cunemori	Cunemor	k1gMnSc5	Cunemor
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnPc1	dvojice
naháněče	naháněč	k1gMnSc2	naháněč
a	a	k8xC	a
vyšetřovatele	vyšetřovatel	k1gMnSc2	vyšetřovatel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
řeší	řešit	k5eAaImIp3nS	řešit
různé	různý	k2eAgInPc4d1	různý
detektivní	detektivní	k2eAgInPc4d1	detektivní
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
obětím	oběť	k1gFnPc3	oběť
zločinu	zločin	k1gInSc2	zločin
se	se	k3xPyFc4	se
hodnota	hodnota	k1gFnSc1	hodnota
součinitele	součinitel	k1gInSc2	součinitel
zločinu	zločin	k1gInSc2	zločin
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
hodně	hodně	k6eAd1	hodně
vysoko	vysoko	k6eAd1	vysoko
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
kvůli	kvůli	k7c3	kvůli
strachu	strach	k1gInSc3	strach
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
schopen	schopen	k2eAgMnSc1d1	schopen
udělat	udělat	k5eAaPmF	udělat
cokoli	cokoli	k3yInSc4	cokoli
aby	aby	kYmCp3nS	aby
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
naháněči	naháněč	k1gMnPc1	naháněč
a	a	k8xC	a
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
musí	muset	k5eAaImIp3nP	muset
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
popřípadě	popřípadě	k6eAd1	popřípadě
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Šin	Šin	k?	Šin
<g/>
'	'	kIx"	'
<g/>
ja	ja	k?	ja
Kógami	Kóga	k1gFnPc7	Kóga
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
狡	狡	k?	狡
慎	慎	k?	慎
<g/>
,	,	kIx,	,
Kógami	Kóga	k1gFnPc7	Kóga
Šin	Šin	k1gMnPc2	Šin
<g/>
'	'	kIx"	'
<g/>
ja	ja	k?	ja
<g/>
)	)	kIx)	)
Seijú	Seijú	k1gMnSc1	Seijú
<g/>
:	:	kIx,	:
Tomokazu	Tomokaz	k1gInSc2	Tomokaz
Seki	Sek	k1gMnSc5	Sek
Akane	Akan	k1gMnSc5	Akan
Cunemori	Cunemor	k1gMnSc5	Cunemor
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
常	常	k?	常
朱	朱	k?	朱
<g/>
,	,	kIx,	,
Cunemori	Cunemor	k1gMnSc5	Cunemor
Akane	Akan	k1gMnSc5	Akan
<g/>
)	)	kIx)	)
Seijú	Seijú	k1gMnSc6	Seijú
<g/>
:	:	kIx,	:
Kana	kanout	k5eAaImSgMnS	kanout
Hanazawa	Hanazaw	k2eAgMnSc4d1	Hanazaw
Nobučika	Nobučik	k1gMnSc4	Nobučik
Ginoza	Ginoz	k1gMnSc4	Ginoz
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
宜	宜	k?	宜
伸	伸	k?	伸
<g/>
,	,	kIx,	,
Ginoza	Ginoza	k1gFnSc1	Ginoza
Nobučika	Nobučika	k1gFnSc1	Nobučika
<g/>
)	)	kIx)	)
Seijú	Seijú	k1gMnSc1	Seijú
<g/>
:	:	kIx,	:
Kendži	Kendž	k1gFnSc6	Kendž
Nodžima	Nodžim	k1gMnSc2	Nodžim
Skladatelem	skladatel	k1gMnSc7	skladatel
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
anime	animat	k5eAaPmIp3nS	animat
Psycho-Pass	Psycho-Pass	k1gInSc1	Psycho-Pass
je	on	k3xPp3gMnPc4	on
Júgo	Júgo	k6eAd1	Júgo
Kanno	Kanno	k6eAd1	Kanno
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgFnSc4d1	úvodní
skladbu	skladba	k1gFnSc4	skladba
Abnormalize	Abnormalize	k1gFnSc2	Abnormalize
k	k	k7c3	k
prvním	první	k4xOgFnPc3	první
dvanácti	dvanáct	k4xCc2	dvanáct
dílům	díl	k1gInPc3	díl
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
skupina	skupina	k1gFnSc1	skupina
Ling	Ling	k1gMnSc1	Ling
Tosite	Tosit	k1gInSc5	Tosit
Sigure	Sigur	k1gMnSc5	Sigur
a	a	k8xC	a
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
znělku	znělka	k1gFnSc4	znělka
Namae	Nama	k1gInSc2	Nama
no	no	k9	no
nai	nai	k?	nai
kaibucu	kaibuca	k1gFnSc4	kaibuca
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
名	名	k?	名
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Bezejmenná	bezejmenný	k2eAgFnSc1d1	bezejmenná
příšera	příšera	k1gFnSc1	příšera
<g/>
)	)	kIx)	)
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
s	s	k7c7	s
pseudonymem	pseudonym	k1gInSc7	pseudonym
Egoist	Egoist	k1gMnSc1	Egoist
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
i	i	k9	i
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
znělku	znělka	k1gFnSc4	znělka
All	All	k1gFnSc2	All
Alone	Alon	k1gInSc5	Alon
With	With	k1gInSc4	With
You	You	k1gMnPc2	You
ke	k	k7c3	k
zbylým	zbylý	k2eAgInPc3d1	zbylý
dílům	díl	k1gInPc3	díl
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
úvodní	úvodní	k2eAgFnSc7d1	úvodní
znělkou	znělka	k1gFnSc7	znělka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
třináctého	třináctý	k4xOgInSc2	třináctý
dílu	díl	k1gInSc2	díl
píseň	píseň	k1gFnSc1	píseň
Out	Out	k1gFnSc1	Out
of	of	k?	of
Control	Controla	k1gFnPc2	Controla
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Nothing	Nothing	k1gInSc1	Nothing
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Carved	Carved	k1gInSc1	Carved
in	in	k?	in
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
