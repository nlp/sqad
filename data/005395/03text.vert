<s>
Noha	noha	k1gFnSc1	noha
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
váhu	váha	k1gFnSc4	váha
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Noha	noha	k1gFnSc1	noha
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
obecně	obecně	k6eAd1	obecně
nazývat	nazývat	k5eAaImF	nazývat
zadní	zadní	k2eAgFnSc1d1	zadní
končetina	končetina	k1gFnSc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
nejdistálnějším	distální	k2eAgMnSc7d3	distální
(	(	kIx(	(
<g/>
nejvzdálenější	vzdálený	k2eAgInPc1d3	nejvzdálenější
od	od	k7c2	od
trupu	trup	k1gInSc2	trup
<g/>
)	)	kIx)	)
oddílem	oddíl	k1gInSc7	oddíl
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
oddílem	oddíl	k1gInSc7	oddíl
horní	horní	k2eAgFnSc2d1	horní
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesně	přesně	k6eNd1	přesně
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
anatomická	anatomický	k2eAgFnSc1d1	anatomická
struktura	struktura	k1gFnSc1	struktura
zvaná	zvaný	k2eAgFnSc1d1	zvaná
noha	noha	k1gFnSc1	noha
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
za	za	k7c4	za
dolní	dolní	k2eAgFnSc4d1	dolní
končetinu	končetina	k1gFnSc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Noha	noha	k1gFnSc1	noha
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
26	[number]	k4	26
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Hlezenní	hlezenní	k2eAgInSc1d1	hlezenní
kloub	kloub	k1gInSc1	kloub
spojuje	spojovat	k5eAaImIp3nS	spojovat
nohu	noha	k1gFnSc4	noha
s	s	k7c7	s
bércem	bérec	k1gInSc7	bérec
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
nohy	noha	k1gFnSc2	noha
je	být	k5eAaImIp3nS	být
vyklenuta	vyklenout	k5eAaPmNgFnS	vyklenout
do	do	k7c2	do
podélné	podélný	k2eAgFnSc2d1	podélná
a	a	k8xC	a
příčné	příčný	k2eAgFnSc2d1	příčná
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
pevnější	pevný	k2eAgMnSc1d2	pevnější
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
méně	málo	k6eAd2	málo
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
noha	noha	k1gFnSc1	noha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
noha	noha	k1gFnSc1	noha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
