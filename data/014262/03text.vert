<s>
Mikroekonomie	mikroekonomie	k1gFnSc1
</s>
<s>
Q	Q	kA
jako	jako	k8xS,k8xC
kvantita	kvantita	k1gFnSc1
a	a	k8xC
P	P	kA
jako	jako	k8xS,k8xC
cena	cena	k1gFnSc1
jednotlivého	jednotlivý	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
navýšení	navýšení	k1gNnSc1
poptávky	poptávka	k1gFnSc2
navýšilo	navýšit	k5eAaPmAgNnS
současně	současně	k6eAd1
i	i	k9
cenuMikroekonomie	cenuMikroekonomie	k1gFnSc1
je	být	k5eAaImIp3nS
obor	obor	k1gInSc4
ekonomické	ekonomický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
zkoumáním	zkoumání	k1gNnSc7
rozhodování	rozhodování	k1gNnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
tržních	tržní	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
–	–	k?
jednotlivců	jednotlivec	k1gMnPc2
<g/>
/	/	kIx~
<g/>
domácností	domácnost	k1gFnPc2
<g/>
,	,	kIx,
firem	firma	k1gFnPc2
a	a	k8xC
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikroekonomie	mikroekonomie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
poptávkou	poptávka	k1gFnSc7
a	a	k8xC
nabídkou	nabídka	k1gFnSc7
v	v	k7c6
jejich	jejich	k3xOp3gFnPc6
individuálních	individuální	k2eAgFnPc6d1
a	a	k8xC
konkrétních	konkrétní	k2eAgFnPc6d1
podobách	podoba	k1gFnPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
makroekonomie	makroekonomie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
agregátní	agregátní	k2eAgFnSc7d1
nabídkou	nabídka	k1gFnSc7
a	a	k8xC
agregátní	agregátní	k2eAgFnSc7d1
poptávkou	poptávka	k1gFnSc7
<g/>
,	,	kIx,
tj.	tj.	kA
komplexními	komplexní	k2eAgInPc7d1
trhy	trh	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
cílů	cíl	k1gInPc2
mikroekonomie	mikroekonomie	k1gFnSc2
je	být	k5eAaImIp3nS
analyzovat	analyzovat	k5eAaImF
tržní	tržní	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
sestavuje	sestavovat	k5eAaImIp3nS
relativní	relativní	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
mezi	mezi	k7c7
produkty	produkt	k1gInPc7
a	a	k8xC
službami	služba	k1gFnPc7
a	a	k8xC
alokovat	alokovat	k5eAaImF
omezené	omezený	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
mezi	mezi	k7c7
alternativními	alternativní	k2eAgFnPc7d1
potřebami	potřeba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikroekonomie	mikroekonomie	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
podmínky	podmínka	k1gFnPc4
pod	pod	k7c7
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
volný	volný	k2eAgInSc4d1
trh	trh	k1gInSc4
vede	vést	k5eAaImIp3nS
k	k	k7c3
vhodným	vhodný	k2eAgFnPc3d1
alokacím	alokace	k1gFnPc3
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
analyzuje	analyzovat	k5eAaImIp3nS
tržní	tržní	k2eAgInSc1d1
propad	propad	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
trh	trh	k1gInSc1
selže	selhat	k5eAaPmIp3nS
v	v	k7c6
produkci	produkce	k1gFnSc6
výkonných	výkonný	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
.	.	kIx.
</s>
<s>
Mikroekonomie	mikroekonomie	k1gFnSc1
je	být	k5eAaImIp3nS
kontrast	kontrast	k1gInSc4
k	k	k7c3
makroekonomii	makroekonomie	k1gFnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
„	„	k?
celkovou	celkový	k2eAgFnSc4d1
část	část	k1gFnSc4
ekonomické	ekonomický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
s	s	k7c7
problémy	problém	k1gInPc7
růstu	růst	k1gInSc2
<g/>
,	,	kIx,
inflace	inflace	k1gFnSc2
a	a	k8xC
nezaměstnanosti	nezaměstnanost	k1gFnSc2
a	a	k8xC
v	v	k7c6
národní	národní	k2eAgFnSc6d1
politice	politika	k1gFnSc6
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
těmto	tento	k3xDgInPc3
problémům	problém	k1gInPc3
také	také	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
Mikroekonomie	mikroekonomie	k1gFnSc1
také	také	k6eAd1
jedná	jednat	k5eAaImIp3nS
s	s	k7c7
efekty	efekt	k1gInPc7
ekonomických	ekonomický	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
(	(	kIx(
jako	jako	k8xC,k8xS
změny	změna	k1gFnPc1
daní	daň	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c4
mikroekonomické	mikroekonomický	k2eAgNnSc4d1
chování	chování	k1gNnSc4
a	a	k8xC
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
na	na	k7c4
výše	vysoce	k6eAd2
uvedené	uvedený	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Zejména	zejména	k9
v	v	k7c6
Lucasově	Lucasův	k2eAgFnSc6d1
kritice	kritika	k1gFnSc6
<g/>
,	,	kIx,
mnoho	mnoho	k6eAd1
z	z	k7c2
moderních	moderní	k2eAgFnPc2d1
makroekonomických	makroekonomický	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
na	na	k7c4
mikrofundace	mikrofundace	k1gFnPc4
-	-	kIx~
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
základních	základní	k2eAgInPc6d1
předpokladech	předpoklad	k1gInPc6
o	o	k7c6
mikroekonomickém	mikroekonomický	k2eAgNnSc6d1
chování	chování	k1gNnSc6
</s>
<s>
Subjekty	subjekt	k1gInPc1
</s>
<s>
Jednotlivci	jednotlivec	k1gMnSc3
(	(	kIx(
<g/>
domácnosti	domácnost	k1gFnSc2
<g/>
)	)	kIx)
poptávají	poptávat	k5eAaImIp3nP
vytvořený	vytvořený	k2eAgInSc4d1
produkt	produkt	k1gInSc4
ekonomiky	ekonomika	k1gFnSc2
na	na	k7c6
trhu	trh	k1gInSc6
statků	statek	k1gInPc2
a	a	k8xC
nabízejí	nabízet	k5eAaImIp3nP
služby	služba	k1gFnPc4
výrobních	výrobní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
(	(	kIx(
<g/>
práce	práce	k1gFnSc1
a	a	k8xC
kapitál	kapitál	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
trhu	trh	k1gInSc6
výrobních	výrobní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analýza	analýza	k1gFnSc1
chování	chování	k1gNnSc2
jednotlivců	jednotlivec	k1gMnPc2
je	být	k5eAaImIp3nS
popsána	popsat	k5eAaPmNgFnS
v	v	k7c6
teorii	teorie	k1gFnSc6
spotřebitele	spotřebitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Firmy	firma	k1gFnPc1
naopak	naopak	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
poptávky	poptávka	k1gFnSc2
jednotlivců	jednotlivec	k1gMnPc2
nabízejí	nabízet	k5eAaImIp3nP
produkt	produkt	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
jehož	jehož	k3xOyRp3gFnSc4
výrobu	výroba	k1gFnSc4
poptávají	poptávat	k5eAaImIp3nP
služby	služba	k1gFnPc1
výrobních	výrobní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
normy	norma	k1gFnPc4
pro	pro	k7c4
fungování	fungování	k1gNnSc4
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podstata	podstata	k1gFnSc1
mikroekonomie	mikroekonomie	k1gFnSc2
</s>
<s>
Studium	studium	k1gNnSc1
mikroekonomie	mikroekonomie	k1gFnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
několik	několik	k4yIc1
„	„	k?
<g/>
klíčových	klíčový	k2eAgMnPc2d1
<g/>
“	“	k?
oblastí	oblast	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
Poptávka	poptávka	k1gFnSc1
<g/>
,	,	kIx,
nabídka	nabídka	k1gFnSc1
a	a	k8xC
rovnováha	rovnováha	k1gFnSc1
</s>
<s>
Nabídka	nabídka	k1gFnSc1
a	a	k8xC
poptávka	poptávka	k1gFnSc1
je	být	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc4d1
model	model	k1gInSc4
odhadu	odhad	k1gInSc2
ceny	cena	k1gFnSc2
na	na	k7c6
velmi	velmi	k6eAd1
konkurenčním	konkurenční	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospívá	dospívat	k5eAaImIp3nS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
konkurenčním	konkurenční	k2eAgInSc6d1
trhu	trh	k1gInSc6
bez	bez	k7c2
externalit	externalita	k1gFnPc2
<g/>
,	,	kIx,
jednotkových	jednotkový	k2eAgFnPc2d1
daní	daň	k1gFnPc2
či	či	k8xC
cenových	cenový	k2eAgFnPc2d1
kontrol	kontrola	k1gFnPc2
je	být	k5eAaImIp3nS
jednotková	jednotkový	k2eAgFnSc1d1
cena	cena	k1gFnSc1
konkrétního	konkrétní	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
se	se	k3xPyFc4
množství	množství	k1gNnSc1
požadované	požadovaný	k2eAgFnSc2d1
spotřebiteli	spotřebitel	k1gMnSc3
rovná	rovnat	k5eAaImIp3nS
množství	množství	k1gNnSc4
dodanému	dodaný	k2eAgMnSc3d1
výrobci	výrobce	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
cena	cena	k1gFnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
stabilní	stabilní	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
rovnováhu	rovnováha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
elasticity	elasticita	k1gFnSc2
</s>
<s>
Elasticita	elasticita	k1gFnSc1
je	být	k5eAaImIp3nS
měření	měření	k1gNnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
citlivá	citlivý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
ekonomická	ekonomický	k2eAgFnSc1d1
proměnná	proměnná	k1gFnSc1
na	na	k7c4
změnu	změna	k1gFnSc4
jiné	jiný	k2eAgFnSc2d1
proměnné	proměnná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elasticita	elasticita	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
kvantifikována	kvantifikovat	k5eAaBmNgFnS
jako	jako	k8xS,k8xC
poměr	poměr	k1gInSc1
změny	změna	k1gFnSc2
jedné	jeden	k4xCgFnSc2
proměnné	proměnná	k1gFnSc2
ke	k	k7c3
změně	změna	k1gFnSc3
jiné	jiný	k2eAgFnSc2d1
proměnné	proměnná	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
pozdější	pozdní	k2eAgFnSc1d2
proměnná	proměnná	k1gFnSc1
má	mít	k5eAaImIp3nS
příčinný	příčinný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
první	první	k4xOgFnSc4
proměnnou	proměnná	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
měření	měření	k1gNnSc4
odezvy	odezva	k1gFnSc2
proměnné	proměnná	k1gFnSc2
nebo	nebo	k8xC
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ji	on	k3xPp3gFnSc4
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ke	k	k7c3
změnám	změna	k1gFnPc3
v	v	k7c6
kauzativních	kauzativní	k2eAgFnPc6d1
proměnných	proměnný	k2eAgFnPc2d1
bezjednotkovými	bezjednotkův	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
často	často	k6eAd1
používané	používaný	k2eAgFnPc4d1
elasticity	elasticita	k1gFnPc4
patří	patřit	k5eAaImIp3nS
cenová	cenový	k2eAgFnSc1d1
elasticita	elasticita	k1gFnSc1
poptávky	poptávka	k1gFnSc2
<g/>
,	,	kIx,
cenová	cenový	k2eAgFnSc1d1
elasticita	elasticita	k1gFnSc1
nabídky	nabídka	k1gFnSc2
<g/>
,	,	kIx,
příjmová	příjmový	k2eAgFnSc1d1
elasticita	elasticita	k1gFnSc1
poptávky	poptávka	k1gFnSc2
<g/>
,	,	kIx,
elasticita	elasticita	k1gFnSc1
substituce	substituce	k1gFnSc1
nebo	nebo	k8xC
konstantní	konstantní	k2eAgFnSc1d1
elasticita	elasticita	k1gFnSc1
substituce	substituce	k1gFnSc1
mezi	mezi	k7c7
výrobními	výrobní	k2eAgInPc7d1
faktory	faktor	k1gInPc7
a	a	k8xC
elasticita	elasticita	k1gFnSc1
intertemporální	intertemporální	k2eAgFnSc2d1
substituce	substituce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
požadavků	požadavek	k1gInPc2
spotřebitele	spotřebitel	k1gMnSc2
</s>
<s>
Teorie	teorie	k1gFnSc1
spotřebitelské	spotřebitelský	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
spojuje	spojovat	k5eAaImIp3nS
preference	preference	k1gFnSc1
spotřeby	spotřeba	k1gFnSc2
zboží	zboží	k1gNnSc2
a	a	k8xC
služeb	služba	k1gFnPc2
se	s	k7c7
spotřebními	spotřební	k2eAgInPc7d1
výdaji	výdaj	k1gInPc7
<g/>
;	;	kIx,
tento	tento	k3xDgInSc4
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
preferencemi	preference	k1gFnPc7
a	a	k8xC
výdaji	výdaj	k1gInPc7
na	na	k7c4
spotřebu	spotřeba	k1gFnSc4
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
používá	používat	k5eAaImIp3nS
k	k	k7c3
přiřazení	přiřazení	k1gNnSc3
preferencí	preference	k1gFnPc2
ke	k	k7c3
křivkám	křivka	k1gFnPc3
spotřebitelské	spotřebitelský	k2eAgFnSc2d1
poptávky	poptávka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vazba	vazba	k1gFnSc1
mezi	mezi	k7c7
osobními	osobní	k2eAgFnPc7d1
preferencemi	preference	k1gFnPc7
<g/>
,	,	kIx,
spotřebou	spotřeba	k1gFnSc7
a	a	k8xC
křivkou	křivka	k1gFnSc7
poptávky	poptávka	k1gFnSc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejvíce	hodně	k6eAd3,k6eAd1
studovaných	studovaný	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
analyzovat	analyzovat	k5eAaImF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
mohou	moct	k5eAaImIp3nP
spotřebitelé	spotřebitel	k1gMnPc1
dosáhnout	dosáhnout	k5eAaPmF
rovnováhy	rovnováha	k1gFnPc4
mezi	mezi	k7c7
preferencemi	preference	k1gFnPc7
a	a	k8xC
výdaji	výdaj	k1gInPc7
maximalizací	maximalizace	k1gFnPc2
užitku	užitek	k1gInSc2
podléhajícího	podléhající	k2eAgInSc2d1
omezením	omezení	k1gNnSc7
spotřebitelského	spotřebitelský	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
produkce	produkce	k1gFnSc2
</s>
<s>
Teorie	teorie	k1gFnSc1
produkce	produkce	k1gFnSc2
je	být	k5eAaImIp3nS
studium	studium	k1gNnSc4
výroby	výroba	k1gFnSc2
nebo	nebo	k8xC
ekonomického	ekonomický	k2eAgInSc2d1
procesu	proces	k1gInSc2
přeměny	přeměna	k1gFnSc2
vstupů	vstup	k1gInPc2
na	na	k7c4
výstupy	výstup	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Produkce	produkce	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
zdroje	zdroj	k1gInPc4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
zboží	zboží	k1gNnSc2
nebo	nebo	k8xC
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
k	k	k7c3
použití	použití	k1gNnSc3
<g/>
,	,	kIx,
darování	darování	k1gNnSc3
v	v	k7c6
dárkové	dárkový	k2eAgFnSc6d1
ekonomice	ekonomika	k1gFnSc6
nebo	nebo	k8xC
k	k	k7c3
výměně	výměna	k1gFnSc3
v	v	k7c6
tržní	tržní	k2eAgFnSc6d1
ekonomice	ekonomika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
skladování	skladování	k1gNnSc4
<g/>
,	,	kIx,
přepravu	přeprava	k1gFnSc4
a	a	k8xC
balení	balení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
ekonomové	ekonom	k1gMnPc1
definují	definovat	k5eAaBmIp3nP
výrobu	výroba	k1gFnSc4
široce	široko	k6eAd1
jako	jako	k8xC,k8xS
veškerou	veškerý	k3xTgFnSc4
jinou	jiný	k2eAgFnSc4d1
hospodářskou	hospodářský	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
než	než	k8xS
spotřebu	spotřeba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vidí	vidět	k5eAaImIp3nS
každou	každý	k3xTgFnSc4
komerční	komerční	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
kromě	kromě	k7c2
konečného	konečný	k2eAgInSc2d1
nákupu	nákup	k1gInSc2
jako	jako	k8xS,k8xC
nějakou	nějaký	k3yIgFnSc4
formu	forma	k1gFnSc4
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
</s>
<s>
Teorie	teorie	k1gFnSc1
výrobních	výrobní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
hodnoty	hodnota	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
cena	cena	k1gFnSc1
předmětu	předmět	k1gInSc2
nebo	nebo	k8xC
podmínky	podmínka	k1gFnSc2
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
součtem	součet	k1gInSc7
nákladů	náklad	k1gInPc2
na	na	k7c4
zdroje	zdroj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
použity	použít	k5eAaPmNgInP
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
výrobu	výroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc7
mohou	moct	k5eAaImIp3nP
zahrnovat	zahrnovat	k5eAaImF
kterýkoli	kterýkoli	k3yIgInSc4
z	z	k7c2
výrobních	výrobní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
:	:	kIx,
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
kapitál	kapitál	k1gInSc1
<g/>
,	,	kIx,
půda	půda	k1gFnSc1
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
technologii	technologie	k1gFnSc6
lze	lze	k6eAd1
pohlížet	pohlížet	k5eAaImF
buď	buď	k8xC
jako	jako	k9
na	na	k7c4
formu	forma	k1gFnSc4
fixního	fixní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
Závod	závod	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
oběžného	oběžný	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
Meziprodukty	meziprodukt	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
matematickém	matematický	k2eAgInSc6d1
modelu	model	k1gInSc6
výrobních	výrobní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
se	se	k3xPyFc4
krátkodobé	krátkodobý	k2eAgInPc1d1
celkové	celkový	k2eAgInPc1d1
náklady	náklad	k1gInPc1
rovnají	rovnat	k5eAaImIp3nP
fixním	fixní	k2eAgInPc3d1
nákladům	náklad	k1gInPc3
plus	plus	k6eAd1
celkové	celkový	k2eAgInPc4d1
variabilní	variabilní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fixní	fixní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
k	k	k7c3
nákladům	náklad	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
vzniknou	vzniknout	k5eAaPmIp3nP
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
kolik	kolik	k9
firma	firma	k1gFnSc1
produkuje	produkovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměnná	proměnný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
funkcí	funkce	k1gFnSc7
množství	množství	k1gNnSc2
vyráběného	vyráběný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c4
příležitost	příležitost	k1gFnSc4
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1
myšlenka	myšlenka	k1gFnSc1
nákladů	náklad	k1gInPc2
na	na	k7c6
příležitosti	příležitost	k1gFnSc6
je	být	k5eAaImIp3nS
úzce	úzko	k6eAd1
spjata	spjat	k2eAgFnSc1d1
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
časových	časový	k2eAgNnPc2d1
omezení	omezení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
může	moct	k5eAaImIp3nS
dělat	dělat	k5eAaImF
jen	jen	k9
jednu	jeden	k4xCgFnSc4
věc	věc	k1gFnSc4
najednou	najednou	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nevyhnutelně	vyhnutelně	k6eNd1
vždy	vždy	k6eAd1
vzdává	vzdávat	k5eAaImIp3nS
jiných	jiný	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příležitostná	příležitostný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
jakékoli	jakýkoli	k3yIgFnSc2
činnosti	činnost	k1gFnSc2
je	být	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
další	další	k2eAgFnSc2d1
nejlepší	dobrý	k2eAgFnSc2d3
alternativy	alternativa	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
člověk	člověk	k1gMnSc1
možná	možná	k9
místo	místo	k7c2
toho	ten	k3xDgNnSc2
udělal	udělat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
příležitosti	příležitost	k1gFnPc4
závisí	záviset	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
hodnotě	hodnota	k1gFnSc6
další	další	k2eAgFnSc2d1
nejlepší	dobrý	k2eAgFnSc2d3
alternativy	alternativa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezáleží	záležet	k5eNaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
pět	pět	k4xCc4
alternativ	alternativa	k1gFnPc2
nebo	nebo	k8xC
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Náklady	náklad	k1gInPc1
na	na	k7c6
příležitosti	příležitost	k1gFnSc6
mohou	moct	k5eAaImIp3nP
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
něco	něco	k3yInSc4
neudělat	udělat	k5eNaPmF
a	a	k8xC
kdy	kdy	k6eAd1
něco	něco	k3yInSc4
udělat	udělat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
rád	rád	k6eAd1
vafle	vafle	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xS,k8xC
čokoláda	čokoláda	k1gFnSc1
ještě	ještě	k6eAd1
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
někdo	někdo	k3yInSc1
nabídne	nabídnout	k5eAaPmIp3nS
pouze	pouze	k6eAd1
oplatky	oplatka	k1gFnPc4
<g/>
,	,	kIx,
vezme	vzít	k5eAaPmIp3nS
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
kdyby	kdyby	kYmCp3nP
nabídli	nabídnout	k5eAaPmAgMnP
vafle	vafle	k1gFnPc4
nebo	nebo	k8xC
čokoládu	čokoláda	k1gFnSc4
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
by	by	kYmCp3nS
si	se	k3xPyFc3
čokoládu	čokoláda	k1gFnSc4
vzal	vzít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
příležitost	příležitost	k1gFnSc4
jíst	jíst	k5eAaImF
vafle	vafle	k1gFnPc1
obětují	obětovat	k5eAaBmIp3nP
šanci	šance	k1gFnSc4
jíst	jíst	k5eAaImF
čokoládu	čokoláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
cena	cena	k1gFnSc1
za	za	k7c4
konzumaci	konzumace	k1gFnSc4
čokolády	čokoláda	k1gFnSc2
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
výhoda	výhoda	k1gFnSc1
konzumace	konzumace	k1gFnSc2
vaflí	vafle	k1gFnPc2
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
smysl	smysl	k1gInSc4
vybrat	vybrat	k5eAaPmF
si	se	k3xPyFc3
vafle	vafle	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmě	samozřejmě	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
někdo	někdo	k3yInSc1
vybere	vybrat	k5eAaPmIp3nS
čokoládu	čokoláda	k1gFnSc4
<g/>
,	,	kIx,
stále	stále	k6eAd1
čelí	čelit	k5eAaImIp3nS
příležitostným	příležitostný	k2eAgInPc3d1
nákladům	náklad	k1gInPc3
vzdání	vzdání	k1gNnSc2
se	se	k3xPyFc4
oplatek	oplatek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
ochoten	ochoten	k2eAgMnSc1d1
to	ten	k3xDgNnSc4
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
náklady	náklad	k1gInPc1
na	na	k7c4
příležitostné	příležitostný	k2eAgInPc4d1
oplatky	oplatek	k1gInPc4
jsou	být	k5eAaImIp3nP
nižší	nízký	k2eAgInPc1d2
než	než	k8xS
přínosy	přínos	k1gInPc1
čokolády	čokoláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
příležitosti	příležitost	k1gFnPc4
jsou	být	k5eAaImIp3nP
nevyhnutelným	vyhnutelný	k2eNgNnSc7d1
omezením	omezení	k1gNnSc7
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
člověk	člověk	k1gMnSc1
musí	muset	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgMnSc1d3
<g/>
,	,	kIx,
a	a	k8xC
vzdát	vzdát	k5eAaPmF
se	se	k3xPyFc4
další	další	k2eAgFnPc1d1
nejlepší	dobrý	k2eAgFnPc1d3
alternativy	alternativa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
trhu	trh	k1gInSc2
</s>
<s>
Struktura	struktura	k1gFnSc1
trhu	trh	k1gInSc2
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
funkce	funkce	k1gFnPc4
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
počtu	počet	k1gInSc2
firem	firma	k1gFnPc2
na	na	k7c6
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
distribuce	distribuce	k1gFnSc1
trhu	trh	k1gInSc6
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
mezi	mezi	k7c4
firmy	firma	k1gFnPc4
<g/>
,	,	kIx,
uniformitu	uniformita	k1gFnSc4
produktů	produkt	k1gInPc2
napříč	napříč	k7c7
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
snadné	snadný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
firmy	firma	k1gFnPc4
vstoupit	vstoupit	k5eAaPmF
na	na	k7c4
trh	trh	k1gInSc4
a	a	k8xC
utváření	utváření	k1gNnSc4
tržní	tržní	k2eAgFnSc2d1
konkurence	konkurence	k1gFnSc2
.	.	kIx.
</s>
<s desamb="1">
Struktura	struktura	k1gFnSc1
trhu	trh	k1gInSc2
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
několik	několik	k4yIc4
typů	typ	k1gInPc2
interakce	interakce	k1gFnSc2
tržních	tržní	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgFnPc4d1
formy	forma	k1gFnPc4
trhu	trh	k1gInSc2
jsou	být	k5eAaImIp3nP
funkcí	funkce	k1gFnSc7
kapitalismu	kapitalismus	k1gInSc2
a	a	k8xC
tržního	tržní	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
obhájci	obhájce	k1gMnPc1
státního	státní	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
<g/>
,	,	kIx,
často	často	k6eAd1
kritizujícími	kritizující	k2eAgInPc7d1
trhy	trh	k1gInPc7
a	a	k8xC
zaměřené	zaměřený	k2eAgFnPc1d1
na	na	k7c4
nahrazení	nahrazení	k1gNnSc4
trhů	trh	k1gInPc2
s	s	k7c7
různými	různý	k2eAgInPc7d1
stupni	stupeň	k1gInPc7
přímého	přímý	k2eAgNnSc2d1
vládního	vládní	k2eAgNnSc2d1
ekonomického	ekonomický	k2eAgNnSc2d1
plánování	plánování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Konkurence	konkurence	k1gFnSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
regulační	regulační	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
pro	pro	k7c4
tržní	tržní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
s	s	k7c7
vládou	vláda	k1gFnSc7
poskytuje	poskytovat	k5eAaImIp3nS
regulaci	regulace	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
nelze	lze	k6eNd1
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
trh	trh	k1gInSc1
bude	být	k5eAaImBp3nS
regulovat	regulovat	k5eAaImF
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
příkladů	příklad	k1gInPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
stavební	stavební	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
v	v	k7c6
případě	případ	k1gInSc6
čistě	čistě	k6eAd1
konkurenčního	konkurenční	k2eAgInSc2d1
regulovaného	regulovaný	k2eAgInSc2d1
tržního	tržní	k2eAgInSc2d1
systému	systém	k1gInSc2
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
vyústit	vyústit	k5eAaPmF
v	v	k7c4
několik	několik	k4yIc4
hrozných	hrozný	k2eAgInPc6d1
zraněních	zranění	k1gNnPc6
nebo	nebo	k8xC
smrtí	smrtit	k5eAaImIp3nP
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
společnosti	společnost	k1gFnPc1
začnou	začít	k5eAaPmIp3nP
zlepšovat	zlepšovat	k5eAaImF
strukturální	strukturální	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
spotřebitel	spotřebitel	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nejdříve	dříve	k6eAd3
znepokojen	znepokojit	k5eAaPmNgMnS
nebo	nebo	k8xC
vědom	vědom	k2eAgMnSc1d1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
začínají	začínat	k5eAaImIp3nP
vyvíjet	vyvíjet	k5eAaImF
tlak	tlak	k1gInSc4
na	na	k7c4
společnosti	společnost	k1gFnPc4
a	a	k8xC
společnosti	společnost	k1gFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
motivovány	motivován	k2eAgFnPc1d1
neposkytovat	poskytovat	k5eNaImF
řádné	řádný	k2eAgNnSc4d1
bezpečnostní	bezpečnostní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
tomu	ten	k3xDgInSc3
na	na	k7c4
kolik	kolik	k4yRc4,k4yQc4,k4yIc4
by	by	kYmCp3nP
to	ten	k3xDgNnSc1
snížilo	snížit	k5eAaPmAgNnS
jejich	jejich	k3xOp3gInPc4
zisky	zisk	k1gInPc4
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Koncept	koncept	k1gInSc1
„	„	k?
<g/>
tržního	tržní	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
rozdílný	rozdílný	k2eAgInSc1d1
od	od	k7c2
konceptu	koncept	k1gInSc2
„	„	k?
<g/>
struktury	struktura	k1gFnPc4
trhu	trh	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
za	za	k7c4
zmínku	zmínka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
hned	hned	k6eAd1
několik	několik	k4yIc1
tržních	tržní	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
</s>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
je	být	k5eAaImIp3nS
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
několik	několik	k4yIc1
malých	malý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
produkujících	produkující	k2eAgInPc2d1
určitý	určitý	k2eAgInSc4d1
produkt	produkt	k1gInSc4
<g/>
,	,	kIx,
soutěží	soutěžit	k5eAaImIp3nP
proti	proti	k7c3
sobě	se	k3xPyFc3
v	v	k7c6
daném	daný	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonalá	dokonalý	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
vede	vést	k5eAaImIp3nS
firmy	firma	k1gFnPc4
k	k	k7c3
produkci	produkce	k1gFnSc3
sociálně	sociálně	k6eAd1
optimální	optimální	k2eAgFnSc4d1
výstupní	výstupní	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
při	při	k7c6
minimálních	minimální	k2eAgInPc6d1
možných	možný	k2eAgInPc6d1
nákladech	náklad	k1gInPc6
na	na	k7c4
jednotku	jednotka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firmy	firma	k1gFnSc2
v	v	k7c6
dokonalé	dokonalý	k2eAgFnSc6d1
konkurenci	konkurence	k1gFnSc6
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
cenový	cenový	k2eAgInSc1d1
příjemci	příjemce	k1gMnSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
nemají	mít	k5eNaImIp3nP
dostatek	dostatek	k1gInSc4
tržní	tržní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
k	k	k7c3
ziskovému	ziskový	k2eAgNnSc3d1
zvýšení	zvýšení	k1gNnSc3
cen	cena	k1gFnPc2
jejich	jejich	k3xOp3gInPc2
produktů	produkt	k1gInPc2
nebo	nebo	k8xC
služeb	služba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrým	dobrý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
digitální	digitální	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
,	,	kIx,
například	například	k6eAd1
eBay	eBa	k2eAgInPc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
mnoho	mnoho	k4c1
různých	různý	k2eAgMnPc2d1
prodejců	prodejce	k1gMnPc2
prodává	prodávat	k5eAaImIp3nS
podobné	podobný	k2eAgInPc4d1
produkty	produkt	k1gInPc4
mnoho	mnoho	k6eAd1
rozdílným	rozdílný	k2eAgMnPc3d1
kupujícím	kupující	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spotřebitel	spotřebitel	k1gMnSc1
v	v	k7c6
dokonalé	dokonalý	k2eAgFnSc6d1
konkurenci	konkurence	k1gFnSc6
na	na	k7c6
trhu	trh	k1gInSc6
má	mít	k5eAaImIp3nS
dokonalé	dokonalý	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
o	o	k7c6
produktech	produkt	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
prodávány	prodávat	k5eAaImNgInP
na	na	k7c6
tomto	tento	k3xDgInSc6
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Oblasti	oblast	k1gFnPc4
zkoumání	zkoumání	k1gNnSc2
mikroekonomie	mikroekonomie	k1gFnSc2
</s>
<s>
Teorie	teorie	k1gFnSc1
spotřebitele	spotřebitel	k1gMnSc2
</s>
<s>
Teorie	teorie	k1gFnSc1
firmy	firma	k1gFnSc2
</s>
<s>
Trh	trh	k1gInSc1
výrobních	výrobní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1
efektivnost	efektivnost	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MACÁKOVÁ	Macáková	k1gFnSc1
<g/>
,	,	kIx,
Libuše	Libuše	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikroekonomie	mikroekonomie	k1gFnSc1
<g/>
:	:	kIx,
základní	základní	k2eAgInSc1d1
kurs	kurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slaný	Slaný	k1gInSc1
<g/>
:	:	kIx,
Melandrium	Melandrium	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86175	#num#	k4
<g/>
-	-	kIx~
<g/>
56	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
261	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4039225-9	4039225-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1219	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MARCHANT	MARCHANT	kA
<g/>
,	,	kIx,
MARY	Mary	k1gFnPc1
A.	A.	kA
<g/>
;	;	kIx,
SNELL	SNELL	kA
<g/>
,	,	kIx,
WILLIAM	WILLIAM	kA
M.	M.	kA
Macroeconomics	Macroeconomics	k1gInSc1
and	and	k?
International	International	k1gFnSc2
Policy	Polica	k1gMnSc2
Terms	Termsa	k1gFnPc2
<g/>
.	.	kIx.
dx	dx	k?
<g/>
.	.	kIx.
<g/>
doi	doi	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Kentucky	Kentucka	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Economics	Economicsa	k1gFnPc2
Glossary	Glossara	k1gFnSc2
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-02-04	2008-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Social	Social	k1gInSc1
Studies	Studies	k1gInSc1
Standards	Standards	k1gInSc4
Glossary	Glossara	k1gFnSc2
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
Public	publicum	k1gNnPc2
Education	Education	k1gInSc4
Department	department	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Glossary	Glossar	k1gInPc1
S	s	k7c7
<g/>
1	#num#	k4
<g/>
:	:	kIx,
Glossary	Glossara	k1gFnSc2
S	s	k7c7
<g/>
1	#num#	k4
<g/>
.	.	kIx.
dx	dx	k?
<g/>
.	.	kIx.
<g/>
doi	doi	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SICKLES	SICKLES	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgMnSc1d1
C.	C.	kA
<g/>
;	;	kIx,
ZELENYUK	ZELENYUK	kA
<g/>
,	,	kIx,
Valentin	Valentin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Measurement	Measurement	k1gMnSc1
of	of	k?
Productivity	Productivita	k1gFnSc2
and	and	k?
Efficiency	Efficienc	k2eAgFnPc1d1
<g/>
:	:	kIx,
Theory	Theora	k1gFnPc1
and	and	k?
Practice	Practice	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
107	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3616	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
139	#num#	k4
<g/>
-	-	kIx~
<g/>
56598	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
9781139565981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
10.101	10.101	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
9781139565981	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HASHIMZADE	HASHIMZADE	kA
<g/>
,	,	kIx,
Nigar	Nigar	k1gMnSc1
<g/>
;	;	kIx,
MYLES	MYLES	kA
<g/>
,	,	kIx,
Gareth	Gareth	k1gMnSc1
<g/>
;	;	kIx,
BLACK	BLACK	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Market	market	k1gInSc1
structure	structur	k1gMnSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780198759430	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
acref	acref	k1gInSc1
<g/>
/	/	kIx~
<g/>
9780198759430.001	9780198759430.001	k4
<g/>
.0001	.0001	k4
<g/>
/	/	kIx~
<g/>
acref-	acref-	k?
<g/>
9780198759430	#num#	k4
<g/>
-e-	-e-	k?
<g/>
1937	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mikroekonomie	mikroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
témata	téma	k1gNnPc4
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
•	•	k?
Nedokonalá	dokonalý	k2eNgFnSc1d1
konkurence	konkurence	k1gFnSc1
•	•	k?
Monopolistická	monopolistický	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
Náklady	náklad	k1gInPc1
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
•	•	k?
Mezní	mezní	k2eAgFnPc1d1
•	•	k?
Obětované	obětovaný	k2eAgFnPc1d1
příležitosti	příležitost	k1gFnPc1
•	•	k?
Sociální	sociální	k2eAgInSc4d1
•	•	k?
Utopené	utopený	k2eAgNnSc1d1
•	•	k?
Transakční	transakční	k2eAgInSc1d1
•	•	k?
Fixní	fixní	k2eAgInSc1d1
•	•	k?
Variabilní	variabilní	k2eAgInSc4d1
•	•	k?
Celkové	celkový	k2eAgNnSc1d1
Struktura	struktura	k1gFnSc1
trhu	trh	k1gInSc6
</s>
<s>
Monopol	monopol	k1gInSc1
•	•	k?
Monopson	Monopson	k1gInSc1
•	•	k?
Oligopol	Oligopol	k1gInSc1
(	(	kIx(
<g/>
duopol	duopol	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Oligopson	Oligopson	k1gInSc1
•	•	k?
Přirozený	přirozený	k2eAgInSc1d1
monopol	monopol	k1gInSc1
•	•	k?
Koncern	koncern	k1gInSc1
(	(	kIx(
<g/>
holding	holding	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Trust	trust	k1gInSc1
•	•	k?
Kartel	kartel	k1gInSc1
•	•	k?
Syndikát	syndikát	k1gInSc1
•	•	k?
Konsorcium	konsorcium	k1gNnSc1
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Agregace	agregace	k1gFnSc1
•	•	k?
Rozpočet	rozpočet	k1gInSc1
•	•	k?
Teorie	teorie	k1gFnSc2
spotřebitele	spotřebitel	k1gMnSc2
•	•	k?
Konvexnost	konvexnost	k1gFnSc4
•	•	k?
Nekonvexnost	Nekonvexnost	k1gFnSc1
•	•	k?
Analýza	analýza	k1gFnSc1
nákladů	náklad	k1gInPc2
a	a	k8xC
přínosů	přínos	k1gInPc2
•	•	k?
Náklady	náklad	k1gInPc1
mrtvé	mrtvý	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
•	•	k?
Distribuce	distribuce	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
z	z	k7c2
rozsahu	rozsah	k1gInSc2
•	•	k?
Úspory	úspora	k1gFnSc2
z	z	k7c2
prostoru	prostor	k1gInSc2
•	•	k?
Elasticita	elasticita	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
•	•	k?
Obchod	obchod	k1gInSc1
•	•	k?
Externalita	externalita	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
firmy	firma	k1gFnSc2
•	•	k?
Statek	statek	k1gInSc1
•	•	k?
Služba	služba	k1gFnSc1
•	•	k?
Rodinná	rodinný	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Křivka	křivka	k1gFnSc1
příjmu	příjem	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
spotřeby	spotřeba	k1gFnSc2
•	•	k?
Informace	informace	k1gFnPc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
•	•	k?
Indiferenční	Indiferenční	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Mezičasová	Mezičasový	k2eAgFnSc1d1
volba	volba	k1gFnSc1
•	•	k?
Trh	trh	k1gInSc1
(	(	kIx(
<g/>
ekonomie	ekonomie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Selhání	selhání	k1gNnSc1
trhu	trh	k1gInSc2
•	•	k?
Paretovo	Paretův	k2eAgNnSc1d1
optimum	optimum	k1gNnSc1
•	•	k?
Preference	preference	k1gFnSc1
•	•	k?
Cena	cena	k1gFnSc1
•	•	k?
Produkce	produkce	k1gFnSc1
•	•	k?
Zisk	zisk	k1gInSc1
•	•	k?
Veřejný	veřejný	k2eAgInSc1d1
statek	statek	k1gInSc1
•	•	k?
Přídělový	přídělový	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Renta	renta	k1gFnSc1
•	•	k?
Averze	averze	k1gFnSc2
k	k	k7c3
riziku	riziko	k1gNnSc3
•	•	k?
Vzácnost	vzácnost	k1gFnSc1
•	•	k?
Nedostatek	nedostatek	k1gInSc1
•	•	k?
Substitut	substitut	k1gInSc1
•	•	k?
Substitutuční	Substitutuční	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Přebytek	přebytek	k1gInSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
volba	volba	k1gFnSc1
•	•	k?
Nabídka	nabídka	k1gFnSc1
a	a	k8xC
poptávka	poptávka	k1gFnSc1
•	•	k?
Nejistota	nejistota	k1gFnSc1
•	•	k?
Užitek	užitek	k1gInSc1
<g/>
(	(	kIx(
Očekávaný	očekávaný	k2eAgInSc1d1
<g/>
,	,	kIx,
Mezní	mezní	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Mzda	mzda	k1gFnSc1
</s>
<s>
Vedlejší	vedlejší	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Behaviorální	behaviorální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
rozhodování	rozhodování	k1gNnSc2
•	•	k?
Ekonometrie	Ekonometrie	k1gFnSc2
•	•	k?
Evoluční	evoluční	k2eAgInSc4d1
přístup	přístup	k1gInSc4
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
•	•	k?
Eperimentální	Eperimentální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
her	hra	k1gFnPc2
•	•	k?
Institucionální	institucionální	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
práce	práce	k1gFnSc2
•	•	k?
Právo	právo	k1gNnSc1
•	•	k?
Operační	operační	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
•	•	k?
Optimalizace	optimalizace	k1gFnSc1
•	•	k?
Blahobyt	blahobyt	k1gInSc1
•	•	k?
Regulace	regulace	k1gFnSc1
cen	cena	k1gFnPc2
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
Robinsona	Robinson	k1gMnSc2
Crusoe	Crusoe	k1gNnSc2
•	•	k?
Engelova	Engelův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Hranice	hranice	k1gFnSc2
produkčních	produkční	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
•	•	k?
Zákon	zákon	k1gInSc1
klesajících	klesající	k2eAgInPc2d1
výnosů	výnos	k1gInPc2
•	•	k?
Udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Makroekonomie	makroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Makroekonomie	makroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
koncepty	koncept	k1gInPc4
</s>
<s>
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Agregátní	agregátní	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
•	•	k?
Deflace	deflace	k1gFnSc2
•	•	k?
Poptávkový	poptávkový	k2eAgInSc1d1
šok	šok	k1gInSc1
•	•	k?
Nabídkový	nabídkový	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
•	•	k?
Dezinflace	Dezinflace	k1gFnSc2
•	•	k?
Efektivní	efektivní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Očekávání	očekávání	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Adaptivní	adaptivní	k2eAgFnPc1d1
•	•	k?
Racionální	racionální	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
•	•	k?
Inflace	inflace	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Tahem	tah	k1gInSc7
poptávky	poptávka	k1gFnSc2
•	•	k?
Nákladová	nákladový	k2eAgFnSc1d1
)	)	kIx)
•	•	k?
Úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Investice	investice	k1gFnSc1
•	•	k?
Past	pasta	k1gFnPc2
na	na	k7c4
likviditu	likvidita	k1gFnSc4
•	•	k?
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
<g/>
(	(	kIx(
<g/>
HDP	HDP	kA
•	•	k?
HNP	HNP	kA
•	•	k?
ČND	ČND	kA
<g/>
)	)	kIx)
•	•	k?
Microfoundations	Microfoundations	k1gInSc4
•	•	k?
Peníze	peníz	k1gInPc4
<g/>
(	(	kIx(
<g/>
Endogenní	endogenní	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Tvorba	tvorba	k1gFnSc1
peněz	peníze	k1gInPc2
•	•	k?
Poptávka	poptávka	k1gFnSc1
po	po	k7c6
penězích	peníze	k1gInPc6
•	•	k?
Preference	preference	k1gFnPc1
likvidity	likvidita	k1gFnSc2
•	•	k?
Peněžní	peněžní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
•	•	k?
Národní	národní	k2eAgInPc4d1
účty	účet	k1gInPc4
<g/>
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
národních	národní	k2eAgInPc2d1
účtů	účet	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Nominal	Nominal	k1gMnSc1
rigidity	rigidita	k1gFnSc2
•	•	k?
Cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
•	•	k?
Recese	recese	k1gFnSc1
•	•	k?
Shrinkflation	Shrinkflation	k1gInSc1
•	•	k?
Stagflace	Stagflace	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
•	•	k?
Nezaměstnanost	nezaměstnanost	k1gFnSc1
Politiky	politika	k1gFnSc2
</s>
<s>
Fiskální	fiskální	k2eAgFnSc1d1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
•	•	k?
Obchodní	obchodní	k2eAgFnSc1d1
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Modely	model	k1gInPc1
</s>
<s>
IS-LM	IS-LM	k?
•	•	k?
AD	ad	k7c4
<g/>
–	–	k?
<g/>
AS	as	k1gNnSc4
•	•	k?
Keynesiánský	keynesiánský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
•	•	k?
Multiplikátor	multiplikátor	k1gInSc1
•	•	k?
Akcelerátorový	Akcelerátorový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Phillipsova	Phillipsův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Arrow	Arrow	k1gFnSc2
<g/>
–	–	k?
<g/>
Debreu	Debreus	k1gInSc2
•	•	k?
Harrod	Harroda	k1gFnPc2
<g/>
–	–	k?
<g/>
Domar	Domar	k1gMnSc1
•	•	k?
Solow	Solow	k1gMnSc1
<g/>
–	–	k?
<g/>
Swan	Swan	k1gMnSc1
•	•	k?
Ramsey	Ramsea	k1gFnSc2
<g/>
–	–	k?
<g/>
Cass	Cass	k1gInSc1
<g/>
–	–	k?
<g/>
Koopmans	Koopmans	k1gInSc1
•	•	k?
Model	modla	k1gFnPc2
překrývajících	překrývající	k2eAgFnPc2d1
se	se	k3xPyFc4
generací	generace	k1gFnPc2
•	•	k?
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
rovnováhy	rovnováha	k1gFnSc2
•	•	k?
Teorie	teorie	k1gFnSc1
endogenního	endogenní	k2eAgInSc2d1
růstu	růst	k1gInSc2
•	•	k?
Teorie	teorie	k1gFnSc1
shody	shoda	k1gFnSc2
•	•	k?
Mundell	Mundell	k1gInSc1
<g/>
–	–	k?
<g/>
Fleming	Fleming	k1gInSc1
•	•	k?
Model	model	k1gInSc1
překročení	překročení	k1gNnSc2
•	•	k?
NAIRU	NAIRU	kA
Související	související	k2eAgFnSc1d1
</s>
<s>
Ekonometrie	Ekonometrie	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Rozvojová	rozvojový	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
Školy	škola	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
proud	proud	k1gInSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo-Nová	Neo-Nová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
(	(	kIx(
<g/>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
obchdního	obchdní	k2eAgInSc2d1
cykklu	cykkl	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
Heterodox	Heterodox	k1gInSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
<g/>
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Cirkulační	cirkulační	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Tržní	tržní	k2eAgInSc1d1
monetarismus	monetarismus	k1gInSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
makroekonomové	makroekonom	k1gMnPc1
</s>
<s>
François	François	k1gFnSc1
Quesnay	Quesnaa	k1gFnSc2
•	•	k?
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Robert	Robert	k1gMnSc1
Malthus	Malthus	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
•	•	k?
Léon	Léon	k1gMnSc1
Walras	Walras	k1gMnSc1
•	•	k?
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Knapp	Knapp	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Irving	Irving	k1gInSc1
Fisher	Fishra	k1gFnPc2
•	•	k?
Wesley	Weslea	k1gFnSc2
Clair	Clair	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Maynard	Maynard	k1gMnSc1
Keynes	Keynes	k1gMnSc1
•	•	k?
Alvin	Alvin	k1gMnSc1
Hansen	Hansen	k2eAgMnSc1d1
•	•	k?
Michał	Michał	k1gMnSc1
Kalecki	Kaleck	k1gFnSc2
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Myrdal	Myrdal	k1gMnSc1
•	•	k?
Simon	Simon	k1gMnSc1
Kuznets	Kuznetsa	k1gFnPc2
•	•	k?
Joan	Joan	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
Hicks	Hicksa	k1gFnPc2
•	•	k?
Richard	Richard	k1gMnSc1
Stone	ston	k1gInSc5
•	•	k?
Hyman	Hyman	k1gMnSc1
Minsky	minsky	k6eAd1
•	•	k?
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
Klein	Klein	k1gMnSc1
•	•	k?
Edmund	Edmund	k1gMnSc1
Phelps	Phelpsa	k1gFnPc2
•	•	k?
Robert	Robert	k1gMnSc1
Lucas	Lucas	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
•	•	k?
Edward	Edward	k1gMnSc1
C.	C.	kA
Prescott	Prescott	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Diamond	Diamond	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Nordhaus	Nordhaus	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Stiglitz	Stiglitz	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
J.	J.	kA
Sargent	Sargent	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
•	•	k?
Gregory	Gregor	k1gMnPc7
Mankiw	Mankiw	k1gFnPc7
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
•	•	k?
Makroekonomický	makroekonomický	k2eAgInSc4d1
model	model	k1gInSc4
•	•	k?
Seznam	seznam	k1gInSc1
významných	významný	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
•	•	k?
Mikroekonomie	mikroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
