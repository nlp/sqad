<s desamb="1">
Mikroekonomie	mikroekonomie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
poptávkou	poptávka	k1gFnSc7
a	a	k8xC
nabídkou	nabídka	k1gFnSc7
v	v	k7c6
jejich	jejich	k3xOp3gFnPc6
individuálních	individuální	k2eAgFnPc6d1
a	a	k8xC
konkrétních	konkrétní	k2eAgFnPc6d1
podobách	podoba	k1gFnPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
makroekonomie	makroekonomie	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
agregátní	agregátní	k2eAgFnSc7d1
nabídkou	nabídka	k1gFnSc7
a	a	k8xC
agregátní	agregátní	k2eAgFnSc7d1
poptávkou	poptávka	k1gFnSc7
<g/>
,	,	kIx,
tj.	tj.	kA
komplexními	komplexní	k2eAgInPc7d1
trhy	trh	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>