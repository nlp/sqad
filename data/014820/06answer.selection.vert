<s>
Kamaldulská	Kamaldulský	k2eAgFnSc1d1
bible	bible	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
Swaté	Swatý	k2eAgNnSc1d1
Biblia	Biblium	k1gNnPc1
Slowénské	Slowénská	k1gFnSc2
aneb	aneb	k?
Pisma	Pisma	k1gFnSc1
Swatého	Swatý	k2eAgNnSc2d1
Částka	částka	k1gFnSc1
I	I	kA
a	a	k8xC
II	II	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
historicky	historicky	k6eAd1
první	první	k4xOgInSc4
překlad	překlad	k1gInSc4
bible	bible	k1gFnSc2
do	do	k7c2
slovenštiny	slovenština	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
kamaldulská	kamaldulský	k2eAgFnSc1d1
slovenština	slovenština	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>