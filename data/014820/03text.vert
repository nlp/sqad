<s>
Kamaldulská	Kamaldulský	k2eAgFnSc1d1
bible	bible	k1gFnSc1
</s>
<s>
Kamaldulská	Kamaldulský	k2eAgFnSc1d1
bible	bible	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
Swaté	Swatý	k2eAgNnSc1d1
Biblia	Biblium	k1gNnPc1
Slowénské	Slowénská	k1gFnSc2
aneb	aneb	k?
Pisma	Pisma	k1gFnSc1
Swatého	Swatý	k2eAgNnSc2d1
Částka	částka	k1gFnSc1
I	I	kA
a	a	k8xC
II	II	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
historicky	historicky	k6eAd1
první	první	k4xOgInSc4
překlad	překlad	k1gInSc4
bible	bible	k1gFnSc2
do	do	k7c2
slovenštiny	slovenština	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
kamaldulská	kamaldulský	k2eAgFnSc1d1
slovenština	slovenština	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Celý	celý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Swaté	Swatý	k2eAgNnSc1d1
Biblia	Biblium	k1gNnPc1
Slowénské	Slowénská	k1gFnSc2
aneb	aneb	k?
Pisma	Pisma	k1gFnSc1
Swatého	Swatý	k2eAgNnSc2d1
Částka	částka	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
Která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
w	w	k?
sebe	sebe	k3xPyFc4
obsahuge	obsahugat	k5eAaPmIp3nS
Starého	Starého	k2eAgNnSc2d1
Testamenta	Testamento	k1gNnSc2
Zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
Zákona	zákon	k1gInSc2
Historiu	Historium	k1gNnSc3
<g/>
:	:	kIx,
Pět	pět	k4xCc1
knih	kniha	k1gFnPc2
Moyžjssowýcg	Moyžjssowýcga	k1gFnPc2
<g/>
,	,	kIx,
Knihi	Knih	k1gFnPc1
Yózue	Yózue	k1gNnSc1
<g/>
,	,	kIx,
Yudikum	Yudikum	k1gNnSc1
<g/>
,	,	kIx,
Ruth	Ruth	k1gFnSc1
<g/>
,	,	kIx,
Kralowské	Kralowské	k1gNnSc1
<g/>
,	,	kIx,
Paralipomenon	paralipomenon	k1gNnSc1
<g/>
,	,	kIx,
Ezdrassowe	Ezdrassowe	k1gNnSc1
<g/>
,	,	kIx,
Tobiassowe	Tobiassowe	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Yudith	Yuditha	k1gFnPc2
<g/>
,	,	kIx,
Ester	Ester	k1gFnPc2
<g/>
,	,	kIx,
Yob	Yob	k1gFnPc2
<g/>
,	,	kIx,
Žalmi	Žal	k1gFnPc7
Dawidowé	Dawidowá	k1gFnSc2
<g/>
,	,	kIx,
Knihi	Knih	k1gFnSc2
Prislow	Prislow	k1gFnSc2
<g/>
,	,	kIx,
Ekklesyastej	Ekklesyastej	k1gFnSc2
<g/>
,	,	kIx,
Pjsňe	Pjsňe	k1gNnSc2
<g/>
,	,	kIx,
Ssalomúnowé	Ssalomúnowé	k1gNnSc2
<g/>
,	,	kIx,
Knihi	Knihi	k1gNnSc2
Mudrosti	Mudrost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Syrách	Syry	k1gFnPc6
<g/>
;	;	kIx,
Částka	částka	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
Která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c4
sebe	sebe	k3xPyFc4
obsahuge	obsahugat	k5eAaPmIp3nS
Prorokú	Prorokú	k1gFnSc1
<g/>
,	,	kIx,
Knihi	Knihi	k1gNnSc1
Machaběgské	Machaběgské	k1gNnSc1
a	a	k8xC
celý	celý	k2eAgInSc1d1
Testament	testament	k1gInSc1
nowý	nowý	k?
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Biblický	biblický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
prostředí	prostředí	k1gNnSc6
mnichů	mnich	k1gInPc2
kamaldulského	kamaldulský	k2eAgInSc2d1
řádu	řád	k1gInSc2
buď	buď	k8xC
v	v	k7c4
Červenom	Červenom	k1gInSc4
Kláštore	Kláštor	k1gMnSc5
nebo	nebo	k8xC
v	v	k7c6
Nitře	Nitra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
autora	autor	k1gMnSc4
<g/>
,	,	kIx,
či	či	k8xC
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
překladu	překlad	k1gInSc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
mnich	mnich	k1gInSc1
Romuald	Romualda	k1gFnPc2
Hadbávný	Hadbávný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
biblického	biblický	k2eAgInSc2d1
textu	text	k1gInSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
vulgátního	vulgátní	k2eAgInSc2d1
textu	text	k1gInSc2
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
originálním	originální	k2eAgInPc3d1
jazykům	jazyk	k1gInPc3
a	a	k8xC
dřívějším	dřívější	k2eAgInPc3d1
překladům	překlad	k1gInPc3
do	do	k7c2
češtiny	čeština	k1gFnSc2
a	a	k8xC
polštiny	polština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
textu	text	k1gInSc6
používá	používat	k5eAaImIp3nS
výrazy	výraz	k1gInPc4
běžné	běžný	k2eAgFnSc2d1
hovorové	hovorový	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
s	s	k7c7
převažujícími	převažující	k2eAgInPc7d1
západoslovenskými	západoslovenský	k2eAgInPc7d1
prvky	prvek	k1gInPc7
a	a	k8xC
některými	některý	k3yIgNnPc7
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
převzatými	převzatý	k2eAgInPc7d1
z	z	k7c2
tzv.	tzv.	kA
kralické	kralický	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznámkový	poznámkový	k2eAgInSc1d1
aparát	aparát	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
latině	latina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Originál	originál	k1gInSc1
překladu	překlad	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
rokem	rok	k1gInSc7
1756	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochován	dochován	k2eAgInSc4d1
je	být	k5eAaImIp3nS
dvousvazkový	dvousvazkový	k2eAgInSc4d1
přepis	přepis	k1gInSc4
z	z	k7c2
let	léto	k1gNnPc2
1756	#num#	k4
<g/>
-	-	kIx~
<g/>
1760	#num#	k4
<g/>
,	,	kIx,
uložený	uložený	k2eAgInSc1d1
v	v	k7c6
archivu	archiv	k1gInSc6
trnavského	trnavský	k2eAgNnSc2d1
arcibiskupství	arcibiskupství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Začátkem	začátkem	k7c2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
Německu	Německo	k1gNnSc6
pořízena	pořízen	k2eAgNnPc1d1
dvousvazková	dvousvazkový	k2eAgNnPc1d1
faksimile	faksimile	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ĎURICA	ĎURICA	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
,	,	kIx,
SJ	SJ	kA
<g/>
:	:	kIx,
Kamaldulská	Kamaldulský	k2eAgFnSc1d1
Biblia	Biblia	k1gFnSc1
–	–	k?
prvý	prvý	k4xOgInSc4
slovenský	slovenský	k2eAgInSc4d1
preklad	preklad	k1gInSc4
v	v	k7c4
európskom	európskom	k1gInSc4
kontexte	kontext	k1gInSc5
(	(	kIx(
<g/>
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Prezentace	prezentace	k1gFnSc1
Kamaldulské	Kamaldulský	k2eAgFnSc2d1
Bible	bible	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bible	bible	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
