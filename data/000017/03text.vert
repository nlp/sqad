<s>
Dalečín	Dalečín	k1gInSc1	Dalečín
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
stojící	stojící	k2eAgFnSc1d1	stojící
uprostřed	uprostřed	k7c2	uprostřed
obce	obec	k1gFnSc2	obec
Dalečín	Dalečína	k1gFnPc2	Dalečína
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
výběžku	výběžek	k1gInSc6	výběžek
chráněn	chránit	k5eAaImNgInS	chránit
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
řekou	řeka	k1gFnSc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Dalečín	Dalečína	k1gFnPc2	Dalečína
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
označovaného	označovaný	k2eAgNnSc2d1	označované
jako	jako	k8xC	jako
Tolenstein	Tolenstein	k1gInSc1	Tolenstein
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
není	být	k5eNaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doložen	doložen	k2eAgInSc1d1	doložen
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
místopisných	místopisný	k2eAgFnPc6d1	místopisná
pracích	práce	k1gFnPc6	práce
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
kladena	klást	k5eAaImNgFnS	klást
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
Skály	skála	k1gFnSc2	skála
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
rozbořen	rozbořit	k5eAaPmNgInS	rozbořit
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc1	sídlo
lupičů	lupič	k1gMnPc2	lupič
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
obnovili	obnovit	k5eAaPmAgMnP	obnovit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
další	další	k2eAgInPc1d1	další
osudy	osud	k1gInPc1	osud
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
a	a	k8xC	a
zboření	zboření	k1gNnSc1	zboření
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1519	[number]	k4	1519
vojsky	vojsky	k6eAd1	vojsky
moravského	moravský	k2eAgMnSc2d1	moravský
hejtmana	hejtman	k1gMnSc2	hejtman
Artleba	Artleb	k1gMnSc2	Artleb
Vranovského	vranovský	k2eAgMnSc2d1	vranovský
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
se	se	k3xPyFc4	se
neopírá	opírat	k5eNaImIp3nS	opírat
o	o	k7c4	o
žádný	žádný	k3yNgInSc4	žádný
věrohodný	věrohodný	k2eAgInSc4d1	věrohodný
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
nepřesný	přesný	k2eNgInSc4d1	nepřesný
výklad	výklad	k1gInSc4	výklad
barokních	barokní	k2eAgMnPc2d1	barokní
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gFnPc6	jejichž
pracích	práce	k1gFnPc6	práce
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
údaj	údaj	k1gInSc1	údaj
ponejprv	ponejprv	k6eAd1	ponejprv
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
postoupil	postoupit	k5eAaPmAgInS	postoupit
Pavlu	Pavel	k1gMnSc3	Pavel
Katharýnovi	Katharýn	k1gMnSc3	Katharýn
z	z	k7c2	z
Katharu	Kathar	k1gInSc2	Kathar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
pustý	pustý	k2eAgMnSc1d1	pustý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
sousedství	sousedství	k1gNnSc6	sousedství
tehdy	tehdy	k6eAd1	tehdy
vzniká	vznikat	k5eAaImIp3nS	vznikat
nový	nový	k2eAgInSc1d1	nový
dvůr	dvůr	k1gInSc1	dvůr
s	s	k7c7	s
drobnou	drobný	k2eAgFnSc7d1	drobná
tvrzí	tvrz	k1gFnSc7	tvrz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
tradici	tradice	k1gFnSc4	tradice
šlechtického	šlechtický	k2eAgNnSc2d1	šlechtické
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
hradu	hrad	k1gInSc2	hrad
upraveny	upravit	k5eAaPmNgFnP	upravit
jako	jako	k8xS	jako
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
mohutná	mohutný	k2eAgFnSc1d1	mohutná
část	část	k1gFnSc1	část
zdiva	zdivo	k1gNnSc2	zdivo
se	s	k7c7	s
střílnami	střílna	k1gFnPc7	střílna
a	a	k8xC	a
okny	okno	k1gNnPc7	okno
obytných	obytný	k2eAgFnPc2d1	obytná
místností	místnost	k1gFnPc2	místnost
<g/>
,	,	kIx,	,
vybavených	vybavený	k2eAgInPc2d1	vybavený
ve	v	k7c6	v
výklencích	výklenek	k1gInPc6	výklenek
sedátky	sedátko	k1gNnPc7	sedátko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
3	[number]	k4	3
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stavebního	stavební	k2eAgInSc2d1	stavební
slohu	sloh	k1gInSc2	sloh
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
gotika	gotika	k1gFnSc1	gotika
-	-	kIx~	-
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
<g/>
.	.	kIx.	.
</s>
<s>
Dispozicí	dispozice	k1gFnSc7	dispozice
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
plášťové	plášťový	k2eAgInPc4d1	plášťový
hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
hlavně	hlavně	k9	hlavně
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
měl	mít	k5eAaImAgMnS	mít
4	[number]	k4	4
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Obvodovou	obvodový	k2eAgFnSc4d1	obvodová
zeď	zeď	k1gFnSc4	zeď
prolamují	prolamovat	k5eAaImIp3nP	prolamovat
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
tři	tři	k4xCgFnPc1	tři
střílny	střílna	k1gFnPc1	střílna
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
patro	patro	k1gNnSc1	patro
mělo	mít	k5eAaImAgNnS	mít
dvě	dva	k4xCgFnPc4	dva
místnosti	místnost	k1gFnPc4	místnost
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zachovaného	zachovaný	k2eAgNnSc2d1	zachované
torza	torzo	k1gNnSc2	torzo
<g/>
)	)	kIx)	)
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
4	[number]	k4	4
okna	okno	k1gNnSc2	okno
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
prevetu	prevet	k1gInSc2	prevet
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
průchodu	průchod	k1gInSc2	průchod
na	na	k7c4	na
vnější	vnější	k2eAgInSc4d1	vnější
plášť	plášť	k1gInSc4	plášť
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
druhým	druhý	k4xOgNnSc7	druhý
patrem	patro	k1gNnSc7	patro
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
část	část	k1gFnSc1	část
obranného	obranný	k2eAgNnSc2d1	obranné
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
vedl	vést	k5eAaImAgInS	vést
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
parkánu	parkán	k1gInSc6	parkán
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
branami	brána	k1gFnPc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dalečín	Dalečína	k1gFnPc2	Dalečína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
