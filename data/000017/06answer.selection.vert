<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Dalečín	Dalečína	k1gFnPc2	Dalečína
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
označovaného	označovaný	k2eAgNnSc2d1	označované
jako	jako	k8xS	jako
Tolenstein	Tolenstein	k1gInSc1	Tolenstein
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
není	být	k5eNaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doložen	doložen	k2eAgInSc1d1	doložen
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
místopisných	místopisný	k2eAgFnPc6d1	místopisná
pracích	práce	k1gFnPc6	práce
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
<g/>
.	.	kIx.	.
</s>
