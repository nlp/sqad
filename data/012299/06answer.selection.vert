<s>
Lávová	lávový	k2eAgFnSc1d1	lávová
lampa	lampa	k1gFnSc1	lampa
(	(	kIx(	(
<g/>
také	také	k9	také
magma	magma	k1gNnSc1	magma
lampa	lampa	k1gFnSc1	lampa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
svítidla	svítidlo	k1gNnSc2	svítidlo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
dekorace	dekorace	k1gFnPc4	dekorace
<g/>
.	.	kIx.	.
</s>
