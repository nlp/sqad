<p>
<s>
Lávová	lávový	k2eAgFnSc1d1	lávová
lampa	lampa	k1gFnSc1	lampa
(	(	kIx(	(
<g/>
také	také	k9	také
magma	magma	k1gNnSc1	magma
lampa	lampa	k1gFnSc1	lampa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
svítidla	svítidlo	k1gNnSc2	svítidlo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
dekorace	dekorace	k1gFnPc4	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Lampa	lampa	k1gFnSc1	lampa
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
s	s	k7c7	s
různě	různě	k6eAd1	různě
zbarvenou	zbarvený	k2eAgFnSc7d1	zbarvená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Podstavec	podstavec	k1gInSc1	podstavec
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
materiálů	materiál	k1gInPc2	materiál
jako	jako	k8xS	jako
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
plast	plast	k1gInSc1	plast
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
fungování	fungování	k1gNnSc2	fungování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
podstavci	podstavec	k1gInSc6	podstavec
lampy	lampa	k1gFnSc2	lampa
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
je	být	k5eAaImIp3nS	být
skleněná	skleněný	k2eAgFnSc1d1	skleněná
nádoba	nádoba	k1gFnSc1	nádoba
naplněná	naplněný	k2eAgFnSc1d1	naplněná
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
olejem	olej	k1gInSc7	olej
<g/>
)	)	kIx)	)
a	a	k8xC	a
směsí	směs	k1gFnSc7	směs
průsvitného	průsvitný	k2eAgInSc2d1	průsvitný
vosku	vosk	k1gInSc2	vosk
a	a	k8xC	a
tetrachlormetanu	tetrachlormetan	k1gInSc2	tetrachlormetan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nádobou	nádoba	k1gFnSc7	nádoba
a	a	k8xC	a
žárovkou	žárovka	k1gFnSc7	žárovka
je	být	k5eAaImIp3nS	být
svinutý	svinutý	k2eAgInSc1d1	svinutý
kovový	kovový	k2eAgInSc1d1	kovový
drátek	drátek	k1gInSc1	drátek
kvůli	kvůli	k7c3	kvůli
lepšímu	dobrý	k2eAgNnSc3d2	lepší
vedení	vedení	k1gNnSc3	vedení
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
překonávání	překonávání	k1gNnSc4	překonávání
povrchového	povrchový	k2eAgNnSc2d1	povrchové
napětí	napětí	k1gNnSc2	napětí
voskových	voskový	k2eAgFnPc2d1	vosková
"	"	kIx"	"
<g/>
bublin	bublina	k1gFnPc2	bublina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
zas	zas	k6eAd1	zas
spojily	spojit	k5eAaPmAgFnP	spojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zapnutí	zapnutí	k1gNnSc6	zapnutí
lampy	lampa	k1gFnSc2	lampa
začne	začít	k5eAaPmIp3nS	začít
žárovka	žárovka	k1gFnSc1	žárovka
zahřívat	zahřívat	k5eAaImF	zahřívat
kovovou	kovový	k2eAgFnSc4d1	kovová
spirálu	spirála	k1gFnSc4	spirála
a	a	k8xC	a
dno	dno	k1gNnSc4	dno
nádoby	nádoba	k1gFnSc2	nádoba
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Vosk	vosk	k1gInSc1	vosk
roztaje	roztát	k5eAaPmIp3nS	roztát
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
stoupat	stoupat	k5eAaImF	stoupat
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ochladí	ochladit	k5eAaPmIp3nS	ochladit
a	a	k8xC	a
opět	opět	k6eAd1	opět
klesá	klesat	k5eAaImIp3nS	klesat
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
a	a	k8xC	a
horní	horní	k2eAgFnSc7d1	horní
částí	část	k1gFnSc7	část
nádoby	nádoba	k1gFnSc2	nádoba
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
několik	několik	k4yIc4	několik
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Vosk	vosk	k1gInSc1	vosk
v	v	k7c6	v
lampě	lampa	k1gFnSc6	lampa
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
větší	veliký	k2eAgFnPc4d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnPc4d2	menší
bubliny	bublina	k1gFnPc4	bublina
<g/>
,	,	kIx,	,
spojuje	spojovat	k5eAaImIp3nS	spojovat
se	se	k3xPyFc4	se
a	a	k8xC	a
trhá	trhat	k5eAaImIp3nS	trhat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provozní	provozní	k2eAgFnSc1d1	provozní
teplota	teplota	k1gFnSc1	teplota
různých	různý	k2eAgFnPc2d1	různá
lávových	lávový	k2eAgFnPc2d1	lávová
lamp	lampa	k1gFnPc2	lampa
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lampě	lampa	k1gFnSc6	lampa
příliš	příliš	k6eAd1	příliš
silná	silný	k2eAgFnSc1d1	silná
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vosk	vosk	k1gInSc4	vosk
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
příliš	příliš	k6eAd1	příliš
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Lávovou	lávový	k2eAgFnSc4d1	lávová
lampu	lampa	k1gFnSc4	lampa
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Angličan	Angličan	k1gMnSc1	Angličan
Edward	Edward	k1gMnSc1	Edward
Craven	Craven	k2eAgInSc4d1	Craven
Walker	Walker	k1gInSc4	Walker
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ji	on	k3xPp3gFnSc4	on
Astrolight	Astrolight	k1gMnSc1	Astrolight
a	a	k8xC	a
představil	představit	k5eAaPmAgMnS	představit
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Lávová	lávový	k2eAgFnSc1d1	lávová
lampa	lampa	k1gFnSc1	lampa
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
šedesátých	šedesátý	k4xOgInPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lavarand	Lavarand	k1gInSc1	Lavarand
</s>
</p>
<p>
<s>
Plazmová	plazmový	k2eAgFnSc1d1	plazmová
lampa	lampa	k1gFnSc1	lampa
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
lávová	lávový	k2eAgFnSc1d1	lávová
lampa	lampa	k1gFnSc1	lampa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
