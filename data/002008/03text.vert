<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
(	(	kIx(	(
<g/>
též	též	k9	též
sudí	sudí	k1gMnPc1	sudí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dbá	dbát	k5eAaImIp3nS	dbát
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
určených	určený	k2eAgFnPc2d1	určená
sportovních	sportovní	k2eAgFnPc2d1	sportovní
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
setkáme	setkat	k5eAaPmIp1nP	setkat
při	při	k7c6	při
sportovním	sportovní	k2eAgNnSc6d1	sportovní
utkání	utkání	k1gNnSc6	utkání
nebo	nebo	k8xC	nebo
při	při	k7c6	při
sportovních	sportovní	k2eAgInPc6d1	sportovní
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
viditelně	viditelně	k6eAd1	viditelně
rozlišen	rozlišen	k2eAgMnSc1d1	rozlišen
od	od	k7c2	od
hráčů	hráč	k1gMnPc2	hráč
či	či	k8xC	či
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
proto	proto	k8xC	proto
odražený	odražený	k2eAgInSc4d1	odražený
míč	míč	k1gInSc4	míč
od	od	k7c2	od
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
do	do	k7c2	do
branky	branka	k1gFnSc2	branka
je	být	k5eAaImIp3nS	být
platný	platný	k2eAgInSc1d1	platný
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
řešili	řešit	k5eAaImAgMnP	řešit
spory	spor	k1gInPc4	spor
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
kapitáni	kapitán	k1gMnPc1	kapitán
obou	dva	k4xCgFnPc6	dva
týmů	tým	k1gInPc2	tým
společnou	společný	k2eAgFnSc7d1	společná
konzultací	konzultace	k1gFnSc7	konzultace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
potřeba	potřeba	k1gFnSc1	potřeba
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
zápasech	zápas	k1gInPc6	zápas
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přibyl	přibýt	k5eAaPmAgInS	přibýt
ještě	ještě	k9	ještě
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
pomáhat	pomáhat	k5eAaImF	pomáhat
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
čárový	čárový	k2eAgInSc1d1	čárový
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
(	(	kIx(	(
<g/>
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zápase	zápas	k1gInSc6	zápas
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
nových	nový	k2eAgNnPc2d1	nové
pravidel	pravidlo	k1gNnPc2	pravidlo
IIHF	IIHF	kA	IIHF
přítomní	přítomný	k2eAgMnPc1d1	přítomný
dva	dva	k4xCgMnPc1	dva
hlavní	hlavní	k2eAgMnPc1d1	hlavní
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
čároví	čárový	k2eAgMnPc1d1	čárový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řešení	řešení	k1gNnSc6	řešení
velmi	velmi	k6eAd1	velmi
složitých	složitý	k2eAgInPc2d1	složitý
sporů	spor	k1gInPc2	spor
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
videorozhodčí	videorozhodčí	k2eAgInSc1d1	videorozhodčí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
poradní	poradní	k2eAgFnSc4d1	poradní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgMnPc1	dva
brankoví	brankový	k2eAgMnPc1d1	brankový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
časoměřič	časoměřič	k1gMnSc1	časoměřič
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
trestoměřiči	trestoměřič	k1gMnPc1	trestoměřič
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
kromě	kromě	k7c2	kromě
videorozhodčích	videorozhodčí	k1gMnPc2	videorozhodčí
<g/>
,	,	kIx,	,
časoměřičů	časoměřič	k1gMnPc2	časoměřič
a	a	k8xC	a
trestoměřičů	trestoměřič	k1gMnPc2	trestoměřič
nosí	nosit	k5eAaImIp3nS	nosit
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
podélně	podélně	k6eAd1	podélně
pruhovaný	pruhovaný	k2eAgInSc4d1	pruhovaný
dres	dres	k1gInSc4	dres
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnPc1d1	hlavní
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
vrchních	vrchní	k2eAgFnPc6d1	vrchní
částech	část	k1gFnPc6	část
obou	dva	k4xCgInPc2	dva
rukávů	rukáv	k1gInPc2	rukáv
oranžové	oranžový	k2eAgFnSc2d1	oranžová
pásky	páska	k1gFnSc2	páska
<g/>
)	)	kIx)	)
a	a	k8xC	a
černé	černý	k2eAgFnPc4d1	černá
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
ledové	ledový	k2eAgFnSc6d1	ledová
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
brusle	brusle	k1gFnPc4	brusle
a	a	k8xC	a
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
přilbu	přilba	k1gFnSc4	přilba
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gFnSc4	jejich
povinnou	povinný	k2eAgFnSc4d1	povinná
výbavu	výbava	k1gFnSc4	výbava
patří	patřit	k5eAaImIp3nS	patřit
kovová	kovový	k2eAgFnSc1d1	kovová
píšťalka	píšťalka	k1gFnSc1	píšťalka
upevněná	upevněný	k2eAgFnSc1d1	upevněná
na	na	k7c6	na
ukazováčku	ukazováček	k1gInSc6	ukazováček
a	a	k8xC	a
prostředníčku	prostředníček	k1gInSc6	prostředníček
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
kovový	kovový	k2eAgInSc4d1	kovový
stahovací	stahovací	k2eAgInSc4d1	stahovací
metr	metr	k1gInSc4	metr
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
m	m	kA	m
na	na	k7c4	na
přeměřování	přeměřování	k1gNnSc4	přeměřování
správné	správný	k2eAgFnSc2d1	správná
délky	délka	k1gFnSc2	délka
výstroje	výstroj	k1gFnSc2	výstroj
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
zápas	zápas	k1gInSc4	zápas
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
přestupky	přestupek	k1gInPc4	přestupek
a	a	k8xC	a
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
pomezní	pomezní	k2eAgMnPc1d1	pomezní
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
neboli	neboli	k8xC	neboli
asistenti	asistent	k1gMnPc1	asistent
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
signalizovat	signalizovat	k5eAaImF	signalizovat
postavení	postavení	k1gNnSc4	postavení
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
(	(	kIx(	(
<g/>
ofsajd	ofsajd	k1gInSc1	ofsajd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přítomný	přítomný	k1gMnSc1	přítomný
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
střídání	střídání	k1gNnSc1	střídání
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
jejich	jejich	k3xOp3gFnSc4	jejich
výstroj	výstroj	k1gFnSc4	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
kontrole	kontrola	k1gFnSc3	kontrola
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
byl	být	k5eAaImAgMnS	být
určen	určen	k2eAgMnSc1d1	určen
delegát	delegát	k1gMnSc1	delegát
posuzující	posuzující	k2eAgFnSc4d1	posuzující
nestrannost	nestrannost	k1gFnSc4	nestrannost
jejich	jejich	k3xOp3gNnSc2	jejich
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
kromě	kromě	k7c2	kromě
delegáta	delegát	k1gMnSc2	delegát
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
dresy	dres	k1gInPc4	dres
žluto-černé	žluto-černý	k2eAgFnSc2d1	žluto-černá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
lépe	dobře	k6eAd2	dobře
odlišeni	odlišen	k2eAgMnPc1d1	odlišen
od	od	k7c2	od
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
trenýrky	trenýrky	k1gFnPc1	trenýrky
<g/>
,	,	kIx,	,
špunty	špunty	k?	špunty
a	a	k8xC	a
kopačky	kopačka	k1gFnSc2	kopačka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výbavě	výbava	k1gFnSc3	výbava
hlavního	hlavní	k2eAgMnSc2d1	hlavní
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
píšťalka	píšťalka	k1gFnSc1	píšťalka
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
pero	pero	k1gNnSc1	pero
se	s	k7c7	s
zápisníkem	zápisník	k1gInSc7	zápisník
na	na	k7c4	na
poznámky	poznámka	k1gFnPc4	poznámka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
na	na	k7c4	na
úvodní	úvodní	k2eAgNnSc4d1	úvodní
losování	losování	k1gNnSc4	losování
polovin	polovina	k1gFnPc2	polovina
pro	pro	k7c4	pro
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
mužstva	mužstvo	k1gNnPc4	mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pomezní	pomezní	k2eAgMnPc1d1	pomezní
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
mají	mít	k5eAaImIp3nP	mít
žluto-oranžové	žlutoranžový	k2eAgFnPc1d1	žluto-oranžová
signalizační	signalizační	k2eAgFnPc1d1	signalizační
vlajky	vlajka	k1gFnPc1	vlajka
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
drží	držet	k5eAaImIp3nS	držet
ruční	ruční	k2eAgFnSc4d1	ruční
světelnou	světelný	k2eAgFnSc4d1	světelná
tabuli	tabule	k1gFnSc4	tabule
<g/>
.	.	kIx.	.
píšťalka	píšťalka	k1gFnSc1	píšťalka
rozlišovací	rozlišovací	k2eAgFnSc1d1	rozlišovací
dres	dres	k1gInSc1	dres
(	(	kIx(	(
<g/>
košile	košile	k1gFnSc1	košile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
,	,	kIx,	,
štulpny	štulpna	k1gFnPc1	štulpna
a	a	k8xC	a
kopačky	kopačka	k1gFnPc1	kopačka
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
karta	karta	k1gFnSc1	karta
tužka	tužka	k1gFnSc1	tužka
a	a	k8xC	a
papír	papír	k1gInSc1	papír
na	na	k7c4	na
poznámky	poznámka	k1gFnPc4	poznámka
hodinky	hodinka	k1gFnSc2	hodinka
mince	mince	k1gFnSc2	mince
na	na	k7c4	na
losování	losování	k1gNnSc4	losování
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
měřit	měřit	k5eAaImF	měřit
dobu	doba	k1gFnSc4	doba
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
dělat	dělat	k5eAaImF	dělat
si	se	k3xPyFc3	se
poznámky	poznámka	k1gFnPc4	poznámka
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
přerušení	přerušení	k1gNnSc6	přerušení
hry	hra	k1gFnSc2	hra
Rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
o	o	k7c6	o
skutečnostech	skutečnost	k1gFnPc6	skutečnost
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
hrou	hra	k1gFnSc7	hra
jsou	být	k5eAaImIp3nP	být
konečná	konečný	k2eAgNnPc1d1	konečné
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
je	on	k3xPp3gNnSc4	on
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyla	být	k5eNaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
hra	hra	k1gFnSc1	hra
nebo	nebo	k8xC	nebo
utkání	utkání	k1gNnSc1	utkání
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
.	.	kIx.	.
pískat	pískat	k5eAaImF	pískat
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
dát	dát	k5eAaPmF	dát
po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
přerušení	přerušení	k1gNnSc6	přerušení
hry	hra	k1gFnSc2	hra
znamení	znamení	k1gNnSc2	znamení
k	k	k7c3	k
pokračování	pokračování	k1gNnSc3	pokračování
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
používat	používat	k5eAaImF	používat
předepsaných	předepsaný	k2eAgInPc2d1	předepsaný
signálů	signál	k1gInPc2	signál
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepřekážel	překážet	k5eNaImAgMnS	překážet
hráčům	hráč	k1gMnPc3	hráč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
<g />
.	.	kIx.	.
</s>
<s>
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
herních	herní	k2eAgFnPc6d1	herní
situacích	situace	k1gFnPc6	situace
a	a	k8xC	a
o	o	k7c6	o
přestupcích	přestupek	k1gInPc6	přestupek
hráčů	hráč	k1gMnPc2	hráč
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
míče	míč	k1gInSc2	míč
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
situaci	situace	k1gFnSc4	situace
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
hřišti	hřiště	k1gNnSc6	hřiště
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
zorném	zorný	k2eAgNnSc6d1	zorné
poli	pole	k1gNnSc6	pole
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
asistentů	asistent	k1gMnPc2	asistent
nebavit	bavit	k5eNaImF	bavit
se	se	k3xPyFc4	se
zbytečně	zbytečně	k6eAd1	zbytečně
s	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
a	a	k8xC	a
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
nedotýkat	dotýkat	k5eNaImF	dotýkat
<g />
.	.	kIx.	.
</s>
<s>
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
přehnané	přehnaný	k2eAgFnSc3d1	přehnaná
gestikulaci	gestikulace	k1gFnSc3	gestikulace
dát	dát	k5eAaPmF	dát
píšťalkou	píšťalka	k1gFnSc7	píšťalka
znamení	znamení	k1gNnSc2	znamení
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
utkání	utkání	k1gNnSc2	utkání
Dříve	dříve	k6eAd2	dříve
jím	on	k3xPp3gMnSc7	on
bývali	bývat	k5eAaImAgMnP	bývat
jen	jen	k9	jen
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
(	(	kIx(	(
<g/>
známou	známý	k2eAgFnSc7d1	známá
českou	český	k2eAgFnSc7d1	Česká
rozhodčí	rozhodčí	k1gMnPc4	rozhodčí
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Dagmar	Dagmar	k1gFnSc1	Dagmar
Damková	Damková	k1gFnSc1	Damková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
tenisový	tenisový	k2eAgMnSc1d1	tenisový
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
orgán	orgán	k1gMnSc1	orgán
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rozhodca	Rozhodca	k?	Rozhodca
(	(	kIx(	(
<g/>
šport	šport	k1gInSc1	šport
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rozhodčí	rozhodčí	k1gMnPc4	rozhodčí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
