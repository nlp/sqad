<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadu	severozápad	k1gInSc2	severozápad
Anglie	Anglie	k1gFnSc2	Anglie
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Lune	Lun	k1gFnSc2	Lun
a	a	k8xC	a
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
má	mít	k5eAaImIp3nS	mít
45	[number]	k4	45
952	[number]	k4	952
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
sousedními	sousední	k2eAgNnPc7d1	sousední
městy	město	k1gNnPc7	město
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vyššího	vysoký	k2eAgInSc2d2	vyšší
správního	správní	k2eAgInSc2d1	správní
celku	celek	k1gInSc2	celek
–	–	k?	–
distriktu	distrikt	k1gInSc2	distrikt
City	City	k1gFnSc2	City
of	of	k?	of
Lancaster	Lancaster	k1gInSc1	Lancaster
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
133	[number]	k4	133
914	[number]	k4	914
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
hrabství	hrabství	k1gNnSc2	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
dalo	dát	k5eAaPmAgNnS	dát
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
komerčním	komerční	k2eAgMnSc7d1	komerční
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgNnSc7d1	kulturní
a	a	k8xC	a
vzdělávacím	vzdělávací	k2eAgNnSc7d1	vzdělávací
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
řeky	řeka	k1gFnSc2	řeka
Lune	Lun	k1gFnSc2	Lun
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jím	on	k3xPp3gNnSc7	on
protéká	protékat	k5eAaImIp3nS	protékat
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
keltštiny	keltština	k1gFnSc2	keltština
<g/>
,	,	kIx,	,
a	a	k8xC	a
impozantního	impozantní	k2eAgInSc2d1	impozantní
hradu	hrad	k1gInSc2	hrad
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
castle	castle	k6eAd1	castle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Loncastra	Loncastrum	k1gNnSc2	Loncastrum
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
Lancaster	Lancaster	k1gInSc4	Lancaster
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lancasterský	Lancasterský	k2eAgInSc1d1	Lancasterský
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
římské	římský	k2eAgFnSc2d1	římská
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
pro	pro	k7c4	pro
procesy	proces	k1gInPc4	proces
s	s	k7c7	s
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1612	[number]	k4	1612
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
odsouzených	odsouzený	k2eAgFnPc2d1	odsouzená
a	a	k8xC	a
oběšených	oběšený	k2eAgFnPc2d1	oběšená
obětí	oběť	k1gFnPc2	oběť
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
mimo	mimo	k6eAd1	mimo
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
obdržel	obdržet	k5eAaPmAgInS	obdržet
roku	rok	k1gInSc2	rok
1193	[number]	k4	1193
výsadu	výsada	k1gFnSc4	výsada
pořádání	pořádání	k1gNnSc2	pořádání
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
trhů	trh	k1gInPc2	trh
ale	ale	k8xC	ale
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
udělen	udělen	k2eAgInSc4d1	udělen
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
podél	podél	k7c2	podél
St.	st.	kA	st.
George	Georg	k1gFnSc2	Georg
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Quay	Quay	k1gInPc7	Quay
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Lancaster	Lancastra	k1gFnPc2	Lancastra
velmi	velmi	k6eAd1	velmi
rušným	rušný	k2eAgNnSc7d1	rušné
městem	město	k1gNnSc7	město
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
centrem	centr	k1gInSc7	centr
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
významného	významný	k2eAgInSc2d1	významný
přístavu	přístav	k1gInSc2	přístav
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
jen	jen	k9	jen
krátké	krátký	k2eAgNnSc4d1	krátké
trvání	trvání	k1gNnSc4	trvání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
stala	stát	k5eAaPmAgFnS	stát
nesplavnou	splavný	k2eNgFnSc4d1	nesplavná
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
ponorem	ponor	k1gInSc7	ponor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
přístavy	přístav	k1gInPc7	přístav
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Morecambe	Morecamb	k1gInSc5	Morecamb
<g/>
,	,	kIx,	,
Glasson	Glasson	k1gNnSc1	Glasson
Dock	Docka	k1gFnPc2	Docka
a	a	k8xC	a
Sunderland	Sunderlanda	k1gFnPc2	Sunderlanda
Point	pointa	k1gFnPc2	pointa
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Heysham	Heysham	k1gInSc1	Heysham
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
je	být	k5eAaImIp3nS	být
nejsevernějším	severní	k2eAgNnSc7d3	nejsevernější
městem	město	k1gNnSc7	město
hrabství	hrabství	k1gNnSc2	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
od	od	k7c2	od
Morecambe	Morecamb	k1gMnSc5	Morecamb
Bay	Bay	k1gMnSc5	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
kolem	kolem	k7c2	kolem
řeky	řeka	k1gFnSc2	řeka
Lune	Lun	k1gFnSc2	Lun
a	a	k8xC	a
Lancasterského	Lancasterský	k2eAgInSc2d1	Lancasterský
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
M6	M6	k1gFnSc2	M6
prochází	procházet	k5eAaImIp3nS	procházet
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
města	město	k1gNnSc2	město
a	a	k8xC	a
West	West	k2eAgInSc1d1	West
Coast	Coast	k1gInSc1	Coast
railway	railwaa	k1gFnSc2	railwaa
line	linout	k5eAaImIp3nS	linout
prochází	procházet	k5eAaImIp3nS	procházet
železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
v	v	k7c6	v
Lancasteru	Lancaster	k1gInSc6	Lancaster
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Lancasteru	Lancaster	k1gInSc2	Lancaster
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
na	na	k7c4	na
poskytování	poskytování	k1gNnSc4	poskytování
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
produkuje	produkovat	k5eAaImIp3nS	produkovat
nebo	nebo	k8xC	nebo
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
krmiva	krmivo	k1gNnPc4	krmivo
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgNnSc4d1	textilní
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
papírenské	papírenský	k2eAgInPc4d1	papírenský
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgNnPc4d1	umělé
vlákna	vlákno	k1gNnPc4	vlákno
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
těžká	těžký	k2eAgNnPc4d1	těžké
nákladní	nákladní	k2eAgNnPc4d1	nákladní
vozidla	vozidlo	k1gNnPc4	vozidlo
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
technologicky	technologicky	k6eAd1	technologicky
vyspělejší	vyspělý	k2eAgFnPc4d2	vyspělejší
výroby	výroba	k1gFnPc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
sídlila	sídlit	k5eAaImAgFnS	sídlit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
centrála	centrála	k1gFnSc1	centrála
společnosti	společnost	k1gFnSc2	společnost
Reebok	Reebok	k1gInSc1	Reebok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Addidas	Addidasa	k1gFnPc2	Addidasa
plánuje	plánovat	k5eAaImIp3nS	plánovat
přesunout	přesunout	k5eAaPmF	přesunout
výrobu	výroba	k1gFnSc4	výroba
do	do	k7c2	do
Boltonu	Bolton	k1gInSc2	Bolton
a	a	k8xC	a
Stockportu	Stockport	k1gInSc2	Stockport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Lancasteru	Lancaster	k1gInSc6	Lancaster
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
nákupní	nákupní	k2eAgNnSc1d1	nákupní
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
nákupů	nákup	k1gInPc2	nákup
pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
Lancashire	Lancashir	k1gInSc5	Lancashir
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Cumbrii	Cumbrie	k1gFnSc6	Cumbrie
<g/>
.	.	kIx.	.
</s>
<s>
Marketgate	Marketgat	k1gMnSc5	Marketgat
Shopping	shopping	k1gInSc1	shopping
Centre	centr	k1gInSc5	centr
a	a	k8xC	a
St.	st.	kA	st.
Nicholas	Nicholas	k1gMnSc1	Nicholas
Arcades	Arcades	k1gMnSc1	Arcades
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgNnPc1d3	nejdůležitější
zastřešená	zastřešený	k2eAgNnPc1d1	zastřešené
obchodní	obchodní	k2eAgNnPc1d1	obchodní
centra	centrum	k1gNnPc1	centrum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
obchodů	obchod	k1gInPc2	obchod
společností	společnost	k1gFnPc2	společnost
významných	významný	k2eAgFnPc2d1	významná
značek	značka	k1gFnPc2	značka
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
menších	malý	k2eAgFnPc2d2	menší
prodejen	prodejna	k1gFnPc2	prodejna
drobných	drobný	k2eAgMnPc2d1	drobný
soukromých	soukromý	k2eAgMnPc2d1	soukromý
obchodníků	obchodník	k1gMnPc2	obchodník
nabízejících	nabízející	k2eAgFnPc2d1	nabízející
oděvy	oděv	k1gInPc7	oděv
a	a	k8xC	a
klenoty	klenot	k1gInPc7	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
obchodní	obchodní	k2eAgNnPc1d1	obchodní
střediska	středisko	k1gNnPc1	středisko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
==	==	k?	==
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
a	a	k8xC	a
sousední	sousední	k2eAgInPc1d1	sousední
Morecambe	Morecamb	k1gInSc5	Morecamb
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
doby	doba	k1gFnPc1	doba
sloučily	sloučit	k5eAaPmAgFnP	sloučit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
konurbace	konurbace	k1gFnSc2	konurbace
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
spravovány	spravovat	k5eAaImNgInP	spravovat
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
a	a	k8xC	a
distrikt	distrikt	k1gInSc1	distrikt
Lancaster	Lancastra	k1gFnPc2	Lancastra
a	a	k8xC	a
municipální	municipální	k2eAgInSc1d1	municipální
distrikt	distrikt	k1gInSc1	distrikt
Morecambe	Morecamb	k1gInSc5	Morecamb
a	a	k8xC	a
Heyshamu	Heysham	k1gInSc2	Heysham
byl	být	k5eAaImAgInS	být
spojen	spojen	k2eAgInSc1d1	spojen
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
distrikt	distrikt	k1gInSc1	distrikt
City	City	k1gFnSc2	City
of	of	k?	of
Lancaster	Lancaster	k1gMnSc1	Lancaster
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hrabství	hrabství	k1gNnSc2	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
distrikt	distrikt	k1gInSc1	distrikt
obdržel	obdržet	k5eAaPmAgInS	obdržet
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spravován	spravovat	k5eAaImNgMnS	spravovat
radou	rada	k1gFnSc7	rada
města	město	k1gNnSc2	město
Lancasteru	Lancaster	k1gInSc2	Lancaster
(	(	kIx(	(
<g/>
Lancaster	Lancaster	k1gInSc1	Lancaster
City	city	k1gNnSc1	city
Council	Council	k1gInSc1	Council
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
Lancaster	Lancaster	k1gMnSc1	Lancaster
and	and	k?	and
Wyre	Wyr	k1gFnSc2	Wyr
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
regionu	region	k1gInSc2	region
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Anglie	Anglie	k1gFnSc2	Anglie
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
jako	jako	k8xS	jako
historické	historický	k2eAgNnSc1d1	historické
město	město	k1gNnSc1	město
nabízí	nabízet	k5eAaImIp3nS	nabízet
průměrný	průměrný	k2eAgInSc4d1	průměrný
rozsah	rozsah	k1gInSc4	rozsah
kulturního	kulturní	k2eAgNnSc2d1	kulturní
vyžití	vyžití	k1gNnSc2	vyžití
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
stavby	stavba	k1gFnPc1	stavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
georgiánské	georgiánský	k2eAgFnSc2d1	georgiánská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
historické	historický	k2eAgFnPc4d1	historická
stavby	stavba	k1gFnPc4	stavba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nS	patřit
Lancasterský	Lancasterský	k2eAgInSc1d1	Lancasterský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
převorský	převorský	k2eAgInSc1d1	převorský
kostel	kostel	k1gInSc1	kostel
Svaté	svatý	k2eAgFnSc2d1	svatá
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Ashtonův	Ashtonův	k2eAgInSc1d1	Ashtonův
památník	památník	k1gInSc1	památník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gMnSc1	Lancaster
Grand	grand	k1gMnSc1	grand
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
Dukes	Dukes	k1gMnSc1	Dukes
Theatre	Theatr	k1gInSc5	Theatr
jsou	být	k5eAaImIp3nP	být
nejdůležitějšími	důležitý	k2eAgNnPc7d3	nejdůležitější
místy	místo	k1gNnPc7	místo
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
také	také	k9	také
koná	konat	k5eAaImIp3nS	konat
festival	festival	k1gInSc1	festival
Play	play	k0	play
in	in	k?	in
the	the	k?	the
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
představení	představení	k1gNnSc2	představení
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
prostranství	prostranství	k1gNnSc6	prostranství
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
Williamson	Williamson	k1gInSc1	Williamson
Parku	park	k1gInSc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
největší	veliký	k2eAgFnSc7d3	veliký
studentskou	studentský	k2eAgFnSc7d1	studentská
scénou	scéna	k1gFnSc7	scéna
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pravidelně	pravidelně	k6eAd1	pravidelně
hostí	hostit	k5eAaImIp3nS	hostit
známé	známý	k2eAgMnPc4d1	známý
komiky	komik	k1gMnPc4	komik
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgInPc1d1	taneční
soubory	soubor	k1gInPc1	soubor
a	a	k8xC	a
dramatická	dramatický	k2eAgNnPc1d1	dramatické
představení	představení	k1gNnPc1	představení
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
kulturními	kulturní	k2eAgFnPc7d1	kulturní
událostmi	událost	k1gFnPc7	událost
jsou	být	k5eAaImIp3nP	být
Lancaster	Lancaster	k1gInSc4	Lancaster
Jazz	jazz	k1gInSc1	jazz
Festival	festival	k1gInSc1	festival
a	a	k8xC	a
Maritime	Maritim	k1gInSc5	Maritim
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Bailrigg	Bailrigga	k1gFnPc2	Bailrigga
sídlí	sídlet	k5eAaImIp3nS	sídlet
Lancasterská	Lancasterský	k2eAgFnSc1d1	Lancasterská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
skvělými	skvělý	k2eAgInPc7d1	skvělý
výsledky	výsledek	k1gInPc7	výsledek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výzkumu	výzkum	k1gInSc2	výzkum
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
univerzit	univerzita	k1gFnPc2	univerzita
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
obchodních	obchodní	k2eAgFnPc2d1	obchodní
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
rozvoje	rozvoj	k1gInSc2	rozvoj
pro	pro	k7c4	pro
informační	informační	k2eAgFnPc4d1	informační
a	a	k8xC	a
komunikační	komunikační	k2eAgFnPc4d1	komunikační
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Lancasteru	Lancaster	k1gInSc6	Lancaster
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
jedna	jeden	k4xCgFnSc1	jeden
fakulta	fakulta	k1gFnSc1	fakulta
školy	škola	k1gFnSc2	škola
University	universita	k1gFnSc2	universita
of	of	k?	of
Cumbria	Cumbrium	k1gNnSc2	Cumbrium
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
bývala	bývat	k5eAaImAgFnS	bývat
St	St	kA	St
Martin	Martin	k1gInSc4	Martin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fakulta	fakulta	k1gFnSc1	fakulta
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
kursy	kurs	k1gInPc4	kurs
postgraduální	postgraduální	k2eAgInPc4d1	postgraduální
i	i	k9	i
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
denního	denní	k2eAgNnSc2d1	denní
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
uměleckých	umělecký	k2eAgInPc2d1	umělecký
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgInPc2d1	sociální
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgInPc2d1	obchodní
<g/>
,	,	kIx,	,
pedagogických	pedagogický	k2eAgInPc2d1	pedagogický
a	a	k8xC	a
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
středoškolského	středoškolský	k2eAgNnSc2d1	středoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Lancasteru	Lancaster	k1gInSc6	Lancaster
například	například	k6eAd1	například
školy	škola	k1gFnPc4	škola
Lancaster	Lancaster	k1gMnSc1	Lancaster
Royal	Royal	k1gMnSc1	Royal
Grammar	Grammar	k1gMnSc1	Grammar
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Lancaster	Lancastra	k1gFnPc2	Lancastra
Girls	girl	k1gFnPc2	girl
<g/>
'	'	kIx"	'
Grammar	Grammar	k1gInSc1	Grammar
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Ripley	Riplea	k1gFnSc2	Riplea
St.	st.	kA	st.
Thomas	Thomas	k1gMnSc1	Thomas
C	C	kA	C
of	of	k?	of
E	E	kA	E
High	High	k1gInSc1	High
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Our	Our	k1gFnSc1	Our
Lady	lady	k1gFnSc1	lady
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Catholic	Catholice	k1gFnPc2	Catholice
College	College	k1gNnSc1	College
<g/>
,	,	kIx,	,
Central	Central	k1gFnSc1	Central
Lancaster	Lancaster	k1gInSc1	Lancaster
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
a	a	k8xC	a
Morecambe	Morecamb	k1gMnSc5	Morecamb
College	Collegus	k1gMnSc5	Collegus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Lancasterský	Lancasterský	k2eAgInSc1d1	Lancasterský
veslařský	veslařský	k2eAgInSc1d1	veslařský
klub	klub	k1gInSc1	klub
John	John	k1gMnSc1	John
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
gaunt	gaunt	k1gInSc1	gaunt
Rowing	Rowing	k1gInSc1	Rowing
Club	club	k1gInSc1	club
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejstarším	starý	k2eAgInSc7d3	nejstarší
veslařským	veslařský	k2eAgInSc7d1	veslařský
klubem	klub	k1gInSc7	klub
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
sídlo	sídlo	k1gNnSc1	sídlo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
hráze	hráz	k1gFnSc2	hráz
poblíž	poblíž	k7c2	poblíž
Skertonu	Skerton	k1gInSc2	Skerton
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Lancaster	Lancastra	k1gFnPc2	Lancastra
City	City	k1gFnSc2	City
FC	FC	kA	FC
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Lancasterský	Lancasterský	k2eAgInSc1d1	Lancasterský
kriketový	kriketový	k2eAgInSc1d1	kriketový
klub	klub	k1gInSc1	klub
sídlí	sídlet	k5eAaImIp3nS	sídlet
nedaleko	nedaleko	k7c2	nedaleko
řeky	řeka	k1gFnSc2	řeka
Lune	Lun	k1gFnSc2	Lun
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
seniorské	seniorský	k2eAgInPc4d1	seniorský
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistické	turistický	k2eAgFnPc1d1	turistická
atrakce	atrakce	k1gFnPc1	atrakce
==	==	k?	==
</s>
</p>
<p>
<s>
Lancasterský	Lancasterský	k2eAgInSc1d1	Lancasterský
hrad	hrad	k1gInSc1	hrad
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gMnSc1	Lancaster
Priory	prior	k1gMnPc7	prior
</s>
</p>
<p>
<s>
Lancasterské	Lancasterský	k2eAgNnSc1d1	Lancasterský
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
Lune	Lune	k6eAd1	Lune
Millennium	millennium	k1gNnSc1	millennium
Bridge	Bridg	k1gFnSc2	Bridg
</s>
</p>
<p>
<s>
Williamson	Williamson	k1gInSc1	Williamson
Park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
Ashton	Ashton	k1gInSc1	Ashton
Memorial	Memorial	k1gInSc1	Memorial
and	and	k?	and
Butterfly	butterfly	k1gInSc1	butterfly
House	house	k1gNnSc1	house
</s>
</p>
<p>
<s>
Lancasterská	Lancasterský	k2eAgFnSc1d1	Lancasterská
katedrála	katedrála	k1gFnSc1	katedrála
</s>
</p>
<p>
<s>
Storey	Storea	k1gFnPc1	Storea
Institute	institut	k1gInSc5	institut
</s>
</p>
<p>
<s>
Judges	Judges	k1gInSc1	Judges
Lodgings	Lodgingsa	k1gFnPc2	Lodgingsa
</s>
</p>
<p>
<s>
Cottage	Cottage	k6eAd1	Cottage
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
University	universita	k1gFnSc2	universita
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ruskin	Ruskin	k1gInSc1	Ruskin
Library	Librar	k1gInPc1	Librar
</s>
</p>
<p>
<s>
Quayside	Quaysid	k1gMnSc5	Quaysid
Maritine	Maritin	k1gMnSc5	Maritin
Museum	museum	k1gNnSc5	museum
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gMnSc1	Lancaster
Royal	Royal	k1gMnSc1	Royal
Grammar	Grammar	k1gMnSc1	Grammar
School	School	k1gInSc4	School
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
Girls	girl	k1gFnPc2	girl
<g/>
'	'	kIx"	'
Grammar	Grammar	k1gInSc1	Grammar
School	Schoola	k1gFnPc2	Schoola
</s>
</p>
<p>
<s>
The	The	k?	The
Duke	Duke	k1gInSc1	Duke
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Playhouse	Playhous	k1gInSc5	Playhous
</s>
</p>
<p>
<s>
The	The	k?	The
Gregson	Gregson	k1gInSc1	Gregson
Centre	centr	k1gInSc5	centr
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gMnSc1	Lancaster
Grand	grand	k1gMnSc1	grand
Theatre	Theatr	k1gMnSc5	Theatr
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
Musicians	Musiciansa	k1gFnPc2	Musiciansa
Co-operative	Coperativ	k1gInSc5	Co-operativ
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gInSc1	Lancaster
Golf	golf	k1gInSc1	golf
Club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lancaster	Lancastra	k1gFnPc2	Lancastra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
Lancasterem	Lancaster	k1gMnSc7	Lancaster
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgMnSc1d1	virtuální
průvodce	průvodce	k1gMnSc1	průvodce
Lancasterem	Lancaster	k1gInSc7	Lancaster
</s>
</p>
<p>
<s>
Lancaster	Lancaster	k1gMnSc1	Lancaster
Guardian	Guardian	k1gMnSc1	Guardian
</s>
</p>
