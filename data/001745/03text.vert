<s>
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnPc4	Berenice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
-	-	kIx~	-
A	a	k9	a
Tale	Tale	k1gNnSc2	Tale
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hororová	hororový	k2eAgFnSc1d1	hororová
povídka	povídka	k1gFnSc1	povídka
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
literárního	literární	k2eAgMnSc2d1	literární
teoretika	teoretik	k1gMnSc2	teoretik
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
periodiku	periodikum	k1gNnSc6	periodikum
Southern	Southerno	k1gNnPc2	Southerno
Literary	Literara	k1gFnSc2	Literara
Messenger	Messengero	k1gNnPc2	Messengero
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
sleduje	sledovat	k5eAaImIp3nS	sledovat
muže	muž	k1gMnSc4	muž
jménem	jméno	k1gNnSc7	jméno
Egeus	Egeus	k1gMnSc1	Egeus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Berenice	Berenice	k1gFnSc2	Berenice
<g/>
.	.	kIx.	.
</s>
<s>
Egeus	Egeus	k1gInSc1	Egeus
trpí	trpět	k5eAaImIp3nS	trpět
návaly	nával	k1gInPc7	nával
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
soustředění	soustředění	k1gNnSc2	soustředění
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
duchem	duch	k1gMnSc7	duch
mimo	mimo	k7c4	mimo
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
pozorovat	pozorovat	k5eAaImF	pozorovat
detaily	detail	k1gInPc4	detail
a	a	k8xC	a
titěrnosti	titěrnost	k1gFnPc4	titěrnost
a	a	k8xC	a
rozjímat	rozjímat	k5eAaImF	rozjímat
nad	nad	k7c7	nad
drobnostmi	drobnost	k1gFnPc7	drobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
objektů	objekt	k1gInPc2	objekt
jeho	jeho	k3xOp3gFnSc2	jeho
obsese	obsese	k1gFnSc2	obsese
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
Bereničiny	Bereničina	k1gFnPc1	Bereničina
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Kolik	kolik	k4yQc1	kolik
podob	podoba	k1gFnPc2	podoba
má	mít	k5eAaImIp3nS	mít
zoufalství	zoufalství	k1gNnPc4	zoufalství
a	a	k8xC	a
strast	strast	k1gFnSc4	strast
<g/>
.	.	kIx.	.
</s>
<s>
Radost	radost	k1gFnSc1	radost
plodí	plodit	k5eAaImIp3nS	plodit
žal	žal	k1gInSc1	žal
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
minulost	minulost	k1gFnSc4	minulost
přináší	přinášet	k5eAaImIp3nS	přinášet
muka	muka	k1gFnSc1	muka
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
úzkosti	úzkost	k1gFnPc1	úzkost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
prožíváme	prožívat	k5eAaImIp1nP	prožívat
<g/>
,	,	kIx,	,
zrodily	zrodit	k5eAaPmAgInP	zrodit
z	z	k7c2	z
rozkoší	rozkoš	k1gFnPc2	rozkoš
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jsme	být	k5eAaImIp1nP	být
mohli	moct	k5eAaImAgMnP	moct
prožívat	prožívat	k5eAaImF	prožívat
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
Egeus	Egeus	k1gMnSc1	Egeus
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
chatrným	chatrný	k2eAgNnSc7d1	chatrné
zdravím	zdraví	k1gNnSc7	zdraví
a	a	k8xC	a
sužovaný	sužovaný	k2eAgInSc1d1	sužovaný
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
obsesí	obsese	k1gFnSc7	obsese
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Mládí	mládí	k1gNnSc1	mládí
proležel	proležet	k5eAaPmAgInS	proležet
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
a	a	k8xC	a
prosnil	prosnít	k5eAaPmAgMnS	prosnít
<g/>
,	,	kIx,	,
budoval	budovat	k5eAaImAgMnS	budovat
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
paláce	palác	k1gInSc2	palác
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
realita	realita	k1gFnSc1	realita
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
působila	působit	k5eAaImAgFnS	působit
jako	jako	k8xS	jako
přelud	přelud	k1gInSc4	přelud
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
nenastal	nastat	k5eNaPmAgInS	nastat
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Egeovu	Egeův	k2eAgFnSc4d1	Egeův
sestřenici	sestřenice	k1gFnSc4	sestřenice
Berenici	Berenice	k1gFnSc4	Berenice
<g/>
,	,	kIx,	,
vitální	vitální	k2eAgFnSc4d1	vitální
čilou	čilý	k2eAgFnSc4d1	čilá
dívku	dívka	k1gFnSc4	dívka
překypující	překypující	k2eAgFnSc7d1	překypující
svěžestí	svěžest	k1gFnSc7	svěžest
<g/>
,	,	kIx,	,
objekt	objekt	k1gInSc1	objekt
Egeových	Egeův	k2eAgFnPc2d1	Egeův
představ	představa	k1gFnPc2	představa
postihne	postihnout	k5eAaPmIp3nS	postihnout
zákeřná	zákeřný	k2eAgFnSc1d1	zákeřná
psychická	psychický	k2eAgFnSc1d1	psychická
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
prodělává	prodělávat	k5eAaImIp3nS	prodělávat
padoucnici	padoucnice	k1gFnSc4	padoucnice
zakončenou	zakončený	k2eAgFnSc4d1	zakončená
transem	trans	k1gInSc7	trans
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Egea	Ege	k1gInSc2	Ege
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
druh	druh	k1gMnSc1	druh
maniacké	maniacký	k2eAgFnSc3d1	maniacký
posedlosti	posedlost	k1gFnSc3	posedlost
podobné	podobný	k2eAgFnSc6d1	podobná
katalepsii	katalepsie	k1gFnSc6	katalepsie
<g/>
,	,	kIx,	,
chorobná	chorobný	k2eAgFnSc1d1	chorobná
citlivost	citlivost	k1gFnSc1	citlivost
ústící	ústící	k2eAgFnSc1d1	ústící
v	v	k7c4	v
mnohahodinové	mnohahodinový	k2eAgNnSc4d1	mnohahodinové
zaměření	zaměření	k1gNnSc4	zaměření
se	se	k3xPyFc4	se
na	na	k7c4	na
detail	detail	k1gInSc4	detail
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
chronického	chronický	k2eAgNnSc2d1	chronické
soustředění	soustředění	k1gNnSc2	soustředění
dokáže	dokázat	k5eAaPmIp3nS	dokázat
probloumat	probloumat	k5eAaPmF	probloumat
v	v	k7c6	v
úvahách	úvaha	k1gFnPc6	úvaha
nad	nad	k7c7	nad
stínem	stín	k1gInSc7	stín
dopadajícím	dopadající	k2eAgInSc7d1	dopadající
na	na	k7c6	na
podlahu	podlaha	k1gFnSc4	podlaha
<g/>
,	,	kIx,	,
probdít	probdít	k5eAaPmF	probdít
noc	noc	k1gFnSc4	noc
s	s	k7c7	s
pohledem	pohled	k1gInSc7	pohled
upřeným	upřený	k2eAgInSc7d1	upřený
na	na	k7c4	na
plamen	plamen	k1gInSc4	plamen
svíce	svíce	k1gFnSc2	svíce
<g/>
,	,	kIx,	,
prosnít	prosnít	k5eAaPmF	prosnít
den	den	k1gInSc4	den
nad	nad	k7c7	nad
vůní	vůně	k1gFnSc7	vůně
květiny	květina	k1gFnSc2	květina
či	či	k8xC	či
donekonečna	donekonečna	k6eAd1	donekonečna
opakovat	opakovat	k5eAaImF	opakovat
jedno	jeden	k4xCgNnSc4	jeden
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jej	on	k3xPp3gMnSc4	on
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
změněný	změněný	k2eAgInSc1d1	změněný
tělesný	tělesný	k2eAgInSc1d1	tělesný
stav	stav	k1gInSc1	stav
své	svůj	k3xOyFgFnSc2	svůj
sestřenice	sestřenice	k1gFnSc2	sestřenice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
slabé	slabý	k2eAgFnSc6d1	slabá
chvilce	chvilka	k1gFnSc6	chvilka
slíbí	slíbit	k5eAaPmIp3nS	slíbit
manželství	manželství	k1gNnSc1	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
Egea	Egeum	k1gNnPc1	Egeum
fascinují	fascinovat	k5eAaBmIp3nP	fascinovat
její	její	k3xOp3gInPc4	její
alabastrové	alabastrový	k2eAgInPc4d1	alabastrový
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jimi	on	k3xPp3gFnPc7	on
tak	tak	k6eAd1	tak
posedlý	posedlý	k2eAgMnSc1d1	posedlý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stanou	stanout	k5eAaPmIp3nP	stanout
jediným	jediný	k2eAgInSc7d1	jediný
předmětem	předmět	k1gInSc7	předmět
svého	svůj	k3xOyFgNnSc2	svůj
abnormálního	abnormální	k2eAgNnSc2d1	abnormální
soustředění	soustředění	k1gNnSc2	soustředění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
si	se	k3xPyFc3	se
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
bere	brát	k5eAaImIp3nS	brát
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
prohlíží	prohlížet	k5eAaImIp3nP	prohlížet
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
přemítá	přemítat	k5eAaImIp3nS	přemítat
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gFnSc7	jejich
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
šílí	šílet	k5eAaImIp3nP	šílet
a	a	k8xC	a
touží	toužit	k5eAaImIp3nP	toužit
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
zmocnit	zmocnit	k5eAaPmF	zmocnit
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
večera	večer	k1gInSc2	večer
jej	on	k3xPp3gMnSc4	on
informuje	informovat	k5eAaBmIp3nS	informovat
služebná	služebný	k2eAgFnSc1d1	služebná
<g/>
,	,	kIx,	,
že	že	k8xS	že
Berenice	Berenice	k1gFnSc1	Berenice
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
mezi	mezi	k7c7	mezi
živými	živý	k2eAgFnPc7d1	živá
<g/>
,	,	kIx,	,
ráno	ráno	k6eAd1	ráno
ji	on	k3xPp3gFnSc4	on
skolil	skolit	k5eAaPmAgInS	skolit
silný	silný	k2eAgInSc1d1	silný
záchvat	záchvat	k1gInSc1	záchvat
epilepsie	epilepsie	k1gFnSc2	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
hrob	hrob	k1gInSc1	hrob
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
moment	moment	k1gInSc4	moment
příčetnost	příčetnost	k1gFnSc1	příčetnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
vědomí	vědomí	k1gNnSc1	vědomí
se	se	k3xPyFc4	se
zaplnilo	zaplnit	k5eAaPmAgNnS	zaplnit
bludy	blud	k1gInPc7	blud
a	a	k8xC	a
děsuplnými	děsuplný	k2eAgInPc7d1	děsuplný
podněty	podnět	k1gInPc7	podnět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
mu	on	k3xPp3gMnSc3	on
zní	znět	k5eAaImIp3nS	znět
pronikavý	pronikavý	k2eAgInSc1d1	pronikavý
výkřik	výkřik	k1gInSc1	výkřik
ženského	ženský	k2eAgInSc2d1	ženský
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Uplyne	uplynout	k5eAaPmIp3nS	uplynout
blíže	blízce	k6eAd2	blízce
nespecifikovaná	specifikovaný	k2eNgFnSc1d1	nespecifikovaná
chvíle	chvíle	k1gFnSc1	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
Egeus	Egeus	k1gInSc1	Egeus
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Podívá	podívat	k5eAaPmIp3nS	podívat
se	se	k3xPyFc4	se
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
skříňku	skříňka	k1gFnSc4	skříňka
vedle	vedle	k7c2	vedle
hořící	hořící	k2eAgFnSc2d1	hořící
lampy	lampa	k1gFnSc2	lampa
<g/>
,	,	kIx,	,
nedovede	dovést	k5eNaPmIp3nS	dovést
si	se	k3xPyFc3	se
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nejasnou	jasný	k2eNgFnSc4d1	nejasná
děsivou	děsivý	k2eAgFnSc4d1	děsivá
předtuchu	předtucha	k1gFnSc4	předtucha
<g/>
.	.	kIx.	.
</s>
<s>
Zrak	zrak	k1gInSc1	zrak
mu	on	k3xPp3gMnSc3	on
utkví	utkvět	k5eAaPmIp3nS	utkvět
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
stránce	stránka	k1gFnSc6	stránka
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
podtržena	podtržen	k2eAgFnSc1d1	podtržena
věta	věta	k1gFnSc1	věta
básníka	básník	k1gMnSc2	básník
Íbn	Íbn	k1gFnSc1	Íbn
Zijáda	Zijáda	k1gFnSc1	Zijáda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Říkali	říkat	k5eAaImAgMnP	říkat
mi	já	k3xPp1nSc3	já
druhové	druhový	k2eAgNnSc4d1	druhové
<g/>
,	,	kIx,	,
že	že	k8xS	že
navštívím	navštívit	k5eAaPmIp1nS	navštívit
<g/>
-li	i	k?	-li
hrob	hrob	k1gInSc1	hrob
své	svůj	k3xOyFgFnSc2	svůj
milé	milá	k1gFnSc2	milá
<g/>
,	,	kIx,	,
mým	můj	k3xOp1gFnPc3	můj
strastem	strast	k1gFnPc3	strast
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
uleví	ulevit	k5eAaPmIp3nS	ulevit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
sluha	sluha	k1gMnSc1	sluha
a	a	k8xC	a
třaslavým	třaslavý	k2eAgInSc7d1	třaslavý
hlasem	hlas	k1gInSc7	hlas
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
zohyzděném	zohyzděný	k2eAgNnSc6d1	zohyzděné
těle	tělo	k1gNnSc6	tělo
v	v	k7c6	v
rubáši	rubáš	k1gInSc6	rubáš
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
hýbalo	hýbat	k5eAaImAgNnS	hýbat
-	-	kIx~	-
dosud	dosud	k6eAd1	dosud
žilo	žít	k5eAaImAgNnS	žít
<g/>
!	!	kIx.	!
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
prstem	prst	k1gInSc7	prst
na	na	k7c4	na
Egeovy	Egeův	k2eAgInPc4d1	Egeův
šaty	šat	k1gInPc4	šat
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
ulpěla	ulpět	k5eAaPmAgFnS	ulpět
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zděšený	zděšený	k2eAgInSc1d1	zděšený
Egeus	Egeus	k1gInSc1	Egeus
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
otevřít	otevřít	k5eAaPmF	otevřít
skříňku	skříňka	k1gFnSc4	skříňka
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozechvění	rozechvění	k1gNnSc6	rozechvění
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nedaří	dařit	k5eNaImIp3nS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Skříňka	skříňka	k1gFnSc1	skříňka
upadne	upadnout	k5eAaPmIp3nS	upadnout
na	na	k7c4	na
podlahu	podlaha	k1gFnSc4	podlaha
<g/>
,	,	kIx,	,
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
se	se	k3xPyFc4	se
a	a	k8xC	a
po	po	k7c6	po
podlaze	podlaha	k1gFnSc6	podlaha
se	se	k3xPyFc4	se
rozkutálí	rozkutálet	k5eAaPmIp3nS	rozkutálet
32	[number]	k4	32
bělostných	bělostný	k2eAgInPc2d1	bělostný
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
drží	držet	k5eAaImIp3nS	držet
tradice	tradice	k1gFnSc1	tradice
předchůdce	předchůdce	k1gMnSc2	předchůdce
hororu	horor	k1gInSc2	horor
<g/>
,	,	kIx,	,
gotického	gotický	k2eAgInSc2d1	gotický
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
oblíbeného	oblíbený	k2eAgInSc2d1	oblíbený
žánru	žánr	k1gInSc2	žánr
amerických	americký	k2eAgMnPc2d1	americký
a	a	k8xC	a
britských	britský	k2eAgMnPc2d1	britský
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Poe	Poe	k?	Poe
své	svůj	k3xOyFgInPc4	svůj
gotické	gotický	k2eAgInPc4d1	gotický
příběhy	příběh	k1gInPc4	příběh
učinil	učinit	k5eAaImAgInS	učinit
sofistikovanější	sofistikovaný	k2eAgInSc1d2	sofistikovanější
<g/>
,	,	kIx,	,
propracovanější	propracovaný	k2eAgInSc1d2	propracovanější
<g/>
,	,	kIx,	,
vystupňoval	vystupňovat	k5eAaPmAgInS	vystupňovat
hrůzu	hrůza	k1gFnSc4	hrůza
více	hodně	k6eAd2	hodně
realistickými	realistický	k2eAgInPc7d1	realistický
obrazy	obraz	k1gInPc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
povídka	povídka	k1gFnSc1	povídka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
autorovy	autorův	k2eAgFnPc4d1	autorova
nejděsivější	děsivý	k2eAgFnPc4d3	nejděsivější
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vypravěč	vypravěč	k1gMnSc1	vypravěč
podívá	podívat	k5eAaPmIp3nS	podívat
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
skříňku	skříňka	k1gFnSc4	skříňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
-	-	kIx~	-
jak	jak	k8xS	jak
podvědomě	podvědomě	k6eAd1	podvědomě
tuší	tušit	k5eAaImIp3nS	tušit
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sestřenčiny	sestřenčin	k2eAgInPc4d1	sestřenčin
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
přečte	přečíst	k5eAaPmIp3nS	přečíst
si	se	k3xPyFc3	se
podtrženou	podtržený	k2eAgFnSc4d1	podtržená
latinsky	latinsky	k6eAd1	latinsky
psanou	psaný	k2eAgFnSc4d1	psaná
větu	věta	k1gFnSc4	věta
básníka	básník	k1gMnSc4	básník
Íbn	Íbn	k1gFnSc4	Íbn
Zijáda	Zijáda	k1gFnSc1	Zijáda
"	"	kIx"	"
<g/>
Dicebant	Dicebant	k1gInSc1	Dicebant
mihi	mihi	k1gNnSc1	mihi
sodales	sodales	k1gInSc1	sodales
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
sepulchrum	sepulchrum	k1gNnSc1	sepulchrum
amicae	amicaat	k5eAaPmIp3nS	amicaat
visitarem	visitar	k1gInSc7	visitar
<g/>
,	,	kIx,	,
curas	curas	k1gInSc1	curas
meas	meas	k1gInSc1	meas
aliquantulum	aliquantulum	k1gInSc1	aliquantulum
fore	fore	k1gFnSc1	fore
levatas	levatas	k1gMnSc1	levatas
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Říkali	říkat	k5eAaImAgMnP	říkat
mi	já	k3xPp1nSc3	já
druhové	druhový	k2eAgNnSc4d1	druhové
<g/>
,	,	kIx,	,
že	že	k8xS	že
navštívím	navštívit	k5eAaPmIp1nS	navštívit
<g/>
-li	i	k?	-li
hrob	hrob	k1gInSc1	hrob
své	svůj	k3xOyFgFnSc2	svůj
milé	milá	k1gFnSc2	milá
<g/>
,	,	kIx,	,
mým	můj	k3xOp1gFnPc3	můj
strastem	strast	k1gFnPc3	strast
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
uleví	ulevit	k5eAaPmIp3nS	ulevit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
táže	tázat	k5eAaImIp3nS	tázat
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
mi	já	k3xPp1nSc3	já
ale	ale	k9	ale
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
těchto	tento	k3xDgInPc2	tento
slov	slovo	k1gNnPc2	slovo
vstávaly	vstávat	k5eAaImAgInP	vstávat
vlasy	vlas	k1gInPc1	vlas
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
mi	já	k3xPp1nSc3	já
tuhla	tuhnout	k5eAaImAgFnS	tuhnout
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Poe	Poe	k1gFnSc1	Poe
nezachází	zacházet	k5eNaImIp3nS	zacházet
do	do	k7c2	do
detailu	detail	k1gInSc2	detail
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
nepopisuje	popisovat	k5eNaImIp3nS	popisovat
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgNnSc7	jaký
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
Bereničiny	Bereničina	k1gFnPc1	Bereničina
zuby	zub	k1gInPc4	zub
do	do	k7c2	do
skříňky	skříňka	k1gFnSc2	skříňka
<g/>
,	,	kIx,	,
soudobí	soudobý	k2eAgMnPc1d1	soudobý
čtenáři	čtenář	k1gMnPc1	čtenář
byli	být	k5eAaImAgMnP	být
zděšeni	zděsit	k5eAaPmNgMnP	zděsit
násilností	násilnost	k1gFnSc7	násilnost
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
si	se	k3xPyFc3	se
redaktorovi	redaktor	k1gMnSc3	redaktor
amerického	americký	k2eAgInSc2d1	americký
časopisu	časopis	k1gInSc2	časopis
Southern	Southern	k1gMnSc1	Southern
Literary	Literara	k1gFnSc2	Literara
Messenger	Messenger	k1gMnSc1	Messenger
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
staví	stavit	k5eAaImIp3nS	stavit
vypravěče	vypravěč	k1gMnSc4	vypravěč
Egea	Egeus	k1gMnSc4	Egeus
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
podobném	podobný	k2eAgInSc6d1	podobný
transu	trans	k1gInSc6	trans
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestřenice	sestřenice	k1gFnSc1	sestřenice
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
živá	živý	k2eAgFnSc1d1	živá
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
jej	on	k3xPp3gMnSc4	on
dokončit	dokončit	k5eAaPmF	dokončit
hrůzný	hrůzný	k2eAgInSc4d1	hrůzný
čin	čin	k1gInSc4	čin
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
tedy	tedy	k8xC	tedy
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
,	,	kIx,	,
Egeus	Egeus	k1gMnSc1	Egeus
si	se	k3xPyFc3	se
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
spáchal	spáchat	k5eAaPmAgInS	spáchat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
tíživý	tíživý	k2eAgInSc1d1	tíživý
dohad	dohad	k1gInSc1	dohad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
povídky	povídka	k1gFnSc2	povídka
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestřenici	sestřenice	k1gFnSc3	sestřenice
bylo	být	k5eAaImAgNnS	být
vyrváno	vyrvat	k5eAaPmNgNnS	vyrvat
všech	všecek	k3xTgInPc2	všecek
32	[number]	k4	32
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Poe	Poe	k?	Poe
zároveň	zároveň	k6eAd1	zároveň
poprvé	poprvé	k6eAd1	poprvé
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
postavu	postava	k1gFnSc4	postava
sužovanou	sužovaný	k2eAgFnSc7d1	sužovaná
psychickou	psychický	k2eAgFnSc7d1	psychická
poruchou	porucha	k1gFnSc7	porucha
monománií	monománie	k1gFnPc2	monománie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
tvorbě	tvorba	k1gFnSc6	tvorba
vícekrát	vícekrát	k6eAd1	vícekrát
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
smrtelnost	smrtelnost	k1gFnSc4	smrtelnost
ve	v	k7c4	v
vícero	vícero	k1gNnSc4	vícero
Poeových	Poeův	k2eAgFnPc6d1	Poeova
povídkách	povídka	k1gFnPc6	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ohnivý	ohnivý	k2eAgMnSc1d1	ohnivý
kůň	kůň	k1gMnSc1	kůň
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Metzengerstein	Metzengerstein	k2eAgMnSc1d1	Metzengerstein
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Fakta	faktum	k1gNnSc2	faktum
případu	případ	k1gInSc2	případ
monsieura	monsieura	k1gFnSc1	monsieura
Valdemara	Valdemara	k1gFnSc1	Valdemara
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Facts	Facts	k1gInSc1	Facts
in	in	k?	in
the	the	k?	the
Case	Cas	k1gFnSc2	Cas
of	of	k?	of
M.	M.	kA	M.
Valdemar	Valdemara	k1gFnPc2	Valdemara
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Skokan	Skokan	k1gMnSc1	Skokan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Hop-Frog	Hop-Frog	k1gInSc1	Hop-Frog
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Egeus	Egeus	k1gInSc1	Egeus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sestřenice	sestřenice	k1gFnSc1	sestřenice
Berenice	Berenice	k1gFnSc1	Berenice
představují	představovat	k5eAaImIp3nP	představovat
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
charaktery	charakter	k1gInPc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Egeus	Egeus	k1gInSc1	Egeus
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
intelektuálnost	intelektuálnost	k1gFnSc4	intelektuálnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tichý	tichý	k2eAgMnSc1d1	tichý
<g/>
,	,	kIx,	,
osamocený	osamocený	k2eAgMnSc1d1	osamocený
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
obsese	obsese	k1gFnPc4	obsese
pouze	pouze	k6eAd1	pouze
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
jeho	jeho	k3xOp3gInSc4	jeho
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
přemýšlivost	přemýšlivost	k1gFnSc4	přemýšlivost
<g/>
.	.	kIx.	.
</s>
<s>
Berenice	Berenice	k1gFnSc1	Berenice
(	(	kIx(	(
<g/>
dokud	dokud	k8xS	dokud
ji	on	k3xPp3gFnSc4	on
nepostihne	postihnout	k5eNaPmIp3nS	postihnout
záhadné	záhadný	k2eAgNnSc1d1	záhadné
psychické	psychický	k2eAgNnSc1d1	psychické
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vitální	vitální	k2eAgNnSc4d1	vitální
děvče	děvče	k1gNnSc4	děvče
radující	radující	k2eAgNnSc4d1	radující
se	se	k3xPyFc4	se
ze	z	k7c2	z
života	život	k1gInSc2	život
a	a	k8xC	a
překypující	překypující	k2eAgFnSc7d1	překypující
čilostí	čilost	k1gFnSc7	čilost
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
jediným	jediný	k2eAgInSc7d1	jediný
smyslem	smysl	k1gInSc7	smysl
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
autorových	autorův	k2eAgFnPc2d1	autorova
ženských	ženský	k2eAgFnPc2d1	ženská
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Egeus	Egeus	k1gInSc1	Egeus
částečně	částečně	k6eAd1	částečně
ztratí	ztratit	k5eAaPmIp3nS	ztratit
o	o	k7c4	o
Berenici	Berenice	k1gFnSc4	Berenice
zájem	zájem	k1gInSc4	zájem
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ona	onen	k3xDgFnSc1	onen
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
;	;	kIx,	;
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
objektem	objekt	k1gInSc7	objekt
k	k	k7c3	k
analýze	analýza	k1gFnSc3	analýza
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
obdivu	obdiv	k1gInSc2	obdiv
<g/>
.	.	kIx.	.
</s>
<s>
Mimochodem	mimochod	k1gInSc7	mimochod
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
autor	autor	k1gMnSc1	autor
vypravěče	vypravěč	k1gMnSc4	vypravěč
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
často	často	k6eAd1	často
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgNnPc2d1	opakující
témat	téma	k1gNnPc2	téma
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
lze	lze	k6eAd1	lze
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
<g/>
"	"	kIx"	"
nalézt	nalézt	k5eAaPmF	nalézt
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
krásné	krásný	k2eAgFnSc2d1	krásná
dívky	dívka	k1gFnSc2	dívka
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
eseji	esej	k1gFnSc6	esej
"	"	kIx"	"
<g/>
Filosofie	filosofie	k1gFnSc1	filosofie
básnické	básnický	k2eAgFnSc2d1	básnická
skladby	skladba	k1gFnSc2	skladba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Ligeia	Ligeia	k1gFnSc1	Ligeia
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Morella	Morella	k1gFnSc1	Morella
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Medailon	medailon	k1gInSc1	medailon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbení	pohřbení	k1gNnSc4	pohřbení
zaživa	zaživa	k6eAd1	zaživa
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Zánik	zánik	k1gInSc4	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sud	sud	k1gInSc1	sud
vína	víno	k1gNnSc2	víno
amontilladského	amontilladský	k2eAgNnSc2d1	amontilladský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Psychické	psychický	k2eAgNnSc1d1	psychické
onemocnění	onemocnění	k1gNnSc1	onemocnění
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Zánik	zánik	k1gInSc4	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zrádné	zrádný	k2eAgNnSc4d1	zrádné
srdce	srdce	k1gNnSc4	srdce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Metoda	metoda	k1gFnSc1	metoda
doktora	doktor	k1gMnSc2	doktor
Téra	Térus	k1gMnSc2	Térus
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
Péra	péro	k1gNnSc2	péro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Katalepsie	katalepsie	k1gFnSc1	katalepsie
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Zánik	zánik	k1gInSc4	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
nebo	nebo	k8xC	nebo
slovensky	slovensky	k6eAd1	slovensky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
sbírkách	sbírka	k1gFnPc6	sbírka
či	či	k8xC	či
antologiích	antologie	k1gFnPc6	antologie
<g/>
:	:	kIx,	:
Anděl	Anděla	k1gFnPc2	Anděla
pitvornosti	pitvornost	k1gFnSc2	pitvornost
(	(	kIx(	(
<g/>
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Hrůzný	hrůzný	k2eAgMnSc1d1	hrůzný
stařec	stařec	k1gMnSc1	stařec
<g/>
:	:	kIx,	:
Deset	deset	k4xCc4	deset
světových	světový	k2eAgInPc2d1	světový
horrorů	horror	k1gInPc2	horror
(	(	kIx(	(
<g/>
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Jáma	jáma	k1gFnSc1	jáma
&	&	k?	&
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
fantastické	fantastický	k2eAgInPc1d1	fantastický
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Odeon	odeon	k1gInSc1	odeon
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
a	a	k8xC	a
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
KMa	KMa	k1gFnSc1	KMa
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Lupiči	lupič	k1gMnPc1	lupič
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
:	:	kIx,	:
Světové	světový	k2eAgInPc1d1	světový
horory	horor	k1gInPc1	horor
(	(	kIx(	(
<g/>
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
:	:	kIx,	:
Horrory	horror	k1gInPc1	horror
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
<g />
.	.	kIx.	.
</s>
<s>
děsivé	děsivý	k2eAgInPc1d1	děsivý
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Hynek	Hynek	k1gMnSc1	Hynek
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Zrádné	zrádný	k2eAgNnSc1d1	zrádné
srdce	srdce	k1gNnSc1	srdce
<g/>
:	:	kIx,	:
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
vázaná	vázané	k1gNnPc4	vázané
s	s	k7c7	s
papírovým	papírový	k2eAgInSc7d1	papírový
přebalem	přebal	k1gInSc7	přebal
<g/>
,	,	kIx,	,
676	[number]	k4	676
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
(	(	kIx(	(
<g/>
Tatran	Tatran	k1gInSc1	Tatran
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
295	[number]	k4	295
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
</s>
