<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Velkou	velký	k2eAgFnSc4d1	velká
proletářskou	proletářský	k2eAgFnSc4d1	proletářská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
nepřetržité	přetržitý	k2eNgFnSc2d1	nepřetržitá
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
masové	masový	k2eAgFnSc2d1	masová
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vymýtit	vymýtit	k5eAaPmF	vymýtit
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
