<s>
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Alt	alt	k1gInSc1	alt
Leskau	Leskaus	k1gInSc2	Leskaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
a	a	k8xC	a
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
také	také	k9	také
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
relativně	relativně	k6eAd1	relativně
blízko	blízko	k7c2	blízko
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Starý	Brno-Starý	k2eAgInSc4d1	Brno-Starý
Lískovec	Lískovec	k1gInSc4	Lískovec
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
číslo	číslo	k1gNnSc1	číslo
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
již	již	k6eAd1	již
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc4	část
Brno-Starý	Brno-Starý	k2eAgInSc4d1	Brno-Starý
Lískovec	Lískovec	k1gInSc4	Lískovec
územně	územně	k6eAd1	územně
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
správní	správní	k2eAgFnSc2d1	správní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Brno-Nový	Brno-Nový	k2eAgInSc1d1	Brno-Nový
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
malá	malý	k2eAgFnSc1d1	malá
okrajová	okrajový	k2eAgFnSc1d1	okrajová
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
přešla	přejít	k5eAaPmAgFnS	přejít
k	k	k7c3	k
území	území	k1gNnSc3	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Nový	Brno-Nový	k2eAgInSc1d1	Brno-Nový
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zase	zase	k9	zase
předala	předat	k5eAaPmAgFnS	předat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
malou	malý	k2eAgFnSc4d1	malá
okrajovou	okrajový	k2eAgFnSc4d1	okrajová
část	část	k1gFnSc4	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Nový	nový	k2eAgInSc4d1	nový
Lískovec	Lískovec	k1gInSc4	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
Zástavbu	zástavba	k1gFnSc4	zástavba
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážně	převážně	k6eAd1	převážně
panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
postavené	postavený	k2eAgInPc1d1	postavený
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
severně	severně	k6eAd1	severně
a	a	k8xC	a
západně	západně	k6eAd1	západně
od	od	k7c2	od
vesnické	vesnický	k2eAgFnSc2d1	vesnická
rodinné	rodinný	k2eAgFnSc2d1	rodinná
zástavby	zástavba	k1gFnSc2	zástavba
původní	původní	k2eAgFnSc2d1	původní
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
koupaliště	koupaliště	k1gNnSc1	koupaliště
a	a	k8xC	a
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
Tatran	Tatran	k1gInSc1	Tatran
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
už	už	k6eAd1	už
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
sportovní	sportovní	k2eAgFnPc1d1	sportovní
prostory	prostora	k1gFnPc1	prostora
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Dělnické	dělnický	k2eAgFnSc2d1	Dělnická
tělovýchovné	tělovýchovný	k2eAgFnSc2d1	Tělovýchovná
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
otevřena	otevřen	k2eAgFnSc1d1	otevřena
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
části	část	k1gFnSc6	část
katastru	katastr	k1gInSc2	katastr
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejnovější	nový	k2eAgFnSc1d3	nejnovější
vícepatrová	vícepatrový	k2eAgFnSc1d1	vícepatrová
budova-poliklinika	budovaoliklinik	k1gMnSc2	budova-poliklinik
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnSc1d2	starší
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
FAKULTNÍ	fakultní	k2eAgFnSc1d1	fakultní
NEMOCNICE	nemocnice	k1gFnSc1	nemocnice
BRNO	Brno	k1gNnSc4	Brno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cca	cca	kA	cca
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2012	[number]	k4	2012
již	již	k9	již
jen	jen	k9	jen
"	"	kIx"	"
<g/>
FN	FN	kA	FN
BRNO	Brno	k1gNnSc1	Brno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
porodnice	porodnice	k1gFnSc1	porodnice
fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
-	-	kIx~	-
2008	[number]	k4	2008
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
původně	původně	k6eAd1	původně
volných	volný	k2eAgFnPc2d1	volná
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
archivu	archiv	k1gInSc2	archiv
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
i	i	k8xC	i
nové	nový	k2eAgNnSc1d1	nové
nákupní	nákupní	k2eAgNnSc1d1	nákupní
centrum	centrum	k1gNnSc1	centrum
Campus	Campus	k1gInSc1	Campus
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
jezdí	jezdit	k5eAaImIp3nP	jezdit
tramvaje	tramvaj	k1gFnPc1	tramvaj
číslo	číslo	k1gNnSc1	číslo
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
a	a	k8xC	a
a	a	k8xC	a
několik	několik	k4yIc1	několik
linek	linka	k1gFnPc2	linka
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
trolejbusů	trolejbus	k1gInPc2	trolejbus
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Elišky	Eliška	k1gFnSc2	Eliška
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Kousek	kousek	k6eAd1	kousek
pod	pod	k7c7	pod
kostelem	kostel	k1gInSc7	kostel
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
naproti	naproti	k6eAd1	naproti
školní	školní	k2eAgFnPc4d1	školní
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
místní	místní	k2eAgInSc1d1	místní
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Zámeček	zámeček	k1gInSc1	zámeček
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Lískovci	Lískovec	k1gInSc6	Lískovec
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
starý	starý	k2eAgInSc1d1	starý
panský	panský	k2eAgInSc1d1	panský
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
Lískovci	Lískovec	k1gInSc6	Lískovec
zřídily	zřídit	k5eAaPmAgFnP	zřídit
starobrněnské	starobrněnský	k2eAgFnPc1d1	starobrněnský
cisterciačky	cisterciačka	k1gFnPc1	cisterciačka
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
vždy	vždy	k6eAd1	vždy
své	svůj	k3xOyFgNnSc4	svůj
správce	správce	k1gMnSc1	správce
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
soukromý	soukromý	k2eAgInSc1d1	soukromý
majetek	majetek	k1gInSc1	majetek
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
náboženský	náboženský	k2eAgInSc1d1	náboženský
fond	fond	k1gInSc1	fond
i	i	k8xC	i
následní	následní	k2eAgMnPc1d1	následní
majitelé	majitel	k1gMnPc1	majitel
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vždycky	vždycky	k6eAd1	vždycky
viděli	vidět	k5eAaImAgMnP	vidět
především	především	k9	především
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
údajný	údajný	k2eAgInSc1d1	údajný
pobyt	pobyt	k1gInSc1	pobyt
Elišky	Eliška	k1gFnSc2	Eliška
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
patří	patřit	k5eAaImIp3nS	patřit
spíše	spíše	k9	spíše
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
nikde	nikde	k6eAd1	nikde
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
Lískovci	Lískovec	k1gInSc6	Lískovec
vůbec	vůbec	k9	vůbec
pobývala	pobývat	k5eAaImAgNnP	pobývat
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
když	když	k8xS	když
náležel	náležet	k5eAaImAgMnS	náležet
k	k	k7c3	k
panství	panství	k1gNnSc3	panství
její	její	k3xOp3gFnSc2	její
největší	veliký	k2eAgFnSc2d3	veliký
sokyně	sokyně	k1gFnSc2	sokyně
<g/>
,	,	kIx,	,
Elišky	Eliška	k1gFnSc2	Eliška
Rejčky	Rejčka	k1gFnSc2	Rejčka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
neměla	mít	k5eNaImAgFnS	mít
z	z	k7c2	z
duše	duše	k1gFnSc2	duše
ráda	rád	k2eAgFnSc1d1	ráda
a	a	k8xC	a
vyhýbala	vyhýbat	k5eAaImAgFnS	vyhýbat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
s	s	k7c7	s
názvem	název	k1gInSc7	název
zdejší	zdejší	k2eAgFnSc2d1	zdejší
ulice	ulice	k1gFnSc2	ulice
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
pověst	pověst	k1gFnSc1	pověst
jen	jen	k9	jen
pramálo	pramálo	k6eAd1	pramálo
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
totiž	totiž	k9	totiž
založila	založit	k5eAaPmAgFnS	založit
klášter	klášter	k1gInSc4	klášter
dominikánek	dominikánka	k1gFnPc2	dominikánka
u	u	k7c2	u
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1312	[number]	k4	1312
na	na	k7c6	na
Pekařské	pekařský	k2eAgFnSc6d1	Pekařská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nemocnice	nemocnice	k1gFnSc1	nemocnice
u	u	k7c2	u
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
Elišky	Eliška	k1gFnSc2	Eliška
Rejčky	Rejčka	k1gFnSc2	Rejčka
neměla	mít	k5eNaImAgFnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
,	,	kIx,	,
snad	snad	k9	snad
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
její	její	k3xOp3gFnSc1	její
konkurentka	konkurentka	k1gFnSc1	konkurentka
založila	založit	k5eAaPmAgFnS	založit
onen	onen	k3xDgInSc4	onen
klášter	klášter	k1gInSc4	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
r.	r.	kA	r.
1323	[number]	k4	1323
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
na	na	k7c6	na
Mendlově	Mendlův	k2eAgNnSc6d1	Mendlovo
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
věnovala	věnovat	k5eAaImAgFnS	věnovat
mu	on	k3xPp3gNnSc3	on
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
panstvím	panství	k1gNnSc7	panství
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
města	město	k1gNnSc2	město
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
č.	č.	k?	č.
6	[number]	k4	6
jedoucí	jedoucí	k2eAgInSc1d1	jedoucí
ze	z	k7c2	z
smyčky	smyčka	k1gFnSc2	smyčka
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
do	do	k7c2	do
Králova	Králův	k2eAgNnSc2d1	Královo
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
či	či	k8xC	či
linky	linka	k1gFnPc4	linka
č.	č.	k?	č.
8	[number]	k4	8
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
Lískovcem	Lískovec	k1gInSc7	Lískovec
a	a	k8xC	a
Líšní	Líšeň	k1gFnSc7	Líšeň
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pomocí	pomocí	k7c2	pomocí
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
č.	č.	k?	č.
25	[number]	k4	25
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgNnSc4d1	zajišťující
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
Lískovcem	Lískovec	k1gInSc7	Lískovec
a	a	k8xC	a
Líšní	Líšeň	k1gFnSc7	Líšeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
č.	č.	k?	č.
37	[number]	k4	37
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
Lískovcem	Lískovec	k1gInSc7	Lískovec
a	a	k8xC	a
Mendlovým	Mendlův	k2eAgNnSc7d1	Mendlovo
náměstím	náměstí	k1gNnSc7	náměstí
přes	přes	k7c4	přes
Kohoutovice	Kohoutovice	k1gFnPc4	Kohoutovice
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
přes	přes	k7c4	přes
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
projíždí	projíždět	k5eAaImIp3nP	projíždět
autobusy	autobus	k1gInPc1	autobus
č.	č.	k?	č.
50	[number]	k4	50
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Komárov	Komárov	k1gInSc1	Komárov
s	s	k7c7	s
Bystrcem	Bystrec	k1gInSc7	Bystrec
a	a	k8xC	a
autobus	autobus	k1gInSc1	autobus
č.	č.	k?	č.
82	[number]	k4	82
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Lískovec	Lískovec	k1gInSc1	Lískovec
s	s	k7c7	s
Vinohrady	Vinohrady	k1gInPc7	Vinohrady
<g/>
,	,	kIx,	,
či	či	k8xC	či
autobusy	autobus	k1gInPc1	autobus
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
blízkým	blízký	k2eAgInSc7d1	blízký
venkovem	venkov	k1gInSc7	venkov
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
autobusy	autobus	k1gInPc1	autobus
č.	č.	k?	č.
401	[number]	k4	401
<g/>
,	,	kIx,	,
402	[number]	k4	402
<g/>
,	,	kIx,	,
403	[number]	k4	403
<g/>
,	,	kIx,	,
404	[number]	k4	404
a	a	k8xC	a
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
expresní	expresní	k2eAgFnSc1d1	expresní
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
E	E	kA	E
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
spojuje	spojovat	k5eAaImIp3nS	spojovat
Černovickou	Černovický	k2eAgFnSc4d1	Černovická
terasu	terasa	k1gFnSc4	terasa
s	s	k7c7	s
nově	nově	k6eAd1	nově
budovaným	budovaný	k2eAgNnSc7d1	budované
sídlištěm	sídliště	k1gNnSc7	sídliště
Kamechy	Kamech	k1gInPc1	Kamech
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
Bystrce	Bystrc	k1gFnSc2	Bystrc
a	a	k8xC	a
Žebětína	Žebětín	k1gInSc2	Žebětín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
je	být	k5eAaImIp3nS	být
doprava	doprava	k1gFnSc1	doprava
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
nočními	noční	k2eAgFnPc7d1	noční
autobusovými	autobusový	k2eAgFnPc7d1	autobusová
linkami	linka	k1gFnPc7	linka
N91	N91	k1gFnSc2	N91
a	a	k8xC	a
N	N	kA	N
<g/>
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
Lískovci	Lískovec	k1gInPc7	Lískovec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1314	[number]	k4	1314
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgNnSc4d2	menší
než	než	k8xS	než
někdejší	někdejší	k2eAgNnSc4d1	někdejší
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
této	tento	k3xDgFnSc2	tento
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
připojené	připojený	k2eAgFnSc2d1	připojená
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
moderní	moderní	k2eAgInSc4d1	moderní
katastr	katastr	k1gInSc4	katastr
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
většina	většina	k1gFnSc1	většina
sousedního	sousední	k2eAgInSc2d1	sousední
moderního	moderní	k2eAgInSc2d1	moderní
katastru	katastr	k1gInSc2	katastr
Nového	Nového	k2eAgInSc2d1	Nového
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
část	část	k1gFnSc4	část
moderního	moderní	k2eAgInSc2d1	moderní
katastru	katastr	k1gInSc2	katastr
Bohunic	Bohunice	k1gFnPc2	Bohunice
se	se	k3xPyFc4	se
západní	západní	k2eAgInSc1d1	západní
stranou	strana	k1gFnSc7	strana
ulice	ulice	k1gFnSc1	ulice
Humenná	Humenné	k1gNnPc1	Humenné
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepatrné	patrný	k2eNgFnPc1d1	patrný
části	část	k1gFnPc1	část
moderních	moderní	k2eAgInPc2d1	moderní
katastrů	katastr	k1gInPc2	katastr
Bosonoh	Bosonoha	k1gFnPc2	Bosonoha
a	a	k8xC	a
Ostopovic	Ostopovice	k1gFnPc2	Ostopovice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
katastrálních	katastrální	k2eAgFnPc6d1	katastrální
změnách	změna	k1gFnPc6	změna
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
provedených	provedený	k2eAgInPc2d1	provedený
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nových	nový	k2eAgNnPc2d1	nové
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Nový	nový	k2eAgInSc1d1	nový
Lískovec	Lískovec	k1gInSc1	Lískovec
a	a	k8xC	a
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
k.	k.	k?	k.
ú.	ú.	k?	ú.
Starý	starý	k2eAgInSc4d1	starý
Lískovec	Lískovec	k1gInSc4	Lískovec
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
vedle	vedle	k7c2	vedle
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgInSc2d1	celý
moderního	moderní	k2eAgInSc2d1	moderní
k.	k.	k?	k.
ú.	ú.	k?	ú.
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
i	i	k8xC	i
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
moderního	moderní	k2eAgInSc2d1	moderní
k.	k.	k?	k.
ú.	ú.	k?	ú.
Nový	nový	k2eAgInSc4d1	nový
Lískovec	Lískovec	k1gInSc4	Lískovec
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
především	především	k6eAd1	především
většinu	většina	k1gFnSc4	většina
dnešního	dnešní	k2eAgNnSc2d1	dnešní
panelového	panelový	k2eAgNnSc2d1	panelové
sídliště	sídliště	k1gNnSc2	sídliště
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Vrch	vrch	k1gInSc1	vrch
a	a	k8xC	a
západ	západ	k1gInSc1	západ
moderního	moderní	k2eAgInSc2d1	moderní
k.	k.	k?	k.
ú.	ú.	k?	ú.
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Lískovci	Lískovec	k1gInSc6	Lískovec
a	a	k8xC	a
Bohunicích	Bohunice	k1gFnPc6	Bohunice
zahájena	zahájit	k5eAaPmNgFnS	zahájit
panelová	panelový	k2eAgFnSc1d1	panelová
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sousedních	sousední	k2eAgFnPc2d1	sousední
Bohunic	Bohunice	k1gFnPc2	Bohunice
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Lískovci	Lískovec	k1gInSc6	Lískovec
provedena	provést	k5eAaPmNgFnS	provést
mnohem	mnohem	k6eAd1	mnohem
citlivěji	citlivě	k6eAd2	citlivě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
původní	původní	k2eAgFnPc4d1	původní
zástavby	zástavba	k1gFnPc4	zástavba
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
Přesah	přesah	k1gInSc1	přesah
bohunické	bohunický	k2eAgFnSc2d1	Bohunická
části	část	k1gFnSc2	část
budovaného	budovaný	k2eAgNnSc2d1	budované
sídliště	sídliště	k1gNnSc2	sídliště
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
katastr	katastr	k1gInSc4	katastr
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
změně	změna	k1gFnSc3	změna
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Bohunicemi	Bohunice	k1gFnPc7	Bohunice
a	a	k8xC	a
Starým	starý	k2eAgInSc7d1	starý
Lískovcem	Lískovec	k1gInSc7	Lískovec
<g/>
,	,	kIx,	,
provedenou	provedený	k2eAgFnSc7d1	provedená
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
změny	změna	k1gFnSc2	změna
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
projednán	projednat	k5eAaPmNgInS	projednat
s	s	k7c7	s
odborem	odbor	k1gInSc7	odbor
Vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
věcí	věc	k1gFnPc2	věc
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nová	nový	k2eAgFnSc1d1	nová
hranice	hranice	k1gFnSc1	hranice
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Bohunicím	Bohunice	k1gFnPc3	Bohunice
ze	z	k7c2	z
Starého	staré	k1gNnSc2	staré
Lískovce	Lískovec	k1gInSc2	Lískovec
přešlo	přejít	k5eAaPmAgNnS	přejít
necelých	celý	k2eNgInPc2d1	necelý
14	[number]	k4	14
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většinu	většina	k1gFnSc4	většina
nové	nový	k2eAgFnSc2d1	nová
hranice	hranice	k1gFnSc2	hranice
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
ulice	ulice	k1gFnSc1	ulice
Osová	osový	k2eAgFnSc1d1	Osová
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
z	z	k7c2	z
402,9	[number]	k4	402,9
hektarů	hektar	k1gInPc2	hektar
na	na	k7c4	na
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
389,1	[number]	k4	389,1
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
katastry	katastr	k1gInPc7	katastr
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
z	z	k7c2	z
1108	[number]	k4	1108
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
1326	[number]	k4	1326
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
řešila	řešit	k5eAaImAgFnS	řešit
i	i	k9	i
výrazná	výrazný	k2eAgFnSc1d1	výrazná
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
a	a	k8xC	a
Novým	nový	k2eAgInSc7d1	nový
Lískovcem	Lískovec	k1gInSc7	Lískovec
<g/>
,	,	kIx,	,
navržená	navržený	k2eAgFnSc1d1	navržená
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přesahu	přesah	k1gInSc2	přesah
panelového	panelový	k2eAgNnSc2d1	panelové
sídliště	sídliště	k1gNnSc2	sídliště
Nový	nový	k2eAgInSc1d1	nový
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
,	,	kIx,	,
budovaného	budovaný	k2eAgMnSc4d1	budovaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
katastru	katastr	k1gInSc2	katastr
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navržen	k2eAgNnSc1d1	navrženo
posunout	posunout	k5eAaPmF	posunout
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
katastry	katastr	k1gInPc4	katastr
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
okraji	okraj	k1gInSc3	okraj
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
Jihlavské	jihlavský	k2eAgFnSc6d1	Jihlavská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
katastr	katastr	k1gInSc4	katastr
Nového	Nového	k2eAgInSc2d1	Nového
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
zvětšit	zvětšit	k5eAaPmF	zvětšit
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
necelých	celý	k2eNgInPc2d1	necelý
113	[number]	k4	113
hektarů	hektar	k1gInPc2	hektar
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
166	[number]	k4	166
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
katastry	katastr	k1gInPc7	katastr
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
měla	mít	k5eAaImAgFnS	mít
zkrátit	zkrátit	k5eAaPmF	zkrátit
na	na	k7c4	na
1020	[number]	k4	1020
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
projednán	projednat	k5eAaPmNgInS	projednat
s	s	k7c7	s
odborem	odbor	k1gInSc7	odbor
Vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
věcí	věc	k1gFnPc2	věc
Národního	národní	k2eAgNnSc2d1	národní
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
zástupcem	zástupce	k1gMnSc7	zástupce
firmy	firma	k1gFnSc2	firma
Brnoprojekt	Brnoprojekt	k1gInSc1	Brnoprojekt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
následně	následně	k6eAd1	následně
projednala	projednat	k5eAaPmAgFnS	projednat
a	a	k8xC	a
schválila	schválit	k5eAaPmAgFnS	schválit
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1979	[number]	k4	1979
rada	rada	k1gFnSc1	rada
Obvodního	obvodní	k2eAgInSc2d1	obvodní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Brno	Brno	k1gNnSc4	Brno
I.	I.	kA	I.
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
schválen	schválit	k5eAaPmNgInS	schválit
i	i	k9	i
na	na	k7c4	na
schůzi	schůze	k1gFnSc4	schůze
rady	rada	k1gFnSc2	rada
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
realizována	realizován	k2eAgFnSc1d1	realizována
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Lískovci	Lískovec	k1gInSc3	Lískovec
přešlo	přejít	k5eAaPmAgNnS	přejít
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
necelých	celý	k2eNgInPc2d1	necelý
53	[number]	k4	53
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
změnám	změna	k1gFnPc3	změna
katastrálních	katastrální	k2eAgFnPc2d1	katastrální
hranic	hranice	k1gFnPc2	hranice
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
28	[number]	k4	28
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
2002	[number]	k4	2002
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
drobná	drobný	k2eAgFnSc1d1	drobná
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
Lískovcem	Lískovec	k1gInSc7	Lískovec
u	u	k7c2	u
severního	severní	k2eAgInSc2d1	severní
okraje	okraj	k1gInSc2	okraj
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
Jihlavské	jihlavský	k2eAgFnSc6d1	Jihlavská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Lískovci	Lískovec	k1gInSc3	Lískovec
přešlo	přejít	k5eAaPmAgNnS	přejít
23	[number]	k4	23
m2	m2	k4	m2
<g/>
,	,	kIx,	,
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jižní	jižní	k2eAgFnSc7d1	jižní
částí	část	k1gFnSc7	část
budovy	budova	k1gFnSc2	budova
s	s	k7c7	s
novolískoveckým	novolískovecký	k2eAgNnSc7d1	novolískovecký
číslem	číslo	k1gNnSc7	číslo
popisným	popisný	k2eAgNnSc7d1	popisné
441	[number]	k4	441
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
2011	[number]	k4	2011
pak	pak	k9	pak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
další	další	k2eAgFnSc1d1	další
změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
Lískovcem	Lískovec	k1gInSc7	Lískovec
a	a	k8xC	a
Bohunicemi	Bohunice	k1gFnPc7	Bohunice
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Osová	osový	k2eAgFnSc1d1	Osová
a	a	k8xC	a
Jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
Lískovce	Lískovec	k1gInSc2	Lískovec
přešlo	přejít	k5eAaPmAgNnS	přejít
k	k	k7c3	k
Bohunicím	Bohunice	k1gFnPc3	Bohunice
4221	[number]	k4	4221
m2	m2	k4	m2
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
Bohunic	Bohunice	k1gFnPc2	Bohunice
ke	k	k7c3	k
Starému	starý	k2eAgInSc3d1	starý
Lískovci	Lískovec	k1gInSc3	Lískovec
jen	jen	k9	jen
3609	[number]	k4	3609
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
