<p>
<s>
Gastronomie	gastronomie	k1gFnSc1	gastronomie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
v	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
pojetí	pojetí	k1gNnSc6	pojetí
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kuchařské	kuchařský	k2eAgNnSc4d1	kuchařské
nebo	nebo	k8xC	nebo
kulinářské	kulinářský	k2eAgNnSc4d1	kulinářské
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
culinaire	culinair	k1gInSc5	culinair
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
gastronomie	gastronomie	k1gFnSc1	gastronomie
(	(	kIx(	(
<g/>
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
žaludku	žaludek	k1gInSc6	žaludek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
-	-	kIx~	-
nikoli	nikoli	k9	nikoli
navíc	navíc	k6eAd1	navíc
-	-	kIx~	-
nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
přípravy	příprava	k1gFnSc2	příprava
jídel	jídlo	k1gNnPc2	jídlo
(	(	kIx(	(
<g/>
vaření	vaření	k1gNnPc2	vaření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
konzumaci	konzumace	k1gFnSc6	konzumace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
(	(	kIx(	(
<g/>
v	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
)	)	kIx)	)
součást	součást	k1gFnSc1	součást
sektoru	sektor	k1gInSc2	sektor
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umění	umění	k1gNnSc1	umění
kuchařské	kuchařský	k2eAgNnSc1d1	kuchařské
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
užité	užitý	k2eAgNnSc4d1	užité
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gastronomovým	gastronomův	k2eAgInSc7d1	gastronomův
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
učinit	učinit	k5eAaPmF	učinit
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
konzumace	konzumace	k1gFnSc2	konzumace
jídla	jídlo	k1gNnSc2	jídlo
akt	akt	k1gInSc1	akt
kultivovaný	kultivovaný	k2eAgMnSc1d1	kultivovaný
a	a	k8xC	a
povýšit	povýšit	k5eAaPmF	povýšit
tím	ten	k3xDgNnSc7	ten
pojídajícího	pojídající	k2eAgMnSc4d1	pojídající
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
ušlechtilou	ušlechtilý	k2eAgFnSc4d1	ušlechtilá
bytost	bytost	k1gFnSc4	bytost
<g/>
,	,	kIx,	,
pozvednout	pozvednout	k5eAaPmF	pozvednout
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
úrovně	úroveň	k1gFnSc2	úroveň
animálního	animální	k2eAgInSc2d1	animální
stroje	stroj	k1gInSc2	stroj
na	na	k7c4	na
zažívání	zažívání	k1gNnSc4	zažívání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
kuchařského	kuchařský	k2eAgNnSc2d1	kuchařské
umění	umění	k1gNnSc2	umění
konkrétně	konkrétně	k6eAd1	konkrétně
je	být	k5eAaImIp3nS	být
připravit	připravit	k5eAaPmF	připravit
pokrm	pokrm	k1gInSc4	pokrm
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zasytil	zasytit	k5eAaPmAgMnS	zasytit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
ducha	duch	k1gMnSc2	duch
konzumentů	konzument	k1gMnPc2	konzument
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
základní	základní	k2eAgFnPc4d1	základní
složky	složka	k1gFnPc4	složka
kuchařského	kuchařský	k2eAgNnSc2d1	kuchařské
řemesla	řemeslo	k1gNnSc2	řemeslo
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kultivace	kultivace	k1gFnSc1	kultivace
chuti	chuť	k1gFnSc2	chuť
pokrmu	pokrm	k1gInSc2	pokrm
</s>
</p>
<p>
<s>
kultivace	kultivace	k1gFnSc1	kultivace
vzhledu	vzhled	k1gInSc2	vzhled
pokrmu	pokrm	k1gInSc2	pokrm
</s>
</p>
<p>
<s>
kultivace	kultivace	k1gFnSc1	kultivace
vůně	vůně	k1gFnSc2	vůně
pokrmu	pokrm	k1gInSc2	pokrm
</s>
</p>
<p>
<s>
kultivace	kultivace	k1gFnSc1	kultivace
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
pokrm	pokrm	k1gInSc1	pokrm
podáván	podáván	k2eAgInSc1d1	podáván
</s>
</p>
<p>
<s>
kultivace	kultivace	k1gFnSc1	kultivace
ideového	ideový	k2eAgInSc2d1	ideový
podtextu	podtext	k1gInSc2	podtext
pokrmu	pokrm	k1gInSc2	pokrm
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
cizokrajné	cizokrajný	k2eAgNnSc1d1	cizokrajné
pojmenování	pojmenování	k1gNnSc1	pojmenování
snižuje	snižovat	k5eAaImIp3nS	snižovat
averzi	averze	k1gFnSc4	averze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Gastronomické	gastronomický	k2eAgFnSc2d1	gastronomická
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
hotelů	hotel	k1gInPc2	hotel
a	a	k8xC	a
restaurací	restaurace	k1gFnPc2	restaurace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
kuchařů	kuchař	k1gMnPc2	kuchař
a	a	k8xC	a
cukrářů	cukrář	k1gMnPc2	cukrář
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
sommelierů	sommelier	k1gMnPc2	sommelier
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
barmanská	barmanský	k2eAgFnSc1d1	barmanská
asociace	asociace	k1gFnSc1	asociace
</s>
</p>
<p>
<s>
HO	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
<g/>
RE	re	k9	re
<g/>
.	.	kIx.	.
<g/>
KA	KA	kA	KA
ČR	ČR	kA	ČR
-	-	kIx~	-
Sdružením	sdružení	k1gNnSc7	sdružení
podnikatelů	podnikatel	k1gMnPc2	podnikatel
v	v	k7c4	v
pohostinství	pohostinství	k1gNnSc4	pohostinství
a	a	k8xC	a
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
gastronomie	gastronomie	k1gFnSc1	gastronomie
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
kulinářský	kulinářský	k2eAgInSc1d1	kulinářský
institut	institut	k1gInSc1	institut
</s>
</p>
<p>
<s>
===	===	k?	===
Slovensko	Slovensko	k1gNnSc4	Slovensko
===	===	k?	===
</s>
</p>
<p>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
zväz	zväz	k1gInSc1	zväz
kuchárov	kuchárov	k1gInSc1	kuchárov
a	a	k8xC	a
cukrárov	cukrárov	k1gInSc1	cukrárov
</s>
</p>
<p>
<s>
Asociácia	Asociácia	k1gFnSc1	Asociácia
somelierov	somelierovo	k1gNnPc2	somelierovo
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Slovak	Slovak	k6eAd1	Slovak
coffee	coffeat	k5eAaPmIp3nS	coffeat
associacion	associacion	k1gInSc1	associacion
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
barmanská	barmanský	k2eAgFnSc1d1	barmanská
asociácia	asociácia	k1gFnSc1	asociácia
(	(	kIx(	(
<g/>
SkBA	SkBA	k1gFnSc1	SkBA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zväz	Zväz	k1gInSc4	Zväz
hotelov	hotelovo	k1gNnPc2	hotelovo
a	a	k8xC	a
reštaurácií	reštaurácia	k1gFnPc2	reštaurácia
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
ZHR	ZHR	kA	ZHR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraničí	zahraničí	k1gNnSc2	zahraničí
===	===	k?	===
</s>
</p>
<p>
<s>
SCAE	SCAE	kA	SCAE
-	-	kIx~	-
Speciality	specialita	k1gFnSc2	specialita
coffee	coffe	k1gMnSc4	coffe
assn	assn	k1gMnSc1	assn
<g/>
.	.	kIx.	.
of	of	k?	of
Europe	Europ	k1gMnSc5	Europ
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zážitková	zážitkový	k2eAgFnSc1d1	zážitková
gastronomie	gastronomie	k1gFnSc1	gastronomie
</s>
</p>
<p>
<s>
Vaření	vaření	k1gNnSc1	vaření
</s>
</p>
<p>
<s>
Koření	kořenit	k5eAaImIp3nS	kořenit
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gastronomie	gastronomie	k1gFnSc2	gastronomie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Gastronomie	gastronomie	k1gFnSc2	gastronomie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Gastronomické	gastronomický	k2eAgInPc1d1	gastronomický
pojmy	pojem	k1gInPc1	pojem
-	-	kIx~	-
gastroslovnik	gastroslovnik	k1gInSc1	gastroslovnik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Gastronomie	gastronomie	k1gFnSc1	gastronomie
-	-	kIx~	-
hostovka	hostovka	k1gFnSc1	hostovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
gastronomie	gastronomie	k1gFnSc2	gastronomie
</s>
</p>
<p>
<s>
www.barcik-cuisine.estranky.cz	www.barcikuisine.estranky.cz	k1gMnSc1	www.barcik-cuisine.estranky.cz
</s>
</p>
