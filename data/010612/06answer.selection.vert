<s>
Gastronomie	gastronomie	k1gFnSc1	gastronomie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
v	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
pojetí	pojetí	k1gNnSc6	pojetí
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kuchařské	kuchařský	k2eAgNnSc4d1	kuchařské
nebo	nebo	k8xC	nebo
kulinářské	kulinářský	k2eAgNnSc4d1	kulinářské
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
culinaire	culinair	k1gInSc5	culinair
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
gastronomie	gastronomie	k1gFnSc1	gastronomie
(	(	kIx(	(
<g/>
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
žaludku	žaludek	k1gInSc6	žaludek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
