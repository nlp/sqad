<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
legendární	legendární	k2eAgFnSc1d1	legendární
nejen	nejen	k6eAd1	nejen
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
maskotovi	maskotův	k2eAgMnPc1d1	maskotův
Eddiemu	Eddiema	k1gFnSc4	Eddiema
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
všech	všecek	k3xTgNnPc2	všecek
alb	album	k1gNnPc2	album
a	a	k8xC	a
singlů	singl	k1gInPc2	singl
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
prvního	první	k4xOgInSc2	první
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Running	Running	k1gInSc1	Running
Free	Fre	k1gInSc2	Fre
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
