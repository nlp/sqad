<s desamb="1">
Tomuto	tento	k3xDgInSc3
procesu	proces	k1gInSc6
výroby	výroba	k1gFnSc2
obuvi	obuv	k1gFnSc2
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
Goodyear	Goodyear	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
počest	počest	k1gFnSc4
Charlese	Charles	k1gMnSc2
Goodyeara	Goodyear	k1gMnSc2
ml.	ml.	kA
(	(	kIx(
<g/>
syna	syn	k1gMnSc2
slavného	slavný	k2eAgMnSc2d1
Charlese	Charles	k1gMnSc2
Goodyeara	Goodyear	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
vynalezl	vynaleznout	k5eAaPmAgMnS
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
stroj	stroj	k1gInSc1
<g/>
.	.	kIx.
</s>