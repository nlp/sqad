<s>
Rám	rám	k1gInSc1
(	(	kIx(
<g/>
boty	bota	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Popis	popis	k1gInSc1
částí	část	k1gFnPc2
boty	bota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rám	rám	k1gInSc1
je	být	k5eAaImIp3nS
proužek	proužek	k1gInSc1
kvalitní	kvalitní	k2eAgFnSc2d1
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
prošitý	prošitý	k2eAgInSc1d1
spolu	spolu	k6eAd1
se	s	k7c7
svrchním	svrchní	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
a	a	k8xC
podšívkou	podšívka	k1gFnSc7
do	do	k7c2
tzv.	tzv.	kA
žebra	žebro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
němu	on	k3xPp3gInSc3
se	se	k3xPyFc4
poté	poté	k6eAd1
sedlářským	sedlářský	k2eAgInSc7d1
stehem	steh	k1gInSc7
přišívá	přišívat	k5eAaImIp3nS
podešev	podešev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
rám	rám	k1gInSc1
uzavírá	uzavírat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
poté	poté	k6eAd1
vyplněn	vyplnit	k5eAaPmNgInS
korkem	korek	k1gInSc7
nebo	nebo	k8xC
jiným	jiný	k2eAgInSc7d1
vhodným	vhodný	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
pórovitým	pórovitý	k2eAgMnPc3d1
nebo	nebo	k8xC
děrovaným	děrovaný	k2eAgMnPc3d1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
prodyšnosti	prodyšnost	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
procesu	proces	k1gInSc6
výroby	výroba	k1gFnSc2
obuvi	obuv	k1gFnSc2
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
Goodyear	Goodyear	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
počest	počest	k1gFnSc4
Charlese	Charles	k1gMnSc2
Goodyeara	Goodyear	k1gMnSc2
ml.	ml.	kA
(	(	kIx(
<g/>
syna	syn	k1gMnSc2
slavného	slavný	k2eAgMnSc2d1
Charlese	Charles	k1gMnSc2
Goodyeara	Goodyear	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
vynalezl	vynaleznout	k5eAaPmAgMnS
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
stroj	stroj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boty	bota	k1gFnPc1
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
lepené	lepený	k2eAgFnPc1d1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
také	také	k9
rám	rám	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
obecně	obecně	k6eAd1
nemá	mít	k5eNaImIp3nS
téměř	téměř	k6eAd1
žádný	žádný	k3yNgInSc4
praktický	praktický	k2eAgInSc4d1
význam	význam	k1gInSc4
a	a	k8xC
plní	plnit	k5eAaImIp3nP
pouze	pouze	k6eAd1
estetickou	estetický	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Goodyear	Goodyear	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tradiční	tradiční	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
výroby	výroba	k1gFnSc2
pánské	pánský	k2eAgFnSc2d1
společenské	společenský	k2eAgFnSc2d1
obuvi	obuv	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
jméno	jméno	k1gNnSc4
dostala	dostat	k5eAaPmAgFnS
podle	podle	k7c2
vynálezce	vynálezce	k1gMnSc2
původního	původní	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nahradil	nahradit	k5eAaPmAgInS
plně	plně	k6eAd1
ruční	ruční	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ve	v	k7c6
světě	svět	k1gInSc6
v	v	k7c6
průběhu	průběh	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
převládala	převládat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
společenských	společenský	k2eAgFnPc2d1
bot	bota	k1gFnPc2
vyráběných	vyráběný	k2eAgInPc2d1
pomocí	pomocí	k7c2
Goodyear	Goodyeara	k1gFnPc2
konstrukce	konstrukce	k1gFnSc2
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
rekonstrukce	rekonstrukce	k1gFnSc1
-	-	kIx~
u	u	k7c2
bot	bota	k1gFnPc2
lze	lze	k6eAd1
jednoduše	jednoduše	k6eAd1
vyměňovat	vyměňovat	k5eAaImF
podešev	podešev	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
prodlouží	prodloužit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gFnSc1
životnost	životnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svrchní	svrchní	k2eAgFnSc1d1
část	část	k1gFnSc1
společenské	společenský	k2eAgFnSc2d1
boty	bota	k1gFnSc2
se	se	k3xPyFc4
napne	napnout	k5eAaPmIp3nS
na	na	k7c4
ševcovské	ševcovský	k2eAgNnSc4d1
kopyto	kopyto	k1gNnSc4
a	a	k8xC
upevní	upevnit	k5eAaPmIp3nS
našitím	našití	k1gNnSc7
koženého	kožený	k2eAgInSc2d1
<g/>
,	,	kIx,
lněného	lněný	k2eAgInSc2d1
nebo	nebo	k8xC
umělohmotného	umělohmotný	k2eAgInSc2d1
proužku	proužek	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
rám	rám	k1gInSc1
boty	bota	k1gFnPc4
<g/>
,	,	kIx,
ke	k	k7c3
stélce	stélka	k1gFnSc3
a	a	k8xC
podšívce	podšívka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rám	rám	k1gInSc1
vytvoří	vytvořit	k5eAaPmIp3nS
dutinu	dutina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
vyplní	vyplnit	k5eAaPmIp3nS
korkem	korek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
částí	část	k1gFnSc7
boty	bota	k1gFnSc2
je	být	k5eAaImIp3nS
podešev	podešev	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
připevňuje	připevňovat	k5eAaImIp3nS
k	k	k7c3
rámu	rám	k1gInSc3
kombinací	kombinace	k1gFnPc2
přišití	přišití	k1gNnSc1
a	a	k8xC
přilepení	přilepení	k1gNnSc1
silným	silný	k2eAgNnSc7d1
lepidlem	lepidlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goodyear	Goodyear	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
se	se	k3xPyFc4
vysoce	vysoce	k6eAd1
cení	cenit	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
voděodolná	voděodolný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
nedovolí	dovolit	k5eNaPmIp3nS
vodě	voda	k1gFnSc3
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
až	až	k9
ke	k	k7c3
stélce	stélka	k1gFnSc3
<g/>
,	,	kIx,
podešev	podešev	k1gFnSc1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
poměrně	poměrně	k6eAd1
jednoduše	jednoduše	k6eAd1
vyměnit	vyměnit	k5eAaPmF
a	a	k8xC
bota	bota	k1gFnSc1
vydrží	vydržet	k5eAaPmIp3nS
déle	dlouho	k6eAd2
<g/>
,	,	kIx,
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
o	o	k7c4
ni	on	k3xPp3gFnSc4
postaráno	postarán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
samé	samý	k3xTgFnSc2
podstaty	podstata	k1gFnSc2
Goodyear	Goodyear	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vyrobit	vyrobit	k5eAaPmF
společenské	společenský	k2eAgFnSc2d1
boty	bota	k1gFnSc2
touto	tento	k3xDgFnSc7
cestou	cesta	k1gFnSc7
trvá	trvat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
déle	dlouho	k6eAd2
než	než	k8xS
u	u	k7c2
levnějších	levný	k2eAgFnPc2d2
alternativ	alternativa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goodyear	Goodyear	k1gMnSc1
konstrukci	konstrukce	k1gFnSc4
používá	používat	k5eAaImIp3nS
řada	řada	k1gFnSc1
zavedených	zavedený	k2eAgFnPc2d1
značek	značka	k1gFnPc2
obuvnického	obuvnický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Alden	Alden	k1gInSc1
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
Sargent	Sargent	k1gMnSc1
<g/>
,	,	kIx,
Allen	Allen	k1gMnSc1
Edmonds	Edmonds	k1gInSc1
<g/>
,	,	kIx,
Barker	Barker	k1gInSc1
<g/>
,	,	kIx,
Boulet	Boulet	k1gInSc1
Boots	Boots	k1gInSc1
<g/>
,	,	kIx,
Brooks	Brooks	k1gInSc1
Brothers	Brothers	k1gInSc1
<g/>
,	,	kIx,
Caterpillar	Caterpillar	k1gMnSc1
(	(	kIx(
<g/>
CAT	CAT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cheaney	Cheaney	k1gInPc1
<g/>
,	,	kIx,
Chippewa	Chippewum	k1gNnPc1
<g/>
,	,	kIx,
Church	Church	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
Crockett	Crockett	k1gMnSc1
&	&	k?
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
Florsheim	Florsheim	k1gMnSc1
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
Cleverley	Cleverlea	k1gMnSc2
(	(	kIx(
<g/>
RTW	RTW	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Grenson	Grenson	k1gNnSc1
<g/>
,	,	kIx,
Loake	Loake	k1gNnPc7
Shoemakers	Shoemakers	k1gInSc1
<g/>
,	,	kIx,
Oliver	Oliver	k1gInSc1
Grey	Grea	k1gFnSc2
<g/>
,	,	kIx,
Oliver	Oliver	k1gInSc4
Sweeney	Sweenea	k1gFnSc2
<g/>
,	,	kIx,
Red	Red	k1gFnSc2
Wing	Winga	k1gFnPc2
Boots	Bootsa	k1gFnPc2
<g/>
,	,	kIx,
Timberland	Timberlanda	k1gFnPc2
a	a	k8xC
Wolverine	Wolverin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Welt	Welt	k2eAgInSc4d1
(	(	kIx(
<g/>
shoe	shoe	k1gInSc4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Goodyear	Goodyear	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
</s>
