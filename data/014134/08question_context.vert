<s>
Kód	kód	k1gInSc1
Navajo	Navajo	k6eAd1
je	být	k5eAaImIp3nS
šifrovací	šifrovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
používali	používat	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
americké	americký	k2eAgFnSc2d1
Námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
označováni	označován	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
Wind	Wind	k1gMnSc1
talkers	talkersa	k1gFnPc2
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
z	z	k7c2
řad	řada	k1gFnPc2
amerických	americký	k2eAgMnPc2d1
Indiánů	Indián	k1gMnPc2
(	(	kIx(
<g/>
Čerokíů	Čerokí	k1gMnPc2
<g/>
,	,	kIx,
Seminolů	Seminol	k1gInPc2
<g/>
,	,	kIx,
Navahů	Navah	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
ale	ale	k8xC
například	například	k6eAd1
i	i	k9
z	z	k7c2
řad	řada	k1gFnPc2
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
Baskové	Bask	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
sloužili	sloužit	k5eAaImAgMnP
jako	jako	k9
radisté	radista	k1gMnPc1
a	a	k8xC
tlumočili	tlumočit	k5eAaImAgMnP
tajné	tajný	k2eAgFnPc4d1
taktické	taktický	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>