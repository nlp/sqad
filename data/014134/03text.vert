<s>
Kód	kód	k1gInSc1
Navajo	Navajo	k6eAd1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Kód	kód	k1gInSc1
Navajo	Navajo	k1gNnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Generál	generál	k1gMnSc1
Douglas	Douglas	k1gMnSc1
MacArthur	MacArthur	k1gMnSc1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
navažskými	navažský	k2eAgInPc7d1
kodéry	kodér	k1gInPc7
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kód	kód	k1gInSc1
Navajo	Navajo	k6eAd1
je	být	k5eAaImIp3nS
šifrovací	šifrovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
používali	používat	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
americké	americký	k2eAgFnSc2d1
Námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
označováni	označován	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
Wind	Wind	k1gMnSc1
talkers	talkersa	k1gFnPc2
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
z	z	k7c2
řad	řada	k1gFnPc2
amerických	americký	k2eAgMnPc2d1
Indiánů	Indián	k1gMnPc2
(	(	kIx(
<g/>
Čerokíů	Čerokí	k1gMnPc2
<g/>
,	,	kIx,
Seminolů	Seminol	k1gInPc2
<g/>
,	,	kIx,
Navahů	Navah	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
ale	ale	k8xC
například	například	k6eAd1
i	i	k9
z	z	k7c2
řad	řada	k1gFnPc2
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
Baskové	Bask	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
sloužili	sloužit	k5eAaImAgMnP
jako	jako	k9
radisté	radista	k1gMnPc1
a	a	k8xC
tlumočili	tlumočit	k5eAaImAgMnP
tajné	tajný	k2eAgFnPc4d1
taktické	taktický	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nasazení	nasazení	k1gNnSc1
vojáků	voják	k1gMnPc2
mluvících	mluvící	k2eAgMnPc2d1
pro	pro	k7c4
dané	daný	k2eAgNnSc4d1
bojiště	bojiště	k1gNnSc4
atypickým	atypický	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
zvyšovalo	zvyšovat	k5eAaImAgNnS
bezpečnost	bezpečnost	k1gFnSc4
rádiové	rádiový	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šifrovanou	šifrovaný	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
v	v	k7c6
jazyce	jazyk	k1gInSc6
Navahů	Navah	k1gInPc2
vymyslelo	vymyslet	k5eAaPmAgNnS
29	#num#	k4
členů	člen	k1gMnPc2
indiánského	indiánský	k2eAgInSc2d1
kmene	kmen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
zejména	zejména	k9
při	při	k7c6
komunikaci	komunikace	k1gFnSc6
v	v	k7c6
bojích	boj	k1gInPc6
proti	proti	k7c3
Japoncům	Japonec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
kmene	kmen	k1gInSc2
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
japonští	japonský	k2eAgMnPc1d1
experti	expert	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
rozluštit	rozluštit	k5eAaPmF
se	s	k7c7
závratnou	závratný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
kteroukoli	kterýkoli	k3yIgFnSc4
šifru	šifra	k1gFnSc4
<g/>
,	,	kIx,
jakou	jaký	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
američtí	americký	k2eAgMnPc1d1
kryptografii	kryptografie	k1gFnSc4
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
vymyslet	vymyslet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojujíce	bojovat	k5eAaImSgFnP
s	s	k7c7
tímto	tento	k3xDgInSc7
problémem	problém	k1gInSc7
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
vymýšlet	vymýšlet	k5eAaImF
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
dál	daleko	k6eAd2
složitější	složitý	k2eAgInPc4d2
kódy	kód	k1gInPc4
<g/>
,	,	kIx,
až	až	k6eAd1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
velitelé	velitel	k1gMnPc1
stěžovali	stěžovat	k5eAaImAgMnP
na	na	k7c4
příliš	příliš	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
potřebnou	potřebný	k2eAgFnSc4d1
k	k	k7c3
šifrování	šifrování	k1gNnSc3
a	a	k8xC
dešifrování	dešifrování	k1gNnSc3
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
vyřešil	vyřešit	k5eAaPmAgMnS
muž	muž	k1gMnSc1
jménem	jméno	k1gNnSc7
Philip	Philip	k1gInSc1
Johnston	Johnston	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
žil	žít	k5eAaImAgInS
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
dítě	dítě	k1gNnSc4
však	však	k9
vyrůstal	vyrůstat	k5eAaImAgMnS
v	v	k7c6
rezervaci	rezervace	k1gFnSc6
indiánského	indiánský	k2eAgInSc2d1
kmene	kmen	k1gInSc2
Navahů	Navah	k1gMnPc2
v	v	k7c6
Arizoně	Arizona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc4
Navahů	Navah	k1gMnPc2
je	být	k5eAaImIp3nS
jedinečný	jedinečný	k2eAgInSc1d1
a	a	k8xC
není	být	k5eNaImIp3nS
příbuzný	příbuzný	k2eAgMnSc1d1
s	s	k7c7
žádným	žádný	k3yNgInSc7
jiným	jiný	k2eAgInSc7d1
evropským	evropský	k2eAgInSc7d1
ani	ani	k8xC
asijským	asijský	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
a	a	k8xC
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
složitý	složitý	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
téměř	téměř	k6eAd1
nikdo	nikdo	k3yNnSc1
není	být	k5eNaImIp3nS
schopen	schopen	k2eAgMnSc1d1
mu	on	k3xPp3gMnSc3
porozumět	porozumět	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johnston	Johnston	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
sám	sám	k3xTgInSc1
kdysi	kdysi	k6eAd1
naučil	naučit	k5eAaPmAgMnS
jazyk	jazyk	k1gInSc4
Navahů	Navah	k1gMnPc2
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
mu	on	k3xPp3gMnSc3
nerozumí	rozumět	k5eNaImIp3nP
jeho	jeho	k3xOp3gMnPc1
krajané	krajan	k1gMnPc1
<g/>
,	,	kIx,
nebudou	být	k5eNaImBp3nP
mu	on	k3xPp3gMnSc3
rozumět	rozumět	k5eAaImF
ani	ani	k8xC
Japonci	Japonec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
se	se	k3xPyFc4
Johnston	Johnston	k1gInSc1
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
generálmajorem	generálmajor	k1gMnSc7
Claytonem	Clayton	k1gInSc7
B.	B.	kA
Vogelem	Vogel	k1gMnSc7
<g/>
,	,	kIx,
velícím	velící	k2eAgMnSc7d1
generálem	generál	k1gMnSc7
obojživelných	obojživelný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Pacifické	pacifický	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
spolupracovníky	spolupracovník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johnston	Johnston	k1gInSc1
představil	představit	k5eAaPmAgInS
za	za	k7c2
simulovaných	simulovaný	k2eAgMnPc2d1
bojových	bojový	k2eAgMnPc2d1
podmínkách	podmínka	k1gFnPc6
analýzu	analýza	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
prokázala	prokázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
kód	kód	k1gInSc4
Navaho	Nava	k1gMnSc2
mohl	moct	k5eAaImAgMnS
kódovat	kódovat	k5eAaBmF
<g/>
,	,	kIx,
přenášet	přenášet	k5eAaImF
a	a	k8xC
dekódovat	dekódovat	k5eAaBmF
zprávy	zpráva	k1gFnPc4
v	v	k7c6
angličtině	angličtina	k1gFnSc6
za	za	k7c4
20	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
oproti	oproti	k7c3
30	#num#	k4
minutám	minuta	k1gFnPc3
<g/>
,	,	kIx,
za	za	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
kódovaly	kódovat	k5eAaBmAgFnP
zprávy	zpráva	k1gFnPc4
stroje	stroj	k1gInSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johnston	Johnston	k1gInSc4
také	také	k9
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
při	při	k7c6
každém	každý	k3xTgInSc6
vojenském	vojenský	k2eAgInSc6d1
praporu	prapor	k1gInSc6
pracovali	pracovat	k5eAaImAgMnP
jako	jako	k9
radiotelegrafisté	radiotelegrafista	k1gMnPc1
dva	dva	k4xCgMnPc1
Navahové	Navah	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
vysílali	vysílat	k5eAaImAgMnP
a	a	k8xC
přijímali	přijímat	k5eAaImAgMnP
zprávy	zpráva	k1gFnPc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
rodném	rodný	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápad	nápad	k1gInSc1
byl	být	k5eAaImAgInS
přijat	přijat	k2eAgMnSc1d1
a	a	k8xC
Vogel	Vogel	k1gMnSc1
doporučil	doporučit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pěchota	pěchota	k1gFnSc1
zrekrutoval	zrekrutovat	k5eAaPmAgMnS
200	#num#	k4
Navahů	Navah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvních	první	k4xOgInPc2
29	#num#	k4
navažských	navažský	k2eAgMnPc2d1
nováčků	nováček	k1gMnPc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
výcviku	výcvik	k1gInSc3
v	v	k7c6
květnu	květen	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
první	první	k4xOgFnSc1
skupina	skupina	k1gFnSc1
pak	pak	k6eAd1
vytvořila	vytvořit	k5eAaPmAgFnS
kód	kód	k1gInSc4
Navajo	Navajo	k6eAd1
v	v	k7c4
Camp	camp	k1gInSc4
Pendleton	Pendleton	k1gInSc4
<g/>
,	,	kIx,
Oceanside	Oceansid	k1gInSc5
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
šifrování	šifrování	k1gNnSc2
</s>
<s>
Princip	princip	k1gInSc1
šifrovaného	šifrovaný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
spočíval	spočívat	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Navahové	Navah	k1gMnPc1
měli	mít	k5eAaImAgMnP
množství	množství	k1gNnSc4
slov	slovo	k1gNnPc2
na	na	k7c4
pojmenování	pojmenování	k1gNnSc4
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
znali	znát	k5eAaImAgMnP
a	a	k8xC
viděli	vidět	k5eAaImAgMnP
<g/>
,	,	kIx,
například	například	k6eAd1
ptáky	pták	k1gMnPc4
a	a	k8xC
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
neuměli	umět	k5eNaImAgMnP
však	však	k9
pojmenovat	pojmenovat	k5eAaPmF
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgMnPc7,k3yRgMnPc7,k3yIgMnPc7
se	se	k3xPyFc4
nesetkali	setkat	k5eNaPmAgMnP
<g/>
,	,	kIx,
například	například	k6eAd1
letecké	letecký	k2eAgFnPc1d1
stíhačky	stíhačka	k1gFnPc1
nebo	nebo	k8xC
ponorky	ponorka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypracovali	vypracovat	k5eAaPmAgMnP
tedy	tedy	k9
seznam	seznam	k1gInSc4
274	#num#	k4
takových	takový	k3xDgNnPc2
slov	slovo	k1gNnPc2
a	a	k8xC
vytvořili	vytvořit	k5eAaPmAgMnP
k	k	k7c3
nim	on	k3xPp3gInPc3
navažské	navažský	k2eAgFnPc1d1
pendanty	pendant	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čluny	člun	k1gInPc7
dostaly	dostat	k5eAaPmAgInP
jména	jméno	k1gNnPc1
ryb	ryba	k1gFnPc2
a	a	k8xC
letadla	letadlo	k1gNnSc2
jména	jméno	k1gNnSc2
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bomby	bomba	k1gFnPc1
byly	být	k5eAaImAgFnP
vejce	vejce	k1gNnSc4
<g/>
,	,	kIx,
Minomety	minomet	k1gInPc1
byly	být	k5eAaImAgInP
sehnuté	sehnutý	k2eAgMnPc4d1
pušky	puška	k1gFnSc2
a	a	k8xC
vojenská	vojenský	k2eAgFnSc1d1
četa	četa	k1gFnSc1
byla	být	k5eAaImAgFnS
černé	černý	k2eAgFnPc4d1
ovce	ovce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanedlouho	zanedlouho	k6eAd1
na	na	k7c4
seznam	seznam	k1gInSc4
přibylo	přibýt	k5eAaPmAgNnS
dalších	další	k2eAgNnPc2d1
234	#num#	k4
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
byly	být	k5eAaImAgInP
i	i	k9
názvy	název	k1gInPc1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
„	„	k?
<g/>
naše	náš	k3xOp1gNnSc4
matka	matka	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
Británie	Británie	k1gFnSc1
„	„	k?
<g/>
mezi	mezi	k7c7
vodami	voda	k1gFnPc7
<g/>
“	“	k?
a	a	k8xC
Německo	Německo	k1gNnSc1
„	„	k?
<g/>
železný	železný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Jazyková	jazykový	k2eAgFnSc1d1
bariéra	bariéra	k1gFnSc1
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nedorozumění	nedorozumění	k1gNnSc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
spojovací	spojovací	k2eAgMnPc1d1
technici	technik	k1gMnPc1
si	se	k3xPyFc3
neuvědomili	uvědomit	k5eNaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
mezi	mezi	k7c7
nimi	on	k3xPp3gNnPc7
pracují	pracovat	k5eAaImIp3nP
i	i	k9
Navahové	Navah	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mysleli	myslet	k5eAaImAgMnP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
vlnách	vlna	k1gFnPc6
armády	armáda	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
vysílají	vysílat	k5eAaImIp3nP
Japonci	Japonec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
však	však	k9
používání	používání	k1gNnSc1
navažského	navažský	k2eAgInSc2d1
mluveného	mluvený	k2eAgInSc2d1
kódu	kód	k1gInSc2
fungovalo	fungovat	k5eAaImAgNnS
bez	bez	k7c2
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
nerozuměla	rozumět	k5eNaImAgFnS
ani	ani	k9
slovo	slovo	k1gNnSc4
a	a	k8xC
nerozuměli	rozumět	k5eNaImAgMnP
ani	ani	k8xC
Japonci	Japonec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
divili	divit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnPc1
rádiové	rádiový	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
jsou	být	k5eAaImIp3nP
plné	plný	k2eAgFnPc1d1
železných	železný	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
(	(	kIx(
<g/>
ponorek	ponorka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolibříků	kolibřík	k1gMnPc2
(	(	kIx(
<g/>
stíhaček	stíhačka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
žraloků	žralok	k1gMnPc2
(	(	kIx(
<g/>
torpédoborců	torpédoborec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Činnost	činnost	k1gFnSc1
navažských	navažský	k2eAgInPc2d1
kodérů	kodér	k1gInPc2
ocenili	ocenit	k5eAaPmAgMnP
Američané	Američan	k1gMnPc1
zejména	zejména	k9
během	během	k7c2
bitvy	bitva	k1gFnSc2
o	o	k7c4
Iwodžimu	Iwodžima	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgInPc6
dnech	den	k1gInPc6
bitvy	bitva	k1gFnSc2
v	v	k7c6
únoru	únor	k1gInSc6
a	a	k8xC
březnu	březen	k1gInSc6
1945	#num#	k4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
dobýt	dobýt	k5eAaPmF
ostrov	ostrov	k1gInSc4
Iwodžima	Iwodžimum	k1gNnSc2
na	na	k7c6
jihu	jih	k1gInSc6
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
odesláno	odeslat	k5eAaPmNgNnS
asi	asi	k9
800	#num#	k4
navažských	navažský	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
bez	bez	k7c2
jediné	jediný	k2eAgFnSc2d1
chyby	chyba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
dostali	dostat	k5eAaPmAgMnP
Navahové	Navahový	k2eAgInPc4d1
zákaz	zákaz	k1gInSc4
o	o	k7c6
své	svůj	k3xOyFgFnSc6
roli	role	k1gFnSc6
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
kód	kód	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
mezi	mezi	k7c7
utajovanými	utajovaný	k2eAgFnPc7d1
informacemi	informace	k1gFnPc7
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
hrdinství	hrdinství	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
tichosti	tichost	k1gFnSc6
zapomnělo	zapomnět	k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
však	však	k9
dostalo	dostat	k5eAaPmAgNnS
chvály	chvála	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
si	se	k3xPyFc3
zasloužili	zasloužit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
USA	USA	kA
14	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
za	za	k7c4
Národní	národní	k2eAgInSc4d1
den	den	k1gInSc4
navažských	navažský	k2eAgMnPc2d1
radiotelegrafistů	radiotelegrafista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
holdem	hold	k1gInSc7
pro	pro	k7c4
Navahy	Navah	k1gInPc4
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInSc1
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
mála	málo	k4c2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
nikdy	nikdy	k6eAd1
nikdo	nikdo	k3yNnSc1
nerozluštil	rozluštit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kód	kód	k1gInSc4
Navajo	Navajo	k6eAd1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Official	Official	k1gInSc1
Site	Site	k1gFnSc1
of	of	k?
the	the	k?
Navajo	Navajo	k6eAd1
Code	Code	k1gNnSc1
Talkers	Talkers	k1gInSc1
<g/>
.	.	kIx.
www.navajocodetalkers.org	www.navajocodetalkers.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Adams	Adams	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
Kódy	kód	k1gInPc1
od	od	k7c2
hieroglifov	hieroglifovo	k1gNnPc2
k	k	k7c3
hackerům	hacker	k1gMnPc3
<g/>
:	:	kIx,
Vydavatelství	vydavatelství	k1gNnSc4
Slovart	Slovarta	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
</s>
<s>
Dowswell	Dowswell	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka-Nejvýznamnější	válka-Nejvýznamný	k2eAgFnSc1d2
události	událost	k1gFnPc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kód	kód	k1gInSc1
Navajo	Navajo	k1gNnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kód	kód	k1gInSc4
Navajo	Navajo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Army	Army	k1gInPc1
Historical	Historical	k1gFnSc1
Foundation	Foundation	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
