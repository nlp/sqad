<s>
Saturnin	Saturnin	k1gMnSc1	Saturnin
je	být	k5eAaImIp3nS	být
humoristický	humoristický	k2eAgInSc4d1	humoristický
román	román	k1gInSc4	román
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
,	,	kIx,	,
nepředvídatelného	předvídatelný	k2eNgMnSc2d1	nepředvídatelný
sluhy	sluha	k1gMnSc2	sluha
Saturnina	Saturnin	k1gMnSc2	Saturnin
<g/>
,	,	kIx,	,
tety	teta	k1gFnSc2	teta
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gMnSc4	její
syna	syn	k1gMnSc4	syn
Milouše	Milouš	k1gMnSc4	Milouš
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnPc1d1	krásná
slečny	slečna	k1gFnPc1	slečna
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
strýce	strýc	k1gMnSc2	strýc
Františka	František	k1gMnSc2	František
<g/>
,	,	kIx,	,
doktora	doktor	k1gMnSc2	doktor
Vlacha	Vlach	k1gMnSc2	Vlach
a	a	k8xC	a
vypravěčova	vypravěčův	k2eAgMnSc2d1	vypravěčův
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
.	.	kIx.	.
</s>
