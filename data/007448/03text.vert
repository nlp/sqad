<p>
<s>
Saturnin	Saturnin	k1gMnSc1	Saturnin
je	být	k5eAaImIp3nS	být
humoristický	humoristický	k2eAgInSc4d1	humoristický
román	román	k1gInSc4	román
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
,	,	kIx,	,
nepředvídatelného	předvídatelný	k2eNgMnSc2d1	nepředvídatelný
sluhy	sluha	k1gMnSc2	sluha
Saturnina	Saturnin	k1gMnSc2	Saturnin
<g/>
,	,	kIx,	,
tety	teta	k1gFnSc2	teta
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gMnSc4	její
syna	syn	k1gMnSc4	syn
Milouše	Milouš	k1gMnSc4	Milouš
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnPc1d1	krásná
slečny	slečna	k1gFnPc1	slečna
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
strýce	strýc	k1gMnSc2	strýc
Františka	František	k1gMnSc2	František
<g/>
,	,	kIx,	,
doktora	doktor	k1gMnSc2	doktor
Vlacha	Vlach	k1gMnSc2	Vlach
a	a	k8xC	a
vypravěčova	vypravěčův	k2eAgMnSc2d1	vypravěčův
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
Jirotkův	Jirotkův	k2eAgInSc4d1	Jirotkův
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
značně	značně	k6eAd1	značně
vyniká	vynikat	k5eAaImIp3nS	vynikat
nad	nad	k7c4	nad
ostatní	ostatní	k1gNnSc4	ostatní
autorova	autorův	k2eAgNnSc2d1	autorovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
literárních	literární	k2eAgMnPc2d1	literární
kritiků	kritik	k1gMnPc2	kritik
se	se	k3xPyFc4	se
Jirotka	Jirotka	k1gFnSc1	Jirotka
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
anglickou	anglický	k2eAgFnSc7d1	anglická
literaturou	literatura	k1gFnSc7	literatura
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
dílem	dílem	k6eAd1	dílem
Jeroma	Jeroma	k1gFnSc1	Jeroma
Klapky	Klapka	k1gMnSc2	Klapka
Jeromeho	Jerome	k1gMnSc2	Jerome
a	a	k8xC	a
romány	román	k1gInPc7	román
a	a	k8xC	a
povídkami	povídka	k1gFnPc7	povídka
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Pelhama	Pelham	k1gMnSc2	Pelham
Grenvilla	Grenvill	k1gMnSc2	Grenvill
Wodehouseho	Wodehouse	k1gMnSc2	Wodehouse
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
sluha	sluha	k1gMnSc1	sluha
Jeeves	Jeeves	k1gMnSc1	Jeeves
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Jirotka	Jirotka	k1gFnSc1	Jirotka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
stal	stát	k5eAaPmAgMnS	stát
díky	díky	k7c3	díky
Janu	Jan	k1gMnSc3	Jan
Drdovi	Drda	k1gMnSc3	Drda
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Drdy	Drda	k1gMnSc2	Drda
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Brdečky	Brdečka	k1gMnSc2	Brdečka
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
Jirotka	Jirotka	k1gFnSc1	Jirotka
historky	historka	k1gFnSc2	historka
ze	z	k7c2	z
života	život	k1gInSc2	život
a	a	k8xC	a
Drda	Drda	k1gMnSc1	Drda
jej	on	k3xPp3gInSc4	on
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
napsal	napsat	k5eAaPmAgMnS	napsat
povídku	povídka	k1gFnSc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
povídka	povídka	k1gFnSc1	povídka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Můj	můj	k1gMnSc1	můj
sluha	sluha	k1gMnSc1	sluha
Saturnin	Saturnin	k1gMnSc1	Saturnin
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
24.	[number]	k4	24.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
románu	román	k1gInSc2	román
psal	psát	k5eAaImAgInS	psát
Jirotka	Jirotka	k1gFnSc1	Jirotka
v	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
svého	svůj	k3xOyFgInSc2	svůj
pražského	pražský	k2eAgInSc2d1	pražský
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
období	období	k1gNnSc6	období
Protektorátu	protektorát	k1gInSc2	protektorát
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
náletům	nálet	k1gInPc3	nálet
nařízeno	nařízen	k2eAgNnSc1d1	nařízeno
zatemnění	zatemnění	k1gNnSc1	zatemnění
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
v	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
okno	okno	k1gNnSc1	okno
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
členěna	členit	k5eAaImNgFnS	členit
do	do	k7c2	do
26	[number]	k4	26
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
římskou	římský	k2eAgFnSc7d1	římská
číslicí	číslice	k1gFnSc7	číslice
<g/>
,	,	kIx,	,
za	za	k7c7	za
níž	jenž	k3xRgFnSc7	jenž
následuje	následovat	k5eAaImIp3nS	následovat
krátký	krátký	k2eAgInSc1d1	krátký
odstavec	odstavec	k1gInSc1	odstavec
s	s	k7c7	s
informací	informace	k1gFnSc7	informace
o	o	k7c6	o
ději	děj	k1gInSc6	děj
dané	daný	k2eAgFnSc2d1	daná
kapitoly	kapitola	k1gFnSc2	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
55 600	[number]	k4	55 600
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnSc2	španělština
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnSc2	italština
a	a	k8xC	a
lotyštiny	lotyština	k1gFnSc2	lotyština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
román	román	k1gInSc1	román
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
čtenářskou	čtenářský	k2eAgFnSc4d1	čtenářská
anketu	anketa	k1gFnSc4	anketa
Kniha	kniha	k1gFnSc1	kniha
mého	můj	k3xOp1gNnSc2	můj
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
cenu	cena	k1gFnSc4	cena
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Knihy	kniha	k1gFnSc2	kniha
století	století	k1gNnSc2	století
podle	podle	k7c2	podle
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
románu	román	k1gInSc2	román
natočil	natočit	k5eAaBmAgMnS	natočit
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Věrčák	Věrčák	k1gMnSc1	Věrčák
čtyřdílný	čtyřdílný	k2eAgInSc4d1	čtyřdílný
seriál	seriál	k1gInSc4	seriál
Saturnin	Saturnin	k1gMnSc1	Saturnin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
sestříhán	sestříhat	k5eAaPmNgInS	sestříhat
i	i	k9	i
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
nakladatelstvím	nakladatelství	k1gNnPc3	nakladatelství
XYZ	XYZ	kA	XYZ
vydáno	vydat	k5eAaPmNgNnS	vydat
volné	volný	k2eAgNnSc1d1	volné
pokračování	pokračování	k1gNnSc1	pokračování
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Saturnin	Saturnin	k1gMnSc1	Saturnin
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Miroslava	Miroslav	k1gMnSc2	Miroslav
Macka	Macek	k1gMnSc2	Macek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
i	i	k9	i
jako	jako	k9	jako
audiokniha	audiokniha	k1gFnSc1	audiokniha
načtená	načtený	k2eAgFnSc1d1	načtená
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Vladykou	vladyka	k1gMnSc7	vladyka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnPc1	vydání
a	a	k8xC	a
ilustrace	ilustrace	k1gFnPc1	ilustrace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
vyšla	vyjít	k5eAaPmAgFnS	vyjít
dvě	dva	k4xCgNnPc4	dva
sešitová	sešitový	k2eAgNnPc4d1	sešitové
vydání	vydání	k1gNnPc4	vydání
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
Nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Knihovna	knihovna	k1gFnSc1	knihovna
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
a	a	k8xC	a
1948	[number]	k4	1948
knihu	kniha	k1gFnSc4	kniha
vydal	vydat	k5eAaPmAgMnS	vydat
nakladatel	nakladatel	k1gMnSc1	nakladatel
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
ilustrátorem	ilustrátor	k1gMnSc7	ilustrátor
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
karikaturista	karikaturista	k1gMnSc1	karikaturista
Otakar	Otakar	k1gMnSc1	Otakar
Mrkvička	mrkvička	k1gFnSc1	mrkvička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
román	román	k1gInSc4	román
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Práce	práce	k1gFnSc2	práce
s	s	k7c7	s
úvodní	úvodní	k2eAgFnSc7d1	úvodní
ilustrací	ilustrace	k1gFnSc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Hochmana	Hochman	k1gMnSc2	Hochman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
9	[number]	k4	9
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
knihu	kniha	k1gFnSc4	kniha
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Václav	Václav	k1gMnSc1	Václav
Pátek	Pátek	k1gMnSc1	Pátek
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Pitra	Pitra	k1gMnSc1	Pitra
a	a	k8xC	a
1964	[number]	k4	1964
Vratislav	Vratislav	k1gMnSc1	Vratislav
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
6	[number]	k4	6
vydání	vydání	k1gNnSc1	vydání
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Stanislava	Stanislav	k1gMnSc2	Stanislav
Dudy	Duda	k1gMnSc2	Duda
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
a	a	k8xC	a
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ilustrací	ilustrace	k1gFnPc2	ilustrace
knihu	kniha	k1gFnSc4	kniha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Soft	Soft	k?	Soft
design	design	k1gInSc1	design
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ji	on	k3xPp3gFnSc4	on
vydávalo	vydávat	k5eAaImAgNnS	vydávat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Šulc	Šulc	k1gMnSc1	Šulc
s	s	k7c7	s
kresbami	kresba	k1gFnPc7	kresba
karikaturisty	karikaturista	k1gMnSc2	karikaturista
Michala	Michal	k1gMnSc2	Michal
Hrdého	Hrdý	k1gMnSc2	Hrdý
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgNnSc4d1	poslední
24.	[number]	k4	24.
vydání	vydání	k1gNnSc4	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
od	od	k7c2	od
stejného	stejný	k2eAgNnSc2d1	stejné
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
ilustracemi	ilustrace	k1gFnPc7	ilustrace
vyšla	vyjít	k5eAaPmAgFnS	vyjít
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
cizojazyčná	cizojazyčný	k2eAgNnPc1d1	cizojazyčné
vydání	vydání	k1gNnPc1	vydání
<g/>
.	.	kIx.	.
<g/>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
nejméně	málo	k6eAd3	málo
šestkrát	šestkrát	k6eAd1	šestkrát
načtena	načten	k2eAgFnSc1d1	načtena
jako	jako	k8xS	jako
audiokniha	audiokniha	k1gFnSc1	audiokniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
román	román	k1gInSc4	román
načetl	načíst	k5eAaBmAgMnS	načíst
Miloš	Miloš	k1gMnSc1	Miloš
Vavruška	Vavruška	k1gMnSc1	Vavruška
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Jan	Jan	k1gMnSc1	Jan
Hartl	Hartl	k1gMnSc1	Hartl
pro	pro	k7c4	pro
Československý	československý	k2eAgInSc4d1	československý
rozhlas	rozhlas	k1gInSc4	rozhlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
1990	[number]	k4	1990
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Beneš	Beneš	k1gMnSc1	Beneš
pro	pro	k7c4	pro
Supraphon	supraphon	k1gInSc4	supraphon
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Jiří	Jiří	k1gMnSc1	Jiří
Samek	Samek	k1gMnSc1	Samek
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Ondřej	Ondřej	k1gMnSc1	Ondřej
Havelka	Havelka	k1gMnSc1	Havelka
a	a	k8xC	a
2010	[number]	k4	2010
Oldřich	Oldřich	k1gMnSc1	Oldřich
Vízner	Vízner	k1gMnSc1	Vízner
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
Saturnin	Saturnin	k1gMnSc1	Saturnin
uveden	uvést	k5eAaPmNgMnS	uvést
jako	jako	k9	jako
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Havelky	Havelka	k1gMnSc2	Havelka
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
<s>
Tutéž	týž	k3xTgFnSc4	týž
hru	hra	k1gFnSc4	hra
uvedlo	uvést	k5eAaPmAgNnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Národní	národní	k2eAgInSc4d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jakuba	Jakub	k1gMnSc2	Jakub
Nvoty	Nvota	k1gMnSc2	Nvota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
režii	režie	k1gFnSc6	režie
Petra	Petr	k1gMnSc2	Petr
Vacka	Vacek	k1gMnSc2	Vacek
uvede	uvést	k5eAaPmIp3nS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
hru	hra	k1gFnSc4	hra
Saturnin	Saturnin	k1gMnSc1	Saturnin
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
Jezerce	Jezerka	k1gFnSc6	Jezerka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
malého	malý	k2eAgNnSc2d1	malé
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nejspíš	nejspíš	k9	nejspíš
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
napsána	napsat	k5eAaPmNgFnS	napsat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1942.	[number]	k4	1942.
(	(	kIx(	(
<g/>
I	i	k8xC	i
když	když	k8xS	když
děj	děj	k1gInSc1	děj
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
z	z	k7c2	z
období	období	k1gNnSc2	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
dvakrát	dvakrát	k6eAd1	dvakrát
zmíněno	zmíněn	k2eAgNnSc4d1	zmíněno
zatemnění	zatemnění	k1gNnSc4	zatemnění
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
1.	[number]	k4	1.
září	září	k1gNnSc1	září
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vtipný	vtipný	k2eAgMnSc1d1	vtipný
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
klidným	klidný	k2eAgInSc7d1	klidný
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
najme	najmout	k5eAaPmIp3nS	najmout
sluhu	sluha	k1gMnSc4	sluha
Saturnina	Saturnin	k1gMnSc4	Saturnin
<g/>
.	.	kIx.	.
</s>
<s>
Netuší	tušit	k5eNaImIp3nS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
příštích	příští	k2eAgInPc6d1	příští
dnech	den	k1gInPc6	den
změní	změnit	k5eAaPmIp3nS	změnit
podstatně	podstatně	k6eAd1	podstatně
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Sluha	sluha	k1gMnSc1	sluha
je	být	k5eAaImIp3nS	být
pracovitý	pracovitý	k2eAgMnSc1d1	pracovitý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
uhlazené	uhlazený	k2eAgInPc4d1	uhlazený
způsoby	způsob	k1gInPc4	způsob
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
však	však	k9	však
nenápadně	nápadně	k6eNd1	nápadně
mezi	mezi	k7c7	mezi
známými	známý	k1gMnPc7	známý
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
roznášet	roznášet	k5eAaImF	roznášet
zvěsti	zvěst	k1gFnPc4	zvěst
o	o	k7c6	o
pánových	pánův	k2eAgFnPc6d1	Pánova
dobrodružných	dobrodružný	k2eAgFnPc6d1	dobrodružná
cestách	cesta	k1gFnPc6	cesta
na	na	k7c4	na
safari	safari	k1gNnSc4	safari
a	a	k8xC	a
byt	byt	k1gInSc1	byt
vybaví	vybavit	k5eAaPmIp3nS	vybavit
trofejemi	trofej	k1gFnPc7	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
nikomu	nikdo	k3yNnSc3	nikdo
sluhovy	sluhův	k2eAgFnPc1d1	sluhova
fabulace	fabulace	k1gFnPc4	fabulace
nevyvrací	vyvracet	k5eNaImIp3nP	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
od	od	k7c2	od
Saturnina	Saturnin	k1gMnSc2	Saturnin
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nebydlí	bydlet	k5eNaImIp3nP	bydlet
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
hausbótu	hausbót	k1gInSc6	hausbót
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
hausbót	hausbót	k1gInSc4	hausbót
se	se	k3xPyFc4	se
nezvána	zván	k2eNgFnSc1d1	nezvána
přistěhuje	přistěhovat	k5eAaPmIp3nS	přistěhovat
teta	teta	k1gFnSc1	teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
–	–	k?	–
s	s	k7c7	s
argumentem	argument	k1gInSc7	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Milouš	Milouš	k1gMnSc1	Milouš
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zdravý	zdravý	k2eAgInSc4d1	zdravý
vzduch	vzduch	k1gInSc4	vzduch
–	–	k?	–
a	a	k8xC	a
vyšoupne	vyšoupnout	k5eAaPmIp3nS	vyšoupnout
vypravěče	vypravěč	k1gMnPc4	vypravěč
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
kabiny	kabina	k1gFnSc2	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Saturnin	Saturnin	k1gMnSc1	Saturnin
neváhá	váhat	k5eNaImIp3nS	váhat
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Varuje	varovat	k5eAaImIp3nS	varovat
tetu	teta	k1gFnSc4	teta
s	s	k7c7	s
Miloušem	Milouš	k1gMnSc7	Milouš
před	před	k7c7	před
hlodavci	hlodavec	k1gMnPc7	hlodavec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
přebíhají	přebíhat	k5eAaImIp3nP	přebíhat
lidem	lid	k1gInSc7	lid
přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
maškarní	maškarní	k2eAgFnSc2d1	maškarní
masky	maska	k1gFnSc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
Teta	Teta	k1gFnSc1	Teta
nevrle	nevrle	k6eAd1	nevrle
vyklidí	vyklidit	k5eAaPmIp3nS	vyklidit
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
děje	děj	k1gInSc2	děj
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
dědečkově	dědečkův	k2eAgNnSc6d1	dědečkovo
venkovském	venkovský	k2eAgNnSc6d1	venkovské
sídle	sídlo	k1gNnSc6	sídlo
během	během	k7c2	během
vypravěčovy	vypravěčův	k2eAgFnSc2d1	vypravěčova
třítýdenní	třítýdenní	k2eAgFnSc2d1	třítýdenní
dovolené	dovolená	k1gFnSc2	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Pozván	pozvat	k5eAaPmNgMnS	pozvat
byl	být	k5eAaImAgMnS	být
také	také	k9	také
doktor	doktor	k1gMnSc1	doktor
Vlach	Vlach	k1gMnSc1	Vlach
<g/>
,	,	kIx,	,
rodinný	rodinný	k2eAgMnSc1d1	rodinný
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
neděje	dít	k5eNaImIp3nS	dít
nic	nic	k3yNnSc1	nic
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
<g/>
;	;	kIx,	;
vypravěč	vypravěč	k1gMnSc1	vypravěč
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
na	na	k7c4	na
příjezd	příjezd	k1gInSc4	příjezd
krásné	krásný	k2eAgFnSc2d1	krásná
slečny	slečna	k1gFnSc2	slečna
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Zná	znát	k5eAaImIp3nS	znát
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
z	z	k7c2	z
tenisového	tenisový	k2eAgInSc2d1	tenisový
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
však	však	k9	však
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
dojem	dojem	k1gInSc4	dojem
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
však	však	k9	však
vmísí	vmísit	k5eAaPmIp3nS	vmísit
teta	teta	k1gFnSc1	teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
s	s	k7c7	s
Miloušem	Milouš	k1gMnSc7	Milouš
a	a	k8xC	a
kazí	kazit	k5eAaImIp3nS	kazit
ostatním	ostatní	k1gNnSc7	ostatní
jinak	jinak	k6eAd1	jinak
příjemné	příjemný	k2eAgInPc1d1	příjemný
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Saturnin	Saturnin	k1gMnSc1	Saturnin
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
sblíží	sblížit	k5eAaPmIp3nS	sblížit
s	s	k7c7	s
dědečkem	dědeček	k1gMnSc7	dědeček
a	a	k8xC	a
naučí	naučit	k5eAaPmIp3nS	naučit
ho	on	k3xPp3gNnSc4	on
džiu-džitsu	džiužitsu	k1gNnSc4	džiu-džitsu
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
neblahé	blahý	k2eNgInPc4d1	neblahý
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
bleskovém	bleskový	k2eAgInSc6d1	bleskový
jednostranném	jednostranný	k2eAgInSc6d1	jednostranný
"	"	kIx"	"
<g/>
souboji	souboj	k1gInSc6	souboj
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
vypravěč	vypravěč	k1gMnSc1	vypravěč
vyvrtne	vyvrtnout	k5eAaPmIp3nS	vyvrtnout
kotník	kotník	k1gInSc4	kotník
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
jet	jet	k5eAaImF	jet
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
ho	on	k3xPp3gMnSc4	on
již	již	k9	již
odváží	odvážit	k5eAaPmIp3nS	odvážit
slečna	slečna	k1gFnSc1	slečna
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
dědečkův	dědečkův	k2eAgInSc4d1	dědečkův
dům	dům	k1gInSc4	dům
od	od	k7c2	od
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozvodnila	rozvodnit	k5eAaPmAgFnS	rozvodnit
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
přejeli	přejet	k5eAaPmAgMnP	přejet
mostek	mostek	k1gInSc4	mostek
<g/>
,	,	kIx,	,
strhla	strhnout	k5eAaPmAgFnS	strhnout
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Zmizela	zmizet	k5eAaPmAgFnS	zmizet
tak	tak	k6eAd1	tak
jediná	jediný	k2eAgFnSc1d1	jediná
přístupová	přístupový	k2eAgFnSc1d1	přístupová
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
na	na	k7c4	na
nákupy	nákup	k1gInPc4	nákup
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
domě	dům	k1gInSc6	dům
z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
důvodu	důvod	k1gInSc2	důvod
přerušen	přerušit	k5eAaPmNgInS	přerušit
přívod	přívod	k1gInSc1	přívod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
tak	tak	k9	tak
prožívá	prožívat	k5eAaImIp3nS	prožívat
malé	malý	k2eAgNnSc4d1	malé
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večerech	večer	k1gInPc6	večer
si	se	k3xPyFc3	se
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
dny	den	k1gInPc4	den
tráví	trávit	k5eAaImIp3nP	trávit
vařením	vaření	k1gNnSc7	vaření
na	na	k7c6	na
ohni	oheň	k1gInSc6	oheň
<g/>
,	,	kIx,	,
procházkami	procházka	k1gFnPc7	procházka
a	a	k8xC	a
odpočinkem	odpočinek	k1gInSc7	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
se	se	k3xPyFc4	se
sbližuje	sbližovat	k5eAaImIp3nS	sbližovat
se	s	k7c7	s
slečnou	slečna	k1gFnSc7	slečna
Barborou	Barbora	k1gFnSc7	Barbora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
otráven	otráven	k2eAgInSc1d1	otráven
chováním	chování	k1gNnSc7	chování
osmnáctiletého	osmnáctiletý	k2eAgMnSc2d1	osmnáctiletý
Milouše	Milouš	k1gMnSc2	Milouš
<g/>
,	,	kIx,	,
neuctivého	uctivý	k2eNgInSc2d1	neuctivý
ke	k	k7c3	k
slečně	slečna	k1gFnSc3	slečna
Barboře	Barbora	k1gFnSc3	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Dohodne	dohodnout	k5eAaPmIp3nS	dohodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
se	s	k7c7	s
Saturninem	Saturnin	k1gMnSc7	Saturnin
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
povedou	vést	k5eAaImIp3nP	vést
proti	proti	k7c3	proti
Miloušovi	Milouš	k1gMnSc3	Milouš
skrytou	skrytý	k2eAgFnSc4d1	skrytá
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
stávají	stávat	k5eAaImIp3nP	stávat
"	"	kIx"	"
<g/>
náhodné	náhodný	k2eAgFnPc1d1	náhodná
<g/>
"	"	kIx"	"
nehody	nehoda	k1gFnPc1	nehoda
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
umělý	umělý	k2eAgInSc1d1	umělý
obraz	obraz	k1gInSc1	obraz
nonšalantního	nonšalantní	k2eAgMnSc2d1	nonšalantní
světáka	světák	k1gMnSc2	světák
se	se	k3xPyFc4	se
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
docházejí	docházet	k5eAaImIp3nP	docházet
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
obejde	obejít	k5eAaPmIp3nS	obejít
pramen	pramen	k1gInSc4	pramen
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
noc	noc	k1gFnSc1	noc
stráví	strávit	k5eAaPmIp3nS	strávit
ve	v	k7c6	v
srubu	srub	k1gInSc6	srub
doktora	doktor	k1gMnSc2	doktor
Vlacha	Vlach	k1gMnSc2	Vlach
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
příjemná	příjemný	k2eAgFnSc1d1	příjemná
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dědeček	dědeček	k1gMnSc1	dědeček
zraní	zranit	k5eAaPmIp3nS	zranit
a	a	k8xC	a
nezbývá	zbývat	k5eNaImIp3nS	zbývat
než	než	k8xS	než
druhou	druhý	k4xOgFnSc4	druhý
noc	noc	k1gFnSc4	noc
přenocovat	přenocovat	k5eAaPmF	přenocovat
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dojdou	dojít	k5eAaPmIp3nP	dojít
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
strženého	stržený	k2eAgInSc2d1	stržený
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
uvidí	uvidět	k5eAaPmIp3nS	uvidět
stát	stát	k5eAaPmF	stát
most	most	k1gInSc4	most
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teta	Teta	k1gFnSc1	Teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
se	se	k3xPyFc4	se
vnutila	vnutit	k5eAaPmAgFnS	vnutit
ošetřovat	ošetřovat	k5eAaImF	ošetřovat
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
měl	mít	k5eAaImAgMnS	mít
pouze	pouze	k6eAd1	pouze
bolavá	bolavý	k2eAgNnPc4d1	bolavé
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepustila	pustit	k5eNaPmAgFnS	pustit
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
ani	ani	k8xC	ani
doktora	doktor	k1gMnSc4	doktor
Vlacha	Vlach	k1gMnSc4	Vlach
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
však	však	k9	však
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
strašného	strašný	k2eAgNnSc2d1	strašné
podlézání	podlézání	k1gNnSc2	podlézání
<g/>
,	,	kIx,	,
nucení	nucení	k1gNnSc2	nucení
<g/>
,	,	kIx,	,
proseb	prosba	k1gFnPc2	prosba
<g/>
,	,	kIx,	,
hraných	hraný	k2eAgInPc2d1	hraný
hysterických	hysterický	k2eAgInPc2d1	hysterický
záchvatů	záchvat	k1gInPc2	záchvat
a	a	k8xC	a
vyhrožování	vyhrožování	k1gNnSc2	vyhrožování
natolik	natolik	k6eAd1	natolik
otráven	otráven	k2eAgMnSc1d1	otráven
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgMnS	začít
předstírat	předstírat	k5eAaImF	předstírat
šílenství	šílenství	k1gNnSc4	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dostal	dostat	k5eAaPmAgMnS	dostat
tetu	teta	k1gFnSc4	teta
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
fingovaně	fingovaně	k6eAd1	fingovaně
sepsal	sepsat	k5eAaPmAgMnS	sepsat
závěť	závěť	k1gFnSc4	závěť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
všechen	všechen	k3xTgInSc4	všechen
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
odkázal	odkázat	k5eAaPmAgMnS	odkázat
na	na	k7c4	na
dobročinné	dobročinný	k2eAgInPc4d1	dobročinný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
teta	teta	k1gFnSc1	teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zbláznil	zbláznit	k5eAaPmAgInS	zbláznit
a	a	k8xC	a
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
požene	pohnat	k5eAaPmIp3nS	pohnat
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
odjela	odjet	k5eAaPmAgFnS	odjet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
finta	finta	k1gFnSc1	finta
byl	být	k5eAaImAgInS	být
Saturninův	Saturninův	k2eAgInSc1d1	Saturninův
rafinovaný	rafinovaný	k2eAgInSc1d1	rafinovaný
nápad	nápad	k1gInSc1	nápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
končí	končit	k5eAaImIp3nS	končit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
domů	dům	k1gInPc2	dům
a	a	k8xC	a
vracejí	vracet	k5eAaImIp3nP	vracet
se	se	k3xPyFc4	se
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
byli	být	k5eAaImAgMnP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
pozve	pozvat	k5eAaPmIp3nS	pozvat
slečnu	slečna	k1gFnSc4	slečna
Barboru	Barbora	k1gFnSc4	Barbora
na	na	k7c4	na
schůzku	schůzka	k1gFnSc4	schůzka
a	a	k8xC	a
z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
od	od	k7c2	od
Saturnina	Saturnin	k1gMnSc2	Saturnin
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saturnin	Saturnin	k1gMnSc1	Saturnin
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
,	,	kIx,	,
že	že	k8xS	že
houseboat	houseboat	k1gInSc1	houseboat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
patří	patřit	k5eAaImIp3nP	patřit
Saturninovi	Saturninův	k2eAgMnPc1d1	Saturninův
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
jeho	jeho	k3xOp3gMnSc4	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
pán	pán	k1gMnSc1	pán
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
si	se	k3xPyFc3	se
se	s	k7c7	s
Saturninem	Saturnin	k1gMnSc7	Saturnin
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
kancelář	kancelář	k1gFnSc4	kancelář
pro	pro	k7c4	pro
napravování	napravování	k1gNnSc4	napravování
nepravostí	nepravost	k1gFnPc2	nepravost
v	v	k7c6	v
románech	román	k1gInPc6	román
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
pravdy	pravda	k1gFnSc2	pravda
přepracovávat	přepracovávat	k5eAaImF	přepracovávat
přehnané	přehnaný	k2eAgFnPc4d1	přehnaná
scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
do	do	k7c2	do
reálnější	reální	k2eAgFnSc2d2	reální
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Teta	Teta	k1gFnSc1	Teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
se	se	k3xPyFc4	se
bohatě	bohatě	k6eAd1	bohatě
vdala	vdát	k5eAaPmAgFnS	vdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Saturnin	Saturnin	k1gMnSc1	Saturnin
–	–	k?	–
osobitý	osobitý	k2eAgMnSc1d1	osobitý
<g/>
,	,	kIx,	,
nápaditý	nápaditý	k2eAgMnSc1d1	nápaditý
a	a	k8xC	a
pohotový	pohotový	k2eAgMnSc1d1	pohotový
sluha	sluha	k1gMnSc1	sluha
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
,	,	kIx,	,
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
chytrý	chytrý	k2eAgInSc1d1	chytrý
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
Saturninových	Saturninův	k2eAgInPc2d1	Saturninův
činů	čin	k1gInPc2	čin
je	být	k5eAaImIp3nS	být
chránit	chránit	k5eAaImF	chránit
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
a	a	k8xC	a
pomáhat	pomáhat	k5eAaImF	pomáhat
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
(	(	kIx(	(
<g/>
v	v	k7c6	v
románu	román	k1gInSc6	román
beze	beze	k7c2	beze
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
filmu	film	k1gInSc2	film
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
Jiří	Jiří	k1gMnSc1	Jiří
Oulický	Oulický	k2eAgMnSc1d1	Oulický
<g/>
)	)	kIx)	)
–	–	k?	–
třicetiletý	třicetiletý	k2eAgMnSc1d1	třicetiletý
svobodný	svobodný	k2eAgMnSc1d1	svobodný
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
trochu	trochu	k6eAd1	trochu
suchopárný	suchopárný	k2eAgMnSc1d1	suchopárný
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
spokojený	spokojený	k2eAgMnSc1d1	spokojený
s	s	k7c7	s
klidným	klidný	k2eAgInSc7d1	klidný
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Saturninem	Saturnin	k1gMnSc7	Saturnin
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
vmanipulovat	vmanipulovat	k5eAaPmF	vmanipulovat
do	do	k7c2	do
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
absurdních	absurdní	k2eAgFnPc2d1	absurdní
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
slečny	slečna	k1gFnSc2	slečna
Barbory	Barbora	k1gFnSc2	Barbora
tluče	tlouct	k5eAaImIp3nS	tlouct
nemožně	možně	k6eNd1	možně
forhand	forhand	k1gInSc4	forhand
a	a	k8xC	a
servíruje	servírovat	k5eAaBmIp3nS	servírovat
jako	jako	k9	jako
babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
autor	autor	k1gMnSc1	autor
Jirotka	Jirotka	k1gFnSc1	Jirotka
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
oženil	oženit	k5eAaPmAgMnS	oženit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
28	[number]	k4	28
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
–	–	k?	–
hodný	hodný	k2eAgMnSc1d1	hodný
starý	starý	k2eAgMnSc1d1	starý
bohatý	bohatý	k2eAgMnSc1d1	bohatý
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
baví	bavit	k5eAaImIp3nS	bavit
bojem	boj	k1gInSc7	boj
příbuzných	příbuzná	k1gFnPc2	příbuzná
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Zapálený	zapálený	k2eAgMnSc1d1	zapálený
příznivec	příznivec	k1gMnSc1	příznivec
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
domě	dům	k1gInSc6	dům
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Vlach	Vlach	k1gMnSc1	Vlach
–	–	k?	–
padesátiletý	padesátiletý	k2eAgMnSc1d1	padesátiletý
lékař	lékař	k1gMnSc1	lékař
libující	libující	k2eAgMnPc1d1	libující
si	se	k3xPyFc3	se
v	v	k7c6	v
sarkasmu	sarkasmus	k1gInSc6	sarkasmus
a	a	k8xC	a
filipikách	filipika	k1gFnPc6	filipika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
různé	různý	k2eAgFnPc4d1	různá
teorie	teorie	k1gFnPc4	teorie
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slečna	slečna	k1gFnSc1	slečna
Barbora	Barbora	k1gFnSc1	Barbora
Terebová	Terebová	k1gFnSc1	Terebová
–	–	k?	–
milá	milý	k2eAgFnSc1d1	Milá
energická	energický	k2eAgFnSc1d1	energická
moderní	moderní	k2eAgFnSc1d1	moderní
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umí	umět	k5eAaImIp3nS	umět
dobře	dobře	k6eAd1	dobře
hrát	hrát	k5eAaImF	hrát
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teta	Teta	k1gFnSc1	Teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
–	–	k?	–
již	již	k6eAd1	již
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
před	před	k7c7	před
ničím	ničí	k3xOyNgNnSc7	ničí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získala	získat	k5eAaPmAgFnS	získat
dědečkovy	dědečkův	k2eAgInPc4d1	dědečkův
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
mladice	mladice	k1gFnSc1	mladice
<g/>
,	,	kIx,	,
groteskně	groteskně	k6eAd1	groteskně
až	až	k9	až
trapně	trapně	k6eAd1	trapně
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
hopsavě	hopsavě	k6eAd1	hopsavě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podlézavá	podlézavý	k2eAgFnSc1d1	podlézavá
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
účelově	účelově	k6eAd1	účelově
měnit	měnit	k5eAaImF	měnit
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
příslovích	přísloví	k1gNnPc6	přísloví
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
uvádí	uvádět	k5eAaImIp3nS	uvádět
či	či	k8xC	či
komentuje	komentovat	k5eAaBmIp3nS	komentovat
kdekterou	kdekterý	k3yIgFnSc4	kdekterý
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
otravuje	otravovat	k5eAaImIp3nS	otravovat
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Milouš	Milouš	k1gMnSc1	Milouš
–	–	k?	–
syn	syn	k1gMnSc1	syn
tety	teta	k1gFnSc2	teta
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
,	,	kIx,	,
neotesaný	otesaný	k2eNgMnSc1d1	neotesaný
budižkničemu	budižkničemu	k1gMnSc1	budižkničemu
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
světák	světák	k1gMnSc1	světák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jím	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strýc	strýc	k1gMnSc1	strýc
František	František	k1gMnSc1	František
–	–	k?	–
zesnulý	zesnulý	k2eAgMnSc1d1	zesnulý
manžel	manžel	k1gMnSc1	manžel
tety	teta	k1gFnSc2	teta
Kateřiny	Kateřina	k1gFnSc2	Kateřina
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Miloušův	Miloušův	k2eAgMnSc1d1	Miloušův
<g/>
;	;	kIx,	;
někdejší	někdejší	k2eAgMnSc1d1	někdejší
majitel	majitel	k1gMnSc1	majitel
továrničky	továrnička	k1gFnSc2	továrnička
na	na	k7c4	na
čisticí	čisticí	k2eAgInPc4d1	čisticí
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
snem	sen	k1gInSc7	sen
bylo	být	k5eAaImAgNnS	být
vyrobit	vyrobit	k5eAaPmF	vyrobit
úžasné	úžasný	k2eAgNnSc4d1	úžasné
mýdlo	mýdlo	k1gNnSc4	mýdlo
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
neměl	mít	k5eNaImAgInS	mít
pražádné	pražádný	k3yNgNnSc4	pražádný
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
odborné	odborný	k2eAgNnSc4d1	odborné
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
továrně	továrna	k1gFnSc6	továrna
bývaly	bývat	k5eAaImAgFnP	bývat
časté	častý	k2eAgFnPc1d1	častá
exploze	exploze	k1gFnPc1	exploze
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
nehody	nehoda	k1gFnPc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
teta	teta	k1gFnSc1	teta
Kateřina	Kateřina	k1gFnSc1	Kateřina
v	v	k7c6	v
rekordním	rekordní	k2eAgInSc6d1	rekordní
čase	čas	k1gInSc6	čas
podnik	podnik	k1gInSc4	podnik
zruinovat	zruinovat	k5eAaPmF	zruinovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LÁNOVÁ	lánový	k2eAgFnSc1d1	lánová
<g/>
,	,	kIx,	,
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Jirotkův	Jirotkův	k2eAgMnSc1d1	Jirotkův
Saturnin	Saturnin	k1gMnSc1	Saturnin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
televizní	televizní	k2eAgFnSc1d1	televizní
a	a	k8xC	a
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
<g/>
.	.	kIx.	.
,	,	kIx,	,
2017	[number]	k4	2017
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018-02-16	[number]	k4	2018-02-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
Ph.	Ph.	k1gMnSc1	Ph.
<g/>
D.	D.	kA	D.
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUSTYKOVÁ	LUSTYKOVÁ	kA	LUSTYKOVÁ
<g/>
,	,	kIx,	,
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
styl	styl	k1gInSc1	styl
a	a	k8xC	a
jazyková	jazykový	k2eAgFnSc1d1	jazyková
komika	komika	k1gFnSc1	komika
v	v	k7c6	v
románu	román	k1gInSc6	román
Saturnin	Saturnin	k1gMnSc1	Saturnin
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018-02-16	[number]	k4	2018-02-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Ivo	Ivo	k1gMnSc1	Ivo
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠEVČÍKOVÁ	Ševčíková	k1gFnSc1	Ševčíková
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
<g/>
.	.	kIx.	.
</s>
<s>
Poetika	poetika	k1gFnSc1	poetika
próz	próza	k1gFnPc2	próza
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
Saturnin	Saturnin	k1gMnSc1	Saturnin
a	a	k8xC	a
Muž	muž	k1gMnSc1	muž
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018-02-16	[number]	k4	2018-02-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
David	David	k1gMnSc1	David
Kroča	Kroča	k1gMnSc1	Kroča
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠPREŇAROVÁ	ŠPREŇAROVÁ	kA	ŠPREŇAROVÁ
<g/>
,	,	kIx,	,
Bohumila	Bohumila	k1gFnSc1	Bohumila
<g/>
.	.	kIx.	.
</s>
<s>
Srbochorvatský	srbochorvatský	k2eAgInSc1d1	srbochorvatský
překlad	překlad	k1gInSc1	překlad
přísloví	přísloví	k1gNnSc2	přísloví
a	a	k8xC	a
pořekadel	pořekadlo	k1gNnPc2	pořekadlo
v	v	k7c6	v
Jirotkově	Jirotkův	k2eAgMnSc6d1	Jirotkův
Saturninovi	Saturnin	k1gMnSc6	Saturnin
<g/>
.	.	kIx.	.
,	,	kIx,	,
2006	[number]	k4	2006
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018-02-16	[number]	k4	2018-02-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Mgr.	Mgr.	kA	Mgr.
Pavel	Pavel	k1gMnSc1	Pavel
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Ph.	Ph.	k1gMnSc1	Ph.
<g/>
D.	D.	kA	D.
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠTENCOVÁ	ŠTENCOVÁ	kA	ŠTENCOVÁ
<g/>
,	,	kIx,	,
Linda	Linda	k1gFnSc1	Linda
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
analýza	analýza	k1gFnSc1	analýza
francouzského	francouzský	k2eAgInSc2d1	francouzský
překladu	překlad	k1gInSc2	překlad
románu	román	k1gInSc2	román
Saturnin	Saturnin	k1gMnSc1	Saturnin
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirotky	Jirotka	k1gFnSc2	Jirotka
<g/>
.	.	kIx.	.
,	,	kIx,	,
2016	[number]	k4	2016
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018-02-16	[number]	k4	2018-02-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
49	[number]	k4	49
s	s	k7c7	s
<g/>
.	.	kIx.	.
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
francouzského	francouzský	k2eAgInSc2d1	francouzský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
práce	práce	k1gFnSc1	práce
PhDr.	PhDr.	kA	PhDr.
Renáta	Renáta	k1gFnSc1	Renáta
Listíková	Listíková	k1gFnSc1	Listíková
<g/>
,	,	kIx,	,
Dr.	dr.	kA	dr.
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Saturnin	Saturnin	k1gMnSc1	Saturnin
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
knize	kniha	k1gFnSc6	kniha
</s>
</p>
