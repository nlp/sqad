<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
Maximilian	Maximilian	k1gMnSc1
Herzog	Herzog	k1gMnSc1
in	in	k?
Bayern	Bayern	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
<g/>
;	;	kIx,
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1808	#num#	k4
<g/>
,	,	kIx,
Bamberk	Bamberk	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1888	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
bavorský	bavorský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
vedlejší	vedlejší	k2eAgFnSc2d1
falcké	falcký	k2eAgFnSc2d1
rodové	rodový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
Wittelsbachů	Wittelsbach	k1gInPc2
z	z	k7c2
Birkenfeldu-Zweibrückenu	Birkenfeldu-Zweibrücken	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
titul	titul	k1gInSc4
formálně	formálně	k6eAd1
zněl	znět	k5eAaImAgMnS
vévoda	vévoda	k1gMnSc1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
(	(	kIx(
<g/>
nikoli	nikoli	k9
tedy	tedy	k9
vévoda	vévoda	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
„	„	k?
<g/>
von	von	k1gInSc1
Bayern	Bayern	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
„	„	k?
<g/>
in	in	k?
Bayern	Bayern	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>