<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
vévoda	vévoda	k1gMnSc1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1808	#num#	k4
</s>
<s>
Bamberk	Bamberk	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1888	#num#	k4
(	(	kIx(
<g/>
79	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Mnichov	Mnichov	k1gInSc1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Ludovika	Ludovika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
BavorskýVilém	BavorskýVilý	k2eAgInSc6d1
Karel	Karla	k1gFnPc2
BavorskýHelena	BavorskýHelen	k2eAgFnSc1d1
BavorskáAlžběta	BavorskáAlžběta	k1gFnSc1
BavorskáKarel	BavorskáKarela	k1gFnPc2
Teodor	Teodor	k1gMnSc1
BavorskýMarie	BavorskýMarie	k1gFnSc2
BavorskáMatylda	BavorskáMatylda	k1gMnSc1
BavorskáMaxmilián	BavorskáMaxmilián	k1gMnSc1
BavorskýSofie	BavorskýSofie	k1gFnSc2
BavorskáMaxmilián	BavorskáMaxmilián	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Wittelsbachové	Wittelsbachové	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Pius	Pius	k1gMnSc1
Augustus	Augustus	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Arenberková	Arenberková	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
Maximilian	Maximilian	k1gMnSc1
Herzog	Herzog	k1gMnSc1
in	in	k?
Bayern	Bayern	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
<g/>
;	;	kIx,
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1808	#num#	k4
<g/>
,	,	kIx,
Bamberk	Bamberk	k1gInSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1888	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
bavorský	bavorský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
z	z	k7c2
vedlejší	vedlejší	k2eAgFnSc2d1
falcké	falcký	k2eAgFnSc2d1
rodové	rodový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
Wittelsbachů	Wittelsbach	k1gInPc2
z	z	k7c2
Birkenfeldu-Zweibrückenu	Birkenfeldu-Zweibrücken	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
titul	titul	k1gInSc4
formálně	formálně	k6eAd1
zněl	znět	k5eAaImAgMnS
vévoda	vévoda	k1gMnSc1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
(	(	kIx(
<g/>
nikoli	nikoli	k9
tedy	tedy	k9
vévoda	vévoda	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
„	„	k?
<g/>
von	von	k1gInSc1
Bayern	Bayern	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
„	„	k?
<g/>
in	in	k?
Bayern	Bayern	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1845	#num#	k4
užívali	užívat	k5eAaImAgMnP
členové	člen	k1gMnPc1
této	tento	k3xDgFnSc2
větve	větev	k1gFnSc2
oslovení	oslovení	k1gNnSc2
„	„	k?
<g/>
královské	královský	k2eAgFnSc2d1
Výsosti	výsost	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
císařovny	císařovna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
<g/>
,	,	kIx,
zvané	zvaný	k2eAgFnSc2d1
Sisi	Sis	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Helene	Helen	k1gInSc5
Bavorské	bavorský	k2eAgFnSc2d1
<g/>
,	,	kIx,
zvané	zvaný	k2eAgFnSc2d1
Nené	Nená	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
syna	syn	k1gMnSc4
bavorského	bavorský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Pia	Pius	k1gMnSc2
Augusta	August	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
ženy	žena	k1gFnSc2
Amálie	Amálie	k1gFnSc2
Luisy	Luisa	k1gFnSc2
<g/>
,	,	kIx,
princezny	princezna	k1gFnSc2
a	a	k8xC
vévodkyně	vévodkyně	k1gFnSc2
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1
výchovy	výchova	k1gFnPc1
se	se	k3xPyFc4
finančně	finančně	k6eAd1
ujal	ujmout	k5eAaPmAgMnS
bavorský	bavorský	k2eAgMnSc1d1
král	král	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
Max	Max	k1gMnSc1
do	do	k7c2
mnichovského	mnichovský	k2eAgInSc2d1
Královského	královský	k2eAgInSc2d1
výchovného	výchovný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
pro	pro	k7c4
studující	studující	k2eAgFnSc4d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
probudil	probudit	k5eAaPmAgInS
velký	velký	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
literaturu	literatura	k1gFnSc4
–	–	k?
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
v	v	k7c6
průběhu	průběh	k1gInSc6
studia	studio	k1gNnSc2
věnovat	věnovat	k5eAaPmF,k5eAaImF
literární	literární	k2eAgFnSc4d1
činnosti	činnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1825	#num#	k4
navštěvoval	navštěvovat	k5eAaImAgInS
přednášky	přednáška	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Landshutu	Landshut	k1gInSc6
a	a	k8xC
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Max	Max	k1gMnSc1
také	také	k6eAd1
majitelem	majitel	k1gMnSc7
9	#num#	k4
<g/>
.	.	kIx.
řadového	řadový	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1828	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
se	s	k7c7
sestřenicí	sestřenice	k1gFnSc7
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Ludovikou	Ludovika	k1gFnSc7
Vilemínou	Vilemína	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
krále	král	k1gMnSc2
Maxmiliána	Maxmilián	k1gMnSc2
I.	I.	kA
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
sňatek	sňatek	k1gInSc1
byl	být	k5eAaImAgInS
dohodnut	dohodnout	k5eAaPmNgInS
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byli	být	k5eAaImAgMnP
budoucí	budoucí	k2eAgMnPc1d1
novomanželé	novomanžel	k1gMnPc1
ještě	ještě	k6eAd1
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Max	Max	k1gMnSc1
a	a	k8xC
Ludovika	Ludovika	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
spolu	spolu	k6eAd1
nakonec	nakonec	k6eAd1
měli	mít	k5eAaImAgMnP
devět	devět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
do	do	k7c2
historie	historie	k1gFnSc2
nejvýrazněji	výrazně	k6eAd3
zasáhla	zasáhnout	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnSc1
dcera	dcera	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
Jejich	jejich	k3xOp3gFnSc4
manželství	manželství	k1gNnSc1
bylo	být	k5eAaImAgNnS
ale	ale	k9
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
nešťastné	šťastný	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Max	Max	k1gMnSc1
měl	mít	k5eAaImAgMnS
řadu	řada	k1gFnSc4
milenek	milenka	k1gFnPc2
a	a	k8xC
nebyl	být	k5eNaImAgInS
vůbec	vůbec	k9
rodinný	rodinný	k2eAgInSc1d1
typ	typ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgFnPc3
nemanželským	manželský	k2eNgFnPc3d1
dětem	dítě	k1gFnPc3
často	často	k6eAd1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
větší	veliký	k2eAgFnSc4d2
pozornost	pozornost	k1gFnSc4
než	než	k8xS
dětem	dítě	k1gFnPc3
manželským	manželský	k2eAgFnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
z	z	k7c2
tehdejšího	tehdejší	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
byl	být	k5eAaImAgMnS
podivín	podivín	k1gMnSc1
–	–	k?
etiketa	etiketa	k1gFnSc1
mu	on	k3xPp3gMnSc3
moc	moc	k6eAd1
neříkala	říkat	k5eNaImAgFnS
<g/>
,	,	kIx,
rád	rád	k6eAd1
se	se	k3xPyFc4
oblékal	oblékat	k5eAaImAgMnS
do	do	k7c2
lidového	lidový	k2eAgInSc2d1
kroje	kroj	k1gInSc2
a	a	k8xC
udržoval	udržovat	k5eAaImAgMnS
styk	styk	k1gInSc4
s	s	k7c7
prostými	prostý	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
a	a	k8xC
měšťany	měšťan	k1gMnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
jeho	jeho	k3xOp3gNnSc4
příbuzní	příbuzný	k1gMnPc1
nemohli	moct	k5eNaImAgMnP
odpustit	odpustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
oblíbeném	oblíbený	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Possenhofenu	Possenhofen	k1gInSc6
kolem	kolem	k6eAd1
sebe	sebe	k3xPyFc4
shromažďoval	shromažďovat	k5eAaImAgMnS
umělce	umělec	k1gMnPc4
a	a	k8xC
vědce	vědec	k1gMnPc4
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
psal	psát	k5eAaImAgInS
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Phantasus	Phantasus	k1gMnSc1
básně	báseň	k1gFnPc4
<g/>
,	,	kIx,
novely	novela	k1gFnPc4
a	a	k8xC
dramata	drama	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
veselého	veselý	k2eAgMnSc4d1
kumpána	kumpána	k?
–	–	k?
rád	rád	k6eAd1
dobře	dobře	k6eAd1
jedl	jíst	k5eAaImAgMnS
a	a	k8xC
pil	pít	k5eAaImAgMnS
a	a	k8xC
na	na	k7c6
hostinách	hostina	k1gFnPc6
pro	pro	k7c4
přátele	přítel	k1gMnPc4
nešetřil	šetřit	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
„	„	k?
<g/>
stolní	stolní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
smysl	smysl	k1gInSc1
spočíval	spočívat	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roli	role	k1gFnSc6
krále	král	k1gMnSc2
Artuše	Artuš	k1gMnSc2
předsedal	předsedat	k5eAaImAgMnS
svým	svůj	k3xOyFgInSc7
čtrnácti	čtrnáct	k4xCc7
přátelům	přítel	k1gMnPc3
a	a	k8xC
vzájemně	vzájemně	k6eAd1
soutěžili	soutěžit	k5eAaImAgMnP
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
víc	hodně	k6eAd2
sní	sníst	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
vypije	vypít	k5eAaPmIp3nS
<g/>
,	,	kIx,
případně	případně	k6eAd1
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
uloví	ulovit	k5eAaPmIp3nS
víc	hodně	k6eAd2
zvěře	zvěř	k1gFnSc2
na	na	k7c6
honu	hon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1845	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
společnosti	společnost	k1gFnSc2
Stará	starý	k2eAgFnSc1d1
Anglie	Anglie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
úkol	úkol	k1gInSc4
pečovat	pečovat	k5eAaImF
o	o	k7c4
družnost	družnost	k1gFnSc4
a	a	k8xC
humor	humor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
velkým	velký	k2eAgMnSc7d1
znalcem	znalec	k1gMnSc7
a	a	k8xC
podporovatelem	podporovatel	k1gMnSc7
bavorské	bavorský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
a	a	k8xC
údajně	údajně	k6eAd1
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
citeru	citera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
jeho	jeho	k3xOp3gFnSc1
družnost	družnost	k1gFnSc1
a	a	k8xC
nenucené	nucený	k2eNgNnSc1d1
užívání	užívání	k1gNnSc1
radovánek	radovánka	k1gFnPc2
stálo	stát	k5eAaImAgNnS
v	v	k7c6
cestě	cesta	k1gFnSc6
spokojenému	spokojený	k2eAgInSc3d1
manželství	manželství	k1gNnSc1
–	–	k?
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
žena	žena	k1gFnSc1
uvažovala	uvažovat	k5eAaImAgFnS
vždy	vždy	k6eAd1
spíše	spíše	k9
prakticky	prakticky	k6eAd1
a	a	k8xC
střízlivě	střízlivě	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
1888	#num#	k4
ho	on	k3xPp3gInSc4
postihl	postihnout	k5eAaPmAgInS
první	první	k4xOgInSc4
záchvat	záchvat	k1gInSc4
mrtvice	mrtvice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
následky	následek	k1gInPc4
dalšího	další	k2eAgInSc2d1
záchvatu	záchvat	k1gInSc2
zemřel	zemřít	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1888	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Maxmiliánova	Maxmiliánův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
a	a	k8xC
královna	královna	k1gFnSc1
obojí	oboj	k1gFnPc2
Sicílie	Sicílie	k1gFnSc1
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
hrdinka	hrdinka	k1gFnSc1
z	z	k7c2
Gaety	Gaeta	k1gFnSc2
</s>
<s>
vévoda	vévoda	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1828	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
nejmladší	mladý	k2eAgFnSc7d3
dcerou	dcera	k1gFnSc7
bavorského	bavorský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Maxmiliána	Maxmilián	k1gMnSc2
I.	I.	kA
Josefa	Josef	k1gMnSc2
<g/>
,	,	kIx,
Ludovikou	Ludovika	k1gFnSc7
a	a	k8xC
měl	mít	k5eAaImAgInS
s	s	k7c7
ní	on	k3xPp3gFnSc7
devět	devět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
(	(	kIx(
<g/>
1831	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
1859	#num#	k4
Jindřiška	Jindřiška	k1gFnSc1
Mendlová	Mendlová	k1gFnSc1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
–	–	k?
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
svobodná	svobodný	k2eAgFnSc1d1
paní	paní	k1gFnSc1
z	z	k7c2
Wallersee	Wallerse	k1gInSc2
</s>
<s>
~	~	kIx~
1892	#num#	k4
Antonie	Antonie	k1gFnSc1
Barthová	Barthová	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
von	von	k1gInSc1
Bartholf	Bartholf	k1gInSc1
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
–	–	k?
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1832	#num#	k4
<g/>
–	–	k?
<g/>
1833	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Helena	Helena	k1gFnSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1858	#num#	k4
kníže	kníže	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Anton	Anton	k1gMnSc1
Thurn-Taxis	Thurn-Taxis	k1gFnSc1
(	(	kIx(
<g/>
1831	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1898	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1854	#num#	k4
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Teodor	Teodor	k1gMnSc1
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
1865	#num#	k4
Žofie	Žofie	k1gFnSc1
Saská	saský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
1874	#num#	k4
Marie	Maria	k1gFnSc2
Josefa	Josefa	k1gFnSc1
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
(	(	kIx(
<g/>
1841	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1859	#num#	k4
král	král	k1gMnSc1
obojí	oboj	k1gFnPc2
Sicílie	Sicílie	k1gFnSc2
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
–	–	k?
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Matylda	Matylda	k1gFnSc1
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1861	#num#	k4
hrabě	hrabě	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Trani	Traň	k1gMnSc3
</s>
<s>
Žofie	Žofie	k1gFnSc1
(	(	kIx(
<g/>
1847	#num#	k4
<g/>
–	–	k?
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1868	#num#	k4
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Alenconu	Alencon	k1gInSc2
Ferdinand	Ferdinand	k1gMnSc1
Orleánský	orleánský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1844	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Max	Max	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
–	–	k?
<g/>
1893	#num#	k4
<g/>
)	)	kIx)
~	~	kIx~
1875	#num#	k4
Amálie	Amálie	k1gFnSc2
Sasko-Coburská	Sasko-Coburský	k2eAgNnPc4d1
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Jan	Jan	k1gMnSc1
Karel	Karel	k1gMnSc1
Falcko-Gelnhausenský	Falcko-Gelnhausenský	k2eAgMnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Falcko-Gelnhausenský	Falcko-Gelnhausenský	k2eAgMnSc1d1
</s>
<s>
Ester	Ester	k1gFnSc1
Marie	Maria	k1gFnSc2
von	von	k1gInSc1
Witzleben	Witzleben	k2eAgInSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Salm-Dhaunský	Salm-Dhaunský	k1gMnSc1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Šarlota	Šarlota	k1gFnSc1
Salm-Dhaunská	Salm-Dhaunská	k1gFnSc1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Nasavsko-Ottweilerská	Nasavsko-Ottweilerský	k2eAgFnSc1d1
</s>
<s>
Pius	Pius	k1gMnSc1
Augustus	Augustus	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Michal	Michal	k1gMnSc1
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Nasavsko-Saarbrückenská	Nasavsko-Saarbrückenský	k2eAgFnSc1d1
</s>
<s>
Maria	Maria	k1gFnSc1
Anna	Anna	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Karel	Karel	k1gMnSc1
Falcko-Sulzbašský	Falcko-Sulzbašský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Františka	Františka	k1gFnSc1
Falcko-Sulzbašská	Falcko-Sulzbašský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Augusta	August	k1gMnSc2
Falcko-Neuburská	Falcko-Neuburský	k2eAgNnPc1d1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
Filip	Filip	k1gMnSc1
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
</s>
<s>
Karel	Karel	k1gMnSc1
Maria	Mario	k1gMnSc2
Raimund	Raimunda	k1gFnPc2
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
</s>
<s>
Maria	Maria	k1gFnSc1
Francesca	Francesc	k2eAgFnSc1d1
Pignatelli	Pignatelle	k1gFnSc3
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
z	z	k7c2
Arenbergu	Arenberg	k1gInSc2
</s>
<s>
Louis	Louis	k1gMnSc1
Engelbert	Engelbert	k1gMnSc1
de	de	k?
La	la	k1gNnSc2
Marck	Marcka	k1gFnPc2
</s>
<s>
Louise-Marguerite	Louise-Marguerit	k1gInSc5
de	de	k?
la	la	k1gNnPc1
Marck	Marck	k1gInSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Anne	Anne	k1gFnSc1
de	de	k?
Visdelou	Visdelý	k2eAgFnSc7d1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Arenberková	Arenberková	k1gFnSc1
</s>
<s>
Louis	Louis	k1gMnSc1
de	de	k?
Mailly	Mailla	k1gFnSc2
</s>
<s>
Louis	Louis	k1gMnSc1
Joseph	Joseph	k1gMnSc1
de	de	k?
Mailly	Mailla	k1gFnSc2
</s>
<s>
Anna	Anna	k1gFnSc1
de	de	k?
Melun	Melun	k1gInSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Adélaï	Adélaï	k1gInSc5
Julie	Julie	k1gFnSc1
de	de	k?
Mailly	Mailla	k1gFnSc2
</s>
<s>
Emmanuel	Emmanuel	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Hautefort	Hautefort	k1gInSc1
</s>
<s>
Adélaï	Adélaï	k6eAd1
Julie	Julie	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Hautefort	Hautefort	k1gInSc1
</s>
<s>
Françoise-Claire	Françoise-Clair	k1gMnSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Harcourt	Harcourt	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DICKINGER	DICKINGER	kA
<g/>
,	,	kIx,
Christian	Christian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černé	Černé	k2eAgFnPc4d1
ovce	ovce	k1gFnPc4
Wittelsbachů	Wittelsbach	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
259	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://genealogy.euweb.cz/wittel/wittel6.html#WiB	http://genealogy.euweb.cz/wittel/wittel6.html#WiB	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RALL	RALL	kA
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
<g/>
;	;	kIx,
RALL	RALL	kA
<g/>
,	,	kIx,
Marga	Marga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Wittelsbacher	Wittelsbachra	k1gFnPc2
in	in	k?
Lebensbildern	Lebensbilderna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Graz	Graz	k1gInSc1
;	;	kIx,
Wien	Wien	k1gInSc1
;	;	kIx,
Köln	Köln	k1gInSc1
;	;	kIx,
Regensburg	Regensburg	k1gInSc1
<g/>
:	:	kIx,
Styria	Styrium	k1gNnSc2
;	;	kIx,
Pustet	Pustet	k1gMnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
431	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
222	#num#	k4
<g/>
-	-	kIx~
<g/>
11669	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Maxmilián	Maxmilián	k1gMnSc1
Josef	Josef	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118967592	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0922	#num#	k4
0181	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79013701	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27871180	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79013701	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
</s>
