<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
orgán	orgán	k1gInSc1	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
kontrolu	kontrola	k1gFnSc4	kontrola
hospodaření	hospodaření	k1gNnSc2	hospodaření
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
plnění	plnění	k1gNnSc1	plnění
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
po	po	k7c4	po
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
existovalo	existovat	k5eAaImAgNnS	existovat
Federální	federální	k2eAgNnSc1d1	federální
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kontroly	kontrola	k1gFnSc2	kontrola
a	a	k8xC	a
republiková	republikový	k2eAgNnPc1d1	republikové
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
kontroly	kontrola	k1gFnSc2	kontrola
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
připravováno	připravován	k2eAgNnSc1d1	připravováno
obnovení	obnovení	k1gNnSc1	obnovení
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
navazujícího	navazující	k2eAgInSc2d1	navazující
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
prvorepublikového	prvorepublikový	k2eAgInSc2d1	prvorepublikový
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
účetního	účetní	k2eAgInSc2d1	účetní
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
NKÚ	NKÚ	kA	NKÚ
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
upraven	upravit	k5eAaPmNgInS	upravit
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
hlavě	hlava	k1gFnSc6	hlava
páté	pátá	k1gFnSc2	pátá
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
mimo	mimo	k7c4	mimo
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgFnSc4d1	soudní
<g/>
,	,	kIx,	,
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
a	a	k8xC	a
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
pilíř	pilíř	k1gInSc1	pilíř
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
zárodek	zárodek	k1gInSc1	zárodek
možné	možný	k2eAgFnSc2d1	možná
moci	moc	k1gFnSc2	moc
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
působnost	působnost	k1gFnSc1	působnost
<g/>
,	,	kIx,	,
organizační	organizační	k2eAgFnSc1d1	organizační
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
podrobnosti	podrobnost	k1gFnPc4	podrobnost
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
166	[number]	k4	166
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
Nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
kontrolním	kontrolní	k2eAgInSc6d1	kontrolní
úřadu	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
NKÚ	NKÚ	kA	NKÚ
je	být	k5eAaImIp3nS	být
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Tokovo	Tokův	k2eAgNnSc1d1	Tokův
v	v	k7c6	v
Praze-Holešovicích	Praze-Holešovice	k1gFnPc6	Praze-Holešovice
<g/>
,	,	kIx,	,
u	u	k7c2	u
Libeňského	libeňský	k2eAgInSc2d1	libeňský
mostu	most	k1gInSc2	most
a	a	k8xC	a
holešovického	holešovický	k2eAgInSc2d1	holešovický
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Nájemní	nájemní	k2eAgFnSc1d1	nájemní
smlouva	smlouva	k1gFnSc1	smlouva
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
skončit	skončit	k5eAaPmF	skončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
PPF	PPF	kA	PPF
údajně	údajně	k6eAd1	údajně
za	za	k7c4	za
pronájem	pronájem	k1gInSc4	pronájem
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
související	související	k2eAgFnPc4d1	související
služby	služba	k1gFnPc4	služba
inkasuje	inkasovat	k5eAaBmIp3nS	inkasovat
od	od	k7c2	od
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
asi	asi	k9	asi
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
vlastníci	vlastník	k1gMnPc1	vlastník
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šmejc	Šmejc	k1gFnSc4	Šmejc
však	však	k9	však
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
odvádějí	odvádět	k5eAaImIp3nP	odvádět
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prý	prý	k9	prý
jejich	jejich	k3xOp3gInPc1	jejich
příjmy	příjem	k1gInPc1	příjem
nepocházejí	pocházet	k5eNaImIp3nP	pocházet
z	z	k7c2	z
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
NKÚ	NKÚ	kA	NKÚ
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
tiskové	tiskový	k2eAgNnSc1d1	tiskové
prohlášení	prohlášení	k1gNnSc3	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
NKÚ	NKÚ	kA	NKÚ
na	na	k7c6	na
základě	základ	k1gInSc6	základ
opčního	opční	k2eAgNnSc2d1	opční
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
trvajících	trvající	k2eAgNnPc2d1	trvající
jednání	jednání	k1gNnPc2	jednání
s	s	k7c7	s
Vládní	vládní	k2eAgFnSc7d1	vládní
dislokační	dislokační	k2eAgFnSc7d1	dislokační
komisí	komise	k1gFnSc7	komise
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
nájem	nájem	k1gInSc1	nájem
budovy	budova	k1gFnSc2	budova
Tokovo	Tokovo	k1gNnSc4	Tokovo
o	o	k7c4	o
1,5	[number]	k4	1,5
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
září	září	k1gNnSc2	září
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
roční	roční	k2eAgNnSc1d1	roční
nájemné	nájemné	k1gNnSc1	nájemné
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
aktuální	aktuální	k2eAgFnSc2d1	aktuální
výše	výše	k1gFnSc2	výše
základního	základní	k2eAgNnSc2d1	základní
nájemného	nájemné	k1gNnSc2	nájemné
44,15	[number]	k4	44,15
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
NKÚ	NKÚ	kA	NKÚ
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pronajato	pronajmout	k5eAaPmNgNnS	pronajmout
9842	[number]	k4	9842
m2	m2	k4	m2
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
393	[number]	k4	393
m2	m2	k4	m2
skladů	sklad	k1gInPc2	sklad
a	a	k8xC	a
75	[number]	k4	75
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
stání	stání	k1gNnPc2	stání
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
prezidenta	prezident	k1gMnSc4	prezident
NKÚ	NKÚ	kA	NKÚ
Františka	František	k1gMnSc4	František
Dohnala	Dohnal	k1gMnSc4	Dohnal
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
situaci	situace	k1gFnSc4	situace
neřešil	řešit	k5eNaImAgMnS	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
hájil	hájit	k5eAaImAgMnS	hájit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nájem	nájem	k1gInSc1	nájem
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stát	stát	k1gInSc1	stát
není	být	k5eNaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
nabídnout	nabídnout	k5eAaPmF	nabídnout
úřadu	úřad	k1gInSc3	úřad
žádnou	žádný	k3yNgFnSc4	žádný
svou	svůj	k3xOyFgFnSc4	svůj
budovu	budova	k1gFnSc4	budova
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
a	a	k8xC	a
vybavení	vybavení	k1gNnSc1	vybavení
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
analýzy	analýza	k1gFnSc2	analýza
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Knight	Knight	k2eAgInSc4d1	Knight
Frank	frank	k1gInSc4	frank
stály	stát	k5eAaImAgInP	stát
dohromady	dohromady	k6eAd1	dohromady
až	až	k9	až
810	[number]	k4	810
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
usnesení	usnesení	k1gNnSc6	usnesení
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
výboru	výbor	k1gInSc2	výbor
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
Česká	český	k2eAgFnSc1d1	Česká
pozice	pozice	k1gFnSc1	pozice
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc4	společnost
Knight	Knight	k2eAgMnSc1d1	Knight
Frank	Frank	k1gMnSc1	Frank
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
pronájem	pronájem	k1gInSc4	pronájem
zbylých	zbylý	k2eAgFnPc2d1	zbylá
volných	volný	k2eAgFnPc2d1	volná
ploch	plocha	k1gFnPc2	plocha
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Tokovo	Tokovo	k1gNnSc1	Tokovo
<g/>
.	.	kIx.	.
</s>
<s>
Police	police	k1gFnSc1	police
sjednávání	sjednávání	k1gNnSc2	sjednávání
nájmu	nájem	k1gInSc2	nájem
prošetřovala	prošetřovat	k5eAaImAgNnP	prošetřovat
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
porušení	porušení	k1gNnSc2	porušení
povinnosti	povinnost	k1gFnSc2	povinnost
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
cizího	cizí	k2eAgInSc2d1	cizí
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
případ	případ	k1gInSc4	případ
odložila	odložit	k5eAaPmAgFnS	odložit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
,	,	kIx,	,
státní	státní	k2eAgMnPc4d1	státní
zástupce	zástupce	k1gMnPc4	zástupce
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
7	[number]	k4	7
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
policie	policie	k1gFnSc2	policie
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
orgány	orgán	k1gInPc4	orgán
NKÚ	NKÚ	kA	NKÚ
patří	patřit	k5eAaImIp3nS	patřit
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
kolegium	kolegium	k1gNnSc1	kolegium
<g/>
,	,	kIx,	,
senáty	senát	k1gInPc1	senát
a	a	k8xC	a
Kárná	kárný	k2eAgFnSc1d1	kárná
komora	komora	k1gFnSc1	komora
Úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
pořádek	pořádek	k1gInSc1	pořádek
upravuje	upravovat	k5eAaImIp3nS	upravovat
organizační	organizační	k2eAgInSc4d1	organizační
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
jednací	jednací	k2eAgInPc4d1	jednací
řády	řád	k1gInPc4	řád
Kolegia	kolegium	k1gNnSc2	kolegium
<g/>
,	,	kIx,	,
senátů	senát	k1gInPc2	senát
a	a	k8xC	a
kárný	kárný	k2eAgInSc4d1	kárný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
NKÚ	NKÚ	kA	NKÚ
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
jmenování	jmenování	k1gNnSc1	jmenování
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
kontrasignaci	kontrasignace	k1gFnSc4	kontrasignace
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Lubomír	Lubomír	k1gMnSc1	Lubomír
Voleník	Voleník	k1gMnSc1	Voleník
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
František	František	k1gMnSc1	František
Dohnal	Dohnal	k1gMnSc1	Dohnal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
vybrala	vybrat	k5eAaPmAgFnS	vybrat
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
za	za	k7c4	za
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
NKÚ	NKÚ	kA	NKÚ
Miloslava	Miloslav	k1gMnSc4	Miloslav
Kalu	Kala	k1gMnSc4	Kala
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kolegium	kolegium	k1gNnSc1	kolegium
Úřadu	úřad	k1gInSc2	úřad
tvoří	tvořit	k5eAaImIp3nS	tvořit
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
viceprezident	viceprezident	k1gMnSc1	viceprezident
a	a	k8xC	a
15	[number]	k4	15
členů	člen	k1gInPc2	člen
Úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc4	člen
volí	volit	k5eAaImIp3nS	volit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
prezidenta	prezident	k1gMnSc2	prezident
Úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
bez	bez	k7c2	bez
časového	časový	k2eAgNnSc2d1	časové
omezení	omezení	k1gNnSc2	omezení
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
funkce	funkce	k1gFnSc1	funkce
zaniká	zanikat	k5eAaImIp3nS	zanikat
<g/>
:	:	kIx,	:
doručením	doručení	k1gNnSc7	doručení
rezignace	rezignace	k1gFnSc2	rezignace
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
,	,	kIx,	,
dosažením	dosažení	k1gNnSc7	dosažení
věku	věk	k1gInSc2	věk
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
odvoláním	odvolání	k1gNnSc7	odvolání
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Kárné	kárný	k2eAgFnSc2d1	kárná
komory	komora	k1gFnSc2	komora
NKÚ	NKÚ	kA	NKÚ
<g/>
,	,	kIx,	,
právní	právní	k2eAgFnSc1d1	právní
mocí	moc	k1gFnSc7	moc
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
člen	člen	k1gMnSc1	člen
omezen	omezit	k5eAaPmNgMnS	omezit
ve	v	k7c6	v
svéprávnosti	svéprávnost	k1gFnSc6	svéprávnost
nebo	nebo	k8xC	nebo
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
pro	pro	k7c4	pro
úmyslný	úmyslný	k2eAgInSc4d1	úmyslný
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
skládají	skládat	k5eAaImIp3nP	skládat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
)	)	kIx)	)
či	či	k8xC	či
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
)	)	kIx)	)
tento	tento	k3xDgInSc4	tento
slib	slib	k1gInSc4	slib
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
věrnost	věrnost	k1gFnSc4	věrnost
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
zachovávat	zachovávat	k5eAaImF	zachovávat
její	její	k3xOp3gFnSc4	její
Ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
vykonávat	vykonávat	k5eAaImF	vykonávat
nezávisle	závisle	k6eNd1	závisle
a	a	k8xC	a
nestranně	stranně	k6eNd1	stranně
a	a	k8xC	a
nezneužiji	zneužít	k5eNaPmIp1nS	zneužít
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Miloslav	Miloslav	k1gMnSc1	Miloslav
Kala	Kala	k1gMnSc1	Kala
(	(	kIx(	(
<g/>
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Horníková	Horníková	k1gFnSc1	Horníková
(	(	kIx(	(
<g/>
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Adámek	Adámek	k1gMnSc1	Adámek
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Adolf	Adolf	k1gMnSc1	Adolf
Beznoska	beznoska	k1gMnSc1	beznoska
(	(	kIx(	(
<g/>
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Pavel	Pavel	k1gMnSc1	Pavel
Hrnčíř	Hrnčíř	k1gMnSc1	Hrnčíř
(	(	kIx(	(
<g/>
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Hana	Hana	k1gFnSc1	Hana
Hykšová	Hykšová	k1gFnSc1	Hykšová
(	(	kIx(	(
<g/>
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Kalivoda	Kalivoda	k1gMnSc1	Kalivoda
(	(	kIx(	(
<g/>
od	od	k7c2	od
17	[number]	k4	17
<g />
.	.	kIx.	.
</s>
<s>
<g/>
září	září	k1gNnSc1	září
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Kubíček	Kubíček	k1gMnSc1	Kubíček
(	(	kIx(	(
<g/>
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Málek	Málek	k1gMnSc1	Málek
(	(	kIx(	(
<g/>
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
Neuvirt	Neuvirta	k1gFnPc2	Neuvirta
(	(	kIx(	(
<g/>
od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Hana	Hana	k1gFnSc1	Hana
Pýchová	Pýchová	k1gFnSc1	Pýchová
(	(	kIx(	(
<g/>
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Daniel	Daniel	k1gMnSc1	Daniel
Reisiegel	Reisiegel	k1gMnSc1	Reisiegel
(	(	kIx(	(
<g/>
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Stárek	Stárek	k1gMnSc1	Stárek
(	(	kIx(	(
<g/>
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Jaromíra	Jaromíra	k1gFnSc1	Jaromíra
Steidlová	Steidlový	k2eAgFnSc1d1	Steidlová
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Vedral	Vedral	k1gMnSc1	Vedral
(	(	kIx(	(
<g/>
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
NKÚ	NKÚ	kA	NKÚ
pracuje	pracovat	k5eAaImIp3nS	pracovat
buď	buď	k8xC	buď
v	v	k7c6	v
Kolegiu	kolegium	k1gNnSc6	kolegium
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
senátech	senát	k1gInPc6	senát
složených	složený	k2eAgFnPc2d1	složená
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
členů	člen	k1gInPc2	člen
Úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
působnosti	působnost	k1gFnSc6	působnost
NKÚ	NKÚ	kA	NKÚ
je	být	k5eAaImIp3nS	být
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
:	:	kIx,	:
hospodaření	hospodaření	k1gNnSc1	hospodaření
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
vybíranými	vybíraný	k2eAgInPc7d1	vybíraný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
prostředků	prostředek	k1gInPc2	prostředek
vybíraných	vybíraný	k2eAgInPc2d1	vybíraný
obcemi	obec	k1gFnPc7	obec
nebo	nebo	k8xC	nebo
kraji	kraj	k1gInPc7	kraj
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
samostatné	samostatný	k2eAgFnSc6d1	samostatná
působnosti	působnost	k1gFnSc6	působnost
státní	státní	k2eAgInSc4d1	státní
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
účet	účet	k1gInSc4	účet
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
plnění	plnění	k1gNnSc1	plnění
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
hospodaření	hospodaření	k1gNnSc1	hospodaření
s	s	k7c7	s
prostředky	prostředek	k1gInPc7	prostředek
poskytnutými	poskytnutý	k2eAgInPc7d1	poskytnutý
státu	stát	k1gInSc2	stát
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
s	s	k7c7	s
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgMnPc4	jenž
převzal	převzít	k5eAaPmAgMnS	převzít
stát	stát	k5eAaPmF	stát
záruky	záruka	k1gFnPc4	záruka
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc4	vydávání
a	a	k8xC	a
umořování	umořování	k1gNnSc4	umořování
státních	státní	k2eAgInPc2d1	státní
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
,	,	kIx,	,
zadávání	zadávání	k1gNnSc3	zadávání
státních	státní	k2eAgFnPc2d1	státní
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
hospodaření	hospodaření	k1gNnSc1	hospodaření
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
pořízení	pořízení	k1gNnSc4	pořízení
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
NKÚ	NKÚ	kA	NKÚ
</s>
