<s>
Železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
pův	pův	k?	pův
<g/>
.	.	kIx.	.
železná	železný	k2eAgFnSc1d1	železná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kolejový	kolejový	k2eAgInSc1d1	kolejový
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kolejové	kolejový	k2eAgFnPc1d1	kolejová
dráhy	dráha	k1gFnPc1	dráha
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
<g/>
,	,	kIx,	,
pozemní	pozemní	k2eAgFnPc1d1	pozemní
lanové	lanový	k2eAgFnPc1d1	lanová
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
nebo	nebo	k8xC	nebo
zábavní	zábavní	k2eAgFnPc1d1	zábavní
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
svým	svůj	k3xOyFgInSc7	svůj
technickým	technický	k2eAgInSc7d1	technický
principem	princip	k1gInSc7	princip
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc4d1	odlišný
stavební	stavební	k2eAgInPc4d1	stavební
a	a	k8xC	a
provozní	provozní	k2eAgInPc4d1	provozní
předpisy	předpis	k1gInPc4	předpis
a	a	k8xC	a
za	za	k7c2	za
železnice	železnice	k1gFnSc2	železnice
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
legislativě	legislativa	k1gFnSc6	legislativa
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
nepovažují	považovat	k5eNaImIp3nP	považovat
<g/>
.	.	kIx.	.
</s>
<s>
Dílčí	dílčí	k2eAgInPc1d1	dílčí
principy	princip	k1gInPc1	princip
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaImNgInP	využívat
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
železnicie	železnicie	k1gFnSc1	železnicie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
kolejová	kolejový	k2eAgNnPc1d1	kolejové
vozidla	vozidlo	k1gNnPc1	vozidlo
byla	být	k5eAaImAgNnP	být
používána	používat	k5eAaImNgNnP	používat
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
tažena	tažen	k2eAgNnPc1d1	taženo
lany	lano	k1gNnPc7	lano
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
navijáků	naviják	k1gInPc2	naviják
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
pozemní	pozemní	k2eAgFnSc2d1	pozemní
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
či	či	k8xC	či
u	u	k7c2	u
kabelové	kabelový	k2eAgFnSc2d1	kabelová
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
vpřed	vpřed	k6eAd1	vpřed
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
umístěn	umístit	k5eAaPmNgInS	umístit
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
vozidle	vozidlo	k1gNnSc6	vozidlo
přepravujícím	přepravující	k2eAgNnSc6d1	přepravující
užitečnou	užitečný	k2eAgFnSc7d1	užitečná
zátěž	zátěž	k1gFnSc4	zátěž
(	(	kIx(	(
<g/>
motorovém	motorový	k2eAgInSc6d1	motorový
voze	vůz	k1gInSc6	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vozidle	vozidlo	k1gNnSc6	vozidlo
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vyhrazeném	vyhrazený	k2eAgNnSc6d1	vyhrazené
(	(	kIx(	(
<g/>
lokomotivě	lokomotiva	k1gFnSc6	lokomotiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgInSc1d1	experimentální
vývoj	vývoj	k1gInSc1	vývoj
železnice	železnice	k1gFnSc2	železnice
směřuje	směřovat	k5eAaImIp3nS	směřovat
například	například	k6eAd1	například
k	k	k7c3	k
monorailu	monorail	k1gInSc3	monorail
na	na	k7c6	na
principu	princip	k1gInSc6	princip
magnetické	magnetický	k2eAgFnSc2d1	magnetická
levitace	levitace	k1gFnSc2	levitace
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
dráha	dráha	k1gFnSc1	dráha
označuje	označovat	k5eAaImIp3nS	označovat
zprvu	zprvu	k6eAd1	zprvu
cestu	cesta	k1gFnSc4	cesta
nebo	nebo	k8xC	nebo
trasu	trasa	k1gFnSc4	trasa
(	(	kIx(	(
<g/>
či	či	k8xC	či
linii	linie	k1gFnSc4	linie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
objekt	objekt	k1gInSc4	objekt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
jízdní	jízdní	k2eAgFnSc1d1	jízdní
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
letištní	letištní	k2eAgFnSc1d1	letištní
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
vzletová	vzletový	k2eAgFnSc1d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pojem	pojem	k1gInSc1	pojem
železnice	železnice	k1gFnSc2	železnice
tedy	tedy	k8xC	tedy
precizně	precizně	k6eAd1	precizně
označuje	označovat	k5eAaImIp3nS	označovat
druh	druh	k1gInSc4	druh
jízdní	jízdní	k2eAgFnSc2d1	jízdní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vychází	vycházet	k5eAaImIp3nS	vycházet
používání	používání	k1gNnSc1	používání
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
železnice	železnice	k1gFnSc1	železnice
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
(	(	kIx(	(
<g/>
jízdní	jízdní	k2eAgFnSc1d1	jízdní
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
na	na	k7c4	na
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
slovo	slovo	k1gNnSc4	slovo
původně	původně	k6eAd1	původně
označovalo	označovat	k5eAaImAgNnS	označovat
dráhy	dráha	k1gFnPc4	dráha
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostředek	prostředek	k1gInSc1	prostředek
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Eisenbahn	Eisenbahn	k1gNnSc1	Eisenbahn
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
chemin	chemin	k1gInSc1	chemin
de	de	k?	de
fer	fer	k?	fer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
spoorweg	spoorweg	k1gInSc1	spoorweg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g />
.	.	kIx.	.
</s>
<s>
ferrocarril	ferrocarril	k1gInSc1	ferrocarril
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
ж	ж	k?	ж
д	д	k?	д
[	[	kIx(	[
<g/>
železnaja	železnaj	k2eAgFnSc1d1	železnaj
daróga	daróga	k1gFnSc1	daróga
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
railway	railwaa	k1gMnSc2	railwaa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
kolejová	kolejový	k2eAgFnSc1d1	kolejová
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
vasút	vasút	k5eAaPmF	vasút
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
ferrovia	ferrovius	k1gMnSc2	ferrovius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
Järnväg	Järnväg	k1gInSc1	Järnväg
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
demiryolu	demiryol	k1gInSc3	demiryol
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
železná	železný	k2eAgFnSc1d1	železná
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjeté	vyjetý	k2eAgFnPc1d1	vyjetá
koleje	kolej	k1gFnPc1	kolej
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
povozy	povoz	k1gInPc4	povoz
po	po	k7c6	po
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgFnP	existovat
odedávna	odedávna	k6eAd1	odedávna
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
normální	normální	k2eAgInSc4d1	normální
rozchod	rozchod	k1gInSc4	rozchod
železničních	železniční	k2eAgFnPc2d1	železniční
kolejí	kolej	k1gFnPc2	kolej
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
standardizovaného	standardizovaný	k2eAgInSc2d1	standardizovaný
rozchodu	rozchod	k1gInSc2	rozchod
kol	kol	k7c2	kol
anglických	anglický	k2eAgInPc2d1	anglický
dostavníků	dostavník	k1gInPc2	dostavník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolech	dol	k1gInPc6	dol
doložitelně	doložitelně	k6eAd1	doložitelně
přinejmenším	přinejmenším	k6eAd1	přinejmenším
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
existovaly	existovat	k5eAaImAgInP	existovat
kolejové	kolejový	k2eAgInPc1d1	kolejový
důlní	důlní	k2eAgInPc1d1	důlní
vozíky	vozík	k1gInPc1	vozík
či	či	k8xC	či
káry	káry	k1gInPc1	káry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hornictví	hornictví	k1gNnSc6	hornictví
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
vozidla	vozidlo	k1gNnPc1	vozidlo
pohybovala	pohybovat	k5eAaImAgNnP	pohybovat
na	na	k7c6	na
kolech	kolo	k1gNnPc6	kolo
s	s	k7c7	s
okolky	okolek	k1gInPc7	okolek
na	na	k7c4	na
(	(	kIx(	(
<g/>
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
ocelových	ocelový	k2eAgFnPc6d1	ocelová
<g/>
)	)	kIx)	)
kolejnicích	kolejnice	k1gFnPc6	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
po	po	k7c6	po
kolejích	kolej	k1gFnPc6	kolej
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
koňských	koňský	k2eAgNnPc2d1	koňské
drah	draha	k1gNnPc2	draha
Wagonway	Wagonwaa	k1gFnSc2	Wagonwaa
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
hranou	hrana	k1gFnSc7	hrana
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Geometrický	geometrický	k2eAgInSc1d1	geometrický
princip	princip	k1gInSc1	princip
Wagonway	Wagonwaa	k1gFnSc2	Wagonwaa
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
překonán	překonat	k5eAaPmNgInS	překonat
<g/>
,	,	kIx,	,
u	u	k7c2	u
autobusových	autobusový	k2eAgFnPc2d1	autobusová
drah	draha	k1gFnPc2	draha
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
dále	daleko	k6eAd2	daleko
rozvíjen	rozvíjen	k2eAgInSc1d1	rozvíjen
<g/>
.	.	kIx.	.
</s>
<s>
Požadavek	požadavek	k1gInSc1	požadavek
hladké	hladký	k2eAgFnSc2d1	hladká
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
strojního	strojní	k2eAgInSc2d1	strojní
pohonu	pohon	k1gInSc2	pohon
vedly	vést	k5eAaImAgFnP	vést
zprvu	zprvu	k6eAd1	zprvu
k	k	k7c3	k
ocelí	ocel	k1gFnPc2	ocel
pobitým	pobitý	k2eAgFnPc3d1	pobitá
fošnám	fošna	k1gFnPc3	fošna
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
montáži	montáž	k1gFnSc3	montáž
ocelových	ocelový	k2eAgFnPc2d1	ocelová
kolejnic	kolejnice	k1gFnPc2	kolejnice
na	na	k7c6	na
kamenných	kamenný	k2eAgInPc6d1	kamenný
blocích	blok	k1gInPc6	blok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dodržení	dodržení	k1gNnSc2	dodržení
rozchodu	rozchod	k1gInSc2	rozchod
kolejí	kolej	k1gFnPc2	kolej
montovány	montován	k2eAgFnPc4d1	montována
na	na	k7c6	na
příčně	příčně	k6eAd1	příčně
položených	položený	k2eAgInPc6d1	položený
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
železničních	železniční	k2eAgInPc6d1	železniční
pražcích	pražec	k1gInPc6	pražec
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
železniční	železniční	k2eAgNnPc1d1	železniční
vozidla	vozidlo	k1gNnPc1	vozidlo
jezdí	jezdit	k5eAaImIp3nP	jezdit
většinou	většina	k1gFnSc7	většina
koly	kola	k1gFnSc2	kola
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
na	na	k7c6	na
ocelových	ocelový	k2eAgFnPc6d1	ocelová
kolejnicích	kolejnice	k1gFnPc6	kolejnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
jízdní	jízdní	k2eAgFnSc6d1	jízdní
dráze	dráha	k1gFnSc6	dráha
jsou	být	k5eAaImIp3nP	být
drženy	držet	k5eAaImNgFnP	držet
díky	díky	k7c3	díky
speciálnímu	speciální	k2eAgInSc3d1	speciální
profilu	profil	k1gInSc3	profil
kol	kola	k1gFnPc2	kola
a	a	k8xC	a
okolku	okolek	k1gInSc2	okolek
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
strojního	strojní	k2eAgInSc2d1	strojní
pohonu	pohon	k1gInSc2	pohon
v	v	k7c6	v
kolejové	kolejový	k2eAgFnSc6d1	kolejová
dopravě	doprava	k1gFnSc6	doprava
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1804	[number]	k4	1804
<g/>
,	,	kIx,	,
když	když	k8xS	když
Richard	Richard	k1gMnSc1	Richard
Trevithick	Trevithick	k1gMnSc1	Trevithick
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
první	první	k4xOgFnSc4	první
parní	parní	k2eAgFnSc4d1	parní
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
tehdy	tehdy	k6eAd1	tehdy
měla	mít	k5eAaImAgFnS	mít
ještě	ještě	k6eAd1	ještě
kola	kolo	k1gNnPc4	kolo
bez	bez	k7c2	bez
okolků	okolek	k1gInPc2	okolek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
veřejná	veřejný	k2eAgFnSc1d1	veřejná
železnice	železnice	k1gFnSc1	železnice
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
z	z	k7c2	z
Stocktonu	Stockton	k1gInSc2	Stockton
do	do	k7c2	do
Darlingtonu	Darlington	k1gInSc2	Darlington
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
mimo	mimo	k7c4	mimo
zboží	zboží	k1gNnSc4	zboží
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
přepravovala	přepravovat	k5eAaImAgFnS	přepravovat
i	i	k9	i
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Kola	Kola	k1gFnSc1	Kola
železničních	železniční	k2eAgInPc2d1	železniční
vozů	vůz	k1gInPc2	vůz
již	již	k6eAd1	již
měla	mít	k5eAaImAgFnS	mít
stejné	stejný	k2eAgInPc4d1	stejný
okolky	okolek	k1gInPc4	okolek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mají	mít	k5eAaImIp3nP	mít
dnešní	dnešní	k2eAgInPc1d1	dnešní
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
dnešní	dnešní	k2eAgInSc4d1	dnešní
normální	normální	k2eAgInSc4d1	normální
rozchod	rozchod	k1gInSc4	rozchod
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
během	během	k7c2	během
několika	několik	k4yIc2	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
hustou	hustý	k2eAgFnSc4d1	hustá
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
významně	významně	k6eAd1	významně
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
dobu	doba	k1gFnSc4	doba
cestování	cestování	k1gNnSc2	cestování
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
potřebu	potřeba	k1gFnSc4	potřeba
vzniku	vznik	k1gInSc2	vznik
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
rozvoji	rozvoj	k1gInSc3	rozvoj
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
předpoklad	předpoklad	k1gInSc4	předpoklad
velké	velký	k2eAgFnSc2d1	velká
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
železe	železo	k1gNnSc6	železo
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc6	ocel
a	a	k8xC	a
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Modernizovala	modernizovat	k5eAaBmAgFnS	modernizovat
se	se	k3xPyFc4	se
technologie	technologie	k1gFnSc1	technologie
stavby	stavba	k1gFnSc2	stavba
mostů	most	k1gInPc2	most
a	a	k8xC	a
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
stavět	stavět	k5eAaImF	stavět
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
potřebu	potřeba	k1gFnSc4	potřeba
kapitálu	kapitál	k1gInSc2	kapitál
pro	pro	k7c4	pro
železniční	železniční	k2eAgInPc4d1	železniční
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
žádný	žádný	k3yNgMnSc1	žádný
soukromý	soukromý	k2eAgMnSc1d1	soukromý
investor	investor	k1gMnSc1	investor
sám	sám	k3xTgMnSc1	sám
financovat	financovat	k5eAaBmF	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
obrovská	obrovský	k2eAgFnSc1d1	obrovská
strategická	strategický	k2eAgFnSc1d1	strategická
výhoda	výhoda	k1gFnSc1	výhoda
dobře	dobře	k6eAd1	dobře
vybudované	vybudovaný	k2eAgFnSc2d1	vybudovaná
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
doprava	doprava	k6eAd1	doprava
vojenských	vojenský	k2eAgInPc2d1	vojenský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
zásobování	zásobování	k1gNnSc1	zásobování
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
získání	získání	k1gNnSc4	získání
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
vlivu	vliv	k1gInSc2	vliv
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vlády	vláda	k1gFnPc1	vláda
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pozorností	pozornost	k1gFnSc7	pozornost
a	a	k8xC	a
rychlostí	rychlost	k1gFnSc7	rychlost
podporovaly	podporovat	k5eAaImAgFnP	podporovat
a	a	k8xC	a
řídily	řídit	k5eAaImAgFnP	řídit
výstavbu	výstavba	k1gFnSc4	výstavba
národních	národní	k2eAgFnPc2d1	národní
železničních	železniční	k2eAgFnPc2d1	železniční
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
význam	význam	k1gInSc1	význam
železnice	železnice	k1gFnSc2	železnice
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
úlohu	úloha	k1gFnSc4	úloha
hrála	hrát	k5eAaImAgFnS	hrát
železnice	železnice	k1gFnSc1	železnice
i	i	k9	i
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mj.	mj.	kA	mj.
umožnila	umožnit	k5eAaPmAgFnS	umožnit
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
záchranu	záchrana	k1gFnSc4	záchrana
sovětského	sovětský	k2eAgInSc2d1	sovětský
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
pro	pro	k7c4	pro
válečnou	válečný	k2eAgFnSc4d1	válečná
výrobu	výroba	k1gFnSc4	výroba
přesunem	přesun	k1gInSc7	přesun
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
strojního	strojní	k2eAgNnSc2d1	strojní
vybavení	vybavení	k1gNnSc2	vybavení
továren	továrna	k1gFnPc2	továrna
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nechvalně	chvalně	k6eNd1	chvalně
proslulé	proslulý	k2eAgFnPc1d1	proslulá
deportace	deportace	k1gFnPc1	deportace
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
do	do	k7c2	do
nacistických	nacistický	k2eAgInPc2d1	nacistický
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
a	a	k8xC	a
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
začal	začít	k5eAaPmAgInS	začít
masívní	masívní	k2eAgInSc1d1	masívní
rozvoj	rozvoj	k1gInSc1	rozvoj
silničního	silniční	k2eAgInSc2d1	silniční
motorismu	motorismus	k1gInSc2	motorismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
za	za	k7c4	za
následek	následek	k1gInSc4	následek
pozastavení	pozastavení	k1gNnSc2	pozastavení
rozovje	rozovj	k1gFnSc2	rozovj
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
mnohde	mnohde	k6eAd1	mnohde
i	i	k9	i
její	její	k3xOp3gNnSc4	její
omezení	omezení	k1gNnSc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
výkonnost	výkonnost	k1gFnSc1	výkonnost
železnice	železnice	k1gFnSc1	železnice
sice	sice	k8xC	sice
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
v	v	k7c6	v
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
jako	jako	k8xS	jako
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1	nákladní
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
si	se	k3xPyFc3	se
dodnes	dodnes	k6eAd1	dodnes
udržela	udržet	k5eAaPmAgFnS	udržet
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
si	se	k3xPyFc3	se
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
dokázala	dokázat	k5eAaPmAgFnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
díky	díky	k7c3	díky
výstavbě	výstavba	k1gFnSc3	výstavba
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1	vysokorychlostní
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
CIA	CIA	kA	CIA
má	mít	k5eAaImIp3nS	mít
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
délku	délka	k1gFnSc4	délka
celkem	celkem	k6eAd1	celkem
1,37	[number]	k4	1,37
miliónů	milión	k4xCgInPc2	milión
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
275	[number]	k4	275
000	[number]	k4	000
km	km	kA	km
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
EU	EU	kA	EU
mají	mít	k5eAaImIp3nP	mít
236	[number]	k4	236
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
87	[number]	k4	87
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
přes	přes	k7c4	přes
75	[number]	k4	75
000	[number]	k4	000
km	km	kA	km
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
63	[number]	k4	63
000	[number]	k4	000
km	km	kA	km
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
mají	mít	k5eAaImIp3nP	mít
výše	vysoce	k6eAd2	vysoce
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
země	zem	k1gFnPc1	zem
polovinu	polovina	k1gFnSc4	polovina
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
s	s	k7c7	s
nejrozsáhlejšími	rozsáhlý	k2eAgFnPc7d3	nejrozsáhlejší
železničními	železniční	k2eAgFnPc7d1	železniční
sítěmi	síť	k1gFnPc7	síť
má	mít	k5eAaImIp3nS	mít
Austrálie	Austrálie	k1gFnSc1	Austrálie
38	[number]	k4	38
550	[number]	k4	550
km	km	kA	km
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
32	[number]	k4	32
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
21	[number]	k4	21
000	[number]	k4	000
km	km	kA	km
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
18	[number]	k4	18
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
9	[number]	k4	9
632	[number]	k4	632
km	km	kA	km
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgInPc1d2	nižší
externí	externí	k2eAgInPc1d1	externí
náklady	náklad	k1gInPc1	náklad
než	než	k8xS	než
silniční	silniční	k2eAgFnSc1d1	silniční
nebo	nebo	k8xC	nebo
letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
jsou	být	k5eAaImIp3nP	být
přepočtu	přepočet	k1gInSc3	přepočet
na	na	k7c4	na
osobokilometr	osobokilometr	k1gInSc4	osobokilometr
asi	asi	k9	asi
čtvrtinové	čtvrtinový	k2eAgNnSc1d1	čtvrtinové
<g/>
,	,	kIx,	,
u	u	k7c2	u
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
šestinové	šestinový	k2eAgFnPc1d1	šestinová
<g/>
.	.	kIx.	.
</s>
<s>
Externí	externí	k2eAgInPc1d1	externí
náklady	náklad	k1gInPc1	náklad
vznikají	vznikat	k5eAaImIp3nP	vznikat
zejména	zejména	k9	zejména
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
znečištěním	znečištění	k1gNnSc7	znečištění
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
hlukem	hluk	k1gInSc7	hluk
a	a	k8xC	a
nehodami	nehoda	k1gFnPc7	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
pochází	pocházet	k5eAaImIp3nS	pocházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
externích	externí	k2eAgInPc2d1	externí
nákladů	náklad	k1gInPc2	náklad
z	z	k7c2	z
nehod	nehoda	k1gFnPc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kapacitu	kapacita	k1gFnSc4	kapacita
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
(	(	kIx(	(
<g/>
čtyřkolejná	čtyřkolejný	k2eAgFnSc1d1	čtyřkolejná
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
dokáže	dokázat	k5eAaPmIp3nS	dokázat
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
přepravit	přepravit	k5eAaPmF	přepravit
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
<g/>
)	)	kIx)	)
a	a	k8xC	a
též	též	k9	též
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
železnice	železnice	k1gFnSc1	železnice
je	být	k5eAaImIp3nS	být
centrálně	centrálně	k6eAd1	centrálně
řízena	řízen	k2eAgFnSc1d1	řízena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgInSc1d1	relativní
zábor	zábor	k1gInSc1	zábor
místa	místo	k1gNnSc2	místo
železnicí	železnice	k1gFnSc7	železnice
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
relativně	relativně	k6eAd1	relativně
úzké	úzký	k2eAgFnSc2d1	úzká
<g/>
,	,	kIx,	,
trasy	trasa	k1gFnPc4	trasa
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
kolejemi	kolej	k1gFnPc7	kolej
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
železniční	železniční	k2eAgFnPc4d1	železniční
křižovatky	křižovatka	k1gFnPc4	křižovatka
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
četné	četný	k2eAgFnPc1d1	četná
něž	jenž	k3xRgFnPc4	jenž
čtyřproudové	čtyřproudový	k2eAgFnPc4d1	čtyřproudová
silnice	silnice	k1gFnPc4	silnice
a	a	k8xC	a
dálniční	dálniční	k2eAgInPc4d1	dálniční
sjezdy	sjezd	k1gInPc4	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
rozdělení	rozdělení	k1gNnSc1	rozdělení
krajiny	krajina	k1gFnSc2	krajina
i	i	k8xC	i
sídelních	sídelní	k2eAgInPc2d1	sídelní
celků	celek	k1gInPc2	celek
železničními	železniční	k2eAgFnPc7d1	železniční
tratěmi	trať	k1gFnPc7	trať
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
především	především	k9	především
novostavby	novostavba	k1gFnSc2	novostavba
pro	pro	k7c4	pro
vysokorychlostní	vysokorychlostní	k2eAgFnSc4d1	vysokorychlostní
železnici	železnice	k1gFnSc4	železnice
<g/>
,	,	kIx,	,
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
více	hodně	k6eAd2	hodně
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
přemostění	přemostění	k1gNnSc4	přemostění
údolí	údolí	k1gNnSc2	údolí
a	a	k8xC	a
pro	pro	k7c4	pro
zářezy	zářez	k1gInPc4	zářez
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
i	i	k9	i
více	hodně	k6eAd2	hodně
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
trať	trať	k1gFnSc1	trať
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
i	i	k8xC	i
novostavby	novostavba	k1gFnPc4	novostavba
elektrického	elektrický	k2eAgNnSc2d1	elektrické
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
i	i	k9	i
hlučnost	hlučnost	k1gFnSc1	hlučnost
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
trasy	trasa	k1gFnPc1	trasa
a	a	k8xC	a
křižovatky	křižovatka	k1gFnPc1	křižovatka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
obydlených	obydlený	k2eAgFnPc6d1	obydlená
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hluk	hluk	k1gInSc1	hluk
projíždějících	projíždějící	k2eAgInPc2d1	projíždějící
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
nočních	noční	k2eAgNnPc2d1	noční
nákladních	nákladní	k2eAgNnPc2d1	nákladní
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velkou	velký	k2eAgFnSc7d1	velká
zátěží	zátěž	k1gFnSc7	zátěž
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
hlučnosti	hlučnost	k1gFnSc2	hlučnost
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
věnuje	věnovat	k5eAaImIp3nS	věnovat
značná	značný	k2eAgFnSc1d1	značná
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnPc1	opatření
proti	proti	k7c3	proti
vznikání	vznikání	k1gNnSc3	vznikání
hluku	hluk	k1gInSc2	hluk
se	se	k3xPyFc4	se
provádějí	provádět	k5eAaImIp3nP	provádět
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
(	(	kIx(	(
<g/>
bezstyková	bezstykový	k2eAgFnSc1d1	bezstyková
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
broušení	broušení	k1gNnSc1	broušení
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
,	,	kIx,	,
tlumící	tlumící	k2eAgNnSc1d1	tlumící
upevnění	upevnění	k1gNnSc1	upevnění
kolejnic	kolejnice	k1gFnPc2	kolejnice
na	na	k7c6	na
pražcích	pražec	k1gInPc6	pražec
<g/>
,	,	kIx,	,
obložení	obložení	k1gNnSc1	obložení
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
tlumení	tlumení	k1gNnPc2	tlumení
agregátů	agregát	k1gInPc2	agregát
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
kotoučové	kotoučový	k2eAgFnPc1d1	kotoučová
brzdy	brzda	k1gFnPc1	brzda
nebo	nebo	k8xC	nebo
nekovové	kovový	k2eNgInPc1d1	nekovový
špalíky	špalík	k1gInPc1	špalík
<g/>
,	,	kIx,	,
tlumiče	tlumič	k1gInPc1	tlumič
hluku	hluk	k1gInSc2	hluk
nebo	nebo	k8xC	nebo
nástřiky	nástřik	k1gInPc4	nástřik
na	na	k7c6	na
kolech	kolo	k1gNnPc6	kolo
<g/>
)	)	kIx)	)
a	a	k8xC	a
odstíněním	odstínění	k1gNnSc7	odstínění
zdrojů	zdroj	k1gInPc2	zdroj
hluku	hluk	k1gInSc2	hluk
(	(	kIx(	(
<g/>
protihlukové	protihlukový	k2eAgFnPc1d1	protihluková
stěny	stěna	k1gFnPc1	stěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
hlučnosti	hlučnost	k1gFnSc3	hlučnost
řeší	řešit	k5eAaImIp3nP	řešit
stavbami	stavba	k1gFnPc7	stavba
protihlukových	protihlukový	k2eAgFnPc2d1	protihluková
zdí	zeď	k1gFnPc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stavby	stavba	k1gFnPc1	stavba
však	však	k9	však
necitlivě	citlivě	k6eNd1	citlivě
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
snižují	snižovat	k5eAaImIp3nP	snižovat
atraktivitu	atraktivita	k1gFnSc4	atraktivita
osobní	osobní	k2eAgFnSc2d1	osobní
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
zamezení	zamezení	k1gNnSc1	zamezení
výhledu	výhled	k1gInSc2	výhled
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Železniční	železniční	k2eAgFnSc1d1	železniční
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kolejovou	kolejový	k2eAgFnSc4d1	kolejová
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
technická	technický	k2eAgNnPc4d1	technické
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
zabezpečovací	zabezpečovací	k2eAgNnPc1d1	zabezpečovací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
k	k	k7c3	k
provozu	provoz	k1gInSc3	provoz
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
provozní	provozní	k2eAgInSc1d1	provozní
celek	celek	k1gInSc1	celek
železniční	železniční	k2eAgFnSc2d1	železniční
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
železniční	železniční	k2eAgFnSc1d1	železniční
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
zhruba	zhruba	k6eAd1	zhruba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nyní	nyní	k6eAd1	nyní
neoficiální	oficiální	k2eNgInSc1d1	neoficiální
pojem	pojem	k1gInSc1	pojem
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
drahách	draha	k1gFnPc6	draha
obecně	obecně	k6eAd1	obecně
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
typy	typ	k1gInPc4	typ
objektů	objekt	k1gInPc2	objekt
jsou	být	k5eAaImIp3nP	být
součástmi	součást	k1gFnPc7	součást
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
ani	ani	k9	ani
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vymezení	vymezení	k1gNnSc1	vymezení
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
souvisejícím	související	k2eAgFnPc3d1	související
stavbám	stavba	k1gFnPc3	stavba
a	a	k8xC	a
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
styku	styk	k1gInSc3	styk
drah	draha	k1gFnPc2	draha
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
infrastrukturními	infrastrukturní	k2eAgInPc7d1	infrastrukturní
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
přejezdy	přejezd	k1gInPc4	přejezd
<g/>
,	,	kIx,	,
propustky	propustka	k1gFnPc4	propustka
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc4	vedení
dráhy	dráha	k1gFnSc2	dráha
po	po	k7c6	po
pozemní	pozemní	k2eAgFnSc6d1	pozemní
komunikaci	komunikace	k1gFnSc6	komunikace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
z	z	k7c2	z
legislativního	legislativní	k2eAgNnSc2d1	legislativní
hlediska	hledisko	k1gNnSc2	hledisko
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
regionálních	regionální	k2eAgFnPc2d1	regionální
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
historické	historický	k2eAgFnPc4d1	historická
místní	místní	k2eAgFnPc4d1	místní
dráhy	dráha	k1gFnPc4	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
regionální	regionální	k2eAgFnSc1d1	regionální
dráha	dráha	k1gFnSc1	dráha
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zákona	zákon	k1gInSc2	zákon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
privatizována	privatizovat	k5eAaImNgFnS	privatizovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
převedena	převést	k5eAaPmNgFnS	převést
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
veřejnoprávní	veřejnoprávní	k2eAgInSc4d1	veřejnoprávní
subjekt	subjekt	k1gInSc4	subjekt
než	než	k8xS	než
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Páteřní	páteřní	k2eAgFnSc1d1	páteřní
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zákona	zákon	k1gInSc2	zákon
jednu	jeden	k4xCgFnSc4	jeden
celostátní	celostátní	k2eAgFnSc4d1	celostátní
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
tratě	trať	k1gFnPc4	trať
sítě	síť	k1gFnSc2	síť
nejsou	být	k5eNaImIp3nP	být
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
dráhami	dráha	k1gFnPc7	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úseky	úsek	k1gInPc1	úsek
celostátní	celostátní	k2eAgFnSc2d1	celostátní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Odbočné	odbočný	k2eAgFnPc1d1	odbočná
dráhy	dráha	k1gFnPc1	dráha
privátního	privátní	k2eAgInSc2d1	privátní
charakteru	charakter	k1gInSc2	charakter
mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
vleček	vlečka	k1gFnPc2	vlečka
<g/>
,	,	kIx,	,
vlečkou	vlečka	k1gFnSc7	vlečka
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
i	i	k9	i
železniční	železniční	k2eAgInSc4d1	železniční
zkušební	zkušební	k2eAgInSc4d1	zkušební
okruh	okruh	k1gInSc4	okruh
Cerhenice	Cerhenice	k1gFnSc2	Cerhenice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
právního	právní	k2eAgNnSc2d1	právní
vymezení	vymezení	k1gNnSc2	vymezení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
drah	draha	k1gFnPc2	draha
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
vymezení	vymezení	k1gNnSc2	vymezení
staničením	staničení	k1gNnPc3	staničení
(	(	kIx(	(
<g/>
kilometráží	kilometráž	k1gFnPc2	kilometráž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
vlastnickou	vlastnický	k2eAgFnSc7d1	vlastnická
a	a	k8xC	a
stavební	stavební	k2eAgFnSc7d1	stavební
historií	historie	k1gFnSc7	historie
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
pochopitelně	pochopitelně	k6eAd1	pochopitelně
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
prováděno	provádět	k5eAaImNgNnS	provádět
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
odbočku	odbočka	k1gFnSc4	odbočka
či	či	k8xC	či
větev	větev	k1gFnSc4	větev
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
vymezení	vymezení	k1gNnSc2	vymezení
tratí	trať	k1gFnPc2	trať
je	být	k5eAaImIp3nS	být
číslování	číslování	k1gNnSc1	číslování
traťových	traťový	k2eAgInPc2d1	traťový
oddílů	oddíl	k1gInPc2	oddíl
v	v	k7c6	v
jízdních	jízdní	k2eAgInPc6d1	jízdní
řádech	řád	k1gInPc6	řád
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
navzájem	navzájem	k6eAd1	navzájem
v	v	k7c6	v
souběhu	souběh	k1gInSc6	souběh
a	a	k8xC	a
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
oddílů	oddíl	k1gInPc2	oddíl
účelově	účelově	k6eAd1	účelově
pospojovány	pospojovat	k5eAaPmNgInP	pospojovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
vlakových	vlakový	k2eAgFnPc2d1	vlaková
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
spojů	spoj	k1gInPc2	spoj
a	a	k8xC	a
číslování	číslování	k1gNnSc2	číslování
a	a	k8xC	a
vymezení	vymezení	k1gNnSc2	vymezení
úseků	úsek	k1gInPc2	úsek
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
toto	tento	k3xDgNnSc1	tento
číslování	číslování	k1gNnSc1	číslování
tratí	trať	k1gFnPc2	trať
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejčastěji	často	k6eAd3	často
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
(	(	kIx(	(
<g/>
mapy	mapa	k1gFnPc4	mapa
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
turistické	turistický	k2eAgFnSc2d1	turistická
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
mapa	mapa	k1gFnSc1	mapa
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
záchranného	záchranný	k2eAgInSc2d1	záchranný
systému	systém	k1gInSc2	systém
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
tratě	trať	k1gFnPc1	trať
a	a	k8xC	a
úseky	úsek	k1gInPc1	úsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
aktuálně	aktuálně	k6eAd1	aktuálně
bez	bez	k7c2	bez
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
číslování	číslování	k1gNnSc2	číslování
zahrnuty	zahrnut	k2eAgInPc1d1	zahrnut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stavebně-provozní	stavebněrovozní	k2eAgNnSc1d1	stavebně-provozní
číslování	číslování	k1gNnSc1	číslování
tratí	trať	k1gFnPc2	trať
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
číslování	číslování	k1gNnSc2	číslování
tratí	trať	k1gFnPc2	trať
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mezi	mezi	k7c4	mezi
železniční	železniční	k2eAgFnPc4d1	železniční
dráhy	dráha	k1gFnPc4	dráha
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
železničních	železniční	k2eAgFnPc2d1	železniční
drah	draha	k1gFnPc2	draha
speciálních	speciální	k2eAgFnPc2d1	speciální
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
tvořilo	tvořit	k5eAaImAgNnS	tvořit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
drah	draha	k1gFnPc2	draha
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
kategorii	kategorie	k1gFnSc4	kategorie
městských	městský	k2eAgFnPc2d1	městská
rychlodrah	rychlodráha	k1gFnPc2	rychlodráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
důlní	důlní	k2eAgFnPc1d1	důlní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
polní	polní	k2eAgFnPc1d1	polní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc1d1	lesní
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
jeřábové	jeřábový	k2eAgInPc4d1	jeřábový
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
pojezdy	pojezd	k1gInPc4	pojezd
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vůbec	vůbec	k9	vůbec
do	do	k7c2	do
působnosti	působnost	k1gFnSc2	působnost
drážního	drážní	k2eAgInSc2d1	drážní
zákona	zákon	k1gInSc2	zákon
nespadají	spadat	k5eNaImIp3nP	spadat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
normální	normální	k2eAgInSc4d1	normální
železniční	železniční	k2eAgInSc4d1	železniční
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
důlních	důlní	k2eAgFnPc6d1	důlní
drahách	draha	k1gFnPc6	draha
v	v	k7c6	v
povrchových	povrchový	k2eAgInPc6d1	povrchový
dolech	dol	k1gInPc6	dol
Severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
formálně	formálně	k6eAd1	formálně
vlečkami	vlečka	k1gFnPc7	vlečka
<g/>
.	.	kIx.	.
</s>
<s>
Lanové	lanový	k2eAgFnPc1d1	lanová
<g/>
,	,	kIx,	,
tramvajové	tramvajový	k2eAgFnPc1d1	tramvajová
<g/>
,	,	kIx,	,
trolejbusové	trolejbusový	k2eAgFnPc1d1	trolejbusová
a	a	k8xC	a
důlní	důlní	k2eAgFnPc1d1	důlní
dráhy	dráha	k1gFnPc1	dráha
nejsou	být	k5eNaImIp3nP	být
podle	podle	k7c2	podle
české	český	k2eAgFnSc2d1	Česká
legislativy	legislativa	k1gFnSc2	legislativa
železničními	železniční	k2eAgFnPc7d1	železniční
drahami	draha	k1gFnPc7	draha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ani	ani	k8xC	ani
když	když	k8xS	když
je	on	k3xPp3gFnPc4	on
provozuje	provozovat	k5eAaImIp3nS	provozovat
železniční	železniční	k2eAgFnSc1d1	železniční
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
pozemní	pozemní	k2eAgFnPc4d1	pozemní
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
či	či	k8xC	či
tramvajové	tramvajový	k2eAgFnPc4d1	tramvajová
dráhy	dráha	k1gFnSc2	dráha
železniční	železniční	k2eAgFnSc1d1	železniční
legislativa	legislativa	k1gFnSc1	legislativa
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Železniční	železniční	k2eAgInSc4d1	železniční
svršek	svršek	k1gInSc4	svršek
a	a	k8xC	a
Železniční	železniční	k2eAgInSc4d1	železniční
spodek	spodek	k1gInSc4	spodek
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
koleje	kolej	k1gFnPc1	kolej
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
,	,	kIx,	,
podélných	podélný	k2eAgInPc2d1	podélný
ocelových	ocelový	k2eAgInPc2d1	ocelový
profilů	profil	k1gInPc2	profil
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
o	o	k7c4	o
stanovený	stanovený	k2eAgInSc4d1	stanovený
rozchod	rozchod	k1gInSc4	rozchod
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
upevněny	upevnit	k5eAaPmNgFnP	upevnit
na	na	k7c6	na
pražcích	pražec	k1gInPc6	pražec
nebo	nebo	k8xC	nebo
na	na	k7c6	na
podkladovém	podkladový	k2eAgInSc6d1	podkladový
panelu	panel	k1gInSc6	panel
či	či	k8xC	či
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Kolejnice	kolejnice	k1gFnPc1	kolejnice
se	se	k3xPyFc4	se
upevňují	upevňovat	k5eAaImIp3nP	upevňovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hřeby	hřeb	k1gInPc7	hřeb
nebo	nebo	k8xC	nebo
svorami	svora	k1gFnPc7	svora
<g/>
.	.	kIx.	.
</s>
<s>
Upevněním	upevnění	k1gNnSc7	upevnění
kolejí	kolej	k1gFnPc2	kolej
se	se	k3xPyFc4	se
jistí	jistit	k5eAaImIp3nS	jistit
rozchod	rozchod	k1gInSc1	rozchod
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
rozjíždění	rozjíždění	k1gNnSc1	rozjíždění
kolejnic	kolejnice	k1gFnPc2	kolejnice
v	v	k7c6	v
příčném	příčný	k2eAgInSc6d1	příčný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Pražce	pražec	k1gInPc1	pražec
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
impregnovaného	impregnovaný	k2eAgNnSc2d1	impregnované
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
či	či	k8xC	či
nověji	nově	k6eAd2	nově
z	z	k7c2	z
předpjatého	předpjatý	k2eAgInSc2d1	předpjatý
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Kolejový	kolejový	k2eAgInSc1d1	kolejový
rošt	rošt	k1gInSc1	rošt
z	z	k7c2	z
kolejnic	kolejnice	k1gFnPc2	kolejnice
a	a	k8xC	a
pražců	pražec	k1gInPc2	pražec
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
na	na	k7c6	na
kolejovém	kolejový	k2eAgNnSc6d1	kolejové
loži	lože	k1gNnSc6	lože
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
ze	z	k7c2	z
štěrku	štěrk	k1gInSc2	štěrk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
statické	statický	k2eAgFnSc3d1	statická
a	a	k8xC	a
dynamické	dynamický	k2eAgFnSc2d1	dynamická
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
předává	předávat	k5eAaImIp3nS	předávat
dál	daleko	k6eAd2	daleko
železničnímu	železniční	k2eAgInSc3d1	železniční
spodku	spodek	k1gInSc3	spodek
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgInSc1d1	železniční
svršek	svršek	k1gInSc1	svršek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kolejnic	kolejnice	k1gFnPc2	kolejnice
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
lože	lože	k1gNnSc2	lože
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
u	u	k7c2	u
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1	vysokorychlostní
železnic	železnice	k1gFnPc2	železnice
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
kolejový	kolejový	k2eAgInSc1d1	kolejový
svršek	svršek	k1gInSc1	svršek
z	z	k7c2	z
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
jsou	být	k5eAaImIp3nP	být
montovány	montován	k2eAgFnPc4d1	montována
kolejnice	kolejnice	k1gFnPc4	kolejnice
s	s	k7c7	s
tlumícími	tlumící	k2eAgInPc7d1	tlumící
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pevná	pevný	k2eAgFnSc1d1	pevná
jízdní	jízdní	k2eAgFnSc1d1	jízdní
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
velká	velký	k2eAgNnPc4d1	velké
stoupání	stoupání	k1gNnPc4	stoupání
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
velké	velký	k2eAgInPc4d1	velký
poloměry	poloměr	k1gInPc4	poloměr
oblouků	oblouk	k1gInPc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
v	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
nutné	nutný	k2eAgFnSc2d1	nutná
složité	složitý	k2eAgFnSc2d1	složitá
konstrukce	konstrukce	k1gFnSc2	konstrukce
umělých	umělý	k2eAgFnPc2d1	umělá
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
železničních	železniční	k2eAgFnPc2d1	železniční
tras	trasa	k1gFnPc2	trasa
přes	přes	k7c4	přes
pohoří	pohoří	k1gNnSc4	pohoří
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
svými	svůj	k3xOyFgInPc7	svůj
složitými	složitý	k2eAgInPc7d1	složitý
mosty	most	k1gInPc7	most
a	a	k8xC	a
tunely	tunel	k1gInPc7	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Semmeringbahn	Semmeringbahn	k1gInSc1	Semmeringbahn
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
či	či	k8xC	či
Albulabahn	Albulabahna	k1gFnPc2	Albulabahna
a	a	k8xC	a
Berninabahn	Berninabahna	k1gFnPc2	Berninabahna
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
železniční	železniční	k2eAgFnPc1d1	železniční
trasy	trasa	k1gFnPc1	trasa
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
tahy	tah	k1gInPc1	tah
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
trasy	trasa	k1gFnPc1	trasa
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
provozu	provoz	k1gInSc2	provoz
se	se	k3xPyFc4	se
budují	budovat	k5eAaImIp3nP	budovat
většinou	většina	k1gFnSc7	většina
dvoukolejné	dvoukolejný	k2eAgInPc1d1	dvoukolejný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vícekolejových	vícekolejový	k2eAgFnPc6d1	vícekolejový
trasách	trasa	k1gFnPc6	trasa
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vlaky	vlak	k1gInPc1	vlak
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
křižovat	křižovat	k5eAaImF	křižovat
a	a	k8xC	a
předjíždět	předjíždět	k5eAaImF	předjíždět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jednokolejných	jednokolejný	k2eAgFnPc6d1	jednokolejná
trasách	trasa	k1gFnPc6	trasa
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
na	na	k7c6	na
nádražích	nádraží	k1gNnPc6	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1	elektrický
pohonné	pohonný	k2eAgInPc1d1	pohonný
vozy	vůz	k1gInPc1	vůz
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
dodávku	dodávka	k1gFnSc4	dodávka
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
za	za	k7c2	za
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
dodáván	dodávat	k5eAaImNgInS	dodávat
z	z	k7c2	z
trolejového	trolejový	k2eAgNnSc2d1	trolejové
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
nad	nad	k7c7	nad
kolejemi	kolej	k1gFnPc7	kolej
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
–	–	k?	–
hlavně	hlavně	k6eAd1	hlavně
u	u	k7c2	u
podzemních	podzemní	k2eAgFnPc2d1	podzemní
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
metra	metro	k1gNnSc2	metro
–	–	k?	–
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
napájecí	napájecí	k2eAgFnSc1d1	napájecí
kolejnice	kolejnice	k1gFnSc1	kolejnice
bočně	bočně	k6eAd1	bočně
od	od	k7c2	od
kolejnic	kolejnice	k1gFnPc2	kolejnice
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
umístěná	umístěný	k2eAgFnSc1d1	umístěná
mezi	mezi	k7c7	mezi
kolejnicemi	kolejnice	k1gFnPc7	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
systému	systém	k1gInSc3	systém
zásobování	zásobování	k1gNnSc2	zásobování
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
trakční	trakční	k2eAgFnSc1d1	trakční
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yRgFnPc4	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
napájení	napájení	k1gNnSc1	napájení
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
železniční	železniční	k2eAgFnPc1d1	železniční
společnosti	společnost	k1gFnPc1	společnost
provozovaly	provozovat	k5eAaImAgFnP	provozovat
<g/>
/	/	kIx~	/
<g/>
provozují	provozovat	k5eAaImIp3nP	provozovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
elektrárny	elektrárna	k1gFnPc4	elektrárna
a	a	k8xC	a
přenosová	přenosový	k2eAgNnPc4d1	přenosové
vedení	vedení	k1gNnPc4	vedení
pro	pro	k7c4	pro
přenosovou	přenosový	k2eAgFnSc4d1	přenosová
napájecí	napájecí	k2eAgFnSc4d1	napájecí
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
hlavové	hlavový	k2eAgFnPc1d1	hlavová
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
(	(	kIx(	(
<g/>
koncové	koncový	k2eAgFnPc1d1	koncová
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
končí	končit	k5eAaImIp3nP	končit
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
úvrať	úvrať	k1gFnSc4	úvrať
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mezilehlé	mezilehlý	k2eAgFnSc2d1	mezilehlá
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
tratě	trať	k1gFnPc1	trať
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
funkce	funkce	k1gFnSc2	funkce
existují	existovat	k5eAaImIp3nP	existovat
železniční	železniční	k2eAgFnPc1d1	železniční
stanice	stanice	k1gFnPc1	stanice
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
nastupování	nastupování	k1gNnSc3	nastupování
<g/>
,	,	kIx,	,
vystupování	vystupování	k1gNnSc3	vystupování
a	a	k8xC	a
k	k	k7c3	k
přestupování	přestupování	k1gNnSc3	přestupování
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
a	a	k8xC	a
nákladní	nákladní	k2eAgFnSc1d1	nákladní
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nakládá	nakládat	k5eAaImIp3nS	nakládat
<g/>
,	,	kIx,	,
vykládá	vykládat	k5eAaImIp3nS	vykládat
a	a	k8xC	a
překládá	překládat	k5eAaImIp3nS	překládat
zboží	zboží	k1gNnSc1	zboží
<g/>
,	,	kIx,	,
seřaďovací	seřaďovací	k2eAgNnPc4d1	seřaďovací
nádraží	nádraží	k1gNnPc4	nádraží
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vozů	vůz	k1gInPc2	vůz
nebo	nebo	k8xC	nebo
skupin	skupina	k1gFnPc2	skupina
vozů	vůz	k1gInPc2	vůz
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
nákladními	nákladní	k2eAgInPc7d1	nákladní
vlaky	vlak	k1gInPc7	vlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
provozní	provozní	k2eAgNnPc1d1	provozní
nádraží	nádraží	k1gNnPc1	nádraží
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
odstavení	odstavení	k1gNnSc3	odstavení
a	a	k8xC	a
údržbě	údržba	k1gFnSc3	údržba
kolejových	kolejový	k2eAgNnPc2d1	kolejové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
mluvě	mluva	k1gFnSc6	mluva
i	i	k8xC	i
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
názvech	název	k1gInPc6	název
se	se	k3xPyFc4	se
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
označují	označovat	k5eAaImIp3nP	označovat
slovem	slovo	k1gNnSc7	slovo
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
též	též	k9	též
dopravny	dopravna	k1gFnPc1	dopravna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
železničními	železniční	k2eAgFnPc7d1	železniční
stanicemi	stanice	k1gFnPc7	stanice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nákladiště	nákladiště	k1gNnSc1	nákladiště
<g/>
,	,	kIx,	,
výhybna	výhybna	k1gFnSc1	výhybna
nebo	nebo	k8xC	nebo
odbočka	odbočka	k1gFnSc1	odbočka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nástupu	nástup	k1gInSc2	nástup
a	a	k8xC	a
výstupu	výstup	k1gInSc2	výstup
cestujících	cestující	k1gMnPc2	cestující
slouží	sloužit	k5eAaImIp3nS	sloužit
též	též	k9	též
železniční	železniční	k2eAgFnSc2d1	železniční
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nejsou	být	k5eNaImIp3nP	být
dopravnami	dopravna	k1gFnPc7	dopravna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
názvosloví	názvosloví	k1gNnSc6	názvosloví
i	i	k8xC	i
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
přepravně-tarifních	přepravněarifní	k2eAgInPc6d1	přepravně-tarifní
předpisech	předpis	k1gInPc6	předpis
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
železniční	železniční	k2eAgFnPc1d1	železniční
zastávky	zastávka	k1gFnPc1	zastávka
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
či	či	k8xC	či
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
z	z	k7c2	z
provozního	provozní	k2eAgNnSc2d1	provozní
hlediska	hledisko	k1gNnSc2	hledisko
mnohdy	mnohdy	k6eAd1	mnohdy
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přepravního	přepravní	k2eAgNnSc2d1	přepravní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
jedné	jeden	k4xCgFnSc2	jeden
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
více	hodně	k6eAd2	hodně
dílčích	dílčí	k2eAgNnPc2d1	dílčí
nádraží	nádraží	k1gNnPc2	nádraží
či	či	k8xC	či
železničních	železniční	k2eAgFnPc2d1	železniční
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
jeden	jeden	k4xCgInSc1	jeden
přepravní	přepravní	k2eAgInSc1d1	přepravní
uzel	uzel	k1gInSc1	uzel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
několika	několik	k4yIc2	několik
sdruženými	sdružený	k2eAgFnPc7d1	sdružená
železničními	železniční	k2eAgFnPc7d1	železniční
stanicemi	stanice	k1gFnPc7	stanice
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
zejména	zejména	k9	zejména
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vývoje	vývoj	k1gInSc2	vývoj
železnice	železnice	k1gFnSc1	železnice
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dráhy	dráha	k1gFnPc4	dráha
různého	různý	k2eAgNnSc2d1	různé
technického	technický	k2eAgNnSc2d1	technické
řešení	řešení	k1gNnSc2	řešení
(	(	kIx(	(
<g/>
rozchod	rozchod	k1gInSc1	rozchod
<g/>
,	,	kIx,	,
napájení	napájení	k1gNnSc1	napájení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolejová	kolejový	k2eAgNnPc1d1	kolejové
vozidla	vozidlo	k1gNnPc1	vozidlo
železnic	železnice	k1gFnPc2	železnice
jsou	být	k5eAaImIp3nP	být
sestavována	sestavovat	k5eAaImNgNnP	sestavovat
do	do	k7c2	do
vlaků	vlak	k1gInPc2	vlak
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
tzv.	tzv.	kA	tzv.
posunujících	posunující	k2eAgInPc2d1	posunující
dílů	díl	k1gInPc2	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
za	za	k7c7	za
sebou	se	k3xPyFc7	se
řazených	řazený	k2eAgInPc2d1	řazený
železničních	železniční	k2eAgInPc2d1	železniční
vozů	vůz	k1gInPc2	vůz
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
vagon	vagon	k1gInSc1	vagon
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
textech	text	k1gInPc6	text
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlak	vlak	k1gInSc1	vlak
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
posunující	posunující	k2eAgInSc1d1	posunující
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tažen	tažen	k2eAgMnSc1d1	tažen
nebo	nebo	k8xC	nebo
posunován	posunován	k2eAgMnSc1d1	posunován
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
nebo	nebo	k8xC	nebo
motorovými	motorový	k2eAgInPc7d1	motorový
vozy	vůz	k1gInPc7	vůz
<g/>
;	;	kIx,	;
tato	tento	k3xDgNnPc1	tento
hnací	hnací	k2eAgNnPc1d1	hnací
vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgNnP	umístit
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlaku	vlak	k1gInSc2	vlak
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
železničními	železniční	k2eAgInPc7d1	železniční
vozy	vůz	k1gInPc7	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vlak	vlak	k1gInSc1	vlak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oficiální	oficiální	k2eAgFnSc6d1	oficiální
železniční	železniční	k2eAgFnSc6d1	železniční
terminologii	terminologie	k1gFnSc6	terminologie
zároveň	zároveň	k6eAd1	zároveň
označením	označení	k1gNnSc7	označení
dopravního	dopravní	k2eAgInSc2d1	dopravní
spoje	spoj	k1gInSc2	spoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
označením	označení	k1gNnSc7	označení
železniční	železniční	k2eAgFnSc2d1	železniční
soupravy	souprava	k1gFnSc2	souprava
nebo	nebo	k8xC	nebo
železničního	železniční	k2eAgNnSc2d1	železniční
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
spoj	spoj	k1gInSc1	spoj
momentálně	momentálně	k6eAd1	momentálně
vykonáván	vykonáván	k2eAgInSc1d1	vykonáván
(	(	kIx(	(
<g/>
v	v	k7c6	v
železničním	železniční	k2eAgInSc6d1	železniční
polooficiálním	polooficiální	k2eAgInSc6d1	polooficiální
slangu	slang	k1gInSc6	slang
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlak	vlak	k1gInSc1	vlak
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
<g/>
"	"	kIx"	"
určitou	určitý	k2eAgFnSc7d1	určitá
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
nebo	nebo	k8xC	nebo
určitou	určitý	k2eAgFnSc7d1	určitá
soupravou	souprava	k1gFnSc7	souprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
jazyce	jazyk	k1gInSc6	jazyk
i	i	k8xC	i
například	například	k6eAd1	například
v	v	k7c6	v
terminologii	terminologie	k1gFnSc6	terminologie
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
vlak	vlak	k1gInSc1	vlak
<g/>
"	"	kIx"	"
nazývá	nazývat	k5eAaImIp3nS	nazývat
fyzicky	fyzicky	k6eAd1	fyzicky
souprava	souprava	k1gFnSc1	souprava
železničních	železniční	k2eAgNnPc2d1	železniční
vozidel	vozidlo	k1gNnPc2	vozidlo
nebo	nebo	k8xC	nebo
samostatně	samostatně	k6eAd1	samostatně
jedoucí	jedoucí	k2eAgNnSc1d1	jedoucí
železniční	železniční	k2eAgNnSc1d1	železniční
vozidlo	vozidlo	k1gNnSc1	vozidlo
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
železničních	železniční	k2eAgInPc2d1	železniční
provozních	provozní	k2eAgInPc2d1	provozní
předpisů	předpis	k1gInPc2	předpis
jimi	on	k3xPp3gFnPc7	on
momentálně	momentálně	k6eAd1	momentálně
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
vlakový	vlakový	k2eAgInSc1d1	vlakový
spoj	spoj	k1gInSc1	spoj
vykonáván	vykonáván	k2eAgInSc1d1	vykonáván
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
posunující	posunující	k2eAgInSc4d1	posunující
díl	díl	k1gInSc4	díl
nebo	nebo	k8xC	nebo
o	o	k7c4	o
momentálně	momentálně	k6eAd1	momentálně
odstavenou	odstavený	k2eAgFnSc4d1	odstavená
soupravu	souprava	k1gFnSc4	souprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
<g/>
,	,	kIx,	,
motorové	motorový	k2eAgInPc1d1	motorový
vozy	vůz	k1gInPc1	vůz
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
pohonné	pohonný	k2eAgFnPc1d1	pohonná
vozidlové	vozidlový	k2eAgFnPc1d1	vozidlová
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
pojmem	pojem	k1gInSc7	pojem
hnací	hnací	k2eAgNnSc1d1	hnací
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgMnSc1d1	železniční
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
hnací	hnací	k2eAgNnSc4d1	hnací
vozidlo	vozidlo	k1gNnSc4	vozidlo
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
řídicího	řídicí	k2eAgInSc2d1	řídicí
vozu	vůz	k1gInSc2	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
strojvedoucí	strojvedoucí	k1gMnPc1	strojvedoucí
<g/>
;	;	kIx,	;
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
drážní	drážní	k2eAgFnSc6d1	drážní
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc2	označení
řidič	řidič	k1gMnSc1	řidič
drážního	drážní	k2eAgNnSc2d1	drážní
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
specifickým	specifický	k2eAgInSc7d1	specifický
typem	typ	k1gInSc7	typ
řidiče	řidič	k1gMnSc2	řidič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc2	počátek
železnic	železnice	k1gFnPc2	železnice
obstarávala	obstarávat	k5eAaImAgFnS	obstarávat
pohon	pohon	k1gInSc4	pohon
tažná	tažný	k2eAgNnPc4d1	tažné
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
s	s	k7c7	s
parním	parní	k2eAgInSc7d1	parní
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
pohonem	pohon	k1gInSc7	pohon
(	(	kIx(	(
<g/>
vynalezen	vynalezen	k2eAgInSc1d1	vynalezen
toho	ten	k3xDgNnSc2	ten
roku	rok	k1gInSc2	rok
Wernerem	Werner	k1gMnSc7	Werner
von	von	k1gInSc4	von
Siemens	siemens	k1gInSc1	siemens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
zážehový	zážehový	k2eAgInSc4d1	zážehový
či	či	k8xC	či
vznětový	vznětový	k2eAgInSc4d1	vznětový
(	(	kIx(	(
<g/>
dieselový	dieselový	k2eAgInSc4d1	dieselový
<g/>
)	)	kIx)	)
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
vlaky	vlak	k1gInPc1	vlak
poháněny	poháněn	k2eAgInPc1d1	poháněn
i	i	k9	i
turbínami	turbína	k1gFnPc7	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Motory	motor	k1gInPc1	motor
přes	přes	k7c4	přes
převody	převod	k1gInPc4	převod
uvádějí	uvádět	k5eAaImIp3nP	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
vlak	vlak	k1gInSc1	vlak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
kolejnicích	kolejnice	k1gFnPc6	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pomocné	pomocný	k2eAgInPc1d1	pomocný
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ozubené	ozubený	k2eAgInPc4d1	ozubený
hřebeny	hřeben	k1gInPc4	hřeben
mezi	mezi	k7c7	mezi
kolejnicemi	kolejnice	k1gFnPc7	kolejnice
ozubnicové	ozubnicový	k2eAgFnSc2d1	ozubnicová
dráhy	dráha	k1gFnSc2	dráha
či	či	k8xC	či
třecí	třecí	k2eAgInPc4d1	třecí
disky	disk	k1gInPc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Pokusně	pokusně	k6eAd1	pokusně
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
i	i	k9	i
vrtulové	vrtulový	k2eAgInPc1d1	vrtulový
a	a	k8xC	a
reaktivní	reaktivní	k2eAgInPc1d1	reaktivní
motory	motor	k1gInPc1	motor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
neosvědčily	osvědčit	k5eNaPmAgFnP	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
používané	používaný	k2eAgInPc1d1	používaný
navijáky	naviják	k1gInPc1	naviják
či	či	k8xC	či
kladky	kladka	k1gFnPc1	kladka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vlaky	vlak	k1gInPc1	vlak
používaly	používat	k5eAaImAgInP	používat
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
stoupáním	stoupání	k1gNnSc7	stoupání
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
technickým	technický	k2eAgInSc7d1	technický
vývojem	vývoj	k1gInSc7	vývoj
pohonných	pohonný	k2eAgFnPc2d1	pohonná
jednotek	jednotka	k1gFnPc2	jednotka
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
překonány	překonán	k2eAgFnPc1d1	překonána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
trasách	trasa	k1gFnPc6	trasa
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
zalidněných	zalidněný	k2eAgFnPc6d1	zalidněná
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
upřednostňován	upřednostňován	k2eAgInSc1d1	upřednostňován
elektrický	elektrický	k2eAgInSc1d1	elektrický
pohon	pohon	k1gInSc1	pohon
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
dieselový	dieselový	k2eAgInSc4d1	dieselový
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
díky	díky	k7c3	díky
levné	levný	k2eAgFnSc3d1	levná
ropě	ropa	k1gFnSc3	ropa
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
dálková	dálkový	k2eAgFnSc1d1	dálková
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnPc1	železnice
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
většinou	většinou	k6eAd1	většinou
budovány	budovat	k5eAaImNgInP	budovat
komerčně	komerčně	k6eAd1	komerčně
privátními	privátní	k2eAgInPc7d1	privátní
subjekty	subjekt	k1gInPc7	subjekt
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
za	za	k7c2	za
silné	silný	k2eAgFnSc2d1	silná
regulace	regulace	k1gFnSc2	regulace
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
koncesí	koncese	k1gFnPc2	koncese
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
za	za	k7c4	za
veřejné	veřejný	k2eAgNnSc4d1	veřejné
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
státní	státní	k2eAgFnSc2d1	státní
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
většinou	většinou	k6eAd1	většinou
(	(	kIx(	(
<g/>
sv	sv	kA	sv
výjimkou	výjimka	k1gFnSc7	výjimka
vleček	vlečka	k1gFnPc2	vlečka
<g/>
)	)	kIx)	)
vykoupeny	vykoupen	k2eAgFnPc1d1	vykoupena
státem	stát	k1gInSc7	stát
a	a	k8xC	a
centralizovány	centralizován	k2eAgFnPc4d1	centralizována
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
režii	režie	k1gFnSc6	režie
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
provozovány	provozovat	k5eAaImNgFnP	provozovat
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnSc1d1	státní
dráhy	dráha	k1gFnPc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
(	(	kIx(	(
<g/>
soukromé	soukromý	k2eAgFnPc1d1	soukromá
dráhy	dráha	k1gFnPc1	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnými	veřejný	k2eAgFnPc7d1	veřejná
drahami	draha	k1gFnPc7	draha
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
za	za	k7c2	za
daných	daný	k2eAgFnPc2d1	daná
regulačních	regulační	k2eAgFnPc2d1	regulační
podmínek	podmínka	k1gFnPc2	podmínka
za	za	k7c4	za
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
úplatu	úplata	k1gFnSc4	úplata
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
každou	každý	k3xTgFnSc7	každý
osobou	osoba	k1gFnSc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
železniční	železniční	k2eAgFnSc1d1	železniční
společnost	společnost	k1gFnSc1	společnost
provozující	provozující	k2eAgFnSc4d1	provozující
dráhu	dráha	k1gFnSc4	dráha
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
provozovala	provozovat	k5eAaImAgFnS	provozovat
i	i	k8xC	i
drážní	drážní	k2eAgFnSc4d1	drážní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
provozování	provozování	k1gNnSc4	provozování
železnice	železnice	k1gFnSc2	železnice
bylo	být	k5eAaImAgNnS	být
dohromady	dohromady	k6eAd1	dohromady
jednou	jeden	k4xCgFnSc7	jeden
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
zaústění	zaústění	k1gNnSc2	zaústění
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
styku	styk	k1gInSc2	styk
drah	draha	k1gFnPc2	draha
vlaky	vlak	k1gInPc1	vlak
ze	z	k7c2	z
zaúsťující	zaúsťující	k2eAgFnSc2d1	zaúsťující
železnice	železnice	k1gFnSc2	železnice
peážovaly	peážovat	k5eAaImAgFnP	peážovat
po	po	k7c6	po
hostující	hostující	k2eAgFnSc6d1	hostující
železnici	železnice	k1gFnSc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
organizačně	organizačně	k6eAd1	organizačně
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
od	od	k7c2	od
dopravy	doprava	k1gFnSc2	doprava
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zajištění	zajištění	k1gNnSc2	zajištění
přístupu	přístup	k1gInSc2	přístup
bez	bez	k7c2	bez
diskriminace	diskriminace	k1gFnSc2	diskriminace
k	k	k7c3	k
železniční	železniční	k2eAgFnSc3d1	železniční
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Kolejová	kolejový	k2eAgFnSc1d1	kolejová
doprava	doprava	k1gFnSc1	doprava
má	mít	k5eAaImIp3nS	mít
mnohé	mnohý	k2eAgFnPc4d1	mnohá
přednosti	přednost	k1gFnPc4	přednost
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
i	i	k9	i
svá	svůj	k3xOyFgNnPc4	svůj
nebezpečí	nebezpečí	k1gNnPc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
mají	mít	k5eAaImIp3nP	mít
kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
a	a	k8xC	a
malému	malý	k2eAgNnSc3d1	malé
tření	tření	k1gNnSc3	tření
mezi	mezi	k7c7	mezi
kolem	kolo	k1gNnSc7	kolo
a	a	k8xC	a
kolejnicí	kolejnice	k1gFnSc7	kolejnice
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
brzdnou	brzdný	k2eAgFnSc4d1	brzdná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pohybu	pohyb	k1gInSc3	pohyb
po	po	k7c6	po
trati	trať	k1gFnSc6	trať
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
směr	směr	k1gInSc4	směr
jízdy	jízda	k1gFnSc2	jízda
kolejového	kolejový	k2eAgNnSc2d1	kolejové
vozidla	vozidlo	k1gNnSc2	vozidlo
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
tak	tak	k9	tak
překážce	překážka	k1gFnSc6	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
čelních	čelní	k2eAgFnPc2d1	čelní
a	a	k8xC	a
bočních	boční	k2eAgFnPc2d1	boční
srážek	srážka	k1gFnPc2	srážka
zapříčiňují	zapříčiňovat	k5eAaImIp3nP	zapříčiňovat
škody	škoda	k1gFnPc4	škoda
i	i	k8xC	i
vykolejení	vykolejení	k1gNnSc4	vykolejení
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
veřejnosti	veřejnost	k1gFnPc1	veřejnost
méně	málo	k6eAd2	málo
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
převrácení	převrácení	k1gNnSc4	převrácení
při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
bočním	boční	k2eAgInSc6d1	boční
větru	vítr	k1gInSc6	vítr
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
těžká	těžký	k2eAgNnPc4d1	těžké
neštěstí	neštěstí	k1gNnSc4	neštěstí
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zohledněna	zohledněn	k2eAgFnSc1d1	zohledněna
v	v	k7c6	v
předpisech	předpis	k1gInPc6	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
přesto	přesto	k8xC	přesto
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
bezpečným	bezpečný	k2eAgInPc3d1	bezpečný
dopravním	dopravní	k2eAgInPc3d1	dopravní
prostředkům	prostředek	k1gInPc3	prostředek
a	a	k8xC	a
k	k	k7c3	k
nehodám	nehoda	k1gFnPc3	nehoda
dochází	docházet	k5eAaImIp3nS	docházet
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
různým	různý	k2eAgNnPc3d1	různé
provozním	provozní	k2eAgNnPc3d1	provozní
opatřením	opatření	k1gNnPc3	opatření
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
přísným	přísný	k2eAgFnPc3d1	přísná
kontrolám	kontrola	k1gFnPc3	kontrola
zodpovědných	zodpovědný	k2eAgInPc2d1	zodpovědný
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
zavedeny	zavést	k5eAaPmNgFnP	zavést
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
principu	princip	k1gInSc2	princip
poměrně	poměrně	k6eAd1	poměrně
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
naslepo	naslepo	k6eAd1	naslepo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
dopravě	doprava	k1gFnSc6	doprava
musí	muset	k5eAaImIp3nS	muset
rychlost	rychlost	k1gFnSc1	rychlost
odpovídat	odpovídat	k5eAaImF	odpovídat
zábrzdné	zábrzdný	k2eAgFnSc3d1	zábrzdná
a	a	k8xC	a
rozhledové	rozhledový	k2eAgFnSc3d1	rozhledová
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
nehody	nehoda	k1gFnPc1	nehoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
mimořádně	mimořádně	k6eAd1	mimořádně
události	událost	k1gFnPc1	událost
systematicky	systematicky	k6eAd1	systematicky
prošetřovány	prošetřován	k2eAgFnPc1d1	prošetřována
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyvozovány	vyvozován	k2eAgInPc4d1	vyvozován
závěry	závěr	k1gInPc7	závěr
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zvyšovány	zvyšován	k2eAgInPc4d1	zvyšován
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
použité	použitý	k2eAgFnSc2d1	použitá
techniky	technika	k1gFnSc2	technika
i	i	k8xC	i
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgNnPc1d1	železniční
vozidla	vozidlo	k1gNnPc1	vozidlo
i	i	k8xC	i
zařízení	zařízení	k1gNnPc1	zařízení
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
železničního	železniční	k2eAgInSc2d1	železniční
provozu	provoz	k1gInSc2	provoz
mají	mít	k5eAaImIp3nP	mít
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
prvky	prvek	k1gInPc1	prvek
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
co	co	k9	co
nejbezpečnějšího	bezpečný	k2eAgInSc2d3	nejbezpečnější
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
návěstidla	návěstidlo	k1gNnPc1	návěstidlo
<g/>
,	,	kIx,	,
stavědla	stavědlo	k1gNnPc1	stavědlo
a	a	k8xC	a
železniční	železniční	k2eAgFnPc4d1	železniční
zabezpečovací	zabezpečovací	k2eAgNnPc4d1	zabezpečovací
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
na	na	k7c6	na
vozidlech	vozidlo	k1gNnPc6	vozidlo
pak	pak	k6eAd1	pak
samočinné	samočinný	k2eAgFnPc1d1	samočinná
brzdy	brzda	k1gFnPc1	brzda
a	a	k8xC	a
vlakové	vlakový	k2eAgInPc1d1	vlakový
zabezpečovače	zabezpečovač	k1gInPc1	zabezpečovač
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zabránit	zabránit	k5eAaPmF	zabránit
lidskému	lidský	k2eAgNnSc3d1	lidské
selhání	selhání	k1gNnSc3	selhání
strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečností	bezpečnost	k1gFnSc7	bezpečnost
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
osvědčených	osvědčený	k2eAgFnPc6d1	osvědčená
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
na	na	k7c6	na
poznatcích	poznatek	k1gInPc6	poznatek
z	z	k7c2	z
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
v	v	k7c6	v
železničním	železniční	k2eAgInSc6d1	železniční
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zohledněny	zohledněn	k2eAgFnPc1d1	zohledněna
příčiny	příčina	k1gFnPc1	příčina
železničních	železniční	k2eAgFnPc2d1	železniční
nehod	nehoda	k1gFnPc2	nehoda
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
neustále	neustále	k6eAd1	neustále
zdokonalována	zdokonalován	k2eAgNnPc1d1	zdokonalováno
<g/>
.	.	kIx.	.
</s>
<s>
Stavědla	stavědlo	k1gNnPc1	stavědlo
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
mechanickými	mechanický	k2eAgMnPc7d1	mechanický
<g/>
,	,	kIx,	,
elektrickými	elektrický	k2eAgInPc7d1	elektrický
a	a	k8xC	a
elektronickými	elektronický	k2eAgInPc7d1	elektronický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
že	že	k8xS	že
výhybky	výhybka	k1gFnPc4	výhybka
<g/>
,	,	kIx,	,
návěstidla	návěstidlo	k1gNnPc4	návěstidlo
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
technická	technický	k2eAgNnPc4d1	technické
zařízení	zařízení	k1gNnPc4	zařízení
jsou	být	k5eAaImIp3nP	být
jsou	být	k5eAaImIp3nP	být
nastavena	nastavit	k5eAaPmNgNnP	nastavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vlakům	vlak	k1gInPc3	vlak
nehrozila	hrozit	k5eNaImAgFnS	hrozit
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
srážka	srážka	k1gFnSc1	srážka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vykolejení	vykolejení	k1gNnSc1	vykolejení
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nepřiměřené	přiměřený	k2eNgNnSc4d1	nepřiměřené
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
provozu	provoz	k1gInSc2	provoz
dráhy	dráha	k1gFnPc1	dráha
skončily	skončit	k5eAaPmAgFnP	skončit
s	s	k7c7	s
jízdou	jízda	k1gFnSc7	jízda
na	na	k7c4	na
rozhled	rozhled	k1gInSc4	rozhled
jako	jako	k8xC	jako
všeobecně	všeobecně	k6eAd1	všeobecně
uplatňovaným	uplatňovaný	k2eAgNnSc7d1	uplatňované
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
tratě	trať	k1gFnPc1	trať
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
na	na	k7c4	na
úseky	úsek	k1gInPc4	úsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
oddělovala	oddělovat	k5eAaImAgNnP	oddělovat
traťová	traťový	k2eAgNnPc1d1	traťové
hradla	hradlo	k1gNnPc1	hradlo
<g/>
.	.	kIx.	.
</s>
<s>
Traťová	traťový	k2eAgNnPc1d1	traťové
zabezpečovací	zabezpečovací	k2eAgNnPc1d1	zabezpečovací
zařízení	zařízení	k1gNnPc1	zařízení
dnes	dnes	k6eAd1	dnes
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
uvedeném	uvedený	k2eAgInSc6d1	uvedený
úseku	úsek	k1gInSc6	úsek
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
vlak	vlak	k1gInSc1	vlak
a	a	k8xC	a
vlaky	vlak	k1gInPc1	vlak
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
pevně	pevně	k6eAd1	pevně
určených	určený	k2eAgFnPc6d1	určená
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vývoje	vývoj	k1gInSc2	vývoj
železnice	železnice	k1gFnSc2	železnice
probíhalo	probíhat	k5eAaImAgNnS	probíhat
postupné	postupný	k2eAgNnSc1d1	postupné
předávání	předávání	k1gNnSc1	předávání
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
za	za	k7c4	za
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
technice	technika	k1gFnSc6	technika
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byla	být	k5eAaImAgFnS	být
obsluha	obsluha	k1gFnSc1	obsluha
traťového	traťový	k2eAgNnSc2d1	traťové
hradla	hradlo	k1gNnSc2	hradlo
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
zaměstnanci	zaměstnanec	k1gMnSc6	zaměstnanec
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vlak	vlak	k1gInSc1	vlak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
traťovém	traťový	k2eAgInSc6d1	traťový
úseku	úsek	k1gInSc6	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
četných	četný	k2eAgFnPc6d1	četná
nehodách	nehoda	k1gFnPc6	nehoda
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
technicky	technicky	k6eAd1	technicky
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlak	vlak	k1gInSc1	vlak
(	(	kIx(	(
<g/>
či	či	k8xC	či
část	část	k1gFnSc1	část
vlaku	vlak	k1gInSc2	vlak
<g/>
)	)	kIx)	)
obsluhu	obsluha	k1gFnSc4	obsluha
skutečně	skutečně	k6eAd1	skutečně
minul	minout	k5eAaImAgMnS	minout
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgInPc1d1	budoucí
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
systémy	systém	k1gInPc1	systém
mají	mít	k5eAaImIp3nP	mít
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlaky	vlak	k1gInPc1	vlak
budou	být	k5eAaImBp3nP	být
jezdit	jezdit	k5eAaImF	jezdit
v	v	k7c6	v
relativní	relativní	k2eAgFnSc6d1	relativní
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
své	svůj	k3xOyFgFnSc2	svůj
brzdné	brzdný	k2eAgFnSc2d1	brzdná
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
kapacita	kapacita	k1gFnSc1	kapacita
a	a	k8xC	a
energetická	energetický	k2eAgFnSc1d1	energetická
hospodárnost	hospodárnost	k1gFnSc1	hospodárnost
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c6	v
přesných	přesný	k2eAgInPc6d1	přesný
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
traťových	traťový	k2eAgInPc6d1	traťový
úsecích	úsek	k1gInPc6	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
systematické	systematický	k2eAgInPc1d1	systematický
a	a	k8xC	a
zajištěné	zajištěný	k2eAgInPc1d1	zajištěný
komunikační	komunikační	k2eAgInPc1d1	komunikační
protokoly	protokol	k1gInPc1	protokol
mezi	mezi	k7c7	mezi
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
ve	v	k7c6	v
vozidlech	vozidlo	k1gNnPc6	vozidlo
a	a	k8xC	a
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
přesná	přesný	k2eAgFnSc1d1	přesná
evidence	evidence	k1gFnSc1	evidence
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
<g/>
;	;	kIx,	;
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
umožnit	umožnit	k5eAaPmF	umožnit
vozidlu	vozidlo	k1gNnSc3	vozidlo
další	další	k2eAgFnSc4d1	další
jízdu	jízda	k1gFnSc4	jízda
při	při	k7c6	při
poruše	porucha	k1gFnSc6	porucha
signalizace	signalizace	k1gFnSc2	signalizace
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
určen	určen	k2eAgInSc1d1	určen
a	a	k8xC	a
možné	možný	k2eAgFnPc1d1	možná
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
manipulace	manipulace	k1gFnPc1	manipulace
se	se	k3xPyFc4	se
stavědlech	stavědlo	k1gNnPc6	stavědlo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
písemně	písemně	k6eAd1	písemně
dokumentovány	dokumentovat	k5eAaBmNgInP	dokumentovat
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
železnice	železnice	k1gFnSc2	železnice
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
vazbou	vazba	k1gFnSc7	vazba
mezi	mezi	k7c7	mezi
organizováním	organizování	k1gNnSc7	organizování
a	a	k8xC	a
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
stále	stále	k6eAd1	stále
mají	mít	k5eAaImIp3nP	mít
lidé	člověk	k1gMnPc1	člověk
při	při	k7c6	při
zajišťování	zajišťování	k1gNnSc6	zajišťování
vlakového	vlakový	k2eAgInSc2d1	vlakový
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rychlostních	rychlostní	k2eAgFnPc6d1	rychlostní
tratích	trať	k1gFnPc6	trať
při	při	k7c6	při
rychlostech	rychlost	k1gFnPc6	rychlost
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
pro	pro	k7c4	pro
vlaky	vlak	k1gInPc4	vlak
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
zaveden	zavést	k5eAaPmNgInS	zavést
tzn.	tzn.	kA	tzn.
Linienzugbeeinflussung	Linienzugbeeinflussung	k1gInSc1	Linienzugbeeinflussung
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
souhlas	souhlas	k1gInSc1	souhlas
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
a	a	k8xC	a
údaje	údaj	k1gInPc4	údaj
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
vlaku	vlak	k1gInSc2	vlak
předávají	předávat	k5eAaImIp3nP	předávat
na	na	k7c4	na
stanoviště	stanoviště	k1gNnSc4	stanoviště
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
sleduje	sledovat	k5eAaImIp3nS	sledovat
parametry	parametr	k1gInPc4	parametr
jízdy	jízda	k1gFnSc2	jízda
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
brzdnou	brzdný	k2eAgFnSc4d1	brzdná
křivku	křivka	k1gFnSc4	křivka
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
překročení	překročení	k1gNnSc2	překročení
rychlosti	rychlost	k1gFnSc2	rychlost
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
rychločinné	rychločinný	k2eAgNnSc1d1	rychločinný
brzdění	brzdění	k1gNnSc1	brzdění
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ETCS	ETCS	kA	ETCS
<g/>
,	,	kIx,	,
ERTMS	ERTMS	kA	ERTMS
a	a	k8xC	a
GSM-R	GSM-R	k1gMnPc1	GSM-R
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
zavedeny	zaveden	k2eAgInPc4d1	zaveden
celoevropské	celoevropský	k2eAgInPc4d1	celoevropský
standardy	standard	k1gInPc4	standard
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
příspěvkem	příspěvek	k1gInSc7	příspěvek
k	k	k7c3	k
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
provozu	provoz	k1gInSc2	provoz
je	být	k5eAaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
poloha	poloha	k1gFnSc1	poloha
kolejnic	kolejnice	k1gFnPc2	kolejnice
a	a	k8xC	a
stálá	stálý	k2eAgFnSc1d1	stálá
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
kolejí	kolej	k1gFnPc2	kolej
se	se	k3xPyFc4	se
následkem	následkem	k7c2	následkem
železničního	železniční	k2eAgInSc2d1	železniční
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
povětrnostními	povětrnostní	k2eAgInPc7d1	povětrnostní
vlivy	vliv	k1gInPc7	vliv
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
přesně	přesně	k6eAd1	přesně
určených	určený	k2eAgInPc6d1	určený
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
proměřování	proměřování	k1gNnSc2	proměřování
geometrie	geometrie	k1gFnSc2	geometrie
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc1	jejich
oprava	oprava	k1gFnSc1	oprava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
měření	měření	k1gNnSc3	měření
existují	existovat	k5eAaImIp3nP	existovat
speciální	speciální	k2eAgNnPc4d1	speciální
železniční	železniční	k2eAgNnPc4d1	železniční
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgInPc1d1	železniční
přejezdy	přejezd	k1gInPc1	přejezd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pozemní	pozemní	k2eAgFnPc1d1	pozemní
komunikace	komunikace	k1gFnPc1	komunikace
úrovňově	úrovňově	k6eAd1	úrovňově
kříží	křížit	k5eAaImIp3nP	křížit
se	s	k7c7	s
železnicí	železnice	k1gFnSc7	železnice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jištěny	jistit	k5eAaImNgFnP	jistit
závorami	závora	k1gFnPc7	závora
<g/>
,	,	kIx,	,
světelným	světelný	k2eAgNnSc7d1	světelné
signalizačním	signalizační	k2eAgNnSc7d1	signalizační
zařízením	zařízení	k1gNnSc7	zařízení
a	a	k8xC	a
nebo	nebo	k8xC	nebo
osazeny	osazen	k2eAgInPc1d1	osazen
pouze	pouze	k6eAd1	pouze
dopravními	dopravní	k2eAgFnPc7d1	dopravní
značkami	značka	k1gFnPc7	značka
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečené	zabezpečený	k2eAgInPc1d1	zabezpečený
železniční	železniční	k2eAgInPc1d1	železniční
přejezdy	přejezd	k1gInPc1	přejezd
jsou	být	k5eAaImIp3nP	být
začleněny	začlenit	k5eAaPmNgInP	začlenit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
stavědel	stavědlo	k1gNnPc2	stavědlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
železničních	železniční	k2eAgInPc6d1	železniční
přejezdech	přejezd	k1gInPc6	přejezd
je	být	k5eAaImIp3nS	být
ohrožena	ohrožen	k2eAgFnSc1d1	ohrožena
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
hlavních	hlavní	k2eAgFnPc6d1	hlavní
tratích	trať	k1gFnPc6	trať
postupně	postupně	k6eAd1	postupně
nahrazovány	nahrazován	k2eAgFnPc1d1	nahrazována
mimoúrovňovými	mimoúrovňový	k2eAgNnPc7d1	mimoúrovňové
kříženími	křížení	k1gNnPc7	křížení
a	a	k8xC	a
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
schvalovány	schvalovat	k5eAaImNgInP	schvalovat
při	při	k7c6	při
stavbách	stavba	k1gFnPc6	stavba
nových	nový	k2eAgFnPc2d1	nová
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgInPc1d1	železniční
přejezdy	přejezd	k1gInPc1	přejezd
omezují	omezovat	k5eAaImIp3nP	omezovat
průjezd	průjezd	k1gInSc4	průjezd
vozidel	vozidlo	k1gNnPc2	vozidlo
složek	složka	k1gFnPc2	složka
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
záchranného	záchranný	k2eAgInSc2d1	záchranný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgInSc1d1	železniční
přejezd	přejezd	k1gInSc1	přejezd
včetně	včetně	k7c2	včetně
vybavení	vybavení	k1gNnSc2	vybavení
(	(	kIx(	(
<g/>
závory	závora	k1gFnPc4	závora
<g/>
,	,	kIx,	,
signalizační	signalizační	k2eAgNnSc4d1	signalizační
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
dopravního	dopravní	k2eAgNnSc2d1	dopravní
značení	značení	k1gNnSc2	značení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
české	český	k2eAgFnSc2d1	Česká
legislativy	legislativa	k1gFnSc2	legislativa
součástí	součást	k1gFnSc7	součást
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
součástí	součást	k1gFnSc7	součást
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
dráhu	dráha	k1gFnSc4	dráha
kříží	křížit	k5eAaImIp3nP	křížit
<g/>
.	.	kIx.	.
</s>
<s>
Nástrojem	nástroj	k1gInSc7	nástroj
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
grafikon	grafikon	k1gNnSc1	grafikon
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plánování	plánování	k1gNnSc6	plánování
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zohledněny	zohledněn	k2eAgFnPc1d1	zohledněna
různé	různý	k2eAgFnPc1d1	různá
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
:	:	kIx,	:
možnosti	možnost	k1gFnPc1	možnost
křižování	křižování	k1gNnSc2	křižování
na	na	k7c6	na
železničních	železniční	k2eAgFnPc6d1	železniční
stanicích	stanice	k1gFnPc6	stanice
ležících	ležící	k2eAgFnPc2d1	ležící
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
možná	možná	k9	možná
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
rychlost	rychlost	k1gFnSc4	rychlost
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgInSc4d1	minimální
odstup	odstup	k1gInSc4	odstup
následujících	následující	k2eAgInPc2d1	následující
vlaků	vlak	k1gInPc2	vlak
(	(	kIx(	(
<g/>
daný	daný	k2eAgInSc1d1	daný
blokovými	blokový	k2eAgInPc7d1	blokový
signály	signál	k1gInPc7	signál
trati	trať	k1gFnSc2	trať
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
návaznost	návaznost	k1gFnSc1	návaznost
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
vlaky	vlak	k1gInPc4	vlak
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
podmínky	podmínka	k1gFnPc4	podmínka
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc4	hmotnost
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
brzdné	brzdný	k2eAgFnPc1d1	brzdná
možnosti	možnost	k1gFnPc1	možnost
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
hospodárný	hospodárný	k2eAgInSc4d1	hospodárný
provoz	provoz	k1gInSc4	provoz
je	být	k5eAaImIp3nS	být
optimální	optimální	k2eAgNnSc4d1	optimální
nasazení	nasazení	k1gNnSc4	nasazení
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
personálu	personál	k1gInSc2	personál
<g/>
:	:	kIx,	:
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemají	mít	k5eNaImIp3nP	mít
např.	např.	kA	např.
zbytečně	zbytečně	k6eAd1	zbytečně
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
jízdní	jízdní	k2eAgInSc1d1	jízdní
řád	řád	k1gInSc1	řád
má	mít	k5eAaImIp3nS	mít
dostatek	dostatek	k1gInSc4	dostatek
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
jejich	jejich	k3xOp3gInSc4	jejich
nadbytek	nadbytek	k1gInSc4	nadbytek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
drobná	drobná	k1gFnSc1	drobná
zpoždění	zpoždění	k1gNnSc2	zpoždění
nepřenášela	přenášet	k5eNaImAgFnS	přenášet
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
železniční	železniční	k2eAgInPc4d1	železniční
spoje	spoj	k1gInPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Taktový	taktový	k2eAgInSc1d1	taktový
jízdní	jízdní	k2eAgInSc1d1	jízdní
řád	řád	k1gInSc1	řád
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
skladbou	skladba	k1gFnSc7	skladba
snadno	snadno	k6eAd1	snadno
zapamatovatelný	zapamatovatelný	k2eAgMnSc1d1	zapamatovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plánovače	plánovač	k1gMnPc4	plánovač
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc4	jeho
přednosti	přednost	k1gFnPc4	přednost
v	v	k7c6	v
jednoduchosti	jednoduchost	k1gFnSc6	jednoduchost
<g/>
,	,	kIx,	,
v	v	k7c6	v
symetrii	symetrie	k1gFnSc6	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Taktové	taktový	k2eAgInPc1d1	taktový
jízdní	jízdní	k2eAgInPc1d1	jízdní
řády	řád	k1gInPc1	řád
se	se	k3xPyFc4	se
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
jako	jako	k9	jako
síťové	síťový	k2eAgInPc1d1	síťový
dopravní	dopravní	k2eAgInPc1d1	dopravní
plány	plán	k1gInPc1	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
je	být	k5eAaImIp3nS	být
rozlišováno	rozlišovat	k5eAaImNgNnS	rozlišovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dálkové	dálkový	k2eAgNnSc4d1	dálkové
vlaků	vlak	k1gInPc2	vlak
InterCity	InterCit	k2eAgFnPc1d1	InterCit
či	či	k8xC	či
příměstská	příměstský	k2eAgFnSc1d1	příměstská
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interním	interní	k2eAgInSc6d1	interní
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
jsou	být	k5eAaImIp3nP	být
zohledněny	zohledněn	k2eAgInPc1d1	zohledněn
i	i	k8xC	i
nákladní	nákladní	k2eAgInPc1d1	nákladní
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
či	či	k8xC	či
jízdy	jízda	k1gFnPc1	jízda
prázdných	prázdný	k2eAgInPc2d1	prázdný
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vlakové	vlakový	k2eAgInPc1d1	vlakový
spoje	spoj	k1gInPc1	spoj
osobní	osobní	k2eAgFnSc2d1	osobní
i	i	k8xC	i
nákladní	nákladní	k2eAgFnSc2d1	nákladní
přepravy	přeprava	k1gFnSc2	přeprava
jsou	být	k5eAaImIp3nP	být
číslovány	číslován	k2eAgFnPc1d1	číslována
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
čísla	číslo	k1gNnSc2	číslo
vlaku	vlak	k1gInSc2	vlak
je	být	k5eAaImIp3nS	být
písmenná	písmenný	k2eAgFnSc1d1	písmenná
zkratka	zkratka	k1gFnSc1	zkratka
ooznačující	ooznačující	k2eAgFnSc1d1	ooznačující
druh	druh	k1gInSc4	druh
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgInPc4d2	významnější
dálkové	dálkový	k2eAgInPc4d1	dálkový
vlaky	vlak	k1gInPc4	vlak
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
cestujících	cestující	k1gMnPc2	cestující
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
železničním	železniční	k2eAgInSc6d1	železniční
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
tradičně	tradičně	k6eAd1	tradičně
pojmenovávány	pojmenováván	k2eAgFnPc1d1	pojmenovávána
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zavádění	zavádění	k1gNnSc3	zavádění
intervalové	intervalový	k2eAgFnSc2d1	intervalová
taktové	taktový	k2eAgFnSc2d1	taktová
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
také	také	k9	také
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
konkurenčního	konkurenční	k2eAgNnSc2d1	konkurenční
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
odpovídajícímu	odpovídající	k2eAgInSc3d1	odpovídající
způsobu	způsob	k1gInSc3	způsob
objednávání	objednávání	k1gNnSc2	objednávání
a	a	k8xC	a
dotování	dotování	k1gNnSc2	dotování
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
linková	linkový	k2eAgFnSc1d1	Linková
organizace	organizace	k1gFnSc1	organizace
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
označení	označení	k1gNnSc1	označení
linky	linka	k1gFnSc2	linka
nabývá	nabývat	k5eAaImIp3nS	nabývat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
cestujícím	cestující	k1gMnPc3	cestující
většího	veliký	k2eAgInSc2d2	veliký
významu	význam	k1gInSc2	význam
než	než	k8xS	než
číslo	číslo	k1gNnSc1	číslo
vlakového	vlakový	k2eAgInSc2d1	vlakový
spoje	spoj	k1gInSc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
automatizován	automatizován	k2eAgMnSc1d1	automatizován
a	a	k8xC	a
centralizován	centralizován	k2eAgMnSc1d1	centralizován
<g/>
.	.	kIx.	.
</s>
<s>
Stavědla	stavědlo	k1gNnSc2	stavědlo
převzala	převzít	k5eAaPmAgFnS	převzít
nastavování	nastavování	k1gNnSc4	nastavování
výhybek	výhybka	k1gFnPc2	výhybka
a	a	k8xC	a
návěstidel	návěstidlo	k1gNnPc2	návěstidlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nasazením	nasazení	k1gNnSc7	nasazení
techniky	technika	k1gFnSc2	technika
řízení	řízení	k1gNnSc2	řízení
železničního	železniční	k2eAgInSc2d1	železniční
provozu	provoz	k1gInSc2	provoz
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dálkově	dálkově	k6eAd1	dálkově
řízena	řízen	k2eAgNnPc4d1	řízeno
návěstidla	návěstidlo	k1gNnPc4	návěstidlo
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Automatické	automatický	k2eAgNnSc4d1	automatické
řízení	řízení	k1gNnSc4	řízení
vlaků	vlak	k1gInPc2	vlak
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
železniční	železniční	k2eAgFnPc4d1	železniční
trasy	trasa	k1gFnPc4	trasa
na	na	k7c6	na
základě	základ	k1gInSc6	základ
elektronicky	elektronicky	k6eAd1	elektronicky
uložených	uložený	k2eAgInPc2d1	uložený
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Eisenbahn	Eisenbahna	k1gFnPc2	Eisenbahna
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
BASS	Bass	k1gMnSc1	Bass
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
našich	náš	k3xOp1gFnPc2	náš
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
–	–	k?	–
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Čestlice	Čestlice	k1gFnSc1	Čestlice
:	:	kIx,	:
Rebo	Rebo	k1gNnSc1	Rebo
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
255	[number]	k4	255
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
287	[number]	k4	287
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
TANEL	TANEL	kA	TANEL
<g/>
,	,	kIx,	,
Franco	Franco	k6eAd1	Franco
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
–	–	k?	–
od	od	k7c2	od
parních	parní	k2eAgFnPc2d1	parní
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
k	k	k7c3	k
vysokorychlostním	vysokorychlostní	k2eAgInPc3d1	vysokorychlostní
vlakům	vlak	k1gInPc3	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7391	[number]	k4	7391
<g/>
-	-	kIx~	-
<g/>
782	[number]	k4	782
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Seznam	seznam	k1gInSc1	seznam
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Trať	trať	k1gFnSc1	trať
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
železniční	železniční	k2eAgFnSc2d1	železniční
unie	unie	k1gFnSc2	unie
Modelová	modelový	k2eAgFnSc1d1	modelová
železnice	železnice	k1gFnSc1	železnice
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
železnice	železnice	k1gFnSc2	železnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
železnice	železnice	k1gFnSc2	železnice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Zelpage	Zelpag	k1gInSc2	Zelpag
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Stránka	stránka	k1gFnSc1	stránka
českých	český	k2eAgMnPc2d1	český
příznivců	příznivec	k1gMnPc2	příznivec
železnice	železnice	k1gFnSc2	železnice
Vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
Stránka	stránka	k1gFnSc1	stránka
slovenských	slovenský	k2eAgMnPc2d1	slovenský
příznivců	příznivec	k1gMnPc2	příznivec
železnice	železnice	k1gFnSc2	železnice
zeleznice	zeleznice	k1gFnSc1	zeleznice
<g/>
.	.	kIx.	.
<g/>
biz	biz	k?	biz
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
galerie	galerie	k1gFnSc1	galerie
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Atlas	Atlas	k1gMnSc1	Atlas
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
K-REPORT	K-REPORT	k1gFnPc2	K-REPORT
český	český	k2eAgInSc1d1	český
dopravní	dopravní	k2eAgInSc1d1	dopravní
server	server	k1gInSc1	server
Heslo	heslo	k1gNnSc1	heslo
železnice	železnice	k1gFnSc2	železnice
na	na	k7c4	na
Open	Open	k1gNnSc4	Open
directory	director	k1gInPc4	director
project	project	k2eAgInSc4d1	project
Babitron	Babitron	k1gInSc4	Babitron
-	-	kIx~	-
aktuální	aktuální	k2eAgFnSc1d1	aktuální
poloha	poloha	k1gFnSc1	poloha
vlaků	vlak	k1gInPc2	vlak
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
dráhách	dráha	k1gFnPc6	dráha
-	-	kIx~	-
znění	znění	k1gNnSc1	znění
platné	platný	k2eAgNnSc1d1	platné
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
zveřejněné	zveřejněný	k2eAgNnSc1d1	zveřejněné
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ČR	ČR	kA	ČR
</s>
