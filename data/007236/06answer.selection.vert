<s>
Dílčí	dílčí	k2eAgInPc1d1	dílčí
principy	princip	k1gInPc1	princip
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaImNgInP	využívat
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
železnicie	železnicie	k1gFnSc1	železnicie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
