<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
Н	Н	k?
Hlavní	hlavní	k2eAgFnSc1d1
třída	třída	k1gFnSc1
Nur-Sultanu	Nur-Sultan	k1gInSc2
v	v	k7c6
pozadí	pozadí	k1gNnSc6
s	s	k7c7
prezidentským	prezidentský	k2eAgInSc7d1
palácem	palác	k1gInSc7
Ak	Ak	k1gFnSc2
Orda	orda	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
71	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
347	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
Oblast	oblast	k1gFnSc1
</s>
<s>
Akmolská	Akmolský	k2eAgFnSc1d1
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
710,2	710,2	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
078	#num#	k4
362	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
518,4	518,4	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Etnické	etnický	k2eAgNnSc4d1
složení	složení	k1gNnSc4
</s>
<s>
Kazaši	Kazach	k1gMnPc1
<g/>
,	,	kIx,
Rusové	Rus	k1gMnPc1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgNnSc1d1
Náboženské	náboženský	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
islám	islám	k1gInSc1
<g/>
,	,	kIx,
pravoslaví	pravoslaví	k1gNnPc1
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Altaj	Altaj	k1gInSc1
Kulginov	Kulginov	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1830	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.astana.kz	www.astana.kz	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+7	+7	k4
7172	#num#	k4
PSČ	PSČ	kA
</s>
<s>
010000	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
Z	Z	kA
a	a	k8xC
01	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
(	(	kIx(
<g/>
kazašsky	kazašsky	k6eAd1
Н	Н	k?
<g/>
;	;	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
známý	známý	k2eAgMnSc1d1
též	též	k9
pod	pod	k7c7
dřívějším	dřívější	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Astana	Astan	k1gMnSc2
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
kazašsky	kazašsky	k6eAd1
А	А	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
pod	pod	k7c7
staršími	starý	k2eAgInPc7d2
názvy	název	k1gInPc7
Akmola	Akmola	k1gFnSc1
nebo	nebo	k8xC
Celinograd	Celinograd	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Kazachstánu	Kazachstán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1,08	1,08	k4
milionu	milion	k4xCgInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
liduprázdných	liduprázdný	k2eAgFnPc2d1
stepí	step	k1gFnPc2
středního	střední	k2eAgInSc2d1
Kazachstánu	Kazachstán	k1gInSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Išim	Išima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Původní	původní	k2eAgInSc1d1
ruský	ruský	k2eAgInSc1d1
název	název	k1gInSc1
města	město	k1gNnSc2
Akmolinsk	Akmolinsk	k1gInSc1
vycházel	vycházet	k5eAaImAgInS
z	z	k7c2
kazašského	kazašský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
akmola	akmola	k1gFnSc1
<g/>
,	,	kIx,
bílý	bílý	k2eAgInSc1d1
hrob	hrob	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1961	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c4
doporučení	doporučení	k1gNnSc4
N.	N.	kA
S.	S.	kA
Chruščova	Chruščův	k2eAgMnSc4d1
město	město	k1gNnSc1
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
Celinograd	Celinograd	k1gInSc4
podle	podle	k7c2
celin	celina	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
obdělávání	obdělávání	k1gNnSc1
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
hlavní	hlavní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
bylo	být	k5eAaImAgNnS
ruské	ruský	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
nahrazeno	nahrazen	k2eAgNnSc1d1
oficiálním	oficiální	k2eAgNnSc7d1
kazašským	kazašský	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Akmola	Akmola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgInSc1d1
název	název	k1gInSc1
Astana	Astan	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
pro	pro	k7c4
město	město	k1gNnSc4
používal	používat	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
kazašsky	kazašsky	k6eAd1
„	„	k?
<g/>
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Nastupující	nastupující	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Kazachstánu	Kazachstán	k1gInSc2
Kasym-Žomart	Kasym-Žomart	k1gInSc1
Tokajev	Tokajev	k1gFnSc2
v	v	k7c6
inaugurační	inaugurační	k2eAgFnSc6d1
řeči	řeč	k1gFnSc6
pronesené	pronesený	k2eAgNnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
navrhl	navrhnout	k5eAaPmAgMnS
přejmenování	přejmenování	k1gNnSc4
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
na	na	k7c4
Nur-Sultan	Nur-Sultan	k1gInSc4
(	(	kIx(
<g/>
Н	Н	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
počest	počest	k1gFnSc4
prvního	první	k4xOgMnSc2
kazachstánského	kazachstánský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Nursultana	Nursultan	k1gMnSc2
Nazarbajeva	Nazarbajev	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Nursultan	Nursultan	k1gInSc1
Nazarbajev	Nazarbajev	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
den	den	k1gInSc4
předtím	předtím	k6eAd1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
v	v	k7c6
úterý	úterý	k1gNnSc6
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Návrh	návrh	k1gInSc1
na	na	k7c4
přejmenování	přejmenování	k1gNnSc4
tehdejší	tehdejší	k2eAgFnSc2d1
Astany	Astana	k1gFnSc2
podpořila	podpořit	k5eAaPmAgFnS
vládnoucí	vládnoucí	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
Marat	Marat	k2eAgMnSc1d1
Beketajev	Beketajev	k1gMnSc1
doplnil	doplnit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
změna	změna	k1gFnSc1
vstoupí	vstoupit	k5eAaPmIp3nS
v	v	k7c4
platnost	platnost	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
bude	být	k5eAaImBp3nS
příslušně	příslušně	k6eAd1
upravena	upravit	k5eAaPmNgFnS
ústava	ústava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Kasym-Žomart	Kasym-Žomart	k1gInSc1
Tokajev	Tokajev	k1gFnSc2
zamítl	zamítnout	k5eAaPmAgInS
vládní	vládní	k2eAgInSc4d1
návrh	návrh	k1gInSc4
na	na	k7c4
vyhlášení	vyhlášení	k1gNnSc4
republikového	republikový	k2eAgNnSc2d1
referenda	referendum	k1gNnSc2
kvůli	kvůli	k7c3
přejmenování	přejmenování	k1gNnSc3
kazachstánského	kazachstánský	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
předložen	předložit	k5eAaPmNgInS
přímo	přímo	k6eAd1
parlamentu	parlament	k1gInSc2
a	a	k8xC
změna	změna	k1gFnSc1
podle	podle	k7c2
článku	článek	k1gInSc2
53	#num#	k4
ústavy	ústava	k1gFnSc2
Kazachstánu	Kazachstán	k1gInSc2
byla	být	k5eAaImAgFnS
ve	v	k7c6
dvou	dva	k4xCgNnPc6
čteních	čtení	k1gNnPc6
102	#num#	k4
poslanci	poslanec	k1gMnPc1
mažlisu	mažlis	k1gInSc2
a	a	k8xC
43	#num#	k4
senátory	senátor	k1gMnPc4
schválena	schválit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
oddíle	oddíl	k1gInSc6
I	I	kA
<g/>
,	,	kIx,
článku	článek	k1gInSc2
2	#num#	k4
ústavy	ústava	k1gFnSc2
byl	být	k5eAaImAgInS
zakotven	zakotvit	k5eAaPmNgInS
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Н	Н	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
konkrétně	konkrétně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c6
schválení	schválení	k1gNnSc6
zákona	zákon	k1gInSc2
№	№	k?
238-VІ	238-VІ	k?
З	З	k?
ze	z	k7c2
dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
"	"	kIx"
<g/>
О	О	k?
в	в	k?
и	и	k?
в	в	k?
К	К	k?
Р	Р	k?
К	К	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někteří	některý	k3yIgMnPc1
kazachstánští	kazachstánský	k2eAgMnPc1d1
právníci	právník	k1gMnPc1
vzápětí	vzápětí	k6eAd1
veřejně	veřejně	k6eAd1
vyslovili	vyslovit	k5eAaPmAgMnP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc4
přejmenování	přejmenování	k1gNnSc4
je	být	k5eAaImIp3nS
nezákonné	zákonný	k2eNgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
ústavě	ústava	k1gFnSc6
je	být	k5eAaImIp3nS
stanoveno	stanovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
dočasně	dočasně	k6eAd1
pověřena	pověřit	k5eAaPmNgFnS
výkonem	výkon	k1gInSc7
prezidentské	prezidentský	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
stávající	stávající	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
funkci	funkce	k1gFnSc6
předčasně	předčasně	k6eAd1
skončil	skončit	k5eAaPmAgMnS
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
právo	právo	k1gNnSc4
předkládat	předkládat	k5eAaImF
návrhy	návrh	k1gInPc4
na	na	k7c4
změnu	změna	k1gFnSc4
ústavy	ústava	k1gFnSc2
a	a	k8xC
že	že	k8xS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
vypsáno	vypsán	k2eAgNnSc4d1
referendum	referendum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
Kazachstánu	Kazachstán	k1gInSc2
tyto	tento	k3xDgFnPc4
námitky	námitka	k1gFnPc4
odmítlo	odmítnout	k5eAaPmAgNnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
změny	změna	k1gFnPc1
byly	být	k5eAaImAgFnP
schváleny	schválen	k2eAgFnPc1d1
parlamentem	parlament	k1gInSc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
tudíž	tudíž	k8xC
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
zněním	znění	k1gNnSc7
ústavy	ústava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Nursultan	Nursultan	k1gInSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
arabštiny	arabština	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
složeno	složen	k2eAgNnSc1d1
ze	z	k7c2
dvou	dva	k4xCgNnPc2
slov	slovo	k1gNnPc2
–	–	k?
„	„	k?
<g/>
nur	nur	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
„	„	k?
<g/>
světlo	světlo	k1gNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
soudobé	soudobý	k2eAgFnSc6d1
kazaštině	kazaština	k1gFnSc6
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
výraz	výraz	k1gInSc1
také	také	k9
překládá	překládat	k5eAaImIp3nS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
záře	zář	k1gFnSc2
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
sluneční	sluneční	k2eAgInSc1d1
paprsek	paprsek	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
sultan	sultan	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
arabsky	arabsky	k6eAd1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
vláda	vláda	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
vládce	vládce	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
autorita	autorita	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
„	„	k?
<g/>
Sultan	Sultan	k1gInSc1
<g/>
“	“	k?
byl	být	k5eAaImAgInS
ve	v	k7c6
středověku	středověk	k1gInSc6
titulem	titul	k1gInSc7
vládce	vládce	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Sultan	Sultan	k1gInSc1
<g/>
“	“	k?
začalo	začít	k5eAaPmAgNnS
objevovat	objevovat	k5eAaImF
jako	jako	k9
osobní	osobní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
neměli	mít	k5eNaImAgMnP
žádné	žádný	k3yNgFnPc4
vazby	vazba	k1gFnPc4
na	na	k7c4
panovnický	panovnický	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
výraz	výraz	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
jak	jak	k6eAd1
ve	v	k7c6
složených	složený	k2eAgNnPc6d1
jménech	jméno	k1gNnPc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
jako	jako	k9
samostatné	samostatný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
u	u	k7c2
příslušníků	příslušník	k1gMnPc2
některých	některý	k3yIgInPc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
například	například	k6eAd1
u	u	k7c2
Kazachů	Kazach	k1gMnPc2
nebo	nebo	k8xC
Uzbeků	Uzbek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
„	„	k?
<g/>
nur	nur	k?
<g/>
“	“	k?
běžně	běžně	k6eAd1
existuje	existovat	k5eAaImIp3nS
jako	jako	k9
součást	součást	k1gFnSc4
mnoha	mnoho	k4c2
složených	složený	k2eAgNnPc2d1
kazašských	kazašský	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
a	a	k8xC
sovětské	sovětský	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Nur-Sultanu	Nur-Sultan	k1gInSc2
založili	založit	k5eAaPmAgMnP
Rusové	Rus	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
vojenskou	vojenský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
o	o	k7c4
osm	osm	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
získala	získat	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Akmolinsk	Akmolinsk	k1gInSc1
a	a	k8xC
kolem	kolem	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
rozvíjet	rozvíjet	k5eAaImF
stejnojmenné	stejnojmenný	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1868	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
Kazachstán	Kazachstán	k1gInSc1
anektován	anektovat	k5eAaBmNgInS
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
město	město	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
administrativním	administrativní	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
již	již	k6eAd1
za	za	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
spolu	spolu	k6eAd1
s	s	k7c7
celou	celý	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
postiženo	postihnout	k5eAaPmNgNnS
hladomorem	hladomor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1939	#num#	k4
se	se	k3xPyFc4
Akmolinsk	Akmolinsk	k1gInSc1
stal	stát	k5eAaPmAgInS
střediskem	středisko	k1gNnSc7
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
zahájena	zahájit	k5eAaPmNgFnS
kampaň	kampaň	k1gFnSc1
obdělávání	obdělávání	k1gNnSc2
celin	celina	k1gFnPc2
a	a	k8xC
význam	význam	k1gInSc1
Akmolinska	Akmolinsko	k1gNnSc2
vzrostl	vzrůst	k5eAaPmAgInS
v	v	k7c6
nové	nový	k2eAgFnSc6d1
roli	role	k1gFnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
tvořeného	tvořený	k2eAgInSc2d1
pěti	pět	k4xCc7
severními	severní	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
Kazachstánu	Kazachstán	k1gInSc2
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
městské	městský	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
a	a	k8xC
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
vybavenosti	vybavenost	k1gFnSc2
města	město	k1gNnSc2
včetně	včetně	k7c2
otevření	otevření	k1gNnSc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1961	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
se	se	k3xPyFc4
město	město	k1gNnSc1
jmenovalo	jmenovat	k5eAaBmAgNnS,k5eAaImAgNnS
Celinograd	Celinograd	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Nezávislý	závislý	k2eNgInSc1d1
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Deklarace	deklarace	k1gFnSc1
o	o	k7c4
svrchovanosti	svrchovanost	k1gFnPc4
Kazašské	kazašský	k2eAgFnPc4d1
SSR	SSR	kA
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
25	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
1991	#num#	k4
byla	být	k5eAaImAgFnS
země	země	k1gFnSc1
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Republiku	republika	k1gFnSc4
Kazachstán	Kazachstán	k1gInSc1
a	a	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
,	,	kIx,
vyhlásila	vyhlásit	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Celinograd	Celinograd	k1gInSc1
tehdy	tehdy	k6eAd1
změnil	změnit	k5eAaPmAgInS
jméno	jméno	k1gNnSc4
na	na	k7c6
Akmolu	Akmol	k1gInSc6
a	a	k8xC
nadále	nadále	k6eAd1
byl	být	k5eAaImAgInS
administrativním	administrativní	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
nové	nový	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Kazachstánu	Kazachstán	k1gInSc2
byla	být	k5eAaImAgFnS
Akmola	Akmola	k1gFnSc1
prohlášena	prohlásit	k5eAaPmNgFnS
v	v	k7c6
červenci	červenec	k1gInSc6
1994	#num#	k4
namísto	namísto	k7c2
dosavadního	dosavadní	k2eAgNnSc2d1
Almaty	Almat	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
září	září	k1gNnSc6
1995	#num#	k4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
potvrdil	potvrdit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Nazarbajev	Nazarbajev	k1gFnSc2
svým	svůj	k3xOyFgInSc7
dekretem	dekret	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
slavnostnímu	slavnostní	k2eAgNnSc3d1
předání	předání	k1gNnSc3
funkce	funkce	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Akmole	Akmol	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
prezidentským	prezidentský	k2eAgInSc7d1
dekretem	dekret	k1gInSc7
ze	z	k7c2
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
byla	být	k5eAaImAgFnS
Akmola	Akmola	k1gFnSc1
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Astanu	Astana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
město	město	k1gNnSc1
zažívá	zažívat	k5eAaImIp3nS
nebývalý	nebývalý	k2eAgInSc4d1,k2eNgInSc4d1
rozmach	rozmach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
posledního	poslední	k2eAgNnSc2d1
desetiletí	desetiletí	k1gNnSc2
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS
a	a	k8xC
ve	v	k7c6
městě	město	k1gNnSc6
vyrostly	vyrůst	k5eAaPmAgFnP
desítky	desítka	k1gFnPc1
moderních	moderní	k2eAgFnPc2d1
kancelářských	kancelářský	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
nové	nový	k2eAgFnPc1d1
ulice	ulice	k1gFnPc1
<g/>
,	,	kIx,
parky	park	k1gInPc1
i	i	k8xC
rezidenční	rezidenční	k2eAgFnPc1d1
čtvrti	čtvrt	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
dnes	dnes	k6eAd1
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Nur-Sultan	Nur-Sultan	k1gInSc4
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
je	být	k5eAaImIp3nS
moderním	moderní	k2eAgInSc7d1
<g/>
,	,	kIx,
pečlivě	pečlivě	k6eAd1
plánovaným	plánovaný	k2eAgNnSc7d1
městem	město	k1gNnSc7
s	s	k7c7
výraznými	výrazný	k2eAgInPc7d1
prvky	prvek	k1gInPc7
socialistické	socialistický	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
však	však	k9
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
regionální	regionální	k2eAgInSc4d1
charakter	charakter	k1gInSc4
nebyla	být	k5eNaImAgFnS
ničím	ničí	k3xOyNgNnSc7
zvlášť	zvlášť	k9
výjimečný	výjimečný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Široké	Široké	k2eAgFnPc4d1
třídy	třída	k1gFnPc4
s	s	k7c7
vládními	vládní	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
<g/>
,	,	kIx,
bývalými	bývalý	k2eAgInPc7d1
stranickými	stranický	k2eAgInPc7d1
sekretariáty	sekretariát	k1gInPc7
a	a	k8xC
několika	několik	k4yIc7
kulturními	kulturní	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
lemují	lemovat	k5eAaImIp3nP
aleje	alej	k1gFnPc1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zde	zde	k6eAd1
hodně	hodně	k6eAd1
staví	stavit	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
vyrůstají	vyrůstat	k5eAaImIp3nP
moderní	moderní	k2eAgInPc1d1
prosklené	prosklený	k2eAgInPc1d1
mrakodrapy	mrakodrap	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výstavba	výstavba	k1gFnSc1
byla	být	k5eAaImAgFnS
svěřena	svěřit	k5eAaPmNgFnS
zahraničním	zahraniční	k2eAgInSc7d1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
tureckým	turecký	k2eAgFnPc3d1
firmám	firma	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
je	být	k5eAaImIp3nS
důležitým	důležitý	k2eAgNnSc7d1
průmyslovým	průmyslový	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc1d3
jsou	být	k5eAaImIp3nP
zdejší	zdejší	k2eAgInPc1d1
závody	závod	k1gInPc1
strojírenské	strojírenský	k2eAgInPc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c4
výrobu	výroba	k1gFnSc4
zemědělských	zemědělský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
potravinářské	potravinářský	k2eAgInPc4d1
závody	závod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
potravinářský	potravinářský	k2eAgInSc1d1
komplex	komplex	k1gInSc1
města	město	k1gNnSc2
představuje	představovat	k5eAaImIp3nS
pivovar	pivovar	k1gInSc1
postavený	postavený	k2eAgInSc1d1
na	na	k7c4
klíč	klíč	k1gInSc4
českou	český	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
VÚCHZ	VÚCHZ	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
současně	současně	k6eAd1
největší	veliký	k2eAgFnSc1d3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
rady	rada	k1gFnSc2
ministrů	ministr	k1gMnPc2
(	(	kIx(
<g/>
vlády	vláda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dvoukomorové	dvoukomorový	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
Kenges	Kenges	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
několik	několik	k4yIc4
velvyslanectví	velvyslanectví	k1gNnPc2
včetně	včetně	k7c2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc4
ambasády	ambasáda	k1gFnPc4
cizích	cizí	k2eAgInPc2d1
států	stát	k1gInPc2
však	však	k9
zůstaly	zůstat	k5eAaPmAgInP
v	v	k7c6
bývalém	bývalý	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
a	a	k8xC
největším	veliký	k2eAgNnSc6d3
městě	město	k1gNnSc6
Kazachstánu	Kazachstán	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c4
Almaty	Almat	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc4
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
na	na	k7c4
několik	několik	k4yIc4
různých	různý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
železniční	železniční	k2eAgFnSc2d1
trati	trať	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
protíná	protínat	k5eAaImIp3nS
Nur-Sultan	Nur-Sultan	k1gInSc4
ve	v	k7c6
směru	směr	k1gInSc6
východ-západ	východ-západ	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
průmyslové	průmyslový	k2eAgFnPc1d1
a	a	k8xC
chudší	chudý	k2eAgFnPc1d2
obytné	obytný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
železniční	železniční	k2eAgFnSc7d1
tratí	trať	k1gFnSc7
a	a	k8xC
řekou	řeka	k1gFnSc7
Išim	Išima	k1gFnPc2
je	být	k5eAaImIp3nS
centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
intenzivní	intenzivní	k2eAgFnSc1d1
stavební	stavební	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
na	na	k7c4
východ	východ	k1gInSc4
jsou	být	k5eAaImIp3nP
větší	veliký	k2eAgFnPc1d2
obytné	obytný	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
s	s	k7c7
parky	park	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
řeky	řeka	k1gFnSc2
Išim	Išim	k1gInSc1
jsou	být	k5eAaImIp3nP
nové	nový	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
mnoho	mnoho	k4c1
velkých	velký	k2eAgInPc2d1
stavebních	stavební	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
výstavba	výstavba	k1gFnSc1
diplomatických	diplomatický	k2eAgFnPc2d1
čtvrtí	čtvrt	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
různé	různý	k2eAgFnPc4d1
vládní	vládní	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2030	#num#	k4
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
tyto	tento	k3xDgFnPc1
čtvrti	čtvrt	k1gFnPc1
dokončeny	dokončit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInPc1d1
plány	plán	k1gInPc1
na	na	k7c4
nový	nový	k2eAgInSc4d1
Nur-Sultan	Nur-Sultan	k1gInSc4
navrhl	navrhnout	k5eAaPmAgMnS
japonský	japonský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Kišó	Kišó	k1gMnSc1
Kurokawa	Kurokawa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
také	také	k9
projektoval	projektovat	k5eAaBmAgMnS
nové	nový	k2eAgNnSc4d1
Astanské	Astanský	k2eAgNnSc4d1
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Panelová	panelový	k2eAgNnPc1d1
sídliště	sídliště	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zůstala	zůstat	k5eAaPmAgFnS
ze	z	k7c2
sovětské	sovětský	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
postupně	postupně	k6eAd1
odstraňována	odstraňován	k2eAgNnPc4d1
a	a	k8xC
nahrazována	nahrazován	k2eAgNnPc4d1
budovami	budova	k1gFnPc7
zcela	zcela	k6eAd1
nových	nový	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
stavebních	stavební	k2eAgFnPc2d1
prací	práce	k1gFnPc2
v	v	k7c6
celém	celý	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Nursultan	Nursultan	k1gInSc1
Nazarbajev	Nazarbajev	k1gFnSc7
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
architektuře	architektura	k1gFnSc3
Nur-Sultanu	Nur-Sultan	k1gInSc2
zvláštní	zvláštní	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
z	z	k7c2
nedávno	nedávno	k6eAd1
dokončených	dokončený	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
projektovali	projektovat	k5eAaBmAgMnP
mezinárodně	mezinárodně	k6eAd1
uznávaní	uznávaný	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
a	a	k8xC
návrháři	návrhář	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Kišó	Kišó	k1gMnSc1
Kurokawa	Kurokawa	k1gMnSc1
nebo	nebo	k8xC
Norman	Norman	k1gMnSc1
Foster	Foster	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
a	a	k8xC
pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
uvést	uvést	k5eAaPmF
zdroje	zdroj	k1gInPc4
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
a	a	k8xC
historické	historický	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
budete	být	k5eAaImBp2nP
v	v	k7c6
Nur-Sultanu	Nur-Sultan	k1gInSc6
hledat	hledat	k5eAaImF
marně	marně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nur-Sultan	Nur-Sultan	k1gInSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
jedno	jeden	k4xCgNnSc1
z	z	k7c2
měst	město	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
podepsal	podepsat	k5eAaPmAgInS
vliv	vliv	k1gInSc1
sovětské	sovětský	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převažuje	převažovat	k5eAaImIp3nS
architektonický	architektonický	k2eAgInSc4d1
styl	styl	k1gInSc4
z	z	k7c2
let	léto	k1gNnPc2
1960	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
čtvrtě	čtvrt	k1gFnPc1
byly	být	k5eAaImAgFnP
nově	nově	k6eAd1
postaveny	postavit	k5eAaPmNgInP
buď	buď	k8xC
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
byly	být	k5eAaImAgFnP
přebudovány	přebudovat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
městské	městský	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
jižně	jižně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Išim	Išim	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
začala	začít	k5eAaPmAgFnS
vznikat	vznikat	k5eAaImF
se	s	k7c7
změnou	změna	k1gFnSc7
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stavby	stavba	k1gFnPc1
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1
vládní	vládní	k2eAgFnPc1d1
čtvrtě	čtvrt	k1gFnPc1
</s>
<s>
Promenáda	promenáda	k1gFnSc1
kolem	kolem	k7c2
řeky	řeka	k1gFnSc2
Išim	Išima	k1gFnPc2
</s>
<s>
Mešita	mešita	k1gFnSc1
Nur-Sultan	Nur-Sultan	k1gInSc1
</s>
<s>
Oceanárium	Oceanárium	k1gNnSc1
(	(	kIx(
<g/>
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejvzdálenějších	vzdálený	k2eAgMnPc2d3
od	od	k7c2
mořského	mořský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
</s>
<s>
Věže	věž	k1gFnPc1
Hotelu	hotel	k1gInSc2
Intercontinental	Intercontinental	k1gMnSc1
</s>
<s>
Tržiště	tržiště	k1gNnSc1
</s>
<s>
Římsko-katolické	římsko-katolický	k2eAgFnPc1d1
katedrály	katedrála	k1gFnPc1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
pravoslavný	pravoslavný	k2eAgInSc1d1
chrám	chrám	k1gInSc1
</s>
<s>
Věž	věž	k1gFnSc1
Bajterek	Bajterka	k1gFnPc2
</s>
<s>
Palác	palác	k1gInSc1
míru	mír	k1gInSc2
a	a	k8xC
harmonie	harmonie	k1gFnSc2
</s>
<s>
Khan	Khan	k1gInSc1
Shatyry	Shatyra	k1gFnSc2
</s>
<s>
Grand	grand	k1gMnSc1
Alatau	Alataus	k1gInSc2
</s>
<s>
Nové	Nové	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
</s>
<s>
Palác	palác	k1gInSc1
míru	mír	k1gInSc2
a	a	k8xC
harmonie	harmonie	k1gFnSc2
</s>
<s>
Pyramida	pyramida	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
navrhl	navrhnout	k5eAaPmAgMnS
architekt	architekt	k1gMnSc1
Norman	Norman	k1gMnSc1
Foster	Foster	k1gMnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
slavnostně	slavnostně	k6eAd1
otevřena	otevřít	k5eAaPmNgFnS
v	v	k7c6
září	září	k1gNnSc6
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytuje	poskytovat	k5eAaImIp3nS
prostor	prostor	k1gInSc4
pro	pro	k7c4
různá	různý	k2eAgNnPc4d1
náboženství	náboženství	k1gNnPc4
<g/>
:	:	kIx,
judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
buddhismus	buddhismus	k1gInSc1
<g/>
,	,	kIx,
<g/>
hinduismus	hinduismus	k1gInSc1
<g/>
,	,	kIx,
taoismus	taoismus	k1gInSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
operní	operní	k2eAgInSc1d1
dům	dům	k1gInSc1
s	s	k7c7
1	#num#	k4
500	#num#	k4
sedadly	sedadlo	k1gNnPc7
<g/>
,	,	kIx,
muzeum	muzeum	k1gNnSc1
národní	národní	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
novou	nový	k2eAgFnSc4d1
"	"	kIx"
<g/>
univerzitu	univerzita	k1gFnSc4
civilizace	civilizace	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
knihovnu	knihovna	k1gFnSc4
a	a	k8xC
výzkumné	výzkumný	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
pro	pro	k7c4
etnické	etnický	k2eAgFnPc4d1
a	a	k8xC
geografické	geografický	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
Kazachstánu	Kazachstán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pyramidě	pyramida	k1gFnSc6
se	s	k7c7
základnou	základna	k1gFnSc7
62	#num#	k4
×	×	k?
62	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
vysoké	vysoký	k2eAgFnSc6d1
77	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
koncipována	koncipovat	k5eAaBmNgFnS
jako	jako	k8xC,k8xS
globální	globální	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
pro	pro	k7c4
náboženské	náboženský	k2eAgNnSc4d1
porozumění	porozumění	k1gNnSc4
<g/>
,	,	kIx,
zřeknutí	zřeknutí	k1gNnSc1
se	se	k3xPyFc4
násilí	násilí	k1gNnSc1
a	a	k8xC
prosazování	prosazování	k1gNnSc1
víry	víra	k1gFnSc2
a	a	k8xC
lidské	lidský	k2eAgFnSc2d1
rovnosti	rovnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyramida	pyramida	k1gFnSc1
míru	mír	k1gInSc2
vyjadřuje	vyjadřovat	k5eAaImIp3nS
ducha	duch	k1gMnSc4
Kazachstánu	Kazachstán	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
tradice	tradice	k1gFnSc1
a	a	k8xC
zástupci	zástupce	k1gMnPc1
různých	různý	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
žijí	žít	k5eAaImIp3nP
v	v	k7c6
harmonii	harmonie	k1gFnSc6
a	a	k8xC
souladu	soulad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
za	za	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
se	se	k3xPyFc4
zde	zde	k6eAd1
bude	být	k5eAaImBp3nS
v	v	k7c6
kruhové	kruhový	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
scházet	scházet	k5eAaImF
200	#num#	k4
delegátů	delegát	k1gMnPc2
světových	světový	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
a	a	k8xC
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyramida	pyramida	k1gFnSc1
je	být	k5eAaImIp3nS
77	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoký	k2eAgFnSc1d1
(	(	kIx(
<g/>
včetně	včetně	k7c2
15	#num#	k4
metrů	metr	k1gInPc2
vysokého	vysoký	k2eAgInSc2d1
podstavce	podstavec	k1gInSc2
zakrytého	zakrytý	k2eAgInSc2d1
zeminou	zemina	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
stála	stát	k5eAaImAgFnS
8,74	8,74	k4
bilionů	bilion	k4xCgInPc2
kazachstánských	kazachstánský	k2eAgInPc2d1
tenge	tenge	k1gInPc2
(	(	kIx(
<g/>
okolo	okolo	k7c2
58	#num#	k4
milionů	milion	k4xCgInPc2
USD	USD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
a	a	k8xC
sponzoroval	sponzorovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
emír	emír	k1gMnSc1
Kataru	katar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
mešity	mešita	k1gFnSc2
<g/>
,	,	kIx,
madrasy	madras	k1gInPc4
a	a	k8xC
knihovny	knihovna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mešita	mešita	k1gFnSc1
má	mít	k5eAaImIp3nS
4	#num#	k4
minarety	minaret	k1gInPc7
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
o	o	k7c6
výšce	výška	k1gFnSc6
63	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
její	její	k3xOp3gFnSc1
kapacita	kapacita	k1gFnSc1
je	být	k5eAaImIp3nS
5	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kopule	kopule	k1gFnSc1
mešity	mešita	k1gFnSc2
měří	měřit	k5eAaImIp3nS
43	#num#	k4
m.	m.	k?
</s>
<s>
Chan	Chan	k1gMnSc1
Šatyr	Šatyr	k1gMnSc1
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2006	#num#	k4
představil	představit	k5eAaPmAgMnS
kazachstánský	kazachstánský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Nursultan	Nursultan	k1gInSc4
Nazarbajev	Nazarbajev	k1gFnPc3
plány	plán	k1gInPc4
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
nákupního	nákupní	k2eAgNnSc2d1
a	a	k8xC
zábavního	zábavní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
Chan	Chan	k1gMnSc1
Šatyr	Šatyr	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
obřího	obří	k2eAgInSc2d1
průhledného	průhledný	k2eAgInSc2d1
stanu	stan	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
150	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoká	k1gFnSc1
a	a	k8xC
navrhl	navrhnout	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
britský	britský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Norman	Norman	k1gMnSc1
Foster	Foster	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
interiérů	interiér	k1gInPc2
centra	centrum	k1gNnSc2
činí	činit	k5eAaImIp3nP
100	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
plášť	plášť	k1gInSc1
stavby	stavba	k1gFnSc2
je	být	k5eAaImIp3nS
konstruován	konstruován	k2eAgInSc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
propouštěl	propouštět	k5eAaImAgMnS
světlo	světlo	k1gNnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
aby	aby	kYmCp3nS
odolal	odolat	k5eAaPmAgInS
extrémním	extrémní	k2eAgNnPc3d1
výkyvům	výkyv	k1gInPc3
venkovních	venkovní	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
kazachstánské	kazachstánský	k2eAgFnSc6d1
metropoli	metropol	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chan	Chan	k1gNnSc1
Šatyr	Šatyra	k1gFnPc2
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
stavbou	stavba	k1gFnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
stanu	stan	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákupní	nákupní	k2eAgInSc4d1
centrum	centrum	k1gNnSc1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ak	Ak	k?
Orda	orda	k1gFnSc1
–	–	k?
Prezidentský	prezidentský	k2eAgInSc1d1
palác	palác	k1gInSc1
</s>
<s>
Ak	Ak	k?
Orda	orda	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
kazaštině	kazaština	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
ruské	ruský	k2eAgFnSc6d1
transkripci	transkripce	k1gFnSc6
А	А	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
Akorda	Akorda	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
z	z	k7c2
kazaštiny	kazaština	k1gFnSc2
doslovně	doslovně	k6eAd1
„	„	k?
<g/>
Bílá	bílý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
ve	v	k7c6
středověku	středověk	k1gInSc6
název	název	k1gInSc1
západní	západní	k2eAgFnSc2d1
části	část	k1gFnSc2
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
<g/>
,	,	kIx,
ustavené	ustavený	k2eAgFnSc2d1
mongolským	mongolský	k2eAgMnSc7d1
chánem	chán	k1gMnSc7
Bátúem	Bátú	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
rezidence	rezidence	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Kazachstánu	Kazachstán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentský	prezidentský	k2eAgInSc1d1
palác	palác	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Išim	Išima	k1gFnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
bulváru	bulvár	k1gInSc2
Nuržol	Nuržola	k1gFnPc2
jako	jako	k8xC,k8xS
pětipatrová	pětipatrový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
z	z	k7c2
monolitického	monolitický	k2eAgInSc2d1
betonu	beton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obklad	obklad	k1gInSc1
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
z	z	k7c2
20	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
cm	cm	kA
silného	silný	k2eAgInSc2d1
italského	italský	k2eAgInSc2d1
mramoru	mramor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výška	výška	k1gFnSc1
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
80	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
celková	celkový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
36	#num#	k4
720	#num#	k4
čtverečných	čtverečný	k2eAgInPc2d1
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ak	Ak	k1gFnSc1
Orda	orda	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
jurtu	jurta	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
haly	hala	k1gFnPc4
navržené	navržený	k2eAgFnPc4d1
z	z	k7c2
mramoru	mramor	k1gInSc2
a	a	k8xC
žuly	žula	k1gFnSc2
<g/>
,	,	kIx,
mramorový	mramorový	k2eAgInSc1d1
sál	sál	k1gInSc1
pro	pro	k7c4
schůzky	schůzka	k1gFnPc4
a	a	k8xC
oficiální	oficiální	k2eAgFnPc4d1
návštěvy	návštěva	k1gFnPc4
zástupců	zástupce	k1gMnPc2
zahraničních	zahraniční	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
zlatý	zlatý	k2eAgInSc1d1
sál	sál	k1gInSc1
pro	pro	k7c4
soukromé	soukromý	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
a	a	k8xC
diskuse	diskuse	k1gFnPc1
mezi	mezi	k7c7
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
a	a	k8xC
představiteli	představitel	k1gMnPc7
jiných	jiný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palác	palác	k1gInSc1
Ak	Ak	k1gFnSc2
Orda	orda	k1gFnSc1
byl	být	k5eAaImAgInS
zařazen	zařazen	k2eAgInSc1d1
mezi	mezi	k7c4
desítku	desítka	k1gFnSc4
nejkrásnějších	krásný	k2eAgFnPc2d3
prezidentských	prezidentský	k2eAgFnPc2d1
rezidencí	rezidence	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muzea	muzeum	k1gNnSc2
</s>
<s>
Prezidentské	prezidentský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
kultury	kultura	k1gFnSc2
</s>
<s>
Mauzoleum	mauzoleum	k1gNnSc1
Kabanbay	Kabanbaa	k1gFnSc2
Batyr	Batyra	k1gFnPc2
</s>
<s>
Etický	etický	k2eAgInSc1d1
památník	památník	k1gInSc1
komplex	komplex	k1gInSc1
"	"	kIx"
<g/>
Mapa	mapa	k1gFnSc1
Kazachstánu	Kazachstán	k1gInSc2
<g/>
"	"	kIx"
Atameken	Atameken	k1gInSc1
</s>
<s>
S.	S.	kA
Seifullin	Seifullin	k1gInSc1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Muzeum	muzeum	k1gNnSc4
prvního	první	k4xOgMnSc2
prezidenta	prezident	k1gMnSc2
Republiky	republika	k1gFnSc2
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Divadla	divadlo	k1gNnSc2
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
činoherní	činoherní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Gorkij	Gorkij	k1gMnSc1
</s>
<s>
Kazašské	kazašský	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
K.	K.	kA
Kuanyshbaev	Kuanyshbavo	k1gNnPc2
</s>
<s>
Národní	národní	k2eAgFnSc1d1
opera	opera	k1gFnSc1
a	a	k8xC
balet	balet	k1gInSc4
K.	K.	kA
Bajseitova	Bajseitův	k2eAgFnSc1d1
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Pomník	pomník	k1gInSc1
Otana	Otan	k1gMnSc2
Korgaushylara	Korgaushylar	k1gMnSc2
</s>
<s>
Památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
politických	politický	k2eAgFnPc2d1
represí	represe	k1gFnPc2
</s>
<s>
Památník	památník	k1gInSc1
Kazachstánských	kazachstánský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
v	v	k7c6
Afghánské	afghánský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
</s>
<s>
Centrální	centrální	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
s	s	k7c7
kašnou	kašna	k1gFnSc7
a	a	k8xC
sousoším	sousoší	k1gNnSc7
</s>
<s>
Památník	památník	k1gInSc1
Kenesary	Kenesara	k1gFnSc2
Khan	Khana	k1gFnPc2
</s>
<s>
Architektonické	architektonický	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Kostel	kostel	k1gInSc1
Konstantina	Konstantin	k1gMnSc2
a	a	k8xC
Jelena	Jelen	k1gMnSc2
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
mešita	mešita	k1gFnSc1
</s>
<s>
Bajterek	Bajterka	k1gFnPc2
</s>
<s>
Bajterek	Bajterka	k1gFnPc2
je	být	k5eAaImIp3nS
nejslavnější	slavný	k2eAgInSc1d3
orientační	orientační	k2eAgInSc1d1
bod	bod	k1gInSc1
v	v	k7c6
Nur-Sultanu	Nur-Sultan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
věž	věž	k1gFnSc1
má	mít	k5eAaImIp3nS
představovat	představovat	k5eAaImF
topol	topol	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
kouzelný	kouzelný	k2eAgMnSc1d1
pták	pták	k1gMnSc1
Samuruk	Samuruk	k1gMnSc1
položil	položit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
vejce	vejce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
kouli	koule	k1gFnSc4
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Bajtereku	Bajterek	k1gInSc2
je	být	k5eAaImIp3nS
otisk	otisk	k1gInSc1
ruky	ruka	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Kazachstánu	Kazachstán	k1gInSc2
Nursultana	Nursultan	k1gMnSc4
Nazarbajeva	Nazarbajev	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Mrakodrapy	mrakodrap	k1gInPc1
v	v	k7c6
Nur-Sultanu	Nur-Sultan	k1gInSc6
</s>
<s>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Astana	Astana	k1gFnSc1
Architecture	Architectur	k1gMnSc5
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
jpgModerní	jpgModerní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Astana	Astana	k1gFnSc1
<g/>
,	,	kIx,
capital	capital	k1gMnSc1
of	of	k?
Kazakhstan	Kazakhstan	k1gInSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
jpgMešita	jpgMešit	k2eAgFnSc1d1
</s>
<s>
Nazarbajevova	Nazarbajevův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
Evropsko-asijská	evropsko-asijský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
L.	L.	kA
N.	N.	kA
Gumiljovova	Gumiljovův	k2eAgMnSc4d1
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
a	a	k8xC
infrastruktura	infrastruktura	k1gFnSc1
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
žije	žít	k5eAaImIp3nS
především	především	k9
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
souvisejících	související	k2eAgFnPc2d1
činností	činnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
hraje	hrát	k5eAaImIp3nS
také	také	k9
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
významným	významný	k2eAgNnSc7d1
odvětvím	odvětví	k1gNnSc7
je	být	k5eAaImIp3nS
zpracování	zpracování	k1gNnSc4
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolí	okolí	k1gNnSc1
města	město	k1gNnSc2
má	mít	k5eAaImIp3nS
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
využití	využití	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
leží	ležet	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
Kazachstánu	Kazachstán	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
má	mít	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
dopravního	dopravní	k2eAgInSc2d1
uzlu	uzel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
velká	velký	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
Astanského	Astanský	k2eAgNnSc2d1
mezinárodního	mezinárodní	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
(	(	kIx(
<g/>
IATA	IATA	kA
<g/>
:	:	kIx,
TSE	TSE	kA
<g/>
,	,	kIx,
ICAO	ICAO	kA
<g/>
:	:	kIx,
UACC	UACC	kA
<g/>
)	)	kIx)
podle	podle	k7c2
projektu	projekt	k1gInSc2
japonského	japonský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Kišo	Kišo	k1gMnSc1
Kurokawy	Kurokawa	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nur-Sultanské	Nur-Sultanský	k2eAgNnSc1d1
vlakové	vlakový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
je	být	k5eAaImIp3nS
významným	významný	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
pro	pro	k7c4
severní	severní	k2eAgInSc4d1
Kazachstán	Kazachstán	k1gInSc4
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc1
zajišťují	zajišťovat	k5eAaImIp3nP
dopravu	doprava	k1gFnSc4
do	do	k7c2
většiny	většina	k1gFnSc2
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
Talgo	Talgo	k6eAd1
Express	express	k1gInSc1
do	do	k7c2
Almaty	Almata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInPc1d1
spoje	spoj	k1gInPc1
do	do	k7c2
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
a	a	k8xC
Uzbekistánu	Uzbekistán	k1gInSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
a	a	k8xC
na	na	k7c4
Ukrajinu	Ukrajina	k1gFnSc4
jezdí	jezdit	k5eAaImIp3nP
většinou	většinou	k6eAd1
jednou	jeden	k4xCgFnSc7
týdně	týdně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
léta	léto	k1gNnSc2
2008	#num#	k4
nabízí	nabízet	k5eAaImIp3nS
jízdní	jízdní	k2eAgInSc4d1
řád	řád	k1gInSc4
také	také	k9
přímé	přímý	k2eAgNnSc1d1
vlakové	vlakový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
do	do	k7c2
Čínské	čínský	k2eAgInPc1d1
Urumči	Urumč	k1gInPc7
(	(	kIx(
<g/>
v	v	k7c6
autonomní	autonomní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Sin-ťiang	Sin-ťiang	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
se	se	k3xPyFc4
populace	populace	k1gFnSc1
Nur-Sultanu	Nur-Sultan	k1gInSc2
více	hodně	k6eAd2
než	než	k8xS
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS
<g/>
,	,	kIx,
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
600	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
s	s	k7c7
očekávaným	očekávaný	k2eAgInSc7d1
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
1	#num#	k4
milion	milion	k4xCgInSc4
v	v	k7c6
roce	rok	k1gInSc6
2030	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Migrující	migrující	k2eAgFnSc1d1
pracovní	pracovní	k2eAgFnSc1d1
síla	síla	k1gFnSc1
–	–	k?
legální	legální	k2eAgInSc1d1
i	i	k8xC
nelegální	legální	k2eNgInSc1d1
–	–	k?
přichází	přicházet	k5eAaImIp3nS
z	z	k7c2
celého	celý	k2eAgInSc2d1
Kazachstánu	Kazachstán	k1gInSc2
a	a	k8xC
sousedních	sousední	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Uzbekistán	Uzbekistán	k1gInSc1
a	a	k8xC
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nur-Sultan	Nur-Sultan	k1gInSc1
je	být	k5eAaImIp3nS
magnetem	magnet	k1gInSc7
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1
odborníky	odborník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
chtějí	chtít	k5eAaImIp3nP
budovat	budovat	k5eAaImF
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
změnilo	změnit	k5eAaPmAgNnS
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
města	město	k1gNnSc2
a	a	k8xC
přineslo	přinést	k5eAaPmAgNnS
více	hodně	k6eAd2
kazašského	kazašský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
dříve	dříve	k6eAd2
mělo	mít	k5eAaImAgNnS
většinu	většina	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
ze	z	k7c2
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Nur-Sultanu	Nur-Sultan	k1gInSc6
vzrostl	vzrůst	k5eAaPmAgInS
počet	počet	k1gInSc1
kazašského	kazašský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
na	na	k7c4
zhruba	zhruba	k6eAd1
60	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
oproti	oproti	k7c3
17	#num#	k4
%	%	kIx~
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
měl	mít	k5eAaImAgInS
Nur-Sultan	Nur-Sultan	k1gInSc1
281	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
30	#num#	k4
%	%	kIx~
kazašského	kazašský	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
70	#num#	k4
%	%	kIx~
národnosti	národnost	k1gFnSc2
ruské	ruský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ukrajinské	ukrajinský	k2eAgFnSc2d1
<g/>
,	,	kIx,
německé	německý	k2eAgFnSc2d1
a	a	k8xC
další	další	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
toto	tento	k3xDgNnSc4
argumentují	argumentovat	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
cesta	cesta	k1gFnSc1
k	k	k7c3
přilákání	přilákání	k1gNnSc3
etnických	etnický	k2eAgMnPc2d1
Kazachů	Kazach	k1gMnPc2
na	na	k7c4
sever	sever	k1gInSc4
byla	být	k5eAaImAgFnS
klíčovým	klíčový	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
při	při	k7c6
přesunu	přesun	k1gInSc6
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
má	mít	k5eAaImIp3nS
Nur-Sultan	Nur-Sultan	k1gInSc4
kolem	kolem	k7c2
750	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
nárůst	nárůst	k1gInSc4
okolo	okolo	k7c2
200	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
za	za	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
40,5	40,5	k4
%	%	kIx~
populace	populace	k1gFnSc2
ruského	ruský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
5,7	5,7	k4
%	%	kIx~
Ukrajinců	Ukrajinec	k1gMnPc2
<g/>
,	,	kIx,
3,0	3,0	k4
%	%	kIx~
Němců	Němec	k1gMnPc2
<g/>
,	,	kIx,
2,6	2,6	k4
%	%	kIx~
Tatarů	Tatar	k1gMnPc2
<g/>
,	,	kIx,
1,8	1,8	k4
%	%	kIx~
Bělorusů	Bělorus	k1gMnPc2
a	a	k8xC
0,8	0,8	k4
%	%	kIx~
Poláků	Polák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
41,8	41,8	k4
%	%	kIx~
Kazachů	Kazach	k1gMnPc2
již	již	k6eAd1
mělo	mít	k5eAaImAgNnS
početní	početní	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
nad	nad	k7c4
Rusy	Rus	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
tvořili	tvořit	k5eAaImAgMnP
největší	veliký	k2eAgFnSc4d3
etnickou	etnický	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
Ingušové	Inguš	k1gMnPc1
a	a	k8xC
Korejci	Korejec	k1gMnPc1
představovali	představovat	k5eAaImAgMnP
0,6	0,6	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k2eAgMnPc1d1
<g/>
,	,	kIx,
hlavně	hlavně	k9
Uzbekové	Uzbek	k1gMnPc1
<g/>
,	,	kIx,
tvořili	tvořit	k5eAaImAgMnP
3,8	3,8	k4
%	%	kIx~
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Zima	zima	k1gFnSc1
v	v	k7c6
Nur-Sultanu	Nur-Sultan	k1gInSc6
<g/>
,	,	kIx,
leden	leden	k1gInSc1
2006	#num#	k4
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
Akmolské	Akmolský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Kazašské	kazašský	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
na	na	k7c6
řece	řeka	k1gFnSc6
Išim	Išima	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
křižovatce	křižovatka	k1gFnSc6
transkazachstánské	transkazachstánský	k2eAgFnSc2d1
a	a	k8xC
jihosibiřské	jihosibiřský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
,	,	kIx,
asi	asi	k9
850	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
bývalého	bývalý	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Kazachstánu	Kazachstán	k1gInSc2
Almaty	Almata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
zeměpisná	zeměpisný	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
je	být	k5eAaImIp3nS
51	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
'	'	kIx"
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
a	a	k8xC
71	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
'	'	kIx"
východní	východní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Prahy	Praha	k1gFnSc2
je	být	k5eAaImIp3nS
vzdálena	vzdálit	k5eAaPmNgFnS
přibližně	přibližně	k6eAd1
4	#num#	k4
400	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
města	město	k1gNnSc2
dosahuje	dosahovat	k5eAaImIp3nS
400	#num#	k4
m.	m.	k?
Nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
<g/>
,	,	kIx,
hladina	hladina	k1gFnSc1
řeky	řeka	k1gFnSc2
Išim	Išima	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
380	#num#	k4
m	m	kA
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
výšce	výška	k1gFnSc6
430	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
denní	denní	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
vzduchu	vzduch	k1gInSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
−	−	k?
°	°	k?
<g/>
C	C	kA
v	v	k7c6
lednu	leden	k1gInSc6
a	a	k8xC
20	#num#	k4
°	°	k?
<g/>
C	C	kA
v	v	k7c6
červenci	červenec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdeštivější	deštivý	k2eAgInSc4d3
měsíc	měsíc	k1gInSc4
je	být	k5eAaImIp3nS
červenec	červenec	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
spadne	spadnout	k5eAaPmIp3nS
okolo	okolo	k7c2
50	#num#	k4
mm	mm	kA
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
podnebi	podnebi	k1gNnSc1
th	th	k?
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
90	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
podnebi	podnebi	k1gNnSc1
td	td	k?
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
</s>
<s>
Nur-Sultan	Nur-Sultan	k1gInSc1
–	–	k?
podnebí	podnebí	k1gNnSc2
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
leden	leden	k1gInSc1
</s>
<s>
únor	únor	k1gInSc1
</s>
<s>
březen	březen	k1gInSc1
</s>
<s>
duben	duben	k1gInSc1
</s>
<s>
květen	květen	k1gInSc4
</s>
<s>
červen	červen	k1gInSc1
</s>
<s>
červenec	červenec	k1gInSc1
</s>
<s>
srpen	srpen	k1gInSc1
</s>
<s>
září	zářit	k5eAaImIp3nS
</s>
<s>
říjen	říjen	k1gInSc1
</s>
<s>
listopad	listopad	k1gInSc1
</s>
<s>
prosinec	prosinec	k1gInSc1
</s>
<s>
rok	rok	k1gInSc4
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1
denní	denní	k2eAgNnSc1d1
maximum	maximum	k1gNnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
9	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
7	#num#	k4
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
3	#num#	k4
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1
denní	denní	k2eAgNnSc1d1
minimum	minimum	k1gNnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
5	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
[	[	kIx(
<g/>
mm	mm	kA
<g/>
]	]	kIx)
</s>
<s>
22	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
327	#num#	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
Pogoda	Pogoda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Ammán	Ammán	k1gInSc1
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ankara	Ankara	k1gFnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bangkok	Bangkok	k1gInSc1
<g/>
,	,	kIx,
Thajsko	Thajsko	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Biškek	Biškek	k1gInSc1
<g/>
,	,	kIx,
Kyrgyzstán	Kyrgyzstán	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gdaňsk	Gdaňsk	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hanoj	Hanoj	k1gFnSc1
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Smyrna	Smyrna	k1gFnSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kazaň	Kazaň	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Manila	Manila	k1gFnSc1
<g/>
,	,	kIx,
Filipíny	Filipíny	k1gFnPc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nice	Nice	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oulu	Oulu	k6eAd1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Riga	Riga	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soul	Soul	k1gInSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tbilisi	Tbilisi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ufa	Ufa	k?
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Záhřeb	Záhřeb	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
působí	působit	k5eAaImIp3nS
profesionální	profesionální	k2eAgFnSc1d1
cyklistická	cyklistický	k2eAgFnSc1d1
stáj	stáj	k1gFnSc1
Astana	Astana	k1gFnSc1
Team	team	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barys	Barysa	k1gFnPc2
Astana	Astana	k1gFnSc1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
Kontinentální	kontinentální	k2eAgFnSc6d1
hokejové	hokejový	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc3d3
ruské	ruský	k2eAgFnSc3d1
lize	liga	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Astana	Astana	k1gFnSc1
letošní	letošní	k2eAgFnSc4d1
sezonu	sezona	k1gFnSc4
2015-2016	2015-2016	k4
hraje	hrát	k5eAaImIp3nS
UEFA	UEFA	kA
Champions	Champions	k1gInSc1
League	League	k1gNnSc2
a	a	k8xC
jako	jako	k9
stadion	stadion	k1gInSc4
mají	mít	k5eAaImIp3nP
Astana	Astana	k1gFnSc1
Arena	Arena	k1gFnSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
30	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
KlubSportLigaStadion	KlubSportLigaStadion	k1gInSc1
</s>
<s>
Lokomotive	Lokomotiv	k1gInSc5
Astana	Astan	k1gMnSc4
</s>
<s>
Fotbal	fotbal	k1gInSc1
</s>
<s>
Premjer	Premjer	k1gInSc1
Ligasy	Ligasa	k1gFnSc2
</s>
<s>
Kazhimukan	Kazhimukan	k1gInSc1
Munaitpasov	Munaitpasov	k1gInSc1
Stadion	stadion	k1gInSc1
</s>
<s>
FK	FK	kA
Astana	Astana	k1gFnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
</s>
<s>
Premjer	Premjer	k1gInSc1
Ligasy	Ligasa	k1gFnSc2
</s>
<s>
Astana	Astana	k1gFnSc1
Arena	Aren	k1gInSc2
</s>
<s>
Team	team	k1gInSc1
Astana	Astan	k1gMnSc2
</s>
<s>
Cyklistika	cyklistika	k1gFnSc1
</s>
<s>
UCI	UCI	kA
ProTour	ProTour	k1gMnSc1
</s>
<s>
-	-	kIx~
</s>
<s>
Astana	Astana	k1gFnSc1
Tigers	Tigersa	k1gFnPc2
</s>
<s>
Basketball	Basketball	k1gMnSc1
</s>
<s>
Kazašská	kazašský	k2eAgFnSc1d1
Basketbalová	basketbalový	k2eAgFnSc1d1
Liga	liga	k1gFnSc1
</s>
<s>
Barys	Barys	k6eAd1
Nur-Sultan	Nur-Sultan	k1gInSc1
</s>
<s>
Hokej	hokej	k1gInSc1
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
Hokejová	hokejový	k2eAgFnSc1d1
Liga	liga	k1gFnSc1
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1
Palác	palác	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
K	k	k7c3
přejmenování	přejmenování	k1gNnSc3
z	z	k7c2
Astany	Astana	k1gFnSc2
na	na	k7c4
Nur-Sultan	Nur-Sultan	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
О	О	k?
п	п	k?
г	г	k?
А	А	k?
—	—	k?
с	с	k?
Р	Р	k?
К	К	k?
в	в	k?
г	г	k?
Н	Н	k?
—	—	k?
с	с	k?
Р	Р	k?
К	К	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akorda	Akorda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
kz	kz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WELLE	WELLE	kA
(	(	kIx(
<g/>
WWW.DW.COM	WWW.DW.COM	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Deutsche	Deutsche	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nursultan	Nursultan	k1gInSc1
<g/>
,	,	kIx,
not	nota	k1gFnPc2
Astana	Astana	k1gFnSc1
—	—	k?
Kazakhstan	Kazakhstan	k1gInSc1
renames	renames	k1gInSc1
capital	capitat	k5eAaPmAgInS,k5eAaImAgInS
to	ten	k3xDgNnSc1
honor	honor	k1gInSc1
Nazarbayev	Nazarbayev	k1gFnSc1
|	|	kIx~
DW	DW	kA
|	|	kIx~
20.03	20.03	k4
<g/>
.2019	.2019	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DW	DW	kA
<g/>
.	.	kIx.
<g/>
COM	COM	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://nationalpost.com/pmn/news-pmn/the-latest-kazakh-capital-renamed-after-outgoing-president1	https://nationalpost.com/pmn/news-pmn/the-latest-kazakh-capital-renamed-after-outgoing-president1	k4
2	#num#	k4
О	О	k?
в	в	k?
и	и	k?
в	в	k?
К	К	k?
Р	Р	k?
К	К	k?
<g/>
.	.	kIx.
З	З	k?
Р	Р	k?
К	К	k?
о	о	k?
23	#num#	k4
м	м	k?
2019	#num#	k4
г	г	k?
№	№	k?
238-VІ	238-VІ	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nur-Sultan	Nur-Sultan	k1gInSc1
(	(	kIx(
<g/>
Astana	Astana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
М	М	k?
ю	ю	k?
Р	Р	k?
К	К	k?
<g/>
:	:	kIx,
И	И	k?
с	с	k?
н	н	k?
п	п	k?
а	а	k?
Р	Р	k?
К	К	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zúrodnění	zúrodnění	k1gNnSc2
celin	celina	k1gFnPc2
<g/>
....	....	k?
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1961	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Т	Т	k?
п	п	k?
п	п	k?
А	А	k?
в	в	k?
Н	Н	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zakon	zakon	k1gInSc1
<g/>
.	.	kIx.
<g/>
kz	kz	k?
<g/>
,	,	kIx,
2019-03-20	2019-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nursultan	Nursultan	k1gInSc1
<g/>
:	:	kIx,
Kazakhstan	Kazakhstan	k1gInSc1
renames	renames	k1gMnSc1
capital	capital	k1gMnSc1
Astana	Astan	k1gMnSc2
after	after	k1gMnSc1
ex-president	ex-president	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
<g/>
,	,	kIx,
2019-03-20	2019-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KARIMOVOVÁ	KARIMOVOVÁ	kA
<g/>
,	,	kIx,
Džamila	Džamila	k1gFnSc1
<g/>
.	.	kIx.
В	В	k?
М	М	k?
о	о	k?
м	м	k?
п	п	k?
с	с	k?
<g/>
.	.	kIx.
lsm	lsm	k?
<g/>
.	.	kIx.
<g/>
kz	kz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lsm	lsm	k?
<g/>
.	.	kIx.
<g/>
kz	kz	k?
<g/>
,	,	kIx,
2019-03-20	2019-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
В	В	k?
<g/>
,	,	kIx,
Т	Т	k?
<g/>
.	.	kIx.
П	П	k?
о	о	k?
п	п	k?
в	в	k?
К	К	k?
о	о	k?
п	п	k?
А	А	k?
<g/>
:	:	kIx,
А	А	k?
п	п	k?
в	в	k?
Н	Н	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vл	Vл	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-20	2019-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
К	К	k?
Р	Р	k?
К	К	k?
<g/>
.	.	kIx.
Р	Р	k?
I.	I.	kA
О	О	k?
п	п	k?
<g/>
.	.	kIx.
С	С	k?
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
П	П	k?
в	в	k?
К	К	k?
о	о	k?
п	п	k?
с	с	k?
в	в	k?
з	з	k?
–	–	k?
М	М	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vл	Vл	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-22	2019-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ч	Ч	k?
о	о	k?
«	«	k?
<g/>
Н	Н	k?
<g/>
»	»	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
Н	Н	k?
К	К	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-22	2019-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Т	Т	k?
ц	ц	k?
«	«	k?
<g/>
Х	Х	k?
Ш	Ш	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CityLife	CityLif	k1gInSc5
<g/>
.	.	kIx.
<g/>
kz	kz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Khan	Khan	k1gMnSc1
Shatyr	Shatyr	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
nákupního	nákupní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
kazašsky	kazašsky	k6eAd1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
А	А	k?
О	О	k?
в	в	k?
в	в	k?
т	т	k?
<g/>
10	#num#	k4
с	с	k?
к	к	k?
п	п	k?
д	д	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kazakhstan	Kazakhstan	k1gInSc1
News	News	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-02	2015-07-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Nur-Sultan	Nur-Sultan	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nur-Sultan	Nur-Sultan	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Astana	Astana	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kazachstán	Kazachstán	k1gInSc1
–	–	k?
Қ	Қ	k?
–	–	k?
(	(	kIx(
<g/>
KZ	KZ	kA
<g/>
)	)	kIx)
Oblasti	oblast	k1gFnPc1
(	(	kIx(
<g/>
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
správní	správní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Akmolská	Akmolský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Kokčetau	Kokčetaus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Aktobská	Aktobský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Aktobe	Aktob	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Almatinská	Almatinský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Taldykorgan	Taldykorgan	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Atyrauská	Atyrauský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Atyrau	Atyraus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Turkestánská	turkestánský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Turkestán	Turkestán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Karagandská	Karagandský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Karaganda	Karaganda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kostanajská	Kostanajský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Kostanaj	Kostanaj	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kyzylordská	Kyzylordský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Kyzylorda	Kyzylorda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Mangystauská	Mangystauský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Aktau	Aktaus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Pavlodarská	Pavlodarský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Pavlodar	Pavlodar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Severokazachstánská	Severokazachstánský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Petropavlovsk	Petropavlovsk	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Východokazachstánská	Východokazachstánský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Öskemen	Öskemen	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Západokazachstánská	Západokazachstánský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Oral	orat	k5eAaImAgInS
<g/>
)	)	kIx)
•	•	k?
Žambylská	Žambylský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Taraz	Taraz	k1gInSc1
<g/>
)	)	kIx)
Samosprávná	samosprávný	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Almaty	Almata	k1gFnPc1
•	•	k?
Bajkonur	Bajkonur	k1gMnSc1
•	•	k?
Nur-Sultan	Nur-Sultan	k1gInSc1
•	•	k?
Šymkent	Šymkent	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
Asie	Asie	k1gFnSc2
Nezávislé	závislý	k2eNgFnSc2d1
státy	stát	k1gInPc1
</s>
<s>
Abú	abú	k1gMnSc1
Zabí	Zabí	k1gFnSc2
(	(	kIx(
<g/>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Ammán	Ammán	k1gInSc1
(	(	kIx(
<g/>
Jordánsko	Jordánsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Ankara	Ankara	k1gFnSc1
(	(	kIx(
<g/>
Turecko	Turecko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Ašchabad	Ašchabad	k1gInSc1
(	(	kIx(
<g/>
Turkmenistán	Turkmenistán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bagdád	Bagdád	k1gInSc1
(	(	kIx(
<g/>
Irák	Irák	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Baku	Baku	k1gNnSc1
(	(	kIx(
<g/>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bandar	Bandar	k1gMnSc1
Seri	Ser	k1gFnSc2
Begawan	Begawan	k1gMnSc1
(	(	kIx(
<g/>
Brunej	Brunej	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Bangkok	Bangkok	k1gInSc1
(	(	kIx(
<g/>
Thajsko	Thajsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Bejrút	Bejrút	k1gInSc1
(	(	kIx(
<g/>
Libanon	Libanon	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Biškek	Biškek	k1gInSc1
(	(	kIx(
<g/>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Damašek	Damašek	k1gInSc1
(	(	kIx(
<g/>
Sýrie	Sýrie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dauhá	Dauhý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Katar	katar	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Dháka	Dháka	k1gFnSc1
(	(	kIx(
<g/>
Bangladéš	Bangladéš	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Dili	Dili	k1gNnSc1
(	(	kIx(
<g/>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Dillí	Dillí	k1gNnSc2
(	(	kIx(
<g/>
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dušanbe	Dušanb	k1gInSc5
(	(	kIx(
<g/>
Tádžikistán	Tádžikistán	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Hanoj	Hanoj	k1gFnSc1
(	(	kIx(
<g/>
Vietnam	Vietnam	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Islámábád	Islámábáda	k1gFnPc2
(	(	kIx(
<g/>
Pákistán	Pákistán	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Jakarta	Jakarta	k1gFnSc1
(	(	kIx(
<g/>
Indonésie	Indonésie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jerevan	Jerevan	k1gMnSc1
(	(	kIx(
<g/>
Arménie	Arménie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
Palestina	Palestina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kábul	Kábul	k1gInSc1
(	(	kIx(
<g/>
Afghánistán	Afghánistán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Káthmándú	Káthmándú	k1gFnSc1
(	(	kIx(
<g/>
Nepál	Nepál	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kuala	Kuala	k1gMnSc1
Lumpur	Lumpur	k1gMnSc1
(	(	kIx(
<g/>
Malajsie	Malajsie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
(	(	kIx(
<g/>
Kuvajt	Kuvajt	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Male	male	k6eAd1
(	(	kIx(
<g/>
Maledivy	Maledivy	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Manáma	Manáma	k1gFnSc1
(	(	kIx(
<g/>
Bahrajn	Bahrajn	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Manila	Manila	k1gFnSc1
(	(	kIx(
<g/>
Filipíny	Filipíny	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Maskat	Maskat	k1gInSc1
(	(	kIx(
<g/>
Omán	Omán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Neipyijto	Neipyijto	k1gNnSc1
(	(	kIx(
<g/>
Myanmar	Myanmar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Nur-Sultan	Nur-Sultan	k1gInSc1
(	(	kIx(
<g/>
Kazachstán	Kazachstán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Peking	Peking	k1gInSc1
(	(	kIx(
<g/>
Čína	Čína	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Phnompenh	Phnompenh	k1gInSc1
(	(	kIx(
<g/>
Kambodža	Kambodža	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pchjongjang	Pchjongjang	k1gInSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Rijád	Rijáda	k1gFnPc2
(	(	kIx(
<g/>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
San	San	k1gFnSc1
<g/>
'	'	kIx"
<g/>
á	á	k0
(	(	kIx(
<g/>
Jemen	Jemen	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Singapur	Singapur	k1gInSc1
(	(	kIx(
<g/>
Singapur	Singapur	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Soul	Soul	k1gInSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Šrí	Šrí	k1gFnSc1
Džajavardanapura	Džajavardanapura	k1gFnSc1
Kotte	Kott	k1gInSc5
(	(	kIx(
<g/>
Srí	Srí	k1gFnSc3
Lanka	lanko	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Taškent	Taškent	k1gInSc1
(	(	kIx(
<g/>
Uzbekistán	Uzbekistán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tbilisi	Tbilisi	k1gNnSc2
(	(	kIx(
<g/>
Gruzie	Gruzie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Teherán	Teherán	k1gInSc1
(	(	kIx(
<g/>
Írán	Írán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Thimbú	Thimbú	k1gFnSc1
(	(	kIx(
<g/>
Bhútán	Bhútán	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tokio	Tokio	k1gNnSc1
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Ulánbátar	Ulánbátar	k1gInSc1
(	(	kIx(
<g/>
Mongolsko	Mongolsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Vientiane	Vientian	k1gMnSc5
(	(	kIx(
<g/>
Laos	Laos	k1gInSc1
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azámořská	azámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
West	West	k2eAgInSc1d1
Island	Island	k1gInSc1
(	(	kIx(
<g/>
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Flying	Flying	k1gInSc1
Fish	Fish	k1gMnSc1
Cove	Cove	k1gInSc1
(	(	kIx(
<g/>
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
<g/>
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
Hongkong	Hongkong	k1gInSc1
(	(	kIx(
<g/>
Hongkong	Hongkong	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Macao	Macao	k1gNnSc1
(	(	kIx(
<g/>
Macao	Macao	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
Episkopi	Episkopi	k1gNnSc1
(	(	kIx(
<g/>
Akrotiri	Akrotiri	k1gNnSc1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Diego	Diego	k1gMnSc1
García	García	k1gMnSc1
(	(	kIx(
<g/>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
)	)	kIx)
Územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
se	se	k3xPyFc4
spornýmmezinárodním	spornýmmezinárodní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Gaza	Gaza	k1gFnSc1
(	(	kIx(
<g/>
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Suchumi	Suchumi	k1gNnSc1
(	(	kIx(
<g/>
Abcházie	Abcházie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Cchinvali	Cchinvali	k1gFnSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Severní	severní	k2eAgFnSc1d1
Nikósie	Nikósie	k1gFnSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Kypr	Kypr	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Stěpanakert	Stěpanakert	k1gInSc1
(	(	kIx(
<g/>
Arcach	Arcach	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tchaj-pej	Tchaj-pej	k1gFnSc1
(	(	kIx(
<g/>
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
)	)	kIx)
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
*	*	kIx~
<g/>
nárokováno	nárokován	k2eAgNnSc4d1
Izraelem	Izrael	k1gInSc7
i	i	k8xC
Palestinou	Palestina	k1gFnSc7
<g/>
,	,	kIx,
faktická	faktický	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
578903	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4449241-8	4449241-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
98095393	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124517895	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
98095393	#num#	k4
</s>
