<s>
U	u	k7c2
rukopisů	rukopis	k1gInPc2
a	a	k8xC
starých	starý	k2eAgInPc2d1
tisků	tisk	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nemají	mít	k5eNaImIp3nP
číslované	číslovaný	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
starší	starý	k2eAgFnSc1d2
foliace	foliace	k1gFnSc1
<g/>
,	,	kIx,
počítání	počítání	k1gNnSc1
a	a	k8xC
číslování	číslování	k1gNnSc1
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>