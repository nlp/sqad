<s>
Paginace	paginace	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
označování	označování	k1gNnSc6
stránek	stránka	k1gFnPc2
knihy	kniha	k1gFnSc2
čísly	čísnout	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Stránkování	stránkování	k1gNnSc4
paměti	paměť	k1gFnSc2
(	(	kIx(
<g/>
paging	paging	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
technika	technika	k1gFnSc1
adresace	adresace	k1gFnSc2
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Paginace	paginace	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
paginování	paginování	k1gNnSc1
<g/>
,	,	kIx,
z	z	k7c2
lat.	lat.	k?
pagina	pagina	k1gFnSc1
<g/>
,	,	kIx,
stránka	stránka	k1gFnSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
stránkování	stránkování	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označování	označování	k1gNnSc1
stránek	stránka	k1gFnPc2
knihy	kniha	k1gFnSc2
nebo	nebo	k8xC
dokumentu	dokument	k1gInSc2
pořadovými	pořadový	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
(	(	kIx(
<g/>
zřídka	zřídka	k6eAd1
písmeny	písmeno	k1gNnPc7
či	či	k8xC
jinými	jiný	k2eAgInPc7d1
grafickými	grafický	k2eAgInPc7d1
znaky	znak	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Paginace	paginace	k1gFnSc1
</s>
<s>
Paginýrka	paginýrka	k1gFnSc1
</s>
<s>
Paginace	paginace	k1gFnSc1
je	být	k5eAaImIp3nS
součást	součást	k1gFnSc1
tzv.	tzv.	kA
záhlaví	záhlaví	k1gNnSc2
nebo	nebo	k8xC
zápatí	zápatí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSN	ČSN	kA
ISO	ISO	kA
5127-2003	5127-2003	k4
rozlišuje	rozlišovat	k5eAaImIp3nS
ještě	ještě	k9
souběžné	souběžný	k2eAgNnSc1d1
stránkování	stránkování	k1gNnSc1
dvojjazyčných	dvojjazyčný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
přerušované	přerušovaný	k2eAgNnSc1d1
stránkování	stránkování	k1gNnSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
použito	použít	k5eAaPmNgNnS
několik	několik	k4yIc1
číselných	číselný	k2eAgFnPc2d1
řad	řada	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
průběžné	průběžný	k2eAgNnSc4d1
stránkování	stránkování	k1gNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
vícesvazkové	vícesvazkový	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
stránkováno	stránkován	k2eAgNnSc1d1
průběžně	průběžně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
tištěna	tisknout	k5eAaImNgFnS
ve	v	k7c6
více	hodně	k6eAd2
sloupcích	sloupec	k1gInPc6
<g/>
,	,	kIx,
doplňuje	doplňovat	k5eAaImIp3nS
se	se	k3xPyFc4
paginace	paginace	k1gFnSc1
někdy	někdy	k6eAd1
ještě	ještě	k9
písmenem	písmeno	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
označuje	označovat	k5eAaImIp3nS
sloupec	sloupec	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
b	b	k?
znamená	znamenat	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
sloupec	sloupec	k1gInSc1
na	na	k7c6
straně	strana	k1gFnSc6
21	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slovnících	slovník	k1gInPc6
a	a	k8xC
encyklopediích	encyklopedie	k1gFnPc6
se	se	k3xPyFc4
někdy	někdy	k6eAd1
číslují	číslovat	k5eAaImIp3nP
přímo	přímo	k6eAd1
sloupce	sloupec	k1gInSc2
a	a	k8xC
odkazuje	odkazovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gMnPc4
zkratkou	zkratka	k1gFnSc7
"	"	kIx"
<g/>
col	cola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
,	,	kIx,
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
columna	column	k1gInSc2
<g/>
,	,	kIx,
sloupec	sloupec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Paginýrka	paginýrka	k1gFnSc1
</s>
<s>
K	k	k7c3
ručnímu	ruční	k2eAgNnSc3d1
číslování	číslování	k1gNnSc3
dokumentů	dokument	k1gInPc2
nebo	nebo	k8xC
stránek	stránka	k1gFnPc2
v	v	k7c6
sešitech	sešit	k1gInPc6
a	a	k8xC
podobně	podobně	k6eAd1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgNnSc4d1
razítko	razítko	k1gNnSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
paginýrka	paginýrka	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
při	při	k7c6
každém	každý	k3xTgInSc6
otištění	otištění	k1gNnSc6
zvyšuje	zvyšovat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
o	o	k7c4
předvolenou	předvolený	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
o	o	k7c4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Foliace	Foliace	k1gFnSc1
</s>
<s>
Verso	Versa	k1gFnSc5
a	a	k8xC
recto	recta	k1gFnSc5
</s>
<s>
U	u	k7c2
rukopisů	rukopis	k1gInPc2
a	a	k8xC
starých	starý	k2eAgInPc2d1
tisků	tisk	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nemají	mít	k5eNaImIp3nP
číslované	číslovaný	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
starší	starý	k2eAgFnSc1d2
foliace	foliace	k1gFnSc1
<g/>
,	,	kIx,
počítání	počítání	k1gNnSc1
a	a	k8xC
číslování	číslování	k1gNnSc1
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
listy	list	k1gInPc1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
zkratkou	zkratka	k1gFnSc7
fol	fol	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
folium	folium	k1gNnSc1
<g/>
,	,	kIx,
list	list	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levá	levý	k2eAgFnSc1d1
či	či	k8xC
rubová	rubový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
se	se	k3xPyFc4
přitom	přitom	k6eAd1
označuje	označovat	k5eAaImIp3nS
latinským	latinský	k2eAgNnSc7d1
verso	versa	k1gFnSc5
(	(	kIx(
<g/>
v.	v.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rub	rub	k1gInSc1
<g/>
,	,	kIx,
pravá	pravý	k2eAgFnSc1d1
čili	čili	k8xC
lícová	lícový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
jako	jako	k8xC,k8xS
recto	recto	k1gNnSc1
(	(	kIx(
<g/>
r.	r.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vedlejším	vedlejší	k2eAgInSc6d1
obrázku	obrázek	k1gInSc6
by	by	kYmCp3nS
levá	levý	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
s	s	k7c7
číslem	číslo	k1gNnSc7
2	#num#	k4
byla	být	k5eAaImAgFnS
označena	označit	k5eAaPmNgFnS
jako	jako	k9
"	"	kIx"
<g/>
fol	fol	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
v	v	k7c4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pravá	pravý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
fol	fol	k?
<g/>
.	.	kIx.
2	#num#	k4
<g/>
r	r	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
lícové	lícový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
označují	označovat	k5eAaImIp3nP
písmenem	písmeno	k1gNnSc7
a	a	k8xC
<g/>
,	,	kIx,
rubové	rubový	k2eAgNnSc1d1
písmenem	písmeno	k1gNnSc7
b.	b.	k?
Pak	pak	k8xC
by	by	kYmCp3nP
se	se	k3xPyFc4
stránky	stránka	k1gFnPc1
na	na	k7c6
obrázku	obrázek	k1gInSc6
označily	označit	k5eAaPmAgFnP
jako	jako	k8xC,k8xS
1	#num#	k4
<g/>
b	b	k?
a	a	k8xC
2	#num#	k4
<g/>
a.	a.	k?
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
text	text	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
dvou	dva	k4xCgMnPc2
(	(	kIx(
<g/>
či	či	k8xC
více	hodně	k6eAd2
<g/>
)	)	kIx)
sloupců	sloupec	k1gInPc2
<g/>
,	,	kIx,
však	však	k9
malá	malý	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
označují	označovat	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
sloupce	sloupec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
by	by	kYmCp3nS
na	na	k7c6
výše	vysoce	k6eAd2
uvedeném	uvedený	k2eAgInSc6d1
příkladu	příklad	k1gInSc6
byly	být	k5eAaImAgInP
na	na	k7c6
levé	levý	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
sloupce	sloupec	k1gInSc2
f	f	k?
<g/>
1	#num#	k4
<g/>
va	va	k0wR
<g/>
,	,	kIx,
f	f	k?
<g/>
1	#num#	k4
<g/>
vb	vb	k?
a	a	k8xC
na	na	k7c6
pravé	pravý	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
sloupce	sloupec	k1gInSc2
f	f	k?
<g/>
2	#num#	k4
<g/>
ra	ra	k0
a	a	k8xC
f	f	k?
<g/>
2	#num#	k4
<g/>
rb	rb	k?
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://aleph.nkp.cz/F/4LVVYLP45EFNQEP8FNIXP265KA6XJRD3YDHSV2JR8E8T3JEFP2-47186?func=find-b&	http://aleph.nkp.cz/F/4LVVYLP45EFNQEP8FNIXP265KA6XJRD3YDHSV2JR8E8T3JEFP2-47186?func=find-b&	k?
NKTD	NKTD	kA
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
paginaceg	paginacega	k1gFnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
studia	studio	k1gNnSc2
staročeských	staročeský	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
a	a	k8xC
tisků	tisk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SPN	SPN	kA
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
74	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Folio	folio	k1gNnSc1
</s>
<s>
Knihařství	knihařství	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Paginace	paginace	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
