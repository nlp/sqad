<s>
Přehrada	přehrada	k1gFnSc1	přehrada
dříve	dříve	k6eAd2	dříve
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xC	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
Vírské	vírský	k2eAgFnSc2d1	Vírská
přehrady	přehrada	k1gFnSc2	přehrada
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
vrtů	vrt	k1gInPc2	vrt
v	v	k7c6	v
Březové	březový	k2eAgFnSc6d1	Březová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
