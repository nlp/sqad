<p>
<s>
Melodický	melodický	k2eAgInSc1d1	melodický
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
označován	označovat	k5eAaImNgInS	označovat
také	také	k9	také
jako	jako	k8xS	jako
melodeath	melodeath	k1gMnSc1	melodeath
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podžánr	podžánr	k1gInSc4	podžánr
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
melodických	melodický	k2eAgInPc2d1	melodický
kytarových	kytarový	k2eAgInPc2d1	kytarový
riffů	riff	k1gInPc2	riff
<g/>
,	,	kIx,	,
sól	sólo	k1gNnPc2	sólo
<g/>
,	,	kIx,	,
akustické	akustický	k2eAgFnPc1d1	akustická
kytary	kytara	k1gFnPc1	kytara
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
melodeath	melodeatha	k1gFnPc2	melodeatha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vedle	vedle	k7c2	vedle
growlingu	growling	k1gInSc2	growling
i	i	k8xC	i
tzn.	tzn.	kA	tzn.
clean	clean	k1gInSc1	clean
neboli	neboli	k8xC	neboli
čisté	čistý	k2eAgInPc1d1	čistý
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
death	death	k1gInSc1	death
metalu	metal	k1gInSc3	metal
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
názvem	název	k1gInSc7	název
Göteborg	Göteborg	k1gMnSc1	Göteborg
(	(	kIx(	(
<g/>
Gothenburg	Gothenburg	k1gMnSc1	Gothenburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc4d1	jiné
označení	označení	k1gNnSc4	označení
melodic	melodic	k1gMnSc1	melodic
death	death	k1gMnSc1	death
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Göteborg	Göteborg	k1gMnSc1	Göteborg
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k9	jako
místo	místo	k1gNnSc1	místo
vzniku	vznik	k1gInSc2	vznik
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
můžeme	moct	k5eAaImIp1nP	moct
považovat	považovat	k5eAaImF	považovat
skupiny	skupina	k1gFnPc1	skupina
In	In	k1gMnSc1	In
Flames	Flames	k1gMnSc1	Flames
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc1d2	starší
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
At	At	k1gMnSc1	At
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
a	a	k8xC	a
například	například	k6eAd1	například
Dark	Dark	k1gInSc4	Dark
Tranquillity	Tranquillita	k1gFnSc2	Tranquillita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Ablaze	Ablaha	k1gFnSc3	Ablaha
My	my	k3xPp1nPc1	my
Sorrow	Sorrow	k1gMnSc4	Sorrow
</s>
</p>
<p>
<s>
Amon	Amon	k1gMnSc1	Amon
Amarth	Amarth	k1gMnSc1	Amarth
</s>
</p>
<p>
<s>
Arch	arch	k1gInSc1	arch
Enemy	Enema	k1gFnSc2	Enema
</s>
</p>
<p>
<s>
Avatar	Avatar	k1gMnSc1	Avatar
</s>
</p>
<p>
<s>
The	The	k?	The
Agonist	Agonist	k1gInSc1	Agonist
</s>
</p>
<p>
<s>
At	At	k?	At
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
</s>
</p>
<p>
<s>
Callenish	Callenish	k1gInSc1	Callenish
Circle	Circle	k1gFnSc2	Circle
</s>
</p>
<p>
<s>
Ceremonial	Ceremonial	k1gMnSc1	Ceremonial
Oath	Oath	k1gMnSc1	Oath
</s>
</p>
<p>
<s>
Dark	Dark	k1gInSc1	Dark
Tranquillity	Tranquillita	k1gFnSc2	Tranquillita
</s>
</p>
<p>
<s>
Darkane	Darkanout	k5eAaPmIp3nS	Darkanout
</s>
</p>
<p>
<s>
Dethklok	Dethklok	k1gInSc1	Dethklok
</s>
</p>
<p>
<s>
Dimension	Dimension	k1gInSc1	Dimension
Zero	Zero	k6eAd1	Zero
</s>
</p>
<p>
<s>
Disarmonia	Disarmonium	k1gNnPc1	Disarmonium
Mundi	Mund	k1gMnPc1	Mund
</s>
</p>
<p>
<s>
Edge	Edge	k1gFnSc1	Edge
of	of	k?	of
Sanity	sanita	k1gFnSc2	sanita
</s>
</p>
<p>
<s>
Eluveitie	Eluveitie	k1gFnSc1	Eluveitie
</s>
</p>
<p>
<s>
Embrace	Embrace	k1gFnSc1	Embrace
The	The	k1gFnSc2	The
Darkness	Darknessa	k1gFnPc2	Darknessa
</s>
</p>
<p>
<s>
Enforsaken	Enforsaken	k1gInSc1	Enforsaken
</s>
</p>
<p>
<s>
Ensiferum	Ensiferum	k1gInSc1	Ensiferum
</s>
</p>
<p>
<s>
Entombed	Entombed	k1gMnSc1	Entombed
</s>
</p>
<p>
<s>
Eternal	Eternat	k5eAaPmAgInS	Eternat
Tears	Tears	k1gInSc1	Tears
of	of	k?	of
Sorrow	Sorrow	k1gFnSc2	Sorrow
</s>
</p>
<p>
<s>
Hypocrisy	Hypocris	k1gInPc1	Hypocris
</s>
</p>
<p>
<s>
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
</s>
</p>
<p>
<s>
In	In	k?	In
Flames	Flames	k1gInSc1	Flames
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc1d2	starší
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Insomnium	Insomnium	k1gNnSc1	Insomnium
</s>
</p>
<p>
<s>
Into	Into	k6eAd1	Into
Eternity	eternit	k1gInPc1	eternit
</s>
</p>
<p>
<s>
Kalmah	Kalmah	k1gMnSc1	Kalmah
</s>
</p>
<p>
<s>
Mercenary	Mercenara	k1gFnPc1	Mercenara
</s>
</p>
<p>
<s>
Mors	Mors	k6eAd1	Mors
Principium	principium	k1gNnSc1	principium
Est	Est	k1gMnSc1	Est
</s>
</p>
<p>
<s>
Nightrage	Nightrage	k6eAd1	Nightrage
</s>
</p>
<p>
<s>
Norther	Northra	k1gFnPc2	Northra
</s>
</p>
<p>
<s>
Noumena	noumenon	k1gNnPc1	noumenon
</s>
</p>
<p>
<s>
Omnium	omnium	k1gNnSc1	omnium
Gatherum	Gatherum	k1gInSc1	Gatherum
</s>
</p>
<p>
<s>
Sentenced	Sentenced	k1gMnSc1	Sentenced
</s>
</p>
<p>
<s>
Soilwork	Soilwork	k1gInSc1	Soilwork
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc1d2	starší
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonic	Sonic	k1gMnSc1	Sonic
Syndicate	Syndicat	k1gInSc5	Syndicat
</s>
</p>
<p>
<s>
Sklepmaster	Sklepmaster	k1gMnSc1	Sklepmaster
</s>
</p>
<p>
<s>
Skyfire	Skyfir	k1gMnSc5	Skyfir
</s>
</p>
<p>
<s>
Suidakra	Suidakra	k6eAd1	Suidakra
</s>
</p>
<p>
<s>
The	The	k?	The
Black	Black	k1gMnSc1	Black
Dahlia	Dahlium	k1gNnSc2	Dahlium
Murder	Murder	k1gMnSc1	Murder
</s>
</p>
<p>
<s>
The	The	k?	The
Duskfall	Duskfall	k1gInSc1	Duskfall
</s>
</p>
<p>
<s>
The	The	k?	The
Unguided	Unguided	k1gInSc1	Unguided
</s>
</p>
<p>
<s>
Unanimated	Unanimated	k1gMnSc1	Unanimated
</s>
</p>
<p>
<s>
Wintersun	Wintersun	k1gMnSc1	Wintersun
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Death	Death	k1gMnSc1	Death
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Brutal	Brutal	k1gMnSc1	Brutal
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Skandinávský	skandinávský	k2eAgInSc1d1	skandinávský
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
</s>
</p>
<p>
<s>
Metalová	metalový	k2eAgFnSc1d1	metalová
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
metalové	metalový	k2eAgFnSc2d1	metalová
hudby	hudba	k1gFnSc2	hudba
</s>
</p>
