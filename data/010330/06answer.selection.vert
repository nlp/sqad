<s>
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Uranus	Uranus	k1gInSc1	Uranus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
