<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
erozivním	erozivní	k2eAgNnSc7d1	erozivní
působením	působení	k1gNnSc7	působení
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
k	k	k7c3	k
zahlubování	zahlubování	k1gNnSc3	zahlubování
koryta	koryto	k1gNnSc2	koryto
do	do	k7c2	do
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
přirozené	přirozený	k2eAgFnSc2d1	přirozená
deprese	deprese	k1gFnSc2	deprese
a	a	k8xC	a
sklonu	sklon	k1gInSc2	sklon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
akumuluje	akumulovat	k5eAaBmIp3nS	akumulovat
a	a	k8xC	a
vlivem	vliv	k1gInSc7	vliv
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tečení	tečení	k1gNnSc2	tečení
voda	voda	k1gFnSc1	voda
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
podloží	podloží	k1gNnSc4	podloží
erozivní	erozivní	k2eAgFnSc7d1	erozivní
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
začíná	začínat	k5eAaImIp3nS	začínat
rozrušovat	rozrušovat	k5eAaImF	rozrušovat
podloží	podloží	k1gNnSc4	podloží
<g/>
,	,	kIx,	,
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
materiál	materiál	k1gInSc4	materiál
transportovat	transportovat	k5eAaBmF	transportovat
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
říční	říční	k2eAgNnSc4d1	říční
koryto	koryto	k1gNnSc4	koryto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
při	při	k7c6	při
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
aktivitě	aktivita	k1gFnSc6	aktivita
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
typům	typ	k1gInPc3	typ
údolí	údolí	k1gNnSc2	údolí
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
ostrými	ostrý	k2eAgInPc7d1	ostrý
svahy	svah	k1gInPc7	svah
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmena	písmeno	k1gNnSc2	písmeno
V	V	kA	V
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
k	k	k7c3	k
vodní	vodní	k2eAgFnSc3d1	vodní
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgNnPc1d1	říční
údolí	údolí	k1gNnPc1	údolí
jsou	být	k5eAaImIp3nP	být
známá	známý	k2eAgFnSc1d1	známá
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
vysokých	vysoký	k2eAgFnPc2d1	vysoká
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	můj	k3xOp1gFnSc1	můj
voda	voda	k1gFnSc1	voda
silnou	silný	k2eAgFnSc4d1	silná
erozivní	erozivní	k2eAgFnSc4d1	erozivní
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tok	tok	k1gInSc1	tok
musí	muset	k5eAaImIp3nS	muset
překonávat	překonávat	k5eAaImF	překonávat
překážku	překážka	k1gFnSc4	překážka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyzvednutí	vyzvednutí	k1gNnSc4	vyzvednutí
kůry	kůra	k1gFnSc2	kůra
tektonickými	tektonický	k2eAgInPc7d1	tektonický
pochody	pochod	k1gInPc7	pochod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soustava	soustava	k1gFnSc1	soustava
říčních	říční	k2eAgNnPc2d1	říční
údolí	údolí	k1gNnPc2	údolí
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
údolní	údolní	k2eAgFnSc4d1	údolní
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typologie	typologie	k1gFnSc2	typologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
neexistuje	existovat	k5eNaImIp3nS	existovat
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
typologie	typologie	k1gFnSc1	typologie
říčních	říční	k2eAgNnPc2d1	říční
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
tak	tak	k9	tak
několik	několik	k4yIc4	několik
klasifikačních	klasifikační	k2eAgInPc2d1	klasifikační
systémů	systém	k1gInPc2	systém
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasifikační	klasifikační	k2eAgInPc4d1	klasifikační
systémy	systém	k1gInPc4	systém
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
autorů	autor	k1gMnPc2	autor
===	===	k?	===
</s>
</p>
<p>
<s>
český	český	k2eAgMnSc1d1	český
geograf	geograf	k1gMnSc1	geograf
Vitásek	Vitásek	k1gMnSc1	Vitásek
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
rozčlenil	rozčlenit	k5eAaPmAgMnS	rozčlenit
říční	říční	k2eAgNnSc4d1	říční
údolí	údolí	k1gNnSc4	údolí
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
příčného	příčný	k2eAgInSc2d1	příčný
profilu	profil	k1gInSc2	profil
na	na	k7c4	na
</s>
</p>
<p>
<s>
soutěsku	soutěska	k1gFnSc4	soutěska
</s>
</p>
<p>
<s>
kaňon	kaňon	k1gInSc1	kaňon
</s>
</p>
<p>
<s>
těsné	těsný	k2eAgNnSc4d1	těsné
údolí	údolí	k1gNnSc4	údolí
</s>
</p>
<p>
<s>
úvalPolský	úvalPolský	k2eAgMnSc1d1	úvalPolský
geolog	geolog	k1gMnSc1	geolog
Klimaszewski	Klimaszewsk	k1gFnSc2	Klimaszewsk
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
člení	členit	k5eAaImIp3nP	členit
říční	říční	k2eAgNnPc1d1	říční
údolí	údolí	k1gNnPc1	údolí
podle	podle	k7c2	podle
monografických	monografický	k2eAgFnPc2d1	monografická
charakteristik	charakteristika	k1gFnPc2	charakteristika
příčného	příčný	k2eAgInSc2d1	příčný
profilu	profil	k1gInSc2	profil
na	na	k7c4	na
</s>
</p>
<p>
<s>
soutěsku	soutěsk	k1gInSc2	soutěsk
<g/>
,	,	kIx,	,
erozní	erozní	k2eAgInSc1d1	erozní
zářez	zářez	k1gInSc1	zářez
<g/>
,	,	kIx,	,
kaňon	kaňon	k1gInSc1	kaňon
<g/>
,	,	kIx,	,
V-údolí	V-údolí	k1gNnSc1	V-údolí
ostré	ostrý	k2eAgInPc1d1	ostrý
<g/>
,	,	kIx,	,
V-údolí	V-údolí	k1gNnSc1	V-údolí
prosté	prostý	k2eAgInPc1d1	prostý
<g/>
,	,	kIx,	,
V-údolí	V-údolí	k1gNnSc1	V-údolí
otevřené	otevřený	k2eAgInPc1d1	otevřený
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc1	údolí
plochého	plochý	k2eAgNnSc2d1	ploché
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc1	údolí
plochého	plochý	k2eAgNnSc2d1	ploché
dna	dno	k1gNnSc2	dno
agradační	agradační	k2eAgFnSc1d1	agradační
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc1	údolí
úvalovité	úvalovitý	k2eAgInPc1d1	úvalovitý
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnPc1	údolí
se	s	k7c7	s
zakleslým	zakleslý	k2eAgInSc7d1	zakleslý
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc2	údolí
úvalovité	úvalovitý	k2eAgNnSc1d1	úvalovitý
s	s	k7c7	s
ohraničením	ohraničení	k1gNnSc7	ohraničení
a	a	k8xC	a
úvalovité	úvalovitý	k2eAgNnSc1d1	úvalovitý
údolí	údolí	k1gNnSc1	údolí
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
tropické	tropický	k2eAgNnSc4d1	tropické
oblastiJedno	oblastiJedno	k1gNnSc4	oblastiJedno
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
členění	členění	k1gNnSc2	členění
přinesl	přinést	k5eAaPmAgInS	přinést
Jaromír	Jaromír	k1gMnSc1	Jaromír
Demek	Demek	k1gMnSc1	Demek
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
vymezil	vymezit	k5eAaPmAgInS	vymezit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
lineární	lineární	k2eAgFnSc7d1	lineární
erozí	eroze	k1gFnSc7	eroze
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
hloubkové	hloubkový	k2eAgFnSc3d1	hloubková
erozi	eroze	k1gFnSc3	eroze
<g/>
)	)	kIx)	)
a	a	k8xC	a
vývojem	vývoj	k1gInSc7	vývoj
svahů	svah	k1gInPc2	svah
(	(	kIx(	(
<g/>
boční	boční	k2eAgFnSc6d1	boční
erozi	eroze	k1gFnSc6	eroze
<g/>
)	)	kIx)	)
tyto	tento	k3xDgInPc4	tento
typy	typ	k1gInPc4	typ
údolí	údolí	k1gNnSc2	údolí
</s>
</p>
<p>
<s>
soutěska	soutěska	k1gFnSc1	soutěska
–	–	k?	–
výrazně	výrazně	k6eAd1	výrazně
převažuje	převažovat	k5eAaImIp3nS	převažovat
hloubková	hloubkový	k2eAgFnSc1d1	hloubková
eroze	eroze	k1gFnSc1	eroze
nad	nad	k7c7	nad
boční	boční	k2eAgFnSc7d1	boční
<g/>
.	.	kIx.	.
</s>
<s>
Svahy	svah	k1gInPc1	svah
soutěsky	soutěska	k1gFnSc2	soutěska
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
a	a	k8xC	a
šířka	šířka	k1gFnSc1	šířka
soutěsky	soutěska	k1gFnSc2	soutěska
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
partiích	partie	k1gFnPc6	partie
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
v	v	k7c6	v
dolních	dolní	k2eAgFnPc6d1	dolní
partiích	partie	k1gFnPc6	partie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
soutěsky	soutěska	k1gFnSc2	soutěska
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
<g/>
,	,	kIx,	,
vodopády	vodopád	k1gInPc1	vodopád
atd.	atd.	kA	atd.
Velmi	velmi	k6eAd1	velmi
hluboké	hluboký	k2eAgInPc4d1	hluboký
soutěsky	soutěsk	k1gInPc4	soutěsk
bývají	bývat	k5eAaImIp3nP	bývat
nazývány	nazýván	k2eAgInPc4d1	nazýván
kaňony	kaňon	k1gInPc4	kaňon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
údolí	údolí	k1gNnPc1	údolí
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
V	v	k7c6	v
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
rovnovážném	rovnovážný	k2eAgInSc6d1	rovnovážný
vývoji	vývoj	k1gInSc6	vývoj
hloubkové	hloubkový	k2eAgFnSc2d1	hloubková
a	a	k8xC	a
boční	boční	k2eAgFnSc2d1	boční
eroze	eroze	k1gFnSc2	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
tvoří	tvořit	k5eAaImIp3nS	tvořit
koryto	koryto	k1gNnSc4	koryto
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
ode	ode	k7c2	ode
dna	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc2	údolí
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Podélný	podélný	k2eAgInSc4d1	podélný
profil	profil	k1gInSc4	profil
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
údolích	údolí	k1gNnPc6	údolí
bývá	bývat	k5eAaImIp3nS	bývat
nevyrovnaný	vyrovnaný	k2eNgInSc4d1	nevyrovnaný
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
přítoků	přítok	k1gInPc2	přítok
se	se	k3xPyFc4	se
nestačí	stačit	k5eNaBmIp3nS	stačit
zahlubovat	zahlubovat	k5eAaImF	zahlubovat
stejně	stejně	k6eAd1	stejně
rychle	rychle	k6eAd1	rychle
jako	jako	k8xC	jako
údolí	údolí	k1gNnSc6	údolí
hlavního	hlavní	k2eAgInSc2d1	hlavní
toku	tok	k1gInSc2	tok
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
tak	tak	k6eAd1	tak
vzniknout	vzniknout	k5eAaPmF	vzniknout
visutá	visutý	k2eAgNnPc4d1	visuté
údolí	údolí	k1gNnPc4	údolí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
voda	voda	k1gFnSc1	voda
přepadá	přepadat	k5eAaImIp3nS	přepadat
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
neckovité	neckovitý	k2eAgNnSc1d1	neckovitý
údolí	údolí	k1gNnSc1	údolí
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
převaze	převaha	k1gFnSc6	převaha
boční	boční	k2eAgFnSc1d1	boční
eroze	eroze	k1gFnSc1	eroze
nad	nad	k7c7	nad
hloubkovou	hloubkový	k2eAgFnSc7d1	hloubková
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
při	při	k7c6	při
širokém	široký	k2eAgNnSc6d1	široké
údolním	údolní	k2eAgNnSc6d1	údolní
dně	dno	k1gNnSc6	dno
a	a	k8xC	a
střídavě	střídavě	k6eAd1	střídavě
podkopává	podkopávat	k5eAaImIp3nS	podkopávat
údolní	údolní	k2eAgInPc4d1	údolní
svahy	svah	k1gInPc4	svah
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
údolními	údolní	k2eAgInPc7d1	údolní
svahy	svah	k1gInPc7	svah
a	a	k8xC	a
dnem	den	k1gInSc7	den
je	být	k5eAaImIp3nS	být
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
lom	lom	k1gInSc1	lom
spádu	spád	k1gInSc2	spád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
údolního	údolní	k2eAgNnSc2d1	údolní
dna	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
nezřídka	nezřídka	k6eAd1	nezřídka
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
údolní	údolní	k2eAgFnSc1d1	údolní
niva	niva	k1gFnSc1	niva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
úvalovité	úvalovitý	k2eAgNnSc1d1	úvalovitý
údolí	údolí	k1gNnSc1	údolí
–	–	k?	–
též	též	k6eAd1	též
úval	úval	k1gInSc4	úval
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
údolí	údolí	k1gNnPc4	údolí
se	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výrazně	výrazně	k6eAd1	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
boční	boční	k2eAgFnSc1d1	boční
eroze	eroze	k1gFnSc1	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
pozvolna	pozvolna	k6eAd1	pozvolna
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
mírných	mírný	k2eAgInPc2d1	mírný
svahů	svah	k1gInPc2	svah
pokrytých	pokrytý	k2eAgInPc2d1	pokrytý
sedimenty	sediment	k1gInPc7	sediment
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
výraznější	výrazný	k2eAgFnSc2d2	výraznější
paty	pata	k1gFnSc2	pata
svahu	svah	k1gInSc2	svah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
řeka	řeka	k1gFnSc1	řeka
</s>
</p>
<p>
<s>
řečiště	řečiště	k1gNnPc1	řečiště
</s>
</p>
<p>
<s>
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Hydrografie	hydrografie	k1gFnSc1	hydrografie
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
(	(	kIx(	(
<g/>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
tvary	tvar	k1gInPc1	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blažková	Blažková	k1gFnSc1	Blažková
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
:	:	kIx,	:
Environmentální	environmentální	k2eAgFnSc2d1	environmentální
geologie	geologie	k1gFnSc2	geologie
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Soubor	soubor	k1gInSc1	soubor
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
</s>
</p>
