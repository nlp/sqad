<s>
Říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
erozivním	erozivní	k2eAgNnSc7d1	erozivní
působením	působení	k1gNnSc7	působení
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
k	k	k7c3	k
zahlubování	zahlubování	k1gNnSc3	zahlubování
koryta	koryto	k1gNnSc2	koryto
do	do	k7c2	do
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
