<s desamb="1">
Přirozená	přirozený	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
základní	základní	k2eAgInPc4d1
matematické	matematický	k2eAgInPc4d1
koncepty	koncept	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
protože	protože	k8xS
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
nejjednodušší	jednoduchý	k2eAgInSc4d3
na	na	k7c4
pochopení	pochopení	k1gNnSc4
<g/>
,	,	kIx,
začíná	začínat	k5eAaImIp3nS
výuka	výuka	k1gFnSc1
matematiky	matematika	k1gFnSc2
obvykle	obvykle	k6eAd1
od	od	k7c2
přirozených	přirozený	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>