<s>
Podobná	podobný	k2eAgNnPc1d1
přírodní	přírodní	k2eAgNnPc1d1
skla	sklo	k1gNnPc1
(	(	kIx(
<g/>
tektity	tektit	k1gInPc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
ještě	ještě	k9
na	na	k7c6
třech	tři	k4xCgNnPc6
místech	místo	k1gNnPc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
vltavíny	vltavín	k1gInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
průhledné	průhledný	k2eAgFnSc6d1
a	a	k8xC
jasně	jasně	k6eAd1
zelené	zelený	k2eAgInPc1d1
a	a	k8xC
tedy	tedy	k9
použitelné	použitelný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
šperkové	šperkový	k2eAgInPc1d1
kameny	kámen	k1gInPc1
(	(	kIx(
<g/>
„	„	k?
<g/>
drahokamy	drahokam	k1gInPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>