<s>
Vltavín	vltavín	k1gInSc1
</s>
<s>
Vltavín	vltavín	k1gInSc1
</s>
<s>
Vltavín	vltavín	k1gInSc1
z	z	k7c2
Besednice	besednice	k1gFnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
Chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
SiO	SiO	k?
<g/>
2	#num#	k4
+	+	kIx~
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
hnědozelená	hnědozelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
černozelená	černozelený	k2eAgFnSc1d1
</s>
<s>
Vzhled	vzhled	k1gInSc1
krystalu	krystal	k1gInSc2
</s>
<s>
průsvitný	průsvitný	k2eAgInSc1d1
až	až	k8xS
průhledný	průhledný	k2eAgInSc1d1
</s>
<s>
Soustava	soustava	k1gFnSc1
</s>
<s>
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
5,5	5,5	k4
<g/>
–	–	k?
<g/>
6,5	6,5	k4
</s>
<s>
Lesk	lesk	k1gInSc1
</s>
<s>
skelný	skelný	k2eAgInSc1d1
</s>
<s>
Štěpnost	štěpnost	k1gFnSc1
</s>
<s>
lasturnatý	lasturnatý	k2eAgInSc1d1
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
1.492	1.492	k4
</s>
<s>
Vryp	vryp	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc1d1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
2,21	2,21	k4
<g/>
–	–	k?
<g/>
2,96	2,96	k4
g	g	kA
⋅	⋅	k?
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
</s>
<s>
rozpustný	rozpustný	k2eAgMnSc1d1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
vltavín	vltavín	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Vltavín	vltavín	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vltavín	vltavín	k1gInSc1
neboli	neboli	k8xC
moldavit	moldavit	k1gInSc1
je	být	k5eAaImIp3nS
drahý	drahý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc2d1
zeleně	zeleň	k1gFnSc2
zbarvené	zbarvený	k2eAgNnSc4d1
sklo	sklo	k1gNnSc4
s	s	k7c7
charakteristickou	charakteristický	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
povrchu	povrch	k1gInSc2
zvanou	zvaný	k2eAgFnSc4d1
skulptace	skulptace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltavíny	vltavín	k1gInPc7
jsou	být	k5eAaImIp3nP
jediné	jediný	k2eAgInPc1d1
evropské	evropský	k2eAgInPc1d1
tektity	tektit	k1gInPc1
<g/>
,	,	kIx,
horniny	hornina	k1gFnPc1
vzniklé	vzniklý	k2eAgFnPc1d1
v	v	k7c6
důsledku	důsledek	k1gInSc6
dopadu	dopad	k1gInSc2
meteoritu	meteorit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbohatší	bohatý	k2eAgNnSc1d3
naleziště	naleziště	k1gNnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
v	v	k7c6
oblastech	oblast	k1gFnPc6
kolem	kolem	k7c2
řeky	řeka	k1gFnSc2
Vltavy	Vltava	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
„	„	k?
<g/>
Moldau	Molda	k1gMnSc3
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Objevení	objevení	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1787	#num#	k4
přednesl	přednést	k5eAaPmAgMnS
profesor	profesor	k1gMnSc1
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
doktor	doktor	k1gMnSc1
Josef	Josef	k1gMnSc1
Mayer	Mayer	k1gMnSc1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
zajímavém	zajímavý	k2eAgInSc6d1
objevu	objev	k1gInSc6
nedaleko	nedaleko	k7c2
Týna	Týn	k1gInSc2
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
panství	panství	k1gNnSc2
pocházelo	pocházet	k5eAaImAgNnS
několik	několik	k4yIc1
přírodních	přírodní	k2eAgNnPc2d1
skel	sklo	k1gNnPc2
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
se	se	k3xPyFc4
ale	ale	k9
pan	pan	k1gMnSc1
profesor	profesor	k1gMnSc1
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nejspíš	nejspíš	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
chryzolity	chryzolit	k1gInPc4
sopečného	sopečný	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
se	se	k3xPyFc4
však	však	k9
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
jiný	jiný	k2eAgInSc4d1
zvláštní	zvláštní	k2eAgInSc4d1
materiál	materiál	k1gInSc4
a	a	k8xC
podle	podle	k7c2
německého	německý	k2eAgInSc2d1
názvu	název	k1gInSc2
Týna	Týn	k1gInSc2
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
Moldauthein	Moldauthein	k1gMnSc1
(	(	kIx(
<g/>
odvozeného	odvozený	k2eAgMnSc2d1
z	z	k7c2
německého	německý	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
Vltavy	Vltava	k1gFnSc2
Moldau	Moldaus	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kustod	kustod	k1gInSc4
sbírek	sbírka	k1gFnPc2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Franz	Franz	k1gMnSc1
Xaver	Xaver	k1gMnSc1
Maxmilian	Maxmiliana	k1gFnPc2
Zippe	Zippe	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1836	#num#	k4
nazýval	nazývat	k5eAaImAgMnS
moldavity	moldavit	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
ekvivalent	ekvivalent	k1gInSc1
vltavín	vltavín	k1gInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
Jubilejní	jubilejní	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
roku	rok	k1gInSc2
1891	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
pak	pak	k6eAd1
byly	být	k5eAaImAgInP
zařazeny	zařadit	k5eAaPmNgInP
do	do	k7c2
skupiny	skupina	k1gFnSc2
tektitů	tektit	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
až	až	k9
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
byl	být	k5eAaImAgMnS
definitivně	definitivně	k6eAd1
prokázán	prokázán	k2eAgInSc4d1
způsob	způsob	k1gInSc4
jejich	jejich	k3xOp3gInSc2
vzniku	vznik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
nalezišť	naleziště	k1gNnPc2
vltavínů	vltavín	k1gInPc2
je	být	k5eAaImIp3nS
horní	horní	k2eAgNnSc1d1
povodí	povodí	k1gNnSc1
Vltavy	Vltava	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
pás	pás	k1gInSc1
lokalit	lokalita	k1gFnPc2
mezi	mezi	k7c7
Prachaticemi	Prachatice	k1gFnPc7
a	a	k8xC
Trhovými	trhový	k2eAgInPc7d1
Sviny	Svin	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltavíny	vltavín	k1gInPc1
nalezené	nalezený	k2eAgInPc1d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Jihlavy	Jihlava	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
někdy	někdy	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
moravity	moravita	k1gFnPc1
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nepoužívaný	používaný	k2eNgInSc4d1
<g/>
,	,	kIx,
možná	možná	k9
pomístní	pomístní	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odborné	odborný	k2eAgFnPc1d1
publikace	publikace	k1gFnPc1
ho	on	k3xPp3gNnSc4
neznají	znát	k5eNaImIp3nP,k5eAaImIp3nP
<g/>
,	,	kIx,
cizojazyčné	cizojazyčný	k2eAgFnPc1d1
pracují	pracovat	k5eAaImIp3nP
s	s	k7c7
názvem	název	k1gInSc7
moldavit	moldavit	k1gInSc1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moravě	Morava	k1gFnSc6
popsali	popsat	k5eAaPmAgMnP
lokality	lokalita	k1gFnPc4
s	s	k7c7
výskytem	výskyt	k1gInSc7
vltavínů	vltavín	k1gInPc2
badatelé	badatel	k1gMnPc1
František	František	k1gMnSc1
Dvorský	Dvorský	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Rudolf	Rudolf	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
méně	málo	k6eAd2
významné	významný	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
v	v	k7c6
Chebské	chebský	k2eAgFnSc6d1
pánvi	pánev	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
Drážďan	Drážďany	k1gInPc2
i	i	k9
v	v	k7c6
sousedním	sousední	k2eAgNnSc6d1
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jihozápadním	jihozápadní	k2eAgNnSc6d1
Polsku	Polsko	k1gNnSc6
a	a	k8xC
ojedinělé	ojedinělý	k2eAgInPc1d1
nálezy	nález	k1gInPc1
v	v	k7c6
zaniklé	zaniklý	k2eAgFnSc6d1
pískovně	pískovna	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
Kobylisích	Kobylisy	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
čítají	čítat	k5eAaImIp3nP
2	#num#	k4
kusy	kus	k1gInPc4
a	a	k8xC
na	na	k7c6
Mělnicku	Mělnick	k1gInSc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
100	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
popsání	popsání	k1gNnSc6
českých	český	k2eAgInPc2d1
vltavínů	vltavín	k1gInPc2
byly	být	k5eAaImAgFnP
popsány	popsat	k5eAaPmNgInP
nálezy	nález	k1gInPc1
vltavínů	vltavín	k1gInPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1
přírodní	přírodní	k2eAgNnPc1d1
skla	sklo	k1gNnPc1
(	(	kIx(
<g/>
tektity	tektit	k1gInPc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
ještě	ještě	k9
na	na	k7c6
třech	tři	k4xCgNnPc6
místech	místo	k1gNnPc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
vltavíny	vltavín	k1gInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
průhledné	průhledný	k2eAgFnSc6d1
a	a	k8xC
jasně	jasně	k6eAd1
zelené	zelený	k2eAgInPc1d1
a	a	k8xC
tedy	tedy	k9
použitelné	použitelný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
šperkové	šperkový	k2eAgInPc1d1
kameny	kámen	k1gInPc1
(	(	kIx(
<g/>
„	„	k?
<g/>
drahokamy	drahokam	k1gInPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
v	v	k7c6
okolí	okolí	k1gNnSc6
řeky	řeka	k1gFnSc2
Vltavy	Vltava	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
největší	veliký	k2eAgNnSc4d3
naleziště	naleziště	k1gNnSc4
Vltavínů	vltavín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
toto	tento	k3xDgNnSc4
místo	místo	k1gNnSc4
ohrožují	ohrožovat	k5eAaImIp3nP
kopáči	kopáč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
aktívně	aktívně	k6eAd1
hledají	hledat	k5eAaImIp3nP
kámen	kámen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ho	on	k3xPp3gMnSc4
sbírat	sbírat	k5eAaImF
pokud	pokud	k8xS
s	s	k7c7
tím	ten	k3xDgNnSc7
souhlasí	souhlasit	k5eAaImIp3nS
majitel	majitel	k1gMnSc1
pozemku	pozemek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
nelegálního	legální	k2eNgNnSc2d1
sbírání	sbírání	k1gNnSc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
trestný	trestný	k2eAgInSc4d1
čin	čin	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Vltavín	vltavín	k1gInSc1
z	z	k7c2
Besednice	besednice	k1gFnSc2
</s>
<s>
Vltavíny	vltavín	k1gInPc1
jsou	být	k5eAaImIp3nP
chemicky	chemicky	k6eAd1
téměř	téměř	k6eAd1
totožné	totožný	k2eAgInPc1d1
s	s	k7c7
jílovitými	jílovitý	k2eAgFnPc7d1
horninami	hornina	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
například	například	k6eAd1
od	od	k7c2
sopečných	sopečný	k2eAgNnPc2d1
skel	sklo	k1gNnPc2
neobsahují	obsahovat	k5eNaImIp3nP
téměř	téměř	k6eAd1
žádnou	žádný	k3yNgFnSc4
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltavínové	vltavínový	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
jisté	jistý	k2eAgFnPc4d1
„	„	k?
<g/>
vady	vada	k1gFnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
vzduchových	vzduchový	k2eAgFnPc2d1
bublinek	bublinka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
časté	častý	k2eAgMnPc4d1
převážně	převážně	k6eAd1
u	u	k7c2
jihočeských	jihočeský	k2eAgInPc2d1
vltavínů	vltavín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bublinky	bublinka	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
drobné	drobný	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
řádově	řádově	k6eAd1
jen	jen	k9
desetiny	desetina	k1gFnSc2
milimetru	milimetr	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
objevit	objevit	k5eAaPmF
i	i	k9
bubliny	bublina	k1gFnPc4
přes	přes	k7c4
1	#num#	k4
centimetr	centimetr	k1gInSc4
dlouhé	dlouhý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlak	tlak	k1gInSc1
v	v	k7c6
těchto	tento	k3xDgFnPc6
bublinách	bublina	k1gFnPc6
je	být	k5eAaImIp3nS
až	až	k6eAd1
překvapivě	překvapivě	k6eAd1
nízký	nízký	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
19	#num#	k4
až	až	k9
25	#num#	k4
<g/>
krát	krát	k6eAd1
nižší	nízký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
tlak	tlak	k1gInSc1
u	u	k7c2
hladiny	hladina	k1gFnSc2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
domněnce	domněnka	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
vltavíny	vltavín	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
v	v	k7c6
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
panuje	panovat	k5eAaImIp3nS
nižší	nízký	k2eAgInSc1d2
tlak	tlak	k1gInSc1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
například	například	k6eAd1
vyšší	vysoký	k2eAgFnPc1d2
vrstvy	vrstva	k1gFnPc1
atmosféry	atmosféra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgInPc2
vltavínů	vltavín	k1gInPc2
jsou	být	k5eAaImIp3nP
dokonce	dokonce	k9
patrné	patrný	k2eAgFnPc1d1
stopy	stopa	k1gFnPc1
po	po	k7c6
průletu	průlet	k1gInSc6
atmosférou	atmosféra	k1gFnSc7
v	v	k7c6
podobě	podoba	k1gFnSc6
aerodynamického	aerodynamický	k2eAgNnSc2d1
zaoblení	zaoblení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sbírky	sbírka	k1gFnPc1
</s>
<s>
Mineralogické	mineralogický	k2eAgFnPc1d1
sbírky	sbírka	k1gFnPc1
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
sbírka	sbírka	k1gFnSc1
vltavínů	vltavín	k1gInPc2
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
další	další	k2eAgFnPc4d1
menší	malý	k2eAgFnPc4d2
sbírky	sbírka	k1gFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zhlédnout	zhlédnout	k5eAaPmF
v	v	k7c6
Moravském	moravský	k2eAgNnSc6d1
zemském	zemský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Jihočeském	jihočeský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
lokálním	lokální	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Týně	Týn	k1gInSc6
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
také	také	k9
v	v	k7c6
muzeu	muzeum	k1gNnSc6
Vysočiny	vysočina	k1gFnSc2
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
tektitů	tektit	k1gInPc2
a	a	k8xC
vltavínů	vltavín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sbírce	sbírka	k1gFnSc6
je	být	k5eAaImIp3nS
vltavín	vltavín	k1gInSc1
ze	z	k7c2
Štěpánovic	Štěpánovice	k1gFnPc2
(	(	kIx(
<g/>
232,5	232,5	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
největší	veliký	k2eAgInSc1d3
vltavín	vltavín	k1gInSc1
jaký	jaký	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
lze	lze	k6eAd1
spatřit	spatřit	k5eAaPmF
v	v	k7c6
muzejní	muzejní	k2eAgFnSc6d1
sbírce	sbírka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediné	jediný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
věnované	věnovaný	k2eAgNnSc1d1
přímo	přímo	k6eAd1
vltavínům	vltavín	k1gInPc3
(	(	kIx(
<g/>
a	a	k8xC
kosmickým	kosmický	k2eAgInPc3d1
impaktům	impakt	k1gInPc3
<g/>
)	)	kIx)
s	s	k7c7
nádhernou	nádherný	k2eAgFnSc7d1
sbírkou	sbírka	k1gFnSc7
vltavínů	vltavín	k1gInPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
červnu	červen	k1gInSc6
2013	#num#	k4
otevřeno	otevřít	k5eAaPmNgNnS
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1
z	z	k7c2
nich	on	k3xPp3gInPc2
jsou	být	k5eAaImIp3nP
ale	ale	k9
umístěny	umístit	k5eAaPmNgInP
v	v	k7c6
soukromých	soukromý	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
všech	všecek	k3xTgInPc2
spadlých	spadlý	k2eAgInPc2d1
vltavínů	vltavín	k1gInPc2
je	být	k5eAaImIp3nS
20	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
hmotnosti	hmotnost	k1gFnSc6
přibližně	přibližně	k6eAd1
275	#num#	k4
tun	tuna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
až	až	k9
300	#num#	k4
tun	tuna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgInPc1d1
množství	množství	k1gNnSc1
vltavínů	vltavín	k1gInPc2
bylo	být	k5eAaImAgNnS
splaveno	splavit	k5eAaPmNgNnS
do	do	k7c2
řek	řeka	k1gFnPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
transportovány	transportován	k2eAgFnPc1d1
a	a	k8xC
následně	následně	k6eAd1
pak	pak	k6eAd1
uloženy	uložit	k5eAaPmNgInP
v	v	k7c6
sedimentech	sediment	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
miliony	milion	k4xCgInPc1
let	léto	k1gNnPc2
naleptávány	naleptáván	k2eAgFnPc4d1
agresivními	agresivní	k2eAgInPc7d1
přírodními	přírodní	k2eAgInPc7d1
roztoky	roztok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naleptávání	naleptávání	k1gNnSc1
nestejně	stejně	k6eNd1
odolného	odolný	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
tektitů	tektit	k1gInPc2
pak	pak	k9
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
různě	různě	k6eAd1
členitou	členitý	k2eAgFnSc4d1
a	a	k8xC
hlubokou	hluboký	k2eAgFnSc4d1
vrásčitost	vrásčitost	k1gFnSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
kousků	kousek	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nazýváme	nazývat	k5eAaImIp1nP
skulptace	skulptace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltavíny	vltavín	k1gInPc7
se	se	k3xPyFc4
dnes	dnes	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
převážně	převážně	k6eAd1
na	na	k7c6
polích	pole	k1gNnPc6
<g/>
,	,	kIx,
pod	pod	k7c7
nimiž	jenž	k3xRgInPc7
leží	ležet	k5eAaImIp3nS
písčitá	písčitý	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
původních	původní	k2eAgInPc2d1
sedimentů	sediment	k1gInPc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
na	na	k7c4
povrch	povrch	k1gInSc4
během	během	k7c2
hluboké	hluboký	k2eAgFnSc2d1
orby	orba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
však	však	k9
často	často	k6eAd1
je	být	k5eAaImIp3nS
poškození	poškození	k1gNnSc4
během	během	k7c2
orby	orba	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
jsou	být	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
cennější	cenný	k2eAgInPc1d2
nálezy	nález	k1gInPc1
z	z	k7c2
pískoven	pískovna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
šance	šance	k1gFnSc1
na	na	k7c4
objev	objev	k1gInSc4
mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc1d2
(	(	kIx(
<g/>
1	#num#	k4
až	až	k9
3	#num#	k4
na	na	k7c4
metr	metr	k1gInSc4
krychlový	krychlový	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInSc1d3
nalezený	nalezený	k2eAgInSc1d1
vltavín	vltavín	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
u	u	k7c2
Slavic	slavice	k1gFnPc2
a	a	k8xC
váží	vážit	k5eAaImIp3nS
265,5	265,5	k4
gramu	gram	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Průměrná	průměrný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
vltavínu	vltavín	k1gInSc2
nalezeného	nalezený	k2eAgInSc2d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
je	být	k5eAaImIp3nS
však	však	k9
pouze	pouze	k6eAd1
6,7	6,7	k4
gramu	gram	k1gInSc2
a	a	k8xC
u	u	k7c2
moravitů	moravit	k1gInPc2
objevených	objevený	k2eAgInPc2d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
je	být	k5eAaImIp3nS
průměrná	průměrný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
rovna	roven	k2eAgFnSc1d1
13,5	13,5	k4
gramu	gram	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Umělecko-historické	umělecko-historický	k2eAgFnPc1d1
sbírky	sbírka	k1gFnPc1
</s>
<s>
Broušený	broušený	k2eAgInSc1d1
vltavín	vltavín	k1gInSc1
</s>
<s>
Ojediněle	ojediněle	k6eAd1
se	se	k3xPyFc4
vltavín	vltavín	k1gInSc1
zasazoval	zasazovat	k5eAaImAgInS
do	do	k7c2
klenotů	klenot	k1gInPc2
či	či	k8xC
šperků	šperk	k1gInPc2
od	od	k7c2
doby	doba	k1gFnSc2
barokní	barokní	k2eAgFnSc2d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
teprve	teprve	k6eAd1
od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3
sbírku	sbírka	k1gFnSc4
historických	historický	k2eAgInPc2d1
šperků	šperk	k1gInPc2
s	s	k7c7
českými	český	k2eAgInPc7d1
vltavíny	vltavín	k1gInPc7
mají	mít	k5eAaImIp3nP
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
oddělení	oddělení	k1gNnSc1
starších	starý	k2eAgFnPc2d2
českých	český	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1
museum	museum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInSc4d1
a	a	k8xC
současný	současný	k2eAgInSc4d1
šperk	šperk	k1gInSc4
sbírají	sbírat	k5eAaImIp3nP
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Moravská	moravský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
exempláře	exemplář	k1gInPc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
sbírkách	sbírka	k1gFnPc6
mají	mít	k5eAaImIp3nP
Jihočeské	jihočeský	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
Českého	český	k2eAgInSc2d1
ráje	ráj	k1gInSc2
v	v	k7c6
Turnově	Turnov	k1gInSc6
a	a	k8xC
další	další	k2eAgNnPc1d1
regionální	regionální	k2eAgNnPc1d1
muzea	muzeum	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgInPc4d3
vltavíny	vltavín	k1gInPc4
nalezené	nalezený	k2eAgInPc4d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
Radomilice	Radomilice	k1gFnSc1
–	–	k?
142,5	142,5	k4
g	g	kA
–	–	k?
soukromá	soukromý	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dubenec	Dubenec	k1gInSc1
–	–	k?
130,7	130,7	k4
g	g	kA
–	–	k?
soukromá	soukromý	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Radomilice	Radomilice	k1gFnSc1
–	–	k?
125,0	125,0	k4
g	g	kA
–	–	k?
soukromá	soukromý	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Strpí	strpět	k5eAaPmIp3nS
–	–	k?
110,89	110,89	k4
g	g	kA
–	–	k?
o	o	k7c6
rozměrech	rozměr	k1gInPc6
65,2	65,2	k4
<g/>
mm	mm	kA
x	x	k?
46,4	46,4	k4
x	x	k?
30,5	30,5	k4
<g/>
mm	mm	kA
-	-	kIx~
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
-	-	kIx~
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Strpí	strpět	k5eAaPmIp3nS
–	–	k?
172,2	172,2	k4
g	g	kA
–	–	k?
soukromá	soukromý	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
</s>
<s>
Dobrkovská	Dobrkovský	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
–	–	k?
171,0	171,0	k4
g	g	kA
–	–	k?
tvar	tvar	k1gInSc1
činka	činka	k1gFnSc1
–	–	k?
soukromá	soukromý	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
</s>
<s>
Největší	veliký	k2eAgInPc4d3
vltavíny	vltavín	k1gInPc4
nalezené	nalezený	k2eAgInPc4d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
Slavice	slavice	k1gFnSc1
–	–	k?
265,5	265,5	k4
g	g	kA
–	–	k?
L.	L.	kA
Šabata	Šabata	k1gMnSc1
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
o	o	k7c6
rozměrech	rozměr	k1gInPc6
100	#num#	k4
<g/>
mm	mm	kA
x	x	k?
45	#num#	k4
<g/>
mm	mm	kA
x	x	k?
30	#num#	k4
<g/>
mm	mm	kA
</s>
<s>
Terůvky	Terůvka	k1gFnPc4
–	–	k?
235,0	235,0	k4
g	g	kA
–	–	k?
R.	R.	kA
Dvořák	Dvořák	k1gMnSc1
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Štěpánovice	Štěpánovice	k1gFnSc1
–	–	k?
232,5	232,5	k4
g	g	kA
–	–	k?
Muzeum	muzeum	k1gNnSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
vzniku	vznik	k1gInSc2
</s>
<s>
Podle	podle	k7c2
nejpravděpodobnější	pravděpodobný	k2eAgFnSc2d3
teorie	teorie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
vznikly	vzniknout	k5eAaPmAgInP
vltavíny	vltavín	k1gInPc1
společně	společně	k6eAd1
se	s	k7c7
vznikem	vznik	k1gInSc7
Rieského	Rieský	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
mezi	mezi	k7c7
Norimberkem	Norimberk	k1gInSc7
<g/>
,	,	kIx,
Stuttgartem	Stuttgart	k1gInSc7
a	a	k8xC
Mnichovem	Mnichov	k1gInSc7
a	a	k8xC
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
středověké	středověký	k2eAgNnSc1d1
městečko	městečko	k1gNnSc1
Nördlingen	Nördlingen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
městečku	městečko	k1gNnSc6
je	být	k5eAaImIp3nS
muzeum	muzeum	k1gNnSc1
kráteru	kráter	k1gInSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nechybí	chybět	k5eNaImIp3nS,k5eNaPmIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
ani	ani	k8xC
vitrína	vitrína	k1gFnSc1
s	s	k7c7
vltavíny	vltavín	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
kráter	kráter	k1gInSc1
má	mít	k5eAaImIp3nS
oválný	oválný	k2eAgInSc1d1
tvar	tvar	k1gInSc1
připomínající	připomínající	k2eAgInSc1d1
šestiúhelník	šestiúhelník	k1gInSc4
se	s	k7c7
zaoblenými	zaoblený	k2eAgInPc7d1
tvary	tvar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středem	středem	k7c2
Rieského	Rieský	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
prochází	procházet	k5eAaImIp3nS
Švábská	švábský	k2eAgFnSc1d1
tektonická	tektonický	k2eAgFnSc1d1
linie	linie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
patrná	patrný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměr	průměr	k1gInSc1
kráteru	kráter	k1gInSc2
je	být	k5eAaImIp3nS
24	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Rieský	Rieský	k2eAgInSc1d1
meteorit	meteorit	k1gInSc1
měřil	měřit	k5eAaImAgInS
v	v	k7c6
průměru	průměr	k1gInSc6
asi	asi	k9
1	#num#	k4
kilometr	kilometr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
jiných	jiný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
dopadu	dopad	k1gInSc2
není	být	k5eNaImIp3nS
tento	tento	k3xDgInSc4
kráter	kráter	k1gInSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
jediný	jediný	k2eAgMnSc1d1
(	(	kIx(
<g/>
36	#num#	k4
kilometrů	kilometr	k1gInPc2
na	na	k7c4
jihozápad	jihozápad	k1gInSc4
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
Steinheimský	Steinheimský	k2eAgInSc1d1
kráter	kráter	k1gInSc1
o	o	k7c6
průměru	průměr	k1gInSc6
3,8	3,8	k4
kilometru	kilometr	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
rozpadu	rozpad	k1gInSc6
jednoho	jeden	k4xCgNnSc2
tělesa	těleso	k1gNnSc2
při	při	k7c6
průletu	průlet	k1gInSc6
atmosférou	atmosféra	k1gFnSc7
na	na	k7c4
dvě	dva	k4xCgFnPc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
o	o	k7c4
vlétnutí	vlétnutí	k1gNnSc4
dvou	dva	k4xCgFnPc2
těles	těleso	k1gNnPc2
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
bezprostřední	bezprostřední	k2eAgFnSc6d1
době	doba	k1gFnSc6
po	po	k7c6
dopadu	dopad	k1gInSc6
patrně	patrně	k6eAd1
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Čech	Čechy	k1gFnPc2
pokryta	pokrýt	k5eAaPmNgFnS
vltavíny	vltavín	k1gInPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Toto	tento	k3xDgNnSc1
tvrzení	tvrzení	k1gNnSc1
neodpovídá	odpovídat	k5eNaImIp3nS
kumulaci	kumulace	k1gFnSc4
vltavínů	vltavín	k1gInPc2
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohem	mnohem	k6eAd1
spíše	spíše	k9
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
proudy	proud	k1gInPc4
tekutého	tekutý	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vystříkly	vystříknout	k5eAaPmAgFnP
určitými	určitý	k2eAgInPc7d1
směry	směr	k1gInPc7
danými	daný	k2eAgInPc7d1
geomorfologickým	geomorfologický	k2eAgInSc7d1
uspořádání	uspořádání	k1gNnSc4
v	v	k7c6
místě	místo	k1gNnSc6
dopadu	dopad	k1gInSc2
<g/>
...	...	k?
<g/>
)	)	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
V	v	k7c6
jižních	jižní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
dle	dle	k7c2
lidové	lidový	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
býval	bývat	k5eAaImAgMnS
s	s	k7c7
vltavíny	vltavín	k1gInPc7
spojen	spojen	k2eAgInSc4d1
i	i	k8xC
jistý	jistý	k2eAgInSc4d1
svatební	svatební	k2eAgInSc4d1
obřad	obřad	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mladý	mladý	k2eAgMnSc1d1
ženich	ženich	k1gMnSc1
musel	muset	k5eAaImAgMnS
dát	dát	k5eAaPmF
vltavín	vltavín	k1gInSc4
nastávající	nastávající	k2eAgFnSc2d1
nevěstě	nevěsta	k1gFnSc3
jako	jako	k8xC,k8xS
důkaz	důkaz	k1gInSc1
svých	svůj	k3xOyFgInPc2
hlubokých	hluboký	k2eAgInPc2d1
citů	cit	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1
nevybroušený	vybroušený	k2eNgInSc1d1
vltavín	vltavín	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
součástí	součást	k1gFnSc7
platinového	platinový	k2eAgInSc2d1
šperku	šperk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
dostala	dostat	k5eAaPmAgFnS
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
darem	dar	k1gInSc7
od	od	k7c2
švýcarské	švýcarský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
dr	dr	kA
<g/>
.	.	kIx.
Fr.	Fr.	k1gMnSc1
Dvorský	Dvorský	k1gMnSc1
<g/>
:	:	kIx,
O	o	k7c6
vltavínech	vltavín	k1gInPc6
moravských	moravský	k2eAgFnPc2d1
<g/>
,	,	kIx,
Františkovo	Františkův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Annales	Annales	k1gInSc4
č.	č.	k?
<g/>
3	#num#	k4
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
55-73	55-73	k4
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Rudolf	Rudolf	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
:	:	kIx,
O	o	k7c6
vltavínech	vltavín	k1gInPc6
<g/>
,	,	kIx,
Od	od	k7c2
Horácka	Horácko	k1gNnSc2
k	k	k7c3
Podyjí	Podyjí	k1gNnSc3
<g/>
,	,	kIx,
ročník	ročník	k1gInSc1
5	#num#	k4
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
4	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
59-66	59-66	k4
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Zelená	zelený	k2eAgFnSc1d1
horečka	horečka	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
drahokam	drahokam	k1gInSc1
vltavín	vltavín	k1gInSc1
láká	lákat	k5eAaImIp3nS
nelegální	legální	k2eNgInPc4d1
kopáče	kopáč	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
Prague	Pragu	k1gInSc2
International	International	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-08-28	2020-08-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Muzeum	muzeum	k1gNnSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
,	,	kIx,
vltavín	vltavín	k1gInSc1
ze	z	k7c2
Štěpánovic	Štěpánovice	k1gFnPc2
(	(	kIx(
<g/>
232,5	232,5	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Muzeum	muzeum	k1gNnSc1
vltavínů	vltavín	k1gInPc2
v	v	k7c6
Českém	český	k2eAgInSc6d1
Krumlově	Krumlov	k1gInSc6
<g/>
,	,	kIx,
www.vltaviny.cz	www.vltaviny.cz	k1gMnSc1
<g/>
↑	↑	k?
http://is.muni.cz/elportal/estud/pedf/js07/mineraly/materialy/pages/tektity.html	http://is.muni.cz/elportal/estud/pedf/js07/mineraly/materialy/pages/tektity.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.novinky.cz/archiv/Index/Kultura/5461b871.html?from=hp	http://www.novinky.cz/archiv/Index/Kultura/5461b871.html?from=hp	k1gMnSc1
(	(	kIx(
<g/>
archiv	archiv	k1gInSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Muzeum	muzeum	k1gNnSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
moravské	moravský	k2eAgInPc1d1
vltavíny	vltavín	k1gInPc1
<g/>
↑	↑	k?
Rudolf	Rudolfa	k1gFnPc2
Rost	rost	k1gInSc1
<g/>
:	:	kIx,
Základní	základní	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
tektitů	tektit	k1gInPc2
<g/>
,	,	kIx,
Říše	říš	k1gFnSc2
hvězd	hvězda	k1gFnPc2
ročník	ročník	k1gInSc1
50	#num#	k4
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
č.	č.	k?
<g/>
3	#num#	k4
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
S.	S.	kA
Houzar	Houzar	k1gInSc1
<g/>
:	:	kIx,
Vltavíny	vltavín	k1gInPc1
a	a	k8xC
tektity	tektit	k1gInPc1
-	-	kIx~
jejich	jejich	k3xOp3gNnSc1
naleziště	naleziště	k1gNnSc1
a	a	k8xC
vznik	vznik	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.21stoleti.cz	www.21stoleti.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RiesKraterMuseum	RiesKraterMuseum	k1gInSc1
<g/>
,	,	kIx,
Nördlingen	Nördlingen	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
GPS	GPS	kA
<g/>
:	:	kIx,
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.mfplus.cz	www.mfplus.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vltavín	vltavín	k1gInSc1
alias	alias	k9
moldavit	moldavit	k1gInSc1
<g/>
:	:	kIx,
záhadný	záhadný	k2eAgInSc1d1
český	český	k2eAgInSc1d1
drahokam	drahokam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
KLENOTA	KLENOTA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
OSWALD	OSWALD	kA
<g/>
:	:	kIx,
Meteorické	meteorický	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
,	,	kIx,
93	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
ROST	rost	k1gInSc1
<g/>
:	:	kIx,
Vltavíny	vltavín	k1gInPc1
a	a	k8xC
tektity	tektit	k1gInPc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1972	#num#	k4
</s>
<s>
Milan	Milan	k1gMnSc1
TRNKA	Trnka	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
HOUZAR	HOUZAR	kA
<g/>
:	:	kIx,
Moravské	moravský	k2eAgInPc1d1
vltavíny	vltavín	k1gInPc1
<g/>
,	,	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1991	#num#	k4
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
BOUŠKA	Bouška	k1gMnSc1
<g/>
:	:	kIx,
Tajemné	tajemný	k2eAgInPc4d1
vltavíny	vltavín	k1gInPc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1992	#num#	k4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1994	#num#	k4
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
BOUŠKA	Bouška	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
KONTA	konto	k1gNnSc2
<g/>
:	:	kIx,
Moldavites	Moldavitesa	k1gFnPc2
–	–	k?
Vltavíny	vltavín	k1gInPc1
<g/>
,	,	kIx,
Universita	universita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1999	#num#	k4
</s>
<s>
Radek	Radek	k1gMnSc1
HANUS	Hanus	k1gMnSc1
<g/>
:	:	kIx,
České	český	k2eAgFnPc1d1
a	a	k8xC
moravské	moravský	k2eAgInPc1d1
vltavíny	vltavín	k1gInPc1
<g/>
,	,	kIx,
Granit	granit	k1gInSc1
2015	#num#	k4
</s>
<s>
J.	J.	kA
BAIER	Baier	k1gMnSc1
<g/>
:	:	kIx,
Zur	Zur	k1gMnSc1
Herkunft	Herkunft	k1gMnSc1
und	und	k?
Bedeutung	Bedeutung	k1gMnSc1
der	drát	k5eAaImRp2nS
Ries-Auswurfprodukte	Ries-Auswurfprodukt	k1gInSc5
für	für	k?
den	den	k1gInSc1
Impakt-Mechanismus	Impakt-Mechanismus	k1gInSc1
<g/>
.	.	kIx.
in	in	k?
<g/>
:	:	kIx,
Jahrbericht	Jahrbericht	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mitteilungen	Mitteilungen	k1gInSc1
oberrheinischen	oberrheinischen	k2eAgInSc1d1
geologischen	geologischen	k2eAgInSc1d1
Verein	Verein	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
F.	F.	kA
91	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
J.	J.	kA
BAIER	Baier	k1gMnSc1
Die	Die	k1gMnSc1
Auswurfprodukte	Auswurfprodukt	k1gInSc5
des	des	k1gNnPc7
Ries-Impakts	Ries-Impakts	k1gInSc1
<g/>
,	,	kIx,
Deutschland	Deutschland	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
Documenta	Documenta	k1gFnSc1
Naturae	Naturae	k1gFnSc1
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
162	#num#	k4
<g/>
,	,	kIx,
München	München	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-3-86544-162-1	978-3-86544-162-1	k4
ISSN	ISSN	kA
0723-8428	0723-8428	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vltavín	vltavín	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
vltavín	vltavín	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
jsou	být	k5eAaImIp3nP
vltavíny	vltavín	k1gInPc1
</s>
<s>
Muzeum	muzeum	k1gNnSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
tektitů	tektit	k1gInPc2
</s>
<s>
Fotografie	fotografia	k1gFnPc1
vltavínů	vltavín	k1gInPc2
na	na	k7c6
Pinterestu	Pinterest	k1gInSc6
</s>
<s>
Fotografie	fotografia	k1gFnPc1
vltavínů	vltavín	k1gInPc2
ze	z	k7c2
sbírky	sbírka	k1gFnSc2
MUDr.	MUDr.	kA
Šilhavého	šilhavý	k2eAgMnSc2d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
127139	#num#	k4
</s>
