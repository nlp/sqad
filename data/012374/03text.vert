<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
(	(	kIx(	(
<g/>
Parnassius	Parnassius	k1gInSc1	Parnassius
apollo	apollo	k1gNnSc1	apollo
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
motýla	motýl	k1gMnSc2	motýl
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
otakárkovitých	otakárkovitý	k2eAgFnPc2d1	otakárkovitý
(	(	kIx(	(
<g/>
Papilionidae	Papilionidae	k1gFnPc2	Papilionidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
jasoňů	jasoň	k1gMnPc2	jasoň
(	(	kIx(	(
<g/>
Parnassius	Parnassius	k1gMnSc1	Parnassius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
celosvětového	celosvětový	k2eAgNnSc2d1	celosvětové
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
znovu	znovu	k6eAd1	znovu
vysazen	vysazen	k2eAgInSc1d1	vysazen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
zákonů	zákon	k1gInPc2	zákon
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnost	vzácnost	k1gFnSc1	vzácnost
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
oddělenost	oddělenost	k1gFnSc1	oddělenost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
existenci	existence	k1gFnSc3	existence
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
poddruhů	poddruh	k1gInPc2	poddruh
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velký	velký	k2eAgMnSc1d1	velký
motýl	motýl	k1gMnSc1	motýl
(	(	kIx(	(
<g/>
rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8,4	[number]	k4	8,4
cm	cm	kA	cm
<g/>
)	)	kIx)	)
s	s	k7c7	s
bílými	bílý	k2eAgNnPc7d1	bílé
křídly	křídlo	k1gNnPc7	křídlo
s	s	k7c7	s
černými	černý	k1gMnPc7	černý
a	a	k8xC	a
červenými	červený	k2eAgFnPc7d1	červená
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
housenka	housenka	k1gFnSc1	housenka
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
s	s	k7c7	s
oranžovými	oranžový	k2eAgFnPc7d1	oranžová
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
kukla	kuknout	k5eAaPmAgFnS	kuknout
je	on	k3xPp3gNnPc4	on
červenohnědá	červenohnědý	k2eAgNnPc4d1	červenohnědé
<g/>
,	,	kIx,	,
ojíněná	ojíněný	k2eAgNnPc4d1	ojíněné
modrobílým	modrobílý	k2eAgInSc7d1	modrobílý
práškem	prášek	k1gInSc7	prášek
<g/>
,	,	kIx,	,
vajíčko	vajíčko	k1gNnSc1	vajíčko
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
1,5	[number]	k4	1,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
alpínských	alpínský	k2eAgInPc6d1	alpínský
ekosystémech	ekosystém	k1gInPc6	ekosystém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
horských	horský	k2eAgFnPc6d1	horská
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
části	část	k1gFnSc3	část
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Housenky	housenka	k1gFnPc1	housenka
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
rozchodníky	rozchodník	k1gInPc1	rozchodník
(	(	kIx(	(
<g/>
Sedum	Sedum	k1gInSc1	Sedum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
například	například	k6eAd1	například
nektarem	nektar	k1gInSc7	nektar
fialově	fialově	k6eAd1	fialově
kvetoucími	kvetoucí	k2eAgInPc7d1	kvetoucí
druhy	druh	k1gInPc7	druh
bodláků	bodlák	k1gInPc2	bodlák
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
heliofilní	heliofilní	k2eAgInSc4d1	heliofilní
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
především	především	k9	především
v	v	k7c6	v
období	období	k1gNnSc6	období
největšího	veliký	k2eAgInSc2d3	veliký
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
třepetavý	třepetavý	k2eAgInSc1d1	třepetavý
let	let	k1gInSc1	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
generaci	generace	k1gFnSc4	generace
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
létá	létat	k5eAaImIp3nS	létat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
páří	pářit	k5eAaImIp3nP	pářit
a	a	k8xC	a
nakladou	naklást	k5eAaPmIp3nP	naklást
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
housenek	housenka	k1gFnPc2	housenka
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
líhne	líhnout	k5eAaImIp3nS	líhnout
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Housenky	housenka	k1gFnPc1	housenka
se	se	k3xPyFc4	se
zakuklují	zakuklovat	k5eAaImIp3nP	zakuklovat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
vykuklí	vykuklit	k5eAaPmIp3nS	vykuklit
se	se	k3xPyFc4	se
asi	asi	k9	asi
za	za	k7c4	za
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
motýli	motýl	k1gMnPc1	motýl
prochází	procházet	k5eAaImIp3nS	procházet
jasoň	jasoň	k1gMnSc1	jasoň
červenooký	červenooký	k2eAgInSc1d1	červenooký
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
proměnou	proměna	k1gFnSc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgNnPc1	čtyři
stádia	stádium	k1gNnPc1	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
larva	larva	k1gFnSc1	larva
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
motýlů	motýl	k1gMnPc2	motýl
housenka	housenka	k1gFnSc1	housenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgMnSc6	třetí
kukla	kukla	k1gFnSc1	kukla
a	a	k8xC	a
posledním	poslední	k2eAgMnSc6d1	poslední
dospělec	dospělec	k1gMnSc1	dospělec
neboli	neboli	k8xC	neboli
imágo	imágo	k1gMnSc1	imágo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc4	všechen
tato	tento	k3xDgNnPc4	tento
stádia	stádium	k1gNnPc4	stádium
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vajíčko	vajíčko	k1gNnSc1	vajíčko
===	===	k?	===
</s>
</p>
<p>
<s>
Vajíčko	vajíčko	k1gNnSc1	vajíčko
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
bělavou	bělavý	k2eAgFnSc4d1	bělavá
<g/>
,	,	kIx,	,
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
1,7	[number]	k4	1,7
mm	mm	kA	mm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
asi	asi	k9	asi
0,9	[number]	k4	0,9
<g/>
–	–	k?	–
<g/>
1,05	[number]	k4	1,05
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčko	vajíčko	k1gNnSc1	vajíčko
je	být	k5eAaImIp3nS	být
dole	dole	k6eAd1	dole
zploštělé	zploštělý	k2eAgNnSc1d1	zploštělé
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
mírně	mírně	k6eAd1	mírně
vyduté	vydutý	k2eAgFnPc1d1	vydutá
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
vajíčka	vajíčko	k1gNnSc2	vajíčko
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
drsný	drsný	k2eAgInSc4d1	drsný
a	a	k8xC	a
nerovnoměrný	rovnoměrný	k2eNgInSc4d1	nerovnoměrný
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčko	vajíčko	k1gNnSc1	vajíčko
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
nažloutle	nažloutle	k6eAd1	nažloutle
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
létě	léto	k1gNnSc6	léto
má	mít	k5eAaImIp3nS	mít
namodrale	namodrale	k6eAd1	namodrale
bílý	bílý	k2eAgInSc1d1	bílý
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Housenka	housenka	k1gFnSc1	housenka
===	===	k?	===
</s>
</p>
<p>
<s>
Housenka	housenka	k1gFnSc1	housenka
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
až	až	k9	až
50	[number]	k4	50
mm	mm	kA	mm
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
sametově	sametově	k6eAd1	sametově
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
masitý	masitý	k2eAgInSc4d1	masitý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žlutě	žlutě	k6eAd1	žlutě
nebo	nebo	k8xC	nebo
oranžově	oranžově	k6eAd1	oranžově
tečkovaná	tečkovaný	k2eAgFnSc1d1	tečkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
housenky	housenka	k1gFnSc2	housenka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nepravidelném	pravidelný	k2eNgNnSc6d1	nepravidelné
rozložení	rozložení	k1gNnSc6	rozložení
drobné	drobný	k2eAgFnSc2d1	drobná
modravé	modravý	k2eAgFnSc2d1	modravá
bradavky	bradavka	k1gFnSc2	bradavka
<g/>
,	,	kIx,	,
za	za	k7c7	za
hlavou	hlava	k1gFnSc7	hlava
má	mít	k5eAaImIp3nS	mít
masité	masitý	k2eAgNnSc1d1	masité
červenooranžové	červenooranžový	k2eAgNnSc1d1	červenooranžové
osmeterium	osmeterium	k1gNnSc1	osmeterium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
podráždění	podráždění	k1gNnSc6	podráždění
vysouvá	vysouvat	k5eAaImIp3nS	vysouvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kukla	Kukla	k1gMnSc1	Kukla
===	===	k?	===
</s>
</p>
<p>
<s>
Kukla	Kukla	k1gMnSc1	Kukla
má	mít	k5eAaImIp3nS	mít
červenohnědou	červenohnědý	k2eAgFnSc4d1	červenohnědá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
ojíněná	ojíněný	k2eAgNnPc4d1	ojíněné
modrobílým	modrobílý	k2eAgInSc7d1	modrobílý
práškem	prášek	k1gInSc7	prášek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k9	jako
namodrale	namodrale	k6eAd1	namodrale
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
a	a	k8xC	a
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Imágo	Imágo	k1gMnSc1	Imágo
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
denním	denní	k2eAgMnPc3d1	denní
motýlům	motýl	k1gMnPc3	motýl
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozpětí	rozpětí	k1gNnSc2	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8,4	[number]	k4	8,4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bíle	bíle	k6eAd1	bíle
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
<g/>
,	,	kIx,	,
přední	přední	k2eAgNnPc4d1	přední
křídla	křídlo	k1gNnPc4	křídlo
mají	mít	k5eAaImIp3nP	mít
tmavé	tmavý	k2eAgInPc1d1	tmavý
<g/>
,	,	kIx,	,
průsvitné	průsvitný	k2eAgInPc1d1	průsvitný
okraje	okraj	k1gInPc1	okraj
(	(	kIx(	(
<g/>
okraje	okraj	k1gInPc1	okraj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
průsvitná	průsvitný	k2eAgFnSc1d1	průsvitná
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
většina	většina	k1gFnSc1	většina
křídla	křídlo	k1gNnSc2	křídlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
křídlech	křídlo	k1gNnPc6	křídlo
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
černých	černý	k2eAgFnPc2d1	černá
skvrn	skvrna	k1gFnPc2	skvrna
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
umístění	umístění	k1gNnSc1	umístění
se	se	k3xPyFc4	se
ale	ale	k9	ale
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
jedinců	jedinec	k1gMnPc2	jedinec
příliš	příliš	k6eAd1	příliš
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Průsvitné	průsvitný	k2eAgNnSc1d1	průsvitné
lemování	lemování	k1gNnSc1	lemování
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
na	na	k7c6	na
zadních	zadní	k2eAgNnPc6d1	zadní
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
předními	přední	k2eAgNnPc7d1	přední
křídly	křídlo	k1gNnPc7	křídlo
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
užší	úzký	k2eAgNnSc1d2	užší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadních	zadní	k2eAgNnPc6d1	zadní
křídlech	křídlo	k1gNnPc6	křídlo
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
čtyři	čtyři	k4xCgFnPc1	čtyři
červené	červená	k1gFnPc1	červená
<g/>
,	,	kIx,	,
černě	čerň	k1gFnPc1	čerň
lemované	lemovaný	k2eAgFnSc2d1	lemovaná
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nachází	nacházet	k5eAaImIp3nS	nacházet
bílá	bílý	k2eAgFnSc1d1	bílá
tečka	tečka	k1gFnSc1	tečka
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
také	také	k9	také
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
křídel	křídlo	k1gNnPc2	křídlo
bývá	bývat	k5eAaImIp3nS	bývat
několik	několik	k4yIc1	několik
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
černých	černý	k2eAgFnPc2d1	černá
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
červeně	červeně	k6eAd1	červeně
vyplněných	vyplněný	k2eAgFnPc2d1	vyplněná
skvrnek	skvrnka	k1gFnPc2	skvrnka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tykadla	tykadlo	k1gNnPc1	tykadlo
jsou	být	k5eAaImIp3nP	být
šedočerná	šedočerný	k2eAgNnPc1d1	šedočerné
<g/>
,	,	kIx,	,
krátká	krátký	k2eAgNnPc1d1	krátké
a	a	k8xC	a
zakončená	zakončený	k2eAgNnPc1d1	zakončené
černým	černý	k2eAgNnSc7d1	černé
zvětšením	zvětšení	k1gNnSc7	zvětšení
kyjovitého	kyjovitý	k2eAgInSc2d1	kyjovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
těla	tělo	k1gNnSc2	tělo
samečků	sameček	k1gMnPc2	sameček
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
bílými	bílý	k2eAgInPc7d1	bílý
chloupky	chloupek	k1gInPc7	chloupek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
<g/>
,	,	kIx,	,
samička	samička	k1gFnSc1	samička
je	být	k5eAaImIp3nS	být
ochlupená	ochlupený	k2eAgFnSc1d1	ochlupená
řídce	řídce	k6eAd1	řídce
či	či	k8xC	či
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc1	tři
páry	pára	k1gFnPc1	pára
plnohodnotných	plnohodnotný	k2eAgFnPc2d1	plnohodnotná
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
vzhled	vzhled	k1gInSc1	vzhled
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
jedinců	jedinec	k1gMnPc2	jedinec
různě	různě	k6eAd1	různě
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaBmF	nalézt
dva	dva	k4xCgInPc4	dva
zcela	zcela	k6eAd1	zcela
shodné	shodný	k2eAgMnPc4d1	shodný
jedince	jedinec	k1gMnPc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
odchylky	odchylka	k1gFnPc4	odchylka
mezi	mezi	k7c7	mezi
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
zapříčinily	zapříčinit	k5eAaPmAgFnP	zapříčinit
vznik	vznik	k1gInSc4	vznik
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgInPc2d1	různý
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
odchylek	odchylka	k1gFnPc2	odchylka
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Odlišnost	odlišnost	k1gFnSc1	odlišnost
těchto	tento	k3xDgInPc2	tento
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
izolací	izolace	k1gFnSc7	izolace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
popisu	popis	k1gInSc2	popis
druhu	druh	k1gInSc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
čtyř	čtyři	k4xCgMnPc2	čtyři
významných	významný	k2eAgMnPc2d1	významný
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
anglického	anglický	k2eAgMnSc2d1	anglický
entomologa	entomolog	k1gMnSc2	entomolog
Thomase	Thomas	k1gMnSc2	Thomas
Mouffata	Mouffat	k1gMnSc2	Mouffat
Insectorum	Insectorum	k1gInSc1	Insectorum
sive	sivat	k5eAaPmIp3nS	sivat
minimorum	minimorum	k1gInSc4	minimorum
animalium	animalium	k1gNnSc4	animalium
theatrum	theatrum	k1gNnSc4	theatrum
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
vědecký	vědecký	k2eAgInSc1d1	vědecký
název	název	k1gInSc1	název
jasoně	jasoň	k1gMnSc2	jasoň
červenookého	červenooký	k2eAgMnSc2d1	červenooký
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Carl	Carla	k1gFnPc2	Carla
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
poprvé	poprvé	k6eAd1	poprvé
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
v	v	k7c6	v
desátém	desátý	k4xOgInSc6	desátý
vydání	vydání	k1gNnSc6	vydání
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
Systema	Systema	k1gFnSc1	Systema
naturae	naturae	k1gFnSc1	naturae
jako	jako	k8xS	jako
Papilio	Papilio	k1gNnSc1	Papilio
apollo	apolnout	k5eAaPmAgNnS	apolnout
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
byl	být	k5eAaImAgMnS	být
druh	druh	k1gMnSc1	druh
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
díle	dílo	k1gNnSc6	dílo
popsán	popsán	k2eAgMnSc1d1	popsán
<g/>
,	,	kIx,	,
pocházeli	pocházet	k5eAaImAgMnP	pocházet
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Druhový	druhový	k2eAgInSc1d1	druhový
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
boha	bůh	k1gMnSc2	bůh
Apollóna	Apollón	k1gMnSc2	Apollón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
bohem	bůh	k1gMnSc7	bůh
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ochráncem	ochránce	k1gMnSc7	ochránce
života	život	k1gInSc2	život
a	a	k8xC	a
pořádku	pořádek	k1gInSc2	pořádek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
reliktní	reliktní	k2eAgInSc1d1	reliktní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
úplně	úplně	k6eAd1	úplně
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
exemplář	exemplář	k1gInSc1	exemplář
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
poblíž	poblíž	k7c2	poblíž
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
v	v	k7c6	v
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
lomu	lom	k1gInSc6	lom
Horní	horní	k2eAgFnSc1d1	horní
Kamenárka	Kamenárka	k1gFnSc1	Kamenárka
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Štramberk	Štramberk	k1gInSc1	Štramberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jasoň	jasoň	k1gMnSc1	jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
původně	původně	k6eAd1	původně
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
<g/>
,	,	kIx,	,
vysazeni	vysazen	k2eAgMnPc1d1	vysazen
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
odchycení	odchycený	k2eAgMnPc1d1	odchycený
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Úplná	úplný	k2eAgFnSc1d1	úplná
reintrodukce	reintrodukce	k1gFnSc1	reintrodukce
jasoně	jasoň	k1gMnSc2	jasoň
červenookého	červenooký	k2eAgMnSc2d1	červenooký
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kategorizace	kategorizace	k1gFnSc2	kategorizace
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
jako	jako	k8xS	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
klasifikace	klasifikace	k1gFnSc1	klasifikace
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
jasoň	jasoň	k1gMnSc1	jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
hodnocen	hodnocen	k2eAgMnSc1d1	hodnocen
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
(	(	kIx(	(
<g/>
gefährdet	gefährdet	k1gMnSc1	gefährdet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
CITES	CITES	kA	CITES
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
příloze	příloha	k1gFnSc6	příloha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchodování	obchodování	k1gNnSc1	obchodování
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
omezeno	omezit	k5eAaPmNgNnS	omezit
a	a	k8xC	a
podřízeno	podřídit	k5eAaPmNgNnS	podřídit
dozoru	dozor	k1gInSc6	dozor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
ohrožení	ohrožení	k1gNnSc2	ohrožení
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
biotopů	biotop	k1gInPc2	biotop
a	a	k8xC	a
protizákonný	protizákonný	k2eAgInSc1d1	protizákonný
odchyt	odchyt	k1gInSc1	odchyt
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
tedy	tedy	k9	tedy
spočívá	spočívat	k5eAaImIp3nS	spočívat
především	především	k9	především
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
jeho	jeho	k3xOp3gInPc2	jeho
přirozených	přirozený	k2eAgInPc2d1	přirozený
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
také	také	k9	také
ochrana	ochrana	k1gFnSc1	ochrana
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
před	před	k7c7	před
případným	případný	k2eAgInSc7d1	případný
nekontrolovaným	kontrolovaný	k2eNgInSc7d1	nekontrolovaný
a	a	k8xC	a
masovým	masový	k2eAgInSc7d1	masový
odchytem	odchyt	k1gInSc7	odchyt
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
sběratelů	sběratel	k1gMnPc2	sběratel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
nebyl	být	k5eNaImAgInS	být
výskyt	výskyt	k1gInSc1	výskyt
dosud	dosud	k6eAd1	dosud
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
také	také	k9	také
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
a	a	k8xC	a
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
byl	být	k5eAaImAgInS	být
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jediném	jediný	k2eAgNnSc6d1	jediné
místě	místo	k1gNnSc6	místo
poblíž	poblíž	k7c2	poblíž
Štramberka	Štramberk	k1gInSc2	Štramberk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
jasoně	jasoň	k1gMnSc2	jasoň
červenookého	červenooký	k2eAgMnSc2d1	červenooký
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
jeho	jeho	k3xOp3gFnPc2	jeho
hostitelských	hostitelský	k2eAgFnPc2d1	hostitelská
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
vázán	vázán	k2eAgInSc1d1	vázán
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
jasoně	jasoň	k1gMnSc2	jasoň
červeného	červené	k1gNnSc2	červené
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
karpatské	karpatský	k2eAgFnSc6d1	Karpatská
oblasti	oblast	k1gFnSc6	oblast
nejčastější	častý	k2eAgFnSc2d3	nejčastější
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
asi	asi	k9	asi
800	[number]	k4	800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
do	do	k7c2	do
asi	asi	k9	asi
1300	[number]	k4	1300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
převážně	převážně	k6eAd1	převážně
vysokohorský	vysokohorský	k2eAgInSc1d1	vysokohorský
<g/>
.	.	kIx.	.
</s>
<s>
Častými	častý	k2eAgInPc7d1	častý
biotopy	biotop	k1gInPc7	biotop
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
skalnaté	skalnatý	k2eAgFnPc1d1	skalnatá
stráně	stráň	k1gFnPc1	stráň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vápencové	vápencový	k2eAgFnPc4d1	vápencová
sutě	suť	k1gFnPc4	suť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
zarostlé	zarostlý	k2eAgInPc1d1	zarostlý
stromy	strom	k1gInPc1	strom
ani	ani	k8xC	ani
keři	keř	k1gInPc7	keř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
údolí	údolí	k1gNnPc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnPc1d3	nejvhodnější
místa	místo	k1gNnPc1	místo
jeho	jeho	k3xOp3gInPc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
přístupné	přístupný	k2eAgInPc1d1	přístupný
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
svahy	svah	k1gInPc1	svah
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
výskytem	výskyt	k1gInSc7	výskyt
vápnomilných	vápnomilný	k2eAgFnPc2d1	vápnomilná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
většinu	většina	k1gFnSc4	většina
lokalit	lokalita	k1gFnPc2	lokalita
jeho	on	k3xPp3gInSc2	on
výskytu	výskyt	k1gInSc2	výskyt
přiřadit	přiřadit	k5eAaPmF	přiřadit
k	k	k7c3	k
alpínským	alpínský	k2eAgInPc3d1	alpínský
ekosystémům	ekosystém	k1gInPc3	ekosystém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Imága	Imága	k1gFnSc1	Imága
(	(	kIx(	(
<g/>
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
motýlů	motýl	k1gMnPc2	motýl
<g/>
)	)	kIx)	)
vylétají	vylétat	k5eAaPmIp3nP	vylétat
asi	asi	k9	asi
od	od	k7c2	od
devíti	devět	k4xCc2	devět
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
ale	ale	k8xC	ale
okolo	okolo	k7c2	okolo
poledne	poledne	k1gNnSc2	poledne
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
druh	druh	k1gInSc4	druh
heliofilní	heliofilní	k2eAgInSc4d1	heliofilní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
bodláků	bodlák	k1gInPc2	bodlák
<g/>
,	,	kIx,	,
chrastavce	chrastavec	k1gInPc1	chrastavec
<g/>
,	,	kIx,	,
hvozdík	hvozdík	k1gInSc1	hvozdík
kartouzek	kartouzek	k1gInSc1	kartouzek
(	(	kIx(	(
<g/>
Dianthus	Dianthus	k1gInSc1	Dianthus
carthusianorum	carthusianorum	k1gInSc1	carthusianorum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
netřesky	netřesk	k1gInPc4	netřesk
<g/>
,	,	kIx,	,
starčky	starček	k1gInPc4	starček
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bodláků	bodlák	k1gInPc2	bodlák
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
fialově	fialově	k6eAd1	fialově
kvetoucí	kvetoucí	k2eAgInPc1d1	kvetoucí
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
chrastavců	chrastavec	k1gInPc2	chrastavec
druhy	druh	k1gInPc1	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Knautia	Knautium	k1gNnSc2	Knautium
<g/>
.	.	kIx.	.
</s>
<s>
Imága	Imág	k1gMnSc4	Imág
létají	létat	k5eAaImIp3nP	létat
také	také	k9	také
na	na	k7c4	na
květy	květ	k1gInPc4	květ
dobromysli	dobromysl	k1gFnSc2	dobromysl
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Origanum	Origanum	k1gInSc1	Origanum
vulgare	vulgar	k1gMnSc5	vulgar
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
květy	květ	k1gInPc4	květ
hostitelských	hostitelský	k2eAgFnPc2d1	hostitelská
rostlin	rostlina	k1gFnPc2	rostlina
housenek	housenka	k1gFnPc2	housenka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
rozchodníků	rozchodník	k1gInPc2	rozchodník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Sedum	Sedum	k1gInSc1	Sedum
(	(	kIx(	(
<g/>
rozchodník	rozchodník	k1gInSc1	rozchodník
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
Sedum	Sedum	k1gInSc1	Sedum
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozchodník	rozchodník	k1gInSc1	rozchodník
velký	velký	k2eAgInSc1d1	velký
(	(	kIx(	(
<g/>
Sedum	Sedum	k1gInSc1	Sedum
maximum	maximum	k1gNnSc1	maximum
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Hylotelephium	Hylotelephium	k1gNnSc1	Hylotelephium
maximum	maximum	k1gNnSc1	maximum
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květech	květ	k1gInPc6	květ
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
přenocují	přenocovat	k5eAaPmIp3nP	přenocovat
<g/>
.	.	kIx.	.
</s>
<s>
Létají	létat	k5eAaImIp3nP	létat
velmi	velmi	k6eAd1	velmi
pomalým	pomalý	k2eAgInSc7d1	pomalý
<g/>
,	,	kIx,	,
třepetavým	třepetavý	k2eAgInSc7d1	třepetavý
letem	let	k1gInSc7	let
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zasednutí	zasednutí	k1gNnSc6	zasednutí
na	na	k7c4	na
květ	květ	k1gInSc4	květ
imágo	imágo	k6eAd1	imágo
roztáhne	roztáhnout	k5eAaPmIp3nS	roztáhnout
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
zanoří	zanořit	k5eAaPmIp3nS	zanořit
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
okvětních	okvětní	k2eAgFnPc2d1	okvětní
částí	část	k1gFnPc2	část
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc4	slunce
zakryté	zakrytý	k2eAgInPc4d1	zakrytý
mraky	mrak	k1gInPc4	mrak
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
imága	imága	k1gFnSc1	imága
značně	značně	k6eAd1	značně
letargická	letargický	k2eAgFnSc1d1	letargická
(	(	kIx(	(
<g/>
netečná	tečný	k2eNgFnSc1d1	netečná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nelétají	létat	k5eNaImIp3nP	létat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
objeveny	objevit	k5eAaPmNgInP	objevit
na	na	k7c6	na
květu	květ	k1gInSc6	květ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
a	a	k8xC	a
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Imága	Imág	k1gMnSc4	Imág
žijí	žít	k5eAaImIp3nP	žít
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
monocyklickým	monocyklický	k2eAgInPc3d1	monocyklický
druhům	druh	k1gInPc3	druh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
generaci	generace	k1gFnSc4	generace
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
období	období	k1gNnSc4	období
výskytu	výskyt	k1gInSc2	výskyt
jasoně	jasoň	k1gMnSc2	jasoň
červenookého	červenooký	k2eAgInSc2d1	červenooký
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
bezchybné	bezchybný	k2eAgInPc1d1	bezchybný
exempláře	exemplář	k1gInPc1	exemplář
samečků	sameček	k1gMnPc2	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
(	(	kIx(	(
<g/>
čtyřech	čtyři	k4xCgMnPc6	čtyři
až	až	k8xS	až
sedmi	sedm	k4xCc7	sedm
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgInPc1	první
exempláře	exemplář	k1gInPc1	exemplář
samiček	samička	k1gFnPc2	samička
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
létají	létat	k5eAaImIp3nP	létat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
těžkopádněji	těžkopádně	k6eAd2	těžkopádně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pomalejší	pomalý	k2eAgInPc1d2	pomalejší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vyrušeny	vyrušen	k2eAgFnPc1d1	vyrušena
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
velmi	velmi	k6eAd1	velmi
prudce	prudko	k6eAd1	prudko
vyletět	vyletět	k5eAaPmF	vyletět
a	a	k8xC	a
ztratit	ztratit	k5eAaPmF	ztratit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sutí	suť	k1gFnSc7	suť
<g/>
.	.	kIx.	.
</s>
<s>
Samiček	samička	k1gFnPc2	samička
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
než	než	k8xS	než
samečků	sameček	k1gMnPc2	sameček
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
samečků	sameček	k1gMnPc2	sameček
k	k	k7c3	k
samičkám	samička	k1gFnPc3	samička
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jsou	být	k5eAaImIp3nP	být
samičky	samička	k1gFnPc1	samička
oplodněny	oplodněn	k2eAgMnPc4d1	oplodněn
samečky	sameček	k1gMnPc4	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Kopulace	kopulace	k1gFnSc1	kopulace
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
travnatém	travnatý	k2eAgInSc6d1	travnatý
porostu	porost	k1gInSc6	porost
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
na	na	k7c6	na
květech	květ	k1gInPc6	květ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samička	samička	k1gFnSc1	samička
obyčejně	obyčejně	k6eAd1	obyčejně
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
květu	květ	k1gInSc6	květ
hlavou	hlava	k1gFnSc7	hlava
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
sameček	sameček	k1gMnSc1	sameček
visí	viset	k5eAaImIp3nS	viset
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
kopulace	kopulace	k1gFnSc1	kopulace
probíhá	probíhat	k5eAaImIp3nS	probíhat
i	i	k9	i
volně	volně	k6eAd1	volně
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
osm	osm	k4xCc4	osm
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
kopulace	kopulace	k1gFnSc2	kopulace
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
samičce	samičko	k6eAd1	samičko
na	na	k7c6	na
konci	konec	k1gInSc6	konec
bříška	bříško	k1gNnSc2	bříško
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
tuhý	tuhý	k2eAgInSc4d1	tuhý
útvar	útvar	k1gInSc4	útvar
ze	z	k7c2	z
sekretu	sekret	k1gInSc2	sekret
samečkových	samečkův	k2eAgFnPc2d1	samečkův
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
sphragis	sphragis	k1gInSc1	sphragis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
že	že	k8xS	že
samička	samička	k1gFnSc1	samička
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
oplodněna	oplodněn	k2eAgFnSc1d1	oplodněna
<g/>
.	.	kIx.	.
<g/>
Jeden	jeden	k4xCgInSc1	jeden
až	až	k9	až
dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
po	po	k7c6	po
oplodnění	oplodnění	k1gNnSc6	oplodnění
začíná	začínat	k5eAaImIp3nS	začínat
samička	samička	k1gFnSc1	samička
klást	klást	k5eAaImF	klást
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
je	být	k5eAaImIp3nS	být
jednotlivě	jednotlivě	k6eAd1	jednotlivě
na	na	k7c4	na
hostitelské	hostitelský	k2eAgFnPc4d1	hostitelská
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c4	na
suchá	suchý	k2eAgNnPc4d1	suché
místa	místo	k1gNnPc4	místo
poblíž	poblíž	k6eAd1	poblíž
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kameny	kámen	k1gInPc4	kámen
či	či	k8xC	či
suché	suchý	k2eAgFnPc4d1	suchá
větvičky	větvička	k1gFnPc4	větvička
a	a	k8xC	a
stonky	stonek	k1gInPc4	stonek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vajíček	vajíčko	k1gNnPc2	vajíčko
samička	samička	k1gFnSc1	samička
snáší	snášet	k5eAaImIp3nS	snášet
různý	různý	k2eAgInSc4d1	různý
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
osmdesát	osmdesát	k4xCc4	osmdesát
až	až	k9	až
sto	sto	k4xCgNnSc4	sto
padesát	padesát	k4xCc4	padesát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
samička	samička	k1gFnSc1	samička
snese	snést	k5eAaPmIp3nS	snést
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
kvality	kvalita	k1gFnPc4	kvalita
<g/>
,	,	kIx,	,
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
vajíček	vajíčko	k1gNnPc2	vajíčko
se	se	k3xPyFc4	se
housenky	housenka	k1gFnPc1	housenka
líhnou	líhnout	k5eAaImIp3nP	líhnout
jen	jen	k9	jen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgFnSc3d1	omezená
míře	míra	k1gFnSc3	míra
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
housenek	housenka	k1gFnPc2	housenka
(	(	kIx(	(
<g/>
asi	asi	k9	asi
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
z	z	k7c2	z
vajíček	vajíčko	k1gNnPc2	vajíčko
vylíhne	vylíhnout	k5eAaPmIp3nS	vylíhnout
ještě	ještě	k9	ještě
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
a	a	k8xC	a
přezimují	přezimovat	k5eAaBmIp3nP	přezimovat
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
housenky	housenka	k1gFnSc2	housenka
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
druhém	druhý	k4xOgInSc6	druhý
instaru	instar	k1gInSc6	instar
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přezimovala	přezimovat	k5eAaBmAgFnS	přezimovat
jako	jako	k9	jako
vajíčko	vajíčko	k1gNnSc4	vajíčko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vylíhne	vylíhnout	k5eAaPmIp3nS	vylíhnout
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Housenka	housenka	k1gFnSc1	housenka
a	a	k8xC	a
kukla	kukla	k1gFnSc1	kukla
==	==	k?	==
</s>
</p>
<p>
<s>
Housenka	housenka	k1gFnSc1	housenka
se	se	k3xPyFc4	se
při	při	k7c6	při
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jenom	jenom	k9	jenom
vykouše	vykousat	k5eAaPmIp3nS	vykousat
z	z	k7c2	z
obalu	obal	k1gInSc2	obal
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
,	,	kIx,	,
obal	obal	k1gInSc4	obal
ale	ale	k8xC	ale
nepožírá	požírat	k5eNaImIp3nS	požírat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
přeleze	přelézt	k5eAaPmIp3nS	přelézt
na	na	k7c4	na
lístky	lístek	k1gInPc4	lístek
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požírá	požírat	k5eAaImIp3nS	požírat
jejich	jejich	k3xOp3gFnPc4	jejich
měkké	měkký	k2eAgFnPc4d1	měkká
<g/>
,	,	kIx,	,
okrajové	okrajový	k2eAgFnPc4d1	okrajová
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
housenky	housenka	k1gFnSc2	housenka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
instarů	instar	k1gInPc2	instar
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
doroste	dorůst	k5eAaPmIp3nS	dorůst
přibližně	přibližně	k6eAd1	přibližně
koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
posledního	poslední	k2eAgInSc2d1	poslední
instaru	instar	k1gInSc2	instar
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
housenka	housenka	k1gFnSc1	housenka
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
asi	asi	k9	asi
45	[number]	k4	45
–	–	k?	–
52	[number]	k4	52
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
imága	imága	k1gFnSc1	imága
i	i	k8xC	i
housenky	housenka	k1gFnPc1	housenka
jsou	být	k5eAaImIp3nP	být
heliofilní	heliofilní	k2eAgFnPc1d1	heliofilní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
setkat	setkat	k5eAaPmF	setkat
hlavně	hlavně	k9	hlavně
přes	přes	k7c4	přes
den	den	k1gInSc4	den
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
největšího	veliký	k2eAgInSc2d3	veliký
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
housenek	housenka	k1gFnPc2	housenka
se	se	k3xPyFc4	se
při	při	k7c6	při
normálním	normální	k2eAgNnSc6d1	normální
počasí	počasí	k1gNnSc6	počasí
zakukluje	zakuklovat	k5eAaImIp3nS	zakuklovat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Housenky	housenka	k1gFnPc1	housenka
se	se	k3xPyFc4	se
zakuklují	zakuklovat	k5eAaImIp3nP	zakuklovat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
kuklí	kuklit	k5eAaImIp3nS	kuklit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
řídkém	řídký	k2eAgInSc6d1	řídký
zámotku	zámotek	k1gInSc6	zámotek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
počet	počet	k1gInSc1	počet
vláken	vlákna	k1gFnPc2	vlákna
nezbytně	nezbytně	k6eAd1	nezbytně
nutný	nutný	k2eAgInSc1d1	nutný
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
stability	stabilita	k1gFnSc2	stabilita
kukly	kukla	k1gFnSc2	kukla
a	a	k8xC	a
případně	případně	k6eAd1	případně
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
housenky	housenka	k1gFnSc2	housenka
na	na	k7c4	na
kuklu	kukla	k1gFnSc4	kukla
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Stadium	stadium	k1gNnSc1	stadium
kukly	kukla	k1gFnSc2	kukla
trvá	trvat	k5eAaImIp3nS	trvat
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
asi	asi	k9	asi
čtyři	čtyři	k4xCgNnPc4	čtyři
až	až	k9	až
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
pět	pět	k4xCc4	pět
a	a	k8xC	a
půl	půl	k1xP	půl
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vykuklení	vykuklení	k1gNnSc6	vykuklení
je	být	k5eAaImIp3nS	být
imágo	imágo	k6eAd1	imágo
vlhké	vlhký	k2eAgNnSc1d1	vlhké
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
žlutě	žlutě	k6eAd1	žlutě
zbarvená	zbarvený	k2eAgNnPc4d1	zbarvené
měkká	měkký	k2eAgNnPc4d1	měkké
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
žlutavě	žlutavě	k6eAd1	žlutavě
hnědé	hnědý	k2eAgNnSc4d1	hnědé
mekonium	mekonium	k1gNnSc4	mekonium
(	(	kIx(	(
<g/>
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
kukly	kukla	k1gFnSc2	kukla
<g/>
)	)	kIx)	)
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hostitelské	hostitelský	k2eAgFnPc1d1	hostitelská
rostliny	rostlina	k1gFnPc1	rostlina
druhu	druh	k1gInSc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
hostitelské	hostitelský	k2eAgFnPc1d1	hostitelská
rostliny	rostlina	k1gFnPc1	rostlina
housenek	housenka	k1gFnPc2	housenka
jsou	být	k5eAaImIp3nP	být
udávány	udáván	k2eAgInPc1d1	udáván
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
rozchodníky	rozchodník	k1gInPc1	rozchodník
(	(	kIx(	(
<g/>
Sedum	Sedum	k1gInSc1	Sedum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
není	být	k5eNaImIp3nS	být
striktní	striktní	k2eAgInSc1d1	striktní
monofág	monofág	k1gInSc1	monofág
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
předpokládáno	předpokládat	k5eAaImNgNnS	předpokládat
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ho	on	k3xPp3gInSc4	on
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
oligofága	oligofág	k1gMnSc4	oligofág
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hostitelskou	hostitelský	k2eAgFnSc7d1	hostitelská
rostlinou	rostlina	k1gFnSc7	rostlina
většiny	většina	k1gFnSc2	většina
poddruhů	poddruh	k1gInPc2	poddruh
jasoně	jasoň	k1gMnPc4	jasoň
červenookého	červenooký	k2eAgNnSc2d1	červenooké
je	být	k5eAaImIp3nS	být
rozchodník	rozchodník	k1gInSc1	rozchodník
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
Sedum	Sedum	k1gInSc1	Sedum
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
také	také	k9	také
rozchodník	rozchodník	k1gInSc1	rozchodník
velký	velký	k2eAgInSc1d1	velký
(	(	kIx(	(
<g/>
Sedum	Sedum	k1gInSc1	Sedum
telephium	telephium	k1gNnSc1	telephium
maximum	maximum	k1gNnSc1	maximum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnPc4	housenka
ale	ale	k8xC	ale
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
rozchodníků	rozchodník	k1gInPc2	rozchodník
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HOFMANNOVÁ	HOFMANNOVÁ	kA	HOFMANNOVÁ
<g/>
,	,	kIx,	,
Helga	Helga	k1gFnSc1	Helga
<g/>
;	;	kIx,	;
MARKTANNER	MARKTANNER	kA	MARKTANNER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgMnPc1d1	denní
a	a	k8xC	a
noční	noční	k2eAgMnPc1d1	noční
motýli	motýl	k1gMnPc1	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Krapml	Krapml	k1gMnSc1	Krapml
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marek	Marek	k1gMnSc1	Marek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7209	[number]	k4	7209
<g/>
-	-	kIx~	-
<g/>
449	[number]	k4	449
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRABÁK	HRABÁK	kA	HRABÁK
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
našich	náš	k3xOp1gMnPc2	náš
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PECINA	Pecina	k1gMnSc1	Pecina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
chráněných	chráněný	k2eAgMnPc2d1	chráněný
a	a	k8xC	a
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WEISS	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
Jean-Claude	Jean-Claud	k1gMnSc5	Jean-Claud
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Parnassiinae	Parnassiinae	k1gInSc1	Parnassiinae
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Canterbury	Canterbura	k1gFnPc1	Canterbura
<g/>
:	:	kIx,	:
Hillside	Hillsid	k1gInSc5	Hillsid
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
jasoň	jasoň	k1gMnSc1	jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jasoň	jasoň	k1gMnSc1	jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc4	taxon
Parnassius	Parnassius	k1gInSc1	Parnassius
apollo	apollo	k1gNnSc1	apollo
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c4	na
Biolib	Biolib	k1gInSc4	Biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c4	na
eZoo	eZoo	k1gNnSc4	eZoo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c4	na
Priroda	Prirod	k1gMnSc4	Prirod
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgInSc4d1	červenooký
na	na	k7c4	na
Tatry	Tatra	k1gFnPc4	Tatra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c4	na
Lepidoptera	Lepidopter	k1gMnSc4	Lepidopter
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c4	na
Encyclopedia	Encyclopedium	k1gNnPc4	Encyclopedium
of	of	k?	of
Life	Life	k1gNnSc6	Life
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jasoň	Jasoň	k1gMnSc1	Jasoň
červenooký	červenooký	k2eAgMnSc1d1	červenooký
na	na	k7c4	na
Leps	Leps	k1gInSc4	Leps
<g/>
.	.	kIx.	.
<g/>
it	it	k?	it
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
a	a	k8xC	a
obrázky	obrázek	k1gInPc1	obrázek
jasoně	jasoň	k1gMnSc2	jasoň
červenookého	červenooký	k2eAgMnSc2d1	červenooký
</s>
</p>
