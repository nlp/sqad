<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
alpínských	alpínský	k2eAgInPc6d1	alpínský
ekosystémech	ekosystém	k1gInPc6	ekosystém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
horských	horský	k2eAgFnPc6d1	horská
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
části	část	k1gFnSc3	část
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
