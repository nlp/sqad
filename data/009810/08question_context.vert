<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
zasedání	zasedání	k1gNnSc6	zasedání
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
představil	představit	k5eAaPmAgMnS	představit
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
:	:	kIx,	:
Dekret	dekret	k1gInSc4	dekret
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
Dekret	dekret	k1gInSc4	dekret
o	o	k7c6	o
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
složení	složení	k1gNnSc6	složení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
