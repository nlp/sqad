<s>
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
sloužící	sloužící	k2eAgNnSc1d1	sloužící
k	k	k7c3	k
pořizování	pořizování	k1gNnSc3	pořizování
a	a	k8xC	a
zaznamenání	zaznamenání	k1gNnSc3	zaznamenání
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
světlotěsně	světlotěsně	k6eAd1	světlotěsně
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
komora	komora	k1gFnSc1	komora
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
otvorem	otvor	k1gInSc7	otvor
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
nějakou	nějaký	k3yIgFnSc7	nějaký
složitější	složitý	k2eAgFnSc7d2	složitější
optickou	optický	k2eAgFnSc7d1	optická
soustavou	soustava	k1gFnSc7	soustava
–	–	k?	–
objektivem	objektiv	k1gInSc7	objektiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
dovnitř	dovnitř	k6eAd1	dovnitř
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
nějakým	nějaký	k3yIgInSc7	nějaký
druhem	druh	k1gInSc7	druh
světlocitlivé	světlocitlivý	k2eAgFnSc2d1	světlocitlivá
záznamové	záznamový	k2eAgFnSc2d1	záznamová
vrstvy	vrstva	k1gFnSc2	vrstva
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
na	na	k7c4	na
níž	nízce	k6eAd2	nízce
dopadající	dopadající	k2eAgNnSc4d1	dopadající
světlo	světlo	k1gNnSc4	světlo
kreslí	kreslit	k5eAaImIp3nS	kreslit
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Otvor	otvor	k1gInSc1	otvor
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
clonou	clona	k1gFnSc7	clona
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc7d1	umožňující
měnit	měnit	k5eAaImF	měnit
jeho	jeho	k3xOp3gFnSc4	jeho
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
množství	množství	k1gNnSc1	množství
vnikajícího	vnikající	k2eAgNnSc2d1	vnikající
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
výslednou	výsledný	k2eAgFnSc4d1	výsledná
světlost	světlost	k1gFnSc4	světlost
fotografované	fotografovaný	k2eAgFnSc2d1	fotografovaná
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
otvor	otvor	k1gInSc1	otvor
menší	malý	k2eAgInSc1d2	menší
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
paprsek	paprsek	k1gInSc1	paprsek
dopadající	dopadající	k2eAgInSc1d1	dopadající
ze	z	k7c2	z
zachytávaného	zachytávaný	k2eAgInSc2d1	zachytávaný
objektu	objekt	k1gInSc2	objekt
užší	úzký	k2eAgNnSc1d2	užší
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
na	na	k7c6	na
výsledném	výsledný	k2eAgInSc6d1	výsledný
snímku	snímek	k1gInSc6	snímek
ostřejší	ostrý	k2eAgFnSc4d2	ostřejší
–	–	k?	–
se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgNnSc7d1	vzrůstající
zacloněním	zaclonění	k1gNnSc7	zaclonění
tedy	tedy	k9	tedy
roste	růst	k5eAaImIp3nS	růst
hloubka	hloubka	k1gFnSc1	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc4	oblast
před	před	k7c7	před
i	i	k8xC	i
za	za	k7c7	za
zaostřeným	zaostřený	k2eAgNnSc7d1	zaostřené
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
předměty	předmět	k1gInPc1	předmět
stále	stále	k6eAd1	stále
jevit	jevit	k5eAaImF	jevit
jako	jako	k9	jako
ostré	ostrý	k2eAgInPc4d1	ostrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
množství	množství	k1gNnSc1	množství
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
závisí	záviset	k5eAaImIp3nS	záviset
i	i	k9	i
na	na	k7c4	na
ohniskové	ohniskový	k2eAgFnPc4d1	ohnisková
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
objektivu	objektiv	k1gInSc2	objektiv
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
určuje	určovat	k5eAaImIp3nS	určovat
"	"	kIx"	"
<g/>
přiblížení	přiblížení	k1gNnSc1	přiblížení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
prostorový	prostorový	k2eAgInSc1d1	prostorový
úhel	úhel	k1gInSc1	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bude	být	k5eAaImBp3nS	být
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
snímat	snímat	k5eAaImF	snímat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neuvádí	uvádět	k5eNaImIp3nS	uvádět
se	se	k3xPyFc4	se
clona	clona	k1gFnSc1	clona
v	v	k7c6	v
absolutních	absolutní	k2eAgFnPc6d1	absolutní
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používá	používat	k5eAaImIp3nS	používat
se	s	k7c7	s
tzv.	tzv.	kA	tzv.
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
poměr	poměr	k1gInSc1	poměr
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
(	(	kIx(	(
<g/>
značené	značený	k2eAgNnSc1d1	značené
f	f	k?	f
<g/>
)	)	kIx)	)
a	a	k8xC	a
otevření	otevření	k1gNnSc4	otevření
clony	clona	k1gFnSc2	clona
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
hodnota	hodnota	k1gFnSc1	hodnota
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
světelnost	světelnost	k1gFnSc1	světelnost
objektivu	objektiv	k1gInSc2	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Závěrka	závěrka	k1gFnSc1	závěrka
(	(	kIx(	(
<g/>
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
je	být	k5eAaImIp3nS	být
závěrka	závěrka	k1gFnSc1	závěrka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
primárním	primární	k2eAgInSc7d1	primární
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
bránit	bránit	k5eAaImF	bránit
dopadu	dopad	k1gInSc3	dopad
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
citlivou	citlivý	k2eAgFnSc4d1	citlivá
vrstvu	vrstva	k1gFnSc4	vrstva
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nefotografuje	fotografovat	k5eNaImIp3nS	fotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
otevření	otevření	k1gNnSc2	otevření
závěrky	závěrka	k1gFnSc2	závěrka
při	při	k7c6	při
expozici	expozice	k1gFnSc6	expozice
také	také	k9	také
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
snímek	snímek	k1gInSc1	snímek
–	–	k?	–
čím	co	k3yQnSc7	co
déle	dlouho	k6eAd2	dlouho
je	být	k5eAaImIp3nS	být
závěrka	závěrka	k1gFnSc1	závěrka
otevřena	otevřít	k5eAaPmNgFnS	otevřít
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
expoziční	expoziční	k2eAgInSc1d1	expoziční
čas	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
světla	světlo	k1gNnSc2	světlo
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
na	na	k7c4	na
citlivou	citlivý	k2eAgFnSc4d1	citlivá
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ale	ale	k8xC	ale
delší	dlouhý	k2eAgInPc1d2	delší
časy	čas	k1gInPc1	čas
expozice	expozice	k1gFnSc2	expozice
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
neostrost	neostrost	k1gFnSc4	neostrost
–	–	k?	–
rozmazání	rozmazání	k1gNnSc2	rozmazání
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
otevření	otevření	k1gNnSc2	otevření
závěrky	závěrka	k1gFnSc2	závěrka
pohnuly	pohnout	k5eAaPmAgFnP	pohnout
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
expoziční	expoziční	k2eAgFnSc1d1	expoziční
doba	doba	k1gFnSc1	doba
naopak	naopak	k6eAd1	naopak
dokáže	dokázat	k5eAaPmIp3nS	dokázat
velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgInPc4d1	rychlý
děje	děj	k1gInPc4	děj
"	"	kIx"	"
<g/>
zmrazit	zmrazit	k5eAaPmF	zmrazit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
zachytit	zachytit	k5eAaPmF	zachytit
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
krátký	krátký	k2eAgInSc4d1	krátký
okamžik	okamžik	k1gInSc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
vykreslení	vykreslení	k1gNnSc4	vykreslení
fotografie	fotografia	k1gFnSc2	fotografia
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
také	také	k9	také
zaostřit	zaostřit	k5eAaPmF	zaostřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
sbíhající	sbíhající	k2eAgInPc1d1	sbíhající
se	se	k3xPyFc4	se
paprsky	paprsek	k1gInPc7	paprsek
z	z	k7c2	z
objektivu	objektiv	k1gInSc2	objektiv
potkaly	potkat	k5eAaPmAgFnP	potkat
právě	právě	k9	právě
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dopadají	dopadat	k5eAaImIp3nP	dopadat
na	na	k7c4	na
citlivou	citlivý	k2eAgFnSc4d1	citlivá
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
nebo	nebo	k8xC	nebo
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
neprojeví	projevit	k5eNaPmIp3nS	projevit
se	se	k3xPyFc4	se
bod	bod	k1gInSc1	bod
na	na	k7c6	na
objektu	objekt	k1gInSc6	objekt
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
snímku	snímek	k1gInSc2	snímek
jako	jako	k9	jako
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
rozmazaný	rozmazaný	k2eAgInSc1d1	rozmazaný
kroužek	kroužek	k1gInSc1	kroužek
–	–	k?	–
snímek	snímek	k1gInSc1	snímek
bude	být	k5eAaImBp3nS	být
neostrý	ostrý	k2eNgInSc1d1	neostrý
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
ostřícím	ostřící	k2eAgInSc7d1	ostřící
mechanismem	mechanismus	k1gInSc7	mechanismus
na	na	k7c6	na
objektivu	objektiv	k1gInSc6	objektiv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
posune	posunout	k5eAaPmIp3nS	posunout
celou	celý	k2eAgFnSc4d1	celá
optickou	optický	k2eAgFnSc4d1	optická
soustavu	soustava	k1gFnSc4	soustava
dopředu	dopředu	k6eAd1	dopředu
nebo	nebo	k8xC	nebo
dozadu	dozadu	k6eAd1	dozadu
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
paprsky	paprsek	k1gInPc1	paprsek
sbíhaly	sbíhat	k5eAaImAgInP	sbíhat
právě	právě	k9	právě
na	na	k7c6	na
citlivé	citlivý	k2eAgFnSc6d1	citlivá
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
aparáty	aparát	k1gInPc1	aparát
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
fix	fixum	k1gNnPc2	fixum
focusem	focus	k1gMnSc7	focus
–	–	k?	–
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
aparáty	aparát	k1gInPc1	aparát
nebo	nebo	k8xC	nebo
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
clonový	clonový	k2eAgInSc1d1	clonový
otvor	otvor	k1gInSc1	otvor
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopadající	dopadající	k2eAgInPc1d1	dopadající
paprsky	paprsek	k1gInPc1	paprsek
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
úzké	úzký	k2eAgFnPc1d1	úzká
k	k	k7c3	k
vykreslení	vykreslení	k1gNnSc3	vykreslení
většiny	většina	k1gFnSc2	většina
předmětů	předmět	k1gInPc2	předmět
v	v	k7c6	v
rozumné	rozumný	k2eAgFnSc6d1	rozumná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ostření	ostření	k1gNnSc1	ostření
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
na	na	k7c6	na
obsluze	obsluha	k1gFnSc6	obsluha
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInPc1d1	moderní
přístroje	přístroj	k1gInPc1	přístroj
ale	ale	k9	ale
disponují	disponovat	k5eAaBmIp3nP	disponovat
možností	možnost	k1gFnSc7	možnost
automatického	automatický	k2eAgNnSc2d1	automatické
ostření	ostření	k1gNnSc2	ostření
(	(	kIx(	(
<g/>
autofokusu	autofokus	k1gInSc2	autofokus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojího	dvojí	k4xRgMnSc2	dvojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
aktivní	aktivní	k2eAgMnSc1d1	aktivní
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
sám	sám	k3xTgInSc1	sám
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
změří	změřit	k5eAaPmIp3nS	změřit
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
odrazu	odraz	k1gInSc2	odraz
infračerveného	infračervený	k2eAgInSc2d1	infračervený
paprsku	paprsek	k1gInSc2	paprsek
od	od	k7c2	od
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
zjištěného	zjištěný	k2eAgInSc2d1	zjištěný
výsledku	výsledek	k1gInSc2	výsledek
zaostří	zaostřit	k5eAaPmIp3nS	zaostřit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pasivní	pasivní	k2eAgMnPc1d1	pasivní
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
ostří	ostří	k1gNnSc2	ostří
"	"	kIx"	"
<g/>
od	od	k7c2	od
oka	oko	k1gNnSc2	oko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
projíždí	projíždět	k5eAaImIp3nS	projíždět
rozsah	rozsah	k1gInSc4	rozsah
zaostření	zaostření	k1gNnSc2	zaostření
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
kontrast	kontrast	k1gInSc4	kontrast
zachycené	zachycený	k2eAgFnSc2d1	zachycená
scény	scéna	k1gFnSc2	scéna
<g/>
;	;	kIx,	;
nejostřejší	ostrý	k2eAgFnSc1d3	nejostřejší
scéna	scéna	k1gFnSc1	scéna
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
největší	veliký	k2eAgInSc1d3	veliký
kontrast	kontrast	k1gInSc1	kontrast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
aktivní	aktivní	k2eAgInSc1d1	aktivní
autofokus	autofokus	k1gInSc1	autofokus
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
nevýhod	nevýhoda	k1gFnPc2	nevýhoda
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
například	například	k6eAd1	například
s	s	k7c7	s
focením	focení	k1gNnSc7	focení
skrz	skrz	k7c4	skrz
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
možnost	možnost	k1gFnSc1	možnost
výměny	výměna	k1gFnSc2	výměna
objektivů	objektiv	k1gInPc2	objektiv
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
"	"	kIx"	"
<g/>
zvětšovacích	zvětšovací	k2eAgFnPc2d1	zvětšovací
<g/>
"	"	kIx"	"
předsádek	předsádka	k1gFnPc2	předsádka
a	a	k8xC	a
podobného	podobný	k2eAgNnSc2d1	podobné
vybavení	vybavení	k1gNnSc2	vybavení
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
fotoaparátech	fotoaparát	k1gInPc6	fotoaparát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zrcadlovky	zrcadlovka	k1gFnPc1	zrcadlovka
jsou	být	k5eAaImIp3nP	být
vybavovány	vybavovat	k5eAaImNgFnP	vybavovat
výhradně	výhradně	k6eAd1	výhradně
ostřením	ostření	k1gNnPc3	ostření
pasivním	pasivní	k2eAgNnPc3d1	pasivní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechno	všechen	k3xTgNnSc1	všechen
měření	měření	k1gNnSc1	měření
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
objektiv	objektiv	k1gInSc4	objektiv
(	(	kIx(	(
<g/>
through	through	k1gInSc1	through
the	the	k?	the
lens	lens	k1gInSc1	lens
–	–	k?	–
TTL	TTL	kA	TTL
<g/>
)	)	kIx)	)
a	a	k8xC	a
objektivy	objektiv	k1gInPc4	objektiv
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
možno	možno	k6eAd1	možno
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgNnSc1d1	pasivní
ostření	ostření	k1gNnSc1	ostření
nefunguje	fungovat	k5eNaImIp3nS	fungovat
při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nutno	nutno	k6eAd1	nutno
ostřit	ostřit	k5eAaImF	ostřit
ručně	ručně	k6eAd1	ručně
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc4	některý
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
též	též	k9	též
pro	pro	k7c4	pro
ten	ten	k3xDgInSc4	ten
účel	účel	k1gInSc4	účel
mají	mít	k5eAaImIp3nP	mít
miniaturní	miniaturní	k2eAgFnPc1d1	miniaturní
přisvětlovací	přisvětlovací	k2eAgFnPc1d1	přisvětlovací
lampy	lampa	k1gFnPc1	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
pomocného	pomocný	k2eAgNnSc2d1	pomocné
přisvětlení	přisvětlení	k1gNnSc2	přisvětlení
scény	scéna	k1gFnSc2	scéna
infračerveným	infračervený	k2eAgInSc7d1	infračervený
paprskem	paprsek	k1gInSc7	paprsek
pro	pro	k7c4	pro
zaostřování	zaostřování	k1gNnSc4	zaostřování
mají	mít	k5eAaImIp3nP	mít
vestavěny	vestavět	k5eAaPmNgInP	vestavět
také	také	k6eAd1	také
některé	některý	k3yIgInPc1	některý
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
externí	externí	k2eAgInPc1d1	externí
blesky	blesk	k1gInPc1	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
Deskové	deskový	k2eAgFnPc4d1	desková
–	–	k?	–
světlocitlivá	světlocitlivý	k2eAgFnSc1d1	světlocitlivá
chemická	chemický	k2eAgFnSc1d1	chemická
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
nanesena	nanést	k5eAaBmNgFnS	nanést
na	na	k7c6	na
skleněné	skleněný	k2eAgFnSc6d1	skleněná
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
se	se	k3xPyFc4	se
využíval	využívat	k5eAaImAgInS	využívat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
fotografování	fotografování	k1gNnSc2	fotografování
v	v	k7c6	v
letech	let	k1gInPc6	let
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgMnPc4d1	filmový
–	–	k?	–
roli	role	k1gFnSc4	role
světlocitlivé	světlocitlivý	k2eAgFnSc2d1	světlocitlivá
vrstvy	vrstva	k1gFnSc2	vrstva
hraje	hrát	k5eAaImIp3nS	hrát
celuloidový	celuloidový	k2eAgInSc1d1	celuloidový
pás	pás	k1gInSc1	pás
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
reagujícími	reagující	k2eAgFnPc7d1	reagující
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
svoji	svůj	k3xOyFgFnSc4	svůj
přeměnou	přeměna	k1gFnSc7	přeměna
<g/>
;	;	kIx,	;
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
snímek	snímek	k1gInSc1	snímek
vyvolán	vyvolán	k2eAgInSc1d1	vyvolán
–	–	k?	–
přeměněné	přeměněný	k2eAgFnPc1d1	přeměněná
chemikálie	chemikálie	k1gFnPc1	chemikálie
se	se	k3xPyFc4	se
další	další	k2eAgFnSc7d1	další
reakcí	reakce	k1gFnSc7	reakce
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
zabarvené	zabarvený	k2eAgFnPc4d1	zabarvená
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
obraz	obraz	k1gInSc4	obraz
Okamžité	okamžitý	k2eAgNnSc1d1	okamžité
-	-	kIx~	-
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
se	s	k7c7	s
samovyvolávacím	samovyvolávací	k2eAgInSc7d1	samovyvolávací
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
původně	původně	k6eAd1	původně
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
společnost	společnost	k1gFnSc1	společnost
Polaroid	polaroid	k1gInSc1	polaroid
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
–	–	k?	–
jako	jako	k8xC	jako
světlocitlivý	světlocitlivý	k2eAgInSc1d1	světlocitlivý
prvek	prvek	k1gInSc1	prvek
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
slouží	sloužit	k5eAaImIp3nS	sloužit
elektronický	elektronický	k2eAgInSc1d1	elektronický
obrazový	obrazový	k2eAgInSc1d1	obrazový
snímač	snímač	k1gInSc1	snímač
CCD	CCD	kA	CCD
nebo	nebo	k8xC	nebo
CMOS	CMOS	kA	CMOS
<g/>
.	.	kIx.	.
</s>
<s>
Zachycené	zachycený	k2eAgFnPc1d1	zachycená
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
digitálně	digitálně	k6eAd1	digitálně
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
do	do	k7c2	do
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Camera	Camera	k1gFnSc1	Camera
obscura	obscura	k1gFnSc1	obscura
neboli	neboli	k8xC	neboli
dírková	dírkový	k2eAgFnSc1d1	dírková
komora	komora	k1gFnSc1	komora
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
pinhole	pinhole	k1gFnSc1	pinhole
<g/>
)	)	kIx)	)
–	–	k?	–
optické	optický	k2eAgNnSc1d1	optické
zařízení	zařízení	k1gNnSc1	zařízení
používané	používaný	k2eAgNnSc1d1	používané
jako	jako	k8xS	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
malířů	malíř	k1gMnPc2	malíř
a	a	k8xC	a
předchůdce	předchůdce	k1gMnSc2	předchůdce
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
schránka	schránka	k1gFnSc1	schránka
s	s	k7c7	s
otvorem	otvor	k1gInSc7	otvor
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
scény	scéna	k1gFnSc2	scéna
po	po	k7c6	po
průchodu	průchod	k1gInSc6	průchod
otvorem	otvor	k1gInSc7	otvor
a	a	k8xC	a
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
na	na	k7c4	na
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
bylo	být	k5eAaImAgNnS	být
zachování	zachování	k1gNnSc4	zachování
perspektivy	perspektiva	k1gFnSc2	perspektiva
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
realističnost	realističnost	k1gFnSc1	realističnost
výsledného	výsledný	k2eAgInSc2d1	výsledný
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Promítaný	promítaný	k2eAgInSc1d1	promítaný
obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
a	a	k8xC	a
převrácený	převrácený	k2eAgMnSc1d1	převrácený
<g/>
.	.	kIx.	.
</s>
<s>
Desková	deskový	k2eAgFnSc1d1	desková
kamera	kamera	k1gFnSc1	kamera
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
snímající	snímající	k2eAgInSc1d1	snímající
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
světlocitlivou	světlocitlivý	k2eAgFnSc4d1	světlocitlivá
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začínal	začínat	k5eAaImAgInS	začínat
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
mokrý	mokrý	k2eAgInSc1d1	mokrý
kolodiový	kolodiový	k2eAgInSc1d1	kolodiový
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Fotografická	fotografický	k2eAgFnSc1d1	fotografická
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
skřínová	skřínový	k2eAgFnSc1d1	skřínový
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
box	box	k1gInSc1	box
camera	camera	k1gFnSc1	camera
<g/>
)	)	kIx)	)
–	–	k?	–
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nejjednodušší	jednoduchý	k2eAgFnSc6d3	nejjednodušší
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ještě	ještě	k6eAd1	ještě
jednodušší	jednoduchý	k2eAgFnSc2d2	jednodušší
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
skříňová	skříňový	k2eAgFnSc1d1	skříňová
kamera	kamera	k1gFnSc1	kamera
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
podobný	podobný	k2eAgInSc1d1	podobný
krabici	krabice	k1gFnSc3	krabice
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
nese	nést	k5eAaImIp3nS	nést
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Kamera	kamera	k1gFnSc1	kamera
má	mít	k5eAaImIp3nS	mít
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
optickou	optický	k2eAgFnSc4d1	optická
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jen	jen	k9	jen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
menisku	meniskus	k1gInSc2	meniskus
(	(	kIx(	(
<g/>
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
vybroušeny	vybrousit	k5eAaPmNgFnP	vybrousit
jako	jako	k8xS	jako
část	část	k1gFnSc1	část
kulové	kulový	k2eAgFnSc2d1	kulová
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
objektivu	objektiv	k1gInSc2	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
obvykle	obvykle	k6eAd1	obvykle
chybí	chybit	k5eAaPmIp3nS	chybit
možnost	možnost	k1gFnSc4	možnost
libovolného	libovolný	k2eAgNnSc2d1	libovolné
zaostření	zaostření	k1gNnSc2	zaostření
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
pevně	pevně	k6eAd1	pevně
zaostřený	zaostřený	k2eAgInSc4d1	zaostřený
objektiv	objektiv	k1gInSc4	objektiv
–	–	k?	–
fixed	fixed	k1gInSc1	fixed
focus	focus	k1gInSc1	focus
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
možnost	možnost	k1gFnSc4	možnost
ovládání	ovládání	k1gNnSc2	ovládání
zaclonění	zaclonění	k1gNnSc2	zaclonění
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
expozičního	expoziční	k2eAgInSc2d1	expoziční
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
rychlosti	rychlost	k1gFnSc2	rychlost
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sklopný	sklopný	k2eAgInSc1d1	sklopný
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
objektiv	objektiv	k1gInSc1	objektiv
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
složen	složit	k5eAaPmNgInS	složit
společně	společně	k6eAd1	společně
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
na	na	k7c4	na
kompaktní	kompaktní	k2eAgFnSc4d1	kompaktní
a	a	k8xC	a
robustní	robustní	k2eAgFnSc4d1	robustní
krabičku	krabička	k1gFnSc4	krabička
<g/>
.	.	kIx.	.
</s>
<s>
Objektiv	objektiv	k1gInSc1	objektiv
je	být	k5eAaImIp3nS	být
připevněn	připevnit	k5eAaPmNgInS	připevnit
mechanismem	mechanismus	k1gInSc7	mechanismus
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
systému	systém	k1gInSc6	systém
pantografu	pantograf	k1gInSc2	pantograf
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
součástí	součást	k1gFnSc7	součást
víka	víko	k1gNnSc2	víko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
složeném	složený	k2eAgInSc6d1	složený
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
velikost	velikost	k1gFnSc4	velikost
blízkou	blízký	k2eAgFnSc7d1	blízká
velikosti	velikost	k1gFnSc6	velikost
fotografické	fotografický	k2eAgFnSc2d1	fotografická
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
–	–	k?	–
typ	typ	k1gInSc1	typ
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
se	s	k7c7	s
samovyvolávacím	samovyvolávací	k2eAgInSc7d1	samovyvolávací
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
původně	původně	k6eAd1	původně
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
společnost	společnost	k1gFnSc1	společnost
Polaroid	polaroid	k1gInSc1	polaroid
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
populární	populární	k2eAgInSc1d1	populární
polaroid	polaroid	k1gInSc1	polaroid
SX-70	SX-70	k1gFnSc2	SX-70
používá	používat	k5eAaImIp3nS	používat
film	film	k1gInSc1	film
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
formátu	formát	k1gInSc2	formát
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
expozice	expozice	k1gFnSc1	expozice
(	(	kIx(	(
<g/>
negativ	negativ	k1gInSc1	negativ
<g/>
,	,	kIx,	,
vývojka	vývojka	k1gFnSc1	vývojka
<g/>
,	,	kIx,	,
ustalovač	ustalovač	k1gInSc1	ustalovač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aparát	aparát	k1gInSc1	aparát
navržený	navržený	k2eAgInSc1d1	navržený
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
fotografie	fotografie	k1gFnSc1	fotografie
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
nebo	nebo	k8xC	nebo
minut	minuta	k1gFnPc2	minuta
od	od	k7c2	od
pořízení	pořízení	k1gNnSc2	pořízení
obrázku	obrázek	k1gInSc2	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
instantním	instantní	k2eAgInSc6d1	instantní
filmu	film	k1gInSc6	film
jsou	být	k5eAaImIp3nP	být
emulze	emulze	k1gFnPc1	emulze
a	a	k8xC	a
zpracovávající	zpracovávající	k2eAgFnPc1d1	zpracovávající
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
spojeny	spojit	k5eAaPmNgFnP	spojit
v	v	k7c6	v
obsažené	obsažený	k2eAgFnSc6d1	obsažená
obálce	obálka	k1gFnSc6	obálka
nebo	nebo	k8xC	nebo
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
fotografickém	fotografický	k2eAgInSc6d1	fotografický
papíru	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
<g/>
,	,	kIx,	,
vyvolání	vyvolání	k1gNnSc1	vyvolání
i	i	k8xC	i
kopie	kopie	k1gFnSc1	kopie
na	na	k7c4	na
speciální	speciální	k2eAgInSc4d1	speciální
papír	papír	k1gInSc4	papír
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Dálkoměrný	dálkoměrný	k2eAgInSc1d1	dálkoměrný
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
s	s	k7c7	s
vestavěným	vestavěný	k2eAgInSc7d1	vestavěný
dálkoměrem	dálkoměr	k1gInSc7	dálkoměr
<g/>
.	.	kIx.	.
</s>
<s>
Ostření	ostření	k1gNnSc1	ostření
neboli	neboli	k8xC	neboli
nastavení	nastavení	k1gNnSc1	nastavení
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
snímaného	snímaný	k2eAgInSc2d1	snímaný
předmětu	předmět	k1gInSc2	předmět
se	se	k3xPyFc4	se
na	na	k7c6	na
fotoaparátu	fotoaparát	k1gInSc6	fotoaparát
řeší	řešit	k5eAaImIp3nS	řešit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
dvojobrazového	dvojobrazový	k2eAgInSc2d1	dvojobrazový
(	(	kIx(	(
<g/>
koincidenčního	koincidenční	k2eAgInSc2d1	koincidenční
<g/>
)	)	kIx)	)
dálkoměru	dálkoměr	k1gInSc2	dálkoměr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
zorného	zorný	k2eAgNnSc2d1	zorné
pole	pole	k1gNnSc2	pole
jsou	být	k5eAaImIp3nP	být
promítány	promítán	k2eAgInPc1d1	promítán
dva	dva	k4xCgInPc1	dva
obrazy	obraz	k1gInPc1	obraz
–	–	k?	–
zaostřeno	zaostřen	k2eAgNnSc1d1	Zaostřeno
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oba	dva	k4xCgInPc1	dva
obrazy	obraz	k1gInPc1	obraz
splynou	splynout	k5eAaPmIp3nP	splynout
(	(	kIx(	(
<g/>
koincidují	koincidovat	k5eAaImIp3nP	koincidovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systémový	systémový	k2eAgInSc1d1	systémový
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
system	systo	k1gNnSc7	systo
camera	camero	k1gNnSc2	camero
<g/>
)	)	kIx)	)
–	–	k?	–
typ	typ	k1gInSc4	typ
profesionálního	profesionální	k2eAgInSc2d1	profesionální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
s	s	k7c7	s
výměnnými	výměnný	k2eAgFnPc7d1	výměnná
základními	základní	k2eAgFnPc7d1	základní
komponentami	komponenta	k1gFnPc7	komponenta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
jádru	jádro	k1gNnSc3	jádro
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
systémových	systémový	k2eAgMnPc2d1	systémový
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
jsou	být	k5eAaImIp3nP	být
zrcadlovky	zrcadlovka	k1gFnPc4	zrcadlovka
malého	malý	k2eAgMnSc2d1	malý
nebo	nebo	k8xC	nebo
středního	střední	k2eAgInSc2d1	střední
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
Fotogrammetrická	fotogrammetrický	k2eAgFnSc1d1	fotogrammetrická
kamera	kamera	k1gFnSc1	kamera
–	–	k?	–
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
hodnotou	hodnota	k1gFnSc7	hodnota
aberace	aberace	k1gFnSc2	aberace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
chybami	chyba	k1gFnPc7	chyba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
kalibrace	kalibrace	k1gFnSc2	kalibrace
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
fotogrammetrii	fotogrammetrie	k1gFnSc6	fotogrammetrie
a	a	k8xC	a
dálkovém	dálkový	k2eAgInSc6d1	dálkový
průzkumu	průzkum	k1gInSc6	průzkum
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hledáčkový	hledáčkový	k2eAgInSc1d1	hledáčkový
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
průzorem	průzor	k1gInSc7	průzor
vedle	vedle	k7c2	vedle
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
hledáčkem	hledáček	k1gInSc7	hledáček
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgInSc2	jenž
fotograf	fotograf	k1gMnSc1	fotograf
komponuje	komponovat	k5eAaImIp3nS	komponovat
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Hledáček	hledáček	k1gInSc1	hledáček
však	však	k9	však
neukazuje	ukazovat	k5eNaImIp3nS	ukazovat
scénu	scéna	k1gFnSc4	scéna
skrz	skrz	k7c4	skrz
objektiv	objektiv	k1gInSc4	objektiv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
velmi	velmi	k6eAd1	velmi
přibližně	přibližně	k6eAd1	přibližně
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
objektiv	objektiv	k1gInSc4	objektiv
mohl	moct	k5eAaImAgInS	moct
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Jednooká	jednooký	k2eAgFnSc1d1	jednooká
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
(	(	kIx(	(
<g/>
SLR	SLR	kA	SLR
<g/>
)	)	kIx)	)
–	–	k?	–
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
hledáčkem	hledáček	k1gInSc7	hledáček
se	s	k7c7	s
sklopným	sklopný	k2eAgNnSc7d1	sklopné
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
:	:	kIx,	:
při	při	k7c6	při
kompozici	kompozice	k1gFnSc6	kompozice
scény	scéna	k1gFnSc2	scéna
se	se	k3xPyFc4	se
fotograf	fotograf	k1gMnSc1	fotograf
dívá	dívat	k5eAaImIp3nS	dívat
přes	přes	k7c4	přes
optiku	optika	k1gFnSc4	optika
hledáčku	hledáček	k1gInSc2	hledáček
přímo	přímo	k6eAd1	přímo
skrz	skrz	k7c4	skrz
objektiv	objektiv	k1gInSc4	objektiv
<g/>
,	,	kIx,	,
při	při	k7c6	při
expozici	expozice	k1gFnSc6	expozice
se	se	k3xPyFc4	se
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
sklopí	sklopit	k5eAaPmIp3nS	sklopit
a	a	k8xC	a
exponuje	exponovat	k5eAaImIp3nS	exponovat
se	se	k3xPyFc4	se
na	na	k7c4	na
citlivou	citlivý	k2eAgFnSc4d1	citlivá
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
;	;	kIx,	;
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
<g />
.	.	kIx.	.
</s>
<s>
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
i	i	k9	i
tzv.	tzv.	kA	tzv.
nepravé	pravý	k2eNgFnPc4d1	nepravá
zrcadlovky	zrcadlovka	k1gFnPc4	zrcadlovka
(	(	kIx(	(
<g/>
SLR-like	SLRike	k1gNnSc4	SLR-like
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sice	sice	k8xC	sice
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
kompozici	kompozice	k1gFnSc6	kompozice
scény	scéna	k1gFnSc2	scéna
fotograf	fotograf	k1gMnSc1	fotograf
vidí	vidět	k5eAaImIp3nS	vidět
živý	živý	k2eAgInSc4d1	živý
obraz	obraz	k1gInSc4	obraz
ze	z	k7c2	z
senzoru	senzor	k1gInSc2	senzor
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
Dvouoká	dvouoký	k2eAgFnSc1d1	dvouoká
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
(	(	kIx(	(
<g/>
TLR	TLR	kA	TLR
<g/>
)	)	kIx)	)
–	–	k?	–
přístroj	přístroj	k1gInSc1	přístroj
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
objektivy	objektiv	k1gInPc7	objektiv
<g/>
;	;	kIx,	;
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
horního	horní	k2eAgNnSc2d1	horní
je	být	k5eAaImIp3nS	být
odrážen	odrážen	k2eAgInSc1d1	odrážen
pevným	pevný	k2eAgNnSc7d1	pevné
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
na	na	k7c6	na
matnici	matnice	k1gFnSc6	matnice
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgNnPc1d1	dolní
slouží	sloužit	k5eAaImIp3nP	sloužit
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
expozici	expozice	k1gFnSc4	expozice
Dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
panoramatická	panoramatický	k2eAgFnSc1d1	panoramatická
kamera	kamera	k1gFnSc1	kamera
–	–	k?	–
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
pro	pro	k7c4	pro
historický	historický	k2eAgInSc4d1	historický
panoramatický	panoramatický	k2eAgInSc4d1	panoramatický
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
německý	německý	k2eAgMnSc1d1	německý
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
Julius	Julius	k1gMnSc1	Julius
Neubronner	Neubronner	k1gMnSc1	Neubronner
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
technologii	technologie	k1gFnSc6	technologie
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
využití	využití	k1gNnSc1	využití
bylo	být	k5eAaImAgNnS	být
především	především	k9	především
v	v	k7c6	v
holubí	holubí	k2eAgFnSc6d1	holubí
fotografii	fotografia	k1gFnSc6	fotografia
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
lehká	lehký	k2eAgFnSc1d1	lehká
miniaturní	miniaturní	k2eAgFnSc1d1	miniaturní
kamera	kamera	k1gFnSc1	kamera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
koženého	kožený	k2eAgInSc2d1	kožený
postroje	postroj	k1gInSc2	postroj
a	a	k8xC	a
hliníkového	hliníkový	k2eAgInSc2d1	hliníkový
kyrysu	kyrys	k1gInSc2	kyrys
připevnila	připevnit	k5eAaPmAgFnS	připevnit
na	na	k7c4	na
prsa	prsa	k1gNnPc4	prsa
holuba	holub	k1gMnSc2	holub
<g/>
.	.	kIx.	.
</s>
<s>
Kamera	kamera	k1gFnSc1	kamera
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
časovací	časovací	k2eAgInSc4d1	časovací
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
prodlevu	prodleva	k1gFnSc4	prodleva
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
expozicemi	expozice	k1gFnPc7	expozice
a	a	k8xC	a
otáčivý	otáčivý	k2eAgInSc1d1	otáčivý
objektiv	objektiv	k1gInSc1	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Pořizovala	pořizovat	k5eAaImAgFnS	pořizovat
obrázky	obrázek	k1gInPc4	obrázek
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
3	[number]	k4	3
×	×	k?	×
8	[number]	k4	8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
až	až	k9	až
35	[number]	k4	35
automatických	automatický	k2eAgFnPc2d1	automatická
expozic	expozice	k1gFnPc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Paralaxa	paralaxa	k1gFnSc1	paralaxa
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
hledáčkových	hledáčkový	k2eAgInPc2d1	hledáčkový
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
a	a	k8xC	a
dvouokých	dvouoký	k2eAgFnPc2d1	dvouoká
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
paralaxa	paralaxa	k1gFnSc1	paralaxa
–	–	k?	–
prostorový	prostorový	k2eAgInSc4d1	prostorový
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
pohledem	pohled	k1gInSc7	pohled
fotografa	fotograf	k1gMnSc2	fotograf
před	před	k7c7	před
pořízením	pořízení	k1gNnSc7	pořízení
záběru	záběr	k1gInSc2	záběr
a	a	k8xC	a
výsledným	výsledný	k2eAgInSc7d1	výsledný
snímkem	snímek	k1gInSc7	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
delší	dlouhý	k2eAgFnSc6d2	delší
snímací	snímací	k2eAgFnSc6d1	snímací
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
je	být	k5eAaImIp3nS	být
efekt	efekt	k1gInSc1	efekt
paralaxy	paralaxa	k1gFnSc2	paralaxa
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
;	;	kIx,	;
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
metr	metr	k1gInSc1	metr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
již	již	k6eAd1	již
paralaxa	paralaxa	k1gFnSc1	paralaxa
začne	začít	k5eAaPmIp3nS	začít
projevovat	projevovat	k5eAaImF	projevovat
a	a	k8xC	a
pro	pro	k7c4	pro
fotografa	fotograf	k1gMnSc4	fotograf
je	být	k5eAaImIp3nS	být
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
scénu	scéna	k1gFnSc4	scéna
nebo	nebo	k8xC	nebo
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
paralaxa	paralaxa	k1gFnSc1	paralaxa
vyrovnána	vyrovnán	k2eAgFnSc1d1	vyrovnána
<g/>
,	,	kIx,	,
scéna	scéna	k1gFnSc1	scéna
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
je	být	k5eAaImIp3nS	být
posunuta	posunut	k2eAgFnSc1d1	posunuta
oproti	oproti	k7c3	oproti
záměru	záměr	k1gInSc3	záměr
<g/>
.	.	kIx.	.
</s>
<s>
BenQ	BenQ	k?	BenQ
Canon	Canon	kA	Canon
Casio	Casio	k1gNnSc1	Casio
Contax	Contax	k1gInSc1	Contax
Exakta	Exakta	k1gMnSc1	Exakta
FED	FED	kA	FED
Fujifilm	Fujifilm	k1gInSc1	Fujifilm
Gorizon	Gorizon	k1gInSc1	Gorizon
Hasselblad	Hasselblad	k1gInSc4	Hasselblad
Hewlett	Hewlett	kA	Hewlett
Packard	Packard	kA	Packard
Kodak	Kodak	kA	Kodak
Konica	Konica	kA	Konica
Leica	Leica	k1gMnSc1	Leica
Linhof	Linhof	k1gMnSc1	Linhof
LOMO	LOMO	kA	LOMO
Lumix	Lumix	k1gInSc1	Lumix
–	–	k?	–
Panasonic	Panasonic	kA	Panasonic
Kiev	Kiev	k1gInSc1	Kiev
Minolta	Minolta	kA	Minolta
Mamiya	Mamiya	k1gFnSc1	Mamiya
Minox	Minox	k1gInSc1	Minox
Mikroma	Mikroma	k1gFnSc1	Mikroma
Nikon	Nikon	k1gMnSc1	Nikon
Olympus	Olympus	k1gMnSc1	Olympus
Pentax	Pentax	k1gInSc4	Pentax
Polaroid	polaroid	k1gInSc1	polaroid
Praktica	Praktic	k1gInSc2	Praktic
Ricoh	Ricoha	k1gFnPc2	Ricoha
Rollei	Rollei	k1gNnSc2	Rollei
Sigma	sigma	k1gNnSc2	sigma
Sony	Sony	kA	Sony
Vivitar	Vivitar	k1gInSc1	Vivitar
Voigtländer	Voigtländer	k1gInSc1	Voigtländer
Zeiss	Zeiss	k1gInSc1	Zeiss
Zenit	zenit	k1gInSc1	zenit
Zenza	Zenz	k1gMnSc2	Zenz
Bronica	Bronicus	k1gMnSc2	Bronicus
Zorki	Zorki	k1gNnSc2	Zorki
Příslušenství	příslušenství	k1gNnSc2	příslušenství
k	k	k7c3	k
fotoaparátům	fotoaparát	k1gInPc3	fotoaparát
<g/>
:	:	kIx,	:
Stativ	stativ	k1gInSc1	stativ
Objektiv	objektiv	k1gInSc1	objektiv
Fotografický	fotografický	k2eAgInSc1d1	fotografický
filtr	filtr	k1gInSc4	filtr
Fotografický	fotografický	k2eAgInSc4d1	fotografický
popruh	popruh	k1gInSc4	popruh
Dálková	dálkový	k2eAgFnSc1d1	dálková
spoušť	spoušť	k1gFnSc1	spoušť
</s>
