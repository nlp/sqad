<s>
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Renatus	Renatus	k1gMnSc1	Renatus
Cartesius	Cartesius	k1gMnSc1	Cartesius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1596	[number]	k4	1596
La	la	k1gNnPc2	la
Haye	Haye	k1gFnPc2	Haye
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Descartes	Descartes	k1gInSc4	Descartes
(	(	kIx(	(
<g/>
Indre-et-Loire	Indret-Loir	k1gMnSc5	Indre-et-Loir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Tours	Toursa	k1gFnPc2	Toursa
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1650	[number]	k4	1650
<g/>
,	,	kIx,	,
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
dekárt	dekárt	k1gInSc1	dekárt
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
[	[	kIx(	[
<g/>
dekárta	dekárta	k1gFnSc1	dekárta
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
psáno	psát	k5eAaImNgNnS	psát
Descartese	Descartese	k1gFnSc2	Descartese
či	či	k8xC	či
Descartesa	Descartesa	k1gFnSc1	Descartesa
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
tvar	tvar	k1gInSc1	tvar
Descarta	Descart	k1gMnSc2	Descart
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
Descartovým	Descartův	k2eAgNnSc7d1	Descartovo
východiskem	východisko	k1gNnSc7	východisko
je	být	k5eAaImIp3nS	být
metodická	metodický	k2eAgFnSc1d1	metodická
skepse	skepse	k1gFnSc1	skepse
<g/>
,	,	kIx,	,
soustavná	soustavný	k2eAgFnSc1d1	soustavná
pochybnost	pochybnost	k1gFnSc1	pochybnost
zejména	zejména	k9	zejména
o	o	k7c6	o
datech	datum	k1gNnPc6	datum
smyslového	smyslový	k2eAgNnSc2d1	smyslové
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
odvážným	odvážný	k2eAgInSc7d1	odvážný
myšlenkovým	myšlenkový	k2eAgInSc7d1	myšlenkový
krokem	krok	k1gInSc7	krok
dokázal	dokázat	k5eAaPmAgInS	dokázat
podstatně	podstatně	k6eAd1	podstatně
změnit	změnit	k5eAaPmF	změnit
evropské	evropský	k2eAgNnSc4d1	Evropské
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
zakladatelem	zakladatel	k1gMnSc7	zakladatel
<g/>
:	:	kIx,	:
moderní	moderní	k2eAgFnSc2d1	moderní
kritické	kritický	k2eAgFnSc2d1	kritická
epistemologie	epistemologie	k1gFnSc2	epistemologie
<g/>
,	,	kIx,	,
novověké	novověký	k2eAgFnSc2d1	novověká
racionalistické	racionalistický	k2eAgFnSc2d1	racionalistická
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
sebejistotě	sebejistota	k1gFnSc6	sebejistota
myslícího	myslící	k2eAgInSc2d1	myslící
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
mechanistického	mechanistický	k2eAgInSc2d1	mechanistický
výkladu	výklad	k1gInSc2	výklad
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
analytické	analytický	k2eAgFnPc1d1	analytická
metody	metoda	k1gFnPc1	metoda
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vědu	věda	k1gFnSc4	věda
jako	jako	k8xC	jako
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
ovšem	ovšem	k9	ovšem
odmítá	odmítat	k5eAaImIp3nS	odmítat
běžnou	běžný	k2eAgFnSc4d1	běžná
skepsi	skepse	k1gFnSc4	skepse
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pochybování	pochybování	k1gNnPc4	pochybování
pro	pro	k7c4	pro
pochybování	pochybování	k1gNnSc4	pochybování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
pochybnost	pochybnost	k1gFnSc1	pochybnost
užívá	užívat	k5eAaImIp3nS	užívat
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dobral	dobrat	k5eAaPmAgMnS	dobrat
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jednat	jednat	k5eAaImF	jednat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
naše	náš	k3xOp1gNnSc1	náš
poznání	poznání	k1gNnSc1	poznání
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc4	první
výslovně	výslovně	k6eAd1	výslovně
formuloval	formulovat	k5eAaImAgMnS	formulovat
zásady	zásada	k1gFnPc4	zásada
vědecké	vědecký	k2eAgFnSc2d1	vědecká
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
číselné	číselný	k2eAgFnSc2d1	číselná
reprezentace	reprezentace	k1gFnSc2	reprezentace
geometrických	geometrický	k2eAgInPc2d1	geometrický
objektů	objekt	k1gInPc2	objekt
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
kartézská	kartézský	k2eAgFnSc1d1	kartézská
soustava	soustava	k1gFnSc1	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
analytické	analytický	k2eAgFnSc2d1	analytická
geometrie	geometrie	k1gFnSc2	geometrie
a	a	k8xC	a
na	na	k7c4	na
matematizaci	matematizace	k1gFnSc4	matematizace
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
vzdělané	vzdělaný	k2eAgFnSc6d1	vzdělaná
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
radou	rada	k1gMnSc7	rada
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
parlamentu	parlament	k1gInSc2	parlament
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c4	v
Rennes	Rennes	k1gInSc4	Rennes
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
brzy	brzy	k6eAd1	brzy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
nadaného	nadaný	k2eAgMnSc2d1	nadaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neduživého	duživý	k2eNgMnSc2d1	neduživý
chlapce	chlapec	k1gMnSc2	chlapec
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1604	[number]	k4	1604
až	až	k9	až
1612	[number]	k4	1612
studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
jezuitské	jezuitský	k2eAgFnSc6d1	jezuitská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
La	la	k1gNnSc6	la
Flè	Flè	k1gFnSc2	Flè
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
důkladného	důkladný	k2eAgNnSc2d1	důkladné
vzdělání	vzdělání	k1gNnSc3	vzdělání
ve	v	k7c6	v
scholastické	scholastický	k2eAgFnSc6d1	scholastická
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
,	,	kIx,	,
matematice	matematika	k1gFnSc6	matematika
i	i	k8xC	i
ve	v	k7c6	v
vědách	věda	k1gFnPc6	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rozpravě	rozprava	k1gFnSc6	rozprava
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
sice	sice	k8xC	sice
o	o	k7c6	o
užitečnosti	užitečnost	k1gFnSc6	užitečnost
tohoto	tento	k3xDgNnSc2	tento
vzdělání	vzdělání	k1gNnSc2	vzdělání
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
však	však	k9	však
později	pozdě	k6eAd2	pozdě
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
tuto	tento	k3xDgFnSc4	tento
univerzitu	univerzita	k1gFnSc4	univerzita
vřele	vřele	k6eAd1	vřele
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1616	[number]	k4	1616
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
licence	licence	k1gFnSc2	licence
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Poitiers	Poitiers	k1gInSc4	Poitiers
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
psal	psát	k5eAaImAgInS	psát
různá	různý	k2eAgNnPc4d1	různé
pojednání	pojednání	k1gNnPc4	pojednání
(	(	kIx(	(
<g/>
o	o	k7c6	o
šermu	šerm	k1gInSc6	šerm
<g/>
,	,	kIx,	,
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
chtěl	chtít	k5eAaImAgMnS	chtít
poznat	poznat	k5eAaPmF	poznat
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
fyzikem	fyzik	k1gMnSc7	fyzik
Beeckmanem	Beeckman	k1gMnSc7	Beeckman
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
dlouho	dlouho	k6eAd1	dlouho
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1617	[number]	k4	1617
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
vojska	vojsko	k1gNnSc2	vojsko
Mořice	Mořic	k1gMnSc2	Mořic
Oranžského	oranžský	k2eAgInSc2d1	oranžský
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
bavorského	bavorský	k2eAgMnSc2d1	bavorský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
Bavorského	bavorský	k2eAgMnSc2d1	bavorský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bitvy	bitva	k1gFnPc1	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1619	[number]	k4	1619
měl	mít	k5eAaImAgInS	mít
blízko	blízko	k7c2	blízko
německého	německý	k2eAgInSc2d1	německý
Ulmu	Ulmus	k1gInSc2	Ulmus
noční	noční	k2eAgNnSc4d1	noční
vidění	vidění	k1gNnSc4	vidění
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
"	"	kIx"	"
<g/>
našel	najít	k5eAaPmAgMnS	najít
základy	základ	k1gInPc4	základ
úžasné	úžasný	k2eAgFnSc2d1	úžasná
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
a	a	k8xC	a
učinil	učinit	k5eAaPmAgMnS	učinit
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
vykoná	vykonat	k5eAaPmIp3nS	vykonat
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
italského	italský	k2eAgNnSc2d1	italské
Loreta	Loreto	k1gNnSc2	Loreto
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
zpeněžil	zpeněžit	k5eAaPmAgInS	zpeněžit
a	a	k8xC	a
uložil	uložit	k5eAaPmAgInS	uložit
zděděný	zděděný	k2eAgInSc1d1	zděděný
majetek	majetek	k1gInSc1	majetek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
svým	svůj	k3xOyFgFnPc3	svůj
zálibám	záliba	k1gFnPc3	záliba
a	a	k8xC	a
cestování	cestování	k1gNnSc1	cestování
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Otcem	otec	k1gMnSc7	otec
Mersennem	Mersenn	k1gMnSc7	Mersenn
<g/>
,	,	kIx,	,
významným	významný	k2eAgMnSc7d1	významný
matematikem	matematik	k1gMnSc7	matematik
a	a	k8xC	a
všestranným	všestranný	k2eAgMnSc7d1	všestranný
učencem	učenec	k1gMnSc7	učenec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
korespondenci	korespondence	k1gFnSc4	korespondence
se	s	k7c7	s
současnými	současný	k2eAgMnPc7d1	současný
vědci	vědec	k1gMnPc7	vědec
(	(	kIx(	(
<g/>
Pascal	Pascal	kA	Pascal
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Fermat	fermata	k1gFnPc2	fermata
<g/>
,	,	kIx,	,
Torricelli	Torricell	k1gMnPc1	Torricell
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Královská	královský	k2eAgFnSc1d1	královská
akademie	akademie	k1gFnSc1	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Descartes	Descartes	k1gMnSc1	Descartes
vedl	vést	k5eAaImAgMnS	vést
svoji	svůj	k3xOyFgFnSc4	svůj
korespondenci	korespondence	k1gFnSc4	korespondence
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
Mersennem	Mersenn	k1gInSc7	Mersenn
a	a	k8xC	a
jak	jak	k6eAd1	jak
víme	vědět	k5eAaImIp1nP	vědět
z	z	k7c2	z
Beeckmanových	Beeckmanův	k2eAgInPc2d1	Beeckmanův
zápisů	zápis	k1gInPc2	zápis
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgInS	psát
o	o	k7c6	o
algebře	algebra	k1gFnSc6	algebra
a	a	k8xC	a
kuželosečkách	kuželosečka	k1gFnPc6	kuželosečka
<g/>
;	;	kIx,	;
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1628	[number]	k4	1628
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
jeho	jeho	k3xOp3gNnPc1	jeho
Pravidla	pravidlo	k1gNnPc1	pravidlo
(	(	kIx(	(
<g/>
Regulae	Regula	k1gInPc1	Regula
ad	ad	k7c4	ad
directionem	direction	k1gInSc7	direction
ingenii	ingenium	k1gNnPc7	ingenium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
mu	on	k3xPp3gNnSc3	on
papežský	papežský	k2eAgInSc4d1	papežský
legát	legát	k1gInSc4	legát
kardinál	kardinál	k1gMnSc1	kardinál
Bérulle	Bérull	k1gMnSc2	Bérull
uložil	uložit	k5eAaPmAgInS	uložit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
tedy	tedy	k9	tedy
hledal	hledat	k5eAaImAgMnS	hledat
samotu	samota	k1gFnSc4	samota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nerušil	rušit	k5eNaImAgMnS	rušit
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Bretagni	Bretagne	k1gFnSc6	Bretagne
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1629	[number]	k4	1629
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
matematice	matematika	k1gFnSc3	matematika
a	a	k8xC	a
zkoumání	zkoumání	k1gNnSc3	zkoumání
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
asistoval	asistovat	k5eAaImAgMnS	asistovat
také	také	k9	také
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
pitev	pitva	k1gFnPc2	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spis	spis	k1gInSc1	spis
O	o	k7c6	o
světě	svět	k1gInSc6	svět
a	a	k8xC	a
Pojednání	pojednání	k1gNnSc6	pojednání
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
jen	jen	k9	jen
v	v	k7c6	v
Leibnizových	Leibnizův	k2eAgInPc6d1	Leibnizův
výpiscích	výpisek	k1gInPc6	výpisek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odsouzení	odsouzení	k1gNnSc6	odsouzení
Galileově	Galileův	k2eAgNnSc6d1	Galileovo
(	(	kIx(	(
<g/>
1633	[number]	k4	1633
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
svá	svůj	k3xOyFgNnPc4	svůj
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
Rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
<g/>
,	,	kIx,	,
Geometrie	geometrie	k1gFnSc1	geometrie
a	a	k8xC	a
Optika	optika	k1gFnSc1	optika
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Meditace	meditace	k1gFnSc1	meditace
o	o	k7c6	o
první	první	k4xOgFnSc6	první
filosofii	filosofie	k1gFnSc6	filosofie
(	(	kIx(	(
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
a	a	k8xC	a
Principy	princip	k1gInPc1	princip
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
hospodyní	hospodyně	k1gFnSc7	hospodyně
Helenou	Helena	k1gFnSc7	Helena
dcerku	dcerka	k1gFnSc4	dcerka
Francine	Francin	k1gInSc5	Francin
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
však	však	k9	však
už	už	k6eAd1	už
1640	[number]	k4	1640
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
mu	on	k3xPp3gMnSc3	on
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc1d3	veliký
zármutek	zármutek	k1gInSc1	zármutek
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1638	[number]	k4	1638
také	také	k9	také
čte	číst	k5eAaImIp3nS	číst
Komenského	Komenského	k2eAgFnSc4d1	Komenského
Pansofii	pansofie	k1gFnSc4	pansofie
a	a	k8xC	a
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
ji	on	k3xPp3gFnSc4	on
zprvu	zprvu	k6eAd1	zprvu
příznivě	příznivě	k6eAd1	příznivě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
1641	[number]	k4	1641
vydává	vydávat	k5eAaPmIp3nS	vydávat
Odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
Hobbesovy	Hobbesův	k2eAgFnPc4d1	Hobbesova
námitky	námitka	k1gFnPc4	námitka
proti	proti	k7c3	proti
Meditacím	meditace	k1gFnPc3	meditace
<g/>
,	,	kIx,	,
1642	[number]	k4	1642
se	se	k3xPyFc4	se
v	v	k7c6	v
Leidenu	Leideno	k1gNnSc6	Leideno
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Komenským	Komenský	k1gMnSc7	Komenský
a	a	k8xC	a
1643	[number]	k4	1643
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc7d3	nejstarší
dcerou	dcera	k1gFnSc7	dcera
Fridricha	Fridrich	k1gMnSc4	Fridrich
Falckého	falcký	k2eAgMnSc4d1	falcký
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
dlouho	dlouho	k6eAd1	dlouho
dopisuje	dopisovat	k5eAaImIp3nS	dopisovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
napsal	napsat	k5eAaPmAgMnS	napsat
Mersennovi	Mersenn	k1gMnSc3	Mersenn
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
už	už	k6eAd1	už
nemohu	moct	k5eNaImIp1nS	moct
číst	číst	k5eAaImF	číst
žádné	žádný	k3yNgFnPc4	žádný
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
leda	leda	k8xS	leda
dopisy	dopis	k1gInPc1	dopis
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Nemíchám	míchat	k5eNaImIp1nS	míchat
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
leda	leda	k8xS	leda
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
poučení	poučení	k1gNnSc4	poučení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Pascalem	Pascal	k1gMnSc7	Pascal
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
mu	on	k3xPp3gMnSc3	on
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
atmosférickým	atmosférický	k2eAgInSc7d1	atmosférický
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
1649	[number]	k4	1649
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
královny	královna	k1gFnSc2	královna
Kristiny	Kristina	k1gFnSc2	Kristina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dlouho	dlouho	k6eAd1	dlouho
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
do	do	k7c2	do
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Schůzky	schůzka	k1gFnPc4	schůzka
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
konaly	konat	k5eAaImAgInP	konat
brzo	brzo	k6eAd1	brzo
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
z	z	k7c2	z
tuhé	tuhý	k2eAgFnSc2d1	tuhá
zimy	zima	k1gFnSc2	zima
Descartes	Descartes	k1gMnSc1	Descartes
dostal	dostat	k5eAaPmAgMnS	dostat
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1650	[number]	k4	1650
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
byl	být	k5eAaImAgInS	být
snad	snad	k9	snad
i	i	k9	i
otráven	otráven	k2eAgMnSc1d1	otráven
<g/>
.	.	kIx.	.
1667	[number]	k4	1667
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
převezeny	převezen	k2eAgInPc1d1	převezen
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
jsou	být	k5eAaImIp3nP	být
pohřbeny	pohřben	k2eAgInPc1d1	pohřben
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Saint-Germain-des-Prés	Saint-Germaines-Prés	k1gInSc1	Saint-Germain-des-Prés
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
Descartovy	Descartův	k2eAgInPc4d1	Descartův
spisy	spis	k1gInPc4	spis
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
;	;	kIx,	;
dekretem	dekret	k1gInSc7	dekret
Svatého	svatý	k2eAgNnSc2d1	svaté
oficia	oficium	k1gNnSc2	oficium
ze	z	k7c2	z
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1663	[number]	k4	1663
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Index	index	k1gInSc4	index
zařazena	zařazen	k2eAgFnSc1d1	zařazena
práce	práce	k1gFnSc1	práce
Meditationes	Meditationes	k1gInSc1	Meditationes
de	de	k?	de
prima	prima	k1gFnSc1	prima
philosophia	philosophia	k1gFnSc1	philosophia
a	a	k8xC	a
dekrety	dekret	k1gInPc1	dekret
Svatého	svatý	k2eAgNnSc2d1	svaté
oficia	oficium	k1gNnSc2	oficium
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1663	[number]	k4	1663
a	a	k8xC	a
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1720	[number]	k4	1720
další	další	k2eAgInPc4d1	další
filozofické	filozofický	k2eAgInPc4d1	filozofický
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Descartovo	Descartův	k2eAgNnSc1d1	Descartovo
myšlení	myšlení	k1gNnSc1	myšlení
–	–	k?	–
filosofické	filosofický	k2eAgFnSc2d1	filosofická
i	i	k8xC	i
vědecké	vědecký	k2eAgFnSc2d1	vědecká
–	–	k?	–
je	být	k5eAaImIp3nS	být
hluboce	hluboko	k6eAd1	hluboko
poznamenáno	poznamenán	k2eAgNnSc4d1	poznamenáno
neklidem	neklid	k1gInSc7	neklid
a	a	k8xC	a
nejistotami	nejistota	k1gFnPc7	nejistota
jeho	jeho	k3xOp3gFnSc2	jeho
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Reformace	reformace	k1gFnPc1	reformace
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
podryly	podrýt	k5eAaPmAgInP	podrýt
středověkou	středověký	k2eAgFnSc4d1	středověká
důvěru	důvěra	k1gFnSc4	důvěra
nejen	nejen	k6eAd1	nejen
v	v	k7c4	v
Boží	boží	k2eAgNnSc4d1	boží
zjevení	zjevení	k1gNnSc4	zjevení
<g/>
,	,	kIx,	,
v	v	k7c6	v
bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
možnosti	možnost	k1gFnSc6	možnost
lidského	lidský	k2eAgNnSc2d1	lidské
poznání	poznání	k1gNnSc2	poznání
a	a	k8xC	a
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Koperník	Koperník	k1gInSc1	Koperník
a	a	k8xC	a
Galilei	Galile	k1gMnPc1	Galile
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
běžné	běžný	k2eAgFnSc2d1	běžná
smyslové	smyslový	k2eAgFnSc2d1	smyslová
jistoty	jistota	k1gFnSc2	jistota
–	–	k?	–
například	například	k6eAd1	například
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
nehybná	hybný	k2eNgFnSc1d1	nehybná
Země	země	k1gFnSc1	země
pod	pod	k7c7	pod
nohama	noha	k1gFnPc7	noha
-	-	kIx~	-
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Descarta	Descarta	k1gFnSc1	Descarta
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
skepse	skepse	k1gFnSc1	skepse
Montaigneova	Montaigneův	k2eAgFnSc1d1	Montaigneův
<g/>
,	,	kIx,	,
rozpad	rozpad	k1gInSc1	rozpad
scholastické	scholastický	k2eAgFnSc2d1	scholastická
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
stoické	stoický	k2eAgFnSc2d1	stoická
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jeho	jeho	k3xOp3gNnSc1	jeho
životní	životní	k2eAgNnSc1d1	životní
úsilí	úsilí	k1gNnSc1	úsilí
je	být	k5eAaImIp3nS	být
usilovným	usilovný	k2eAgNnSc7d1	usilovné
hledáním	hledání	k1gNnSc7	hledání
pevných	pevný	k2eAgFnPc2d1	pevná
jistot	jistota	k1gFnPc2	jistota
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
matematických	matematický	k2eAgFnPc2d1	matematická
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
filosofických	filosofický	k2eAgNnPc2d1	filosofické
<g/>
:	:	kIx,	:
Descartovo	Descartův	k2eAgNnSc1d1	Descartovo
řešení	řešení	k1gNnSc1	řešení
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
radikální	radikální	k2eAgFnSc6d1	radikální
skepsi	skepse	k1gFnSc6	skepse
<g/>
,	,	kIx,	,
metodickém	metodický	k2eAgNnSc6d1	metodické
zpochybnění	zpochybnění	k1gNnSc6	zpochybnění
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yQnSc6	co
lze	lze	k6eAd1	lze
pochybovat	pochybovat	k5eAaImF	pochybovat
–	–	k?	–
a	a	k8xC	a
právě	právě	k9	právě
tam	tam	k6eAd1	tam
najde	najít	k5eAaPmIp3nS	najít
pevnou	pevný	k2eAgFnSc4d1	pevná
půdu	půda	k1gFnSc4	půda
v	v	k7c6	v
sebejistotě	sebejistota	k1gFnSc6	sebejistota
myslící	myslící	k2eAgFnSc2d1	myslící
duše	duše	k1gFnSc2	duše
či	či	k8xC	či
subjektu	subjekt	k1gInSc2	subjekt
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
být	být	k5eAaImF	být
všechno	všechen	k3xTgNnSc1	všechen
poznání	poznání	k1gNnSc1	poznání
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
nejjednodušších	jednoduchý	k2eAgInPc2d3	nejjednodušší
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
si	se	k3xPyFc3	se
napřed	napřed	k6eAd1	napřed
zjednat	zjednat	k5eAaPmF	zjednat
jistotu	jistota	k1gFnSc4	jistota
o	o	k7c6	o
nepochybnosti	nepochybnost	k1gFnSc6	nepochybnost
svého	svůj	k3xOyFgNnSc2	svůj
východiska	východisko	k1gNnSc2	východisko
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
?	?	kIx.	?
</s>
<s>
Abych	aby	kYmCp1nS	aby
mohl	moct	k5eAaImAgInS	moct
bezpečně	bezpečně	k6eAd1	bezpečně
postupovat	postupovat	k5eAaImF	postupovat
kupředu	kupředu	k6eAd1	kupředu
<g/>
,	,	kIx,	,
nebudu	být	k5eNaImBp1nS	být
zprvu	zprvu	k6eAd1	zprvu
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
jisté	jistý	k2eAgNnSc4d1	jisté
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Budu	být	k5eAaImBp1nS	být
pochybovat	pochybovat	k5eAaImF	pochybovat
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
obstojí	obstát	k5eAaPmIp3nS	obstát
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
radikální	radikální	k2eAgFnSc7d1	radikální
pochybností	pochybnost	k1gFnSc7	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Pochybovat	pochybovat	k5eAaImF	pochybovat
musím	muset	k5eAaImIp1nS	muset
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
anebo	anebo	k8xC	anebo
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tento	tento	k3xDgInSc1	tento
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mne	já	k3xPp1nSc4	já
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
je	být	k5eAaImIp3nS	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
výmyslem	výmysl	k1gInSc7	výmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
zda	zda	k8xS	zda
jej	on	k3xPp3gMnSc4	on
vnímám	vnímat	k5eAaImIp1nS	vnímat
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
vždyť	vždyť	k9	vždyť
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obětí	oběť	k1gFnSc7	oběť
mnohých	mnohý	k2eAgInPc2d1	mnohý
smyslových	smyslový	k2eAgInPc2d1	smyslový
klamů	klam	k1gInPc2	klam
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pochybovat	pochybovat	k5eAaImF	pochybovat
musím	muset	k5eAaImIp1nS	muset
rovněž	rovněž	k9	rovněž
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
vůbec	vůbec	k9	vůbec
nejjistější	jistý	k2eAgFnPc1d3	nejjistější
<g/>
,	,	kIx,	,
o	o	k7c6	o
principech	princip	k1gInPc6	princip
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
náš	náš	k3xOp1gInSc1	náš
lidský	lidský	k2eAgInSc1d1	lidský
rozum	rozum	k1gInSc1	rozum
nedostačuje	dostačovat	k5eNaImIp3nS	dostačovat
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
nás	my	k3xPp1nPc4	my
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c4	v
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
takto	takto	k6eAd1	takto
<g />
.	.	kIx.	.
</s>
<s>
zpochybnili	zpochybnit	k5eAaPmAgMnP	zpochybnit
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yInSc6	co
můžeme	moct	k5eAaImIp1nP	moct
nějak	nějak	k6eAd1	nějak
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
předstírali	předstírat	k5eAaImAgMnP	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tělesa	těleso	k1gNnPc1	těleso
<g/>
;	;	kIx,	;
že	že	k8xS	že
sami	sám	k3xTgMnPc1	sám
nemáme	mít	k5eNaImIp1nP	mít
ruce	ruka	k1gFnPc4	ruka
ani	ani	k8xC	ani
nohy	noha	k1gFnPc4	noha
ani	ani	k8xC	ani
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nemůžeme	moct	k5eNaImIp1nP	moct
však	však	k9	však
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsme	být	k5eNaImIp1nP	být
my	my	k3xPp1nPc1	my
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
to	ten	k3xDgNnSc4	ten
myslíme	myslet	k5eAaImIp1nP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
neexistovalo	existovat	k5eNaImAgNnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
:	:	kIx,	:
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
Ego	ego	k1gNnSc4	ego
cogito	cogit	k2eAgNnSc4d1	cogito
<g/>
,	,	kIx,	,
ergo	ergo	k9	ergo
sum	suma	k1gFnPc2	suma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
první	první	k4xOgFnSc1	první
a	a	k8xC	a
nejjistější	jistý	k2eAgFnSc1d3	nejjistější
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
každému	každý	k3xTgMnSc3	každý
řádně	řádně	k6eAd1	řádně
filosofujícímu	filosofující	k2eAgMnSc3d1	filosofující
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podobnou	podobný	k2eAgFnSc4d1	podobná
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pochybuji	pochybovat	k5eAaImIp1nS	pochybovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgInS	napsat
už	už	k9	už
sv.	sv.	kA	sv.
Augustin	Augustin	k1gMnSc1	Augustin
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
nerozvedl	rozvést	k5eNaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Descartova	Descartův	k2eAgFnSc1d1	Descartova
pochybnost	pochybnost	k1gFnSc1	pochybnost
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
samoúčelná	samoúčelný	k2eAgFnSc1d1	samoúčelná
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
jisté	jistý	k2eAgFnSc2d1	jistá
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jsem	být	k5eAaImIp1nS	být
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
neotřesitelná	otřesitelný	k2eNgFnSc1d1	neotřesitelná
jistota	jistota	k1gFnSc1	jistota
v	v	k7c6	v
radikálním	radikální	k2eAgNnSc6d1	radikální
pochybování	pochybování	k1gNnSc6	pochybování
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
klam	klam	k1gInSc1	klam
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ne	ne	k9	ne
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
teď	teď	k6eAd1	teď
<g/>
,	,	kIx,	,
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
jistotě	jistota	k1gFnSc6	jistota
mám	mít	k5eAaImIp1nS	mít
zároveň	zároveň	k6eAd1	zároveň
kritérium	kritérium	k1gNnSc1	kritérium
a	a	k8xC	a
vzor	vzor	k1gInSc1	vzor
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
poznávám	poznávat	k5eAaImIp1nS	poznávat
stejně	stejně	k6eAd1	stejně
bezprostředně	bezprostředně	k6eAd1	bezprostředně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
jako	jako	k8xS	jako
tuto	tento	k3xDgFnSc4	tento
větu	věta	k1gFnSc4	věta
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
stejně	stejně	k6eAd1	stejně
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
ještě	ještě	k9	ještě
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
právě	právě	k9	právě
tak	tak	k6eAd1	tak
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
učiníme	učinit	k5eAaImIp1nP	učinit
další	další	k2eAgInSc4d1	další
krok	krok	k1gInSc4	krok
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
opravdové	opravdový	k2eAgFnSc2d1	opravdová
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jako	jako	k8xS	jako
další	další	k2eAgFnSc4d1	další
jistotu	jistota	k1gFnSc4	jistota
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
ideu	idea	k1gFnSc4	idea
Boha	bůh	k1gMnSc2	bůh
jako	jako	k8xS	jako
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
<g/>
,	,	kIx,	,
všemocné	všemocný	k2eAgFnSc2d1	všemocná
a	a	k8xC	a
vševědoucí	vševědoucí	k2eAgFnSc2d1	vševědoucí
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
idea	idea	k1gFnSc1	idea
nemůže	moct	k5eNaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
vnímání	vnímání	k1gNnSc2	vnímání
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
ani	ani	k8xC	ani
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nemohl	moct	k5eNaImAgMnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
konečný	konečný	k2eAgInSc1d1	konečný
a	a	k8xC	a
nedokonalý	dokonalý	k2eNgMnSc1d1	nedokonalý
tvor	tvor	k1gMnSc1	tvor
si	se	k3xPyFc3	se
nemůže	moct	k5eNaImIp3nS	moct
sám	sám	k3xTgMnSc1	sám
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
vytvořit	vytvořit	k5eAaPmF	vytvořit
ideu	idea	k1gFnSc4	idea
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
a	a	k8xC	a
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druhý	druhý	k4xOgInSc1	druhý
krok	krok	k1gInSc1	krok
můžeme	moct	k5eAaImIp1nP	moct
označit	označit	k5eAaPmF	označit
jako	jako	k9	jako
ontologický	ontologický	k2eAgInSc4d1	ontologický
důkaz	důkaz	k1gInSc4	důkaz
existence	existence	k1gFnSc2	existence
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
získává	získávat	k5eAaImIp3nS	získávat
jistotu	jistota	k1gFnSc4	jistota
o	o	k7c6	o
realitě	realita	k1gFnSc6	realita
smyslového	smyslový	k2eAgInSc2d1	smyslový
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
K	k	k7c3	k
vlastnostem	vlastnost	k1gFnPc3	vlastnost
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
bytosti	bytost	k1gFnSc2	bytost
musí	muset	k5eAaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
náležet	náležet	k5eAaImF	náležet
i	i	k9	i
pravdivost	pravdivost	k1gFnSc4	pravdivost
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
Bůh	bůh	k1gMnSc1	bůh
nebyl	být	k5eNaImAgMnS	být
pravdivý	pravdivý	k2eAgMnSc1d1	pravdivý
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
by	by	kYmCp3nS	by
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nemyslitelné	myslitelný	k2eNgNnSc1d1	nemyslitelné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mne	já	k3xPp1nSc4	já
chtěl	chtít	k5eAaImAgMnS	chtít
pravdymilovný	pravdymilovný	k2eAgMnSc1d1	pravdymilovný
Bůh	bůh	k1gMnSc1	bůh
klamat	klamat	k5eAaImF	klamat
a	a	k8xC	a
předstíral	předstírat	k5eAaImAgMnS	předstírat
před	před	k7c7	před
mým	můj	k3xOp1gInSc7	můj
zrakem	zrak	k1gInSc7	zrak
tento	tento	k3xDgInSc4	tento
svět	svět	k1gInSc1	svět
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
klamný	klamný	k2eAgInSc4d1	klamný
přelud	přelud	k1gInSc4	přelud
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
Descartových	Descartův	k2eAgInPc2d1	Descartův
výkonů	výkon	k1gInPc2	výkon
je	být	k5eAaImIp3nS	být
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
formulaci	formulace	k1gFnSc4	formulace
spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
obecné	obecný	k2eAgFnPc1d1	obecná
analytické	analytický	k2eAgFnPc1d1	analytická
metody	metoda	k1gFnPc1	metoda
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starší	starý	k2eAgFnSc2d2	starší
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgMnSc1	každý
autor	autor	k1gMnSc1	autor
začínal	začínat	k5eAaImAgMnS	začínat
znovu	znovu	k6eAd1	znovu
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
odvolával	odvolávat	k5eAaImAgMnS	odvolávat
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
předchůdce	předchůdce	k1gMnPc4	předchůdce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analytická	analytický	k2eAgFnSc1d1	analytická
metoda	metoda	k1gFnSc1	metoda
rozložení	rozložení	k1gNnSc4	rozložení
problému	problém	k1gInSc2	problém
na	na	k7c4	na
co	co	k3yInSc4	co
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lze	lze	k6eAd1	lze
spolehlivě	spolehlivě	k6eAd1	spolehlivě
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
,	,	kIx,	,
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jedni	jeden	k4xCgMnPc1	jeden
mohli	moct	k5eAaImAgMnP	moct
bez	bez	k7c2	bez
obav	obava	k1gFnPc2	obava
stavět	stavět	k5eAaImF	stavět
na	na	k7c6	na
výsledcích	výsledek	k1gInPc6	výsledek
druhých	druhý	k4xOgMnPc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
vědecká	vědecký	k2eAgFnSc1d1	vědecká
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
učinila	učinit	k5eAaPmAgFnS	učinit
z	z	k7c2	z
vědy	věda	k1gFnSc2	věda
kolektivní	kolektivní	k2eAgNnSc4d1	kolektivní
dílo	dílo	k1gNnSc4	dílo
tisíců	tisíc	k4xCgInPc2	tisíc
a	a	k8xC	a
tak	tak	k6eAd1	tak
umožnila	umožnit	k5eAaPmAgFnS	umožnit
její	její	k3xOp3gInSc4	její
nevídaný	vídaný	k2eNgInSc4d1	nevídaný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
odvodil	odvodit	k5eAaPmAgMnS	odvodit
ze	z	k7c2	z
zkušenosti	zkušenost	k1gFnSc2	zkušenost
s	s	k7c7	s
geometrickými	geometrický	k2eAgInPc7d1	geometrický
důkazy	důkaz	k1gInPc7	důkaz
<g/>
,	,	kIx,	,
formuloval	formulovat	k5eAaImAgMnS	formulovat
Descartes	Descartes	k1gMnSc1	Descartes
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
:	:	kIx,	:
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
(	(	kIx(	(
<g/>
nedokončených	dokončený	k2eNgNnPc6d1	nedokončené
<g/>
)	)	kIx)	)
Pravidlech	pravidlo	k1gNnPc6	pravidlo
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
intelektu	intelekt	k1gInSc2	intelekt
(	(	kIx(	(
<g/>
Regulae	Regulae	k1gInSc1	Regulae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
stručněji	stručně	k6eAd2	stručně
v	v	k7c6	v
Rozpravě	rozprava	k1gFnSc6	rozprava
<g/>
:	:	kIx,	:
Přijímat	přijímat	k5eAaImF	přijímat
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
samo	sám	k3xTgNnSc1	sám
představuje	představovat	k5eAaImIp3nS	představovat
tak	tak	k6eAd1	tak
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
zřetelně	zřetelně	k6eAd1	zřetelně
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nemohu	moct	k5eNaImIp1nS	moct
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
problém	problém	k1gInSc1	problém
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
co	co	k9	co
nejjednodušší	jednoduchý	k2eAgFnSc6d3	nejjednodušší
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
bezpečně	bezpečně	k6eAd1	bezpečně
poznat	poznat	k5eAaPmF	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Postupovat	postupovat	k5eAaImF	postupovat
od	od	k7c2	od
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
ke	k	k7c3	k
složitému	složitý	k2eAgNnSc3d1	složité
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Sestavit	sestavit	k5eAaPmF	sestavit
úplné	úplný	k2eAgInPc4d1	úplný
seznamy	seznam	k1gInPc4	seznam
a	a	k8xC	a
obecné	obecný	k2eAgInPc4d1	obecný
přehledy	přehled	k1gInPc4	přehled
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
nezapomněli	zapomnět	k5eNaImAgMnP	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
ty	ten	k3xDgInPc4	ten
nejobtížnější	obtížný	k2eAgInPc4d3	nejobtížnější
vědecké	vědecký	k2eAgInPc4d1	vědecký
problémy	problém	k1gInPc4	problém
lze	lze	k6eAd1	lze
takto	takto	k6eAd1	takto
rozložit	rozložit	k5eAaPmF	rozložit
na	na	k7c4	na
"	"	kIx"	"
<g/>
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
řetězce	řetězec	k1gInPc4	řetězec
<g/>
"	"	kIx"	"
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
kroků	krok	k1gInPc2	krok
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zachová	zachovat	k5eAaPmIp3nS	zachovat
jejich	jejich	k3xOp3gNnSc4	jejich
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Každá	každý	k3xTgFnSc1	každý
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsem	být	k5eAaImIp1nS	být
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
byla	být	k5eAaImAgFnS	být
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
mi	já	k3xPp1nSc3	já
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
nalézat	nalézat	k5eAaImF	nalézat
další	další	k2eAgNnSc1d1	další
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Descartes	Descartes	k1gMnSc1	Descartes
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
Boha	bůh	k1gMnSc4	bůh
jakožto	jakožto	k8xS	jakožto
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
a	a	k8xC	a
nestvořenou	stvořený	k2eNgFnSc4d1	nestvořená
substanci	substance	k1gFnSc4	substance
<g/>
,	,	kIx,	,
a	a	k8xC	a
vedle	vedle	k7c2	vedle
něho	on	k3xPp3gMnSc2	on
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
substance	substance	k1gFnPc1	substance
stvořené	stvořený	k2eAgFnPc1d1	stvořená
<g/>
:	:	kIx,	:
res	res	k?	res
extensa	extensa	k1gFnSc1	extensa
<g/>
,	,	kIx,	,
svět	svět	k1gInSc1	svět
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
substanci	substance	k1gFnSc4	substance
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
atributem	atribut	k1gInSc7	atribut
rozprostraněnosti	rozprostraněnost	k1gFnSc2	rozprostraněnost
<g/>
;	;	kIx,	;
res	res	k?	res
cogitans	cogitans	k1gInSc4	cogitans
<g/>
,	,	kIx,	,
nerozprostraněnou	rozprostraněný	k2eNgFnSc4d1	rozprostraněný
duchovní	duchovní	k2eAgFnSc4d1	duchovní
substanci	substance	k1gFnSc4	substance
s	s	k7c7	s
atributem	atribut	k1gInSc7	atribut
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
neprostorový	prostorový	k2eNgMnSc1d1	prostorový
a	a	k8xC	a
netělesný	tělesný	k2eNgMnSc1d1	netělesný
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
učení	učení	k1gNnSc6	učení
zastánce	zastánce	k1gMnSc2	zastánce
dualismu	dualismus	k1gInSc2	dualismus
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
subjektu	subjekt	k1gInSc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
i	i	k8xC	i
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc1	tělo
i	i	k8xC	i
duch	duch	k1gMnSc1	duch
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
člověku	člověk	k1gMnSc6	člověk
"	"	kIx"	"
<g/>
nejtěsněji	těsně	k6eAd3	těsně
svázány	svázán	k2eAgFnPc1d1	svázána
a	a	k8xC	a
jaksi	jaksi	k6eAd1	jaksi
promíšeny	promíšen	k2eAgInPc1d1	promíšen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
zásadně	zásadně	k6eAd1	zásadně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
substance	substance	k1gFnPc1	substance
být	být	k5eAaImF	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
bytosti	bytost	k1gFnSc6	bytost
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
působit	působit	k5eAaImF	působit
<g/>
,	,	kIx,	,
nedovede	dovést	k5eNaPmIp3nS	dovést
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
otázky	otázka	k1gFnPc4	otázka
pak	pak	k6eAd1	pak
navázali	navázat	k5eAaPmAgMnP	navázat
Descartovi	Descartův	k2eAgMnPc1d1	Descartův
následovníci	následovník	k1gMnPc1	následovník
(	(	kIx(	(
<g/>
Leibniz	Leibniz	k1gMnSc1	Leibniz
<g/>
,	,	kIx,	,
Spinoza	Spinoza	k1gFnSc1	Spinoza
<g/>
,	,	kIx,	,
okasionalismus	okasionalismus	k1gInSc1	okasionalismus
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poznávání	poznávání	k1gNnSc4	poznávání
světa	svět	k1gInSc2	svět
těles	těleso	k1gNnPc2	těleso
smysly	smysl	k1gInPc4	smysl
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
nejasné	jasný	k2eNgNnSc1d1	nejasné
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
klamné	klamný	k2eAgFnPc4d1	klamná
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
filosofové	filosof	k1gMnPc1	filosof
tohoto	tento	k3xDgInSc2	tento
racionalistického	racionalistický	k2eAgInSc2d1	racionalistický
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gInSc1	Descartes
to	ten	k3xDgNnSc1	ten
dokládá	dokládat	k5eAaImIp3nS	dokládat
příkladem	příklad	k1gInSc7	příklad
kousku	kousek	k1gInSc2	kousek
vosku	vosk	k1gInSc2	vosk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pouhým	pouhý	k2eAgNnSc7d1	pouhé
zahřátím	zahřátí	k1gNnSc7	zahřátí
změní	změnit	k5eAaPmIp3nS	změnit
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Plnohodnotné	plnohodnotný	k2eAgNnSc1d1	plnohodnotné
poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
v	v	k7c6	v
průzračných	průzračný	k2eAgInPc6d1	průzračný
<g/>
,	,	kIx,	,
rozumových	rozumový	k2eAgInPc6d1	rozumový
<g/>
,	,	kIx,	,
matematických	matematický	k2eAgInPc6d1	matematický
pojmech	pojem	k1gInPc6	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svět	svět	k1gInSc4	svět
těles	těleso	k1gNnPc2	těleso
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rozlehlost	rozlehlost	k1gFnSc1	rozlehlost
<g/>
,	,	kIx,	,
vyplnění	vyplnění	k1gNnSc1	vyplnění
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
jsou	být	k5eAaImIp3nP	být
prostorem	prostor	k1gInSc7	prostor
a	a	k8xC	a
prostor	prostor	k1gInSc1	prostor
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
prázdný	prázdný	k2eAgInSc1d1	prázdný
prostor	prostor	k1gInSc1	prostor
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
právě	právě	k6eAd1	právě
tím	ten	k3xDgMnSc7	ten
filosofem	filosof	k1gMnSc7	filosof
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nejvíc	nejvíc	k6eAd1	nejvíc
přičinil	přičinit	k5eAaPmAgMnS	přičinit
o	o	k7c6	o
vydělení	vydělení	k1gNnSc6	vydělení
vědy	věda	k1gFnSc2	věda
z	z	k7c2	z
celku	celek	k1gInSc2	celek
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
vědecké	vědecký	k2eAgFnSc6d1	vědecká
metodě	metoda	k1gFnSc6	metoda
(	(	kIx(	(
<g/>
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
zastřešení	zastřešení	k1gNnSc2	zastřešení
dílčích	dílčí	k2eAgFnPc2d1	dílčí
věd	věda	k1gFnPc2	věda
pomocí	pomocí	k7c2	pomocí
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
první	první	k4xOgFnSc6	první
filosofii	filosofie	k1gFnSc6	filosofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
"	"	kIx"	"
<g/>
Rozpravy	rozprava	k1gFnSc2	rozprava
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
vlastně	vlastně	k9	vlastně
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc4	první
tematizuje	tematizovat	k5eAaImIp3nS	tematizovat
samotný	samotný	k2eAgInSc1d1	samotný
proces	proces	k1gInSc1	proces
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Postupujeme	postupovat	k5eAaImIp1nP	postupovat
<g/>
-li	i	k?	-li
nejprve	nejprve	k6eAd1	nejprve
skeptickou	skeptický	k2eAgFnSc7d1	skeptická
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdybychom	kdyby	kYmCp1nP	kdyby
vše	všechen	k3xTgNnSc1	všechen
popřeli	popřít	k5eAaPmAgMnP	popřít
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
JÁ	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
popírá	popírat	k5eAaImIp3nS	popírat
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
jistota	jistota	k1gFnSc1	jistota
cogito	cogít	k5eAaPmNgNnS	cogít
<g/>
.	.	kIx.	.
</s>
<s>
Descartes	Descartes	k1gMnSc1	Descartes
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Fermatem	Ferma	k1gNnSc7	Ferma
platí	platit	k5eAaImIp3nS	platit
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
analytické	analytický	k2eAgFnSc2d1	analytická
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
aplikace	aplikace	k1gFnPc1	aplikace
algebraických	algebraický	k2eAgFnPc2d1	algebraická
metod	metoda	k1gFnPc2	metoda
v	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
číselného	číselný	k2eAgNnSc2d1	číselné
zkoumání	zkoumání	k1gNnSc2	zkoumání
geometrických	geometrický	k2eAgInPc2d1	geometrický
objektů	objekt	k1gInPc2	objekt
<g/>
;	;	kIx,	;
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
zavedení	zavedení	k1gNnSc4	zavedení
"	"	kIx"	"
<g/>
kartézského	kartézský	k2eAgInSc2d1	kartézský
<g/>
"	"	kIx"	"
systému	systém	k1gInSc2	systém
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starší	starší	k1gMnPc4	starší
spíše	spíše	k9	spíše
názorné	názorný	k2eAgFnSc2d1	názorná
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
obrazy	obraz	k1gInPc7	obraz
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
přímek	přímka	k1gFnPc2	přímka
<g/>
,	,	kIx,	,
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
analytická	analytický	k2eAgFnSc1d1	analytická
geometrie	geometrie	k1gFnSc1	geometrie
řeší	řešit	k5eAaImIp3nS	řešit
úlohy	úloha	k1gFnPc4	úloha
početně	početně	k6eAd1	početně
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
úlohu	úloha	k1gFnSc4	úloha
najít	najít	k5eAaPmF	najít
průsečík	průsečík	k1gInSc4	průsečík
dvou	dva	k4xCgFnPc2	dva
přímek	přímka	k1gFnPc2	přímka
se	se	k3xPyFc4	se
neřeší	řešit	k5eNaImIp3nS	řešit
graficky	graficky	k6eAd1	graficky
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
početně	početně	k6eAd1	početně
jako	jako	k9	jako
společné	společný	k2eAgNnSc4d1	společné
řešení	řešení	k1gNnSc4	řešení
dvou	dva	k4xCgFnPc2	dva
lineárních	lineární	k2eAgFnPc2d1	lineární
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
lze	lze	k6eAd1	lze
zkoumat	zkoumat	k5eAaImF	zkoumat
i	i	k9	i
složitější	složitý	k2eAgFnPc4d2	složitější
křivky	křivka	k1gFnPc4	křivka
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc4	řešení
jsou	být	k5eAaImIp3nP	být
libovolně	libovolně	k6eAd1	libovolně
přesná	přesný	k2eAgNnPc1d1	přesné
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
hledat	hledat	k5eAaImF	hledat
i	i	k9	i
v	v	k7c6	v
prostorech	prostor	k1gInPc6	prostor
o	o	k7c6	o
více	hodně	k6eAd2	hodně
dimenzích	dimenze	k1gFnPc6	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Geometrii	geometrie	k1gFnSc6	geometrie
předvádí	předvádět	k5eAaImIp3nS	předvádět
také	také	k9	také
obecné	obecný	k2eAgNnSc4d1	obecné
řešení	řešení	k1gNnSc4	řešení
tzv.	tzv.	kA	tzv.
Pappovy	Pappův	k2eAgFnSc2d1	Pappova
úlohy	úloha	k1gFnSc2	úloha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
starověký	starověký	k2eAgMnSc1d1	starověký
matematik	matematik	k1gMnSc1	matematik
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyřešit	vyřešit	k5eAaPmF	vyřešit
jen	jen	k9	jen
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
přímky	přímka	k1gFnPc4	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1	obecné
řešení	řešení	k1gNnSc1	řešení
úlohy	úloha	k1gFnSc2	úloha
platilo	platit	k5eAaImAgNnS	platit
za	za	k7c4	za
významný	významný	k2eAgInSc4d1	významný
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
že	že	k8xS	že
novověká	novověký	k2eAgFnSc1d1	novověká
věda	věda	k1gFnSc1	věda
už	už	k6eAd1	už
předčila	předčít	k5eAaPmAgFnS	předčít
řeckou	řecký	k2eAgFnSc7d1	řecká
<g/>
.	.	kIx.	.
</s>
<s>
Nejsoustavnější	soustavní	k2eAgInSc1d3	soustavní
výklad	výklad	k1gInSc1	výklad
Descartovy	Descartův	k2eAgFnSc2d1	Descartova
fyziky	fyzika	k1gFnSc2	fyzika
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
O	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sice	sice	k8xC	sice
napsal	napsat	k5eAaBmAgInS	napsat
už	už	k6eAd1	už
1633	[number]	k4	1633
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nepublikoval	publikovat	k5eNaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Spis	spis	k1gInSc1	spis
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
výklad	výklad	k1gInSc4	výklad
hypotetického	hypotetický	k2eAgInSc2d1	hypotetický
vzniku	vznik	k1gInSc2	vznik
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chce	chtít	k5eAaImIp3nS	chtít
Descartes	Descartes	k1gMnSc1	Descartes
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
"	"	kIx"	"
<g/>
možný	možný	k2eAgInSc1d1	možný
<g/>
"	"	kIx"	"
svět	svět	k1gInSc1	svět
musí	muset	k5eAaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
vypadat	vypadat	k5eAaImF	vypadat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vypadá	vypadat	k5eAaPmIp3nS	vypadat
ten	ten	k3xDgInSc1	ten
skutečný	skutečný	k2eAgInSc1d1	skutečný
<g/>
.	.	kIx.	.
</s>
<s>
Descartova	Descartův	k2eAgFnSc1d1	Descartova
fyzika	fyzika	k1gFnSc1	fyzika
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
geometrická	geometrický	k2eAgFnSc1d1	geometrická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
hmotu	hmota	k1gFnSc4	hmota
s	s	k7c7	s
rozlehlostí	rozlehlost	k1gFnSc7	rozlehlost
a	a	k8xC	a
ruší	rušit	k5eAaImIp3nS	rušit
starší	starý	k2eAgFnPc4d2	starší
představy	představa	k1gFnPc4	představa
tíže	tíž	k1gFnSc2	tíž
a	a	k8xC	a
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
pohybu	pohyb	k1gInSc2	pohyb
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
princip	princip	k1gInSc4	princip
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
,	,	kIx,	,
zachování	zachování	k1gNnSc4	zachování
souhrnné	souhrnný	k2eAgFnSc2d1	souhrnná
hybnosti	hybnost	k1gFnSc2	hybnost
při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
a	a	k8xC	a
přednost	přednost	k1gFnSc4	přednost
kruhového	kruhový	k2eAgInSc2d1	kruhový
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Optice	optika	k1gFnSc6	optika
formuluje	formulovat	k5eAaImIp3nS	formulovat
Descartes	Descartes	k1gInSc4	Descartes
zákony	zákon	k1gInPc1	zákon
odrazu	odraz	k1gInSc2	odraz
a	a	k8xC	a
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Snellův	Snellův	k2eAgInSc1d1	Snellův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
podává	podávat	k5eAaImIp3nS	podávat
také	také	k9	také
úplnou	úplný	k2eAgFnSc4d1	úplná
teorii	teorie	k1gFnSc4	teorie
duhy	duha	k1gFnSc2	duha
<g/>
,	,	kIx,	,
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
složitější	složitý	k2eAgFnSc2d2	složitější
soustavy	soustava	k1gFnSc2	soustava
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
mikroskop	mikroskop	k1gInSc4	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
Météores	Météoresa	k1gFnPc2	Météoresa
je	být	k5eAaImIp3nS	být
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
většinou	většinou	k6eAd1	většinou
mechanické	mechanický	k2eAgNnSc4d1	mechanické
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
různých	různý	k2eAgInPc2d1	různý
jevů	jev	k1gInPc2	jev
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
,	,	kIx,	,
mraků	mrak	k1gInPc2	mrak
<g/>
,	,	kIx,	,
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
duhy	duha	k1gFnSc2	duha
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Descartovy	Descartův	k2eAgFnPc1d1	Descartova
Vášně	vášeň	k1gFnPc1	vášeň
duše	duše	k1gFnSc2	duše
jsou	být	k5eAaImIp3nP	být
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
soustavný	soustavný	k2eAgInSc4d1	soustavný
výklad	výklad	k1gInSc4	výklad
"	"	kIx"	"
<g/>
pohybů	pohyb	k1gInPc2	pohyb
<g/>
"	"	kIx"	"
lidské	lidský	k2eAgFnPc4d1	lidská
duše	duše	k1gFnPc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starší	starý	k2eAgFnSc2d2	starší
<g/>
,	,	kIx,	,
aristotelské	aristotelský	k2eAgFnSc2d1	aristotelská
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokládala	pokládat	k5eAaImAgFnS	pokládat
duši	duše	k1gFnSc4	duše
za	za	k7c4	za
princip	princip	k1gInSc4	princip
života	život	k1gInSc2	život
a	a	k8xC	a
přisuzovala	přisuzovat	k5eAaImAgFnS	přisuzovat
ji	on	k3xPp3gFnSc4	on
tedy	tedy	k9	tedy
i	i	k9	i
rostlinám	rostlina	k1gFnPc3	rostlina
a	a	k8xC	a
živočichům	živočich	k1gMnPc3	živočich
<g/>
,	,	kIx,	,
připisuje	připisovat	k5eAaImIp3nS	připisovat
Descartes	Descartes	k1gMnSc1	Descartes
duši	duše	k1gFnSc4	duše
hlavně	hlavně	k6eAd1	hlavně
intelektuální	intelektuální	k2eAgFnSc2d1	intelektuální
schopnosti	schopnost	k1gFnSc2	schopnost
a	a	k8xC	a
pokládá	pokládat	k5eAaImIp3nS	pokládat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
výsadu	výsada	k1gFnSc4	výsada
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
se	se	k3xPyFc4	se
neřídí	řídit	k5eNaImIp3nS	řídit
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
zákony	zákon	k1gInPc7	zákon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
působí	působit	k5eAaImIp3nP	působit
pohyby	pohyb	k1gInPc1	pohyb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
substancí	substance	k1gFnSc7	substance
<g/>
;	;	kIx,	;
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
těsně	těsně	k6eAd1	těsně
spojena	spojit	k5eAaPmNgFnS	spojit
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
mozkové	mozkový	k2eAgFnSc2d1	mozková
šišinky	šišinka	k1gFnSc2	šišinka
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
živočichů	živočich	k1gMnPc2	živočich
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Descartes	Descartes	k1gMnSc1	Descartes
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
fungování	fungování	k1gNnSc4	fungování
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
krevní	krevní	k2eAgInSc1d1	krevní
oběh	oběh	k1gInSc1	oběh
i	i	k8xC	i
dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
ochlazování	ochlazování	k1gNnSc4	ochlazování
a	a	k8xC	a
ohřívání	ohřívání	k1gNnSc4	ohřívání
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
tělesných	tělesný	k2eAgFnPc2d1	tělesná
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
afekty	afekt	k1gInPc1	afekt
-	-	kIx~	-
"	"	kIx"	"
<g/>
vášněmi	vášeň	k1gFnPc7	vášeň
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
6	[number]	k4	6
základních	základní	k2eAgFnPc2d1	základní
<g/>
:	:	kIx,	:
údiv	údiv	k1gInSc1	údiv
<g/>
,	,	kIx,	,
touhu	touha	k1gFnSc4	touha
<g/>
,	,	kIx,	,
smutek	smutek	k1gInSc4	smutek
<g/>
,	,	kIx,	,
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
cca	cca	kA	cca
40	[number]	k4	40
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
naděje	naděje	k1gFnSc1	naděje
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
radosti	radost	k1gFnSc2	radost
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
-	-	kIx~	-
duše	duše	k1gFnPc1	duše
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
po	po	k7c6	po
čem	co	k3yInSc6	co
touží	toužit	k5eAaImIp3nS	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
afektů	afekt	k1gInPc2	afekt
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
vyvolávána	vyvoláván	k2eAgFnSc1d1	vyvolávána
podněty	podnět	k1gInPc7	podnět
přicházejícími	přicházející	k2eAgMnPc7d1	přicházející
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
škodlivé	škodlivý	k2eAgInPc1d1	škodlivý
silné	silný	k2eAgInPc1d1	silný
afekty	afekt	k1gInPc1	afekt
lze	lze	k6eAd1	lze
potlačit	potlačit	k5eAaPmF	potlačit
pomocí	pomocí	k7c2	pomocí
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
