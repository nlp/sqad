<s>
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
stranaKereszténydemokrata	stranaKereszténydemokrata	k1gFnSc1
Néppárt	Néppárta	k1gFnPc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
KDNP	KDNP	kA
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
19431989	#num#	k4
(	(	kIx(
<g/>
znovuobnovení	znovuobnovení	k1gNnSc1
<g/>
)	)	kIx)
Datum	datum	k1gNnSc1
rozpuštění	rozpuštění	k1gNnSc1
</s>
<s>
1949	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Zsolt	Zsolt	k2eAgInSc4d1
Semjén	Semjén	k1gInSc4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
1072	#num#	k4
Budapest	Budapest	k1gMnSc1
<g/>
,	,	kIx,
<g/>
István	István	k2eAgMnSc1d1
utca	utca	k1gMnSc1
-	-	kIx~
Dózsa	Dózsa	k1gFnSc1
György	Györg	k1gInPc1
út	út	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Katolikus	Katolikus	k1gInSc1
Szociális	Szociális	k1gFnSc2
NépmozgalomDemokrata	NépmozgalomDemokrat	k1gMnSc2
Néppárt	Néppárta	k1gFnPc2
Ideologie	ideologie	k1gFnSc2
</s>
<s>
křesťanská	křesťanský	k2eAgNnPc1d1
demokracienárodní	demokracienárodní	k2eAgNnPc1d1
konzervatismussociální	konzervatismussociální	k2eAgNnPc1d1
konzervatismusreakcionářství	konzervatismusreakcionářství	k1gNnPc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
pravice	pravice	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Mládežnická	mládežnický	k2eAgFnSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Ifjúsági	Ifjúsági	k6eAd1
Kereszténydemokrata	Kereszténydemokrata	k1gFnSc1
Szövetség	Szövetséga	k1gFnPc2
Stranické	stranický	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
</s>
<s>
Hazánk	Hazánk	k1gInSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Barvy	barva	k1gFnSc2
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
zlatá	zlatý	k2eAgFnSc1d1
Volební	volební	k2eAgFnSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
49,27	49,27	k4
<g/>
%	%	kIx~
(	(	kIx(
<g/>
volby	volba	k1gFnSc2
2018	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.kdnp.hu	www.kdnp.h	k1gMnSc3
Zisk	zisk	k1gInSc4
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Maďarský	maďarský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
199	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
KDNP	KDNP	kA
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Kereszténydemokrata	Kereszténydemokrata	k1gFnSc1
Néppárt	Néppárt	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
parlamentní	parlamentní	k2eAgFnSc1d1
pravicová	pravicový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
spolupracuje	spolupracovat	k5eAaImIp3nS
se	s	k7c7
stranou	strana	k1gFnSc7
Fidesz	Fidesz	k1gFnSc1
–	–	k?
Maďarská	maďarský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
1943	#num#	k4
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Maďarském	maďarský	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
její	její	k3xOp3gFnSc1
existence	existence	k1gFnSc1
neměla	mít	k5eNaImAgFnS
dlouhého	dlouhý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
nástupem	nástup	k1gInSc7
komunismu	komunismus	k1gInSc2
a	a	k8xC
vyhlášením	vyhlášení	k1gNnSc7
Maďarské	maďarský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
strana	strana	k1gFnSc1
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
působení	působení	k1gNnSc1
opět	opět	k6eAd1
zahájila	zahájit	k5eAaPmAgNnP
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgFnPc6
svobodných	svobodný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
1990	#num#	k4
získala	získat	k5eAaPmAgFnS
21	#num#	k4
křesel	křeslo	k1gNnPc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
vítězem	vítěz	k1gMnSc7
voleb	volba	k1gFnPc2
pravicovým	pravicový	k2eAgInSc7d1
MDF	MDF	kA
utvořila	utvořit	k5eAaPmAgFnS
vládní	vládní	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
1994	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
drtivě	drtivě	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
levicová	levicový	k2eAgFnSc1d1
MSZP	MSZP	kA
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
KDNP	KDNP	kA
v	v	k7c4
opozici	opozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
po	po	k7c4
dvě	dva	k4xCgNnPc4
volební	volební	k2eAgNnPc4d1
období	období	k1gNnPc4
1998	#num#	k4
a	a	k8xC
2002	#num#	k4
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
strana	strana	k1gFnSc1
do	do	k7c2
parlamentu	parlament	k1gInSc2
vůbec	vůbec	k9
nedostala	dostat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
volbami	volba	k1gFnPc7
2006	#num#	k4
se	se	k3xPyFc4
spojila	spojit	k5eAaPmAgFnS
do	do	k7c2
volební	volební	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
se	s	k7c7
stranou	strana	k1gFnSc7
Fidesz	Fidesza	k1gFnPc2
a	a	k8xC
znovu	znovu	k6eAd1
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
opoziční	opoziční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
se	s	k7c7
23	#num#	k4
mandáty	mandát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
parlamentní	parlamentní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
2010	#num#	k4
se	se	k3xPyFc4
KDNP	KDNP	kA
opět	opět	k6eAd1
rozhodla	rozhodnout	k5eAaPmAgFnS
jít	jít	k5eAaImF
do	do	k7c2
volební	volební	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
a	a	k8xC
sestavit	sestavit	k5eAaPmF
společnou	společný	k2eAgFnSc4d1
kandidátku	kandidátka	k1gFnSc4
s	s	k7c7
nejsilnější	silný	k2eAgFnSc7d3
pravicovou	pravicový	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Fidesz	Fidesza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Volební	volební	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
Fidesz	Fidesz	k1gInSc1
<g/>
–	–	k?
<g/>
KDNP	KDNP	kA
kandidovala	kandidovat	k5eAaImAgFnS
i	i	k9
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
a	a	k8xC
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
na	na	k7c6
jaře	jaro	k1gNnSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgFnPc6
volbách	volba	k1gFnPc6
strana	strana	k1gFnSc1
zvítězila	zvítězit	k5eAaPmAgFnS
<g/>
,	,	kIx,
v	v	k7c6
Parlamentních	parlamentní	k2eAgFnPc6d1
obhájila	obhájit	k5eAaPmAgFnS
ústavní	ústavní	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
(	(	kIx(
<g/>
133	#num#	k4
mandátů	mandát	k1gInPc2
z	z	k7c2
nově	nově	k6eAd1
199	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
získala	získat	k5eAaPmAgFnS
koalice	koalice	k1gFnSc1
13	#num#	k4
mandátů	mandát	k1gInPc2
z	z	k7c2
21	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
pro	pro	k7c4
KDNP	KDNP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2018	#num#	k4
strany	strana	k1gFnSc2
opět	opět	k6eAd1
kandidovaly	kandidovat	k5eAaImAgFnP
společně	společně	k6eAd1
a	a	k8xC
opět	opět	k6eAd1
obhájily	obhájit	k5eAaPmAgFnP
ústavní	ústavní	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
v	v	k7c6
parlamentu	parlament	k1gInSc6
133	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
16	#num#	k4
pro	pro	k7c4
KDNP	KDNP	kA
<g/>
.	.	kIx.
</s>
<s>
Předsedové	předseda	k1gMnPc1
strany	strana	k1gFnSc2
</s>
<s>
Sándor	Sándor	k1gMnSc1
Keresztes	Keresztes	k1gMnSc1
<g/>
:	:	kIx,
1989	#num#	k4
-	-	kIx~
1990	#num#	k4
</s>
<s>
László	László	k?
Surján	Surján	k1gInSc1
<g/>
:	:	kIx,
1990	#num#	k4
-	-	kIx~
1995	#num#	k4
</s>
<s>
György	Györg	k1gInPc1
Giczy	Gicza	k1gFnSc2
<g/>
:	:	kIx,
1995	#num#	k4
-	-	kIx~
2001	#num#	k4
</s>
<s>
Tivadar	Tivadar	k1gMnSc1
Bártók	Bártók	k1gMnSc1
<g/>
:	:	kIx,
2001	#num#	k4
-	-	kIx~
2002	#num#	k4
</s>
<s>
László	László	k?
Varga	Varga	k1gMnSc1
<g/>
:	:	kIx,
2002	#num#	k4
-	-	kIx~
2003	#num#	k4
</s>
<s>
Zsolt	Zsolt	k2eAgInSc1d1
Semjén	Semjén	k1gInSc1
<g/>
:	:	kIx,
2003	#num#	k4
-	-	kIx~
současnost	současnost	k1gFnSc4
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Demokrata	demokrat	k1gMnSc2
Néppárt	Néppárta	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
Hlasy	hlas	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
mandátů	mandát	k1gInPc2
</s>
<s>
Mandáty	mandát	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Úloha	úloha	k1gFnSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
</s>
<s>
1945	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
21	#num#	k4
</s>
<s>
0,49	0,49	k4
<g/>
%	%	kIx~
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
1947	#num#	k4
</s>
<s>
824	#num#	k4
259	#num#	k4
</s>
<s>
16,5	16,5	k4
<g/>
%	%	kIx~
</s>
<s>
60	#num#	k4
</s>
<s>
14,6	14,6	k4
<g/>
%	%	kIx~
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
Podle	podle	k7c2
dohody	dohoda	k1gFnSc2
získala	získat	k5eAaPmAgFnS
strana	strana	k1gFnSc1
dvě	dva	k4xCgNnPc4
křesla	křeslo	k1gNnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
FKgP	FKgP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Kereszténydemokrata	Kereszténydemokrata	k1gFnSc1
Néppárt	Néppárta	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Graf	graf	k1gInSc4
voleb	volba	k1gFnPc2
2006	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
I.	I.	kA
kolo	kolo	k1gNnSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
mandátů	mandát	k1gInPc2
</s>
<s>
Mandáty	mandát	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Úloha	úloha	k1gFnSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
Hlasy	hlas	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
Hlasy	hlas	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
1990	#num#	k4
</s>
<s>
317	#num#	k4
183	#num#	k4
</s>
<s>
6,46	6,46	k4
<g/>
%	%	kIx~
</s>
<s>
126	#num#	k4
636	#num#	k4
</s>
<s>
3,71	3,71	k4
<g/>
%	%	kIx~
</s>
<s>
21	#num#	k4
</s>
<s>
5,44	5,44	k4
<g/>
%	%	kIx~
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
379	#num#	k4
573	#num#	k4
</s>
<s>
7,03	7,03	k4
<g/>
%	%	kIx~
</s>
<s>
126	#num#	k4
616	#num#	k4
</s>
<s>
2,95	2,95	k4
<g/>
%	%	kIx~
</s>
<s>
22	#num#	k4
</s>
<s>
5,70	5,70	k4
<g/>
%	%	kIx~
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
116	#num#	k4
065	#num#	k4
</s>
<s>
2,59	2,59	k4
<g/>
%	%	kIx~
</s>
<s>
1	#num#	k4
479	#num#	k4
</s>
<s>
0,31	0,31	k4
<g/>
%	%	kIx~
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Mimo	mimo	k7c4
parlament	parlament	k1gInSc4
</s>
<s>
20021	#num#	k4
</s>
<s>
219	#num#	k4
029	#num#	k4
</s>
<s>
3,90	3,90	k4
<g/>
%	%	kIx~
</s>
<s>
5	#num#	k4
280	#num#	k4
<g/>
*	*	kIx~
</s>
<s>
0,12	0,12	k4
<g/>
%	%	kIx~
<g/>
*	*	kIx~
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Mimo	mimo	k7c4
parlament	parlament	k1gInSc4
</s>
<s>
20062	#num#	k4
</s>
<s>
2	#num#	k4
272	#num#	k4
979	#num#	k4
</s>
<s>
42,03	42,03	k4
<g/>
%	%	kIx~
</s>
<s>
1	#num#	k4
511	#num#	k4
176	#num#	k4
</s>
<s>
46,65	46,65	k4
<g/>
%	%	kIx~
</s>
<s>
23	#num#	k4
</s>
<s>
5,95	5,95	k4
<g/>
%	%	kIx~
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
20103	#num#	k4
</s>
<s>
2	#num#	k4
706	#num#	k4
292	#num#	k4
</s>
<s>
52,73	52,73	k4
<g/>
%	%	kIx~
</s>
<s>
620	#num#	k4
138	#num#	k4
</s>
<s>
53,81	53,81	k4
<g/>
%	%	kIx~
</s>
<s>
36	#num#	k4
</s>
<s>
9,32	9,32	k4
<g/>
%	%	kIx~
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
20144	#num#	k4
</s>
<s>
2	#num#	k4
264	#num#	k4
780	#num#	k4
</s>
<s>
44,87	44,87	k4
<g/>
%	%	kIx~
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
16	#num#	k4
</s>
<s>
8,04	8,04	k4
<g/>
%	%	kIx~
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
Společná	společný	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
KDNP	KDNP	kA
a	a	k8xC
Összefogás	Összefogás	k1gInSc4
Magyarországért	Magyarországért	k1gInSc1
Centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
Společná	společný	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
Fidesz	Fidesz	k1gInSc1
<g/>
–	–	k?
<g/>
KDNP	KDNP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
nezávislá	závislý	k2eNgFnSc1d1
parlamentní	parlamentní	k2eAgFnSc1d1
frakce	frakce	k1gFnSc1
KDNP	KDNP	kA
s	s	k7c7
23	#num#	k4
poslanci	poslanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
Společná	společný	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
Fidesz	Fidesz	k1gInSc1
<g/>
–	–	k?
<g/>
KDNP	KDNP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
nezávislá	závislý	k2eNgFnSc1d1
parlamentní	parlamentní	k2eAgFnSc1d1
frakce	frakce	k1gFnSc1
KDNP	KDNP	kA
s	s	k7c7
36	#num#	k4
poslanci	poslanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
Společná	společný	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
Fidesz	Fidesz	k1gInSc1
<g/>
–	–	k?
<g/>
KDNP	KDNP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
volbách	volba	k1gFnPc6
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
nezávislá	závislý	k2eNgFnSc1d1
parlamentní	parlamentní	k2eAgFnSc1d1
frakce	frakce	k1gFnSc1
KDNP	KDNP	kA
s	s	k7c7
16	#num#	k4
poslanci	poslanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kereszténydemokrata	Kereszténydemokrat	k1gMnSc2
Néppárt	Néppárta	k1gFnPc2
na	na	k7c6
maďarské	maďarský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Maďarský	maďarský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
Maďarska	Maďarsko	k1gNnSc2
</s>
<s>
Fidesz	Fidesz	k1gInSc1
–	–	k?
Maďarská	maďarský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
KDNP	KDNP	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
Maďarský	maďarský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
199	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
)	)	kIx)
<g/>
18	#num#	k4
<g/>
.	.	kIx.
parlamentní	parlamentní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2022	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fidesz	Fidesz	k1gInSc1
–	–	k?
Maďarská	maďarský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
117	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hnutí	hnutí	k1gNnSc2
za	za	k7c4
lepší	dobrý	k2eAgNnSc4d2
Maďarsko	Maďarsko	k1gNnSc4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maďarská	maďarský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Demokratická	demokratický	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Politika	politika	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
jiná	jiná	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
9	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
za	za	k7c4
Nový	nový	k2eAgInSc4d1
začátek	začátek	k1gInSc4
a	a	k8xC
1	#num#	k4
za	za	k7c4
Společně	společně	k6eAd1
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dialog	dialog	k1gInSc1
za	za	k7c4
Maďarsko	Maďarsko	k1gNnSc4
(	(	kIx(
<g/>
5	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
za	za	k7c2
Maďarská	maďarský	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
nestraníci	nestraník	k1gMnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Tamás	Tamás	k1gInSc1
Mellár	Mellár	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Zemská	zemský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
Němců	Němec	k1gMnPc2
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Imre	Imre	k1gInSc1
Ritter	Ritter	k1gInSc1
<g/>
)	)	kIx)
Strany	strana	k1gFnPc1
zastoupené	zastoupený	k2eAgFnPc1d1
v	v	k7c6
maďarskémparlamentu	maďarskémparlament	k1gInSc6
v	v	k7c6
letech	léto	k1gNnPc6
1990	#num#	k4
až	až	k9
2018	#num#	k4
</s>
<s>
EKGP	EKGP	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FKgP	FKgP	k1gFnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MDF	MDF	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MDNP	MDNP	kA
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MIÉP	MIÉP	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Összefogás	Összefogás	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
SZDSZ	SZDSZ	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
)	)	kIx)
<g/>
28	#num#	k4
<g/>
.	.	kIx.
parlamentní	parlamentní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fidesz	Fidesz	k1gInSc1
<g/>
−	−	k?
<g/>
KDNP	KDNP	kA
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jobbik	Jobbik	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MSZP	MSZP	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DK	DK	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
E	E	kA
<g/>
14	#num#	k4
<g/>
–	–	k?
<g/>
PM	PM	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
LMP	LMP	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Strany	strana	k1gFnSc2
zastoupené	zastoupený	k2eAgFnSc2d1
v	v	k7c6
Evropskémparlamentu	Evropskémparlament	k1gInSc6
v	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
až	až	k9
2014	#num#	k4
</s>
<s>
MDF	MDF	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SZDSZ	SZDSZ	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgInPc1d1
politické	politický	k2eAgInPc1d1
subjekty	subjekt	k1gInPc1
</s>
<s>
4	#num#	k4
<g/>
K	K	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
CSP	CSP	kA
•	•	k?
CM	cm	kA
•	•	k?
CP	CP	kA
•	•	k?
FKgP	FKgP	k1gFnSc2
•	•	k?
HP	HP	kA
•	•	k?
HVIM	HVIM	kA
•	•	k?
Iránytű	Iránytű	k1gFnSc2
•	•	k?
JESZ	JESZ	kA
•	•	k?
KÖSSZ	KÖSSZ	kA
•	•	k?
KTI	KTI	kA
•	•	k?
MCF	MCF	kA
•	•	k?
MCP	MCP	kA
•	•	k?
MIÉP	MIÉP	kA
•	•	k?
MKKP	MKKP	kA
•	•	k?
MMP	MMP	kA
•	•	k?
MMP	MMP	kA
2006	#num#	k4
•	•	k?
MoMa	MoM	k1gInSc2
•	•	k?
Momentum	Momentum	k1gNnSc1
•	•	k?
MSZDP	MSZDP	kA
•	•	k?
ÖP	ÖP	kA
•	•	k?
PKP	PKP	kA
•	•	k?
S	s	k7c7
•	•	k?
SEM	sem	k6eAd1
•	•	k?
Szem	Szem	k1gMnSc1
Párt	Párt	k1gMnSc1
•	•	k?
SMS	SMS	kA
•	•	k?
SZAVA	SZAVA	kA
•	•	k?
SZDP	SZDP	kA
•	•	k?
Szocdemek	Szocdemek	k1gInSc1
•	•	k?
SZU	SZU	kA
•	•	k?
TAM	tam	k6eAd1
•	•	k?
ÚK	ÚK	kA
•	•	k?
ZB	ZB	kA
Menšinové	menšinový	k2eAgFnSc2d1
a	a	k8xC
etnické	etnický	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
</s>
<s>
BOÖ	BOÖ	kA
•	•	k?
MGOÖ	MGOÖ	kA
•	•	k?
MNOÖ	MNOÖ	kA
•	•	k?
MROÖ	MROÖ	kA
•	•	k?
OHÖ	OHÖ	kA
•	•	k?
OLÖ	OLÖ	kA
•	•	k?
OÖÖ	OÖÖ	kA
<g/>
•	•	k?
ORÖ	ORÖ	kA
•	•	k?
ORÖ	ORÖ	kA
•	•	k?
OSZÖ	OSZÖ	kA
•	•	k?
OSZÖ	OSZÖ	kA
•	•	k?
SZOÖ	SZOÖ	kA
•	•	k?
UOÖ	UOÖ	kA
1	#num#	k4
Parlamentní	parlamentní	k2eAgFnSc2d1
volby	volba	k1gFnSc2
2018	#num#	k4
•	•	k?
2	#num#	k4
Volby	volba	k1gFnSc2
do	do	k7c2
EP	EP	kA
2014	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
Parlamentní	parlamentní	k2eAgFnSc2d1
volby	volba	k1gFnSc2
</s>
<s>
1848	#num#	k4
•	•	k?
1861	#num#	k4
•	•	k?
1865	#num#	k4
•	•	k?
1869	#num#	k4
•	•	k?
1872	#num#	k4
•	•	k?
1875	#num#	k4
•	•	k?
1878	#num#	k4
•	•	k?
1881	#num#	k4
•	•	k?
1884	#num#	k4
•	•	k?
1887	#num#	k4
•	•	k?
1892	#num#	k4
•	•	k?
1896	#num#	k4
•	•	k?
1901	#num#	k4
•	•	k?
1905	#num#	k4
•	•	k?
1906	#num#	k4
•	•	k?
1910	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1922	#num#	k4
•	•	k?
1926	#num#	k4
•	•	k?
1931	#num#	k4
•	•	k?
1935	#num#	k4
•	•	k?
1939	#num#	k4
•	•	k?
1945	#num#	k4
•	•	k?
1947	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1958	#num#	k4
•	•	k?
1963	#num#	k4
•	•	k?
1967	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
Prezidentské	prezidentský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
</s>
<s>
1946	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2022	#num#	k4
Komunální	komunální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
</s>
<s>
1949	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1958	#num#	k4
•	•	k?
1963	#num#	k4
•	•	k?
1967	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2019	#num#	k4
Eurovolby	Eurovolba	k1gFnSc2
</s>
<s>
2004	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2019	#num#	k4
Referenda	referendum	k1gNnSc2
</s>
<s>
1921	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2016	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
5085517-7	5085517-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83221292	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
127397595	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83221292	#num#	k4
</s>
