<s>
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
KDNP	KDNP	kA
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Kereszténydemokrata	Kereszténydemokrata	k1gFnSc1
Néppárt	Néppárt	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
parlamentní	parlamentní	k2eAgFnSc1d1
pravicová	pravicový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
spolupracuje	spolupracovat	k5eAaImIp3nS
se	s	k7c7
stranou	strana	k1gFnSc7
Fidesz	Fidesz	k1gFnSc1
–	–	k?
Maďarská	maďarský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>