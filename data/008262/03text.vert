<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Magnitskij	Magnitskij	k1gMnSc1	Magnitskij
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
Л	Л	k?	Л
М	М	k?	М
<g/>
;	;	kIx,	;
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1972	[number]	k4	1972
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
auditor	auditor	k1gMnSc1	auditor
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
uvěznění	uvěznění	k1gNnSc1	uvěznění
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
smrt	smrt	k1gFnSc1	smrt
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
porušení	porušení	k1gNnSc2	porušení
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedmatřicetiletý	sedmatřicetiletý	k2eAgMnSc1d1	sedmatřicetiletý
právník	právník	k1gMnSc1	právník
Magnitskij	Magnitskij	k1gMnSc1	Magnitskij
totiž	totiž	k9	totiž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
roční	roční	k2eAgFnSc6d1	roční
vazbě	vazba	k1gFnSc6	vazba
za	za	k7c4	za
dosud	dosud	k6eAd1	dosud
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
obvinil	obvinit	k5eAaPmAgMnS	obvinit
skupinu	skupina	k1gFnSc4	skupina
vysokých	vysoký	k2eAgMnPc2d1	vysoký
hodnostářů	hodnostář	k1gMnPc2	hodnostář
z	z	k7c2	z
miliardových	miliardový	k2eAgFnPc2d1	miliardová
machinací	machinace	k1gFnPc2	machinace
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
fondu	fond	k1gInSc2	fond
Hermitage	Hermitage	k1gFnSc1	Hermitage
Capital	Capital	k1gMnSc1	Capital
a	a	k8xC	a
ruského	ruský	k2eAgInSc2d1	ruský
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
za	za	k7c7	za
mřížemi	mříž	k1gFnPc7	mříž
s	s	k7c7	s
obviněním	obvinění	k1gNnSc7	obvinění
z	z	k7c2	z
údajných	údajný	k2eAgMnPc2d1	údajný
daňových	daňový	k2eAgMnPc2d1	daňový
úniků	únik	k1gInPc2	únik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnitskij	Magnitskít	k5eAaPmRp2nS	Magnitskít
si	se	k3xPyFc3	se
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
na	na	k7c4	na
nesnesitelné	snesitelný	k2eNgFnPc4d1	nesnesitelná
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
odpírání	odpírání	k1gNnPc4	odpírání
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
doznat	doznat	k5eAaPmF	doznat
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
důvodem	důvod	k1gInSc7	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc1	zánět
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
a	a	k8xC	a
zástava	zástava	k1gFnSc1	zástava
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ochránci	ochránce	k1gMnPc1	ochránce
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
mají	mít	k5eAaImIp3nP	mít
podezření	podezření	k1gNnSc3	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
cele	cela	k1gFnSc6	cela
umlátili	umlátit	k5eAaPmAgMnP	umlátit
vězeňští	vězeňský	k2eAgMnPc1d1	vězeňský
dozorci	dozorce	k1gMnPc1	dozorce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvinění	obviněný	k1gMnPc1	obviněný
proti	proti	k7c3	proti
jedinému	jediný	k2eAgInSc3d1	jediný
souzenému	souzený	k2eAgInSc3d1	souzený
v	v	k7c6	v
případě	případ	k1gInSc6	případ
smrti	smrt	k1gFnSc2	smrt
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
ruská	ruský	k2eAgFnSc1d1	ruská
prokuratura	prokuratura	k1gFnSc1	prokuratura
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
postup	postup	k1gInSc4	postup
ruských	ruský	k2eAgMnPc2d1	ruský
úřadů	úřada	k1gMnPc2	úřada
zavedly	zavést	k5eAaPmAgFnP	zavést
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
zákon	zákon	k1gInSc4	zákon
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
se	s	k7c7	s
sankcemi	sankce	k1gFnPc7	sankce
vůči	vůči	k7c3	vůči
ruským	ruský	k2eAgMnPc3d1	ruský
úředníkům	úředník	k1gMnPc3	úředník
porušujícím	porušující	k2eAgMnPc3d1	porušující
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
označila	označit	k5eAaPmAgFnS	označit
americký	americký	k2eAgInSc4d1	americký
krok	krok	k1gInSc4	krok
za	za	k7c4	za
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
úmrtí	úmrtí	k1gNnSc2	úmrtí
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
spáchání	spáchání	k1gNnSc3	spáchání
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
údajně	údajně	k6eAd1	údajně
způsobila	způsobit	k5eAaPmAgFnS	způsobit
srdeční	srdeční	k2eAgFnSc1d1	srdeční
nedostatečnost	nedostatečnost	k1gFnSc1	nedostatečnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otokem	otok	k1gInSc7	otok
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
důsledkem	důsledek	k1gInSc7	důsledek
jeho	on	k3xPp3gNnSc2	on
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
onemocnění	onemocnění	k1gNnSc2	onemocnění
cukrovkou	cukrovka	k1gFnSc7	cukrovka
a	a	k8xC	a
chronickou	chronický	k2eAgFnSc7d1	chronická
žloutenkou	žloutenka	k1gFnSc7	žloutenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Proces	proces	k1gInSc1	proces
se	s	k7c7	s
zemřelým	zemřelý	k1gMnSc7	zemřelý
==	==	k?	==
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
justice	justice	k1gFnSc1	justice
Magnitského	Magnitský	k2eAgNnSc2d1	Magnitský
ještě	ještě	k6eAd1	ještě
posmrtně	posmrtně	k6eAd1	posmrtně
obvinila	obvinit	k5eAaPmAgFnS	obvinit
z	z	k7c2	z
daňového	daňový	k2eAgInSc2d1	daňový
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
novodobé	novodobý	k2eAgFnSc6d1	novodobá
historii	historie	k1gFnSc6	historie
tak	tak	k6eAd1	tak
soudila	soudit	k5eAaImAgFnS	soudit
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
moskevského	moskevský	k2eAgInSc2d1	moskevský
soudu	soud	k1gInSc2	soud
zahájit	zahájit	k5eAaPmF	zahájit
proces	proces	k1gInSc4	proces
nezvrátila	zvrátit	k5eNaPmAgFnS	zvrátit
ani	ani	k8xC	ani
kritika	kritika	k1gFnSc1	kritika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Lidskoprávní	lidskoprávní	k2eAgFnPc1d1	lidskoprávní
organizace	organizace	k1gFnPc1	organizace
proces	proces	k1gInSc4	proces
kritizovaly	kritizovat	k5eAaImAgFnP	kritizovat
a	a	k8xC	a
upozorňovaly	upozorňovat	k5eAaImAgFnP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevře	otevřít	k5eAaPmIp3nS	otevřít
úplně	úplně	k6eAd1	úplně
novou	nový	k2eAgFnSc4d1	nová
kapitolu	kapitola	k1gFnSc4	kapitola
omezování	omezování	k1gNnSc2	omezování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
shledal	shledat	k5eAaPmAgInS	shledat
ruský	ruský	k2eAgInSc1d1	ruský
soud	soud	k1gInSc1	soud
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
Sergeje	Sergej	k1gMnSc2	Sergej
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
vinným	vinný	k1gMnPc3	vinný
z	z	k7c2	z
daňových	daňový	k2eAgInPc2d1	daňový
úniků	únik	k1gInPc2	únik
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
aktivitám	aktivita	k1gFnPc3	aktivita
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
a	a	k8xC	a
také	také	k9	také
ředitele	ředitel	k1gMnSc4	ředitel
britského	britský	k2eAgInSc2d1	britský
investičního	investiční	k2eAgInSc2d1	investiční
fondu	fond	k1gInSc2	fond
Hermitage	Hermitag	k1gFnSc2	Hermitag
Capital	Capital	k1gMnSc1	Capital
Williama	William	k1gMnSc4	William
Browdera	Browder	k1gMnSc4	Browder
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
poslal	poslat	k5eAaPmAgInS	poslat
soud	soud	k1gInSc1	soud
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
na	na	k7c4	na
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgInS	přijít
podle	podle	k7c2	podle
prokurátora	prokurátor	k1gMnSc4	prokurátor
místní	místní	k2eAgInSc1d1	místní
<g/>
,	,	kIx,	,
státní	státní	k2eAgInSc1d1	státní
i	i	k8xC	i
federální	federální	k2eAgInSc1d1	federální
rozpočet	rozpočet	k1gInSc1	rozpočet
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
522	[number]	k4	522
milionů	milion	k4xCgInPc2	milion
rublů	rubl	k1gInPc2	rubl
(	(	kIx(	(
<g/>
asi	asi	k9	asi
321	[number]	k4	321
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
Magnitského	Magnitský	k2eAgInSc2d1	Magnitský
byl	být	k5eAaImAgInS	být
případ	případ	k1gInSc1	případ
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Browdera	Browdero	k1gNnSc2	Browdero
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
politicky	politicky	k6eAd1	politicky
motivovaný	motivovaný	k2eAgInSc1d1	motivovaný
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zdiskreditovat	zdiskreditovat	k5eAaPmF	zdiskreditovat
jeho	jeho	k3xOp3gFnSc4	jeho
i	i	k9	i
zesnulého	zesnulý	k1gMnSc2	zesnulý
Magnitského	Magnitský	k2eAgMnSc2d1	Magnitský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
