<s>
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
Hertfordu	Hertford	k1gInSc6	Hertford
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
propagátorem	propagátor	k1gMnSc7	propagátor
byl	být	k5eAaImAgMnS	být
zejména	zejména	k9	zejména
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
<g/>
,	,	kIx,	,
a	a	k8xC	a
hardrockových	hardrockový	k2eAgInPc2d1	hardrockový
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
Ritchie	Ritchie	k1gFnPc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
těchto	tento	k3xDgMnPc2	tento
dvou	dva	k4xCgMnPc2	dva
druhů	druh	k1gMnPc2	druh
hudby	hudba	k1gFnSc2	hudba
oslovilo	oslovit	k5eAaPmAgNnS	oslovit
mladé	mladá	k1gFnSc2	mladá
posluchače	posluchač	k1gMnSc4	posluchač
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
tehdy	tehdy	k6eAd1	tehdy
patřili	patřit	k5eAaImAgMnP	patřit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
rockové	rockový	k2eAgFnPc4d1	rocková
skupiny	skupina	k1gFnPc4	skupina
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
také	také	k9	také
pocházejí	pocházet	k5eAaImIp3nP	pocházet
jejich	jejich	k3xOp3gFnPc4	jejich
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
koncertní	koncertní	k2eAgNnSc4d1	koncertní
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
průkopníky	průkopník	k1gMnPc4	průkopník
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
moderního	moderní	k2eAgInSc2d1	moderní
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
za	za	k7c4	za
heavymetalovou	heavymetalový	k2eAgFnSc4d1	heavymetalová
skupinu	skupina	k1gFnSc4	skupina
nepovažovali	považovat	k5eNaImAgMnP	považovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
Episode	Episod	k1gMnSc5	Episod
Six	Six	k1gMnSc5	Six
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vydala	vydat	k5eAaPmAgFnS	vydat
několik	několik	k4yIc4	několik
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skupinu	skupina	k1gFnSc4	skupina
tvořili	tvořit	k5eAaImAgMnP	tvořit
Ian	Ian	k1gMnSc1	Ian
Gillan	Gillan	k1gMnSc1	Gillan
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Graham	graham	k1gInSc1	graham
Dimmock	Dimmock	k1gInSc1	Dimmock
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
Glover	Glover	k1gInSc1	Glover
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Lander	Lander	k1gMnSc1	Lander
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Sheila	Sheila	k1gFnSc1	Sheila
Carter	Carter	k1gMnSc1	Carter
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
Harvey	Harvey	k1gInPc4	Harvey
Shields	Shieldsa	k1gFnPc2	Shieldsa
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
přes	přes	k7c4	přes
četná	četný	k2eAgNnPc4d1	četné
turné	turné	k1gNnPc4	turné
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
skupina	skupina	k1gFnSc1	skupina
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
The	The	k1gFnSc1	The
Flower	Flower	k1gInSc4	Flower
Pot	pot	k1gInSc1	pot
Men	Men	k1gFnSc1	Men
and	and	k?	and
their	their	k1gInSc1	their
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
The	The	k1gFnSc1	The
Ivy	Ivo	k1gMnSc2	Ivo
League	Leagu	k1gMnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
názvu	název	k1gInSc2	název
dětského	dětský	k2eAgNnSc2d1	dětské
představení	představení	k1gNnSc2	představení
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Flowerpot	Flowerpot	k1gMnSc1	Flowerpot
Men	Men	k1gMnSc1	Men
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
slovní	slovní	k2eAgFnSc1d1	slovní
hříčka	hříčka	k1gFnSc1	hříčka
narážející	narážející	k2eAgFnSc1d1	narážející
na	na	k7c4	na
éru	éra	k1gFnSc4	éra
"	"	kIx"	"
<g/>
flower	flower	k1gMnSc1	flower
power	power	k1gMnSc1	power
<g/>
"	"	kIx"	"
a	a	k8xC	a
pěstování	pěstování	k1gNnSc1	pěstování
"	"	kIx"	"
<g/>
konopí	konopí	k1gNnSc1	konopí
<g/>
"	"	kIx"	"
v	v	k7c6	v
květináčích	květináč	k1gInPc6	květináč
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
písnička	písnička	k1gFnSc1	písnička
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Go	Go	k1gFnSc7	Go
to	ten	k3xDgNnSc4	ten
San	San	k1gFnSc7	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
některými	některý	k3yIgFnPc7	některý
posluchači	posluchač	k1gMnPc7	posluchač
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
parodii	parodie	k1gFnSc4	parodie
na	na	k7c4	na
písničku	písnička	k1gFnSc4	písnička
"	"	kIx"	"
<g/>
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
(	(	kIx(	(
<g/>
Be	Be	k1gMnSc1	Be
Sure	Sur	k1gFnSc2	Sur
to	ten	k3xDgNnSc4	ten
Wear	Wear	k1gMnSc1	Wear
Flowers	Flowersa	k1gFnPc2	Flowersa
in	in	k?	in
Your	Your	k1gMnSc1	Your
Hair	Hair	k1gMnSc1	Hair
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
Scotta	Scott	k1gMnSc2	Scott
McKenzieho	McKenzie	k1gMnSc2	McKenzie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skupina	skupina	k1gFnSc1	skupina
to	ten	k3xDgNnSc4	ten
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
tvořili	tvořit	k5eAaImAgMnP	tvořit
Tony	Tony	k1gMnSc1	Tony
Burrows	Burrows	k1gInSc4	Burrows
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Landon	Landon	k1gMnSc1	Landon
<g/>
,	,	kIx,	,
Robin	robin	k2eAgMnSc1d1	robin
Shaw	Shaw	k1gMnSc1	Shaw
a	a	k8xC	a
Pete	Pete	k1gNnSc1	Pete
Nelson	Nelson	k1gMnSc1	Nelson
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zpěváci	zpěvák	k1gMnPc1	zpěvák
<g/>
,	,	kIx,	,
Ged	Ged	k1gMnSc1	Ged
Peck	Peck	k1gMnSc1	Peck
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Nick	Nick	k1gInSc1	Nick
Simper	Simper	k1gInSc1	Simper
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
Carlo	Carlo	k1gNnSc1	Carlo
Little	Little	k1gFnSc2	Little
bicí	bicí	k2eAgFnSc2d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Lord	lord	k1gMnSc1	lord
dříve	dříve	k6eAd2	dříve
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
The	The	k1gMnPc7	The
Artwoods	Artwoods	k1gInSc4	Artwoods
<g/>
,	,	kIx,	,
Nick	Nick	k1gInSc4	Nick
Simper	Simpra	k1gFnPc2	Simpra
s	s	k7c7	s
Johnny	Johnno	k1gNnPc7	Johnno
Kidd	Kidd	k1gMnSc1	Kidd
&	&	k?	&
The	The	k1gMnSc1	The
Pirates	Pirates	k1gMnSc1	Pirates
<g/>
,	,	kIx,	,
Screaming	Screaming	k1gInSc1	Screaming
Lord	lord	k1gMnSc1	lord
Sutch	Sutch	k1gMnSc1	Sutch
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
The	The	k1gMnSc1	The
Savages	Savages	k1gMnSc1	Savages
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
též	též	k9	též
kytarista	kytarista	k1gMnSc1	kytarista
Ritchie	Ritchie	k1gFnSc2	Ritchie
Blackmore	Blackmor	k1gMnSc5	Blackmor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Searchers	Searchers	k1gInSc1	Searchers
<g/>
,	,	kIx,	,
Chris	Chris	k1gInSc1	Chris
Curtis	Curtis	k1gFnSc7	Curtis
londýnského	londýnský	k2eAgMnSc2d1	londýnský
obchodníka	obchodník	k1gMnSc2	obchodník
Tonyho	Tony	k1gMnSc2	Tony
Edwardse	Edwards	k1gMnSc2	Edwards
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
dělat	dělat	k5eAaImF	dělat
manažera	manažer	k1gMnSc4	manažer
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
skupině	skupina	k1gFnSc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Edwards	Edwards	k6eAd1	Edwards
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
financoval	financovat	k5eAaBmAgMnS	financovat
tento	tento	k3xDgInSc4	tento
podnik	podnik	k1gInSc4	podnik
společně	společně	k6eAd1	společně
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
dalšími	další	k2eAgMnPc7d1	další
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
Johnem	John	k1gMnSc7	John
Colettou	Coletta	k1gMnSc7	Coletta
a	a	k8xC	a
Ronem	Ron	k1gMnSc7	Ron
Hirem	Hir	k1gMnSc7	Hir
(	(	kIx(	(
<g/>
Hire-Edwards-Coletta	Hire-Edwards-Coletta	k1gFnSc1	Hire-Edwards-Coletta
-	-	kIx~	-
HEC	HEC	k?	HEC
Enterprises	Enterprises	k1gInSc1	Enterprises
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Curtis	Curtis	k1gFnSc1	Curtis
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
budování	budování	k1gNnSc2	budování
skupiny	skupina	k1gFnSc2	skupina
známé	známý	k1gMnPc4	známý
jako	jako	k8xS	jako
Roundabout	Roundabout	k1gMnSc1	Roundabout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc3	první
příchozí	příchozí	k1gFnSc3	příchozí
byl	být	k5eAaImAgMnS	být
klávesista	klávesista	k1gMnSc1	klávesista
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
varhany	varhany	k1gInPc4	varhany
zn.	zn.	kA	zn.
Hammond	Hammond	k1gInSc4	Hammond
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
kytaristu	kytarista	k1gMnSc4	kytarista
Ritchieho	Ritchie	k1gMnSc2	Ritchie
Blackmora	Blackmor	k1gMnSc2	Blackmor
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Hamburku	Hamburk	k1gInSc2	Hamburk
na	na	k7c4	na
konkurz	konkurz	k1gInSc4	konkurz
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Curtis	Curtis	k1gFnSc1	Curtis
sám	sám	k3xTgInSc4	sám
pak	pak	k6eAd1	pak
brzy	brzy	k6eAd1	brzy
odpadl	odpadnout	k5eAaPmAgInS	odpadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
HEC	HEC	k?	HEC
Enterprises	Enterprises	k1gInSc1	Enterprises
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Lord	lord	k1gMnSc1	lord
a	a	k8xC	a
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
chuť	chuť	k1gFnSc4	chuť
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
brzy	brzy	k6eAd1	brzy
byli	být	k5eAaImAgMnP	být
přibráni	přibrat	k5eAaPmNgMnP	přibrat
basák	basák	k1gMnSc1	basák
Nick	Nick	k1gMnSc1	Nick
Simper	Simper	k1gMnSc1	Simper
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zpěvák	zpěvák	k1gMnSc1	zpěvák
Rod	rod	k1gInSc4	rod
Evans	Evansa	k1gFnPc2	Evansa
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
Ian	Ian	k1gMnSc1	Ian
Paice	Paice	k1gMnSc1	Paice
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc3	The
Maze	Maga	k1gFnSc3	Maga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
vystoupeních	vystoupení	k1gNnPc6	vystoupení
na	na	k7c6	na
krátkém	krátký	k2eAgNnSc6d1	krátké
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Dánsku	Dánsko	k1gNnSc6	Dánsko
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
jménem	jméno	k1gNnSc7	jméno
navrženým	navržený	k2eAgNnSc7d1	navržené
Ritchiem	Ritchium	k1gNnSc7	Ritchium
podle	podle	k7c2	podle
písničky	písnička	k1gFnSc2	písnička
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
od	od	k7c2	od
Petera	Peter	k1gMnSc2	Peter
De	De	k?	De
Roseho	Rose	k1gMnSc2	Rose
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
písničkou	písnička	k1gFnSc7	písnička
Ritchieho	Ritchie	k1gMnSc2	Ritchie
babičky	babička	k1gFnSc2	babička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
měla	mít	k5eAaImAgFnS	mít
skupina	skupina	k1gFnSc1	skupina
fantastický	fantastický	k2eAgInSc4d1	fantastický
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
ne	ne	k9	ne
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
s	s	k7c7	s
předělávkou	předělávka	k1gFnSc7	předělávka
od	od	k7c2	od
Joe	Joe	k1gMnSc2	Joe
Southa	South	k1gMnSc2	South
"	"	kIx"	"
<g/>
Hush	Hush	k1gMnSc1	Hush
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
Shades	Shades	k1gMnSc1	Shades
of	of	k?	of
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byli	být	k5eAaImAgMnP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
jako	jako	k9	jako
předkapela	předkapela	k1gFnSc1	předkapela
pro	pro	k7c4	pro
turné	turné	k1gNnSc4	turné
skupiny	skupina	k1gFnSc2	skupina
Cream	Cream	k1gInSc4	Cream
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Goodbye	Goodbye	k1gNnSc6	Goodbye
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
vyřazeni	vyřazen	k2eAgMnPc1d1	vyřazen
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
proto	proto	k8xC	proto
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
lepší	dobrý	k2eAgFnSc4d2	lepší
než	než	k8xS	než
Cream	Cream	k1gInSc4	Cream
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Taliesyn	Taliesyn	k1gMnSc1	Taliesyn
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
USA	USA	kA	USA
shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
turné	turné	k1gNnSc7	turné
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
doma	doma	k6eAd1	doma
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
třetím	třetí	k4xOgNnSc7	třetí
albem	album	k1gNnSc7	album
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
skladbu	skladba	k1gFnSc4	skladba
se	s	k7c7	s
smyčci	smyčec	k1gInPc7	smyčec
a	a	k8xC	a
dechovými	dechový	k2eAgInPc7d1	dechový
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
třech	tři	k4xCgInPc6	tři
albech	album	k1gNnPc6	album
a	a	k8xC	a
intenzivním	intenzivní	k2eAgNnSc6d1	intenzivní
koncertování	koncertování	k1gNnSc6	koncertování
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Rod	rod	k1gInSc4	rod
Evans	Evansa	k1gFnPc2	Evansa
a	a	k8xC	a
Nick	Nicka	k1gFnPc2	Nicka
Simper	Simpra	k1gFnPc2	Simpra
bez	bez	k7c2	bez
okolků	okolek	k1gInPc2	okolek
vykopnuti	vykopnout	k5eAaPmNgMnP	vykopnout
a	a	k8xC	a
nahrazeni	nahradit	k5eAaPmNgMnP	nahradit
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Ianem	Ianus	k1gMnSc7	Ianus
Gillanem	Gillan	k1gMnSc7	Gillan
a	a	k8xC	a
basákem	basák	k1gMnSc7	basák
Rogerem	Roger	k1gMnSc7	Roger
Gloverem	Glover	k1gInSc7	Glover
<g/>
,	,	kIx,	,
bývalými	bývalý	k2eAgInPc7d1	bývalý
členy	člen	k1gInPc7	člen
skupiny	skupina	k1gFnSc2	skupina
Episode	Episod	k1gMnSc5	Episod
Six	Six	k1gMnSc5	Six
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
sestava	sestava	k1gFnSc1	sestava
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc1	Purple
"	"	kIx"	"
<g/>
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obsazení	obsazený	k2eAgMnPc1d1	obsazený
vydali	vydat	k5eAaPmAgMnP	vydat
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
předělávku	předělávka	k1gFnSc4	předělávka
písničky	písnička	k1gFnSc2	písnička
dvojice	dvojice	k1gFnSc1	dvojice
Greenaway-Cook	Greenaway-Cook	k1gInSc1	Greenaway-Cook
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Hallelujah	Hallelujaha	k1gFnPc2	Hallelujaha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
propadákem	propadák	k1gInSc7	propadák
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
"	"	kIx"	"
<g/>
Hair	Hair	k1gMnSc1	Hair
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
pak	pak	k6eAd1	pak
získala	získat	k5eAaPmAgFnS	získat
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
publicitu	publicita	k1gFnSc4	publicita
třídílným	třídílný	k2eAgInSc7d1	třídílný
symfonickým	symfonický	k2eAgInSc7d1	symfonický
eposem	epos	k1gInSc7	epos
Concerto	Concerta	k1gFnSc5	Concerta
for	forum	k1gNnPc2	forum
Group	Group	k1gInSc1	Group
and	and	k?	and
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
složeným	složený	k2eAgMnSc7d1	složený
Johnem	John	k1gMnSc7	John
Lordem	lord	k1gMnSc7	lord
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc7	jeho
sólovým	sólový	k2eAgInSc7d1	sólový
projektem	projekt	k1gInSc7	projekt
a	a	k8xC	a
provedeným	provedený	k2eAgInSc7d1	provedený
v	v	k7c6	v
Royal	Royal	k1gInSc1	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gInSc1	Hall
skupinou	skupina	k1gFnSc7	skupina
společně	společně	k6eAd1	společně
se	s	k7c7	s
symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
Royal	Royal	k1gInSc1	Royal
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
,	,	kIx,	,
řízeným	řízený	k2eAgNnSc7d1	řízené
Malcolm	Malcol	k1gNnSc7	Malcol
Arnoldem	Arnold	k1gMnSc7	Arnold
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
Five	Fiv	k1gInSc2	Fiv
Bridges	Bridges	k1gInSc4	Bridges
od	od	k7c2	od
The	The	k1gFnSc2	The
Nice	Nice	k1gFnSc2	Nice
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
spoluprací	spolupráce	k1gFnPc2	spolupráce
mezi	mezi	k7c7	mezi
rockovou	rockový	k2eAgFnSc7d1	rocková
skupinou	skupina	k1gFnSc7	skupina
a	a	k8xC	a
symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
(	(	kIx(	(
<g/>
Blackmore	Blackmor	k1gMnSc5	Blackmor
a	a	k8xC	a
zvláště	zvláště	k9	zvláště
pak	pak	k6eAd1	pak
Gillan	Gillana	k1gFnPc2	Gillana
<g/>
)	)	kIx)	)
nebyli	být	k5eNaImAgMnP	být
moc	moc	k6eAd1	moc
rádi	rád	k2eAgMnPc1d1	rád
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
označováni	označovat	k5eAaImNgMnP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
oni	onen	k3xDgMnPc1	onen
měli	mít	k5eAaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
hard	hard	k6eAd1	hard
rockovou	rockový	k2eAgFnSc7d1	rocková
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
nahrávky	nahrávka	k1gFnSc2	nahrávka
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
začala	začít	k5eAaPmAgFnS	začít
skupina	skupina	k1gFnSc1	skupina
hodně	hodně	k6eAd1	hodně
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
první	první	k4xOgMnSc1	první
studiové	studiový	k2eAgFnSc2d1	studiová
LP	LP	kA	LP
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
in	in	k?	in
Rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
následoval	následovat	k5eAaImAgInS	následovat
anglický	anglický	k2eAgInSc1d1	anglický
top-ten	topen	k2eAgInSc1d1	top-ten
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Black	Black	k1gMnSc1	Black
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Fireball	Fireballum	k1gNnPc2	Fireballum
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
také	také	k6eAd1	také
zabodovala	zabodovat	k5eAaPmAgFnS	zabodovat
s	s	k7c7	s
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
Strange	Strange	k1gInSc1	Strange
Kind	Kind	k1gInSc1	Kind
of	of	k?	of
Woman	Woman	k1gInSc1	Woman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
a	a	k8xC	a
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
Purple	Purple	k1gMnPc1	Purple
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc4	základ
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Deep	Deep	k1gInSc4	Deep
Purple	Purple	k1gMnPc1	Purple
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
vystoupení	vystoupení	k1gNnPc1	vystoupení
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
naprosto	naprosto	k6eAd1	naprosto
strhující	strhující	k2eAgFnSc1d1	strhující
<g/>
.	.	kIx.	.
</s>
<s>
Oplývala	oplývat	k5eAaImAgFnS	oplývat
hudební	hudební	k2eAgFnSc7d1	hudební
virtuozitou	virtuozita	k1gFnSc7	virtuozita
<g/>
,	,	kIx,	,
muzikálností	muzikálnost	k1gFnSc7	muzikálnost
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
skvělými	skvělý	k2eAgFnPc7d1	skvělá
verzemi	verze	k1gFnPc7	verze
studiových	studiový	k2eAgFnPc2d1	studiová
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1971	[number]	k4	1971
odjeli	odjet	k5eAaPmAgMnP	odjet
DP	DP	kA	DP
do	do	k7c2	do
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
Montreaux	Montreaux	k1gInSc4	Montreaux
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zde	zde	k6eAd1	zde
natočili	natočit	k5eAaBmAgMnP	natočit
novou	nový	k2eAgFnSc4d1	nová
studiovou	studiový	k2eAgFnSc4d1	studiová
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
také	také	k9	také
živé	živý	k2eAgNnSc4d1	živé
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
již	již	k6eAd1	již
notoricky	notoricky	k6eAd1	notoricky
známé	známý	k2eAgFnPc4d1	známá
události	událost	k1gFnPc4	událost
<g/>
:	:	kIx,	:
při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
Franka	Frank	k1gMnSc2	Frank
Zappy	Zappa	k1gFnSc2	Zappa
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
The	The	k1gFnSc2	The
Mothers	Mothersa	k1gFnPc2	Mothersa
v	v	k7c6	v
Casinu	Casino	k1gNnSc6	Casino
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
fanoušek	fanoušek	k1gMnSc1	fanoušek
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
světlici	světlice	k1gFnSc4	světlice
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
shořela	shořet	k5eAaPmAgFnS	shořet
do	do	k7c2	do
základů	základ	k1gInPc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
událost	událost	k1gFnSc1	událost
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Rogera	Roger	k1gMnSc4	Roger
Glovera	Glover	k1gMnSc4	Glover
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
písně	píseň	k1gFnSc2	píseň
Smoke	Smok	k1gFnSc2	Smok
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Water	Watra	k1gFnPc2	Watra
<g/>
.	.	kIx.	.
</s>
<s>
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
se	se	k3xPyFc4	se
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
Grand	grand	k1gMnSc1	grand
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
natočili	natočit	k5eAaBmAgMnP	natočit
písně	píseň	k1gFnPc4	píseň
pro	pro	k7c4	pro
desku	deska	k1gFnSc4	deska
Machine	Machin	k1gInSc5	Machin
Head	Head	k1gMnSc1	Head
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
pilířů	pilíř	k1gInPc2	pilíř
hardrocku	hardrock	k1gInSc2	hardrock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1972	[number]	k4	1972
zamířila	zamířit	k5eAaPmAgFnS	zamířit
skupina	skupina	k1gFnSc1	skupina
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
zaslíbené	zaslíbený	k2eAgFnSc2d1	zaslíbená
země	zem	k1gFnSc2	zem
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
Ósace	Ósaka	k1gFnSc6	Ósaka
a	a	k8xC	a
Tokiu	Tokio	k1gNnSc6	Tokio
nahrála	nahrát	k5eAaPmAgFnS	nahrát
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Made	Mad	k1gFnSc2	Mad
in	in	k?	in
Japan	japan	k1gInSc1	japan
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
původně	původně	k6eAd1	původně
vyjít	vyjít	k5eAaPmF	vyjít
právě	právě	k9	právě
jen	jen	k9	jen
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
složení	složení	k1gNnSc6	složení
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
v	v	k7c6	v
nekonečných	konečný	k2eNgFnPc6d1	nekonečná
koncertních	koncertní	k2eAgFnPc6d1	koncertní
šňůrách	šňůra	k1gFnPc6	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
nahrávání	nahrávání	k1gNnSc2	nahrávání
dalšího	další	k2eAgNnSc2d1	další
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
Who	Who	k1gFnSc2	Who
Do	do	k7c2	do
We	We	k1gFnSc2	We
Think	Think	k1gMnSc1	Think
We	We	k1gMnSc1	We
Are	ar	k1gInSc5	ar
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
hit	hit	k1gInSc4	hit
Woman	Woman	k1gMnSc1	Woman
from	from	k1gMnSc1	from
Tokyo	Tokyo	k1gMnSc1	Tokyo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Ianem	Ianus	k1gMnSc7	Ianus
Gillanem	Gillan	k1gMnSc7	Gillan
a	a	k8xC	a
Ritchie	Ritchie	k1gFnPc1	Ritchie
Blackmorem	Blackmor	k1gInSc7	Blackmor
neúnosné	únosný	k2eNgNnSc1d1	neúnosné
<g/>
.	.	kIx.	.
</s>
<s>
Gillan	Gillan	k1gMnSc1	Gillan
oznámil	oznámit	k5eAaPmAgMnS	oznámit
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
netajil	tajit	k5eNaImAgMnS	tajit
svými	svůj	k3xOyFgFnPc7	svůj
sólovými	sólový	k2eAgFnPc7d1	sólová
ambicemi	ambice	k1gFnPc7	ambice
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgInS	dát
jako	jako	k9	jako
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
setrvání	setrvání	k1gNnSc4	setrvání
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
změnu	změna	k1gFnSc4	změna
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stovek	stovka	k1gFnPc2	stovka
adeptů	adept	k1gMnPc2	adept
na	na	k7c4	na
post	post	k1gInSc4	post
sólového	sólový	k2eAgMnSc2d1	sólový
zpěváka	zpěvák	k1gMnSc2	zpěvák
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nepříliš	příliš	k6eNd1	příliš
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
demosnímku	demosnímek	k1gInSc2	demosnímek
vybrán	vybrat	k5eAaPmNgMnS	vybrat
prakticky	prakticky	k6eAd1	prakticky
neznámý	známý	k2eNgMnSc1d1	neznámý
David	David	k1gMnSc1	David
Coverdale	Coverdal	k1gInSc5	Coverdal
<g/>
.	.	kIx.	.
</s>
<s>
Baskytaristou	baskytarista	k1gMnSc7	baskytarista
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
vokalistou	vokalista	k1gMnSc7	vokalista
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
Glenn	Glenn	k1gMnSc1	Glenn
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dříve	dříve	k6eAd2	dříve
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Trapeze	Trapeze	k1gFnSc2	Trapeze
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nové	nový	k2eAgNnSc1d1	nové
složení	složení	k1gNnSc1	složení
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
s	s	k7c7	s
heavy	heavo	k1gNnPc7	heavo
bluesovým	bluesový	k2eAgFnPc3d1	bluesová
<g/>
/	/	kIx~	/
<g/>
rockovým	rockový	k2eAgNnSc7d1	rockové
albem	album	k1gNnSc7	album
Burn	Burna	k1gFnPc2	Burna
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
nahrávkou	nahrávka	k1gFnSc7	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Hughes	Hughes	k1gMnSc1	Hughes
a	a	k8xC	a
Coverdale	Coverdala	k1gFnSc6	Coverdala
přidali	přidat	k5eAaPmAgMnP	přidat
funky	funk	k1gInPc4	funk
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
/	/	kIx~	/
<g/>
soul	soul	k1gInSc1	soul
element	element	k1gInSc1	element
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
zvuk	zvuk	k1gInSc1	zvuk
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
patrný	patrný	k2eAgInSc1d1	patrný
na	na	k7c6	na
nahrávce	nahrávka	k1gFnSc6	nahrávka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
Stormbringer	Stormbringra	k1gFnPc2	Stormbringra
<g/>
.	.	kIx.	.
</s>
<s>
Blackmore	Blackmor	k1gMnSc5	Blackmor
nebyl	být	k5eNaImAgMnS	být
spokojený	spokojený	k2eAgMnSc1d1	spokojený
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
a	a	k8xC	a
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
-	-	kIx~	-
Rainbow	Rainbow	k1gFnSc4	Rainbow
<g/>
.	.	kIx.	.
</s>
<s>
Mezera	mezera	k1gFnSc1	mezera
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Blackmora	Blackmora	k1gFnSc1	Blackmora
byla	být	k5eAaImAgFnS	být
zaplněna	zaplnit	k5eAaPmNgFnS	zaplnit
Američanem	Američan	k1gMnSc7	Američan
Tommy	Tomma	k1gFnSc2	Tomma
Bolinem	Bolin	k1gMnSc7	Bolin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následující	následující	k2eAgNnSc4d1	následující
album	album	k1gNnSc4	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
Come	Com	k1gFnSc2	Com
Taste	tasit	k5eAaPmRp2nP	tasit
the	the	k?	the
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
všechnu	všechen	k3xTgFnSc4	všechen
svoji	svůj	k3xOyFgFnSc4	svůj
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
mezi	mezi	k7c7	mezi
skalními	skalní	k2eAgMnPc7d1	skalní
fanoušky	fanoušek	k1gMnPc7	fanoušek
a	a	k8xC	a
nepřitáhlo	přitáhnout	k5eNaPmAgNnS	přitáhnout
žádné	žádný	k3yNgFnPc4	žádný
nové	nový	k2eAgFnPc4d1	nová
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
odchýlila	odchýlit	k5eAaPmAgFnS	odchýlit
od	od	k7c2	od
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
zvuku	zvuk	k1gInSc2	zvuk
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Bolin	Bolin	k1gInSc1	Bolin
měl	mít	k5eAaImAgInS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
(	(	kIx(	(
<g/>
heroin	heroin	k1gInSc1	heroin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
všechny	všechen	k3xTgInPc1	všechen
problémy	problém	k1gInPc1	problém
skupiny	skupina	k1gFnSc2	skupina
dělaly	dělat	k5eAaImAgInP	dělat
horšími	zlý	k2eAgFnPc7d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
problematickém	problematický	k2eAgNnSc6d1	problematické
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
Come	Com	k1gFnSc2	Com
Taste	tasit	k5eAaPmRp2nP	tasit
the	the	k?	the
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Tommy	Tomma	k1gFnPc1	Tomma
Bolin	Bolina	k1gFnPc2	Bolina
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c4	na
předávkování	předávkování	k1gNnSc4	předávkování
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
většina	většina	k1gFnSc1	většina
bývalých	bývalý	k2eAgMnPc2d1	bývalý
členů	člen	k1gMnPc2	člen
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
měli	mít	k5eAaImAgMnP	mít
značný	značný	k2eAgInSc4d1	značný
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kapelách	kapela	k1gFnPc6	kapela
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Rainbow	Rainbow	k1gFnSc6	Rainbow
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
jel	jet	k5eAaImAgInS	jet
Rod	rod	k1gInSc1	rod
Evans	Evansa	k1gFnPc2	Evansa
turné	turné	k1gNnSc1	turné
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
mladých	mladý	k2eAgMnPc2d1	mladý
hudebníků	hudebník	k1gMnPc2	hudebník
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
jediný	jediný	k2eAgMnSc1d1	jediný
původní	původní	k2eAgMnSc1d1	původní
člen	člen	k1gMnSc1	člen
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
trochu	trochu	k6eAd1	trochu
známý	známý	k2eAgMnSc1d1	známý
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
fanoušci	fanoušek	k1gMnPc1	fanoušek
i	i	k8xC	i
tisk	tisk	k1gInSc1	tisk
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
vysmívali	vysmívat	k5eAaImAgMnP	vysmívat
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
označováni	označovat	k5eAaImNgMnP	označovat
za	za	k7c4	za
podvod	podvod	k1gInSc4	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
než	než	k8xS	než
jim	on	k3xPp3gMnPc3	on
úřady	úřad	k1gInPc1	úřad
zakázaly	zakázat	k5eAaPmAgInP	zakázat
používání	používání	k1gNnSc4	používání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
opravdové	opravdový	k2eAgNnSc1d1	opravdové
a	a	k8xC	a
legální	legální	k2eAgNnSc1d1	legální
opětovné	opětovný	k2eAgNnSc1d1	opětovné
sjednocení	sjednocení	k1gNnSc1	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
rádia	rádio	k1gNnSc2	rádio
BBC	BBC	kA	BBC
The	The	k1gFnSc2	The
Friday	Fridaa	k1gFnSc2	Fridaa
Rock	rock	k1gInSc1	rock
Show	show	k1gFnSc4	show
že	že	k8xS	že
klasické	klasický	k2eAgNnSc4d1	klasické
složení	složení	k1gNnSc4	složení
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
-	-	kIx~	-
Blackmore	Blackmor	k1gMnSc5	Blackmor
<g/>
,	,	kIx,	,
Gillan	Gillan	k1gMnSc1	Gillan
<g/>
,	,	kIx,	,
Glover	Glover	k1gMnSc1	Glover
<g/>
,	,	kIx,	,
Lord	lord	k1gMnSc1	lord
<g/>
,	,	kIx,	,
a	a	k8xC	a
Paice	Paice	k1gFnSc1	Paice
spolu	spolu	k6eAd1	spolu
opět	opět	k6eAd1	opět
nahrávají	nahrávat	k5eAaImIp3nP	nahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Polydor	Polydora	k1gFnPc2	Polydora
a	a	k8xC	a
Mercury	Mercura	k1gFnSc2	Mercura
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Perfect	Perfecta	k1gFnPc2	Perfecta
Strangers	Strangersa	k1gFnPc2	Strangersa
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1984	[number]	k4	1984
a	a	k8xC	a
turné	turné	k1gNnSc1	turné
následovalo	následovat	k5eAaImAgNnS	následovat
<g/>
,	,	kIx,	,
začínajíc	začínajíc	k7c7	začínajíc
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
svoji	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
oklikou	oklika	k1gFnSc7	oklika
přes	přes	k7c4	přes
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
ohromný	ohromný	k2eAgInSc4d1	ohromný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
nahrála	nahrát	k5eAaPmAgFnS	nahrát
The	The	k1gFnSc4	The
House	house	k1gNnSc1	house
of	of	k?	of
Blue	Blu	k1gInSc2	Blu
Light	Lighta	k1gFnPc2	Lighta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
a	a	k8xC	a
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Nobody	Noboda	k1gFnSc2	Noboda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Perfect	Perfecta	k1gFnPc2	Perfecta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
Hush	Hush	k1gInSc1	Hush
k	k	k7c3	k
dvacátému	dvacátý	k4xOgNnSc3	dvacátý
výročí	výročí	k1gNnSc2	výročí
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
Ian	Ian	k1gMnSc1	Ian
Gillan	Gillan	k1gMnSc1	Gillan
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
neshodám	neshoda	k1gFnPc3	neshoda
s	s	k7c7	s
Blackmorem	Blackmor	k1gInSc7	Blackmor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Rainbow	Rainbow	k1gMnSc7	Rainbow
Joe	Joe	k1gMnSc7	Joe
Lynn	Lynn	k1gMnSc1	Lynn
Turnerem	turner	k1gMnSc7	turner
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
složení	složení	k1gNnSc1	složení
nahrálo	nahrát	k5eAaBmAgNnS	nahrát
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Slaves	Slaves	k1gInSc1	Slaves
and	and	k?	and
Masters	Masters	k1gInSc1	Masters
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
však	však	k9	však
chtělo	chtít	k5eAaImAgNnS	chtít
zpět	zpět	k6eAd1	zpět
Gillana	Gillana	k1gFnSc1	Gillana
místo	místo	k7c2	místo
Turnera	turner	k1gMnSc2	turner
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Lord	lord	k1gMnSc1	lord
<g/>
,	,	kIx,	,
Paice	Paice	k1gMnSc1	Paice
a	a	k8xC	a
Glover	Glovra	k1gFnPc2	Glovra
tlačili	tlačit	k5eAaImAgMnP	tlačit
na	na	k7c4	na
Turnera	turner	k1gMnSc4	turner
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odešel	odejít	k5eAaPmAgInS	odejít
a	a	k8xC	a
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
chtěla	chtít	k5eAaImAgFnS	chtít
zpět	zpět	k6eAd1	zpět
Gillana	Gillana	k1gFnSc1	Gillana
<g/>
.	.	kIx.	.
</s>
<s>
Blackmore	Blackmor	k1gMnSc5	Blackmor
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
a	a	k8xC	a
klasické	klasický	k2eAgNnSc1d1	klasické
složení	složení	k1gNnSc1	složení
nahrálo	nahrát	k5eAaBmAgNnS	nahrát
The	The	k1gFnSc4	The
Battle	Battle	k1gFnSc2	Battle
Rages	Rages	k1gMnSc1	Rages
On	on	k3xPp3gMnSc1	on
<g/>
...	...	k?	...
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
velice	velice	k6eAd1	velice
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
turné	turné	k1gNnSc2	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
Blackmorem	Blackmor	k1gInSc7	Blackmor
a	a	k8xC	a
Gillanem	Gillan	k1gInSc7	Gillan
opět	opět	k6eAd1	opět
narůstalo	narůstat	k5eAaImAgNnS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Blackmore	Blackmor	k1gMnSc5	Blackmor
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
Joe	Joe	k1gFnSc6	Joe
Satriani	Satriaň	k1gFnSc6	Satriaň
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
koncert	koncert	k1gInSc1	koncert
(	(	kIx(	(
<g/>
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
mohl	moct	k5eAaImAgInS	moct
konat	konat	k5eAaImF	konat
<g/>
.	.	kIx.	.
</s>
<s>
Satriani	Satriaň	k1gFnSc3	Satriaň
zůstal	zůstat	k5eAaPmAgInS	zůstat
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
letní	letní	k2eAgNnSc4d1	letní
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
stále	stále	k6eAd1	stále
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
kvůli	kvůli	k7c3	kvůli
již	již	k6eAd1	již
podepsaným	podepsaný	k2eAgInPc3d1	podepsaný
kontraktům	kontrakt	k1gInPc3	kontrakt
a	a	k8xC	a
nesouhlasu	nesouhlas	k1gInSc3	nesouhlas
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
stát	stát	k5eAaPmF	stát
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
Purple	Purple	k1gMnSc7	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
hledání	hledání	k1gNnSc2	hledání
lístečkovou	lístečkový	k2eAgFnSc7d1	lístečková
metodou	metoda	k1gFnSc7	metoda
jednomyslně	jednomyslně	k6eAd1	jednomyslně
přijala	přijmout	k5eAaPmAgFnS	přijmout
Steva	Steve	k1gMnSc2	Steve
Morse	Morse	k1gMnSc1	Morse
z	z	k7c2	z
Dixie	Dixie	k1gFnSc2	Dixie
Dregs	Dregs	k1gInSc1	Dregs
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
definitivně	definitivně	k6eAd1	definitivně
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Blackmora	Blackmora	k1gFnSc1	Blackmora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2001	[number]	k4	2001
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
dočasně	dočasně	k6eAd1	dočasně
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
i	i	k9	i
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odehrát	odehrát	k5eAaPmF	odehrát
smluvené	smluvený	k2eAgInPc4d1	smluvený
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vybrat	vybrat	k5eAaPmF	vybrat
svého	svůj	k3xOyFgMnSc4	svůj
zástupce	zástupce	k1gMnSc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jonova	Jonův	k2eAgFnSc1d1	Jonova
volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
Dona	Don	k1gMnSc4	Don
Aireyho	Airey	k1gMnSc4	Airey
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
Don	Don	k1gInSc1	Don
na	na	k7c6	na
letních	letní	k2eAgInPc6d1	letní
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
USA	USA	kA	USA
i	i	k8xC	i
Evropě	Evropa	k1gFnSc6	Evropa
dobře	dobře	k6eAd1	dobře
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
,	,	kIx,	,
Jon	Jon	k1gFnSc1	Jon
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
definitivně	definitivně	k6eAd1	definitivně
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
kapelu	kapela	k1gFnSc4	kapela
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
milované	milovaný	k2eAgFnSc3d1	milovaná
vážné	vážný	k2eAgFnSc3d1	vážná
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
opustil	opustit	k5eAaPmAgInS	opustit
kapelu	kapela	k1gFnSc4	kapela
definitivně	definitivně	k6eAd1	definitivně
po	po	k7c6	po
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
složení	složení	k1gNnSc1	složení
Purple	Purple	k1gFnSc2	Purple
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
Ian	Ian	k1gMnSc1	Ian
Gillan	Gillan	k1gMnSc1	Gillan
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Steve	Steve	k1gMnSc1	Steve
Morse	Morse	k1gMnSc1	Morse
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Don	dona	k1gFnPc2	dona
Airey	Airea	k1gFnSc2	Airea
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
Roger	Roger	k1gMnSc1	Roger
Glover	Glover	k1gMnSc1	Glover
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Ian	Ian	k1gFnSc1	Ian
Paice	Paice	k1gFnSc1	Paice
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
nahrála	nahrát	k5eAaPmAgFnS	nahrát
poslední	poslední	k2eAgInSc4d1	poslední
tři	tři	k4xCgInPc4	tři
alba	album	k1gNnPc4	album
Bananas	Bananasa	k1gFnPc2	Bananasa
<g/>
,	,	kIx,	,
Rapture	Raptur	k1gMnSc5	Raptur
Of	Of	k1gMnSc5	Of
The	The	k1gMnPc1	The
Deep	Deep	k1gMnSc1	Deep
a	a	k8xC	a
Now	Now	k1gMnSc1	Now
What	What	k1gMnSc1	What
<g/>
?!	?!	k?	?!
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavně	hlavně	k9	hlavně
druhou	druhý	k4xOgFnSc4	druhý
desku	deska	k1gFnSc4	deska
fanoušci	fanoušek	k1gMnPc1	fanoušek
i	i	k8xC	i
kritici	kritik	k1gMnPc1	kritik
přijali	přijmout	k5eAaPmAgMnP	přijmout
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
Paice	Paice	k1gFnSc1	Paice
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Ian	Ian	k1gFnSc1	Ian
Gillan	Gillana	k1gFnPc2	Gillana
-	-	kIx~	-
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
konga	konga	k1gFnSc1	konga
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Roger	Roger	k1gMnSc1	Roger
Glover	Glover	k1gMnSc1	Glover
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Steve	Steve	k1gMnSc1	Steve
Morse	Morse	k1gMnSc1	Morse
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Don	Don	k1gMnSc1	Don
Airey	Airea	k1gFnSc2	Airea
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
varhany	varhany	k1gFnPc1	varhany
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Jon	Jon	k1gMnSc1	Jon
Lord	lord	k1gMnSc1	lord
-	-	kIx~	-
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
;	;	kIx,	;
zemřel	zemřít	k5eAaPmAgInS	zemřít
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Nick	Nick	k1gMnSc1	Nick
Simper	Simper	k1gMnSc1	Simper
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Rod	rod	k1gInSc1	rod
Evans	Evans	k1gInSc1	Evans
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Glenn	Glenn	k1gMnSc1	Glenn
Hughes	Hughes	k1gMnSc1	Hughes
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Coverdale	Coverdala	k1gFnSc6	Coverdala
-	-	kIx~	-
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Tommy	Tomma	k1gFnSc2	Tomma
Bolin	Bolin	k1gInSc1	Bolin
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
;	;	kIx,	;
zemřel	zemřít	k5eAaPmAgInS	zemřít
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Joe	Joe	k1gMnSc1	Joe
Lynn	Lynn	k1gMnSc1	Lynn
Turner	turner	k1gMnSc1	turner
-	-	kIx~	-
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Joe	Joe	k1gFnPc2	Joe
Satriani	Satrian	k1gMnPc1	Satrian
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
Shades	Shades	k1gMnSc1	Shades
of	of	k?	of
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
Taliesyn	Taliesyn	k1gMnSc1	Taliesyn
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
in	in	k?	in
Rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Fireball	Fireballa	k1gFnPc2	Fireballa
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Machine	Machin	k1gInSc5	Machin
Head	Head	k1gMnSc1	Head
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Who	Who	k1gFnSc1	Who
<g />
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
We	We	k1gFnSc2	We
Think	Think	k1gMnSc1	Think
We	We	k1gMnSc1	We
Are	ar	k1gInSc5	ar
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Burn	Burna	k1gFnPc2	Burna
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Stormbringer	Stormbringra	k1gFnPc2	Stormbringra
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Come	Come	k1gInSc4	Come
Taste	tasit	k5eAaPmRp2nP	tasit
the	the	k?	the
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Perfect	Perfect	k2eAgInSc1d1	Perfect
Strangers	Strangers	k1gInSc1	Strangers
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
House	house	k1gNnSc1	house
of	of	k?	of
Blue	Blu	k1gInSc2	Blu
Light	Lighta	k1gFnPc2	Lighta
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Slaves	Slaves	k1gInSc1	Slaves
and	and	k?	and
<g />
.	.	kIx.	.
</s>
<s>
Masters	Masters	k1gInSc1	Masters
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Battle	Battle	k1gFnSc2	Battle
Rages	Rages	k1gMnSc1	Rages
On	on	k3xPp3gMnSc1	on
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Purpendicular	Purpendiculara	k1gFnPc2	Purpendiculara
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Abandon	Abandona	k1gFnPc2	Abandona
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Bananas	Bananasa	k1gFnPc2	Bananasa
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Rapture	Raptur	k1gMnSc5	Raptur
of	of	k?	of
the	the	k?	the
Deep	Deep	k1gInSc1	Deep
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Now	Now	k1gMnSc1	Now
What	What	k1gMnSc1	What
<g/>
?!	?!	k?	?!
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Infinite	Infinit	k1gInSc5	Infinit
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
