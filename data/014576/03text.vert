<s>
Author	Author	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Author	Author	k1gMnSc1
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1993	#num#	k4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
author	author	k1gMnSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Edvard	Edvard	k1gMnSc1
Kožušník	Kožušník	k1gMnSc1
po	po	k7c6
příjezdu	příjezd	k1gInSc6
na	na	k7c6
kole	kolo	k1gNnSc6
Author	Authora	k1gFnPc2
na	na	k7c4
první	první	k4xOgNnSc4
zasedání	zasedání	k1gNnSc4
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
</s>
<s>
Author	Author	k1gInSc1
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
značka	značka	k1gFnSc1
jízdních	jízdní	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
bike	bik	k1gInSc2
příslušenství	příslušenství	k1gNnPc2
a	a	k8xC
sportovních	sportovní	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značka	značka	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Author	Author	k1gMnSc1
vyrábí	vyrábět	k5eAaImIp3nS
jízdní	jízdní	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
Silniční	silniční	k2eAgFnSc1d1
kola	kola	k1gFnSc1
<g/>
,	,	kIx,
Horská	Horská	k1gFnSc1
kola	kolo	k1gNnSc2
(	(	kIx(
<g/>
cross	cross	k6eAd1
country	country	k2eAgNnSc1d1
<g/>
,	,	kIx,
enduro	endura	k1gFnSc5
<g/>
,	,	kIx,
celoodpružená	celoodpružený	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Krosová	krosový	k2eAgNnPc1d1
kola	kolo	k1gNnPc1
<g/>
,	,	kIx,
Touring	Touring	k1gInSc1
<g/>
,	,	kIx,
Freestyle	Freestyl	k1gInSc5
(	(	kIx(
<g/>
mtb	mtb	k?
<g/>
,	,	kIx,
bmx	bmx	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Junior	junior	k1gMnSc1
a	a	k8xC
Dámská	dámský	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značka	značka	k1gFnSc1
Author	Authora	k1gFnPc2
podporuje	podporovat	k5eAaImIp3nS
závodní	závodní	k2eAgInPc1d1
týmy	tým	k1gInPc1
Author	Authora	k1gFnPc2
Gang	Ganga	k1gFnPc2
-	-	kIx~
4	#num#	k4
<g/>
cross	crossa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
BMX	BMX	kA
(	(	kIx(
<g/>
Michal	Michal	k1gMnSc1
Prokop	Prokop	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Tamme	Tamm	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
triatlonový	triatlonový	k2eAgInSc1d1
team	team	k1gInSc1
Author	Author	k1gMnSc1
Tufo	Tufo	k1gMnSc1
TRI	TRI	kA
LIFE	LIFE	kA
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
Petr	Petr	k1gMnSc1
Vabroušek	Vabroušek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
PSK	PSK	kA
Whirlpool	Whirlpool	k1gInSc1
-	-	kIx~
Author	Author	k1gMnSc1
(	(	kIx(
<g/>
Petr	Petr	k1gMnSc1
Benčík	Benčík	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Alpine	Alpin	k1gInSc5
Pro	pro	k7c4
-	-	kIx~
Author	Author	k1gInSc4
Tým	tým	k1gInSc1
(	(	kIx(
<g/>
Václav	Václav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Author	Author	k1gInSc1
–	–	k?
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Cyklistika	cyklistika	k1gFnSc1
</s>
