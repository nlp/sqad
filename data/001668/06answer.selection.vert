<s>
Luis	Luisa	k1gFnPc2	Luisa
Walter	Walter	k1gMnSc1	Walter
Alvarez	Alvarez	k1gMnSc1	Alvarez
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
hypotézou	hypotéza	k1gFnSc7	hypotéza
o	o	k7c6	o
vymření	vymření	k1gNnSc6	vymření
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
vlivem	vlivem	k7c2	vlivem
impaktu	impakt	k1gInSc2	impakt
mimozemského	mimozemský	k2eAgNnSc2d1	mimozemské
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
výzkumem	výzkum	k1gInSc7	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
