<s>
Jidiš	jidiš	k1gNnSc1	jidiš
(	(	kIx(	(
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ד	ד	k?	ד
nebo	nebo	k8xC	nebo
א	א	k?	א
idiš	idiš	k1gInSc1	idiš
"	"	kIx"	"
<g/>
židovský	židovský	k2eAgMnSc1d1	židovský
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
západogermánský	západogermánský	k2eAgInSc1d1	západogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
kolem	kolem	k7c2	kolem
čtyř	čtyři	k4xCgInPc2	čtyři
miliónů	milión	k4xCgInPc2	milión
Židů	Žid	k1gMnPc2	Žid
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
