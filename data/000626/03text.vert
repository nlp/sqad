<s>
Jidiš	jidiš	k1gNnSc1	jidiš
(	(	kIx(	(
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ד	ד	k?	ד
nebo	nebo	k8xC	nebo
א	א	k?	א
idiš	idiš	k1gInSc1	idiš
"	"	kIx"	"
<g/>
židovský	židovský	k2eAgMnSc1d1	židovský
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
západogermánský	západogermánský	k2eAgInSc1d1	západogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
kolem	kolem	k7c2	kolem
čtyř	čtyři	k4xCgInPc2	čtyři
miliónů	milión	k4xCgInPc2	milión
Židů	Žid	k1gMnPc2	Žid
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
název	název	k1gInSc1	název
jidiš	jidiš	k6eAd1	jidiš
znamená	znamenat	k5eAaImIp3nS	znamenat
židovský	židovský	k2eAgInSc4d1	židovský
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Jüdisch	Jüdisch	k1gInSc1	Jüdisch
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
za	za	k7c4	za
původní	původní	k2eAgFnSc4d1	původní
"	"	kIx"	"
<g/>
jidiš	jidiš	k6eAd1	jidiš
dajtš	dajtš	k5eAaPmIp2nS	dajtš
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ד	ד	k?	ד
<g/>
-	-	kIx~	-
<g/>
ד	ד	k?	ד
<g/>
ַ	ַ	k?	ַ
<g/>
ט	ט	k?	ט
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
židovská	židovský	k2eAgFnSc1d1	židovská
němčina	němčina	k1gFnSc1	němčina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mluvená	mluvený	k2eAgFnSc1d1	mluvená
forma	forma	k1gFnSc1	forma
je	být	k5eAaImIp3nS	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
němčině	němčina	k1gFnSc3	němčina
blízká	blízký	k2eAgFnSc1d1	blízká
<g/>
,	,	kIx,	,
jidiš	jidiš	k6eAd1	jidiš
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
z	z	k7c2	z
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
a	a	k8xC	a
jazyků	jazyk	k1gMnPc2	jazyk
mnoha	mnoho	k4c2	mnoho
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
sousedství	sousedství	k1gNnSc6	sousedství
Židé	Žid	k1gMnPc1	Žid
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Jidiš	jidiš	k6eAd1	jidiš
sdílí	sdílet	k5eAaImIp3nS	sdílet
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
sobě	se	k3xPyFc3	se
podobají	podobat	k5eAaImIp3nP	podobat
i	i	k9	i
gramaticky	gramaticky	k6eAd1	gramaticky
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vznesena	vznesen	k2eAgFnSc1d1	vznesena
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yQnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
mluvčí	mluvčí	k1gMnPc1	mluvčí
němčiny	němčina	k1gFnSc2	němčina
rozumí	rozumět	k5eAaImIp3nP	rozumět
řeči	řeč	k1gFnPc1	řeč
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
němčině	němčina	k1gFnSc3	němčina
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
proto	proto	k8xC	proto
považovali	považovat	k5eAaImAgMnP	považovat
jidiš	jidiš	k6eAd1	jidiš
za	za	k7c4	za
dialekt	dialekt	k1gInSc4	dialekt
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
němčinu	němčina	k1gFnSc4	němčina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lingvistů	lingvista	k1gMnPc2	lingvista
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
jidiš	jidiš	k1gNnSc4	jidiš
a	a	k8xC	a
němčinu	němčina	k1gFnSc4	němčina
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
následující	následující	k2eAgInPc1d1	následující
důvody	důvod	k1gInPc7	důvod
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
jidiš	jidiš	k6eAd1	jidiš
nejsou	být	k5eNaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgNnSc4d1	srozumitelné
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
zvláště	zvláště	k6eAd1	zvláště
o	o	k7c6	o
německých	německý	k2eAgFnPc6d1	německá
mluvčích	mluvčí	k1gFnPc6	mluvčí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
problém	problém	k1gInSc4	problém
porozumět	porozumět	k5eAaPmF	porozumět
jidiš	jidiš	k6eAd1	jidiš
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
procent	procento	k1gNnPc2	procento
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
jidiš	jidiš	k6eAd1	jidiš
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
<g/>
;	;	kIx,	;
nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
část	část	k1gFnSc1	část
gramatiky	gramatika	k1gFnSc2	gramatika
jidiš	jidiš	k1gNnSc2	jidiš
se	se	k3xPyFc4	se
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
němčiny	němčina	k1gFnPc1	němčina
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
dalšími	další	k2eAgMnPc7d1	další
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovanskými	slovanský	k2eAgInPc7d1	slovanský
<g/>
)	)	kIx)	)
jazyky	jazyk	k1gInPc7	jazyk
<g/>
;	;	kIx,	;
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
zeměpisné	zeměpisný	k2eAgNnSc1d1	zeměpisné
rozšíření	rozšíření	k1gNnSc1	rozšíření
i	i	k8xC	i
kulturní	kulturní	k2eAgNnPc1d1	kulturní
pozadí	pozadí	k1gNnPc1	pozadí
<g/>
.	.	kIx.	.
historické	historický	k2eAgNnSc4d1	historické
zatížení	zatížení	k1gNnSc4	zatížení
vztahu	vztah	k1gInSc2	vztah
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
Lingvista	lingvista	k1gMnSc1	lingvista
Paul	Paul	k1gMnSc1	Paul
Wexler	Wexler	k1gMnSc1	Wexler
šel	jít	k5eAaImAgMnS	jít
dokonce	dokonce	k9	dokonce
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jidiš	jidiš	k6eAd1	jidiš
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
slovanský	slovanský	k2eAgInSc1d1	slovanský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
německými	německý	k2eAgFnPc7d1	německá
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
většinou	většina	k1gFnSc7	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lingvistů	lingvista	k1gMnPc2	lingvista
odmítán	odmítán	k2eAgMnSc1d1	odmítán
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jiní	jiný	k1gMnPc1	jiný
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
jazykem	jazyk	k1gInSc7	jazyk
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
dialektem	dialekt	k1gInSc7	dialekt
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
mlhavá	mlhavý	k2eAgFnSc1d1	mlhavá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
<g/>
:	:	kIx,	:
jazyky	jazyk	k1gInPc1	jazyk
jako	jako	k8xC	jako
dánština	dánština	k1gFnSc1	dánština
<g/>
,	,	kIx,	,
švédština	švédština	k1gFnSc1	švédština
a	a	k8xC	a
norština	norština	k1gFnSc1	norština
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
mnohem	mnohem	k6eAd1	mnohem
bližší	blízký	k2eAgMnSc1d2	bližší
než	než	k8xS	než
jidiš	jidiš	k6eAd1	jidiš
a	a	k8xC	a
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelné	srozumitelný	k2eAgNnSc1d1	srozumitelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
;	;	kIx,	;
západní	západní	k2eAgInSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgInSc4d1	východní
dialekt	dialekt	k1gInSc4	dialekt
jidiš	jidiš	k6eAd1	jidiš
jsou	být	k5eAaImIp3nP	být
natolik	natolik	k6eAd1	natolik
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
lingvisté	lingvista	k1gMnPc1	lingvista
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jidiš	jidiš	k1gNnPc2	jidiš
se	se	k3xPyFc4	se
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
(	(	kIx(	(
<g/>
německé	německý	k2eAgFnSc6d1	německá
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgNnPc4d1	východní
jidiš	jidiš	k1gNnPc4	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
litviš	litviš	k5eAaPmIp2nS	litviš
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jidiš	jidiš	k1gNnSc4	jidiš
<g/>
,	,	kIx,	,
středovýchodní	středovýchodní	k2eAgNnSc4d1	středovýchodní
(	(	kIx(	(
<g/>
polsko-haličské	polskoaličský	k2eAgNnSc4d1	polsko-haličský
<g/>
)	)	kIx)	)
jidiš	jidiš	k1gNnSc4	jidiš
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
(	(	kIx(	(
<g/>
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
<g/>
)	)	kIx)	)
jidiš	jidiš	k1gNnSc7	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnPc1d1	východní
nářečí	nářečí	k1gNnPc1	nářečí
a	a	k8xC	a
moderní	moderní	k2eAgInPc1d1	moderní
jidiš	jidiš	k6eAd1	jidiš
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
slov	slovo	k1gNnPc2	slovo
přejatých	přejatý	k2eAgFnPc6d1	přejatá
ze	z	k7c2	z
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
židovská	židovský	k2eAgFnSc1d1	židovská
arabština	arabština	k1gFnSc1	arabština
a	a	k8xC	a
ladino	ladino	k1gNnSc1	ladino
(	(	kIx(	(
<g/>
židovská	židovský	k2eAgFnSc1d1	židovská
španělština	španělština	k1gFnSc1	španělština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jidiš	jidiš	k6eAd1	jidiš
používá	používat	k5eAaImIp3nS	používat
upravenou	upravený	k2eAgFnSc4d1	upravená
hebrejskou	hebrejský	k2eAgFnSc4d1	hebrejská
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Jidiš	jidiš	k6eAd1	jidiš
samo	sám	k3xTgNnSc1	sám
však	však	k9	však
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
hebrejštinou	hebrejština	k1gFnSc7	hebrejština
lingvisticky	lingvisticky	k6eAd1	lingvisticky
nijak	nijak	k6eAd1	nijak
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
přejalo	přejmout	k5eAaPmAgNnS	přejmout
stovky	stovka	k1gFnPc4	stovka
hebrejských	hebrejský	k2eAgInPc2d1	hebrejský
a	a	k8xC	a
aramejských	aramejský	k2eAgInPc2d1	aramejský
výrazů	výraz	k1gInPc2	výraz
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
židovské	židovský	k2eAgFnSc2d1	židovská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
rysem	rys	k1gInSc7	rys
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
latinské	latinský	k2eAgFnSc2d1	Latinská
odvozeniny	odvozenina	k1gFnSc2	odvozenina
pro	pro	k7c4	pro
mnohá	mnohý	k2eAgNnPc4d1	mnohé
slova	slovo	k1gNnPc4	slovo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
náboženských	náboženský	k2eAgInPc2d1	náboženský
rituálů	rituál	k1gInPc2	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
vypůjčili	vypůjčit	k5eAaPmAgMnP	vypůjčit
terminologii	terminologie	k1gFnSc4	terminologie
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
francouzštiny	francouzština	k1gFnSc2	francouzština
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
Alsasku	Alsasko	k1gNnSc6	Alsasko
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
bentšn	bentšn	k1gMnSc1	bentšn
(	(	kIx(	(
<g/>
ב	ב	k?	ב
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
benšovat	benšovat	k5eAaPmF	benšovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
požehnat	požehnat	k5eAaPmF	požehnat
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
požehnáním	požehnání	k1gNnSc7	požehnání
po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	on	k3xPp3gMnPc4	on
příbuzné	příbuzný	k1gMnPc4	příbuzný
s	s	k7c7	s
výrazem	výraz	k1gInSc7	výraz
benedico	benedico	k6eAd1	benedico
<g/>
,	,	kIx,	,
žehnat	žehnat	k5eAaImF	žehnat
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
lejenen	lejenna	k1gFnPc2	lejenna
(	(	kIx(	(
<g/>
ל	ל	k?	ל
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
číst	číst	k5eAaImF	číst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
také	také	k9	také
odráží	odrážet	k5eAaImIp3nS	odrážet
románský	románský	k2eAgInSc1d1	románský
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
roztroušenosti	roztroušenost	k1gFnSc3	roztroušenost
Židů	Žid	k1gMnPc2	Žid
po	po	k7c6	po
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
i	i	k9	i
u	u	k7c2	u
jidiš	jidiš	k6eAd1	jidiš
těžké	těžký	k2eAgNnSc1d1	těžké
jmenovat	jmenovat	k5eAaImF	jmenovat
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
nejvíce	hodně	k6eAd3	hodně
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
používají	používat	k5eAaImIp3nP	používat
jidiš	jidiš	k6eAd1	jidiš
hlavně	hlavně	k9	hlavně
ultraortodoxní	ultraortodoxní	k2eAgMnPc1d1	ultraortodoxní
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
považují	považovat	k5eAaImIp3nP	považovat
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
za	za	k7c4	za
posvátný	posvátný	k2eAgInSc4d1	posvátný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nevhodný	vhodný	k2eNgInSc4d1	nevhodný
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
komunity	komunita	k1gFnPc1	komunita
mluvčích	mluvčí	k1gMnPc2	mluvčí
jidiš	jidiš	k6eAd1	jidiš
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zlikvidovány	zlikvidovat	k5eAaPmNgInP	zlikvidovat
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zbyly	zbýt	k5eAaPmAgInP	zbýt
na	na	k7c6	na
území	území	k1gNnSc6	území
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
;	;	kIx,	;
mnozí	mnohý	k2eAgMnPc1d1	mnohý
navíc	navíc	k6eAd1	navíc
už	už	k6eAd1	už
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
jazyk	jazyk	k1gInSc4	jazyk
používají	používat	k5eAaImIp3nP	používat
ruštinu	ruština	k1gFnSc4	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
žije	žít	k5eAaImIp3nS	žít
největší	veliký	k2eAgFnSc1d3	veliký
komunita	komunita	k1gFnSc1	komunita
mluvčích	mluvčí	k1gMnPc2	mluvčí
jidiš	jidiš	k6eAd1	jidiš
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
jidiš	jidiš	k6eAd1	jidiš
vznikal	vznikat	k5eAaImAgInS	vznikat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
mezi	mezi	k7c4	mezi
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
jako	jako	k8xC	jako
jakýsi	jakýsi	k3yIgInSc1	jakýsi
slepenec	slepenec	k1gInSc1	slepenec
středoněmeckých	středoněmecký	k2eAgNnPc2d1	středoněmecký
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
glosy	glosa	k1gFnPc1	glosa
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
hebrejských	hebrejský	k2eAgInPc6d1	hebrejský
rukopisech	rukopis	k1gInPc6	rukopis
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
dílo	dílo	k1gNnSc1	dílo
vytištěné	vytištěný	k2eAgFnSc2d1	vytištěná
kompletně	kompletně	k6eAd1	kompletně
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hromadném	hromadný	k2eAgInSc6d1	hromadný
odchodu	odchod	k1gInSc6	odchod
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
užívání	užívání	k1gNnSc2	užívání
jidiš	jidiš	k6eAd1	jidiš
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
i	i	k9	i
slovanskými	slovanský	k2eAgInPc7d1	slovanský
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
polštinou	polština	k1gFnSc7	polština
a	a	k8xC	a
ukrajinštinou	ukrajinština	k1gFnSc7	ukrajinština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
také	také	k9	také
časově	časově	k6eAd1	časově
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
oživením	oživení	k1gNnSc7	oživení
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
jako	jako	k8xC	jako
mluveného	mluvený	k2eAgInSc2d1	mluvený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
zakladatele	zakladatel	k1gMnSc2	zakladatel
novodobé	novodobý	k2eAgFnSc2d1	novodobá
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
Mendele	Mendel	k1gMnSc5	Mendel
Mocher	Mochra	k1gFnPc2	Mochra
Sforima	Sforim	k1gMnSc2	Sforim
<g/>
,	,	kIx,	,
Šoloma	Šolom	k1gMnSc2	Šolom
Alejchema	Alejchem	k1gMnSc2	Alejchem
a	a	k8xC	a
J.	J.	kA	J.
L.	L.	kA	L.
Peretze	Peretze	k1gFnSc1	Peretze
<g/>
.	.	kIx.	.
</s>
<s>
Solomon	Solomon	k1gMnSc1	Solomon
Rabinowitz	Rabinowitz	k1gMnSc1	Rabinowitz
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Šolom	Šolom	k1gInSc1	Šolom
Alejchem	Alejch	k1gMnSc7	Alejch
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
humoristů	humorist	k1gMnPc2	humorist
(	(	kIx(	(
<g/>
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
např.	např.	kA	např.
Tovje	Tovj	k1gFnSc2	Tovj
vdává	vdávat	k5eAaImIp3nS	vdávat
dcery	dcera	k1gFnSc2	dcera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
židovský	židovský	k2eAgInSc4d1	židovský
protějšek	protějšek	k1gInSc4	protějšek
Marka	Marek	k1gMnSc2	Marek
Twaina	Twain	k1gMnSc2	Twain
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
jidiš	jidiš	k6eAd1	jidiš
stával	stávat	k5eAaImAgMnS	stávat
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
východoevropských	východoevropský	k2eAgInPc2d1	východoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vydávala	vydávat	k5eAaPmAgFnS	vydávat
se	se	k3xPyFc4	se
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
rozmach	rozmach	k1gInSc4	rozmach
zažívalo	zažívat	k5eAaImAgNnS	zažívat
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
film	film	k1gInSc1	film
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Jidiš	jidiš	k6eAd1	jidiš
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
té	ten	k3xDgFnSc2	ten
části	část	k1gFnSc2	část
židovského	židovský	k2eAgNnSc2d1	Židovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odmítala	odmítat	k5eAaImAgFnS	odmítat
sionismus	sionismus	k1gInSc4	sionismus
a	a	k8xC	a
dávala	dávat	k5eAaImAgFnS	dávat
přednost	přednost	k1gFnSc4	přednost
získání	získání	k1gNnSc2	získání
kulturní	kulturní	k2eAgFnSc2d1	kulturní
autonomie	autonomie	k1gFnSc2	autonomie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Holokaust	holokaust	k1gInSc1	holokaust
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc2	století
však	však	k9	však
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
náhlému	náhlý	k2eAgInSc3d1	náhlý
prudkému	prudký	k2eAgInSc3d1	prudký
úpadku	úpadek	k1gInSc3	úpadek
užívání	užívání	k1gNnSc2	užívání
jidiš	jidiš	k6eAd1	jidiš
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
židovské	židovský	k2eAgFnPc1d1	židovská
komunity	komunita	k1gFnPc1	komunita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
používaly	používat	k5eAaImAgFnP	používat
jidiš	jidiš	k6eAd1	jidiš
v	v	k7c6	v
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vyvražděny	vyvražděn	k2eAgFnPc1d1	vyvražděna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jidiš	jidiš	k6eAd1	jidiš
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
jazykem	jazyk	k1gInSc7	jazyk
"	"	kIx"	"
<g/>
židovského	židovský	k2eAgInSc2d1	židovský
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hebrejština	hebrejština	k1gFnSc1	hebrejština
se	se	k3xPyFc4	se
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
buržoazní	buržoazní	k2eAgInSc4d1	buržoazní
<g/>
"	"	kIx"	"
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
tam	tam	k6eAd1	tam
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
užívání	užívání	k1gNnSc3	užívání
jidiš	jidiš	k6eAd1	jidiš
silně	silně	k6eAd1	silně
podporováno	podporován	k2eAgNnSc1d1	podporováno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
od	od	k7c2	od
používání	používání	k1gNnSc2	používání
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
snažil	snažit	k5eAaImAgMnS	snažit
občany	občan	k1gMnPc4	občan
spíš	spíš	k9	spíš
odradit	odradit	k5eAaPmF	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
30	[number]	k4	30
<g/>
.	.	kIx.	.
léty	léto	k1gNnPc7	léto
však	však	k9	však
rostly	růst	k5eAaImAgFnP	růst
antisemitské	antisemitský	k2eAgFnPc1d1	antisemitská
tendence	tendence	k1gFnPc1	tendence
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
jidiš	jidiš	k6eAd1	jidiš
začalo	začít	k5eAaPmAgNnS	začít
rovněž	rovněž	k9	rovněž
mizet	mizet	k5eAaImF	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Přežilo	přežít	k5eAaPmAgNnS	přežít
jen	jen	k9	jen
pár	pár	k4xCyI	pár
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
literární	literární	k2eAgInSc1d1	literární
magazín	magazín	k1gInSc1	magazín
Sovjetiš	Sovjetiš	k1gInSc1	Sovjetiš
hejmland	hejmlanda	k1gFnPc2	hejmlanda
a	a	k8xC	a
noviny	novina	k1gFnSc2	novina
Birobidžaner	Birobidžaner	k1gMnSc1	Birobidžaner
štern	štern	k1gMnSc1	štern
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
Židé	Žid	k1gMnPc1	Žid
se	se	k3xPyFc4	se
vesměs	vesměs	k6eAd1	vesměs
integrovali	integrovat	k5eAaBmAgMnP	integrovat
do	do	k7c2	do
sovětské	sovětský	k2eAgFnSc2d1	sovětská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
přednost	přednost	k1gFnSc4	přednost
ruštině	ruština	k1gFnSc3	ruština
před	před	k7c7	před
jidiš	jidiš	k1gNnSc7	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
bylo	být	k5eAaImAgNnS	být
jidiš	jidiš	k6eAd1	jidiš
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pojítek	pojítko	k1gNnPc2	pojítko
pro	pro	k7c4	pro
židovské	židovský	k2eAgMnPc4d1	židovský
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
velmi	velmi	k6eAd1	velmi
lišili	lišit	k5eAaImAgMnP	lišit
svým	svůj	k3xOyFgNnSc7	svůj
kulturním	kulturní	k2eAgNnSc7d1	kulturní
zázemím	zázemí	k1gNnSc7	zázemí
a	a	k8xC	a
národní	národní	k2eAgFnSc7d1	národní
identitou	identita	k1gFnSc7	identita
<g/>
.	.	kIx.	.
</s>
<s>
Vydávaly	vydávat	k5eAaImAgFnP	vydávat
se	se	k3xPyFc4	se
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
(	(	kIx(	(
<g/>
např.	např.	kA	např.
פ	פ	k?	פ
<g/>
ֿ	ֿ	k?	ֿ
<g/>
א	א	k?	א
<g/>
ָ	ָ	k?	ָ
<g/>
ר	ר	k?	ר
<g/>
,	,	kIx,	,
Forverts	Forverts	k1gInSc1	Forverts
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
Forward	Forward	k1gMnSc1	Forward
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgFnS	vznikat
americká	americký	k2eAgFnSc1d1	americká
židovská	židovský	k2eAgFnSc1d1	židovská
hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
(	(	kIx(	(
<g/>
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
klezmerské	klezmerský	k2eAgFnSc2d1	klezmerská
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
"	"	kIx"	"
<g/>
jidišismů	jidišismus	k1gInPc2	jidišismus
<g/>
"	"	kIx"	"
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
newyorské	newyorský	k2eAgFnSc2d1	newyorská
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
nežidé	nežid	k1gMnPc1	nežid
tyto	tento	k3xDgInPc1	tento
výrazy	výraz	k1gInPc1	výraz
používají	používat	k5eAaImIp3nP	používat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
židy	žid	k1gMnPc7	žid
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
jejich	jejich	k3xOp3gInSc4	jejich
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
USA	USA	kA	USA
většinou	většinou	k6eAd1	většinou
nepředávali	předávat	k5eNaImAgMnP	předávat
jidiš	jidiš	k1gNnSc4	jidiš
svým	svůj	k3xOyFgMnPc3	svůj
potomkům	potomek	k1gMnPc3	potomek
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
asimilovali	asimilovat	k5eAaBmAgMnP	asimilovat
a	a	k8xC	a
přijali	přijmout	k5eAaPmAgMnP	přijmout
angličtinu	angličtina	k1gFnSc4	angličtina
jako	jako	k8xC	jako
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Isaac	Isaac	k1gFnSc1	Isaac
Bashevis	Bashevis	k1gFnSc2	Bashevis
Singer	Singra	k1gFnPc2	Singra
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgInPc1d1	píšící
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jidiš	jidiš	k6eAd1	jidiš
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
hebrejským	hebrejský	k2eAgNnSc7d1	hebrejské
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kromě	kromě	k7c2	kromě
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
znaků	znak	k1gInPc2	znak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
spřežek	spřežka	k1gFnPc2	spřežka
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jediným	jediný	k2eAgInSc7d1	jediný
germánským	germánský	k2eAgInSc7d1	germánský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nepoužívá	používat	k5eNaImIp3nS	používat
latinku	latinka	k1gFnSc4	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
změn	změna	k1gFnPc2	změna
oproti	oproti	k7c3	oproti
hebrejštině	hebrejština	k1gFnSc3	hebrejština
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
zápis	zápis	k1gInSc1	zápis
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
samohlásky	samohláska	k1gFnPc1	samohláska
většinou	většinou	k6eAd1	většinou
nezapisují	zapisovat	k5eNaImIp3nP	zapisovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnSc2	písmeno
alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
ajin	ajin	k1gInSc1	ajin
<g/>
,	,	kIx,	,
jod	jod	k1gInSc1	jod
a	a	k8xC	a
vav	vav	k?	vav
tak	tak	k6eAd1	tak
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
mater	mater	k1gFnSc2	mater
lectionis	lectionis	k1gFnSc2	lectionis
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
o	o	k7c4	o
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
o	o	k0	o
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
do	do	k7c2	do
řádků	řádek	k1gInPc2	řádek
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
písmena	písmeno	k1gNnPc1	písmeno
seřazena	seřadit	k5eAaPmNgNnP	seřadit
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
následující	následující	k2eAgInPc4d1	následující
názvy	název	k1gInPc4	název
(	(	kIx(	(
<g/>
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
shodné	shodný	k2eAgFnSc3d1	shodná
s	s	k7c7	s
těmi	ten	k3xDgFnPc7	ten
hebrejskými	hebrejský	k2eAgFnPc7d1	hebrejská
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
štume	štumat	k5eAaPmIp3nS	štumat
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
alef	alef	k1gInSc1	alef
||	||	k?	||
pasech-alef	pasechlef	k1gInSc1	pasech-alef
||	||	k?	||
komec-alef	komeclef	k1gInSc1	komec-alef
||	||	k?	||
bejs	bejs	k1gInSc1	bejs
||	||	k?	||
vejs	vejs	k1gInSc1	vejs
||	||	k?	||
giml	giml	k1gInSc1	giml
||	||	k?	||
dalet	dalet	k5eAaPmF	dalet
||	||	k?	||
hej	hej	k6eAd1	hej
||	||	k?	||
vov	vov	k?	vov
||	||	k?	||
melupm	melupm	k1gMnSc1	melupm
vov	vov	k?	vov
||	||	k?	||
cvej	cvej	k1gMnSc1	cvej
vovn	vovn	k1gMnSc1	vovn
||	||	k?	||
vov-jud	vovud	k1gMnSc1	vov-jud
<g />
.	.	kIx.	.
</s>
<s>
||	||	k?	||
zajen	zajen	k2eAgInSc4d1	zajen
||	||	k?	||
zajen-šin	zajen-šin	k2eAgInSc4d1	zajen-šin
||	||	k?	||
ches	ches	k1gInSc4	ches
||	||	k?	||
tes	tes	k1gInSc4	tes
||	||	k?	||
tes-šin	tes-šin	k2eAgInSc4d1	tes-šin
||	||	k?	||
jud	judo	k1gNnPc2	judo
||	||	k?	||
chirik-yud	chirikud	k1gMnSc1	chirik-yud
||	||	k?	||
cvej	cvej	k1gMnSc1	cvej
judn	judn	k1gMnSc1	judn
||	||	k?	||
pasech	pas	k1gInPc6	pas
cvej	cvej	k1gMnSc1	cvej
judn	judn	k1gMnSc1	judn
||	||	k?	||
kof	kof	k?	kof
||	||	k?	||
chof	chof	k1gMnSc1	chof
/	/	kIx~	/
lange	lange	k1gFnSc1	lange
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
chof	chof	k1gMnSc1	chof
||	||	k?	||
lamed	lamed	k1gMnSc1	lamed
||	||	k?	||
mem	mem	k?	mem
/	/	kIx~	/
šlos	šlos	k1gInSc1	šlos
mem	mem	k?	mem
||	||	k?	||
nun	nun	k?	nun
/	/	kIx~	/
lange	lange	k1gFnSc1	lange
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
nun	nun	k?	nun
||	||	k?	||
samech	samech	k1gInSc1	samech
||	||	k?	||
ajin	ajin	k1gInSc1	ajin
||	||	k?	||
pej	pej	k?	pej
||	||	k?	||
fej	fej	k?	fej
||	||	k?	||
cadek	cadek	k1gMnSc1	cadek
/	/	kIx~	/
lange	lange	k1gFnSc1	lange
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
cadek	cadek	k1gMnSc1	cadek
||	||	k?	||
kuf	kuf	k?	kuf
||	||	k?	||
rejš	rejš	k1gMnSc1	rejš
||	||	k?	||
šin	šin	k?	šin
||	||	k?	||
sin	sin	kA	sin
||	||	k?	||
tof	tof	k?	tof
||	||	k?	||
sof	sofa	k1gFnPc2	sofa
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
pravopisné	pravopisný	k2eAgFnPc1d1	pravopisná
normy	norma	k1gFnPc1	norma
jidiš	jidiš	k6eAd1	jidiš
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
hebrejská	hebrejský	k2eAgNnPc1d1	hebrejské
a	a	k8xC	a
aramejská	aramejský	k2eAgNnPc1d1	aramejské
slova	slovo	k1gNnPc1	slovo
foneticky	foneticky	k6eAd1	foneticky
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
jejich	jejich	k3xOp3gInSc4	jejich
původní	původní	k2eAgInSc4d1	původní
pravopis	pravopis	k1gInSc4	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
a	a	k8xC	a
N	N	kA	N
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
jidiš	jidiš	k6eAd1	jidiš
slabikotvorné	slabikotvorný	k2eAgFnSc6d1	slabikotvorný
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
cvej	cvej	k1gMnSc1	cvej
judn	judn	k1gMnSc1	judn
<g/>
,	,	kIx,	,
pasech	pas	k1gInPc6	pas
cvej	cvej	k1gMnSc1	cvej
judn	judn	k1gMnSc1	judn
<g/>
,	,	kIx,	,
cvej	cvej	k1gMnSc1	cvej
vovn	vovn	k1gMnSc1	vovn
a	a	k8xC	a
vov-jud	vovud	k1gMnSc1	vov-jud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Unikódu	Unikódo	k1gNnSc6	Unikódo
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
písmena	písmeno	k1gNnPc1	písmeno
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
kódy	kód	k1gInPc4	kód
na	na	k7c6	na
konci	konec	k1gInSc6	konec
Unikódu	Unikód	k1gInSc2	Unikód
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
také	také	k9	také
tvořit	tvořit	k5eAaImF	tvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
napíše	napsat	k5eAaPmIp3nS	napsat
základní	základní	k2eAgNnSc1d1	základní
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmeno	písmeno	k1gNnSc1	písmeno
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
diakritické	diakritický	k2eAgNnSc1d1	diakritické
znaménko	znaménko	k1gNnSc1	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
"	"	kIx"	"
<g/>
kof	kof	k?	kof
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
spojením	spojení	k1gNnSc7	spojení
"	"	kIx"	"
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
KAF	KAF	kA	KAF
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1499	[number]	k4	1499
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
DAGESH	DAGESH	kA	DAGESH
OR	OR	kA	OR
MAPIQ	MAPIQ	kA	MAPIQ
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1468	[number]	k4	1468
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
װ	װ	k?	װ
(	(	kIx(	(
<g/>
tsvey	tsvea	k1gMnSc2	tsvea
vovn	vovn	k1gNnSc4	vovn
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
v	v	k7c4	v
<g/>
]	]	kIx)	]
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgNnSc4d1	samostatné
písmeno	písmeno	k1gNnSc4	písmeno
a	a	k8xC	a
nepíše	psát	k5eNaImIp3nS	psát
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
vov	vov	k?	vov
za	za	k7c4	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gInSc5	Unicod
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ױ	ױ	k?	ױ
(	(	kIx(	(
<g/>
vov-yud	vovud	k1gMnSc1	vov-yud
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
oj	oj	k1gFnSc1	oj
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgNnSc4d1	samostatné
<g />
.	.	kIx.	.
</s>
<s>
písmeno	písmeno	k1gNnSc1	písmeno
a	a	k8xC	a
nepíše	psát	k5eNaImIp3nS	psát
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
vov	vov	k?	vov
a	a	k8xC	a
jud	judo	k1gNnPc2	judo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gInSc5	Unicod
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ײ	ײ	k?	ײ
(	(	kIx(	(
<g/>
tsvey	tsvea	k1gMnSc2	tsvea
yudn	yudn	k1gNnSc4	yudn
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ej	ej	k0	ej
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgNnSc4d1	samostatné
písmeno	písmeno	k1gNnSc4	písmeno
a	a	k8xC	a
nepíše	psát	k5eNaImIp3nS	psát
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
yud	yud	k?	yud
za	za	k7c4	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gInSc5	Unicod
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ײ	ײ	k?	ײ
<g/>
ַ	ַ	k?	ַ
(	(	kIx(	(
<g/>
pasekh	pasekh	k1gMnSc1	pasekh
tsvey	tsvea	k1gFnSc2	tsvea
yudn	yudn	k1gMnSc1	yudn
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
aj	aj	kA	aj
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64287	[number]	k4	64287
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
F	F	kA	F
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LIGATURE	LIGATURE	kA	LIGATURE
YIDDISH	YIDDISH	kA	YIDDISH
DOUBLE	double	k1gInSc4	double
YOD	YOD	kA	YOD
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
PATAH	PATAH	kA	PATAH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
א	א	k?	א
<g/>
ַ	ַ	k?	ַ
(	(	kIx(	(
<g/>
pasekh	pasekh	k1gInSc1	pasekh
alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64302	[number]	k4	64302
(	(	kIx(	(
<g/>
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
alef	alef	k1gInSc1	alef
je	být	k5eAaImIp3nS	být
1488	[number]	k4	1488
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
D	D	kA	D
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
ALEF	alef	k1gInSc1	alef
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
PATAH	PATAH	kA	PATAH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
א	א	k?	א
<g/>
ָ	ָ	k?	ָ
(	(	kIx(	(
<g/>
komets	komets	k1gInSc1	komets
alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
o	o	k0	o
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64303	[number]	k4	64303
(	(	kIx(	(
<g/>
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
alef	alef	k1gInSc1	alef
je	být	k5eAaImIp3nS	být
1488	[number]	k4	1488
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
D	D	kA	D
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
ALEF	alef	k1gInSc1	alef
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
8	[number]	k4	8
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
QAMATS	QAMATS	kA	QAMATS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ב	ב	k?	ב
<g/>
ֿ	ֿ	k?	ֿ
(	(	kIx(	(
<g/>
veys	veys	k1gInSc1	veys
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
v	v	k7c4	v
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
semitského	semitský	k2eAgInSc2d1	semitský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
Unicode	Unicod	k1gInSc5	Unicod
64332	[number]	k4	64332
(	(	kIx(	(
<g/>
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
většinou	většina	k1gFnSc7	většina
pomocí	pomocí	k7c2	pomocí
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
beth	beth	k1gInSc1	beth
<g/>
/	/	kIx~	/
<g/>
beys	beys	k1gInSc1	beys
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
1489	[number]	k4	1489
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
D	D	kA	D
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
BET	BET	kA	BET
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
BF	BF	kA	BF
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
RAFE	RAFE	kA	RAFE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ו	ו	k?	ו
<g/>
ּ	ּ	k?	ּ
(	(	kIx(	(
<g/>
melupm	melupm	k1gMnSc1	melupm
vov	vov	k?	vov
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
u	u	k7c2	u
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64309	[number]	k4	64309
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
D	D	kA	D
<g/>
5	[number]	k4	5
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
VAV	VAV	kA	VAV
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
BC	BC	kA	BC
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
DAGESH	DAGESH	kA	DAGESH
OR	OR	kA	OR
MAPIQ	MAPIQ	kA	MAPIQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
(	(	kIx(	(
<g/>
khirik	khirik	k1gMnSc1	khirik
yud	yud	k?	yud
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64285	[number]	k4	64285
(	(	kIx(	(
<g/>
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
yud	yud	k?	yud
je	být	k5eAaImIp3nS	být
1497	[number]	k4	1497
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
D	D	kA	D
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
YOD	YOD	kA	YOD
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
HIRIQ	HIRIQ	kA	HIRIQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
כ	כ	k?	כ
<g/>
ּ	ּ	k?	ּ
(	(	kIx(	(
<g/>
kof	kof	k?	kof
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
semitského	semitský	k2eAgInSc2d1	semitský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
Unicode	Unicod	k1gInSc5	Unicod
64315	[number]	k4	64315
(	(	kIx(	(
<g/>
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
většinou	většina	k1gFnSc7	většina
pomocí	pomocí	k7c2	pomocí
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
kaf	kaf	k?	kaf
<g/>
/	/	kIx~	/
<g/>
khof	khof	k1gMnSc1	khof
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
1499	[number]	k4	1499
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
DB	db	kA	db
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
KAF	KAF	kA	KAF
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
BC	BC	kA	BC
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
DAGESH	DAGESH	kA	DAGESH
OR	OR	kA	OR
MAPIQ	MAPIQ	kA	MAPIQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
פ	פ	k?	פ
<g/>
ּ	ּ	k?	ּ
(	(	kIx(	(
<g/>
pey	pey	k?	pey
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
p	p	k?	p
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64324	[number]	k4	64324
(	(	kIx(	(
<g/>
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
protipól	protipól	k1gInSc1	protipól
je	být	k5eAaImIp3nS	být
1508	[number]	k4	1508
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemění	měnit	k5eNaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
fej	fej	k?	fej
ano	ano	k9	ano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
E	E	kA	E
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
PE	PE	kA	PE
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
BC	BC	kA	BC
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
DAGESH	DAGESH	kA	DAGESH
OR	OR	kA	OR
MAPIQ	MAPIQ	kA	MAPIQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
פ	פ	k?	פ
<g/>
ֿ	ֿ	k?	ֿ
(	(	kIx(	(
<g/>
fey	fey	k?	fey
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
f	f	k?	f
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
Unicode	Unicod	k1gInSc5	Unicod
64334	[number]	k4	64334
(	(	kIx(	(
<g/>
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
protipól	protipól	k1gInSc1	protipól
je	být	k5eAaImIp3nS	být
1508	[number]	k4	1508
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
E	E	kA	E
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
PE	PE	kA	PE
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
BF	BF	kA	BF
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
RAFE	RAFE	kA	RAFE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ת	ת	k?	ת
<g/>
ּ	ּ	k?	ּ
(	(	kIx(	(
<g/>
tof	tof	k?	tof
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
semitského	semitský	k2eAgInSc2d1	semitský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
pomocí	pomocí	k7c2	pomocí
Unicode	Unicod	k1gInSc5	Unicod
64330	[number]	k4	64330
(	(	kIx(	(
<g/>
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
většinou	většina	k1gFnSc7	většina
pomocí	pomocí	k7c2	pomocí
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
tav	tav	k1gInSc4	tav
<g/>
/	/	kIx~	/
<g/>
sof	sofa	k1gFnPc2	sofa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
1514	[number]	k4	1514
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
EA	EA	kA	EA
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
TAV	tav	k1gInSc1	tav
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
BC	BC	kA	BC
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
DAGESH	DAGESH	kA	DAGESH
OR	OR	kA	OR
MAPIQ	MAPIQ	kA	MAPIQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ש	ש	k?	ש
<g/>
ׂ	ׂ	k?	ׂ
(	(	kIx(	(
<g/>
sin	sin	kA	sin
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
vždy	vždy	k6eAd1	vždy
píše	psát	k5eAaImIp3nS	psát
s	s	k7c7	s
tečkou	tečka	k1gFnSc7	tečka
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gInSc5	Unicod
64299	[number]	k4	64299
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
shin	shina	k1gFnPc2	shina
(	(	kIx(	(
<g/>
Unicode	Unicod	k1gInSc5	Unicod
1513	[number]	k4	1513
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
zanedbává	zanedbávat	k5eAaImIp3nS	zanedbávat
<g/>
.	.	kIx.	.
</s>
<s>
Alternativně	alternativně	k6eAd1	alternativně
kombinací	kombinace	k1gFnPc2	kombinace
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
E	E	kA	E
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
LETTER	LETTER	kA	LETTER
SHIN	SHIN	kA	SHIN
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
HEBREW	HEBREW	kA	HEBREW
POINT	pointa	k1gFnPc2	pointa
SIN	sin	kA	sin
DOT	DOT	kA	DOT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
nebo	nebo	k8xC	nebo
na	na	k7c6	na
kořeni	kořen	k1gInSc6	kořen
složených	složený	k2eAgNnPc2d1	složené
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
němčině	němčina	k1gFnSc3	němčina
jednodušší	jednoduchý	k2eAgFnSc3d2	jednodušší
a	a	k8xC	a
slovosled	slovosled	k1gInSc1	slovosled
volnější	volný	k2eAgInSc1d2	volnější
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgInSc1d1	blízký
slovanskému	slovanský	k2eAgInSc3d1	slovanský
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
70	[number]	k4	70
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
je	být	k5eAaImIp3nS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
německých	německý	k2eAgMnPc2d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Výrazy	výraz	k1gInPc1	výraz
související	související	k2eAgMnSc1d1	související
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
kulturní	kulturní	k2eAgFnSc7d1	kulturní
tradicí	tradice	k1gFnSc7	tradice
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
nebo	nebo	k8xC	nebo
aramejského	aramejský	k2eAgInSc2d1	aramejský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
románských	románský	k2eAgInPc2d1	románský
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
choč	choč	k1gFnSc1	choč
=	=	kIx~	=
"	"	kIx"	"
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
večere	večer	k1gInSc5	večer
=	=	kIx~	=
"	"	kIx"	"
<g/>
večeře	večeře	k1gFnSc1	večeře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pamelech	pamel	k1gMnPc6	pamel
=	=	kIx~	=
"	"	kIx"	"
<g/>
pomalu	pomalu	k6eAd1	pomalu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přibývají	přibývat	k5eAaImIp3nP	přibývat
i	i	k9	i
slova	slovo	k1gNnPc4	slovo
anglická	anglický	k2eAgNnPc4d1	anglické
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
jidiš	jidiš	k1gNnSc1	jidiš
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
čurbes	čurbes	k1gInSc1	čurbes
dales	dales	k1gInSc1	dales
chucpe	chucpat	k5eAaPmIp3nS	chucpat
jarmulka	jarmulka	k1gFnSc1	jarmulka
klezmer	klezmra	k1gFnPc2	klezmra
jidiš	jidiš	k6eAd1	jidiš
kepule	kepule	k1gFnSc1	kepule
kibic	kibic	k1gMnSc1	kibic
mešuge	mešuge	k2eAgInSc4d1	mešuge
pajzl	pajzl	k1gInSc4	pajzl
póvl	póvl	k1gInSc1	póvl
šábes	šábes	k1gInSc1	šábes
šikse	šikse	k1gFnSc1	šikse
<g/>
,	,	kIx,	,
šiksa	šiksa	k1gFnSc1	šiksa
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
etymologie	etymologie	k1gFnSc2	etymologie
slova	slovo	k1gNnSc2	slovo
šmok	šmok	k1gMnSc1	šmok
šoufl	šoufl	k?	šoufl
šoulet	šoulet	k1gInSc1	šoulet
štetl	štetnout	k5eAaPmAgInS	štetnout
</s>
