<p>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
V.	V.	kA	V.
(	(	kIx(	(
<g/>
*	*	kIx~	*
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
zemřel	zemřít	k5eAaPmAgInS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
965	[number]	k4	965
nebo	nebo	k8xC	nebo
966	[number]	k4	966
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
))	))	k?	))
byl	být	k5eAaImAgMnS	být
132	[number]	k4	132
<g/>
.	.	kIx.	.
papež	papež	k1gMnSc1	papež
římsko-katolické	římskoatolický	k2eAgFnSc2d1	římsko-katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
vznešené	vznešený	k2eAgFnSc2d1	vznešená
římské	římský	k2eAgFnSc2d1	římská
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Regio	Regio	k6eAd1	Regio
Marini	Marin	k2eAgMnPc1d1	Marin
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jáhnem	jáhen	k1gMnSc7	jáhen
a	a	k8xC	a
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
byl	být	k5eAaImAgInS	být
přezdíván	přezdíván	k2eAgInSc1d1	přezdíván
grammaticus	grammaticus	k1gInSc1	grammaticus
<g/>
.	.	kIx.	.
</s>
<s>
Papežem	Papež	k1gMnSc7	Papež
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Římany	Říman	k1gMnPc7	Říman
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
jejich	jejich	k3xOp3gFnSc2	jejich
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
proti	proti	k7c3	proti
římskému	římský	k2eAgMnSc3d1	římský
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
den	den	k1gInSc1	den
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
synodem	synod	k1gInSc7	synod
sesazeného	sesazený	k2eAgMnSc2d1	sesazený
Jana	Jan	k1gMnSc2	Jan
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
964	[number]	k4	964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Ota	Oto	k1gMnSc2	Oto
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
volbu	volba	k1gFnSc4	volba
neuznal	uznat	k5eNaPmAgMnS	uznat
<g/>
,	,	kIx,	,
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
a	a	k8xC	a
vyhladověl	vyhladovět	k5eAaPmAgMnS	vyhladovět
město	město	k1gNnSc4	město
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
Benedikta	Benedikt	k1gMnSc2	Benedikt
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
zajal	zajmout	k5eAaPmAgInS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
svého	svůj	k3xOyFgMnSc2	svůj
papežského	papežský	k2eAgMnSc2d1	papežský
kandidáta	kandidát	k1gMnSc2	kandidát
Lva	lev	k1gInSc2	lev
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	benedikt	k1gInSc1	benedikt
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
slitování	slitování	k1gNnSc4	slitování
a	a	k8xC	a
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
jej	on	k3xPp3gMnSc4	on
císař	císař	k1gMnSc1	císař
odvezl	odvézt	k5eAaPmAgMnS	odvézt
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
ponechal	ponechat	k5eAaPmAgMnS	ponechat
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
biskupa	biskup	k1gMnSc2	biskup
Adalga	Adalg	k1gMnSc2	Adalg
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
tam	tam	k6eAd1	tam
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
v	v	k7c6	v
mariánském	mariánský	k2eAgInSc6d1	mariánský
chrámu	chrám	k1gInSc6	chrám
(	(	kIx(	(
<g/>
dómu	dóm	k1gInSc2	dóm
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
poctami	pocta	k1gFnPc7	pocta
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrob	hrob	k1gInSc1	hrob
a	a	k8xC	a
kenotaf	kenotaf	k1gInSc1	kenotaf
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
998	[number]	k4	998
-	-	kIx~	-
1000	[number]	k4	1000
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
císaře	císař	k1gMnSc2	císař
Otty	Otta	k1gMnSc2	Otta
III	III	kA	III
<g/>
.	.	kIx.	.
wormským	wormský	k2eAgInSc7d1	wormský
biskupem	biskup	k1gInSc7	biskup
vyzvednuty	vyzvednout	k5eAaPmNgFnP	vyzvednout
a	a	k8xC	a
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chóru	chór	k1gInSc6	chór
starého	starý	k2eAgInSc2d1	starý
hamburského	hamburský	k2eAgInSc2d1	hamburský
dómu	dóm	k1gInSc2	dóm
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
kenotaf	kenotaf	k1gInSc1	kenotaf
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
obložen	obložen	k2eAgInSc1d1	obložen
deskami	deska	k1gFnPc7	deska
z	z	k7c2	z
pestrobarevně	pestrobarevně	k6eAd1	pestrobarevně
malované	malovaný	k2eAgFnSc2d1	malovaná
terakoty	terakota	k1gFnSc2	terakota
<g/>
.	.	kIx.	.
</s>
<s>
Kenotaf	kenotaf	k1gInSc1	kenotaf
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
rozbit	rozbit	k2eAgInSc1d1	rozbit
<g/>
,	,	kIx,	,
po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
nového	nový	k2eAgInSc2d1	nový
mariánského	mariánský	k2eAgInSc2d1	mariánský
dómu	dóm	k1gInSc2	dóm
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
tři	tři	k4xCgInPc1	tři
fragmenty	fragment	k1gInPc1	fragment
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
ambitu	ambit	k1gInSc6	ambit
dómu	dóm	k1gInSc2	dóm
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
hamburských	hamburský	k2eAgFnPc2d1	hamburská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Filip	Filip	k1gMnSc1	Filip
Kronberger	Kronberger	k1gMnSc1	Kronberger
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Lergetporer	Lergetporra	k1gFnPc2	Lergetporra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
1086	[number]	k4	1086
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
284	[number]	k4	284
</s>
</p>
