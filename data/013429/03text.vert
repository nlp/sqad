<p>
<s>
Hamrštejn	Hamrštejn	k1gNnSc1	Hamrštejn
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hammerstein	Hammerstein	k2eAgInSc1d1	Hammerstein
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
postaveného	postavený	k2eAgInSc2d1	postavený
na	na	k7c6	na
skalním	skalní	k2eAgInSc6d1	skalní
hřebenu	hřeben	k1gInSc6	hřeben
Ovčí	ovčí	k2eAgFnSc2d1	ovčí
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
496	[number]	k4	496
m	m	kA	m
<g/>
)	)	kIx)	)
zvaném	zvaný	k2eAgInSc6d1	zvaný
Zámecký	zámecký	k2eAgInSc1d1	zámecký
kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
375	[number]	k4	375
m	m	kA	m
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
meandrem	meandr	k1gInSc7	meandr
Lužické	lužický	k2eAgFnSc2d1	Lužická
Nisy	Nisa	k1gFnSc2	Nisa
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
liberecké	liberecký	k2eAgFnSc2d1	liberecká
čtvrti	čtvrt	k1gFnSc2	čtvrt
Machnín	Machnína	k1gFnPc2	Machnína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Hamrštejn	Hamrštejna	k1gFnPc2	Hamrštejna
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Machnín	Machnín	k1gMnSc1	Machnín
hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
089	[number]	k4	089
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
český	český	k2eAgInSc1d1	český
transbordér	transbordér	k1gInSc1	transbordér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
hradu	hrad	k1gInSc2	hrad
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obklopen	obklopit	k5eAaPmNgInS	obklopit
strmými	strmý	k2eAgInPc7d1	strmý
svahy	svah	k1gInPc7	svah
a	a	k8xC	a
obehnán	obehnán	k2eAgInSc4d1	obehnán
dodnes	dodnes	k6eAd1	dodnes
částečně	částečně	k6eAd1	částečně
zachovalou	zachovalý	k2eAgFnSc7d1	zachovalá
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
oválného	oválný	k2eAgInSc2d1	oválný
opevněného	opevněný	k2eAgInSc2d1	opevněný
prostoru	prostor	k1gInSc2	prostor
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
67	[number]	k4	67
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
tyčily	tyčit	k5eAaImAgFnP	tyčit
válcové	válcový	k2eAgFnPc1d1	válcová
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc4d2	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
bývala	bývat	k5eAaImAgFnS	bývat
na	na	k7c6	na
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
straně	strana	k1gFnSc6	strana
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
10,5	[number]	k4	10,5
metru	metr	k1gInSc2	metr
při	při	k7c6	při
šířce	šířka	k1gFnSc6	šířka
zdí	zeď	k1gFnPc2	zeď
mezi	mezi	k7c7	mezi
1,5	[number]	k4	1,5
a	a	k8xC	a
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
věž	věž	k1gFnSc1	věž
obytná	obytný	k2eAgFnSc1d1	obytná
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
nadzemní	nadzemní	k2eAgInSc1d1	nadzemní
zbytek	zbytek	k1gInSc1	zbytek
této	tento	k3xDgFnSc2	tento
věže	věž	k1gFnSc2	věž
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
podkotlán	podkotlán	k2eAgMnSc1d1	podkotlán
a	a	k8xC	a
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
sesune	sesunout	k5eAaPmIp3nS	sesunout
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
věž	věž	k1gFnSc1	věž
pak	pak	k6eAd1	pak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
plnila	plnit	k5eAaImAgFnS	plnit
funkci	funkce	k1gFnSc4	funkce
hlásky	hláska	k1gFnSc2	hláska
a	a	k8xC	a
pozorovatelny	pozorovatelna	k1gFnSc2	pozorovatelna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
a	a	k8xC	a
přístupná	přístupný	k2eAgFnSc1d1	přístupná
po	po	k7c6	po
schůdcích	schůdek	k1gInPc6	schůdek
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
uvnitř	uvnitř	k7c2	uvnitř
opevněné	opevněný	k2eAgFnSc2d1	opevněná
části	část	k1gFnSc2	část
nebyly	být	k5eNaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
žádné	žádný	k3yNgInPc1	žádný
zbytky	zbytek	k1gInPc1	zbytek
kamenných	kamenný	k2eAgFnPc2d1	kamenná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
případné	případný	k2eAgFnPc1d1	případná
stavby	stavba	k1gFnPc1	stavba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hradní	hradní	k2eAgInSc1d1	hradní
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
<g/>
.	.	kIx.	.
</s>
<s>
Přístupová	přístupový	k2eAgFnSc1d1	přístupová
brána	brána	k1gFnSc1	brána
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
částí	část	k1gFnSc7	část
zdi	zeď	k1gFnSc2	zeď
zřítila	zřítit	k5eAaPmAgFnS	zřítit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
založení	založení	k1gNnSc2	založení
hradu	hrad	k1gInSc2	hrad
není	být	k5eNaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
již	již	k6eAd1	již
stojící	stojící	k2eAgMnSc1d1	stojící
<g/>
)	)	kIx)	)
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
kterou	který	k3yRgFnSc4	který
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
věrnost	věrnost	k1gFnSc4	věrnost
císaři	císař	k1gMnSc3	císař
Karlu	Karel	k1gMnSc3	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
tehdy	tehdy	k6eAd1	tehdy
jako	jako	k8xC	jako
správní	správní	k2eAgNnSc1d1	správní
centrum	centrum	k1gNnSc1	centrum
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
části	část	k1gFnSc2	část
biberštejnského	biberštejnského	k2eAgNnSc1d1	biberštejnského
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
centrem	centrum	k1gNnSc7	centrum
byl	být	k5eAaImAgInS	být
Frýdlant	Frýdlant	k1gInSc1	Frýdlant
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
hradu	hrad	k1gInSc2	hrad
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
hamrů	hamr	k1gInPc2	hamr
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
umístění	umístění	k1gNnSc4	umístění
této	tento	k3xDgFnSc2	tento
středověké	středověký	k2eAgFnSc2d1	středověká
železárny	železárna	k1gFnSc2	železárna
neznáme	znát	k5eNaImIp1nP	znát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hamr	hamr	k1gInSc1	hamr
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
již	již	k6eAd1	již
koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
hradu	hrad	k1gInSc2	hrad
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
bibrštejnského	bibrštejnského	k2eAgNnSc1d1	bibrštejnského
panství	panství	k1gNnSc1	panství
napovídá	napovídat	k5eAaBmIp3nS	napovídat
o	o	k7c6	o
snaze	snaha	k1gFnSc6	snaha
bránit	bránit	k5eAaImF	bránit
jeho	jeho	k3xOp3gFnSc4	jeho
územní	územní	k2eAgFnSc4d1	územní
celistvost	celistvost	k1gFnSc4	celistvost
proti	proti	k7c3	proti
rodu	rod	k1gInSc3	rod
Donínů	Donín	k1gInPc2	Donín
na	na	k7c6	na
sousedním	sousední	k2eAgInSc6d1	sousední
hradě	hrad	k1gInSc6	hrad
Grabštejně	Grabštejně	k1gFnSc2	Grabštejně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
často	často	k6eAd1	často
uváděná	uváděný	k2eAgFnSc1d1	uváděná
domněnka	domněnka	k1gFnSc1	domněnka
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
dolů	dol	k1gInPc2	dol
na	na	k7c4	na
železnou	železný	k2eAgFnSc4d1	železná
<g/>
,	,	kIx,	,
olověnou	olověný	k2eAgFnSc4d1	olověná
<g/>
,	,	kIx,	,
měděnou	měděný	k2eAgFnSc4d1	měděná
a	a	k8xC	a
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
rudu	ruda	k1gFnSc4	ruda
u	u	k7c2	u
Andělské	andělský	k2eAgFnSc2d1	Andělská
hory	hora	k1gFnSc2	hora
není	být	k5eNaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
doly	dol	k1gInPc1	dol
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c4	na
panství	panství	k1gNnSc4	panství
Donínů	Donín	k1gMnPc2	Donín
a	a	k8xC	a
Biberštejnové	Biberštejn	k1gMnPc1	Biberštejn
tak	tak	k6eAd1	tak
neměli	mít	k5eNaImAgMnP	mít
důvod	důvod	k1gInSc4	důvod
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
hradu	hrad	k1gInSc2	hrad
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
majetku	majetek	k1gInSc2	majetek
cizího	cizí	k2eAgInSc2d1	cizí
(	(	kIx(	(
<g/>
a	a	k8xC	a
často	často	k6eAd1	často
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
<g/>
)	)	kIx)	)
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posádku	posádka	k1gFnSc4	posádka
hradu	hrad	k1gInSc2	hrad
tvořilo	tvořit	k5eAaImAgNnS	tvořit
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
mužů	muž	k1gMnPc2	muž
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
purkrabího	purkrabí	k1gMnSc2	purkrabí
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
známým	známý	k1gMnSc7	známý
purkrabím	purkrabí	k1gMnPc3	purkrabí
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
Hans	hansa	k1gFnPc2	hansa
Dachs	Dachsa	k1gFnPc2	Dachsa
ze	z	k7c2	z
slezské	slezský	k2eAgFnSc2d1	Slezská
rytířské	rytířský	k2eAgFnSc2d1	rytířská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1414	[number]	k4	1414
až	až	k8xS	až
1425	[number]	k4	1425
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
purkrabím	purkrabí	k1gMnSc7	purkrabí
Fredemann	Fredemann	k1gMnSc1	Fredemann
z	z	k7c2	z
Gersdorfu	Gersdorf	k1gInSc2	Gersdorf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dochované	dochovaný	k2eAgFnSc6d1	dochovaná
listině	listina	k1gFnSc6	listina
datované	datovaný	k2eAgNnSc1d1	datované
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1411	[number]	k4	1411
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úřadu	úřad	k1gInSc6	úřad
uveden	uvést	k5eAaPmNgInS	uvést
Mikuláš	mikuláš	k1gInSc1	mikuláš
Dachs	Dachsa	k1gFnPc2	Dachsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
husitské	husitský	k2eAgFnSc2d1	husitská
patřili	patřit	k5eAaImAgMnP	patřit
Biberštejnové	Biberštejnová	k1gFnPc4	Biberštejnová
k	k	k7c3	k
protivníkům	protivník	k1gMnPc3	protivník
husitů	husita	k1gMnPc2	husita
a	a	k8xC	a
do	do	k7c2	do
protikališnického	protikališnický	k2eAgInSc2d1	protikališnický
tábora	tábor	k1gInSc2	tábor
tak	tak	k6eAd1	tak
patřil	patřit	k5eAaImAgInS	patřit
přirozeně	přirozeně	k6eAd1	přirozeně
i	i	k9	i
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dachs	Dachs	k1gInSc1	Dachs
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
hradu	hrad	k1gInSc2	hrad
Hamrštejn	Hamrštejno	k1gNnPc2	Hamrštejno
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
bylo	být	k5eAaImAgNnS	být
poměrně	poměrně	k6eAd1	poměrně
problematické	problematický	k2eAgNnSc1d1	problematické
–	–	k?	–
pouhých	pouhý	k2eAgInPc2d1	pouhý
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
byl	být	k5eAaImAgInS	být
husity	husita	k1gMnSc2	husita
obsazený	obsazený	k2eAgInSc4d1	obsazený
Grabštejn	Grabštejn	k1gInSc4	Grabštejn
<g/>
,	,	kIx,	,
husitskou	husitský	k2eAgFnSc4d1	husitská
posádku	posádka	k1gFnSc4	posádka
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
tvrz	tvrz	k1gFnSc1	tvrz
v	v	k7c6	v
Chrastavě	chrastavě	k6eAd1	chrastavě
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Raimundu	Raimunda	k1gFnSc4	Raimunda
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
nespolehlivá	spolehlivý	k2eNgFnSc1d1	nespolehlivá
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
posádka	posádka	k1gFnSc1	posádka
Hamrštejnu	Hamrštejn	k1gInSc2	Hamrštejn
postupovala	postupovat	k5eAaImAgFnS	postupovat
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
aktivně	aktivně	k6eAd1	aktivně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dachs	Dachs	k1gInSc1	Dachs
dodával	dodávat	k5eAaImAgInS	dodávat
do	do	k7c2	do
Lužice	Lužice	k1gFnSc2	Lužice
cenné	cenný	k2eAgFnSc2d1	cenná
zprávy	zpráva	k1gFnSc2	zpráva
a	a	k8xC	a
že	že	k8xS	že
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
zajížděli	zajíždět	k5eAaImAgMnP	zajíždět
žitavští	žitavský	k2eAgMnPc1d1	žitavský
poslové	posel	k1gMnPc1	posel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1433	[number]	k4	1433
napadli	napadnout	k5eAaPmAgMnP	napadnout
žoldnéři	žoldnér	k1gMnPc1	žoldnér
Oldřicha	Oldřich	k1gMnSc2	Oldřich
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
část	část	k1gFnSc4	část
grabštejnské	grabštejnský	k2eAgFnSc2d1	grabštejnský
husitské	husitský	k2eAgFnSc2d1	husitská
posádky	posádka	k1gFnSc2	posádka
vracející	vracející	k2eAgFnSc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
nájezdu	nájezd	k1gInSc2	nájezd
na	na	k7c4	na
Frýdlantsko	Frýdlantsko	k1gNnSc4	Frýdlantsko
a	a	k8xC	a
porazili	porazit	k5eAaPmAgMnP	porazit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
pak	pak	k6eAd1	pak
využili	využít	k5eAaPmAgMnP	využít
<g/>
,	,	kIx,	,
podnikli	podniknout	k5eAaPmAgMnP	podniknout
výpad	výpad	k1gInSc4	výpad
k	k	k7c3	k
Chrastavě	chrastavě	k6eAd1	chrastavě
a	a	k8xC	a
tamní	tamní	k2eAgFnSc1d1	tamní
tvrz	tvrz	k1gFnSc1	tvrz
vypálili	vypálit	k5eAaPmAgMnP	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
aby	aby	kYmCp3nP	aby
husité	husita	k1gMnPc1	husita
náhradou	náhrada	k1gFnSc7	náhrada
získali	získat	k5eAaPmAgMnP	získat
jiné	jiný	k2eAgNnSc4d1	jiné
opevněné	opevněný	k2eAgNnSc4d1	opevněné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
zformovali	zformovat	k5eAaPmAgMnP	zformovat
u	u	k7c2	u
Českého	český	k2eAgInSc2d1	český
Dubu	dub	k1gInSc2	dub
oddíl	oddíl	k1gInSc1	oddíl
o	o	k7c6	o
400	[number]	k4	400
jezdcích	jezdec	k1gInPc6	jezdec
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Jana	Jan	k1gMnSc2	Jan
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
Sán	sán	k2eAgMnSc1d1	sán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
proti	proti	k7c3	proti
Hamrštejnu	Hamrštejn	k1gInSc3	Hamrštejn
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vyslal	vyslat	k5eAaPmAgMnS	vyslat
ještě	ještě	k6eAd1	ještě
Dachs	Dachs	k1gInSc4	Dachs
do	do	k7c2	do
Zhořelce	Zhořelec	k1gInSc2	Zhořelec
posla	posel	k1gMnSc2	posel
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
však	však	k9	však
ta	ten	k3xDgFnSc1	ten
dorazila	dorazit	k5eAaPmAgFnS	dorazit
<g/>
,	,	kIx,	,
drželi	držet	k5eAaImAgMnP	držet
již	již	k6eAd1	již
Hamrštejn	Hamrštejn	k1gNnSc4	Hamrštejn
husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
obránci	obránce	k1gMnPc1	obránce
patrně	patrně	k6eAd1	patrně
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
mnohonásobné	mnohonásobný	k2eAgFnSc2d1	mnohonásobná
přesily	přesila	k1gFnSc2	přesila
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
husitských	husitský	k2eAgFnPc6d1	husitská
rukách	ruka	k1gFnPc6	ruka
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1435	[number]	k4	1435
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Dachsova	Dachsův	k2eAgNnSc2d1	Dachsův
držení	držení	k1gNnSc2	držení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
se	se	k3xPyFc4	se
rozhořelo	rozhořet	k5eAaPmAgNnS	rozhořet
množství	množství	k1gNnSc1	množství
regionálních	regionální	k2eAgInPc2d1	regionální
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
šlechtici	šlechtic	k1gMnPc7	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Českolipsku	Českolipsko	k1gNnSc6	Českolipsko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
mezi	mezi	k7c7	mezi
Lužicí	Lužice	k1gFnSc7	Lužice
a	a	k8xC	a
Vartenberky	Vartenberk	k1gInPc7	Vartenberk
<g/>
,	,	kIx,	,
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c7	mezi
Biberštejny	Biberštejn	k1gMnPc7	Biberštejn
a	a	k8xC	a
Doníny	Donín	k1gMnPc7	Donín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1450	[number]	k4	1450
se	se	k3xPyFc4	se
Biberštejnové	Biberštejn	k1gMnPc1	Biberštejn
přidali	přidat	k5eAaPmAgMnP	přidat
k	k	k7c3	k
vojsku	vojsko	k1gNnSc3	vojsko
lužických	lužický	k2eAgNnPc2d1	Lužické
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
oblehlo	oblehnout	k5eAaPmAgNnS	oblehnout
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
dobylo	dobýt	k5eAaPmAgNnS	dobýt
Grabštejn	Grabštejn	k1gInSc4	Grabštejn
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Donína	Donín	k1gInSc2	Donín
musel	muset	k5eAaImAgMnS	muset
zavázat	zavázat	k5eAaPmF	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nebude	být	k5eNaImBp3nS	být
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
Lužici	Lužice	k1gFnSc3	Lužice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1467	[number]	k4	1467
napadl	napadnout	k5eAaPmAgMnS	napadnout
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Dachs	Dachs	k1gInSc4	Dachs
Chrastavu	Chrastav	k1gInSc2	Chrastav
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
Donínové	Donín	k1gMnPc1	Donín
na	na	k7c4	na
čas	čas	k1gInSc4	čas
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Hamrštejna	Hamrštejno	k1gNnPc4	Hamrštejno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1452	[number]	k4	1452
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
byl	být	k5eAaImAgInS	být
purkrabím	purkrabí	k1gMnSc7	purkrabí
na	na	k7c6	na
Hamrštejně	Hamrštejně	k1gFnSc6	Hamrštejně
Heinz	Heinza	k1gFnPc2	Heinza
Dachs	Dachs	k1gInSc1	Dachs
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Dachse	Dachse	k1gFnSc2	Dachse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1454	[number]	k4	1454
patřil	patřit	k5eAaImAgInS	patřit
hrad	hrad	k1gInSc1	hrad
Oldřichovi	Oldřich	k1gMnSc3	Oldřich
<g/>
,	,	kIx,	,
Václavovi	Václav	k1gMnSc3	Václav
a	a	k8xC	a
Bedřichovi	Bedřich	k1gMnSc3	Bedřich
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
poté	poté	k6eAd1	poté
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
stál	stát	k5eAaImAgMnS	stát
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
smíru	smír	k1gInSc2	smír
neuznali	uznat	k5eNaPmAgMnP	uznat
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Biberštejna	Biberštejno	k1gNnSc2	Biberštejno
včas	včas	k6eAd1	včas
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonského	jagellonský	k2eAgMnSc4d1	jagellonský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Hamrštejn	Hamrštejn	k1gInSc1	Hamrštejn
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
propadlé	propadlý	k2eAgNnSc4d1	propadlé
léno	léno	k1gNnSc4	léno
a	a	k8xC	a
purkrabím	purkrabí	k1gMnPc3	purkrabí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Hanuš	Hanuš	k1gMnSc1	Hanuš
z	z	k7c2	z
Rechenberka	Rechenberka	k1gFnSc1	Rechenberka
<g/>
.	.	kIx.	.
</s>
<s>
Hanuš	Hanuš	k1gMnSc1	Hanuš
byl	být	k5eAaImAgMnS	být
nařčen	nařknout	k5eAaPmNgMnS	nařknout
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Donína	Donín	k1gInSc2	Donín
z	z	k7c2	z
neoprávněného	oprávněný	k2eNgNnSc2d1	neoprávněné
držení	držení	k1gNnSc2	držení
vesnic	vesnice	k1gFnPc2	vesnice
Machnín	Machnína	k1gFnPc2	Machnína
<g/>
,	,	kIx,	,
Krásná	krásný	k2eAgFnSc1d1	krásná
Studánka	studánka	k1gFnSc1	studánka
<g/>
,	,	kIx,	,
Svárov	Svárov	k1gInSc1	Svárov
a	a	k8xC	a
Molberk	Molberk	k1gInSc1	Molberk
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Machnín	Machnín	k1gInSc1	Machnín
tehdy	tehdy	k6eAd1	tehdy
připadl	připadnout	k5eAaPmAgInS	připadnout
Donínům	Donín	k1gInPc3	Donín
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgFnSc2d1	zbylá
vesnice	vesnice	k1gFnSc2	vesnice
měly	mít	k5eAaImAgFnP	mít
patřit	patřit	k5eAaImF	patřit
Hamrštejnu	Hamrštejna	k1gFnSc4	Hamrštejna
<g/>
.	.	kIx.	.
</s>
<s>
Vrchními	vrchní	k2eAgMnPc7d1	vrchní
pány	pan	k1gMnPc7	pan
oblasti	oblast	k1gFnSc2	oblast
tak	tak	k6eAd1	tak
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
Biberštejnové	Biberštejn	k1gMnPc1	Biberštejn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
<g/>
,	,	kIx,	,
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Oldřichovi	Oldřich	k1gMnSc3	Oldřich
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
opět	opět	k6eAd1	opět
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
statky	statek	k1gInPc4	statek
<g/>
,	,	kIx,	,
Hamrštejn	Hamrštejn	k1gInSc4	Hamrštejn
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
připadl	připadnout	k5eAaPmAgInS	připadnout
hrad	hrad	k1gInSc1	hrad
jako	jako	k8xS	jako
zástava	zástava	k1gFnSc1	zástava
Ulrichu	Ulrich	k1gMnSc3	Ulrich
Gotschovi	Gotsch	k1gMnSc3	Gotsch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc7	jeho
majiteli	majitel	k1gMnPc7	majitel
opět	opět	k6eAd1	opět
stali	stát	k5eAaPmAgMnP	stát
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1545	[number]	k4	1545
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
vleklého	vleklý	k2eAgNnSc2d1	vleklé
(	(	kIx(	(
<g/>
bezmála	bezmála	k6eAd1	bezmála
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c4	mezi
Doníny	Donína	k1gFnPc4	Donína
a	a	k8xC	a
Bibrštejny	Bibrštejn	k1gInPc4	Bibrštejn
o	o	k7c4	o
ves	ves	k1gFnSc4	ves
Machnín	Machnína	k1gFnPc2	Machnína
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
vítězství	vítězství	k1gNnSc1	vítězství
Donínů	Donín	k1gMnPc2	Donín
a	a	k8xC	a
naprostá	naprostý	k2eAgFnSc1d1	naprostá
izolace	izolace	k1gFnSc1	izolace
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsudku	rozsudek	k1gInSc6	rozsudek
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
patří	patřit	k5eAaImIp3nS	patřit
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
kam	kam	k6eAd1	kam
dopadají	dopadat	k5eAaImIp3nP	dopadat
kapky	kapka	k1gFnPc1	kapka
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1551	[number]	k4	1551
smrtí	smrt	k1gFnSc7	smrt
Kryštofa	Kryštof	k1gMnSc2	Kryštof
z	z	k7c2	z
Biberštejna	Biberštejn	k1gInSc2	Biberštejn
vymírá	vymírat	k5eAaImIp3nS	vymírat
frýdlantská	frýdlantský	k2eAgFnSc1d1	Frýdlantská
větev	větev	k1gFnSc1	větev
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc1	veškerý
majetek	majetek	k1gInSc1	majetek
připadá	připadat	k5eAaPmIp3nS	připadat
jako	jako	k9	jako
odumřelé	odumřelý	k2eAgNnSc4d1	odumřelé
léno	léno	k1gNnSc4	léno
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1558	[number]	k4	1558
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Redernu	Rederna	k1gFnSc4	Rederna
frýdlantské	frýdlantský	k2eAgFnPc1d1	Frýdlantská
a	a	k8xC	a
liberecké	liberecký	k2eAgNnSc1d1	liberecké
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Hamrštejn	Hamrštejn	k1gMnSc1	Hamrštejn
již	již	k6eAd1	již
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xC	jako
pustý	pustý	k2eAgMnSc1d1	pustý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Machnín	Machnín	k1gInSc4	Machnín
hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
Zámecký	zámecký	k2eAgInSc1d1	zámecký
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ostroh	ostroh	k1gInSc1	ostroh
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
obtékaný	obtékaný	k2eAgInSc1d1	obtékaný
Lužickou	lužický	k2eAgFnSc7d1	Lužická
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Zastávka	zastávka	k1gFnSc1	zastávka
nemá	mít	k5eNaImIp3nS	mít
zpevněné	zpevněný	k2eAgNnSc4d1	zpevněné
nástupiště	nástupiště	k1gNnSc4	nástupiště
ani	ani	k8xC	ani
nástupní	nástupní	k2eAgFnSc4d1	nástupní
hranu	hrana	k1gFnSc4	hrana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
jen	jen	k9	jen
malým	malý	k2eAgInSc7d1	malý
plechovým	plechový	k2eAgInSc7d1	plechový
přístřeškem	přístřešek	k1gInSc7	přístřešek
<g/>
,	,	kIx,	,
přístupovou	přístupový	k2eAgFnSc7d1	přístupová
cestou	cesta	k1gFnSc7	cesta
vedoucí	vedoucí	k1gFnSc2	vedoucí
od	od	k7c2	od
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
rozcestí	rozcestí	k1gNnSc2	rozcestí
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
pěšina	pěšina	k1gFnSc1	pěšina
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
protilehlé	protilehlý	k2eAgFnSc6d1	protilehlá
straně	strana	k1gFnSc6	strana
trati	trať	k1gFnSc2	trať
než	než	k8xS	než
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
trať	trať	k1gFnSc4	trať
zde	zde	k6eAd1	zde
však	však	k9	však
není	být	k5eNaImIp3nS	být
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
žádný	žádný	k3yNgInSc1	žádný
legální	legální	k2eAgInSc1d1	legální
přechod	přechod	k1gInSc1	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
asi	asi	k9	asi
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
zastávky	zastávka	k1gFnSc2	zastávka
<g/>
,	,	kIx,	,
při	při	k7c6	při
pěšině	pěšina	k1gFnSc6	pěšina
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
zbytky	zbytek	k1gInPc1	zbytek
značení	značení	k1gNnSc1	značení
dávno	dávno	k6eAd1	dávno
zrušené	zrušený	k2eAgFnPc4d1	zrušená
odbočky	odbočka	k1gFnPc4	odbočka
turistického	turistický	k2eAgNnSc2d1	turistické
značení	značení	k1gNnSc2	značení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zastávce	zastávka	k1gFnSc6	zastávka
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
jen	jen	k9	jen
část	část	k1gFnSc1	část
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
jezdících	jezdící	k2eAgInPc2d1	jezdící
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
hradu	hrad	k1gInSc2	hrad
projíždí	projíždět	k5eAaImIp3nS	projíždět
té	ten	k3xDgFnSc3	ten
liberecká	liberecký	k2eAgFnSc1d1	liberecká
městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
číslo	číslo	k1gNnSc1	číslo
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
zastávka	zastávka	k1gFnSc1	zastávka
Hamrštejn	Hamrštejna	k1gFnPc2	Hamrštejna
leží	ležet	k5eAaImIp3nS	ležet
však	však	k9	však
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
areálu	areál	k1gInSc6	areál
a	a	k8xC	a
osadě	osada	k1gFnSc6	osada
na	na	k7c6	na
protilehlé	protilehlý	k2eAgFnSc6d1	protilehlá
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
nevede	vést	k5eNaImIp3nS	vést
ke	k	k7c3	k
hradu	hrad	k1gInSc2	hrad
most	most	k1gInSc1	most
<g/>
;	;	kIx,	;
v	v	k7c6	v
pěším	pěší	k2eAgInSc6d1	pěší
dosahu	dosah	k1gInSc6	dosah
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
zastávky	zastávka	k1gFnPc1	zastávka
Kryštofovo	Kryštofův	k2eAgNnSc1d1	Kryštofovo
Údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
či	či	k8xC	či
Machnín	Machnín	k1gInSc1	Machnín
<g/>
,	,	kIx,	,
žel	žel	k9	žel
<g/>
.	.	kIx.	.
zast.	zast.	k?	zast.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
cestách	cesta	k1gFnPc6	cesta
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
vedou	vést	k5eAaImIp3nP	vést
cyklotrasy	cyklotrasa	k1gFnPc1	cyklotrasa
3008	[number]	k4	3008
a	a	k8xC	a
21	[number]	k4	21
a	a	k8xC	a
žlutě	žlutě	k6eAd1	žlutě
a	a	k8xC	a
modře	modro	k6eAd1	modro
značené	značený	k2eAgFnPc4d1	značená
pěší	pěší	k2eAgFnPc4d1	pěší
trasy	trasa	k1gFnPc4	trasa
KČT	KČT	kA	KČT
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
břehy	břeh	k1gInPc1	břeh
Lužické	lužický	k2eAgFnSc2d1	Lužická
Nisy	Nisa	k1gFnSc2	Nisa
tu	tu	k6eAd1	tu
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
železničním	železniční	k2eAgInSc7d1	železniční
mostem	most	k1gInSc7	most
spojuje	spojovat	k5eAaImIp3nS	spojovat
i	i	k9	i
malý	malý	k2eAgInSc1d1	malý
transbordér	transbordér	k1gInSc1	transbordér
neboli	neboli	k8xC	neboli
gondolový	gondolový	k2eAgInSc1d1	gondolový
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KARPAŠ	KARPAŠ	kA	KARPAŠ
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
:	:	kIx,	:
Dialog	dialog	k1gInSc1	dialog
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86761	[number]	k4	86761
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hamrštejn	Hamrštejn	k1gMnSc1	Hamrštejn
<g/>
.	.	kIx.	.
</s>
<s>
Minulost	minulost	k1gFnSc1	minulost
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
zříceniny	zřícenina	k1gFnSc2	zřícenina
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Renata	Renata	k1gFnSc1	Renata
Tišerová	Tišerová	k1gFnSc1	Tišerová
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
územní	územní	k2eAgNnSc4d1	územní
odborné	odborný	k2eAgNnSc4d1	odborné
pracoviště	pracoviště	k1gNnSc4	pracoviště
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
93	[number]	k4	93
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903934	[number]	k4	903934
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hradů	hrad	k1gInPc2	hrad
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hamrštejn	Hamrštejna	k1gFnPc2	Hamrštejna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
na	na	k7c4	na
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Hamrštejn	Hamrštejn	k1gInSc1	Hamrštejn
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
