<p>
<s>
Bardoňovo	Bardoňův	k2eAgNnSc1d1	Bardoňův
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Barsbaracska	Barsbaracsek	k1gMnSc2	Barsbaracsek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nové	Nové	k2eAgInPc1d1	Nové
Zámky	zámek	k1gInPc1	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1269	[number]	k4	1269
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Barakcha	Barakch	k1gMnSc2	Barakch
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Brakcha	Brakcha	k1gFnSc1	Brakcha
(	(	kIx(	(
<g/>
1298	[number]	k4	1298
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baracha	Baracha	k1gMnSc1	Baracha
(	(	kIx(	(
<g/>
1327	[number]	k4	1327
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nogbrakcha	Nogbrakcha	k1gMnSc1	Nogbrakcha
(	(	kIx(	(
<g/>
1394	[number]	k4	1394
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barachko	Barachko	k1gNnSc1	Barachko
(	(	kIx(	(
<g/>
1395	[number]	k4	1395
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baracžka	Baracžka	k1gFnSc1	Baracžka
(	(	kIx(	(
<g/>
1773	[number]	k4	1773
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baracska	Baracska	k1gFnSc1	Baracska
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bardoňovo	Bardoňův	k2eAgNnSc1d1	Bardoňův
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
obce	obec	k1gFnSc2	obec
byla	být	k5eAaImAgFnS	být
osídlená	osídlený	k2eAgFnSc1d1	osídlená
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tu	tu	k6eAd1	tu
zanechala	zanechat	k5eAaPmAgFnS	zanechat
své	svůj	k3xOyFgFnPc4	svůj
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
archeologicky	archeologicky	k6eAd1	archeologicky
doložené	doložený	k2eAgNnSc1d1	doložené
sídlo	sídlo	k1gNnSc1	sídlo
čačanské	čačanský	k2eAgFnSc2d1	čačanská
a	a	k8xC	a
velatické	velatický	k2eAgFnSc2d1	velatická
kultury	kultura	k1gFnSc2	kultura
z	z	k7c2	z
Mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgNnSc4d1	bronzové
a	a	k8xC	a
římsko-barbarské	římskoarbarský	k2eAgNnSc4d1	římsko-barbarský
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
získali	získat	k5eAaPmAgMnP	získat
silnější	silný	k2eAgMnPc1d2	silnější
obyvatelé	obyvatel	k1gMnPc1	obyvatel
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
osídlenci	osídlenec	k1gMnPc1	osídlenec
je	on	k3xPp3gMnPc4	on
nazvali	nazvat	k5eAaPmAgMnP	nazvat
Zeměpány	zeměpán	k1gMnPc4	zeměpán
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
patřila	patřit	k5eAaImAgFnS	patřit
zemanům	zeman	k1gMnPc3	zeman
z	z	k7c2	z
Baračky	Baračka	k1gFnSc2	Baračka
<g/>
,	,	kIx,	,
Berše	Berše	k1gFnSc2	Berše
<g/>
,	,	kIx,	,
Kľačan	Kľačan	k1gMnSc1	Kľačan
a	a	k8xC	a
Šimonovan	Šimonovan	k1gMnSc1	Šimonovan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1395	[number]	k4	1395
zeměpán	zeměpán	k1gMnSc1	zeměpán
Barakchai	Barakchai	k1gNnSc2	Barakchai
Ján	Ján	k1gMnSc1	Ján
daroval	darovat	k5eAaPmAgMnS	darovat
svou	svůj	k3xOyFgFnSc4	svůj
část	část	k1gFnSc4	část
Malou	malý	k2eAgFnSc4d1	malá
Barakchu	Barakcha	k1gFnSc4	Barakcha
opatství	opatství	k1gNnSc2	opatství
klášteru	klášter	k1gInSc3	klášter
v	v	k7c6	v
Hronském	hronský	k2eAgNnSc6d1	hronský
Beňadiku	Beňadikum	k1gNnSc6	Beňadikum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluúčastníkem	spoluúčastník	k1gMnSc7	spoluúčastník
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1601	[number]	k4	1601
měla	mít	k5eAaImAgFnS	mít
velkostatek	velkostatek	k1gInSc4	velkostatek
a	a	k8xC	a
44	[number]	k4	44
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
vinice	vinice	k1gFnSc1	vinice
23	[number]	k4	23
daňovníků	daňovník	k1gInPc2	daňovník
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
84	[number]	k4	84
domů	dům	k1gInPc2	dům
a	a	k8xC	a
591	[number]	k4	591
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
na	na	k7c6	na
polnohospodářství	polnohospodářství	k1gNnSc6	polnohospodářství
a	a	k8xC	a
vinohradnictví	vinohradnictví	k1gNnSc6	vinohradnictví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
velkostatky	velkostatek	k1gInPc4	velkostatek
Kelcsényiho	Kelcsényi	k1gMnSc4	Kelcsényi
a	a	k8xC	a
Hoffera	Hoffer	k1gMnSc4	Hoffer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
až	až	k9	až
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
připojená	připojený	k2eAgFnSc1d1	připojená
k	k	k7c3	k
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symboly	symbol	k1gInPc1	symbol
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Znak	znak	k1gInSc4	znak
obce	obec	k1gFnSc2	obec
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
patě	pata	k1gFnSc6	pata
červeného	červený	k2eAgInSc2d1	červený
štítu	štít	k1gInSc2	štít
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
radlice	radlice	k1gFnPc4	radlice
-	-	kIx~	-
souměrně	souměrně	k6eAd1	souměrně
skolněný	skolněný	k2eAgInSc1d1	skolněný
lemeš	lemeš	k1gInSc1	lemeš
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
položenými	položený	k2eAgFnPc7d1	položená
radlicemi	radlice	k1gFnPc7	radlice
po	po	k7c6	po
boku	bok	k1gInSc6	bok
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tím	ten	k3xDgInSc7	ten
zlatovlasí	zlatovlasý	k2eAgMnPc1d1	zlatovlasý
<g/>
,	,	kIx,	,
stříbrno	stříbrno	k1gNnSc4	stříbrno
odění	odění	k1gNnSc2	odění
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
obutí	obutý	k2eAgMnPc1d1	obutý
apoštolové	apoštol	k1gMnPc1	apoštol
svatý	svatý	k1gMnSc1	svatý
Petr	Petr	k1gMnSc1	Petr
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
knihou	kniha	k1gFnSc7	kniha
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
před	před	k7c7	před
sebou	se	k3xPyFc7	se
a	a	k8xC	a
v	v	k7c6	v
levici	levice	k1gFnSc6	levice
zlatý	zlatý	k1gInSc1	zlatý
perlou	perla	k1gFnSc7	perla
zdobený	zdobený	k2eAgInSc1d1	zdobený
klíč	klíč	k1gInSc1	klíč
a	a	k8xC	a
svatý	svatý	k2eAgMnSc1d1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
v	v	k7c6	v
pravici	pravice	k1gFnSc6	pravice
se	s	k7c7	s
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
skloněným	skloněný	k2eAgInSc7d1	skloněný
mečem	meč	k1gInSc7	meč
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
rukovětí	rukověť	k1gFnSc7	rukověť
a	a	k8xC	a
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
knihou	kniha	k1gFnSc7	kniha
v	v	k7c6	v
levici	levice	k1gFnSc6	levice
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
převýšené	převýšený	k2eAgNnSc1d1	převýšené
třemi	tři	k4xCgFnPc7	tři
stříbrnými	stříbrný	k2eAgFnPc7d1	stříbrná
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pečeť	pečetit	k5eAaImRp2nS	pečetit
obce	obec	k1gFnSc2	obec
===	===	k?	===
</s>
</p>
<p>
<s>
Pečeť	pečeť	k1gFnSc1	pečeť
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
s	s	k7c7	s
obecním	obecní	k2eAgInSc7d1	obecní
symbolem	symbol	k1gInSc7	symbol
s	s	k7c7	s
kruhopisem	kruhopis	k1gInSc7	kruhopis
OBEC	obec	k1gFnSc1	obec
BARDOŇOVO	BARDOŇOVO	kA	BARDOŇOVO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
obce	obec	k1gFnSc2	obec
===	===	k?	===
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
podélných	podélný	k2eAgInPc2d1	podélný
pruhů	pruh	k1gInPc2	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
žluté	žlutý	k2eAgFnSc2d1	žlutá
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnSc2d1	červená
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
a	a	k8xC	a
žluté	žlutý	k2eAgInPc1d1	žlutý
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc1	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
ukončená	ukončená	k1gFnSc1	ukončená
je	být	k5eAaImIp3nS	být
třemi	tři	k4xCgInPc7	tři
cípy	cíp	k1gInPc7	cíp
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
dvěma	dva	k4xCgInPc7	dva
zástřihy	zástřih	k1gInPc7	zástřih
<g/>
,	,	kIx,	,
sahajícími	sahající	k2eAgInPc7d1	sahající
do	do	k7c2	do
třetiny	třetina	k1gFnSc2	třetina
jejího	její	k3xOp3gInSc2	její
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
a	a	k8xC	a
osobnosti	osobnost	k1gFnPc1	osobnost
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Kaštel	kaštel	k1gInSc1	kaštel
s	s	k7c7	s
parkem	park	k1gInSc7	park
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
apoštola	apoštol	k1gMnSc2	apoštol
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
reformované	reformovaný	k2eAgFnSc2d1	reformovaná
církveIzidor	církveIzidor	k1gMnSc1	církveIzidor
Mihályk	Mihályk	k1gMnSc1	Mihályk
-	-	kIx~	-
zakladatel	zakladatel	k1gMnSc1	zakladatel
celostátního	celostátní	k2eAgInSc2d1	celostátní
samovzdělávacího	samovzdělávací	k2eAgInSc2d1	samovzdělávací
a	a	k8xC	a
důchodového	důchodový	k2eAgInSc2d1	důchodový
spolku	spolek	k1gInSc2	spolek
pro	pro	k7c4	pro
nevidící	vidící	k2eNgNnSc4d1	nevidící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bardoňovo	Bardoňův	k2eAgNnSc1d1	Bardoňův
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
obce	obec	k1gFnSc2	obec
</s>
</p>
