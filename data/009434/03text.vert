<p>
<s>
Néstor	Néstor	k1gMnSc1	Néstor
Rolando	Rolanda	k1gFnSc5	Rolanda
Clausen	Clausen	k2eAgMnSc1d1	Clausen
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Arrufo	Arrufo	k6eAd1	Arrufo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
argentinský	argentinský	k2eAgMnSc1d1	argentinský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
postu	post	k1gInSc6	post
obránce	obránce	k1gMnSc2	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
argentinskou	argentinský	k2eAgFnSc7d1	Argentinská
reprezentací	reprezentace	k1gFnSc7	reprezentace
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
..	..	k?	..
V	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
mužstvu	mužstvo	k1gNnSc6	mužstvo
odehrál	odehrát	k5eAaPmAgMnS	odehrát
26	[number]	k4	26
utkání	utkání	k1gNnPc4	utkání
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
klubem	klub	k1gInSc7	klub
CA	ca	kA	ca
Independiente	Independient	k1gInSc5	Independient
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Interkontinentální	interkontinentální	k2eAgInSc4d1	interkontinentální
pohár	pohár	k1gInSc4	pohár
1984	[number]	k4	1984
a	a	k8xC	a
Pohár	pohár	k1gInSc1	pohár
osvoboditelů	osvoboditel	k1gMnPc2	osvoboditel
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
Independiente	Independient	k1gInSc5	Independient
získal	získat	k5eAaPmAgMnS	získat
též	též	k9	též
dva	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
FC	FC	kA	FC
Sion	Sion	k1gInSc4	Sion
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
<g/>
)	)	kIx)	)
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
fotbalovým	fotbalový	k2eAgMnSc7d1	fotbalový
trenérem	trenér	k1gMnSc7	trenér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
