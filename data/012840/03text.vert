<p>
<s>
Farmacie	farmacie	k1gFnSc1	farmacie
(	(	kIx(	(
<g/>
též	též	k9	též
lékárnictví	lékárnictví	k1gNnSc2	lékárnictví
<g/>
;	;	kIx,	;
řec.	řec.	k?	řec.
"	"	kIx"	"
<g/>
farmakon	farmakon	k1gMnSc1	farmakon
<g/>
"	"	kIx"	"
=	=	kIx~	=
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zdravotnické	zdravotnický	k2eAgNnSc4d1	zdravotnické
odvětví	odvětví	k1gNnSc4	odvětví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
léčiv	léčivo	k1gNnPc2	léčivo
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jejich	jejich	k3xOp3gInSc4	jejich
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc4	distribuce
<g/>
,	,	kIx,	,
skladování	skladování	k1gNnSc4	skladování
a	a	k8xC	a
výdej	výdej	k1gInSc4	výdej
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
lékárnách	lékárna	k1gFnPc6	lékárna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Farmaceutické	farmaceutický	k2eAgFnPc1d1	farmaceutická
disciplíny	disciplína	k1gFnPc1	disciplína
==	==	k?	==
</s>
</p>
<p>
<s>
Farmakologie	farmakologie	k1gFnSc1	farmakologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
účinkem	účinek	k1gInSc7	účinek
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
osudem	osud	k1gInSc7	osud
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Farmakognozie	Farmakognozie	k1gFnSc1	Farmakognozie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
léčivy	léčivo	k1gNnPc7	léčivo
přírodního	přírodní	k2eAgInSc2d1	přírodní
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
chemie	chemie	k1gFnSc1	chemie
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
vztah	vztah	k1gInSc4	vztah
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
účinku	účinek	k1gInSc2	účinek
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
též	též	k9	též
syntézou	syntéza	k1gFnSc7	syntéza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
kvality	kvalita	k1gFnSc2	kvalita
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
metod	metoda	k1gFnPc2	metoda
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
technologie	technologie	k1gFnSc1	technologie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
a	a	k8xC	a
přípravou	příprava	k1gFnSc7	příprava
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
farmacie	farmacie	k1gFnSc1	farmacie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
fungováním	fungování	k1gNnSc7	fungování
farmacie	farmacie	k1gFnSc2	farmacie
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
postavením	postavení	k1gNnSc7	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lékárenství	lékárenství	k1gNnSc1	lékárenství
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
fungování	fungování	k1gNnSc2	fungování
lékáren	lékárna	k1gFnPc2	lékárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Farmaceutické	farmaceutický	k2eAgNnSc1d1	farmaceutické
školství	školství	k1gNnSc1	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Farmaceuti	farmaceut	k1gMnPc1	farmaceut
(	(	kIx(	(
<g/>
lékárníci	lékárník	k1gMnPc1	lékárník
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
fakulta	fakulta	k1gFnSc1	fakulta
Veterinární	veterinární	k2eAgFnSc2d1	veterinární
a	a	k8xC	a
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
BrněStudium	BrněStudium	k1gNnSc4	BrněStudium
je	být	k5eAaImIp3nS	být
pětileté	pětiletý	k2eAgNnSc1d1	pětileté
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
absolvování	absolvování	k1gNnSc6	absolvování
získávají	získávat	k5eAaImIp3nP	získávat
farmaceuti	farmaceut	k1gMnPc1	farmaceut
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
kvalifikováni	kvalifikovat	k5eAaBmNgMnP	kvalifikovat
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
činností	činnost	k1gFnPc2	činnost
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
farmaceutických	farmaceutický	k2eAgNnPc6d1	farmaceutické
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rigorózním	rigorózní	k2eAgNnSc6d1	rigorózní
řízení	řízení	k1gNnSc6	řízení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
též	též	k9	též
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
PharmDr.	PharmDr.	k1gFnSc1	PharmDr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
farmaceutické	farmaceutický	k2eAgMnPc4d1	farmaceutický
pracovníky	pracovník	k1gMnPc4	pracovník
patří	patřit	k5eAaImIp3nP	patřit
též	též	k9	též
farmaceutičtí	farmaceutický	k2eAgMnPc1d1	farmaceutický
asistenti	asistent	k1gMnPc1	asistent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
získávají	získávat	k5eAaImIp3nP	získávat
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
odborných	odborný	k2eAgFnPc6d1	odborná
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
titul	titul	k1gInSc4	titul
DiS.	DiS.	k1gFnSc2	DiS.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
farmaceutičtí	farmaceutický	k2eAgMnPc1d1	farmaceutický
laboranti	laborant	k1gMnPc1	laborant
a	a	k8xC	a
získávali	získávat	k5eAaImAgMnP	získávat
vzdělání	vzdělání	k1gNnSc4	vzdělání
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
zdravotních	zdravotní	k2eAgFnPc6d1	zdravotní
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Farmaceut	farmaceut	k1gMnSc1	farmaceut
</s>
</p>
<p>
<s>
Farmakologie	farmakologie	k1gFnSc1	farmakologie
</s>
</p>
<p>
<s>
Magistr	magistr	k1gMnSc1	magistr
farmacie	farmacie	k1gFnSc2	farmacie
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
farmacie	farmacie	k1gFnSc2	farmacie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
farmacie	farmacie	k1gFnSc2	farmacie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Lékárnictví	lékárnictví	k1gNnSc2	lékárnictví
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
