<s>
Farmacie	farmacie	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
lékárnictví	lékárnictví	k1gNnSc2
<g/>
;	;	kIx,
řec.	řec.	k?
"	"	kIx"
<g/>
farmakon	farmakon	k1gMnSc1
<g/>
"	"	kIx"
=	=	kIx~
léčivo	léčivo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zdravotnické	zdravotnický	k2eAgNnSc4d1
odvětví	odvětví	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
zabezpečení	zabezpečení	k1gNnSc3
léčiv	léčivo	k1gNnPc2
pro	pro	k7c4
pacienty	pacient	k1gMnPc4
<g/>
.	.	kIx.
</s>