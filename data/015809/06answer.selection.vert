<s desamb="1">
Byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mnoha	mnoho	k4c2
německých	německý	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
dílo	dílo	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
odsouzeno	odsoudit	k5eAaPmNgNnS
jako	jako	k9
zvrhlé	zvrhlý	k2eAgNnSc4d1
umění	umění	k1gNnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
vystaveno	vystavit	k5eAaPmNgNnS
na	na	k7c6
výstavě	výstava	k1gFnSc6
Entartete	Entartete	k1gMnSc1
Kunst	Kunst	k1gMnSc1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>