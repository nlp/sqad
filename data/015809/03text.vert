<s>
Hans	Hans	k1gMnSc1
Jürgen	Jürgen	k1gInSc4
Kallmann	Kallmann	k1gInSc1
</s>
<s>
Hans	Hans	k1gMnSc1
Jürgen	Jürgen	k1gInSc4
Kallmann	Kallmann	k1gInSc4
Narození	narození	k1gNnPc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1908	#num#	k4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
Povolání	povolání	k1gNnPc2
</s>
<s>
malíř	malíř	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hans	Hans	k1gMnSc1
Jürgen	Jürgen	k1gInSc4
Kallmann	Kallmann	k1gInSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1908	#num#	k4
<g/>
,	,	kIx,
Wollstein	Wollstein	k1gInSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Wolsztyn	Wolsztyn	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Pullach	Pullach	k1gMnSc1
im	im	k?
Isartal	Isartal	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mnoha	mnoho	k4c2
německých	německý	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
dílo	dílo	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
odsouzeno	odsoudit	k5eAaPmNgNnS
jako	jako	k9
zvrhlé	zvrhlý	k2eAgNnSc4d1
umění	umění	k1gNnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
vystaveno	vystavit	k5eAaPmNgNnS
na	na	k7c6
výstavě	výstava	k1gFnSc6
Entartete	Entartete	k1gMnSc1
Kunst	Kunst	k1gMnSc1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
Bundesverdienstkreuz	Bundesverdienstkreuz	k1gInSc1
<g/>
,	,	kIx,
Vyznamenání	vyznamenání	k1gNnPc1
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
Spolkové	spolkový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Německo	Německo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://rektorenportraits.uni-koeln.de/kuenstler_innen/hans_juergen_kallmann/	http://rektorenportraits.uni-koeln.de/kuenstler_innen/hans_juergen_kallmann/	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Umění	umění	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118559583	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1041	#num#	k4
2816	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2006054247	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500149121	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
16653132	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2006054247	#num#	k4
</s>
