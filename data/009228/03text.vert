<p>
<s>
David	David	k1gMnSc1	David
Don	Don	k1gMnSc1	Don
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
Doo	Doo	k1gMnSc1	Doo
Hillock	Hillock	k1gMnSc1	Hillock
<g/>
,	,	kIx,	,
Forfarshire	Forfarshir	k1gMnSc5	Forfarshir
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
botanik	botanik	k1gMnSc1	botanik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Don	Don	k1gMnSc1	Don
byl	být	k5eAaImAgMnS	být
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
botanika	botanik	k1gMnSc4	botanik
George	Georg	k1gMnSc4	Georg
Dona	Don	k1gMnSc4	Don
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1822	[number]	k4	1822
až	až	k9	až
1841	[number]	k4	1841
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
knihovník	knihovník	k1gMnSc1	knihovník
Linnean	Linnean	k1gMnSc1	Linnean
Society	societa	k1gFnSc2	societa
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1836	[number]	k4	1836
až	až	k9	až
1841	[number]	k4	1841
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
botaniky	botanika	k1gFnSc2	botanika
na	na	k7c4	na
King	King	k1gInSc4	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Don	Don	k1gMnSc1	Don
popsal	popsat	k5eAaPmAgMnS	popsat
několik	několik	k4yIc4	několik
významných	významný	k2eAgInPc2d1	významný
druhů	druh	k1gInPc2	druh
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
sekvoj	sekvoj	k1gInSc1	sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
<g/>
,	,	kIx,	,
Abies	Abies	k1gInSc1	Abies
bracteata	bracteata	k1gFnSc1	bracteata
nebo	nebo	k8xC	nebo
kryptomerie	kryptomerie	k1gFnSc1	kryptomerie
japonská	japonský	k2eAgFnSc1d1	japonská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
popsal	popsat	k5eAaPmAgMnS	popsat
tibetské	tibetský	k2eAgFnPc4d1	tibetská
orchideje	orchidea	k1gFnPc4	orchidea
Pleione	Pleion	k1gInSc5	Pleion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
knihovník	knihovník	k1gMnSc1	knihovník
a	a	k8xC	a
tajemník	tajemník	k1gMnSc1	tajemník
botanika	botanik	k1gMnSc2	botanik
Lamberta	Lambert	k1gMnSc2	Lambert
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Prodromus	prodromus	k1gInSc4	prodromus
florae	flora	k1gMnSc2	flora
nepalensis	nepalensis	k1gFnSc2	nepalensis
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
sbírkách	sbírka	k1gFnPc6	sbírka
Hamiltona	Hamilton	k1gMnSc2	Hamilton
a	a	k8xC	a
Wallicha	Wallich	k1gMnSc2	Wallich
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
Lambertovu	Lambertův	k2eAgNnSc3d1	Lambertovo
A	a	k8xC	a
description	description	k1gInSc1	description
of	of	k?	of
the	the	k?	the
genus	genus	k1gMnSc1	genus
Pinus	Pinus	k1gMnSc1	Pinus
doplnil	doplnit	k5eAaPmAgMnS	doplnit
popisky	popisek	k1gInPc4	popisek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Prodromus	prodromus	k1gInSc1	prodromus
florae	flora	k1gFnSc2	flora
nepalensis	nepalensis	k1gFnSc2	nepalensis
<g/>
,	,	kIx,	,
1825	[number]	k4	1825
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
David	David	k1gMnSc1	David
Don	Don	k1gMnSc1	Don
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
<g/>
Robert	Robert	k1gMnSc1	Robert
Zander	Zander	k1gMnSc1	Zander
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Encke	Encke	k1gFnPc2	Encke
<g/>
,	,	kIx,	,
Günther	Günthra	k1gFnPc2	Günthra
Buchheim	Buchheima	k1gFnPc2	Buchheima
<g/>
,	,	kIx,	,
Siegmund	Siegmund	k1gMnSc1	Siegmund
Seybold	Seybold	k1gMnSc1	Seybold
<g/>
:	:	kIx,	:
Handwörterbuch	Handwörterbuch	k1gMnSc1	Handwörterbuch
der	drát	k5eAaImRp2nS	drát
Pflanzennamen	Pflanzennamen	k1gInSc1	Pflanzennamen
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Ulmer	Ulmer	k1gMnSc1	Ulmer
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8001	[number]	k4	8001
<g/>
-	-	kIx~	-
<g/>
5042	[number]	k4	5042
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
