<s>
Ozon	ozon	k1gInSc1	ozon
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ο	ο	k?	ο
<g/>
,	,	kIx,	,
ozó	ozó	k?	ozó
<g/>
,	,	kIx,	,
voním	vonět	k5eAaImIp1nS	vonět
<g/>
)	)	kIx)	)
racionální	racionální	k2eAgInSc4d1	racionální
chemický	chemický	k2eAgInSc4d1	chemický
název	název	k1gInSc4	název
trikyslík	trikyslík	k1gMnSc1	trikyslík
je	být	k5eAaImIp3nS	být
alotropní	alotropní	k2eAgFnPc4d1	alotropní
modifikace	modifikace	k1gFnPc4	modifikace
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
obvyklých	obvyklý	k2eAgFnPc2d1	obvyklá
dvouatomových	dvouatomový	k2eAgFnPc2d1	dvouatomová
molekul	molekula	k1gFnPc2	molekula
O2	O2	k1gFnPc2	O2
se	se	k3xPyFc4	se
kyslík	kyslík	k1gInSc1	kyslík
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tříatomové	tříatomový	k2eAgFnSc2d1	tříatomová
molekuly	molekula	k1gFnSc2	molekula
jako	jako	k8xC	jako
ozon	ozon	k1gInSc1	ozon
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vysoce	vysoce	k6eAd1	vysoce
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
plyn	plyn	k1gInSc1	plyn
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
zápachu	zápach	k1gInSc2	zápach
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
silnými	silný	k2eAgInPc7d1	silný
oxidačními	oxidační	k2eAgInPc7d1	oxidační
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
-112	-112	k4	-112
°	°	k?	°
<g/>
C	C	kA	C
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
na	na	k7c4	na
kapalný	kapalný	k2eAgInSc4d1	kapalný
tmavě	tmavě	k6eAd1	tmavě
modrý	modrý	k2eAgInSc4d1	modrý
ozon	ozon	k1gInSc4	ozon
a	a	k8xC	a
při	při	k7c6	při
-193	-193	k4	-193
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
červenofialový	červenofialový	k2eAgInSc1d1	červenofialový
pevný	pevný	k2eAgInSc1d1	pevný
ozon	ozon	k1gInSc1	ozon
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
ozonu	ozon	k1gInSc2	ozon
sestávají	sestávat	k5eAaImIp3nP	sestávat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
atomů	atom	k1gInPc2	atom
kyslíku	kyslík	k1gInSc2	kyslík
namísto	namísto	k7c2	namísto
dvou	dva	k4xCgFnPc2	dva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
molekuly	molekula	k1gFnPc4	molekula
stabilního	stabilní	k2eAgInSc2d1	stabilní
běžného	běžný	k2eAgInSc2d1	běžný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
ozonu	ozon	k1gInSc2	ozon
je	být	k5eAaImIp3nS	být
lomená	lomený	k2eAgFnSc1d1	lomená
a	a	k8xC	a
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
svírají	svírat	k5eAaImIp3nP	svírat
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
116,8	[number]	k4	116,8
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
mesomerní	mesomerní	k2eAgInPc1d1	mesomerní
strukturní	strukturní	k2eAgInPc1d1	strukturní
vzorce	vzorec	k1gInPc1	vzorec
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
infobox	infobox	k1gInSc1	infobox
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
představující	představující	k2eAgFnPc1d1	představující
mezní	mezní	k2eAgFnPc4d1	mezní
elektronové	elektronový	k2eAgFnPc4d1	elektronová
konfigurace	konfigurace	k1gFnPc4	konfigurace
této	tento	k3xDgFnSc2	tento
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
prostředním	prostřední	k2eAgInSc6d1	prostřední
atomu	atom	k1gInSc6	atom
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
krajních	krajní	k2eAgNnPc2d1	krajní
je	být	k5eAaImIp3nS	být
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
poloviční	poloviční	k2eAgFnSc2d1	poloviční
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
a	a	k8xC	a
svému	svůj	k3xOyFgInSc3	svůj
lomenému	lomený	k2eAgInSc3d1	lomený
tvaru	tvar	k1gInSc3	tvar
má	mít	k5eAaImIp3nS	mít
molekula	molekula	k1gFnSc1	molekula
značný	značný	k2eAgInSc4d1	značný
dipólový	dipólový	k2eAgInSc4d1	dipólový
moment	moment	k1gInSc4	moment
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
dipólového	dipólový	k2eAgInSc2d1	dipólový
momentu	moment	k1gInSc2	moment
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
zesílení	zesílení	k1gNnSc3	zesílení
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Waalsových	Waalsová	k1gFnPc6	Waalsová
mezimolekulových	mezimolekulův	k2eAgFnPc2d1	mezimolekulův
přitažlivých	přitažlivý	k2eAgFnPc2d1	přitažlivá
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hmotností	hmotnost	k1gFnSc7	hmotnost
molekuly	molekula	k1gFnPc4	molekula
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
těkavosti	těkavost	k1gFnSc2	těkavost
ozonu	ozon	k1gInSc2	ozon
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Ozon	ozon	k1gInSc1	ozon
vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
elektrických	elektrický	k2eAgInPc2d1	elektrický
výbojů	výboj	k1gInPc2	výboj
nebo	nebo	k8xC	nebo
krátkovlnného	krátkovlnný	k2eAgNnSc2d1	krátkovlnné
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
UV-C	UV-C	k1gMnSc2	UV-C
<g/>
)	)	kIx)	)
na	na	k7c4	na
molekuly	molekula	k1gFnPc4	molekula
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
dodaná	dodaná	k1gFnSc1	dodaná
energie	energie	k1gFnSc2	energie
rozštěpí	rozštěpit	k5eAaPmIp3nS	rozštěpit
dvouatomovou	dvouatomový	k2eAgFnSc4d1	dvouatomová
molekulu	molekula	k1gFnSc4	molekula
dikyslíku	dikyslík	k1gInSc2	dikyslík
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
atomy	atom	k1gInPc4	atom
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
vysoce	vysoce	k6eAd1	vysoce
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
jednoatomové	jednoatomový	k2eAgInPc4d1	jednoatomový
radikály	radikál	k1gInPc4	radikál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
spojí	spojit	k5eAaPmIp3nP	spojit
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
molekulou	molekula	k1gFnSc7	molekula
dikyslíku	dikyslík	k1gInSc2	dikyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ozonu	ozon	k1gInSc2	ozon
<g/>
:	:	kIx,	:
O2	O2	k1gFnSc1	O2
+	+	kIx~	+
hν	hν	k?	hν
→	→	k?	→
2	[number]	k4	2
O	O	kA	O
<g/>
,	,	kIx,	,
O2	O2	k1gFnSc1	O2
+	+	kIx~	+
O	o	k7c6	o
→	→	k?	→
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
normální	normální	k2eAgFnSc6d1	normální
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
ozon	ozon	k1gInSc1	ozon
namodralý	namodralý	k2eAgInSc1d1	namodralý
plyn	plyn	k1gInSc4	plyn
s	s	k7c7	s
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
pachem	pach	k1gInSc7	pach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
člověk	člověk	k1gMnSc1	člověk
registruje	registrovat	k5eAaBmIp3nS	registrovat
při	při	k7c6	při
koncentraci	koncentrace	k1gFnSc6	koncentrace
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
0,1	[number]	k4	0,1
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
citlivost	citlivost	k1gFnSc1	citlivost
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
jedince	jedinec	k1gMnSc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ochlazování	ochlazování	k1gNnSc6	ochlazování
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
tmavě	tmavě	k6eAd1	tmavě
modrou	modrý	k2eAgFnSc4d1	modrá
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
v	v	k7c4	v
tmavě	tmavě	k6eAd1	tmavě
modrou	modrý	k2eAgFnSc4d1	modrá
pevnou	pevný	k2eAgFnSc4d1	pevná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ozon	ozon	k1gInSc1	ozon
je	být	k5eAaImIp3nS	být
silné	silný	k2eAgNnSc4d1	silné
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
reakcí	reakce	k1gFnSc7	reakce
2O3	[number]	k4	2O3
→	→	k?	→
3O2	[number]	k4	3O2
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
se	s	k7c7	s
stoupající	stoupající	k2eAgFnSc7d1	stoupající
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
stoupajícím	stoupající	k2eAgInSc7d1	stoupající
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Přeměnu	přeměna	k1gFnSc4	přeměna
ozonu	ozon	k1gInSc2	ozon
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
a	a	k8xC	a
radikály	radikál	k1gInPc1	radikál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
atomy	atom	k1gInPc1	atom
fluoru	fluor	k1gInSc2	fluor
a	a	k8xC	a
chloru	chlor	k1gInSc2	chlor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
heslo	heslo	k1gNnSc4	heslo
ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
koncentracích	koncentrace	k1gFnPc6	koncentrace
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tvorbu	tvorba	k1gFnSc4	tvorba
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
živočichy	živočich	k1gMnPc4	živočich
karcinogenní	karcinogenní	k2eAgNnSc1d1	karcinogenní
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
U	u	k7c2	u
řady	řada	k1gFnSc2	řada
druhů	druh	k1gMnPc2	druh
bakterií	bakterie	k1gFnPc2	bakterie
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
i	i	k8xC	i
mutagenicita	mutagenicita	k1gFnSc1	mutagenicita
ozonu	ozon	k1gInSc2	ozon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
ozon	ozon	k1gInSc4	ozon
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Opakem	opak	k1gInSc7	opak
životu	život	k1gInSc2	život
prospěšného	prospěšný	k2eAgInSc2d1	prospěšný
ozonu	ozon	k1gInSc2	ozon
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
přízemní	přízemní	k2eAgMnSc1d1	přízemní
neboli	neboli	k8xC	neboli
troposférický	troposférický	k2eAgInSc1d1	troposférický
ozon	ozon	k1gInSc1	ozon
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
lidskému	lidský	k2eAgNnSc3d1	lidské
zdraví	zdraví	k1gNnSc3	zdraví
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
dráždění	dráždění	k1gNnPc4	dráždění
a	a	k8xC	a
nemoci	nemoc	k1gFnPc4	nemoc
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
astmatických	astmatický	k2eAgInPc2d1	astmatický
záchvatů	záchvat	k1gInPc2	záchvat
<g/>
,	,	kIx,	,
podráždění	podráždění	k1gNnSc3	podráždění
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ozon	ozon	k1gInSc1	ozon
je	být	k5eAaImIp3nS	být
minoritní	minoritní	k2eAgFnSc7d1	minoritní
složkou	složka	k1gFnSc7	složka
nízké	nízký	k2eAgFnSc2d1	nízká
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
fotochemického	fotochemický	k2eAgInSc2d1	fotochemický
smogu	smog	k1gInSc2	smog
<g/>
.	.	kIx.	.
</s>
<s>
Troposférický	troposférický	k2eAgInSc1d1	troposférický
ozon	ozon	k1gInSc1	ozon
vzniká	vznikat	k5eAaImIp3nS	vznikat
složitými	složitý	k2eAgFnPc7d1	složitá
chemickými	chemický	k2eAgFnPc7d1	chemická
reakcemi	reakce	k1gFnPc7	reakce
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
s	s	k7c7	s
těkavými	těkavý	k2eAgFnPc7d1	těkavá
organickými	organický	k2eAgFnPc7d1	organická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
za	za	k7c2	za
horkých	horký	k2eAgInPc2d1	horký
letních	letní	k2eAgInPc2d1	letní
dnů	den	k1gInPc2	den
a	a	k8xC	a
bezvětří	bezvětří	k1gNnSc2	bezvětří
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Evropské	evropský	k2eAgFnSc2d1	Evropská
agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
nadlimitním	nadlimitní	k2eAgFnPc3d1	nadlimitní
koncentracím	koncentrace	k1gFnPc3	koncentrace
ozonu	ozon	k1gInSc2	ozon
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
6	[number]	k4	6
z	z	k7c2	z
10	[number]	k4	10
obyvatel	obyvatel	k1gMnPc2	obyvatel
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
oblastech	oblast	k1gFnPc6	oblast
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
koncentrace	koncentrace	k1gFnSc2	koncentrace
překračují	překračovat	k5eAaImIp3nP	překračovat
kritické	kritický	k2eAgFnPc1d1	kritická
hodnoty	hodnota	k1gFnPc1	hodnota
pro	pro	k7c4	pro
negativní	negativní	k2eAgInSc4d1	negativní
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
lesy	les	k1gInPc4	les
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
plodiny	plodina	k1gFnPc4	plodina
(	(	kIx(	(
<g/>
odplavuje	odplavovat	k5eAaImIp3nS	odplavovat
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
z	z	k7c2	z
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
vznik	vznik	k1gInSc4	vznik
přízemního	přízemní	k2eAgInSc2d1	přízemní
ozonu	ozon	k1gInSc2	ozon
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
především	především	k9	především
za	za	k7c2	za
slunečných	slunečný	k2eAgInPc2d1	slunečný
horkých	horký	k2eAgInPc2d1	horký
letních	letní	k2eAgInPc2d1	letní
dnů	den	k1gInPc2	den
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
-	-	kIx~	-
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
těkavých	těkavý	k2eAgFnPc2d1	těkavá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	s	k7c7	s
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
suchý	suchý	k2eAgInSc4d1	suchý
smog	smog	k1gInSc4	smog
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
fotochemický	fotochemický	k2eAgInSc4d1	fotochemický
smog	smog	k1gInSc4	smog
nebo	nebo	k8xC	nebo
losangelský	losangelský	k2eAgInSc4d1	losangelský
smog	smog	k1gInSc4	smog
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
častého	častý	k2eAgInSc2d1	častý
výskytu	výskyt	k1gInSc2	výskyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ozon	ozon	k1gInSc1	ozon
také	také	k9	také
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
požárech	požár	k1gInPc6	požár
či	či	k8xC	či
spalování	spalování	k1gNnSc1	spalování
biomasy	biomasa	k1gFnSc2	biomasa
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
přenášet	přenášet	k5eAaImF	přenášet
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
roli	role	k1gFnSc4	role
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Evropské	evropský	k2eAgFnSc2d1	Evropská
agentury	agentura	k1gFnSc2	agentura
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
nadlimitním	nadlimitní	k2eAgFnPc3d1	nadlimitní
koncentracím	koncentrace	k1gFnPc3	koncentrace
ozonu	ozon	k1gInSc2	ozon
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
6	[number]	k4	6
z	z	k7c2	z
10	[number]	k4	10
obyvatel	obyvatel	k1gMnPc2	obyvatel
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
oblastech	oblast	k1gFnPc6	oblast
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
koncentrace	koncentrace	k1gFnSc2	koncentrace
překračují	překračovat	k5eAaImIp3nP	překračovat
kritické	kritický	k2eAgFnPc1d1	kritická
hodnoty	hodnota	k1gFnPc1	hodnota
pro	pro	k7c4	pro
negativní	negativní	k2eAgInSc4d1	negativní
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
lesy	les	k1gInPc4	les
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
100	[number]	k4	100
let	léto	k1gNnPc2	léto
ztrojnásobila	ztrojnásobit	k5eAaPmAgFnS	ztrojnásobit
a	a	k8xC	a
tak	tak	k6eAd1	tak
patří	patřit	k5eAaImIp3nS	patřit
společně	společně	k6eAd1	společně
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
k	k	k7c3	k
nejdůležitějším	důležitý	k2eAgMnPc3d3	nejdůležitější
přispěvatelům	přispěvatel	k1gMnPc3	přispěvatel
k	k	k7c3	k
antropogennímu	antropogenní	k2eAgNnSc3d1	antropogenní
zesílení	zesílení	k1gNnSc3	zesílení
globálního	globální	k2eAgInSc2d1	globální
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
2	[number]	k4	2
až	až	k9	až
8	[number]	k4	8
ppm	ppm	k?	ppm
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
mezi	mezi	k7c7	mezi
10	[number]	k4	10
až	až	k9	až
50	[number]	k4	50
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
většinu	většina	k1gFnSc4	většina
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
UV-B	UV-B	k1gMnSc2	UV-B
<g/>
)	)	kIx)	)
přicházejícího	přicházející	k2eAgMnSc2d1	přicházející
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
veškerý	veškerý	k3xTgInSc1	veškerý
ozon	ozon	k1gInSc1	ozon
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
stlačil	stlačit	k5eAaPmAgMnS	stlačit
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
cca	cca	kA	cca
1000	[number]	k4	1000
hPa	hPa	k?	hPa
(	(	kIx(	(
<g/>
1	[number]	k4	1
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
by	by	kYmCp3nP	by
vrstvu	vrstva	k1gFnSc4	vrstva
tenkou	tenký	k2eAgFnSc4d1	tenká
3,5	[number]	k4	3,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
pozemský	pozemský	k2eAgInSc4d1	pozemský
život	život	k1gInSc4	život
hraje	hrát	k5eAaImIp3nS	hrát
ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
planetu	planeta	k1gFnSc4	planeta
před	před	k7c7	před
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
slunečním	sluneční	k2eAgNnSc7d1	sluneční
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
část	část	k1gFnSc1	část
stratosféry	stratosféra	k1gFnSc2	stratosféra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
25	[number]	k4	25
-	-	kIx~	-
35	[number]	k4	35
km	km	kA	km
nad	nad	k7c7	nad
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
značně	značně	k6eAd1	značně
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
poměr	poměr	k1gInSc1	poměr
ozonu	ozon	k1gInSc2	ozon
vůči	vůči	k7c3	vůči
běžnému	běžný	k2eAgInSc3d1	běžný
dvouatomovému	dvouatomový	k2eAgInSc3d1	dvouatomový
kyslíku	kyslík	k1gInSc3	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
ozon	ozon	k1gInSc1	ozon
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
teplokrevných	teplokrevný	k2eAgMnPc2d1	teplokrevný
živočichů	živočich	k1gMnPc2	živočich
produkován	produkovat	k5eAaImNgInS	produkovat
v	v	k7c6	v
bílých	bílý	k2eAgFnPc6d1	bílá
krvinkách	krvinka	k1gFnPc6	krvinka
a	a	k8xC	a
uvolňován	uvolňovat	k5eAaImNgInS	uvolňovat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
likvidaci	likvidace	k1gFnSc6	likvidace
choroboplodných	choroboplodný	k2eAgInPc2d1	choroboplodný
zárodků	zárodek	k1gInPc2	zárodek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ozon	ozon	k1gInSc1	ozon
jako	jako	k8xC	jako
silné	silný	k2eAgNnSc1d1	silné
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
škálou	škála	k1gFnSc7	škála
biologicky	biologicky	k6eAd1	biologicky
významných	významný	k2eAgFnPc2d1	významná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vznik	vznik	k1gInSc4	vznik
peroxidů	peroxid	k1gInPc2	peroxid
polynenasycených	polynenasycený	k2eAgFnPc2d1	polynenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
koenzymů	koenzym	k1gInPc2	koenzym
atd.	atd.	kA	atd.
Proto	proto	k8xC	proto
působí	působit	k5eAaImIp3nS	působit
negativně	negativně	k6eAd1	negativně
na	na	k7c4	na
buněčné	buněčný	k2eAgFnPc4d1	buněčná
membrány	membrána	k1gFnPc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Vdechování	vdechování	k1gNnSc1	vdechování
ozonu	ozon	k1gInSc2	ozon
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
pokles	pokles	k1gInSc1	pokles
kapacity	kapacita	k1gFnSc2	kapacita
plic	plíce	k1gFnPc2	plíce
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
koncentraci	koncentrace	k1gFnSc6	koncentrace
a	a	k8xC	a
na	na	k7c6	na
hloubce	hloubka	k1gFnSc6	hloubka
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
vdechování	vdechování	k1gNnSc1	vdechování
ozonu	ozon	k1gInSc2	ozon
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
zánětlivých	zánětlivý	k2eAgInPc2d1	zánětlivý
onemocnění	onemocnění	k1gNnSc4	onemocnění
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
narušení	narušení	k1gNnSc1	narušení
vývoje	vývoj	k1gInSc2	vývoj
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
hospitalizací	hospitalizace	k1gFnPc2	hospitalizace
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
spotřeba	spotřeba	k1gFnSc1	spotřeba
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Poškození	poškození	k1gNnSc2	poškození
rostlin	rostlina	k1gFnPc2	rostlina
ozónem	ozón	k1gInSc7	ozón
<g/>
.	.	kIx.	.
</s>
<s>
Poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
negativní	negativní	k2eAgInPc1d1	negativní
účinky	účinek	k1gInPc1	účinek
emisí	emise	k1gFnPc2	emise
ozonu	ozon	k1gInSc2	ozon
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
u	u	k7c2	u
jasanu	jasan	k1gInSc2	jasan
<g/>
,	,	kIx,	,
buku	buk	k1gInSc2	buk
<g/>
,	,	kIx,	,
pajasanu	pajasan	k1gInSc2	pajasan
a	a	k8xC	a
liliovníku	liliovník	k1gInSc2	liliovník
<g/>
.	.	kIx.	.
</s>
<s>
Příznakem	příznak	k1gInSc7	příznak
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc4d1	žlutá
chlorotické	chlorotický	k2eAgFnPc4d1	chlorotická
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
drobné	drobný	k2eAgFnPc1d1	drobná
červené	červený	k2eAgFnPc1d1	červená
skvrnky	skvrnka	k1gFnPc1	skvrnka
<g/>
,	,	kIx,	,
bronzovité	bronzovitý	k2eAgNnSc1d1	bronzovitý
zbarvení	zbarvení	k1gNnSc1	zbarvení
horní	horní	k2eAgFnSc2d1	horní
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
žilky	žilka	k1gFnPc1	žilka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
laboratořích	laboratoř	k1gFnPc6	laboratoř
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xS	jako
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
a	a	k8xC	a
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
některých	některý	k3yIgFnPc2	některý
peroxidických	peroxidický	k2eAgFnPc2d1	peroxidický
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
k	k	k7c3	k
bělení	bělení	k1gNnSc3	bělení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
textilních	textilní	k2eAgFnPc2d1	textilní
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
dezinfekci	dezinfekce	k1gFnSc6	dezinfekce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ozonizace	ozonizace	k1gFnSc1	ozonizace
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
běžnější	běžný	k2eAgFnSc1d2	běžnější
a	a	k8xC	a
lacinější	laciný	k2eAgFnSc1d2	lacinější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdravotně	zdravotně	k6eAd1	zdravotně
méně	málo	k6eAd2	málo
výhodné	výhodný	k2eAgNnSc1d1	výhodné
chlorování	chlorování	k1gNnSc1	chlorování
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
provozoven	provozovna	k1gFnPc2	provozovna
a	a	k8xC	a
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
konzervaci	konzervace	k1gFnSc3	konzervace
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
v	v	k7c4	v
zemědělství	zemědělství	k1gNnSc4	zemědělství
k	k	k7c3	k
povrchovému	povrchový	k2eAgNnSc3d1	povrchové
ošetření	ošetření	k1gNnSc3	ošetření
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zabránění	zabránění	k1gNnSc1	zabránění
růstu	růst	k1gInSc2	růst
plísní	plíseň	k1gFnPc2	plíseň
a	a	k8xC	a
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
kapalného	kapalný	k2eAgInSc2d1	kapalný
ozonu	ozon	k1gInSc2	ozon
a	a	k8xC	a
90	[number]	k4	90
%	%	kIx~	%
kapalného	kapalný	k2eAgInSc2d1	kapalný
kyslíku	kyslík	k1gInSc2	kyslík
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
testována	testovat	k5eAaImNgFnS	testovat
jako	jako	k8xC	jako
okysličovadlo	okysličovadlo	k1gNnSc1	okysličovadlo
v	v	k7c6	v
raketových	raketový	k2eAgInPc6d1	raketový
kapalinových	kapalinový	k2eAgInPc6d1	kapalinový
motorech	motor	k1gInPc6	motor
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
specifického	specifický	k2eAgInSc2d1	specifický
impulsu	impuls	k1gInSc2	impuls
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Ozon	ozon	k1gInSc1	ozon
je	být	k5eAaImIp3nS	být
silné	silný	k2eAgNnSc4d1	silné
oxidační	oxidační	k2eAgNnSc4d1	oxidační
činidlo	činidlo	k1gNnSc4	činidlo
<g/>
..	..	k?	..
Jeho	jeho	k3xOp3gNnSc1	jeho
desinfekční	desinfekční	k2eAgFnPc1d1	desinfekční
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
vynikající	vynikající	k2eAgMnPc4d1	vynikající
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
mimořádně	mimořádně	k6eAd1	mimořádně
velkou	velký	k2eAgFnSc4d1	velká
mikrobiocidní	mikrobiocidní	k2eAgFnSc4d1	mikrobiocidní
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Ozon	ozon	k1gInSc1	ozon
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
živou	živý	k2eAgFnSc7d1	živá
tkání	tkáň	k1gFnSc7	tkáň
v	v	k7c6	v
přiměřených	přiměřený	k2eAgFnPc6d1	přiměřená
koncentracích	koncentrace	k1gFnPc6	koncentrace
tuto	tento	k3xDgFnSc4	tento
tkáň	tkáň	k1gFnSc4	tkáň
ozdravuje	ozdravovat	k5eAaImIp3nS	ozdravovat
<g/>
,	,	kIx,	,
prokrvuje	prokrvovat	k5eAaImIp3nS	prokrvovat
a	a	k8xC	a
také	také	k9	také
velmi	velmi	k6eAd1	velmi
důkladně	důkladně	k6eAd1	důkladně
dezinfikuje	dezinfikovat	k5eAaBmIp3nS	dezinfikovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
například	například	k6eAd1	například
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
akné	akné	k1gFnPc2	akné
<g/>
,	,	kIx,	,
atopických	atopický	k2eAgInPc2d1	atopický
ekzémů	ekzém	k1gInPc2	ekzém
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
kožních	kožní	k2eAgInPc2d1	kožní
defektů	defekt	k1gInPc2	defekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
ozonu	ozon	k1gInSc2	ozon
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
stomatologii	stomatologie	k1gFnSc6	stomatologie
v	v	k7c6	v
Ozonoterapii	Ozonoterapie	k1gFnSc6	Ozonoterapie
<g/>
.	.	kIx.	.
2H2O2	[number]	k4	2H2O2
→	→	k?	→
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
Poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
ozon	ozon	k1gInSc4	ozon
tichým	tichý	k2eAgInSc7d1	tichý
elektrickým	elektrický	k2eAgInSc7d1	elektrický
výbojem	výboj	k1gInSc7	výboj
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
směs	směs	k1gFnSc1	směs
kyslíku	kyslík	k1gInSc2	kyslík
s	s	k7c7	s
ozonem	ozon	k1gInSc7	ozon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podíl	podíl	k1gInSc1	podíl
O3	O3	k1gFnSc2	O3
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvykle	obvykle	k6eAd1	obvykle
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
ozon	ozon	k1gInSc1	ozon
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
připravit	připravit	k5eAaPmF	připravit
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
této	tento	k3xDgFnSc2	tento
plynné	plynný	k2eAgFnSc2d1	plynná
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
ozonu	ozon	k1gInSc2	ozon
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
jeho	jeho	k3xOp3gInPc7	jeho
silnými	silný	k2eAgInPc7d1	silný
oxidačními	oxidační	k2eAgInPc7d1	oxidační
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
sterilizaci	sterilizace	k1gFnSc3	sterilizace
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
diskutabilní	diskutabilní	k2eAgInPc1d1	diskutabilní
jsou	být	k5eAaImIp3nP	být
účinky	účinek	k1gInPc1	účinek
dnes	dnes	k6eAd1	dnes
poměrně	poměrně	k6eAd1	poměrně
populární	populární	k2eAgFnPc1d1	populární
ozonové	ozonový	k2eAgFnPc1d1	ozonová
terapie	terapie	k1gFnPc1	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Baktericidní	baktericidní	k2eAgInPc1d1	baktericidní
účinky	účinek	k1gInPc1	účinek
ozonu	ozon	k1gInSc2	ozon
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
namísto	namísto	k7c2	namísto
plynného	plynný	k2eAgInSc2d1	plynný
chloru	chlor	k1gInSc2	chlor
<g/>
,	,	kIx,	,
chlornanů	chlornan	k1gInPc2	chlornan
<g/>
,	,	kIx,	,
chloraminu	chloramin	k1gInSc2	chloramin
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
chloričitého	chloričitý	k2eAgInSc2d1	chloričitý
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k8xC	jako
první	první	k4xOgFnPc4	první
fáze	fáze	k1gFnPc4	fáze
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
uvedených	uvedený	k2eAgFnPc2d1	uvedená
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
preozonizace	preozonizace	k1gFnSc2	preozonizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
při	při	k7c6	při
malých	malý	k2eAgFnPc6d1	malá
dávkách	dávka	k1gFnPc6	dávka
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
parazitické	parazitický	k2eAgMnPc4d1	parazitický
prvoky	prvok	k1gMnPc4	prvok
a	a	k8xC	a
že	že	k8xS	že
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
karcinogenních	karcinogenní	k2eAgInPc2d1	karcinogenní
trihalomethanů	trihalomethan	k1gInPc2	trihalomethan
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgInPc1d1	silný
oxidační	oxidační	k2eAgInPc1d1	oxidační
účinky	účinek	k1gInPc1	účinek
ozonu	ozon	k1gInSc2	ozon
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
v	v	k7c6	v
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
bělení	bělení	k1gNnSc3	bělení
celulózy	celulóza	k1gFnSc2	celulóza
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
zápach	zápach	k1gInSc4	zápach
ozonu	ozon	k1gInSc2	ozon
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
jiskrami	jiskra	k1gFnPc7	jiskra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
vědec	vědec	k1gMnSc1	vědec
Martinus	Martinus	k1gMnSc1	Martinus
van	vana	k1gFnPc2	vana
Marum	Marum	k1gInSc1	Marum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
zapomenut	zapomenout	k5eAaPmNgInS	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
ozon	ozon	k1gInSc4	ozon
objevil	objevit	k5eAaPmAgMnS	objevit
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
Christian	Christian	k1gMnSc1	Christian
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schönbein	Schönbein	k1gMnSc1	Schönbein
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Pokládal	pokládat	k5eAaImAgMnS	pokládat
jej	on	k3xPp3gNnSc4	on
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
název	název	k1gInSc1	název
ozon	ozon	k1gInSc1	ozon
<g/>
.	.	kIx.	.
</s>
<s>
Pravou	pravý	k2eAgFnSc4d1	pravá
podstatu	podstata	k1gFnSc4	podstata
ozonu	ozon	k1gInSc2	ozon
odhalili	odhalit	k5eAaPmAgMnP	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
francouzští	francouzský	k2eAgMnPc1d1	francouzský
vědci	vědec	k1gMnPc1	vědec
Auguste	August	k1gMnSc5	August
de	de	k?	de
la	la	k1gNnPc2	la
Rive	Rive	k1gInSc1	Rive
a	a	k8xC	a
Jean-Charles	Jean-Charles	k1gInSc1	Jean-Charles
de	de	k?	de
Marignac	Marignac	k1gInSc1	Marignac
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
správně	správně	k6eAd1	správně
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
běžného	běžný	k2eAgInSc2d1	běžný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
teorii	teorie	k1gFnSc4	teorie
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Angličan	Angličan	k1gMnSc1	Angličan
Thomas	Thomas	k1gMnSc1	Thomas
Andrews	Andrews	k1gInSc4	Andrews
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
