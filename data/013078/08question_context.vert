<s>
Tis	tis	k1gInSc1
červený	červený	k2eAgInSc1d1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ťis	ťis	k?
<g/>
]	]	kIx)
<g/>
;	;	kIx,
Taxus	Taxus	k1gInSc1
baccata	baccat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
tis	tis	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dvoudomá	dvoudomý	k2eAgFnSc1d1
stálezelená	stálezelený	k2eAgFnSc1d1
jehličnatá	jehličnatý	k2eAgFnSc1d1
dřevina	dřevina	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
tisovitých	tisovití	k1gMnPc2
<g/>
.	.	kIx.
</s>