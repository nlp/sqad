<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
(	(	kIx(	(
<g/>
Cola	cola	k1gFnSc1	cola
acuminata	acuminata	k1gFnSc1	acuminata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
kola	kolo	k1gNnPc4	kolo
zašpičatělá	zašpičatělý	k2eAgNnPc4d1	zašpičatělé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slézovitých	slézovitý	k2eAgFnPc2d1	slézovitý
(	(	kIx(	(
<g/>
Malvaceae	Malvaceae	k1gFnPc2	Malvaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
v	v	k7c6	v
podčeledi	podčeleď	k1gFnSc6	podčeleď
lejnicových	lejnicový	k2eAgMnPc2d1	lejnicový
(	(	kIx(	(
<g/>
Sterculioideae	Sterculioidea	k1gMnSc2	Sterculioidea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přes	přes	k7c4	přes
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
kolovník	kolovník	k1gInSc1	kolovník
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
a	a	k8xC	a
kolovník	kolovník	k1gInSc1	kolovník
lesklý	lesklý	k2eAgInSc1d1	lesklý
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
pro	pro	k7c4	pro
kolová	kolový	k2eAgNnPc4d1	kolové
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
vysoce	vysoce	k6eAd1	vysoce
ceněna	cenit	k5eAaImNgFnS	cenit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Kolová	kolový	k2eAgNnPc1d1	kolové
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
územích	území	k1gNnPc6	území
Afriky	Afrika	k1gFnSc2	Afrika
používána	používat	k5eAaImNgFnS	používat
jako	jako	k8xC	jako
místní	místní	k2eAgNnSc1d1	místní
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
považují	považovat	k5eAaImIp3nP	považovat
kolová	kolový	k2eAgNnPc4d1	kolové
semínka	semínko	k1gNnPc4	semínko
za	za	k7c4	za
posvátná	posvátný	k2eAgNnPc4d1	posvátné
<g/>
.	.	kIx.	.
</s>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
deštných	deštný	k2eAgInPc6d1	deštný
pralesích	prales	k1gInPc6	prales
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
<g/>
,	,	kIx,	,
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
pěstování	pěstování	k1gNnSc1	pěstování
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
jižních	jižní	k2eAgFnPc6d1	jižní
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měří	měřit	k5eAaImIp3nS	měřit
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
m.	m.	k?	m.
Připomíná	připomínat	k5eAaImIp3nS	připomínat
vzhledem	vzhledem	k7c3	vzhledem
kaštan	kaštan	k1gInSc1	kaštan
koňský	koňský	k2eAgMnSc1d1	koňský
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
oválné	oválný	k2eAgNnSc1d1	oválné
<g/>
,	,	kIx,	,
kožovité	kožovitý	k2eAgInPc1d1	kožovitý
<g/>
,	,	kIx,	,
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
7	[number]	k4	7
cm	cm	kA	cm
široké	široký	k2eAgInPc4d1	široký
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gInSc1	čepel
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
kadeřavá	kadeřavý	k2eAgFnSc1d1	kadeřavá
<g/>
,	,	kIx,	,
řapík	řapík	k1gInSc1	řapík
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
jedno-	jedno-	k?	jedno-
nebo	nebo	k8xC	nebo
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
<g/>
.	.	kIx.	.
</s>
<s>
Kvetou	kvést	k5eAaImIp3nP	kvést
bíle	bíle	k6eAd1	bíle
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
seskupené	seskupený	k2eAgInPc4d1	seskupený
do	do	k7c2	do
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
starších	starý	k2eAgFnPc6d2	starší
větvích	větev	k1gFnPc6	větev
<g/>
.	.	kIx.	.
</s>

