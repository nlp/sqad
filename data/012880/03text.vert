<p>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
(	(	kIx(	(
<g/>
Cola	cola	k1gFnSc1	cola
acuminata	acuminata	k1gFnSc1	acuminata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
kola	kolo	k1gNnPc4	kolo
zašpičatělá	zašpičatělý	k2eAgNnPc4d1	zašpičatělé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slézovitých	slézovitý	k2eAgFnPc2d1	slézovitý
(	(	kIx(	(
<g/>
Malvaceae	Malvaceae	k1gFnPc2	Malvaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
v	v	k7c6	v
podčeledi	podčeleď	k1gFnSc6	podčeleď
lejnicových	lejnicový	k2eAgMnPc2d1	lejnicový
(	(	kIx(	(
<g/>
Sterculioideae	Sterculioidea	k1gMnSc2	Sterculioidea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přes	přes	k7c4	přes
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
kolovník	kolovník	k1gInSc1	kolovník
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
a	a	k8xC	a
kolovník	kolovník	k1gInSc1	kolovník
lesklý	lesklý	k2eAgInSc1d1	lesklý
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
pro	pro	k7c4	pro
kolová	kolový	k2eAgNnPc4d1	kolové
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
vysoce	vysoce	k6eAd1	vysoce
ceněna	cenit	k5eAaImNgFnS	cenit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Kolová	kolový	k2eAgNnPc1d1	kolové
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
územích	území	k1gNnPc6	území
Afriky	Afrika	k1gFnSc2	Afrika
používána	používat	k5eAaImNgFnS	používat
jako	jako	k8xC	jako
místní	místní	k2eAgNnSc1d1	místní
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
považují	považovat	k5eAaImIp3nP	považovat
kolová	kolový	k2eAgNnPc4d1	kolové
semínka	semínko	k1gNnPc4	semínko
za	za	k7c4	za
posvátná	posvátný	k2eAgNnPc4d1	posvátné
<g/>
.	.	kIx.	.
</s>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
deštných	deštný	k2eAgInPc6d1	deštný
pralesích	prales	k1gInPc6	prales
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
<g/>
,	,	kIx,	,
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
pěstování	pěstování	k1gNnSc1	pěstování
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
jižních	jižní	k2eAgFnPc6d1	jižní
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kolovník	Kolovník	k1gInSc1	Kolovník
zašpičatělý	zašpičatělý	k2eAgInSc1d1	zašpičatělý
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měří	měřit	k5eAaImIp3nS	měřit
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
m.	m.	k?	m.
Připomíná	připomínat	k5eAaImIp3nS	připomínat
vzhledem	vzhledem	k7c3	vzhledem
kaštan	kaštan	k1gInSc1	kaštan
koňský	koňský	k2eAgMnSc1d1	koňský
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
oválné	oválný	k2eAgNnSc1d1	oválné
<g/>
,	,	kIx,	,
kožovité	kožovitý	k2eAgInPc1d1	kožovitý
<g/>
,	,	kIx,	,
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
7	[number]	k4	7
cm	cm	kA	cm
široké	široký	k2eAgInPc4d1	široký
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gInSc1	čepel
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
kadeřavá	kadeřavý	k2eAgFnSc1d1	kadeřavá
<g/>
,	,	kIx,	,
řapík	řapík	k1gInSc1	řapík
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
jedno-	jedno-	k?	jedno-
nebo	nebo	k8xC	nebo
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
<g/>
.	.	kIx.	.
</s>
<s>
Kvetou	kvést	k5eAaImIp3nP	kvést
bíle	bíle	k6eAd1	bíle
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
seskupené	seskupený	k2eAgInPc4d1	seskupený
do	do	k7c2	do
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
starších	starý	k2eAgFnPc6d2	starší
větvích	větev	k1gFnPc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgInPc1d1	samičí
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
větší	veliký	k2eAgInPc4d2	veliký
než	než	k8xS	než
květy	květ	k1gInPc1	květ
samčí	samčí	k2eAgInPc1d1	samčí
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
kolovníku	kolovník	k1gInSc2	kolovník
jsou	být	k5eAaImIp3nP	být
podlouhlé	podlouhlý	k2eAgNnSc1d1	podlouhlé
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
)	)	kIx)	)
měchýřky	měchýřek	k1gInPc1	měchýřek
<g/>
.	.	kIx.	.
</s>
<s>
Zralé	zralý	k2eAgFnPc1d1	zralá
jsou	být	k5eAaImIp3nP	být
nahnědlé	nahnědlý	k2eAgFnPc1d1	nahnědlá
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
měchýřky	měchýřek	k1gInPc1	měchýřek
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgInPc1d1	složený
do	do	k7c2	do
kulovitého	kulovitý	k2eAgInSc2d1	kulovitý
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
jich	on	k3xPp3gFnPc2	on
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
5-13	[number]	k4	5-13
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Osemení	osemení	k1gNnSc1	osemení
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
sladkokyselou	sladkokyselý	k2eAgFnSc4d1	sladkokyselá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zárodek	zárodek	k1gInSc1	zárodek
s	s	k7c7	s
3-5	[number]	k4	3-5
načervenalými	načervenalý	k2eAgFnPc7d1	načervenalá
dělohami	děloha	k1gFnPc7	děloha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nahořklé	nahořklý	k2eAgFnPc1d1	nahořklá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Semena	semeno	k1gNnPc1	semeno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
převážně	převážně	k6eAd1	převážně
škrob	škrob	k1gInSc4	škrob
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
theobromin	theobromin	k1gInSc1	theobromin
<g/>
,	,	kIx,	,
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
<g/>
,	,	kIx,	,
silice	silice	k1gFnPc1	silice
a	a	k8xC	a
barvivo	barvivo	k1gNnSc1	barvivo
kolová	kolová	k1gFnSc1	kolová
červeň	červeň	k1gFnSc1	červeň
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
škrobu	škrob	k1gInSc2	škrob
a	a	k8xC	a
povzbuzujícího	povzbuzující	k2eAgInSc2d1	povzbuzující
kofeinu	kofein	k1gInSc2	kofein
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolovníková	kolovníkový	k2eAgNnPc1d1	kolovníkový
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
vyhledávána	vyhledáván	k2eAgNnPc1d1	vyhledáváno
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Semeno	semeno	k1gNnSc1	semeno
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
až	až	k9	až
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
zbude	zbýt	k5eAaPmIp3nS	zbýt
jen	jen	k9	jen
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kolový	kolový	k2eAgInSc1d1	kolový
oříšek	oříšek	k1gInSc1	oříšek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
domorodci	domorodec	k1gMnPc1	domorodec
krájejí	krájet	k5eAaImIp3nP	krájet
na	na	k7c4	na
kolečka	kolečko	k1gNnPc4	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
má	mít	k5eAaImIp3nS	mít
nahořklou	nahořklý	k2eAgFnSc4d1	nahořklá
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
nasládlou	nasládlý	k2eAgFnSc4d1	nasládlá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Tiší	tišit	k5eAaImIp3nS	tišit
také	také	k9	také
žízeň	žízeň	k1gFnSc1	žízeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgNnP	být
semena	semeno	k1gNnPc1	semeno
koly	kola	k1gFnSc2	kola
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
americký	americký	k2eAgMnSc1d1	americký
lékárník	lékárník	k1gMnSc1	lékárník
John	John	k1gMnSc1	John
Pemberton	Pemberton	k1gInSc4	Pemberton
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
nápoj	nápoj	k1gInSc4	nápoj
obsahující	obsahující	k2eAgNnSc4d1	obsahující
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
výtažky	výtažek	k1gInPc4	výtažek
z	z	k7c2	z
kokainovníku	kokainovník	k1gInSc2	kokainovník
pravého	pravý	k2eAgInSc2d1	pravý
a	a	k8xC	a
kolovníku	kolovník	k1gInSc2	kolovník
lesklého	lesklý	k2eAgInSc2d1	lesklý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prohibici	prohibice	k1gFnSc6	prohibice
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
víno	víno	k1gNnSc4	víno
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
sodou	soda	k1gFnSc7	soda
a	a	k8xC	a
nápoj	nápoj	k1gInSc1	nápoj
nazván	nazván	k2eAgInSc1d1	nazván
Coca-Cola	cocaola	k1gFnSc1	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k8xC	i
prodej	prodej	k1gInSc4	prodej
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
narkotické	narkotický	k2eAgInPc4d1	narkotický
účinky	účinek	k1gInPc4	účinek
kokainu	kokain	k1gInSc2	kokain
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
Coca-Cola	cocaola	k1gFnSc1	coca-cola
už	už	k6eAd1	už
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
ani	ani	k8xC	ani
výtažky	výtažek	k1gInPc4	výtažek
z	z	k7c2	z
koky	koka	k1gFnSc2	koka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
pak	pak	k6eAd1	pak
německá	německý	k2eAgFnSc1d1	německá
čokoládovna	čokoládovna	k1gFnSc1	čokoládovna
Hildebrand	Hildebranda	k1gFnPc2	Hildebranda
uvedla	uvést	k5eAaPmAgFnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
energetickou	energetický	k2eAgFnSc4d1	energetická
čokoládu	čokoláda	k1gFnSc4	čokoláda
Scho-ka-kola	Schoaola	k1gFnSc1	Scho-ka-kola
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
složek	složka	k1gFnPc2	složka
výtažek	výtažek	k1gInSc4	výtažek
z	z	k7c2	z
kolového	kolový	k2eAgInSc2d1	kolový
oříšku	oříšek	k1gInSc2	oříšek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
čokoláda	čokoláda	k1gFnSc1	čokoláda
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
kolovník	kolovník	k1gInSc1	kolovník
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
průmyslu	průmysl	k1gInSc6	průmysl
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
povzbuzujících	povzbuzující	k2eAgInPc2d1	povzbuzující
preparátů	preparát	k1gInPc2	preparát
(	(	kIx(	(
<g/>
kolové	kolový	k2eAgNnSc4d1	kolové
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
kolové	kolový	k2eAgFnPc4d1	kolová
tabletky	tabletka	k1gFnPc4	tabletka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
osvěžujících	osvěžující	k2eAgInPc2d1	osvěžující
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
energetických	energetický	k2eAgFnPc2d1	energetická
čokolád	čokoláda	k1gFnPc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
černými	černý	k2eAgMnPc7d1	černý
domorodci	domorodec	k1gMnPc7	domorodec
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
zvyk	zvyk	k1gInSc1	zvyk
žvýkání	žvýkání	k1gNnSc2	žvýkání
kolových	kolový	k2eAgInPc2d1	kolový
oříšků	oříšek	k1gInPc2	oříšek
značně	značně	k6eAd1	značně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolovník	kolovník	k1gMnSc1	kolovník
zašpičatělý	zašpičatělý	k2eAgMnSc1d1	zašpičatělý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Cola	cola	k1gFnSc1	cola
acuminata	acuminata	k1gFnSc1	acuminata
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
Biolib	Bioliba	k1gFnPc2	Bioliba
</s>
</p>
<p>
<s>
web	web	k1gInSc1	web
Oko	oko	k1gNnSc1	oko
</s>
</p>
