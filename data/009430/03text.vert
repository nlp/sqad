<p>
<s>
Kečup	kečup	k1gInSc1	kečup
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
husté	hustý	k2eAgFnSc2d1	hustá
červené	červený	k2eAgFnSc2d1	červená
rajčatové	rajčatový	k2eAgFnSc2d1	rajčatová
omáčky	omáčka	k1gFnSc2	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
základ	základ	k1gInSc1	základ
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
jako	jako	k8xC	jako
omáčka	omáčka	k1gFnSc1	omáčka
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
kečup	kečup	k1gInSc1	kečup
tvoří	tvořit	k5eAaImIp3nS	tvořit
rajčatový	rajčatový	k2eAgInSc1d1	rajčatový
protlak	protlak	k1gInSc1	protlak
<g/>
,	,	kIx,	,
koření	koření	k1gNnPc1	koření
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
přísady	přísada	k1gFnPc1	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
kečup	kečup	k1gInSc1	kečup
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
domácím	domácí	k2eAgInSc7d1	domácí
způsobem	způsob	k1gInSc7	způsob
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
rajčata	rajče	k1gNnPc4	rajče
<g/>
,	,	kIx,	,
cibuli	cibule	k1gFnSc4	cibule
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
koření	koření	k1gNnSc4	koření
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kečup	kečup	k1gInSc1	kečup
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
ochucení	ochucení	k1gNnSc3	ochucení
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kečup	kečup	k1gInSc1	kečup
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
konzumován	konzumovat	k5eAaBmNgInS	konzumovat
s	s	k7c7	s
klobásami	klobása	k1gFnPc7	klobása
<g/>
,	,	kIx,	,
hamburgery	hamburger	k1gInPc7	hamburger
<g/>
,	,	kIx,	,
hranolky	hranolek	k1gInPc1	hranolek
<g/>
,	,	kIx,	,
párky	párek	k1gInPc1	párek
v	v	k7c6	v
rohlíku	rohlík	k1gInSc6	rohlík
<g/>
,	,	kIx,	,
s	s	k7c7	s
těstovinami	těstovina	k1gFnPc7	těstovina
i	i	k9	i
se	s	k7c7	s
sýrem	sýr	k1gInSc7	sýr
<g/>
,	,	kIx,	,
rýži	rýže	k1gFnSc4	rýže
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
jí	jíst	k5eAaImIp3nS	jíst
s	s	k7c7	s
fish	fish	k1gInSc1	fish
and	and	k?	and
chips	chips	k1gInSc1	chips
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
kečupu	kečup	k1gInSc2	kečup
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
počátek	počátek	k1gInSc4	počátek
zasazují	zasazovat	k5eAaImIp3nP	zasazovat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
nebo	nebo	k8xC	nebo
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
kečup	kečup	k1gInSc1	kečup
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
různými	různý	k2eAgNnPc7d1	různé
jmény	jméno	k1gNnPc7	jméno
<g/>
:	:	kIx,	:
catsup	catsup	k1gInSc1	catsup
<g/>
,	,	kIx,	,
catchup	catchup	k1gMnSc1	catchup
<g/>
,	,	kIx,	,
ketsup	ketsup	k1gMnSc1	ketsup
<g/>
,	,	kIx,	,
kitchup	kitchup	k1gMnSc1	kitchup
či	či	k8xC	či
ketchup	ketchup	k1gMnSc1	ketchup
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
husté	hustý	k2eAgFnSc2d1	hustá
východoindické	východoindický	k2eAgFnSc2d1	Východoindická
omáčky	omáčka	k1gFnSc2	omáčka
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
omáčky	omáčka	k1gFnPc4	omáčka
obsahující	obsahující	k2eAgInSc1d1	obsahující
ocet	ocet	k1gInSc1	ocet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
kečup	kečup	k1gInSc1	kečup
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
výslovnost	výslovnost	k1gFnSc1	výslovnost
"	"	kIx"	"
<g/>
ketjap	ketjap	k1gInSc1	ketjap
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
popularizaci	popularizace	k1gFnSc4	popularizace
a	a	k8xC	a
název	název	k1gInSc4	název
téměř	téměř	k6eAd1	téměř
zachovala	zachovat	k5eAaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Kečupy	kečup	k1gInPc7	kečup
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
norem	norma	k1gFnPc2	norma
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
kvality	kvalita	k1gFnSc2	kvalita
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
kečupů	kečup	k1gInPc2	kečup
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
šizeny	šizen	k2eAgInPc4d1	šizen
vodou	voda	k1gFnSc7	voda
se	s	k7c7	s
zahušťovadly	zahušťovadlo	k1gNnPc7	zahušťovadlo
<g/>
,	,	kIx,	,
barvivy	barvivo	k1gNnPc7	barvivo
a	a	k8xC	a
ochucovadly	ochucovadlo	k1gNnPc7	ochucovadlo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
přidávány	přidáván	k2eAgFnPc1d1	přidávána
náhradní	náhradní	k2eAgFnPc4d1	náhradní
levné	levný	k2eAgFnPc4d1	levná
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jablka	jablko	k1gNnPc1	jablko
či	či	k8xC	či
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
částečnému	částečný	k2eAgNnSc3d1	částečné
zlepšení	zlepšení	k1gNnSc3	zlepšení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
Vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
157	[number]	k4	157
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrobek	výrobek	k1gInSc1	výrobek
označený	označený	k2eAgInSc1d1	označený
jako	jako	k8xS	jako
kečup	kečup	k1gInSc1	kečup
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
12	[number]	k4	12
<g/>
%	%	kIx~	%
refraktometrické	refraktometrický	k2eAgFnSc2d1	refraktometrická
sušiny	sušina	k1gFnSc2	sušina
<g/>
,	,	kIx,	,
kečupy	kečup	k1gInPc1	kečup
označené	označený	k2eAgInPc1d1	označený
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Prima	prima	k1gFnSc1	prima
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Speciál	speciál	k1gInSc1	speciál
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Extra	extra	k2eAgFnSc1d1	extra
<g/>
"	"	kIx"	"
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
30	[number]	k4	30
%	%	kIx~	%
sušiny	sušina	k1gFnSc2	sušina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
tuto	tento	k3xDgFnSc4	tento
vyhlášku	vyhláška	k1gFnSc4	vyhláška
nedodržovali	dodržovat	k5eNaImAgMnP	dodržovat
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
buď	buď	k8xC	buď
k	k	k7c3	k
šizení	šizení	k1gNnSc3	šizení
nebo	nebo	k8xC	nebo
uvádění	uvádění	k1gNnSc3	uvádění
na	na	k7c4	na
trh	trh	k1gInSc4	trh
podobných	podobný	k2eAgInPc2d1	podobný
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
kečupová	kečupový	k2eAgFnSc1d1	kečupová
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
parametry	parametr	k1gInPc4	parametr
pro	pro	k7c4	pro
kečup	kečup	k1gInSc4	kečup
<g/>
.	.	kIx.	.
<g/>
Ani	ani	k8xC	ani
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
výsledky	výsledek	k1gInPc1	výsledek
kontrol	kontrola	k1gFnPc2	kontrola
kvality	kvalita	k1gFnSc2	kvalita
kečupů	kečup	k1gInPc2	kečup
nedopadly	dopadnout	k5eNaPmAgFnP	dopadnout
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
výrobcích	výrobek	k1gInPc6	výrobek
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
jedy	jed	k1gInPc1	jed
z	z	k7c2	z
plísní	plíseň	k1gFnPc2	plíseň
či	či	k8xC	či
pesticidy	pesticid	k1gInPc4	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
kvalita	kvalita	k1gFnSc1	kvalita
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
kečupů	kečup	k1gInPc2	kečup
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
některé	některý	k3yIgFnSc2	některý
hospodyňky	hospodyňka	k1gFnSc2	hospodyňka
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
tuto	tento	k3xDgFnSc4	tento
pochutinu	pochutina	k1gFnSc4	pochutina
podomácku	podomácku	k6eAd1	podomácku
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
firmou	firma	k1gFnSc7	firma
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
podle	podle	k7c2	podle
prodaného	prodaný	k2eAgInSc2d1	prodaný
objemu	objem	k1gInSc2	objem
kečupů	kečup	k1gInPc2	kečup
i	i	k8xC	i
výše	výše	k1gFnSc1	výše
tržeb	tržba	k1gFnPc2	tržba
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Hamé	Hamá	k1gFnSc2	Hamá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
podle	podle	k7c2	podle
firmy	firma	k1gFnSc2	firma
Heinz	Heinza	k1gFnPc2	Heinza
prodalo	prodat	k5eAaPmAgNnS	prodat
asi	asi	k9	asi
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
kečupu	kečup	k1gInSc2	kečup
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vychází	vycházet	k5eAaImIp3nS	vycházet
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
1,5	[number]	k4	1,5
kg	kg	kA	kg
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Unileveru	Unilever	k1gInSc2	Unilever
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc1	výrobce
kečupů	kečup	k1gInPc2	kečup
Hellmann	Hellmann	k1gMnSc1	Hellmann
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
3,9	[number]	k4	3,9
kg	kg	kA	kg
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
mají	mít	k5eAaImIp3nP	mít
údajně	údajně	k6eAd1	údajně
nejraději	rád	k6eAd3	rád
sladké	sladký	k2eAgInPc1d1	sladký
kečupy	kečup	k1gInPc1	kečup
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
jemné	jemný	k2eAgFnPc1d1	jemná
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
tvoří	tvořit	k5eAaImIp3nP	tvořit
osmdesát	osmdesát	k4xCc4	osmdesát
procent	procento	k1gNnPc2	procento
všech	všecek	k3xTgMnPc2	všecek
prodejů	prodej	k1gInPc2	prodej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
prodá	prodat	k5eAaPmIp3nS	prodat
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
více	hodně	k6eAd2	hodně
tatarské	tatarský	k2eAgFnSc2d1	tatarská
omáčky	omáčka	k1gFnSc2	omáčka
než	než	k8xS	než
kečupu	kečup	k1gInSc2	kečup
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
evropského	evropský	k2eAgNnSc2d1	Evropské
hlediska	hledisko	k1gNnSc2	hledisko
unikát	unikát	k1gInSc1	unikát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kečupová	Kečupový	k2eAgFnSc1d1	Kečupový
láhev	láhev	k1gFnSc1	láhev
==	==	k?	==
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
kečup	kečup	k1gInSc4	kečup
umělohmotnou	umělohmotný	k2eAgFnSc4d1	umělohmotná
láhev	láhev	k1gFnSc4	láhev
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
uzávěrem	uzávěr	k1gInSc7	uzávěr
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
kečup	kečup	k1gInSc4	kečup
mazat	mazat	k5eAaImF	mazat
obtížně	obtížně	k6eAd1	obtížně
nožem	nůž	k1gInSc7	nůž
<g/>
,	,	kIx,	,
nekape	kapat	k5eNaImIp3nS	kapat
<g/>
,	,	kIx,	,
dávkuje	dávkovat	k5eAaImIp3nS	dávkovat
přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
"	"	kIx"	"
<g/>
neprdí	prdět	k5eNaImIp3nS	prdět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
nahromaděný	nahromaděný	k2eAgInSc4d1	nahromaděný
vzduch	vzduch	k1gInSc4	vzduch
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kečup	kečup	k1gInSc1	kečup
je	být	k5eAaImIp3nS	být
nahromaděný	nahromaděný	k2eAgInSc1d1	nahromaděný
dole	dole	k6eAd1	dole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
uzávěr	uzávěr	k1gInSc1	uzávěr
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
láhev	láhev	k1gFnSc4	láhev
byl	být	k5eAaImAgInS	být
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
Heinz	Heinz	k1gMnSc1	Heinz
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
uvedla	uvést	k5eAaPmAgFnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
obrácené	obrácený	k2eAgFnSc2d1	obrácená
láhve	láhev	k1gFnSc2	láhev
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
uzávěrem	uzávěr	k1gInSc7	uzávěr
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
úsilí	úsilí	k1gNnSc2	úsilí
přimět	přimět	k5eAaPmF	přimět
spotřebitele	spotřebitel	k1gMnPc4	spotřebitel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jedli	jíst	k5eAaImAgMnP	jíst
více	hodně	k6eAd2	hodně
kečupu	kečup	k1gInSc2	kečup
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
i	i	k9	i
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kečup	kečup	k1gInSc1	kečup
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kečup	kečup	k1gInSc1	kečup
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
