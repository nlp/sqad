<p>
<s>
Fernã	Fernã	k?	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gFnSc1	Magalhã
(	(	kIx(	(
<g/>
vys	vys	k?	vys
<g/>
:	:	kIx,	:
fernau	fernau	k6eAd1	fernau
de	de	k?	de
magaljajnš	magaljajnš	k1gInSc1	magaljajnš
(	(	kIx(	(
<g/>
port	port	k1gInSc1	port
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesprávně	správně	k6eNd1	správně
magalienš	magalienš	k5eAaPmIp2nS	magalienš
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Fernando	Fernanda	k1gFnSc5	Fernanda
Magallanes	Magallanes	k1gInSc4	Magallanes
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Ferdinandus	Ferdinandus	k1gInSc1	Ferdinandus
Magellanus	Magellanus	k1gInSc1	Magellanus
<g/>
;	;	kIx,	;
jaro	jaro	k1gNnSc1	jaro
1480	[number]	k4	1480
<g/>
,	,	kIx,	,
Sabrosa	Sabrosa	k1gFnSc1	Sabrosa
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
Mactan	Mactan	k1gInSc4	Mactan
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
portugalský	portugalský	k2eAgMnSc1d1	portugalský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
španělské	španělský	k2eAgFnSc3d1	španělská
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
například	například	k6eAd1	například
Magalhã	Magalhã	k1gFnSc4	Magalhã
průliv	průliv	k1gInSc4	průliv
nebo	nebo	k8xC	nebo
také	také	k9	také
galaxie	galaxie	k1gFnPc4	galaxie
Velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Magellanův	Magellanův	k2eAgInSc1d1	Magellanův
oblak	oblak	k1gInSc1	oblak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
kulatosti	kulatost	k1gFnSc6	kulatost
Země	zem	k1gFnSc2	zem
přinesla	přinést	k5eAaPmAgFnS	přinést
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
Magalhã	Magalhã	k1gMnSc1	Magalhã
vedl	vést	k5eAaImAgMnS	vést
a	a	k8xC	a
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
poprvé	poprvé	k6eAd1	poprvé
obeplula	obeplout	k5eAaPmAgFnS	obeplout
celou	celý	k2eAgFnSc4d1	celá
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pěti	pět	k4xCc2	pět
lodí	loď	k1gFnPc2	loď
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
obeplutí	obeplutí	k1gNnSc2	obeplutí
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
dokončila	dokončit	k5eAaPmAgFnS	dokončit
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Magalhã	Magalhã	k1gFnSc7	Magalhã
zahynul	zahynout	k5eAaPmAgInS	zahynout
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rukama	ruka	k1gFnPc7	ruka
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
)	)	kIx)	)
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Magalhã	Magalhã	k1gMnSc1	Magalhã
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
námořníky	námořník	k1gMnPc7	námořník
uskutečňoval	uskutečňovat	k5eAaImAgMnS	uskutečňovat
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Španělé	Španěl	k1gMnPc1	Španěl
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c6	na
dobývání	dobývání	k1gNnSc6	dobývání
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnSc2d1	objevená
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
mořeplavby	mořeplavba	k1gFnPc1	mořeplavba
v	v	k7c6	v
portugalských	portugalský	k2eAgFnPc6d1	portugalská
službách	služba	k1gFnPc6	služba
==	==	k?	==
</s>
</p>
<p>
<s>
Magalhã	Magalhã	k?	Magalhã
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
zchudlé	zchudlý	k2eAgFnSc6d1	zchudlá
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
severoportugalské	severoportugalský	k2eAgFnSc6d1	severoportugalský
provincii	provincie	k1gFnSc6	provincie
Trás-os-Montes	Tráss-Montes	k1gMnSc1	Trás-os-Montes
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Pedro	Pedro	k1gNnSc1	Pedro
Rui	Rui	k1gMnSc1	Rui
de	de	k?	de
Magalhã	Magalhã	k1gMnSc1	Magalhã
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
Sabrosy	Sabrosa	k1gFnSc2	Sabrosa
<g/>
.	.	kIx.	.
</s>
<s>
Fernã	Fernã	k?	Fernã
měl	mít	k5eAaImAgInS	mít
dva	dva	k4xCgMnPc4	dva
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc4	bratr
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
deset	deset	k4xCc1	deset
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pážetem	páže	k1gNnSc7	páže
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
Jana	Jan	k1gMnSc2	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
Eleonory	Eleonora	k1gFnSc2	Eleonora
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrancem	bratranec	k1gMnSc7	bratranec
Franciskem	Francisko	k1gNnSc7	Francisko
Serrã	Serrã	k1gMnSc1	Serrã
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
ho	on	k3xPp3gNnSc4	on
geografie	geografie	k1gFnSc1	geografie
a	a	k8xC	a
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
věcech	věc	k1gFnPc6	věc
byl	být	k5eAaImAgInS	být
vyučován	vyučovat	k5eAaImNgInS	vyučovat
Martinem	Martin	k1gMnSc7	Martin
Behaimem	Behaim	k1gMnSc7	Behaim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Magalhã	Magalhã	k1gMnSc1	Magalhã
panošem	panoš	k1gMnSc7	panoš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1505	[number]	k4	1505
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtěl	chtít	k5eAaImAgMnS	chtít
budoucí	budoucí	k2eAgMnSc1d1	budoucí
místokrál	místokrál	k1gMnSc1	místokrál
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Almeida	Almeida	k1gFnSc1	Almeida
vytvořit	vytvořit	k5eAaPmF	vytvořit
vojenské	vojenský	k2eAgFnSc3d1	vojenská
a	a	k8xC	a
námořní	námořní	k2eAgFnSc2d1	námořní
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
bitvou	bitva	k1gFnSc7	bitva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1506	[number]	k4	1506
přistál	přistát	k5eAaPmAgInS	přistát
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1510	[number]	k4	1510
je	být	k5eAaImIp3nS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
své	svůj	k3xOyFgNnSc4	svůj
velení	velení	k1gNnSc4	velení
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
když	když	k8xS	když
plachtil	plachtit	k5eAaImAgMnS	plachtit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1511	[number]	k4	1511
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1513	[number]	k4	1513
bojoval	bojovat	k5eAaImAgMnS	bojovat
a	a	k8xC	a
zranil	zranit	k5eAaPmAgMnS	zranit
si	se	k3xPyFc3	se
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
koleno	koleno	k1gNnSc1	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
kulhal	kulhat	k5eAaImAgMnS	kulhat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ilegálně	ilegálně	k6eAd1	ilegálně
obchodoval	obchodovat	k5eAaImAgMnS	obchodovat
s	s	k7c7	s
Maury	Maur	k1gMnPc7	Maur
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1514	[number]	k4	1514
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
z	z	k7c2	z
portugalských	portugalský	k2eAgFnPc2d1	portugalská
státních	státní	k2eAgFnPc2d1	státní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
rozhořčen	rozhořčit	k5eAaPmNgMnS	rozhořčit
a	a	k8xC	a
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgInS	přijmout
španělské	španělský	k2eAgNnSc4d1	španělské
jméno	jméno	k1gNnSc4	jméno
Fernando	Fernanda	k1gFnSc5	Fernanda
de	de	k?	de
Magallanes	Magallanesa	k1gFnPc2	Magallanesa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
označován	označován	k2eAgMnSc1d1	označován
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ztratil	ztratit	k5eAaPmAgInS	ztratit
čest	čest	k1gFnSc4	čest
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
plavba	plavba	k1gFnSc1	plavba
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátek	počátek	k1gInSc1	počátek
výpravy	výprava	k1gFnSc2	výprava
===	===	k?	===
</s>
</p>
<p>
<s>
Magalhã	Magalhã	k?	Magalhã
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Naklonil	naklonit	k5eAaPmAgMnS	naklonit
si	se	k3xPyFc3	se
biskupa	biskup	k1gMnSc4	biskup
Juana	Juan	k1gMnSc4	Juan
de	de	k?	de
Fonseca	Fonseca	k1gMnSc1	Fonseca
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
španělskému	španělský	k2eAgMnSc3d1	španělský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
(	(	kIx(	(
<g/>
Carlos	Carlos	k1gMnSc1	Carlos
I.	I.	kA	I.
<g/>
)	)	kIx)	)
schválil	schválit	k5eAaPmAgInS	schválit
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1518	[number]	k4	1518
vyslání	vyslání	k1gNnSc4	vyslání
expedice	expedice	k1gFnSc2	expedice
na	na	k7c4	na
západ	západ	k1gInSc4	západ
až	až	k9	až
k	k	k7c3	k
Ostrovům	ostrov	k1gInPc3	ostrov
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Flotila	flotila	k1gFnSc1	flotila
pak	pak	k6eAd1	pak
vyplula	vyplout	k5eAaPmAgFnS	vyplout
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1519	[number]	k4	1519
<g/>
.	.	kIx.	.
</s>
<s>
Tvořilo	tvořit	k5eAaImAgNnS	tvořit
ji	on	k3xPp3gFnSc4	on
pět	pět	k4xCc1	pět
starých	starý	k2eAgFnPc2d1	stará
karavel	karavela	k1gFnPc2	karavela
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Trinidad	Trinidad	k1gInSc1	Trinidad
<g/>
,	,	kIx,	,
130	[number]	k4	130
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
55	[number]	k4	55
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
:	:	kIx,	:
Fernã	Fernã	k1gMnSc1	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gFnSc1	Magalhã
(	(	kIx(	(
<g/>
zajata	zajat	k2eAgFnSc1d1	zajata
Portugalci	Portugalec	k1gMnPc1	Portugalec
na	na	k7c6	na
Molukách	Moluky	k1gFnPc6	Moluky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
San	San	k?	San
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
,	,	kIx,	,
130	[number]	k4	130
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
60	[number]	k4	60
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
:	:	kIx,	:
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Cartagena	Cartagena	k1gFnSc1	Cartagena
(	(	kIx(	(
<g/>
dezertovala	dezertovat	k5eAaBmAgFnS	dezertovat
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
u	u	k7c2	u
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Concepción	Concepción	k1gMnSc1	Concepción
<g/>
,	,	kIx,	,
90	[number]	k4	90
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
45	[number]	k4	45
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
:	:	kIx,	:
Gaspar	Gaspar	k1gMnSc1	Gaspar
de	de	k?	de
Quesada	Quesada	k1gFnSc1	Quesada
(	(	kIx(	(
<g/>
spálena	spálen	k2eAgFnSc1d1	spálena
při	při	k7c6	při
boji	boj	k1gInSc6	boj
s	s	k7c7	s
domorodci	domorodec	k1gMnPc7	domorodec
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnPc4	Victorium
<g/>
,	,	kIx,	,
90	[number]	k4	90
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
42	[number]	k4	42
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
:	:	kIx,	:
Luis	Luisa	k1gFnPc2	Luisa
de	de	k?	de
Mendoza	Mendoz	k1gMnSc2	Mendoz
(	(	kIx(	(
<g/>
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
pěti	pět	k4xCc2	pět
lodí	loď	k1gFnPc2	loď
výpravy	výprava	k1gFnSc2	výprava
obeplula	obeplout	k5eAaPmAgFnS	obeplout
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
,	,	kIx,	,
60	[number]	k4	60
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
32	[number]	k4	32
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
:	:	kIx,	:
Joã	Joã	k1gMnSc1	Joã
Serrã	Serrã	k1gMnSc1	Serrã
(	(	kIx(	(
<g/>
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
u	u	k7c2	u
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
<g/>
Posádku	posádka	k1gFnSc4	posádka
lodí	loď	k1gFnPc2	loď
tvořili	tvořit	k5eAaImAgMnP	tvořit
většinou	většinou	k6eAd1	většinou
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
národnosti	národnost	k1gFnPc4	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
postavou	postava	k1gFnSc7	postava
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
italský	italský	k2eAgMnSc1d1	italský
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
Antonio	Antonio	k1gMnSc1	Antonio
Pigafetta	Pigafetta	k1gMnSc1	Pigafetta
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
o	o	k7c6	o
výpravě	výprava	k1gFnSc6	výprava
vedl	vést	k5eAaImAgMnS	vést
záznamy	záznam	k1gInPc4	záznam
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
18	[number]	k4	18
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokončili	dokončit	k5eAaPmAgMnP	dokončit
obeplutí	obeplutí	k1gNnSc4	obeplutí
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
zbývající	zbývající	k2eAgFnSc7d1	zbývající
lodí	loď	k1gFnSc7	loď
výpravy	výprava	k1gFnSc2	výprava
<g/>
;	;	kIx,	;
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
zachovala	zachovat	k5eAaPmAgFnS	zachovat
podrobná	podrobný	k2eAgFnSc1d1	podrobná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Magalhã	Magalhã	k?	Magalhã
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
zanechal	zanechat	k5eAaPmAgInS	zanechat
svou	svůj	k3xOyFgFnSc4	svůj
těhotnou	těhotný	k2eAgFnSc4d1	těhotná
ženu	žena	k1gFnSc4	žena
Beatriz	Beatriza	k1gFnPc2	Beatriza
a	a	k8xC	a
dvouletého	dvouletý	k2eAgMnSc2d1	dvouletý
syna	syn	k1gMnSc2	syn
Rodrigueze	Rodrigueze	k1gFnSc2	Rodrigueze
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ani	ani	k8xC	ani
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
potomci	potomek	k1gMnPc1	potomek
se	se	k3xPyFc4	se
návratu	návrat	k1gInSc2	návrat
výpravy	výprava	k1gFnSc2	výprava
nedožili	dožít	k5eNaPmAgMnP	dožít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
Magalhã	Magalhã	k1gFnSc1	Magalhã
zastávkou	zastávka	k1gFnSc7	zastávka
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
první	první	k4xOgInPc1	první
konflikty	konflikt	k1gInPc1	konflikt
s	s	k7c7	s
podřízenými	podřízený	k2eAgMnPc7d1	podřízený
kapitány	kapitán	k1gMnPc7	kapitán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Plavba	plavba	k1gFnSc1	plavba
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
rovník	rovník	k1gInSc4	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
jim	on	k3xPp3gMnPc3	on
příliš	příliš	k6eAd1	příliš
nepřálo	přát	k5eNaImAgNnS	přát
–	–	k?	–
vály	vál	k1gInPc1	vál
nepříznivé	příznivý	k2eNgInPc1d1	nepříznivý
větry	vítr	k1gInPc1	vítr
a	a	k8xC	a
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
pršelo	pršet	k5eAaImAgNnS	pršet
pak	pak	k6eAd1	pak
šedesát	šedesát	k4xCc4	šedesát
dnů	den	k1gInPc2	den
bez	bez	k7c2	bez
ustání	ustání	k1gNnSc2	ustání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odporuje	odporovat	k5eAaImIp3nS	odporovat
názoru	názor	k1gInSc2	názor
starých	starý	k2eAgMnPc2d1	starý
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dopluli	doplout	k5eAaPmAgMnP	doplout
do	do	k7c2	do
zátoky	zátoka	k1gFnSc2	zátoka
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Ria	Ria	k1gMnSc2	Ria
de	de	k?	de
Janeira	Janeir	k1gMnSc2	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Juana	Juan	k1gMnSc4	Juan
de	de	k?	de
Cartagenu	Cartagen	k1gInSc2	Cartagen
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc4	druhý
muže	muž	k1gMnSc4	muž
flotily	flotila	k1gFnSc2	flotila
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
dát	dát	k5eAaPmF	dát
Magalhã	Magalhã	k1gFnSc4	Magalhã
za	za	k7c4	za
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
výprava	výprava	k1gFnSc1	výprava
ubírala	ubírat	k5eAaImAgFnS	ubírat
podél	podél	k7c2	podél
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
břehů	břeh	k1gInPc2	břeh
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
ledna	leden	k1gInSc2	leden
1520	[number]	k4	1520
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
ústí	ústí	k1gNnSc4	ústí
La	la	k1gNnSc2	la
Plata	plato	k1gNnSc2	plato
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgNnSc6	jenž
zpočátku	zpočátku	k6eAd1	zpočátku
soudili	soudit	k5eAaImAgMnP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hledaný	hledaný	k2eAgInSc4d1	hledaný
průliv	průliv	k1gInSc4	průliv
do	do	k7c2	do
"	"	kIx"	"
<g/>
Jižního	jižní	k2eAgNnSc2d1	jižní
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
pronikli	proniknout	k5eAaPmAgMnP	proniknout
až	až	k9	až
do	do	k7c2	do
nehostinných	hostinný	k2eNgFnPc2d1	nehostinná
patagonských	patagonský	k2eAgFnPc2d1	patagonská
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1520	[number]	k4	1520
se	se	k3xPyFc4	se
Magalhã	Magalhã	k1gMnSc1	Magalhã
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
marných	marný	k2eAgInPc6d1	marný
bojích	boj	k1gInPc6	boj
se	s	k7c7	s
zimními	zimní	k2eAgFnPc7d1	zimní
bouřemi	bouř	k1gFnPc7	bouř
stráví	strávit	k5eAaPmIp3nP	strávit
zimu	zima	k1gFnSc4	zima
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
po	po	k7c6	po
sv.	sv.	kA	sv.
Juliánovi	Julián	k1gMnSc6	Julián
(	(	kIx(	(
<g/>
Puerto	Puerta	k1gFnSc5	Puerta
San	San	k1gFnPc7	San
Julián	Julián	k1gMnSc1	Julián
<g/>
,	,	kIx,	,
49	[number]	k4	49
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
propukla	propuknout	k5eAaPmAgFnS	propuknout
vzpoura	vzpoura	k1gFnSc1	vzpoura
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
organizátoři	organizátor	k1gMnPc1	organizátor
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
byli	být	k5eAaImAgMnP	být
vysazeni	vysazen	k2eAgMnPc1d1	vysazen
na	na	k7c4	na
patagonské	patagonský	k2eAgNnSc4d1	patagonské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1520	[number]	k4	1520
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
strávili	strávit	k5eAaPmAgMnP	strávit
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
na	na	k7c4	na
50	[number]	k4	50
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
také	také	k9	také
Magalhã	Magalhã	k1gMnSc1	Magalhã
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
loď	loď	k1gFnSc4	loď
–	–	k?	–
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
zničena	zničit	k5eAaPmNgNnP	zničit
v	v	k7c6	v
bouři	bouř	k1gFnSc6	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
,	,	kIx,	,
dezertovala	dezertovat	k5eAaBmAgFnS	dezertovat
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
nazpět	nazpět	k6eAd1	nazpět
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavního	hlavní	k2eAgMnSc2d1	hlavní
kapitána	kapitán	k1gMnSc2	kapitán
obvinila	obvinit	k5eAaPmAgFnS	obvinit
z	z	k7c2	z
kolaborace	kolaborace	k1gFnSc2	kolaborace
s	s	k7c7	s
Portugalci	Portugalec	k1gMnPc7	Portugalec
a	a	k8xC	a
vzpouře	vzpoura	k1gFnSc3	vzpoura
proti	proti	k7c3	proti
španělskému	španělský	k2eAgMnSc3d1	španělský
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
Magalhã	Magalhã	k1gMnSc4	Magalhã
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1520	[number]	k4	1520
objevili	objevit	k5eAaPmAgMnP	objevit
mezi	mezi	k7c7	mezi
Ohňovou	ohňový	k2eAgFnSc7d1	ohňová
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Chile	Chile	k1gNnSc6	Chile
ústí	ústí	k1gNnSc4	ústí
hledaného	hledaný	k2eAgInSc2d1	hledaný
průlivu	průliv	k1gInSc2	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Průliv	průliv	k1gInSc1	průliv
kapitán	kapitán	k1gMnSc1	kapitán
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
Kanál	kanál	k1gInSc4	kanál
všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
námořníky	námořník	k1gMnPc4	námořník
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
Magalhã	Magalhã	k1gMnSc1	Magalhã
průliv	průliv	k1gInSc1	průliv
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
nese	nést	k5eAaImIp3nS	nést
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Průliv	průliv	k1gInSc4	průliv
zdolali	zdolat	k5eAaPmAgMnP	zdolat
za	za	k7c4	za
38	[number]	k4	38
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
otázka	otázka	k1gFnSc1	otázka
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
i	i	k9	i
pro	pro	k7c4	pro
zkušené	zkušený	k2eAgMnPc4d1	zkušený
kapitány	kapitán	k1gMnPc4	kapitán
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
dnes	dnes	k6eAd1	dnes
obtížná	obtížný	k2eAgFnSc1d1	obtížná
plavba	plavba	k1gFnSc1	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
průlivem	průliv	k1gInSc7	průliv
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1520	[number]	k4	1520
propluly	proplout	k5eAaPmAgFnP	proplout
tři	tři	k4xCgFnPc4	tři
zbývající	zbývající	k2eAgFnPc4d1	zbývající
lodě	loď	k1gFnPc4	loď
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Magalhã	Magalhã	k?	Magalhã
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Moluky	Moluky	k1gFnPc4	Moluky
bude	být	k5eAaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
už	už	k6eAd1	už
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
výpravě	výprava	k1gFnSc6	výprava
počasí	počasí	k1gNnSc1	počasí
přálo	přát	k5eAaImAgNnS	přát
–	–	k?	–
vál	vát	k5eAaImAgInS	vát
příznivý	příznivý	k2eAgInSc1d1	příznivý
vítr	vítr	k1gInSc1	vítr
a	a	k8xC	a
nepotkala	potkat	k5eNaPmAgFnS	potkat
je	být	k5eAaImIp3nS	být
ani	ani	k8xC	ani
žádná	žádný	k3yNgFnSc1	žádný
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
Jižní	jižní	k2eAgNnSc1d1	jižní
moře	moře	k1gNnSc1	moře
Magellanem	Magellan	k1gMnSc7	Magellan
překřtěno	překřtít	k5eAaPmNgNnS	překřtít
na	na	k7c4	na
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
El	Ela	k1gFnPc2	Ela
mar	mar	k?	mar
pacífico	pacífico	k1gNnSc4	pacífico
=	=	kIx~	=
klidné	klidný	k2eAgNnSc4d1	klidné
<g/>
,	,	kIx,	,
tiché	tichý	k2eAgNnSc4d1	tiché
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pluli	plout	k5eAaImAgMnP	plout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
z	z	k7c2	z
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tak	tak	k9	tak
nešťastně	šťastně	k6eNd1	šťastně
<g/>
,	,	kIx,	,
že	že	k8xS	že
neviděli	vidět	k5eNaImAgMnP	vidět
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
skoro	skoro	k6eAd1	skoro
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
nenarazila	narazit	k5eNaPmAgFnS	narazit
na	na	k7c4	na
kousek	kousek	k1gInSc4	kousek
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
posádku	posádka	k1gFnSc4	posádka
trápil	trápit	k5eAaImAgInS	trápit
hlad	hlad	k1gInSc1	hlad
<g/>
,	,	kIx,	,
žízeň	žízeň	k1gFnSc1	žízeň
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
mužstva	mužstvo	k1gNnSc2	mužstvo
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
kurdějemi	kurděje	k1gFnPc7	kurděje
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
strádání	strádání	k1gNnSc4	strádání
popsal	popsat	k5eAaPmAgMnS	popsat
Antonio	Antonio	k1gMnSc1	Antonio
Pigafetta	Pigafetta	k1gMnSc1	Pigafetta
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Minuli	minout	k5eAaImAgMnP	minout
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
skalnaté	skalnatý	k2eAgInPc4d1	skalnatý
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
rovníku	rovník	k1gInSc2	rovník
změnili	změnit	k5eAaPmAgMnP	změnit
kurz	kurz	k1gInSc4	kurz
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
naštěstí	naštěstí	k6eAd1	naštěstí
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1521	[number]	k4	1521
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
Marianské	Marianský	k2eAgInPc4d1	Marianský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nalezli	nalézt	k5eAaBmAgMnP	nalézt
spoustu	spousta	k1gFnSc4	spousta
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc2	pití
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
výprava	výprava	k1gMnSc1	výprava
opět	opět	k6eAd1	opět
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
dorazili	dorazit	k5eAaPmAgMnP	dorazit
námořníci	námořník	k1gMnPc1	námořník
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Homonhon	Homonhona	k1gFnPc2	Homonhona
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
měla	mít	k5eAaImAgFnS	mít
výprava	výprava	k1gFnSc1	výprava
150	[number]	k4	150
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magalhã	Magalhã	k?	Magalhã
mohl	moct	k5eAaImAgInS	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
se	s	k7c7	s
zdejšími	zdejší	k2eAgMnPc7d1	zdejší
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
otrok	otrok	k1gMnSc1	otrok
–	–	k?	–
malajský	malajský	k2eAgMnSc1d1	malajský
tlumočník	tlumočník	k1gMnSc1	tlumočník
–	–	k?	–
se	se	k3xPyFc4	se
uměl	umět	k5eAaImAgMnS	umět
domluvit	domluvit	k5eAaPmF	domluvit
jejich	jejich	k3xOp3gInSc7	jejich
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
dary	dar	k1gInPc4	dar
s	s	k7c7	s
králem	král	k1gMnSc7	král
Limasawy	Limasawa	k1gFnSc2	Limasawa
rádžou	rádža	k1gMnSc7	rádža
Ka	Ka	k1gMnSc7	Ka
Lambem	Lamb	k1gMnSc7	Lamb
<g/>
.	.	kIx.	.
</s>
<s>
Ka	Ka	k?	Ka
Lambo	Lamba	k1gMnSc5	Lamba
je	on	k3xPp3gInPc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Cebu	Cebus	k1gInSc2	Cebus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1521	[number]	k4	1521
<g/>
.	.	kIx.	.
</s>
<s>
Přivítal	přivítat	k5eAaPmAgMnS	přivítat
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgMnSc1d1	zdejší
král	král	k1gMnSc1	král
rádža	rádža	k1gMnSc1	rádža
Humabon	Humabon	k1gMnSc1	Humabon
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
Magalhã	Magalhã	k1gMnSc1	Magalhã
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
sousednímu	sousední	k2eAgInSc3d1	sousední
kmenu	kmen	k1gInSc3	kmen
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Mactan	Mactan	k1gInSc1	Mactan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1521	[number]	k4	1521
byl	být	k5eAaImAgInS	být
Magalhã	Magalhã	k1gFnSc2	Magalhã
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Mactan	Mactan	k1gInSc1	Mactan
zabit	zabit	k2eAgInSc1d1	zabit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výprava	výprava	k1gMnSc1	výprava
po	po	k7c6	po
Magalhã	Magalhã	k1gFnSc6	Magalhã
smrti	smrt	k1gFnSc2	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postavil	postavit	k5eAaPmAgMnS	postavit
Juan	Juan	k1gMnSc1	Juan
Sebastián	Sebastián	k1gMnSc1	Sebastián
del	del	k?	del
Cano	Cano	k1gMnSc1	Cano
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
kormidelník	kormidelník	k1gMnSc1	kormidelník
lodi	loď	k1gFnSc2	loď
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musel	muset	k5eAaImAgInS	muset
rychle	rychle	k6eAd1	rychle
prchnout	prchnout	k5eAaPmF	prchnout
před	před	k7c7	před
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ztratil	ztratit	k5eAaPmAgMnS	ztratit
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
třetí	třetí	k4xOgFnSc4	třetí
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
Concepción	Concepción	k1gInSc4	Concepción
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
spálena	spálit	k5eAaPmNgFnS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
na	na	k7c4	na
Borneo	Borneo	k1gNnSc4	Borneo
a	a	k8xC	a
pak	pak	k6eAd1	pak
Moluky	Moluky	k1gFnPc1	Moluky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nabrali	nabrat	k5eAaPmAgMnP	nabrat
koření	koření	k1gNnSc4	koření
pro	pro	k7c4	pro
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zchátralá	zchátralý	k2eAgFnSc1d1	zchátralá
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
loď	loď	k1gFnSc1	loď
Trinidad	Trinidad	k1gInSc4	Trinidad
náklad	náklad	k1gInSc4	náklad
neunesla	unést	k5eNaPmAgFnS	unést
a	a	k8xC	a
prohnilý	prohnilý	k2eAgInSc1d1	prohnilý
trup	trup	k1gInSc1	trup
praskl	prasknout	k5eAaPmAgInS	prasknout
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
vytažena	vytáhnout	k5eAaPmNgFnS	vytáhnout
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
opravována	opravován	k2eAgFnSc1d1	opravována
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vydala	vydat	k5eAaPmAgFnS	vydat
jen	jen	k6eAd1	jen
Victoria	Victorium	k1gNnPc1	Victorium
s	s	k7c7	s
padesátkou	padesátka	k1gFnSc7	padesátka
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Trinidad	Trinidad	k1gInSc4	Trinidad
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
a	a	k8xC	a
pokusila	pokusit	k5eAaPmAgFnS	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
cestou	cesta	k1gFnSc7	cesta
zpět	zpět	k6eAd1	zpět
přes	přes	k7c4	přes
Pacifik	Pacifik	k1gInSc4	Pacifik
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
bouřím	bouř	k1gFnPc3	bouř
a	a	k8xC	a
nepříznivým	příznivý	k2eNgInPc3d1	nepříznivý
větrům	vítr	k1gInPc3	vítr
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
navrátit	navrátit	k5eAaPmF	navrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Moluky	Moluky	k1gFnPc4	Moluky
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
však	však	k9	však
zajata	zajmout	k5eAaPmNgFnS	zajmout
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
hlídkou	hlídka	k1gFnSc7	hlídka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
zajetí	zajetí	k1gNnSc2	zajetí
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
vrátil	vrátit	k5eAaPmAgInS	vrátit
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
pouze	pouze	k6eAd1	pouze
kapitán	kapitán	k1gMnSc1	kapitán
Gonzales	Gonzales	k1gMnSc1	Gonzales
de	de	k?	de
Espinoza	Espinoza	k1gFnSc1	Espinoza
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Victorie	Victorie	k1gFnPc1	Victorie
plaví	plavit	k5eAaImIp3nP	plavit
vodami	voda	k1gFnPc7	voda
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
posledním	poslední	k2eAgMnSc6d1	poslední
<g/>
,	,	kIx,	,
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
úseku	úsek	k1gInSc6	úsek
cesty	cesta	k1gFnSc2	cesta
loď	loď	k1gFnSc4	loď
žádná	žádný	k3yNgFnSc1	žádný
pohroma	pohroma	k1gFnSc1	pohroma
nepotkala	potkat	k5eNaPmAgFnS	potkat
až	až	k9	až
ke	k	k7c3	k
Kapverdám	Kapverda	k1gFnPc3	Kapverda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
cestou	cesta	k1gFnSc7	cesta
zemřely	zemřít	k5eAaPmAgFnP	zemřít
další	další	k2eAgInPc1d1	další
dvě	dva	k4xCgFnPc1	dva
desítky	desítka	k1gFnPc1	desítka
vyčerpaných	vyčerpaný	k2eAgMnPc2d1	vyčerpaný
námořníků	námořník	k1gMnPc2	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kapverdách	Kapverda	k1gFnPc6	Kapverda
padlo	padnout	k5eAaPmAgNnS	padnout
třináct	třináct	k4xCc1	třináct
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
portugalského	portugalský	k2eAgNnSc2d1	portugalské
zajetí	zajetí	k1gNnSc2	zajetí
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
propuštěni	propuštěn	k2eAgMnPc1d1	propuštěn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
kapitán	kapitán	k1gMnSc1	kapitán
del	del	k?	del
Cano	Cano	k6eAd1	Cano
jen	jen	k9	jen
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
s	s	k7c7	s
18	[number]	k4	18
muži	muž	k1gMnPc7	muž
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1522	[number]	k4	1522
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
cesta	cesta	k1gFnSc1	cesta
trvala	trvat	k5eAaImAgFnS	trvat
3	[number]	k4	3
roky	rok	k1gInPc4	rok
a	a	k8xC	a
29	[number]	k4	29
dní	den	k1gInPc2	den
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
výpravy	výprava	k1gFnSc2	výprava
==	==	k?	==
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nad	nad	k7c7	nad
návratem	návrat	k1gInSc7	návrat
výpravy	výprava	k1gFnSc2	výprava
zaradoval	zaradovat	k5eAaPmAgInS	zaradovat
(	(	kIx(	(
<g/>
ostatně	ostatně	k6eAd1	ostatně
prodejem	prodej	k1gInSc7	prodej
přivezeného	přivezený	k2eAgInSc2d1	přivezený
hřebíčku	hřebíček	k1gInSc2	hřebíček
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vrátil	vrátit	k5eAaPmAgInS	vrátit
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
vložených	vložený	k2eAgFnPc2d1	vložená
investic	investice	k1gFnPc2	investice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
neubránil	ubránit	k5eNaPmAgMnS	ubránit
jistému	jistý	k2eAgNnSc3d1	jisté
zklamání	zklamání	k1gNnSc3	zklamání
<g/>
:	:	kIx,	:
Magalhã	Magalhã	k1gFnSc1	Magalhã
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Pacifik	Pacifik	k1gInSc4	Pacifik
na	na	k7c4	na
Moluky	Moluky	k1gFnPc4	Moluky
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
nesnadnější	snadný	k2eNgFnSc1d2	nesnadnější
než	než	k8xS	než
cesta	cesta	k1gFnSc1	cesta
Portugalců	Portugalec	k1gMnPc2	Portugalec
kolem	kolem	k7c2	kolem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
nejprve	nejprve	k6eAd1	nejprve
pokusil	pokusit	k5eAaPmAgMnS	pokusit
ještě	ještě	k9	ještě
několikrát	několikrát	k6eAd1	několikrát
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
navázat	navázat	k5eAaPmF	navázat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1525	[number]	k4	1525
vyslal	vyslat	k5eAaPmAgMnS	vyslat
flotilu	flotila	k1gFnSc4	flotila
sedmi	sedm	k4xCc2	sedm
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
plul	plout	k5eAaImAgInS	plout
i	i	k9	i
del	del	k?	del
Cano	Cano	k1gNnSc1	Cano
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kvůli	kvůli	k7c3	kvůli
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
počasí	počasí	k1gNnSc2	počasí
postupně	postupně	k6eAd1	postupně
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
smlouvou	smlouva	k1gFnSc7	smlouva
v	v	k7c6	v
Zaragoze	Zaragoz	k1gInSc5	Zaragoz
Moluk	Moluky	k1gFnPc2	Moluky
vzdal	vzdát	k5eAaPmAgMnS	vzdát
–	–	k?	–
za	za	k7c7	za
350.000	[number]	k4	350.000
dukátů	dukát	k1gInPc2	dukát
posunul	posunout	k5eAaPmAgInS	posunout
demarkační	demarkační	k2eAgFnSc4d1	demarkační
linii	linie	k1gFnSc4	linie
o	o	k7c4	o
17	[number]	k4	17
<g/>
°	°	k?	°
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výprava	výprava	k1gMnSc1	výprava
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
geografických	geografický	k2eAgFnPc2d1	geografická
vědomostí	vědomost	k1gFnPc2	vědomost
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
rozměry	rozměr	k1gInPc4	rozměr
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magalhã	Magalhã	k?	Magalhã
výpravu	výprava	k1gFnSc4	výprava
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
zopakoval	zopakovat	k5eAaPmAgInS	zopakovat
až	až	k9	až
roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
Angličan	Angličan	k1gMnSc1	Angličan
Francis	Francis	k1gFnSc4	Francis
Drake	Drak	k1gFnSc2	Drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Magalhã	Magalhã	k1gFnSc1	Magalhã
v	v	k7c6	v
letech	let	k1gInPc6	let
1521	[number]	k4	1521
<g/>
–	–	k?	–
<g/>
1522	[number]	k4	1522
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Filipín	Filipíny	k1gFnPc2	Filipíny
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
skutečně	skutečně	k6eAd1	skutečně
prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obeplul	obeplout	k5eAaPmAgMnS	obeplout
svět	svět	k1gInSc4	svět
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
splnil	splnit	k5eAaPmAgInS	splnit
hlavní	hlavní	k2eAgInSc1d1	hlavní
úkol	úkol	k1gInSc1	úkol
<g/>
:	:	kIx,	:
nalezl	nalézt	k5eAaBmAgInS	nalézt
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Ostrovům	ostrov	k1gInPc3	ostrov
koření	koření	k1gNnSc2	koření
plavbou	plavba	k1gFnSc7	plavba
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
uznáván	uznáván	k2eAgMnSc1d1	uznáván
i	i	k8xC	i
svými	svůj	k3xOyFgMnPc7	svůj
námořníky	námořník	k1gMnPc7	námořník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BOMBARD	bombarda	k1gFnPc2	bombarda
<g/>
,	,	kIx,	,
Alain	Alaina	k1gFnPc2	Alaina
<g/>
.	.	kIx.	.
</s>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
JANÁČEK	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
zámořských	zámořský	k2eAgInPc2d1	zámořský
objevů	objev	k1gInPc2	objev
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
<g/>
–	–	k?	–
<g/>
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KNOX-JOHNSTON	KNOX-JOHNSTON	k?	KNOX-JOHNSTON
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
<g/>
.	.	kIx.	.
</s>
<s>
Mys	mys	k1gInSc1	mys
Horn	Horn	k1gMnSc1	Horn
–	–	k?	–
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
námořnictví	námořnictví	k1gNnSc2	námořnictví
<g/>
.	.	kIx.	.
</s>
<s>
Trango	Trango	k1gMnSc1	Trango
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OPATRNÝ	opatrný	k2eAgMnSc1d1	opatrný
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
plavba	plavba	k1gFnSc1	plavba
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
5	[number]	k4	5
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
218	[number]	k4	218
<g/>
–	–	k?	–
<g/>
223	[number]	k4	223
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PATOČKA	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Plachty	plachta	k1gFnPc1	plachta
objevují	objevovat	k5eAaImIp3nP	objevovat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PATOČKA	Patočka	k1gMnSc1	Patočka
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Válečníci	válečník	k1gMnPc1	válečník
pod	pod	k7c7	pod
plachtami	plachta	k1gFnPc7	plachta
korábů	koráb	k1gInPc2	koráb
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PIGAFETTA	PIGAFETTA	kA	PIGAFETTA
<g/>
,	,	kIx,	,
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
SKŘIVAN	Skřivan	k1gMnSc1	Skřivan
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
KŘIVSKÝ	KŘIVSKÝ	kA	KŘIVSKÝ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
objevy	objev	k1gInPc1	objev
<g/>
,	,	kIx,	,
staletí	staletí	k1gNnPc1	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
VODÁK	Vodák	k1gMnSc1	Vodák
<g/>
,	,	kIx,	,
J.	J.	kA	J.
B.	B.	kA	B.
Příběhy	příběh	k1gInPc1	příběh
sedmi	sedm	k4xCc2	sedm
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweiga	k1gFnPc2	Zweiga
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Mann	Mann	k1gMnSc1	Mann
und	und	k?	und
Seine	Sein	k1gMnSc5	Sein
Tat	Tat	k1gMnSc5	Tat
–	–	k?	–
životopis	životopis	k1gInSc4	životopis
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Magellana	Magellan	k1gMnSc2	Magellan
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fernã	Fernã	k1gMnSc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gMnSc2	Magalhã
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Fernã	Fernã	k1gFnSc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gFnSc2	Magalhã
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Fernã	Fernã	k1gFnSc1	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gFnSc2	Magalhã
</s>
</p>
