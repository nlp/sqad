<s>
Catfish	Catfish	k1gInSc1	Catfish
Rising	Rising	k1gInSc1	Rising
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc4	album
britské	britský	k2eAgFnSc2d1	britská
progressive	progressiev	k1gFnSc2	progressiev
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tulla	k1gFnPc2	Tulla
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
a	a	k8xC	a
posledním	poslední	k2eAgNnSc6d1	poslední
bluesově	bluesově	k6eAd1	bluesově
laděným	laděný	k2eAgNnSc7d1	laděné
albem	album	k1gNnSc7	album
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tull	k1gInSc1	Tull
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
This	This	k1gInSc4	This
Was	Was	k1gMnPc2	Was
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
Jethro	Jethra	k1gFnSc5	Jethra
Tull	Tull	k1gMnSc1	Tull
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgMnSc1d1	nový
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
Andrew	Andrew	k1gFnSc2	Andrew
Giddings	Giddingsa	k1gFnPc2	Giddingsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
remasterované	remasterovaný	k2eAgNnSc1d1	remasterované
album	album	k1gNnSc1	album
s	s	k7c7	s
následujícími	následující	k2eAgFnPc7d1	následující
dodatečnými	dodatečný	k2eAgFnPc7d1	dodatečná
skladbami	skladba	k1gFnPc7	skladba
<g/>
:	:	kIx,	:
Night	Night	k1gInSc1	Night
in	in	k?	in
the	the	k?	the
Wilderness	Wilderness	k1gInSc1	Wilderness
B-strana	Btran	k1gMnSc2	B-stran
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
singlu	singl	k1gInSc2	singl
vydaného	vydaný	k2eAgInSc2d1	vydaný
v	v	k7c6	v
době	doba	k1gFnSc6	doba
éry	éra	k1gFnSc2	éra
Catfish	Catfish	k1gInSc1	Catfish
Rising	Rising	k1gInSc1	Rising
<g/>
.	.	kIx.	.
</s>
<s>
Jump	Jump	k1gInSc1	Jump
Start	start	k1gInSc1	start
(	(	kIx(	(
<g/>
live	live	k1gInSc1	live
<g/>
)	)	kIx)	)
Nahráno	nahrát	k5eAaPmNgNnS	nahrát
v	v	k7c6	v
The	The	k1gFnSc6	The
Tower	Tower	k1gMnSc1	Tower
Theater	Theater	k1gMnSc1	Theater
<g/>
,	,	kIx,	,
Upper	Upper	k1gMnSc1	Upper
Darby	Darba	k1gFnSc2	Darba
<g/>
,	,	kIx,	,
PA	Pa	kA	Pa
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadun	listopadun	k1gInSc1	listopadun
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
B-strana	Btrana	k1gFnSc1	B-strana
UK	UK	kA	UK
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
Not	nota	k1gFnPc2	nota
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ian	Ian	k1gMnSc1	Ian
Anderson	Anderson	k1gMnSc1	Anderson
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
akustická	akustický	k2eAgFnSc1d1	akustická
a	a	k8xC	a
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
perkusy	perkus	k1gInPc1	perkus
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
Martin	Martin	k1gMnSc1	Martin
Barre	Barr	k1gInSc5	Barr
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Dave	Dav	k1gInSc5	Dav
Pegg	Pegg	k1gInSc1	Pegg
-	-	kIx~	-
akustická	akustický	k2eAgFnSc1d1	akustická
a	a	k8xC	a
elektrická	elektrický	k2eAgFnSc1d1	elektrická
baskytara	baskytara	k1gFnSc1	baskytara
Doane	Doan	k1gInSc5	Doan
<g />
.	.	kIx.	.
</s>
<s>
Perry	Perra	k1gFnPc4	Perra
-	-	kIx~	-
bicí	bicí	k2eAgMnPc1d1	bicí
Dále	daleko	k6eAd2	daleko
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
<g/>
:	:	kIx,	:
Andy	Anda	k1gFnPc4	Anda
Giddings	Giddings	k1gInSc1	Giddings
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
na	na	k7c6	na
stopách	stopa	k1gFnPc6	stopa
1	[number]	k4	1
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
8	[number]	k4	8
Foss	Foss	k1gInSc1	Foss
Peterson	Peterson	k1gInSc4	Peterson
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
10	[number]	k4	10
John	John	k1gMnSc1	John
Bundrick	Bundrick	k1gMnSc1	Bundrick
-	-	kIx~	-
klávesy	kláves	k1gInPc1	kláves
na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
11	[number]	k4	11
Matt	Mattum	k1gNnPc2	Mattum
Pegg	Pegga	k1gFnPc2	Pegga
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
na	na	k7c6	na
stopách	stopa	k1gFnPc6	stopa
1	[number]	k4	1
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
"	"	kIx"	"
<g/>
Roll	Roll	k1gMnSc1	Roll
Yer	Yer	k1gMnSc1	Yer
Own	Own	k1gMnSc1	Own
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
písnička	písnička	k1gFnSc1	písnička
o	o	k7c6	o
ženské	ženský	k2eAgFnSc6d1	ženská
masturbaci	masturbace	k1gFnSc6	masturbace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
vydáno	vydat	k5eAaPmNgNnS	vydat
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
Ian	Ian	k1gMnSc1	Ian
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
amerického	americký	k2eAgNnSc2d1	americké
rádia	rádio	k1gNnSc2	rádio
<g/>
,	,	kIx,	,
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Upclose	Upclos	k1gInSc5	Upclos
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pořad	pořad	k1gInSc1	pořad
byl	být	k5eAaImAgInS	být
nahrán	nahrát	k5eAaBmNgInS	nahrát
a	a	k8xC	a
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
CD	CD	kA	CD
(	(	kIx(	(
<g/>
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
edici	edice	k1gFnSc6	edice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pořadu	pořad	k1gInSc2	pořad
Ian	Ian	k1gMnSc1	Ian
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
pár	pár	k4xCyI	pár
týdny	týden	k1gInPc7	týden
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
velice	velice	k6eAd1	velice
trapné	trapný	k2eAgFnSc6d1	trapná
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
když	když	k8xS	když
dělal	dělat	k5eAaImAgInS	dělat
pořad	pořad	k1gInSc1	pořad
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc4	radio
2	[number]	k4	2
stanici	stanice	k1gFnSc3	stanice
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Ian	Ian	k1gMnSc1	Ian
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
cílena	cílen	k2eAgFnSc1d1	cílena
na	na	k7c4	na
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
mohu	moct	k5eAaImIp1nS	moct
odvážit	odvážit	k5eAaPmF	odvážit
říct	říct	k5eAaPmF	říct
a	a	k8xC	a
nemám	mít	k5eNaImIp1nS	mít
rád	rád	k6eAd1	rád
to	ten	k3xDgNnSc4	ten
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc4	žena
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gloria	Gloria	k1gFnSc1	Gloria
Hunniford	Hunniford	k1gInSc1	Hunniford
<g/>
,	,	kIx,	,
hostitelka	hostitelka	k1gFnSc1	hostitelka
toho	ten	k3xDgInSc2	ten
pořadu	pořad	k1gInSc2	pořad
<g/>
,	,	kIx,	,
nechtěla	chtít	k5eNaImAgFnS	chtít
hrát	hrát	k5eAaImF	hrát
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
Not	nota	k1gFnPc2	nota
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
že	že	k8xS	že
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
tak	tak	k9	tak
trochu	trochu	k6eAd1	trochu
příliš	příliš	k6eAd1	příliš
rockové	rockový	k2eAgInPc1d1	rockový
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
pořad	pořad	k1gInSc4	pořad
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
radši	rád	k6eAd2	rád
hrála	hrát	k5eAaImAgFnS	hrát
"	"	kIx"	"
<g/>
Roll	Roll	k1gMnSc1	Roll
Yer	Yer	k1gMnSc1	Yer
Own	Own	k1gMnSc1	Own
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
požádala	požádat	k5eAaPmAgFnS	požádat
Iana	Ianus	k1gMnSc4	Ianus
"	"	kIx"	"
<g/>
aby	aby	k9	aby
[	[	kIx(	[
<g/>
jí	jíst	k5eAaImIp3nS	jíst
<g/>
]	]	kIx)	]
řekl	říct	k5eAaPmAgMnS	říct
o	o	k7c6	o
čem	co	k3yQnSc6	co
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
snažil	snažit	k5eAaImAgMnS	snažit
vyhnout	vyhnout	k5eAaPmF	vyhnout
a	a	k8xC	a
dělal	dělat	k5eAaImAgInS	dělat
zoufalá	zoufalý	k2eAgNnPc4d1	zoufalé
gesta	gesto	k1gNnPc4	gesto
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
,	,	kIx,	,
zkoušejíc	zkoušet	k5eAaImSgNnS	zkoušet
změnit	změnit	k5eAaPmF	změnit
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
Nechme	nechat	k5eAaPmRp1nP	nechat
toho	ten	k3xDgMnSc4	ten
a	a	k8xC	a
pusťme	pustit	k5eAaPmRp1nP	pustit
si	se	k3xPyFc3	se
radši	rád	k6eAd2	rád
písničku	písnička	k1gFnSc4	písnička
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
Gloria	Gloria	k1gFnSc1	Gloria
se	se	k3xPyFc4	se
vytrvale	vytrvale	k6eAd1	vytrvale
držela	držet	k5eAaImAgFnS	držet
svého	svůj	k3xOyFgMnSc4	svůj
<g/>
,	,	kIx,	,
až	až	k9	až
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
docela	docela	k6eAd1	docela
rozhozená	rozhozený	k2eAgFnSc1d1	rozhozená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc3	on
Ian	Ian	k1gFnSc3	Ian
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
konečně	konečně	k6eAd1	konečně
Ian	Ian	k1gMnSc1	Ian
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Glorie	Glorie	k1gFnSc1	Glorie
<g/>
,	,	kIx,	,
promiň	prominout	k5eAaPmRp2nS	prominout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dohnala	dohnat	k5eAaPmAgFnS	dohnat
jsi	být	k5eAaImIp2nS	být
mě	já	k3xPp1nSc4	já
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
-	-	kIx~	-
zoufale	zoufale	k6eAd1	zoufale
hledám	hledat	k5eAaImIp1nS	hledat
vhodné	vhodný	k2eAgNnSc1d1	vhodné
slovo	slovo	k1gNnSc1	slovo
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
slovníku	slovník	k1gInSc6	slovník
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
nemusel	muset	k5eNaImAgMnS	muset
v	v	k7c6	v
živém	živé	k1gNnSc6	živé
vysílání	vysílání	k1gNnSc2	vysílání
radia	radio	k1gNnSc2	radio
BBC	BBC	kA	BBC
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
denní	denní	k2eAgFnSc4d1	denní
dobu	doba	k1gFnSc4	doba
použít	použít	k5eAaPmF	použít
slovo	slovo	k1gNnSc4	slovo
ženská	ženský	k2eAgFnSc1d1	ženská
masturbace	masturbace	k1gFnSc1	masturbace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gloria	Gloria	k1gFnSc1	Gloria
se	se	k3xPyFc4	se
naštvala	naštvat	k5eAaBmAgFnS	naštvat
a	a	k8xC	a
zakřičela	zakřičet	k5eAaPmAgFnS	zakřičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cože	cože	k6eAd1	cože
<g/>
?	?	kIx.	?
</s>
<s>
Vypadni	vypadnout	k5eAaPmRp2nS	vypadnout
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodila	vyhodit	k5eAaPmAgFnS	vyhodit
Iana	Ianus	k1gMnSc4	Ianus
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
i	i	k9	i
přes	přes	k7c4	přes
tu	ten	k3xDgFnSc4	ten
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
sama	sám	k3xTgFnSc1	sám
donutila	donutit	k5eAaPmAgFnS	donutit
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
