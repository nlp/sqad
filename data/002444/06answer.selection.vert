<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
zvaný	zvaný	k2eAgInSc1d1	zvaný
Spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
(	(	kIx(	(
<g/>
1138	[number]	k4	1138
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1194	[number]	k4	1194
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
polský	polský	k2eAgInSc4d1	polský
kníže-senior	knížeenior	k1gInSc4	kníže-senior
vládnoucí	vládnoucí	k2eAgMnPc4d1	vládnoucí
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1177	[number]	k4	1177
až	až	k9	až
1194	[number]	k4	1194
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
sandoměřský	sandoměřský	k2eAgMnSc1d1	sandoměřský
<g/>
,	,	kIx,	,
mazovský	mazovský	k2eAgInSc1d1	mazovský
<g/>
,	,	kIx,	,
kališský	kališský	k2eAgInSc1d1	kališský
a	a	k8xC	a
hnězdenský	hnězdenský	k2eAgInSc1d1	hnězdenský
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
