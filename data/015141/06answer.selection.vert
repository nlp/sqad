<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
windsorské	windsorský	k2eAgFnSc2d1	Windsorská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Philipa	Philip	k1gMnSc4	Philip
<g/>
,	,	kIx,	,
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
