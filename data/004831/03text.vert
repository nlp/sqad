<s>
GoHands	GoHands	k1gInSc1	GoHands
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
založené	založený	k2eAgNnSc4d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bývalými	bývalý	k2eAgMnPc7d1	bývalý
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
studia	studio	k1gNnSc2	studio
Satelight	Satelight	k1gInSc1	Satelight
<g/>
.	.	kIx.	.
</s>
<s>
Princess	Princess	k6eAd1	Princess
Lover	Lover	k1gInSc1	Lover
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Čeburaška	Čeburašek	k1gInSc2	Čeburašek
arere	arer	k1gMnSc5	arer
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Seitokai	Seitoka	k1gFnPc4	Seitoka
jakuindomo	jakuindomo	k6eAd1	jakuindomo
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
K	k	k7c3	k
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Coppelion	Coppelion	k1gInSc1	Coppelion
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Seitokai	Seitoka	k1gFnPc4	Seitoka
jakuindomo	jakuindomo	k6eAd1	jakuindomo
<g/>
*	*	kIx~	*
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
K	k	k7c3	k
<g/>
:	:	kIx,	:
Return	Return	k1gMnSc1	Return
of	of	k?	of
Kings	Kings	k1gInSc1	Kings
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Hand	Hand	k1gInSc1	Hand
Shakers	Shakers	k1gInSc1	Shakers
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
K	k	k7c3	k
<g/>
:	:	kIx,	:
Seven	Seven	k2eAgMnSc1d1	Seven
Stories	Stories	k1gMnSc1	Stories
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Seitokai	Seitoka	k1gFnPc4	Seitoka
jakuindomo	jakuindomo	k6eAd1	jakuindomo
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Asa	Asa	k1gFnSc1	Asa
made	mad	k1gFnSc2	mad
džugjó	džugjó	k?	džugjó
ču	ču	k?	ču
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Mardock	Mardock	k1gMnSc1	Mardock
Scramble	Scramble	k1gMnSc1	Scramble
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
First	First	k1gMnSc1	First
Compression	Compression	k1gInSc1	Compression
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Mardock	Mardock	k1gMnSc1	Mardock
Scramble	Scramble	k1gMnSc1	Scramble
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Second	Second	k1gMnSc1	Second
Combustion	Combustion	k1gInSc1	Combustion
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Mardock	Mardock	k1gMnSc1	Mardock
Scramble	Scramble	k1gMnSc1	Scramble
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Third	Third	k1gMnSc1	Third
Exhaust	Exhaust	k1gMnSc1	Exhaust
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
K	k	k7c3	k
<g/>
:	:	kIx,	:
Missing	Missing	k1gInSc1	Missing
Kings	Kings	k1gInSc1	Kings
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
