<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
severo	severo	k1gNnSc1	severo
<g/>
)	)	kIx)	)
<g/>
evropský	evropský	k2eAgMnSc1d1	evropský
<g/>
,	,	kIx,	,
především	především	k9	především
německý	německý	k2eAgInSc4d1	německý
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
protiklad	protiklad	k1gInSc1	protiklad
impresionismu	impresionismus	k1gInSc2	impresionismus
a	a	k8xC	a
naturalismu	naturalismus	k1gInSc2	naturalismus
tzn.	tzn.	kA	tzn.
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
umění	umění	k1gNnSc4	umění
zobrazující	zobrazující	k2eAgFnSc1d1	zobrazující
skutečnost	skutečnost	k1gFnSc1	skutečnost
i	i	k8xC	i
umění	umění	k1gNnSc1	umění
snažící	snažící	k2eAgNnSc1d1	snažící
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
pomocí	pomocí	k7c2	pomocí
dojmů	dojem	k1gInPc2	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vlastní	vlastní	k2eAgInPc4d1	vlastní
prožitky	prožitek	k1gInPc4	prožitek
a	a	k8xC	a
vlastní	vlastní	k2eAgInPc4d1	vlastní
pocity	pocit	k1gInPc4	pocit
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
konvence	konvence	k1gFnPc4	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
efektu	efekt	k1gInSc2	efekt
prožitku	prožitek	k1gInSc2	prožitek
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
expresionismu	expresionismus	k1gInSc2	expresionismus
bylo	být	k5eAaImAgNnS	být
zavrhnout	zavrhnout	k5eAaPmF	zavrhnout
atavismy	atavismus	k1gInPc4	atavismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
romantismus	romantismus	k1gInSc1	romantismus
<g/>
,	,	kIx,	,
akademismus	akademismus	k1gInSc1	akademismus
<g/>
,	,	kIx,	,
realistickou	realistický	k2eAgFnSc4d1	realistická
líbivost	líbivost	k1gFnSc4	líbivost
a	a	k8xC	a
maloměšťáctví	maloměšťáctví	k1gNnSc4	maloměšťáctví
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
civilizační	civilizační	k2eAgFnSc4d1	civilizační
krizi	krize	k1gFnSc4	krize
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
-	-	kIx~	-
pocity	pocit	k1gInPc1	pocit
úpadku	úpadek	k1gInSc2	úpadek
<g/>
,	,	kIx,	,
marnosti	marnost	k1gFnSc2	marnost
<g/>
,	,	kIx,	,
zkaženosti	zkaženost	k1gFnSc2	zkaženost
a	a	k8xC	a
beznaděje	beznaděj	k1gFnSc2	beznaděj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacistů	nacista	k1gMnPc2	nacista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
degenerovaný	degenerovaný	k2eAgInSc4d1	degenerovaný
směr	směr	k1gInSc4	směr
a	a	k8xC	a
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
kultu	kult	k1gInSc2	kult
nového	nový	k2eAgMnSc2d1	nový
německého	německý	k2eAgMnSc2d1	německý
nadčlověka	nadčlověk	k1gMnSc2	nadčlověk
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
realistickým	realistický	k2eAgInSc7d1	realistický
monumentálním	monumentální	k2eAgInSc7d1	monumentální
stylem	styl	k1gInSc7	styl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
expresionistická	expresionistický	k2eAgNnPc1d1	expresionistické
díla	dílo	k1gNnPc1	dílo
společně	společně	k6eAd1	společně
s	s	k7c7	s
díly	díl	k1gInPc7	díl
ostatních	ostatní	k2eAgInPc2d1	ostatní
progresivních	progresivní	k2eAgInPc2d1	progresivní
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
směrů	směr	k1gInPc2	směr
či	či	k8xC	či
secese	secese	k1gFnSc1	secese
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
výstavu	výstava	k1gFnSc4	výstava
"	"	kIx"	"
<g/>
Zvrhlé	zvrhlý	k2eAgNnSc1d1	zvrhlé
umění	umění	k1gNnSc1	umění
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Entartete	Entartete	k1gMnSc1	Entartete
Kunst	Kunst	k1gMnSc1	Kunst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
putovala	putovat	k5eAaImAgFnS	putovat
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zesměšnit	zesměšnit	k5eAaPmF	zesměšnit
modernistické	modernistický	k2eAgMnPc4d1	modernistický
představitele	představitel	k1gMnPc4	představitel
a	a	k8xC	a
zejména	zejména	k9	zejména
dílo	dílo	k1gNnSc1	dílo
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Nezřídka	nezřídka	k6eAd1	nezřídka
však	však	k9	však
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pravý	pravý	k2eAgInSc4d1	pravý
opak	opak	k1gInSc4	opak
<g/>
.	.	kIx.	.
</s>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
základů	základ	k1gInPc2	základ
jako	jako	k8xS	jako
fauvismus	fauvismus	k1gInSc4	fauvismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
počátky	počátek	k1gInPc4	počátek
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
expresionismus	expresionismus	k1gInSc1	expresionismus
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
především	především	k9	především
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
fauvismus	fauvismus	k1gInSc1	fauvismus
byl	být	k5eAaImAgInS	být
populární	populární	k2eAgMnSc1d1	populární
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převažoval	převažovat	k5eAaImAgInS	převažovat
její	její	k3xOp3gInSc4	její
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
expresionismu	expresionismus	k1gInSc2	expresionismus
započal	započnout	k5eAaPmAgInS	započnout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
začala	začít	k5eAaPmAgFnS	začít
stoupat	stoupat	k5eAaImF	stoupat
během	běh	k1gInSc7	běh
a	a	k8xC	a
především	především	k9	především
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
především	především	k9	především
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
nastaly	nastat	k5eAaPmAgInP	nastat
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
mimořádně	mimořádně	k6eAd1	mimořádně
příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
poražená	poražený	k2eAgFnSc1d1	poražená
země	země	k1gFnSc1	země
potýkala	potýkat	k5eAaImAgFnS	potýkat
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
psychickou	psychický	k2eAgFnSc7d1	psychická
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
mohl	moct	k5eAaImAgInS	moct
expresionismus	expresionismus	k1gInSc1	expresionismus
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vystihovat	vystihovat	k5eAaImF	vystihovat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rozmach	rozmach	k1gInSc1	rozmach
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
nastal	nastat	k5eAaPmAgInS	nastat
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
toto	tento	k3xDgNnSc1	tento
desetiletí	desetiletí	k1gNnSc1	desetiletí
bývá	bývat	k5eAaImIp3nS	bývat
nazýváno	nazývat	k5eAaImNgNnS	nazývat
expresionistickým	expresionistický	k2eAgInSc7d1	expresionistický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
vliv	vliv	k1gInSc1	vliv
expresionismu	expresionismus	k1gInSc2	expresionismus
zmenšoval	zmenšovat	k5eAaImAgInS	zmenšovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
umění	umění	k1gNnSc1	umění
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zakazováno	zakazován	k2eAgNnSc1d1	zakazováno
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
odstraňováno	odstraňován	k2eAgNnSc1d1	odstraňován
z	z	k7c2	z
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
nových	nový	k2eAgFnPc2d1	nová
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
zveřejňování	zveřejňování	k1gNnSc2	zveřejňování
materiálů	materiál	k1gInPc2	materiál
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
hnutí	hnutí	k1gNnSc6	hnutí
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
umělecká	umělecký	k2eAgFnSc1d1	umělecká
skupina	skupina	k1gFnSc1	skupina
Die	Die	k1gFnSc1	Die
Brücke	Brücke	k1gFnSc1	Brücke
(	(	kIx(	(
<g/>
Most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
neměla	mít	k5eNaImAgFnS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
expresionismus	expresionismus	k1gInSc4	expresionismus
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
r.	r.	kA	r.
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
expresionismem	expresionismus	k1gInSc7	expresionismus
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
spojena	spojit	k5eAaPmNgFnS	spojit
skupina	skupina	k1gFnSc1	skupina
Der	drát	k5eAaImRp2nS	drát
Blaue	Blaue	k1gNnSc4	Blaue
Reiter	Reiter	k1gInSc1	Reiter
(	(	kIx(	(
<g/>
Modrý	modrý	k2eAgInSc1d1	modrý
jezdec	jezdec	k1gInSc1	jezdec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
r.	r.	kA	r.
1911	[number]	k4	1911
a	a	k8xC	a
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
se	se	k3xPyFc4	se
v	v	k7c6	v
r.	r.	kA	r.
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
nejvíce	hodně	k6eAd3	hodně
prosadil	prosadit	k5eAaPmAgInS	prosadit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
proti	proti	k7c3	proti
měšťáckému	měšťácký	k2eAgInSc3d1	měšťácký
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Deformace	deformace	k1gFnSc1	deformace
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
zvýrazňováním	zvýrazňování	k1gNnSc7	zvýrazňování
vlastních	vlastní	k2eAgInPc2d1	vlastní
prožitků	prožitek	k1gInPc2	prožitek
byla	být	k5eAaImAgFnS	být
patrná	patrný	k2eAgFnSc1d1	patrná
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Poezie	poezie	k1gFnSc1	poezie
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Stearns	Stearnsa	k1gFnPc2	Stearnsa
Eliot	Eliot	k1gMnSc1	Eliot
Federico	Federico	k1gMnSc1	Federico
García	García	k1gMnSc1	García
Lorca	Lorca	k1gMnSc1	Lorca
Georg	Georg	k1gMnSc1	Georg
Trakl	Trakl	k1gInSc4	Trakl
Georg	Georg	k1gMnSc1	Georg
Heym	Heym	k1gMnSc1	Heym
Johannes	Johannes	k1gMnSc1	Johannes
Robert	Robert	k1gMnSc1	Robert
Becher	Bechra	k1gFnPc2	Bechra
Gottfried	Gottfried	k1gMnSc1	Gottfried
Benn	Benn	k1gMnSc1	Benn
Geo	Geo	k1gMnSc1	Geo
Milev	Mileva	k1gFnPc2	Mileva
Paul	Paul	k1gMnSc1	Paul
van	vana	k1gFnPc2	vana
Ostaijen	Ostaijna	k1gFnPc2	Ostaijna
Próza	próza	k1gFnSc1	próza
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
Drama	drama	k1gFnSc1	drama
<g/>
:	:	kIx,	:
Tennessee	Tennessee	k1gFnSc1	Tennessee
Williams	Williamsa	k1gFnPc2	Williamsa
Georg	Georg	k1gMnSc1	Georg
Kaiser	Kaiser	k1gMnSc1	Kaiser
Ernst	Ernst	k1gMnSc1	Ernst
Toller	Toller	k1gMnSc1	Toller
Federico	Federico	k1gMnSc1	Federico
García	García	k1gMnSc1	García
Lorca	Lorca	k1gMnSc1	Lorca
Do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
dostal	dostat	k5eAaPmAgInS	dostat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
shromážděnou	shromážděný	k2eAgFnSc7d1	shromážděná
kolem	kolem	k7c2	kolem
literárního	literární	k2eAgMnSc2d1	literární
kritika	kritik	k1gMnSc2	kritik
F.	F.	kA	F.
Götze	Götze	k1gFnSc2	Götze
<g/>
,	,	kIx,	,
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
časopisem	časopis	k1gInSc7	časopis
zabývajícím	zabývající	k2eAgMnSc7d1	zabývající
se	se	k3xPyFc4	se
expresionismem	expresionismus	k1gInSc7	expresionismus
byl	být	k5eAaImAgMnS	být
Host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
Lev	Lev	k1gMnSc1	Lev
Blatný	blatný	k2eAgMnSc1d1	blatný
František	František	k1gMnSc1	František
Götz	Götz	k1gMnSc1	Götz
Ladislav	Ladislav	k1gMnSc1	Ladislav
Klíma	Klíma	k1gMnSc1	Klíma
Richard	Richard	k1gMnSc1	Richard
Weiner	Weiner	k1gMnSc1	Weiner
Josef	Josef	k1gMnSc1	Josef
Váchal	Váchal	k1gMnSc1	Váchal
Drama	drama	k1gFnSc1	drama
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Hugo	Hugo	k1gMnSc1	Hugo
Hilar	Hilar	k1gMnSc1	Hilar
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
jménem	jméno	k1gNnSc7	jméno
K.	K.	kA	K.
H.	H.	kA	H.
Bakule	bakule	k1gFnSc1	bakule
Je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
psychické	psychický	k2eAgInPc4d1	psychický
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
deformací	deformace	k1gFnSc7	deformace
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc1	období
mělo	mít	k5eAaImAgNnS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
pravěké	pravěký	k2eAgNnSc1d1	pravěké
a	a	k8xC	a
lidové	lidový	k2eAgNnSc1d1	lidové
malířství	malířství	k1gNnSc1	malířství
<g/>
;	;	kIx,	;
dětská	dětský	k2eAgFnSc1d1	dětská
kresba	kresba	k1gFnSc1	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgFnPc1d1	barevná
plochy	plocha	k1gFnPc1	plocha
jsou	být	k5eAaImIp3nP	být
ohraničovány	ohraničovat	k5eAaImNgInP	ohraničovat
linkou	linka	k1gFnSc7	linka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používali	používat	k5eAaImAgMnP	používat
čisté	čistý	k2eAgFnPc4d1	čistá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
nesnažili	snažit	k5eNaImAgMnP	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
perspektivu	perspektiva	k1gFnSc4	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Využíval	využívat	k5eAaImAgInS	využívat
se	se	k3xPyFc4	se
protiklad	protiklad	k1gInSc1	protiklad
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
stínu	stín	k1gInSc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
expresionismu	expresionismus	k1gInSc2	expresionismus
byly	být	k5eAaImAgInP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
i	i	k8xC	i
neoimpresionismem	neoimpresionismus	k1gInSc7	neoimpresionismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
expresionismus	expresionismus	k1gInSc1	expresionismus
později	pozdě	k6eAd2	pozdě
zcela	zcela	k6eAd1	zcela
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
a	a	k8xC	a
vůči	vůči	k7c3	vůči
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
do	do	k7c2	do
protikladu	protiklad	k1gInSc2	protiklad
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stylu	styl	k1gInSc2	styl
tvorby	tvorba	k1gFnSc2	tvorba
lze	lze	k6eAd1	lze
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
expresionismu	expresionismus	k1gInSc2	expresionismus
považovat	považovat	k5eAaImF	považovat
Mathiase	Mathiasa	k1gFnSc3	Mathiasa
Grünewald	Grünewald	k1gMnSc1	Grünewald
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Mathis	Mathis	k1gInSc1	Mathis
Gothar	Gothar	k1gInSc1	Gothar
Neithart	Neithart	k1gInSc4	Neithart
asi	asi	k9	asi
1475	[number]	k4	1475
<g/>
-	-	kIx~	-
<g/>
1528	[number]	k4	1528
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Greco	Greco	k6eAd1	Greco
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Domenico	Domenico	k6eAd1	Domenico
Theotocopuli	Theotocopule	k1gFnSc4	Theotocopule
1541	[number]	k4	1541
<g/>
-	-	kIx~	-
<g/>
1614	[number]	k4	1614
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paula	Paula	k1gFnSc1	Paula
Modersohnová-Beckerová	Modersohnová-Beckerová	k1gFnSc1	Modersohnová-Beckerová
Max	Max	k1gMnSc1	Max
Beckmann	Beckmann	k1gMnSc1	Beckmann
Lovis	Lovis	k1gFnPc2	Lovis
Corinth	Corinth	k1gMnSc1	Corinth
Otto	Otto	k1gMnSc1	Otto
Dix	Dix	k1gMnSc1	Dix
George	Georg	k1gMnSc2	Georg
Grosz	Grosz	k1gMnSc1	Grosz
Erich	Erich	k1gMnSc1	Erich
Heckel	Heckel	k1gMnSc1	Heckel
Vasilij	Vasilij	k1gMnSc1	Vasilij
Kandinskij	Kandinskij	k1gMnSc1	Kandinskij
Ernst	Ernst	k1gMnSc1	Ernst
Ludwig	Ludwig	k1gMnSc1	Ludwig
Kirchner	Kirchner	k1gMnSc1	Kirchner
Paul	Paul	k1gMnSc1	Paul
Klee	Klee	k1gFnPc2	Klee
Oskar	Oskar	k1gMnSc1	Oskar
Kokoschka	Kokoschka	k1gMnSc1	Kokoschka
Franz	Franza	k1gFnPc2	Franza
Marc	Marc	k1gInSc1	Marc
August	August	k1gMnSc1	August
Macke	Macke	k1gFnPc2	Macke
Ludwig	Ludwig	k1gMnSc1	Ludwig
Meidner	Meidner	k1gMnSc1	Meidner
Otto	Otto	k1gMnSc1	Otto
Mueller	Mueller	k1gMnSc1	Mueller
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
Gabriele	Gabriela	k1gFnSc3	Gabriela
Münterová	Münterová	k1gFnSc1	Münterová
Emil	Emil	k1gMnSc1	Emil
Nolde	Nold	k1gInSc5	Nold
Max	max	kA	max
Pechstein	Pechstein	k2eAgMnSc1d1	Pechstein
Christian	Christian	k1gMnSc1	Christian
Rohlfs	Rohlfsa	k1gFnPc2	Rohlfsa
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
Arnold	Arnold	k1gMnSc1	Arnold
Schönberg	Schönberg	k1gMnSc1	Schönberg
Chaim	Chaim	k1gMnSc1	Chaim
Soutine	Soutin	k1gInSc5	Soutin
V	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
se	se	k3xPyFc4	se
expresionismus	expresionismus	k1gInSc1	expresionismus
prosadil	prosadit	k5eAaPmAgInS	prosadit
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
významní	významný	k2eAgMnPc1d1	významný
sochaři	sochař	k1gMnPc1	sochař
např.	např.	kA	např.
Ernst	Ernst	k1gMnSc1	Ernst
Barlach	Barlach	k1gMnSc1	Barlach
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgMnPc4d1	český
představitele	představitel	k1gMnPc4	představitel
hnutí	hnutí	k1gNnSc2	hnutí
soustředěné	soustředěný	k2eAgNnSc4d1	soustředěné
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Osma	osma	k1gFnSc1	osma
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
řadit	řadit	k5eAaImF	řadit
spíše	spíše	k9	spíše
ke	k	k7c3	k
kubistům	kubista	k1gMnPc3	kubista
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
sice	sice	k8xC	sice
zpočátku	zpočátku	k6eAd1	zpočátku
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
Edvardem	Edvard	k1gMnSc7	Edvard
Munchem	Munch	k1gInSc7	Munch
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
trvale	trvale	k6eAd1	trvale
se	se	k3xPyFc4	se
přikláněli	přiklánět	k5eAaImAgMnP	přiklánět
k	k	k7c3	k
frankofonnímu	frankofonní	k2eAgNnSc3d1	frankofonní
prostředí	prostředí	k1gNnSc3	prostředí
i	i	k8xC	i
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
historická	historický	k2eAgFnSc1d1	historická
danost	danost	k1gFnSc1	danost
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
expresionismus	expresionismus	k1gInSc1	expresionismus
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
spíše	spíše	k9	spíše
přehlížen	přehlížen	k2eAgMnSc1d1	přehlížen
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
představitelů	představitel	k1gMnPc2	představitel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Willi	Wille	k1gFnSc6	Wille
Nowak	Nowak	k1gInSc1	Nowak
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
přímé	přímý	k2eAgNnSc1d1	přímé
spojení	spojení	k1gNnSc1	spojení
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
expresionismus	expresionismus	k1gInSc4	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
konečně	konečně	k6eAd1	konečně
nahlas	nahlas	k6eAd1	nahlas
přiznat	přiznat	k5eAaPmF	přiznat
a	a	k8xC	a
docenit	docenit	k5eAaPmF	docenit
význam	význam	k1gInSc4	význam
zásadního	zásadní	k2eAgMnSc2d1	zásadní
představitele	představitel	k1gMnSc2	představitel
českého	český	k2eAgInSc2d1	český
expresionismu	expresionismus	k1gInSc2	expresionismus
Josefa	Josef	k1gMnSc2	Josef
Váchala	Váchal	k1gMnSc2	Váchal
<g/>
.	.	kIx.	.
</s>
<s>
Rozsahem	rozsah	k1gInSc7	rozsah
<g/>
,	,	kIx,	,
objemem	objem	k1gInSc7	objem
i	i	k8xC	i
šíří	šíř	k1gFnSc7	šíř
záběru	záběr	k1gInSc2	záběr
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
evropskou	evropský	k2eAgFnSc4d1	Evropská
expresionistickou	expresionistický	k2eAgFnSc4d1	expresionistická
špičku	špička	k1gFnSc4	špička
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
neobjevil	objevit	k5eNaPmAgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
častým	častý	k2eAgFnPc3d1	častá
roztržkám	roztržka	k1gFnPc3	roztržka
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
oficiální	oficiální	k2eAgFnSc2d1	oficiální
české	český	k2eAgFnSc2d1	Česká
kulturní	kulturní	k2eAgFnSc2d1	kulturní
scény	scéna	k1gFnSc2	scéna
a	a	k8xC	a
výtvarné	výtvarný	k2eAgFnSc3d1	výtvarná
orientaci	orientace	k1gFnSc3	orientace
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
expresionismus	expresionismus	k1gInSc4	expresionismus
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
přijímán	přijímán	k2eAgMnSc1d1	přijímán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
"	"	kIx"	"
<g/>
frčel	frčet	k5eAaImAgMnS	frčet
<g/>
"	"	kIx"	"
především	především	k9	především
kubismus	kubismus	k1gInSc1	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Váchal	Váchal	k1gMnSc1	Váchal
byl	být	k5eAaImAgMnS	být
univerzálním	univerzální	k2eAgMnSc7d1	univerzální
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
díla	dílo	k1gNnSc2	dílo
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
literární	literární	k2eAgFnSc4d1	literární
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
každý	každý	k3xTgMnSc1	každý
skutečný	skutečný	k2eAgMnSc1d1	skutečný
světový	světový	k2eAgMnSc1d1	světový
expresionista	expresionista	k1gMnSc1	expresionista
zblízka	zblízka	k6eAd1	zblízka
poznal	poznat	k5eAaPmAgMnS	poznat
bojiště	bojiště	k1gNnSc4	bojiště
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
rakousko-uherského	rakouskoherský	k2eAgMnSc4d1	rakousko-uherský
vojáka	voják	k1gMnSc4	voják
<g/>
)	)	kIx)	)
na	na	k7c6	na
obtížné	obtížný	k2eAgFnSc6d1	obtížná
horské	horský	k2eAgFnSc6d1	horská
italsko-rakouské	italskoakouský	k2eAgFnSc6d1	italsko-rakouská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
byl	být	k5eAaImAgMnS	být
Váchal	Váchal	k1gMnSc1	Váchal
nežádoucím	žádoucí	k2eNgMnSc7d1	nežádoucí
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Váchal	Váchal	k1gMnSc1	Váchal
Václav	Václav	k1gMnSc1	Václav
Špála	Špála	k1gMnSc1	Špála
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
Willi	Will	k1gMnPc1	Will
Nowak	Nowak	k1gMnSc1	Nowak
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kremlička	Kremlička	k1gFnSc1	Kremlička
V	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
se	se	k3xPyFc4	se
expresionismus	expresionismus	k1gInSc1	expresionismus
prosadil	prosadit	k5eAaPmAgInS	prosadit
zejména	zejména	k9	zejména
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
byli	být	k5eAaImAgMnP	být
expresionisté	expresionista	k1gMnPc1	expresionista
průkopníky	průkopník	k1gMnPc7	průkopník
používání	používání	k1gNnSc1	používání
kombinací	kombinace	k1gFnSc7	kombinace
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
Wenzel	Wenzel	k1gMnSc1	Wenzel
Hablik	Hablik	k1gMnSc1	Hablik
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bruno	Bruno	k1gMnSc1	Bruno
Tautem	Taut	k1gMnSc7	Taut
propagátorem	propagátor	k1gMnSc7	propagátor
krystalických	krystalický	k2eAgFnPc2d1	krystalická
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Ústí	ústí	k1gNnSc4	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
expresionistický	expresionistický	k2eAgMnSc1d1	expresionistický
architekt	architekt	k1gMnSc1	architekt
Fritz	Fritza	k1gFnPc2	Fritza
Höger	Höger	k1gMnSc1	Höger
budovy	budova	k1gFnSc2	budova
policejního	policejní	k2eAgNnSc2d1	policejní
prezidia	prezidium	k1gNnSc2	prezidium
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Taut	Taut	k1gMnSc1	Taut
Erich	Erich	k1gMnSc1	Erich
Mendelsohn	Mendelsohn	k1gMnSc1	Mendelsohn
Hans	Hans	k1gMnSc1	Hans
Poelzig	Poelzig	k1gMnSc1	Poelzig
Fritz	Fritz	k1gMnSc1	Fritz
Höger	Höger	k1gMnSc1	Höger
Wenzel	Wenzel	k1gMnSc1	Wenzel
Hablik	Hablik	k1gMnSc1	Hablik
Fritz	Fritz	k1gMnSc1	Fritz
Lehmann	Lehmann	k1gMnSc1	Lehmann
Rudolf	Rudolf	k1gMnSc1	Rudolf
Perthen	Perthna	k1gFnPc2	Perthna
Paul	Paul	k1gMnSc1	Paul
Sydow	Sydow	k1gMnSc1	Sydow
Josef	Josef	k1gMnSc1	Josef
Zasche	Zasche	k1gFnSc1	Zasche
Hudba	hudba	k1gFnSc1	hudba
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
expresionismem	expresionismus	k1gInSc7	expresionismus
byla	být	k5eAaImAgFnS	být
populární	populární	k2eAgFnSc1d1	populární
především	především	k9	především
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
skladatelé	skladatel	k1gMnPc1	skladatel
odmítali	odmítat	k5eAaImAgMnP	odmítat
tradiční	tradiční	k2eAgInPc4d1	tradiční
hudební	hudební	k2eAgInPc4d1	hudební
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gNnSc4	on
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
novými	nový	k2eAgNnPc7d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Alban	Alban	k1gMnSc1	Alban
Berg	Berg	k1gMnSc1	Berg
-	-	kIx~	-
Rakušan	Rakušan	k1gMnSc1	Rakušan
Ernst	Ernst	k1gMnSc1	Ernst
Křenek	Křenek	k1gMnSc1	Křenek
-	-	kIx~	-
Rakušan	Rakušan	k1gMnSc1	Rakušan
Arnold	Arnold	k1gMnSc1	Arnold
Schönberg	Schönberg	k1gMnSc1	Schönberg
-	-	kIx~	-
Rakušan	Rakušan	k1gMnSc1	Rakušan
Anton	Anton	k1gMnSc1	Anton
Webern	Webern	k1gMnSc1	Webern
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
-	-	kIx~	-
Němec	Němec	k1gMnSc1	Němec
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Skrjabin	Skrjabin	k2eAgMnSc1d1	Skrjabin
-	-	kIx~	-
Rus	Rus	k1gMnSc1	Rus
Béla	Béla	k1gMnSc1	Béla
Bartók	Bartók	k1gMnSc1	Bartók
-	-	kIx~	-
Maďar	Maďar	k1gMnSc1	Maďar
Alois	Alois	k1gMnSc1	Alois
Hába	Háb	k1gInSc2	Háb
Začátek	začátek	k1gInSc1	začátek
filmového	filmový	k2eAgInSc2d1	filmový
expresionismu	expresionismus	k1gInSc2	expresionismus
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
premiérou	premiéra	k1gFnSc7	premiéra
filmu	film	k1gInSc2	film
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
režiséra	režisér	k1gMnSc2	režisér
Roberta	Robert	k1gMnSc2	Robert
Wieneho	Wiene	k1gMnSc2	Wiene
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1920	[number]	k4	1920
a	a	k8xC	a
kritici	kritik	k1gMnPc1	kritik
ihned	ihned	k6eAd1	ihned
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
expresionismus	expresionismus	k1gInSc1	expresionismus
našel	najít	k5eAaPmAgInS	najít
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
filmu	film	k1gInSc2	film
a	a	k8xC	a
diskutovali	diskutovat	k5eAaImAgMnP	diskutovat
o	o	k7c6	o
přínosu	přínos	k1gInSc6	přínos
tohoto	tento	k3xDgInSc2	tento
nového	nový	k2eAgInSc2d1	nový
vývoje	vývoj	k1gInSc2	vývoj
pro	pro	k7c4	pro
filmové	filmový	k2eAgNnSc4d1	filmové
umění	umění	k1gNnSc4	umění
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přínos	přínos	k1gInSc1	přínos
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
opravdu	opravdu	k6eAd1	opravdu
veliký	veliký	k2eAgInSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
expresionismu	expresionismus	k1gInSc3	expresionismus
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
zlatého	zlatý	k2eAgInSc2d1	zlatý
fondu	fond	k1gInSc2	fond
světové	světový	k2eAgFnSc2d1	světová
kinematografie	kinematografie	k1gFnSc2	kinematografie
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
území	území	k1gNnSc4	území
Německa	Německo	k1gNnSc2	Německo
natočeno	natočen	k2eAgNnSc1d1	natočeno
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
Kabinetu	kabinet	k1gInSc2	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
expresionistickými	expresionistický	k2eAgInPc7d1	expresionistický
filmy	film	k1gInPc7	film
doslova	doslova	k6eAd1	doslova
roztrhl	roztrhnout	k5eAaPmAgInS	roztrhnout
pytel	pytel	k1gInSc1	pytel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
společnost	společnost	k1gFnSc1	společnost
Decla-Bioscop	Decla-Bioscop	k1gInSc1	Decla-Bioscop
po	po	k7c6	po
fúzi	fúze	k1gFnSc6	fúze
společností	společnost	k1gFnPc2	společnost
Deutsche	Deutsche	k1gFnPc2	Deutsche
Bioscop	Bioscop	k1gInSc1	Bioscop
a	a	k8xC	a
Decla	Decla	k1gFnSc1	Decla
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
tohoto	tento	k3xDgNnSc2	tento
nového	nový	k2eAgNnSc2d1	nové
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
Erich	Erich	k1gMnSc1	Erich
Pommer	Pommer	k1gMnSc1	Pommer
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
významný	významný	k2eAgMnSc1d1	významný
producent	producent	k1gMnSc1	producent
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Decla-Bioscop	Decla-Bioscop	k1gInSc1	Decla-Bioscop
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
Pommerovým	Pommerův	k2eAgNnSc7d1	Pommerův
vedením	vedení	k1gNnSc7	vedení
produkovala	produkovat	k5eAaImAgFnS	produkovat
mnohé	mnohý	k2eAgInPc4d1	mnohý
významné	významný	k2eAgInPc4d1	významný
expresionistické	expresionistický	k2eAgInPc4d1	expresionistický
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
raných	raný	k2eAgNnPc6d1	rané
letech	léto	k1gNnPc6	léto
nebyla	být	k5eNaImAgFnS	být
výprava	výprava	k1gFnSc1	výprava
expresionistických	expresionistický	k2eAgInPc2d1	expresionistický
filmů	film	k1gInPc2	film
nijak	nijak	k6eAd1	nijak
nákladná	nákladný	k2eAgFnSc1d1	nákladná
<g/>
.	.	kIx.	.
</s>
<s>
Stačilo	stačit	k5eAaBmAgNnS	stačit
pouze	pouze	k6eAd1	pouze
vytvořit	vytvořit	k5eAaPmF	vytvořit
dekorace	dekorace	k1gFnPc4	dekorace
v	v	k7c6	v
expresionistickém	expresionistický	k2eAgNnSc6d1	expresionistické
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
touhou	touha	k1gFnSc7	touha
diváků	divák	k1gMnPc2	divák
po	po	k7c6	po
novém	nový	k2eAgInSc6d1	nový
prvku	prvek	k1gInSc6	prvek
nebo	nebo	k8xC	nebo
žánru	žánr	k1gInSc6	žánr
v	v	k7c6	v
kinematografii	kinematografie	k1gFnSc6	kinematografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
expresionistické	expresionistický	k2eAgInPc1d1	expresionistický
filmy	film	k1gInPc1	film
začaly	začít	k5eAaPmAgInP	začít
natáčet	natáčet	k5eAaImF	natáčet
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
uveden	uvést	k5eAaPmNgMnS	uvést
Angol	Angola	k1gFnPc2	Angola
Hanse	Hans	k1gMnSc2	Hans
Werekmeistera	Werekmeister	k1gMnSc2	Werekmeister
<g/>
,	,	kIx,	,
Genuine	Genuin	k1gInSc5	Genuin
Roberta	Robert	k1gMnSc4	Robert
Wieneho	Wiene	k1gMnSc4	Wiene
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
expresionistického	expresionistický	k2eAgNnSc2d1	expresionistické
hnutí	hnutí	k1gNnSc2	hnutí
zvlášť	zvlášť	k6eAd1	zvlášť
plodným	plodný	k2eAgInSc7d1	plodný
a	a	k8xC	a
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
expresionistou	expresionista	k1gMnSc7	expresionista
<g/>
,	,	kIx,	,
Torus	torus	k1gInSc4	torus
Hanse	Hans	k1gMnSc2	Hans
Kobeho	Kobe	k1gMnSc2	Kobe
či	či	k8xC	či
Od	od	k7c2	od
rána	ráno	k1gNnSc2	ráno
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
(	(	kIx(	(
<g/>
Von	von	k1gInSc1	von
Morgens	Morgens	k1gInSc1	Morgens
bis	bis	k?	bis
Mitternacht	Mitternacht	k1gInSc1	Mitternacht
<g/>
)	)	kIx)	)
Karla	Karel	k1gMnSc2	Karel
Heinze	Heinze	k1gFnSc1	Heinze
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
i	i	k9	i
film	film	k1gInSc1	film
režisérů	režisér	k1gMnPc2	režisér
Paula	Paul	k1gMnSc2	Paul
Wegenera	Wegener	k1gMnSc2	Wegener
a	a	k8xC	a
Carla	Carl	k1gMnSc2	Carl
Boese	Boes	k1gMnSc2	Boes
Jak	jak	k8xS	jak
Golem	Golem	k1gMnSc1	Golem
na	na	k7c4	na
svět	svět	k1gInSc4	svět
přišel	přijít	k5eAaPmAgMnS	přijít
(	(	kIx(	(
<g/>
Golem	Golem	k1gMnSc1	Golem
<g/>
,	,	kIx,	,
wie	wie	k?	wie
er	er	k?	er
in	in	k?	in
die	die	k?	die
Welt	Welt	k1gInSc1	Welt
kam	kam	k6eAd1	kam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
si	se	k3xPyFc3	se
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
pamatovali	pamatovat	k5eAaImAgMnP	pamatovat
hlavně	hlavně	k9	hlavně
zajímavě	zajímavě	k6eAd1	zajímavě
vyřešenou	vyřešený	k2eAgFnSc4d1	vyřešená
postavu	postava	k1gFnSc4	postava
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
Golema	Golem	k1gMnSc2	Golem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
použili	použít	k5eAaPmAgMnP	použít
tvůrci	tvůrce	k1gMnPc1	tvůrce
seriálu	seriál	k1gInSc2	seriál
Simpsonovi	Simpsonovi	k1gRnPc1	Simpsonovi
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Simpsons	Simpsonsa	k1gFnPc2	Simpsonsa
<g/>
)	)	kIx)	)
do	do	k7c2	do
Speciálního	speciální	k2eAgInSc2d1	speciální
čarodějnického	čarodějnický	k2eAgInSc2d1	čarodějnický
dílu	díl	k1gInSc2	díl
(	(	kIx(	(
<g/>
Treehouse	Treehouse	k1gFnSc1	Treehouse
of	of	k?	of
Horror	horror	k1gInSc1	horror
<g/>
)	)	kIx)	)
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
již	již	k9	již
o	o	k7c4	o
něco	něco	k3yInSc4	něco
chudší	chudý	k2eAgMnSc1d2	chudší
na	na	k7c4	na
expresionistické	expresionistický	k2eAgInPc4d1	expresionistický
filmy	film	k1gInPc4	film
než	než	k8xS	než
rok	rok	k1gInSc4	rok
předešlý	předešlý	k2eAgInSc4d1	předešlý
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
potenciál	potenciál	k1gInSc4	potenciál
expresionismu	expresionismus	k1gInSc2	expresionismus
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
důležitějším	důležitý	k2eAgMnSc7d2	důležitější
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
kvalitněji	kvalitně	k6eAd2	kvalitně
natočen	natočit	k5eAaBmNgInS	natočit
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgInS	dokázat
více	hodně	k6eAd2	hodně
ohromit	ohromit	k5eAaPmF	ohromit
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
uvedeny	uvést	k5eAaPmNgInP	uvést
pouze	pouze	k6eAd1	pouze
filmy	film	k1gInPc4	film
Unavená	unavený	k2eAgFnSc1d1	unavená
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
müde	müde	k1gInSc4	müde
Tod	Tod	k1gMnSc2	Tod
<g/>
)	)	kIx)	)
Fritze	Fritze	k1gFnSc1	Fritze
Langa	Lang	k1gMnSc2	Lang
a	a	k8xC	a
Měsíční	měsíční	k2eAgInSc1d1	měsíční
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Das	Das	k1gFnSc1	Das
Haus	Hausa	k1gFnPc2	Hausa
zum	zum	k?	zum
Mond	Mond	k1gMnSc1	Mond
<g/>
)	)	kIx)	)
Karla	Karel	k1gMnSc2	Karel
Heinze	Heinze	k1gFnSc1	Heinze
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pohltila	pohltit	k5eAaPmAgFnS	pohltit
UFA	UFA	kA	UFA
společnost	společnost	k1gFnSc1	společnost
Decla-Bioscop	Decla-Bioscop	k1gInSc1	Decla-Bioscop
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k8xC	ale
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstala	zůstat	k5eAaPmAgFnS	zůstat
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
produkční	produkční	k2eAgFnSc7d1	produkční
jednotkou	jednotka	k1gFnSc7	jednotka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ericha	Erich	k1gMnSc2	Erich
Pommera	Pommer	k1gMnSc2	Pommer
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
natočeno	natočit	k5eAaBmNgNnS	natočit
osm	osm	k4xCc1	osm
filmů	film	k1gInPc2	film
v	v	k7c6	v
expresionistickém	expresionistický	k2eAgInSc6d1	expresionistický
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
šest	šest	k4xCc1	šest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
od	od	k7c2	od
Kabinetu	kabinet	k1gInSc2	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
až	až	k6eAd1	až
snímky	snímka	k1gFnPc4	snímka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kultovní	kultovní	k2eAgFnSc4d1	kultovní
klasiku	klasika	k1gFnSc4	klasika
Upír	upír	k1gMnSc1	upír
Nosferatu	Nosferat	k1gInSc2	Nosferat
(	(	kIx(	(
<g/>
Nosferatu	Nosferat	k1gInSc2	Nosferat
<g/>
,	,	kIx,	,
eine	eine	k6eAd1	eine
Symphonie	Symphonie	k1gFnSc1	Symphonie
des	des	k1gNnSc2	des
Grauens	Grauensa	k1gFnPc2	Grauensa
<g/>
)	)	kIx)	)
Friedricha	Friedrich	k1gMnSc2	Friedrich
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Murnaua	Murnauus	k1gMnSc2	Murnauus
a	a	k8xC	a
také	také	k9	také
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
epos	epos	k1gInSc4	epos
Fritze	Fritze	k1gFnSc2	Fritze
Langa	Lang	k1gMnSc2	Lang
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mabuse	Mabuse	k1gFnSc1	Mabuse
<g/>
,	,	kIx,	,
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mabuse	Mabus	k1gMnSc5	Mabus
<g/>
,	,	kIx,	,
der	drát	k5eAaImRp2nS	drát
Spieler	Spieler	k1gInSc1	Spieler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
největší	veliký	k2eAgInSc1d3	veliký
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Upír	upír	k1gMnSc1	upír
Nosferatu	Nosferat	k1gInSc2	Nosferat
přišel	přijít	k5eAaPmAgMnS	přijít
tehdy	tehdy	k6eAd1	tehdy
s	s	k7c7	s
něčím	něčí	k3xOyIgInSc7	něčí
úplně	úplně	k6eAd1	úplně
novým	nový	k2eAgInSc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
natáčeny	natáčet	k5eAaImNgInP	natáčet
v	v	k7c6	v
interiérech	interiér	k1gInPc6	interiér
s	s	k7c7	s
expresionistickými	expresionistický	k2eAgFnPc7d1	expresionistická
dekoracemi	dekorace	k1gFnPc7	dekorace
<g/>
,	,	kIx,	,
Murnau	Murnaa	k1gFnSc4	Murnaa
si	se	k3xPyFc3	se
však	však	k9	však
se	s	k7c7	s
štábem	štáb	k1gInSc7	štáb
vyjel	vyjet	k5eAaPmAgInS	vyjet
až	až	k9	až
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
Oravský	oravský	k2eAgInSc1d1	oravský
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
Vrátna	Vráten	k2eAgFnSc1d1	Vrátna
dolina	dolina	k1gFnSc1	dolina
<g/>
)	)	kIx)	)
a	a	k8xC	a
část	část	k1gFnSc4	část
filmu	film	k1gInSc2	film
natočil	natočit	k5eAaBmAgInS	natočit
v	v	k7c6	v
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
Lubeck	Lubeck	k1gInSc4	Lubeck
a	a	k8xC	a
Wismar	Wismar	k1gInSc4	Wismar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1923	[number]	k4	1923
a	a	k8xC	a
1924	[number]	k4	1924
se	se	k3xPyFc4	se
filmaři	filmař	k1gMnPc1	filmař
navrátili	navrátit	k5eAaPmAgMnP	navrátit
k	k	k7c3	k
tradiční	tradiční	k2eAgFnSc3d1	tradiční
formě	forma	k1gFnSc3	forma
natáčení	natáčení	k1gNnSc1	natáčení
ve	v	k7c6	v
studiových	studiový	k2eAgInPc6d1	studiový
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
i	i	k9	i
díky	díky	k7c3	díky
narůstajícím	narůstající	k2eAgInPc3d1	narůstající
rozpočtům	rozpočet	k1gInPc3	rozpočet
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
natočených	natočený	k2eAgInPc2d1	natočený
a	a	k8xC	a
uvedených	uvedený	k2eAgInPc2d1	uvedený
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
německých	německý	k2eAgNnPc6d1	německé
kinech	kino	k1gNnPc6	kino
uvedeno	uvést	k5eAaPmNgNnS	uvést
těchto	tento	k3xDgInPc2	tento
pět	pět	k4xCc4	pět
filmů	film	k1gInPc2	film
<g/>
:	:	kIx,	:
Varovné	varovný	k2eAgInPc1d1	varovný
stíny	stín	k1gInPc1	stín
(	(	kIx(	(
<g/>
Schatten	Schatten	k2eAgInSc1d1	Schatten
<g/>
)	)	kIx)	)
od	od	k7c2	od
Artura	Artur	k1gMnSc2	Artur
Robinsona	Robinson	k1gMnSc2	Robinson
<g/>
,	,	kIx,	,
Poklad	poklad	k1gInSc4	poklad
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Schatz	Schatz	k1gInSc1	Schatz
<g/>
)	)	kIx)	)
klasika	klasik	k1gMnSc2	klasik
Georga	Georg	k1gMnSc2	Georg
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Pabsta	Pabst	k1gMnSc2	Pabst
<g/>
,	,	kIx,	,
Raskolnikov	Raskolnikov	k1gInSc4	Raskolnikov
Roberta	Robert	k1gMnSc2	Robert
Wieneho	Wiene	k1gMnSc2	Wiene
<g/>
,	,	kIx,	,
Lulu	lula	k1gFnSc4	lula
(	(	kIx(	(
<g/>
Erdgeist	Erdgeist	k1gInSc4	Erdgeist
<g/>
)	)	kIx)	)
Leopolda	Leopold	k1gMnSc2	Leopold
Jessnera	Jessner	k1gMnSc2	Jessner
a	a	k8xC	a
Fritz	Fritz	k1gMnSc1	Fritz
Wendhausen	Wendhausen	k1gInSc4	Wendhausen
přispěl	přispět	k5eAaPmAgMnS	přispět
svým	svůj	k3xOyFgMnSc7	svůj
Kamenným	kamenný	k2eAgMnSc7d1	kamenný
jezdcem	jezdec	k1gMnSc7	jezdec
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
steinere	steiner	k1gMnSc5	steiner
Reiter	Reitrum	k1gNnPc2	Reitrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1923	[number]	k4	1923
znamenal	znamenat	k5eAaImAgInS	znamenat
vrchol	vrchol	k1gInSc1	vrchol
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
expresionistických	expresionistický	k2eAgInPc2d1	expresionistický
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
hodně	hodně	k6eAd1	hodně
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
měnícími	měnící	k2eAgInPc7d1	měnící
se	se	k3xPyFc4	se
vlivy	vliv	k1gInPc1	vliv
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
podzim	podzim	k1gInSc4	podzim
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skončila	skončit	k5eAaPmAgFnS	skončit
hyperinflace	hyperinflace	k1gFnPc4	hyperinflace
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Úpadek	úpadek	k1gInSc1	úpadek
a	a	k8xC	a
konec	konec	k1gInSc1	konec
expresionistického	expresionistický	k2eAgNnSc2d1	expresionistické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
natočil	natočit	k5eAaBmAgMnS	natočit
Paul	Paul	k1gMnSc1	Paul
Leni	Leni	k?	Leni
Kabinet	kabinet	k1gInSc1	kabinet
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
(	(	kIx(	(
<g/>
Wachsfigurenkabinett	Wachsfigurenkabinett	k1gInSc1	Wachsfigurenkabinett
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Lang	Lang	k1gMnSc1	Lang
režíroval	režírovat	k5eAaImAgMnS	režírovat
Nibelungy	Nibelung	k1gMnPc4	Nibelung
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Nibelungen	Nibelungen	k1gInSc1	Nibelungen
<g/>
)	)	kIx)	)
a	a	k8xC	a
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
expresionistického	expresionistický	k2eAgNnSc2d1	expresionistické
hnutí	hnutí	k1gNnSc2	hnutí
Robert	Robert	k1gMnSc1	Robert
Wiene	Wien	k1gInSc5	Wien
natočil	natočit	k5eAaBmAgMnS	natočit
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
film	film	k1gInSc4	film
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
Orlakovy	Orlakův	k2eAgFnPc1d1	Orlakův
ruce	ruka	k1gFnPc1	ruka
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Orlac	Orlac	k1gFnSc1	Orlac
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hände	Händ	k1gInSc5	Händ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
hnutí	hnutí	k1gNnSc2	hnutí
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
premiérou	premiéra	k1gFnSc7	premiéra
filmu	film	k1gInSc2	film
Metropolis	Metropolis	k1gFnSc2	Metropolis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
hřebíček	hřebíček	k1gInSc1	hřebíček
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
si	se	k3xPyFc3	se
však	však	k9	však
tvůrci	tvůrce	k1gMnPc1	tvůrce
a	a	k8xC	a
producenti	producent	k1gMnPc1	producent
expresionistického	expresionistický	k2eAgInSc2d1	expresionistický
filmu	film	k1gInSc2	film
začali	začít	k5eAaPmAgMnP	začít
zatloukat	zatloukat	k5eAaImF	zatloukat
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
hřebíčkem	hřebíček	k1gInSc7	hřebíček
byly	být	k5eAaImAgFnP	být
přemrštěné	přemrštěný	k2eAgFnPc1d1	přemrštěná
rozpočty	rozpočet	k1gInPc4	rozpočet
<g/>
,	,	kIx,	,
Metropolis	Metropolis	k1gFnPc4	Metropolis
budiž	budiž	k9	budiž
důkazem	důkaz	k1gInSc7	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
expresionismus	expresionismus	k1gInSc1	expresionismus
nejprve	nejprve	k6eAd1	nejprve
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
historickým	historický	k2eAgInPc3d1	historický
velkofilmům	velkofilm	k1gInPc3	velkofilm
a	a	k8xC	a
ztřeštěným	ztřeštěný	k2eAgFnPc3d1	ztřeštěná
komediím	komedie	k1gFnPc3	komedie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
nečekaně	nečekaně	k6eAd1	nečekaně
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
prvních	první	k4xOgInPc2	první
filmů	film	k1gInPc2	film
a	a	k8xC	a
s	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
megalomanstvím	megalomanství	k1gNnSc7	megalomanství
některých	některý	k3yIgMnPc2	některý
filmařů	filmař	k1gMnPc2	filmař
se	se	k3xPyFc4	se
rozpočty	rozpočet	k1gInPc7	rozpočet
snímků	snímek	k1gInPc2	snímek
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
a	a	k8xC	a
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
těchto	tento	k3xDgInPc2	tento
kroků	krok	k1gInPc2	krok
byl	být	k5eAaImAgMnS	být
zmenšující	zmenšující	k2eAgMnSc1d1	zmenšující
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
natočených	natočený	k2eAgInPc2d1	natočený
filmů	film	k1gInPc2	film
v	v	k7c6	v
expresionistickém	expresionistický	k2eAgInSc6d1	expresionistický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
snímky	snímek	k1gInPc4	snímek
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
F.	F.	kA	F.
W.	W.	kA	W.
Murnaua	Murnaua	k1gMnSc1	Murnaua
a	a	k8xC	a
Kronika	kronika	k1gFnSc1	kronika
z	z	k7c2	z
Grieshuusu	Grieshuus	k1gInSc2	Grieshuus
Paula	Paula	k1gFnSc1	Paula
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
Faust	Faust	k1gInSc1	Faust
F.	F.	kA	F.
W.	W.	kA	W.
Murnaua	Murnauum	k1gNnSc2	Murnauum
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
film	film	k1gInSc1	film
Metropolis	Metropolis	k1gFnSc2	Metropolis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
promítalo	promítat	k5eAaImAgNnS	promítat
šest	šest	k4xCc1	šest
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
hřebíky	hřebík	k1gInPc1	hřebík
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
přibyly	přibýt	k5eAaPmAgFnP	přibýt
s	s	k7c7	s
definitivním	definitivní	k2eAgInSc7d1	definitivní
koncem	konec	k1gInSc7	konec
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc1	export
filmů	film	k1gInPc2	film
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
stále	stále	k6eAd1	stále
výhodný	výhodný	k2eAgInSc1d1	výhodný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
ne	ne	k9	ne
tolik	tolik	k6eAd1	tolik
jako	jako	k8xS	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
koncem	konec	k1gInSc7	konec
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
snižovat	snižovat	k5eAaImF	snižovat
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měna	měna	k1gFnSc1	měna
již	již	k6eAd1	již
neztrácela	ztrácet	k5eNaImAgFnS	ztrácet
tolik	tolik	k6eAd1	tolik
na	na	k7c6	na
hodnotě	hodnota	k1gFnSc6	hodnota
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
vydělané	vydělaný	k2eAgInPc4d1	vydělaný
peníze	peníz	k1gInPc4	peníz
mohli	moct	k5eAaImAgMnP	moct
šetřit	šetřit	k5eAaImF	šetřit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
teď	teď	k6eAd1	teď
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
distributory	distributor	k1gMnPc4	distributor
často	často	k6eAd1	často
levnější	levný	k2eAgMnSc1d2	levnější
koupit	koupit	k5eAaPmF	koupit
film	film	k1gInSc4	film
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
než	než	k8xS	než
financovat	financovat	k5eAaBmF	financovat
výrobu	výroba	k1gFnSc4	výroba
filmu	film	k1gInSc2	film
německého	německý	k2eAgInSc2d1	německý
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
počtu	počet	k1gInSc2	počet
dovezených	dovezený	k2eAgInPc2d1	dovezený
filmů	film	k1gInPc2	film
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
v	v	k7c6	v
německých	německý	k2eAgNnPc6d1	německé
kinech	kino	k1gNnPc6	kino
promítalo	promítat	k5eAaImAgNnS	promítat
417	[number]	k4	417
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
60,6	[number]	k4	60,6
<g/>
%	%	kIx~	%
německých	německý	k2eAgMnPc2d1	německý
a	a	k8xC	a
24,5	[number]	k4	24,5
<g/>
%	%	kIx~	%
amerických	americký	k2eAgFnPc2d1	americká
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
560	[number]	k4	560
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
německý	německý	k2eAgInSc1d1	německý
podíl	podíl	k1gInSc1	podíl
snížil	snížit	k5eAaPmAgInS	snížit
na	na	k7c4	na
39,3	[number]	k4	39,3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
filmů	film	k1gInPc2	film
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
ale	ale	k9	ale
zvedl	zvednout	k5eAaPmAgMnS	zvednout
na	na	k7c4	na
33,2	[number]	k4	33,2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
již	již	k9	již
Američané	Američan	k1gMnPc1	Američan
měli	mít	k5eAaImAgMnP	mít
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
uvedených	uvedený	k2eAgInPc2d1	uvedený
filmů	film	k1gInPc2	film
než	než	k8xS	než
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
také	také	k6eAd1	také
velkou	velký	k2eAgFnSc7d1	velká
krizí	krize	k1gFnSc7	krize
UFA	UFA	kA	UFA
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
neměla	mít	k5eNaImAgFnS	mít
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
bankrotu	bankrot	k1gInSc3	bankrot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
také	také	k9	také
doslova	doslova	k6eAd1	doslova
překopal	překopat	k5eAaPmAgMnS	překopat
zaběhnuté	zaběhnutý	k2eAgInPc4d1	zaběhnutý
pořádky	pořádek	k1gInPc4	pořádek
ve	v	k7c6	v
fungování	fungování	k1gNnSc4	fungování
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
inflačním	inflační	k2eAgNnSc6d1	inflační
období	období	k1gNnSc6	období
rychle	rychle	k6eAd1	rychle
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
na	na	k7c4	na
úvěr	úvěr	k1gInSc4	úvěr
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
zkrachovaly	zkrachovat	k5eAaPmAgFnP	zkrachovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgFnP	muset
zredukovat	zredukovat	k5eAaPmF	zredukovat
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
počet	počet	k1gInSc1	počet
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
a	a	k8xC	a
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Měnící	měnící	k2eAgMnSc1d1	měnící
se	se	k3xPyFc4	se
kulturní	kulturní	k2eAgNnSc1d1	kulturní
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
klima	klima	k1gNnSc1	klima
tak	tak	k6eAd1	tak
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
i	i	k9	i
do	do	k7c2	do
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
filmové	filmový	k2eAgInPc1d1	filmový
směry	směr	k1gInPc1	směr
<g/>
:	:	kIx,	:
komorní	komorní	k2eAgNnSc1d1	komorní
drama	drama	k1gNnSc1	drama
(	(	kIx(	(
<g/>
Kammerspiel	Kammerspiel	k1gInSc1	Kammerspiel
<g/>
)	)	kIx)	)
a	a	k8xC	a
film	film	k1gInSc4	film
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
proudy	proud	k1gInPc1	proud
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
sociální	sociální	k2eAgFnSc7d1	sociální
problematikou	problematika	k1gFnSc7	problematika
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
filmů	film	k1gInPc2	film
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
právě	právě	k9	právě
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hyperinflace	hyperinflace	k1gFnSc1	hyperinflace
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
a	a	k8xC	a
diváci	divák	k1gMnPc1	divák
se	se	k3xPyFc4	se
tak	tak	k9	tak
s	s	k7c7	s
osudy	osud	k1gInPc7	osud
postav	postava	k1gFnPc2	postava
mohli	moct	k5eAaImAgMnP	moct
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
popularita	popularita	k1gFnSc1	popularita
Kammerspielu	Kammerspiel	k1gInSc2	Kammerspiel
a	a	k8xC	a
filmu	film	k1gInSc2	film
ulice	ulice	k1gFnSc2	ulice
také	také	k9	také
napomohla	napomoct	k5eAaPmAgFnS	napomoct
pádu	pád	k1gInSc3	pád
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
hřebík	hřebík	k1gInSc4	hřebík
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
zatloukly	zatlouct	k5eAaPmAgFnP	zatlouct
americké	americký	k2eAgFnPc1d1	americká
filmové	filmový	k2eAgFnPc1d1	filmová
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nalákaly	nalákat	k5eAaPmAgInP	nalákat
mnoho	mnoho	k4c4	mnoho
expresionistických	expresionistický	k2eAgMnPc2d1	expresionistický
tvůrců	tvůrce	k1gMnPc2	tvůrce
na	na	k7c4	na
větší	veliký	k2eAgInPc4d2	veliký
příjmy	příjem	k1gInPc4	příjem
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
špičkovou	špičkový	k2eAgFnSc7d1	špičková
filmovou	filmový	k2eAgFnSc7d1	filmová
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
Fox	fox	k1gInSc1	fox
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
th-Century	th-Centura	k1gFnPc1	th-Centura
se	se	k3xPyFc4	se
například	například	k6eAd1	například
upsal	upsat	k5eAaPmAgMnS	upsat
F.	F.	kA	F.
W.	W.	kA	W.
Murnau	Murnaus	k1gInSc2	Murnaus
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Leni	Leni	k?	Leni
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Kabinetu	kabinet	k1gInSc2	kabinet
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Universalem	Universal	k1gMnSc7	Universal
a	a	k8xC	a
herci	herec	k1gMnPc7	herec
Conrad	Conrada	k1gFnPc2	Conrada
Veidt	Veidt	k1gInSc1	Veidt
(	(	kIx(	(
<g/>
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
<g/>
)	)	kIx)	)
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Jannings	Jannings	k1gInSc1	Jannings
(	(	kIx(	(
<g/>
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
)	)	kIx)	)
také	také	k9	také
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Conrad	Conrad	k1gInSc1	Conrad
Veidt	Veidt	k1gMnSc1	Veidt
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
například	například	k6eAd1	například
zahrál	zahrát	k5eAaPmAgInS	zahrát
ve	v	k7c6	v
slavných	slavný	k2eAgInPc6d1	slavný
filmech	film	k1gInPc6	film
Zloděj	zloděj	k1gMnSc1	zloděj
z	z	k7c2	z
Bagdádu	Bagdád	k1gInSc2	Bagdád
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Thief	Thief	k1gMnSc1	Thief
of	of	k?	of
Bagdad	Bagdad	k1gInSc1	Bagdad
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
režiséra	režisér	k1gMnSc2	režisér
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Bergera	Berger	k1gMnSc2	Berger
a	a	k8xC	a
Casablanca	Casablanca	k1gFnSc1	Casablanca
(	(	kIx(	(
<g/>
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
od	od	k7c2	od
Michaela	Michael	k1gMnSc2	Michael
Curtize	Curtize	k1gFnSc1	Curtize
<g/>
,	,	kIx,	,
emigranta	emigrant	k1gMnSc4	emigrant
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
věhlasných	věhlasný	k2eAgFnPc2d1	věhlasná
postav	postava	k1gFnPc2	postava
expresionistického	expresionistický	k2eAgNnSc2d1	expresionistické
hnutí	hnutí	k1gNnSc2	hnutí
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
jen	jen	k9	jen
málo	málo	k4c4	málo
filmařů	filmař	k1gMnPc2	filmař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Expresionistické	expresionistický	k2eAgNnSc1d1	expresionistické
hnutí	hnutí	k1gNnSc1	hnutí
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
kinematografie	kinematografie	k1gFnSc2	kinematografie
výraznou	výrazný	k2eAgFnSc4d1	výrazná
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
kultovních	kultovní	k2eAgFnPc2d1	kultovní
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
filmaři	filmař	k1gMnPc1	filmař
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
hnutí	hnutí	k1gNnSc3	hnutí
vydobyli	vydobýt	k5eAaPmAgMnP	vydobýt
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgNnSc1d3	veliký
<g/>
,	,	kIx,	,
co	co	k8xS	co
expresionismus	expresionismus	k1gInSc1	expresionismus
kinematografii	kinematografie	k1gFnSc3	kinematografie
zanechal	zanechat	k5eAaPmAgInS	zanechat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
působivou	působivý	k2eAgFnSc4d1	působivá
hororovou	hororový	k2eAgFnSc4d1	hororová
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
tím	ten	k3xDgInSc7	ten
nejpodstatnějším	podstatný	k2eAgInSc7d3	nejpodstatnější
prvkem	prvek	k1gInSc7	prvek
vykreslujícím	vykreslující	k2eAgInSc7d1	vykreslující
hororovou	hororový	k2eAgFnSc4d1	hororová
náladu	nálada	k1gFnSc4	nálada
byly	být	k5eAaImAgFnP	být
zdeformované	zdeformovaný	k2eAgFnPc1d1	zdeformovaná
kulisy	kulisa	k1gFnPc1	kulisa
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
si	se	k3xPyFc3	se
filmaři	filmař	k1gMnPc1	filmař
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
často	často	k6eAd1	často
vypůjčovali	vypůjčovat	k5eAaImAgMnP	vypůjčovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gInPc1	jejich
filmy	film	k1gInPc1	film
získaly	získat	k5eAaPmAgInP	získat
hororový	hororový	k2eAgInSc4d1	hororový
nádech	nádech	k1gInSc4	nádech
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
duševní	duševní	k2eAgInSc1d1	duševní
stav	stav	k1gInSc1	stav
filmových	filmový	k2eAgFnPc2d1	filmová
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
měl	mít	k5eAaImAgInS	mít
expresionismus	expresionismus	k1gInSc4	expresionismus
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
díky	díky	k7c3	díky
velkému	velký	k2eAgInSc3d1	velký
podílu	podíl	k1gInSc3	podíl
německých	německý	k2eAgFnPc2d1	německá
filmu	film	k1gInSc2	film
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
nízké	nízký	k2eAgFnSc6d1	nízká
konkurenci	konkurence	k1gFnSc6	konkurence
domácích	domácí	k2eAgMnPc2d1	domácí
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Marcel	Marcel	k1gMnSc1	Marcel
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Herbier	Herbier	k1gMnSc1	Herbier
(	(	kIx(	(
<g/>
Eldorádo	Eldorádo	k1gNnSc1	Eldorádo
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
použil	použít	k5eAaPmAgMnS	použít
již	již	k6eAd1	již
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
deformaci	deformace	k1gFnSc4	deformace
kulis	kulisa	k1gFnPc2	kulisa
a	a	k8xC	a
expresionistické	expresionistický	k2eAgInPc4d1	expresionistický
kostýmy	kostým	k1gInPc4	kostým
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
snímku	snímek	k1gInSc6	snímek
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
a	a	k8xC	a
Faust	Faust	k1gMnSc1	Faust
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
et	et	k?	et
Faust	Faust	k1gMnSc1	Faust
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Herbier	Herbier	k1gMnSc1	Herbier
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
paralelně	paralelně	k6eAd1	paralelně
příběh	příběh	k1gInSc4	příběh
obou	dva	k4xCgMnPc2	dva
slavných	slavný	k2eAgMnPc2d1	slavný
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Faustovy	Faustův	k2eAgFnSc2d1	Faustova
scény	scéna	k1gFnSc2	scéna
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
kulisách	kulisa	k1gFnPc6	kulisa
a	a	k8xC	a
kostýmech	kostým	k1gInPc6	kostým
v	v	k7c6	v
expresionistickém	expresionistický	k2eAgInSc6d1	expresionistický
stylu	styl	k1gInSc6	styl
<g/>
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Epstein	Epstein	k1gMnSc1	Epstein
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
zase	zase	k9	zase
docílil	docílit	k5eAaPmAgMnS	docílit
expresionistickou	expresionistický	k2eAgFnSc7d1	expresionistická
výpravou	výprava	k1gFnSc7	výprava
tajuplné	tajuplný	k2eAgFnSc2d1	tajuplná
a	a	k8xC	a
zlověstné	zlověstný	k2eAgFnSc2d1	zlověstná
nálady	nálada	k1gFnSc2	nálada
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
inspirovaným	inspirovaný	k2eAgMnSc7d1	inspirovaný
povídkou	povídka	k1gFnSc7	povídka
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poa	Poa	k1gFnSc1	Poa
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
(	(	kIx(	(
<g/>
La	la	k0	la
Chute	Chut	k1gMnSc5	Chut
de	de	k?	de
la	la	k1gNnSc1	la
maison	maison	k1gMnSc1	maison
Usher	Usher	k1gMnSc1	Usher
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
pronikl	proniknout	k5eAaPmAgInS	proniknout
i	i	k9	i
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Teinosuke	Teinosuke	k1gFnSc1	Teinosuke
Kinugasa	Kinugasa	k1gFnSc1	Kinugasa
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
filmař	filmař	k1gMnSc1	filmař
fascinovaný	fascinovaný	k2eAgInSc1d1	fascinovaný
avantgardním	avantgardní	k2eAgNnSc7d1	avantgardní
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
expresionismu	expresionismus	k1gInSc2	expresionismus
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
Kurutta	Kurutta	k1gFnSc1	Kurutta
ippédži	ippédzat	k5eAaPmIp1nS	ippédzat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kinugasa	Kinugasa	k1gFnSc1	Kinugasa
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
Kabinetem	kabinet	k1gInSc7	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
a	a	k8xC	a
děj	děj	k1gInSc1	děj
tak	tak	k6eAd1	tak
zasadil	zasadit	k5eAaPmAgInS	zasadit
do	do	k7c2	do
blázince	blázinec	k1gInSc2	blázinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Československé	československý	k2eAgFnSc6d1	Československá
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
natočil	natočit	k5eAaBmAgInS	natočit
film	film	k1gInSc1	film
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Jurajem	Juraj	k1gMnSc7	Juraj
Herzem	Herz	k1gMnSc7	Herz
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
hrdinou	hrdina	k1gMnSc7	hrdina
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
pan	pan	k1gMnSc1	pan
Karl	Karl	k1gMnSc1	Karl
Kopfrkingl	Kopfrkingl	k1gMnSc1	Kopfrkingl
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Hrušínským	Hrušínský	k2eAgMnSc7d1	Hrušínský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
svého	své	k1gNnSc2	své
nacisticky	nacisticky	k6eAd1	nacisticky
smýšlejícího	smýšlející	k2eAgMnSc2d1	smýšlející
přítele	přítel	k1gMnSc2	přítel
promění	proměnit	k5eAaPmIp3nS	proměnit
z	z	k7c2	z
mírumilovného	mírumilovný	k2eAgMnSc2d1	mírumilovný
manžela	manžel	k1gMnSc2	manžel
a	a	k8xC	a
otce	otec	k1gMnSc4	otec
na	na	k7c4	na
nacistického	nacistický	k2eAgMnSc4d1	nacistický
fanatika	fanatik	k1gMnSc4	fanatik
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
neváhá	váhat	k5eNaImIp3nS	váhat
zabít	zabít	k5eAaPmF	zabít
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
ženu	žena	k1gFnSc4	žena
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
i	i	k9	i
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
odrazem	odraz	k1gInSc7	odraz
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
sice	sice	k8xC	sice
nenatáčel	natáčet	k5eNaImAgMnS	natáčet
film	film	k1gInSc4	film
s	s	k7c7	s
deformovanými	deformovaný	k2eAgFnPc7d1	deformovaná
dekoracemi	dekorace	k1gFnPc7	dekorace
v	v	k7c6	v
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
kameře	kamera	k1gFnSc3	kamera
Stanislava	Stanislav	k1gMnSc2	Stanislav
Miloty	milota	k1gFnSc2	milota
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
nabudit	nabudit	k5eAaPmF	nabudit
expresivní	expresivní	k2eAgInSc4d1	expresivní
dojem	dojem	k1gInSc4	dojem
a	a	k8xC	a
podtrhnout	podtrhnout	k5eAaPmF	podtrhnout
tak	tak	k6eAd1	tak
duševní	duševní	k2eAgInSc4d1	duševní
stav	stav	k1gInSc4	stav
šíleného	šílený	k2eAgMnSc2d1	šílený
Karla	Karel	k1gMnSc2	Karel
Kopfrkingla	Kopfrkingl	k1gMnSc2	Kopfrkingl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
řeči	řeč	k1gFnSc2	řeč
patří	patřit	k5eAaImIp3nS	patřit
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nějak	nějak	k6eAd1	nějak
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
mizascéna	mizascéna	k6eAd1	mizascéna
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
umístěné	umístěný	k2eAgInPc1d1	umístěný
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
natočeny	natočen	k2eAgFnPc1d1	natočena
<g/>
:	:	kIx,	:
dekorace	dekorace	k1gFnPc1	dekorace
a	a	k8xC	a
rekvizity	rekvizit	k1gInPc1	rekvizit
<g/>
,	,	kIx,	,
osvětlení	osvětlení	k1gNnPc1	osvětlení
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
<g/>
,	,	kIx,	,
make-up	makep	k1gInSc1	make-up
a	a	k8xC	a
interakce	interakce	k1gFnSc1	interakce
postav	postava	k1gFnPc2	postava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herectví	herectví	k1gNnSc1	herectví
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
<g/>
,	,	kIx,	,
kamera	kamera	k1gFnSc1	kamera
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
expresionistických	expresionistický	k2eAgInPc2d1	expresionistický
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
složky	složka	k1gFnPc4	složka
tvůrčích	tvůrčí	k2eAgInPc2d1	tvůrčí
postupů	postup	k1gInPc2	postup
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
typické	typický	k2eAgFnSc6d1	typická
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgInPc7	takový
filmy	film	k1gInPc7	film
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
již	již	k6eAd1	již
nesčetněkrát	nesčetněkrát	k6eAd1	nesčetněkrát
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
nebo	nebo	k8xC	nebo
Raskolnikov	Raskolnikov	k1gInSc4	Raskolnikov
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
filmů	film	k1gInPc2	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
některé	některý	k3yIgInPc4	některý
expresionistické	expresionistický	k2eAgInPc4d1	expresionistický
tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
postupy	postup	k1gInPc4	postup
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
herectví	herectví	k1gNnSc1	herectví
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
tematiku	tematika	k1gFnSc4	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravé	pravý	k2eAgInPc1d1	pravý
expresionistické	expresionistický	k2eAgInPc1d1	expresionistický
filmy	film	k1gInPc1	film
připomínají	připomínat	k5eAaImIp3nP	připomínat
Kabinet	kabinet	k1gInSc4	kabinet
doktora	doktor	k1gMnSc4	doktor
Caligariho	Caligari	k1gMnSc4	Caligari
použitím	použití	k1gNnSc7	použití
zdeformovaného	zdeformovaný	k2eAgInSc2d1	zdeformovaný
stylu	styl	k1gInSc2	styl
mizascén	mizascén	k1gInSc1	mizascén
<g/>
,	,	kIx,	,
odvozeného	odvozený	k2eAgInSc2d1	odvozený
z	z	k7c2	z
divadelního	divadelní	k2eAgInSc2d1	divadelní
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgInPc2	takový
filmů	film	k1gInPc2	film
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
pouhého	pouhý	k2eAgInSc2d1	pouhý
půl	půl	k1xP	půl
tuctu	tucet	k1gInSc2	tucet
<g/>
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
historici	historik	k1gMnPc1	historik
do	do	k7c2	do
filmového	filmový	k2eAgMnSc2d1	filmový
expresionismus	expresionismus	k1gInSc4	expresionismus
řadí	řadit	k5eAaImIp3nS	řadit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
snímky	snímek	k1gInPc7	snímek
s	s	k7c7	s
deformovanou	deformovaný	k2eAgFnSc7d1	deformovaná
výtvarnou	výtvarný	k2eAgFnSc7d1	výtvarná
stylizací	stylizace	k1gFnSc7	stylizace
také	také	k6eAd1	také
filmy	film	k1gInPc1	film
obsahující	obsahující	k2eAgFnSc4d1	obsahující
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
stylizaci	stylizace	k1gFnSc4	stylizace
dekorací	dekorace	k1gFnPc2	dekorace
oproti	oproti	k7c3	oproti
Kabinetu	kabinet	k1gInSc3	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
která	který	k3yRgFnSc1	který
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
působí	působit	k5eAaImIp3nS	působit
stejně	stejně	k6eAd1	stejně
expresivně	expresivně	k6eAd1	expresivně
jako	jako	k8xS	jako
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
stylizace	stylizace	k1gFnSc1	stylizace
v	v	k7c4	v
Caligarim	Caligarim	k1gInSc4	Caligarim
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgInPc2	takový
filmů	film	k1gInPc2	film
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jen	jen	k6eAd1	jen
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
tucty	tucet	k1gInPc4	tucet
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
stylizace	stylizace	k1gFnSc1	stylizace
v	v	k7c6	v
expresionistických	expresionistický	k2eAgInPc6d1	expresionistický
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
na	na	k7c4	na
již	již	k6eAd1	již
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
deformovanou	deformovaný	k2eAgFnSc4d1	deformovaná
anebo	anebo	k8xC	anebo
na	na	k7c4	na
symetrickou	symetrický	k2eAgFnSc4d1	symetrická
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
abstraktními	abstraktní	k2eAgInPc7d1	abstraktní
geometrickými	geometrický	k2eAgInPc7d1	geometrický
obrazci	obrazec	k1gInPc7	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
expresionismus	expresionismus	k1gInSc4	expresionismus
ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
propojení	propojení	k1gNnSc1	propojení
mizascény	mizascéna	k1gFnSc2	mizascéna
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
na	na	k7c6	na
sobě	se	k3xPyFc3	se
například	například	k6eAd1	například
měli	mít	k5eAaImAgMnP	mít
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
zapadali	zapadat	k5eAaPmAgMnP	zapadat
do	do	k7c2	do
dekorací	dekorace	k1gFnPc2	dekorace
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
postava	postava	k1gFnSc1	postava
Golema	Golem	k1gMnSc2	Golem
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgNnP	být
mizascéna	mizascéna	k6eAd1	mizascéna
postavena	postavit	k5eAaPmNgNnP	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Upír	upír	k1gMnSc1	upír
Nosferatu	Nosferat	k1gMnSc3	Nosferat
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
sekvenci	sekvence	k1gFnSc6	sekvence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
hraběte	hrabě	k1gMnSc2	hrabě
Orloka	Orloek	k1gMnSc2	Orloek
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
samotného	samotný	k2eAgMnSc4d1	samotný
pána	pán	k1gMnSc4	pán
hradu	hrad	k1gInSc2	hrad
shrbeného	shrbený	k2eAgInSc2d1	shrbený
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
před	před	k7c7	před
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
klenba	klenba	k1gFnSc1	klenba
brány	brána	k1gFnSc2	brána
jakoby	jakoby	k8xS	jakoby
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Orlokovo	Orlokův	k2eAgNnSc1d1	Orlokův
tělo	tělo	k1gNnSc1	tělo
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Expresionističtí	expresionistický	k2eAgMnPc1d1	expresionistický
filmaři	filmař	k1gMnPc1	filmař
většinou	většinou	k6eAd1	většinou
používali	používat	k5eAaImAgMnP	používat
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
nasvícení	nasvícení	k1gNnSc4	nasvícení
zepředu	zepředu	k6eAd1	zepředu
a	a	k8xC	a
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
<g/>
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
nasvícena	nasvítit	k5eAaPmNgFnS	nasvítit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
propojení	propojení	k1gNnSc2	propojení
mezi	mezi	k7c7	mezi
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
dekoracemi	dekorace	k1gFnPc7	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
filmech	film	k1gInPc6	film
využili	využít	k5eAaPmAgMnP	využít
filmaři	filmař	k1gMnPc1	filmař
stínů	stín	k1gInPc2	stín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
deformaci	deformace	k1gFnSc4	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Nádherně	nádherně	k6eAd1	nádherně
to	ten	k3xDgNnSc4	ten
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
sekvenci	sekvence	k1gFnSc6	sekvence
z	z	k7c2	z
Upíra	upír	k1gMnSc2	upír
Nosferatu	Nosferat	k1gInSc2	Nosferat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrabě	hrabě	k1gMnSc1	hrabě
Orlok	Orlok	k1gInSc4	Orlok
stoupá	stoupat	k5eAaImIp3nS	stoupat
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
a	a	k8xC	a
poté	poté	k6eAd1	poté
sahá	sahat	k5eAaImIp3nS	sahat
po	po	k7c6	po
klice	klika	k1gFnSc6	klika
od	od	k7c2	od
pokoje	pokoj	k1gInSc2	pokoj
jeho	jeho	k3xOp3gFnSc2	jeho
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
stín	stín	k1gInSc1	stín
ruky	ruka	k1gFnSc2	ruka
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
přes	přes	k7c4	přes
roh	roh	k1gInSc4	roh
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
prsty	prst	k1gInPc4	prst
se	se	k3xPyFc4	se
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
sahají	sahat	k5eAaImIp3nP	sahat
skoro	skoro	k6eAd1	skoro
až	až	k9	až
ke	k	k7c3	k
klice	klika	k1gFnSc3	klika
ode	ode	k7c2	ode
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmy	kostým	k1gInPc1	kostým
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
odvíjely	odvíjet	k5eAaImAgFnP	odvíjet
od	od	k7c2	od
tematiky	tematika	k1gFnSc2	tematika
filmů	film	k1gInPc2	film
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
dobového	dobový	k2eAgNnSc2d1	dobové
zařazení	zařazení	k1gNnSc2	zařazení
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
filmech	film	k1gInPc6	film
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
nakombinování	nakombinování	k1gNnSc3	nakombinování
dobových	dobový	k2eAgInPc2d1	dobový
obleků	oblek	k1gInPc2	oblek
s	s	k7c7	s
obleky	oblek	k1gInPc7	oblek
expresionistickými	expresionistický	k2eAgInPc7d1	expresionistický
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c4	v
Caligarim	Caligarim	k1gInSc4	Caligarim
nosí	nosit	k5eAaImIp3nS	nosit
většina	většina	k1gFnSc1	většina
postav	postava	k1gFnPc2	postava
oblečení	oblečení	k1gNnSc4	oblečení
typické	typický	k2eAgNnSc4d1	typické
pro	pro	k7c4	pro
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vymyká	vymykat	k5eAaImIp3nS	vymykat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
kostým	kostým	k1gInSc1	kostým
náměsíčníka	náměsíčník	k1gMnSc2	náměsíčník
Cesara	Cesar	k1gMnSc2	Cesar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
kostým	kostým	k1gInSc4	kostým
dívky	dívka	k1gFnSc2	dívka
jménem	jméno	k1gNnSc7	jméno
Jane	Jan	k1gMnSc5	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
o	o	k7c6	o
Nibelunzích	Nibelung	k1gMnPc6	Nibelung
od	od	k7c2	od
Fritze	Fritze	k1gFnSc2	Fritze
Langa	Lang	k1gMnSc2	Lang
jsou	být	k5eAaImIp3nP	být
zase	zase	k9	zase
používány	používán	k2eAgInPc1d1	používán
středověké	středověký	k2eAgInPc1d1	středověký
obleky	oblek	k1gInPc1	oblek
od	od	k7c2	od
různých	různý	k2eAgInPc2d1	různý
hábitů	hábit	k1gInPc2	hábit
po	po	k7c6	po
brnění	brnění	k1gNnSc6	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
němých	němý	k2eAgInPc6d1	němý
filmech	film	k1gInPc6	film
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
herci	herec	k1gMnPc1	herec
byli	být	k5eAaImAgMnP	být
silně	silně	k6eAd1	silně
nalíčeni	nalíčen	k2eAgMnPc1d1	nalíčen
<g/>
.	.	kIx.	.
</s>
<s>
Nejinak	nejinak	k6eAd1	nejinak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
v	v	k7c6	v
expresionistickém	expresionistický	k2eAgInSc6d1	expresionistický
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
přidalo	přidat	k5eAaPmAgNnS	přidat
pár	pár	k4xCyI	pár
nových	nový	k2eAgInPc2d1	nový
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xC	jako
zabarvení	zabarvení	k1gNnSc2	zabarvení
oblasti	oblast	k1gFnSc2	oblast
očí	oko	k1gNnPc2	oko
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
tvůrčím	tvůrčí	k2eAgInSc7d1	tvůrčí
postupem	postup	k1gInSc7	postup
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
expresionismus	expresionismus	k1gInSc1	expresionismus
ve	v	k7c6	v
filmu	film	k1gInSc6	film
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
běžných	běžný	k2eAgInPc2d1	běžný
němých	němý	k2eAgInPc2d1	němý
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
herectví	herectví	k1gNnSc1	herectví
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
hraní	hraní	k1gNnSc2	hraní
byl	být	k5eAaImAgInS	být
přejat	přejmout	k5eAaPmNgInS	přejmout
z	z	k7c2	z
expresionistického	expresionistický	k2eAgNnSc2d1	expresionistické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
herci	herec	k1gMnPc7	herec
až	až	k6eAd1	až
extrémním	extrémní	k2eAgInSc7d1	extrémní
způsobem	způsob	k1gInSc7	způsob
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c6	o
vyjádření	vyjádření	k1gNnSc6	vyjádření
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
herců	herec	k1gMnPc2	herec
byla	být	k5eAaImAgFnS	být
místy	místy	k6eAd1	místy
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
(	(	kIx(	(
<g/>
trhavé	trhavý	k2eAgInPc1d1	trhavý
pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
křičení	křičení	k1gNnPc1	křičení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
tedy	tedy	k8xC	tedy
termín	termín	k1gInSc4	termín
přehnané	přehnaný	k2eAgNnSc4d1	přehnané
herectví	herectví	k1gNnSc4	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
konání	konání	k1gNnSc4	konání
herce	herec	k1gMnSc2	herec
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
vytvořenou	vytvořený	k2eAgFnSc7d1	vytvořená
mizascénou	mizascéna	k1gFnSc7	mizascéna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
herci	herec	k1gMnPc1	herec
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
nečekaně	nečekaně	k6eAd1	nečekaně
zrychlovali	zrychlovat	k5eAaImAgMnP	zrychlovat
své	svůj	k3xOyFgInPc4	svůj
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
ustrnuli	ustrnout	k5eAaPmAgMnP	ustrnout
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
póze	póza	k1gFnSc6	póza
<g/>
.	.	kIx.	.
</s>
<s>
Střihová	střihový	k2eAgFnSc1d1	střihová
skladba	skladba	k1gFnSc1	skladba
filmů	film	k1gInPc2	film
expresionistického	expresionistický	k2eAgNnSc2d1	expresionistické
hnutí	hnutí	k1gNnSc2	hnutí
byla	být	k5eAaImAgFnS	být
většinou	většinou	k6eAd1	většinou
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgInPc1d1	využívající
prostředky	prostředek	k1gInPc1	prostředek
jako	jako	k8xS	jako
záběr	záběr	k1gInSc1	záběr
<g/>
/	/	kIx~	/
<g/>
protizáběr	protizáběr	k1gInSc1	protizáběr
a	a	k8xC	a
prostřih	prostřih	k1gInSc1	prostřih
scén	scéna	k1gFnPc2	scéna
<g/>
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
rytmickému	rytmický	k2eAgInSc3d1	rytmický
rychlému	rychlý	k2eAgInSc3d1	rychlý
střihu	střih	k1gInSc3	střih
francouzských	francouzský	k2eAgMnPc2d1	francouzský
impresionistů	impresionista	k1gMnPc2	impresionista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tempo	tempo	k1gNnSc4	tempo
filmu	film	k1gInSc2	film
velmi	velmi	k6eAd1	velmi
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
pomalejšímu	pomalý	k2eAgNnSc3d2	pomalejší
tempu	tempo	k1gNnSc3	tempo
expresionistických	expresionistický	k2eAgInPc2d1	expresionistický
filmů	film	k1gInPc2	film
má	mít	k5eAaImIp3nS	mít
divák	divák	k1gMnSc1	divák
větší	veliký	k2eAgInSc4d2	veliký
čas	čas	k1gInSc4	čas
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
filmového	filmový	k2eAgInSc2d1	filmový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
kamery	kamera	k1gFnSc2	kamera
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
funkční	funkční	k2eAgNnSc1d1	funkční
než	než	k8xS	než
efektní	efektní	k2eAgNnSc1d1	efektní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
rozkolem	rozkol	k1gInSc7	rozkol
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
impresionistů	impresionista	k1gMnPc2	impresionista
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
kamerou	kamera	k1gFnSc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgInPc4d1	důležitý
tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
individuálních	individuální	k2eAgFnPc6d1	individuální
schopnostech	schopnost	k1gFnPc6	schopnost
režiséra	režisér	k1gMnSc2	režisér
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
moc	moc	k6eAd1	moc
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
experimentovat	experimentovat	k5eAaImF	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejvýraznější	výrazný	k2eAgMnSc1d3	nejvýraznější
experimentátor	experimentátor	k1gMnSc1	experimentátor
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgMnS	projevit
Robert	Robert	k1gMnSc1	Robert
Wiene	Wien	k1gInSc5	Wien
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
takový	takový	k3xDgInSc4	takový
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInPc3	jaký
byl	být	k5eAaImAgInS	být
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
<g/>
,	,	kIx,	,
natočit	natočit	k5eAaBmF	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
experimentátorem	experimentátor	k1gMnSc7	experimentátor
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Murnau	Murna	k1gMnSc3	Murna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
a	a	k8xC	a
také	také	k6eAd1	také
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
natočí	natočit	k5eAaBmIp3nS	natočit
expresionistický	expresionistický	k2eAgInSc4d1	expresionistický
film	film	k1gInSc4	film
v	v	k7c6	v
exteriérech	exteriér	k1gInPc6	exteriér
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
francouzští	francouzský	k2eAgMnPc1d1	francouzský
impresionisté	impresionista	k1gMnPc1	impresionista
tíhli	tíhnout	k5eAaImAgMnP	tíhnout
i	i	k9	i
němečtí	německý	k2eAgMnPc1d1	německý
expresionisté	expresionista	k1gMnPc1	expresionista
k	k	k7c3	k
určitým	určitý	k2eAgInPc3d1	určitý
typům	typ	k1gInPc3	typ
narace	narace	k1gFnSc2	narace
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
jejich	jejich	k3xOp3gNnPc3	jejich
stylu	styl	k1gInSc6	styl
<g/>
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
použil	použít	k5eAaPmAgMnS	použít
příběh	příběh	k1gInSc4	příběh
šílence	šílenec	k1gMnSc2	šílenec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
před	před	k7c7	před
diváky	divák	k1gMnPc7	divák
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
nezvyklé	zvyklý	k2eNgFnSc2d1	nezvyklá
expresionistické	expresionistický	k2eAgFnSc2d1	expresionistická
deformace	deformace	k1gFnSc2	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
subjektivity	subjektivita	k1gFnSc2	subjektivita
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
filmů	film	k1gInPc2	film
neplatí	platit	k5eNaImIp3nP	platit
<g/>
.	.	kIx.	.
</s>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
využíval	využívat	k5eAaImAgInS	využívat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
zasazeny	zasadit	k5eAaPmNgInP	zasadit
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
nebo	nebo	k8xC	nebo
do	do	k7c2	do
exotického	exotický	k2eAgNnSc2d1	exotické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
filmech	film	k1gInPc6	film
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
prvky	prvek	k1gInPc4	prvek
hororu	horor	k1gInSc2	horor
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
tematika	tematika	k1gFnSc1	tematika
expresionistických	expresionistický	k2eAgInPc2d1	expresionistický
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
hlavně	hlavně	k9	hlavně
příběhů	příběh	k1gInPc2	příběh
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
<g/>
:	:	kIx,	:
Nibelungové	Nibelung	k1gMnPc1	Nibelung
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
národního	národní	k2eAgInSc2d1	národní
hrdinského	hrdinský	k2eAgInSc2d1	hrdinský
eposu	epos	k1gInSc2	epos
<g/>
,	,	kIx,	,
Upír	upír	k1gMnSc1	upír
Nosferatu	Nosferat	k1gMnSc3	Nosferat
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Golem	Golem	k1gMnSc1	Golem
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc4	příběh
ze	z	k7c2	z
středověkého	středověký	k2eAgNnSc2d1	středověké
židovského	židovský	k2eAgNnSc2d1	Židovské
ghetta	ghetto	k1gNnSc2	ghetto
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Faust	Faust	k1gFnSc1	Faust
se	se	k3xPyFc4	se
také	také	k9	také
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
metropoli	metropol	k1gFnSc6	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgInPc6	tento
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
rysy	rys	k1gInPc4	rys
fantasy	fantas	k1gInPc4	fantas
a	a	k8xC	a
hororu	horor	k1gInSc6	horor
<g/>
,	,	kIx,	,
expresionisty	expresionista	k1gMnPc4	expresionista
tolik	tolik	k6eAd1	tolik
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
jen	jen	k9	jen
film	film	k1gInSc4	film
Metropolis	Metropolis	k1gFnSc2	Metropolis
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
snímku	snímek	k1gInSc6	snímek
najdeme	najít	k5eAaPmIp1nP	najít
mnoho	mnoho	k4c4	mnoho
fantastických	fantastický	k2eAgInPc2d1	fantastický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
se	se	k3xPyFc4	se
expresionismus	expresionismus	k1gInSc1	expresionismus
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
kritice	kritika	k1gFnSc3	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgInPc7	takový
filmy	film	k1gInPc7	film
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mabuse	Mabuse	k1gFnSc1	Mabuse
<g/>
,	,	kIx,	,
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
či	či	k8xC	či
Algol	Algol	k1gInSc1	Algol
<g/>
.	.	kIx.	.
</s>
<s>
Algol	Algol	k1gInSc1	Algol
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
chamtivém	chamtivý	k2eAgMnSc6d1	chamtivý
průmyslníkovi	průmyslník	k1gMnSc6	průmyslník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
nadpřirozených	nadpřirozený	k2eAgFnPc2d1	nadpřirozená
schopností	schopnost	k1gFnPc2	schopnost
díky	díky	k7c3	díky
záhadné	záhadný	k2eAgFnSc3d1	záhadná
hvězdě	hvězda	k1gFnSc3	hvězda
a	a	k8xC	a
poté	poté	k6eAd1	poté
ony	onen	k3xDgFnPc4	onen
schopnosti	schopnost	k1gFnPc4	schopnost
využije	využít	k5eAaPmIp3nS	využít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
expresionistických	expresionistický	k2eAgInPc2d1	expresionistický
filmů	film	k1gInPc2	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rámcové	rámcový	k2eAgFnPc4d1	rámcová
či	či	k8xC	či
samotné	samotný	k2eAgInPc1d1	samotný
příběhy	příběh	k1gInPc1	příběh
zasazené	zasazený	k2eAgInPc1d1	zasazený
do	do	k7c2	do
většího	veliký	k2eAgInSc2d2	veliký
narativního	narativní	k2eAgInSc2d1	narativní
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Skvělým	skvělý	k2eAgInSc7d1	skvělý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Metropolis	Metropolis	k1gFnSc1	Metropolis
obsahující	obsahující	k2eAgFnSc1d1	obsahující
krom	krom	k7c2	krom
hlavní	hlavní	k2eAgFnSc2d1	hlavní
dějové	dějový	k2eAgFnSc2d1	dějová
linie	linie	k1gFnSc2	linie
i	i	k9	i
několik	několik	k4yIc4	několik
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
například	například	k6eAd1	například
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
vývoj	vývoj	k1gInSc4	vývoj
vztahu	vztah	k1gInSc2	vztah
dvou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
112	[number]	k4	112
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
112	[number]	k4	112
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
121	[number]	k4	121
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
120	[number]	k4	120
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
122	[number]	k4	122
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
178	[number]	k4	178
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
114	[number]	k4	114
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
116	[number]	k4	116
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
116	[number]	k4	116
[	[	kIx(	[
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
Bordwell	Bordwell	k1gInSc1	Bordwell
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
s.	s.	k?	s.
116	[number]	k4	116
JELÍNKOVÁ	Jelínková	k1gFnSc1	Jelínková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
:	:	kIx,	:
Echa	echo	k1gNnSc2	echo
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Recepce	recepce	k1gFnSc1	recepce
německého	německý	k2eAgNnSc2d1	německé
literárního	literární	k2eAgNnSc2d1	literární
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
avantgardě	avantgarda	k1gFnSc6	avantgarda
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
305	[number]	k4	305
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
