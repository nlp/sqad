<s>
RegioJet	RegioJet	k1gInSc1	RegioJet
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
VKM	VKM	kA	VKM
<g/>
:	:	kIx,	:
RJ	RJ	kA	RJ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
provozovatel	provozovatel	k1gMnSc1	provozovatel
osobní	osobní	k2eAgFnSc2d1	osobní
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Stoprocentním	stoprocentní	k2eAgMnSc7d1	stoprocentní
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
Radimem	Radim	k1gMnSc7	Radim
Jančurou	Jančura	k1gFnSc7	Jančura
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
nabídkách	nabídka	k1gFnPc6	nabídka
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
pobočkou	pobočka	k1gFnSc7	pobočka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
firmy	firma	k1gFnSc2	firma
Keolis	Keolis	k1gFnSc2	Keolis
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostnou	příležitostný	k2eAgFnSc4d1	příležitostná
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
železniční	železniční	k2eAgFnSc4d1	železniční
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
společnost	společnost	k1gFnSc1	společnost
poprvé	poprvé	k6eAd1	poprvé
prováděla	provádět	k5eAaImAgFnS	provádět
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc1	provoz
první	první	k4xOgFnSc2	první
linky	linka	k1gFnSc2	linka
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
společnost	společnost	k1gFnSc1	společnost
ztrátu	ztráta	k1gFnSc4	ztráta
59	[number]	k4	59
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
ztrátu	ztráta	k1gFnSc4	ztráta
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
na	na	k7c4	na
76	[number]	k4	76
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
při	při	k7c6	při
tržbách	tržba	k1gFnPc6	tržba
267	[number]	k4	267
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
očekávala	očekávat	k5eAaImAgFnS	očekávat
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
<g/>
.	.	kIx.	.
</s>
<s>
Sesterská	sesterský	k2eAgFnSc1d1	sesterská
společnost	společnost	k1gFnSc1	společnost
shodného	shodný	k2eAgInSc2d1	shodný
názvu	název	k1gInSc2	název
RegioJet	RegioJet	k1gMnSc1	RegioJet
a.	a.	k?	a.
s.	s.	k?	s.
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
je	být	k5eAaImIp3nS	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
osobní	osobní	k2eAgFnSc2d1	osobní
železniční	železniční	k2eAgFnSc2d1	železniční
a	a	k8xC	a
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
začala	začít	k5eAaPmAgFnS	začít
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
RegioJet	RegioJet	k1gInSc4	RegioJet
provozovat	provozovat	k5eAaImF	provozovat
slovenská	slovenský	k2eAgFnSc1d1	slovenská
pobočka	pobočka	k1gFnSc1	pobočka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
i	i	k8xC	i
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zahájen	zahájit	k5eAaPmNgInS	zahájit
rebranding	rebranding	k1gInSc1	rebranding
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
na	na	k7c4	na
značku	značka	k1gFnSc4	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
byla	být	k5eAaImAgFnS	být
RegioJet	RegioJet	k1gInSc4	RegioJet
a.s.	a.s.	k?	a.s.
zapsána	zapsán	k2eAgFnSc1d1	zapsána
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
s	s	k7c7	s
předmětem	předmět	k1gInSc7	předmět
podnikání	podnikání	k1gNnSc2	podnikání
"	"	kIx"	"
<g/>
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
neuvedené	uvedený	k2eNgFnPc1d1	neuvedená
v	v	k7c6	v
přílohách	příloha	k1gFnPc6	příloha
1	[number]	k4	1
až	až	k8xS	až
3	[number]	k4	3
živnostenského	živnostenský	k2eAgInSc2d1	živnostenský
zákona	zákon	k1gInSc2	zákon
<g/>
"	"	kIx"	"
a	a	k8xC	a
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
předmět	předmět	k1gInSc1	předmět
činnosti	činnost	k1gFnSc2	činnost
"	"	kIx"	"
<g/>
provozování	provozování	k1gNnSc3	provozování
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
dvoučlenného	dvoučlenný	k2eAgNnSc2d1	dvoučlenné
představenstva	představenstvo	k1gNnSc2	představenstvo
je	být	k5eAaImIp3nS	být
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančura	k1gFnSc1	Jančura
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgMnSc1d1	obchodní
ředitel	ředitel	k1gMnSc1	ředitel
Jiří	Jiří	k1gMnSc1	Jiří
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgMnSc1d1	provozní
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Jan	Jan	k1gMnSc1	Jan
Paroubek	Paroubek	k1gInSc4	Paroubek
dal	dát	k5eAaPmAgMnS	dát
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
RegioJet	RegioJet	k1gMnSc1	RegioJet
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
provozu	provoz	k1gInSc2	provoz
výpověď	výpověď	k1gFnSc1	výpověď
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
nesystémovými	systémový	k2eNgInPc7d1	nesystémový
zásahy	zásah	k1gInPc7	zásah
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
špatným	špatný	k2eAgInSc7d1	špatný
systémem	systém	k1gInSc7	systém
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
dotaz	dotaz	k1gInSc4	dotaz
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
komentovat	komentovat	k5eAaBmF	komentovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
médií	médium	k1gNnPc2	médium
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
do	do	k7c2	do
obdobné	obdobný	k2eAgFnSc2d1	obdobná
funkce	funkce	k1gFnSc2	funkce
ke	k	k7c3	k
konkurenčnímu	konkurenční	k2eAgMnSc3d1	konkurenční
dopravci	dopravce	k1gMnSc3	dopravce
LEO	Leo	k1gMnSc1	Leo
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
připravit	připravit	k5eAaPmF	připravit
rozjezd	rozjezd	k1gInSc4	rozjezd
linky	linka	k1gFnSc2	linka
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
provozním	provozní	k2eAgMnSc7d1	provozní
ředitelem	ředitel	k1gMnSc7	ředitel
RegioJet	RegioJet	k1gMnSc1	RegioJet
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
Petr	Petr	k1gMnSc1	Petr
Prchal	Prchal	k1gMnSc1	Prchal
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
přivedl	přivést	k5eAaPmAgMnS	přivést
a	a	k8xC	a
"	"	kIx"	"
<g/>
vychoval	vychovat	k5eAaPmAgInS	vychovat
<g/>
"	"	kIx"	"
Paroubek	Paroubek	k1gInSc1	Paroubek
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgMnSc1d1	provozní
ředitel	ředitel	k1gMnSc1	ředitel
Petr	Petr	k1gMnSc1	Petr
Prchal	Prchal	k1gMnSc1	Prchal
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2012	[number]	k4	2012
má	mít	k5eAaImIp3nS	mít
ukončit	ukončit	k5eAaPmF	ukončit
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
RegioJetu	RegioJet	k1gInSc6	RegioJet
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
mluvčího	mluvčí	k1gMnSc2	mluvčí
firmy	firma	k1gFnSc2	firma
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
má	mít	k5eAaImIp3nS	mít
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
Leo	Leo	k1gMnSc1	Leo
Expressu	express	k1gInSc2	express
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gInSc1	RegioJet
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
inzerátem	inzerát	k1gInSc7	inzerát
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
hledal	hledat	k5eAaImAgMnS	hledat
nového	nový	k2eAgMnSc4d1	nový
provozního	provozní	k2eAgMnSc4d1	provozní
ředitele	ředitel	k1gMnSc4	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
v	v	k7c6	v
několika	několik	k4yIc6	několik
rozhovorech	rozhovor	k1gInPc6	rozhovor
záměr	záměr	k1gInSc1	záměr
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
v	v	k7c6	v
horizontu	horizont	k1gInSc6	horizont
zhruba	zhruba	k6eAd1	zhruba
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
stala	stát	k5eAaPmAgFnS	stát
i	i	k9	i
železničním	železniční	k2eAgMnSc7d1	železniční
dopravcem	dopravce	k1gMnSc7	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
provozování	provozování	k1gNnSc1	provozování
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
konkurovala	konkurovat	k5eAaImAgFnS	konkurovat
vlakům	vlak	k1gInPc3	vlak
EC	EC	kA	EC
<g/>
/	/	kIx~	/
<g/>
IC	IC	kA	IC
a	a	k8xC	a
SC	SC	kA	SC
Pendolino	Pendolina	k1gFnSc5	Pendolina
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
výhledově	výhledově	k6eAd1	výhledově
naznačil	naznačit	k5eAaPmAgInS	naznačit
i	i	k9	i
možnou	možný	k2eAgFnSc4d1	možná
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
dotované	dotovaný	k2eAgFnSc6d1	dotovaná
regionální	regionální	k2eAgFnSc6d1	regionální
dopravě	doprava	k1gFnSc6	doprava
nebo	nebo	k8xC	nebo
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
dopravě	doprava	k1gFnSc6	doprava
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Berlín	Berlín	k1gInSc1	Berlín
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
probíhá	probíhat	k5eAaImIp3nS	probíhat
výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
výrobce	výrobce	k1gMnSc4	výrobce
vlaku	vlak	k1gInSc2	vlak
pro	pro	k7c4	pro
trať	trať	k1gFnSc4	trať
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
pět	pět	k4xCc4	pět
žlutých	žlutý	k2eAgFnPc2d1	žlutá
souprav	souprava	k1gFnPc2	souprava
v	v	k7c6	v
typické	typický	k2eAgFnSc6d1	typická
žluté	žlutý	k2eAgFnSc6d1	žlutá
firemní	firemní	k2eAgFnSc6d1	firemní
barvě	barva	k1gFnSc6	barva
(	(	kIx(	(
<g/>
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
dohromady	dohromady	k6eAd1	dohromady
o	o	k7c4	o
šest	šest	k4xCc4	šest
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednu	jeden	k4xCgFnSc4	jeden
rezervní	rezervní	k2eAgFnSc1d1	rezervní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
levnější	levný	k2eAgFnSc1d2	levnější
a	a	k8xC	a
luxusnější	luxusní	k2eAgFnSc1d2	luxusnější
než	než	k8xS	než
v	v	k7c6	v
Pendolinu	Pendolin	k1gInSc6	Pendolin
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
souprav	souprava	k1gFnPc2	souprava
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
poloviční	poloviční	k2eAgFnSc1d1	poloviční
oproti	oproti	k7c3	oproti
Pendolinům	Pendolin	k1gInPc3	Pendolin
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
firmy	firma	k1gFnPc1	firma
Siemens	siemens	k1gInSc1	siemens
a	a	k8xC	a
Stadler	Stadler	k1gInSc1	Stadler
<g/>
,	,	kIx,	,
design	design	k1gInSc1	design
vlaku	vlak	k1gInSc2	vlak
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
blížit	blížit	k5eAaImF	blížit
vlakům	vlak	k1gInPc3	vlak
ICE	ICE	kA	ICE
3	[number]	k4	3
<g/>
,	,	kIx,	,
jezdícím	jezdící	k2eAgNnSc6d1	jezdící
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
–	–	k?	–
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
rok	rok	k1gInSc4	rok
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vlaku	vlak	k1gInSc2	vlak
a	a	k8xC	a
rok	rok	k1gInSc4	rok
pro	pro	k7c4	pro
homologaci	homologace	k1gFnSc4	homologace
–	–	k?	–
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
zprávách	zpráva	k1gFnPc6	zpráva
a	a	k8xC	a
rozhovorech	rozhovor	k1gInPc6	rozhovor
pak	pak	k6eAd1	pak
uváděl	uvádět	k5eAaImAgMnS	uvádět
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
přibyl	přibýt	k5eAaPmAgInS	přibýt
ještě	ještě	k9	ještě
rok	rok	k1gInSc4	rok
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
bohatnutí	bohatnutí	k1gNnSc4	bohatnutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
oznámil	oznámit	k5eAaPmAgInS	oznámit
záměr	záměr	k1gInSc1	záměr
obsadit	obsadit	k5eAaPmF	obsadit
výhledově	výhledově	k6eAd1	výhledově
všechny	všechen	k3xTgFnPc4	všechen
železniční	železniční	k2eAgFnPc4d1	železniční
dálkové	dálkový	k2eAgFnPc4d1	dálková
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
Jančura	Jančura	k1gFnSc1	Jančura
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
společného	společný	k2eAgInSc2d1	společný
podniku	podnik	k1gInSc2	podnik
s	s	k7c7	s
třetím	třetí	k4xOgMnSc7	třetí
největším	veliký	k2eAgMnSc7d3	veliký
dopravcem	dopravce	k1gMnSc7	dopravce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
firmou	firma	k1gFnSc7	firma
Keolis	Keolis	k1gFnSc2	Keolis
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
společně	společně	k6eAd1	společně
jednají	jednat	k5eAaImIp3nP	jednat
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
kraji	kraj	k1gInPc7	kraj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
výběrová	výběrový	k2eAgNnPc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c6	na
jimi	on	k3xPp3gMnPc7	on
dotovanou	dotovaný	k2eAgFnSc4d1	dotovaná
regionální	regionální	k2eAgFnSc4d1	regionální
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Keolis	Keolis	k1gFnSc1	Keolis
Deutschland	Deutschlanda	k1gFnPc2	Deutschlanda
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
se	se	k3xPyFc4	se
ucházely	ucházet	k5eAaImAgFnP	ucházet
ve	v	k7c6	v
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
řízení	řízení	k1gNnSc6	řízení
o	o	k7c4	o
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
%	%	kIx~	%
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgNnSc1d1	označované
též	též	k9	též
Jizerskohorská	jizerskohorský	k2eAgFnSc1d1	Jizerskohorská
železnice	železnice	k1gFnSc1	železnice
<g/>
)	)	kIx)	)
objednávané	objednávaný	k2eAgNnSc1d1	objednávané
Libereckým	liberecký	k2eAgInSc7d1	liberecký
krajem	kraj	k1gInSc7	kraj
na	na	k7c4	na
15	[number]	k4	15
let	léto	k1gNnPc2	léto
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
vyhlášeném	vyhlášený	k2eAgMnSc6d1	vyhlášený
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
co	co	k3yInSc1	co
společně	společně	k6eAd1	společně
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
dalšími	další	k2eAgMnPc7d1	další
zájemci	zájemce	k1gMnPc7	zájemce
(	(	kIx(	(
<g/>
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
Viamont	Viamont	k1gMnSc1	Viamont
a	a	k8xC	a
Arriva	Arriva	k1gFnSc1	Arriva
<g/>
)	)	kIx)	)
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
pěti	pět	k4xCc2	pět
postoupily	postoupit	k5eAaPmAgFnP	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
ještě	ještě	k9	ještě
před	před	k7c7	před
podáním	podání	k1gNnSc7	podání
závazných	závazný	k2eAgFnPc2d1	závazná
nabídek	nabídka	k1gFnPc2	nabídka
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
odstoupily	odstoupit	k5eAaPmAgInP	odstoupit
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgInSc4d1	poslední
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
soustavně	soustavně	k6eAd1	soustavně
varováni	varovat	k5eAaImNgMnP	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dumpingová	dumpingový	k2eAgFnSc1d1	dumpingová
cena	cena	k1gFnSc1	cena
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
výsměchem	výsměch	k1gInSc7	výsměch
rovné	rovný	k2eAgFnSc3d1	rovná
soutěži	soutěž	k1gFnSc3	soutěž
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
mohou	moct	k5eAaImIp3nP	moct
krýt	krýt	k5eAaImF	krýt
ztráty	ztráta	k1gFnPc4	ztráta
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
úhrad	úhrada	k1gFnPc2	úhrada
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
rychlíkové	rychlíkový	k2eAgInPc4d1	rychlíkový
spoje	spoj	k1gInPc4	spoj
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
úřad	úřad	k1gInSc1	úřad
neprověřuje	prověřovat	k5eNaImIp3nS	prověřovat
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
iracionální	iracionální	k2eAgInSc1d1	iracionální
systém	systém	k1gInSc1	systém
vymykající	vymykající	k2eAgFnSc2d1	vymykající
se	se	k3xPyFc4	se
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
se	se	k3xPyFc4	se
ohradily	ohradit	k5eAaPmAgFnP	ohradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
ceně	cena	k1gFnSc6	cena
nabízené	nabízený	k2eAgInPc1d1	nabízený
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
mohla	moct	k5eAaImAgFnS	moct
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
získat	získat	k5eAaPmF	získat
pouze	pouze	k6eAd1	pouze
nelegální	legální	k2eNgFnSc7d1	nelegální
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
rovněž	rovněž	k9	rovněž
firmu	firma	k1gFnSc4	firma
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
nařkl	nařknout	k5eAaPmAgMnS	nařknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
uvést	uvést	k5eAaPmF	uvést
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
železniční	železniční	k2eAgInSc4d1	železniční
projekt	projekt	k1gInSc4	projekt
v	v	k7c4	v
život	život	k1gInSc4	život
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
této	tento	k3xDgFnSc2	tento
problematiky	problematika	k1gFnSc2	problematika
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
zviditelňování	zviditelňování	k1gNnSc3	zviditelňování
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgMnSc1d1	provozní
ředitel	ředitel	k1gMnSc1	ředitel
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnPc4	Agenca
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvojice	dvojice	k1gFnSc1	dvojice
společností	společnost	k1gFnPc2	společnost
od	od	k7c2	od
podání	podání	k1gNnSc2	podání
závazné	závazný	k2eAgFnSc2d1	závazná
nabídky	nabídka	k1gFnSc2	nabídka
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
firmy	firma	k1gFnSc2	firma
Keolis	Keolis	k1gFnSc2	Keolis
a	a	k8xC	a
že	že	k8xS	že
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
tohoto	tento	k3xDgInSc2	tento
ústupu	ústup	k1gInSc2	ústup
lituje	litovat	k5eAaImIp3nS	litovat
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
se	se	k3xPyFc4	se
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
Jihomoravským	jihomoravský	k2eAgInSc7d1	jihomoravský
krajem	kraj	k1gInSc7	kraj
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypracuje	vypracovat	k5eAaPmIp3nS	vypracovat
nabídky	nabídka	k1gFnPc4	nabídka
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
jihomoravských	jihomoravský	k2eAgFnPc2d1	Jihomoravská
linek	linka	k1gFnPc2	linka
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
S4	S4	k1gFnSc1	S4
a	a	k8xC	a
S	s	k7c7	s
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
kraj	kraj	k1gInSc1	kraj
oznámil	oznámit	k5eAaPmAgInS	oznámit
záměr	záměr	k1gInSc4	záměr
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
finanční	finanční	k2eAgInPc1d1	finanční
požadavky	požadavek	k1gInPc1	požadavek
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
připadaly	připadat	k5eAaImAgInP	připadat
neúměrné	úměrný	k2eNgInPc1d1	neúměrný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dopravce	dopravce	k1gMnSc1	dopravce
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
již	již	k6eAd1	již
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
omezení	omezení	k1gNnSc1	omezení
nabídky	nabídka	k1gFnSc2	nabídka
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
S2	S2	k1gFnPc2	S2
a	a	k8xC	a
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
nabídkou	nabídka	k1gFnSc7	nabídka
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kraj	kraj	k1gInSc1	kraj
zajistil	zajistit	k5eAaPmAgInS	zajistit
podmínky	podmínka	k1gFnPc4	podmínka
čerpání	čerpání	k1gNnSc2	čerpání
státní	státní	k2eAgFnSc2d1	státní
dotace	dotace	k1gFnSc2	dotace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Memoranda	memorandum	k1gNnSc2	memorandum
o	o	k7c4	o
zajištění	zajištění	k1gNnSc4	zajištění
stability	stabilita	k1gFnSc2	stabilita
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
55	[number]	k4	55
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
ze	z	k7c2	z
smlouvy	smlouva	k1gFnSc2	smlouva
vyjmuto	vyjmut	k2eAgNnSc1d1	vyjmuto
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
kraj	kraj	k1gInSc1	kraj
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
smlouvu	smlouva	k1gFnSc4	smlouva
dosavadnímu	dosavadní	k2eAgMnSc3d1	dosavadní
dopravci	dopravce	k1gMnSc3	dopravce
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
věděl	vědět	k5eAaImAgMnS	vědět
o	o	k7c6	o
zájmu	zájem	k1gInSc6	zájem
jiné	jiný	k2eAgFnSc2d1	jiná
společnosti	společnost	k1gFnSc2	společnost
dopravu	doprava	k1gFnSc4	doprava
provozovat	provozovat	k5eAaImF	provozovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
distancoval	distancovat	k5eAaBmAgMnS	distancovat
se	se	k3xPyFc4	se
od	od	k7c2	od
závěrů	závěr	k1gInPc2	závěr
dosavadních	dosavadní	k2eAgNnPc2d1	dosavadní
jednání	jednání	k1gNnPc2	jednání
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
dopravcem	dopravce	k1gMnSc7	dopravce
i	i	k8xC	i
Sdružením	sdružení	k1gNnSc7	sdružení
železničních	železniční	k2eAgFnPc2d1	železniční
společností	společnost	k1gFnPc2	společnost
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
krajem	krajem	k6eAd1	krajem
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
rovněž	rovněž	k9	rovněž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hejtman	hejtman	k1gMnSc1	hejtman
Hašek	Hašek	k1gMnSc1	Hašek
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
obhájit	obhájit	k5eAaPmF	obhájit
krok	krok	k1gInSc4	krok
kraje	kraj	k1gInSc2	kraj
označoval	označovat	k5eAaImAgMnS	označovat
dotyčné	dotyčný	k2eAgFnSc3d1	dotyčná
tratě	trata	k1gFnSc3	trata
"	"	kIx"	"
<g/>
rozinkami	rozinka	k1gFnPc7	rozinka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tratě	trať	k1gFnPc4	trať
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
ztrátovostí	ztrátovost	k1gFnSc7	ztrátovost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
a	a	k8xC	a
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
prioritou	priorita	k1gFnSc7	priorita
obnovy	obnova	k1gFnSc2	obnova
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
rada	rada	k1gFnSc1	rada
kraje	kraj	k1gInSc2	kraj
záměr	záměr	k1gInSc4	záměr
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
schválila	schválit	k5eAaPmAgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
zaslala	zaslat	k5eAaPmAgFnS	zaslat
Evropské	evropský	k2eAgFnSc6d1	Evropská
komisi	komise	k1gFnSc6	komise
stížnost	stížnost	k1gFnSc4	stížnost
na	na	k7c4	na
netransparentní	transparentní	k2eNgInSc4d1	netransparentní
a	a	k8xC	a
diskriminační	diskriminační	k2eAgInSc4d1	diskriminační
přístup	přístup	k1gInSc4	přístup
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
za	za	k7c4	za
chybný	chybný	k2eAgInSc4d1	chybný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
liberalizaci	liberalizace	k1gFnSc3	liberalizace
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
zažaluje	zažalovat	k5eAaPmIp3nS	zažalovat
u	u	k7c2	u
evropské	evropský	k2eAgFnSc2d1	Evropská
justice	justice	k1gFnSc2	justice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2010	[number]	k4	2010
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
českého	český	k2eAgInSc2d1	český
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
požádala	požádat	k5eAaPmAgFnS	požádat
kraj	kraj	k1gInSc4	kraj
o	o	k7c4	o
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc4	kraj
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
prodloužení	prodloužení	k1gNnSc4	prodloužení
30	[number]	k4	30
<g/>
denní	denní	k2eAgFnPc4d1	denní
lhůty	lhůta	k1gFnPc4	lhůta
pro	pro	k7c4	pro
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
rada	rada	k1gFnSc1	rada
kraje	kraj	k1gInSc2	kraj
schválila	schválit	k5eAaPmAgFnS	schválit
svou	svůj	k3xOyFgFnSc4	svůj
28	[number]	k4	28
<g/>
stránkovou	stránkový	k2eAgFnSc4d1	stránková
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
kraj	kraj	k1gInSc1	kraj
<g />
.	.	kIx.	.
</s>
<s>
údajně	údajně	k6eAd1	údajně
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Radima	Radim	k1gMnSc4	Radim
Jančuru	Jančur	k1gInSc2	Jančur
ze	z	k7c2	z
lži	lež	k1gFnSc2	lež
a	a	k8xC	a
hájil	hájit	k5eAaImAgMnS	hájit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
dopravce	dopravce	k1gMnSc1	dopravce
nepředložil	předložit	k5eNaPmAgMnS	předložit
nabídku	nabídka	k1gFnSc4	nabídka
pro	pro	k7c4	pro
provozování	provozování	k1gNnSc4	provozování
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
kraje	kraj	k1gInPc1	kraj
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
obdobné	obdobný	k2eAgFnPc4d1	obdobná
smlouvy	smlouva	k1gFnPc4	smlouva
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgInS	vydat
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
tiskovou	tiskový	k2eAgFnSc4d1	tisková
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
rada	rada	k1gFnSc1	rada
kraje	kraj	k1gInSc2	kraj
schválila	schválit	k5eAaPmAgFnS	schválit
přípravu	příprava	k1gFnSc4	příprava
nabídkového	nabídkový	k2eAgNnSc2d1	nabídkové
řízení	řízení	k1gNnSc2	řízení
pro	pro	k7c4	pro
linky	linka	k1gFnPc4	linka
S6	S6	k1gMnPc2	S6
a	a	k8xC	a
R6	R6	k1gMnPc2	R6
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Bučovice	Bučovice	k1gFnPc4	Bučovice
–	–	k?	–
Kyjov	Kyjov	k1gInSc1	Kyjov
–	–	k?	–
Zlínský	zlínský	k2eAgInSc1d1	zlínský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
18	[number]	k4	18
%	%	kIx~	%
vlakové	vlakový	k2eAgFnSc2d1	vlaková
dopravy	doprava	k1gFnSc2	doprava
objednávané	objednávaný	k2eAgFnPc1d1	objednávaná
krajem	krajem	k6eAd1	krajem
<g/>
,	,	kIx,	,
vybraný	vybraný	k2eAgMnSc1d1	vybraný
dopravce	dopravce	k1gMnSc1	dopravce
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
dopravu	doprava	k1gFnSc4	doprava
převzít	převzít	k5eAaPmF	převzít
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
zvažoval	zvažovat	k5eAaImAgInS	zvažovat
i	i	k9	i
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
S2	S2	k1gMnPc2	S2
a	a	k8xC	a
S3	S3	k1gMnPc2	S3
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Blansko	Blansko	k1gNnSc1	Blansko
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Tišnov	Tišnov	k1gInSc1	Tišnov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
záměr	záměr	k1gInSc1	záměr
zatím	zatím	k6eAd1	zatím
odložil	odložit	k5eAaPmAgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
z	z	k7c2	z
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
podá	podat	k5eAaPmIp3nS	podat
ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
nabídku	nabídka	k1gFnSc4	nabídka
provozování	provozování	k1gNnSc2	provozování
rychlíkové	rychlíkový	k2eAgFnSc2d1	rychlíková
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
evropského	evropský	k2eAgNnSc2d1	Evropské
práva	právo	k1gNnSc2	právo
by	by	kYmCp3nS	by
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
takové	takový	k3xDgFnSc2	takový
nabídky	nabídka	k1gFnSc2	nabídka
muselo	muset	k5eAaImAgNnS	muset
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vypsat	vypsat	k5eAaPmF	vypsat
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
nabídnout	nabídnout	k5eAaPmF	nabídnout
cenu	cena	k1gFnSc4	cena
až	až	k9	až
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
náklady	náklad	k1gInPc1	náklad
účtované	účtovaný	k2eAgInPc1d1	účtovaný
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
trasách	trasa	k1gFnPc6	trasa
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
(	(	kIx(	(
<g/>
cca	cca	kA	cca
120	[number]	k4	120
Kč	Kč	kA	Kč
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
pak	pak	k6eAd1	pak
podala	podat	k5eAaPmAgFnS	podat
nabídku	nabídka	k1gFnSc4	nabídka
provozování	provozování	k1gNnSc2	provozování
rychlíkové	rychlíkový	k2eAgFnSc2d1	rychlíková
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
15	[number]	k4	15
linkách	linka	k1gFnPc6	linka
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
3	[number]	k4	3
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Linky	linka	k1gFnPc1	linka
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
nabídky	nabídka	k1gFnSc2	nabídka
obsluhovány	obsluhován	k2eAgInPc1d1	obsluhován
novými	nový	k2eAgInPc7d1	nový
vlaky	vlak	k1gInPc7	vlak
Siemens	siemens	k1gInSc1	siemens
Desiro	Desiro	k1gNnSc4	Desiro
ML	ml	kA	ml
(	(	kIx(	(
<g/>
50	[number]	k4	50
souprav	souprava	k1gFnPc2	souprava
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgInPc6	čtyři
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
linek	linka	k1gFnPc2	linka
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
–	–	k?	–
elektrické	elektrický	k2eAgFnSc2d1	elektrická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Desiro	Desiro	k1gNnSc1	Desiro
Classic	Classice	k1gFnPc2	Classice
(	(	kIx(	(
<g/>
35	[number]	k4	35
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
linek	linka	k1gFnPc2	linka
3	[number]	k4	3
–	–	k?	–
dieselové	dieselový	k2eAgFnPc1d1	dieselová
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
připravenost	připravenost	k1gFnSc4	připravenost
vstoupit	vstoupit	k5eAaPmF	vstoupit
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
dopravci	dopravce	k1gMnPc7	dopravce
do	do	k7c2	do
tarifních	tarifní	k2eAgInPc2d1	tarifní
svazů	svaz	k1gInPc2	svaz
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
skutečně	skutečně	k6eAd1	skutečně
RegioJet	RegioJet	k1gInSc1	RegioJet
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
provozování	provozování	k1gNnSc4	provozování
rychlíků	rychlík	k1gInPc2	rychlík
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
10	[number]	k4	10
let	léto	k1gNnPc2	léto
na	na	k7c6	na
15	[number]	k4	15
železničních	železniční	k2eAgFnPc6d1	železniční
linkách	linka	k1gFnPc6	linka
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
by	by	kYmCp3nS	by
nasadil	nasadit	k5eAaPmAgMnS	nasadit
nové	nový	k2eAgInPc4d1	nový
elektrické	elektrický	k2eAgInPc4d1	elektrický
a	a	k8xC	a
dieselové	dieselový	k2eAgInPc4d1	dieselový
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
rozsahu	rozsah	k1gInSc2	rozsah
rychlíkové	rychlíkový	k2eAgFnSc2d1	rychlíková
dopravy	doprava	k1gFnSc2	doprava
objednávané	objednávaný	k2eAgNnSc1d1	objednávané
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ČR	ČR	kA	ČR
bez	bez	k7c2	bez
výběrových	výběrový	k2eAgNnPc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
u	u	k7c2	u
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Partnerem	partner	k1gMnSc7	partner
pro	pro	k7c4	pro
investice	investice	k1gFnPc4	investice
do	do	k7c2	do
vlaků	vlak	k1gInPc2	vlak
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
francouzská	francouzský	k2eAgFnSc1d1	francouzská
firma	firma	k1gFnSc1	firma
Keolis	Keolis	k1gFnSc2	Keolis
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nabídce	nabídka	k1gFnSc6	nabídka
Keolis	Keolis	k1gFnSc2	Keolis
zaangažován	zaangažován	k2eAgMnSc1d1	zaangažován
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
linek	linka	k1gFnPc2	linka
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Chomutov	Chomutov	k1gInSc1	Chomutov
–	–	k?	–
Cheb	Cheb	k1gInSc1	Cheb
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Děčín	Děčín	k1gInSc1	Děčín
Kolín	Kolín	k1gInSc1	Kolín
–	–	k?	–
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Bohumín	Bohumín	k1gInSc1	Bohumín
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Břeclav	Břeclav	k1gFnSc1	Břeclav
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Tábor	Tábor	k1gInSc4	Tábor
–	–	k?	–
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Písek	Písek	k1gInSc1	Písek
–	–	k?	–
České	český	k2eAgFnSc2d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
Budějovice	Budějovice	k1gInPc1	Budějovice
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Tanvald	Tanvald	k1gInSc1	Tanvald
Kolín	Kolín	k1gInSc1	Kolín
–	–	k?	–
Rumburk	Rumburk	k1gInSc1	Rumburk
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Rakovník	Rakovník	k1gInSc1	Rakovník
Ostrava	Ostrava	k1gFnSc1	Ostrava
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
/	/	kIx~	/
<g/>
Jeseník	Jeseník	k1gInSc1	Jeseník
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
kraj	kraj	k1gInSc1	kraj
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
neelektrizovaných	elektrizovaný	k2eNgFnPc6d1	neelektrizovaná
tratích	trať	k1gFnPc6	trať
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
–	–	k?	–
<g/>
Blatno	Blatno	k1gNnSc1	Blatno
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
–	–	k?	–
<g/>
Domažlice	Domažlice	k1gFnPc1	Domažlice
a	a	k8xC	a
Nýřany	Nýřan	k1gInPc1	Nýřan
–	–	k?	–
Heřmanova	Heřmanův	k2eAgFnSc1d1	Heřmanova
Huť	huť	k1gFnSc1	huť
<g/>
)	)	kIx)	)
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2026	[number]	k4	2026
<g/>
.	.	kIx.	.
</s>
<s>
Nabídky	nabídka	k1gFnPc1	nabídka
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
otevírány	otevírán	k2eAgFnPc4d1	otevírán
29	[number]	k4	29
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
vyhlášeny	vyhlášen	k2eAgInPc4d1	vyhlášen
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Zadávací	zadávací	k2eAgFnPc1d1	zadávací
podmínky	podmínka	k1gFnPc1	podmínka
si	se	k3xPyFc3	se
vyzvedlo	vyzvednout	k5eAaPmAgNnS	vyzvednout
8	[number]	k4	8
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
přihlásily	přihlásit	k5eAaPmAgFnP	přihlásit
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
Viamont	Viamont	k1gInSc1	Viamont
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Jablonec	Jablonec	k1gInSc1	Jablonec
a	a	k8xC	a
RegioJet	RegioJet	k1gInSc1	RegioJet
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
vydala	vydat	k5eAaPmAgFnS	vydat
vláda	vláda	k1gFnSc1	vláda
"	"	kIx"	"
<g/>
Memorandum	memorandum	k1gNnSc1	memorandum
o	o	k7c4	o
zajištění	zajištění	k1gNnSc4	zajištění
stabilního	stabilní	k2eAgNnSc2d1	stabilní
financování	financování	k1gNnSc2	financování
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obslužnosti	obslužnost	k1gFnSc2	obslužnost
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
regionální	regionální	k2eAgFnSc7d1	regionální
železniční	železniční	k2eAgFnSc7d1	železniční
osobní	osobní	k2eAgFnSc7d1	osobní
dopravou	doprava	k1gFnSc7	doprava
<g/>
"	"	kIx"	"
a	a	k8xC	a
všechny	všechen	k3xTgMnPc4	všechen
<g />
.	.	kIx.	.
</s>
<s>
kraje	kraj	k1gInPc1	kraj
i	i	k8xC	i
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
urychleně	urychleně	k6eAd1	urychleně
a	a	k8xC	a
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
10	[number]	k4	10
let	léto	k1gNnPc2	léto
s	s	k7c7	s
opcí	opce	k1gFnSc7	opce
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
5	[number]	k4	5
let	léto	k1gNnPc2	léto
s	s	k7c7	s
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
nařízení	nařízení	k1gNnSc2	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
ES	ES	kA	ES
č.	č.	k?	č.
1370	[number]	k4	1370
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
výběrová	výběrový	k2eAgNnPc4d1	výběrové
řízení	řízení	k1gNnPc4	řízení
pro	pro	k7c4	pro
přidělování	přidělování	k1gNnSc4	přidělování
zakázek	zakázka	k1gFnPc2	zakázka
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rada	rada	k1gFnSc1	rada
kraje	kraj	k1gInSc2	kraj
soutěž	soutěž	k1gFnSc1	soutěž
zrušila	zrušit	k5eAaPmAgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Soukromí	soukromý	k2eAgMnPc1d1	soukromý
dopravci	dopravce	k1gMnPc1	dopravce
včetně	včetně	k7c2	včetně
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
upevnění	upevnění	k1gNnSc3	upevnění
monopolu	monopol	k1gInSc2	monopol
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
ostře	ostro	k6eAd1	ostro
ohradili	ohradit	k5eAaPmAgMnP	ohradit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
RegioJet	RegioJet	k1gMnSc1	RegioJet
oznámil	oznámit	k5eAaPmAgMnS	oznámit
reklamní	reklamní	k2eAgInSc4d1	reklamní
provoz	provoz	k1gInSc4	provoz
soupravy	souprava	k1gFnSc2	souprava
Siemens	siemens	k1gInSc1	siemens
Desiro	Desiro	k1gNnSc4	Desiro
vypůjčené	vypůjčený	k2eAgNnSc4d1	vypůjčené
od	od	k7c2	od
leasingové	leasingový	k2eAgFnSc2d1	leasingová
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Souprava	souprava	k1gFnSc1	souprava
ve	v	k7c6	v
žluté	žlutý	k2eAgFnSc6d1	žlutá
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
24	[number]	k4	24
<g/>
.	.	kIx.	.
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
jezdila	jezdit	k5eAaImAgFnS	jezdit
za	za	k7c4	za
symbolické	symbolický	k2eAgNnSc4d1	symbolické
jízdné	jízdné	k1gNnSc4	jízdné
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
132	[number]	k4	132
mezi	mezi	k7c7	mezi
Děčínem	Děčín	k1gInSc7	Děčín
a	a	k8xC	a
Krupkou	krupka	k1gFnSc7	krupka
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
vyjel	vyjet	k5eAaPmAgInS	vyjet
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
v	v	k7c4	v
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
z	z	k7c2	z
Děčína	Děčín	k1gInSc2	Děčín
hl.	hl.	k?	hl.
n.	n.	k?	n.
Ve	v	k7c6	v
všedních	všední	k2eAgInPc6d1	všední
dnech	den	k1gInPc6	den
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
souprava	souprava	k1gFnSc1	souprava
jezdila	jezdit	k5eAaImAgFnS	jezdit
mezi	mezi	k7c7	mezi
Ústím	ústí	k1gNnSc7	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Litvínovem	Litvínov	k1gInSc7	Litvínov
(	(	kIx(	(
<g/>
tratě	trať	k1gFnSc2	trať
130	[number]	k4	130
a	a	k8xC	a
134	[number]	k4	134
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
měla	mít	k5eAaImAgFnS	mít
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
tvrzení	tvrzení	k1gNnSc4	tvrzení
některých	některý	k3yIgMnPc2	některý
sociálnědemokratických	sociálnědemokratický	k2eAgMnPc2d1	sociálnědemokratický
hejtmanů	hejtman	k1gMnPc2	hejtman
<g/>
,	,	kIx,	,
že	že	k8xS	že
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
nikdy	nikdy	k6eAd1	nikdy
s	s	k7c7	s
žádnými	žádný	k3yNgInPc7	žádný
vlaky	vlak	k1gInPc7	vlak
nevyjede	vyjet	k5eNaPmIp3nS	vyjet
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohou	moct	k5eAaImIp3nP	moct
moderní	moderní	k2eAgNnPc4d1	moderní
vozidla	vozidlo	k1gNnPc4	vozidlo
přispět	přispět	k5eAaPmF	přispět
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
a	a	k8xC	a
zkvalitnění	zkvalitnění	k1gNnSc3	zkvalitnění
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
technickým	technický	k2eAgInPc3d1	technický
problémům	problém	k1gInPc3	problém
trati	trať	k1gFnSc2	trať
i	i	k8xC	i
velkému	velký	k2eAgInSc3d1	velký
zájmu	zájem	k1gInSc3	zájem
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
4000	[number]	k4	4000
za	za	k7c4	za
první	první	k4xOgInSc4	první
víkend	víkend	k1gInSc4	víkend
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
tyto	tento	k3xDgInPc1	tento
reklamní	reklamní	k2eAgInPc1d1	reklamní
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
zpoždění	zpoždění	k1gNnSc2	zpoždění
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
spoj	spoj	k1gInSc1	spoj
nabral	nabrat	k5eAaPmAgInS	nabrat
hodinové	hodinový	k2eAgNnSc4d1	hodinové
zpoždění	zpoždění	k1gNnSc4	zpoždění
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
spoje	spoj	k1gInPc4	spoj
kolem	kolem	k7c2	kolem
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
dopravce	dopravce	k1gMnPc4	dopravce
reagoval	reagovat	k5eAaBmAgInS	reagovat
zrušením	zrušení	k1gNnSc7	zrušení
symbolického	symbolický	k2eAgNnSc2d1	symbolické
jízdného	jízdné	k1gNnSc2	jízdné
a	a	k8xC	a
kompenzačním	kompenzační	k2eAgNnSc7d1	kompenzační
rozdáváním	rozdávání	k1gNnSc7	rozdávání
tatranek	tatranka	k1gFnPc2	tatranka
a	a	k8xC	a
džusů	džus	k1gInPc2	džus
a	a	k8xC	a
rozpojením	rozpojení	k1gNnSc7	rozpojení
soupravy	souprava	k1gFnSc2	souprava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
dne	den	k1gInSc2	den
jezdil	jezdit	k5eAaImAgMnS	jezdit
po	po	k7c6	po
trati	trať	k1gFnSc6	trať
každý	každý	k3xTgInSc4	každý
vůz	vůz	k1gInSc4	vůz
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akce	akce	k1gFnSc2	akce
"	"	kIx"	"
<g/>
Žluté	žlutý	k2eAgNnSc1d1	žluté
jaro	jaro	k1gNnSc1	jaro
na	na	k7c4	na
železnici	železnice	k1gFnSc4	železnice
<g/>
"	"	kIx"	"
by	by	kYmCp3nP	by
tato	tento	k3xDgFnSc1	tento
souprava	souprava	k1gFnSc1	souprava
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
českých	český	k2eAgInPc2d1	český
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
a	a	k8xC	a
koordinátor	koordinátor	k1gMnSc1	koordinátor
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
JIKORD	JIKORD	kA	JIKORD
připustily	připustit	k5eAaPmAgInP	připustit
možnost	možnost	k1gFnSc4	možnost
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
až	až	k9	až
na	na	k7c4	na
25	[number]	k4	25
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
desetiletá	desetiletý	k2eAgFnSc1d1	desetiletá
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
regionální	regionální	k2eAgFnSc2d1	regionální
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
závazku	závazek	k1gInSc6	závazek
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
západně	západně	k6eAd1	západně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
,	,	kIx,	,
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
194	[number]	k4	194
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
–	–	k?	–
Černý	Černý	k1gMnSc1	Černý
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
197	[number]	k4	197
Číčenice	Číčenice	k1gFnSc2	Číčenice
–	–	k?	–
Volary	Volara	k1gFnSc2	Volara
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
Údolí	údolí	k1gNnSc4	údolí
a	a	k8xC	a
198	[number]	k4	198
Strakonice	Strakonice	k1gFnPc4	Strakonice
<g/>
–	–	k?	–
<g/>
Volary	Volara	k1gFnSc2	Volara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
jednatelé	jednatel	k1gMnPc1	jednatel
JIKORDu	JIKORDa	k1gMnSc4	JIKORDa
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
zahájení	zahájení	k1gNnSc4	zahájení
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
oznámení	oznámení	k1gNnSc4	oznámení
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
společnost	společnost	k1gFnSc1	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
prezentační	prezentační	k2eAgFnSc2d1	prezentační
jízdy	jízda	k1gFnSc2	jízda
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
–	–	k?	–
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
zde	zde	k6eAd1	zde
také	také	k9	také
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
svůj	svůj	k3xOyFgInSc4	svůj
on-line	onin	k1gInSc5	on-lin
odbavovací	odbavovací	k2eAgInSc1d1	odbavovací
systém	systém	k1gInSc4	systém
umožňující	umožňující	k2eAgInSc4d1	umožňující
platit	platit	k5eAaImF	platit
platební	platební	k2eAgFnSc7d1	platební
kartou	karta	k1gFnSc7	karta
i	i	k8xC	i
číst	číst	k5eAaImF	číst
elektronické	elektronický	k2eAgInPc4d1	elektronický
průkazy	průkaz	k1gInPc4	průkaz
jiných	jiný	k2eAgMnPc2d1	jiný
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
schválilo	schválit	k5eAaPmAgNnS	schválit
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
záměr	záměr	k1gInSc1	záměr
na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
nabídkového	nabídkový	k2eAgNnSc2d1	nabídkové
řízení	řízení	k1gNnSc2	řízení
"	"	kIx"	"
<g/>
Provozního	provozní	k2eAgInSc2d1	provozní
souboru	soubor	k1gInSc2	soubor
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
"	"	kIx"	"
včetně	včetně	k7c2	včetně
časového	časový	k2eAgInSc2d1	časový
harmonogramu	harmonogram	k1gInSc2	harmonogram
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
předběžné	předběžný	k2eAgNnSc1d1	předběžné
oznámení	oznámení	k1gNnSc1	oznámení
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
soutěže	soutěž	k1gFnSc2	soutěž
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
RegioJet	RegioJet	k1gInSc1	RegioJet
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nepřihlásil	přihlásit	k5eNaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejdříve	dříve	k6eAd3	dříve
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2011	[number]	k4	2011
chce	chtít	k5eAaImIp3nS	chtít
provozovat	provozovat	k5eAaImF	provozovat
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
–	–	k?	–
<g/>
Domažlice	Domažlice	k1gFnPc1	Domažlice
(	(	kIx(	(
<g/>
současně	současně	k6eAd1	současně
ke	k	k7c3	k
stejnému	stejný	k2eAgNnSc3d1	stejné
datu	datum	k1gNnSc3	datum
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
i	i	k9	i
záměr	záměr	k1gInSc4	záměr
ohledně	ohledně	k7c2	ohledně
trasy	trasa	k1gFnSc2	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obě	dva	k4xCgNnPc4	dva
tyto	tento	k3xDgFnPc4	tento
tratě	trať	k1gFnPc4	trať
zažádala	zažádat	k5eAaPmAgFnS	zažádat
o	o	k7c4	o
kapacitu	kapacita	k1gFnSc4	kapacita
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
–	–	k?	–
<g/>
Domažlice	Domažlice	k1gFnPc1	Domažlice
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
2010	[number]	k4	2010
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
jezdila	jezdit	k5eAaImAgFnS	jezdit
pronajatými	pronajatý	k2eAgInPc7d1	pronajatý
reklamními	reklamní	k2eAgInPc7d1	reklamní
vlaky	vlak	k1gInPc7	vlak
Siemens	siemens	k1gInSc1	siemens
Desiro	Desiro	k1gNnSc1	Desiro
Classic	Classice	k1gFnPc2	Classice
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c4	o
provozování	provozování	k1gNnSc4	provozování
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
neobjevovaly	objevovat	k5eNaImAgFnP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejdříve	dříve	k6eAd3	dříve
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2011	[number]	k4	2011
chce	chtít	k5eAaImIp3nS	chtít
provozovat	provozovat	k5eAaImF	provozovat
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
ze	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
jí	jíst	k5eAaImIp3nS	jíst
SŽDC	SŽDC	kA	SŽDC
kapacitu	kapacita	k1gFnSc4	kapacita
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
přidělila	přidělit	k5eAaPmAgFnS	přidělit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
připravovala	připravovat	k5eAaImAgFnS	připravovat
spustit	spustit	k5eAaPmF	spustit
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
expresní	expresní	k2eAgFnSc4d1	expresní
dopravu	doprava	k1gFnSc4	doprava
vlaky	vlak	k1gInPc1	vlak
IC	IC	kA	IC
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
–	–	k?	–
<g/>
Čadca	Čadca	k1gFnSc1	Čadca
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
směřoval	směřovat	k5eAaImAgMnS	směřovat
dopravce	dopravce	k1gMnSc1	dopravce
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c6	na
spuštění	spuštění	k1gNnSc6	spuštění
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
léta	léto	k1gNnSc2	léto
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
skutečně	skutečně	k6eAd1	skutečně
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc4	provoz
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Havířovem	Havířov	k1gInSc7	Havířov
a	a	k8xC	a
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
spojů	spoj	k1gInPc2	spoj
prodloužen	prodloužen	k2eAgMnSc1d1	prodloužen
do	do	k7c2	do
Žiliny	Žilina	k1gFnSc2	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
plánovala	plánovat	k5eAaImAgFnS	plánovat
prodloužení	prodloužení	k1gNnSc4	prodloužení
trasy	trasa	k1gFnSc2	trasa
do	do	k7c2	do
Košic	Košice	k1gInPc2	Košice
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
ve	v	k7c6	v
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
řízení	řízení	k1gNnSc6	řízení
a	a	k8xC	a
uzavření	uzavření	k1gNnSc6	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
dotované	dotovaný	k2eAgFnSc2d1	dotovaná
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Bratislava	Bratislava	k1gFnSc1	Bratislava
–	–	k?	–
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
Streda	Stred	k1gMnSc2	Stred
–	–	k?	–
Komárno	Komárno	k1gNnSc1	Komárno
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2010	[number]	k4	2010
RegioJet	RegioJet	k1gMnSc1	RegioJet
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
i	i	k9	i
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
slovenské	slovenský	k2eAgFnPc4d1	slovenská
relace	relace	k1gFnPc4	relace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyhlášena	vyhlášen	k2eAgNnPc1d1	vyhlášeno
výběrová	výběrový	k2eAgNnPc1d1	výběrové
řízení	řízení	k1gNnPc1	řízení
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
trasu	trasa	k1gFnSc4	trasa
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
–	–	k?	–
<g/>
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Plánoval	plánovat	k5eAaImAgMnS	plánovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
slovenské	slovenský	k2eAgFnSc2d1	slovenská
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
záměr	záměr	k1gInSc4	záměr
provozovat	provozovat	k5eAaImF	provozovat
vlaky	vlak	k1gInPc4	vlak
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Bratislava-Žilina-Košice	Bratislava-Žilina-Košic	k1gMnSc2	Bratislava-Žilina-Košic
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
začal	začít	k5eAaPmAgInS	začít
RegioJet	RegioJet	k1gInSc1	RegioJet
provozovat	provozovat	k5eAaImF	provozovat
jeden	jeden	k4xCgInSc4	jeden
denní	denní	k2eAgInSc4d1	denní
pár	pár	k4xCyI	pár
spojů	spoj	k1gInPc2	spoj
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Košicemi	Košice	k1gInPc7	Košice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
spojích	spoj	k1gInPc6	spoj
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
restaurační	restaurační	k2eAgInSc1d1	restaurační
vůz	vůz	k1gInSc1	vůz
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
konzumace	konzumace	k1gFnSc2	konzumace
teplých	teplý	k2eAgNnPc2d1	teplé
jídel	jídlo	k1gNnPc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
médii	médium	k1gNnPc7	médium
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
chce	chtít	k5eAaImIp3nS	chtít
navrhnout	navrhnout	k5eAaPmF	navrhnout
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
převzetí	převzetí	k1gNnSc2	převzetí
současných	současný	k2eAgInPc2d1	současný
vlaků	vlak	k1gInPc2	vlak
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
zachováním	zachování	k1gNnSc7	zachování
stávajícího	stávající	k2eAgInSc2d1	stávající
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
dotací	dotace	k1gFnPc2	dotace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
Brněnský	brněnský	k2eAgInSc1d1	brněnský
deník	deník	k1gInSc1	deník
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
chce	chtít	k5eAaImIp3nS	chtít
ministra	ministr	k1gMnSc4	ministr
dopravy	doprava	k1gFnSc2	doprava
Zbyňka	Zbyňka	k1gFnSc1	Zbyňka
Stanjuru	Stanjura	k1gFnSc4	Stanjura
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
jezdit	jezdit	k5eAaImF	jezdit
bez	bez	k7c2	bez
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
dotací	dotace	k1gFnPc2	dotace
a	a	k8xC	a
bez	bez	k7c2	bez
konkurence	konkurence	k1gFnSc2	konkurence
dotované	dotovaný	k2eAgFnSc2d1	dotovaná
dopravy	doprava	k1gFnSc2	doprava
ČD	ČD	kA	ČD
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
že	že	k8xS	že
se	se	k3xPyFc4	se
nebrání	bránit	k5eNaImIp3nS	bránit
vstupu	vstup	k1gInSc3	vstup
konkurence	konkurence	k1gFnSc2	konkurence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
jezdila	jezdit	k5eAaImAgFnS	jezdit
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
komerční	komerční	k2eAgNnPc4d1	komerční
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgMnSc1d1	předchozí
ministr	ministr	k1gMnSc1	ministr
Pavel	Pavel	k1gMnSc1	Pavel
Dobeš	Dobeš	k1gMnSc1	Dobeš
na	na	k7c4	na
podobné	podobný	k2eAgInPc4d1	podobný
požadavky	požadavek	k1gInPc4	požadavek
a	a	k8xC	a
nabídky	nabídka	k1gFnSc2	nabídka
reagoval	reagovat	k5eAaBmAgInS	reagovat
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
schválený	schválený	k2eAgInSc4d1	schválený
harmonogram	harmonogram	k1gInSc4	harmonogram
liberalizace	liberalizace	k1gFnSc2	liberalizace
dálkové	dálkový	k2eAgFnSc2d1	dálková
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jančura	Jančura	k1gFnSc1	Jančura
avizoval	avizovat	k5eAaBmAgMnS	avizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
zajistit	zajistit	k5eAaPmF	zajistit
provoz	provoz	k1gInSc4	provoz
16	[number]	k4	16
párů	pár	k1gInPc2	pár
vlaků	vlak	k1gInPc2	vlak
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Brnem	Brno	k1gNnSc7	Brno
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
by	by	kYmCp3nS	by
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
médii	médium	k1gNnPc7	médium
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
vyjednává	vyjednávat	k5eAaImIp3nS	vyjednávat
s	s	k7c7	s
Deutsche	Deutsche	k1gNnSc7	Deutsche
Bahn	Bahna	k1gFnPc2	Bahna
o	o	k7c6	o
společném	společný	k2eAgNnSc6d1	společné
provozování	provozování	k1gNnSc6	provozování
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
spojů	spoj	k1gInPc2	spoj
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
či	či	k8xC	či
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
DB	db	kA	db
oznámily	oznámit	k5eAaPmAgInP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nedohodly	dohodnout	k5eNaPmAgInP	dohodnout
na	na	k7c4	na
budoucí	budoucí	k2eAgFnSc4d1	budoucí
spolupráci	spolupráce	k1gFnSc4	spolupráce
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
vlaků	vlak	k1gInPc2	vlak
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
–	–	k?	–
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
–	–	k?	–
<g/>
Hamburk	Hamburk	k1gInSc1	Hamburk
s	s	k7c7	s
žádným	žádný	k3yNgMnSc7	žádný
dopravcem	dopravce	k1gMnSc7	dopravce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
dále	daleko	k6eAd2	daleko
jednat	jednat	k5eAaImF	jednat
exkluzivně	exkluzivně	k6eAd1	exkluzivně
již	již	k9	již
jen	jen	k9	jen
se	se	k3xPyFc4	se
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Dozorčí	dozorčí	k2eAgFnSc1d1	dozorčí
rada	rada	k1gFnSc1	rada
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
informace	informace	k1gFnSc2	informace
schválila	schválit	k5eAaPmAgFnS	schválit
zrušení	zrušení	k1gNnSc4	zrušení
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
chystaný	chystaný	k2eAgInSc4d1	chystaný
nákup	nákup	k1gInSc4	nákup
souprav	souprava	k1gFnPc2	souprava
Railjet	Railjeta	k1gFnPc2	Railjeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
DB	db	kA	db
i	i	k8xC	i
RegioJet	RegioJet	k1gInSc4	RegioJet
údajně	údajně	k6eAd1	údajně
deklarovaly	deklarovat	k5eAaBmAgInP	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgMnSc1d1	nový
dopravce	dopravce	k1gMnSc1	dopravce
převezme	převzít	k5eAaPmIp3nS	převzít
provoz	provoz	k1gInSc4	provoz
vlaků	vlak	k1gInPc2	vlak
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
relaci	relace	k1gFnSc6	relace
již	již	k6eAd1	již
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
připravena	připravit	k5eAaPmNgFnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
DB	db	kA	db
však	však	k9	však
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
oznámily	oznámit	k5eAaPmAgInP	oznámit
odložení	odložení	k1gNnSc6	odložení
změny	změna	k1gFnPc4	změna
dopravce	dopravce	k1gMnSc2	dopravce
a	a	k8xC	a
požádaly	požádat	k5eAaPmAgFnP	požádat
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
o	o	k7c4	o
prodloužení	prodloužení	k1gNnSc4	prodloužení
spolupráce	spolupráce	k1gFnSc2	spolupráce
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
odložení	odložení	k1gNnSc2	odložení
termínu	termín	k1gInSc6	termín
žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
nezveřejnila	zveřejnit	k5eNaPmAgNnP	zveřejnit
<g/>
,	,	kIx,	,
iDnes	iDnes	k1gInSc1	iDnes
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
nejmenované	jmenovaný	k2eNgMnPc4d1	nejmenovaný
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
považují	považovat	k5eAaImIp3nP	považovat
nedostatek	nedostatek	k1gInSc4	nedostatek
vlaků	vlak	k1gInPc2	vlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
RegioJetu	RegioJet	k1gInSc2	RegioJet
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
mezitím	mezitím	k6eAd1	mezitím
deklarovaly	deklarovat	k5eAaBmAgFnP	deklarovat
zájem	zájem	k1gInSc4	zájem
provozovat	provozovat	k5eAaImF	provozovat
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
vlaky	vlak	k1gInPc1	vlak
i	i	k9	i
nadále	nadále	k6eAd1	nadále
a	a	k8xC	a
začaly	začít	k5eAaPmAgInP	začít
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
německým	německý	k2eAgMnSc7d1	německý
dopravcem	dopravce	k1gMnSc7	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zájem	zájem	k1gInSc1	zájem
tyto	tento	k3xDgInPc4	tento
vlaky	vlak	k1gInPc4	vlak
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
úseku	úsek	k1gInSc6	úsek
objednávat	objednávat	k5eAaImF	objednávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
dotovat	dotovat	k5eAaBmF	dotovat
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
odkládání	odkládání	k1gNnSc6	odkládání
ale	ale	k8xC	ale
DB	db	kA	db
začalo	začít	k5eAaPmAgNnS	začít
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
smlouvě	smlouva	k1gFnSc6	smlouva
o	o	k7c6	o
desetiletém	desetiletý	k2eAgInSc6d1	desetiletý
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
opět	opět	k6eAd1	opět
s	s	k7c7	s
ČD	ČD	kA	ČD
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
dopravce	dopravce	k1gMnPc4	dopravce
na	na	k7c6	na
rychlíkové	rychlíkový	k2eAgFnSc6d1	rychlíková
lince	linka	k1gFnSc6	linka
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
–	–	k?	–
<g/>
Krnov	Krnov	k1gInSc1	Krnov
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
vypsalo	vypsat	k5eAaPmAgNnS	vypsat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
rychlíkové	rychlíkový	k2eAgFnSc2d1	rychlíková
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
–	–	k?	–
<g/>
Opava	Opava	k1gFnSc1	Opava
<g/>
–	–	k?	–
<g/>
Krnov	Krnov	k1gInSc1	Krnov
<g/>
–	–	k?	–
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
s	s	k7c7	s
požadovaným	požadovaný	k2eAgNnSc7d1	požadované
zahájením	zahájení	k1gNnSc7	zahájení
provozu	provoz	k1gInSc2	provoz
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
do	do	k7c2	do
konce	konec	k1gInSc2	konec
platnosti	platnost	k1gFnSc2	platnost
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
2028	[number]	k4	2028
<g/>
/	/	kIx~	/
<g/>
2029	[number]	k4	2029
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
10	[number]	k4	10
zájemců	zájemce	k1gMnPc2	zájemce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
vyzvedli	vyzvednout	k5eAaPmAgMnP	vyzvednout
zadávací	zadávací	k2eAgFnSc4d1	zadávací
dokumentaci	dokumentace	k1gFnSc4	dokumentace
<g/>
,	,	kIx,	,
nabídku	nabídka	k1gFnSc4	nabídka
nakonec	nakonec	k6eAd1	nakonec
podali	podat	k5eAaPmAgMnP	podat
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
jen	jen	k9	jen
dva	dva	k4xCgMnPc1	dva
zájemci	zájemce	k1gMnPc1	zájemce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konsorcium	konsorcium	k1gNnSc1	konsorcium
dvou	dva	k4xCgFnPc2	dva
společností	společnost	k1gFnPc2	společnost
skupiny	skupina	k1gFnSc2	skupina
Arriva	Arriva	k1gFnSc1	Arriva
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
.	.	kIx.	.
</s>
<s>
Arriva	Arriva	k1gFnSc1	Arriva
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
požadovala	požadovat	k5eAaImAgFnS	požadovat
kompenzaci	kompenzace	k1gFnSc4	kompenzace
(	(	kIx(	(
<g/>
dotaci	dotace	k1gFnSc4	dotace
<g/>
)	)	kIx)	)
189	[number]	k4	189
Kč	Kč	kA	Kč
na	na	k7c4	na
vlakokilometr	vlakokilometr	k1gInSc4	vlakokilometr
<g/>
,	,	kIx,	,
RegioJet	RegioJet	k1gInSc4	RegioJet
188	[number]	k4	188
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
dopravu	doprava	k1gFnSc4	doprava
novými	nový	k2eAgInPc7d1	nový
vlaky	vlak	k1gInPc7	vlak
<g/>
,	,	kIx,	,
Arriva	Arriva	k1gFnSc1	Arriva
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
tři	tři	k4xCgFnPc4	tři
varianty	varianta	k1gFnPc4	varianta
<g/>
,	,	kIx,	,
počítající	počítající	k2eAgInSc4d1	počítající
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
nových	nový	k2eAgFnPc2d1	nová
nebo	nebo	k8xC	nebo
nově	nově	k6eAd1	nově
rekonstruovaných	rekonstruovaný	k2eAgInPc2d1	rekonstruovaný
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
údajně	údajně	k6eAd1	údajně
jako	jako	k8xS	jako
jediné	jediný	k2eAgFnPc1d1	jediná
musely	muset	k5eAaImAgFnP	muset
při	při	k7c6	při
zadávání	zadávání	k1gNnSc6	zadávání
objednávky	objednávka	k1gFnSc2	objednávka
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
soupravy	souprava	k1gFnPc4	souprava
postupovat	postupovat	k5eAaImF	postupovat
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zadávání	zadávání	k1gNnSc6	zadávání
veřejných	veřejný	k2eAgFnPc2d1	veřejná
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
PESA	peso	k1gNnPc4	peso
Bydgoszcz	Bydgoszcz	k1gInSc1	Bydgoszcz
pak	pak	k6eAd1	pak
s	s	k7c7	s
toutéž	týž	k3xTgFnSc7	týž
nabídkou	nabídka	k1gFnSc7	nabídka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ČD	ČD	kA	ČD
musely	muset	k5eAaImAgInP	muset
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
,	,	kIx,	,
oslovila	oslovit	k5eAaPmAgFnS	oslovit
RegioJet	RegioJet	k1gInSc4	RegioJet
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
podala	podat	k5eAaPmAgFnS	podat
Arriva	Arriva	k1gFnSc1	Arriva
společně	společně	k6eAd1	společně
s	s	k7c7	s
TRANSCENTRUM	TRANSCENTRUM	kA	TRANSCENTRUM
bus	bus	k1gInSc1	bus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nedodala	dodat	k5eNaPmAgNnP	dodat
potřebná	potřebný	k2eAgNnPc1d1	potřebné
osvědčení	osvědčení	k1gNnPc1	osvědčení
o	o	k7c4	o
vzdělání	vzdělání	k1gNnSc4	vzdělání
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgFnPc1d1	soutěžní
podmínky	podmínka	k1gFnPc1	podmínka
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
dvě	dva	k4xCgFnPc1	dva
odborně	odborně	k6eAd1	odborně
způsobilé	způsobilý	k2eAgFnPc4d1	způsobilá
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
drážní	drážní	k2eAgInSc1d1	drážní
zákon	zákon	k1gInSc1	zákon
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Arriva	Arriva	k1gFnSc1	Arriva
neuspěla	uspět	k5eNaPmAgFnS	uspět
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nS	by
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
by	by	kYmCp3nS	by
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
soutěž	soutěž	k1gFnSc4	soutěž
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Arriva	Arriva	k1gFnSc1	Arriva
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
odvolala	odvolat	k5eAaPmAgFnS	odvolat
a	a	k8xC	a
po	po	k7c6	po
zamítnutí	zamítnutí	k1gNnSc6	zamítnutí
odvolání	odvolání	k1gNnSc2	odvolání
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
soutěž	soutěž	k1gFnSc4	soutěž
nerušilo	rušit	k5eNaImAgNnS	rušit
a	a	k8xC	a
zakázku	zakázka	k1gFnSc4	zakázka
přidělilo	přidělit	k5eAaPmAgNnS	přidělit
zbylému	zbylý	k2eAgMnSc3d1	zbylý
zájemci	zájemce	k1gMnSc3	zájemce
<g/>
,	,	kIx,	,
RegioJetu	RegioJet	k1gMnSc3	RegioJet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakázku	zakázka	k1gFnSc4	zakázka
bez	bez	k7c2	bez
další	další	k2eAgFnSc2d1	další
soutěže	soutěž	k1gFnSc2	soutěž
zadá	zadat	k5eAaPmIp3nS	zadat
společnosti	společnost	k1gFnSc3	společnost
RegioJet	RegioJet	k1gInSc4	RegioJet
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
RegioJet	RegioJet	k1gMnSc1	RegioJet
bude	být	k5eAaImBp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
jezdit	jezdit	k5eAaImF	jezdit
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
soutěžní	soutěžní	k2eAgFnSc6d1	soutěžní
nabídce	nabídka	k1gFnSc6	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
zájem	zájem	k1gInSc4	zájem
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Stanjura	Stanjur	k1gMnSc2	Stanjur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
s	s	k7c7	s
RegioJetem	RegioJet	k1gMnSc7	RegioJet
dohodlo	dohodnout	k5eAaPmAgNnS	dohodnout
na	na	k7c6	na
základních	základní	k2eAgInPc6d1	základní
předpokladech	předpoklad	k1gInPc6	předpoklad
uzavření	uzavření	k1gNnSc2	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
odcházející	odcházející	k2eAgMnSc1d1	odcházející
ministr	ministr	k1gMnSc1	ministr
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Stanjura	Stanjura	k1gFnSc1	Stanjura
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
smlouvu	smlouva	k1gFnSc4	smlouva
nepodepsat	podepsat	k5eNaPmF	podepsat
a	a	k8xC	a
následující	následující	k2eAgMnSc1d1	následující
ministr	ministr	k1gMnSc1	ministr
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Žák	Žák	k1gMnSc1	Žák
s	s	k7c7	s
podpisem	podpis	k1gInSc7	podpis
váhá	váhat	k5eAaImIp3nS	váhat
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
nejprve	nejprve	k6eAd1	nejprve
znovu	znovu	k6eAd1	znovu
vše	všechen	k3xTgNnSc4	všechen
prověřit	prověřit	k5eAaPmF	prověřit
a	a	k8xC	a
probrat	probrat	k5eAaPmF	probrat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
provozovala	provozovat	k5eAaImAgFnS	provozovat
společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
podnikatelské	podnikatelský	k2eAgNnSc4d1	podnikatelské
riziko	riziko	k1gNnSc4	riziko
provozovatele	provozovatel	k1gMnSc2	provozovatel
s	s	k7c7	s
financováním	financování	k1gNnSc7	financování
ING	ing	kA	ing
Lease	Lease	k1gFnSc2	Lease
a	a	k8xC	a
Société	Sociétý	k2eAgFnSc2d1	Sociétý
Générale	Général	k1gMnSc5	Général
Equipment	Equipment	k1gInSc1	Equipment
Finance	finance	k1gFnPc1	finance
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
objednávky	objednávka	k1gFnSc2	objednávka
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
kompenzacemi	kompenzace	k1gFnPc7	kompenzace
žákovských	žákovský	k2eAgFnPc2d1	žákovská
<g/>
,	,	kIx,	,
studentských	studentský	k2eAgFnPc2d1	studentská
a	a	k8xC	a
důchodcovských	důchodcovský	k2eAgFnPc2d1	důchodcovská
slev	sleva	k1gFnPc2	sleva
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
začala	začít	k5eAaPmAgFnS	začít
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
páry	pár	k1gInPc7	pár
spojů	spoj	k1gInPc2	spoj
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Havířovem	Havířov	k1gInSc7	Havířov
<g/>
,	,	kIx,	,
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přidáním	přidání	k1gNnSc7	přidání
třetí	třetí	k4xOgFnSc2	třetí
soupravy	souprava	k1gFnSc2	souprava
přibyl	přibýt	k5eAaPmAgInS	přibýt
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
pár	pár	k1gInSc1	pár
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Výhledově	výhledově	k6eAd1	výhledově
mělo	mít	k5eAaImAgNnS	mít
trasu	trasa	k1gFnSc4	trasa
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
6	[number]	k4	6
souprav	souprava	k1gFnPc2	souprava
9	[number]	k4	9
páry	pára	k1gFnSc2	pára
spojů	spoj	k1gInPc2	spoj
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
jezdí	jezdit	k5eAaImIp3nP	jezdit
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
dvouhodinovém	dvouhodinový	k2eAgInSc6d1	dvouhodinový
intervalu	interval	k1gInSc6	interval
a	a	k8xC	a
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
stanic	stanice	k1gFnPc2	stanice
Ostravska	Ostravsko	k1gNnSc2	Ostravsko
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
Třinec	Třinec	k1gInSc1	Třinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
spojů	spoj	k1gInPc2	spoj
začal	začít	k5eAaPmAgInS	začít
zajíždět	zajíždět	k5eAaImF	zajíždět
až	až	k9	až
do	do	k7c2	do
Žiliny	Žilina	k1gFnSc2	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
se	se	k3xPyFc4	se
odjezdy	odjezd	k1gInPc1	odjezd
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
posunuly	posunout	k5eAaPmAgFnP	posunout
o	o	k7c4	o
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
vlaky	vlak	k1gInPc1	vlak
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
12	[number]	k4	12
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
linky	linka	k1gFnSc2	linka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
–	–	k?	–
Praha-Libeň	Praha-Libeň	k1gFnSc1	Praha-Libeň
–	–	k?	–
Pardubice	Pardubice	k1gInPc1	Pardubice
hlavní	hlavní	k2eAgInPc1d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
–	–	k?	–
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
–	–	k?	–
Zábřeh	Zábřeh	k1gInSc4	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
–	–	k?	–
Hranice	hranice	k1gFnSc1	hranice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
–	–	k?	–
Ostrava-Svinov	Ostrava-Svinov	k1gInSc1	Ostrava-Svinov
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
–	–	k?	–
Ostrava-Stodolní	Ostrava-Stodolní	k2eAgInSc1d1	Ostrava-Stodolní
–	–	k?	–
Havířov	Havířov	k1gInSc1	Havířov
–	–	k?	–
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
–	–	k?	–
Třinec	Třinec	k1gInSc1	Třinec
–	–	k?	–
Čadca	Čadca	k1gFnSc1	Čadca
–	–	k?	–
Kysucké	Kysucký	k2eAgNnSc1d1	Kysucký
Nové	Nové	k2eAgNnSc1d1	Nové
Mesto	Mesto	k1gNnSc1	Mesto
–	–	k?	–
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
spoje	spoj	k1gInPc4	spoj
kromě	kromě	k7c2	kromě
IC	IC	kA	IC
1006	[number]	k4	1006
a	a	k8xC	a
IC	IC	kA	IC
1013	[number]	k4	1013
končily	končit	k5eAaImAgInP	končit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
či	či	k8xC	či
začínají	začínat	k5eAaImIp3nP	začínat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
Havířově	Havířov	k1gInSc6	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
spojů	spoj	k1gInPc2	spoj
mezi	mezi	k7c7	mezi
Havířovem	Havířov	k1gInSc7	Havířov
a	a	k8xC	a
Žilinou	Žilina	k1gFnSc7	Žilina
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
až	až	k9	až
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
zastavování	zastavování	k1gNnSc1	zastavování
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
Praha-Libeň	Praha-Libeň	k1gFnSc1	Praha-Libeň
a	a	k8xC	a
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zábřehu	Zábřeh	k1gInSc6	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
jen	jen	k9	jen
první	první	k4xOgInSc4	první
ranní	ranní	k2eAgInSc4d1	ranní
spoj	spoj	k1gInSc4	spoj
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
IC	IC	kA	IC
1000	[number]	k4	1000
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
večerní	večerní	k2eAgInSc4d1	večerní
spoj	spoj	k1gInSc4	spoj
na	na	k7c4	na
Třinec	Třinec	k1gInSc4	Třinec
IC	IC	kA	IC
1017	[number]	k4	1017
<g/>
.	.	kIx.	.
</s>
<s>
Vlak	vlak	k1gInSc1	vlak
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
řady	řada	k1gFnSc2	řada
162	[number]	k4	162
a	a	k8xC	a
4-5	[number]	k4	4-5
rekonstruovanými	rekonstruovaný	k2eAgInPc7d1	rekonstruovaný
vozy	vůz	k1gInPc7	vůz
rakouských	rakouský	k2eAgFnPc2d1	rakouská
drah	draha	k1gFnPc2	draha
řady	řada	k1gFnSc2	řada
Ampz	Ampz	k1gInSc1	Ampz
a	a	k8xC	a
ABmz	ABmz	k1gInSc1	ABmz
s	s	k7c7	s
48	[number]	k4	48
místy	místo	k1gNnPc7	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
řazení	řazení	k1gNnSc1	řazení
vozů	vůz	k1gInPc2	vůz
pětivozového	pětivozový	k2eAgInSc2d1	pětivozový
vlaku	vlak	k1gInSc2	vlak
je	být	k5eAaImIp3nS	být
symetrické	symetrický	k2eAgNnSc1d1	symetrické
<g/>
,	,	kIx,	,
Ampz	Ampz	k1gMnSc1	Ampz
+	+	kIx~	+
ABmz	ABmz	k1gMnSc1	ABmz
+	+	kIx~	+
Ampz	Ampz	k1gMnSc1	Ampz
+	+	kIx~	+
ABmz	ABmz	k1gMnSc1	ABmz
+	+	kIx~	+
Ampz	Ampz	k1gMnSc1	Ampz
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
vlaku	vlak	k1gInSc2	vlak
je	být	k5eAaImIp3nS	být
240	[number]	k4	240
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
až	až	k9	až
na	na	k7c4	na
400	[number]	k4	400
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
mají	mít	k5eAaImIp3nP	mít
přibýt	přibýt	k5eAaPmF	přibýt
i	i	k9	i
vozy	vůz	k1gInPc4	vůz
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
jezdily	jezdit	k5eAaImAgInP	jezdit
vlaky	vlak	k1gInPc1	vlak
vždy	vždy	k6eAd1	vždy
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
všechny	všechen	k3xTgFnPc1	všechen
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
využity	využít	k5eAaPmNgFnP	využít
<g/>
,	,	kIx,	,
jezdily	jezdit	k5eAaImAgInP	jezdit
vlaky	vlak	k1gInPc1	vlak
už	už	k9	už
jen	jen	k9	jen
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
záložní	záložní	k2eAgFnSc1d1	záložní
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
se	s	k7c7	s
strojvůdcem	strojvůdce	k1gMnSc7	strojvůdce
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
trasy	trasa	k1gFnSc2	trasa
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Třebové	Třebová	k1gFnSc6	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
chtěl	chtít	k5eAaImAgMnS	chtít
s	s	k7c7	s
Českými	český	k2eAgFnPc7d1	Česká
dráhami	dráha	k1gFnPc7	dráha
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
za	za	k7c4	za
určitý	určitý	k2eAgInSc4d1	určitý
poplatek	poplatek	k1gInSc4	poplatek
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poruchy	porucha	k1gFnSc2	porucha
poskytly	poskytnout	k5eAaPmAgFnP	poskytnout
náhradní	náhradní	k2eAgFnSc4d1	náhradní
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
rovněž	rovněž	k9	rovněž
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
RegioJetu	RegioJet	k2eAgFnSc4d1	RegioJet
svou	svůj	k3xOyFgFnSc4	svůj
myčku	myčka	k1gFnSc4	myčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
je	být	k5eAaImIp3nS	být
poskytováno	poskytovat	k5eAaImNgNnS	poskytovat
cestujícím	cestující	k1gMnPc3	cestující
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
občerstvení	občerstvení	k1gNnSc1	občerstvení
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
či	či	k8xC	či
voda	voda	k1gFnSc1	voda
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
za	za	k7c4	za
nízkonákladové	nízkonákladový	k2eAgFnPc4d1	nízkonákladová
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
voze	vůz	k1gInSc6	vůz
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jedna	jeden	k4xCgFnSc1	jeden
stevardka	stevardka	k1gFnSc1	stevardka
či	či	k8xC	či
stevard	stevard	k1gMnSc1	stevard
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
jsou	být	k5eAaImIp3nP	být
povinně	povinně	k6eAd1	povinně
místenkové	místenkový	k2eAgFnPc1d1	místenková
<g/>
,	,	kIx,	,
přeprava	přeprava	k1gFnSc1	přeprava
stojících	stojící	k2eAgMnPc2d1	stojící
cestujících	cestující	k1gMnPc2	cestující
je	být	k5eAaImIp3nS	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
po	po	k7c6	po
přidání	přidání	k1gNnSc6	přidání
třetí	třetí	k4xOgFnSc2	třetí
soupravy	souprava	k1gFnSc2	souprava
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
vyčíslil	vyčíslit	k5eAaPmAgMnS	vyčíslit
počet	počet	k1gInSc4	počet
svých	svůj	k3xOyFgMnPc2	svůj
stevardů	stevard	k1gMnPc2	stevard
a	a	k8xC	a
stevardek	stevardka	k1gFnPc2	stevardka
na	na	k7c4	na
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
období	období	k1gNnSc4	období
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
uvádí	uvádět	k5eAaImIp3nS	uvádět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
přepravených	přepravený	k2eAgMnPc2d1	přepravený
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
značný	značný	k2eAgInSc1d1	značný
mediální	mediální	k2eAgInSc1d1	mediální
ohlas	ohlas	k1gInSc1	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
i	i	k9	i
selháním	selhání	k1gNnSc7	selhání
dopravce	dopravce	k1gMnSc2	dopravce
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
dní	den	k1gInPc2	den
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
provozu	provoz	k1gInSc2	provoz
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
<g/>
,	,	kIx,	,
projela	projet	k5eAaPmAgFnS	projet
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
přesunu	přesun	k1gInSc6	přesun
v	v	k7c6	v
Lipníku	Lipník	k1gInSc6	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
návěst	návěst	k1gFnSc1	návěst
Stůj	stát	k5eAaImRp2nS	stát
a	a	k8xC	a
poškodila	poškodit	k5eAaPmAgFnS	poškodit
výhybku	výhybka	k1gFnSc4	výhybka
<g/>
.	.	kIx.	.
</s>
<s>
Strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
vyšetření	vyšetření	k1gNnSc2	vyšetření
případu	případ	k1gInSc2	případ
stažen	stáhnout	k5eAaPmNgInS	stáhnout
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
škodu	škoda	k1gFnSc4	škoda
mluvčí	mluvčí	k1gFnSc2	mluvčí
Drážní	drážní	k2eAgFnSc2d1	drážní
inspekce	inspekce	k1gFnSc2	inspekce
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
na	na	k7c4	na
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
provozu	provoz	k1gInSc2	provoz
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Záboří	Záboří	k1gNnSc6	Záboří
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
kolem	kolem	k7c2	kolem
20	[number]	k4	20
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
k	k	k7c3	k
poruše	porucha	k1gFnSc3	porucha
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
vlaku	vlak	k1gInSc2	vlak
IC	IC	kA	IC
1016	[number]	k4	1016
s	s	k7c7	s
následkem	následek	k1gInSc7	následek
20	[number]	k4	20
<g/>
minutového	minutový	k2eAgNnSc2d1	minutové
zpoždění	zpoždění	k1gNnSc2	zpoždění
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uváděly	uvádět	k5eAaImAgInP	uvádět
více	hodně	k6eAd2	hodně
32	[number]	k4	32
<g/>
minutové	minutový	k2eAgNnSc4d1	minutové
zpoždění	zpoždění	k1gNnPc4	zpoždění
<g/>
,	,	kIx,	,
na	na	k7c6	na
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
27	[number]	k4	27
<g/>
minutové	minutový	k2eAgInPc1d1	minutový
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
RegioJetu	RegioJet	k1gInSc2	RegioJet
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
visela	viset	k5eAaImAgFnS	viset
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlak	vlak	k1gInSc1	vlak
má	mít	k5eAaImIp3nS	mít
0	[number]	k4	0
minut	minuta	k1gFnPc2	minuta
zpoždění	zpoždění	k1gNnSc2	zpoždění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
IC	IC	kA	IC
1010	[number]	k4	1010
přejel	přejet	k5eAaPmAgMnS	přejet
stanici	stanice	k1gFnSc4	stanice
Hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
zastaven	zastavit	k5eAaPmNgMnS	zastavit
záchrannou	záchranný	k2eAgFnSc7d1	záchranná
brzdou	brzda	k1gFnSc7	brzda
a	a	k8xC	a
k	k	k7c3	k
nástupišti	nástupiště	k1gNnSc3	nástupiště
nacouvat	nacouvat	k5eAaPmF	nacouvat
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
Petr	Petr	k1gMnSc1	Petr
Žaluda	Žalud	k1gMnSc2	Žalud
posměšně	posměšně	k6eAd1	posměšně
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtyřvozové	čtyřvozový	k2eAgInPc1d1	čtyřvozový
vlaky	vlak	k1gInPc1	vlak
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
jezdí	jezdit	k5eAaImIp3nP	jezdit
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jedna	jeden	k4xCgFnSc1	jeden
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kritice	kritika	k1gFnSc3	kritika
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
i	i	k9	i
exministr	exministr	k1gMnSc1	exministr
dopravy	doprava	k1gFnSc2	doprava
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
kvalita	kvalita	k1gFnSc1	kvalita
vlaků	vlak	k1gInPc2	vlak
zklamala	zklamat	k5eAaPmAgFnS	zklamat
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akce	akce	k1gFnSc2	akce
ČD	ČD	kA	ČD
Promo	Proma	k1gFnSc5	Proma
zlevnily	zlevnit	k5eAaPmAgFnP	zlevnit
některé	některý	k3yIgFnPc1	některý
relace	relace	k1gFnPc1	relace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
jízdné	jízdné	k1gNnSc1	jízdné
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
nová	nový	k2eAgFnSc1d1	nová
cena	cena	k1gFnSc1	cena
jízdného	jízdné	k1gNnSc2	jízdné
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
Regiojetem	Regioje	k1gNnSc7	Regioje
<g/>
,	,	kIx,	,
obvinil	obvinit	k5eAaPmAgMnS	obvinit
mluvčí	mluvčí	k1gMnSc1	mluvčí
Regiojetu	Regiojet	k1gInSc2	Regiojet
Aleš	Aleš	k1gMnSc1	Aleš
Ondrůj	Ondrůj	k1gMnSc1	Ondrůj
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
vedoucí	vedoucí	k1gMnSc1	vedoucí
odboru	odbor	k1gInSc2	odbor
komunikace	komunikace	k1gFnSc2	komunikace
ČD	ČD	kA	ČD
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
z	z	k7c2	z
nekalé	kalý	k2eNgFnSc2d1	nekalá
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
zneužití	zneužití	k1gNnSc2	zneužití
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
RegioJet	RegioJet	k1gInSc1	RegioJet
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
ztrátu	ztráta	k1gFnSc4	ztráta
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
"	"	kIx"	"
<g/>
predátorskými	predátorský	k2eAgFnPc7d1	predátorská
cenami	cena	k1gFnPc7	cena
<g/>
"	"	kIx"	"
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Ostravou	Ostrava	k1gFnSc7	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Obsazenost	obsazenost	k1gFnSc1	obsazenost
vlaků	vlak	k1gInPc2	vlak
vyčíslil	vyčíslit	k5eAaPmAgInS	vyčíslit
na	na	k7c4	na
asi	asi	k9	asi
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
vyčíslil	vyčíslit	k5eAaPmAgMnS	vyčíslit
ztrátu	ztráta	k1gFnSc4	ztráta
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
na	na	k7c4	na
asi	asi	k9	asi
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztráta	ztráta	k1gFnSc1	ztráta
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dotována	dotovat	k5eAaBmNgFnS	dotovat
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
společnosti	společnost	k1gFnSc2	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
na	na	k7c4	na
asi	asi	k9	asi
130	[number]	k4	130
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
ztráta	ztráta	k1gFnSc1	ztráta
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
vyčíslena	vyčíslit	k5eAaPmNgFnS	vyčíslit
na	na	k7c4	na
76	[number]	k4	76
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
při	při	k7c6	při
tržbách	tržba	k1gFnPc6	tržba
267	[number]	k4	267
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
očekával	očekávat	k5eAaImAgMnS	očekávat
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
obdobný	obdobný	k2eAgInSc4d1	obdobný
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
negativního	negativní	k2eAgInSc2d1	negativní
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
výsledku	výsledek	k1gInSc2	výsledek
viní	vinit	k5eAaImIp3nS	vinit
cenovou	cenový	k2eAgFnSc4d1	cenová
politiku	politika	k1gFnSc4	politika
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
u	u	k7c2	u
antimonopolního	antimonopolní	k2eAgInSc2d1	antimonopolní
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
napadnout	napadnout	k5eAaPmF	napadnout
i	i	k9	i
soudně	soudně	k6eAd1	soudně
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc4	ztráta
chce	chtít	k5eAaImIp3nS	chtít
hradit	hradit	k5eAaImF	hradit
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
Student	student	k1gMnSc1	student
Agency	Agenca	k1gMnSc2	Agenca
vykázala	vykázat	k5eAaPmAgFnS	vykázat
zisk	zisk	k1gInSc4	zisk
110	[number]	k4	110
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
při	při	k7c6	při
tržbách	tržba	k1gFnPc6	tržba
přes	přes	k7c4	přes
1,6	[number]	k4	1,6
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
prostředků	prostředek	k1gInPc2	prostředek
dostala	dostat	k5eAaPmAgFnS	dostat
firma	firma	k1gFnSc1	firma
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
za	za	k7c4	za
studentské	studentský	k2eAgFnPc4d1	studentská
slevy	sleva	k1gFnPc4	sleva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
kompenzaci	kompenzace	k1gFnSc4	kompenzace
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
prostředků	prostředek	k1gInPc2	prostředek
33,3	[number]	k4	33,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
nejprve	nejprve	k6eAd1	nejprve
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
zruší	zrušit	k5eAaPmIp3nS	zrušit
přepravu	přeprava	k1gFnSc4	přeprava
vozíčkářů	vozíčkář	k1gMnPc2	vozíčkář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kvůli	kvůli	k7c3	kvůli
administrativním	administrativní	k2eAgInPc3d1	administrativní
a	a	k8xC	a
technickým	technický	k2eAgInPc3d1	technický
problémům	problém	k1gInPc3	problém
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zpožďování	zpožďování	k1gNnSc3	zpožďování
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
si	se	k3xPyFc3	se
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
použití	použití	k1gNnSc4	použití
zvedací	zvedací	k2eAgFnSc2d1	zvedací
plošiny	plošina	k1gFnSc2	plošina
účtovaly	účtovat	k5eAaImAgInP	účtovat
3600	[number]	k4	3600
až	až	k6eAd1	až
7200	[number]	k4	7200
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
objednání	objednání	k1gNnSc6	objednání
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nejméně	málo	k6eAd3	málo
den	den	k1gInSc4	den
a	a	k8xC	a
půl	půl	k1xP	půl
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
však	však	k9	však
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
nápravě	náprava	k1gFnSc6	náprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
handicapovaným	handicapovaný	k2eAgInSc7d1	handicapovaný
využít	využít	k5eAaPmF	využít
služeb	služba	k1gFnPc2	služba
společnosti	společnost	k1gFnSc2	společnost
Leo	Leo	k1gMnSc1	Leo
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
Václav	Václav	k1gMnSc1	Václav
Krása	krása	k1gFnSc1	krása
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
diskriminační	diskriminační	k2eAgInPc4d1	diskriminační
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
záměrně	záměrně	k6eAd1	záměrně
chce	chtít	k5eAaImIp3nS	chtít
situaci	situace	k1gFnSc4	situace
vyhrotit	vyhrotit	k5eAaPmF	vyhrotit
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k5eAaPmF	RegioJet
poté	poté	k6eAd1	poté
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
neodmítá	odmítat	k5eNaImIp3nS	odmítat
přepravu	přeprava	k1gFnSc4	přeprava
vozíčkářů	vozíčkář	k1gMnPc2	vozíčkář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
objednávku	objednávka	k1gFnSc4	objednávka
plošin	plošina	k1gFnPc2	plošina
pro	pro	k7c4	pro
nakládání	nakládání	k1gNnSc4	nakládání
vozíčkářů	vozíčkář	k1gMnPc2	vozíčkář
do	do	k7c2	do
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
cestující	cestující	k2eAgFnSc1d1	cestující
s	s	k7c7	s
postižením	postižení	k1gNnSc7	postižení
nakládat	nakládat	k5eAaImF	nakládat
palubní	palubní	k2eAgInSc4d1	palubní
personál	personál	k1gInSc4	personál
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dělal	dělat	k5eAaImAgMnS	dělat
i	i	k9	i
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Jančura	Jančur	k1gMnSc2	Jančur
si	se	k3xPyFc3	se
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Krásou	krása	k1gFnSc7	krása
situaci	situace	k1gFnSc4	situace
vyříkali	vyříkat	k5eAaPmAgMnP	vyříkat
a	a	k8xC	a
pak	pak	k6eAd1	pak
Krása	krása	k1gFnSc1	krása
začal	začít	k5eAaPmAgMnS	začít
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
takto	takto	k6eAd1	takto
nezneužívají	zneužívat	k5eNaImIp3nP	zneužívat
plošiny	plošina	k1gFnPc4	plošina
pořízené	pořízený	k2eAgFnPc4d1	pořízená
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
zástupci	zástupce	k1gMnPc1	zástupce
handicapovaných	handicapovaný	k2eAgNnPc2d1	handicapované
rozloučení	rozloučení	k1gNnPc2	rozloučení
s	s	k7c7	s
přepravou	přeprava	k1gFnSc7	přeprava
vozíčkářů	vozíčkář	k1gMnPc2	vozíčkář
a	a	k8xC	a
u	u	k7c2	u
té	ten	k3xDgFnSc2	ten
příležitosti	příležitost	k1gFnSc2	příležitost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
provedené	provedený	k2eAgFnSc2d1	provedená
zkoušky	zkouška	k1gFnSc2	zkouška
ruční	ruční	k2eAgNnSc4d1	ruční
nakládání	nakládání	k1gNnSc4	nakládání
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
jako	jako	k8xC	jako
nedůstojné	důstojný	k2eNgNnSc4d1	nedůstojné
<g/>
:	:	kIx,	:
dvojici	dvojice	k1gFnSc4	dvojice
stevardů	stevard	k1gMnPc2	stevard
museli	muset	k5eAaImAgMnP	muset
vypomoci	vypomoct	k5eAaPmF	vypomoct
přítomní	přítomný	k2eAgMnPc1d1	přítomný
novináři	novinář	k1gMnPc1	novinář
a	a	k8xC	a
vozík	vozík	k1gInSc4	vozík
a	a	k8xC	a
cestujícího	cestující	k1gMnSc2	cestující
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
přenášet	přenášet	k5eAaImF	přenášet
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Čipera	Čipera	k1gMnSc1	Čipera
ze	z	k7c2	z
sdružení	sdružení	k1gNnSc2	sdružení
rovněž	rovněž	k9	rovněž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
přístup	přístup	k1gInSc4	přístup
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
plošina	plošina	k1gFnSc1	plošina
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
vybavením	vybavení	k1gNnSc7	vybavení
každého	každý	k3xTgNnSc2	každý
nástupiště	nástupiště	k1gNnSc2	nástupiště
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
by	by	kYmCp3nP	by
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
chtít	chtít	k5eAaImF	chtít
ani	ani	k8xC	ani
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
nějaký	nějaký	k3yIgInSc4	nějaký
manipulační	manipulační	k2eAgInSc4d1	manipulační
poplatek	poplatek	k1gInSc4	poplatek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
RegioJet	RegioJet	k1gMnSc1	RegioJet
získal	získat	k5eAaPmAgMnS	získat
devítiletou	devítiletý	k2eAgFnSc4d1	devítiletá
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
slovenským	slovenský	k2eAgNnSc7d1	slovenské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
dotovanou	dotovaný	k2eAgFnSc4d1	dotovaná
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
z	z	k7c2	z
Bratislavy	Bratislava	k1gFnSc2	Bratislava
do	do	k7c2	do
Komárna	Komárno	k1gNnSc2	Komárno
přes	přes	k7c4	přes
Dunajskou	dunajský	k2eAgFnSc4d1	Dunajská
Stredu	Stred	k1gMnSc3	Stred
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
smlouvy	smlouva	k1gFnSc2	smlouva
činí	činit	k5eAaImIp3nS	činit
cca	cca	kA	cca
1,25	[number]	k4	1,25
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
vlkm	vlkm	k6eAd1	vlkm
ročně	ročně	k6eAd1	ročně
při	při	k7c6	při
nákladech	náklad	k1gInPc6	náklad
(	(	kIx(	(
<g/>
reálné	reálný	k2eAgFnSc3d1	reálná
ceně	cena	k1gFnSc3	cena
<g/>
)	)	kIx)	)
5,7	[number]	k4	5,7
€	€	k?	€
za	za	k7c4	za
vlkm	vlkm	k1gInSc4	vlkm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přičtení	přičtení	k1gNnSc6	přičtení
zisku	zisk	k1gInSc2	zisk
a	a	k8xC	a
odečtení	odečtení	k1gNnSc6	odečtení
výnosů	výnos	k1gInPc2	výnos
bude	být	k5eAaImBp3nS	být
úhrada	úhrada	k1gFnSc1	úhrada
činit	činit	k5eAaImF	činit
4,52	[number]	k4	4,52
€	€	k?	€
za	za	k7c4	za
vlkm	vlkm	k1gInSc4	vlkm
bez	bez	k7c2	bez
přičtení	přičtení	k1gNnSc2	přičtení
propláceného	proplácený	k2eAgInSc2d1	proplácený
poplatku	poplatek	k1gInSc2	poplatek
za	za	k7c4	za
dopravní	dopravní	k2eAgFnSc4d1	dopravní
cestu	cesta	k1gFnSc4	cesta
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
1,45	[number]	k4	1,45
€	€	k?	€
za	za	k7c4	za
vlkm	vlkm	k1gInSc4	vlkm
<g/>
)	)	kIx)	)
a	a	k8xC	a
bez	bez	k7c2	bez
započtení	započtení	k1gNnSc2	započtení
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
dotace	dotace	k1gFnSc1	dotace
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
asi	asi	k9	asi
7,1	[number]	k4	7,1
miliónu	milión	k4xCgInSc2	milión
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
1,3	[number]	k4	1,3
miliónu	milión	k4xCgInSc2	milión
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
dotace	dotace	k1gFnSc1	dotace
státnímu	státní	k2eAgMnSc3d1	státní
dopravci	dopravce	k1gMnSc3	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Zakázku	zakázka	k1gFnSc4	zakázka
získala	získat	k5eAaPmAgFnS	získat
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
veřejné	veřejný	k2eAgFnSc2d1	veřejná
soutěže	soutěž	k1gFnSc2	soutěž
MDVRR	MDVRR	kA	MDVRR
SR	SR	kA	SR
vypsané	vypsaný	k2eAgFnPc1d1	vypsaná
ministrem	ministr	k1gMnSc7	ministr
dopravy	doprava	k1gFnSc2	doprava
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Vážným	vážný	k1gMnSc7	vážný
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
přímého	přímý	k2eAgNnSc2d1	přímé
zadání	zadání	k1gNnSc2	zadání
výkonů	výkon	k1gInPc2	výkon
ZSSK	ZSSK	kA	ZSSK
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
dotací	dotace	k1gFnPc2	dotace
z	z	k7c2	z
Operačního	operační	k2eAgInSc2d1	operační
programu	program	k1gInSc2	program
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
OPD	OPD	kA	OPD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
měl	mít	k5eAaImAgInS	mít
RegioJet	RegioJet	k1gInSc4	RegioJet
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
moderními	moderní	k2eAgFnPc7d1	moderní
dieselovými	dieselový	k2eAgFnPc7d1	dieselová
klimatizovanými	klimatizovaný	k2eAgFnPc7d1	klimatizovaná
jednotkami	jednotka	k1gFnPc7	jednotka
Bombardier	Bombardira	k1gFnPc2	Bombardira
Talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
umožnily	umožnit	k5eAaPmAgFnP	umožnit
zkrátit	zkrátit	k5eAaPmF	zkrátit
jízdní	jízdní	k2eAgFnPc1d1	jízdní
doby	doba	k1gFnPc1	doba
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Bratislava	Bratislava	k1gFnSc1	Bratislava
–	–	k?	–
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
Streda	Stred	k1gMnSc4	Stred
na	na	k7c4	na
43	[number]	k4	43
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
plánován	plánovat	k5eAaImNgInS	plánovat
na	na	k7c4	na
hodinový	hodinový	k2eAgInSc4d1	hodinový
interval	interval	k1gInSc4	interval
vlaků	vlak	k1gInPc2	vlak
Bratislava	Bratislava	k1gFnSc1	Bratislava
hl.	hl.	k?	hl.
st.	st.	kA	st.
–	–	k?	–
Komárno	Komárno	k1gNnSc1	Komárno
posílený	posílený	k2eAgMnSc1d1	posílený
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
na	na	k7c4	na
půlhodinový	půlhodinový	k2eAgInSc4d1	půlhodinový
interval	interval	k1gInSc4	interval
o	o	k7c4	o
zrychlené	zrychlený	k2eAgInPc4d1	zrychlený
spoje	spoj	k1gInPc4	spoj
Bratislava-Nové	Bratislava-Nové	k2eAgNnSc4d1	Bratislava-Nové
Mesto	Mesto	k1gNnSc4	Mesto
–	–	k?	–
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
Streda	Streda	k1gMnSc1	Streda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
RegioJet	RegioJet	k1gMnSc1	RegioJet
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
trase	trasa	k1gFnSc6	trasa
zkušební	zkušební	k2eAgFnSc2d1	zkušební
a	a	k8xC	a
prezentační	prezentační	k2eAgFnSc2d1	prezentační
jízdy	jízda	k1gFnSc2	jízda
přístupné	přístupný	k2eAgFnSc2d1	přístupná
i	i	k8xC	i
veřejnosti	veřejnost	k1gFnSc2	veřejnost
za	za	k7c4	za
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
cenu	cena	k1gFnSc4	cena
50	[number]	k4	50
eurocentů	eurocent	k1gInPc2	eurocent
(	(	kIx(	(
<g/>
12	[number]	k4	12
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
za	za	k7c4	za
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zkušební	zkušební	k2eAgInSc4d1	zkušební
provoz	provoz	k1gInSc4	provoz
si	se	k3xPyFc3	se
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
od	od	k7c2	od
německého	německý	k2eAgMnSc2d1	německý
dopravce	dopravce	k1gMnSc2	dopravce
Eurobahn	Eurobahn	k1gNnSc1	Eurobahn
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Keolis	Keolis	k1gInSc1	Keolis
soupravu	souprava	k1gFnSc4	souprava
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
provozu	provoz	k1gInSc6	provoz
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
RB	RB	kA	RB
71	[number]	k4	71
a	a	k8xC	a
RB	RB	kA	RB
73	[number]	k4	73
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bielefeldu	Bielefeld	k1gInSc2	Bielefeld
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Porýní-Vestfálsku	Porýní-Vestfálsko	k1gNnSc6	Porýní-Vestfálsko
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
SME	SME	k?	SME
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Berlína	Berlín	k1gInSc2	Berlín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Prignitzer	Prignitzer	k1gMnSc1	Prignitzer
Eisenbahn-Gesellschaft	Eisenbahn-Gesellschaft	k1gMnSc1	Eisenbahn-Gesellschaft
(	(	kIx(	(
<g/>
PEG	PEG	kA	PEG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
pod	pod	k7c4	pod
skupinu	skupina	k1gFnSc4	skupina
Netinera	Netiner	k1gMnSc2	Netiner
<g/>
,	,	kIx,	,
německou	německý	k2eAgFnSc4d1	německá
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
Italských	italský	k2eAgFnPc2d1	italská
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
převzetí	převzetí	k1gNnSc2	převzetí
německé	německý	k2eAgFnSc2d1	německá
části	část	k1gFnSc2	část
Arriva	Arriv	k1gMnSc2	Arriv
od	od	k7c2	od
koncernu	koncern	k1gInSc2	koncern
DB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zkušebních	zkušební	k2eAgFnPc6d1	zkušební
jízdách	jízda	k1gFnPc6	jízda
byla	být	k5eAaImAgFnS	být
souprava	souprava	k1gFnSc1	souprava
vybavena	vybavit	k5eAaPmNgFnS	vybavit
jen	jen	k9	jen
německými	německý	k2eAgInPc7d1	německý
nápisy	nápis	k1gInPc7	nápis
a	a	k8xC	a
dopravce	dopravce	k1gMnSc1	dopravce
neposkytoval	poskytovat	k5eNaImAgMnS	poskytovat
občerstvení	občerstvení	k1gNnPc4	občerstvení
ani	ani	k8xC	ani
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
wifi	wifi	k6eAd1	wifi
internet	internet	k1gInSc1	internet
však	však	k9	však
byl	být	k5eAaImAgInS	být
funkční	funkční	k2eAgInSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
linky	linka	k1gFnSc2	linka
si	se	k3xPyFc3	se
má	mít	k5eAaImIp3nS	mít
RegioJet	RegioJet	k1gInSc4	RegioJet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
na	na	k7c4	na
9	[number]	k4	9
let	léto	k1gNnPc2	léto
zapůjčit	zapůjčit	k5eAaPmF	zapůjčit
na	na	k7c4	na
operativní	operativní	k2eAgInSc4d1	operativní
leasing	leasing	k1gInSc4	leasing
9	[number]	k4	9
podobných	podobný	k2eAgFnPc2d1	podobná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
starých	starý	k2eAgFnPc2d1	stará
asi	asi	k9	asi
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
vybaví	vybavit	k5eAaPmIp3nP	vybavit
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
maďarskými	maďarský	k2eAgInPc7d1	maďarský
nápisy	nápis	k1gInPc7	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Soupravy	souprava	k1gFnPc1	souprava
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
kamerovým	kamerový	k2eAgInSc7d1	kamerový
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
provoz	provoz	k1gInSc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Nasazeny	nasazen	k2eAgFnPc1d1	nasazena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
původními	původní	k2eAgInPc7d1	původní
plány	plán	k1gInPc7	plán
nízkopodlažní	nízkopodlažní	k2eAgFnSc2d1	nízkopodlažní
klimatizované	klimatizovaný	k2eAgFnSc2d1	klimatizovaná
soupravy	souprava	k1gFnSc2	souprava
Bombardier	Bombardira	k1gFnPc2	Bombardira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
RegioJet	RegioJet	k1gMnSc1	RegioJet
oznámil	oznámit	k5eAaPmAgMnS	oznámit
zahájení	zahájení	k1gNnSc4	zahájení
příprav	příprava	k1gFnPc2	příprava
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
trasu	trasa	k1gFnSc4	trasa
Bratislava	Bratislava	k1gFnSc1	Bratislava
–	–	k?	–
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
až	až	k6eAd1	až
čtyřmi	čtyři	k4xCgNnPc7	čtyři
páry	pár	k1gInPc1	pár
spojů	spoj	k1gInPc2	spoj
od	od	k7c2	od
změny	změna	k1gFnSc2	změna
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgFnSc1d1	stávající
spoj	spoj	k1gFnSc1	spoj
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Žilina	Žilina	k1gFnSc1	Žilina
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
také	také	k9	také
prodloužen	prodloužen	k2eAgMnSc1d1	prodloužen
do	do	k7c2	do
Košic	Košice	k1gInPc2	Košice
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
zahájen	zahájit	k5eAaPmNgInS	zahájit
třemi	tři	k4xCgInPc7	tři
páry	pár	k1gInPc7	pár
spojů	spoj	k1gInPc2	spoj
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
jízdy	jízda	k1gFnSc2	jízda
necelých	celý	k2eNgFnPc2d1	necelá
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
šestihodinovém	šestihodinový	k2eAgInSc6d1	šestihodinový
intervalu	interval	k1gInSc6	interval
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
prodej	prodej	k1gInSc1	prodej
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
zastavovaly	zastavovat	k5eAaImAgInP	zastavovat
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
,	,	kIx,	,
Považská	Považský	k2eAgFnSc1d1	Považská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Vrútky	Vrútka	k1gFnPc1	Vrútka
<g/>
,	,	kIx,	,
Ružomberok	Ružomberok	k1gInSc1	Ružomberok
<g/>
,	,	kIx,	,
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
Kysak	Kysak	k1gInSc1	Kysak
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
RegioJet	RegioJet	k1gMnSc1	RegioJet
žádal	žádat	k5eAaImAgMnS	žádat
od	od	k7c2	od
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stát	stát	k1gInSc1	stát
proplácel	proplácet	k5eAaImAgInS	proplácet
kompenzace	kompenzace	k1gFnPc4	kompenzace
na	na	k7c4	na
slevy	sleva	k1gFnPc4	sleva
sociálním	sociální	k2eAgFnPc3d1	sociální
skupinám	skupina	k1gFnPc3	skupina
rovnoprávně	rovnoprávně	k6eAd1	rovnoprávně
všem	všecek	k3xTgMnPc3	všecek
dopravcům	dopravce	k1gMnPc3	dopravce
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
pouze	pouze	k6eAd1	pouze
vybranému	vybraný	k2eAgMnSc3d1	vybraný
státnímu	státní	k2eAgMnSc3d1	státní
dopravci	dopravce	k1gMnSc3	dopravce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prý	prý	k9	prý
za	za	k7c2	za
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
důchodci	důchodce	k1gMnPc1	důchodce
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
jezdí	jezdit	k5eAaImIp3nP	jezdit
ve	v	k7c6	v
vlacích	vlak	k1gInPc6	vlak
státních	státní	k2eAgFnPc2d1	státní
železnic	železnice	k1gFnPc2	železnice
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tuto	tento	k3xDgFnSc4	tento
nejvytíženější	vytížený	k2eAgFnSc4d3	nejvytíženější
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
linku	linka	k1gFnSc4	linka
provozovat	provozovat	k5eAaImF	provozovat
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
studenty	student	k1gMnPc4	student
a	a	k8xC	a
důchodce	důchodce	k1gMnPc4	důchodce
vozit	vozit	k5eAaImF	vozit
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mu	on	k3xPp3gMnSc3	on
stát	stát	k1gInSc1	stát
proplatí	proplatit	k5eAaPmIp3nS	proplatit
polovinu	polovina	k1gFnSc4	polovina
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc4	Jančur
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
slovenského	slovenský	k2eAgMnSc4d1	slovenský
ministra	ministr	k1gMnSc4	ministr
dopravy	doprava	k1gFnSc2	doprava
Jána	Ján	k1gMnSc4	Ján
Počiatka	Počiatka	k1gFnSc1	Počiatka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
neřeší	řešit	k5eNaImIp3nS	řešit
podnákladové	podnákladový	k2eAgFnPc4d1	podnákladová
ceny	cena	k1gFnPc4	cena
ztrátových	ztrátový	k2eAgInPc2d1	ztrátový
vlaků	vlak	k1gInPc2	vlak
IC	IC	kA	IC
společnosti	společnost	k1gFnSc2	společnost
ZSSK	ZSSK	kA	ZSSK
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
protizákonné	protizákonný	k2eAgNnSc1d1	protizákonné
křížové	křížový	k2eAgNnSc1d1	křížové
financování	financování	k1gNnSc1	financování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
RegioJet	RegioJet	k1gMnSc1	RegioJet
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
slovenský	slovenský	k2eAgInSc4d1	slovenský
antimonopolní	antimonopolní	k2eAgInSc4d1	antimonopolní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
nerozhodl	rozhodnout	k5eNaPmAgMnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
Jančura	Jančura	k1gFnSc1	Jančura
oznámil	oznámit	k5eAaPmAgInS	oznámit
postupné	postupný	k2eAgNnSc4d1	postupné
opuštění	opuštění	k1gNnSc4	opuštění
této	tento	k3xDgFnSc2	tento
trasy	trasa	k1gFnSc2	trasa
<g/>
:	:	kIx,	:
nejprve	nejprve	k6eAd1	nejprve
omezí	omezit	k5eAaPmIp3nS	omezit
provoz	provoz	k1gInSc4	provoz
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
páry	pára	k1gFnPc4	pára
vlaků	vlak	k1gInPc2	vlak
denně	denně	k6eAd1	denně
a	a	k8xC	a
zruší	zrušit	k5eAaPmIp3nS	zrušit
noční	noční	k2eAgInSc4d1	noční
vlak	vlak	k1gInSc4	vlak
<g/>
,	,	kIx,	,
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
čtvrtletí	čtvrtletí	k1gNnSc2	čtvrtletí
opustí	opustit	k5eAaPmIp3nS	opustit
trať	trať	k1gFnSc1	trať
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
své	svůj	k3xOyFgFnSc2	svůj
firmy	firma	k1gFnSc2	firma
označil	označit	k5eAaPmAgMnS	označit
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
socialismu	socialismus	k1gInSc2	socialismus
nad	nad	k7c7	nad
podnikáním	podnikání	k1gNnSc7	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněnou	uvolněný	k2eAgFnSc4d1	uvolněná
kapacitu	kapacita	k1gFnSc4	kapacita
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
chce	chtít	k5eAaImIp3nS	chtít
Jančura	Jančura	k1gFnSc1	Jančura
využít	využít	k5eAaPmF	využít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chce	chtít	k5eAaImIp3nS	chtít
prodloužit	prodloužit	k5eAaPmF	prodloužit
soupravy	souprava	k1gFnPc4	souprava
až	až	k9	až
na	na	k7c4	na
13	[number]	k4	13
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Linky	linka	k1gFnPc4	linka
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Košice	Košice	k1gInPc1	Košice
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
omezení	omezení	k1gNnSc1	omezení
dotknout	dotknout	k5eAaPmF	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
vyjel	vyjet	k5eAaPmAgInS	vyjet
RegioJet	RegioJet	k1gInSc1	RegioJet
mezi	mezi	k7c7	mezi
Bratislavou	Bratislava	k1gFnSc7	Bratislava
a	a	k8xC	a
Košicemi	Košice	k1gInPc7	Košice
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
diskriminačními	diskriminační	k2eAgFnPc7d1	diskriminační
podmínkami	podmínka	k1gFnPc7	podmínka
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
zavedl	zavést	k5eAaPmAgInS	zavést
Regiojet	Regiojet	k1gInSc4	Regiojet
denní	denní	k2eAgFnSc2d1	denní
pár	pár	k4xCyI	pár
spojů	spoj	k1gInPc2	spoj
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Košice	Košice	k1gInPc4	Košice
(	(	kIx(	(
<g/>
se	s	k7c7	s
stanicemi	stanice	k1gFnPc7	stanice
Praha	Praha	k1gFnSc1	Praha
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
Ostrava-Svinov	Ostrava-Svinov	k1gInSc1	Ostrava-Svinov
<g/>
,	,	kIx,	,
Čadca	Čadca	k1gFnSc1	Čadca
<g/>
,	,	kIx,	,
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Vrútky	Vrútka	k1gFnPc1	Vrútka
<g/>
,	,	kIx,	,
Ružomberok	Ružomberok	k1gInSc1	Ružomberok
<g/>
,	,	kIx,	,
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
Štrba	Štrba	k1gFnSc1	Štrba
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Poprad	Poprad	k1gInSc1	Poprad
<g/>
,	,	kIx,	,
Kysak	Kysak	k1gInSc1	Kysak
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
zavedl	zavést	k5eAaPmAgInS	zavést
RegioJet	RegioJet	k1gInSc4	RegioJet
noční	noční	k2eAgInPc1d1	noční
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
tří	tři	k4xCgInPc2	tři
párů	pár	k1gInPc2	pár
spojů	spoj	k1gInPc2	spoj
týdně	týdně	k6eAd1	týdně
(	(	kIx(	(
<g/>
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
stávajícím	stávající	k2eAgFnPc3d1	stávající
linkám	linka	k1gFnPc3	linka
zde	zde	k6eAd1	zde
navíc	navíc	k6eAd1	navíc
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
snídani	snídaně	k1gFnSc4	snídaně
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
stálý	stálý	k2eAgInSc4d1	stálý
dohled	dohled	k1gInSc4	dohled
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
personálu	personál	k1gInSc2	personál
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
4	[number]	k4	4
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
noční	noční	k2eAgInSc4d1	noční
klid	klid	k1gInSc4	klid
a	a	k8xC	a
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
personál	personál	k1gInSc1	personál
zapůjčuje	zapůjčovat	k5eAaImIp3nS	zapůjčovat
cestujícím	cestující	k1gMnPc3	cestující
deky	deka	k1gFnSc2	deka
<g/>
,	,	kIx,	,
na	na	k7c4	na
požádání	požádání	k1gNnSc4	požádání
personál	personál	k1gInSc4	personál
cestující	cestující	k2eAgInSc4d1	cestující
před	před	k7c7	před
jejich	jejich	k3xOp3gFnSc7	jejich
výstupní	výstupní	k2eAgFnSc7d1	výstupní
stanicí	stanice	k1gFnSc7	stanice
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
změny	změna	k1gFnSc2	změna
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
každodenní	každodenní	k2eAgFnPc4d1	každodenní
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
přidány	přidat	k5eAaPmNgInP	přidat
lehátkové	lehátkový	k2eAgInPc1d1	lehátkový
a	a	k8xC	a
lůžkové	lůžkový	k2eAgInPc1d1	lůžkový
vozy	vůz	k1gInPc1	vůz
<g/>
.	.	kIx.	.
<g/>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
cenové	cenový	k2eAgFnPc4d1	cenová
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lehátkové	lehátkový	k2eAgInPc4d1	lehátkový
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kupé	kupé	k1gNnPc2	kupé
šest	šest	k4xCc4	šest
lehátek	lehátko	k1gNnPc2	lehátko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestující	cestující	k1gMnPc4	cestující
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
lůžko	lůžko	k1gNnSc1	lůžko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kupé	kupé	k1gNnSc6	kupé
využita	využít	k5eAaPmNgFnS	využít
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc1	tři
lehátka	lehátko	k1gNnPc1	lehátko
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgNnPc1d1	zbylé
tři	tři	k4xCgFnPc1	tři
jsou	být	k5eAaImIp3nP	být
nevyužita	využit	k2eNgNnPc4d1	nevyužito
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
–	–	k?	–
Zvolen	Zvolen	k1gInSc1	Zvolen
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
RegioJet	RegioJet	k1gInSc4	RegioJet
k	k	k7c3	k
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
spojů	spoj	k1gInPc2	spoj
denně	denně	k6eAd1	denně
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
–	–	k?	–
Otrokovice	Otrokovice	k1gFnPc4	Otrokovice
–	–	k?	–
Hulín	Hulín	k1gInSc1	Hulín
–	–	k?	–
Přerov	Přerov	k1gInSc1	Přerov
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Zábřeh	Zábřeh	k1gInSc4	Zábřeh
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
–	–	k?	–
Pardubice	Pardubice	k1gInPc1	Pardubice
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
zavedl	zavést	k5eAaPmAgMnS	zavést
dopravce	dopravce	k1gMnSc1	dopravce
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ranní	ranní	k2eAgInSc1d1	ranní
spoj	spoj	k1gInSc1	spoj
jede	jet	k5eAaImIp3nS	jet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
večerní	večerní	k2eAgNnSc1d1	večerní
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
páry	pár	k1gInPc1	pár
spojů	spoj	k1gInPc2	spoj
začne	začít	k5eAaPmIp3nS	začít
dopravce	dopravce	k1gMnSc1	dopravce
provozovat	provozovat	k5eAaImF	provozovat
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
mezi	mezi	k7c7	mezi
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
spoje	spoj	k1gInPc1	spoj
budou	být	k5eAaImBp3nP	být
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dva	dva	k4xCgInPc4	dva
spoje	spoj	k1gInPc4	spoj
pojedou	pojet	k5eAaPmIp3nP	pojet
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
spoj	spoj	k1gInSc4	spoj
pojede	pojet	k5eAaPmIp3nS	pojet
přes	přes	k7c4	přes
Břeclav	Břeclav	k1gFnSc4	Břeclav
do	do	k7c2	do
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
tyto	tento	k3xDgInPc4	tento
spoje	spoj	k1gInPc4	spoj
chtěl	chtít	k5eAaImAgMnS	chtít
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc4	místo
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
nakonec	nakonec	k6eAd1	nakonec
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
žádostí	žádost	k1gFnPc2	žádost
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
kapacitu	kapacita	k1gFnSc4	kapacita
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
postupovat	postupovat	k5eAaImF	postupovat
podle	podle	k7c2	podle
zásad	zásada	k1gFnPc2	zásada
procesu	proces	k1gInSc2	proces
koordinace	koordinace	k1gFnSc2	koordinace
žádostí	žádost	k1gFnPc2	žádost
a	a	k8xC	a
prioritních	prioritní	k2eAgNnPc2d1	prioritní
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
trasy	trasa	k1gFnSc2	trasa
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Bratislava	Bratislava	k1gFnSc1	Bratislava
s	s	k7c7	s
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
hl.	hl.	k?	hl.
n.	n.	k?	n.
v	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
a	a	k8xC	a
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
byla	být	k5eAaImAgFnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
kapacita	kapacita	k1gFnSc1	kapacita
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
dopravci	dopravce	k1gMnPc1	dopravce
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dané	daný	k2eAgInPc1d1	daný
spoje	spoj	k1gInPc1	spoj
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dle	dle	k7c2	dle
objednávky	objednávka	k1gFnSc2	objednávka
dopravní	dopravní	k2eAgFnSc4d1	dopravní
obslužnost	obslužnost	k1gFnSc4	obslužnost
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Spoje	spoj	k1gInPc1	spoj
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
hl.	hl.	k?	hl.
n.	n.	k?	n.
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
do	do	k7c2	do
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
a	a	k8xC	a
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
v	v	k7c6	v
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
z	z	k7c2	z
brněnského	brněnský	k2eAgNnSc2d1	brněnské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
byly	být	k5eAaImAgFnP	být
dopravci	dopravce	k1gMnPc1	dopravce
schváleny	schválit	k5eAaPmNgInP	schválit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
četnosti	četnost	k1gFnSc2	četnost
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dopravce	dopravce	k1gMnSc1	dopravce
zažádal	zažádat	k5eAaPmAgMnS	zažádat
o	o	k7c4	o
kapacitu	kapacita	k1gFnSc4	kapacita
uvedených	uvedený	k2eAgInPc2d1	uvedený
spojů	spoj	k1gInPc2	spoj
denně	denně	k6eAd1	denně
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
větší	veliký	k2eAgInSc4d2	veliký
rozsah	rozsah	k1gInSc4	rozsah
dopravní	dopravní	k2eAgFnSc2d1	dopravní
obslužnosti	obslužnost	k1gFnSc2	obslužnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
začal	začít	k5eAaPmAgMnS	začít
dopravce	dopravce	k1gMnSc1	dopravce
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
lince	linka	k1gFnSc6	linka
poprvé	poprvé	k6eAd1	poprvé
nabízet	nabízet	k5eAaImF	nabízet
třídu	třída	k1gFnSc4	třída
Low	Low	k1gFnPc2	Low
Cost	Cost	k1gInSc4	Cost
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
cenou	cena	k1gFnSc7	cena
a	a	k8xC	a
omezeným	omezený	k2eAgInSc7d1	omezený
servisem	servis	k1gInSc7	servis
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
dopravce	dopravce	k1gMnSc1	dopravce
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
cestujících	cestující	k1gMnPc2	cestující
část	část	k1gFnSc1	část
služeb	služba	k1gFnPc2	služba
ráda	rád	k2eAgFnSc1d1	ráda
obětuje	obětovat	k5eAaBmIp3nS	obětovat
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
výhodnější	výhodný	k2eAgNnSc4d2	výhodnější
jízdné	jízdné	k1gNnSc4	jízdné
<g/>
,	,	kIx,	,
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
si	se	k3xPyFc3	se
nižší	nízký	k2eAgInSc4d2	nižší
výnos	výnos	k1gInSc4	výnos
z	z	k7c2	z
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
jízdenky	jízdenka	k1gFnSc2	jízdenka
vynahradí	vynahradit	k5eAaPmIp3nS	vynahradit
větší	veliký	k2eAgFnSc7d2	veliký
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
společnosti	společnost	k1gFnSc2	společnost
RegioJet	RegioJet	k1gInSc1	RegioJet
udělil	udělit	k5eAaPmAgInS	udělit
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
Banskobystrický	banskobystrický	k2eAgInSc1d1	banskobystrický
samosprávný	samosprávný	k2eAgInSc1d1	samosprávný
kraj	kraj	k1gInSc1	kraj
licenci	licence	k1gFnSc4	licence
k	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
nedotované	dotovaný	k2eNgFnSc2d1	nedotovaná
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
–	–	k?	–
Nitra	Nitra	k1gFnSc1	Nitra
–	–	k?	–
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gInSc1	RegioJet
zahájí	zahájit	k5eAaPmIp3nS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
linku	linka	k1gFnSc4	linka
budou	být	k5eAaImBp3nP	být
nasazeny	nasadit	k5eAaPmNgInP	nasadit
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgInPc1d1	nový
autokary	autokar	k1gInPc1	autokar
Irizar	Irizara	k1gFnPc2	Irizara
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
Volvo	Volvo	k1gNnSc1	Volvo
s	s	k7c7	s
výbavou	výbava	k1gFnSc7	výbava
FUN	FUN	kA	FUN
&	&	k?	&
RELAX	RELAX	kA	RELAX
ve	v	k7c6	v
žlutém	žlutý	k2eAgNnSc6d1	žluté
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
oznámil	oznámit	k5eAaPmAgInS	oznámit
RegioJet	RegioJet	k1gInSc1	RegioJet
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
Bratislava	Bratislava	k1gFnSc1	Bratislava
–	–	k?	–
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tak	tak	k9	tak
bude	být	k5eAaImBp3nS	být
konkurovat	konkurovat	k5eAaImF	konkurovat
autobusovému	autobusový	k2eAgMnSc3d1	autobusový
dopravci	dopravce	k1gMnSc3	dopravce
Slovak	Slovak	k1gMnSc1	Slovak
Lines	Lines	k1gMnSc1	Lines
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
provozu	provoz	k1gInSc2	provoz
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
16	[number]	k4	16
párů	pár	k1gInPc2	pár
spojů	spoj	k1gInPc2	spoj
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2016	[number]	k4	2016
postupně	postupně	k6eAd1	postupně
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
značku	značka	k1gFnSc4	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prý	prý	k9	prý
bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
srozumitelnější	srozumitelný	k2eAgFnSc1d2	srozumitelnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
příběh	příběh	k1gInSc1	příběh
firmy	firma	k1gFnSc2	firma
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
lidé	člověk	k1gMnPc1	člověk
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
neznají	neznat	k5eAaImIp3nP	neznat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nepraktický	praktický	k2eNgInSc1d1	nepraktický
název	název	k1gInSc1	název
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgFnSc4d1	dopravní
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
zavádí	zavádět	k5eAaImIp3nP	zavádět
i	i	k9	i
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
rebranding	rebranding	k1gInSc1	rebranding
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
oficiálně	oficiálně	k6eAd1	oficiálně
zahájen	zahájit	k5eAaPmNgInS	zahájit
<g/>
,	,	kIx,	,
logo	logo	k1gNnSc4	logo
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
měněno	měnit	k5eAaImNgNnS	měnit
na	na	k7c6	na
autobusových	autobusový	k2eAgInPc6d1	autobusový
nádražích	nádraží	k1gNnPc6	nádraží
<g/>
,	,	kIx,	,
autobusech	autobus	k1gInPc6	autobus
<g/>
,	,	kIx,	,
v	v	k7c6	v
informačních	informační	k2eAgNnPc6d1	informační
hlášeních	hlášení	k1gNnPc6	hlášení
<g/>
,	,	kIx,	,
na	na	k7c6	na
informačních	informační	k2eAgInPc6d1	informační
a	a	k8xC	a
propagačních	propagační	k2eAgInPc6d1	propagační
materiálech	materiál	k1gInPc6	materiál
<g/>
,	,	kIx,	,
prodejních	prodejní	k2eAgNnPc6d1	prodejní
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
chce	chtít	k5eAaImIp3nS	chtít
RegioJet	RegioJet	k1gInSc4	RegioJet
prezentovat	prezentovat	k5eAaBmF	prezentovat
jako	jako	k9	jako
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
síť	síť	k1gFnSc4	síť
autobusových	autobusový	k2eAgInPc2d1	autobusový
a	a	k8xC	a
vlakových	vlakový	k2eAgInPc2d1	vlakový
spojů	spoj	k1gInPc2	spoj
s	s	k7c7	s
přepravními	přepravní	k2eAgFnPc7d1	přepravní
službami	služba	k1gFnPc7	služba
špičkové	špičkový	k2eAgFnSc2d1	špičková
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
otevřel	otevřít	k5eAaPmAgInS	otevřít
RegioJet	RegioJet	k1gInSc1	RegioJet
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
ČD	ČD	kA	ČD
centra	centrum	k1gNnPc1	centrum
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
prodejní	prodejní	k2eAgFnSc4d1	prodejní
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zahájení	zahájení	k1gNnSc2	zahájení
předprodeje	předprodej	k1gInSc2	předprodej
pro	pro	k7c4	pro
vlaky	vlak	k1gInPc4	vlak
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
železničních	železniční	k2eAgFnPc2d1	železniční
jízdenek	jízdenka	k1gFnPc2	jízdenka
z	z	k7c2	z
prodejního	prodejní	k2eAgInSc2d1	prodejní
a	a	k8xC	a
rezervačního	rezervační	k2eAgInSc2d1	rezervační
systému	systém	k1gInSc2	systém
DB	db	kA	db
Bahn	Bahn	k1gMnSc1	Bahn
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
sousední	sousední	k2eAgFnSc1d1	sousední
kancelář	kancelář	k1gFnSc1	kancelář
ČD	ČD	kA	ČD
Travel	Travela	k1gFnPc2	Travela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prodej	prodej	k1gInSc1	prodej
autobusových	autobusový	k2eAgFnPc2d1	autobusová
jízdenek	jízdenka	k1gFnPc2	jízdenka
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
express	express	k1gInSc1	express
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
doplňkových	doplňkový	k2eAgFnPc2d1	doplňková
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
cestovního	cestovní	k2eAgNnSc2d1	cestovní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
,	,	kIx,	,
či	či	k8xC	či
zprostředkovávat	zprostředkovávat	k5eAaImF	zprostředkovávat
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
autopůjčoven	autopůjčovna	k1gFnPc2	autopůjčovna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
železniční	železniční	k2eAgFnSc4d1	železniční
jízdenku	jízdenka	k1gFnSc4	jízdenka
prodal	prodat	k5eAaPmAgMnS	prodat
v	v	k7c4	v
den	den	k1gInSc4	den
otevření	otevření	k1gNnSc2	otevření
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Düsseldorfu	Düsseldorf	k1gInSc2	Düsseldorf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
otevřel	otevřít	k5eAaPmAgMnS	otevřít
další	další	k2eAgNnSc4d1	další
prodejní	prodejní	k2eAgNnSc4d1	prodejní
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
témž	týž	k3xTgNnSc6	týž
podlaží	podlaží	k1gNnSc6	podlaží
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
ČD	ČD	kA	ČD
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zahájil	zahájit	k5eAaPmAgInS	zahájit
prodej	prodej	k1gInSc1	prodej
železničních	železniční	k2eAgFnPc2d1	železniční
jízdenek	jízdenka	k1gFnPc2	jízdenka
i	i	k9	i
v	v	k7c6	v
centrále	centrála	k1gFnSc6	centrála
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
pobočce	pobočka	k1gFnSc6	pobočka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
Ječné	ječný	k2eAgFnSc6d1	Ječná
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
prodejních	prodejní	k2eAgNnPc6d1	prodejní
místech	místo	k1gNnPc6	místo
SA	SA	kA	SA
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
železniční	železniční	k2eAgFnPc4d1	železniční
jízdenky	jízdenka	k1gFnPc4	jízdenka
objednat	objednat	k5eAaPmF	objednat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
týdnech	týden	k1gInPc6	týden
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
možnost	možnost	k1gFnSc1	možnost
internetového	internetový	k2eAgInSc2d1	internetový
a	a	k8xC	a
telefonického	telefonický	k2eAgInSc2d1	telefonický
nákupu	nákup	k1gInSc2	nákup
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
počet	počet	k1gInSc1	počet
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
prodejních	prodejní	k2eAgNnPc2d1	prodejní
míst	místo	k1gNnPc2	místo
na	na	k7c4	na
33	[number]	k4	33
<g/>
,	,	kIx,	,
4	[number]	k4	4
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
x	x	k?	x
Košice	Košice	k1gInPc1	Košice
a	a	k8xC	a
1	[number]	k4	1
<g/>
x	x	k?	x
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jízdenku	jízdenka	k1gFnSc4	jízdenka
dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
zakoupit	zakoupit	k5eAaPmF	zakoupit
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
provizních	provizní	k2eAgMnPc2d1	provizní
prodejců	prodejce	k1gMnPc2	prodejce
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
oficiální	oficiální	k2eAgNnPc1d1	oficiální
prodejní	prodejní	k2eAgNnPc1d1	prodejní
místa	místo	k1gNnPc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
stojí	stát	k5eAaImIp3nS	stát
jízdenky	jízdenka	k1gFnPc4	jízdenka
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
při	při	k7c6	při
nákupu	nákup	k1gInSc6	nákup
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
žádal	žádat	k5eAaImAgMnS	žádat
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
provizní	provizní	k2eAgFnPc1d1	provizní
prodej	prodej	k1gInSc4	prodej
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zákonná	zákonný	k2eAgFnSc1d1	zákonná
povinnost	povinnost	k1gFnSc1	povinnost
prodat	prodat	k5eAaPmF	prodat
jízdenku	jízdenka	k1gFnSc4	jízdenka
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
trasu	trasa	k1gFnSc4	trasa
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc1	kolik
dopravců	dopravce	k1gMnPc2	dopravce
se	se	k3xPyFc4	se
na	na	k7c6	na
přepravě	přeprava	k1gFnSc6	přeprava
podílí	podílet	k5eAaImIp3nS	podílet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
uzavřít	uzavřít	k5eAaPmF	uzavřít
provizní	provizní	k2eAgFnSc4d1	provizní
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
RegioJet	RegioJet	k1gInSc1	RegioJet
mohl	moct	k5eAaImAgInS	moct
prodávat	prodávat	k5eAaImF	prodávat
jízdenky	jízdenka	k1gFnPc4	jízdenka
pro	pro	k7c4	pro
vlaky	vlak	k1gInPc4	vlak
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
RegioJet	RegioJet	k1gMnSc1	RegioJet
proto	proto	k8xC	proto
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
zásah	zásah	k1gInSc4	zásah
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Rusnokovy	Rusnokův	k2eAgFnSc2d1	Rusnokova
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
výměně	výměna	k1gFnSc6	výměna
vedení	vedení	k1gNnSc2	vedení
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
nový	nový	k2eAgMnSc1d1	nový
ředitel	ředitel	k1gMnSc1	ředitel
ČD	ČD	kA	ČD
Dalibor	Dalibor	k1gMnSc1	Dalibor
Zelený	Zelený	k1gMnSc1	Zelený
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
RegioJetem	RegioJet	k1gInSc7	RegioJet
i	i	k8xC	i
s	s	k7c7	s
LEO	Lea	k1gFnSc5	Lea
Expressem	express	k1gInSc7	express
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
prodeje	prodej	k1gInSc2	prodej
jízdenek	jízdenka	k1gFnPc2	jízdenka
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
i	i	k9	i
propojení	propojení	k1gNnSc4	propojení
rezervačních	rezervační	k2eAgInPc2d1	rezervační
systémů	systém	k1gInPc2	systém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
idnes	idnesa	k1gFnPc2	idnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
předmětem	předmět	k1gInSc7	předmět
jednání	jednání	k1gNnPc2	jednání
i	i	k8xC	i
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
uznávání	uznávání	k1gNnSc1	uznávání
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
RegioJetu	RegioJet	k1gInSc2	RegioJet
uvítalo	uvítat	k5eAaPmAgNnS	uvítat
možnost	možnost	k1gFnSc4	možnost
prodávat	prodávat	k5eAaImF	prodávat
jízdenky	jízdenka	k1gFnPc4	jízdenka
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
a	a	k8xC	a
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ČD	ČD	kA	ČD
prodávaly	prodávat	k5eAaImAgInP	prodávat
jízdenky	jízdenka	k1gFnPc1	jízdenka
RegioJetu	RegioJeta	k1gFnSc4	RegioJeta
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vedení	vedení	k1gNnSc4	vedení
Leo	Leo	k1gMnSc1	Leo
Expressu	express	k1gInSc2	express
uvítalo	uvítat	k5eAaPmAgNnS	uvítat
snahu	snaha	k1gFnSc4	snaha
ČD	ČD	kA	ČD
o	o	k7c4	o
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
se	se	k3xPyFc4	se
slovenskému	slovenský	k2eAgInSc3d1	slovenský
státu	stát	k1gInSc3	stát
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaměstná	zaměstnat	k5eAaPmIp3nS	zaměstnat
slovenské	slovenský	k2eAgMnPc4d1	slovenský
železničáře	železničář	k1gMnPc4	železničář
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgInS	vydat
tiskovou	tiskový	k2eAgFnSc4d1	tisková
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zahájil	zahájit	k5eAaPmAgInS	zahájit
nábor	nábor	k1gInSc4	nábor
80	[number]	k4	80
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
manažerů	manažer	k1gMnPc2	manažer
<g/>
,	,	kIx,	,
strojvedoucích	strojvedoucí	k1gMnPc2	strojvedoucí
<g/>
,	,	kIx,	,
vlakvedoucích	vlakvedoucí	k1gFnPc2	vlakvedoucí
–	–	k?	–
stevardů	stevard	k1gMnPc2	stevard
i	i	k8xC	i
pokladních	pokladní	k1gFnPc2	pokladní
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
hledal	hledat	k5eAaImAgInS	hledat
pro	pro	k7c4	pro
trať	trať	k1gFnSc4	trať
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
strojvedoucí	strojvedoucí	k1gMnSc1	strojvedoucí
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
spustil	spustit	k5eAaPmAgInS	spustit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
fakultou	fakulta	k1gFnSc7	fakulta
VŠB	VŠB	kA	VŠB
–	–	k?	–
TU	tu	k9	tu
Ostrava	Ostrava	k1gFnSc1	Ostrava
nábor	nábor	k1gInSc1	nábor
SuperStevardů	SuperStevard	k1gInPc2	SuperStevard
a	a	k8xC	a
SuperStevardek	SuperStevardka	k1gFnPc2	SuperStevardka
pro	pro	k7c4	pro
trať	trať	k1gFnSc4	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Stevardi	stevard	k1gMnPc1	stevard
a	a	k8xC	a
stevardky	stevardka	k1gFnPc1	stevardka
nosí	nosit	k5eAaImIp3nP	nosit
jako	jako	k9	jako
uniformu	uniforma	k1gFnSc4	uniforma
růžovofialové	růžovofialový	k2eAgFnSc2d1	růžovofialová
košile	košile	k1gFnSc2	košile
a	a	k8xC	a
blůzy	blůza	k1gFnSc2	blůza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc2	Jančur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
probíhá	probíhat	k5eAaImIp3nS	probíhat
výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
pět	pět	k4xCc4	pět
souprav	souprava	k1gFnPc2	souprava
v	v	k7c6	v
typické	typický	k2eAgFnSc6d1	typická
žluté	žlutý	k2eAgFnSc6d1	žlutá
firemní	firemní	k2eAgFnSc6d1	firemní
barvě	barva	k1gFnSc6	barva
(	(	kIx(	(
<g/>
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
dohromady	dohromady	k6eAd1	dohromady
o	o	k7c4	o
šest	šest	k4xCc4	šest
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednu	jeden	k4xCgFnSc4	jeden
rezervní	rezervní	k2eAgFnSc4d1	rezervní
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
trať	trať	k1gFnSc4	trať
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
firmy	firma	k1gFnPc1	firma
Siemens	siemens	k1gInSc1	siemens
a	a	k8xC	a
Stadler	Stadler	k1gInSc1	Stadler
<g/>
,	,	kIx,	,
design	design	k1gInSc1	design
vlaku	vlak	k1gInSc2	vlak
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
blížit	blížit	k5eAaImF	blížit
vlakům	vlak	k1gInPc3	vlak
ICE	ICE	kA	ICE
3	[number]	k4	3
<g/>
,	,	kIx,	,
jezdícím	jezdící	k2eAgNnSc6d1	jezdící
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
rychlíkových	rychlíkový	k2eAgFnPc2d1	rychlíková
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
podané	podaný	k2eAgFnPc1d1	podaná
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
dopravu	doprava	k1gFnSc4	doprava
novými	nový	k2eAgInPc7d1	nový
vlaky	vlak	k1gInPc7	vlak
Siemens	siemens	k1gInSc1	siemens
Desiro	Desiro	k1gNnSc4	Desiro
ML	ml	kA	ml
(	(	kIx(	(
<g/>
50	[number]	k4	50
souprav	souprava	k1gFnPc2	souprava
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
až	až	k6eAd1	až
čtyřech	čtyři	k4xCgInPc6	čtyři
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
linek	linka	k1gFnPc2	linka
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
–	–	k?	–
elektrické	elektrický	k2eAgFnSc2d1	elektrická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Desiro	Desiro	k1gNnSc1	Desiro
Classic	Classice	k1gFnPc2	Classice
(	(	kIx(	(
<g/>
35	[number]	k4	35
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
linek	linka	k1gFnPc2	linka
3	[number]	k4	3
–	–	k?	–
dieselové	dieselový	k2eAgFnPc1d1	dieselová
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
a	a	k8xC	a
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jančura	Jančura	k1gFnSc1	Jančura
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
nákupu	nákup	k1gInSc6	nákup
60	[number]	k4	60
použitých	použitý	k2eAgInPc2d1	použitý
vozů	vůz	k1gInPc2	vůz
Talgo	Talgo	k1gMnSc1	Talgo
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
RegioJet	RegioJet	k1gMnSc1	RegioJet
pořádal	pořádat	k5eAaImAgMnS	pořádat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
propagační	propagační	k2eAgFnSc2d1	propagační
jízdy	jízda	k1gFnSc2	jízda
jednotkami	jednotka	k1gFnPc7	jednotka
Siemens	siemens	k1gInSc1	siemens
Desiro	Desira	k1gFnSc5	Desira
vypůjčenými	vypůjčený	k2eAgInPc7d1	vypůjčený
od	od	k7c2	od
leasingové	leasingový	k2eAgFnSc2d1	leasingová
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
lince	linka	k1gFnSc6	linka
měl	mít	k5eAaImAgInS	mít
RegioJet	RegioJet	k1gInSc1	RegioJet
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
moderními	moderní	k2eAgFnPc7d1	moderní
dieselovými	dieselový	k2eAgFnPc7d1	dieselová
klimatizovanými	klimatizovaný	k2eAgFnPc7d1	klimatizovaná
jednotkami	jednotka	k1gFnPc7	jednotka
Bombardier	Bombardira	k1gFnPc2	Bombardira
Talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zkušební	zkušební	k2eAgFnSc4d1	zkušební
a	a	k8xC	a
prezentační	prezentační	k2eAgFnSc2d1	prezentační
jízdy	jízda	k1gFnSc2	jízda
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
trase	trasa	k1gFnSc6	trasa
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
si	se	k3xPyFc3	se
zapůjčil	zapůjčit	k5eAaPmAgMnS	zapůjčit
od	od	k7c2	od
německého	německý	k2eAgMnSc2d1	německý
dopravce	dopravce	k1gMnSc2	dopravce
Eurobahn	Eurobahn	k1gNnSc1	Eurobahn
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Keolis	Keolis	k1gInSc1	Keolis
soupravu	souprava	k1gFnSc4	souprava
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
provozu	provoz	k1gInSc6	provoz
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
RB	RB	kA	RB
71	[number]	k4	71
a	a	k8xC	a
RB	RB	kA	RB
73	[number]	k4	73
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bielefeldu	Bielefeld	k1gInSc2	Bielefeld
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
<g />
.	.	kIx.	.
</s>
<s>
Porýní-Vestfálsku	Porýní-Vestfálska	k1gFnSc4	Porýní-Vestfálska
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
SME	SME	k?	SME
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Berlína	Berlín	k1gInSc2	Berlín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Prignitzer	Prignitzer	k1gMnSc1	Prignitzer
Eisenbahn-Gesellschaft	Eisenbahn-Gesellschaft	k1gMnSc1	Eisenbahn-Gesellschaft
(	(	kIx(	(
<g/>
PEG	PEG	kA	PEG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
pod	pod	k7c4	pod
skupinu	skupina	k1gFnSc4	skupina
Netinera	Netiner	k1gMnSc2	Netiner
<g/>
,	,	kIx,	,
německou	německý	k2eAgFnSc4d1	německá
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
Italských	italský	k2eAgFnPc2d1	italská
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
FS	FS	kA	FS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
převzetí	převzetí	k1gNnSc2	převzetí
německé	německý	k2eAgFnSc2d1	německá
části	část	k1gFnSc2	část
Arriva	Arriv	k1gMnSc2	Arriv
od	od	k7c2	od
koncernu	koncern	k1gInSc2	koncern
DB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
linky	linka	k1gFnSc2	linka
si	se	k3xPyFc3	se
má	mít	k5eAaImIp3nS	mít
RegioJet	RegioJet	k1gInSc4	RegioJet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
na	na	k7c4	na
9	[number]	k4	9
let	léto	k1gNnPc2	léto
zapůjčit	zapůjčit	k5eAaPmF	zapůjčit
na	na	k7c4	na
operativní	operativní	k2eAgInSc4d1	operativní
leasing	leasing	k1gInSc4	leasing
9	[number]	k4	9
podobných	podobný	k2eAgFnPc2d1	podobná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
starých	starý	k2eAgFnPc2d1	stará
asi	asi	k9	asi
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
vybaví	vybavit	k5eAaPmIp3nP	vybavit
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
maďarskými	maďarský	k2eAgInPc7d1	maďarský
nápisy	nápis	k1gInPc7	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Soupravy	souprava	k1gFnPc1	souprava
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
kamerovým	kamerový	k2eAgInSc7d1	kamerový
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
nákup	nákup	k1gInSc1	nákup
devíti	devět	k4xCc2	devět
elektrických	elektrický	k2eAgFnPc2d1	elektrická
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
řady	řada	k1gFnSc2	řada
E	E	kA	E
630	[number]	k4	630
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jako	jako	k8xS	jako
ř.	ř.	k?	ř.
163	[number]	k4	163
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
pod	pod	k7c7	pod
výrobními	výrobní	k2eAgNnPc7d1	výrobní
čísly	číslo	k1gNnPc7	číslo
8853	[number]	k4	8853
až	až	k8xS	až
8861	[number]	k4	8861
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
pro	pro	k7c4	pro
italské	italský	k2eAgInPc4d1	italský
poměry	poměr	k1gInPc4	poměr
a	a	k8xC	a
prodány	prodán	k2eAgFnPc1d1	prodána
italské	italský	k2eAgFnPc1d1	italská
společnosti	společnost	k1gFnPc1	společnost
Ferrovie	Ferrovie	k1gFnSc2	Ferrovie
Nord	Norda	k1gFnPc2	Norda
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
RegioJetu	RegioJet	k1gInSc2	RegioJet
však	však	k9	však
jejich	jejich	k3xOp3gNnSc4	jejich
opotřebení	opotřebení	k1gNnSc4	opotřebení
"	"	kIx"	"
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
tříletému	tříletý	k2eAgInSc3d1	tříletý
provozu	provoz	k1gInSc3	provoz
<g/>
"	"	kIx"	"
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dohromady	dohromady	k6eAd1	dohromady
hodnotu	hodnota	k1gFnSc4	hodnota
"	"	kIx"	"
<g/>
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kupní	kupní	k2eAgFnSc1d1	kupní
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Údaj	údaj	k1gInSc1	údaj
ČTK	ČTK	kA	ČTK
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
ceně	cena	k1gFnSc6	cena
za	za	k7c4	za
nákup	nákup	k1gInSc4	nákup
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
RegioJet	RegioJet	k1gMnSc1	RegioJet
rezolutně	rezolutně	k6eAd1	rezolutně
popřel	popřít	k5eAaPmAgMnS	popřít
jako	jako	k9	jako
nesprávný	správný	k2eNgMnSc1d1	nesprávný
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
byla	být	k5eAaImAgFnS	být
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
většina	většina	k1gFnSc1	většina
úprav	úprava	k1gFnPc2	úprava
pro	pro	k7c4	pro
FNM	FNM	kA	FNM
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
převodový	převodový	k2eAgInSc1d1	převodový
poměr	poměr	k1gInSc1	poměr
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
maximální	maximální	k2eAgFnSc2d1	maximální
rychlosti	rychlost	k1gFnSc2	rychlost
ze	z	k7c2	z
120	[number]	k4	120
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
na	na	k7c4	na
140	[number]	k4	140
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Stanoviště	stanoviště	k1gNnSc1	stanoviště
strojvůdce	strojvůdce	k1gMnSc2	strojvůdce
je	být	k5eAaImIp3nS	být
přemístěno	přemístěn	k2eAgNnSc1d1	přemístěno
zpět	zpět	k6eAd1	zpět
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
zpět	zpět	k6eAd1	zpět
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odstraněno	odstraněn	k2eAgNnSc1d1	odstraněno
ovládání	ovládání	k1gNnSc1	ovládání
z	z	k7c2	z
řídícího	řídící	k2eAgInSc2d1	řídící
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
kondenzátory	kondenzátor	k1gInPc1	kondenzátor
filtru	filtr	k1gInSc2	filtr
jsou	být	k5eAaImIp3nP	být
přemístěny	přemístit	k5eAaPmNgFnP	přemístit
zpod	zpod	k7c2	zpod
rámu	rám	k1gInSc2	rám
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
strojovny	strojovna	k1gFnSc2	strojovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
opět	opět	k6eAd1	opět
neprůchozí	průchozí	k2eNgFnSc1d1	neprůchozí
pod	pod	k7c7	pod
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
nová	nový	k2eAgFnSc1d1	nová
radiostanice	radiostanice	k1gFnSc1	radiostanice
<g/>
,	,	kIx,	,
vlakový	vlakový	k2eAgInSc1d1	vlakový
zabezpečovač	zabezpečovač	k1gInSc1	zabezpečovač
MIREL	MIREL	kA	MIREL
VZ1	VZ1	k1gFnSc2	VZ1
a	a	k8xC	a
elektronický	elektronický	k2eAgInSc1d1	elektronický
rychloměr	rychloměr	k1gInSc1	rychloměr
Tramex	Tramex	k1gInSc1	Tramex
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
je	být	k5eAaImIp3nS	být
instalováno	instalován	k2eAgNnSc1d1	instalováno
přemostění	přemostění	k1gNnSc1	přemostění
záchranné	záchranný	k2eAgFnSc2d1	záchranná
brzdy	brzda	k1gFnSc2	brzda
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgNnSc1d1	centrální
ovládání	ovládání	k1gNnSc1	ovládání
dveří	dveře	k1gFnPc2	dveře
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
a	a	k8xC	a
UIC	UIC	kA	UIC
zásuvky	zásuvka	k1gFnSc2	zásuvka
na	na	k7c6	na
čelech	čelo	k1gNnPc6	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgInP	upravit
sběrače	sběrač	k1gInPc1	sběrač
dosazením	dosazení	k1gNnSc7	dosazení
smykadel	smykadlo	k1gNnPc2	smykadlo
šířky	šířka	k1gFnSc2	šířka
1950	[number]	k4	1950
mm	mm	kA	mm
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c4	na
část	část	k1gFnSc4	část
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
byly	být	k5eAaImAgInP	být
dosazeny	dosazen	k2eAgInPc1d1	dosazen
sběrače	sběrač	k1gInPc1	sběrač
nové	nový	k2eAgInPc1d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgNnSc1d1	výrazné
snížení	snížení	k1gNnSc1	snížení
odporníkové	odporníkové	k?	odporníkové
skříně	skříň	k1gFnSc2	skříň
elektrodynamické	elektrodynamický	k2eAgFnSc2d1	elektrodynamická
brzdy	brzda	k1gFnSc2	brzda
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Úpravy	úprava	k1gFnPc4	úprava
provedla	provést	k5eAaPmAgFnS	provést
firma	firma	k1gFnSc1	firma
ČMŽO	ČMŽO	kA	ČMŽO
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
úprav	úprava	k1gFnPc2	úprava
nebyla	být	k5eNaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
9	[number]	k4	9
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
řady	řada	k1gFnSc2	řada
162	[number]	k4	162
(	(	kIx(	(
<g/>
162.119	[number]	k4	162.119
<g/>
)	)	kIx)	)
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
technicko-bezpečnostní	technickoezpečnostní	k2eAgFnSc4d1	technicko-bezpečnostní
zkoušku	zkouška	k1gFnSc4	zkouška
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
330	[number]	k4	330
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
testy	test	k1gInPc1	test
nutné	nutný	k2eAgInPc1d1	nutný
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
typu	typ	k1gInSc2	typ
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
ještě	ještě	k9	ještě
na	na	k7c6	na
Železničním	železniční	k2eAgInSc6d1	železniční
zkušebním	zkušební	k2eAgInSc6d1	zkušební
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
Cerhenicích	Cerhenice	k1gFnPc6	Cerhenice
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
testovanou	testovaný	k2eAgFnSc7d1	testovaná
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
byla	být	k5eAaImAgFnS	být
162.120	[number]	k4	162.120
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
absolvovaly	absolvovat	k5eAaPmAgFnP	absolvovat
technicko-bezpečnostní	technickoezpečnostní	k2eAgFnSc4d1	technicko-bezpečnostní
zkoušku	zkouška	k1gFnSc4	zkouška
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
a	a	k8xC	a
čekaly	čekat	k5eAaImAgFnP	čekat
na	na	k7c6	na
schválení	schválení	k1gNnSc6	schválení
Drážním	drážní	k2eAgInSc7d1	drážní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
provozu	provoz	k1gInSc2	provoz
vlastních	vlastní	k2eAgInPc2d1	vlastní
expresů	expres	k1gInPc2	expres
pronajímala	pronajímat	k5eAaImAgFnS	pronajímat
SA	SA	kA	SA
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
jiným	jiný	k2eAgMnPc3d1	jiný
dopravcům	dopravce	k1gMnPc3	dopravce
pro	pro	k7c4	pro
vozbu	vozba	k1gFnSc4	vozba
nákladních	nákladní	k2eAgInPc2d1	nákladní
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
služební	služební	k2eAgInPc4d1	služební
účely	účel	k1gInPc4	účel
pořídil	pořídit	k5eAaPmAgMnS	pořídit
RegioJet	RegioJet	k1gMnSc1	RegioJet
starší	starý	k2eAgFnSc4d2	starší
dieselovou	dieselový	k2eAgFnSc4d1	dieselová
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kovošrotu	kovošrot	k1gInSc2	kovošrot
v	v	k7c6	v
Polance	polanka	k1gFnSc6	polanka
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
byl	být	k5eAaImAgMnS	být
odkoupen	odkoupen	k2eAgInSc4d1	odkoupen
stroj	stroj	k1gInSc4	stroj
721.151	[number]	k4	721.151
<g/>
.	.	kIx.	.
</s>
<s>
Opraven	opraven	k2eAgMnSc1d1	opraven
a	a	k8xC	a
do	do	k7c2	do
firemního	firemní	k2eAgInSc2d1	firemní
laku	lak	k1gInSc2	lak
natřen	natřen	k2eAgMnSc1d1	natřen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
lokomotivních	lokomotivní	k2eAgFnPc6d1	lokomotivní
opravnách	opravna	k1gFnPc6	opravna
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
lokomotivami	lokomotiva	k1gFnPc7	lokomotiva
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Prahy-Zličína	Prahy-Zličín	k1gInSc2	Prahy-Zličín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
ovládnout	ovládnout	k5eAaPmF	ovládnout
trasu	trasa	k1gFnSc4	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
dalších	další	k2eAgFnPc2d1	další
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
nabídky	nabídka	k1gFnPc4	nabídka
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
koupené	koupený	k2eAgFnPc1d1	koupená
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
dodány	dodat	k5eAaPmNgInP	dodat
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
by	by	kYmCp3nS	by
o	o	k7c6	o
pronájmu	pronájem	k1gInSc6	pronájem
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
RegioJet	RegioJet	k1gMnSc1	RegioJet
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
Rakouských	rakouský	k2eAgFnPc2d1	rakouská
spolkových	spolkový	k2eAgFnPc2d1	spolková
drah	draha	k1gFnPc2	draha
14	[number]	k4	14
ojetých	ojetý	k2eAgInPc2d1	ojetý
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
v	v	k7c6	v
letech	let	k1gInPc6	let
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
již	již	k6eAd1	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
rekonstruovány	rekonstruovat	k5eAaBmNgFnP	rekonstruovat
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
klimatizaci	klimatizace	k1gFnSc4	klimatizace
<g/>
,	,	kIx,	,
vakuové	vakuový	k2eAgFnPc1d1	vakuová
toalety	toaleta	k1gFnPc1	toaleta
a	a	k8xC	a
elektrické	elektrický	k2eAgFnPc1d1	elektrická
zásuvky	zásuvka	k1gFnPc1	zásuvka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
úprav	úprava	k1gFnPc2	úprava
pro	pro	k7c4	pro
RegioJet	RegioJet	k1gInSc4	RegioJet
mají	mít	k5eAaImIp3nP	mít
získat	získat	k5eAaPmF	získat
například	například	k6eAd1	například
nový	nový	k2eAgInSc4d1	nový
lak	lak	k1gInSc4	lak
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc4d1	nový
koberce	koberec	k1gInPc4	koberec
apod.	apod.	kA	apod.
Lakování	lakování	k1gNnPc2	lakování
do	do	k7c2	do
korporátních	korporátní	k2eAgFnPc2d1	korporátní
barev	barva	k1gFnPc2	barva
realizuje	realizovat	k5eAaBmIp3nS	realizovat
OLPAS	OLPAS	kA	OLPAS
Moravia	Moravia	k1gFnSc1	Moravia
v	v	k7c6	v
Krnově	Krnov	k1gInSc6	Krnov
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc4	oprava
a	a	k8xC	a
úpravy	úprava	k1gFnPc4	úprava
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
pak	pak	k9	pak
METRANS	METRANS	kA	METRANS
DYKO	DYKO	kA	DYKO
Rail	Rail	k1gMnSc1	Rail
Repair	Repair	k1gMnSc1	Repair
Shop	shop	k1gInSc4	shop
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
14	[number]	k4	14
vozů	vůz	k1gInPc2	vůz
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
kombinovaných	kombinovaný	k2eAgInPc2d1	kombinovaný
vozů	vůz	k1gInPc2	vůz
1	[number]	k4	1
<g/>
.	.	kIx.	.
plus	plus	k1gInSc1	plus
2	[number]	k4	2
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
ÖBB	ÖBB	kA	ÖBB
dodáno	dodat	k5eAaPmNgNnS	dodat
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
ÖBB	ÖBB	kA	ÖBB
dodáno	dodat	k5eAaPmNgNnS	dodat
12	[number]	k4	12
kusů	kus	k1gInPc2	kus
řady	řada	k1gFnSc2	řada
ABmz	ABmz	k1gInSc1	ABmz
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
8	[number]	k4	8
kusů	kus	k1gInPc2	kus
řady	řada	k1gFnSc2	řada
Ampz	Ampz	k1gInSc4	Ampz
18-91	[number]	k4	18-91
a	a	k8xC	a
8	[number]	k4	8
kusů	kus	k1gInPc2	kus
řady	řada	k1gFnSc2	řada
Bmz	Bmz	k1gFnSc1	Bmz
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Vozům	vůz	k1gInPc3	vůz
zůstala	zůstat	k5eAaPmAgFnS	zůstat
původní	původní	k2eAgFnSc1d1	původní
rakouská	rakouský	k2eAgFnSc1d1	rakouská
registrace	registrace	k1gFnSc1	registrace
<g/>
,	,	kIx,	,
změnila	změnit	k5eAaPmAgFnS	změnit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
značka	značka	k1gFnSc1	značka
vlastníka	vlastník	k1gMnSc2	vlastník
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dodané	dodaný	k2eAgInPc1d1	dodaný
vozy	vůz	k1gInPc1	vůz
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
označeny	označit	k5eAaPmNgInP	označit
jako	jako	k8xC	jako
druhá	druhý	k4xOgFnSc1	druhý
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
u	u	k7c2	u
ÖBB	ÖBB	kA	ÖBB
sloužily	sloužit	k5eAaImAgInP	sloužit
vozy	vůz	k1gInPc1	vůz
a	a	k8xC	a
části	část	k1gFnSc2	část
vozů	vůz	k1gInPc2	vůz
řady	řada	k1gFnSc2	řada
A	a	k9	a
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
třída	třída	k1gFnSc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
Bmz	Bmz	k1gFnPc2	Bmz
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
nebyly	být	k5eNaImAgFnP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
rozsáhlejší	rozsáhlý	k2eAgFnSc4d2	rozsáhlejší
modernizaci	modernizace	k1gFnSc4	modernizace
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
vybaveny	vybaven	k2eAgFnPc4d1	vybavena
WiFi	WiF	k1gFnPc4	WiF
signálem	signál	k1gInSc7	signál
<g/>
.	.	kIx.	.
</s>
<s>
Dodané	dodaný	k2eAgInPc1d1	dodaný
vozy	vůz	k1gInPc1	vůz
<g/>
:	:	kIx,	:
12	[number]	k4	12
vozů	vůz	k1gInPc2	vůz
ABmz	ABmza	k1gFnPc2	ABmza
<g/>
,	,	kIx,	,
61	[number]	k4	61
81	[number]	k4	81
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
firmou	firma	k1gFnSc7	firma
SGP	SGP	kA	SGP
Simmering	Simmering	k1gInSc1	Simmering
<g/>
,	,	kIx,	,
podvozky	podvozek	k1gInPc1	podvozek
SGP	SGP	kA	SGP
VS-RIC	VS-RIC	k1gFnSc1	VS-RIC
75	[number]	k4	75
<g/>
,	,	kIx,	,
modernizace	modernizace	k1gFnSc1	modernizace
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc4	některý
vozy	vůz	k1gInPc4	vůz
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oddílové	oddílový	k2eAgNnSc1d1	oddílové
uspořádání	uspořádání	k1gNnSc1	uspořádání
(	(	kIx(	(
<g/>
8	[number]	k4	8
kupé	kupé	k1gNnPc2	kupé
po	po	k7c6	po
6	[number]	k4	6
místech	místo	k1gNnPc6	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
48	[number]	k4	48
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
(	(	kIx(	(
<g/>
4	[number]	k4	4
prostornější	prostorný	k2eAgInSc4d2	prostornější
kupé	kupé	k1gNnSc1	kupé
<g/>
,	,	kIx,	,
4	[number]	k4	4
menší	malý	k2eAgInSc1d2	menší
kupé	kupé	k1gNnSc2	kupé
a	a	k8xC	a
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
přípravna	přípravna	k1gFnSc1	přípravna
občerstvení	občerstvení	k1gNnSc4	občerstvení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pětivozových	pětivozový	k2eAgFnPc6d1	pětivozový
soupravách	souprava	k1gFnPc6	souprava
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
vozy	vůz	k1gInPc4	vůz
řazeny	řazen	k2eAgInPc4d1	řazen
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
jako	jako	k9	jako
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
jako	jako	k9	jako
předposlední	předposlední	k2eAgMnSc1d1	předposlední
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtyřvozových	čtyřvozový	k2eAgFnPc6d1	čtyřvozová
soupravách	souprava	k1gFnPc6	souprava
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
vozy	vůz	k1gInPc1	vůz
řazeny	řazen	k2eAgInPc1d1	řazen
střídavě	střídavě	k6eAd1	střídavě
s	s	k7c7	s
vozy	vůz	k1gInPc7	vůz
Ampz	Ampza	k1gFnPc2	Ampza
<g/>
.	.	kIx.	.
8	[number]	k4	8
vozů	vůz	k1gInPc2	vůz
Ampz	Ampza	k1gFnPc2	Ampza
<g/>
,	,	kIx,	,
61	[number]	k4	61
81	[number]	k4	81
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
,	,	kIx,	,
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
firmou	firma	k1gFnSc7	firma
Jenbacher	Jenbachra	k1gFnPc2	Jenbachra
jako	jako	k8xC	jako
Amz	Amz	k1gFnPc2	Amz
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
71.0	[number]	k4	71.0
<g/>
,	,	kIx,	,
podvozky	podvozek	k1gInPc1	podvozek
FIAT	fiat	k1gInSc1	fiat
Y	Y	kA	Y
0	[number]	k4	0
<g/>
270	[number]	k4	270
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byly	být	k5eAaImAgInP	být
4	[number]	k4	4
oddíly	oddíl	k1gInPc4	oddíl
(	(	kIx(	(
<g/>
kupé	kupé	k1gNnSc2	kupé
<g/>
)	)	kIx)	)
upraveny	upravit	k5eAaPmNgInP	upravit
na	na	k7c4	na
čtyřmístné	čtyřmístný	k2eAgFnPc4d1	čtyřmístná
Business	business	k1gInSc1	business
oddíly	oddíl	k1gInPc1	oddíl
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
16	[number]	k4	16
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgFnSc1d1	zbylá
část	část	k1gFnSc1	část
vozu	vůz	k1gInSc2	vůz
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
velkoprostorový	velkoprostorový	k2eAgInSc4d1	velkoprostorový
oddíl	oddíl	k1gInSc4	oddíl
s	s	k7c7	s
32	[number]	k4	32
místy	místo	k1gNnPc7	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zvenku	zvenku	k6eAd1	zvenku
označeny	označit	k5eAaPmNgInP	označit
názvem	název	k1gInSc7	název
Relax	Relax	k1gInSc1	Relax
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pětivozových	pětivozový	k2eAgFnPc6d1	pětivozový
soupravách	souprava	k1gFnPc6	souprava
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
jako	jako	k9	jako
první	první	k4xOgFnPc1	první
<g/>
,	,	kIx,	,
prostřední	prostřední	k2eAgFnPc1d1	prostřední
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtyřvozových	čtyřvozový	k2eAgFnPc6d1	čtyřvozová
soupravách	souprava	k1gFnPc6	souprava
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
vozy	vůz	k1gInPc1	vůz
řazeny	řazen	k2eAgInPc1d1	řazen
střídavě	střídavě	k6eAd1	střídavě
s	s	k7c7	s
vozy	vůz	k1gInPc7	vůz
ABmz	ABmza	k1gFnPc2	ABmza
<g/>
.	.	kIx.	.
8	[number]	k4	8
vozů	vůz	k1gInPc2	vůz
Bmz	Bmz	k1gFnPc2	Bmz
<g/>
,	,	kIx,	,
61	[number]	k4	61
81	[number]	k4	81
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
vozů	vůz	k1gInPc2	vůz
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
firmou	firma	k1gFnSc7	firma
SGP	SGP	kA	SGP
Graz	Graz	k1gMnSc1	Graz
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
vozů	vůz	k1gInPc2	vůz
v	v	k7c6	v
letech	let	k1gInPc6	let
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
firmou	firma	k1gFnSc7	firma
SGP	SGP	kA	SGP
Simmering	Simmering	k1gInSc1	Simmering
<g/>
,	,	kIx,	,
podvozky	podvozek	k1gInPc1	podvozek
SGP	SGP	kA	SGP
VS-RIC	VS-RIC	k1gFnSc1	VS-RIC
75	[number]	k4	75
<g/>
,	,	kIx,	,
modernizace	modernizace	k1gFnSc1	modernizace
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
oddílové	oddílový	k2eAgNnSc1d1	oddílové
uspořádání	uspořádání	k1gNnSc1	uspořádání
(	(	kIx(	(
<g/>
kupé	kupé	k1gNnSc1	kupé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vozy	vůz	k1gInPc1	vůz
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
rozsáhlejší	rozsáhlý	k2eAgFnSc4d2	rozsáhlejší
modernizaci	modernizace	k1gFnSc4	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
začal	začít	k5eAaPmAgMnS	začít
RegioJet	RegioJet	k1gMnSc1	RegioJet
nasazovat	nasazovat	k5eAaImF	nasazovat
dva	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc4	vůz
typu	typ	k1gInSc2	typ
ASmz	ASmza	k1gFnPc2	ASmza
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
část	část	k1gFnSc4	část
se	s	k7c7	s
4	[number]	k4	4
oddíly	oddíl	k1gInPc4	oddíl
a	a	k8xC	a
velkoprostorovou	velkoprostorový	k2eAgFnSc4d1	velkoprostorová
s	s	k7c7	s
uspořádáním	uspořádání	k1gNnSc7	uspořádání
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
vozům	vůz	k1gInPc3	vůz
Ampz	Ampza	k1gFnPc2	Ampza
však	však	k8xC	však
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
velkoprostorové	velkoprostorový	k2eAgFnSc6d1	velkoprostorová
části	část	k1gFnSc6	část
klasická	klasický	k2eAgNnPc4d1	klasické
pevná	pevný	k2eAgNnPc4d1	pevné
sedadla	sedadlo	k1gNnPc4	sedadlo
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
celých	celý	k2eAgNnPc2d1	celé
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nízká	nízký	k2eAgNnPc4d1	nízké
volná	volný	k2eAgNnPc4d1	volné
křesla	křeslo	k1gNnPc4	křeslo
<g/>
.	.	kIx.	.
iDnes	iDnesa	k1gFnPc2	iDnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
RegioJet	RegioJet	k1gInSc1	RegioJet
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kolem	kolem	k6eAd1	kolem
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
koupil	koupit	k5eAaPmAgMnS	koupit
RegioJet	RegioJet	k1gMnSc1	RegioJet
od	od	k7c2	od
ÖBB	ÖBB	kA	ÖBB
dalších	další	k2eAgFnPc2d1	další
45	[number]	k4	45
vozů	vůz	k1gInPc2	vůz
téže	tenže	k3xDgFnSc2	tenže
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
27	[number]	k4	27
jsou	být	k5eAaImIp3nP	být
vozy	vůz	k1gInPc1	vůz
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
p	p	k?	p
proloužení	proloužení	k1gNnSc3	proloužení
stávajících	stávající	k2eAgInPc2d1	stávající
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
špičkách	špička	k1gFnPc6	špička
<g/>
,	,	kIx,	,
zásobu	zásoba	k1gFnSc4	zásoba
vozů	vůz	k1gInPc2	vůz
chce	chtít	k5eAaImIp3nS	chtít
využít	využít	k5eAaPmF	využít
i	i	k9	i
k	k	k7c3	k
nabídkám	nabídka	k1gFnPc3	nabídka
převzetí	převzetí	k1gNnSc2	převzetí
dosud	dosud	k6eAd1	dosud
dotovaných	dotovaný	k2eAgInPc2d1	dotovaný
spojů	spoj	k1gInPc2	spoj
objednávaných	objednávaný	k2eAgInPc2d1	objednávaný
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2010	[number]	k4	2010
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
výběrové	výběrový	k2eAgNnSc4d1	výběrové
řízení	řízení	k1gNnSc4	řízení
na	na	k7c4	na
50	[number]	k4	50
nových	nový	k2eAgInPc2d1	nový
vozů	vůz	k1gInPc2	vůz
za	za	k7c4	za
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
€	€	k?	€
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Valer	Valer	k1gMnSc1	Valer
Blidar	Blidar	k1gMnSc1	Blidar
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
rumunské	rumunský	k2eAgFnSc2d1	rumunská
společnosti	společnost	k1gFnSc2	společnost
ASTRA	astra	k1gFnSc1	astra
Vagoane	Vagoan	k1gMnSc5	Vagoan
Călători	Călător	k1gMnSc5	Călător
<g/>
,	,	kIx,	,
sdělil	sdělit	k5eAaPmAgMnS	sdělit
v	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
rumunskému	rumunský	k2eAgInSc3d1	rumunský
ekonomickému	ekonomický	k2eAgInSc3d1	ekonomický
listu	list	k1gInSc3	list
Ziarul	Ziarul	k1gInSc4	Ziarul
Financiar	Financiar	k1gInSc4	Financiar
Daily	Daila	k1gFnSc2	Daila
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
má	mít	k5eAaImIp3nS	mít
nejmenovanému	jmenovaný	k2eNgMnSc3d1	nejmenovaný
odběrateli	odběratel	k1gMnSc3	odběratel
dodat	dodat	k5eAaPmF	dodat
50	[number]	k4	50
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
smlouva	smlouva	k1gFnSc1	smlouva
je	být	k5eAaImIp3nS	být
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
podpisem	podpis	k1gInSc7	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Rumunský	rumunský	k2eAgInSc1d1	rumunský
server	server	k1gInSc1	server
Financiarul	Financiarula	k1gFnPc2	Financiarula
<g/>
.	.	kIx.	.
<g/>
ro	ro	k?	ro
zprávu	zpráva	k1gFnSc4	zpráva
podal	podat	k5eAaPmAgMnS	podat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
50	[number]	k4	50
vozů	vůz	k1gInPc2	vůz
od	od	k7c2	od
nejmenované	jmenovaný	k2eNgFnSc2d1	nejmenovaná
české	český	k2eAgFnSc2d1	Česká
firmy	firma	k1gFnSc2	firma
již	již	k6eAd1	již
získala	získat	k5eAaPmAgFnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Radim	Radim	k1gMnSc1	Radim
Jančura	Jančur	k1gMnSc4	Jančur
však	však	k9	však
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2010	[number]	k4	2010
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
smlouva	smlouva	k1gFnSc1	smlouva
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
AVC	AVC	kA	AVC
zakázku	zakázka	k1gFnSc4	zakázka
získala	získat	k5eAaPmAgFnS	získat
nebo	nebo	k8xC	nebo
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
před	před	k7c7	před
podepsáním	podepsání	k1gNnSc7	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
záležitost	záležitost	k1gFnSc4	záležitost
komentovat	komentovat	k5eAaBmF	komentovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
oznámí	oznámit	k5eAaPmIp3nS	oznámit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
či	či	k8xC	či
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
Jančura	Jančura	k1gFnSc1	Jančura
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
užívat	užívat	k5eAaImF	užívat
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
části	část	k1gFnSc2	část
též	též	k9	též
operativně	operativně	k6eAd1	operativně
pronajaté	pronajatý	k2eAgInPc4d1	pronajatý
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nestihne	stihnout	k5eNaPmIp3nS	stihnout
do	do	k7c2	do
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
získat	získat	k5eAaPmF	získat
dostatek	dostatek	k1gInSc4	dostatek
vlastních	vlastní	k2eAgInPc2d1	vlastní
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2011	[number]	k4	2011
už	už	k9	už
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
hotové	hotový	k2eAgInPc1d1	hotový
všechny	všechen	k3xTgInPc1	všechen
vlastní	vlastní	k2eAgInPc1d1	vlastní
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pronajaté	pronajatý	k2eAgInPc1d1	pronajatý
vozy	vůz	k1gInPc1	vůz
budou	být	k5eAaImBp3nP	být
i	i	k9	i
poté	poté	k6eAd1	poté
vypomáhat	vypomáhat	k5eAaImF	vypomáhat
ve	v	k7c6	v
špičkách	špička	k1gFnPc6	špička
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
sliboval	slibovat	k5eAaImAgMnS	slibovat
že	že	k8xS	že
výrobce	výrobce	k1gMnPc4	výrobce
nových	nový	k2eAgInPc2d1	nový
vozů	vůz	k1gInPc2	vůz
odtajní	odtajnit	k5eAaPmIp3nS	odtajnit
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
neprozradil	prozradit	k5eNaPmAgMnS	prozradit
jej	on	k3xPp3gMnSc4	on
ani	ani	k8xC	ani
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
důvodem	důvod	k1gInSc7	důvod
zatajování	zatajování	k1gNnPc1	zatajování
byly	být	k5eAaImAgFnP	být
případné	případný	k2eAgFnPc4d1	případná
potíže	potíž	k1gFnPc4	potíž
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
projekt	projekt	k1gInSc1	projekt
představit	představit	k5eAaPmF	představit
komplexně	komplexně	k6eAd1	komplexně
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
bude	být	k5eAaImBp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
první	první	k4xOgFnSc1	první
souprava	souprava	k1gFnSc1	souprava
–	–	k?	–
celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
objednáno	objednán	k2eAgNnSc4d1	objednáno
6	[number]	k4	6
pětivozových	pětivozový	k2eAgFnPc2d1	pětivozový
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
stále	stále	k6eAd1	stále
nebyly	být	k5eNaImAgFnP	být
bližší	blízký	k2eAgFnPc4d2	bližší
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
výrobci	výrobce	k1gMnSc6	výrobce
a	a	k8xC	a
typu	typ	k1gInSc3	typ
vlastních	vlastní	k2eAgInPc2d1	vlastní
vozů	vůz	k1gInPc2	vůz
ani	ani	k8xC	ani
o	o	k7c6	o
pronajatých	pronajatý	k2eAgInPc6d1	pronajatý
vozech	vůz	k1gInPc6	vůz
známy	znám	k2eAgInPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Nasazení	nasazení	k1gNnSc1	nasazení
nových	nový	k2eAgInPc2d1	nový
vozů	vůz	k1gInPc2	vůz
je	být	k5eAaImIp3nS	být
předpokládáno	předpokládat	k5eAaImNgNnS	předpokládat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
všechny	všechen	k3xTgMnPc4	všechen
zabezpečené	zabezpečený	k2eAgMnPc4d1	zabezpečený
kamerovým	kamerový	k2eAgInSc7d1	kamerový
bezpečnostním	bezpečnostní	k2eAgInSc7d1	bezpečnostní
systém	systém	k1gInSc4	systém
CCTV	CCTV	kA	CCTV
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
provedeních	provedení	k1gNnPc6	provedení
<g/>
:	:	kIx,	:
vozy	vůz	k1gInPc1	vůz
Premium	Premium	k1gNnSc1	Premium
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
velkoprostorové	velkoprostorový	k2eAgFnSc6d1	velkoprostorová
s	s	k7c7	s
velkými	velká	k1gFnPc7	velká
<g/>
,	,	kIx,	,
pohodlnými	pohodlný	k2eAgNnPc7d1	pohodlné
křesly	křeslo	k1gNnPc7	křeslo
<g/>
.	.	kIx.	.
vozy	vůz	k1gInPc4	vůz
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
prostoru	prostor	k1gInSc2	prostor
než	než	k8xS	než
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
dotykové	dotykový	k2eAgFnPc1d1	dotyková
obrazovky	obrazovka	k1gFnPc1	obrazovka
v	v	k7c6	v
opěradlech	opěradlo	k1gNnPc6	opěradlo
sedaček	sedačka	k1gFnPc2	sedačka
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
použití	použití	k1gNnSc2	použití
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc1	sledování
filmů	film	k1gInPc2	film
a	a	k8xC	a
poslechu	poslech	k1gInSc2	poslech
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
kuchyňky	kuchyňka	k1gFnPc4	kuchyňka
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
přípravu	příprava	k1gFnSc4	příprava
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
.	.	kIx.	.
vozy	vůz	k1gInPc7	vůz
typu	typ	k1gInSc2	typ
Restaurant	restaurant	k1gInSc4	restaurant
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
distribuovat	distribuovat	k5eAaBmF	distribuovat
kvalitní	kvalitní	k2eAgNnPc4d1	kvalitní
teplá	teplý	k2eAgNnPc4d1	teplé
jídla	jídlo	k1gNnPc4	jídlo
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
soupravě	souprava	k1gFnSc6	souprava
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
bistro	bistro	k1gNnSc1	bistro
<g/>
.	.	kIx.	.
vozy	vůz	k1gInPc4	vůz
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
typu	typ	k1gInSc2	typ
Zábava	zábava	k1gFnSc1	zábava
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
dotykové	dotykový	k2eAgFnPc4d1	dotyková
obrazovky	obrazovka	k1gFnPc4	obrazovka
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
prohlížení	prohlížení	k1gNnSc2	prohlížení
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc1	sledování
filmů	film	k1gInPc2	film
a	a	k8xC	a
poslechu	poslech	k1gInSc2	poslech
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Velkoprostorové	velkoprostorový	k2eAgFnPc1d1	velkoprostorová
toalety	toaleta	k1gFnPc1	toaleta
<g/>
,	,	kIx,	,
kuchyňka	kuchyňka	k1gFnSc1	kuchyňka
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
.	.	kIx.	.
vozy	vůz	k1gInPc4	vůz
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
typu	typ	k1gInSc2	typ
Pro	pro	k7c4	pro
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Plošina	plošina	k1gFnSc1	plošina
a	a	k8xC	a
dvě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
vyhrazená	vyhrazený	k2eAgNnPc1d1	vyhrazené
pro	pro	k7c4	pro
imobilní	imobilní	k2eAgMnPc4d1	imobilní
cestující	cestující	k1gMnPc4	cestující
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
doprovod	doprovod	k1gInSc1	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
odložit	odložit	k5eAaPmF	odložit
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
lyže	lyže	k1gFnSc2	lyže
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
palubní	palubní	k2eAgInSc1d1	palubní
personál	personál	k1gInSc1	personál
zabezpečí	zabezpečit	k5eAaPmIp3nS	zabezpečit
zámkem	zámek	k1gInSc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Uzamykatelný	uzamykatelný	k2eAgInSc1d1	uzamykatelný
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
běžná	běžný	k2eAgNnPc4d1	běžné
zavazadla	zavazadlo	k1gNnPc4	zavazadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
koutek	koutek	k1gInSc1	koutek
s	s	k7c7	s
hračkami	hračka	k1gFnPc7	hračka
a	a	k8xC	a
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
sledování	sledování	k1gNnSc2	sledování
dětských	dětský	k2eAgInPc2d1	dětský
pořadů	pořad	k1gInPc2	pořad
na	na	k7c6	na
videu	video	k1gNnSc6	video
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc1	dva
kupé	kupé	k1gNnPc1	kupé
budou	být	k5eAaImBp3nP	být
vyčleněna	vyčlenit	k5eAaPmNgNnP	vyčlenit
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
možnost	možnost	k1gFnSc1	možnost
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
obrazovkách	obrazovka	k1gFnPc6	obrazovka
sledovat	sledovat	k5eAaImF	sledovat
pořady	pořad	k1gInPc4	pořad
pro	pro	k7c4	pro
větší	veliký	k2eAgFnPc4d2	veliký
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
kupé	kupé	k1gNnPc4	kupé
bude	být	k5eAaImBp3nS	být
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaImF	využívat
jako	jako	k8xS	jako
jednací	jednací	k2eAgFnSc1d1	jednací
místnost	místnost	k1gFnSc1	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
možnost	možnost	k1gFnSc1	možnost
připojení	připojení	k1gNnSc2	připojení
notebooku	notebook	k1gInSc2	notebook
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
monitory	monitor	k1gInPc4	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
pohodlí	pohodlí	k1gNnSc4	pohodlí
všech	všecek	k3xTgMnPc2	všecek
cestujících	cestující	k1gMnPc2	cestující
odhlučněn	odhlučněn	k2eAgInSc1d1	odhlučněn
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nové	nový	k2eAgInPc1d1	nový
vozy	vůz	k1gInPc1	vůz
mají	mít	k5eAaImIp3nP	mít
postupně	postupně	k6eAd1	postupně
doplnit	doplnit	k5eAaPmF	doplnit
vlak	vlak	k1gInSc4	vlak
na	na	k7c4	na
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
Jančurových	Jančurův	k2eAgNnPc2d1	Jančurovo
sdělení	sdělení	k1gNnPc2	sdělení
vyvodila	vyvodit	k5eAaBmAgFnS	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgInPc4	žádný
nové	nový	k2eAgInPc4d1	nový
vagóny	vagón	k1gInPc4	vagón
pro	pro	k7c4	pro
trasu	trasa	k1gFnSc4	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Havířov	Havířov	k1gInSc1	Havířov
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
dodány	dodat	k5eAaPmNgInP	dodat
nebudou	být	k5eNaImBp3nP	být
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
vystačí	vystačit	k5eAaBmIp3nS	vystačit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
rekonstruovanými	rekonstruovaný	k2eAgInPc7d1	rekonstruovaný
rakouskými	rakouský	k2eAgInPc7d1	rakouský
vozy	vůz	k1gInPc7	vůz
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc6	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
trať	trať	k1gFnSc4	trať
čtyřicet	čtyřicet	k4xCc4	čtyřicet
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
Jančura	Jančura	k1gFnSc1	Jančura
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
73	[number]	k4	73
vagónů	vagón	k1gInPc2	vagón
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
případné	případný	k2eAgNnSc4d1	případné
další	další	k1gNnSc4	další
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
představila	představit	k5eAaPmAgFnS	představit
slovenská	slovenský	k2eAgFnSc1d1	slovenská
firma	firma	k1gFnSc1	firma
Molpir	Molpir	k1gMnSc1	Molpir
<g/>
,	,	kIx,	,
dodavatel	dodavatel	k1gMnSc1	dodavatel
palubní	palubní	k2eAgFnSc2d1	palubní
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
na	na	k7c6	na
veletrhu	veletrh	k1gInSc6	veletrh
Innotrans	Innotransa	k1gFnPc2	Innotransa
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
velkoprostorový	velkoprostorový	k2eAgInSc4d1	velkoprostorový
vůz	vůz	k1gInSc4	vůz
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
z	z	k7c2	z
rumunské	rumunský	k2eAgFnSc2d1	rumunská
vagónky	vagónek	k1gInPc1	vagónek
Astra	astra	k1gFnSc1	astra
Vagone	vagon	k1gInSc5	vagon
Calagori	Calagori	k1gNnSc5	Calagori
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
interiérovým	interiérový	k2eAgNnSc7d1	interiérové
studiem	studio	k1gNnSc7	studio
Flagu-Coplass	Flagu-Coplassa	k1gFnPc2	Flagu-Coplassa
a	a	k8xC	a
architektem	architekt	k1gMnSc7	architekt
Patrikem	Patrik	k1gMnSc7	Patrik
Kotasem	Kotas	k1gMnSc7	Kotas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
jsou	být	k5eAaImIp3nP	být
sedačky	sedačka	k1gFnPc1	sedačka
od	od	k7c2	od
německé	německý	k2eAgFnSc2d1	německá
firmy	firma	k1gFnSc2	firma
Grammer	Grammra	k1gFnPc2	Grammra
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
sedačce	sedačka	k1gFnSc6	sedačka
je	být	k5eAaImIp3nS	být
obrazovka	obrazovka	k1gFnSc1	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
vůz	vůz	k1gInSc1	vůz
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
zhruba	zhruba	k6eAd1	zhruba
milión	milión	k4xCgInSc4	milión
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jej	on	k3xPp3gMnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
ReigoJet	ReigoJet	k1gInSc1	ReigoJet
poprvé	poprvé	k6eAd1	poprvé
představit	představit	k5eAaPmF	představit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
říjnovém	říjnový	k2eAgInSc6d1	říjnový
týdnu	týden	k1gInSc6	týden
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
jezdit	jezdit	k5eAaImF	jezdit
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Žilinou	Žilina	k1gFnSc7	Žilina
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
rumunské	rumunský	k2eAgFnSc2d1	rumunská
vagónky	vagónek	k1gInPc7	vagónek
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
objednáno	objednán	k2eAgNnSc1d1	objednáno
10	[number]	k4	10
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
během	během	k7c2	během
dalším	další	k2eAgMnPc3d1	další
měsíců	měsíc	k1gInPc2	měsíc
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
postupně	postupně	k6eAd1	postupně
nasazovány	nasazovat	k5eAaImNgInP	nasazovat
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
špatným	špatný	k2eAgFnPc3d1	špatná
zkušenostem	zkušenost	k1gFnPc3	zkušenost
z	z	k7c2	z
českým	český	k2eAgInSc7d1	český
Drážním	drážní	k2eAgInSc7d1	drážní
úřadem	úřad	k1gInSc7	úřad
si	se	k3xPyFc3	se
RegioJet	RegioJet	k1gInSc1	RegioJet
chce	chtít	k5eAaImIp3nS	chtít
vozy	vůz	k1gInPc4	vůz
nechat	nechat	k5eAaPmF	nechat
registrovat	registrovat	k5eAaBmF	registrovat
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
oznámily	oznámit	k5eAaPmAgFnP	oznámit
Švýcarské	švýcarský	k2eAgFnPc1d1	švýcarská
spolkové	spolkový	k2eAgFnPc1d1	spolková
dráhy	dráha	k1gFnPc1	dráha
(	(	kIx(	(
<g/>
SBB	SBB	kA	SBB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
společnost	společnost	k1gFnSc1	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
koupila	koupit	k5eAaPmAgFnS	koupit
12	[number]	k4	12
vozů	vůz	k1gInPc2	vůz
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
vozy	vůz	k1gInPc4	vůz
typového	typový	k2eAgNnSc2d1	typové
označení	označení	k1gNnSc2	označení
Am	Am	k1gFnSc2	Am
<g/>
61	[number]	k4	61
o	o	k7c6	o
maximální	maximální	k2eAgFnSc6d1	maximální
rychlosti	rychlost	k1gFnSc6	rychlost
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Prodejní	prodejní	k2eAgFnSc4d1	prodejní
cenu	cena	k1gFnSc4	cena
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
nezveřejnila	zveřejnit	k5eNaPmAgFnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
nasazeny	nasadit	k5eAaPmNgInP	nasadit
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
RegioJetu	RegioJet	k1gInSc2	RegioJet
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
nasazením	nasazení	k1gNnSc7	nasazení
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
bude	být	k5eAaImBp3nS	být
kompletně	kompletně	k6eAd1	kompletně
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
interiér	interiér	k1gInSc1	interiér
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
vozy	vůz	k1gInPc1	vůz
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
pomoci	pomoct	k5eAaPmF	pomoct
zvýšit	zvýšit	k5eAaPmF	zvýšit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
počet	počet	k1gInSc4	počet
7	[number]	k4	7
párů	pár	k1gInPc2	pár
spojů	spoj	k1gInPc2	spoj
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
9	[number]	k4	9
uvedených	uvedený	k2eAgFnPc2d1	uvedená
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
vozidla	vozidlo	k1gNnPc1	vozidlo
mají	mít	k5eAaImIp3nP	mít
korporátní	korporátní	k2eAgInSc4d1	korporátní
žlutočerný	žlutočerný	k2eAgInSc4d1	žlutočerný
vnější	vnější	k2eAgInSc4d1	vnější
nátěr	nátěr	k1gInSc4	nátěr
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
vozidla	vozidlo	k1gNnPc1	vozidlo
opatřená	opatřený	k2eAgNnPc1d1	opatřené
pouze	pouze	k6eAd1	pouze
polepy	polep	k1gInPc7	polep
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
červená	červený	k2eAgFnSc1d1	červená
střecha	střecha	k1gFnSc1	střecha
od	od	k7c2	od
ÖBB	ÖBB	kA	ÖBB
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
vozidlům	vozidlo	k1gNnPc3	vozidlo
jiných	jiný	k1gMnPc2	jiný
dopravců	dopravce	k1gMnPc2	dopravce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nenápadné	nápadný	k2eNgNnSc4d1	nenápadné
typové	typový	k2eAgNnSc4d1	typové
a	a	k8xC	a
registrační	registrační	k2eAgNnSc4d1	registrační
označení	označení	k1gNnSc4	označení
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgNnSc1d1	výrazné
je	být	k5eAaImIp3nS	být
především	především	k9	především
modročervené	modročervený	k2eAgNnSc4d1	modročervené
logo	logo	k1gNnSc4	logo
dopravce	dopravce	k1gMnSc1	dopravce
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
větším	veliký	k2eAgInSc7d2	veliký
nápisem	nápis	k1gInSc7	nápis
REGIOJET	REGIOJET	kA	REGIOJET
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
menším	malý	k2eAgInSc7d2	menší
nápisem	nápis	k1gInSc7	nápis
STUDENT	student	k1gMnSc1	student
AGENCY	AGENCY	kA	AGENCY
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
vstupních	vstupní	k2eAgFnPc2d1	vstupní
dveří	dveře	k1gFnPc2	dveře
vozů	vůz	k1gInPc2	vůz
je	být	k5eAaImIp3nS	být
výrazné	výrazný	k2eAgNnSc4d1	výrazné
označení	označení	k1gNnSc4	označení
2	[number]	k4	2
<g/>
.	.	kIx.	.
vozové	vozový	k2eAgFnSc2d1	vozová
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
drobnější	drobný	k2eAgInPc1d2	drobnější
symboly	symbol	k1gInPc1	symbol
označující	označující	k2eAgInPc1d1	označující
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůz	vůz	k1gInSc1	vůz
je	být	k5eAaImIp3nS	být
nekuřácký	kuřácký	k2eNgInSc1d1	nekuřácký
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vyhrazená	vyhrazený	k2eAgNnPc4d1	vyhrazené
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
invalidy	invalid	k1gInPc4	invalid
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
dopravce	dopravce	k1gMnSc1	dopravce
cestovní	cestovní	k2eAgFnSc2d1	cestovní
třídy	třída	k1gFnSc2	třída
nerozlišoval	rozlišovat	k5eNaImAgMnS	rozlišovat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zavedl	zavést	k5eAaPmAgInS	zavést
na	na	k7c6	na
komerčních	komerční	k2eAgInPc6d1	komerční
linkových	linkový	k2eAgInPc6d1	linkový
spojích	spoj	k1gInPc6	spoj
čtyři	čtyři	k4xCgFnPc4	čtyři
třídy	třída	k1gFnPc4	třída
<g/>
:	:	kIx,	:
Low	Low	k1gMnSc1	Low
cost	cost	k1gMnSc1	cost
<g/>
:	:	kIx,	:
vůz	vůz	k1gInSc1	vůz
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zásuvek	zásuvka	k1gFnPc2	zásuvka
<g/>
,	,	kIx,	,
dopravce	dopravce	k1gMnSc1	dopravce
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
balenou	balený	k2eAgFnSc4d1	balená
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
denní	denní	k2eAgInSc4d1	denní
tisk	tisk	k1gInSc4	tisk
Standard	standard	k1gInSc1	standard
<g/>
:	:	kIx,	:
kupé	kupé	k1gNnSc1	kupé
pro	pro	k7c4	pro
6	[number]	k4	6
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
dětského	dětský	k2eAgInSc2d1	dětský
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
tichého	tichý	k2eAgNnSc2d1	tiché
kupé	kupé	k1gNnSc2	kupé
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
vozu	vůz	k1gInSc2	vůz
Astra	astra	k1gFnSc1	astra
s	s	k7c7	s
multimediálním	multimediální	k2eAgInSc7d1	multimediální
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
obrazovkách	obrazovka	k1gFnPc6	obrazovka
Relax	Relax	k1gInSc1	Relax
<g/>
,	,	kIx,	,
vůz	vůz	k1gInSc1	vůz
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
prostorem	prostor	k1gInSc7	prostor
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
samostatného	samostatný	k2eAgNnSc2d1	samostatné
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc4d1	velký
stolky	stolek	k1gInPc4	stolek
Business	business	k1gInSc1	business
<g/>
,	,	kIx,	,
kupé	kupé	k1gNnPc1	kupé
pro	pro	k7c4	pro
4	[number]	k4	4
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
tichého	tichý	k2eAgNnSc2d1	tiché
kupé	kupé	k1gNnSc2	kupé
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
doplňková	doplňkový	k2eAgFnSc1d1	doplňková
nabídka	nabídka	k1gFnSc1	nabídka
(	(	kIx(	(
<g/>
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
časopisy	časopis	k1gInPc1	časopis
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
čaje	čaj	k1gInPc1	čaj
<g/>
,	,	kIx,	,
pomerančový	pomerančový	k2eAgInSc4d1	pomerančový
džus	džus	k1gInSc4	džus
<g/>
)	)	kIx)	)
Kromě	kromě	k7c2	kromě
třídy	třída	k1gFnSc2	třída
Low	Low	k1gMnPc1	Low
cost	costa	k1gFnPc2	costa
jsou	být	k5eAaImIp3nP	být
všech	všecek	k3xTgFnPc6	všecek
čtyřech	čtyři	k4xCgFnPc6	čtyři
třídách	třída	k1gFnPc6	třída
dostupné	dostupný	k2eAgFnPc1d1	dostupná
zásuvky	zásuvka	k1gFnPc1	zásuvka
na	na	k7c4	na
230	[number]	k4	230
V.	V.	kA	V.
Sedačky	sedačka	k1gFnPc1	sedačka
jsou	být	k5eAaImIp3nP	být
kožené	kožený	k2eAgFnPc1d1	kožená
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
Standard	standard	k1gInSc1	standard
též	též	k6eAd1	též
plyšové	plyšový	k2eAgFnSc2d1	plyšová
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
vybraných	vybraný	k2eAgInPc2d1	vybraný
typů	typ	k1gInPc2	typ
vozidel	vozidlo	k1gNnPc2	vozidlo
provozovaných	provozovaný	k2eAgNnPc2d1	provozované
RegioJetem	RegioJet	k1gInSc7	RegioJet
<g/>
:	:	kIx,	:
Konkurence	konkurence	k1gFnSc1	konkurence
dopravců	dopravce	k1gMnPc2	dopravce
v	v	k7c6	v
osobní	osobní	k2eAgFnSc6d1	osobní
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
RegioJet	RegioJet	k1gInSc4	RegioJet
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
web	web	k1gInSc1	web
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
RegioJet	RegioJet	k1gMnSc1	RegioJet
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
web	web	k1gInSc1	web
slovenské	slovenský	k2eAgFnSc2d1	slovenská
společnosti	společnost	k1gFnSc2	společnost
Ostře	ostro	k6eAd1	ostro
sledované	sledovaný	k2eAgInPc1d1	sledovaný
žluté	žlutý	k2eAgInPc1d1	žlutý
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
profil	profil	k1gInSc4	profil
společnosti	společnost	k1gFnSc2	společnost
@	@	kIx~	@
<g/>
RegioJet	RegioJet	k1gMnSc1	RegioJet
<g/>
,	,	kIx,	,
Twitter	Twitter	k1gMnSc1	Twitter
profil	profil	k1gInSc4	profil
společnosti	společnost	k1gFnSc2	společnost
RegioJet	RegioJet	k1gInSc1	RegioJet
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
plus	plus	k1gInSc1	plus
profil	profil	k1gInSc4	profil
společnosti	společnost	k1gFnSc2	společnost
7	[number]	k4	7
neue	uat	k5eNaPmIp3nS	uat
Eisenbahnunternehmen	Eisenbahnunternehmen	k2eAgInSc4d1	Eisenbahnunternehmen
Open	Open	k1gInSc4	Open
Access	Accessa	k1gFnPc2	Accessa
2013	[number]	k4	2013
–	–	k?	–
Mediarail	Mediaraila	k1gFnPc2	Mediaraila
<g/>
.	.	kIx.	.
<g/>
be	be	k?	be
Databáze	databáze	k1gFnSc2	databáze
vozů	vůz	k1gInPc2	vůz
RegioJet	RegioJet	k1gInSc1	RegioJet
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Jana	Jan	k1gMnSc2	Jan
Mácy	Máca	k1gMnSc2	Máca
</s>
