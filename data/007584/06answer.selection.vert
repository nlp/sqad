<s>
Stoprocentním	stoprocentní	k2eAgMnSc7d1	stoprocentní
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
Radimem	Radim	k1gMnSc7	Radim
Jančurou	Jančura	k1gFnSc7	Jančura
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
nabídkách	nabídka	k1gFnPc6	nabídka
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
pobočkou	pobočka	k1gFnSc7	pobočka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
firmy	firma	k1gFnSc2	firma
Keolis	Keolis	k1gFnSc2	Keolis
<g/>
.	.	kIx.	.
</s>
