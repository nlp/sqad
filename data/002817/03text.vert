<s>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vančury	Vančura	k1gMnSc2	Vančura
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgMnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Pyramida	pyramida	k1gFnSc1	pyramida
jakožto	jakožto	k8xS	jakožto
její	její	k3xOp3gMnSc1	její
15	[number]	k4	15
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
baladickou	baladický	k2eAgFnSc4d1	baladická
prózu	próza	k1gFnSc4	próza
<g/>
,	,	kIx,	,
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
Jiřímu	Jiří	k1gMnSc3	Jiří
Mahenovi	Mahen	k1gMnSc3	Mahen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
Vančurovým	Vančurův	k2eAgMnSc7d1	Vančurův
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
do	do	k7c2	do
kraje	kraj	k1gInSc2	kraj
mladoboleslavského	mladoboleslavský	k2eAgInSc2d1	mladoboleslavský
<g/>
,	,	kIx,	,
do	do	k7c2	do
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
maje	mít	k5eAaImSgMnS	mít
ukrutné	ukrutný	k2eAgFnPc4d1	ukrutná
potíže	potíž	k1gFnPc4	potíž
se	s	k7c7	s
šlechtici	šlechtic	k1gMnPc7	šlechtic
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
vedli	vést	k5eAaImAgMnP	vést
doslova	doslova	k6eAd1	doslova
zlodějsky	zlodějsky	k6eAd1	zlodějsky
<g/>
,	,	kIx,	,
a	a	k8xC	a
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgMnSc1d2	horší
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prolévali	prolévat	k5eAaImAgMnP	prolévat
krev	krev	k1gFnSc4	krev
málem	málem	k6eAd1	málem
se	se	k3xPyFc4	se
chechtajíce	chechtat	k5eAaImSgFnP	chechtat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
díky	dík	k1gInPc1	dík
této	tento	k3xDgFnSc2	tento
časové	časový	k2eAgFnSc2d1	časová
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
neurčitosti	neurčitost	k1gFnSc2	neurčitost
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
Marketu	market	k1gInSc2	market
Lazarovou	Lazarová	k1gFnSc7	Lazarová
za	za	k7c4	za
román	román	k1gInSc4	román
historický	historický	k2eAgInSc4d1	historický
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
scottovském	scottovský	k2eAgNnSc6d1	scottovský
či	či	k8xC	či
jiráskovském	jiráskovský	k2eAgMnSc6d1	jiráskovský
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
prokazatelně	prokazatelně	k6eAd1	prokazatelně
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
historiků	historik	k1gMnPc2	historik
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nevystupují	vystupovat	k5eNaImIp3nP	vystupovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
historické	historický	k2eAgFnSc6d1	historická
osobnosti	osobnost	k1gFnSc6	osobnost
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
nepodřizuje	podřizovat	k5eNaImIp3nS	podřizovat
dobovým	dobový	k2eAgFnPc3d1	dobová
reáliím	reálie	k1gFnPc3	reálie
<g/>
.	.	kIx.	.
</s>
<s>
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
zachycení	zachycení	k1gNnSc4	zachycení
historie	historie	k1gFnSc2	historie
příliš	příliš	k6eAd1	příliš
nevítá	vítat	k5eNaImIp3nS	vítat
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zmýlil	zmýlit	k5eAaPmAgMnS	zmýlit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
však	však	k9	však
notně	notně	k6eAd1	notně
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
tuto	tento	k3xDgFnSc4	tento
raubířskou	raubířský	k2eAgFnSc4d1	raubířská
dobrodružnost	dobrodružnost	k1gFnSc4	dobrodružnost
chtěl	chtít	k5eAaImAgMnS	chtít
jakkoliv	jakkoliv	k6eAd1	jakkoliv
uvést	uvést	k5eAaPmF	uvést
v	v	k7c4	v
blízkost	blízkost	k1gFnSc4	blízkost
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
odvozujících	odvozující	k2eAgInPc2d1	odvozující
se	se	k3xPyFc4	se
z	z	k7c2	z
ušlechtilého	ušlechtilý	k2eAgInSc2d1	ušlechtilý
romantismu	romantismus	k1gInSc2	romantismus
scottovského	scottovský	k2eAgInSc2d1	scottovský
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
obrací	obracet	k5eAaImIp3nP	obracet
se	se	k3xPyFc4	se
zády	záda	k1gNnPc7	záda
ke	k	k7c3	k
kulturní	kulturní	k2eAgFnSc3d1	kulturní
archeologii	archeologie	k1gFnSc3	archeologie
a	a	k8xC	a
nechce	chtít	k5eNaImIp3nS	chtít
pranic	pranic	k6eAd1	pranic
věděti	vědět	k5eAaImF	vědět
o	o	k7c6	o
ideovém	ideový	k2eAgNnSc6d1	ideové
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
o	o	k7c6	o
hlubším	hluboký	k2eAgInSc6d2	hlubší
smyslu	smysl	k1gInSc6	smysl
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nanáší	nanášet	k5eAaImIp3nP	nanášet
až	až	k9	až
pastosně	pastosně	k6eAd1	pastosně
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Jaké	jaký	k3yRgNnSc1	jaký
potom	potom	k6eAd1	potom
starožitnictví	starožitnictví	k1gNnSc1	starožitnictví
bez	bez	k7c2	bez
chronologie	chronologie	k1gFnSc2	chronologie
<g/>
?	?	kIx.	?
</s>
<s>
Nač	nač	k6eAd1	nač
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
kroje	kroj	k1gInPc4	kroj
<g/>
,	,	kIx,	,
mravy	mrav	k1gInPc4	mrav
a	a	k8xC	a
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
,	,	kIx,	,
nedbá	dbát	k5eNaImIp3nS	dbát
<g/>
-li	i	k?	-li
spisovatel	spisovatel	k1gMnSc1	spisovatel
schválně	schválně	k6eAd1	schválně
o	o	k7c6	o
určení	určení	k1gNnSc6	určení
století	století	k1gNnSc2	století
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Zde	zde	k6eAd1	zde
v	v	k7c6	v
prudkém	prudký	k2eAgInSc6d1	prudký
sledu	sled	k1gInSc6	sled
<g/>
,	,	kIx,	,
ráz	ráz	k1gInSc4	ráz
na	na	k7c4	na
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
s	s	k7c7	s
opětovným	opětovný	k2eAgNnSc7d1	opětovné
překvapením	překvapení	k1gNnSc7	překvapení
a	a	k8xC	a
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
bez	bez	k7c2	bez
logiky	logika	k1gFnSc2	logika
se	se	k3xPyFc4	se
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
děje	děj	k1gInPc1	děj
Markety	market	k1gInPc4	market
Lazarové	Lazarová	k1gFnSc3	Lazarová
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nimiž	jenž	k3xRgInPc7	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ani	ani	k9	ani
inteligence	inteligence	k1gFnSc1	inteligence
nejúporněji	úporně	k6eAd3	úporně
slídící	slídící	k2eAgFnSc1d1	slídící
nedopátrala	dopátrat	k5eNaPmAgFnS	dopátrat
skladných	skladný	k2eAgFnPc2d1	skladná
idejí	idea	k1gFnPc2	idea
<g/>
,	,	kIx,	,
hýbajících	hýbající	k2eAgInPc2d1	hýbající
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Nebojuje	bojovat	k5eNaImIp3nS	bojovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
a	a	k8xC	a
nevraždí	vraždit	k5eNaImIp3nS	vraždit
ani	ani	k8xC	ani
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
víry	víra	k1gFnSc2	víra
ani	ani	k8xC	ani
za	za	k7c4	za
národnost	národnost	k1gFnSc4	národnost
<g/>
;	;	kIx,	;
lapkové	lapka	k1gMnPc1	lapka
<g/>
,	,	kIx,	,
rytíři	rytíř	k1gMnPc1	rytíř
a	a	k8xC	a
královští	královský	k2eAgMnPc1d1	královský
žoldnéři	žoldnér	k1gMnPc1	žoldnér
nestojí	stát	k5eNaImIp3nP	stát
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
stavů	stav	k1gInPc2	stav
nebo	nebo	k8xC	nebo
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
pražádná	pražádný	k3yNgFnSc1	pražádný
význačná	význačný	k2eAgFnSc1d1	význačná
stránka	stránka	k1gFnSc1	stránka
našich	náš	k3xOp1gFnPc2	náš
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
neilustruje	ilustrovat	k5eNaBmIp3nS	ilustrovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Většině	většina	k1gFnSc3	většina
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Arne	Arne	k1gMnSc2	Arne
Nováka	Novák	k1gMnSc2	Novák
<g/>
,	,	kIx,	,
nevadilo	vadit	k5eNaImAgNnS	vadit
pevné	pevný	k2eAgNnSc1d1	pevné
neukotvení	neukotvení	k1gNnSc1	neukotvení
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
kniha	kniha	k1gFnSc1	kniha
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouhým	pouhý	k2eAgNnSc7d1	pouhé
pozadím	pozadí	k1gNnSc7	pozadí
pro	pro	k7c4	pro
vykreslení	vykreslení	k1gNnSc4	vykreslení
psychologie	psychologie	k1gFnSc2	psychologie
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
pro	pro	k7c4	pro
fabulačně	fabulačně	k6eAd1	fabulačně
neomezené	omezený	k2eNgNnSc4d1	neomezené
tvoření	tvoření	k1gNnSc4	tvoření
děje	dít	k5eAaImIp3nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
za	za	k7c4	za
všechny	všechen	k3xTgMnPc4	všechen
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Sezima	Sezim	k1gMnSc2	Sezim
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
historie	historie	k1gFnSc2	historie
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
literárního	literární	k2eAgNnSc2d1	literární
pozadí	pozadí	k1gNnSc2	pozadí
naopak	naopak	k6eAd1	naopak
vidí	vidět	k5eAaImIp3nS	vidět
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nepoután	poutat	k5eNaImNgInS	poutat
realistickým	realistický	k2eAgInSc7d1	realistický
detailem	detail	k1gInSc7	detail
<g/>
,	,	kIx,	,
skutečnostmi	skutečnost	k1gFnPc7	skutečnost
dávnověkými	dávnověký	k2eAgFnPc7d1	dávnověká
ani	ani	k8xC	ani
novodobými	novodobý	k2eAgFnPc7d1	novodobá
<g/>
,	,	kIx,	,
oproštěn	oproštěn	k2eAgMnSc1d1	oproštěn
naopak	naopak	k6eAd1	naopak
jak	jak	k8xS	jak
od	od	k7c2	od
úzkých	úzký	k2eAgInPc2d1	úzký
zřetelů	zřetel	k1gInPc2	zřetel
současných	současný	k2eAgInPc2d1	současný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
od	od	k7c2	od
šedivé	šedivý	k2eAgFnSc2d1	šedivá
historické	historický	k2eAgFnSc2d1	historická
průkaznosti	průkaznost	k1gFnSc2	průkaznost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
básník	básník	k1gMnSc1	básník
s	s	k7c7	s
neomezenou	omezený	k2eNgFnSc7d1	neomezená
svobodou	svoboda	k1gFnSc7	svoboda
obrazivosti	obrazivost	k1gFnSc2	obrazivost
i	i	k8xC	i
fabulační	fabulační	k2eAgFnSc2d1	fabulační
motivace	motivace	k1gFnSc2	motivace
dosyta	dosyta	k6eAd1	dosyta
ukájet	ukájet	k5eAaImF	ukájet
své	svůj	k3xOyFgInPc4	svůj
záliby	zálib	k1gInPc4	zálib
v	v	k7c6	v
primitivním	primitivní	k2eAgNnSc6d1	primitivní
barbarství	barbarství	k1gNnSc6	barbarství
pudů	pud	k1gInPc2	pud
<g/>
,	,	kIx,	,
v	v	k7c6	v
nezřízených	zřízený	k2eNgInPc6d1	nezřízený
výbuších	výbuch	k1gInPc6	výbuch
a	a	k8xC	a
slepých	slepý	k2eAgFnPc6d1	slepá
náruživostech	náruživost	k1gFnPc6	náruživost
lidské	lidský	k2eAgFnSc2d1	lidská
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
nedaleko	daleko	k6eNd1	daleko
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
dvě	dva	k4xCgFnPc1	dva
znepřátelené	znepřátelený	k2eAgFnPc1d1	znepřátelená
loupežnické	loupežnický	k2eAgFnPc1d1	loupežnická
rodiny	rodina	k1gFnPc1	rodina
-	-	kIx~	-
Kozlíkova	kozlíkův	k2eAgNnPc1d1	kozlíkův
a	a	k8xC	a
Lazarova	Lazarův	k2eAgNnPc1d1	Lazarovo
<g/>
.	.	kIx.	.
</s>
<s>
Kozlík	kozlík	k1gMnSc1	kozlík
si	se	k3xPyFc3	se
znepřátelí	znepřátelit	k5eAaPmIp3nS	znepřátelit
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
vyšle	vyslat	k5eAaPmIp3nS	vyslat
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Kozlík	kozlík	k1gMnSc1	kozlík
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
smír	smír	k1gInSc4	smír
s	s	k7c7	s
Lazarem	Lazar	k1gMnSc7	Lazar
a	a	k8xC	a
následně	následně	k6eAd1	následně
vypálí	vypálit	k5eAaPmIp3nS	vypálit
jeho	jeho	k3xOp3gFnSc1	jeho
tvrz	tvrz	k1gFnSc1	tvrz
a	a	k8xC	a
zajme	zajmout	k5eAaPmIp3nS	zajmout
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
Markétu	Markéta	k1gFnSc4	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zaslíbena	zaslíbit	k5eAaPmNgFnS	zaslíbit
bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
strávit	strávit	k5eAaPmF	strávit
život	život	k1gInSc4	život
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
loupežníky	loupežník	k1gMnPc7	loupežník
nalézá	nalézat	k5eAaImIp3nS	nalézat
lásku	láska	k1gFnSc4	láska
v	v	k7c6	v
Kozlíkově	kozlíkův	k2eAgMnSc6d1	kozlíkův
synovi	syn	k1gMnSc6	syn
<g/>
,	,	kIx,	,
Mikolášovi	Mikoláš	k1gMnSc6	Mikoláš
<g/>
.	.	kIx.	.
</s>
<s>
Kozlík	kozlík	k1gInSc1	kozlík
-	-	kIx~	-
Loupeživý	loupeživý	k2eAgMnSc1d1	loupeživý
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Kateřinou	Kateřina	k1gFnSc7	Kateřina
má	mít	k5eAaImIp3nS	mít
osm	osm	k4xCc4	osm
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
devět	devět	k4xCc4	devět
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
tvrzi	tvrz	k1gFnSc6	tvrz
Roháček	roháček	k1gInSc4	roháček
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
surový	surový	k2eAgInSc1d1	surový
a	a	k8xC	a
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
statečný	statečný	k2eAgMnSc1d1	statečný
a	a	k8xC	a
hrdý	hrdý	k2eAgMnSc1d1	hrdý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ochotný	ochotný	k2eAgMnSc1d1	ochotný
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
nebojí	bát	k5eNaImIp3nP	bát
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
cíle	cíl	k1gInSc2	cíl
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
jej	on	k3xPp3gMnSc4	on
nazval	nazvat	k5eAaPmAgMnS	nazvat
loupeživým	loupeživý	k2eAgMnSc7d1	loupeživý
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
děti	dítě	k1gFnPc1	dítě
mu	on	k3xPp3gMnSc3	on
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
soupeřem	soupeř	k1gMnSc7	soupeř
je	být	k5eAaImIp3nS	být
Lazar	Lazar	k1gMnSc1	Lazar
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
loupeživá	loupeživý	k2eAgFnSc1d1	loupeživá
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
počínáním	počínání	k1gNnSc7	počínání
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
poštve	poštvat	k5eAaPmIp3nS	poštvat
královské	královský	k2eAgNnSc4d1	královské
vojsko	vojsko	k1gNnSc4	vojsko
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
pokusu	pokus	k1gInSc3	pokus
o	o	k7c6	o
usmíření	usmíření	k1gNnSc6	usmíření
s	s	k7c7	s
Lazarem	Lazar	k1gMnSc7	Lazar
<g/>
.	.	kIx.	.
</s>
<s>
Lazar	Lazar	k1gMnSc1	Lazar
-	-	kIx~	-
Sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
tvrzi	tvrz	k1gFnSc6	tvrz
Obořiště	Obořiště	k1gNnSc2	Obořiště
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
loupežná	loupežný	k2eAgFnSc1d1	loupežná
banda	banda	k1gFnSc1	banda
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
s	s	k7c7	s
Kozlíkem	kozlík	k1gInSc7	kozlík
<g/>
.	.	kIx.	.
</s>
<s>
Lazar	Lazar	k1gMnSc1	Lazar
je	být	k5eAaImIp3nS	být
opakem	opak	k1gInSc7	opak
Kozlíka	kozlík	k1gInSc2	kozlík
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
autora	autor	k1gMnSc2	autor
je	být	k5eAaImIp3nS	být
Lazar	Lazar	k1gMnSc1	Lazar
obyčejným	obyčejný	k2eAgMnSc7d1	obyčejný
loupežníkem	loupežník	k1gMnSc7	loupežník
<g/>
.	.	kIx.	.
</s>
<s>
Odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
královskému	královský	k2eAgNnSc3d1	královské
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
-	-	kIx~	-
Kozlíkův	kozlíkův	k2eAgMnSc1d1	kozlíkův
nejdivočejší	divoký	k2eAgMnSc1d3	nejdivočejší
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
surový	surový	k2eAgInSc1d1	surový
<g/>
.	.	kIx.	.
</s>
<s>
Unese	unést	k5eAaPmIp3nS	unést
Markétu	Markéta	k1gFnSc4	Markéta
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
-	-	kIx~	-
Dcera	dcera	k1gFnSc1	dcera
Lazarova	Lazarův	k2eAgFnSc1d1	Lazarova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
zaslíbena	zaslíben	k2eAgFnSc1d1	zaslíbena
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
unesena	unesen	k2eAgFnSc1d1	unesena
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
<g/>
.	.	kIx.	.
</s>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
-	-	kIx~	-
Syn	syn	k1gMnSc1	syn
saského	saský	k2eAgMnSc2d1	saský
šlechtice	šlechtic	k1gMnSc2	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
unesen	unést	k5eAaPmNgMnS	unést
Kozlíkovou	kozlíkový	k2eAgFnSc7d1	Kozlíková
bandou	banda	k1gFnSc7	banda
<g/>
.	.	kIx.	.
</s>
<s>
Zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Kozlíkovy	kozlíkův	k2eAgFnSc2d1	Kozlíkova
dcery	dcera	k1gFnSc2	dcera
Alexandry	Alexandra	k1gFnSc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
citlivý	citlivý	k2eAgInSc1d1	citlivý
a	a	k8xC	a
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
.	.	kIx.	.
</s>
<s>
Alexandra	Alexandra	k1gFnSc1	Alexandra
-	-	kIx~	-
Dcera	dcera	k1gFnSc1	dcera
Kozlíkova	kozlíkův	k2eAgFnSc1d1	Kozlíkova
<g/>
.	.	kIx.	.
</s>
<s>
Zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Kristiána	Kristián	k1gMnSc2	Kristián
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
měla	mít	k5eAaImAgFnS	mít
milenecký	milenecký	k2eAgInSc4d1	milenecký
poměr	poměr	k1gInSc4	poměr
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Adamem	Adam	k1gMnSc7	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
-	-	kIx~	-
Hejtman	hejtman	k1gMnSc1	hejtman
královského	královský	k2eAgNnSc2d1	královské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
obchodoval	obchodovat	k5eAaImAgMnS	obchodovat
s	s	k7c7	s
pivem	pivo	k1gNnSc7	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
přijata	přijat	k2eAgFnSc1d1	přijata
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
nejen	nejen	k6eAd1	nejen
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
recenzí	recenze	k1gFnPc2	recenze
v	v	k7c6	v
dobových	dobový	k2eAgInPc6d1	dobový
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
o	o	k7c4	o
nejzajímavější	zajímavý	k2eAgFnSc4d3	nejzajímavější
knihu	kniha	k1gFnSc4	kniha
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
F.	F.	kA	F.
X.	X.	kA	X.
Šalda	Šalda	k1gMnSc1	Šalda
nazývá	nazývat	k5eAaImIp3nS	nazývat
Marketu	market	k1gInSc2	market
Lazarovou	Lazarův	k2eAgFnSc7d1	Lazarova
knihou	kniha	k1gFnSc7	kniha
"	"	kIx"	"
<g/>
až	až	k9	až
nedovoleně	dovoleně	k6eNd1	dovoleně
krásnou	krásný	k2eAgFnSc7d1	krásná
a	a	k8xC	a
omamnou	omamný	k2eAgFnSc7d1	omamná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgMnPc1d1	literární
kritikové	kritik	k1gMnPc1	kritik
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
kritikách	kritika	k1gFnPc6	kritika
shodně	shodně	k6eAd1	shodně
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hodnotu	hodnota	k1gFnSc4	hodnota
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
napsána	napsat	k5eAaBmNgFnS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
znechucen	znechucen	k2eAgMnSc1d1	znechucen
rozbředlým	rozbředlý	k2eAgInSc7d1	rozbředlý
jazykem	jazyk	k1gInSc7	jazyk
českého	český	k2eAgInSc2d1	český
impresionismu	impresionismus	k1gInSc2	impresionismus
a	a	k8xC	a
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
nezvyklým	zvyklý	k2eNgFnPc3d1	nezvyklá
<g/>
,	,	kIx,	,
jazykem	jazyk	k1gInSc7	jazyk
obtížným	obtížný	k2eAgInSc7d1	obtížný
<g/>
,	,	kIx,	,
stylizovaným	stylizovaný	k2eAgInSc7d1	stylizovaný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
a	a	k8xC	a
výstižným	výstižný	k2eAgInSc7d1	výstižný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
kritiky	kritika	k1gFnSc2	kritika
pozitivně	pozitivně	k6eAd1	pozitivně
hodnoceným	hodnocený	k2eAgMnPc3d1	hodnocený
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tento	tento	k3xDgInSc1	tento
sloh	sloh	k1gInSc1	sloh
<g/>
,	,	kIx,	,
snad	snad	k9	snad
poněkud	poněkud	k6eAd1	poněkud
romanticky	romanticky	k6eAd1	romanticky
-	-	kIx~	-
řekl	říct	k5eAaPmAgInS	říct
bych	by	kYmCp1nS	by
-	-	kIx~	-
hankovsky	hankovsky	k6eAd1	hankovsky
přebarvený	přebarvený	k2eAgInSc1d1	přebarvený
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
plným	plný	k2eAgInSc7d1	plný
životem	život	k1gInSc7	život
v	v	k7c6	v
Marketě	Marketa	k1gFnSc6	Marketa
Lazarové	Lazarová	k1gFnSc2	Lazarová
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
svou	svůj	k3xOyFgFnSc7	svůj
úžasnou	úžasný	k2eAgFnSc7d1	úžasná
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
<g/>
,	,	kIx,	,
zhuštěností	zhuštěnost	k1gFnSc7	zhuštěnost
a	a	k8xC	a
dynamičností	dynamičnost	k1gFnSc7	dynamičnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
píše	psát	k5eAaImIp3nS	psát
Roman	Roman	k1gMnSc1	Roman
Jakobson	Jakobson	k1gMnSc1	Jakobson
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kritice	kritika	k1gFnSc6	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Dodává	dodávat	k5eAaImIp3nS	dodávat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebýt	být	k5eNaImF	být
právě	právě	k6eAd1	právě
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
pouze	pouze	k6eAd1	pouze
dalším	další	k2eAgInSc7d1	další
průměrným	průměrný	k2eAgInSc7d1	průměrný
zbojnickým	zbojnický	k2eAgInSc7d1	zbojnický
románem	román	k1gInSc7	román
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zápletkou	zápletka	k1gFnSc7	zápletka
ani	ani	k8xC	ani
dějem	děj	k1gInSc7	děj
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
nejmenším	malý	k2eAgNnSc6d3	nejmenší
neodlišuje	odlišovat	k5eNaImIp3nS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Štoll	Štolla	k1gFnPc2	Štolla
také	také	k9	také
Vančurův	Vančurův	k2eAgInSc4d1	Vančurův
jazyk	jazyk	k1gInSc4	jazyk
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
velice	velice	k6eAd1	velice
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Knihami	kniha	k1gFnPc7	kniha
tohoto	tento	k3xDgMnSc2	tento
básníka	básník	k1gMnSc2	básník
se	se	k3xPyFc4	se
ubíráte	ubírat	k5eAaImIp2nP	ubírat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byste	by	kYmCp2nP	by
kráčeli	kráčet	k5eAaImAgMnP	kráčet
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
kamenité	kamenitý	k2eAgFnSc2d1	kamenitá
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
jdete	jít	k5eAaImIp2nP	jít
těžce	těžce	k6eAd1	těžce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
osvěžené	osvěžený	k2eAgInPc1d1	osvěžený
smysly	smysl	k1gInPc1	smysl
nás	my	k3xPp1nPc4	my
mámí	mámit	k5eAaImIp3nP	mámit
jakousi	jakýsi	k3yIgFnSc7	jakýsi
hořkou	hořký	k2eAgFnSc7d1	hořká
vizionářskou	vizionářský	k2eAgFnSc7d1	vizionářská
krásou	krása	k1gFnSc7	krása
prokleté	prokletý	k2eAgFnSc2d1	prokletá
lidské	lidský	k2eAgFnSc2d1	lidská
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
hořkou	hořký	k2eAgFnSc7d1	hořká
krásou	krása	k1gFnSc7	krása
srdce	srdce	k1gNnSc2	srdce
Marhoulů	Marhoul	k1gMnPc2	Marhoul
<g/>
,	,	kIx,	,
těchto	tento	k3xDgMnPc2	tento
novodobých	novodobý	k2eAgMnPc2d1	novodobý
Kristů	Kristus	k1gMnPc2	Kristus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prvotních	prvotní	k2eAgFnPc2d1	prvotní
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
autora	autor	k1gMnSc2	autor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Sezima	Sezimum	k1gNnSc2	Sezimum
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
především	především	k9	především
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
okázalému	okázalý	k2eAgInSc3d1	okázalý
kultu	kult	k1gInSc3	kult
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
samo	sám	k3xTgNnSc1	sám
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
tu	tu	k6eAd1	tu
viděti	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
nejlíp	dobře	k6eAd3	dobře
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
v	v	k7c6	v
písemnictví	písemnictví	k1gNnSc6	písemnictví
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
umělecké	umělecký	k2eAgNnSc1d1	umělecké
jak	jak	k6eAd1	jak
<g/>
,	,	kIx,	,
básnická	básnický	k2eAgFnSc1d1	básnická
kvalita	kvalita	k1gFnSc1	kvalita
nad	nad	k7c7	nad
syrovou	syrový	k2eAgFnSc7d1	syrová
látkou	látka	k1gFnSc7	látka
nebo	nebo	k8xC	nebo
myšlenkovou	myšlenkový	k2eAgFnSc7d1	myšlenková
předmětností	předmětnost	k1gFnSc7	předmětnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rozporuplně	rozporuplně	k6eAd1	rozporuplně
hodnocenou	hodnocený	k2eAgFnSc7d1	hodnocená
věcí	věc	k1gFnSc7	věc
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
Vančury	Vančura	k1gMnSc2	Vančura
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
jakožto	jakožto	k8xS	jakožto
vypravěče	vypravěč	k1gMnPc4	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Renč	Renč	k?	Renč
to	ten	k3xDgNnSc1	ten
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
prvek	prvek	k1gInSc1	prvek
narušitelský	narušitelský	k2eAgInSc1d1	narušitelský
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ponejvíce	ponejvíce	k6eAd1	ponejvíce
ty	ten	k3xDgFnPc4	ten
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
naštěstí	naštěstí	k6eAd1	naštěstí
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Vančurovi	Vančura	k1gMnSc3	Vančura
uteče	utéct	k5eAaPmIp3nS	utéct
názor	názor	k1gInSc4	názor
anebo	anebo	k8xC	anebo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
setkáte	setkat	k5eAaPmIp2nP	setkat
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
jste	být	k5eAaImIp2nP	být
ho	on	k3xPp3gMnSc4	on
nečekali	čekat	k5eNaImAgMnP	čekat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zatímco	zatímco	k8xS	zatímco
např.	např.	kA	např.
A.	A.	kA	A.
M.	M.	kA	M.
Píša	Píša	k1gMnSc1	Píša
právě	právě	k6eAd1	právě
toto	tento	k3xDgNnSc4	tento
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vančura	Vančura	k1gMnSc1	Vančura
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
výsadu	výsada	k1gFnSc4	výsada
naivních	naivní	k2eAgMnPc2d1	naivní
lidových	lidový	k2eAgMnPc2d1	lidový
vypravěčů	vypravěč	k1gMnPc2	vypravěč
<g/>
:	:	kIx,	:
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
důvěrně	důvěrně	k6eAd1	důvěrně
čtenáře	čtenář	k1gMnPc4	čtenář
i	i	k8xC	i
své	svůj	k3xOyFgFnPc4	svůj
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
glosuje	glosovat	k5eAaBmIp3nS	glosovat
jejich	jejich	k3xOp3gNnSc4	jejich
počínání	počínání	k1gNnSc4	počínání
i	i	k8xC	i
osudy	osud	k1gInPc4	osud
<g/>
,	,	kIx,	,
prokládá	prokládat	k5eAaImIp3nS	prokládat
vyprávění	vyprávění	k1gNnSc2	vyprávění
úvahami	úvaha	k1gFnPc7	úvaha
i	i	k8xC	i
mravním	mravní	k2eAgNnSc7d1	mravní
ponaučením	ponaučení	k1gNnSc7	ponaučení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Někteří	některý	k3yIgMnPc1	některý
kritikové	kritik	k1gMnPc1	kritik
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
také	také	k9	také
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
první	první	k4xOgInSc4	první
Vančurův	Vančurův	k2eAgInSc4d1	Vančurův
román	román	k1gInSc4	román
s	s	k7c7	s
ženskou	ženský	k2eAgFnSc7d1	ženská
hrdinkou	hrdinka	k1gFnSc7	hrdinka
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Götz	Götz	k1gMnSc1	Götz
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
Vančura	Vančura	k1gMnSc1	Vančura
byl	být	k5eAaImAgMnS	být
dosud	dosud	k6eAd1	dosud
básníkem	básník	k1gMnSc7	básník
mužským	mužský	k2eAgNnSc7d1	mužské
povýtce	povýtce	k6eAd1	povýtce
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
S	s	k7c7	s
uzráváním	uzrávání	k1gNnSc7	uzrávání
názorovým	názorový	k2eAgNnSc7d1	názorové
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
Markete	market	k1gInSc5	market
Lazarové	Lazarové	k2eAgFnSc3d1	Lazarové
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Vančurově	Vančurův	k2eAgNnSc6d1	Vančurovo
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xC	jako
základní	základní	k2eAgInSc4d1	základní
kompoziční	kompoziční	k2eAgInSc4d1	kompoziční
element	element	k1gInSc4	element
vroucně	vroucně	k6eAd1	vroucně
postižený	postižený	k2eAgInSc4d1	postižený
ženský	ženský	k2eAgInSc4d1	ženský
osud	osud	k1gInSc4	osud
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Touto	tento	k3xDgFnSc7	tento
kresbou	kresba	k1gFnSc7	kresba
ženského	ženský	k2eAgInSc2d1	ženský
osudu	osud	k1gInSc2	osud
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
Vančurovu	Vančurův	k2eAgInSc3d1	Vančurův
románu	román	k1gInSc3	román
živelné	živelný	k2eAgFnSc2d1	živelná
vitality	vitalita	k1gFnSc2	vitalita
a	a	k8xC	a
v	v	k7c6	v
ohledu	ohled	k1gInSc6	ohled
básnickém	básnický	k2eAgInSc6d1	básnický
velké	velký	k2eAgFnSc2d1	velká
názorové	názorový	k2eAgFnSc2d1	názorová
srostitosti	srostitost	k1gFnSc2	srostitost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
naopak	naopak	k6eAd1	naopak
Vančurovi	Vančura	k1gMnSc3	Vančura
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Marketa	Marketa	k1gFnSc1	Marketa
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
názvu	název	k1gInSc6	název
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc1	román
už	už	k6eAd1	už
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
spíše	spíše	k9	spíše
o	o	k7c6	o
Kozlíkovi	kozlík	k1gMnSc6	kozlík
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
<g/>
,	,	kIx,	,
než	než	k8xS	než
přímo	přímo	k6eAd1	přímo
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
posunování	posunování	k1gNnSc3	posunování
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
než	než	k8xS	než
samotným	samotný	k2eAgMnSc7d1	samotný
posuzovatelem	posuzovatel	k1gMnSc7	posuzovatel
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgInPc1d1	dobový
ohlasy	ohlas	k1gInPc1	ohlas
na	na	k7c6	na
Marketu	market	k1gInSc6	market
Lazarovou	Lazarová	k1gFnSc4	Lazarová
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
podobné	podobný	k2eAgFnPc1d1	podobná
-	-	kIx~	-
kritikové	kritik	k1gMnPc1	kritik
se	se	k3xPyFc4	se
zamýšlí	zamýšlet	k5eAaImIp3nP	zamýšlet
nad	nad	k7c7	nad
nekonkrétním	konkrétní	k2eNgNnSc7d1	nekonkrétní
pojetím	pojetí	k1gNnSc7	pojetí
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nikterak	nikterak	k6eAd1	nikterak
nesnižuje	snižovat	k5eNaImIp3nS	snižovat
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
psána	psát	k5eAaImNgFnS	psát
<g/>
,	,	kIx,	,
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
lidskost	lidskost	k1gFnSc4	lidskost
a	a	k8xC	a
pudovost	pudovost	k1gFnSc4	pudovost
Vančurových	Vančurových	k2eAgFnPc2d1	Vančurových
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Uměleckou	umělecký	k2eAgFnSc4d1	umělecká
kvalitu	kvalita	k1gFnSc4	kvalita
knize	kniha	k1gFnSc3	kniha
neupírá	upírat	k5eNaImIp3nS	upírat
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
jen	jen	k9	jen
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vidí	vidět	k5eAaImIp3nS	vidět
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
dosavadními	dosavadní	k2eAgFnPc7d1	dosavadní
knihami	kniha	k1gFnPc7	kniha
Vančurovými	Vančurův	k2eAgFnPc7d1	Vančurova
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Götz	Götz	k1gMnSc1	Götz
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Považuji	považovat	k5eAaImIp1nS	považovat
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
za	za	k7c4	za
dílo	dílo	k1gNnSc4	dílo
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
významu	význam	k1gInSc2	význam
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
tohoto	tento	k3xDgMnSc2	tento
básníka	básník	k1gMnSc2	básník
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
pronikl	proniknout	k5eAaPmAgMnS	proniknout
ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
pravé	pravý	k2eAgFnSc2d1	pravá
epičnosti	epičnost	k1gFnSc2	epičnost
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
Marketě	Market	k1gInSc6	Market
Lazarové	Lazarové	k2eAgMnSc1d1	Lazarové
Vančura	Vančura	k1gMnSc1	Vančura
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dává	dávat	k5eAaImIp3nS	dávat
každé	každý	k3xTgNnSc1	každý
dobré	dobrý	k2eAgNnSc1d1	dobré
epické	epický	k2eAgNnSc1d1	epické
dílo	dílo	k1gNnSc1	dílo
<g/>
:	:	kIx,	:
rozproudil	rozproudit	k5eAaPmAgInS	rozproudit
slavnou	slavný	k2eAgFnSc4d1	slavná
vlnu	vlna	k1gFnSc4	vlna
kolektivního	kolektivní	k2eAgInSc2d1	kolektivní
osudu	osud	k1gInSc2	osud
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
nejnebezpečnější	bezpečný	k2eNgFnSc6d3	nejnebezpečnější
době	doba	k1gFnSc6	doba
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
iluzi	iluze	k1gFnSc4	iluze
velebně	velebně	k6eAd1	velebně
plynoucího	plynoucí	k2eAgInSc2d1	plynoucí
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
s	s	k7c7	s
sebou	se	k3xPyFc7	se
strhuje	strhovat	k5eAaImIp3nS	strhovat
lidský	lidský	k2eAgInSc4d1	lidský
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
cíli	cíl	k1gInSc3	cíl
vede	vést	k5eAaImIp3nS	vést
jedině	jedině	k6eAd1	jedině
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
uzrání	uzrání	k1gNnSc4	uzrání
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
oproštěním	oproštění	k1gNnSc7	oproštění
názorovým	názorový	k2eAgNnSc7d1	názorové
a	a	k8xC	a
plnou	plný	k2eAgFnSc7d1	plná
epickou	epický	k2eAgFnSc7d1	epická
předmětností	předmětnost	k1gFnSc7	předmětnost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
i	i	k9	i
vidím	vidět	k5eAaImIp1nS	vidět
v	v	k7c6	v
Marketě	Market	k1gInSc6	Market
Lazarové	Lazarová	k1gFnSc2	Lazarová
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejpozoruhodnějších	pozoruhodný	k2eAgNnPc2d3	nejpozoruhodnější
románových	románový	k2eAgNnPc2d1	románové
děl	dělo	k1gNnPc2	dělo
naší	náš	k3xOp1gFnSc2	náš
nové	nový	k2eAgFnSc2d1	nová
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
doklad	doklad	k1gInSc4	doklad
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
generace	generace	k1gFnSc1	generace
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
Zrovna	zrovna	k6eAd1	zrovna
tak	tak	k6eAd1	tak
píše	psát	k5eAaImIp3nS	psát
i	i	k9	i
Václav	Václav	k1gMnSc1	Václav
Renč	Renč	k?	Renč
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
mezník	mezník	k1gInSc1	mezník
důležitý	důležitý	k2eAgInSc1d1	důležitý
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
samotného	samotný	k2eAgMnSc4d1	samotný
Vančuru	Vančura	k1gMnSc4	Vančura
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
pro	pro	k7c4	pro
celkový	celkový	k2eAgInSc4d1	celkový
vývoj	vývoj	k1gInSc4	vývoj
románové	románový	k2eAgFnSc2d1	románová
tvorby	tvorba	k1gFnSc2	tvorba
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
velice	velice	k6eAd1	velice
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
i	i	k9	i
A.	A.	kA	A.
M.	M.	kA	M.
Píša	Píša	k1gMnSc1	Píša
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
autorova	autorův	k2eAgNnSc2d1	autorovo
vyzrávání	vyzrávání	k1gNnSc2	vyzrávání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
knih	kniha	k1gFnPc2	kniha
naší	náš	k3xOp1gFnSc2	náš
mladé	mladý	k2eAgFnSc2d1	mladá
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
J.	J.	kA	J.
B.	B.	kA	B.
Čapek	Čapek	k1gMnSc1	Čapek
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hrdelní	hrdelní	k2eAgFnSc1d1	hrdelní
pře	pře	k1gFnSc1	pře
i	i	k8xC	i
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
jsou	být	k5eAaImIp3nP	být
díla	dílo	k1gNnPc1	dílo
umělecky	umělecky	k6eAd1	umělecky
silná	silný	k2eAgNnPc1d1	silné
<g/>
,	,	kIx,	,
z	z	k7c2	z
určitého	určitý	k2eAgNnSc2d1	určité
hlediska	hledisko	k1gNnSc2	hledisko
však	však	k9	však
lze	lze	k6eAd1	lze
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
zastávkami	zastávka	k1gFnPc7	zastávka
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
mezníky	mezník	k1gInPc7	mezník
Vančurova	Vančurův	k2eAgInSc2d1	Vančurův
vývoje	vývoj	k1gInSc2	vývoj
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
Pekaře	Pekař	k1gMnSc2	Pekař
Jana	Jan	k1gMnSc2	Jan
Marhoula	Marhoul	k1gMnSc2	Marhoul
se	se	k3xPyFc4	se
k	k	k7c3	k
dílům	díl	k1gInPc3	díl
skutečně	skutečně	k6eAd1	skutečně
ohniskovým	ohniskový	k2eAgInPc3d1	ohniskový
teprve	teprve	k9	teprve
chystá	chystat	k5eAaImIp3nS	chystat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
není	být	k5eNaImIp3nS	být
románem	román	k1gInSc7	román
historickým	historický	k2eAgInSc7d1	historický
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jiřího	Jiří	k1gMnSc2	Jiří
Poláčka	Poláček	k1gMnSc2	Poláček
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
básnický	básnický	k2eAgInSc1d1	básnický
mýtus	mýtus	k1gInSc1	mýtus
<g/>
,	,	kIx,	,
oslavující	oslavující	k2eAgFnSc4d1	oslavující
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
životní	životní	k2eAgFnSc4d1	životní
plnost	plnost	k1gFnSc4	plnost
a	a	k8xC	a
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
,	,	kIx,	,
družnost	družnost	k1gFnSc4	družnost
<g/>
,	,	kIx,	,
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
polemizující	polemizující	k2eAgFnSc6d1	polemizující
s	s	k7c7	s
mdlým	mdlý	k2eAgInSc7d1	mdlý
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
a	a	k8xC	a
rozkošnou	rozkošný	k2eAgFnSc7d1	rozkošná
složitostí	složitost	k1gFnSc7	složitost
literatury	literatura	k1gFnSc2	literatura
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
filmově	filmově	k6eAd1	filmově
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
režisérem	režisér	k1gMnSc7	režisér
Františkem	František	k1gMnSc7	František
Vláčilem	Vláčil	k1gMnSc7	Vláčil
s	s	k7c7	s
Magdou	Magda	k1gFnSc7	Magda
Vášáryovou	Vášáryový	k2eAgFnSc7d1	Vášáryová
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
publicistů	publicista	k1gMnPc2	publicista
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
filmem	film	k1gInSc7	film
stoleté	stoletý	k2eAgFnSc2d1	stoletá
historie	historie	k1gFnSc2	historie
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
.	.	kIx.	.
</s>
