<p>
<s>
Marshall	Marshall	k1gMnSc1	Marshall
Bruce	Bruce	k1gMnSc1	Bruce
Mathers	Mathersa	k1gFnPc2	Mathersa
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Eminem	Emin	k1gInSc7	Emin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
Shady	Shada	k1gFnSc2	Shada
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
detroitské	detroitský	k2eAgFnSc2d1	detroitská
skupiny	skupina	k1gFnSc2	skupina
D	D	kA	D
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
patnáct	patnáct	k4xCc4	patnáct
prestižních	prestižní	k2eAgFnPc2d1	prestižní
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
i	i	k8xC	i
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
Lose	los	k1gInSc6	los
Yourself	Yourself	k1gInSc1	Yourself
<g/>
"	"	kIx"	"
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
filmu	film	k1gInSc2	film
8	[number]	k4	8
<g/>
.	.	kIx.	.
míle	míle	k1gFnSc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejúspěšnějšími	úspěšný	k2eAgMnPc7d3	nejúspěšnější
alby	alba	k1gFnPc4	alba
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Eminem	Emino	k1gNnSc7	Emino
Show	show	k1gNnSc1	show
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
celosvětově	celosvětově	k6eAd1	celosvětově
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
diamantová	diamantový	k2eAgFnSc1d1	Diamantová
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
písně	píseň	k1gFnPc4	píseň
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lose	los	k1gInSc6	los
Yourself	Yourself	k1gInSc1	Yourself
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
Afraid	Afraid	k1gInSc1	Afraid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
the	the	k?	the
Way	Way	k1gMnSc1	Way
You	You	k1gMnSc1	You
Lie	Lie	k1gMnSc1	Lie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Monster	monstrum	k1gNnPc2	monstrum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
všechny	všechen	k3xTgFnPc4	všechen
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
společností	společnost	k1gFnSc7	společnost
Billboard	billboard	k1gInSc1	billboard
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
umělcem	umělec	k1gMnSc7	umělec
dekády	dekáda	k1gFnSc2	dekáda
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
Umělec	umělec	k1gMnSc1	umělec
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
při	při	k7c6	při
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc6	první
ročníku	ročník	k1gInSc2	ročník
Youtube	Youtub	k1gInSc5	Youtub
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
udělování	udělování	k1gNnSc6	udělování
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
EMA	Ema	k1gMnSc1	Ema
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
Hip-Hopového	Hip-Hopový	k2eAgMnSc4d1	Hip-Hopový
umělce	umělec	k1gMnSc4	umělec
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
Global	globat	k5eAaImAgMnS	globat
Icon	Icon	k1gMnSc1	Icon
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
je	být	k5eAaImIp3nS	být
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
rapper	rapper	k1gInSc1	rapper
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
prodaných	prodaný	k2eAgNnPc2d1	prodané
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Mathers	Mathers	k6eAd1	Mathers
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Saint	Saint	k1gInSc4	Saint
Joseph	Josepha	k1gFnPc2	Josepha
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Missouri	Missouri	k1gFnSc2	Missouri
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Porod	porod	k1gInSc1	porod
trval	trvat	k5eAaImAgInS	trvat
73	[number]	k4	73
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Debbie	Debbie	k1gFnSc2	Debbie
Mathers-Briggs	Mathers-Briggsa	k1gFnPc2	Mathers-Briggsa
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
málem	málem	k6eAd1	málem
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Eminemův	Eminemův	k2eAgMnSc1d1	Eminemův
otec	otec	k1gMnSc1	otec
Marshall	Marshall	k1gMnSc1	Marshall
Mathers	Mathersa	k1gFnPc2	Mathersa
Junior	junior	k1gMnSc1	junior
opustil	opustit	k5eAaPmAgMnS	opustit
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Eminemovi	Eminemův	k2eAgMnPc1d1	Eminemův
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
dětství	dětství	k1gNnSc2	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
stěhováním	stěhování	k1gNnSc7	stěhování
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
mezi	mezi	k7c4	mezi
Saint	Saint	k1gInSc4	Saint
Joseph	Josepha	k1gFnPc2	Josepha
a	a	k8xC	a
okolím	okolí	k1gNnSc7	okolí
Detroitu	Detroit	k1gInSc2	Detroit
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
často	často	k6eAd1	často
střídal	střídat	k5eAaImAgMnS	střídat
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
tak	tak	k9	tak
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Stával	stávat	k5eAaImAgInS	stávat
se	se	k3xPyFc4	se
také	také	k9	také
obětí	oběť	k1gFnSc7	oběť
šikany	šikana	k1gFnSc2	šikana
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ostatně	ostatně	k6eAd1	ostatně
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Brain	Brain	k2eAgInSc1d1	Brain
Damage	Damage	k1gInSc1	Damage
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
stěhování	stěhování	k1gNnSc2	stěhování
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
byla	být	k5eAaImAgFnS	být
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
často	často	k6eAd1	často
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
ubytovnách	ubytovna	k1gFnPc6	ubytovna
<g/>
,	,	kIx,	,
přívěsech	přívěs	k1gInPc6	přívěs
nebo	nebo	k8xC	nebo
u	u	k7c2	u
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
užívala	užívat	k5eAaImAgFnS	užívat
Debbie	Debbie	k1gFnSc1	Debbie
léky	lék	k1gInPc4	lék
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
(	(	kIx(	(
<g/>
Vicodin	Vicodin	k2eAgMnSc1d1	Vicodin
a	a	k8xC	a
Valium	Valium	k1gNnSc1	Valium
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eminem	Emin	k1gInSc7	Emin
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
a	a	k8xC	a
rozhovorech	rozhovor	k1gInPc6	rozhovor
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
lécích	lék	k1gInPc6	lék
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
ho	on	k3xPp3gNnSc4	on
později	pozdě	k6eAd2	pozdě
zažalovala	zažalovat	k5eAaPmAgFnS	zažalovat
o	o	k7c4	o
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
dostala	dostat	k5eAaPmAgFnS	dostat
pouhých	pouhý	k2eAgInPc2d1	pouhý
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Eminemovi	Eminemův	k2eAgMnPc1d1	Eminemův
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
Proofem	Proof	k1gMnSc7	Proof
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
Kimberly	Kimberl	k1gMnPc7	Kimberl
Ann	Ann	k1gMnSc1	Ann
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
Eminema	Eminem	k1gMnSc4	Eminem
motivace	motivace	k1gFnSc2	motivace
získat	získat	k5eAaPmF	získat
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
zaopatřit	zaopatřit	k5eAaPmF	zaopatřit
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Hailie	Hailie	k1gFnSc1	Hailie
Jade	Jade	k1gFnSc1	Jade
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
pak	pak	k6eAd1	pak
Eminem	Emino	k1gNnSc7	Emino
vydal	vydat	k5eAaPmAgMnS	vydat
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
album	album	k1gNnSc4	album
Infinite	Infinit	k1gInSc5	Infinit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nezaznamenalo	zaznamenat	k5eNaPmAgNnS	zaznamenat
větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
(	(	kIx(	(
<g/>
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
jen	jen	k9	jen
1000	[number]	k4	1000
kopií	kopie	k1gFnPc2	kopie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kim	Kim	k1gFnSc1	Kim
ukončila	ukončit	k5eAaPmAgFnS	ukončit
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
předávkováním	předávkování	k1gNnSc7	předávkování
prášky	prášek	k1gInPc7	prášek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
získat	získat	k5eAaPmF	získat
zpátky	zpátky	k6eAd1	zpátky
jak	jak	k8xC	jak
Kim	Kim	k1gFnSc4	Kim
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
slávu	sláva	k1gFnSc4	sláva
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Slim	Slim	k1gMnSc1	Slim
Shady	Shada	k1gFnSc2	Shada
LP	LP	kA	LP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Eminem	Emin	k1gInSc7	Emin
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
když	když	k8xS	když
nenabitou	nabitý	k2eNgFnSc7d1	nenabitá
zbraní	zbraň	k1gFnSc7	zbraň
ohrožoval	ohrožovat	k5eAaImAgMnS	ohrožovat
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
líbal	líbat	k5eAaImAgMnS	líbat
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
rokům	rok	k1gInPc3	rok
podmíněně	podmíněně	k6eAd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
soudu	soud	k1gInSc2	soud
se	se	k3xPyFc4	se
Kim	Kim	k1gFnSc1	Kim
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
podřezáním	podřezání	k1gNnSc7	podřezání
žil	žít	k5eAaImAgInS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gMnSc7	Emin
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
dát	dát	k5eAaPmF	dát
rozvést	rozvést	k5eAaPmF	rozvést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kim	Kim	k1gFnSc1	Kim
přivedla	přivést	k5eAaPmAgFnS	přivést
celou	celý	k2eAgFnSc4d1	celá
věc	věc	k1gFnSc4	věc
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požadovala	požadovat	k5eAaImAgFnS	požadovat
odebrání	odebrání	k1gNnSc4	odebrání
dcery	dcera	k1gFnSc2	dcera
Hailie	Hailie	k1gFnSc2	Hailie
a	a	k8xC	a
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
odškodného	odškodné	k1gNnSc2	odškodné
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
mimosoudně	mimosoudně	k6eAd1	mimosoudně
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
budou	být	k5eAaImBp3nP	být
starat	starat	k5eAaImF	starat
střídavě	střídavě	k6eAd1	střídavě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
se	se	k3xPyFc4	se
Kim	Kim	k1gFnSc1	Kim
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
podmíněně	podmíněně	k6eAd1	podmíněně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eminem	Emino	k1gNnSc7	Emino
s	s	k7c7	s
Kim	Kim	k1gFnSc7	Kim
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vzali	vzít	k5eAaPmAgMnP	vzít
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
dostala	dostat	k5eAaPmAgFnS	dostat
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pár	pár	k1gInSc1	pár
opět	opět	k6eAd1	opět
rozvedl	rozvést	k5eAaPmAgInS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
stará	starý	k2eAgNnPc1d1	staré
jak	jak	k8xC	jak
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
Hailie	Hailie	k1gFnSc2	Hailie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
sestřenici	sestřenice	k1gFnSc6	sestřenice
Alainu	Alaina	k1gFnSc4	Alaina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
také	také	k9	také
smrtelně	smrtelně	k6eAd1	smrtelně
postřelen	postřelen	k2eAgMnSc1d1	postřelen
Proof	Proof	k1gMnSc1	Proof
v	v	k7c6	v
detroitském	detroitský	k2eAgInSc6d1	detroitský
klubu	klub	k1gInSc6	klub
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
8	[number]	k4	8
Mile	mile	k6eAd1	mile
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rapperovi	rapper	k1gMnSc3	rapper
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
těžko	těžko	k6eAd1	těžko
vyléčitelné	vyléčitelný	k2eAgInPc4d1	vyléčitelný
šrámy	šrám	k1gInPc4	šrám
a	a	k8xC	a
donutilo	donutit	k5eAaPmAgNnS	donutit
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
stáhnout	stáhnout	k5eAaPmF	stáhnout
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
užívat	užívat	k5eAaImF	užívat
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
prášky	prášek	k1gInPc1	prášek
na	na	k7c4	na
spaní	spaní	k1gNnSc4	spaní
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
závislý	závislý	k2eAgMnSc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
léčit	léčit	k5eAaImF	léčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
slibuje	slibovat	k5eAaImIp3nS	slibovat
Proofovi	Proof	k1gMnSc3	Proof
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
písních	píseň	k1gFnPc6	píseň
popisuje	popisovat	k5eAaImIp3nS	popisovat
ono	onen	k3xDgNnSc1	onen
těžké	těžký	k2eAgNnSc1d1	těžké
období	období	k1gNnSc1	období
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
rap	rap	k1gMnSc1	rap
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
už	už	k6eAd1	už
jako	jako	k9	jako
teenager	teenager	k1gMnSc1	teenager
a	a	k8xC	a
vystupovat	vystupovat	k5eAaImF	vystupovat
začal	začít	k5eAaPmAgInS	začít
už	už	k6eAd1	už
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Soul	Soul	k1gInSc1	Soul
Intent	Intent	k1gInSc1	Intent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vydal	vydat	k5eAaPmAgMnS	vydat
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
album	album	k1gNnSc4	album
Infinite	Infinit	k1gInSc5	Infinit
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc4d1	následované
albem	album	k1gNnSc7	album
Slim	Slimo	k1gNnPc2	Slimo
Shady	Shada	k1gFnSc2	Shada
EP	EP	kA	EP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Hip	hip	k0	hip
hopová	hopový	k2eAgFnSc1d1	hopová
scéna	scéna	k1gFnSc1	scéna
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
<g/>
,	,	kIx,	,
komiksový	komiksový	k2eAgInSc4d1	komiksový
styl	styl	k1gInSc4	styl
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
našel	najít	k5eAaPmAgMnS	najít
Eminemovo	Eminemův	k2eAgNnSc4d1	Eminemovo
demo	demo	k2eAgNnSc4d1	demo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Iovinea	Iovineus	k1gMnSc2	Iovineus
<g/>
,	,	kIx,	,
hudebního	hudební	k2eAgMnSc2d1	hudební
ředitele	ředitel	k1gMnSc2	ředitel
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Interscope	Interscop	k1gInSc5	Interscop
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
demo	demo	k2eAgMnSc1d1	demo
sice	sice	k8xC	sice
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc2	Dre
nepřesvědčilo	přesvědčit	k5eNaPmAgNnS	přesvědčit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
Eminem	Emin	k1gInSc7	Emin
získal	získat	k5eAaPmAgMnS	získat
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Rap	rap	k1gMnSc1	rap
Olympics	Olympics	k1gInSc1	Olympics
(	(	kIx(	(
<g/>
rapová	rapový	k2eAgFnSc1d1	rapová
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	s	k7c7	s
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Eminem	Emino	k1gNnSc7	Emino
poprvé	poprvé	k6eAd1	poprvé
potkal	potkat	k5eAaPmAgInS	potkat
s	s	k7c7	s
Drem	Dre	k1gNnSc7	Dre
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
zářivě	zářivě	k6eAd1	zářivě
žluté	žlutý	k2eAgNnSc1d1	žluté
tričko	tričko	k1gNnSc1	tričko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neměl	mít	k5eNaImAgMnS	mít
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
decentnější	decentní	k2eAgNnSc4d2	decentnější
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
si	se	k3xPyFc3	se
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypadal	vypadat	k5eAaPmAgInS	vypadat
jako	jako	k9	jako
banán	banán	k1gInSc1	banán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infinite	Infinit	k1gInSc5	Infinit
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Infinite	Infinit	k1gInSc5	Infinit
je	on	k3xPp3gMnPc4	on
Eminemovo	Eminemův	k2eAgNnSc1d1	Eminemovo
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
pod	pod	k7c4	pod
Web	web	k1gInSc4	web
Entertainment	Entertainment	k1gInSc1	Entertainment
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
Eminemův	Eminemův	k2eAgInSc1d1	Eminemův
talent	talent	k1gInSc1	talent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rapu	rapa	k1gFnSc4	rapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gMnSc2	The
Slim	Slim	k1gMnSc1	Slim
Shady	Shada	k1gMnSc2	Shada
LP	LP	kA	LP
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Interscope	Interscop	k1gInSc5	Interscop
vydal	vydat	k5eAaPmAgInS	vydat
Eminem	Emin	k1gInSc7	Emin
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Slim	Slim	k1gMnSc1	Slim
Shady	Shada	k1gFnSc2	Shada
LP	LP	kA	LP
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
nahrávek	nahrávka	k1gFnPc2	nahrávka
roku	rok	k1gInSc2	rok
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třikrát	třikrát	k6eAd1	třikrát
platinové	platinový	k2eAgNnSc1d1	platinové
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
popularitou	popularita	k1gFnSc7	popularita
alba	album	k1gNnSc2	album
ale	ale	k8xC	ale
přišel	přijít	k5eAaPmAgInS	přijít
také	také	k9	také
rozruch	rozruch	k1gInSc1	rozruch
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gInPc2	jeho
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
97	[number]	k4	97
Bonnie	Bonnie	k1gFnSc2	Bonnie
and	and	k?	and
Clyde	Clyd	k1gMnSc5	Clyd
<g/>
"	"	kIx"	"
popisuje	popisovat	k5eAaImIp3nS	popisovat
Eminem	Emin	k1gInSc7	Emin
výlet	výlet	k1gInSc4	výlet
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dcerkou	dcerka	k1gFnSc7	dcerka
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
zbaví	zbavit	k5eAaPmIp3nS	zbavit
těla	tělo	k1gNnSc2	tělo
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gMnSc4	její
milence	milenec	k1gMnSc4	milenec
a	a	k8xC	a
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Guilty	Guilt	k1gInPc1	Guilt
Conscience	Conscience	k1gFnSc2	Conscience
<g/>
"	"	kIx"	"
zase	zase	k9	zase
končí	končit	k5eAaImIp3nS	končit
pasáží	pasáž	k1gFnSc7	pasáž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eminem	Emino	k1gNnSc7	Emino
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
nutí	nutit	k5eAaImIp3nS	nutit
muže	muž	k1gMnPc4	muž
zabít	zabít	k5eAaPmF	zabít
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
jejího	její	k3xOp3gMnSc4	její
milence	milenec	k1gMnSc4	milenec
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
Bottom	Bottom	k1gInSc1	Bottom
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Eminem	Emino	k1gNnSc7	Emino
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
den	den	k1gInSc4	den
před	před	k7c7	před
narozeninami	narozeniny	k1gFnPc7	narozeniny
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
a	a	k8xC	a
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc4	jaký
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
být	být	k5eAaImF	být
na	na	k7c6	na
úplném	úplný	k2eAgNnSc6d1	úplné
dně	dno	k1gNnSc6	dno
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
psychickém	psychický	k2eAgMnSc6d1	psychický
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
finančním	finanční	k2eAgMnSc7d1	finanční
<g/>
.	.	kIx.	.
</s>
<s>
Fyzické	fyzický	k2eAgNnSc1d1	fyzické
dno	dno	k1gNnSc1	dno
popisuje	popisovat	k5eAaImIp3nS	popisovat
zase	zase	k9	zase
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Brain	Brain	k2eAgInSc1d1	Brain
Damage	Damage	k1gInSc1	Damage
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
Eminem	Emin	k1gInSc7	Emin
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
klukovi	kluk	k1gMnSc6	kluk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
malého	malý	k1gMnSc4	malý
zbil	zbít	k5eAaPmAgMnS	zbít
tak	tak	k6eAd1	tak
brutálně	brutálně	k6eAd1	brutálně
<g/>
,	,	kIx,	,
že	že	k8xS	že
malý	malý	k2eAgInSc1d1	malý
Mathers	Mathers	k1gInSc1	Mathers
byl	být	k5eAaImAgInS	být
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
v	v	k7c6	v
kómatu	kóma	k1gNnSc6	kóma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
5,437	[number]	k4	5,437
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gFnSc1	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
The	The	k1gMnPc2	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathersa	k1gFnPc2	Mathersa
LP	LP	kA	LP
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2000	[number]	k4	2000
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Real	Real	k1gInSc4	Real
Slim	Slim	k1gInSc1	Slim
Shady	Shada	k1gFnSc2	Shada
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rozruch	rozruch	k1gInSc4	rozruch
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
Eminem	Emin	k1gInSc7	Emin
dělal	dělat	k5eAaImAgMnS	dělat
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
celebrit	celebrita	k1gFnPc2	celebrita
(	(	kIx(	(
<g/>
Christina	Christin	k2eAgMnSc4d1	Christin
Aguilera	Aguiler	k1gMnSc4	Aguiler
<g/>
,	,	kIx,	,
Will	Will	k1gMnSc1	Will
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Britney	Britney	k1gInPc1	Britney
Spears	Spears	k1gInSc1	Spears
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomlouval	pomlouvat	k5eAaImAgMnS	pomlouvat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jména	jméno	k1gNnPc4	jméno
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
písničce	písnička	k1gFnSc6	písnička
rýmovalo	rýmovat	k5eAaImAgNnS	rýmovat
a	a	k8xC	a
nikoho	nikdo	k3yNnSc4	nikdo
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
urazit	urazit	k5eAaPmF	urazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
singlu	singl	k1gInSc6	singl
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Stan	stan	k1gInSc1	stan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Eminem	Emin	k1gInSc7	Emin
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
jeho	jeho	k3xOp3gMnSc6	jeho
fanouškovi	fanoušek	k1gMnSc6	fanoušek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
tak	tak	k6eAd1	tak
posedlý	posedlý	k2eAgInSc1d1	posedlý
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakonec	nakonec	k6eAd1	nakonec
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Eminem	Emin	k1gMnSc7	Emin
neodpověděl	odpovědět	k5eNaPmAgMnS	odpovědět
včas	včas	k6eAd1	včas
na	na	k7c4	na
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
sebe	sebe	k3xPyFc4	sebe
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
těhotnou	těhotný	k2eAgFnSc4d1	těhotná
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
na	na	k7c4	na
The	The	k1gMnPc4	The
Slim	Sli	k1gNnSc7	Sli
Shady	Shada	k1gFnSc2	Shada
LP	LP	kA	LP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
1,7	[number]	k4	1,7
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
stalo	stát	k5eAaPmAgNnS	stát
nejrychleji	rychle	k6eAd3	rychle
se	s	k7c7	s
prodávaným	prodávaný	k2eAgNnSc7d1	prodávané
hip-hopovým	hipopový	k2eAgNnSc7d1	hip-hopové
albem	album	k1gNnSc7	album
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
singlem	singl	k1gInSc7	singl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Real	Real	k1gInSc4	Real
Slim	Slim	k1gInSc1	Slim
Shady	Shada	k1gFnSc2	Shada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
album	album	k1gNnSc1	album
přesáhlo	přesáhnout	k5eAaPmAgNnS	přesáhnout
hranici	hranice	k1gFnSc4	hranice
deseti	deset	k4xCc2	deset
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgMnPc2d1	prodaný
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získalo	získat	k5eAaPmAgNnS	získat
certifikaci	certifikace	k1gFnSc4	certifikace
diamantová	diamantový	k2eAgFnSc1d1	Diamantová
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
10,818	[number]	k4	10,818
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gMnPc4	The
Eminem	Emino	k1gNnSc7	Emino
Show	show	k1gFnSc2	show
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Eminem	Emin	k1gInSc7	Emin
Show	show	k1gFnSc2	show
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mu	on	k3xPp3gMnSc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
další	další	k2eAgFnSc4d1	další
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
dceři	dcera	k1gFnSc3	dcera
Hailie	Hailie	k1gFnSc2	Hailie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
"	"	kIx"	"
<g/>
rapovala	rapovat	k5eAaImAgFnS	rapovat
<g/>
"	"	kIx"	"
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
My	my	k3xPp1nPc1	my
Dad	Dad	k1gMnPc1	Dad
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gone	Gone	k1gNnSc7	Gone
Crazy	Craza	k1gFnSc2	Craza
<g/>
.	.	kIx.	.
<g/>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
o	o	k7c4	o
den	den	k1gInSc4	den
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
,	,	kIx,	,
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
280	[number]	k4	280
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
1,3	[number]	k4	1,3
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
singlem	singl	k1gInSc7	singl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Without	Without	k1gMnSc1	Without
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Cleanin	Cleanin	k2eAgMnSc1d1	Cleanin
<g/>
'	'	kIx"	'
Out	Out	k1gMnSc1	Out
My	my	k3xPp1nPc1	my
Closet	Closeta	k1gFnPc2	Closeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eminem	Emin	k1gInSc7	Emin
přeje	přát	k5eAaImIp3nS	přát
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
shořela	shořet	k5eAaPmAgFnS	shořet
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
album	album	k1gNnSc1	album
přesáhlo	přesáhnout	k5eAaPmAgNnS	přesáhnout
hranici	hranice	k1gFnSc4	hranice
deseti	deset	k4xCc2	deset
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgMnPc2d1	prodaný
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
také	také	k6eAd1	také
získalo	získat	k5eAaPmAgNnS	získat
certifikaci	certifikace	k1gFnSc4	certifikace
diamantová	diamantový	k2eAgFnSc1d1	Diamantová
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
10,332	[number]	k4	10,332
milionu	milion	k4xCgInSc2	milion
kůsů	kůs	k1gInPc2	kůs
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
na	na	k7c4	na
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Encore	Encor	k1gInSc5	Encor
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vydal	vydat	k5eAaPmAgInS	vydat
Eminem	Emino	k1gNnSc7	Emino
album	album	k1gNnSc4	album
Encore	Encor	k1gInSc5	Encor
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Lose	los	k1gMnSc5	los
It	It	k1gMnSc5	It
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
utahuje	utahovat	k5eAaImIp3nS	utahovat
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
lidí	člověk	k1gMnPc2	člověk
dostaly	dostat	k5eAaPmAgInP	dostat
i	i	k9	i
"	"	kIx"	"
<g/>
Mosh	Mosh	k1gInSc1	Mosh
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Like	Like	k1gFnSc1	Like
Toy	Toy	k1gMnSc1	Toy
Soldiers	Soldiers	k1gInSc1	Soldiers
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Písnička	písnička	k1gFnSc1	písnička
"	"	kIx"	"
<g/>
Mosh	Mosh	k1gInSc1	Mosh
<g/>
"	"	kIx"	"
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
předvolební	předvolební	k2eAgInSc4d1	předvolební
lobbing	lobbing	k1gInSc4	lobbing
<g/>
,	,	kIx,	,
nabádající	nabádající	k2eAgMnPc4d1	nabádající
mladé	mladý	k1gMnPc4	mladý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
volili	volit	k5eAaImAgMnP	volit
proti	proti	k7c3	proti
prezidentu	prezident	k1gMnSc3	prezident
Georgovi	Georg	k1gMnSc3	Georg
W.	W.	kA	W.
Bushovi	Bush	k1gMnSc3	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
"	"	kIx"	"
<g/>
Like	Like	k1gNnSc1	Like
Toy	Toy	k1gFnPc2	Toy
Soldiers	Soldiersa	k1gFnPc2	Soldiersa
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
jeho	on	k3xPp3gInSc4	on
přítel	přítel	k1gMnSc1	přítel
Proof	Proof	k1gMnSc1	Proof
zahrál	zahrát	k5eAaPmAgMnS	zahrát
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
ho	on	k3xPp3gMnSc4	on
měli	mít	k5eAaImAgMnP	mít
postřelit	postřelit	k5eAaPmF	postřelit
muži	muž	k1gMnPc1	muž
z	z	k7c2	z
jakési	jakýsi	k3yIgFnSc2	jakýsi
frakce	frakce	k1gFnSc2	frakce
stojící	stojící	k2eAgFnSc2d1	stojící
proti	proti	k7c3	proti
Eminemovi	Eminem	k1gMnSc3	Eminem
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
videoklip	videoklip	k1gInSc1	videoklip
výplodem	výplod	k1gInSc7	výplod
fikce	fikce	k1gFnSc2	fikce
<g/>
,	,	kIx,	,
Proof	Proof	k1gInSc4	Proof
ani	ani	k8xC	ani
ne	ne	k9	ne
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
skutečně	skutečně	k6eAd1	skutečně
utrpěl	utrpět	k5eAaPmAgInS	utrpět
smrtelná	smrtelný	k2eAgNnPc4d1	smrtelné
zranění	zranění	k1gNnPc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Mockingbird	Mockingbird	k1gInSc1	Mockingbird
<g/>
"	"	kIx"	"
popisuje	popisovat	k5eAaImIp3nS	popisovat
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Hailie	Hailie	k1gFnSc2	Hailie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
nedostalo	dostat	k5eNaPmAgNnS	dostat
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepovedlo	povést	k5eNaPmAgNnS	povést
po	po	k7c6	po
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
za	za	k7c7	za
sebou	se	k3xPyFc7	se
proměnit	proměnit	k5eAaPmF	proměnit
Grammy	Gramm	k1gInPc4	Gramm
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rapové	rapový	k2eAgNnSc4d1	rapové
album	album	k1gNnSc4	album
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
rapper	rapper	k1gMnSc1	rapper
[	[	kIx(	[
<g/>
Kanye	Kanye	k1gFnSc1	Kanye
West	West	k1gMnSc1	West
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Curtain	Curtain	k2eAgInSc1d1	Curtain
Call	Call	k1gInSc1	Call
a	a	k8xC	a
tříletá	tříletý	k2eAgFnSc1d1	tříletá
pauza	pauza	k1gFnSc1	pauza
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
hodlá	hodlat	k5eAaImIp3nS	hodlat
Eminem	Emin	k1gInSc7	Emin
ukončit	ukončit	k5eAaPmF	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Nasvědčovala	nasvědčovat	k5eAaImAgFnS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
albu	album	k1gNnSc6	album
The	The	k1gMnSc2	The
Funeral	Funeral	k1gMnSc2	Funeral
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
a	a	k8xC	a
mezi	mezi	k7c7	mezi
největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
objevit	objevit	k5eAaPmF	objevit
také	také	k9	také
některé	některý	k3yIgFnPc4	některý
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
Eminem	Emin	k1gInSc7	Emin
rozloučí	rozloučit	k5eAaPmIp3nS	rozloučit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
3	[number]	k4	3
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgFnPc4	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
rapoval	rapovat	k5eAaImAgMnS	rapovat
(	(	kIx(	(
<g/>
Eminem	Emin	k1gMnSc7	Emin
<g/>
,	,	kIx,	,
Slim	Slim	k1gMnSc1	Slim
Shady	Shada	k1gFnSc2	Shada
<g/>
,	,	kIx,	,
Marshall	Marshall	k1gMnSc1	Marshall
Mathers	Mathers	k1gInSc1	Mathers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
se	se	k3xPyFc4	se
v	v	k7c6	v
mediích	medium	k1gNnPc6	medium
objevily	objevit	k5eAaPmAgFnP	objevit
informace	informace	k1gFnPc1	informace
od	od	k7c2	od
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
rapperova	rapperův	k2eAgNnSc2d1	rapperovo
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
Eminem	Emino	k1gNnSc7	Emino
skutečně	skutečně	k6eAd1	skutečně
chystá	chystat	k5eAaImIp3nS	chystat
skončit	skončit	k5eAaPmF	skončit
s	s	k7c7	s
kariérou	kariéra	k1gFnSc7	kariéra
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
pouze	pouze	k6eAd1	pouze
produkování	produkování	k1gNnSc4	produkování
nahrávek	nahrávka	k1gFnPc2	nahrávka
a	a	k8xC	a
řízení	řízení	k1gNnSc1	řízení
své	svůj	k3xOyFgFnSc2	svůj
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Encore	Encor	k1gInSc5	Encor
by	by	kYmCp3nS	by
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
více	hodně	k6eAd2	hodně
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
a	a	k8xC	a
neteři	neteř	k1gFnSc3	neteř
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
započaté	započatý	k2eAgFnSc6d1	započatá
herecké	herecký	k2eAgFnSc6d1	herecká
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
sám	sám	k3xTgMnSc1	sám
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
MTV	MTV	kA	MTV
News	News	k1gInSc1	News
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odejít	odejít	k5eAaPmF	odejít
nehodlá	hodlat	k5eNaImIp3nS	hodlat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možná	možná	k9	možná
si	se	k3xPyFc3	se
dá	dát	k5eAaPmIp3nS	dát
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
přestávku	přestávka	k1gFnSc4	přestávka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
věnovat	věnovat	k5eAaPmF	věnovat
produkování	produkování	k1gNnSc4	produkování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
jeho	on	k3xPp3gNnSc2	on
amerického	americký	k2eAgNnSc2d1	americké
turné	turné	k1gNnSc2	turné
Anger	Angra	k1gFnPc2	Angra
Management	management	k1gInSc1	management
Tour	Tour	k1gMnSc1	Tour
Eminem	Emin	k1gMnSc7	Emin
zrušil	zrušit	k5eAaPmAgMnS	zrušit
vyprodané	vyprodaný	k2eAgInPc4d1	vyprodaný
koncerty	koncert	k1gInPc4	koncert
následného	následný	k2eAgNnSc2d1	následné
evropského	evropský	k2eAgNnSc2d1	Evropské
turné	turné	k1gNnSc2	turné
kvůli	kvůli	k7c3	kvůli
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
a	a	k8xC	a
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
léčbu	léčba	k1gFnSc4	léčba
pro	pro	k7c4	pro
odvykání	odvykání	k1gNnSc4	odvykání
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
prášcích	prášek	k1gInPc6	prášek
na	na	k7c6	na
spaní	spaní	k1gNnSc6	spaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eminem	Emin	k1gInSc7	Emin
pak	pak	k6eAd1	pak
skutečně	skutečně	k6eAd1	skutečně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
Curtain	Curtain	k2eAgInSc1d1	Curtain
Call	Call	k1gInSc1	Call
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
novými	nový	k2eAgFnPc7d1	nová
písněmi	píseň	k1gFnPc7	píseň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
naznačovat	naznačovat	k5eAaImF	naznačovat
závěrečné	závěrečný	k2eAgNnSc4d1	závěrečné
bilancování	bilancování	k1gNnSc4	bilancování
a	a	k8xC	a
konec	konec	k1gInSc4	konec
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
When	When	k1gInSc1	When
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Gone	Gone	k1gFnSc6	Gone
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
Eminem	Emino	k1gNnSc7	Emino
věnuje	věnovat	k5eAaImIp3nS	věnovat
vztahu	vztah	k1gInSc6	vztah
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
<g/>
,	,	kIx,	,
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
také	také	k9	také
problém	problém	k1gInSc1	problém
s	s	k7c7	s
prášky	prášek	k1gInPc7	prášek
na	na	k7c6	na
spaní	spaní	k1gNnSc6	spaní
<g/>
.	.	kIx.	.
</s>
<s>
Věty	věta	k1gFnPc1	věta
na	na	k7c6	na
konci	konec	k1gInSc6	konec
písně	píseň	k1gFnSc2	píseň
I	i	k9	i
turn	turn	k1gMnSc1	turn
around	around	k1gMnSc1	around
<g/>
,	,	kIx,	,
find	find	k?	find
a	a	k8xC	a
gun	gun	k?	gun
on	on	k3xPp3gMnSc1	on
the	the	k?	the
ground	ground	k1gInSc1	ground
/	/	kIx~	/
Cock	Cock	k1gInSc1	Cock
it	it	k?	it
<g/>
,	,	kIx,	,
put	puta	k1gFnPc2	puta
it	it	k?	it
to	ten	k3xDgNnSc1	ten
my	my	k3xPp1nPc1	my
brain	brain	k2eAgInSc1d1	brain
/	/	kIx~	/
Scream	Scream	k1gInSc1	Scream
"	"	kIx"	"
<g/>
Die	Die	k1gFnSc1	Die
Shady	Shada	k1gFnSc2	Shada
<g/>
"	"	kIx"	"
and	and	k?	and
pop	pop	k1gMnSc1	pop
it	it	k?	it
(	(	kIx(	(
<g/>
Otočím	otočit	k5eAaPmIp1nS	otočit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zvednu	zvednout	k5eAaPmIp1nS	zvednout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
pistoli	pistole	k1gFnSc4	pistole
/	/	kIx~	/
Natáhnu	natáhnout	k5eAaPmIp1nS	natáhnout
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
dám	dát	k5eAaPmIp1nS	dát
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
/	/	kIx~	/
Zařvu	zařvat	k5eAaPmIp1nS	zařvat
"	"	kIx"	"
<g/>
Chcípni	chcípnout	k5eAaPmRp2nS	chcípnout
<g/>
,	,	kIx,	,
Shady	Shad	k1gMnPc7	Shad
<g/>
"	"	kIx"	"
a	a	k8xC	a
vystřelím	vystřelit	k5eAaPmIp1nS	vystřelit
<g/>
)	)	kIx)	)
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Eminem	Emino	k1gNnSc7	Emino
jako	jako	k8xC	jako
Slim	Sli	k1gNnSc7	Sli
Shady	Shada	k1gFnSc2	Shada
už	už	k6eAd1	už
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
Curtain	Curtain	k1gMnSc1	Curtain
Call	Call	k1gMnSc1	Call
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Eminem	Emin	k1gMnSc7	Emin
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
branži	branže	k1gFnSc6	branže
končil	končit	k5eAaImAgMnS	končit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
přestávce	přestávka	k1gFnSc6	přestávka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
má	můj	k3xOp1gFnSc1	můj
kariéra	kariéra	k1gFnSc1	kariéra
ubírá	ubírat	k5eAaImIp3nS	ubírat
<g/>
...	...	k?	...
<g/>
.	.	kIx.	.
<g/>
Proto	proto	k8xC	proto
jsme	být	k5eAaImIp1nP	být
album	album	k1gNnSc4	album
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Curtain	Curtain	k2eAgInSc4d1	Curtain
Call	Call	k1gInSc4	Call
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Prostě	prostě	k9	prostě
to	ten	k3xDgNnSc4	ten
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Eminem	Emin	k1gInSc7	Emin
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2006	[number]	k4	2006
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
singlu	singl	k1gInSc6	singl
Smack	Smack	k1gInSc1	Smack
That	That	k1gInSc1	That
zpěváka	zpěvák	k1gMnSc2	zpěvák
Akona	Akon	k1gMnSc2	Akon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eminem	Emin	k1gInSc7	Emin
nekončí	končit	k5eNaImIp3nS	končit
a	a	k8xC	a
chystá	chystat	k5eAaImIp3nS	chystat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgMnS	vydat
Eminem	Emin	k1gInSc7	Emin
album	album	k1gNnSc4	album
The	The	k1gFnPc2	The
Re-Up	Re-Up	k1gMnSc1	Re-Up
-	-	kIx~	-
kompilaci	kompilace	k1gFnSc4	kompilace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
uvedení	uvedení	k1gNnSc4	uvedení
nových	nový	k2eAgInPc2d1	nový
rapperů	rapper	k1gInPc2	rapper
v	v	k7c4	v
Shady	Shad	k1gMnPc4	Shad
Records	Recordsa	k1gFnPc2	Recordsa
(	(	kIx(	(
<g/>
Stat	Stat	k1gMnSc1	Stat
Quo	Quo	k1gMnSc1	Quo
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
$	$	kIx~	$
<g/>
his	his	k1gNnPc1	his
a	a	k8xC	a
Bobby	Bobba	k1gFnPc1	Bobba
Creekwater	Creekwatra	k1gFnPc2	Creekwatra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
produkci	produkce	k1gFnSc4	produkce
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výběru	výběr	k1gInSc2	výběr
Curtain	Curtain	k1gMnSc1	Curtain
Call	Call	k1gMnSc1	Call
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
3,782	[number]	k4	3,782
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
Eminem	Emin	k1gMnSc7	Emin
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
chystá	chystat	k5eAaImIp3nS	chystat
vydat	vydat	k5eAaPmF	vydat
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Funeral	Funeral	k1gFnSc2	Funeral
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabije	zabít	k5eAaPmIp3nS	zabít
všechna	všechen	k3xTgNnPc4	všechen
svá	svůj	k3xOyFgNnPc4	svůj
ega	ego	k1gNnPc4	ego
(	(	kIx(	(
<g/>
Eminem	Emino	k1gNnSc7	Emino
<g/>
,	,	kIx,	,
Slim	Sli	k1gNnSc7	Sli
Shady	Shada	k1gFnSc2	Shada
<g/>
,	,	kIx,	,
Marshall	Marshall	k1gMnSc1	Marshall
Mathers	Mathers	k1gInSc1	Mathers
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
něco	něco	k3yInSc4	něco
úplně	úplně	k6eAd1	úplně
nového	nový	k2eAgNnSc2d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nezmínil	zmínit	k5eNaPmAgMnS	zmínit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
poslední	poslední	k2eAgNnSc4d1	poslední
Eminemovo	Eminemův	k2eAgNnSc4d1	Eminemovo
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
King	Kinga	k1gFnPc2	Kinga
Mathers	Mathersa	k1gFnPc2	Mathersa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
uniklo	uniknout	k5eAaPmAgNnS	uniknout
pár	pár	k4xCyI	pár
písniček	písnička	k1gFnPc2	písnička
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
na	na	k7c4	na
EP	EP	kA	EP
Straight	Straight	k1gMnSc1	Straight
from	from	k1gMnSc1	from
the	the	k?	the
Vault	Vault	k1gMnSc1	Vault
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
písně	píseň	k1gFnPc1	píseň
jako	jako	k9	jako
The	The	k1gMnPc1	The
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Syllables	Syllables	k1gMnSc1	Syllables
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Dre	Dre	k1gMnSc1	Dre
<g/>
,	,	kIx,	,
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
<g/>
,	,	kIx,	,
Jay-Z	Jay-Z	k1gFnSc1	Jay-Z
<g/>
,	,	kIx,	,
Cashis	Cashis	k1gFnSc1	Cashis
<g/>
,	,	kIx,	,
Stat	Stat	k1gMnSc1	Stat
Quo	Quo	k1gMnSc1	Quo
<g/>
,	,	kIx,	,
Ballin	Ballin	k2eAgMnSc1d1	Ballin
<g/>
'	'	kIx"	'
Uncontrollably	Uncontrollably	k1gMnSc1	Uncontrollably
<g/>
,	,	kIx,	,
Difficult	Difficult	k1gMnSc1	Difficult
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Obie	Obie	k1gInSc1	Obie
Trice	Trice	k1gFnSc2	Trice
a	a	k8xC	a
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Been	Been	k1gInSc1	Been
Real	Real	k1gInSc1	Real
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
plánované	plánovaný	k2eAgNnSc1d1	plánované
jako	jako	k8xS	jako
úplně	úplně	k6eAd1	úplně
poslední	poslední	k2eAgNnSc1d1	poslední
rozloučení	rozloučení	k1gNnSc1	rozloučení
s	s	k7c7	s
rapem	rape	k1gNnSc7	rape
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
děkuje	děkovat	k5eAaImIp3nS	děkovat
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gInSc1	King
Mathers	Mathersa	k1gFnPc2	Mathersa
nakonec	nakonec	k6eAd1	nakonec
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
a	a	k8xC	a
Eminem	Emino	k1gNnSc7	Emino
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
rapu	rap	k1gMnSc6	rap
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Relapse	relaps	k1gInSc5	relaps
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
diskuze	diskuze	k1gFnPc1	diskuze
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
rapového	rapový	k2eAgInSc2d1	rapový
důchodu	důchod	k1gInSc2	důchod
však	však	k8xC	však
Eminem	Emino	k1gNnSc7	Emino
ukončil	ukončit	k5eAaPmAgInS	ukončit
vydáním	vydání	k1gNnSc7	vydání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
Relapse	relaps	k1gInSc5	relaps
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
skladbách	skladba	k1gFnPc6	skladba
zde	zde	k6eAd1	zde
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
svou	svůj	k3xOyFgFnSc4	svůj
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
absenci	absence	k1gFnSc4	absence
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
svoje	svůj	k3xOyFgFnPc4	svůj
závislosti	závislost	k1gFnPc4	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
i	i	k8xC	i
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
ovlivňovalo	ovlivňovat	k5eAaImAgNnS	ovlivňovat
život	život	k1gInSc4	život
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
také	také	k9	také
opět	opět	k6eAd1	opět
oživil	oživit	k5eAaPmAgMnS	oživit
svoje	svůj	k3xOyFgNnSc4	svůj
alter-ego	altergo	k6eAd1	alter-ego
Slim	Slim	k1gMnSc1	Slim
Shady	Shada	k1gFnSc2	Shada
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
produkoval	produkovat	k5eAaImAgMnS	produkovat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc2	Dre
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
Eminem	Emin	k1gInSc7	Emin
produkoval	produkovat	k5eAaImAgMnS	produkovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jediní	jediný	k2eAgMnPc1d1	jediný
rappeři	rapper	k1gMnPc1	rapper
doprovázející	doprovázející	k2eAgNnPc4d1	doprovázející
Eminema	Eminemum	k1gNnPc4	Eminemum
na	na	k7c6	na
albu	album	k1gNnSc6	album
jsou	být	k5eAaImIp3nP	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
a	a	k8xC	a
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvlášť	zvlášť	k6eAd1	zvlášť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
nových	nový	k2eAgFnPc6d1	nová
skladbách	skladba	k1gFnPc6	skladba
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
velmi	velmi	k6eAd1	velmi
usilovně	usilovně	k6eAd1	usilovně
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k6eAd1	tak
víc	hodně	k6eAd2	hodně
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vešlo	vejít	k5eAaPmAgNnS	vejít
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gMnSc7	Emin
tak	tak	k6eAd1	tak
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
vydání	vydání	k1gNnSc4	vydání
jeho	jeho	k3xOp3gFnSc2	jeho
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
ale	ale	k8xC	ale
oznámil	oznámit	k5eAaPmAgMnS	oznámit
odložení	odložení	k1gNnSc4	odložení
tohoto	tento	k3xDgInSc2	tento
termínu	termín	k1gInSc2	termín
až	až	k9	až
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vydal	vydat	k5eAaPmAgMnS	vydat
21.12	[number]	k4	21.12
<g/>
.2009	.2009	k4	.2009
kratší	krátký	k2eAgInSc4d2	kratší
album	album	k1gNnSc1	album
Relapse	relaps	k1gInSc5	relaps
<g/>
:	:	kIx,	:
Refill	Refilla	k1gFnPc2	Refilla
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
v	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
roce	rok	k1gInSc6	rok
dát	dát	k5eAaPmF	dát
fanouškům	fanoušek	k1gMnPc3	fanoušek
více	hodně	k6eAd2	hodně
materiálu	materiál	k1gInSc2	materiál
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
původně	původně	k6eAd1	původně
plánoval	plánovat	k5eAaImAgMnS	plánovat
<g/>
...	...	k?	...
<g/>
doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladby	skladba	k1gFnPc1	skladba
na	na	k7c4	na
Refill	Refill	k1gInSc4	Refill
fanoušky	fanoušek	k1gMnPc7	fanoušek
trochu	trochu	k6eAd1	trochu
uklidní	uklidnit	k5eAaPmIp3nS	uklidnit
<g/>
,	,	kIx,	,
než	než	k8xS	než
vydáme	vydat	k5eAaPmIp1nP	vydat
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc7	Dre
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
producenty	producent	k1gMnPc7	producent
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
úplně	úplně	k6eAd1	úplně
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
musel	muset	k5eAaImAgMnS	muset
začít	začít	k5eAaPmF	začít
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
skladby	skladba	k1gFnPc1	skladba
začaly	začít	k5eAaPmAgFnP	začít
znít	znít	k5eAaImF	znít
výrazně	výrazně	k6eAd1	výrazně
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
co	co	k3yRnSc4	co
jsem	být	k5eAaImIp1nS	být
původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
na	na	k7c4	na
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
jsem	být	k5eAaImIp1nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
slyšeli	slyšet	k5eAaImAgMnP	slyšet
i	i	k8xC	i
ty	ten	k3xDgFnPc1	ten
původní	původní	k2eAgFnPc1d1	původní
skladby	skladba	k1gFnPc1	skladba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
Eminem	Emin	k1gMnSc7	Emin
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
nebude	být	k5eNaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Recovery	Recovera	k1gFnPc1	Recovera
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgInS	říct
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
jsem	být	k5eAaImIp1nS	být
plánoval	plánovat	k5eAaImAgMnS	plánovat
vydání	vydání	k1gNnSc4	vydání
'	'	kIx"	'
<g/>
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
<g/>
'	'	kIx"	'
na	na	k7c4	na
minulý	minulý	k2eAgInSc4d1	minulý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jak	jak	k8xS	jak
jsem	být	k5eAaImIp1nS	být
nahrával	nahrávat	k5eAaImAgInS	nahrávat
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgInS	pracovat
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
producenty	producent	k1gMnPc7	producent
<g/>
,	,	kIx,	,
představa	představa	k1gFnSc1	představa
pokračování	pokračování	k1gNnSc2	pokračování
'	'	kIx"	'
<g/>
Relapse	relaps	k1gInSc5	relaps
<g/>
'	'	kIx"	'
mi	já	k3xPp1nSc3	já
začala	začít	k5eAaPmAgFnS	začít
dávat	dávat	k5eAaImF	dávat
menší	malý	k2eAgInSc4d2	menší
a	a	k8xC	a
menší	malý	k2eAgInSc4d2	menší
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
úplně	úplně	k6eAd1	úplně
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
na	na	k7c4	na
'	'	kIx"	'
<g/>
Recovery	Recover	k1gInPc4	Recover
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
'	'	kIx"	'
<g/>
Relapse	relaps	k1gInSc5	relaps
<g/>
'	'	kIx"	'
a	a	k8xC	a
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
vlastní	vlastní	k2eAgInSc4d1	vlastní
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
Recovery	Recovera	k1gFnSc2	Recovera
a	a	k8xC	a
EP	EP	kA	EP
Hell	Hell	k1gMnSc1	Hell
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Sequel	Sequel	k1gMnSc1	Sequel
pak	pak	k6eAd1	pak
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
označil	označit	k5eAaPmAgMnS	označit
Relapse	relaps	k1gInSc5	relaps
za	za	k7c4	za
nepovedené	povedený	k2eNgNnSc4d1	nepovedené
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
singlu	singl	k1gInSc6	singl
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
Afraid	Afraid	k1gInSc1	Afraid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
be	be	k?	be
honest	honest	k1gMnSc1	honest
<g/>
,	,	kIx,	,
that	that	k1gMnSc1	that
last	last	k1gMnSc1	last
Relapse	relaps	k1gInSc5	relaps
CD	CD	kA	CD
was	was	k?	was
"	"	kIx"	"
<g/>
ehhh	ehhh	k1gMnSc1	ehhh
<g/>
"	"	kIx"	"
Perhaps	Perhapsa	k1gFnPc2	Perhapsa
I	i	k8xC	i
ran	rána	k1gFnPc2	rána
them	thema	k1gFnPc2	thema
accents	accents	k1gInSc1	accents
into	into	k1gMnSc1	into
the	the	k?	the
ground	ground	k1gMnSc1	ground
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Upřímně	upřímně	k6eAd1	upřímně
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgNnSc1d1	poslední
CD	CD	kA	CD
Relapse	relaps	k1gInSc5	relaps
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
ehhh	ehhh	k1gInSc1	ehhh
<g/>
"	"	kIx"	"
Možná	možná	k9	možná
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
těmi	ten	k3xDgInPc7	ten
přízvuky	přízvuk	k1gInPc7	přízvuk
přehnal	přehnat	k5eAaPmAgMnS	přehnat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nevydaného	vydaný	k2eNgNnSc2d1	nevydané
alba	album	k1gNnSc2	album
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
pár	pár	k4xCyI	pár
písní	píseň	k1gFnPc2	píseň
jako	jako	k8xC	jako
Oh	oh	k0	oh
No	no	k9	no
<g/>
,	,	kIx,	,
Nut	Nut	k1gMnSc1	Nut
Nut	Nut	k1gMnSc1	Nut
(	(	kIx(	(
<g/>
Snippet	Snippet	k1gMnSc1	Snippet
<g/>
)	)	kIx)	)
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
Detroit	Detroit	k1gInSc1	Detroit
Basketball	Basketballa	k1gFnPc2	Basketballa
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gInSc7	Emin
poté	poté	k6eAd1	poté
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgFnPc4	žádný
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
Relapse	relaps	k1gInSc5	relaps
<g/>
:	:	kIx,	:
Refill	Refillum	k1gNnPc2	Refillum
nebyly	být	k5eNaImAgFnP	být
původně	původně	k6eAd1	původně
plánovány	plánovat	k5eAaImNgFnP	plánovat
na	na	k7c4	na
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
nebylo	být	k5eNaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
kritiky	kritika	k1gFnSc2	kritika
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnPc4	album
Relapse	relaps	k1gInSc5	relaps
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
objevit	objevit	k5eAaPmF	objevit
umělci	umělec	k1gMnPc1	umělec
jako	jako	k9	jako
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
<g/>
,	,	kIx,	,
Lloyd	Lloyd	k1gInSc1	Lloyd
Banks	Banks	k1gInSc1	Banks
a	a	k8xC	a
D	D	kA	D
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Rosenberg	Rosenberg	k1gMnSc1	Rosenberg
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
i	i	k9	i
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
písně	píseň	k1gFnSc2	píseň
Blood	Blood	k1gInSc4	Blood
Red	Red	k1gFnSc2	Red
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
nejspíše	nejspíše	k9	nejspíše
objevit	objevit	k5eAaPmF	objevit
na	na	k7c4	na
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Recovery	Recover	k1gInPc7	Recover
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc4	jeho
sedmé	sedmý	k4xOgNnSc4	sedmý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
nazváno	nazván	k2eAgNnSc4d1	nazváno
Recovery	Recover	k1gMnPc7	Recover
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
s	s	k7c7	s
741	[number]	k4	741
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
druhý	druhý	k4xOgInSc4	druhý
týden	týden	k1gInSc4	týden
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
platinovým	platinový	k2eAgMnSc7d1	platinový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
žebříčků	žebříček	k1gInPc2	žebříček
album	album	k1gNnSc4	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
4,513	[number]	k4	4,513
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Dopomohly	dopomoct	k5eAaPmAgFnP	dopomoct
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
"	"	kIx"	"
<g/>
number-one	numbern	k1gInSc5	number-on
<g/>
"	"	kIx"	"
hity	hit	k1gInPc4	hit
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
Afraid	Afraid	k1gInSc1	Afraid
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
the	the	k?	the
Way	Way	k1gMnSc1	Way
You	You	k1gMnSc1	You
Lie	Lie	k1gMnSc1	Lie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bad	Bad	k1gFnPc2	Bad
Meets	Meets	k1gInSc1	Meets
Evil	Evil	k1gMnSc1	Evil
–	–	k?	–
Hell	Hell	k1gMnSc1	Hell
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Sequel	Sequel	k1gMnSc1	Sequel
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
obnovil	obnovit	k5eAaPmAgInS	obnovit
duo	duo	k1gNnSc4	duo
Bad	Bad	k1gFnSc2	Bad
Meets	Meetsa	k1gFnPc2	Meetsa
Evil	Evil	k1gInSc1	Evil
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
rapper	rapper	k1gInSc4	rapper
Royce	Royec	k1gInSc2	Royec
da	da	k?	da
5	[number]	k4	5
<g/>
́	́	k?	́
<g/>
9	[number]	k4	9
<g/>
́ ́	́ ́	k?	́ ́
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
spolu	spolu	k6eAd1	spolu
vydali	vydat	k5eAaPmAgMnP	vydat
EP	EP	kA	EP
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Hell	Hell	k1gInSc4	Hell
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Sequel	Sequel	k1gMnSc1	Sequel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
se	s	k7c7	s
171	[number]	k4	171
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
EP	EP	kA	EP
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
693	[number]	k4	693
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
singly	singl	k1gInPc7	singl
z	z	k7c2	z
EP	EP	kA	EP
jsou	být	k5eAaImIp3nP	být
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Fast	Fast	k2eAgInSc1d1	Fast
Lane	Lane	k1gInSc1	Lane
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Lighters	Lighters	k1gInSc1	Lighters
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Mars	Mars	k1gMnSc1	Mars
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gFnSc1	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
2	[number]	k4	2
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
svého	svůj	k3xOyFgNnSc2	svůj
osmého	osmý	k4xOgNnSc2	osmý
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Marshall	Marshall	k1gInSc1	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
2	[number]	k4	2
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
a	a	k8xC	a
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
rap-rocková	rapockový	k2eAgFnSc1d1	rap-rockový
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Berzerk	Berzerk	k1gInSc1	Berzerk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
prodalo	prodat	k5eAaPmAgNnS	prodat
360	[number]	k4	360
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Survival	Survival	k1gFnSc1	Survival
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
US	US	kA	US
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
singlem	singl	k1gInSc7	singl
poté	poté	k6eAd1	poté
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Rap	rap	k1gMnSc1	rap
God	God	k1gMnSc1	God
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Monster	monstrum	k1gNnPc2	monstrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rihannou	Rihanný	k2eAgFnSc7d1	Rihanný
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
tím	ten	k3xDgMnSc7	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
3	[number]	k4	3
<g/>
×	×	k?	×
platinovým	platinový	k2eAgMnSc7d1	platinový
<g/>
.	.	kIx.	.
<g/>
V	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
793	[number]	k4	793
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
album	album	k1gNnSc4	album
tím	ten	k3xDgNnSc7	ten
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
druhý	druhý	k4xOgInSc4	druhý
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
dalších	další	k2eAgInPc2d1	další
210	[number]	k4	210
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
prodej	prodej	k1gInSc1	prodej
překročil	překročit	k5eAaPmAgInS	překročit
hranici	hranice	k1gFnSc6	hranice
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
září	září	k1gNnSc2	září
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
prodáno	prodat	k5eAaPmNgNnS	prodat
již	již	k6eAd1	již
2	[number]	k4	2
203	[number]	k4	203
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
Eminem	Emin	k1gInSc7	Emin
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
řekl	říct	k5eAaPmAgMnS	říct
:	:	kIx,	:
"	"	kIx"	"
<g/>
Právě	právě	k9	právě
teď	teď	k6eAd1	teď
<g/>
,	,	kIx,	,
pracuju	pracovat	k5eAaImIp1nS	pracovat
asi	asi	k9	asi
nejpoctivěji	poctivě	k6eAd3	poctivě
<g/>
,	,	kIx,	,
nejtvrději	tvrdě	k6eAd3	tvrdě
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
bych	by	kYmCp1nS	by
to	ten	k3xDgNnSc1	ten
nepojmenoval	pojmenovat	k5eNaPmAgMnS	pojmenovat
Marshall	Marshall	k1gMnSc1	Marshall
Mathers	Mathersa	k1gFnPc2	Mathersa
LP2	LP2	k1gMnSc1	LP2
jen	jen	k9	jen
tak	tak	k6eAd1	tak
bez	bez	k7c2	bez
smyslu	smysl	k1gInSc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
jistý	jistý	k2eAgInSc4d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
ty	ten	k3xDgFnPc4	ten
správné	správný	k2eAgFnPc4d1	správná
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
to	ten	k3xDgNnSc4	ten
mohl	moct	k5eAaImAgMnS	moct
takhle	takhle	k6eAd1	takhle
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
poslechnete	poslechnout	k5eAaPmIp2nP	poslechnout
<g/>
,	,	kIx,	,
budete	být	k5eAaImBp2nP	být
mi	já	k3xPp1nSc3	já
rozumět	rozumět	k5eAaImF	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Cítím	cítit	k5eAaImIp1nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tohle	tenhle	k3xDgNnSc1	tenhle
potřebuju	potřebovat	k5eAaImIp1nS	potřebovat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
domaloval	domalovat	k5eAaPmAgInS	domalovat
celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
které	který	k3yIgFnPc4	který
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
zdát	zdát	k5eAaPmF	zdát
ohlašovalo	ohlašovat	k5eAaImAgNnS	ohlašovat
že	že	k9	že
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgMnSc1d1	poslední
Eminem	Emin	k1gMnSc7	Emin
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Rap	rap	k1gMnSc1	rap
City	City	k1gFnSc4	City
uvedl	uvést	k5eAaPmAgMnS	uvést
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
rapovat	rapovat	k5eAaImF	rapovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
bude	být	k5eAaImBp3nS	být
zabavné	zabavný	k2eAgNnSc1d1	zabavné
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Až	až	k9	až
zahodim	zahodim	k?	zahodim
mikrofon	mikrofon	k1gInSc1	mikrofon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
cítím	cítit	k5eAaImIp1nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chci	chtít	k5eAaImIp1nS	chtít
dělat	dělat	k5eAaImF	dělat
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgMnSc4d1	jiný
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Třeba	třeba	k6eAd1	třeba
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
cokoli	cokoli	k3yInSc1	cokoli
...	...	k?	...
Pořád	pořád	k6eAd1	pořád
se	se	k3xPyFc4	se
chci	chtít	k5eAaImIp1nS	chtít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Shady	Shada	k1gFnSc2	Shada
XV	XV	kA	XV
a	a	k8xC	a
Southpaw	Southpaw	k1gMnSc1	Southpaw
OST	OST	kA	OST
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
vydal	vydat	k5eAaPmAgInS	vydat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Guts	Guts	k1gInSc1	Guts
Over	Over	k1gMnSc1	Over
Fear	Fear	k1gMnSc1	Fear
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Sia	Sia	k?	Sia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
traileru	trailer	k1gInSc6	trailer
k	k	k7c3	k
filmu	film	k1gInSc3	film
Equalizer	Equalizra	k1gFnPc2	Equalizra
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
začaly	začít	k5eAaPmAgFnP	začít
kolovat	kolovat	k5eAaImF	kolovat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Shady	Shada	k1gFnSc2	Shada
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
kompilační	kompilační	k2eAgNnSc4d1	kompilační
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
labelu	label	k1gInSc2	label
Shady	Shada	k1gFnSc2	Shada
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc4	jeden
CD	CD	kA	CD
starších	starý	k2eAgInPc2d2	starší
hitů	hit	k1gInPc2	hit
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
CD	CD	kA	CD
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
materiálem	materiál	k1gInSc7	materiál
členů	člen	k1gMnPc2	člen
labelu	label	k1gInSc2	label
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podílí	podílet	k5eAaImIp3nS	podílet
nejenom	nejenom	k6eAd1	nejenom
Eminem	Emin	k1gInSc7	Emin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
rapper	rapper	k1gMnSc1	rapper
Yelawolf	Yelawolf	k1gMnSc1	Yelawolf
a	a	k8xC	a
skupiny	skupina	k1gFnPc1	skupina
D12	D12	k1gFnSc2	D12
a	a	k8xC	a
Slaughterhouse	Slaughterhouse	k1gFnSc2	Slaughterhouse
<g/>
.	.	kIx.	.
</s>
<s>
Kompilační	kompilační	k2eAgNnSc1d1	kompilační
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
231	[number]	k4	231
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
57	[number]	k4	57
<g/>
.	.	kIx.	.
předávání	předávání	k1gNnSc4	předávání
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rapové	rapový	k2eAgNnSc4d1	rapové
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
rap	rapa	k1gFnPc2	rapa
/	/	kIx~	/
zpěv	zpěv	k1gInSc4	zpěv
spolupráci	spolupráce	k1gFnSc4	spolupráce
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc4	The
Monster	monstrum	k1gNnPc2	monstrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
Rihannou	Rihanný	k2eAgFnSc7d1	Rihanný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc2	film
Bojovník	bojovník	k1gMnSc1	bojovník
(	(	kIx(	(
<g/>
Southpaw	Southpaw	k1gMnSc1	Southpaw
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
producent	producent	k1gMnSc1	producent
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
čtyřech	čtyři	k4xCgFnPc6	čtyři
skladbách	skladba	k1gFnPc6	skladba
a	a	k8xC	a
jako	jako	k8xC	jako
rapper	rapper	k1gMnSc1	rapper
také	také	k9	také
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Phenomenal	Phenomenal	k1gFnPc1	Phenomenal
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Kings	Kings	k1gInSc1	Kings
Never	Never	k1gMnSc1	Never
Die	Die	k1gMnSc1	Die
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Gwen	Gwen	k1gInSc1	Gwen
Stefani	Stefaň	k1gFnSc3	Stefaň
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
vybrány	vybrat	k5eAaPmNgInP	vybrat
za	za	k7c7	za
singly	singl	k1gInPc7	singl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
47	[number]	k4	47
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c4	na
80	[number]	k4	80
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
písně	píseň	k1gFnPc4	píseň
nahrál	nahrát	k5eAaBmAgMnS	nahrát
v	v	k7c6	v
duu	duo	k1gNnSc6	duo
Bad	Bad	k1gFnSc2	Bad
Meets	Meetsa	k1gFnPc2	Meetsa
Evil	Evila	k1gFnPc2	Evila
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Raw	Raw	k1gFnPc4	Raw
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
I	i	k8xC	i
Think	Think	k1gMnSc1	Think
About	About	k1gMnSc1	About
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Kill	Kill	k1gInSc1	Kill
For	forum	k1gNnPc2	forum
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
zpěvačky	zpěvačka	k1gFnPc1	zpěvačka
Skylar	Skylar	k1gInSc4	Skylar
Grey	Grea	k1gFnSc2	Grea
<g/>
,	,	kIx,	,
produkoval	produkovat	k5eAaImAgMnS	produkovat
i	i	k9	i
její	její	k3xOp3gFnSc4	její
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Come	Come	k1gInSc1	Come
Up	Up	k1gFnSc4	Up
For	forum	k1gNnPc2	forum
Air	Air	k1gFnSc2	Air
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Revival	revival	k1gInSc1	revival
a	a	k8xC	a
Kamikaze	kamikaze	k1gMnSc1	kamikaze
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Eminem	Emin	k1gInSc7	Emin
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
osmiminutovou	osmiminutový	k2eAgFnSc4d1	osmiminutová
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Campaign	Campaign	k1gInSc1	Campaign
Speech	speech	k1gInSc1	speech
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
kandidáta	kandidát	k1gMnSc4	kandidát
Donalda	Donald	k1gMnSc4	Donald
Trumpa	Trump	k1gMnSc4	Trump
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
písně	píseň	k1gFnSc2	píseň
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2017	[number]	k4	2017
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
rappera	rapper	k1gMnSc2	rapper
Big	Big	k1gMnSc2	Big
Seana	Sean	k1gMnSc2	Sean
"	"	kIx"	"
<g/>
No	no	k9	no
Favors	Favors	k1gInSc1	Favors
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
a	a	k8xC	a
urážel	urážet	k5eAaPmAgMnS	urážet
Donalda	Donald	k1gMnSc4	Donald
Trumpa	Trump	k1gMnSc4	Trump
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
v	v	k7c6	v
roli	role	k1gFnSc6	role
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sloce	sloka	k1gFnSc6	sloka
také	také	k9	také
rapoval	rapovat	k5eAaImAgMnS	rapovat
o	o	k7c4	o
znásilnění	znásilnění	k1gNnSc4	znásilnění
republikánské	republikánský	k2eAgFnSc2d1	republikánská
politické	politický	k2eAgFnSc2d1	politická
komentátorky	komentátorka	k1gFnSc2	komentátorka
Anny	Anna	k1gFnSc2	Anna
Coulterové	Coulterová	k1gFnSc2	Coulterová
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
různých	různý	k2eAgInPc2d1	různý
podivných	podivný	k2eAgInPc2d1	podivný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
ve	v	k7c6	v
skupinovém	skupinový	k2eAgInSc6d1	skupinový
freestylovém	freestylový	k2eAgInSc6d1	freestylový
vstupu	vstup	k1gInSc6	vstup
na	na	k7c6	na
předávání	předávání	k1gNnSc6	předávání
cen	cena	k1gFnPc2	cena
televizní	televizní	k2eAgFnSc2d1	televizní
hudební	hudební	k2eAgFnSc2d1	hudební
stanice	stanice	k1gFnSc2	stanice
BET	BET	kA	BET
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
freestylu	freestyl	k1gInSc6	freestyl
opět	opět	k6eAd1	opět
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
prezidenta	prezident	k1gMnSc4	prezident
Trumpa	Trump	k1gMnSc4	Trump
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
blížícím	blížící	k2eAgNnSc6d1	blížící
se	se	k3xPyFc4	se
vydání	vydání	k1gNnSc1	vydání
nového	nový	k2eAgNnSc2d1	nové
Eminemova	Eminemův	k2eAgNnSc2d1	Eminemovo
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Walk	Walk	k1gMnSc1	Walk
on	on	k3xPp3gMnSc1	on
Water	Water	k1gMnSc1	Water
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Beyoncé	Beyoncé	k1gNnSc1	Beyoncé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
opět	opět	k6eAd1	opět
produkovali	produkovat	k5eAaImAgMnP	produkovat
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
a	a	k8xC	a
Skylar	Skylar	k1gMnSc1	Skylar
Grey	Grea	k1gFnSc2	Grea
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emino	k1gNnSc7	Emino
se	s	k7c7	s
singlem	singl	k1gInSc7	singl
poprvé	poprvé	k6eAd1	poprvé
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
na	na	k7c6	na
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Music	k1gMnSc1	Music
Awards	Awards	k1gInSc4	Awards
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Beyoncé	Beyoncé	k1gNnSc2	Beyoncé
zastoupila	zastoupit	k5eAaPmAgFnS	zastoupit
právě	právě	k6eAd1	právě
Skylar	Skylar	k1gInSc4	Skylar
Grey	Grea	k1gFnSc2	Grea
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k2eAgInSc4d1	Night
Live	Live	k1gInSc4	Live
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
prosincový	prosincový	k2eAgInSc1d1	prosincový
termín	termín	k1gInSc1	termín
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
Eminem	Emin	k1gMnSc7	Emin
vydal	vydat	k5eAaPmAgInS	vydat
propagační	propagační	k2eAgInSc1d1	propagační
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Untouchable	Untouchable	k1gFnSc1	Untouchable
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
86	[number]	k4	86
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
deváté	devátý	k4xOgNnSc1	devátý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Revival	revival	k1gInSc1	revival
uniklo	uniknout	k5eAaPmAgNnS	uniknout
na	na	k7c4	na
internet	internet	k1gInSc1	internet
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
řádným	řádný	k2eAgNnSc7d1	řádné
datem	datum	k1gNnSc7	datum
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
byl	být	k5eAaImAgInS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
(	(	kIx(	(
<g/>
se	s	k7c7	s
267	[number]	k4	267
000	[number]	k4	000
ks	ks	kA	ks
prodanými	prodaný	k2eAgInPc7d1	prodaný
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
po	po	k7c6	po
započítání	započítání	k1gNnSc6	započítání
streamů	stream	k1gInPc2	stream
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
již	již	k6eAd1	již
Eminemovo	Eminemův	k2eAgNnSc1d1	Eminemovo
osmé	osmý	k4xOgFnPc4	osmý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
hudebním	hudební	k2eAgMnSc7d1	hudební
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
toho	ten	k3xDgMnSc4	ten
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
průměrným	průměrný	k2eAgNnSc7d1	průměrné
přijetím	přijetí	k1gNnSc7	přijetí
u	u	k7c2	u
hudebních	hudební	k2eAgMnPc2d1	hudební
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
serveru	server	k1gInSc6	server
Metacritic	Metacritice	k1gFnPc2	Metacritice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
agreguje	agregovat	k5eAaImIp3nS	agregovat
recenze	recenze	k1gFnPc4	recenze
<g/>
,	,	kIx,	,
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
50	[number]	k4	50
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Skóre	skóre	k1gNnSc1	skóre
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
24	[number]	k4	24
profesionálních	profesionální	k2eAgFnPc6d1	profesionální
recenzích	recenze	k1gFnPc6	recenze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
vydal	vydat	k5eAaPmAgInS	vydat
druhý	druhý	k4xOgInSc4	druhý
oficiální	oficiální	k2eAgInSc4d1	oficiální
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
River	River	k1gInSc4	River
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
Sheeran	Sheeran	k1gInSc1	Sheeran
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
US	US	kA	US
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
například	například	k6eAd1	například
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
vydané	vydaný	k2eAgFnPc1d1	vydaná
jako	jako	k8xC	jako
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Nowhere	Nowher	k1gInSc5	Nowher
Fast	Fastum	k1gNnPc2	Fastum
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Remind	Remind	k1gMnSc1	Remind
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
v	v	k7c6	v
USA	USA	kA	USA
nezabodovaly	zabodovat	k5eNaPmAgFnP	zabodovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
vydal	vydat	k5eAaPmAgInS	vydat
remix	remix	k1gInSc1	remix
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Chloraseptic	Chloraseptice	k1gFnPc2	Chloraseptice
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Revival	revival	k1gInSc1	revival
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nově	nově	k6eAd1	nově
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
sloky	sloka	k1gFnSc2	sloka
od	od	k7c2	od
2	[number]	k4	2
Chainze	Chainze	k1gFnSc2	Chainze
a	a	k8xC	a
Phreshera	Phreshero	k1gNnSc2	Phreshero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
Eminem	Emino	k1gNnSc7	Emino
zesměšňoval	zesměšňovat	k5eAaImAgMnS	zesměšňovat
kritiky	kritika	k1gFnSc2	kritika
svého	svůj	k3xOyFgNnSc2	svůj
posledního	poslední	k2eAgNnSc2d1	poslední
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emino	k1gNnSc7	Emino
později	pozdě	k6eAd2	pozdě
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
s	s	k7c7	s
producenty	producent	k1gMnPc7	producent
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Drem	Drem	k1gMnSc1	Drem
a	a	k8xC	a
Mike	Mike	k1gFnSc1	Mike
Will	Willa	k1gFnPc2	Willa
Made	Mad	k1gMnSc2	Mad
It.	It.	k1gMnSc2	It.
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
teaser	teaser	k1gInSc1	teaser
nové	nový	k2eAgFnSc2d1	nová
písně	píseň	k1gFnSc2	píseň
s	s	k7c7	s
popiskem	popisek	k1gInSc7	popisek
Venom	Venom	k1gInSc1	Venom
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
streamovacích	streamovací	k2eAgFnPc6d1	streamovací
službách	služba	k1gFnPc6	služba
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
předchozí	předchozí	k2eAgFnSc2d1	předchozí
propagace	propagace	k1gFnSc2	propagace
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
desáté	desátý	k4xOgNnSc4	desátý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Kamikaze	kamikaze	k1gMnSc2	kamikaze
<g/>
.	.	kIx.	.
</s>
<s>
Výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgMnS	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
Eminema	Eminem	k1gMnSc2	Eminem
podílel	podílet	k5eAaImAgMnS	podílet
třemi	tři	k4xCgNnPc7	tři
písněmi	píseň	k1gFnPc7	píseň
také	také	k9	také
trapový	trapový	k2eAgMnSc1d1	trapový
hitmaker	hitmaker	k1gMnSc1	hitmaker
Mike	Mik	k1gFnSc2	Mik
Will	Will	k1gMnSc1	Will
Made	Mad	k1gMnSc4	Mad
It	It	k1gMnSc4	It
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eminem	Emino	k1gNnSc7	Emino
v	v	k7c6	v
textech	text	k1gInPc6	text
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
současný	současný	k2eAgInSc4d1	současný
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
témata	téma	k1gNnPc4	téma
trapu	trap	k1gInSc2	trap
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Venom	Venom	k1gInSc1	Venom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
tehdy	tehdy	k6eAd1	tehdy
nadcházejícímu	nadcházející	k2eAgInSc3d1	nadcházející
filmu	film	k1gInSc3	film
Venom	Venom	k1gInSc4	Venom
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
se	s	k7c7	s
434	[number]	k4	434
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
prodeje	prodej	k1gInSc2	prodej
(	(	kIx(	(
<g/>
252	[number]	k4	252
000	[number]	k4	000
ks	ks	kA	ks
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
prodeji	prodej	k1gInSc6	prodej
+	+	kIx~	+
225	[number]	k4	225
milionů	milion	k4xCgInPc2	milion
streamů	stream	k1gInPc2	stream
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kamikaze	kamikaze	k1gMnSc3	kamikaze
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
Eminemovým	Eminemův	k2eAgNnSc7d1	Eminemovo
devátým	devátý	k4xOgFnPc3	devátý
albem	album	k1gNnSc7	album
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
příčce	příčka	k1gFnSc6	příčka
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgFnPc2	všecek
jedenáct	jedenáct	k4xCc1	jedenáct
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Lucky	Lucka	k1gFnPc1	Lucka
You	You	k1gMnPc2	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ringer	Ringer	k1gMnSc1	Ringer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emino	k1gNnSc7	Emino
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
historicky	historicky	k6eAd1	historicky
pátým	pátý	k4xOgMnSc7	pátý
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
současně	současně	k6eAd1	současně
debutovaly	debutovat	k5eAaBmAgFnP	debutovat
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
písních	píseň	k1gFnPc6	píseň
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
celkem	celkem	k6eAd1	celkem
916	[number]	k4	916
000	[number]	k4	000
ks	ks	kA	ks
<g/>
.	.	kIx.	.
<g/>
Album	album	k1gNnSc1	album
Kamikaze	kamikaze	k1gMnPc2	kamikaze
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
několik	několik	k4yIc1	několik
disstracků	disstracek	k1gMnPc2	disstracek
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mladé	mladý	k2eAgFnPc4d1	mladá
rappery	rappera	k1gFnPc4	rappera
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Ringer	Ringer	k1gMnSc1	Ringer
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
kritikou	kritika	k1gFnSc7	kritika
rapperů	rapper	k1gInPc2	rapper
Lil	lít	k5eAaImAgInS	lít
Pumpa	pumpa	k1gFnSc1	pumpa
<g/>
,	,	kIx,	,
Lil	lít	k5eAaImAgMnS	lít
Xana	Xana	k1gMnSc1	Xana
nebo	nebo	k8xC	nebo
Lil	lít	k5eAaImAgMnS	lít
Yachtyho	Yachty	k1gMnSc4	Yachty
<g/>
.	.	kIx.	.
</s>
<s>
Kritice	kritika	k1gFnSc3	kritika
se	se	k3xPyFc4	se
nevyhnuli	vyhnout	k5eNaPmAgMnP	vyhnout
ani	ani	k8xC	ani
rappeři	rapper	k1gMnPc1	rapper
Drake	Drake	k1gNnSc2	Drake
<g/>
,	,	kIx,	,
Chance	Chanec	k1gInSc2	Chanec
the	the	k?	the
Rapper	Rapper	k1gMnSc1	Rapper
<g/>
,	,	kIx,	,
Tyler	Tyler	k1gMnSc1	Tyler
<g/>
,	,	kIx,	,
the	the	k?	the
Creator	Creator	k1gMnSc1	Creator
a	a	k8xC	a
Earl	earl	k1gMnSc1	earl
Sweatshirt	Sweatshirta	k1gFnPc2	Sweatshirta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Eminem	Emin	k1gMnSc7	Emin
zmiňoval	zmiňovat	k5eAaImAgInS	zmiňovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Fall	Fall	k1gInSc1	Fall
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
reakci	reakce	k1gFnSc4	reakce
ale	ale	k8xC	ale
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
spor	spor	k1gInSc1	spor
s	s	k7c7	s
rapperem	rapper	k1gInSc7	rapper
Machine	Machin	k1gInSc5	Machin
Gun	Gun	k1gFnPc7	Gun
Kellym	Kellym	k1gInSc1	Kellym
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
Eminem	Emin	k1gMnSc7	Emin
dissoval	dissovat	k5eAaBmAgInS	dissovat
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
Alike	Alike	k1gInSc1	Alike
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Machine	Machinout	k5eAaPmIp3nS	Machinout
Gun	Gun	k1gFnSc4	Gun
Kelly	Kella	k1gFnSc2	Kella
nahrál	nahrát	k5eAaPmAgMnS	nahrát
na	na	k7c4	na
Eminema	Eminema	k1gNnSc4	Eminema
disstrack	disstracka	k1gFnPc2	disstracka
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Rap	rap	k1gMnSc1	rap
Devil	Devil	k1gMnSc1	Devil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
Eminemův	Eminemův	k2eAgInSc1d1	Eminemův
beat	beat	k1gInSc1	beat
z	z	k7c2	z
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Rap	rap	k1gMnSc1	rap
God	God	k1gMnSc1	God
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
Eminem	Emino	k1gNnSc7	Emino
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
disstrackem	disstrack	k1gInSc7	disstrack
"	"	kIx"	"
<g/>
Killshot	Killshot	k1gInSc1	Killshot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
rekord	rekord	k1gInSc4	rekord
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
38	[number]	k4	38
miliony	milion	k4xCgInPc7	milion
zhlédnutími	zhlédnutí	k1gNnPc7	zhlédnutí
stal	stát	k5eAaPmAgInS	stát
nejvíce	hodně	k6eAd3	hodně
sledovaným	sledovaný	k2eAgNnSc7d1	sledované
hip-hopovým	hipopový	k2eAgNnSc7d1	hip-hopové
videem	video	k1gNnSc7	video
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
audio	audio	k2eAgMnPc4d1	audio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
enormní	enormní	k2eAgFnSc7d1	enormní
popularitou	popularita	k1gFnSc7	popularita
Eminemova	Eminemův	k2eAgInSc2d1	Eminemův
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
kontroverze	kontroverze	k1gFnSc1	kontroverze
okolo	okolo	k7c2	okolo
jeho	jeho	k3xOp3gFnSc2	jeho
osoby	osoba	k1gFnSc2	osoba
ještě	ještě	k6eAd1	ještě
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
Grammy	Gramma	k1gFnPc4	Gramma
za	za	k7c4	za
Album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Mathers	Mathers	k1gInSc1	Mathers
vždy	vždy	k6eAd1	vždy
prohlašoval	prohlašovat	k5eAaImAgInS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
texty	text	k1gInPc1	text
nemají	mít	k5eNaImIp3nP	mít
brát	brát	k5eAaImF	brát
příliš	příliš	k6eAd1	příliš
vážně	vážně	k6eAd1	vážně
a	a	k8xC	a
že	že	k8xS	že
proti	proti	k7c3	proti
homosexuálům	homosexuál	k1gMnPc3	homosexuál
a	a	k8xC	a
ženám	žena	k1gFnPc3	žena
nic	nic	k3yNnSc1	nic
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnPc4	sdružení
homosexuálů	homosexuál	k1gMnPc2	homosexuál
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gNnSc3	on
bojkotovalo	bojkotovat	k5eAaImAgNnS	bojkotovat
předávání	předávání	k1gNnSc1	předávání
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Mathers	Mathers	k6eAd1	Mathers
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
vystoupením	vystoupení	k1gNnSc7	vystoupení
s	s	k7c7	s
homosexuálem	homosexuál	k1gMnSc7	homosexuál
Eltonem	Elton	k1gMnSc7	Elton
Johnem	John	k1gMnSc7	John
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
zpívali	zpívat	k5eAaImAgMnP	zpívat
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Stan	stan	k1gInSc4	stan
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
objali	obejmout	k5eAaPmAgMnP	obejmout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
homosexuálům	homosexuál	k1gMnPc3	homosexuál
nic	nic	k6eAd1	nic
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
sice	sice	k8xC	sice
šokovalo	šokovat	k5eAaBmAgNnS	šokovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgMnPc4	všechen
kritiky	kritik	k1gMnPc4	kritik
to	ten	k3xDgNnSc1	ten
rozhodně	rozhodně	k6eAd1	rozhodně
neumlčelo	umlčet	k5eNaPmAgNnS	umlčet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Eminem	Emino	k1gNnSc7	Emino
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
klasické	klasický	k2eAgInPc1d1	klasický
vševědoucí	vševědoucí	k2eAgInPc1d1	vševědoucí
životopisy	životopis	k1gInPc1	životopis
různorodých	různorodý	k2eAgFnPc2d1	různorodá
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Shady	Shada	k1gFnPc1	Shada
Bizzness	Bizznessa	k1gFnPc2	Bizznessa
od	od	k7c2	od
jeho	on	k3xPp3gMnSc2	on
bývalého	bývalý	k2eAgMnSc2d1	bývalý
bodyguarda	bodyguard	k1gMnSc2	bodyguard
Byrona	Byron	k1gMnSc2	Byron
Williamse	Williams	k1gMnSc2	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Eminem	Emin	k1gMnSc7	Emin
napsal	napsat	k5eAaBmAgInS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
knihu	kniha	k1gFnSc4	kniha
jménem	jméno	k1gNnSc7	jméno
Angry	Angra	k1gMnSc2	Angra
Blonde	blond	k1gInSc5	blond
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
emoce	emoce	k1gFnPc1	emoce
a	a	k8xC	a
záměry	záměr	k1gInPc1	záměr
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
textů	text	k1gInPc2	text
z	z	k7c2	z
Marshall	Marshalla	k1gFnPc2	Marshalla
Mathers	Mathersa	k1gFnPc2	Mathersa
LP	LP	kA	LP
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
svůj	svůj	k3xOyFgInSc4	svůj
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
rapu	rap	k1gMnSc3	rap
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
členů	člen	k1gMnPc2	člen
rapové	rapový	k2eAgFnSc2d1	rapová
skupiny	skupina	k1gFnSc2	skupina
D12	D12	k1gFnSc2	D12
se	se	k3xPyFc4	se
Eminem	Emin	k1gInSc7	Emin
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
albu	album	k1gNnSc6	album
Devil	Devil	k1gInSc4	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Night	k1gInSc1	Night
<g/>
,	,	kIx,	,
vydaném	vydaný	k2eAgInSc6d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
několikrát	několikrát	k6eAd1	několikrát
platinové	platinový	k2eAgNnSc1d1	platinové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eminemovo	Eminemův	k2eAgNnSc4d1	Eminemovo
páté	pátý	k4xOgNnSc4	pátý
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Eminem	Emino	k1gNnSc7	Emino
Show	show	k1gFnSc2	show
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Without	Without	k1gMnSc1	Without
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eminem	Emin	k1gInSc7	Emin
dělá	dělat	k5eAaImIp3nS	dělat
urážlivé	urážlivý	k2eAgFnPc4d1	urážlivá
poznámky	poznámka	k1gFnPc4	poznámka
o	o	k7c6	o
chlapeckých	chlapecký	k2eAgFnPc6d1	chlapecká
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
Mobym	Mobym	k1gInSc1	Mobym
<g/>
,	,	kIx,	,
Lynne	Lynn	k1gInSc5	Lynn
Cheney	Chenea	k1gMnSc2	Chenea
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
přilila	přilít	k5eAaPmAgFnS	přilít
olej	olej	k1gInSc4	olej
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
tiskové	tiskový	k2eAgFnSc2d1	tisková
konference	konference	k1gFnSc2	konference
časopisu	časopis	k1gInSc2	časopis
The	The	k1gMnSc2	The
Source	Source	k1gMnSc2	Source
a	a	k8xC	a
páska	pásek	k1gMnSc2	pásek
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
přehraná	přehraný	k2eAgFnSc1d1	přehraná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kazetě	kazeta	k1gFnSc6	kazeta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
Mathersův	Mathersův	k2eAgInSc1d1	Mathersův
freestyle	freestyl	k1gInSc5	freestyl
rap	rapa	k1gFnPc2	rapa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
urážel	urážet	k5eAaImAgMnS	urážet
černošky	černošek	k1gMnPc4	černošek
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
hloupé	hloupý	k2eAgNnSc4d1	hloupé
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
bílými	bílý	k2eAgFnPc7d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
nahrávku	nahrávka	k1gFnSc4	nahrávka
údajně	údajně	k6eAd1	údajně
pořídil	pořídit	k5eAaPmAgMnS	pořídit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
černošskou	černošský	k2eAgFnSc7d1	černošská
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prověřovala	prověřovat	k5eAaImAgNnP	prověřovat
tvrzení	tvrzení	k1gNnPc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mathers	Mathers	k1gInSc1	Mathers
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
We	We	k1gFnSc1	We
as	as	k1gInSc1	as
Americans	Americans	k1gInSc1	Americans
<g/>
"	"	kIx"	"
unikla	uniknout	k5eAaPmAgFnS	uniknout
neoficiálně	oficiálně	k6eNd1	oficiálně
na	na	k7c4	na
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Fuck	Fuck	k1gInSc1	Fuck
money	monea	k1gFnSc2	monea
<g/>
/	/	kIx~	/
<g/>
I	i	k9	i
don	don	k1gMnSc1	don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
rap	rap	k1gMnSc1	rap
for	forum	k1gNnPc2	forum
dead	dead	k6eAd1	dead
presidents	presidents	k6eAd1	presidents
<g/>
/	/	kIx~	/
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
rather	rathra	k1gFnPc2	rathra
see	see	k?	see
the	the	k?	the
president	president	k1gMnSc1	president
dead	dead	k1gMnSc1	dead
<g/>
/	/	kIx~	/
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
never	never	k1gMnSc1	never
been	been	k1gMnSc1	been
said	said	k1gMnSc1	said
<g/>
,	,	kIx,	,
but	but	k?	but
I	i	k9	i
set	set	k1gInSc4	set
precedents	precedents	k1gInSc1	precedents
(	(	kIx(	(
<g/>
Seru	srát	k5eAaImIp1nS	srát
na	na	k7c4	na
peníze	peníz	k1gInPc4	peníz
<g/>
/	/	kIx~	/
<g/>
Nerapuju	rapovat	k5eNaImIp1nS	rapovat
pro	pro	k7c4	pro
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
prezidenty	prezident	k1gMnPc7	prezident
<g/>
/	/	kIx~	/
<g/>
Radši	rád	k6eAd2	rád
bych	by	kYmCp1nS	by
viděl	vidět	k5eAaImAgMnS	vidět
prezidenta	prezident	k1gMnSc4	prezident
mrtvýho	mrtvýho	k?	mrtvýho
<g/>
/	/	kIx~	/
<g/>
Nemluví	mluvit	k5eNaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
já	já	k3xPp1nSc1	já
vytvářím	vytvářit	k5eAaPmIp1nS	vytvářit
precedenty	precedent	k1gInPc4	precedent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Eminem	Emin	k1gMnSc7	Emin
natočil	natočit	k5eAaBmAgMnS	natočit
video	video	k1gNnSc4	video
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Band	banda	k1gFnPc2	banda
<g/>
"	"	kIx"	"
s	s	k7c7	s
D	D	kA	D
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
odpovědí	odpověď	k1gFnSc7	odpověď
časté	častý	k2eAgNnSc4d1	časté
označování	označování	k1gNnSc4	označování
medií	medium	k1gNnPc2	medium
D12	D12	k1gFnSc2	D12
jako	jako	k8xC	jako
Eminemovy	Eminemův	k2eAgFnSc2d1	Eminemova
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc4	množství
parodií	parodie	k1gFnPc2	parodie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
známého	známý	k2eAgInSc2d1	známý
incidentu	incident	k1gInSc2	incident
Janet	Janet	k1gMnSc1	Janet
Jackson	Jackson	k1gMnSc1	Jackson
o	o	k7c6	o
přestávce	přestávka	k1gFnSc6	přestávka
Super	super	k2eAgInSc2d1	super
Bowlu	Bowl	k1gInSc2	Bowl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Lose	los	k1gMnSc5	los
It	It	k1gMnSc5	It
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prvního	první	k4xOgNnSc2	první
videa	video	k1gNnSc2	video
a	a	k8xC	a
singlu	singl	k1gInSc2	singl
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
Encore	Encor	k1gInSc5	Encor
<g/>
,	,	kIx,	,
volal	volat	k5eAaImAgMnS	volat
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
Los	los	k1gInSc4	los
Angeleské	Angeleský	k2eAgNnSc4d1	Angeleský
radiové	radiový	k2eAgNnSc4d1	radiové
show	show	k1gNnSc4	show
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
videem	video	k1gNnSc7	video
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
paroduje	parodovat	k5eAaImIp3nS	parodovat
Jacksonova	Jacksonův	k2eAgNnPc4d1	Jacksonovo
obvinění	obvinění	k1gNnPc4	obvinění
ze	z	k7c2	z
zneužívání	zneužívání	k1gNnSc2	zneužívání
nezletilých	nezletilý	k1gMnPc2	nezletilý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
plastiku	plastika	k1gFnSc4	plastika
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
Jacksonovi	Jacksonův	k2eAgMnPc1d1	Jacksonův
chytly	chytnout	k5eAaPmAgInP	chytnout
vlasy	vlas	k1gInPc4	vlas
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c6	na
Pepsi	Pepsi	k1gFnSc6	Pepsi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
MTV	MTV	kA	MTV
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
videa	video	k1gNnSc2	video
a	a	k8xC	a
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Lose	los	k1gInSc6	los
It	It	k1gMnPc1	It
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejžádanějším	žádaný	k2eAgInSc7d3	nejžádanější
klipem	klip	k1gInSc7	klip
<g/>
.	.	kIx.	.
</s>
<s>
CEO	CEO	kA	CEO
časopisu	časopis	k1gInSc2	časopis
The	The	k1gFnSc2	The
Source	Source	k1gMnSc1	Source
Raymond	Raymond	k1gMnSc1	Raymond
"	"	kIx"	"
<g/>
Benzino	Benzino	k1gNnSc1	Benzino
<g/>
"	"	kIx"	"
Scott	Scott	k1gMnSc1	Scott
chtěl	chtít	k5eAaImAgMnS	chtít
nejen	nejen	k6eAd1	nejen
zastavit	zastavit	k5eAaPmF	zastavit
vysílání	vysílání	k1gNnSc4	vysílání
klipu	klip	k1gInSc2	klip
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
odstranění	odstranění	k1gNnSc1	odstranění
skladby	skladba	k1gFnSc2	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
a	a	k8xC	a
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
omluvu	omluva	k1gFnSc4	omluva
Jacksonovi	Jackson	k1gMnSc3	Jackson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Týden	týden	k1gInSc1	týden
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Eminem	Emin	k1gInSc7	Emin
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
videoklip	videoklip	k1gInSc4	videoklip
své	svůj	k3xOyFgFnSc2	svůj
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Mosh	Mosh	k1gInSc1	Mosh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
proti	proti	k7c3	proti
G.	G.	kA	G.
W.	W.	kA	W.
Bushovi	Bush	k1gMnSc6	Bush
<g/>
,	,	kIx,	,
s	s	k7c7	s
texty	text	k1gInPc7	text
jako	jako	k9	jako
fuck	fuck	k1gInSc1	fuck
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
serte	srát	k5eAaImRp2nP	srát
na	na	k7c4	na
Bushe	Bush	k1gMnSc4	Bush
<g/>
)	)	kIx)	)
a	a	k8xC	a
this	this	k6eAd1	this
weapon	weapon	k1gInSc1	weapon
of	of	k?	of
mass	mass	k1gInSc1	mass
destruction	destruction	k1gInSc1	destruction
that	that	k1gInSc1	that
we	we	k?	we
call	call	k1gMnSc1	call
our	our	k?	our
president	president	k1gMnSc1	president
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
zbraň	zbraň	k1gFnSc1	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
říkáme	říkat	k5eAaImIp1nP	říkat
náš	náš	k3xOp1gMnSc1	náš
prezident	prezident	k1gMnSc1	prezident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
videu	video	k1gNnSc6	video
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
Eminem	Emin	k1gInSc7	Emin
armádu	armáda	k1gFnSc4	armáda
lidí	člověk	k1gMnPc2	člověk
prezentovaných	prezentovaný	k2eAgFnPc2d1	prezentovaná
jako	jako	k9	jako
oběti	oběť	k1gFnPc1	oběť
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
administrativy	administrativa	k1gFnSc2	administrativa
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
je	on	k3xPp3gFnPc4	on
k	k	k7c3	k
Bílému	bílý	k2eAgInSc3d1	bílý
domu	dům	k1gInSc3	dům
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
pouze	pouze	k6eAd1	pouze
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
k	k	k7c3	k
volbám	volba	k1gFnPc3	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
videoklip	videoklip	k1gInSc1	videoklip
končí	končit	k5eAaImIp3nS	končit
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
VOLTE	volit	k5eAaImRp2nP	volit
v	v	k7c4	v
úterý	úterý	k1gNnSc4	úterý
2	[number]	k4	2
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
Eminem	Emin	k1gMnSc7	Emin
debutoval	debutovat	k5eAaBmAgInS	debutovat
na	na	k7c6	na
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
plátně	plátno	k1gNnSc6	plátno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
částečně	částečně	k6eAd1	částečně
autobiografickým	autobiografický	k2eAgInSc7d1	autobiografický
snímkem	snímek	k1gInSc7	snímek
8	[number]	k4	8
<g/>
.	.	kIx.	.
míle	míle	k1gFnSc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
nahrál	nahrát	k5eAaPmAgMnS	nahrát
několik	několik	k4yIc4	několik
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
včetně	včetně	k7c2	včetně
"	"	kIx"	"
<g/>
Lose	los	k1gInSc6	los
Yourself	Yourself	k1gInSc1	Yourself
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eminem	Emin	k1gInSc7	Emin
také	také	k9	také
vlastní	vlastní	k2eAgNnSc4d1	vlastní
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Shady	Shada	k1gFnSc2	Shada
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
které	který	k3yRgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
50	[number]	k4	50
Cent	cent	k1gInSc4	cent
<g/>
,	,	kIx,	,
D12	D12	k1gFnSc4	D12
a	a	k8xC	a
Obie	Obie	k1gFnSc4	Obie
Trice	Trice	k1gFnSc2	Trice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Eminemovi	Eminemův	k2eAgMnPc1d1	Eminemův
nepřátelé	nepřítel	k1gMnPc1	nepřítel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Eminem	Emin	k1gMnSc7	Emin
a	a	k8xC	a
Xzibit	Xzibit	k1gMnSc7	Xzibit
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Canibus	Canibus	k1gInSc1	Canibus
a	a	k8xC	a
Jermaine	Jermain	k1gMnSc5	Jermain
Dupri	Dupr	k1gMnSc5	Dupr
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Say	Say	k1gFnSc6	Say
What	What	k2eAgMnSc1d1	What
You	You	k1gMnSc1	You
Say	Say	k1gMnSc1	Say
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Eminem	Emino	k1gNnSc7	Emino
Show	show	k1gFnSc1	show
urazil	urazit	k5eAaPmAgMnS	urazit
Jermaine	Jermain	k1gInSc5	Jermain
Dupriho	Dupriha	k1gFnSc5	Dupriha
větou	věta	k1gFnSc7	věta
Fuck	Fuck	k1gInSc1	Fuck
Jermaine	Jermain	k1gInSc5	Jermain
...	...	k?	...
over	over	k1gInSc4	over
80	[number]	k4	80
million	million	k1gInSc1	million
records	records	k6eAd1	records
sold	sold	k1gInSc4	sold
<g/>
/	/	kIx~	/
<g/>
And	Anda	k1gFnPc2	Anda
I	i	k8xC	i
ain	ain	k?	ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
had	had	k1gMnSc1	had
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
it	it	k?	it
with	with	k1gInSc1	with
10	[number]	k4	10
and	and	k?	and
11	[number]	k4	11
year	year	k1gInSc1	year
olds	olds	k1gInSc4	olds
(	(	kIx(	(
<g/>
Seru	srát	k5eAaImIp1nS	srát
na	na	k7c4	na
Jermaina	Jermaino	k1gNnPc4	Jermaino
...	...	k?	...
přes	přes	k7c4	přes
80	[number]	k4	80
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgFnPc2d1	prodaná
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokážu	dokázat	k5eAaPmIp1nS	dokázat
bez	bez	k7c2	bez
10	[number]	k4	10
nebo	nebo	k8xC	nebo
11	[number]	k4	11
<g/>
letejch	letejch	k?	letejch
děcek	děcko	k1gNnPc2	děcko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
tak	tak	k9	tak
na	na	k7c4	na
Dupriho	Dupri	k1gMnSc4	Dupri
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vydělala	vydělat	k5eAaPmAgFnS	vydělat
miliony	milion	k4xCgInPc4	milion
na	na	k7c6	na
dětských	dětský	k2eAgFnPc6d1	dětská
rapových	rapový	k2eAgFnPc6d1	rapová
hvězdách	hvězda	k1gFnPc6	hvězda
jako	jako	k8xC	jako
Kris	kris	k1gInSc4	kris
Kross	Krossa	k1gFnPc2	Krossa
a	a	k8xC	a
Lil	lít	k5eAaImAgMnS	lít
Bow	Bow	k1gMnSc1	Bow
Wow	Wow	k1gMnSc1	Wow
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gMnSc7	Emin
a	a	k8xC	a
Xzibit	Xzibit	k1gMnPc1	Xzibit
urazili	urazit	k5eAaPmAgMnP	urazit
podobně	podobně	k6eAd1	podobně
Dupriho	Dupri	k1gMnSc2	Dupri
(	(	kIx(	(
<g/>
a	a	k8xC	a
Canibuse	Canibuse	k1gFnSc1	Canibuse
<g/>
)	)	kIx)	)
na	na	k7c6	na
mixu	mix	k1gInSc6	mix
DJe	DJe	k1gMnSc2	DJe
Kay	Kay	k1gMnSc2	Kay
Slay	Slaa	k1gMnSc2	Slaa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Eminem	Emin	k1gInSc7	Emin
rapuje	rapovat	k5eAaImIp3nS	rapovat
I	i	k9	i
gave	gave	k1gFnSc1	gave
Jay-Z	Jay-Z	k1gFnSc1	Jay-Z
a	a	k8xC	a
beat	beat	k1gInSc1	beat
for	forum	k1gNnPc2	forum
free	fre	k1gFnSc2	fre
<g/>
,	,	kIx,	,
you	you	k?	you
want	want	k1gInSc1	want
one	one	k?	one
-	-	kIx~	-
tell	tell	k1gMnSc1	tell
me	me	k?	me
<g/>
,	,	kIx,	,
unless	unless	k1gInSc1	unless
you	you	k?	you
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
canibitch	canibitch	k1gMnSc1	canibitch
<g/>
,	,	kIx,	,
then	then	k1gMnSc1	then
you	you	k?	you
get	get	k?	get
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
fee	fee	k?	fee
<g/>
.	.	kIx.	.
</s>
<s>
Dupri	Dupri	k6eAd1	Dupri
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
obviněním	obvinění	k1gNnSc7	obvinění
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc2	Dre
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
přivlastňuje	přivlastňovat	k5eAaImIp3nS	přivlastňovat
cizí	cizí	k2eAgFnPc4d1	cizí
zásluhy	zásluha	k1gFnPc4	zásluha
-	-	kIx~	-
I	i	k9	i
know	know	k?	know
you	you	k?	you
don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
do	do	k7c2	do
half	halfa	k1gFnPc2	halfa
the	the	k?	the
work	work	k1gMnSc1	work
in	in	k?	in
the	the	k?	the
studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
plus	plus	k1gNnSc1	plus
your	youra	k1gFnPc2	youra
little	littlat	k5eAaPmIp3nS	littlat
niggas	niggas	k1gInSc1	niggas
play	play	k0	play
with	with	k1gInSc1	with
your	your	k1gInSc4	your
booty	boota	k1gFnSc2	boota
hole	hole	k6eAd1	hole
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
urážkou	urážka	k1gFnSc7	urážka
Eminema	Eminemum	k1gNnSc2	Eminemum
větou	věta	k1gFnSc7	věta
I	i	k9	i
left	left	k1gMnSc1	left
<g />
.	.	kIx.	.
</s>
<s>
you	you	k?	you
out	out	k?	out
deliberately	deliberatela	k1gFnSc2	deliberatela
<g/>
,	,	kIx,	,
you	you	k?	you
know	know	k?	know
why	why	k?	why
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
me	me	k?	me
you	you	k?	you
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
like	like	k1gNnSc1	like
a	a	k8xC	a
character	character	k1gInSc1	character
in	in	k?	in
Disney	Disnea	k1gFnSc2	Disnea
world	world	k1gMnSc1	world
<g/>
,	,	kIx,	,
known	known	k1gMnSc1	known
for	forum	k1gNnPc2	forum
dissin	dissin	k1gInSc1	dissin
<g/>
'	'	kIx"	'
pop	pop	k1gMnSc1	pop
groups	groupsa	k1gFnPc2	groupsa
and	and	k?	and
Justin	Justina	k1gFnPc2	Justina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
ex-girl	exirl	k1gInSc1	ex-girl
<g/>
,	,	kIx,	,
shit	shit	k1gInSc1	shit
<g/>
,	,	kIx,	,
ain	ain	k?	ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
no	no	k9	no
one	one	k?	one
take	take	k1gInSc1	take
you	you	k?	you
seriously	seriousnout	k5eAaPmAgFnP	seriousnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eminem	Emin	k1gMnSc7	Emin
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Benzino	Benzino	k1gNnSc1	Benzino
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bostonský	bostonský	k2eAgInSc4d1	bostonský
rapper	rapper	k1gInSc4	rapper
Benzino	Benzino	k1gNnSc4	Benzino
vydal	vydat	k5eAaPmAgInS	vydat
skladbu	skladba	k1gFnSc4	skladba
s	s	k7c7	s
textem	text	k1gInSc7	text
You	You	k1gMnPc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
the	the	k?	the
rap	rap	k1gMnSc1	rap
David	David	k1gMnSc1	David
Duke	Duke	k1gFnSc1	Duke
<g/>
/	/	kIx~	/
<g/>
The	The	k1gMnSc1	The
rap	rap	k1gMnSc1	rap
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
...	...	k?	...
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
the	the	k?	the
rap	rap	k1gMnSc1	rap
Malcolm	Malcolm	k1gMnSc1	Malcolm
<g/>
,	,	kIx,	,
the	the	k?	the
rap	rap	k1gMnSc1	rap
Martin	Martin	k1gMnSc1	Martin
(	(	kIx(	(
<g/>
Seš	Seš	k?	Seš
rapovej	rapovej	k?	rapovej
David	David	k1gMnSc1	David
Duke	Duk	k1gFnSc2	Duk
<g/>
/	/	kIx~	/
<g/>
Rapovej	Rapovej	k?	Rapovej
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
...	...	k?	...
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
rapovej	rapovej	k?	rapovej
Malcolm	Malcolm	k1gInSc1	Malcolm
<g/>
,	,	kIx,	,
rapovej	rapovej	k?	rapovej
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
namířenou	namířený	k2eAgFnSc4d1	namířená
proti	proti	k7c3	proti
Eminemovi	Eminem	k1gMnSc3	Eminem
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emino	k1gNnSc7	Emino
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
Benzina	Benzina	k1gMnSc1	Benzina
"	"	kIx"	"
<g/>
83	[number]	k4	83
<g/>
letým	letý	k2eAgInSc7d1	letý
umělým	umělý	k2eAgInSc7d1	umělý
Al	ala	k1gFnPc2	ala
Pacinem	Pacino	k1gNnSc7	Pacino
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Benzino	Benzino	k1gNnSc4	Benzino
popisoval	popisovat	k5eAaImAgMnS	popisovat
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eminemova	Eminemův	k2eAgFnSc1d1	Eminemova
sláva	sláva	k1gFnSc1	sláva
je	být	k5eAaImIp3nS	být
počátkem	počátkem	k7c2	počátkem
konce	konec	k1gInSc2	konec
afro-americké	afromerický	k2eAgFnSc2d1	afro-americká
dominance	dominance	k1gFnSc2	dominance
v	v	k7c6	v
hip-hopu	hipop	k1gInSc6	hip-hop
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Eminema	Eminema	k1gNnSc4	Eminema
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
konzumností	konzumnost	k1gFnSc7	konzumnost
moderního	moderní	k2eAgInSc2d1	moderní
hip-hopu	hipop	k1gInSc2	hip-hop
a	a	k8xC	a
stěžoval	stěžovat	k5eAaImAgInS	stěžovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
musí	muset	k5eAaImIp3nS	muset
rapovat	rapovat	k5eAaImF	rapovat
jen	jen	k9	jen
žvásty	žvásta	k1gFnPc4	žvásta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
posluchači	posluchač	k1gMnPc1	posluchač
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Eminem	Emino	k1gNnSc7	Emino
přitom	přitom	k6eAd1	přitom
může	moct	k5eAaImIp3nS	moct
rapovat	rapovat	k5eAaImF	rapovat
o	o	k7c6	o
hlubokých	hluboký	k2eAgInPc6d1	hluboký
osobních	osobní	k2eAgInPc6d1	osobní
problémech	problém	k1gInPc6	problém
a	a	k8xC	a
pocitech	pocit	k1gInPc6	pocit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
Benzina	Benzin	k1gMnSc4	Benzin
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hádkami	hádka	k1gFnPc7	hádka
s	s	k7c7	s
Eminemem	Eminem	k1gInSc7	Eminem
snaží	snažit	k5eAaImIp3nS	snažit
jen	jen	k9	jen
přilákat	přilákat	k5eAaPmF	přilákat
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eminem	Emin	k1gMnSc7	Emin
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Everlast	Everlast	k1gInSc1	Everlast
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
Dilated	Dilated	k1gMnSc1	Dilated
Peoples	Peoples	k1gMnSc1	Peoples
urazil	urazit	k5eAaPmAgMnS	urazit
hip-hopový	hipopový	k2eAgMnSc1d1	hip-hopový
<g/>
/	/	kIx~	/
<g/>
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
Everlast	Everlast	k1gInSc4	Everlast
Eminema	Eminemum	k1gNnSc2	Eminemum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
I	i	k9	i
Remember	Remember	k1gInSc1	Remember
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
poněkud	poněkud	k6eAd1	poněkud
kontroverzně	kontroverzně	k6eAd1	kontroverzně
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gNnSc4	jeho
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
Islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Everlast	Everlast	k1gInSc4	Everlast
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Whitey	Whitea	k1gFnPc1	Whitea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Revenge	Revenge	k1gFnSc7	Revenge
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Eminem	Emino	k1gNnSc7	Emino
pak	pak	k8xC	pak
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Quitter	Quitter	k1gInSc1	Quitter
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spoustou	spousta	k1gFnSc7	spousta
nadávek	nadávka	k1gFnPc2	nadávka
a	a	k8xC	a
účastí	účast	k1gFnSc7	účast
D-	D-	k1gFnSc2	D-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eminem	Emino	k1gNnSc7	Emino
a	a	k8xC	a
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Ja	Ja	k?	Ja
Rule	rula	k1gFnSc3	rula
===	===	k?	===
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
rivality	rivalita	k1gFnSc2	rivalita
mezi	mezi	k7c7	mezi
Eminemem	Eminem	k1gInSc7	Eminem
a	a	k8xC	a
Ja	Ja	k?	Ja
Rulem	Rulem	k1gInSc1	Rulem
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jistě	jistě	k6eAd1	jistě
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
MTV	MTV	kA	MTV
vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Ja	Ja	k?	Ja
Rule	rula	k1gFnSc3	rula
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
jeho	jeho	k3xOp3gNnSc3	jeho
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
<g/>
,	,	kIx,	,
Irv	Irv	k1gFnSc3	Irv
Gotti	Gotť	k1gFnSc2	Gotť
<g/>
,	,	kIx,	,
fyzicky	fyzicky	k6eAd1	fyzicky
napadli	napadnout	k5eAaPmAgMnP	napadnout
50	[number]	k4	50
Centa	Cent	k1gInSc2	Cent
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Ja	Ja	k?	Ja
Rule	rula	k1gFnSc6	rula
urazil	urazit	k5eAaPmAgInS	urazit
Eminema	Eminema	k1gNnSc4	Eminema
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
skladbě	skladba	k1gFnSc6	skladba
<g/>
,	,	kIx,	,
odpověděli	odpovědět	k5eAaPmAgMnP	odpovědět
Eminem	Emino	k1gNnSc7	Emino
a	a	k8xC	a
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
opět	opět	k6eAd1	opět
mixem	mix	k1gInSc7	mix
od	od	k7c2	od
DJe	DJe	k1gFnSc2	DJe
Kay	Kay	k1gMnSc2	Kay
Slay	Slaa	k1gMnSc2	Slaa
–	–	k?	–
Irv	Irv	k1gFnSc2	Irv
Gotti	Gotť	k1gFnSc2	Gotť
<g/>
,	,	kIx,	,
too	too	k?	too
much	moucha	k1gFnPc2	moucha
Bacardi	Bacard	k1gMnPc1	Bacard
in	in	k?	in
his	his	k1gNnSc2	his
body	bod	k1gInPc7	bod
<g/>
/	/	kIx~	/
<g/>
You	You	k1gFnSc1	You
ain	ain	k?	ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
a	a	k8xC	a
killer	killer	k1gMnSc1	killer
<g/>
,	,	kIx,	,
you	you	k?	you
a	a	k8xC	a
pussy	pussa	k1gFnSc2	pussa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
That	That	k1gInSc1	That
ecstasy	ecstasa	k1gFnSc2	ecstasa
got	got	k?	got
you	you	k?	you
all	all	k?	all
emotional	emotionat	k5eAaImAgMnS	emotionat
and	and	k?	and
mushy	musha	k1gFnPc4	musha
<g/>
/	/	kIx~	/
<g/>
Bitches	Bitches	k1gInSc1	Bitches
wearing	wearing	k1gInSc1	wearing
rags	rags	k1gInSc1	rags
in	in	k?	in
photos	photos	k1gInSc1	photos
<g/>
/	/	kIx~	/
<g/>
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
word	word	k1gMnSc1	word
being	being	k1gMnSc1	being
quoted	quoted	k1gMnSc1	quoted
<g/>
/	/	kIx~	/
<g/>
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Source	Source	k1gMnSc1	Source
<g/>
,	,	kIx,	,
stealing	stealing	k1gInSc1	stealing
Pac	pac	k1gInSc1	pac
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
shit	shita	k1gFnPc2	shita
like	like	k1gNnSc1	like
he	he	k0	he
just	just	k6eAd1	just
wrote	wrote	k5eAaPmIp2nP	wrote
it.	it.	k?	it.
Eminem	Emin	k1gMnSc7	Emin
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
mixu	mix	k1gInSc6	mix
od	od	k7c2	od
DJe	DJe	k1gFnSc2	DJe
Green	Green	k2eAgInSc4d1	Green
Lantern	Lantern	k1gInSc4	Lantern
společně	společně	k6eAd1	společně
s	s	k7c7	s
Busta	busta	k1gFnSc1	busta
Rhymesem	Rhymes	k1gMnSc7	Rhymes
a	a	k8xC	a
urazili	urazit	k5eAaPmAgMnP	urazit
Ja	Ja	k?	Ja
Rulea	Rulea	k1gMnSc1	Rulea
<g/>
,	,	kIx,	,
Murder	Murder	k1gMnSc1	Murder
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Gottiho	Gotti	k1gMnSc2	Gotti
a	a	k8xC	a
Ja	Ja	k?	Ja
Ruleovo	Ruleův	k2eAgNnSc1d1	Ruleův
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Benzina	Benzina	k1gFnSc1	Benzina
a	a	k8xC	a
Royce	Royce	k1gFnSc1	Royce
Da	Da	k1gFnSc1	Da
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
9	[number]	k4	9
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ja	Ja	k?	Ja
Rule	rula	k1gFnSc3	rula
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
textem	text	k1gInSc7	text
Em	Ema	k1gFnPc2	Ema
<g/>
,	,	kIx,	,
you	you	k?	you
claim	claim	k1gInSc1	claim
your	your	k1gMnSc1	your
mother	mothra	k1gFnPc2	mothra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
crackhead	crackhead	k6eAd1	crackhead
<g/>
/	/	kIx~	/
And	Anda	k1gFnPc2	Anda
Kim	Kim	k1gFnPc2	Kim
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
known	known	k1gMnSc1	known
slut	slut	k2eAgMnSc1d1	slut
<g/>
/	/	kIx~	/
So	So	kA	So
what	what	k1gInSc1	what
Hailie	Hailie	k1gFnSc1	Hailie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
gonna	gonna	k6eAd1	gonna
be	be	k?	be
when	when	k1gInSc1	when
she	she	k?	she
grows	grows	k1gInSc1	grows
up	up	k?	up
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Eme	Eme	k1gMnSc5	Eme
<g/>
,	,	kIx,	,
tvrdíš	tvrdit	k5eAaImIp2nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoje	tvůj	k3xOp2gFnSc1	tvůj
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
feťačka	feťačka	k1gFnSc1	feťačka
<g/>
/	/	kIx~	/
<g/>
A	a	k8xC	a
Kim	Kim	k1gFnSc1	Kim
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
děvka	děvka	k1gFnSc1	děvka
<g/>
/	/	kIx~	/
<g/>
Tak	tak	k9	tak
co	co	k3yQnSc4	co
asi	asi	k9	asi
bude	být	k5eAaImBp3nS	být
z	z	k7c2	z
Hailie	Hailie	k1gFnSc2	Hailie
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vyroste	vyrůst	k5eAaPmIp3nS	vyrůst
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gMnSc7	Emin
vzápětí	vzápětí	k6eAd1	vzápětí
nahrál	nahrát	k5eAaPmAgMnS	nahrát
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Doe	Doe	k1gMnSc1	Doe
Rae	Rae	k1gMnSc1	Rae
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hailie	Hailie	k1gFnSc1	Hailie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Revenge	Revenge	k1gFnSc7	Revenge
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Hailie	Hailie	k1gFnSc1	Hailie
a	a	k8xC	a
D	D	kA	D
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
dělají	dělat	k5eAaImIp3nP	dělat
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
Ja	Ja	k?	Ja
Rulovy	Rulovy	k?	Rulovy
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hailie	Hailie	k1gFnSc1	Hailie
říká	říkat	k5eAaImIp3nS	říkat
Daddy	Dadda	k1gFnPc4	Dadda
is	is	k?	is
Ja	Ja	k?	Ja
Rule	rula	k1gFnSc3	rula
taller	taller	k1gMnSc1	taller
than	than	k1gMnSc1	than
me	me	k?	me
<g/>
?	?	kIx.	?
</s>
<s>
No	no	k9	no
honey	honey	k1gInPc1	honey
you	you	k?	you
guys	guys	k1gInSc1	guys
are	ar	k1gInSc5	ar
the	the	k?	the
same	sam	k1gMnSc4	sam
size	siz	k1gMnSc4	siz
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tati	tati	k1gMnSc1	tati
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Ja	Ja	k?	Ja
Rule	rula	k1gFnSc6	rula
větší	veliký	k2eAgFnSc6d2	veliký
než	než	k8xS	než
já	já	k3xPp1nSc1	já
<g/>
?	?	kIx.	?
</s>
<s>
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
jste	být	k5eAaImIp2nP	být
oba	dva	k4xCgMnPc1	dva
stejně	stejně	k6eAd1	stejně
velcí	velký	k2eAgMnPc1d1	velký
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eminem	Emin	k1gMnSc7	Emin
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Machine	Machinout	k5eAaPmIp3nS	Machinout
Gun	Gun	k1gFnSc1	Gun
Kelly	Kella	k1gFnSc2	Kella
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eminem	Emino	k1gNnSc7	Emino
byl	být	k5eAaImAgInS	být
idol	idol	k1gInSc1	idol
Machine	Machin	k1gMnSc5	Machin
Gun	Gun	k1gMnSc5	Gun
Kellyho	Kellyha	k1gMnSc5	Kellyha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
,,	,,	k?	,,
<g/>
Beef	Beef	k1gInSc1	Beef
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
Eminemem	Eminem	k1gInSc7	Eminem
a	a	k8xC	a
Machine	Machin	k1gInSc5	Machin
Gun	Gun	k1gMnSc4	Gun
Kellym	Kellym	k1gInSc1	Kellym
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
velký	velký	k2eAgMnSc1d1	velký
fanoušek	fanoušek	k1gMnSc1	fanoušek
MGK	MGK	kA	MGK
napsal	napsat	k5eAaBmAgMnS	napsat
na	na	k7c4	na
Twitter	Twitter	k1gInSc4	Twitter
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
ok	oka	k1gFnPc2	oka
so	so	k?	so
i	i	k8xC	i
just	just	k6eAd1	just
saw	saw	k?	saw
a	a	k8xC	a
picture	pictur	k1gMnSc5	pictur
of	of	k?	of
Eminem	Emin	k1gInSc7	Emin
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
daughter	daughtra	k1gFnPc2	daughtra
<g/>
...	...	k?	...
and	and	k?	and
i	i	k9	i
have	havat	k5eAaPmIp3nS	havat
to	ten	k3xDgNnSc1	ten
say	say	k?	say
<g/>
,	,	kIx,	,
she	she	k?	she
is	is	k?	is
hot	hot	k0	hot
as	as	k9	as
fuck	fuck	k6eAd1	fuck
<g/>
,	,	kIx,	,
in	in	k?	in
the	the	k?	the
most	most	k1gInSc1	most
respectful	respectful	k1gInSc1	respectful
way	way	k?	way
possible	possible	k6eAd1	possible
cuz	cuz	k?	cuz
Em	Ema	k1gFnPc2	Ema
<g />
.	.	kIx.	.
</s>
<s>
is	is	k?	is
king	king	k1gInSc1	king
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
,,	,,	k?	,,
<g/>
ok	oka	k1gFnPc2	oka
zrovna	zrovna	k6eAd1	zrovna
jsem	být	k5eAaImIp1nS	být
viděl	vidět	k5eAaImAgMnS	vidět
obrázek	obrázek	k1gInSc4	obrázek
Eminemovi	Eminem	k1gMnSc6	Eminem
dcery	dcera	k1gFnSc2	dcera
<g/>
...	...	k?	...
a	a	k8xC	a
musím	muset	k5eAaImIp1nS	muset
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
hezká	hezký	k2eAgFnSc1d1	hezká
<g/>
,	,	kIx,	,
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
respektem	respekt	k1gInSc7	respekt
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
protože	protože	k8xS	protože
Eminem	Emin	k1gMnSc7	Emin
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
Hailie	Hailie	k1gFnSc2	Hailie
16	[number]	k4	16
let	léto	k1gNnPc2	léto
a	a	k8xC	a
MGKeyovi	MGKeyův	k2eAgMnPc1d1	MGKeyův
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tweet	tweet	k1gInSc1	tweet
ovšem	ovšem	k9	ovšem
nepotěšil	potěšit	k5eNaPmAgInS	potěšit
Eminema	Eminema	k1gNnSc4	Eminema
<g/>
.	.	kIx.	.
</s>
<s>
MGKeyovi	MGKeya	k1gMnSc3	MGKeya
písně	píseň	k1gFnSc2	píseň
odstřihl	odstřihnout	k5eAaPmAgMnS	odstřihnout
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
rádií	rádio	k1gNnPc2	rádio
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
jako	jako	k8xS	jako
pomstu	pomsta	k1gFnSc4	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
beef	beef	k1gInSc1	beef
utišil	utišit	k5eAaPmAgInS	utišit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
Machine	Machin	k1gInSc5	Machin
Gun	Gun	k1gFnSc7	Gun
Kelly	Kell	k1gInPc7	Kell
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
No	no	k9	no
Reason	Reason	k1gMnSc1	Reason
od	od	k7c2	od
Tech	Tech	k?	Tech
9	[number]	k4	9
<g/>
ina	ina	k?	ina
zmínil	zmínit	k5eAaPmAgMnS	zmínit
nepřímo	přímo	k6eNd1	přímo
Eminema	Eminem	k1gMnSc4	Eminem
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Popped	Popped	k1gInSc1	Popped
in	in	k?	in
on	on	k3xPp3gMnSc1	on
the	the	k?	the
top	topit	k5eAaImRp2nS	topit
charts	charts	k1gInSc1	charts
out	out	k?	out
the	the	k?	the
cop	cop	k1gInSc1	cop
car	car	k1gMnSc1	car
<g/>
/	/	kIx~	/
<g/>
to	ten	k3xDgNnSc1	ten
remind	remind	k1gInSc1	remind
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
all	all	k?	all
<g />
.	.	kIx.	.
</s>
<s>
you	you	k?	you
just	just	k6eAd1	just
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
you	you	k?	you
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
not	nota	k1gFnPc2	nota
God	God	k1gMnSc1	God
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Zjevil	zjevit	k5eAaPmAgMnS	zjevit
se	se	k3xPyFc4	se
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
hitparád	hitparáda	k1gFnPc2	hitparáda
z	z	k7c2	z
policejního	policejní	k2eAgNnSc2d1	policejní
auta	auto	k1gNnSc2	auto
<g/>
/	/	kIx~	/
připomenout	připomenout	k5eAaPmF	připomenout
všem	všecek	k3xTgMnPc3	všecek
že	že	k8xS	že
jen	jen	k6eAd1	jen
rapuješ	rapovat	k5eAaImIp2nS	rapovat
ale	ale	k8xC	ale
nejsi	být	k5eNaImIp2nS	být
bůh	bůh	k1gMnSc1	bůh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
Eminemuv	Eminemuv	k1gInSc4	Eminemuv
song	song	k1gInSc1	song
Rap	rapa	k1gFnPc2	rapa
God	God	k1gFnSc2	God
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
od	od	k7c2	od
Eminema	Eminemum	k1gNnSc2	Eminemum
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
desátém	desátý	k4xOgNnSc6	desátý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
Kamikaze	kamikaze	k1gMnSc1	kamikaze
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Not	nota	k1gFnPc2	nota
Alike	Alik	k1gFnSc2	Alik
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Kamikaze	kamikaze	k1gMnSc2	kamikaze
vydal	vydat	k5eAaPmAgMnS	vydat
Machine	Machin	k1gInSc5	Machin
Gun	Gun	k1gMnSc1	Gun
Kelly	Kella	k1gFnSc2	Kella
svůj	svůj	k3xOyFgInSc4	svůj
populární	populární	k2eAgInSc4d1	populární
disstrack	disstrack	k1gInSc4	disstrack
na	na	k7c4	na
Eminema	Eminema	k1gNnSc4	Eminema
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Rap	rap	k1gMnSc1	rap
Devil	Devil	k1gMnSc1	Devil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
což	což	k3yRnSc4	což
Eminem	Emino	k1gNnSc7	Emino
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
disstrackem	disstracko	k1gNnSc7	disstracko
Killshot	Killshota	k1gFnPc2	Killshota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mnozí	mnohý	k2eAgMnPc1d1	mnohý
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
ukončil	ukončit	k5eAaPmAgMnS	ukončit
MGKeyovi	MGKeya	k1gMnSc3	MGKeya
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Audio	audio	k2eAgFnSc1d1	audio
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
na	na	k7c4	na
Youtube	Youtub	k1gInSc5	Youtub
nabralo	nabrat	k5eAaPmAgNnS	nabrat
jen	jen	k9	jen
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
přes	přes	k7c4	přes
36	[number]	k4	36
miliónů	milión	k4xCgInPc2	milión
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
udělalo	udělat	k5eAaPmAgNnS	udělat
3	[number]	k4	3
<g/>
.	.	kIx.	.
nejsledovanější	sledovaný	k2eAgNnSc1d3	nejsledovanější
video	video	k1gNnSc1	video
na	na	k7c4	na
Youtube	Youtub	k1gInSc5	Youtub
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Eminem	Emin	k1gMnSc7	Emin
poté	poté	k6eAd1	poté
zmínil	zmínit	k5eAaPmAgMnS	zmínit
MGKeye	MGKeye	k1gFnSc4	MGKeye
2	[number]	k4	2
<g/>
x	x	k?	x
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
I	i	k9	i
don	don	k1gMnSc1	don
<g/>
́	́	k?	́
<g/>
t	t	k?	t
think	think	k1gInSc1	think
it	it	k?	it
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
cool	coonout	k5eAaPmAgInS	coonout
to	ten	k3xDgNnSc1	ten
disrespect	disrespect	k2eAgInSc1d1	disrespect
the	the	k?	the
dead	dead	k1gInSc1	dead
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
,,	,,	k?	,,
<g/>
Nemyslím	myslet	k5eNaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
urážet	urážet	k5eAaPmF	urážet
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnSc7d1	další
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
Eminemovy	Eminemův	k2eAgMnPc4d1	Eminemův
nepřátele	nepřítel	k1gMnPc4	nepřítel
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
jeho	jeho	k3xOp3gMnSc4	jeho
bývalého	bývalý	k2eAgMnSc4d1	bývalý
přítele	přítel	k1gMnSc4	přítel
Freda	Fred	k1gMnSc4	Fred
Dursta	Durst	k1gMnSc4	Durst
<g/>
,	,	kIx,	,
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k1gMnSc1	Bizkit
<g/>
,	,	kIx,	,
Mobyho	Moby	k1gMnSc2	Moby
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Insane	Insan	k1gMnSc5	Insan
Clown	Clown	k1gMnSc1	Clown
Posse	Posse	k1gFnSc2	Posse
–	–	k?	–
rapovou	rapový	k2eAgFnSc4d1	rapová
skupinu	skupina	k1gFnSc4	skupina
z	z	k7c2	z
Detroitu	Detroit	k1gInSc2	Detroit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Infinite	Infinit	k1gInSc5	Infinit
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
The	The	k1gMnPc2	The
Slim	Slim	k1gMnSc1	Slim
Shady	Shada	k1gFnSc2	Shada
LP	LP	kA	LP
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Eminem	Emin	k1gInSc7	Emin
Show	show	k1gFnSc2	show
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Encore	Encor	k1gMnSc5	Encor
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Relapse	relaps	k1gInSc5	relaps
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Recovery	Recovera	k1gFnSc2	Recovera
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Marshall	Marshall	k1gInSc4	Marshall
Mathers	Mathers	k1gInSc1	Mathers
LP	LP	kA	LP
2	[number]	k4	2
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Revival	revival	k1gInSc1	revival
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Kamikaze	kamikaze	k1gMnSc1	kamikaze
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
8	[number]	k4	8
Mile	mile	k6eAd1	mile
OST	OST	kA	OST
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Curtain	Curtain	k2eAgInSc1d1	Curtain
Call	Call	k1gInSc1	Call
<g/>
:	:	kIx,	:
the	the	k?	the
Hits	Hits	k1gInSc1	Hits
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Eminem	Emin	k1gInSc7	Emin
Presents	Presents	k1gInSc1	Presents
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Re-Up	Re-Up	k1gMnSc1	Re-Up
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Shady	Shada	k1gFnSc2	Shada
XV	XV	kA	XV
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Southpaw	Southpaw	k1gFnSc2	Southpaw
OST	OST	kA	OST
</s>
</p>
<p>
<s>
===	===	k?	===
Nevydané	vydaný	k2eNgMnPc4d1	nevydaný
===	===	k?	===
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Funeral	Funeral	k1gMnSc1	Funeral
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
King	King	k1gInSc1	King
Mathers	Mathersa	k1gFnPc2	Mathersa
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Relapse	relaps	k1gInSc5	relaps
2	[number]	k4	2
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eminem	Emin	k1gInSc7	Emin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
na	na	k7c4	na
IMDb	IMDb	k1gInSc4	IMDb
</s>
</p>
<p>
<s>
Eminem	Emino	k1gNnSc7	Emino
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Eminem	Emino	k1gNnSc7	Emino
Lyrics	Lyricsa	k1gFnPc2	Lyricsa
</s>
</p>
