<p>
<s>
Termokrasové	Termokrasový	k2eAgNnSc1d1	Termokrasový
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zmrznutím	zmrznutí	k1gNnSc7	zmrznutí
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
mělká	mělký	k2eAgNnPc1d1	mělké
jezera	jezero	k1gNnPc1	jezero
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
věčně	věčně	k6eAd1	věčně
zmrzlé	zmrzlý	k2eAgFnPc1d1	zmrzlá
půdy	půda	k1gFnPc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
podlouhlý	podlouhlý	k2eAgInSc4d1	podlouhlý
tvar	tvar	k1gInSc4	tvar
protažený	protažený	k2eAgInSc4d1	protažený
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
převládajících	převládající	k2eAgInPc2d1	převládající
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
jezera	jezero	k1gNnPc1	jezero
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
množstvích	množství	k1gNnPc6	množství
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
navzájem	navzájem	k6eAd1	navzájem
propojena	propojit	k5eAaPmNgFnS	propojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
alasové	alasový	k2eAgNnSc1d1	alasový
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
pingové	pingový	k2eAgNnSc1d1	pingový
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
