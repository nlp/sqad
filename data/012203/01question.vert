<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
druh	druh	k1gInSc1	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zmrznutím	zmrznutí	k1gNnSc7	zmrznutí
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
?	?	kIx.	?
</s>
