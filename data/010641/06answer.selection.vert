<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
archaický	archaický	k2eAgInSc4d1	archaický
a	a	k8xC	a
zastaralý	zastaralý	k2eAgInSc4d1	zastaralý
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
toto	tento	k3xDgNnSc1	tento
letadlo	letadlo	k1gNnSc1	letadlo
mnohé	mnohý	k2eAgFnSc2d1	mnohá
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
zejména	zejména	k9	zejména
poškozením	poškození	k1gNnSc7	poškození
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodi	loď	k1gFnSc2	loď
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
skočilo	skočit	k5eAaPmAgNnS	skočit
jejím	její	k3xOp3gNnSc7	její
následným	následný	k2eAgNnSc7d1	následné
potopením	potopení	k1gNnSc7	potopení
<g/>
.	.	kIx.	.
</s>
