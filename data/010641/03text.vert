<p>
<s>
Fairey	Faire	k2eAgInPc1d1	Faire
Swordfish	Swordfish	k1gInSc1	Swordfish
byl	být	k5eAaImAgInS	být
britský	britský	k2eAgInSc1d1	britský
jednomotorový	jednomotorový	k2eAgInSc1d1	jednomotorový
palubní	palubní	k2eAgInSc1d1	palubní
torpédový	torpédový	k2eAgInSc1d1	torpédový
letoun	letoun	k1gInSc1	letoun
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
firmou	firma	k1gFnSc7	firma
Fairey	Fairea	k1gFnSc2	Fairea
Aviation	Aviation	k1gInSc1	Aviation
Company	Compana	k1gFnSc2	Compana
užívaný	užívaný	k2eAgInSc1d1	užívaný
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
archaický	archaický	k2eAgInSc4d1	archaický
a	a	k8xC	a
zastaralý	zastaralý	k2eAgInSc4d1	zastaralý
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
toto	tento	k3xDgNnSc1	tento
letadlo	letadlo	k1gNnSc1	letadlo
mnohé	mnohý	k2eAgFnSc2d1	mnohá
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
zejména	zejména	k9	zejména
poškozením	poškození	k1gNnSc7	poškození
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodi	loď	k1gFnSc2	loď
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
skočilo	skočit	k5eAaPmAgNnS	skočit
jejím	její	k3xOp3gNnSc7	její
následným	následný	k2eAgNnSc7d1	následné
potopením	potopení	k1gNnSc7	potopení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pomalé	pomalý	k2eAgNnSc4d1	pomalé
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
potažené	potažený	k2eAgNnSc1d1	potažené
plátnem	plátno	k1gNnSc7	plátno
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c7	mezi
130	[number]	k4	130
<g/>
–	–	k?	–
<g/>
245	[number]	k4	245
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Jeho	jeho	k3xOp3gFnSc4	jeho
nosnost	nosnost	k1gFnSc4	nosnost
byla	být	k5eAaImAgFnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
nést	nést	k5eAaImF	nést
torpédo	torpédo	k1gNnSc1	torpédo
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
730	[number]	k4	730
kg	kg	kA	kg
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
osm	osm	k4xCc1	osm
hlubinných	hlubinný	k2eAgFnPc2d1	hlubinná
náloží	nálož	k1gFnPc2	nálož
po	po	k7c4	po
45	[number]	k4	45
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Fairey	Faire	k2eAgInPc1d1	Faire
Swordfish	Swordfish	k1gInSc1	Swordfish
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
z	z	k7c2	z
třímístného	třímístný	k2eAgInSc2d1	třímístný
letounu	letoun	k1gInSc2	letoun
TSR	TSR	kA	TSR
(	(	kIx(	(
<g/>
Torpedo-Strike-Reconnaissance	Torpedo-Strike-Reconnaissanec	k1gMnSc2	Torpedo-Strike-Reconnaissanec
<g/>
)	)	kIx)	)
s	s	k7c7	s
hvězdicovým	hvězdicový	k2eAgInSc7d1	hvězdicový
motorem	motor	k1gInSc7	motor
Bristol	Bristol	k1gInSc1	Bristol
Pegasus	Pegasus	k1gMnSc1	Pegasus
II	II	kA	II
M	M	kA	M
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
467	[number]	k4	467
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Prototyp	prototyp	k1gInSc1	prototyp
TSR	TSR	kA	TSR
(	(	kIx(	(
<g/>
výrobní	výrobní	k2eAgNnSc4d1	výrobní
číslo	číslo	k1gNnSc4	číslo
F	F	kA	F
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
bez	bez	k7c2	bez
předchozí	předchozí	k2eAgFnSc2d1	předchozí
objednávky	objednávka	k1gFnSc2	objednávka
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
společností	společnost	k1gFnPc2	společnost
Fairey	Faire	k2eAgFnPc4d1	Faire
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1933	[number]	k4	1933
pilotován	pilotován	k2eAgInSc1d1	pilotován
nadporučíkem	nadporučík	k1gMnSc7	nadporučík
Chrisem	Chris	k1gInSc7	Chris
S.	S.	kA	S.
Stanilandem	Stanilando	k1gNnSc7	Stanilando
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
léta	léto	k1gNnSc2	léto
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
letounu	letoun	k1gInSc2	letoun
vestavěn	vestavěn	k2eAgInSc4d1	vestavěn
dvouhvězdicový	dvouhvězdicový	k2eAgInSc4d1	dvouhvězdicový
čtrnáctiválec	čtrnáctiválec	k1gInSc4	čtrnáctiválec
Armstrong	Armstrong	k1gMnSc1	Armstrong
Siddeley	Siddelea	k1gFnSc2	Siddelea
Panther	Panthra	k1gFnPc2	Panthra
VI	VI	kA	VI
o	o	k7c4	o
460	[number]	k4	460
kW	kW	kA	kW
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
však	však	k9	však
opět	opět	k6eAd1	opět
zaměněn	zaměněn	k2eAgMnSc1d1	zaměněn
za	za	k7c4	za
Pegasus	Pegasus	k1gMnSc1	Pegasus
II	II	kA	II
M	M	kA	M
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
Townendově	Townendův	k2eAgInSc6d1	Townendův
prstenci	prstenec	k1gInSc6	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dvouplošníkem	dvouplošník	k1gInSc7	dvouplošník
přeznačeným	přeznačený	k2eAgInSc7d1	přeznačený
na	na	k7c4	na
TSR	TSR	kA	TSR
I	i	k9	i
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
Staniland	Staniland	k1gInSc1	Staniland
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Letové	letový	k2eAgFnPc1d1	letová
zkoušky	zkouška	k1gFnPc1	zkouška
probíhaly	probíhat	k5eAaImAgFnP	probíhat
uspokojivě	uspokojivě	k6eAd1	uspokojivě
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stroj	stroj	k1gInSc1	stroj
po	po	k7c6	po
nevybrání	nevybrání	k1gNnSc6	nevybrání
ploché	plochý	k2eAgFnSc2d1	plochá
vývrtky	vývrtka	k1gFnSc2	vývrtka
zřítil	zřítit	k5eAaPmAgInS	zřítit
a	a	k8xC	a
pilot	pilot	k1gInSc1	pilot
Staniland	Stanilanda	k1gFnPc2	Stanilanda
musel	muset	k5eAaImAgInS	muset
stroj	stroj	k1gInSc1	stroj
opustit	opustit	k5eAaPmF	opustit
na	na	k7c6	na
padáku	padák	k1gInSc6	padák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fairey	Fairey	k1gInPc1	Fairey
S.	S.	kA	S.
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
označený	označený	k2eAgInSc4d1	označený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
objednávky	objednávka	k1gFnSc2	objednávka
britského	britský	k2eAgNnSc2d1	Britské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
letectví	letectví	k1gNnSc2	letectví
z	z	k7c2	z
června	červen	k1gInSc2	červen
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
zalétal	zalétat	k5eAaImAgInS	zalétat
opět	opět	k6eAd1	opět
pilot	pilot	k1gInSc1	pilot
Staniland	Stanilanda	k1gFnPc2	Stanilanda
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
identický	identický	k2eAgInSc1d1	identický
s	s	k7c7	s
TSR	TSR	kA	TSR
I	I	kA	I
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
osazen	osadit	k5eAaPmNgInS	osadit
kapalinovým	kapalinový	k2eAgInSc7d1	kapalinový
dvanáctiválcem	dvanáctiválec	k1gInSc7	dvanáctiválec
Rolls-Royce	Rolls-Royce	k1gFnSc1	Rolls-Royce
Kestrel	Kestrel	k1gMnSc1	Kestrel
II	II	kA	II
MS	MS	kA	MS
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
386	[number]	k4	386
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
létal	létat	k5eAaImAgMnS	létat
s	s	k7c7	s
kolovým	kolový	k2eAgInSc7d1	kolový
podvozkem	podvozek	k1gInSc7	podvozek
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1934	[number]	k4	1934
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
zaměněným	zaměněný	k2eAgInSc7d1	zaměněný
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
podtrupový	podtrupový	k2eAgInSc4d1	podtrupový
centrální	centrální	k2eAgInSc4d1	centrální
plovák	plovák	k1gInSc4	plovák
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
vyrovnávací	vyrovnávací	k2eAgInPc4d1	vyrovnávací
plováky	plovák	k1gInPc4	plovák
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
TSR	TSR	kA	TSR
I	i	k8xC	i
zahájili	zahájit	k5eAaPmAgMnP	zahájit
konstruktéři	konstruktér	k1gMnPc1	konstruktér
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
M.	M.	kA	M.
Lobella	Lobella	k1gFnSc1	Lobella
vývoj	vývoj	k1gInSc1	vývoj
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
letounu	letoun	k1gInSc2	letoun
označeného	označený	k2eAgMnSc2d1	označený
Fairey	Fairea	k1gMnSc2	Fairea
TSR	TSR	kA	TSR
II	II	kA	II
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
K	k	k7c3	k
<g/>
4190	[number]	k4	4190
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postavený	postavený	k2eAgInSc1d1	postavený
stroj	stroj	k1gInSc1	stroj
dostal	dostat	k5eAaPmAgInS	dostat
za	za	k7c4	za
pohonnou	pohonný	k2eAgFnSc4d1	pohonná
jednotku	jednotka	k1gFnSc4	jednotka
hvězdicový	hvězdicový	k2eAgInSc4d1	hvězdicový
devítiválec	devítiválec	k1gInSc4	devítiválec
Bristol	Bristol	k1gInSc1	Bristol
Pegasus	Pegasus	k1gMnSc1	Pegasus
III	III	kA	III
M	M	kA	M
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
až	až	k9	až
507	[number]	k4	507
kW	kW	kA	kW
s	s	k7c7	s
dvoulistou	dvoulistý	k2eAgFnSc7d1	dvoulistá
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
vrtulí	vrtule	k1gFnSc7	vrtule
Watts	Wattsa	k1gFnPc2	Wattsa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
stroje	stroj	k1gInSc2	stroj
se	se	k3xPyFc4	se
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
zejména	zejména	k9	zejména
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
trupem	trup	k1gInSc7	trup
<g/>
,	,	kIx,	,
změněnou	změněný	k2eAgFnSc7d1	změněná
geometrií	geometrie	k1gFnSc7	geometrie
horního	horní	k2eAgNnSc2d1	horní
křídla	křídlo	k1gNnSc2	křídlo
se	s	k7c7	s
šípovitostí	šípovitost	k1gFnSc7	šípovitost
4	[number]	k4	4
°	°	k?	°
a	a	k8xC	a
širší	široký	k2eAgFnSc7d2	širší
a	a	k8xC	a
nižší	nízký	k2eAgFnSc7d2	nižší
svislou	svislý	k2eAgFnSc7d1	svislá
ocasní	ocasní	k2eAgFnSc7d1	ocasní
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
z	z	k7c2	z
továrního	tovární	k2eAgNnSc2d1	tovární
letiště	letiště	k1gNnSc2	letiště
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouměsíčních	dvouměsíční	k2eAgFnPc6d1	dvouměsíční
zkouškách	zkouška	k1gFnPc6	zkouška
byl	být	k5eAaImAgInS	být
přelétnut	přelétnout	k5eAaPmNgInS	přelétnout
do	do	k7c2	do
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
výzbroje	výzbroj	k1gFnSc2	výzbroj
v	v	k7c4	v
Martlesham	Martlesham	k1gInSc4	Martlesham
Heath	Heatha	k1gFnPc2	Heatha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
zkoušen	zkoušet	k5eAaImNgMnS	zkoušet
v	v	k7c6	v
Královském	královský	k2eAgInSc6d1	královský
leteckém	letecký	k2eAgInSc6d1	letecký
ústavu	ústav	k1gInSc6	ústav
ve	v	k7c4	v
Farnborough	Farnborough	k1gInSc4	Farnborough
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgFnPc1d1	následná
zkoušky	zkouška	k1gFnPc1	zkouška
vzletu	vzlet	k1gInSc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc2	přistání
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
probíhaly	probíhat	k5eAaImAgInP	probíhat
na	na	k7c4	na
HMS	HMS	kA	HMS
Courageous	Courageous	k1gInSc4	Courageous
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
absolvování	absolvování	k1gNnSc6	absolvování
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
továrny	továrna	k1gFnSc2	továrna
Fairey	Fairea	k1gFnSc2	Fairea
v	v	k7c6	v
Hamble	Hamble	k1gFnSc6	Hamble
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
dvěma	dva	k4xCgInPc7	dva
plováky	plovák	k1gInPc7	plovák
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
zalétán	zalétán	k2eAgMnSc1d1	zalétán
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ověřoval	ověřovat	k5eAaImAgInS	ověřovat
u	u	k7c2	u
zkušební	zkušební	k2eAgFnSc2d1	zkušební
torpédové	torpédový	k2eAgFnSc2d1	torpédová
jednotky	jednotka	k1gFnSc2	jednotka
v	v	k7c6	v
Gosportu	Gosport	k1gInSc6	Gosport
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1935	[number]	k4	1935
havaroval	havarovat	k5eAaPmAgMnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1936	[number]	k4	1936
opět	opět	k6eAd1	opět
létal	létat	k5eAaImAgMnS	létat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
testovacím	testovací	k2eAgMnSc7d1	testovací
letounem	letoun	k1gMnSc7	letoun
Ředitelství	ředitelství	k1gNnSc2	ředitelství
technického	technický	k2eAgInSc2d1	technický
vývoje	vývoj	k1gInSc2	vývoj
britského	britský	k2eAgNnSc2d1	Britské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1937	[number]	k4	1937
dostal	dostat	k5eAaPmAgMnS	dostat
dvojí	dvojí	k4xRgNnSc4	dvojí
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
odnímatelný	odnímatelný	k2eAgInSc4d1	odnímatelný
kryt	kryt	k1gInSc4	kryt
předního	přední	k2eAgInSc2d1	přední
pilotního	pilotní	k2eAgInSc2d1	pilotní
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
bez	bez	k7c2	bez
vidu	vid	k1gInSc2	vid
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
plováky	plovák	k1gInPc7	plovák
a	a	k8xC	a
změněnými	změněný	k2eAgNnPc7d1	změněné
kormidly	kormidlo	k1gNnPc7	kormidlo
odeslán	odeslat	k5eAaPmNgInS	odeslat
opět	opět	k6eAd1	opět
do	do	k7c2	do
Gosportu	Gosport	k1gInSc2	Gosport
ke	k	k7c3	k
zkouškám	zkouška	k1gFnPc3	zkouška
s	s	k7c7	s
torpédy	torpédo	k1gNnPc7	torpédo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
všech	všecek	k3xTgFnPc2	všecek
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
zkoušek	zkouška	k1gFnPc2	zkouška
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
letectví	letectví	k1gNnSc2	letectví
typ	typa	k1gFnPc2	typa
přijalo	přijmout	k5eAaPmAgNnS	přijmout
a	a	k8xC	a
dalo	dát	k5eAaPmAgNnS	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc4	jméno
Swordfish	Swordfish	k1gMnSc1	Swordfish
(	(	kIx(	(
<g/>
Mečoun	mečoun	k1gMnSc1	mečoun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
byly	být	k5eAaImAgInP	být
objednány	objednán	k2eAgInPc1d1	objednán
tři	tři	k4xCgInPc1	tři
předsériové	předsériový	k2eAgInPc1d1	předsériový
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
(	(	kIx(	(
<g/>
sériové	sériový	k2eAgNnSc4d1	sériové
číslo	číslo	k1gNnSc4	číslo
K	k	k7c3	k
<g/>
5660	[number]	k4	5660
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgNnSc4d1	výrobní
číslo	číslo	k1gNnSc4	číslo
F	F	kA	F
<g/>
2142	[number]	k4	2142
<g/>
)	)	kIx)	)
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
5661	[number]	k4	5661
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
2143	[number]	k4	2143
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1936	[number]	k4	1936
dodán	dodat	k5eAaPmNgInS	dodat
do	do	k7c2	do
Gosportu	Gosport	k1gInSc2	Gosport
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
předsériový	předsériový	k2eAgInSc4d1	předsériový
Swordfish	Swordfish	k1gInSc4	Swordfish
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
5662	[number]	k4	5662
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
2144	[number]	k4	2144
<g/>
)	)	kIx)	)
s	s	k7c7	s
plováky	plovák	k1gInPc7	plovák
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
ke	k	k7c3	k
zkouškám	zkouška	k1gFnPc3	zkouška
do	do	k7c2	do
střediska	středisko	k1gNnSc2	středisko
Felixtowe	Felixtow	k1gFnSc2	Felixtow
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
předsériové	předsériový	k2eAgInPc1d1	předsériový
letouny	letoun	k1gInPc1	letoun
poháněly	pohánět	k5eAaImAgInP	pohánět
motory	motor	k1gInPc4	motor
Pegasus	Pegasus	k1gMnSc1	Pegasus
III	III	kA	III
M3	M3	k1gMnSc1	M3
s	s	k7c7	s
třílistými	třílistý	k2eAgFnPc7d1	třílistá
nestavitelnými	stavitelný	k2eNgFnPc7d1	nestavitelná
vrtulemi	vrtule	k1gFnPc7	vrtule
Fairey-Reed	Fairey-Reed	k1gInSc1	Fairey-Reed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sériové	sériový	k2eAgInPc1d1	sériový
stroje	stroj	k1gInPc1	stroj
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfish	k1gMnSc1	Swordfish
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k8xC	i
začaly	začít	k5eAaPmAgFnP	začít
opouštět	opouštět	k5eAaImF	opouštět
výrobní	výrobní	k2eAgFnPc4d1	výrobní
linky	linka	k1gFnPc4	linka
v	v	k7c4	v
Hayes	Hayes	k1gInSc4	Hayes
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
narušilo	narušit	k5eAaPmAgNnS	narušit
plynulost	plynulost	k1gFnSc4	plynulost
výroby	výroba	k1gFnSc2	výroba
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
Fairey	Fairea	k1gFnSc2	Fairea
zavedení	zavedení	k1gNnSc4	zavedení
sériové	sériový	k2eAgFnSc2d1	sériová
výroby	výroba	k1gFnSc2	výroba
strojů	stroj	k1gInPc2	stroj
Fairey	Fairea	k1gFnSc2	Fairea
Albacore	Albacor	k1gInSc5	Albacor
a	a	k8xC	a
pokračující	pokračující	k2eAgFnSc1d1	pokračující
výroba	výroba	k1gFnSc1	výroba
letounů	letoun	k1gInPc2	letoun
Fairey	Fairea	k1gFnSc2	Fairea
Fulmar	Fulmara	k1gFnPc2	Fulmara
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
britskou	britský	k2eAgFnSc7d1	britská
admiralitou	admiralita	k1gFnSc7	admiralita
navrženo	navržen	k2eAgNnSc1d1	navrženo
převést	převést	k5eAaPmF	převést
výrobu	výroba	k1gFnSc4	výroba
Swordfishů	Swordfish	k1gInPc2	Swordfish
k	k	k7c3	k
firmě	firma	k1gFnSc3	firma
Blackburn	Blackburn	k1gMnSc1	Blackburn
Aircraft	Aircraft	k1gMnSc1	Aircraft
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
závodu	závod	k1gInSc2	závod
v	v	k7c4	v
Sherburn-in-Elmet	Sherburnn-Elmet	k1gInSc4	Sherburn-in-Elmet
ve	v	k7c4	v
West	West	k1gInSc4	West
Yorkshiru	Yorkshir	k1gInSc2	Yorkshir
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
ihned	ihned	k6eAd1	ihned
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1940	[number]	k4	1940
vyšly	vyjít	k5eAaPmAgFnP	vyjít
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
výrobní	výrobní	k2eAgFnSc2d1	výrobní
linky	linka	k1gFnSc2	linka
první	první	k4xOgFnSc2	první
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
neoficiálně	neoficiálně	k6eAd1	neoficiálně
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
Blackfish	Blackfish	k1gInSc4	Blackfish
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
produkce	produkce	k1gFnSc1	produkce
typu	typ	k1gInSc2	typ
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	I	kA	I
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
992	[number]	k4	992
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
výrobních	výrobní	k2eAgFnPc6d1	výrobní
linkách	linka	k1gFnPc6	linka
firmy	firma	k1gFnSc2	firma
Blackburn	Blackburna	k1gFnPc2	Blackburna
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
objevila	objevit	k5eAaPmAgFnS	objevit
nová	nový	k2eAgFnSc1d1	nová
varianta	varianta	k1gFnSc1	varianta
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfish	k1gMnSc1	Swordfish
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
potahem	potah	k1gInSc7	potah
spodní	spodní	k2eAgFnSc2d1	spodní
plochy	plocha	k1gFnSc2	plocha
dolního	dolní	k2eAgNnSc2d1	dolní
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
vhodného	vhodný	k2eAgInSc2d1	vhodný
pro	pro	k7c4	pro
zavěšení	zavěšení	k1gNnSc4	zavěšení
až	až	k9	až
osmi	osm	k4xCc2	osm
neřízených	řízený	k2eNgFnPc2d1	neřízená
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
1080	[number]	k4	1080
strojů	stroj	k1gInPc2	stroj
varianty	varianta	k1gFnSc2	varianta
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
verze	verze	k1gFnSc1	verze
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfish	k1gMnSc1	Swordfish
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
320	[number]	k4	320
kusů	kus	k1gInPc2	kus
s	s	k7c7	s
vestavěným	vestavěný	k2eAgInSc7d1	vestavěný
protilodním	protilodní	k2eAgInSc7d1	protilodní
radarem	radar	k1gInSc7	radar
ASV	ASV	kA	ASV
Mk	Mk	k1gFnSc6	Mk
<g/>
.	.	kIx.	.
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
umístěným	umístěný	k2eAgNnSc7d1	umístěné
ve	v	k7c6	v
výstupku	výstupek	k1gInSc6	výstupek
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgFnPc7d1	hlavní
nohami	noha	k1gFnPc7	noha
podvozku	podvozek	k1gInSc2	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
stroje	stroj	k1gInPc1	stroj
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
Mk	Mk	k1gFnPc1	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
měly	mít	k5eAaImAgInP	mít
motor	motor	k1gInSc4	motor
Bristol	Bristol	k1gInSc1	Bristol
Pegasus	Pegasus	k1gMnSc1	Pegasus
30	[number]	k4	30
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
zvýšeném	zvýšený	k2eAgInSc6d1	zvýšený
na	na	k7c4	na
551	[number]	k4	551
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
byly	být	k5eAaImAgInP	být
vyrobeny	vyroben	k2eAgInPc4d1	vyroben
letouny	letoun	k1gInPc4	letoun
Swordfish	Swordfish	k1gMnSc1	Swordfish
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
IV	IV	kA	IV
se	s	k7c7	s
zakrytou	zakrytý	k2eAgFnSc7d1	zakrytá
kabinou	kabina	k1gFnSc7	kabina
<g/>
,	,	kIx,	,
upraveným	upravený	k2eAgInSc7d1	upravený
karburátorem	karburátor	k1gInSc7	karburátor
a	a	k8xC	a
vrtulí	vrtule	k1gFnSc7	vrtule
<g/>
,	,	kIx,	,
odledováním	odledování	k1gNnSc7	odledování
čelního	čelní	k2eAgInSc2d1	čelní
štítku	štítek	k1gInSc2	štítek
kabiny	kabina	k1gFnSc2	kabina
a	a	k8xC	a
vybavením	vybavení	k1gNnSc7	vybavení
pro	pro	k7c4	pro
elektricky	elektricky	k6eAd1	elektricky
vyhřívané	vyhřívaný	k2eAgFnPc4d1	vyhřívaná
kombinézy	kombinéza	k1gFnPc4	kombinéza
určené	určený	k2eAgFnPc4d1	určená
pro	pro	k7c4	pro
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
modifikované	modifikovaný	k2eAgInPc1d1	modifikovaný
stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgFnP	být
Mk	Mk	k1gFnPc1	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
bez	bez	k7c2	bez
vybavení	vybavení	k1gNnSc2	vybavení
pro	pro	k7c4	pro
vypouštění	vypouštění	k1gNnSc4	vypouštění
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
bez	bez	k7c2	bez
předního	přední	k2eAgInSc2d1	přední
kulometu	kulomet	k1gInSc2	kulomet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
Swordfishem	Swordfish	k1gInSc7	Swordfish
byl	být	k5eAaImAgInS	být
Mk	Mk	k1gFnSc4	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
se	s	k7c7	s
sériovým	sériový	k2eAgNnSc7d1	sériové
číslem	číslo	k1gNnSc7	číslo
NS	NS	kA	NS
<g/>
204	[number]	k4	204
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
továrnu	továrna	k1gFnSc4	továrna
Blackburn	Blackburn	k1gInSc4	Blackburn
v	v	k7c6	v
Sherburnu	Sherburno	k1gNnSc6	Sherburno
opustil	opustit	k5eAaPmAgMnS	opustit
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
Swordfishe	Swordfish	k1gFnPc1	Swordfish
byly	být	k5eAaImAgFnP	být
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
prvosledových	prvosledův	k2eAgInPc2d1	prvosledův
útvarů	útvar	k1gInPc2	útvar
vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
operační	operační	k2eAgFnSc2d1	operační
letky	letka	k1gFnSc2	letka
č.	č.	k?	č.
836	[number]	k4	836
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
rozpuštěno	rozpustit	k5eAaPmNgNnS	rozpustit
i	i	k9	i
několik	několik	k4yIc1	několik
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
cvičných	cvičný	k2eAgFnPc2d1	cvičná
letek	letka	k1gFnPc2	letka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bojové	bojový	k2eAgNnSc1d1	bojové
užití	užití	k1gNnSc1	užití
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
Fairey	Fairea	k1gFnPc4	Fairea
Swordfish	Swordfish	k1gMnSc1	Swordfish
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
I	I	kA	I
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
výzbroje	výzbroj	k1gFnSc2	výzbroj
825	[number]	k4	825
<g/>
.	.	kIx.	.
letka	letka	k1gFnSc1	letka
Fleet	Fleet	k1gMnSc1	Fleet
Air	Air	k1gMnSc1	Air
Arm	Arm	k1gMnSc1	Arm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
stroje	stroj	k1gInPc4	stroj
Fairey	Fairea	k1gFnSc2	Fairea
Seal	Seala	k1gFnPc2	Seala
se	s	k7c7	s
základnou	základna	k1gFnSc7	základna
na	na	k7c6	na
letadlové	letadlový	k2eAgFnSc6d1	letadlová
lodi	loď	k1gFnSc6	loď
HMS	HMS	kA	HMS
Glorious	Glorious	k1gInSc1	Glorious
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
Swordfishe	Swordfish	k1gInSc2	Swordfish
nahradily	nahradit	k5eAaPmAgInP	nahradit
letouny	letoun	k1gInPc1	letoun
Blackburn	Blackburna	k1gFnPc2	Blackburna
Baffin	Baffina	k1gFnPc2	Baffina
u	u	k7c2	u
811	[number]	k4	811
<g/>
.	.	kIx.	.
a	a	k8xC	a
812	[number]	k4	812
<g/>
.	.	kIx.	.
letky	letka	k1gFnPc1	letka
a	a	k8xC	a
Sealy	Seal	k1gInPc1	Seal
u	u	k7c2	u
letky	letka	k1gFnSc2	letka
č.	č.	k?	č.
823	[number]	k4	823
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
nahradily	nahradit	k5eAaPmAgFnP	nahradit
Blackburn	Blackburn	k1gNnSc4	Blackburn
Shark	Shark	k1gInSc1	Shark
u	u	k7c2	u
810	[number]	k4	810
<g/>
.	.	kIx.	.
820	[number]	k4	820
<g/>
.	.	kIx.	.
821	[number]	k4	821
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Swordfishe	Swordfish	k1gInSc2	Swordfish
přešly	přejít	k5eAaPmAgInP	přejít
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
822	[number]	k4	822
<g/>
.	.	kIx.	.
a	a	k8xC	a
824	[number]	k4	824
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c2	za
Sealy	Seala	k1gFnSc2	Seala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Mečouny	mečoun	k1gMnPc7	mečoun
<g/>
"	"	kIx"	"
vybavena	vybavit	k5eAaPmNgNnP	vybavit
první	první	k4xOgFnSc1	první
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgFnSc1d1	nová
814	[number]	k4	814
<g/>
.	.	kIx.	.
letka	letka	k1gFnSc1	letka
operující	operující	k2eAgFnSc1d1	operující
zprvu	zprvu	k6eAd1	zprvu
z	z	k7c2	z
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
a	a	k8xC	a
poté	poté	k6eAd1	poté
z	z	k7c2	z
HMS	HMS	kA	HMS
Hermes	Hermes	k1gMnSc1	Hermes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
nové	nový	k2eAgFnPc1d1	nová
letky	letka	k1gFnPc1	letka
vybavené	vybavený	k2eAgFnSc2d1	vybavená
Swordfishi	Swordfish	k1gFnSc2	Swordfish
č.	č.	k?	č.
816	[number]	k4	816
<g/>
.	.	kIx.	.
a	a	k8xC	a
818	[number]	k4	818
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
815	[number]	k4	815
<g/>
.	.	kIx.	.
letka	letka	k1gFnSc1	letka
se	s	k7c7	s
základnou	základna	k1gFnSc7	základna
na	na	k7c6	na
letadlové	letadlový	k2eAgFnSc6d1	letadlová
lodi	loď	k1gFnSc6	loď
HMS	HMS	kA	HMS
Illustrious	Illustrious	k1gInSc1	Illustrious
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1940	[number]	k4	1940
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
819	[number]	k4	819
<g/>
.	.	kIx.	.
letka	letka	k1gFnSc1	letka
<g/>
.	.	kIx.	.
</s>
<s>
Ochraňovaly	ochraňovat	k5eAaImAgFnP	ochraňovat
britské	britský	k2eAgFnPc1d1	britská
válečné	válečná	k1gFnPc1	válečná
i	i	k8xC	i
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
a	a	k8xC	a
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
konvoje	konvoj	k1gInPc1	konvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
poprvé	poprvé	k6eAd1	poprvé
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
stroje	stroj	k1gInPc1	stroj
816	[number]	k4	816
<g/>
.	.	kIx.	.
a	a	k8xC	a
818	[number]	k4	818
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
z	z	k7c2	z
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
Furious	Furious	k1gInSc4	Furious
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
a	a	k8xC	a
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
u	u	k7c2	u
Trondheimu	Trondheim	k1gInSc2	Trondheim
německý	německý	k2eAgInSc1d1	německý
torpédoborec	torpédoborec	k1gInSc1	torpédoborec
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vyslala	vyslat	k5eAaPmAgFnS	vyslat
svůj	svůj	k3xOyFgInSc4	svůj
plovákový	plovákový	k2eAgInSc4d1	plovákový
Swordfish	Swordfish	k1gInSc4	Swordfish
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
HMS	HMS	kA	HMS
Warspite	Warspit	k1gInSc5	Warspit
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Ofotského	Ofotský	k2eAgInSc2d1	Ofotský
fjordu	fjord	k1gInSc2	fjord
u	u	k7c2	u
Narviku	Narvik	k1gInSc2	Narvik
<g/>
.	.	kIx.	.
</s>
<s>
Tříčlenná	tříčlenný	k2eAgFnSc1d1	tříčlenná
posádka	posádka	k1gFnSc1	posádka
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
podporučíka	podporučík	k1gMnSc2	podporučík
Rice	Ric	k1gFnSc2	Ric
naváděla	navádět	k5eAaImAgFnS	navádět
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1	dělostřelecká
palbu	palba	k1gFnSc4	palba
Warspite	Warspit	k1gInSc5	Warspit
a	a	k8xC	a
poté	poté	k6eAd1	poté
zpozorovala	zpozorovat	k5eAaPmAgFnS	zpozorovat
ponorku	ponorka	k1gFnSc4	ponorka
U	u	k7c2	u
64	[number]	k4	64
zakotvenou	zakotvený	k2eAgFnSc7d1	zakotvená
v	v	k7c6	v
Bjerkvickém	Bjerkvický	k2eAgInSc6d1	Bjerkvický
fjordu	fjord	k1gInSc6	fjord
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
minimálně	minimálně	k6eAd1	minimálně
jednou	jeden	k4xCgFnSc7	jeden
pumou	puma	k1gFnSc7	puma
poslali	poslat	k5eAaPmAgMnP	poslat
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
26	[number]	k4	26
Swordfishů	Swordfish	k1gMnPc2	Swordfish
a	a	k8xC	a
10	[number]	k4	10
strojů	stroj	k1gInPc2	stroj
Blackburn	Blackburn	k1gMnSc1	Blackburn
Skua	Skua	k1gMnSc1	Skua
z	z	k7c2	z
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
Furious	Furious	k1gInSc4	Furious
a	a	k8xC	a
Glorious	Glorious	k1gInSc4	Glorious
podporovalo	podporovat	k5eAaImAgNnS	podporovat
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
britskou	britský	k2eAgFnSc4d1	britská
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
invazi	invaze	k1gFnSc4	invaze
v	v	k7c6	v
Trondheimu	Trondheimo	k1gNnSc6	Trondheimo
a	a	k8xC	a
Narviku	Narvikum	k1gNnSc6	Narvikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1940	[number]	k4	1940
stroje	stroj	k1gInSc2	stroj
812	[number]	k4	812
<g/>
.	.	kIx.	.
letky	letka	k1gFnPc1	letka
operovaly	operovat	k5eAaImAgFnP	operovat
ze	z	k7c2	z
souše	souš	k1gFnSc2	souš
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Coastal	Coastal	k1gFnSc2	Coastal
Command	Commanda	k1gFnPc2	Commanda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
při	při	k7c6	při
kladení	kladení	k1gNnSc6	kladení
min	mina	k1gFnPc2	mina
v	v	k7c6	v
přístavech	přístav	k1gInPc6	přístav
kanálu	kanál	k1gInSc2	kanál
La	la	k1gNnSc1	la
Manche	Manche	k1gInSc1	Manche
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
tyto	tento	k3xDgInPc4	tento
přístavy	přístav	k1gInPc4	přístav
za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
již	již	k6eAd1	již
i	i	k9	i
bombardovaly	bombardovat	k5eAaImAgFnP	bombardovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Itálie	Itálie	k1gFnSc1	Itálie
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
Swordfishů	Swordfish	k1gInPc2	Swordfish
Mk	Mk	k1gFnPc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
z	z	k7c2	z
cvičné	cvičný	k2eAgFnSc2d1	cvičná
letky	letka	k1gFnSc2	letka
č.	č.	k?	č.
767	[number]	k4	767
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
na	na	k7c6	na
jihofrancouzské	jihofrancouzský	k2eAgFnSc6d1	jihofrancouzská
námořní	námořní	k2eAgFnSc6d1	námořní
letecké	letecký	k2eAgFnSc6d1	letecká
základně	základna	k1gFnSc6	základna
v	v	k7c6	v
Hyè	Hyè	k1gFnSc6	Hyè
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvními	první	k4xOgInPc7	první
letadly	letadlo	k1gNnPc7	letadlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Letka	letka	k1gFnSc1	letka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
24	[number]	k4	24
Swordfishů	Swordfish	k1gInPc2	Swordfish
pak	pak	k6eAd1	pak
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
opustila	opustit	k5eAaPmAgFnS	opustit
Hyè	Hyè	k1gFnSc1	Hyè
a	a	k8xC	a
odletěla	odletět	k5eAaPmAgFnS	odletět
do	do	k7c2	do
alžírského	alžírský	k2eAgInSc2d1	alžírský
Bône	Bôn	k1gInSc2	Bôn
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
letky	letka	k1gFnSc2	letka
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výcviku	výcvik	k1gInSc3	výcvik
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
přes	přes	k7c4	přes
Casablanku	Casablanca	k1gFnSc4	Casablanca
a	a	k8xC	a
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
operační	operační	k2eAgInSc4d1	operační
zbytek	zbytek	k1gInSc4	zbytek
odletěl	odletět	k5eAaPmAgMnS	odletět
na	na	k7c4	na
Maltu	Malta	k1gFnSc4	Malta
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
přeznačen	přeznačen	k2eAgInSc4d1	přeznačen
na	na	k7c4	na
830	[number]	k4	830
<g/>
.	.	kIx.	.
letku	letek	k1gInSc2	letek
a	a	k8xC	a
již	již	k6eAd1	již
za	za	k7c4	za
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
po	po	k7c6	po
vzletu	vzlet	k1gInSc6	vzlet
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
Hal	hala	k1gFnPc2	hala
Far	fara	k1gFnPc2	fara
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
ropné	ropný	k2eAgFnPc4d1	ropná
nádrže	nádrž	k1gFnPc4	nádrž
v	v	k7c6	v
sicilské	sicilský	k2eAgFnSc6d1	sicilská
Augustě	Augusta	k1gFnSc6	Augusta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Francie	Francie	k1gFnSc2	Francie
útočily	útočit	k5eAaImAgFnP	útočit
Swordfishe	Swordfish	k1gFnPc1	Swordfish
Mk	Mk	k1gMnPc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1940	[number]	k4	1940
na	na	k7c4	na
francouzské	francouzský	k2eAgNnSc4d1	francouzské
loďstvo	loďstvo	k1gNnSc4	loďstvo
v	v	k7c6	v
Mers-el-Kébiru	Mersl-Kébiro	k1gNnSc6	Mers-el-Kébiro
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
dvakrát	dvakrát	k6eAd1	dvakrát
neúspěšně	úspěšně	k6eNd1	úspěšně
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
bitevní	bitevní	k2eAgInSc4d1	bitevní
křižník	křižník	k1gInSc4	křižník
Strasbourg	Strasbourg	k1gMnSc1	Strasbourg
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prchal	prchat	k5eAaImAgMnS	prchat
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
Mers-el-Kébir	Mersl-Kébir	k1gInSc4	Mers-el-Kébir
do	do	k7c2	do
Toulonu	Toulon	k1gInSc2	Toulon
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Swordfishe	Swordfishe	k1gFnSc1	Swordfishe
z	z	k7c2	z
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
<g/>
,	,	kIx,	,
plující	plující	k2eAgFnSc6d1	plující
nedaleko	nedaleko	k7c2	nedaleko
Gibraltaru	Gibraltar	k1gInSc2	Gibraltar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusily	pokusit	k5eAaPmAgInP	pokusit
bombardovat	bombardovat	k5eAaImF	bombardovat
Strasbourg	Strasbourg	k1gInSc4	Strasbourg
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
svého	svůj	k3xOyFgInSc2	svůj
doletu	dolet	k1gInSc2	dolet
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
z	z	k7c2	z
vypuštěných	vypuštěný	k2eAgNnPc2d1	vypuštěné
torpéd	torpédo	k1gNnPc2	torpédo
však	však	k9	však
křižník	křižník	k1gInSc4	křižník
ani	ani	k8xC	ani
pět	pět	k4xCc4	pět
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
nezasáhlo	zasáhnout	k5eNaPmAgNnS	zasáhnout
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenka	k1gFnSc6	červenka
tři	tři	k4xCgFnPc1	tři
vlny	vlna	k1gFnPc1	vlna
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letadlech	letadlo	k1gNnPc6	letadlo
Swordfish	Swordfish	k1gInSc4	Swordfish
z	z	k7c2	z
810	[number]	k4	810
<g/>
.	.	kIx.	.
a	a	k8xC	a
820	[number]	k4	820
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
z	z	k7c2	z
Ark	Ark	k1gMnSc2	Ark
Royal	Royal	k1gInSc1	Royal
bombardovaly	bombardovat	k5eAaImAgFnP	bombardovat
bitevní	bitevní	k2eAgInSc4d1	bitevní
křižník	křižník	k1gInSc4	křižník
Dunkerque	Dunkerqu	k1gFnSc2	Dunkerqu
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
puma	puma	k1gFnSc1	puma
zničila	zničit	k5eAaPmAgFnS	zničit
muniční	muniční	k2eAgInSc4d1	muniční
člun	člun	k1gInSc4	člun
kotvící	kotvící	k2eAgInSc4d1	kotvící
vedle	vedle	k7c2	vedle
Dunkerque	Dunkerqu	k1gInSc2	Dunkerqu
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výbuch	výbuch	k1gInSc1	výbuch
křižník	křižník	k1gInSc4	křižník
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
z	z	k7c2	z
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
šest	šest	k4xCc4	šest
Swordfishů	Swordfish	k1gMnPc2	Swordfish
z	z	k7c2	z
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
Hermes	Hermes	k1gMnSc1	Hermes
plující	plující	k2eAgMnSc1d1	plující
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
torpédovalo	torpédovat	k5eAaPmAgNnS	torpédovat
bitevní	bitevní	k2eAgFnSc4d1	bitevní
loď	loď	k1gFnSc4	loď
Richelieu	Richelieu	k1gMnPc2	Richelieu
zakotvenou	zakotvený	k2eAgFnSc4d1	zakotvená
v	v	k7c6	v
Dakaru	Dakar	k1gInSc6	Dakar
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
ji	on	k3xPp3gFnSc4	on
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
se	se	k3xPyFc4	se
také	také	k9	také
britská	britský	k2eAgFnSc1d1	britská
středozemní	středozemní	k2eAgFnSc1d1	středozemní
flota	flota	k1gFnSc1	flota
<g/>
,	,	kIx,	,
operující	operující	k2eAgFnSc1d1	operující
z	z	k7c2	z
egyptské	egyptský	k2eAgFnSc2d1	egyptská
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
konvojů	konvoj	k1gInPc2	konvoj
z	z	k7c2	z
Malty	Malta	k1gFnSc2	Malta
<g/>
,	,	kIx,	,
utkala	utkat	k5eAaPmAgFnS	utkat
s	s	k7c7	s
italským	italský	k2eAgNnSc7d1	italské
válečným	válečný	k2eAgNnSc7d1	válečné
loďstvem	loďstvo	k1gNnSc7	loďstvo
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vzlétlo	vzlétnout	k5eAaPmAgNnS	vzlétnout
z	z	k7c2	z
HMS	HMS	kA	HMS
Eagle	Eagle	k1gInSc1	Eagle
devět	devět	k4xCc1	devět
Swordfishů	Swordfish	k1gInPc2	Swordfish
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
italskou	italský	k2eAgFnSc4d1	italská
flotu	flota	k1gFnSc4	flota
nenašlo	najít	k5eNaPmAgNnS	najít
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
svými	svůj	k3xOyFgNnPc7	svůj
torpédy	torpédo	k1gNnPc7	torpédo
neúspěšně	úspěšně	k6eNd1	úspěšně
napadlo	napadnout	k5eAaPmAgNnS	napadnout
osamocený	osamocený	k2eAgInSc4d1	osamocený
křižník	křižník	k1gInSc4	křižník
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
útočná	útočný	k2eAgFnSc1d1	útočná
vlna	vlna	k1gFnSc1	vlna
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
napadla	napadnout	k5eAaPmAgFnS	napadnout
křižníky	křižník	k1gInPc4	křižník
Trento	Trenta	k1gFnSc5	Trenta
a	a	k8xC	a
Bolzano	Bolzana	k1gFnSc5	Bolzana
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
devět	devět	k4xCc1	devět
Swordfishů	Swordfish	k1gMnPc2	Swordfish
z	z	k7c2	z
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
Eagle	Eagle	k1gFnSc2	Eagle
potopilo	potopit	k5eAaPmAgNnS	potopit
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Augusta	August	k1gMnSc2	August
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
italský	italský	k2eAgMnSc1d1	italský
torpédoborec	torpédoborec	k1gMnSc1	torpédoborec
Leone	Leo	k1gMnSc5	Leo
Pancaldo	Pancalda	k1gMnSc5	Pancalda
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
Swordfishe	Swordfishe	k1gFnSc1	Swordfishe
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
operující	operující	k2eAgNnPc1d1	operující
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1940	[number]	k4	1940
potopily	potopit	k5eAaPmAgInP	potopit
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
Zaffiro	Zaffiro	k1gNnSc4	Zaffiro
s	s	k7c7	s
motorovou	motorový	k2eAgFnSc7d1	motorová
lodí	loď	k1gFnSc7	loď
Manzoni	Manzon	k1gMnPc1	Manzon
a	a	k8xC	a
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
Euro	euro	k1gNnSc4	euro
s	s	k7c7	s
parníkem	parník	k1gInSc7	parník
Liquria	Liqurium	k1gNnSc2	Liqurium
poškodily	poškodit	k5eAaPmAgInP	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
Eagle	Eagle	k1gFnSc2	Eagle
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
její	její	k3xOp3gFnPc1	její
Swordfishe	Swordfish	k1gFnPc1	Swordfish
byly	být	k5eAaImAgFnP	být
převeleny	převelet	k5eAaPmNgFnP	převelet
k	k	k7c3	k
velitelství	velitelství	k1gNnSc3	velitelství
pouštního	pouštní	k2eAgNnSc2d1	pouštní
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
O.	O.	kA	O.
Patche	Patch	k1gMnSc2	Patch
vyslány	vyslat	k5eAaPmNgFnP	vyslat
k	k	k7c3	k
torpédovému	torpédový	k2eAgInSc3d1	torpédový
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
italskou	italský	k2eAgFnSc4d1	italská
ponorku	ponorka	k1gFnSc4	ponorka
kotvící	kotvící	k2eAgFnSc4d1	kotvící
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zásobovací	zásobovací	k2eAgFnSc7d1	zásobovací
lodí	loď	k1gFnSc7	loď
v	v	k7c6	v
Sidi	Sid	k1gFnSc6	Sid
Barrani	Barraň	k1gFnSc6	Barraň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
přibližovaly	přibližovat	k5eAaImAgFnP	přibližovat
k	k	k7c3	k
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
spatřil	spatřit	k5eAaPmAgMnS	spatřit
kapitán	kapitán	k1gMnSc1	kapitán
Patch	Patch	k1gMnSc1	Patch
jinou	jiný	k2eAgFnSc4d1	jiná
velkou	velký	k2eAgFnSc4d1	velká
ponorku	ponorka	k1gFnSc4	ponorka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
torpédem	torpédo	k1gNnSc7	torpédo
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
uprostřed	uprostřed	k6eAd1	uprostřed
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zničil	zničit	k5eAaPmAgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
dvě	dva	k4xCgNnPc4	dva
letadla	letadlo	k1gNnPc4	letadlo
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
k	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
původnímu	původní	k2eAgInSc3d1	původní
cíli	cíl	k1gInSc3	cíl
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osádky	osádka	k1gFnPc1	osádka
napadly	napadnout	k5eAaPmAgFnP	napadnout
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
kotvící	kotvící	k2eAgFnSc4d1	kotvící
ponorku	ponorka	k1gFnSc4	ponorka
<g/>
,	,	kIx,	,
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
a	a	k8xC	a
zásobovací	zásobovací	k2eAgFnSc4d1	zásobovací
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Piloti	pilot	k1gMnPc1	pilot
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
obě	dva	k4xCgNnPc4	dva
krajní	krajní	k2eAgNnPc4d1	krajní
plavidla	plavidlo	k1gNnPc4	plavidlo
a	a	k8xC	a
požár	požár	k1gInSc4	požár
z	z	k7c2	z
vybuchující	vybuchující	k2eAgFnSc2d1	vybuchující
ponorky	ponorka	k1gFnSc2	ponorka
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
i	i	k9	i
na	na	k7c4	na
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vybuchl	vybuchnout	k5eAaPmAgInS	vybuchnout
muniční	muniční	k2eAgInSc1d1	muniční
sklad	sklad	k1gInSc1	sklad
na	na	k7c4	na
zásobovací	zásobovací	k2eAgFnPc4d1	zásobovací
lodi	loď	k1gFnPc4	loď
a	a	k8xC	a
všechna	všechen	k3xTgNnPc4	všechen
tři	tři	k4xCgNnPc1	tři
plavidla	plavidlo	k1gNnPc1	plavidlo
byla	být	k5eAaImAgNnP	být
zničena	zničit	k5eAaPmNgNnP	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1940	[number]	k4	1940
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgFnP	proslavit
účastí	účast	k1gFnSc7	účast
v	v	k7c6	v
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
italské	italský	k2eAgNnSc4d1	italské
loďstvo	loďstvo	k1gNnSc4	loďstvo
kotvící	kotvící	k2eAgNnSc4d1	kotvící
v	v	k7c6	v
Tarentu	Tarento	k1gNnSc6	Tarento
(	(	kIx(	(
<g/>
Operation	Operation	k1gInSc1	Operation
Judgement	Judgement	k1gInSc1	Judgement
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
strojů	stroj	k1gInPc2	stroj
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
vybraných	vybraný	k2eAgInPc2d1	vybraný
z	z	k7c2	z
letek	letka	k1gFnPc2	letka
č.	č.	k?	č.
815	[number]	k4	815
<g/>
,	,	kIx,	,
819	[number]	k4	819
<g/>
,	,	kIx,	,
813	[number]	k4	813
a	a	k8xC	a
824	[number]	k4	824
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
z	z	k7c2	z
HMS	HMS	kA	HMS
Illustrious	Illustrious	k1gInSc1	Illustrious
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
K.	K.	kA	K.
Williamsona	Williamson	k1gMnSc2	Williamson
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
stroj	stroj	k1gInSc1	stroj
naplno	naplno	k6eAd1	naplno
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
torpédem	torpédo	k1gNnSc7	torpédo
bitevní	bitevní	k2eAgNnSc4d1	bitevní
loď	loď	k1gFnSc1	loď
Conte	Cont	k1gInSc5	Cont
di	di	k?	di
Cavour	Cavoura	k1gFnPc2	Cavoura
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
stroje	stroj	k1gInPc1	stroj
prvního	první	k4xOgNnSc2	první
roje	roj	k1gInPc1	roj
cíle	cíl	k1gInSc2	cíl
minuly	minout	k5eAaImAgInP	minout
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
roj	roj	k1gInSc1	roj
však	však	k9	však
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
možných	možný	k2eAgInPc2d1	možný
dva	dva	k4xCgInPc1	dva
zásahy	zásah	k1gInPc1	zásah
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodě	loď	k1gFnSc2	loď
Littorio	Littorio	k1gNnSc1	Littorio
<g/>
.	.	kIx.	.
</s>
<s>
Pumy	puma	k1gFnPc1	puma
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Swordfishů	Swordfish	k1gMnPc2	Swordfish
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
hangáry	hangár	k1gInPc4	hangár
základny	základna	k1gFnSc2	základna
vodních	vodní	k2eAgInPc2d1	vodní
letounů	letoun	k1gInPc2	letoun
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
devíti	devět	k4xCc2	devět
strojů	stroj	k1gInPc2	stroj
z	z	k7c2	z
Illustrious	Illustrious	k1gInSc4	Illustrious
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
bitevní	bitevní	k2eAgFnSc1d1	bitevní
loď	loď	k1gFnSc1	loď
Caio	Caio	k6eAd1	Caio
Duilio	Duilio	k6eAd1	Duilio
a	a	k8xC	a
křižník	křižník	k1gInSc1	křižník
Trento	Trento	k1gNnSc1	Trento
<g/>
.	.	kIx.	.
</s>
<s>
Fotoprůzkumná	Fotoprůzkumný	k2eAgNnPc1d1	Fotoprůzkumný
letadla	letadlo	k1gNnPc1	letadlo
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
ráno	ráno	k6eAd1	ráno
prokázala	prokázat	k5eAaPmAgFnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitevní	bitevní	k2eAgFnPc1d1	bitevní
lodě	loď	k1gFnPc1	loď
Littorio	Littorio	k6eAd1	Littorio
a	a	k8xC	a
Conte	Cont	k1gInSc5	Cont
di	di	k?	di
Cavour	Cavoura	k1gFnPc2	Cavoura
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
potopily	potopit	k5eAaPmAgFnP	potopit
v	v	k7c6	v
mělké	mělký	k2eAgFnSc6d1	mělká
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Caio	Caio	k6eAd1	Caio
Duilio	Duilio	k6eAd1	Duilio
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
musela	muset	k5eAaImAgFnS	muset
najet	najet	k5eAaPmF	najet
na	na	k7c4	na
mělčinu	mělčina	k1gFnSc4	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
poškozeny	poškozen	k2eAgFnPc1d1	poškozena
byly	být	k5eAaImAgFnP	být
i	i	k9	i
křižník	křižník	k1gInSc1	křižník
Trento	Trento	k1gNnSc1	Trento
a	a	k8xC	a
také	také	k9	také
torpédoborce	torpédoborec	k1gMnSc4	torpédoborec
Libeccio	Libeccio	k6eAd1	Libeccio
a	a	k8xC	a
Pessagno	Pessagno	k6eAd1	Pessagno
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
vše	všechen	k3xTgNnSc1	všechen
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
dvou	dva	k4xCgMnPc2	dva
ztracených	ztracený	k2eAgMnPc2d1	ztracený
Swordfishů	Swordfish	k1gMnPc2	Swordfish
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
akcí	akce	k1gFnSc7	akce
815	[number]	k4	815
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tehdy	tehdy	k6eAd1	tehdy
operovala	operovat	k5eAaImAgFnS	operovat
z	z	k7c2	z
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
vyřazení	vyřazení	k1gNnSc4	vyřazení
italského	italský	k2eAgInSc2d1	italský
křižníku	křižník	k1gInSc2	křižník
Pola	pola	k1gFnSc1	pola
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
mysu	mys	k1gInSc2	mys
Metapan	Metapana	k1gFnPc2	Metapana
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgInP	účastnit
její	její	k3xOp3gInPc1	její
dva	dva	k4xCgInPc1	dva
Swordfishe	Swordfish	k1gInPc1	Swordfish
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
šest	šest	k4xCc1	šest
"	"	kIx"	"
<g/>
Mečounů	mečoun	k1gMnPc2	mečoun
<g/>
"	"	kIx"	"
814	[number]	k4	814
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
z	z	k7c2	z
HMS	HMS	kA	HMS
Hermes	Hermes	k1gMnSc1	Hermes
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
v	v	k7c6	v
Shaibahu	Shaibah	k1gInSc6	Shaibah
<g/>
,	,	kIx,	,
účastnilo	účastnit	k5eAaImAgNnS	účastnit
při	při	k7c6	při
potlačení	potlačení	k1gNnSc6	potlačení
irácké	irácký	k2eAgFnSc2d1	irácká
proněmecké	proněmecký	k2eAgFnSc2d1	proněmecká
vzpoury	vzpoura	k1gFnSc2	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
rovněž	rovněž	k9	rovněž
Swordfishe	Swordfishe	k1gInSc1	Swordfishe
815	[number]	k4	815
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
z	z	k7c2	z
Kypru	Kypr	k1gInSc2	Kypr
napadaly	napadat	k5eAaPmAgFnP	napadat
vichistické	vichistický	k2eAgFnPc1d1	vichistická
lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
cíle	cíl	k1gInPc1	cíl
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letouny	letoun	k1gInPc1	letoun
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
se	se	k3xPyFc4	se
také	také	k9	také
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
zničení	zničení	k1gNnSc6	zničení
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodě	loď	k1gFnSc2	loď
Bismarck	Bismarck	k1gMnSc1	Bismarck
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
HMS	HMS	kA	HMS
Victorious	Victorious	k1gInSc1	Victorious
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
190	[number]	k4	190
km	km	kA	km
od	od	k7c2	od
Bismarcku	Bismarck	k1gInSc2	Bismarck
a	a	k8xC	a
vyslala	vyslat	k5eAaPmAgFnS	vyslat
devět	devět	k4xCc4	devět
Swordfishů	Swordfish	k1gInPc2	Swordfish
825	[number]	k4	825
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
kapitána	kapitán	k1gMnSc2	kapitán
E.	E.	kA	E.
Esmonda	Esmond	k1gMnSc2	Esmond
<g/>
.	.	kIx.	.
</s>
<s>
Torpédové	torpédový	k2eAgInPc1d1	torpédový
letouny	letoun	k1gInPc1	letoun
sice	sice	k8xC	sice
za	za	k7c2	za
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
nalezly	naleznout	k5eAaPmAgFnP	naleznout
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
bitevní	bitevní	k2eAgFnSc4d1	bitevní
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
britské	britský	k2eAgNnSc4d1	Britské
torpédo	torpédo	k1gNnSc4	torpédo
ji	on	k3xPp3gFnSc4	on
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
do	do	k7c2	do
boční	boční	k2eAgFnSc2d1	boční
obšívky	obšívka	k1gFnSc2	obšívka
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
málo	málo	k4c1	málo
škod	škoda	k1gFnPc2	škoda
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
Bismarck	Bismarck	k1gMnSc1	Bismarck
musel	muset	k5eAaImAgMnS	muset
snížit	snížit	k5eAaPmF	snížit
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
za	za	k7c2	za
zhoršujícího	zhoršující	k2eAgNnSc2d1	zhoršující
se	se	k3xPyFc4	se
počasí	počasí	k1gNnSc2	počasí
podařilo	podařit	k5eAaPmAgNnS	podařit
setřást	setřást	k5eAaPmF	setřást
své	svůj	k3xOyFgMnPc4	svůj
pronásledovatele	pronásledovatel	k1gMnPc4	pronásledovatel
a	a	k8xC	a
mířil	mířit	k5eAaImAgMnS	mířit
k	k	k7c3	k
Brestu	Brest	k1gInSc3	Brest
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Gibraltaru	Gibraltar	k1gInSc2	Gibraltar
byl	být	k5eAaImAgMnS	být
britské	britský	k2eAgFnSc3d1	britská
Home	Home	k1gFnSc3	Home
Fleet	Fleet	k1gInSc4	Fleet
vyslán	vyslán	k2eAgInSc4d1	vyslán
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
svaz	svaz	k1gInSc4	svaz
Force	force	k1gFnSc2	force
H	H	kA	H
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
letadlovou	letadlový	k2eAgFnSc4d1	letadlová
loď	loď	k1gFnSc4	loď
Ark	Ark	k1gFnPc2	Ark
Royal	Royal	k1gInSc4	Royal
s	s	k7c7	s
810	[number]	k4	810
<g/>
.	.	kIx.	.
a	a	k8xC	a
818	[number]	k4	818
<g/>
.	.	kIx.	.
letkou	letka	k1gFnSc7	letka
Swordfishů	Swordfish	k1gMnPc2	Swordfish
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Bismarck	Bismarcka	k1gFnPc2	Bismarcka
zahlédla	zahlédnout	k5eAaPmAgFnS	zahlédnout
osádka	osádka	k1gFnSc1	osádka
letounu	letoun	k1gInSc2	letoun
Catalina	Catalin	k2eAgNnSc2d1	Catalino
Pobřežního	pobřežní	k2eAgNnSc2d1	pobřežní
velitelství	velitelství	k1gNnSc2	velitelství
RAF	raf	k0	raf
a	a	k8xC	a
HMS	HMS	kA	HMS
Ark	Ark	k1gMnSc1	Ark
Royal	Royal	k1gMnSc1	Royal
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
od	od	k7c2	od
Bismarcku	Bismarck	k1gInSc2	Bismarck
<g/>
.	.	kIx.	.
15	[number]	k4	15
Swordfishů	Swordfish	k1gInPc2	Swordfish
Mk	Mk	k1gFnPc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
kapitána	kapitán	k1gMnSc2	kapitán
T.	T.	kA	T.
P.	P.	kA	P.
Cooda	Cooda	k1gFnSc1	Cooda
však	však	k9	však
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
vichřici	vichřice	k1gFnSc6	vichřice
omylem	omylem	k6eAd1	omylem
torpédovala	torpédovat	k5eAaPmAgFnS	torpédovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
těžký	těžký	k2eAgInSc4d1	těžký
křižník	křižník	k1gInSc4	křižník
HMS	HMS	kA	HMS
Sheffield	Sheffield	k1gInSc1	Sheffield
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Bismarck	Bismarck	k1gInSc1	Bismarck
sledoval	sledovat	k5eAaImAgInS	sledovat
z	z	k7c2	z
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
všem	všecek	k3xTgNnPc3	všecek
odhozeným	odhozený	k2eAgNnPc3d1	odhozené
torpédům	torpédo	k1gNnPc3	torpédo
dokázal	dokázat	k5eAaPmAgInS	dokázat
vyhnout	vyhnout	k5eAaPmF	vyhnout
a	a	k8xC	a
letouny	letoun	k1gInPc1	letoun
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgInP	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
letadlovou	letadlový	k2eAgFnSc4d1	letadlová
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
znovu	znovu	k6eAd1	znovu
vzlétly	vzlétnout	k5eAaPmAgInP	vzlétnout
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
a	a	k8xC	a
za	za	k7c2	za
husté	hustý	k2eAgFnSc2d1	hustá
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
palby	palba	k1gFnSc2	palba
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
Bismarck	Bismarck	k1gInSc4	Bismarck
z	z	k7c2	z
několika	několik	k4yIc2	několik
směrů	směr	k1gInPc2	směr
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
v	v	k7c6	v
rojích	roj	k1gInPc6	roj
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
až	až	k6eAd1	až
třech	tři	k4xCgFnPc6	tři
v	v	k7c6	v
minimální	minimální	k2eAgFnSc3d1	minimální
výšce	výška	k1gFnSc3	výška
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
torpéd	torpédo	k1gNnPc2	torpédo
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
kormidla	kormidlo	k1gNnSc2	kormidlo
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
loď	loď	k1gFnSc1	loď
mohla	moct	k5eAaImAgFnS	moct
plout	plout	k5eAaImF	plout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Bitevní	bitevní	k2eAgFnPc1d1	bitevní
lodě	loď	k1gFnPc1	loď
Home	Home	k1gFnPc2	Home
Fleet	Fleeta	k1gFnPc2	Fleeta
neovladatelný	ovladatelný	k2eNgInSc4d1	neovladatelný
Bismarck	Bismarck	k1gInSc4	Bismarck
nakonec	nakonec	k6eAd1	nakonec
potopily	potopit	k5eAaPmAgInP	potopit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1941	[number]	k4	1941
ustavilo	ustavit	k5eAaPmAgNnS	ustavit
a	a	k8xC	a
Swordfishi	Swordfishi	k1gNnSc1	Swordfishi
vybavilo	vybavit	k5eAaPmAgNnS	vybavit
anglické	anglický	k2eAgNnSc1d1	anglické
velení	velení	k1gNnSc1	velení
ještě	ještě	k6eAd1	ještě
letky	letka	k1gFnSc2	letka
č.	č.	k?	č.
833	[number]	k4	833
a	a	k8xC	a
834	[number]	k4	834
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
měla	mít	k5eAaImAgFnS	mít
základny	základna	k1gFnPc4	základna
na	na	k7c6	na
doprovodných	doprovodný	k2eAgFnPc6d1	doprovodná
letadlových	letadlový	k2eAgFnPc6d1	letadlová
lodích	loď	k1gFnPc6	loď
HMS	HMS	kA	HMS
Biter	Biter	k1gInSc4	Biter
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Avenger	Avenger	k1gInSc1	Avenger
a	a	k8xC	a
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
HMS	HMS	kA	HMS
Argus	Argus	k1gMnSc1	Argus
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c6	na
doprovodných	doprovodný	k2eAgFnPc6d1	doprovodná
letadlových	letadlový	k2eAgFnPc6d1	letadlová
lodích	loď	k1gFnPc6	loď
HMS	HMS	kA	HMS
Archer	Archra	k1gFnPc2	Archra
<g/>
,	,	kIx,	,
HMS	HMS	kA	HMS
Hunter	Hunter	k1gInSc1	Hunter
a	a	k8xC	a
HMS	HMS	kA	HMS
Battler	Battler	k1gInSc1	Battler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1942	[number]	k4	1942
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
německé	německý	k2eAgFnPc4d1	německá
válečné	válečný	k2eAgFnPc4d1	válečná
lodě	loď	k1gFnPc4	loď
Scharnhorst	Scharnhorst	k1gInSc4	Scharnhorst
<g/>
,	,	kIx,	,
Gneisenau	Gneisenaa	k1gFnSc4	Gneisenaa
a	a	k8xC	a
Prinz	Prinz	k1gInSc4	Prinz
Eugen	Eugen	k2eAgInSc4d1	Eugen
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
kapitána	kapitán	k1gMnSc2	kapitán
Esmonda	Esmond	k1gMnSc2	Esmond
Fairey	Fairea	k1gMnSc2	Fairea
Swordfishe	Swordfish	k1gMnSc2	Swordfish
Mk	Mk	k1gMnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
od	od	k7c2	od
825	[number]	k4	825
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
se	s	k7c7	s
základnou	základna	k1gFnSc7	základna
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Manstonu	Manston	k1gInSc6	Manston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
Swordfishe	Swordfishe	k1gFnSc1	Swordfishe
Mk	Mk	k1gFnSc2	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
byly	být	k5eAaImAgInP	být
jednotkám	jednotka	k1gFnPc3	jednotka
dodány	dodat	k5eAaPmNgInP	dodat
začátkem	začátkem	k7c2	začátkem
jara	jaro	k1gNnSc2	jaro
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1943	[number]	k4	1943
jeden	jeden	k4xCgInSc4	jeden
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
Mk	Mk	k1gFnPc2	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
od	od	k7c2	od
819	[number]	k4	819
<g/>
.	.	kIx.	.
letky	letka	k1gFnSc2	letka
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
letadlové	letadlový	k2eAgFnSc2d1	letadlová
lodi	loď	k1gFnSc2	loď
Archer	Archra	k1gFnPc2	Archra
<g/>
,	,	kIx,	,
pilotovaný	pilotovaný	k2eAgInSc1d1	pilotovaný
poručíkem	poručík	k1gMnSc7	poručík
H.	H.	kA	H.
Horrockem	Horrocko	k1gNnSc7	Horrocko
<g/>
,	,	kIx,	,
1280	[number]	k4	1280
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
irského	irský	k2eAgNnSc2d1	irské
pobřeží	pobřeží	k1gNnSc2	pobřeží
přispěl	přispět	k5eAaPmAgMnS	přispět
raketami	raketa	k1gFnPc7	raketa
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
německé	německý	k2eAgFnSc2d1	německá
ponorky	ponorka	k1gFnSc2	ponorka
U	u	k7c2	u
752	[number]	k4	752
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
vzniklým	vzniklý	k2eAgInSc7d1	vzniklý
útvarem	útvar	k1gInSc7	útvar
Swordfishů	Swordfish	k1gInPc2	Swordfish
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
860	[number]	k4	860
<g/>
.	.	kIx.	.
letka	letka	k1gFnSc1	letka
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Swordfishe	Swordfishe	k1gFnSc1	Swordfishe
Mk	Mk	k1gFnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
II	II	kA	II
a	a	k8xC	a
Mk	Mk	k1gMnSc1	Mk
<g/>
.	.	kIx.	.
<g/>
III	III	kA	III
letek	letek	k1gInSc1	letek
číslo	číslo	k1gNnSc1	číslo
835	[number]	k4	835
<g/>
,	,	kIx,	,
836	[number]	k4	836
<g/>
,	,	kIx,	,
837	[number]	k4	837
<g/>
,	,	kIx,	,
838	[number]	k4	838
<g/>
,	,	kIx,	,
840	[number]	k4	840
a	a	k8xC	a
842	[number]	k4	842
dále	daleko	k6eAd2	daleko
vzlétaly	vzlétat	k5eAaImAgFnP	vzlétat
z	z	k7c2	z
palub	paluba	k1gFnPc2	paluba
obchodních	obchodní	k2eAgFnPc2d1	obchodní
letadlových	letadlový	k2eAgFnPc2d1	letadlová
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
MAC	Mac	kA	Mac
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
ponorkám	ponorka	k1gFnPc3	ponorka
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
konvojů	konvoj	k1gInPc2	konvoj
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
i	i	k8xC	i
na	na	k7c6	na
lodních	lodní	k2eAgFnPc6d1	lodní
trasách	trasa	k1gFnPc6	trasa
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1944	[number]	k4	1944
hlubinnými	hlubinný	k2eAgFnPc7d1	hlubinná
náložemi	nálož	k1gFnPc7	nálož
potopily	potopit	k5eAaPmAgFnP	potopit
ponorky	ponorka	k1gFnPc4	ponorka
U	u	k7c2	u
277	[number]	k4	277
<g/>
,	,	kIx,	,
U	u	k7c2	u
674	[number]	k4	674
a	a	k8xC	a
U	u	k7c2	u
959	[number]	k4	959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgInPc1d1	hlavní
technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc1	údaj
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
Mk	Mk	k1gFnSc4	Mk
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
v	v	k7c6	v
plovákové	plovákový	k2eAgFnSc6d1	plováková
verzi	verze	k1gFnSc6	verze
</s>
</p>
<p>
<s>
Osádka	osádka	k1gFnSc1	osádka
<g/>
:	:	kIx,	:
3	[number]	k4	3
(	(	kIx(	(
<g/>
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
,	,	kIx,	,
radista-střelec	radistatřelec	k1gMnSc1	radista-střelec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
<g/>
:	:	kIx,	:
13,87	[number]	k4	13,87
m	m	kA	m
</s>
</p>
<p>
<s>
Šířka	šířka	k1gFnSc1	šířka
se	se	k3xPyFc4	se
slož	slož	k1gFnSc4	slož
<g/>
.	.	kIx.	.
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
:	:	kIx,	:
5,26	[number]	k4	5,26
m	m	kA	m
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
12,30	[number]	k4	12,30
m	m	kA	m
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
4,45	[number]	k4	4,45
m	m	kA	m
</s>
</p>
<p>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
<g/>
:	:	kIx,	:
56,40	[number]	k4	56,40
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
prázdného	prázdný	k2eAgInSc2d1	prázdný
letounu	letoun	k1gInSc2	letoun
<g/>
:	:	kIx,	:
2267	[number]	k4	2267
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
4037	[number]	k4	4037
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
1	[number]	k4	1
×	×	k?	×
devítiválcový	devítiválcový	k2eAgMnSc1d1	devítiválcový
hvězdicový	hvězdicový	k2eAgMnSc1d1	hvězdicový
<g/>
,	,	kIx,	,
vzduchem	vzduch	k1gInSc7	vzduch
chlazený	chlazený	k2eAgInSc1d1	chlazený
motor	motor	k1gInSc1	motor
Bristol	Bristol	k1gInSc1	Bristol
Pegasus	Pegasus	k1gMnSc1	Pegasus
IIIM3	IIIM3	k1gMnSc1	IIIM3
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
pohonné	pohonný	k2eAgFnSc2d1	pohonná
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
690	[number]	k4	690
k	k	k7c3	k
(	(	kIx(	(
<g/>
507	[number]	k4	507
kW	kW	kA	kW
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
v	v	k7c6	v
1400	[number]	k4	1400
m	m	kA	m
<g/>
:	:	kIx,	:
228	[number]	k4	228
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
175	[number]	k4	175
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
:	:	kIx,	:
4,4	[number]	k4	4,4
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
4	[number]	k4	4
340	[number]	k4	340
m	m	kA	m
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
dolet	dolet	k1gInSc1	dolet
s	s	k7c7	s
příd	příd	k?	příd
<g/>
.	.	kIx.	.
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
:	:	kIx,	:
1	[number]	k4	1
700	[number]	k4	700
km	km	kA	km
</s>
</p>
<p>
<s>
===	===	k?	===
Výzbroj	výzbroj	k1gInSc4	výzbroj
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
×	×	k?	×
pevný	pevný	k2eAgInSc1d1	pevný
synchronizovaný	synchronizovaný	k2eAgInSc1d1	synchronizovaný
kulomet	kulomet	k1gInSc1	kulomet
Vickers	Vickers	k1gInSc1	Vickers
ráže	ráže	k1gFnSc1	ráže
7,7	[number]	k4	7,7
mm	mm	kA	mm
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
</s>
</p>
<p>
<s>
1	[number]	k4	1
×	×	k?	×
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
kulomet	kulomet	k1gInSc1	kulomet
Lewis	Lewis	k1gFnSc2	Lewis
či	či	k8xC	či
Vickers	Vickers	k1gInSc1	Vickers
K	K	kA	K
ráže	ráže	k1gFnSc2	ráže
7,7	[number]	k4	7,7
mm	mm	kA	mm
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
kabině	kabina	k1gFnSc6	kabina
</s>
</p>
<p>
<s>
1	[number]	k4	1
×	×	k?	×
torpédo	torpédo	k1gNnSc4	torpédo
ráže	ráže	k1gFnSc2	ráže
457	[number]	k4	457
mm	mm	kA	mm
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
730	[number]	k4	730
kg	kg	kA	kg
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
námořní	námořní	k2eAgFnSc1d1	námořní
mina	mina	k1gFnSc1	mina
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
680	[number]	k4	680
kg	kg	kA	kg
pod	pod	k7c7	pod
trupem	trup	k1gInSc7	trup
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
neřízených	řízený	k2eNgFnPc2d1	neřízená
raket	raketa	k1gFnPc2	raketa
o	o	k7c4	o
hmotnost	hmotnost	k1gFnSc4	hmotnost
27	[number]	k4	27
kg	kg	kA	kg
typu	typ	k1gInSc2	typ
RP-3	RP-3	k1gFnSc2	RP-3
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NICCOLI	NICCOLI	kA	NICCOLI
<g/>
,	,	kIx,	,
Riccardo	Riccardo	k1gNnSc1	Riccardo
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
současné	současný	k2eAgFnPc1d1	současná
i	i	k8xC	i
historické	historický	k2eAgFnPc1d1	historická
typy	typa	k1gFnPc1	typa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
651	[number]	k4	651
<g/>
-	-	kIx~	-
<g/>
x.	x.	k?	x.
Kapitola	kapitola	k1gFnSc1	kapitola
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
97	[number]	k4	97
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfish	k1gInSc1	Swordfish
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kamufláže	kamufláž	k1gFnPc1	kamufláž
letounu	letoun	k1gInSc2	letoun
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
</s>
</p>
<p>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
letounu	letoun	k1gInSc2	letoun
Fairey	Fairea	k1gFnSc2	Fairea
Swordfish	Swordfisha	k1gFnPc2	Swordfisha
</s>
</p>
