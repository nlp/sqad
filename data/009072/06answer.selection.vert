<s>
Výroba	výroba	k1gFnSc1	výroba
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
přírodních	přírodní	k2eAgFnPc6d1	přírodní
rudách	ruda	k1gFnPc6	ruda
jej	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
niob	niob	k1gInSc1	niob
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
chemické	chemický	k2eAgNnSc1d1	chemické
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
