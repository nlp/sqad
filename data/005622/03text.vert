<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
rody	rod	k1gInPc4	rod
chobotnatců	chobotnatec	k1gMnPc2	chobotnatec
-	-	kIx~	-
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
a	a	k8xC	a
Elephas	Elephas	k1gMnSc1	Elephas
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
suchozemský	suchozemský	k2eAgMnSc1d1	suchozemský
savec	savec	k1gMnSc1	savec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
narození	narození	k1gNnSc6	narození
váží	vážit	k5eAaImIp3nS	vážit
okolo	okolo	k7c2	okolo
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
slona	slon	k1gMnSc2	slon
je	být	k5eAaImIp3nS	být
březí	březí	k1gNnSc4	březí
20	[number]	k4	20
až	až	k9	až
22	[number]	k4	22
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
doba	doba	k1gFnSc1	doba
březosti	březost	k1gFnSc2	březost
u	u	k7c2	u
suchozemského	suchozemský	k2eAgNnSc2d1	suchozemské
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
60	[number]	k4	60
až	až	k9	až
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgMnSc2d3	veliký
slona	slon	k1gMnSc2	slon
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
vážil	vážit	k5eAaImAgMnS	vážit
12	[number]	k4	12
000	[number]	k4	000
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Rody	rod	k1gInPc1	rod
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
slon	slon	k1gMnSc1	slon
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tři	tři	k4xCgInPc1	tři
žijící	žijící	k2eAgInPc1d1	žijící
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
rodů	rod	k1gInPc2	rod
-	-	kIx~	-
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
(	(	kIx(	(
<g/>
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
a	a	k8xC	a
pralesní	pralesní	k2eAgMnSc1d1	pralesní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Elephas	Elephas	k1gMnSc1	Elephas
(	(	kIx(	(
<g/>
slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
blízkým	blízký	k2eAgMnSc7d1	blízký
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
byl	být	k5eAaImAgMnS	být
i	i	k9	i
vyhynulý	vyhynulý	k2eAgMnSc1d1	vyhynulý
mamut	mamut	k1gMnSc1	mamut
(	(	kIx(	(
<g/>
Mammuthus	Mammuthus	k1gMnSc1	Mammuthus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
svým	svůj	k3xOyFgInSc7	svůj
chobotem	chobot	k1gInSc7	chobot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
horního	horní	k2eAgInSc2d1	horní
rtu	ret	k1gInSc2	ret
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoko	vysoko	k6eAd1	vysoko
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopen	schopen	k2eAgMnSc1d1	schopen
jím	jíst	k5eAaImIp1nS	jíst
zvednout	zvednout	k5eAaPmF	zvednout
náklad	náklad	k1gInSc4	náklad
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
až	až	k9	až
1	[number]	k4	1
tuny	tuna	k1gFnPc4	tuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horku	horko	k1gNnSc6	horko
si	se	k3xPyFc3	se
často	často	k6eAd1	často
chobotem	chobot	k1gInSc7	chobot
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
házejí	házet	k5eAaImIp3nP	házet
písek	písek	k1gInSc4	písek
<g/>
,	,	kIx,	,
prach	prach	k1gInSc4	prach
či	či	k8xC	či
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
kly	kel	k1gInPc1	kel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
velké	velký	k2eAgInPc1d1	velký
zuby	zub	k1gInPc1	zub
jim	on	k3xPp3gMnPc3	on
vyčnívají	vyčnívat	k5eAaImIp3nP	vyčnívat
z	z	k7c2	z
tlamy	tlama	k1gFnSc2	tlama
<g/>
.	.	kIx.	.
</s>
<s>
Sloní	sloní	k2eAgInPc1d1	sloní
kly	kel	k1gInPc1	kel
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
jejich	jejich	k3xOp3gInSc2	jejich
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
jsou	být	k5eAaImIp3nP	být
býložraví	býložravý	k2eAgMnPc1d1	býložravý
a	a	k8xC	a
tráví	trávit	k5eAaImIp3nP	trávit
denně	denně	k6eAd1	denně
až	až	k9	až
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
konzumací	konzumace	k1gFnPc2	konzumace
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
potravy	potrava	k1gFnSc2	potrava
tvoří	tvořit	k5eAaImIp3nS	tvořit
trávy	tráva	k1gFnPc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
listí	listí	k1gNnSc4	listí
<g/>
,	,	kIx,	,
kořínky	kořínek	k1gInPc4	kořínek
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
,	,	kIx,	,
semínka	semínko	k1gNnPc4	semínko
či	či	k8xC	či
květiny	květina	k1gFnPc4	květina
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
slon	slon	k1gMnSc1	slon
dokáže	dokázat	k5eAaPmIp3nS	dokázat
sežrat	sežrat	k5eAaPmF	sežrat
100	[number]	k4	100
až	až	k9	až
320	[number]	k4	320
kg	kg	kA	kg
potravy	potrava	k1gFnSc2	potrava
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgNnSc1d1	trávicí
ústrojí	ústrojí	k1gNnSc1	ústrojí
slona	slon	k1gMnSc2	slon
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
zpracovat	zpracovat	k5eAaPmF	zpracovat
pouze	pouze	k6eAd1	pouze
40	[number]	k4	40
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
zkonzumovaného	zkonzumovaný	k2eAgNnSc2d1	zkonzumované
množství	množství	k1gNnSc2	množství
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
projde	projít	k5eAaPmIp3nS	projít
nestráven	strávit	k5eNaPmNgInS	strávit
zažívacím	zažívací	k2eAgInSc7d1	zažívací
traktem	trakt	k1gInSc7	trakt
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
nejprve	nejprve	k6eAd1	nejprve
z	z	k7c2	z
jemu	on	k3xPp3gMnSc3	on
podobného	podobný	k2eAgMnSc4d1	podobný
předka	předek	k1gMnSc4	předek
mastodonta	mastodont	k1gMnSc4	mastodont
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c4	v
mamuta	mamut	k1gMnSc4	mamut
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
jídlu	jídlo	k1gNnSc3	jídlo
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
jeho	jeho	k3xOp3gMnPc1	jeho
předci	předek	k1gMnPc1	předek
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
legální	legální	k2eAgInSc1d1	legální
nebo	nebo	k8xC	nebo
ilegální	ilegální	k2eAgMnSc1d1	ilegální
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
neočekávané	očekávaný	k2eNgInPc4d1	neočekávaný
důsledky	důsledek	k1gInPc4	důsledek
v	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Afričtí	africký	k2eAgMnPc1d1	africký
lovci	lovec	k1gMnPc1	lovec
slonů	slon	k1gMnPc2	slon
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
slony	slon	k1gMnPc4	slon
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
kly	kel	k1gInPc7	kel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dává	dávat	k5eAaImIp3nS	dávat
šanci	šance	k1gFnSc4	šance
přežít	přežít	k5eAaPmF	přežít
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
slonům	slon	k1gMnPc3	slon
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
kly	kel	k1gInPc7	kel
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
klů	kel	k1gInPc2	kel
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
propagaci	propagace	k1gFnSc3	propagace
genu	gen	k1gInSc2	gen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slon	slon	k1gMnSc1	slon
nemá	mít	k5eNaImIp3nS	mít
kly	kel	k1gInPc4	kel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
populacích	populace	k1gFnPc6	populace
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
slonů	slon	k1gMnPc2	slon
bez	bez	k7c2	bez
klů	kel	k1gInPc2	kel
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
klů	kel	k1gInPc2	kel
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
velmi	velmi	k6eAd1	velmi
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
genetická	genetický	k2eAgFnSc1d1	genetická
anomálie	anomálie	k1gFnSc1	anomálie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
stává	stávat	k5eAaImIp3nS	stávat
velmi	velmi	k6eAd1	velmi
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
dědičným	dědičný	k2eAgInSc7d1	dědičný
rysem	rys	k1gInSc7	rys
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
soustavným	soustavný	k2eAgInSc7d1	soustavný
lovem	lov	k1gInSc7	lov
slonů	slon	k1gMnPc2	slon
s	s	k7c7	s
kly	kel	k1gInPc7	kel
a	a	k8xC	a
prosazováním	prosazování	k1gNnSc7	prosazování
slonů	slon	k1gMnPc2	slon
bez	bez	k7c2	bez
klů	kel	k1gInPc2	kel
nakonec	nakonec	k9	nakonec
kly	kel	k1gInPc4	kel
u	u	k7c2	u
afrických	africký	k2eAgMnPc2d1	africký
slonů	slon	k1gMnPc2	slon
zcela	zcela	k6eAd1	zcela
zmizí	zmizet	k5eAaPmIp3nP	zmizet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
mít	mít	k5eAaImF	mít
dramatický	dramatický	k2eAgInSc4d1	dramatický
dopad	dopad	k1gInSc4	dopad
jak	jak	k8xS	jak
na	na	k7c4	na
slony	slon	k1gMnPc4	slon
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
své	svůj	k3xOyFgNnSc4	svůj
kly	kel	k1gInPc1	kel
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
"	"	kIx"	"
<g/>
orání	orání	k1gNnSc3	orání
<g/>
"	"	kIx"	"
země	země	k1gFnSc1	země
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
průchodu	průchod	k1gInSc2	průchod
hustou	hustý	k2eAgFnSc7d1	hustá
vegetací	vegetace	k1gFnSc7	vegetace
či	či	k8xC	či
při	při	k7c6	při
boji	boj	k1gInSc6	boj
mezi	mezi	k7c7	mezi
samci	samec	k1gMnPc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Budou	být	k5eAaImBp3nP	být
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
rodit	rodit	k5eAaImF	rodit
převážně	převážně	k6eAd1	převážně
sloni	slon	k1gMnPc1	slon
bez	bez	k7c2	bez
klů	kel	k1gInPc2	kel
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
chování	chování	k1gNnSc1	chování
slonů	slon	k1gMnPc2	slon
dramaticky	dramaticky	k6eAd1	dramaticky
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
byli	být	k5eAaImAgMnP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
používáni	používat	k5eAaImNgMnP	používat
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Váleční	váleční	k2eAgMnPc1d1	váleční
sloni	slon	k1gMnPc1	slon
byli	být	k5eAaImAgMnP	být
používáni	používat	k5eAaImNgMnP	používat
armádami	armáda	k1gFnPc7	armáda
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
a	a	k8xC	a
Peršany	peršan	k1gInPc7	peršan
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
používáni	používán	k2eAgMnPc1d1	používán
v	v	k7c6	v
hellénistických	hellénistický	k2eAgFnPc6d1	hellénistická
říších	říš	k1gFnPc6	říš
<g/>
,	,	kIx,	,
Ptolemaiovců	Ptolemaiovec	k1gMnPc2	Ptolemaiovec
a	a	k8xC	a
Seleukovců	Seleukovec	k1gMnPc2	Seleukovec
<g/>
.	.	kIx.	.
</s>
<s>
Kartaginský	kartaginský	k2eAgMnSc1d1	kartaginský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Hannibal	Hannibal	k1gInSc4	Hannibal
použil	použít	k5eAaPmAgMnS	použít
slony	slon	k1gMnPc4	slon
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
dokonce	dokonce	k9	dokonce
překročil	překročit	k5eAaPmAgMnS	překročit
Alpy	Alpy	k1gFnPc4	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Indičtí	indický	k2eAgMnPc1d1	indický
sloni	slon	k1gMnPc1	slon
jsou	být	k5eAaImIp3nP	být
používání	používání	k1gNnSc4	používání
pro	pro	k7c4	pro
transport	transport	k1gInSc4	transport
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
známí	známý	k1gMnPc1	známý
v	v	k7c6	v
cirkusových	cirkusový	k2eAgNnPc6d1	cirkusové
představeních	představení	k1gNnPc6	představení
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
sloni	slon	k1gMnPc1	slon
nebyli	být	k5eNaImAgMnP	být
nikdy	nikdy	k6eAd1	nikdy
plně	plně	k6eAd1	plně
domestikováni	domestikován	k2eAgMnPc1d1	domestikován
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
slonů	slon	k1gMnPc2	slon
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
období	období	k1gNnSc6	období
říje	říje	k1gFnSc2	říje
velmi	velmi	k6eAd1	velmi
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
používaní	používaný	k2eAgMnPc1d1	používaný
lidmi	člověk	k1gMnPc7	člověk
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Váleční	váleční	k2eAgMnPc1d1	váleční
sloni	slon	k1gMnPc1	slon
jsou	být	k5eAaImIp3nP	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ekonomičtější	ekonomický	k2eAgNnSc1d2	ekonomičtější
chytit	chytit	k5eAaPmF	chytit
divokého	divoký	k2eAgMnSc4d1	divoký
mladého	mladý	k2eAgMnSc4d1	mladý
slona	slon	k1gMnSc4	slon
<g/>
,	,	kIx,	,
než	než	k8xS	než
vychovat	vychovat	k5eAaPmF	vychovat
slona	slon	k1gMnSc4	slon
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgMnPc4d1	africký
slony	slon	k1gMnPc4	slon
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nepodaří	podařit	k5eNaPmIp3nS	podařit
domestikovat	domestikovat	k5eAaBmF	domestikovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
někteří	některý	k3yIgMnPc1	některý
dobří	dobrý	k2eAgMnPc1d1	dobrý
chovatelé	chovatel	k1gMnPc1	chovatel
uspěli	uspět	k5eAaPmAgMnP	uspět
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přivedli	přivést	k5eAaPmAgMnP	přivést
k	k	k7c3	k
africkému	africký	k2eAgMnSc3d1	africký
slonu	slon	k1gMnSc3	slon
slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
slonů	slon	k1gMnPc2	slon
–	–	k?	–
slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
pralesní	pralesní	k2eAgMnSc1d1	pralesní
a	a	k8xC	a
slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Elephas	Elephas	k1gMnSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Africký	africký	k2eAgMnSc1d1	africký
slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
<g/>
(	(	kIx(	(
<g/>
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
4	[number]	k4	4
m	m	kA	m
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
7	[number]	k4	7
500	[number]	k4	500
kg	kg	kA	kg
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgNnPc4d2	veliký
uši	ucho	k1gNnPc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Cirkulace	cirkulace	k1gFnSc1	cirkulace
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
tepnách	tepna	k1gFnPc6	tepna
boltců	boltec	k1gInPc2	boltec
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
ochlazování	ochlazování	k1gNnSc3	ochlazování
pod	pod	k7c7	pod
africkým	africký	k2eAgNnSc7d1	africké
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
samice	samice	k1gFnSc1	samice
slona	slon	k1gMnSc2	slon
afrického	africký	k2eAgInSc2d1	africký
má	mít	k5eAaImIp3nS	mít
kly	kel	k1gInPc1	kel
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
je	on	k3xPp3gMnPc4	on
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
asijského	asijský	k2eAgMnSc2d1	asijský
bratrance	bratranec	k1gMnSc2	bratranec
má	mít	k5eAaImIp3nS	mít
africký	africký	k2eAgMnSc1d1	africký
slon	slon	k1gMnSc1	slon
na	na	k7c6	na
chobotu	chobot	k1gInSc6	chobot
dva	dva	k4xCgInPc4	dva
"	"	kIx"	"
<g/>
prsty	prst	k1gInPc7	prst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
dvě	dva	k4xCgFnPc1	dva
populace	populace	k1gFnPc1	populace
slonů	slon	k1gMnPc2	slon
–	–	k?	–
savanní	savanní	k2eAgInSc1d1	savanní
a	a	k8xC	a
pralesní	pralesní	k2eAgInSc1d1	pralesní
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
genetické	genetický	k2eAgFnPc1d1	genetická
studie	studie	k1gFnPc1	studie
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pralesní	pralesní	k2eAgFnSc1d1	pralesní
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Loxodonta	Loxodonta	k1gFnSc1	Loxodonta
cyclotis	cyclotis	k1gFnSc1	cyclotis
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
pralesní	pralesní	k2eAgMnSc1d1	pralesní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
savanová	savanová	k1gFnSc1	savanová
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	african	k1gMnSc2	african
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reklasifikace	reklasifikace	k1gFnSc1	reklasifikace
druhu	druh	k1gInSc2	druh
byla	být	k5eAaImAgFnS	být
důkazem	důkaz	k1gInSc7	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dvě	dva	k4xCgFnPc4	dva
tak	tak	k9	tak
malé	malý	k2eAgFnPc1d1	malá
populace	populace	k1gFnPc1	populace
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
významný	významný	k2eAgInSc1d1	významný
objev	objev	k1gInSc1	objev
-	-	kIx~	-
Slon	slon	k1gMnSc1	slon
nepálský	nepálský	k2eAgMnSc1d1	nepálský
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
himálajský	himálajský	k2eAgInSc1d1	himálajský
-	-	kIx~	-
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
nebyly	být	k5eNaImAgFnP	být
brány	brána	k1gFnPc1	brána
vědci	vědec	k1gMnPc7	vědec
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
povedlo	povést	k5eAaPmAgNnS	povést
badateli	badatel	k1gMnSc3	badatel
Johnu	John	k1gMnSc3	John
Blashford-Snellovi	Blashford-Snell	k1gMnSc3	Blashford-Snell
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
fotograficky	fotograficky	k6eAd1	fotograficky
zdokumentovat	zdokumentovat	k5eAaPmF	zdokumentovat
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
agresivnější	agresivní	k2eAgFnSc1d2	agresivnější
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
hrboly	hrbol	k1gInPc1	hrbol
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
poddruh	poddruh	k1gInSc4	poddruh
slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
nebo	nebo	k8xC	nebo
také	také	k9	také
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnou	úplný	k2eAgFnSc4d1	úplná
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
analýzu	analýza	k1gFnSc4	analýza
se	se	k3xPyFc4	se
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
uhynulého	uhynulý	k2eAgMnSc4d1	uhynulý
jedince	jedinec	k1gMnSc4	jedinec
či	či	k8xC	či
nalezení	nalezení	k1gNnSc4	nalezení
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
vyhynula	vyhynout	k5eAaPmAgFnS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc4	jejich
kompletní	kompletní	k2eAgInSc4d1	kompletní
taxonomický	taxonomický	k2eAgInSc4d1	taxonomický
systém	systém	k1gInSc4	systém
počínaje	počínaje	k7c7	počínaje
čeledí	čeleď	k1gFnSc7	čeleď
Slonovití	slonovitý	k2eAgMnPc1d1	slonovitý
<g/>
.	.	kIx.	.
</s>
<s>
Podčeleď	podčeleď	k1gFnSc1	podčeleď
Elephantinae	Elephantinae	k1gFnPc2	Elephantinae
Tribus	Tribus	k1gMnSc1	Tribus
Elephantini	Elephantin	k2eAgMnPc1d1	Elephantin
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
<g/>
)	)	kIx)	)
Subtribus	Subtribus	k1gMnSc1	Subtribus
Primelephantina	Primelephantin	k1gMnSc2	Primelephantin
†	†	k?	†
Rod	rod	k1gInSc1	rod
Primelephas	Primelephas	k1gMnSc1	Primelephas
†	†	k?	†
Subtribus	Subtribus	k1gMnSc1	Subtribus
Loxodontini	Loxodontin	k2eAgMnPc1d1	Loxodontin
Rod	rod	k1gInSc1	rod
Loxodon	Loxodon	k1gNnSc1	Loxodon
Podrod	podrod	k1gInSc1	podrod
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
Druh	druh	k1gMnSc1	druh
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
africana	africana	k1gFnSc1	africana
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	african	k1gMnSc2	african
adaurora	adauror	k1gMnSc2	adauror
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	african	k1gMnSc2	african
africana	african	k1gMnSc2	african
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	africana	k1gFnSc1	africana
oxyotis	oxyotis	k1gFnSc1	oxyotis
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Slon	slon	k1gMnSc1	slon
africký	africký	k2eAgMnSc1d1	africký
stepní	stepní	k2eAgMnSc1d1	stepní
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	africana	k1gFnSc1	africana
pharaonensis	pharaonensis	k1gFnSc1	pharaonensis
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
severoafrický	severoafrický	k2eAgMnSc1d1	severoafrický
<g/>
,	,	kIx,	,
Slon	slon	k1gMnSc1	slon
kartágský	kartágský	k1gMnSc1	kartágský
nebo	nebo	k8xC	nebo
Slon	slon	k1gMnSc1	slon
atlaský	atlaský	k1gMnSc1	atlaský
<g/>
)	)	kIx)	)
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Loxodonta	Loxodont	k1gMnSc2	Loxodont
africana	african	k1gMnSc2	african
pumilio	pumilio	k1gMnSc1	pumilio
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Loxodonta	Loxodonta	k1gMnSc1	Loxodonta
fransseni	franssen	k2eAgMnPc1d1	franssen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
<g/>
)	)	kIx)	)
Druh	druh	k1gInSc1	druh
Loxodonta	Loxodont	k1gInSc2	Loxodont
cyclotis	cyclotis	k1gFnSc2	cyclotis
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
pralesní	pralesní	k2eAgMnSc1d1	pralesní
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Subtribus	Subtribus	k1gMnSc1	Subtribus
Elephantini	Elephantin	k2eAgMnPc1d1	Elephantin
Rod	rod	k1gInSc1	rod
Mammuthus	Mammuthus	k1gInSc1	Mammuthus
†	†	k?	†
Rod	rod	k1gInSc1	rod
Elephas	Elephas	k1gMnSc1	Elephas
Druh	druh	k1gMnSc1	druh
Elephas	Elephas	k1gMnSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
indicus	indicus	k1gMnSc1	indicus
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
indický	indický	k2eAgMnSc1d1	indický
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
maximus	maximus	k1gMnSc1	maximus
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
cejlonský	cejlonský	k2eAgMnSc1d1	cejlonský
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gInSc1	Elephas
maximus	maximus	k1gInSc4	maximus
sumatrensis	sumatrensis	k1gFnSc2	sumatrensis
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gInSc1	Elephas
maximus	maximus	k1gInSc4	maximus
borneensis	borneensis	k1gFnSc2	borneensis
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
bornejský	bornejský	k2eAgMnSc1d1	bornejský
nebo	nebo	k8xC	nebo
Asijský	asijský	k2eAgMnSc1d1	asijský
slon	slon	k1gMnSc1	slon
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
<g/>
)	)	kIx)	)
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gInSc1	Elephas
maximus	maximus	k1gInSc1	maximus
rubridens	rubridens	k1gInSc1	rubridens
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
)	)	kIx)	)
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
?	?	kIx.	?
</s>
<s>
Slon	slon	k1gMnSc1	slon
vietnamský	vietnamský	k2eAgMnSc1d1	vietnamský
a	a	k8xC	a
Slon	slon	k1gMnSc1	slon
laoský	laoský	k2eAgInSc4d1	laoský
1	[number]	k4	1
Poddruh	poddruh	k1gInSc4	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
maximus	maximus	k1gMnSc1	maximus
asurus	asurus	k1gMnSc1	asurus
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
mezopotámský	mezopotámský	k2eAgMnSc1d1	mezopotámský
<g/>
)	)	kIx)	)
†	†	k?	†
Druh	druh	k1gInSc1	druh
Elephas	Elephasa	k1gFnPc2	Elephasa
beyeri	beyer	k1gFnSc2	beyer
†	†	k?	†
Druh	druh	k1gInSc1	druh
Elephas	Elephas	k1gMnSc1	Elephas
celebensis	celebensis	k1gInSc1	celebensis
†	†	k?	†
Druh	druh	k1gInSc1	druh
Elephas	Elephas	k1gMnSc1	Elephas
iolensis	iolensis	k1gInSc1	iolensis
†	†	k?	†
Druh	druh	k1gInSc1	druh
Elephas	Elephas	k1gMnSc1	Elephas
planifrons	planifronsa	k1gFnPc2	planifronsa
†	†	k?	†
Druh	druh	k1gMnSc1	druh
Elephas	Elephas	k1gMnSc1	Elephas
platycephalus	platycephalus	k1gMnSc1	platycephalus
†	†	k?	†
Druh	druh	k1gMnSc1	druh
Elephas	Elephas	k1gMnSc1	Elephas
recki	recki	k6eAd1	recki
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephasa	k1gFnPc2	Elephasa
recki	recki	k1gNnSc1	recki
atavus	atavus	k1gMnSc1	atavus
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephasa	k1gFnPc2	Elephasa
recki	reck	k1gFnSc2	reck
brumpti	brumpť	k1gFnSc2	brumpť
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephasa	k1gFnPc2	Elephasa
recki	reck	k1gFnSc2	reck
ileretensis	ileretensis	k1gInSc1	ileretensis
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephasa	k1gFnPc2	Elephasa
recki	reck	k1gFnSc2	reck
illertensis	illertensis	k1gInSc1	illertensis
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephasa	k1gFnPc2	Elephasa
recki	recki	k6eAd1	recki
recki	recki	k6eAd1	recki
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephasa	k1gFnPc2	Elephasa
recki	reck	k1gFnSc2	reck
shungurensis	shungurensis	k1gInSc1	shungurensis
†	†	k?	†
Druh	druh	k1gInSc1	druh
Palaeoloxodon	Palaeoloxodon	k1gInSc1	Palaeoloxodon
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
antiquus	antiquus	k1gMnSc1	antiquus
(	(	kIx(	(
<g/>
Slon	slon	k1gMnSc1	slon
s	s	k7c7	s
rovnými	rovný	k2eAgInPc7d1	rovný
kly	kel	k1gInPc7	kel
<g/>
)	)	kIx)	)
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
creticus	creticus	k1gMnSc1	creticus
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
creutzburgi	creutzburgi	k6eAd1	creutzburgi
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodona	k1gFnPc2	Palaeoloxodona
chaniensis	chaniensis	k1gInSc4	chaniensis
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
cypriotes	cypriotes	k1gMnSc1	cypriotes
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodona	k1gFnPc2	Palaeoloxodona
ekorensis	ekorensis	k1gInSc4	ekorensis
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
falconeri	falconeri	k6eAd1	falconeri
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodona	k1gFnPc2	Palaeoloxodona
mnaidriensis	mnaidriensis	k1gInSc4	mnaidriensis
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodona	k1gFnPc2	Palaeoloxodona
melitensis	melitensis	k1gInSc4	melitensis
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
namadicus	namadicus	k1gMnSc1	namadicus
†	†	k?	†
Poddruh	poddruh	k1gInSc1	poddruh
Elephas	Elephas	k1gMnSc1	Elephas
Palaeoloxodon	Palaeoloxodon	k1gMnSc1	Palaeoloxodon
naumanni	naumanň	k1gMnSc3	naumanň
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sloní	sloní	k2eAgFnPc1d1	sloní
populace	populace	k1gFnPc1	populace
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
Laosu	Laos	k1gInSc2	Laos
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
podrobována	podrobován	k2eAgFnSc1d1	podrobována
testům	test	k1gMnPc3	test
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pátý	pátý	k4xOgInSc4	pátý
poddruh	poddruh	k1gInSc4	poddruh
slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
a	a	k8xC	a
značným	značný	k2eAgInPc3d1	značný
tělesným	tělesný	k2eAgInPc3d1	tělesný
rozměrům	rozměr	k1gInPc3	rozměr
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
jako	jako	k8xC	jako
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
společensky	společensky	k6eAd1	společensky
neobratného	obratný	k2eNgNnSc2d1	neobratné
či	či	k8xC	či
fyzicky	fyzicky	k6eAd1	fyzicky
hřmotného	hřmotný	k2eAgMnSc2d1	hřmotný
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
český	český	k2eAgInSc1d1	český
frazeologizmus	frazeologizmus	k1gInSc1	frazeologizmus
Choval	chovat	k5eAaImAgInS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
slon	slon	k1gMnSc1	slon
v	v	k7c6	v
porcelánu	porcelán	k1gInSc6	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
štěstí	štěstí	k1gNnSc2	štěstí
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podobizny	podobizna	k1gFnPc1	podobizna
slona	slon	k1gMnSc2	slon
se	se	k3xPyFc4	se
ztvárňují	ztvárňovat	k5eAaImIp3nP	ztvárňovat
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
<g/>
,	,	kIx,	,
soškách	soška	k1gFnPc6	soška
<g/>
,	,	kIx,	,
amuletech	amulet	k1gInPc6	amulet
<g/>
,	,	kIx,	,
špercích	šperk	k1gInPc6	šperk
<g/>
,	,	kIx,	,
obrazech	obraz	k1gInPc6	obraz
i	i	k8xC	i
dekoračních	dekorační	k2eAgInPc6d1	dekorační
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
dětských	dětský	k2eAgFnPc6d1	dětská
postýlkách	postýlka	k1gFnPc6	postýlka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plyšáků	plyšáků	k?	plyšáků
<g/>
,	,	kIx,	,
na	na	k7c6	na
lednicích	lednice	k1gFnPc6	lednice
jako	jako	k8xS	jako
magnety	magnet	k1gInPc7	magnet
i	i	k8xC	i
v	v	k7c6	v
ložnici	ložnice	k1gFnSc6	ložnice
jako	jako	k8xC	jako
sošky	soška	k1gFnSc2	soška
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
Růžový	růžový	k2eAgMnSc1d1	růžový
slon	slon	k1gMnSc1	slon
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nedávají	dávat	k5eNaImIp3nP	dávat
logiku	logika	k1gFnSc4	logika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
absurdní	absurdní	k2eAgFnSc1d1	absurdní
a	a	k8xC	a
beze	beze	k7c2	beze
smyslu	smysl	k1gInSc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
růžoví	růžový	k2eAgMnPc1d1	růžový
sloni	slon	k1gMnPc1	slon
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
dětských	dětský	k2eAgFnPc6d1	dětská
pohádkách	pohádka	k1gFnPc6	pohádka
a	a	k8xC	a
příbězích	příběh	k1gInPc6	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
také	také	k9	také
rčení	rčení	k1gNnSc1	rčení
"	"	kIx"	"
<g/>
hlavně	hlavně	k9	hlavně
nemyslete	myslet	k5eNaImRp2nP	myslet
na	na	k7c4	na
růžového	růžový	k2eAgMnSc4d1	růžový
slona	slon	k1gMnSc4	slon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nutí	nutit	k5eAaImIp3nP	nutit
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
právě	právě	k9	právě
o	o	k7c4	o
něm.	něm.	k?	něm.
Další	další	k2eAgInPc4d1	další
významy	význam	k1gInPc4	význam
<g/>
:	:	kIx,	:
Bílý	bílý	k1gMnSc1	bílý
slon	slon	k1gMnSc1	slon
-	-	kIx~	-
neochota	neochota	k1gFnSc1	neochota
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
něčeho	něco	k3yInSc2	něco
Válečný	válečný	k2eAgMnSc1d1	válečný
slon	slon	k1gMnSc1	slon
-	-	kIx~	-
slon	slon	k1gMnSc1	slon
použitý	použitý	k2eAgMnSc1d1	použitý
v	v	k7c6	v
boji	boj	k1gInSc6	boj
Řád	řád	k1gInSc4	řád
slona	slon	k1gMnSc4	slon
-	-	kIx~	-
dánský	dánský	k2eAgInSc4d1	dánský
řád	řád	k1gInSc4	řád
Sloní	slonit	k5eAaImIp3nS	slonit
muž	muž	k1gMnSc1	muž
-	-	kIx~	-
tělesné	tělesný	k2eAgNnSc1d1	tělesné
postižení	postižení	k1gNnSc1	postižení
obličeje	obličej	k1gInSc2	obličej
Sloní	sloní	k2eAgInPc1d1	sloní
kameny	kámen	k1gInPc1	kámen
-	-	kIx~	-
skály	skála	k1gFnPc1	skála
u	u	k7c2	u
Liberce	Liberec	k1gInSc2	Liberec
Sloní	sloní	k2eAgFnSc1d1	sloní
nemoc	nemoc	k1gFnSc1	nemoc
-	-	kIx~	-
nemoc	nemoc	k1gFnSc1	nemoc
Sloní	sloní	k2eAgFnSc1d1	sloní
noha	noha	k1gFnSc1	noha
-	-	kIx~	-
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
Bastilský	Bastilský	k2eAgMnSc1d1	Bastilský
slon	slon	k1gMnSc1	slon
-	-	kIx~	-
fontána	fontána	k1gFnSc1	fontána
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Sloní	sloní	k2eAgInSc4d1	sloní
ostrov	ostrov	k1gInSc4	ostrov
-	-	kIx~	-
ostrov	ostrov	k1gInSc4	ostrov
u	u	k7c2	u
Antarktidy	Antarktida	k1gFnSc2	Antarktida
Sloní	sloní	k2eAgNnSc1d1	sloní
pólo	pólo	k1gNnSc1	pólo
-	-	kIx~	-
sport	sport	k1gInSc1	sport
</s>
