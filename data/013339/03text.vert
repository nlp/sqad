<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1456	[number]	k4	1456
Krakov	Krakov	k1gInSc1	Krakov
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1516	[number]	k4	1516
Budín	Budín	k1gInSc1	Budín
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
litevského	litevský	k2eAgInSc2d1	litevský
velkoknížecího	velkoknížecí	k2eAgInSc2d1	velkoknížecí
a	a	k8xC	a
polského	polský	k2eAgInSc2d1	polský
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
se	se	k3xPyFc4	se
jménem	jméno	k1gNnSc7	jméno
Władysław	Władysław	k1gFnPc2	Władysław
Jagiełło	Jagiełło	k6eAd1	Jagiełło
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
králem	král	k1gMnSc7	král
českým	český	k2eAgFnPc3d1	Česká
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
jako	jako	k8xC	jako
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
markrabětem	markrabě	k1gMnSc7	markrabě
moravským	moravský	k2eAgMnSc7d1	moravský
a	a	k8xC	a
králem	král	k1gMnSc7	král
uherským	uherský	k2eAgInPc3d1	uherský
také	také	k9	také
jako	jako	k9	jako
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Ulászló	Ulászló	k1gMnSc1	Ulászló
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
bývá	bývat	k5eAaImIp3nS	bývat
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslabších	slabý	k2eAgMnPc2d3	nejslabší
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
úpadku	úpadek	k1gInSc3	úpadek
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
15	[number]	k4	15
letech	let	k1gInPc6	let
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
předchůdci	předchůdce	k1gMnSc6	předchůdce
Jiřím	Jiří	k1gMnSc6	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
zdědil	zdědit	k5eAaPmAgInS	zdědit
složitou	složitý	k2eAgFnSc4d1	složitá
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
provázely	provázet	k5eAaImAgInP	provázet
střety	střet	k1gInPc1	střet
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
podporovaným	podporovaný	k2eAgMnSc7d1	podporovaný
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
zisku	zisk	k1gInSc6	zisk
uherské	uherský	k2eAgFnSc2d1	uherská
koruny	koruna	k1gFnSc2	koruna
pak	pak	k6eAd1	pak
především	především	k6eAd1	především
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
turecká	turecký	k2eAgFnSc1d1	turecká
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Povahou	povaha	k1gFnSc7	povaha
flegmatický	flegmatický	k2eAgMnSc1d1	flegmatický
Vladislav	Vladislav	k1gMnSc1	Vladislav
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
domácím	domácí	k2eAgFnPc3d1	domácí
záležitostem	záležitost	k1gFnPc3	záležitost
mnoho	mnoho	k6eAd1	mnoho
pozornosti	pozornost	k1gFnSc2	pozornost
a	a	k8xC	a
mezi	mezi	k7c7	mezi
panstvem	panstvo	k1gNnSc7	panstvo
se	se	k3xPyFc4	se
vžila	vžít	k5eAaPmAgFnS	vžít
přezdívka	přezdívka	k1gFnSc1	přezdívka
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
bene	bene	k6eAd1	bene
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dynastické	dynastický	k2eAgFnSc6d1	dynastická
politice	politika	k1gFnSc6	politika
však	však	k9	však
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
významného	významný	k2eAgInSc2d1	významný
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
Matyášově	Matyášův	k2eAgFnSc6d1	Matyášova
smrti	smrt	k1gFnSc6	smrt
zvolen	zvolit	k5eAaPmNgMnS	zvolit
a	a	k8xC	a
korunován	korunovat	k5eAaBmNgMnS	korunovat
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navzdory	navzdory	k7c3	navzdory
záměrům	záměr	k1gInPc3	záměr
krakovského	krakovský	k2eAgInSc2d1	krakovský
dvora	dvůr	k1gInSc2	dvůr
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
Kazimíra	Kazimír	k1gMnSc2	Kazimír
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
prosazujícího	prosazující	k2eAgMnSc2d1	prosazující
Vladislavova	Vladislavův	k2eAgMnSc2d1	Vladislavův
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
(	(	kIx(	(
<g/>
zvanou	zvaný	k2eAgFnSc7d1	zvaná
matkou	matka	k1gFnSc7	matka
králů	král	k1gMnPc2	král
–	–	k?	–
5	[number]	k4	5
synů	syn	k1gMnPc2	syn
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
králi	král	k1gMnSc3	král
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Ladislava	Ladislav	k1gMnSc2	Ladislav
Pohrobka	pohrobek	k1gMnSc2	pohrobek
s	s	k7c7	s
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
po	po	k7c6	po
něm.	něm.	k?	něm.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
Jagellonci	Jagellonek	k1gMnPc1	Jagellonek
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1465	[number]	k4	1465
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
reprezentovaná	reprezentovaný	k2eAgFnSc1d1	reprezentovaná
Jednotou	jednota	k1gFnSc7	jednota
zelenohorskou	zelenohorský	k2eAgFnSc7d1	Zelenohorská
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
odbojné	odbojný	k2eAgNnSc1d1	odbojné
katolické	katolický	k2eAgNnSc1d1	katolické
panstvo	panstvo	k1gNnSc1	panstvo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
odporu	odpor	k1gInSc3	odpor
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgNnP	přidat
i	i	k9	i
katolická	katolický	k2eAgNnPc1d1	katolické
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
jednoty	jednota	k1gFnSc2	jednota
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
přijmout	přijmout	k5eAaPmF	přijmout
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
panovníka	panovník	k1gMnSc4	panovník
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Kazimíra	Kazimír	k1gMnSc2	Kazimír
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1466	[number]	k4	1466
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
proti	proti	k7c3	proti
kacířským	kacířský	k2eAgFnPc3d1	kacířská
Čechám	Čechy	k1gFnPc3	Čechy
křížovou	křížový	k2eAgFnSc7d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
si	se	k3xPyFc3	se
katoličtí	katolický	k2eAgMnPc1d1	katolický
páni	pan	k1gMnPc1	pan
zvolili	zvolit	k5eAaPmAgMnP	zvolit
roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
ohrožení	ohrožení	k1gNnSc2	ohrožení
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnPc1	Jiří
prozíravě	prozíravě	k6eAd1	prozíravě
vzdal	vzdát	k5eAaPmAgInS	vzdát
dynastických	dynastický	k2eAgInPc2d1	dynastický
nároků	nárok	k1gInPc2	nárok
svých	svůj	k3xOyFgMnPc2	svůj
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
Jagelloncům	Jagellonec	k1gMnPc3	Jagellonec
<g/>
.	.	kIx.	.
</s>
<s>
Necelých	celý	k2eNgInPc2d1	necelý
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
volbě	volba	k1gFnSc6	volba
český	český	k2eAgInSc1d1	český
sněm	sněm	k1gInSc1	sněm
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Matyáše	Matyáš	k1gMnSc2	Matyáš
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
uznal	uznat	k5eAaPmAgMnS	uznat
nástupcem	nástupce	k1gMnSc7	nástupce
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
Kazimírova	Kazimírův	k2eAgMnSc2d1	Kazimírův
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Vladislava	Vladislav	k1gMnSc2	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
smrti	smrt	k1gFnSc6	smrt
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1471	[number]	k4	1471
Vladislava	Vladislav	k1gMnSc2	Vladislav
jako	jako	k8xC	jako
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
novou	nový	k2eAgFnSc7d1	nová
volbou	volba	k1gFnSc7	volba
s	s	k7c7	s
příslibem	příslib	k1gInSc7	příslib
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
urovnání	urovnání	k1gNnSc2	urovnání
vztahů	vztah	k1gInPc2	vztah
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Sixtem	Sixtus	k1gMnSc7	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
splacení	splacení	k1gNnSc2	splacení
značných	značný	k2eAgInPc2d1	značný
dluhů	dluh	k1gInPc2	dluh
české	český	k2eAgFnSc2d1	Česká
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc4	král
Jiřího	Jiří	k1gMnSc4	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Příchodem	příchod	k1gInSc7	příchod
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
slavila	slavit	k5eAaImAgFnS	slavit
dynastická	dynastický	k2eAgFnSc1d1	dynastická
politika	politika	k1gFnSc1	politika
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
významný	významný	k2eAgInSc4d1	významný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
dědictví	dědictví	k1gNnSc4	dědictví
po	po	k7c4	po
Ladislavu	Ladislava	k1gFnSc4	Ladislava
Pohrobkovi	pohrobek	k1gMnSc3	pohrobek
–	–	k?	–
vedle	vedle	k7c2	vedle
Království	království	k1gNnSc2	království
uherského	uherský	k2eAgNnSc2d1	Uherské
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
však	však	k9	však
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinášel	přinášet	k5eAaImAgMnS	přinášet
řadu	řada	k1gFnSc4	řada
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástup	nástup	k1gInSc1	nástup
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
nazítří	nazítří	k6eAd1	nazítří
po	po	k7c6	po
Vladislavově	Vladislavův	k2eAgNnSc6d1	Vladislavovo
zvolení	zvolení	k1gNnSc6	zvolení
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
korunovat	korunovat	k5eAaBmF	korunovat
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
i	i	k8xC	i
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navzdory	navzdory	k7c3	navzdory
jagellonskému	jagellonský	k2eAgInSc3d1	jagellonský
úspěchu	úspěch	k1gInSc3	úspěch
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
stále	stále	k6eAd1	stále
razantněji	razantně	k6eAd2	razantně
svoji	svůj	k3xOyFgFnSc4	svůj
hegemonii	hegemonie	k1gFnSc4	hegemonie
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podržel	podržet	k5eAaPmAgMnS	podržet
si	se	k3xPyFc3	se
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
země	země	k1gFnSc1	země
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgMnPc4d1	český
–	–	k?	–
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc4	Slezsko
a	a	k8xC	a
Lužici	Lužice	k1gFnSc4	Lužice
–	–	k?	–
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
ho	on	k3xPp3gInSc4	on
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
podporovala	podporovat	k5eAaImAgFnS	podporovat
katolická	katolický	k2eAgFnSc1d1	katolická
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
ovládající	ovládající	k2eAgInSc1d1	ovládající
jih	jih	k1gInSc1	jih
a	a	k8xC	a
severozápad	severozápad	k1gInSc1	severozápad
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
panovníka	panovník	k1gMnSc4	panovník
ho	on	k3xPp3gMnSc2	on
považovala	považovat	k5eAaImAgFnS	považovat
také	také	k9	také
některá	některý	k3yIgFnSc1	některý
česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
)	)	kIx)	)
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neuznala	uznat	k5eNaPmAgFnS	uznat
Vladislavovu	Vladislavův	k2eAgFnSc4d1	Vladislavova
volbu	volba	k1gFnSc4	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
Vladislavovou	Vladislavův	k2eAgFnSc7d1	Vladislavova
volbou	volba	k1gFnSc7	volba
zpočátku	zpočátku	k6eAd1	zpočátku
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
ani	ani	k8xC	ani
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1474	[number]	k4	1474
se	se	k3xPyFc4	se
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
Vladislavovi	Vladislavův	k2eAgMnPc1d1	Vladislavův
diplomaté	diplomat	k1gMnPc1	diplomat
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
budou	být	k5eAaImBp3nP	být
navzájem	navzájem	k6eAd1	navzájem
podporovat	podporovat	k5eAaImF	podporovat
proti	proti	k7c3	proti
uherskému	uherský	k2eAgMnSc3d1	uherský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Fridrich	Fridrich	k1gMnSc1	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
oficiálně	oficiálně	k6eAd1	oficiálně
uznal	uznat	k5eAaPmAgMnS	uznat
Vladislava	Vladislav	k1gMnSc4	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
a	a	k8xC	a
říšským	říšský	k2eAgMnSc7d1	říšský
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
mu	on	k3xPp3gMnSc3	on
udělit	udělit	k5eAaPmF	udělit
země	zem	k1gFnSc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaPmAgInS	učinit
tak	tak	k9	tak
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
papež	papež	k1gMnSc1	papež
Sixtus	Sixtus	k1gMnSc1	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
usedl	usednout	k5eAaPmAgMnS	usednout
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
protikandidát	protikandidát	k1gMnSc1	protikandidát
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
rovněž	rovněž	k9	rovněž
ucházel	ucházet	k5eAaImAgInS	ucházet
na	na	k7c6	na
kutnohorském	kutnohorský	k2eAgInSc6d1	kutnohorský
sněmu	sněm	k1gInSc6	sněm
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
znovuzvolení	znovuzvolení	k1gNnSc4	znovuzvolení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
výhodou	výhoda	k1gFnSc7	výhoda
byla	být	k5eAaImAgFnS	být
nejen	nejen	k6eAd1	nejen
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
podpora	podpora	k1gFnSc1	podpora
papežské	papežský	k2eAgFnSc2d1	Papežská
kurie	kurie	k1gFnSc2	kurie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vkládala	vkládat	k5eAaImAgFnS	vkládat
své	svůj	k3xOyFgFnPc4	svůj
naděje	naděje	k1gFnPc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
skoncuje	skoncovat	k5eAaPmIp3nS	skoncovat
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
utrakvismem	utrakvismus	k1gInSc7	utrakvismus
a	a	k8xC	a
také	také	k9	také
zastaví	zastavit	k5eAaPmIp3nS	zastavit
turecký	turecký	k2eAgInSc4d1	turecký
nápor	nápor	k1gInSc4	nápor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
faktická	faktický	k2eAgFnSc1d1	faktická
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
zemí	zem	k1gFnPc2	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
smlouva	smlouva	k1gFnSc1	smlouva
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
Vladislavem	Vladislav	k1gMnSc7	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1471	[number]	k4	1471
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vedla	vést	k5eAaImAgFnS	vést
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
panoval	panovat	k5eAaImAgInS	panovat
neklid	neklid	k1gInSc1	neklid
způsobený	způsobený	k2eAgInSc1d1	způsobený
neustálými	neustálý	k2eAgInPc7d1	neustálý
konflikty	konflikt	k1gInPc7	konflikt
kališníků	kališník	k1gMnPc2	kališník
a	a	k8xC	a
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1472	[number]	k4	1472
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
česko-uhersko-polský	českoherskoolský	k2eAgInSc4d1	česko-uhersko-polský
konflikt	konflikt	k1gInSc4	konflikt
uklidnit	uklidnit	k5eAaPmF	uklidnit
a	a	k8xC	a
obrátit	obrátit	k5eAaPmF	obrátit
zájem	zájem	k1gInSc4	zájem
znepřátelených	znepřátelený	k2eAgFnPc2d1	znepřátelená
stran	strana	k1gFnPc2	strana
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
o	o	k7c6	o
smíření	smíření	k1gNnSc6	smíření
nestála	stát	k5eNaImAgFnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1474	[number]	k4	1474
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
příměří	příměří	k1gNnSc1	příměří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
vzápětí	vzápětí	k6eAd1	vzápětí
porušeno	porušit	k5eAaPmNgNnS	porušit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
Matyášově	Matyášův	k2eAgFnSc3d1	Matyášova
převaze	převaha	k1gFnSc3	převaha
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
mladého	mladý	k2eAgMnSc4d1	mladý
Jagellonce	Jagellonec	k1gMnSc4	Jagellonec
vytlačit	vytlačit	k5eAaPmF	vytlačit
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
zůstával	zůstávat	k5eAaImAgInS	zůstávat
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Korvína	Korvín	k1gInSc2	Korvín
panovníkem	panovník	k1gMnSc7	panovník
mírné	mírný	k2eAgFnSc2d1	mírná
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
letech	let	k1gInPc6	let
válek	válek	k1gInSc4	válek
již	již	k6eAd1	již
nesmírně	smírně	k6eNd1	smírně
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
názoru	názor	k1gInSc3	názor
postupně	postupně	k6eAd1	postupně
přiklonily	přiklonit	k5eAaPmAgFnP	přiklonit
všechny	všechen	k3xTgFnPc1	všechen
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
zúčastněné	zúčastněný	k2eAgFnPc1d1	zúčastněná
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
utrakvisté	utrakvista	k1gMnPc1	utrakvista
<g/>
,	,	kIx,	,
Vladislavovi	Vladislavův	k2eAgMnPc1d1	Vladislavův
přívrženci	přívrženec	k1gMnPc1	přívrženec
<g/>
,	,	kIx,	,
katolíci	katolík	k1gMnPc1	katolík
stranící	stranící	k2eAgMnPc1d1	stranící
Matyášovi	Matyášův	k2eAgMnPc1d1	Matyášův
i	i	k8xC	i
uherští	uherský	k2eAgMnPc1d1	uherský
útočníci	útočník	k1gMnPc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
dědičných	dědičný	k2eAgFnPc2d1	dědičná
habsburských	habsburský	k2eAgFnPc2d1	habsburská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
tak	tak	k6eAd1	tak
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
boje	boj	k1gInPc1	boj
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
frontách	fronta	k1gFnPc6	fronta
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
finanční	finanční	k2eAgFnPc4d1	finanční
možnosti	možnost	k1gFnPc4	možnost
příliš	příliš	k6eAd1	příliš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1478	[number]	k4	1478
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zástupci	zástupce	k1gMnPc1	zástupce
obou	dva	k4xCgMnPc2	dva
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
připravili	připravit	k5eAaPmAgMnP	připravit
návrh	návrh	k1gInSc4	návrh
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnPc4d1	Česká
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
práva	právo	k1gNnSc2	právo
titulovat	titulovat	k5eAaImF	titulovat
se	se	k3xPyFc4	se
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
rozhodnější	rozhodný	k2eAgNnSc4d2	rozhodnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zvítězila	zvítězit	k5eAaPmAgNnP	zvítězit
uherská	uherský	k2eAgNnPc1d1	Uherské
vojska	vojsko	k1gNnPc1	vojsko
nad	nad	k7c7	nad
Vladislavovými	Vladislavův	k2eAgMnPc7d1	Vladislavův
bojovníky	bojovník	k1gMnPc7	bojovník
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
přepracována	přepracován	k2eAgFnSc1d1	přepracována
–	–	k?	–
a	a	k8xC	a
nad	nad	k7c7	nad
novými	nový	k2eAgMnPc7d1	nový
návrhem	návrh	k1gInSc7	návrh
se	se	k3xPyFc4	se
Vladislav	Vladislav	k1gMnSc1	Vladislav
s	s	k7c7	s
Matyášem	Matyáš	k1gMnSc7	Matyáš
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1479	[number]	k4	1479
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Olomouckými	olomoucký	k2eAgFnPc7d1	olomoucká
úmluvami	úmluva	k1gFnPc7	úmluva
uznal	uznat	k5eAaPmAgMnS	uznat
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
status	status	k1gInSc1	status
quo	quo	k?	quo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
navzdory	navzdory	k6eAd1	navzdory
ustanovením	ustanovení	k1gNnSc7	ustanovení
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
nedělitelnosti	nedělitelnost	k1gFnSc6	nedělitelnost
rozpolceno	rozpolcen	k2eAgNnSc1d1	rozpolceno
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
měli	mít	k5eAaImAgMnP	mít
užívat	užívat	k5eAaImF	užívat
titulu	titul	k1gInSc3	titul
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
dva	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
vládnout	vládnout	k5eAaImF	vládnout
dědičně	dědičně	k6eAd1	dědičně
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgInS	získat
–	–	k?	–
rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
–	–	k?	–
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
Lužice	Lužice	k1gFnPc1	Lužice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Korvínově	Korvínův	k2eAgFnSc6d1	Korvínova
smrti	smrt	k1gFnSc6	smrt
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
sice	sice	k8xC	sice
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
země	zem	k1gFnPc4	zem
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
přičleněny	přičlenit	k5eAaPmNgInP	přičlenit
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
400	[number]	k4	400
tisíc	tisíc	k4xCgInSc4	tisíc
uherských	uherský	k2eAgInPc2d1	uherský
zlatých	zlatý	k1gInPc2	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nP	by
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
uherského	uherský	k2eAgMnSc2d1	uherský
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
nemělo	mít	k5eNaImAgNnS	mít
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc4	ten
stát	stát	k1gInSc1	stát
zhola	zhola	k6eAd1	zhola
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
však	však	k9	však
Vladislav	Vladislav	k1gMnSc1	Vladislav
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
dědiců	dědic	k1gMnPc2	dědic
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
by	by	k9	by
Matyáše	Matyáš	k1gMnSc4	Matyáš
přijali	přijmout	k5eAaPmAgMnP	přijmout
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
panovníka	panovník	k1gMnSc4	panovník
<g/>
,	,	kIx,	,
vrátily	vrátit	k5eAaPmAgFnP	vrátit
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
země	zem	k1gFnPc1	zem
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
mělo	mít	k5eAaImAgNnS	mít
inspirovat	inspirovat	k5eAaBmF	inspirovat
český	český	k2eAgInSc4d1	český
volební	volební	k2eAgInSc4d1	volební
sněm	sněm	k1gInSc4	sněm
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
zvolil	zvolit	k5eAaPmAgMnS	zvolit
králem	král	k1gMnSc7	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
králové	král	k1gMnPc1	král
budou	být	k5eAaImBp3nP	být
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
urovnání	urovnání	k1gNnSc3	urovnání
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c4	mezi
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
papežskou	papežský	k2eAgFnSc7d1	Papežská
kurií	kurie	k1gFnSc7	kurie
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
instalován	instalován	k2eAgMnSc1d1	instalován
katolický	katolický	k2eAgMnSc1d1	katolický
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
smíru	smír	k1gInSc2	smír
také	také	k9	také
mezi	mezi	k7c7	mezi
Matyášem	Matyáš	k1gMnSc7	Matyáš
a	a	k8xC	a
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
Kazimírem	Kazimír	k1gMnSc7	Kazimír
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
byl	být	k5eAaImAgMnS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
zvolen	zvolit	k5eAaPmNgInS	zvolit
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1490	[number]	k4	1490
také	také	k9	také
králem	král	k1gMnSc7	král
uherským	uherský	k2eAgMnSc7d1	uherský
a	a	k8xC	a
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Budína	Budín	k1gInSc2	Budín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náboženské	náboženský	k2eAgInPc4d1	náboženský
poměry	poměr	k1gInPc4	poměr
a	a	k8xC	a
kutnohorský	kutnohorský	k2eAgInSc4d1	kutnohorský
smír	smír	k1gInSc4	smír
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Vladislav	Vladislav	k1gMnSc1	Vladislav
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
zásluhou	zásluhou	k7c2	zásluhou
kališníků	kališník	k1gMnPc2	kališník
(	(	kIx(	(
<g/>
utrakvistů	utrakvista	k1gMnPc2	utrakvista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
sbližovat	sbližovat	k5eAaImF	sbližovat
s	s	k7c7	s
katolickým	katolický	k2eAgNnSc7d1	katolické
panstvem	panstvo	k1gNnSc7	panstvo
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
kališnickou	kališnický	k2eAgFnSc7d1	kališnická
většinou	většina	k1gFnSc7	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Krvavé	krvavý	k2eAgNnSc1d1	krvavé
povstání	povstání	k1gNnSc1	povstání
pražských	pražský	k2eAgMnPc2d1	pražský
kališníků	kališník	k1gMnPc2	kališník
zmařilo	zmařit	k5eAaPmAgNnS	zmařit
naděje	naděje	k1gFnSc2	naděje
katolických	katolický	k2eAgMnPc2d1	katolický
předáků	předák	k1gMnPc2	předák
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
předhusitským	předhusitský	k2eAgInPc3d1	předhusitský
církevním	církevní	k2eAgInPc3d1	církevní
poměrům	poměr	k1gInPc3	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
obnoven	obnovit	k5eAaPmNgMnS	obnovit
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
smír	smír	k1gInSc1	smír
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
království	království	k1gNnPc2	království
včetně	včetně	k7c2	včetně
poddaných	poddaná	k1gFnPc2	poddaná
právo	právo	k1gNnSc4	právo
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nevztahovala	vztahovat	k5eNaImAgFnS	vztahovat
na	na	k7c4	na
jednotu	jednota	k1gFnSc4	jednota
bratrskou	bratrský	k2eAgFnSc4d1	bratrská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vladislavské	vladislavský	k2eAgNnSc4d1	Vladislavské
zřízení	zřízení	k1gNnSc4	zřízení
zemské	zemský	k2eAgFnSc2d1	zemská
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc1d1	vydané
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc1	důkaz
slabosti	slabost	k1gFnSc2	slabost
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgNnSc2	tento
zřízení	zřízení	k1gNnSc2	zřízení
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
omezení	omezení	k1gNnSc1	omezení
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtě	šlechta	k1gFnSc3	šlechta
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zformulovat	zformulovat	k5eAaPmF	zformulovat
své	svůj	k3xOyFgNnSc4	svůj
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
a	a	k8xC	a
tvůrcem	tvůrce	k1gMnSc7	tvůrce
ducha	duch	k1gMnSc2	duch
zákona	zákon	k1gInSc2	zákon
byl	být	k5eAaImAgMnS	být
odpůrce	odpůrce	k1gMnSc1	odpůrce
politického	politický	k2eAgInSc2d1	politický
vzestupu	vzestup	k1gInSc2	vzestup
městského	městský	k2eAgInSc2d1	městský
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
královský	královský	k2eAgMnSc1d1	královský
prokurátor	prokurátor	k1gMnSc1	prokurátor
Albrecht	Albrecht	k1gMnSc1	Albrecht
Rendl	Rendl	k1gMnSc1	Rendl
z	z	k7c2	z
Oušavy	Oušava	k1gFnSc2	Oušava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zákoníku	zákoník	k1gInSc6	zákoník
byla	být	k5eAaImAgFnS	být
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
koncepce	koncepce	k1gFnSc1	koncepce
<g/>
,	,	kIx,	,
že	že	k8xS	že
královská	královský	k2eAgNnPc1d1	královské
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
poddanými	poddaná	k1gFnPc7	poddaná
krále	král	k1gMnSc2	král
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
rovnocenným	rovnocenný	k2eAgMnSc7d1	rovnocenný
partnerem	partner	k1gMnSc7	partner
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákoník	zákoník	k1gMnSc1	zákoník
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
přijetí	přijetí	k1gNnSc2	přijetí
lišil	lišit	k5eAaImAgInS	lišit
od	od	k7c2	od
obdobných	obdobný	k2eAgNnPc2d1	obdobné
zemských	zemský	k2eAgNnPc2d1	zemské
zřízení	zřízení	k1gNnPc2	zřízení
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
vydával	vydávat	k5eAaImAgInS	vydávat
zemské	zemský	k2eAgNnSc4d1	zemské
zřízení	zřízení	k1gNnSc4	zřízení
zeměpán	zeměpán	k1gMnSc1	zeměpán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
vzal	vzít	k5eAaPmAgMnS	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
zájmy	zájem	k1gInPc7	zájem
stavů	stav	k1gInPc2	stav
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
rozhodovaly	rozhodovat	k5eAaImAgFnP	rozhodovat
o	o	k7c6	o
zemském	zemský	k2eAgNnSc6d1	zemské
zřízení	zřízení	k1gNnSc6	zřízení
privilegované	privilegovaný	k2eAgInPc4d1	privilegovaný
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
uvážení	uvážení	k1gNnSc2	uvážení
zohlednily	zohlednit	k5eAaPmAgFnP	zohlednit
zájmy	zájem	k1gInPc4	zájem
svého	svůj	k1gMnSc2	svůj
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Stavovská	stavovský	k2eAgFnSc1d1	stavovská
formulace	formulace	k1gFnSc1	formulace
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
byla	být	k5eAaImAgFnS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
převážně	převážně	k6eAd1	převážně
limitující	limitující	k2eAgFnSc1d1	limitující
a	a	k8xC	a
přinášela	přinášet	k5eAaImAgFnS	přinášet
zásadní	zásadní	k2eAgNnSc4d1	zásadní
omezení	omezení	k1gNnSc4	omezení
moci	moc	k1gFnSc2	moc
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydání	vydání	k1gNnSc1	vydání
zákoníku	zákoník	k1gInSc2	zákoník
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
politickém	politický	k2eAgInSc6d1	politický
boji	boj	k1gInSc6	boj
s	s	k7c7	s
městským	městský	k2eAgInSc7d1	městský
stavem	stav	k1gInSc7	stav
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
jako	jako	k9	jako
obhájce	obhájce	k1gMnSc4	obhájce
zákonnosti	zákonnost	k1gFnSc2	zákonnost
a	a	k8xC	a
starých	starý	k2eAgNnPc2d1	staré
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
za	za	k7c2	za
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
shrnutí	shrnutí	k1gNnSc2	shrnutí
nový	nový	k2eAgInSc4d1	nový
zákoník	zákoník	k1gInSc4	zákoník
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
situace	situace	k1gFnSc1	situace
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
dává	dávat	k5eAaImIp3nS	dávat
převahu	převah	k1gInSc2	převah
šlechtě	šlechta	k1gFnSc3	šlechta
a	a	k8xC	a
města	město	k1gNnPc1	město
musí	muset	k5eAaImIp3nP	muset
těžce	těžce	k6eAd1	těžce
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
ztracených	ztracený	k2eAgFnPc2d1	ztracená
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Král	Král	k1gMnSc1	Král
Bene	bene	k6eAd1	bene
==	==	k?	==
</s>
</p>
<p>
<s>
Přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
Bene	bene	k6eAd1	bene
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
Vladislav	Vladislav	k1gMnSc1	Vladislav
údajně	údajně	k6eAd1	údajně
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlechtě	šlechta	k1gFnSc3	šlechta
na	na	k7c6	na
žádosti	žádost	k1gFnSc6	žádost
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
bene	bene	k6eAd1	bene
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
dobře	dobře	k6eAd1	dobře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc4	spor
obvykle	obvykle	k6eAd1	obvykle
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yInSc4	kdo
slyšel	slyšet	k5eAaImAgMnS	slyšet
mluvit	mluvit	k5eAaImF	mluvit
posledního	poslední	k2eAgMnSc4d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nacházely	nacházet	k5eAaImAgFnP	nacházet
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
situaci	situace	k1gFnSc6	situace
–	–	k?	–
země	zem	k1gFnPc1	zem
byly	být	k5eAaImAgFnP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
mezi	mezi	k7c4	mezi
dva	dva	k4xCgMnPc4	dva
panovníky	panovník	k1gMnPc4	panovník
(	(	kIx(	(
<g/>
Vladislav	Vladislav	k1gMnSc1	Vladislav
a	a	k8xC	a
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
)	)	kIx)	)
–	–	k?	–
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
panovník	panovník	k1gMnSc1	panovník
dávat	dávat	k5eAaImF	dávat
přednost	přednost	k1gFnSc4	přednost
kompromisům	kompromis	k1gInPc3	kompromis
a	a	k8xC	a
neztížit	ztížit	k5eNaPmF	ztížit
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
zbytečnými	zbytečný	k2eAgInPc7d1	zbytečný
spory	spor	k1gInPc7	spor
se	se	k3xPyFc4	se
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
ovladatelný	ovladatelný	k2eAgInSc1d1	ovladatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
královny	královna	k1gFnPc1	královna
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1476	[number]	k4	1476
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
dvacetiletý	dvacetiletý	k2eAgMnSc1d1	dvacetiletý
Vladislav	Vladislav	k1gMnSc1	Vladislav
sňatek	sňatek	k1gInSc4	sňatek
per	pero	k1gNnPc2	pero
procurationem	procuration	k1gInSc7	procuration
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
velela	velet	k5eAaImAgFnS	velet
dobová	dobový	k2eAgFnSc1d1	dobová
zvyklost	zvyklost	k1gFnSc1	zvyklost
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ansbachu	Ansbach	k1gInSc6	Ansbach
s	s	k7c7	s
Barborou	Barbora	k1gFnSc7	Barbora
Braniborskou	braniborský	k2eAgFnSc7d1	Braniborská
<g/>
,	,	kIx,	,
ovdovělou	ovdovělý	k2eAgFnSc7d1	ovdovělá
dvanáctiletou	dvanáctiletý	k2eAgFnSc7d1	dvanáctiletá
hlohovskou	hlohovský	k2eAgFnSc7d1	hlohovský
kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
braniborského	braniborský	k2eAgMnSc4d1	braniborský
kurfiřta	kurfiřt	k1gMnSc4	kurfiřt
Albrechta	Albrecht	k1gMnSc2	Albrecht
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Achilla	Achilles	k1gMnSc4	Achilles
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Hohenzollernů	Hohenzollern	k1gInPc2	Hohenzollern
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
svoji	svůj	k3xOyFgFnSc4	svůj
choť	choť	k1gFnSc4	choť
nikdy	nikdy	k6eAd1	nikdy
nepřevzal	převzít	k5eNaPmAgMnS	převzít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vinu	vinout	k5eAaImIp1nS	vinout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měl	mít	k5eAaImAgMnS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1476	[number]	k4	1476
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
slezských	slezský	k2eAgNnPc2d1	Slezské
knížectví	knížectví	k1gNnPc2	knížectví
a	a	k8xC	a
s	s	k7c7	s
Hlohovskem	Hlohovsko	k1gNnSc7	Hlohovsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zdědila	zdědit	k5eAaPmAgFnS	zdědit
Barbora	Barbora	k1gFnSc1	Barbora
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
zesnulém	zesnulý	k1gMnSc6	zesnulý
choti	choť	k1gMnSc6	choť
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
jej	on	k3xPp3gInSc4	on
přinést	přinést	k5eAaPmF	přinést
věnem	věno	k1gNnSc7	věno
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
naložil	naložit	k5eAaPmAgMnS	naložit
jako	jako	k9	jako
s	s	k7c7	s
odumřelým	odumřelý	k2eAgNnSc7d1	odumřelé
lénem	léno	k1gNnSc7	léno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
se	se	k3xPyFc4	se
prý	prý	k9	prý
cítil	cítit	k5eAaImAgInS	cítit
vázán	vázat	k5eAaImNgInS	vázat
manželským	manželský	k2eAgInSc7d1	manželský
slibem	slib	k1gInSc7	slib
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
jeho	jeho	k3xOp3gMnPc1	jeho
rádci	rádce	k1gMnPc1	rádce
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
vhodné	vhodný	k2eAgFnSc6d1	vhodná
nevěstě	nevěsta	k1gFnSc6	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
začal	začít	k5eAaPmAgMnS	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
<g/>
,	,	kIx,	,
stranící	stranící	k2eAgFnSc1d1	stranící
Matyáši	Matyáš	k1gMnSc3	Matyáš
Korvínovi	Korvín	k1gMnSc3	Korvín
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
celá	celý	k2eAgNnPc1d1	celé
léta	léto	k1gNnPc1	léto
nevyslyšela	vyslyšet	k5eNaPmAgNnP	vyslyšet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
jak	jak	k6eAd1	jak
uherskému	uherský	k2eAgMnSc3d1	uherský
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
proti	proti	k7c3	proti
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
intrikoval	intrikovat	k5eAaImAgMnS	intrikovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ani	ani	k8xC	ani
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
si	se	k3xPyFc3	se
nepřáli	přát	k5eNaImAgMnP	přát
spojení	spojení	k1gNnSc4	spojení
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
s	s	k7c7	s
Hohenzollerny	Hohenzollern	k1gInPc7	Hohenzollern
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přece	přece	k9	přece
jen	jen	k9	jen
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
za	za	k7c2	za
Vladislava	Vladislav	k1gMnSc2	Vladislav
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
přimlouval	přimlouvat	k5eAaImAgMnS	přimlouvat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pomýšlel	pomýšlet	k5eAaImAgMnS	pomýšlet
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
sňatek	sňatek	k1gInSc4	sňatek
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Kunhutou	Kunhuta	k1gFnSc7	Kunhuta
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
Řím	Řím	k1gInSc1	Řím
neoblomný	oblomný	k2eNgMnSc1d1	neoblomný
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
dokonce	dokonce	k9	dokonce
roku	rok	k1gInSc2	rok
1481	[number]	k4	1481
králi	král	k1gMnSc3	král
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svůj	svůj	k3xOyFgInSc4	svůj
sňatek	sňatek	k1gInSc4	sňatek
konzumoval	konzumovat	k5eAaBmAgMnS	konzumovat
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
neposlechl	poslechnout	k5eNaPmAgMnS	poslechnout
a	a	k8xC	a
Barboru	Barbora	k1gFnSc4	Barbora
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
nepřivedl	přivést	k5eNaPmAgInS	přivést
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnPc4	manželství
zrušil	zrušit	k5eAaPmAgMnS	zrušit
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
se	se	k3xPyFc4	se
Vladislav	Vladislav	k1gMnSc1	Vladislav
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Beatricí	Beatrice	k1gFnSc7	Beatrice
Neapolskou	neapolský	k2eAgFnSc7d1	neapolská
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
neapolského	neapolský	k2eAgMnSc4d1	neapolský
krále	král	k1gMnSc4	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
(	(	kIx(	(
<g/>
Ferrante	Ferrant	k1gMnSc5	Ferrant
<g/>
)	)	kIx)	)
z	z	k7c2	z
nelegitimní	legitimní	k2eNgFnSc2d1	nelegitimní
větve	větev	k1gFnSc2	větev
aragonské	aragonský	k2eAgFnSc2d1	Aragonská
dynastie	dynastie	k1gFnSc2	dynastie
Trastámara	Trastámar	k1gMnSc2	Trastámar
a	a	k8xC	a
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
Matyáši	Matyáš	k1gMnSc6	Matyáš
Korvínovi	Korvín	k1gMnSc6	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
královna	královna	k1gFnSc1	královna
disponovala	disponovat	k5eAaBmAgFnS	disponovat
značnými	značný	k2eAgInPc7d1	značný
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
silami	síla	k1gFnPc7	síla
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
udržela	udržet	k5eAaPmAgFnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
neoblíbená	oblíbený	k2eNgFnSc1d1	neoblíbená
<g/>
,	,	kIx,	,
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Matyášovi	Matyášův	k2eAgMnPc1d1	Matyášův
neporodila	porodit	k5eNaPmAgFnS	porodit
dědice	dědic	k1gMnPc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jejího	její	k3xOp3gInSc2	její
sňatku	sňatek	k1gInSc2	sňatek
s	s	k7c7	s
Vladislavem	Vladislav	k1gMnSc7	Vladislav
bylo	být	k5eAaImAgNnS	být
již	jenž	k3xRgFnSc4	jenž
víceméně	víceméně	k9	víceméně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
zůstane	zůstat	k5eAaPmIp3nS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozdní	pozdní	k2eAgFnSc3d1	pozdní
rozluce	rozluka	k1gFnSc3	rozluka
Vladislavova	Vladislavův	k2eAgNnSc2d1	Vladislavovo
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Barborou	Barbora	k1gFnSc7	Barbora
Braniborskou	braniborský	k2eAgFnSc7d1	Braniborská
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
sňatek	sňatek	k1gInSc4	sňatek
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neplatný	platný	k2eNgInSc1d1	neplatný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anna	Anna	k1gFnSc1	Anna
z	z	k7c2	z
Foix	Foix	k1gInSc4	Foix
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sbližování	sbližování	k1gNnSc2	sbližování
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
sňatcích	sňatek	k1gInPc6	sňatek
<g/>
,	,	kIx,	,
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1502	[number]	k4	1502
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Vladislavovi	Vladislavův	k2eAgMnPc1d1	Vladislavův
zplnomocněnci	zplnomocněnec	k1gMnPc1	zplnomocněnec
svatební	svatební	k2eAgFnSc4d1	svatební
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
z	z	k7c2	z
Foix	Foix	k1gInSc1	Foix
a	a	k8xC	a
Candale	Candala	k1gFnSc3	Candala
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Candale	Candala	k1gFnSc3	Candala
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kateřiny	Kateřina	k1gFnPc1	Kateřina
z	z	k7c2	z
Foix	Foix	k1gInSc1	Foix
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellonským	jagellonský	k2eAgMnSc7d1	jagellonský
byla	být	k5eAaImAgFnS	být
Anna	Anna	k1gFnSc1	Anna
korunována	korunován	k2eAgFnSc1d1	korunována
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1502	[number]	k4	1502
ve	v	k7c6	v
Stoličném	stoličný	k1gMnSc6	stoličný
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
uherskou	uherský	k2eAgFnSc7d1	uherská
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc7d1	Česká
královnou	královna	k1gFnSc7	královna
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
korunovat	korunovat	k5eAaBmF	korunovat
nedala	dát	k5eNaPmAgFnS	dát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Anna	Anna	k1gFnSc1	Anna
konečně	konečně	k6eAd1	konečně
porodila	porodit	k5eAaPmAgFnS	porodit
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Annu	Anna	k1gFnSc4	Anna
(	(	kIx(	(
<g/>
1503	[number]	k4	1503
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Ludvíka	Ludvík	k1gMnSc4	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dcery	dcera	k1gFnSc2	dcera
Anny	Anna	k1gFnSc2	Anna
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1506	[number]	k4	1506
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
I.	I.	kA	I.
dohodu	dohoda	k1gFnSc4	dohoda
to	ten	k3xDgNnSc1	ten
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Anna	Anna	k1gFnSc1	Anna
vezme	vzít	k5eAaPmIp3nS	vzít
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Maxmiliánových	Maxmiliánův	k2eAgMnPc2d1	Maxmiliánův
vnuků	vnuk	k1gMnPc2	vnuk
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
narodí	narodit	k5eAaPmIp3nS	narodit
Vladislavovi	Vladislavův	k2eAgMnPc1d1	Vladislavův
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1506	[number]	k4	1506
stalo	stát	k5eAaPmAgNnS	stát
<g/>
)	)	kIx)	)
ožení	oženit	k5eAaPmIp3nS	oženit
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
s	s	k7c7	s
Maxmiliánovou	Maxmiliánův	k2eAgFnSc7d1	Maxmiliánova
vnučkou	vnučka	k1gFnSc7	vnučka
Marií	Maria	k1gFnPc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
touto	tento	k3xDgFnSc7	tento
dohodou	dohoda	k1gFnSc7	dohoda
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
římském	římský	k2eAgMnSc6d1	římský
králi	král	k1gMnSc6	král
mocného	mocný	k2eAgNnSc2d1	mocné
spojence	spojenka	k1gFnSc6	spojenka
proti	proti	k7c3	proti
odbojným	odbojný	k2eAgMnPc3d1	odbojný
uherským	uherský	k2eAgMnPc3d1	uherský
magnátům	magnát	k1gMnPc3	magnát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
mocný	mocný	k2eAgMnSc1d1	mocný
Jan	Jan	k1gMnSc1	Jan
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
ale	ale	k8xC	ale
na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
příliš	příliš	k6eAd1	příliš
ohled	ohled	k1gInSc1	ohled
nebral	brát	k5eNaImAgInS	brát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1506	[number]	k4	1506
tedy	tedy	k9	tedy
Maxmiliánova	Maxmiliánův	k2eAgNnSc2d1	Maxmiliánovo
vojska	vojsko	k1gNnSc2	vojsko
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Prešpurk	Prešpurk	k?	Prešpurk
a	a	k8xC	a
Šoproň	Šoproň	k1gFnSc1	Šoproň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přichází	přicházet	k5eAaImIp3nS	přicházet
hájit	hájit	k5eAaImF	hájit
svá	svůj	k3xOyFgNnPc4	svůj
dědická	dědický	k2eAgNnPc4d1	dědické
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kritické	kritický	k2eAgFnSc6d1	kritická
chvíli	chvíle	k1gFnSc6	chvíle
obrátil	obrátit	k5eAaPmAgMnS	obrátit
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
české	český	k2eAgInPc4d1	český
a	a	k8xC	a
moravské	moravský	k2eAgInPc4d1	moravský
stavy	stav	k1gInPc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Králův	Králův	k2eAgMnSc1d1	Králův
vyslanec	vyslanec	k1gMnSc1	vyslanec
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
Filipec	Filipec	k1gMnSc1	Filipec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
urovnat	urovnat	k5eAaPmF	urovnat
spory	spor	k1gInPc4	spor
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
I.	I.	kA	I.
Stárnoucí	stárnoucí	k2eAgFnPc1d1	stárnoucí
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1504	[number]	k4	1504
také	také	k9	také
vážně	vážně	k6eAd1	vážně
nemocný	nemocný	k2eAgMnSc1d1	nemocný
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
se	s	k7c7	s
26	[number]	k4	26
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1506	[number]	k4	1506
dočkal	dočkat	k5eAaPmAgMnS	dočkat
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
narození	narození	k1gNnSc2	narození
syna	syn	k1gMnSc2	syn
a	a	k8xC	a
dědice	dědic	k1gMnSc2	dědic
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
milovaná	milovaný	k2eAgFnSc1d1	milovaná
manželka	manželka	k1gFnSc1	manželka
Anna	Anna	k1gFnSc1	Anna
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
chorobě	choroba	k1gFnSc3	choroba
ji	on	k3xPp3gFnSc4	on
přežil	přežít	k5eAaPmAgMnS	přežít
o	o	k7c4	o
celých	celý	k2eAgNnPc2d1	celé
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
skutečně	skutečně	k6eAd1	skutečně
provdala	provdat	k5eAaPmAgFnS	provdat
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gInPc2	Habsburk
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Habsburskou	habsburský	k2eAgFnSc7d1	habsburská
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
roku	rok	k1gInSc2	rok
1506	[number]	k4	1506
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Jagellonci	Jagellonec	k1gMnPc7	Jagellonec
a	a	k8xC	a
Habsburky	Habsburk	k1gMnPc7	Habsburk
vlastně	vlastně	k9	vlastně
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
pozdějším	pozdní	k2eAgInSc6d2	pozdější
vzniku	vznik	k1gInSc6	vznik
podunajské	podunajský	k2eAgFnSc2d1	Podunajská
monarchie	monarchie	k1gFnSc2	monarchie
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc4	dítě
měl	mít	k5eAaImAgMnS	mít
Vladislav	Vladislav	k1gMnSc1	Vladislav
až	až	k9	až
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
třetí	třetí	k4xOgFnSc7	třetí
manželkou	manželka	k1gFnSc7	manželka
Annou	Anna	k1gFnSc7	Anna
z	z	k7c2	z
Foix	Foix	k1gInSc1	Foix
(	(	kIx(	(
<g/>
1484	[number]	k4	1484
<g/>
–	–	k?	–
<g/>
1506	[number]	k4	1506
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
uherská	uherský	k2eAgFnSc1d1	uherská
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
královna	královna	k1gFnSc1	královna
<g/>
∞	∞	k?	∞
1521	[number]	k4	1521
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
HabsburskýLudvík	HabsburskýLudvík	k1gMnSc1	HabsburskýLudvík
(	(	kIx(	(
<g/>
1506	[number]	k4	1506
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
uherský	uherský	k2eAgInSc4d1	uherský
<g/>
∞	∞	k?	∞
1522	[number]	k4	1522
Marie	Maria	k1gFnSc2	Maria
Habsburská	habsburský	k2eAgFnSc5d1	habsburská
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1437	[number]	k4	1437
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Jagellonci	Jagellonek	k1gMnPc1	Jagellonek
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
300	[number]	k4	300
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
493	[number]	k4	493
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
273	[number]	k4	273
<g/>
-	-	kIx~	-
<g/>
285	[number]	k4	285
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
BARTLOVÁ	Bartlová	k1gFnSc1	Bartlová
<g/>
,	,	kIx,	,
Milena	Milena	k1gFnSc1	Milena
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
VI	VI	kA	VI
<g/>
.	.	kIx.	.
1437	[number]	k4	1437
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
839	[number]	k4	839
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
873	[number]	k4	873
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
věk	věk	k1gInSc1	věk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
základna	základna	k1gFnSc1	základna
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
342	[number]	k4	342
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
věk	věk	k1gInSc1	věk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
356	[number]	k4	356
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
věk	věk	k1gInSc1	věk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
387	[number]	k4	387
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
629	[number]	k4	629
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
věk	věk	k1gInSc1	věk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Venkovský	venkovský	k2eAgInSc1d1	venkovský
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
národnostní	národnostní	k2eAgFnSc1d1	národnostní
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
223	[number]	k4	223
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
699	[number]	k4	699
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
a	a	k8xC	a
zbožnost	zbožnost	k1gFnSc1	zbožnost
jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
488	[number]	k4	488
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
ženy	žena	k1gFnPc1	žena
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
238	[number]	k4	238
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
232	[number]	k4	232
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vladislavské	vladislavský	k2eAgNnSc1d1	Vladislavské
zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgFnSc2d1	zemská
</s>
</p>
<p>
<s>
Vladislavská	vladislavský	k2eAgFnSc1d1	vladislavská
gotika	gotika	k1gFnSc1	gotika
</s>
</p>
<p>
<s>
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
smlouva	smlouva	k1gFnSc1	smlouva
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
</s>
</p>
<p>
<s>
Zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgFnSc2d1	zemská
vladislavské	vladislavský	k2eAgFnSc2d1	vladislavská
z	z	k7c2	z
r.	r.	kA	r.
1500	[number]	k4	1500
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
