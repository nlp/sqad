<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1456	[number]	k4	1456
Krakov	Krakov	k1gInSc1	Krakov
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1516	[number]	k4	1516
Budín	Budín	k1gInSc1	Budín
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
litevského	litevský	k2eAgInSc2d1	litevský
velkoknížecího	velkoknížecí	k2eAgInSc2d1	velkoknížecí
a	a	k8xC	a
polského	polský	k2eAgInSc2d1	polský
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
se	se	k3xPyFc4	se
jménem	jméno	k1gNnSc7	jméno
Władysław	Władysław	k1gFnPc2	Władysław
Jagiełło	Jagiełło	k6eAd1	Jagiełło
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
králem	král	k1gMnSc7	král
českým	český	k2eAgFnPc3d1	Česká
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
jako	jako	k8xC	jako
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
markrabětem	markrabě	k1gMnSc7	markrabě
moravským	moravský	k2eAgMnSc7d1	moravský
a	a	k8xC	a
králem	král	k1gMnSc7	král
uherským	uherský	k2eAgInPc3d1	uherský
také	také	k9	také
jako	jako	k9	jako
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Ulászló	Ulászló	k1gMnSc1	Ulászló
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
