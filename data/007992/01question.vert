<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
orgán	orgán	k1gInSc1	orgán
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
orientaci	orientace	k1gFnSc4	orientace
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
?	?	kIx.	?
</s>
