<s>
Ryby	Ryby	k1gFnPc1	Ryby
(	(	kIx(	(
<g/>
Osteichthyes	Osteichthyes	k1gInSc1	Osteichthyes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
kostnaté	kostnatý	k2eAgFnPc4d1	kostnatá
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
skupina	skupina	k1gFnSc1	skupina
primárně	primárně	k6eAd1	primárně
vodních	vodní	k2eAgMnPc2d1	vodní
obratlovců	obratlovec	k1gMnPc2	obratlovec
vyznačujících	vyznačující	k2eAgFnPc2d1	vyznačující
se	se	k3xPyFc4	se
osifikovanou	osifikovaný	k2eAgFnSc7d1	osifikovaná
kostrou	kostra	k1gFnSc7	kostra
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
však	však	k9	však
kostra	kostra	k1gFnSc1	kostra
druhotně	druhotně	k6eAd1	druhotně
chrupavčitá	chrupavčitý	k2eAgFnSc1d1	chrupavčitá
<g/>
)	)	kIx)	)
a	a	k8xC	a
skřelemi	skřele	k1gFnPc7	skřele
kryjícími	kryjící	k2eAgInPc7d1	kryjící
žábry	žábry	k1gFnPc1	žábry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradičních	tradiční	k2eAgInPc6d1	tradiční
taxonomických	taxonomický	k2eAgInPc6d1	taxonomický
systémech	systém	k1gInPc6	systém
měly	mít	k5eAaImAgFnP	mít
ryby	ryba	k1gFnPc1	ryba
postavení	postavení	k1gNnSc2	postavení
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nadtřídu	nadtřída	k1gFnSc4	nadtřída
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
parafyletismus	parafyletismus	k1gInSc4	parafyletismus
nejsou	být	k5eNaImIp3nP	být
jako	jako	k9	jako
taxon	taxon	k1gInSc4	taxon
užívány	užívat	k5eAaImNgInP	užívat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
ryby	ryba	k1gFnPc1	ryba
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
potomky	potomek	k1gMnPc4	potomek
posledního	poslední	k2eAgMnSc4d1	poslední
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
,	,	kIx,	,
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
by	by	kYmCp3nP	by
i	i	k9	i
čtyřnožce	čtyřnožec	k1gMnPc4	čtyřnožec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
takto	takto	k6eAd1	takto
definovaná	definovaný	k2eAgFnSc1d1	definovaná
holofyletická	holofyletický	k2eAgFnSc1d1	holofyletický
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Euteleostomi	Euteleosto	k1gFnPc7	Euteleosto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ryby	ryba	k1gFnPc4	ryba
patří	patřit	k5eAaImIp3nP	patřit
dvě	dva	k4xCgFnPc1	dva
třídy	třída	k1gFnPc1	třída
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
nozdratí	nozdratit	k5eAaPmIp3nS	nozdratit
(	(	kIx(	(
<g/>
Sarcopterygii	Sarcopterygie	k1gFnSc4	Sarcopterygie
<g/>
)	)	kIx)	)
v	v	k7c6	v
parafyletickém	parafyletický	k2eAgInSc6d1	parafyletický
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
paprskoploutví	paprskoploutví	k1gNnSc6	paprskoploutví
(	(	kIx(	(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc6	Actinopterygie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
paprskoploutví	paprskoploutví	k1gNnSc1	paprskoploutví
jsou	být	k5eAaImIp3nP	být
monofyletický	monofyletický	k2eAgInSc4d1	monofyletický
taxon	taxon	k1gInSc4	taxon
<g/>
,	,	kIx,	,
nozdratí	nozdratit	k5eAaPmIp3nS	nozdratit
(	(	kIx(	(
<g/>
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
taxon	taxon	k1gInSc4	taxon
parafyletický	parafyletický	k2eAgInSc4d1	parafyletický
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
za	za	k7c7	za
potomky	potomek	k1gMnPc7	potomek
lalokoploutvých	lalokoploutvý	k2eAgFnPc2d1	lalokoploutvá
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
třídy	třída	k1gFnPc1	třída
strunatců	strunatec	k1gMnPc2	strunatec
včetně	včetně	k7c2	včetně
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
skupina	skupina	k1gFnSc1	skupina
nozdratých	nozdratý	k2eAgFnPc2d1	nozdratý
a	a	k8xC	a
parskoploutvých	parskoploutvý	k2eAgFnPc2d1	parskoploutvý
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
rovněž	rovněž	k9	rovněž
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsoben	k2eAgFnPc1d1	přizpůsobena
životu	život	k1gInSc3	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
obývají	obývat	k5eAaImIp3nP	obývat
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
,	,	kIx,	,
brakické	brakický	k2eAgFnPc1d1	brakická
(	(	kIx(	(
<g/>
smíšené	smíšený	k2eAgMnPc4d1	smíšený
<g/>
)	)	kIx)	)
i	i	k9	i
slané	slaný	k2eAgFnPc1d1	slaná
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
extrémních	extrémní	k2eAgInPc2d1	extrémní
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
sodné	sodný	k2eAgInPc4d1	sodný
termální	termální	k2eAgInPc4d1	termální
prameny	pramen	k1gInPc4	pramen
<g/>
,	,	kIx,	,
periodicky	periodicky	k6eAd1	periodicky
vysychající	vysychající	k2eAgFnPc1d1	vysychající
vody	voda	k1gFnPc1	voda
nebo	nebo	k8xC	nebo
vody	voda	k1gFnPc1	voda
podzemní	podzemní	k2eAgFnPc1d1	podzemní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
obojživelné	obojživelný	k2eAgFnPc1d1	obojživelná
a	a	k8xC	a
určitý	určitý	k2eAgInSc1d1	určitý
čas	čas	k1gInSc1	čas
tráví	trávit	k5eAaImIp3nS	trávit
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
recentních	recentní	k2eAgInPc2d1	recentní
druhů	druh	k1gInPc2	druh
kostnatých	kostnatý	k2eAgFnPc2d1	kostnatá
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
24	[number]	k4	24
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Variabilita	variabilita	k1gFnSc1	variabilita
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
druhy	druh	k1gMnPc4	druh
dravé	dravý	k2eAgMnPc4d1	dravý
<g/>
,	,	kIx,	,
všežravé	všežravý	k2eAgFnPc1d1	všežravá
<g/>
,	,	kIx,	,
býložravé	býložravý	k2eAgFnPc1d1	býložravá
i	i	k8xC	i
parazitické	parazitický	k2eAgFnPc1d1	parazitická
<g/>
,	,	kIx,	,
druhy	druh	k1gInPc1	druh
žijící	žijící	k2eAgInPc1d1	žijící
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
,	,	kIx,	,
v	v	k7c6	v
párech	pár	k1gInPc6	pár
i	i	k8xC	i
ve	v	k7c6	v
statisícových	statisícový	k2eAgNnPc6d1	statisícové
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnPc1d3	nejmenší
ryby	ryba	k1gFnPc1	ryba
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejmenší	malý	k2eAgMnPc4d3	nejmenší
obratlovce	obratlovec	k1gMnPc4	obratlovec
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
8	[number]	k4	8
mm	mm	kA	mm
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgInPc1d1	jiný
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
metry	metr	k1gMnPc7	metr
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Rekordně	rekordně	k6eAd1	rekordně
velikou	veliký	k2eAgFnSc7d1	veliká
rybou	ryba	k1gFnSc7	ryba
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
geologická	geologický	k2eAgNnPc4d1	geologické
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejspíš	nejspíš	k9	nejspíš
střednějurský	střednějurský	k2eAgInSc1d1	střednějurský
rod	rod	k1gInSc1	rod
Leedsichthys	Leedsichthys	k1gInSc1	Leedsichthys
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
až	až	k9	až
přes	přes	k7c4	přes
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
mají	mít	k5eAaImIp3nP	mít
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vodních	vodní	k2eAgInPc6d1	vodní
ekosystémech	ekosystém	k1gInPc6	ekosystém
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
významnou	významný	k2eAgFnSc7d1	významná
lidskou	lidský	k2eAgFnSc7d1	lidská
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
předmětem	předmět	k1gInSc7	předmět
obchodu	obchod	k1gInSc2	obchod
pro	pro	k7c4	pro
okrasné	okrasný	k2eAgInPc4d1	okrasný
účely	účel	k1gInPc4	účel
a	a	k8xC	a
akvaristiku	akvaristika	k1gFnSc4	akvaristika
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
přijímají	přijímat	k5eAaImIp3nP	přijímat
potravu	potrava	k1gFnSc4	potrava
ústy	ústa	k1gNnPc7	ústa
(	(	kIx(	(
<g/>
ústa	ústa	k1gNnPc1	ústa
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
horní	horní	k2eAgFnSc1d1	horní
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
nebo	nebo	k8xC	nebo
koncová	koncový	k2eAgFnSc1d1	koncová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnPc1d1	ústní
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neprodukují	produkovat	k5eNaImIp3nP	produkovat
sliny	slina	k1gFnPc4	slina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
dutinou	dutina	k1gFnSc7	dutina
ústní	ústní	k2eAgFnSc7d1	ústní
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hltan	hltan	k1gInSc1	hltan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
dně	dno	k1gNnSc6	dno
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
skupin	skupina	k1gFnPc2	skupina
umístěny	umístit	k5eAaPmNgInP	umístit
požerákové	požerákový	k2eAgInPc1d1	požerákový
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
pátého	pátý	k4xOgInSc2	pátý
páru	pár	k1gInSc2	pár
žaberních	žaberní	k2eAgInPc2d1	žaberní
oblouků	oblouk	k1gInPc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Požerákové	požerákový	k2eAgInPc1d1	požerákový
zuby	zub	k1gInPc1	zub
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Trávící	trávící	k2eAgFnSc1d1	trávící
trubice	trubice	k1gFnSc1	trubice
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jícnem	jícen	k1gInSc7	jícen
<g/>
,	,	kIx,	,
žaludkem	žaludek	k1gInSc7	žaludek
<g/>
,	,	kIx,	,
střevem	střevo	k1gNnSc7	střevo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
jsou	být	k5eAaImIp3nP	být
pylorické	pylorický	k2eAgInPc1d1	pylorický
výběžky	výběžek	k1gInPc1	výběžek
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
sekrece	sekrece	k1gFnSc2	sekrece
trávících	trávící	k2eAgInPc2d1	trávící
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
trávící	trávící	k2eAgFnSc2d1	trávící
soustavy	soustava	k1gFnSc2	soustava
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
slezina	slezina	k1gFnSc1	slezina
<g/>
,	,	kIx,	,
žlučník	žlučník	k1gInSc1	žlučník
<g/>
,	,	kIx,	,
slinivka	slinivka	k1gFnSc1	slinivka
břišní	břišní	k2eAgFnSc1d1	břišní
<g/>
.	.	kIx.	.
</s>
<s>
Koncové	koncový	k2eAgNnSc1d1	koncové
střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
též	též	k9	též
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgInSc7d1	poslední
úsekem	úsek	k1gInSc7	úsek
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Zbytkům	zbytek	k1gInPc3	zbytek
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
odnímána	odnímán	k2eAgFnSc1d1	odnímána
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
tak	tak	k9	tak
pevné	pevný	k2eAgInPc1d1	pevný
výkaly	výkal	k1gInPc1	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgFnSc1d1	trávicí
trubice	trubice	k1gFnSc1	trubice
končí	končit	k5eAaImIp3nS	končit
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
řitní	řitní	k2eAgFnSc7d1	řitní
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dvojdyšných	dvojdyšní	k1gMnPc2	dvojdyšní
je	být	k5eAaImIp3nS	být
řitní	řitní	k2eAgInSc4d1	řitní
otvor	otvor	k1gInSc4	otvor
vždy	vždy	k6eAd1	vždy
samostatný	samostatný	k2eAgInSc4d1	samostatný
<g/>
,	,	kIx,	,
nespojený	spojený	k2eNgInSc4d1	nespojený
s	s	k7c7	s
vývodem	vývod	k1gInSc7	vývod
jiné	jiný	k2eAgFnSc2d1	jiná
orgánové	orgánový	k2eAgFnSc2d1	orgánová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
pátého	pátý	k4xOgInSc2	pátý
páru	pár	k1gInSc2	pár
žaberního	žaberní	k2eAgInSc2d1	žaberní
oblouku	oblouk	k1gInSc2	oblouk
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
hltanu	hltan	k1gInSc6	hltan
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
bolena	bolen	k2eAgMnSc4d1	bolen
<g/>
,	,	kIx,	,
kapra	kapr	k1gMnSc4	kapr
<g/>
,	,	kIx,	,
pstruha	pstruh	k1gMnSc4	pstruh
<g/>
,	,	kIx,	,
plotice	plotice	k1gFnSc1	plotice
<g/>
,	,	kIx,	,
střevle	střevle	k1gFnSc1	střevle
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rybám	Ryby	k1gFnPc3	Ryby
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
polykat	polykat	k5eAaImF	polykat
<g/>
,	,	kIx,	,
posouvat	posouvat	k5eAaImF	posouvat
potravu	potrava	k1gFnSc4	potrava
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
jícnu	jícen	k1gInSc2	jícen
a	a	k8xC	a
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
ryb	ryba	k1gFnPc2	ryba
mechanicky	mechanicky	k6eAd1	mechanicky
trávit	trávit	k5eAaImF	trávit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Požerákové	požerákový	k2eAgInPc1d1	požerákový
zuby	zub	k1gInPc1	zub
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
-	-	kIx~	-
špičaté	špičatý	k2eAgInPc1d1	špičatý
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgInPc1d1	plochý
<g/>
,	,	kIx,	,
tupé	tupý	k2eAgInPc1d1	tupý
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
třech	tři	k4xCgFnPc6	tři
řadách	řada	k1gFnPc6	řada
na	na	k7c6	na
požerákové	požerákový	k2eAgFnSc6d1	požerákový
kosti	kost	k1gFnSc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
Požerákové	požerákový	k2eAgInPc1d1	požerákový
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
možným	možný	k2eAgInSc7d1	možný
rozlišovacím	rozlišovací	k2eAgInSc7d1	rozlišovací
a	a	k8xC	a
identifikačním	identifikační	k2eAgInSc7d1	identifikační
znakem	znak	k1gInSc7	znak
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
druh	druh	k1gInSc4	druh
ryby	ryba	k1gFnSc2	ryba
je	být	k5eAaImIp3nS	být
uspořádání	uspořádání	k1gNnSc1	uspořádání
a	a	k8xC	a
počet	počet	k1gInSc1	počet
požerákových	požerákový	k2eAgInPc2d1	požerákový
zubů	zub	k1gInPc2	zub
typický	typický	k2eAgMnSc1d1	typický
a	a	k8xC	a
ozubení	ozubení	k1gNnSc1	ozubení
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
zapsat	zapsat	k5eAaPmF	zapsat
vzorcem	vzorec	k1gInSc7	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Odpadní	odpadní	k2eAgInPc1d1	odpadní
produkty	produkt	k1gInPc1	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
jsou	být	k5eAaImIp3nP	být
vylučovány	vylučován	k2eAgInPc1d1	vylučován
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
vylučován	vylučovat	k5eAaImNgInS	vylučovat
žábrami	žábry	k1gFnPc7	žábry
nebo	nebo	k8xC	nebo
přídatnými	přídatný	k2eAgInPc7d1	přídatný
dýchacími	dýchací	k2eAgInPc7d1	dýchací
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Žábry	žábry	k1gFnPc1	žábry
se	se	k3xPyFc4	se
také	také	k9	také
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
vylučování	vylučování	k1gNnSc4	vylučování
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
nadbytečných	nadbytečný	k2eAgInPc2d1	nadbytečný
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
odpadní	odpadní	k2eAgFnPc1d1	odpadní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
produkty	produkt	k1gInPc1	produkt
rozkladu	rozklad	k1gInSc2	rozklad
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vylučovány	vylučovat	k5eAaImNgFnP	vylučovat
trávicí	trávicí	k2eAgFnSc7d1	trávicí
soustavou	soustava	k1gFnSc7	soustava
a	a	k8xC	a
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
odcházejí	odcházet	k5eAaImIp3nP	odcházet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výkaly	výkal	k1gInPc7	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
vylučovací	vylučovací	k2eAgFnSc4d1	vylučovací
funkci	funkce	k1gFnSc4	funkce
mají	mít	k5eAaImIp3nP	mít
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
,	,	kIx,	,
odstraňující	odstraňující	k2eAgFnPc4d1	odstraňující
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
škodliviny	škodlivina	k1gFnSc2	škodlivina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dusíkaté	dusíkatý	k2eAgFnPc1d1	dusíkatá
látky	látka	k1gFnPc1	látka
vznikající	vznikající	k2eAgFnPc1d1	vznikající
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Odpadní	odpadní	k2eAgFnPc1d1	odpadní
dusíkaté	dusíkatý	k2eAgFnPc1d1	dusíkatá
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
obecně	obecně	k6eAd1	obecně
vylučovány	vylučovat	k5eAaImNgInP	vylučovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
základních	základní	k2eAgFnPc6d1	základní
formách	forma	k1gFnPc6	forma
-	-	kIx~	-
jako	jako	k8xS	jako
amoniak	amoniak	k1gInSc1	amoniak
<g/>
,	,	kIx,	,
močovina	močovina	k1gFnSc1	močovina
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
kyselina	kyselina	k1gFnSc1	kyselina
močová	močový	k2eAgFnSc1d1	močová
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
formou	forma	k1gFnSc7	forma
odpadního	odpadní	k2eAgInSc2d1	odpadní
dusíku	dusík	k1gInSc2	dusík
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
amoniak	amoniak	k1gInSc1	amoniak
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
močovina	močovina	k1gFnSc1	močovina
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
efektivně	efektivně	k6eAd1	efektivně
vylučovány	vylučovat	k5eAaImNgFnP	vylučovat
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
nejen	nejen	k6eAd1	nejen
ledvinami	ledvina	k1gFnPc7	ledvina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
žábrami	žábry	k1gFnPc7	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Kyselinu	kyselina	k1gFnSc4	kyselina
močovou	močový	k2eAgFnSc4d1	močová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
nevyužívají	využívat	k5eNaImIp3nP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Ledviny	ledvina	k1gFnPc1	ledvina
zároveň	zároveň	k6eAd1	zároveň
regulují	regulovat	k5eAaImIp3nP	regulovat
obsah	obsah	k1gInSc4	obsah
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
ledviny	ledvina	k1gFnPc4	ledvina
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
uloženy	uložit	k5eAaPmNgInP	uložit
pod	pod	k7c7	pod
páteří	páteř	k1gFnSc7	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
párové	párový	k2eAgInPc1d1	párový
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lososovití	lososovití	k1gMnPc1	lososovití
<g/>
)	)	kIx)	)
srůstají	srůstat	k5eAaImIp3nP	srůstat
v	v	k7c4	v
jediný	jediný	k2eAgInSc4d1	jediný
nepárový	párový	k2eNgInSc4d1	nepárový
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Moč	moč	k1gFnSc1	moč
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ledvin	ledvina	k1gFnPc2	ledvina
odváděna	odváděn	k2eAgFnSc1d1	odváděna
močovody	močovod	k1gInPc4	močovod
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
i	i	k9	i
močový	močový	k2eAgInSc1d1	močový
měchýř	měchýř	k1gInSc1	měchýř
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vycházející	vycházející	k2eAgFnSc1d1	vycházející
nepárová	párový	k2eNgFnSc1d1	nepárová
močová	močový	k2eAgFnSc1d1	močová
trubice	trubice	k1gFnSc1	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Močové	močový	k2eAgFnPc1d1	močová
cesty	cesta	k1gFnPc1	cesta
ústí	ústit	k5eAaImIp3nP	ústit
samostatnými	samostatný	k2eAgInPc7d1	samostatný
otvory	otvor	k1gInPc7	otvor
na	na	k7c6	na
močopohlavní	močopohlavní	k2eAgFnSc6d1	močopohlavní
bradavce	bradavka	k1gFnSc6	bradavka
(	(	kIx(	(
<g/>
urogenitální	urogenitální	k2eAgFnSc3d1	urogenitální
papile	papila	k1gFnSc3	papila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
mezi	mezi	k7c7	mezi
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
a	a	k8xC	a
řitní	řitní	k2eAgFnSc7d1	řitní
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Sladkovodní	sladkovodní	k2eAgFnPc1d1	sladkovodní
ryby	ryba	k1gFnPc1	ryba
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
hypotonickém	hypotonický	k2eAgNnSc6d1	hypotonické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
osmoticky	osmoticky	k6eAd1	osmoticky
proto	proto	k8xC	proto
přijímají	přijímat	k5eAaImIp3nP	přijímat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
skrze	skrze	k?	skrze
žábry	žábry	k1gFnPc4	žábry
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
tělesné	tělesný	k2eAgInPc4d1	tělesný
povrchy	povrch	k1gInPc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Přebytečnou	přebytečný	k2eAgFnSc4d1	přebytečná
vodu	voda	k1gFnSc4	voda
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
ledvinami	ledvina	k1gFnPc7	ledvina
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
velmi	velmi	k6eAd1	velmi
řídké	řídký	k2eAgFnSc6d1	řídká
moči	moč	k1gFnSc6	moč
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
k	k	k7c3	k
citelným	citelný	k2eAgFnPc3d1	citelná
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Ionty	ion	k1gInPc1	ion
jsou	být	k5eAaImIp3nP	být
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
potravou	potrava	k1gFnSc7	potrava
a	a	k8xC	a
výměnou	výměna	k1gFnSc7	výměna
odpadních	odpadní	k2eAgInPc2d1	odpadní
amonných	amonný	k2eAgInPc2d1	amonný
kationtů	kation	k1gInPc2	kation
za	za	k7c4	za
kationty	kation	k1gInPc4	kation
sodné	sodný	k2eAgInPc4d1	sodný
v	v	k7c6	v
žábrách	žábry	k1gFnPc6	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgFnPc1d1	mořská
ryby	ryba	k1gFnPc1	ryba
žijí	žít	k5eAaImIp3nP	žít
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
hypertonickém	hypertonický	k2eAgNnSc6d1	hypertonické
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
proto	proto	k8xC	proto
osmoticky	osmoticky	k6eAd1	osmoticky
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
vody	voda	k1gFnSc2	voda
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
pitím	pití	k1gNnSc7	pití
<g/>
.	.	kIx.	.
</s>
<s>
Přebytečné	přebytečný	k2eAgFnPc1d1	přebytečná
soli	sůl	k1gFnPc1	sůl
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
silně	silně	k6eAd1	silně
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
močí	moč	k1gFnSc7	moč
a	a	k8xC	a
žábrami	žábry	k1gFnPc7	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Amoniak	amoniak	k1gInSc1	amoniak
je	být	k5eAaImIp3nS	být
vylučován	vylučovat	k5eAaImNgInS	vylučovat
ledvinami	ledvina	k1gFnPc7	ledvina
i	i	k8xC	i
žábrami	žábry	k1gFnPc7	žábry
<g/>
.	.	kIx.	.
</s>
<s>
Primárním	primární	k2eAgInSc7d1	primární
dýchacím	dýchací	k2eAgInSc7d1	dýchací
orgánem	orgán	k1gInSc7	orgán
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc4	čtyři
páry	pár	k1gInPc4	pár
žaber	žábry	k1gFnPc2	žábry
umístěné	umístěný	k2eAgFnPc4d1	umístěná
na	na	k7c6	na
kostěných	kostěný	k2eAgInPc6d1	kostěný
žaberních	žaberní	k2eAgInPc6d1	žaberní
obloucích	oblouk	k1gInPc6	oblouk
v	v	k7c6	v
žaberní	žaberní	k2eAgFnSc6d1	žaberní
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Žábry	žábry	k1gFnPc1	žábry
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
bohatě	bohatě	k6eAd1	bohatě
prokrvenými	prokrvený	k2eAgInPc7d1	prokrvený
žaberními	žaberní	k2eAgInPc7d1	žaberní
lístky	lístek	k1gInPc7	lístek
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
jemnou	jemný	k2eAgFnSc7d1	jemná
kapilární	kapilární	k2eAgFnSc7d1	kapilární
sítí	síť	k1gFnSc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
omývající	omývající	k2eAgFnPc1d1	omývající
žábry	žábry	k1gFnPc1	žábry
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
odděleny	oddělit	k5eAaPmNgInP	oddělit
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc7d1	tenká
kapilární	kapilární	k2eAgFnSc7d1	kapilární
stěnou	stěna	k1gFnSc7	stěna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
výměnu	výměna	k1gFnSc4	výměna
dýchacích	dýchací	k2eAgMnPc2d1	dýchací
plynů	plyn	k1gInPc2	plyn
prostou	prostý	k2eAgFnSc7d1	prostá
difúzí	difúze	k1gFnSc7	difúze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozdílu	rozdíl	k1gInSc2	rozdíl
parciálních	parciální	k2eAgInPc2d1	parciální
tlaků	tlak	k1gInPc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dýchání	dýchání	k1gNnSc2	dýchání
se	se	k3xPyFc4	se
žábry	žábry	k1gFnPc1	žábry
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nP	podílet
i	i	k9	i
na	na	k7c4	na
vylučování	vylučování	k1gNnSc4	vylučování
odpadních	odpadní	k2eAgFnPc2d1	odpadní
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
udržování	udržování	k1gNnSc1	udržování
rovnováhy	rovnováha	k1gFnSc2	rovnováha
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
obojživelné	obojživelný	k2eAgFnPc1d1	obojživelná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
různé	různý	k2eAgInPc1d1	různý
přídavné	přídavný	k2eAgInPc1d1	přídavný
dýchací	dýchací	k2eAgInPc1d1	dýchací
orgány	orgán	k1gInPc1	orgán
sloužící	sloužící	k2eAgInPc1d1	sloužící
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
kyslíku	kyslík	k1gInSc2	kyslík
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
dýchání	dýchání	k1gNnSc1	dýchání
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
jen	jen	k9	jen
doplňkové	doplňkový	k2eAgInPc1d1	doplňkový
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
mají	mít	k5eAaImIp3nP	mít
zachované	zachovaný	k2eAgFnPc1d1	zachovaná
žábry	žábry	k1gFnPc1	žábry
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
závislé	závislý	k2eAgInPc1d1	závislý
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nemožnosti	nemožnost	k1gFnSc2	nemožnost
nadechnutí	nadechnutí	k1gNnSc2	nadechnutí
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
se	se	k3xPyFc4	se
utopí	utopit	k5eAaPmIp3nP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Labyrintky	Labyrintka	k1gFnPc1	Labyrintka
(	(	kIx(	(
<g/>
ryby	ryba	k1gFnPc1	ryba
podřádu	podřád	k1gInSc2	podřád
Anabantoidei	Anabantoide	k1gFnPc1	Anabantoide
<g/>
)	)	kIx)	)
získaly	získat	k5eAaPmAgFnP	získat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
labyrintu	labyrint	k1gInSc2	labyrint
<g/>
,	,	kIx,	,
kostěného	kostěný	k2eAgInSc2d1	kostěný
lamelovitého	lamelovitý	k2eAgInSc2d1	lamelovitý
útvaru	útvar	k1gInSc2	útvar
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
prvního	první	k4xOgInSc2	první
žaberního	žaberní	k2eAgInSc2d1	žaberní
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
pokrytého	pokrytý	k2eAgInSc2d1	pokrytý
hustě	hustě	k6eAd1	hustě
prokrvenou	prokrvený	k2eAgFnSc7d1	prokrvená
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Labyrintky	Labyrintek	k1gInPc1	Labyrintek
vdechují	vdechovat	k5eAaImIp3nP	vdechovat
tlamkou	tlamka	k1gFnSc7	tlamka
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
bublina	bublina	k1gFnSc1	bublina
se	se	k3xPyFc4	se
dopraví	dopravit	k5eAaPmIp3nS	dopravit
do	do	k7c2	do
žaberní	žaberní	k2eAgFnSc2d1	žaberní
dutiny	dutina	k1gFnSc2	dutina
k	k	k7c3	k
labyrintu	labyrint	k1gInSc3	labyrint
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
dýchacích	dýchací	k2eAgInPc2d1	dýchací
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
labyrintky	labyrintek	k1gMnPc7	labyrintek
paří	pařit	k5eAaImIp3nS	pařit
i	i	k9	i
lezoun	lezoun	k1gMnSc1	lezoun
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Anabas	Anabas	k1gMnSc1	Anabas
testudineus	testudineus	k1gMnSc1	testudineus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
opouští	opouštět	k5eAaImIp3nS	opouštět
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
až	až	k9	až
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
metrů	metr	k1gInPc2	metr
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
sucha	sucho	k1gNnSc2	sucho
přežívá	přežívat	k5eAaImIp3nS	přežívat
zahrabaný	zahrabaný	k2eAgMnSc1d1	zahrabaný
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
sumci	sumec	k1gMnPc1	sumec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pancéřníčci	pancéřníček	k1gMnPc5	pancéřníček
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
Callichthyidae	Callichthyida	k1gFnSc2	Callichthyida
<g/>
)	)	kIx)	)
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
krunýřovci	krunýřovec	k1gMnPc1	krunýřovec
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
Loricariidae	Loricariida	k1gFnSc2	Loricariida
<g/>
)	)	kIx)	)
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
střevní	střevní	k2eAgNnSc4d1	střevní
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
polykají	polykat	k5eAaImIp3nP	polykat
<g/>
,	,	kIx,	,
bublina	bublina	k1gFnSc1	bublina
prochází	procházet	k5eAaImIp3nS	procházet
trávicím	trávicí	k2eAgInSc7d1	trávicí
traktem	trakt	k1gInSc7	trakt
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
části	část	k1gFnSc6	část
střeva	střevo	k1gNnSc2	střevo
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
krunýřovců	krunýřovec	k1gInPc2	krunýřovec
žaludku	žaludek	k1gInSc2	žaludek
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vzduch	vzduch	k1gInSc1	vzduch
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
rybami	ryba	k1gFnPc7	ryba
jsou	být	k5eAaImIp3nP	být
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
přežívat	přežívat	k5eAaImF	přežívat
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
zahrabáni	zahrabat	k5eAaPmNgMnP	zahrabat
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Střevní	střevní	k2eAgNnSc1d1	střevní
dýchání	dýchání	k1gNnSc1	dýchání
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
sumců	sumec	k1gMnPc2	sumec
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
i	i	k8xC	i
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
sekavcovitých	sekavcovitý	k2eAgInPc2d1	sekavcovitý
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
Cobitidae	Cobitida	k1gFnSc2	Cobitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
žijícího	žijící	k2eAgMnSc2d1	žijící
piskoře	piskoř	k1gMnSc2	piskoř
pruhovaného	pruhovaný	k2eAgMnSc2d1	pruhovaný
(	(	kIx(	(
<g/>
Misgurnus	Misgurnus	k1gInSc1	Misgurnus
fossilis	fossilis	k1gFnSc2	fossilis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
sumci	sumec	k1gMnPc1	sumec
<g/>
,	,	kIx,	,
keříčkovci	keříčkovec	k1gMnPc1	keříčkovec
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
Clariidae	Clariida	k1gFnSc2	Clariida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
další	další	k2eAgInSc4d1	další
typ	typ	k1gInSc4	typ
přídavného	přídavný	k2eAgNnSc2d1	přídavné
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Je	on	k3xPp3gNnSc4	on
jím	jíst	k5eAaImIp1nS	jíst
keříčkovitý	keříčkovitý	k2eAgInSc1d1	keříčkovitý
útvar	útvar	k1gInSc1	útvar
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
z	z	k7c2	z
druhého	druhý	k4xOgMnSc2	druhý
až	až	k8xS	až
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jen	jen	k6eAd1	jen
druhého	druhý	k4xOgMnSc4	druhý
a	a	k8xC	a
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
<g/>
)	)	kIx)	)
žaberního	žaberní	k2eAgInSc2d1	žaberní
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
vybíhající	vybíhající	k2eAgMnSc1d1	vybíhající
dozadu	dozadu	k6eAd1	dozadu
do	do	k7c2	do
protažené	protažený	k2eAgFnSc2d1	protažená
žaberní	žaberní	k2eAgFnSc2d1	žaberní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Keříčkovci	Keříčkovec	k1gMnPc1	Keříčkovec
často	často	k6eAd1	často
opouští	opouštět	k5eAaImIp3nP	opouštět
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
i	i	k9	i
hledají	hledat	k5eAaImIp3nP	hledat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Keříčkovci	Keříčkovec	k1gMnPc1	Keříčkovec
čeledi	čeleď	k1gFnSc2	čeleď
Heteropneustidae	Heteropneustida	k1gFnSc2	Heteropneustida
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
dvě	dva	k4xCgFnPc1	dva
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
vakovité	vakovitý	k2eAgFnPc1d1	vakovitá
trubice	trubice	k1gFnPc1	trubice
vybíhající	vybíhající	k2eAgFnPc1d1	vybíhající
dozadu	dozadu	k6eAd1	dozadu
podél	podél	k7c2	podél
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
oni	onen	k3xDgMnPc1	onen
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
denních	denní	k2eAgFnPc2d1	denní
ryb	ryba	k1gFnPc2	ryba
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
smyslem	smysl	k1gInSc7	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
pro	pro	k7c4	pro
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
detekci	detekce	k1gFnSc4	detekce
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
i	i	k9	i
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
jedinci	jedinec	k1gMnPc7	jedinec
vlastního	vlastní	k2eAgInSc2d1	vlastní
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc1	dva
komorové	komorový	k2eAgFnPc1d1	komorová
oči	oko	k1gNnPc1	oko
bez	bez	k7c2	bez
víček	víčko	k1gNnPc2	víčko
umístěné	umístěný	k2eAgFnSc2d1	umístěná
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezené	omezený	k2eAgFnSc3d1	omezená
viditelnosti	viditelnost	k1gFnSc3	viditelnost
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
rybí	rybí	k2eAgNnSc1d1	rybí
oko	oko	k1gNnSc1	oko
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
zaostřeno	zaostřen	k2eAgNnSc1d1	Zaostřeno
na	na	k7c4	na
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Přeostření	přeostření	k1gNnSc1	přeostření
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
změnou	změna	k1gFnSc7	změna
tvaru	tvar	k1gInSc2	tvar
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přiblížením	přiblížení	k1gNnSc7	přiblížení
celé	celý	k2eAgFnSc2d1	celá
čočky	čočka	k1gFnSc2	čočka
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
sítnici	sítnice	k1gFnSc3	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
vnímají	vnímat	k5eAaImIp3nP	vnímat
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
rozsah	rozsah	k1gInSc4	rozsah
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xS	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
750	[number]	k4	750
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ryb	ryba	k1gFnPc2	ryba
vidí	vidět	k5eAaImIp3nS	vidět
barevně	barevně	k6eAd1	barevně
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
o	o	k7c6	o
kratší	krátký	k2eAgFnSc6d2	kratší
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
ale	ale	k8xC	ale
hůře	zle	k6eAd2	zle
proniká	pronikat	k5eAaImIp3nS	pronikat
vodním	vodní	k2eAgInSc7d1	vodní
sloupcem	sloupec	k1gInSc7	sloupec
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
žijících	žijící	k2eAgInPc2d1	žijící
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
hloubkách	hloubka	k1gFnPc6	hloubka
či	či	k8xC	či
kalných	kalný	k2eAgFnPc6d1	kalná
vodách	voda	k1gFnPc6	voda
nemá	mít	k5eNaImIp3nS	mít
schopnost	schopnost	k1gFnSc1	schopnost
vnímat	vnímat	k5eAaImF	vnímat
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
trvale	trvale	k6eAd1	trvale
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
úplné	úplný	k2eAgFnSc6d1	úplná
tmě	tma	k1gFnSc6	tma
mívají	mívat	k5eAaImIp3nP	mívat
oči	oko	k1gNnPc4	oko
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
redukovány	redukován	k2eAgFnPc1d1	redukována
<g/>
.	.	kIx.	.
</s>
<s>
Čich	čich	k1gInSc1	čich
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
převáděny	převáděn	k2eAgInPc1d1	převáděn
do	do	k7c2	do
čelních	čelní	k2eAgFnPc2d1	čelní
oblastí	oblast	k1gFnPc2	oblast
mozku	mozek	k1gInSc2	mozek
skrze	skrze	k?	skrze
první	první	k4xOgMnSc1	první
hlavový	hlavový	k2eAgInSc1d1	hlavový
nerv	nerv	k1gInSc1	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Čich	čich	k1gInSc1	čich
slouží	sloužit	k5eAaImIp3nS	sloužit
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
chutí	chuť	k1gFnSc7	chuť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
detekce	detekce	k1gFnSc1	detekce
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
udržování	udržování	k1gNnSc1	udržování
hejna	hejno	k1gNnSc2	hejno
<g/>
,	,	kIx,	,
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
či	či	k8xC	či
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
trdliště	trdliště	k1gNnSc2	trdliště
u	u	k7c2	u
tažných	tažný	k2eAgFnPc2d1	tažná
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
lososů	losos	k1gMnPc2	losos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
smysl	smysl	k1gInSc4	smysl
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
chuťových	chuťový	k2eAgInPc2d1	chuťový
pohárků	pohárek	k1gInPc2	pohárek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
informace	informace	k1gFnSc1	informace
přenášena	přenášen	k2eAgFnSc1d1	přenášena
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
sedmým	sedmý	k4xOgNnSc7	sedmý
<g/>
,	,	kIx,	,
devátým	devátý	k4xOgInSc7	devátý
a	a	k8xC	a
desátým	desátý	k4xOgInSc7	desátý
hlavovým	hlavový	k2eAgInSc7d1	hlavový
nervem	nerv	k1gInSc7	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
ovšem	ovšem	k9	ovšem
umístěny	umístit	k5eAaPmNgFnP	umístit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
hltanu	hltan	k1gInSc2	hltan
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
ventrálním	ventrální	k2eAgInSc6d1	ventrální
epitelu	epitel	k1gInSc6	epitel
a	a	k8xC	a
žaberních	žaberní	k2eAgFnPc6d1	žaberní
tyčinkách	tyčinka	k1gFnPc6	tyčinka
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
jako	jako	k8xS	jako
u	u	k7c2	u
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
částech	část	k1gFnPc6	část
vnějšího	vnější	k2eAgInSc2d1	vnější
povrchu	povrch	k1gInSc2	povrch
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pysky	pysk	k1gInPc4	pysk
<g/>
,	,	kIx,	,
vousky	vousek	k1gInPc4	vousek
či	či	k8xC	či
ploutve	ploutev	k1gFnPc4	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
sumců	sumec	k1gMnPc2	sumec
a	a	k8xC	a
kaprovitých	kaprovití	k1gMnPc2	kaprovití
jsou	být	k5eAaImIp3nP	být
chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
rozprostřeny	rozprostřen	k2eAgInPc1d1	rozprostřen
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
povrchu	povrch	k1gInSc6	povrch
těla	tělo	k1gNnSc2	tělo
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vnímat	vnímat	k5eAaImF	vnímat
i	i	k9	i
relativně	relativně	k6eAd1	relativně
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
podněty	podnět	k1gInPc4	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
tak	tak	k9	tak
u	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
funkčně	funkčně	k6eAd1	funkčně
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
čichem	čich	k1gInSc7	čich
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
využívána	využívat	k5eAaPmNgFnS	využívat
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
spojitosti	spojitost	k1gFnSc6	spojitost
s	s	k7c7	s
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
a	a	k8xC	a
příjmem	příjem	k1gInSc7	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
třech	tři	k4xCgInPc2	tři
polokruhovitých	polokruhovitý	k2eAgFnPc2d1	polokruhovitá
chodbiček	chodbička	k1gFnPc2	chodbička
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc2	dva
váčků	váček	k1gInPc2	váček
a	a	k8xC	a
otolitů	otolit	k1gInPc2	otolit
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zjištění	zjištění	k1gNnSc3	zjištění
polohy	poloha	k1gFnSc2	poloha
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
postranní	postranní	k2eAgFnSc1d1	postranní
čára	čára	k1gFnSc1	čára
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
smyslový	smyslový	k2eAgInSc4d1	smyslový
orgán	orgán	k1gInSc4	orgán
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
boku	bok	k1gInSc6	bok
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
kalné	kalný	k2eAgFnSc6d1	kalná
vodě	voda	k1gFnSc6	voda
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hmatové	hmatový	k2eAgInPc4d1	hmatový
vjemy	vjem	k1gInPc4	vjem
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
chromozomálně	chromozomálně	k6eAd1	chromozomálně
i	i	k9	i
hormonálně	hormonálně	k6eAd1	hormonálně
určené	určený	k2eAgNnSc4d1	určené
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
heterogametickým	heterogametický	k2eAgNnSc7d1	heterogametický
pohlavím	pohlaví	k1gNnSc7	pohlaví
samec	samec	k1gMnSc1	samec
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
XY	XY	kA	XY
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
menšiny	menšina	k1gFnSc2	menšina
samice	samice	k1gFnSc1	samice
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
ZW	ZW	kA	ZW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
ryb	ryba	k1gFnPc2	ryba
vydává	vydávat	k5eAaPmIp3nS	vydávat
zvuky	zvuk	k1gInPc4	zvuk
jako	jako	k8xS	jako
komunikační	komunikační	k2eAgInSc4d1	komunikační
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
je	být	k5eAaImIp3nS	být
stridulace	stridulace	k1gFnSc1	stridulace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
gonochoristé	gonochorista	k1gMnPc1	gonochorista
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
klasický	klasický	k2eAgInSc1d1	klasický
i	i	k8xC	i
postupný	postupný	k2eAgInSc1d1	postupný
hermafroditismus	hermafroditismus	k1gInSc1	hermafroditismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc4d1	výrazný
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
je	být	k5eAaImIp3nS	být
pohlaví	pohlaví	k1gNnPc4	pohlaví
obtížně	obtížně	k6eAd1	obtížně
odlišitelné	odlišitelný	k2eAgFnPc1d1	odlišitelná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ryb	ryba	k1gFnPc2	ryba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vnějšímu	vnější	k2eAgNnSc3d1	vnější
oplození	oplození	k1gNnSc3	oplození
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
tření	tření	k1gNnSc3	tření
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
oplození	oplození	k1gNnSc3	oplození
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
typ	typ	k1gInSc4	typ
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
potomstvo	potomstvo	k1gNnSc4	potomstvo
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
asi	asi	k9	asi
u	u	k7c2	u
pětiny	pětina	k1gFnSc2	pětina
známých	známý	k2eAgInPc2d1	známý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Afrofilní	Afrofilní	k2eAgFnPc1d1	Afrofilní
ryby	ryba	k1gFnPc1	ryba
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
staví	stavit	k5eAaImIp3nP	stavit
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
listem	list	k1gInSc7	list
rostliny	rostlina	k1gFnSc2	rostlina
ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
sloupci	sloupec	k1gInSc6	sloupec
pěnová	pěnový	k2eAgNnPc4d1	pěnové
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
velcí	velký	k2eAgMnPc1d1	velký
pancéřníčci	pancéřníček	k1gMnPc1	pancéřníček
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Callichtys	Callichtysa	k1gFnPc2	Callichtysa
<g/>
,	,	kIx,	,
Hoplosternum	Hoplosternum	k1gNnSc1	Hoplosternum
<g/>
,	,	kIx,	,
Dianema	Dianema	k1gNnSc1	Dianema
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
labyrintek	labyrintka	k1gFnPc2	labyrintka
(	(	kIx(	(
<g/>
podřád	podřád	k1gInSc1	podřád
Anabantoidei	Anabantoide	k1gFnSc2	Anabantoide
<g/>
)	)	kIx)	)
a	a	k8xC	a
tetrovitá	tetrovitý	k2eAgFnSc1d1	tetrovitý
ryba	ryba	k1gFnSc1	ryba
Hepsetus	Hepsetus	k1gMnSc1	Hepsetus
odoë	odoë	k?	odoë
<g/>
.	.	kIx.	.
</s>
<s>
Pěnová	pěnový	k2eAgNnPc1d1	pěnové
hnízda	hnízdo	k1gNnPc1	hnízdo
staví	stavit	k5eAaImIp3nP	stavit
obvykle	obvykle	k6eAd1	obvykle
samec	samec	k1gInSc4	samec
z	z	k7c2	z
bublinek	bublinka	k1gFnPc2	bublinka
tvořených	tvořený	k2eAgFnPc2d1	tvořená
ústním	ústní	k2eAgInSc7d1	ústní
sekretem	sekret	k1gInSc7	sekret
a	a	k8xC	a
případně	případně	k6eAd1	případně
úlomků	úlomek	k1gInPc2	úlomek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
sociální	sociální	k2eAgNnSc1d1	sociální
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
uplatňující	uplatňující	k2eAgMnSc1d1	uplatňující
se	se	k3xPyFc4	se
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
partnera	partner	k1gMnSc2	partner
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
populacích	populace	k1gFnPc6	populace
živorodky	živorodka	k1gFnSc2	živorodka
duhové	duhový	k2eAgFnSc2d1	Duhová
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc2	samice
s	s	k7c7	s
geneticky	geneticky	k6eAd1	geneticky
podmíněnou	podmíněný	k2eAgFnSc7d1	podmíněná
preferencí	preference	k1gFnSc7	preference
barevných	barevný	k2eAgInPc2d1	barevný
samců	samec	k1gInPc2	samec
volí	volit	k5eAaImIp3nP	volit
samce	samec	k1gInPc1	samec
nebarevné	barevný	k2eNgInPc1d1	nebarevný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
takovou	takový	k3xDgFnSc4	takový
volbu	volba	k1gFnSc4	volba
spatřily	spatřit	k5eAaPmAgInP	spatřit
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Napodobování	napodobování	k1gNnSc1	napodobování
výběru	výběr	k1gInSc2	výběr
partnera	partner	k1gMnSc4	partner
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
rybích	rybí	k2eAgInPc2d1	rybí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
akváriích	akvárium	k1gNnPc6	akvárium
i	i	k8xC	i
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
získané	získaný	k2eAgFnPc1d1	získaná
preference	preference	k1gFnPc1	preference
výběru	výběr	k1gInSc2	výběr
partnera	partner	k1gMnSc2	partner
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
geneticky	geneticky	k6eAd1	geneticky
podmíněnými	podmíněný	k2eAgFnPc7d1	podmíněná
preferencemi	preference	k1gFnPc7	preference
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
udržovány	udržovat	k5eAaImNgFnP	udržovat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
předávány	předáván	k2eAgFnPc1d1	předávána
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
předmětem	předmět	k1gInSc7	předmět
kulturní	kulturní	k2eAgFnSc2d1	kulturní
dědičnosti	dědičnost	k1gFnSc2	dědičnost
a	a	k8xC	a
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vodního	vodní	k2eAgNnSc2d1	vodní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
dělíme	dělit	k5eAaImIp1nP	dělit
ryby	ryba	k1gFnPc1	ryba
na	na	k7c6	na
<g/>
:	:	kIx,	:
Sladkovodní	sladkovodní	k2eAgFnSc6d1	sladkovodní
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
Brakické	brakický	k2eAgFnSc6d1	brakická
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
brakické	brakický	k2eAgFnSc6d1	brakická
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
sladké	sladký	k2eAgFnSc2d1	sladká
a	a	k8xC	a
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řek	řeka	k1gFnPc2	řeka
<g/>
)	)	kIx)	)
Mořské	mořský	k2eAgInPc1d1	mořský
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
oceány	oceán	k1gInPc1	oceán
<g/>
)	)	kIx)	)
Tažné	tažný	k2eAgFnSc3d1	tažná
–	–	k?	–
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
<g/>
:	:	kIx,	:
Anadromní	Anadromní	k2eAgInSc4d1	Anadromní
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
losos	losos	k1gMnSc1	losos
<g/>
)	)	kIx)	)
Katadromní	Katadromní	k2eAgInSc1d1	Katadromní
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úhoř	úhoř	k1gMnSc1	úhoř
<g/>
)	)	kIx)	)
Polotažné	Polotažný	k2eAgNnSc1d1	Polotažný
–	–	k?	–
během	během	k7c2	během
života	život	k1gInSc2	život
mění	měnit	k5eAaImIp3nS	měnit
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
HOFMANN	HOFMANN	kA	HOFMANN
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Akvaristika	akvaristika	k1gFnSc1	akvaristika
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
chovat	chovat	k5eAaImF	chovat
tropické	tropický	k2eAgFnPc1d1	tropická
ryby	ryba	k1gFnPc1	ryba
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Egem	ego	k1gNnSc7	ego
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7199	[number]	k4	7199
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
HANEL	HANEL	kA	HANEL
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
;	;	kIx,	;
ANDRESKA	ANDRESKA	kA	ANDRESKA
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
evropských	evropský	k2eAgFnPc2d1	Evropská
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
ilustracích	ilustrace	k1gFnPc6	ilustrace
Květoslava	Květoslava	k1gFnSc1	Květoslava
Híska	Híska	k1gFnSc1	Híska
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Artia	Artius	k1gMnSc2	Artius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7442	[number]	k4	7442
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
BARUŠ	BARUŠ	k?	BARUŠ
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
;	;	kIx,	;
OLIVA	Oliva	k1gMnSc1	Oliva
<g/>
,	,	kIx,	,
Ota	Ota	k1gMnSc1	Ota
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Mihulovci	Mihulovec	k1gMnPc1	Mihulovec
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
28	[number]	k4	28
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
BARUŠ	BARUŠ	k?	BARUŠ
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
;	;	kIx,	;
OLIVA	Oliva	k1gMnSc1	Oliva
<g/>
,	,	kIx,	,
Ota	Ota	k1gMnSc1	Ota
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Mihulovci	Mihulovec	k1gMnPc1	Mihulovec
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
28	[number]	k4	28
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
218	[number]	k4	218
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Rybolov	rybolov	k1gInSc4	rybolov
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ryby	ryba	k1gFnSc2	ryba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Ryby	ryba	k1gFnSc2	ryba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ryba	ryba	k1gFnSc1	ryba
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Sladkovodní	sladkovodní	k2eAgFnSc2d1	sladkovodní
a	a	k8xC	a
mořské	mořský	k2eAgFnSc2d1	mořská
ryby	ryba	k1gFnSc2	ryba
Evropy	Evropa	k1gFnSc2	Evropa
Taxonomie	taxonomie	k1gFnSc2	taxonomie
ryb	ryba	k1gFnPc2	ryba
na	na	k7c6	na
BioLibu	BioLib	k1gInSc6	BioLib
Přehled	přehled	k1gInSc1	přehled
systému	systém	k1gInSc2	systém
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
rybovitých	rybovitý	k2eAgMnPc2d1	rybovitý
obratllovců	obratllovec	k1gMnPc2	obratllovec
Atlas	Atlas	k1gInSc1	Atlas
ryb	ryba	k1gFnPc2	ryba
on-line	onin	k1gInSc5	on-lin
Malá	malý	k2eAgFnSc1d1	malá
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
'	'	kIx"	'
<g/>
Naše	náš	k3xOp1gFnPc1	náš
ryby	ryba	k1gFnPc1	ryba
<g/>
'	'	kIx"	'
</s>
