<s>
Tzv.	tzv.	kA	tzv.
postranní	postranní	k2eAgFnSc1d1	postranní
čára	čára	k1gFnSc1	čára
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
smyslový	smyslový	k2eAgInSc4d1	smyslový
orgán	orgán	k1gInSc4	orgán
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
boku	bok	k1gInSc6	bok
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
kalné	kalný	k2eAgFnSc6d1	kalná
vodě	voda	k1gFnSc6	voda
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hmatové	hmatový	k2eAgInPc4d1	hmatový
vjemy	vjem	k1gInPc4	vjem
<g/>
.	.	kIx.	.
</s>
