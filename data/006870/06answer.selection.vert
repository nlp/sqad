<s>
Společnost	společnost	k1gFnSc1	společnost
řízení	řízení	k1gNnSc2	řízení
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
Information	Information	k1gInSc1	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
záznamů	záznam	k1gInPc2	záznam
(	(	kIx(	(
<g/>
Records	Records	k1gInSc1	Records
Managements	Managementsa	k1gFnPc2	Managementsa
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
profesní	profesní	k2eAgFnSc1d1	profesní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
manažery	manažer	k1gMnPc4	manažer
řízení	řízení	k1gNnSc1	řízení
záznamů	záznam	k1gInPc2	záznam
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
