<s>
Společnost	společnost	k1gFnSc1	společnost
řízení	řízení	k1gNnSc2	řízení
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
Information	Information	k1gInSc1	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
záznamů	záznam	k1gInPc2	záznam
(	(	kIx(	(
<g/>
Records	Records	k1gInSc1	Records
Managements	Managementsa	k1gFnPc2	Managementsa
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
profesní	profesní	k2eAgFnSc1d1	profesní
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
manažery	manažer	k1gMnPc4	manažer
řízení	řízení	k1gNnSc1	řízení
záznamů	záznam	k1gInPc2	záznam
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
znalostí	znalost	k1gFnPc2	znalost
a	a	k8xC	a
řádné	řádný	k2eAgFnSc2d1	řádná
správy	správa	k1gFnSc2	správa
záznamů	záznam	k1gInPc2	záznam
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
obchodní	obchodní	k2eAgFnSc2d1	obchodní
činnosti	činnost	k1gFnSc2	činnost
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
nosiče	nosič	k1gInPc4	nosič
záznamů	záznam	k1gInPc2	záznam
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInPc1d1	obchodní
vztahy	vztah	k1gInPc1	vztah
nebo	nebo	k8xC	nebo
kooperace	kooperace	k1gFnSc1	kooperace
mezi	mezi	k7c7	mezi
jednotlivci	jednotlivec	k1gMnPc7	jednotlivec
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
řízením	řízení	k1gNnSc7	řízení
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
profesní	profesní	k2eAgFnSc4d1	profesní
nebo	nebo	k8xC	nebo
organizační	organizační	k2eAgFnSc4d1	organizační
úroveň	úroveň	k1gFnSc4	úroveň
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
Information	Information	k1gInSc4	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
přes	přes	k7c4	přes
1100	[number]	k4	1100
členů	člen	k1gInPc2	člen
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
30	[number]	k4	30
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
zájmové	zájmový	k2eAgFnPc1d1	zájmová
skupiny	skupina	k1gFnPc1	skupina
pro	pro	k7c4	pro
veřejný	veřejný	k2eAgInSc4d1	veřejný
sektor	sektor	k1gInSc4	sektor
a	a	k8xC	a
instituce	instituce	k1gFnSc1	instituce
vyššího	vysoký	k2eAgNnSc2d2	vyšší
a	a	k8xC	a
dalšího	další	k2eAgNnSc2d1	další
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
lokalizované	lokalizovaný	k2eAgFnPc1d1	lokalizovaná
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
organizuje	organizovat	k5eAaBmIp3nS	organizovat
setkání	setkání	k1gNnSc4	setkání
a	a	k8xC	a
výroční	výroční	k2eAgFnSc2d1	výroční
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
časopis	časopis	k1gInSc1	časopis
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Bulletin	bulletin	k1gInSc4	bulletin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
komentáře	komentář	k1gInPc4	komentář
<g/>
,	,	kIx,	,
analýzy	analýza	k1gFnPc4	analýza
<g/>
,	,	kIx,	,
případové	případový	k2eAgFnPc4d1	Případová
studie	studie	k1gFnPc4	studie
a	a	k8xC	a
zprávy	zpráva	k1gFnPc4	zpráva
u	u	k7c2	u
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
scény	scéna	k1gFnSc2	scéna
řízení	řízení	k1gNnSc2	řízení
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
společnost	společnost	k1gFnSc1	společnost
vydává	vydávat	k5eAaImIp3nS	vydávat
informační	informační	k2eAgFnPc4d1	informační
příručky	příručka	k1gFnPc4	příručka
o	o	k7c6	o
otázkách	otázka	k1gFnPc6	otázka
uchování	uchování	k1gNnSc2	uchování
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pořádá	pořádat	k5eAaImIp3nS	pořádat
a	a	k8xC	a
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c4	o
školení	školení	k1gNnPc4	školení
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
změnila	změnit	k5eAaPmAgFnS	změnit
společnost	společnost	k1gFnSc1	společnost
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Information	Information	k1gInSc4	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
její	její	k3xOp3gFnSc4	její
partnerská	partnerský	k2eAgFnSc1d1	partnerská
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
Information	Information	k1gInSc1	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
–	–	k?	–
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
Information	Information	k1gInSc1	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
Information	Information	k1gInSc1	Information
and	and	k?	and
Records	Records	k1gInSc1	Records
Management	management	k1gInSc1	management
Society	societa	k1gFnSc2	societa
-	-	kIx~	-
Czech	Czech	k1gMnSc1	Czech
Republic	Republice	k1gFnPc2	Republice
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
záznamů	záznam	k1gInPc2	záznam
–	–	k?	–
Skupina	skupina	k1gFnSc1	skupina
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
</s>
