<s>
Zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
ptačí	ptačí	k2eAgFnSc2d1	ptačí
kostry	kostra	k1gFnSc2	kostra
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
bezzubý	bezzubý	k2eAgInSc4d1	bezzubý
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
horního	horní	k2eAgInSc2d1	horní
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
pletenec	pletenec	k1gInSc1	pletenec
hrudní	hrudní	k2eAgFnSc2d1	hrudní
končetiny	končetina	k1gFnSc2	končetina
přeměněné	přeměněný	k2eAgFnSc2d1	přeměněná
v	v	k7c4	v
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
osifikace	osifikace	k1gFnPc4	osifikace
hrudních	hrudní	k2eAgInPc2d1	hrudní
úseků	úsek	k1gInPc2	úsek
žeber	žebro	k1gNnPc2	žebro
a	a	k8xC	a
zpevnění	zpevnění	k1gNnSc2	zpevnění
hrudníku	hrudník	k1gInSc2	hrudník
pomocí	pomocí	k7c2	pomocí
háčkovitých	háčkovitý	k2eAgInPc2d1	háčkovitý
výběžků	výběžek	k1gInPc2	výběžek
<g/>
.	.	kIx.	.
</s>
