<s>
Slepičiny	slepičina	k1gFnPc1	slepičina
(	(	kIx(	(
<g/>
též	též	k9	též
slepičince	slepičinec	k1gInPc1	slepičinec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
typem	typ	k1gInSc7	typ
organického	organický	k2eAgNnSc2d1	organické
hnojiva	hnojivo	k1gNnSc2	hnojivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
fermentací	fermentace	k1gFnSc7	fermentace
slepičích	slepičí	k2eAgInPc2d1	slepičí
výkalů	výkal	k1gInPc2	výkal
<g/>
.	.	kIx.	.
</s>
