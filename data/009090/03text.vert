<p>
<s>
Slepičiny	slepičina	k1gFnPc1	slepičina
(	(	kIx(	(
<g/>
též	též	k9	též
slepičince	slepičinec	k1gInPc1	slepičinec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
typem	typ	k1gInSc7	typ
organického	organický	k2eAgNnSc2d1	organické
hnojiva	hnojivo	k1gNnSc2	hnojivo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
fermentací	fermentace	k1gFnSc7	fermentace
slepičích	slepičí	k2eAgInPc2d1	slepičí
výkalů	výkal	k1gInPc2	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Slepičiny	slepičina	k1gFnPc1	slepičina
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
připravují	připravovat	k5eAaImIp3nP	připravovat
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
výkaly	výkal	k1gInPc1	výkal
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
steliva	stelivo	k1gNnSc2	stelivo
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
ředí	ředit	k5eAaImIp3nS	ředit
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
otevřena	otevřít	k5eAaPmNgFnS	otevřít
pro	pro	k7c4	pro
vnik	vnik	k1gInSc4	vnik
vody	voda	k1gFnSc2	voda
dešťové	dešťový	k2eAgFnSc2d1	dešťová
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hustou	hustý	k2eAgFnSc4d1	hustá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
vůně	vůně	k1gFnPc1	vůně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slepičiny	slepičina	k1gFnPc1	slepičina
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
hnojení	hnojení	k1gNnSc3	hnojení
okrasných	okrasný	k2eAgFnPc2d1	okrasná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vyzrálé	vyzrálý	k2eAgFnPc1d1	vyzrálá
slepičiny	slepičina	k1gFnPc1	slepičina
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
ředit	ředit	k5eAaImF	ředit
vodou	voda	k1gFnSc7	voda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
popálení	popálení	k1gNnSc3	popálení
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
na	na	k7c6	na
statcích	statek	k1gInPc6	statek
a	a	k8xC	a
chalupách	chalupa	k1gFnPc6	chalupa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoliv	nikoliv	k9	nikoliv
ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
<g/>
.	.	kIx.	.
</s>
</p>
