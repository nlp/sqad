<p>
<s>
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
Saskačevan	Saskačevan	k1gMnSc1	Saskačevan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadská	kanadský	k2eAgFnSc1d1	kanadská
provincie	provincie	k1gFnSc1	provincie
<g/>
,	,	kIx,	,
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
kanadských	kanadský	k2eAgFnPc2d1	kanadská
prérijních	prérijní	k2eAgFnPc2d1	prérijní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Albertou	Alberta	k1gFnSc7	Alberta
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Severozápadními	severozápadní	k2eAgNnPc7d1	severozápadní
teritorii	teritorium	k1gNnPc7	teritorium
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Manitobou	Manitoba	k1gFnSc7	Manitoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
státy	stát	k1gInPc1	stát
USA	USA	kA	USA
Montanou	Montana	k1gFnSc7	Montana
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
Dakotou	Dakota	k1gFnSc7	Dakota
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
651	[number]	k4	651
900	[number]	k4	900
km2	km2	k4	km2
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
milión	milión	k4xCgInSc4	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
988	[number]	k4	988
980	[number]	k4	980
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dubnu	duben	k1gInSc3	duben
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
1	[number]	k4	1
053	[number]	k4	053
960	[number]	k4	960
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
usedlíci	usedlík	k1gMnPc1	usedlík
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
polovině	polovina	k1gFnSc6	polovina
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
250	[number]	k4	250
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
městě	město	k1gNnSc6	město
provincie	provincie	k1gFnSc2	provincie
Saskatoonu	Saskatoon	k1gInSc2	Saskatoon
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zhruba	zhruba	k6eAd1	zhruba
210	[number]	k4	210
000	[number]	k4	000
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
provincie	provincie	k1gFnSc2	provincie
Regině	Regina	k1gFnSc3	Regina
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
velkými	velký	k2eAgMnPc7d1	velký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Prince	princa	k1gFnSc3	princa
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
Moose	Moose	k1gFnSc1	Moose
Jaw	jawa	k1gFnPc2	jawa
<g/>
,	,	kIx,	,
Yorkton	Yorkton	k1gInSc1	Yorkton
<g/>
,	,	kIx,	,
Swift	Swift	k1gMnSc1	Swift
Current	Current	k1gMnSc1	Current
a	a	k8xC	a
North	North	k1gMnSc1	North
Battleford	Battleford	k1gMnSc1	Battleford
<g/>
.	.	kIx.	.
<g/>
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
prozkoumáván	prozkoumáván	k2eAgInSc1d1	prozkoumáván
Evropany	Evropan	k1gMnPc7	Evropan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
a	a	k8xC	a
obydlen	obydlen	k2eAgInSc1d1	obydlen
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
<g/>
,	,	kIx,	,
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
obýván	obývat	k5eAaImNgInS	obývat
různými	různý	k2eAgFnPc7d1	různá
domorodými	domorodý	k2eAgFnPc7d1	domorodá
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bělochů	běloch	k1gMnPc2	běloch
představují	představovat	k5eAaImIp3nP	představovat
významnou	významný	k2eAgFnSc4d1	významná
etnickou	etnický	k2eAgFnSc4d1	etnická
skupinu	skupina	k1gFnSc4	skupina
indiáni	indián	k1gMnPc1	indián
(	(	kIx(	(
<g/>
14,8	[number]	k4	14,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Métisové	Métisový	k2eAgInPc4d1	Métisový
a	a	k8xC	a
První	první	k4xOgInPc4	první
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Provincií	provincie	k1gFnSc7	provincie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
premiér	premiér	k1gMnSc1	premiér
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
je	být	k5eAaImIp3nS	být
Brad	brada	k1gFnPc2	brada
Wall	Wall	k1gMnSc1	Wall
a	a	k8xC	a
guvernérporučík	guvernérporučík	k1gMnSc1	guvernérporučík
Saskatchewanu	Saskatchewana	k1gFnSc4	Saskatchewana
je	být	k5eAaImIp3nS	být
Gordon	Gordon	k1gMnSc1	Gordon
Barnhart	Barnhart	k1gInSc1	Barnhart
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
odvětvími	odvětví	k1gNnPc7	odvětví
Saskatchewanu	Saskatchewan	k1gInSc3	Saskatchewan
jsou	být	k5eAaImIp3nP	být
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
energetika	energetika	k1gFnSc1	energetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
provincie	provincie	k1gFnSc2	provincie
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Saskatchewan	Saskatchewany	k1gInPc2	Saskatchewany
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
nazývána	nazýván	k2eAgFnSc1d1	nazývána
kisiskā	kisiskā	k?	kisiskā
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bystře	bystro	k6eAd1	bystro
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Krí	Krí	k1gFnSc2	Krí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
vypadá	vypadat	k5eAaPmIp3nS	vypadat
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
téměř	téměř	k6eAd1	téměř
jako	jako	k9	jako
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velikosti	velikost	k1gFnSc3	velikost
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
úseky	úsek	k1gInPc4	úsek
hranice	hranice	k1gFnSc2	hranice
stanovené	stanovený	k2eAgInPc4d1	stanovený
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
(	(	kIx(	(
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
zakřiveny	zakřiven	k2eAgFnPc1d1	zakřivena
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
částečně	částečně	k6eAd1	částečně
přihnutá	přihnutý	k2eAgFnSc1d1	přihnutá
a	a	k8xC	a
odkloněná	odkloněný	k2eAgFnSc1d1	odkloněná
od	od	k7c2	od
linie	linie	k1gFnSc2	linie
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
prvními	první	k4xOgInPc7	první
průzkumníky	průzkumník	k1gMnPc7	průzkumník
byla	být	k5eAaImAgFnS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
korekční	korekční	k2eAgFnSc1d1	korekční
linie	linie	k1gFnSc1	linie
ještě	ještě	k9	ještě
před	před	k7c7	před
usedlostním	usedlostní	k2eAgInSc7d1	usedlostní
plánem	plán	k1gInSc7	plán
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Albertou	Alberta	k1gFnSc7	Alberta
je	být	k5eAaImIp3nS	být
Saskatchewan	Saskatchewan	k1gMnSc1	Saskatchewan
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
vnitrozemských	vnitrozemský	k2eAgFnPc2d1	vnitrozemská
provincií	provincie	k1gFnPc2	provincie
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Převládající	převládající	k2eAgFnSc1d1	převládající
většina	většina	k1gFnSc1	většina
Saskatchewanského	Saskatchewanský	k2eAgNnSc2d1	Saskatchewanský
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
obývá	obývat	k5eAaImIp3nS	obývat
jižní	jižní	k2eAgFnSc1d1	jižní
třetinu	třetina	k1gFnSc4	třetina
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
53	[number]	k4	53
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
přírodními	přírodní	k2eAgFnPc7d1	přírodní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
:	:	kIx,	:
Kanadským	kanadský	k2eAgInSc7d1	kanadský
štítem	štít	k1gInSc7	štít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
planinami	planina	k1gFnPc7	planina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
většinou	většinou	k6eAd1	většinou
boreálním	boreální	k2eAgInSc7d1	boreální
lesem	les	k1gInSc7	les
až	až	k9	až
na	na	k7c4	na
písečné	písečný	k2eAgInPc4d1	písečný
přesypy	přesyp	k1gInPc4	přesyp
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Athabaska	Athabasko	k1gNnSc2	Athabasko
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc1d3	veliký
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
duny	duna	k1gFnPc1	duna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
severně	severně	k6eAd1	severně
od	od	k7c2	od
58	[number]	k4	58
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
<g/>
,	,	kIx,	,
přilehlé	přilehlý	k2eAgNnSc1d1	přilehlé
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
břehu	břeh	k1gInSc3	břeh
jezera	jezero	k1gNnSc2	jezero
Athabaska	Athabask	k1gInSc2	Athabask
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
další	další	k2eAgInSc1d1	další
areál	areál	k1gInSc1	areál
plný	plný	k2eAgInSc1d1	plný
dun	duna	k1gFnPc2	duna
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Velké	velký	k2eAgInPc1d1	velký
písečné	písečný	k2eAgInPc1d1	písečný
kopce	kopec	k1gInPc1	kopec
<g/>
"	"	kIx"	"
pokrývající	pokrývající	k2eAgMnSc1d1	pokrývající
přes	přes	k7c4	přes
300	[number]	k4	300
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Cypřišové	cypřišový	k2eAgInPc1d1	cypřišový
kopce	kopec	k1gInPc1	kopec
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
rohu	roh	k1gInSc6	roh
Saskatchewanu	Saskatchewana	k1gFnSc4	Saskatchewana
a	a	k8xC	a
Killdeer	Killdeer	k1gInSc4	Killdeer
Badlands	Badlands	k1gInSc1	Badlands
(	(	kIx(	(
<g/>
Grasslands	Grasslands	k1gInSc1	Grasslands
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnPc4d1	ledová
nezaledněny	zaledněn	k2eNgFnPc4d1	zaledněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
provincie	provincie	k1gFnSc2	provincie
má	mít	k5eAaImIp3nS	mít
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
1468	[number]	k4	1468
m	m	kA	m
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Cypřišových	cypřišový	k2eAgInPc6d1	cypřišový
kopcích	kopec	k1gInPc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bod	bod	k1gInSc1	bod
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
mezi	mezi	k7c7	mezi
Skalistými	skalistý	k2eAgFnPc7d1	skalistý
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Québecem	Québeec	k1gInSc7	Québeec
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Athabaska	Athabasek	k1gMnSc2	Athabasek
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
213	[number]	k4	213
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Provincie	provincie	k1gFnSc1	provincie
má	mít	k5eAaImIp3nS	mít
čtrnáct	čtrnáct	k4xCc4	čtrnáct
hlavních	hlavní	k2eAgFnPc2d1	hlavní
odtokových	odtokový	k2eAgFnPc2d1	odtoková
oblastí	oblast	k1gFnPc2	oblast
různých	různý	k2eAgFnPc2d1	různá
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
povodí	povodí	k1gNnSc1	povodí
vlévajících	vlévající	k2eAgInPc2d1	vlévající
se	se	k3xPyFc4	se
do	do	k7c2	do
Arktického	arktický	k2eAgInSc2d1	arktický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
slunečných	slunečný	k2eAgFnPc2d1	Slunečná
hodin	hodina	k1gFnPc2	hodina
než	než	k8xS	než
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
jiná	jiný	k2eAgFnSc1d1	jiná
kanadská	kanadský	k2eAgFnSc1d1	kanadská
provincie	provincie	k1gFnSc1	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
provincie	provincie	k1gFnSc1	provincie
leží	ležet	k5eAaImIp3nS	ležet
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
význačnějších	význačný	k2eAgFnPc2d2	význačnější
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
teplou	teplý	k2eAgFnSc4d1	teplá
variantu	varianta	k1gFnSc4	varianta
humidního	humidní	k2eAgNnSc2d1	humidní
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
Köppnův	Köppnův	k2eAgInSc1d1	Köppnův
typ	typ	k1gInSc1	typ
Dfb	Dfb	k1gFnSc2	Dfb
<g/>
)	)	kIx)	)
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
<g/>
,	,	kIx,	,
většině	většina	k1gFnSc6	většina
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Cypřišových	cypřišový	k2eAgInPc6d1	cypřišový
kopcích	kopec	k1gInPc6	kopec
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
podnebí	podnebí	k1gNnSc2	podnebí
postupně	postupně	k6eAd1	postupně
vysušuje	vysušovat	k5eAaImIp3nS	vysušovat
až	až	k6eAd1	až
semiaridnímu	semiaridní	k2eAgNnSc3d1	semiaridní
stepnímu	stepní	k2eAgNnSc3d1	stepní
klimatu	klima	k1gNnSc3	klima
(	(	kIx(	(
<g/>
Köppnův	Köppnův	k2eAgInSc1d1	Köppnův
typ	typ	k1gInSc1	typ
BSk	BSk	k1gFnSc2	BSk
<g/>
)	)	kIx)	)
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnPc1d1	severní
části	část	k1gFnPc1	část
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
–	–	k?	–
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
La	la	k1gNnSc2	la
Ronge	Rong	k1gFnSc2	Rong
na	na	k7c4	na
sever	sever	k1gInSc4	sever
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
subarktické	subarktický	k2eAgNnSc1d1	subarktické
klima	klima	k1gNnSc1	klima
(	(	kIx(	(
<g/>
Köppnův	Köppnův	k2eAgInSc1d1	Köppnův
typ	typ	k1gInSc1	typ
Dfc	Dfc	k1gFnSc2	Dfc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
horká	horký	k2eAgFnSc1d1	horká
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
přes	přes	k7c4	přes
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vlhkost	vlhkost	k1gFnSc1	vlhkost
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihozápadu	jihozápad	k1gInSc3	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Teplé	Teplé	k2eAgInPc1d1	Teplé
jižní	jižní	k2eAgInPc1d1	jižní
větry	vítr	k1gInPc1	vítr
vanou	vanout	k5eAaImIp3nP	vanout
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zimy	zima	k1gFnPc1	zima
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mrazivé	mrazivý	k2eAgFnPc1d1	mrazivá
až	až	k8xS	až
do	do	k7c2	do
morku	morek	k1gInSc2	morek
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
s	s	k7c7	s
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
teplotami	teplota	k1gFnPc7	teplota
nepřesahujícími	přesahující	k2eNgNnPc7d1	nepřesahující
-17	-17	k4	-17
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Teplé	Teplé	k2eAgInPc7d1	Teplé
větry	vítr	k1gInPc7	vítr
chinook	chinook	k1gInSc1	chinook
často	často	k6eAd1	často
vanou	vanout	k5eAaImIp3nP	vanout
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
přinášejíce	přinášet	k5eAaImSgMnP	přinášet
období	období	k1gNnSc6	období
mírnějšího	mírný	k2eAgNnSc2d2	mírnější
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInPc1d1	roční
srážkové	srážkový	k2eAgInPc1d1	srážkový
průměry	průměr	k1gInPc1	průměr
jdou	jít	k5eAaImIp3nP	jít
od	od	k7c2	od
300	[number]	k4	300
do	do	k7c2	do
450	[number]	k4	450
milimetrů	milimetr	k1gInPc2	milimetr
napříč	napříč	k7c7	napříč
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
objemy	objem	k1gInPc7	objem
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
<g/>
Historicky	historicky	k6eAd1	historicky
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
teploty	teplota	k1gFnSc2	teplota
naměřené	naměřený	k2eAgFnSc2d1	naměřená
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
zde	zde	k6eAd1	zde
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
až	až	k9	až
45	[number]	k4	45
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
Midale	Midala	k1gFnSc6	Midala
a	a	k8xC	a
v	v	k7c6	v
Yellow	Yellow	k1gFnSc6	Yellow
Grassu	Grass	k1gInSc2	Grass
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgMnSc1d3	nejnižší
vůbec	vůbec	k9	vůbec
kdy	kdy	k6eAd1	kdy
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
Prince	princa	k1gFnSc6	princa
Albertu	Alberta	k1gFnSc4	Alberta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Saskatoonu	Saskatoon	k1gInSc2	Saskatoon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
správní	správní	k2eAgFnSc7d1	správní
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
Saskatchewanu	Saskatchewan	k1gInSc6	Saskatchewan
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
(	(	kIx(	(
<g/>
community	communita	k1gFnPc1	communita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
třídí	třídit	k5eAaImIp3nP	třídit
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
správy	správa	k1gFnSc2	správa
na	na	k7c4	na
sdružené	sdružený	k2eAgFnPc4d1	sdružená
do	do	k7c2	do
místních	místní	k2eAgFnPc2d1	místní
rad	rada	k1gFnPc2	rada
(	(	kIx(	(
<g/>
incorporated	incorporated	k1gMnSc1	incorporated
municipalities	municipalities	k1gMnSc1	municipalities
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesdružené	sdružený	k2eNgFnPc1d1	sdružený
(	(	kIx(	(
<g/>
unincorporated	unincorporated	k1gMnSc1	unincorporated
communities	communities	k1gMnSc1	communities
<g/>
)	)	kIx)	)
a	a	k8xC	a
původní	původní	k2eAgInPc4d1	původní
národy	národ	k1gInPc4	národ
(	(	kIx(	(
<g/>
First	First	k1gFnSc1	First
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místních	místní	k2eAgFnPc2d1	místní
rad	rada	k1gFnPc2	rada
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
786	[number]	k4	786
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
má	mít	k5eAaImIp3nS	mít
Severosaskatchewanská	Severosaskatchewanský	k2eAgFnSc1d1	Severosaskatchewanský
správní	správní	k2eAgFnSc1d1	správní
oblast	oblast	k1gFnSc1	oblast
(	(	kIx(	(
<g/>
Northern	Northern	k1gInSc1	Northern
Saskatchewan	Saskatchewana	k1gFnPc2	Saskatchewana
Administration	Administration	k1gInSc1	Administration
District	District	k1gMnSc1	District
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jehož	k3xOyRp3gFnPc4	jehož
obce	obec	k1gFnPc4	obec
platí	platit	k5eAaImIp3nS	platit
jiné	jiný	k2eAgFnPc4d1	jiná
definice	definice	k1gFnPc4	definice
než	než	k8xS	než
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
statistické	statistický	k2eAgInPc4d1	statistický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
18	[number]	k4	18
statistických	statistický	k2eAgInPc2d1	statistický
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
census	census	k1gInSc1	census
division	division	k1gInSc1	division
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
číslovány	číslovat	k5eAaImNgInP	číslovat
od	od	k7c2	od
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
jejich	jejich	k3xOp3gNnPc4	jejich
čísla	číslo	k1gNnPc4	číslo
a	a	k8xC	a
příslušná	příslušný	k2eAgNnPc4d1	příslušné
nejlidnatější	lidnatý	k2eAgNnPc4d3	nejlidnatější
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
Estevan	Estevan	k1gMnSc1	Estevan
</s>
</p>
<p>
<s>
2	[number]	k4	2
Weyburn	Weyburn	k1gNnSc1	Weyburn
</s>
</p>
<p>
<s>
3	[number]	k4	3
Assiniboia	Assiniboia	k1gFnSc1	Assiniboia
</s>
</p>
<p>
<s>
4	[number]	k4	4
Maple	Maple	k1gNnSc1	Maple
Creek	Creky	k1gFnPc2	Creky
</s>
</p>
<p>
<s>
5	[number]	k4	5
Melville	Melville	k1gNnSc1	Melville
</s>
</p>
<p>
<s>
6	[number]	k4	6
Regina	Regina	k1gFnSc1	Regina
</s>
</p>
<p>
<s>
7	[number]	k4	7
Moose	Moose	k1gFnSc1	Moose
Jaw	jawa	k1gFnPc2	jawa
</s>
</p>
<p>
<s>
8	[number]	k4	8
Swift	Swift	k1gMnSc1	Swift
Current	Current	k1gMnSc1	Current
</s>
</p>
<p>
<s>
9	[number]	k4	9
Yorkton	Yorkton	k1gInSc1	Yorkton
</s>
</p>
<p>
<s>
10	[number]	k4	10
Wynyard	Wynyard	k1gInSc1	Wynyard
</s>
</p>
<p>
<s>
11	[number]	k4	11
Saskatoon	Saskatoon	k1gNnSc1	Saskatoon
</s>
</p>
<p>
<s>
12	[number]	k4	12
Battleford	Battleford	k1gInSc1	Battleford
</s>
</p>
<p>
<s>
13	[number]	k4	13
Kindersley	Kindersleum	k1gNnPc7	Kindersleum
</s>
</p>
<p>
<s>
14	[number]	k4	14
Melfort	Melfort	k1gInSc1	Melfort
</s>
</p>
<p>
<s>
15	[number]	k4	15
Prince	princ	k1gMnSc2	princ
Albert	Albert	k1gMnSc1	Albert
</s>
</p>
<p>
<s>
16	[number]	k4	16
North	North	k1gMnSc1	North
Battleford	Battleford	k1gMnSc1	Battleford
</s>
</p>
<p>
<s>
17	[number]	k4	17
Lloydminster	Lloydminster	k1gInSc1	Lloydminster
</s>
</p>
<p>
<s>
18	[number]	k4	18
La	la	k1gNnSc1	la
Ronge	Rong	k1gInSc2	Rong
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Saskatchewan	Saskatchewana	k1gFnPc2	Saskatchewana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Saskatchewan	Saskatchewana	k1gFnPc2	Saskatchewana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Saskatchewan	Saskatchewany	k1gInPc2	Saskatchewany
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Saskačevan	Saskačevan	k1gMnSc1	Saskačevan
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Municipalities	Municipalities	k1gMnSc1	Municipalities
Act	Act	k1gMnSc1	Act
</s>
</p>
