<s>
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gMnSc1	Boletus
satanas	satanas	k1gMnSc1	satanas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
houba	houba	k1gFnSc1	houba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
především	především	k9	především
nížiny	nížina	k1gFnPc4	nížina
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
)	)	kIx)	)
na	na	k7c6	na
vápenitém	vápenitý	k2eAgInSc6d1	vápenitý
podkladě	podklad	k1gInSc6	podklad
pod	pod	k7c7	pod
listnatými	listnatý	k2eAgInPc7d1	listnatý
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
