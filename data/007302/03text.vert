<s>
Otrava	otrava	k1gFnSc1	otrava
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gMnSc7	satan
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
zřídkavým	zřídkavý	k2eAgMnPc3d1	zřídkavý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obávaným	obávaný	k2eAgFnPc3d1	obávaná
houbovým	houbový	k2eAgFnPc3d1	houbová
otravám	otrava	k1gFnPc3	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
průběh	průběh	k1gInSc1	průběh
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
silným	silný	k2eAgNnSc7d1	silné
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
provází	provázet	k5eAaImIp3nS	provázet
dehydratace	dehydratace	k1gFnSc1	dehydratace
a	a	k8xC	a
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
organismu	organismus	k1gInSc2	organismus
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
nekončí	končit	k5eNaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jedovatost	jedovatost	k1gFnSc1	jedovatost
houby	houba	k1gFnSc2	houba
byla	být	k5eAaImAgFnS	být
zdokumentována	zdokumentovat	k5eAaPmNgFnS	zdokumentovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Haralda	Harald	k1gMnSc2	Harald
Othmara	Othmar	k1gMnSc2	Othmar
Lenze	Lenze	k1gFnSc2	Lenze
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
satan	satan	k1gInSc4	satan
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
odborně	odborně	k6eAd1	odborně
popsal	popsat	k5eAaPmAgMnS	popsat
(	(	kIx(	(
<g/>
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgFnPc7	tři
osobami	osoba	k1gFnPc7	osoba
otrávil	otrávit	k5eAaPmAgInS	otrávit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
<g/>
.	.	kIx.	.
</s>
<s>
Hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gMnSc1	Boletus
satanas	satanas	k1gMnSc1	satanas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
houba	houba	k1gFnSc1	houba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
především	především	k9	především
nížiny	nížina	k1gFnPc4	nížina
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
)	)	kIx)	)
na	na	k7c6	na
vápenitém	vápenitý	k2eAgInSc6d1	vápenitý
podkladě	podklad	k1gInSc6	podklad
pod	pod	k7c7	pod
listnatými	listnatý	k2eAgInPc7d1	listnatý
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
hřib	hřib	k1gInSc4	hřib
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gInSc4	on
poznat	poznat	k5eAaPmF	poznat
podle	podle	k7c2	podle
bílého	bílý	k2eAgInSc2d1	bílý
(	(	kIx(	(
<g/>
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
světlého	světlý	k2eAgInSc2d1	světlý
<g/>
)	)	kIx)	)
klobouku	klobouk	k1gInSc2	klobouk
<g/>
,	,	kIx,	,
červených	červený	k2eAgInPc2d1	červený
pórů	pór	k1gInPc2	pór
a	a	k8xC	a
zavalitého	zavalitý	k2eAgInSc2d1	zavalitý
třeně	třeň	k1gInSc2	třeň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
červeně	červeně	k6eAd1	červeně
nebo	nebo	k8xC	nebo
červenožlutě	červenožlutě	k6eAd1	červenožlutě
zbarven	zbarven	k2eAgInSc1d1	zbarven
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc4	jehož
povrch	povrch	k1gInSc4	povrch
kryje	krýt	k5eAaImIp3nS	krýt
červená	červený	k2eAgFnSc1d1	červená
síťka	síťka	k1gFnSc1	síťka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
příbuzných	příbuzný	k1gMnPc2	příbuzný
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
zasyrova	zasyrova	k6eAd1	zasyrova
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
<g/>
)	)	kIx)	)
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hřib	hřib	k1gInSc4	hřib
nachový	nachový	k2eAgInSc4d1	nachový
nebo	nebo	k8xC	nebo
hřib	hřib	k1gInSc4	hřib
Le	Le	k1gFnSc2	Le
Galové	Galová	k1gFnSc2	Galová
jej	on	k3xPp3gMnSc4	on
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
celková	celkový	k2eAgFnSc1d1	celková
mohutnost	mohutnost	k1gFnSc1	mohutnost
a	a	k8xC	a
typický	typický	k2eAgInSc1d1	typický
zápach	zápach	k1gInSc1	zápach
starých	starý	k2eAgFnPc2d1	stará
plodnic	plodnice	k1gFnPc2	plodnice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
popisován	popisován	k2eAgInSc1d1	popisován
jako	jako	k8xC	jako
hnilobný	hnilobný	k2eAgInSc1d1	hnilobný
a	a	k8xC	a
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
mršině	mršina	k1gFnSc3	mršina
<g/>
,	,	kIx,	,
zpoceným	zpocený	k2eAgFnPc3d1	zpocená
nohám	noha	k1gFnPc3	noha
<g/>
,	,	kIx,	,
hnijící	hnijící	k2eAgFnSc3d1	hnijící
cibuli	cibule	k1gFnSc3	cibule
<g/>
,	,	kIx,	,
zkaženému	zkažený	k2eAgNnSc3d1	zkažené
masu	maso	k1gNnSc3	maso
nebo	nebo	k8xC	nebo
zkaženému	zkažený	k2eAgNnSc3d1	zkažené
zelí	zelí	k1gNnSc3	zelí
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
plodnice	plodnice	k1gFnPc1	plodnice
nepáchnou	páchnout	k5eNaImIp3nP	páchnout
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
vůně	vůně	k1gFnSc1	vůně
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
houbová	houbový	k2eAgFnSc1d1	houbová
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kořenitá	kořenitý	k2eAgFnSc1d1	kořenitá
a	a	k8xC	a
připomínat	připomínat	k5eAaImF	připomínat
pestřec	pestřec	k1gInSc4	pestřec
<g/>
.	.	kIx.	.
</s>
<s>
Satan	Satan	k1gMnSc1	Satan
není	být	k5eNaImIp3nS	být
hořký	hořký	k2eAgMnSc1d1	hořký
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
mírná	mírný	k2eAgFnSc1d1	mírná
<g/>
,	,	kIx,	,
příjemná	příjemný	k2eAgFnSc1d1	příjemná
<g/>
,	,	kIx,	,
popisována	popisován	k2eAgFnSc1d1	popisována
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
jako	jako	k9	jako
nasládlá	nasládlý	k2eAgFnSc1d1	nasládlá
až	až	k6eAd1	až
oříšková	oříškový	k2eAgFnSc1d1	oříšková
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
starých	starý	k2eAgInPc2d1	starý
exemplářů	exemplář	k1gInPc2	exemplář
je	být	k5eAaImIp3nS	být
nepříjemná	příjemný	k2eNgFnSc1d1	nepříjemná
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
nepravým	nepravý	k1gMnPc3	nepravý
(	(	kIx(	(
<g/>
psychosomatickým	psychosomatický	k2eAgFnPc3d1	psychosomatická
<g/>
)	)	kIx)	)
otravám	otrava	k1gFnPc3	otrava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
postižený	postižený	k1gMnSc1	postižený
pozřel	pozřít	k5eAaPmAgMnS	pozřít
houbu	houba	k1gFnSc4	houba
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
netypických	typický	k2eNgFnPc2d1	netypická
chuťových	chuťový	k2eAgFnPc2d1	chuťová
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
hořkost	hořkost	k1gFnSc1	hořkost
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jedovatého	jedovatý	k2eAgMnSc4d1	jedovatý
satana	satan	k1gMnSc4	satan
<g/>
,	,	kIx,	,
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
otravu	otrava	k1gFnSc4	otrava
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gInSc7	satan
a	a	k8xC	a
po	po	k7c6	po
ošetření	ošetření	k1gNnSc6	ošetření
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
zpět	zpět	k6eAd1	zpět
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
jde	jít	k5eAaImIp3nS	jít
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
hřib	hřib	k1gInSc4	hřib
žlučový	žlučový	k2eAgInSc4d1	žlučový
(	(	kIx(	(
<g/>
Tylophyllus	Tylophyllus	k1gInSc1	Tylophyllus
felleus	felleus	k1gInSc1	felleus
<g/>
)	)	kIx)	)
lidově	lidově	k6eAd1	lidově
zvaný	zvaný	k2eAgInSc1d1	zvaný
hořčák	hořčák	k1gInSc1	hořčák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
řada	řada	k1gFnSc1	řada
houbařů	houbař	k1gMnPc2	houbař
mylně	mylně	k6eAd1	mylně
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
hřib	hřib	k1gInSc4	hřib
satan	satan	k1gMnSc1	satan
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
záměn	záměna	k1gFnPc2	záměna
připadají	připadat	k5eAaImIp3nP	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k6eAd1	už
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vzhledu	vzhled	k1gInSc2	vzhled
nebo	nebo	k8xC	nebo
lidových	lidový	k2eAgFnPc2d1	lidová
pověr	pověra	k1gFnPc2	pověra
<g/>
)	)	kIx)	)
tyto	tento	k3xDgFnPc1	tento
houby	houba	k1gFnPc1	houba
<g/>
:	:	kIx,	:
hřib	hřib	k1gInSc1	hřib
žlučový	žlučový	k2eAgInSc1d1	žlučový
(	(	kIx(	(
<g/>
Tylophyllus	Tylophyllus	k1gInSc1	Tylophyllus
felleus	felleus	k1gInSc1	felleus
<g/>
)	)	kIx)	)
-	-	kIx~	-
silně	silně	k6eAd1	silně
hořký	hořký	k2eAgInSc1d1	hořký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neškodný	škodný	k2eNgMnSc1d1	neškodný
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc4d1	odlišný
vzhled	vzhled	k1gInSc4	vzhled
i	i	k8xC	i
výskyt	výskyt	k1gInSc4	výskyt
hřib	hřib	k1gInSc1	hřib
kříšť	kříšť	k1gInSc1	kříšť
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gMnSc1	Boletus
callopus	callopus	k1gMnSc1	callopus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
silně	silně	k6eAd1	silně
hořký	hořký	k2eAgInSc4d1	hořký
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgInPc4d1	žlutý
póry	pór	k1gInPc4	pór
<g/>
,	,	kIx,	,
výskyt	výskyt	k1gInSc1	výskyt
pod	pod	k7c7	pod
jehličnany	jehličnan	k1gInPc7	jehličnan
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
hřib	hřib	k1gInSc1	hřib
medotrpký	medotrpký	k2eAgInSc1d1	medotrpký
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
radicans	radicans	k1gInSc1	radicans
<g/>
)	)	kIx)	)
-	-	kIx~	-
hořký	hořký	k2eAgInSc4d1	hořký
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgInPc4d1	žlutý
póry	pór	k1gInPc4	pór
<g/>
,	,	kIx,	,
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
pod	pod	k7c7	pod
duby	dub	k1gInPc7	dub
<g/>
,	,	kIx,	,
na	na	k7c6	na
hrázích	hráz	k1gFnPc6	hráz
rybníků	rybník	k1gInPc2	rybník
hřib	hřib	k1gInSc1	hřib
kovář	kovář	k1gMnSc1	kovář
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gMnSc1	Boletus
luridiformis	luridiformis	k1gFnSc2	luridiformis
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
-	-	kIx~	-
jedlý	jedlý	k2eAgInSc1d1	jedlý
<g/>
,	,	kIx,	,
za	za	k7c2	za
syrova	syrov	k1gInSc2	syrov
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
trávící	trávící	k2eAgFnPc4d1	trávící
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgInSc1d1	hnědý
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
síťku	síťka	k1gFnSc4	síťka
na	na	k7c6	na
třeni	třeň	k1gInSc6	třeň
hřib	hřib	k1gInSc1	hřib
koloděj	koloděj	k1gMnSc1	koloděj
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gMnSc1	Boletus
luridus	luridus	k1gMnSc1	luridus
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedlý	jedlý	k2eAgMnSc1d1	jedlý
<g/>
,	,	kIx,	,
síťka	síťka	k1gFnSc1	síťka
na	na	k7c6	na
třeni	třeň	k1gInSc6	třeň
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
protáhlými	protáhlý	k2eAgInPc7d1	protáhlý
oky	oka	k1gFnPc1	oka
<g/>
,	,	kIx,	,
klobouk	klobouk	k1gInSc1	klobouk
krémově	krémově	k6eAd1	krémově
hnědý	hnědý	k2eAgInSc1d1	hnědý
Velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
xanthoidní	xanthoidní	k2eAgFnSc1d1	xanthoidní
(	(	kIx(	(
<g/>
odbarvená	odbarvený	k2eAgFnSc1d1	odbarvená
<g/>
)	)	kIx)	)
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
hřib	hřib	k1gInSc1	hřib
hlohový	hlohový	k2eAgInSc1d1	hlohový
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
crataegi	crataeg	k1gFnSc2	crataeg
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
hřib	hřib	k1gInSc1	hřib
satan	satan	k1gInSc1	satan
hlohový	hlohový	k2eAgInSc1d1	hlohový
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
satanas	satanas	k1gInSc1	satanas
f.	f.	k?	f.
crataegi	crataeg	k1gFnSc2	crataeg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
bílým	bílý	k2eAgNnSc7d1	bílé
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
světlým	světlý	k2eAgInSc7d1	světlý
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
,	,	kIx,	,
žlutými	žlutý	k2eAgFnPc7d1	žlutá
rourkami	rourka	k1gFnPc7	rourka
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
třeněm	třeň	k1gInSc7	třeň
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
vzhled	vzhled	k1gInSc1	vzhled
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
nejedlý	jedlý	k2eNgInSc1d1	nejedlý
(	(	kIx(	(
<g/>
hořký	hořký	k2eAgInSc1d1	hořký
<g/>
)	)	kIx)	)
hřib	hřib	k1gInSc1	hřib
medotrpký	medotrpký	k2eAgInSc1d1	medotrpký
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
radicans	radicans	k1gInSc1	radicans
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedlý	jedlý	k2eAgMnSc1d1	jedlý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zákonem	zákon	k1gInSc7	zákon
chráněný	chráněný	k2eAgInSc1d1	chráněný
hřib	hřib	k1gInSc1	hřib
Fechtnerův	Fechtnerův	k2eAgInSc1d1	Fechtnerův
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
fechtneri	fechtner	k1gFnSc2	fechtner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vhodné	vhodný	k2eAgNnSc1d1	vhodné
se	se	k3xPyFc4	se
všem	všecek	k3xTgInPc3	všecek
hřibům	hřib	k1gInPc3	hřib
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
kloboukem	klobouk	k1gInSc7	klobouk
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
třeněm	třeň	k1gInSc7	třeň
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
otravách	otrava	k1gFnPc6	otrava
má	mít	k5eAaImIp3nS	mít
obsah	obsah	k1gInSc1	obsah
dlouho	dlouho	k6eAd1	dlouho
neznámé	známý	k2eNgFnSc2d1	neznámá
toxické	toxický	k2eAgFnSc2d1	toxická
termolabilní	termolabilní	k2eAgFnSc2d1	termolabilní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
teplem	teplo	k1gNnSc7	teplo
rozložitelné	rozložitelný	k2eAgFnPc1d1	rozložitelná
<g/>
)	)	kIx)	)
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
zahřívání	zahřívání	k1gNnSc1	zahřívání
na	na	k7c4	na
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
popsána	popsán	k2eAgFnSc1d1	popsána
a	a	k8xC	a
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jako	jako	k8xC	jako
bolesatin	bolesatina	k1gFnPc2	bolesatina
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
glykoprotein	glykoprotein	k1gInSc4	glykoprotein
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
termostabilní	termostabilní	k2eAgFnSc1d1	termostabilní
a	a	k8xC	a
rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
vůči	vůči	k7c3	vůči
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
proteolytickými	proteolytický	k2eAgInPc7d1	proteolytický
enzymy	enzym	k1gInPc7	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
toxiny	toxin	k1gInPc1	toxin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
teplem	teplo	k1gNnSc7	teplo
nerozkládají	rozkládat	k5eNaImIp3nP	rozkládat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
indolové	indolový	k2eAgInPc1d1	indolový
deriváty	derivát	k1gInPc1	derivát
(	(	kIx(	(
<g/>
psychotoxin	psychotoxin	k1gInSc1	psychotoxin
<g/>
)	)	kIx)	)
a	a	k8xC	a
stopy	stopa	k1gFnPc4	stopa
muskarinu	muskarin	k1gInSc2	muskarin
<g/>
.	.	kIx.	.
</s>
<s>
Satan	satan	k1gInSc1	satan
však	však	k9	však
nelze	lze	k6eNd1	lze
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
doporučit	doporučit	k5eAaPmF	doporučit
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
ani	ani	k8xC	ani
po	po	k7c6	po
tepelném	tepelný	k2eAgNnSc6d1	tepelné
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Otravu	otrava	k1gFnSc4	otrava
způsobí	způsobit	k5eAaPmIp3nS	způsobit
požití	požití	k1gNnSc1	požití
syrové	syrový	k2eAgFnSc2d1	syrová
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
nedostatečně	dostatečně	k6eNd1	dostatečně
tepelně	tepelně	k6eAd1	tepelně
zpracované	zpracovaný	k2eAgFnPc4d1	zpracovaná
<g/>
)	)	kIx)	)
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
ochutnat	ochutnat	k5eAaPmF	ochutnat
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
zvracení	zvracení	k1gNnSc1	zvracení
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
kousek	kousek	k1gInSc4	kousek
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
ořechu	ořech	k1gInSc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
dokážou	dokázat	k5eAaPmIp3nP	dokázat
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
výpary	výpar	k1gInPc4	výpar
ze	z	k7c2	z
syrové	syrový	k2eAgFnSc2d1	syrová
houby	houba	k1gFnSc2	houba
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Popsán	popsat	k5eAaPmNgInS	popsat
byl	být	k5eAaImAgInS	být
i	i	k9	i
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
otravu	otrava	k1gFnSc4	otrava
provázenou	provázený	k2eAgFnSc4d1	provázená
dvouhodinovým	dvouhodinový	k2eAgNnSc7d1	dvouhodinové
zvracením	zvracení	k1gNnSc7	zvracení
a	a	k8xC	a
celodenními	celodenní	k2eAgFnPc7d1	celodenní
bolestmi	bolest	k1gFnPc7	bolest
břicha	břicho	k1gNnSc2	břicho
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
jen	jen	k9	jen
ochutnání	ochutnání	k1gNnSc1	ochutnání
kousku	kousek	k1gInSc2	kousek
syrové	syrový	k2eAgFnSc2d1	syrová
houby	houba	k1gFnSc2	houba
bez	bez	k7c2	bez
spolknutí	spolknutí	k1gNnSc2	spolknutí
dužniny	dužnina	k1gFnSc2	dužnina
<g/>
.	.	kIx.	.
</s>
<s>
Otravu	otrava	k1gFnSc4	otrava
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
úporné	úporný	k2eAgInPc4d1	úporný
a	a	k8xC	a
silné	silný	k2eAgFnPc4d1	silná
žaludeční	žaludeční	k2eAgFnPc4d1	žaludeční
a	a	k8xC	a
střevní	střevní	k2eAgFnPc4d1	střevní
obtíže	obtíž	k1gFnPc4	obtíž
provázené	provázený	k2eAgNnSc1d1	provázené
silnou	silný	k2eAgFnSc7d1	silná
dehydratací	dehydratace	k1gFnSc7	dehydratace
organismu	organismus	k1gInSc2	organismus
-	-	kIx~	-
gastroenterodyspeptický	gastroenterodyspeptický	k2eAgInSc1d1	gastroenterodyspeptický
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
30	[number]	k4	30
-	-	kIx~	-
120	[number]	k4	120
(	(	kIx(	(
<g/>
240	[number]	k4	240
<g/>
)	)	kIx)	)
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastává	nastávat	k5eAaImIp3nS	nastávat
silné	silný	k2eAgNnSc4d1	silné
zvracení	zvracení	k1gNnSc4	zvracení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Provází	provázet	k5eAaImIp3nS	provázet
jej	on	k3xPp3gMnSc4	on
celkové	celkový	k2eAgNnSc1d1	celkové
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
dehydratace	dehydratace	k1gFnSc2	dehydratace
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
však	však	k9	však
na	na	k7c4	na
území	území	k1gNnSc4	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
nebyla	být	k5eNaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
vypít	vypít	k5eAaPmF	vypít
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyhledat	vyhledat	k5eAaPmF	vyhledat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vypumpování	vypumpování	k1gNnSc6	vypumpování
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
postup	postup	k1gInSc1	postup
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
pacienta	pacient	k1gMnSc2	pacient
-	-	kIx~	-
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
doplnit	doplnit	k5eAaPmF	doplnit
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc4	účinek
satanu	satan	k1gInSc2	satan
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
asistentem	asistent	k1gMnSc7	asistent
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
mnichovský	mnichovský	k2eAgMnSc1d1	mnichovský
profesor	profesor	k1gMnSc1	profesor
Harald	Harald	k1gMnSc1	Harald
Othmar	Othmar	k1gMnSc1	Othmar
Lenz	Lenz	k1gMnSc1	Lenz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
houbu	houba	k1gFnSc4	houba
odborně	odborně	k6eAd1	odborně
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
Boletus	Boletus	k1gMnSc1	Boletus
satanas	satanas	k1gMnSc1	satanas
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Satanpilz	Satanpilz	k1gInSc1	Satanpilz
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
jako	jako	k9	jako
hřib	hřib	k1gInSc4	hřib
satan	satan	k1gInSc1	satan
<g/>
)	)	kIx)	)
mu	on	k3xPp3gNnSc3	on
dal	dát	k5eAaPmAgMnS	dát
právě	právě	k6eAd1	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
zkušenosti	zkušenost	k1gFnSc2	zkušenost
(	(	kIx(	(
<g/>
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
podle	podle	k7c2	podle
překladu	překlad	k1gInSc2	překlad
Jana	Jan	k1gMnSc2	Jan
Bezděka	Bezděk	k1gMnSc2	Bezděk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1830	[number]	k4	1830
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgInS	vypravit
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
-	-	kIx~	-
studentem	student	k1gMnSc7	student
medicíny	medicína	k1gFnSc2	medicína
Karlem	Karel	k1gMnSc7	Karel
Salzmannem	Salzmann	k1gMnSc7	Salzmann
-	-	kIx~	-
na	na	k7c4	na
houby	houba	k1gFnPc4	houba
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
našli	najít	k5eAaPmAgMnP	najít
plodnice	plodnice	k1gFnPc4	plodnice
satanu	satan	k1gInSc2	satan
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gInSc4	on
obzvlášť	obzvlášť	k6eAd1	obzvlášť
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
předchozích	předchozí	k2eAgMnPc2d1	předchozí
badatelů	badatel	k1gMnPc2	badatel
nebyl	být	k5eNaImAgInS	být
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
v	v	k7c4	v
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
ochutnal	ochutnat	k5eAaPmAgInS	ochutnat
kousek	kousek	k1gInSc1	kousek
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
houby	houba	k1gFnSc2	houba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
rozkousal	rozkousat	k5eAaPmAgMnS	rozkousat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyplivl	vyplivnout	k5eAaPmAgMnS	vyplivnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgInS	dostavit
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
nepoznal	poznat	k5eNaPmAgInS	poznat
-	-	kIx~	-
přirovnával	přirovnávat	k5eAaImAgInS	přirovnávat
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
mrtvici	mrtvice	k1gFnSc3	mrtvice
a	a	k8xC	a
vzpamatoval	vzpamatovat	k5eAaPmAgMnS	vzpamatovat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Lenz	Lenz	k1gMnSc1	Lenz
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	on	k3xPp3gInSc4	on
stav	stav	k1gInSc4	stav
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nachlazení	nachlazení	k1gNnSc1	nachlazení
při	při	k7c6	při
sběru	sběr	k1gInSc6	sběr
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
bádání	bádání	k1gNnSc6	bádání
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
ochutnal	ochutnat	k5eAaPmAgMnS	ochutnat
kousek	kousek	k1gInSc4	kousek
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
hodin	hodina	k1gFnPc2	hodina
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
malátnost	malátnost	k1gFnSc1	malátnost
a	a	k8xC	a
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
před	před	k7c7	před
20	[number]	k4	20
<g/>
.	.	kIx.	.
hodinou	hodina	k1gFnSc7	hodina
a	a	k8xC	a
opakovalo	opakovat	k5eAaImAgNnS	opakovat
se	se	k3xPyFc4	se
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
Nucení	nucení	k1gNnPc1	nucení
ke	k	k7c3	k
zvracení	zvracení	k1gNnSc3	zvracení
přicházelo	přicházet	k5eAaImAgNnS	přicházet
znenáhla	znenáhla	k6eAd1	znenáhla
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezičasech	mezičas	k1gInPc6	mezičas
Lenz	Lenz	k1gInSc4	Lenz
téměř	téměř	k6eAd1	téměř
nepozoroval	pozorovat	k5eNaImAgMnS	pozorovat
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
zvracení	zvracení	k1gNnSc1	zvracení
se	se	k3xPyFc4	se
dostavilo	dostavit	k5eAaPmAgNnS	dostavit
kolem	kolem	k7c2	kolem
22	[number]	k4	22
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
a	a	k8xC	a
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
hodinou	hodina	k1gFnSc7	hodina
ranní	ranní	k1gFnSc2	ranní
se	se	k3xPyFc4	se
již	již	k6eAd1	již
cítil	cítit	k5eAaImAgMnS	cítit
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
extrémní	extrémní	k2eAgFnSc1d1	extrémní
slabost	slabost	k1gFnSc1	slabost
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
jen	jen	k9	jen
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
obtížemi	obtíž	k1gFnPc7	obtíž
vstát	vstát	k5eAaPmF	vstát
a	a	k8xC	a
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Léčil	léčit	k5eAaImAgInS	léčit
se	se	k3xPyFc4	se
pitím	pití	k1gNnSc7	pití
olivového	olivový	k2eAgInSc2d1	olivový
a	a	k8xC	a
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
vysílen	vysílen	k2eAgInSc1d1	vysílen
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
zdravý	zdravý	k2eAgMnSc1d1	zdravý
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
večera	večer	k1gInSc2	večer
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Lenz	Lenz	k1gMnSc1	Lenz
začal	začít	k5eAaPmAgMnS	začít
pozorovat	pozorovat	k5eAaImF	pozorovat
první	první	k4xOgInPc4	první
příznaky	příznak	k1gInPc4	příznak
otravy	otrava	k1gFnSc2	otrava
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
o	o	k7c6	o
21	[number]	k4	21
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Karel	Karel	k1gMnSc1	Karel
Salzmann	Salzmann	k1gMnSc1	Salzmann
<g/>
.	.	kIx.	.
</s>
<s>
Sdělil	sdělit	k5eAaPmAgMnS	sdělit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednu	jeden	k4xCgFnSc4	jeden
zdravou	zdravý	k2eAgFnSc4d1	zdravá
plodnici	plodnice	k1gFnSc4	plodnice
oloupal	oloupat	k5eAaPmAgMnS	oloupat
<g/>
,	,	kIx,	,
zbavil	zbavit	k5eAaPmAgMnS	zbavit
rourek	rourka	k1gFnPc2	rourka
<g/>
,	,	kIx,	,
nakrájel	nakrájet	k5eAaPmAgMnS	nakrájet
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
na	na	k7c6	na
másle	máslo	k1gNnSc6	máslo
společně	společně	k6eAd1	společně
se	s	k7c7	s
slaninou	slanina	k1gFnSc7	slanina
<g/>
,	,	kIx,	,
cibulí	cibule	k1gFnSc7	cibule
<g/>
,	,	kIx,	,
moukou	mouka	k1gFnSc7	mouka
a	a	k8xC	a
trochou	trocha	k1gFnSc7	trocha
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
pokrm	pokrm	k1gInSc4	pokrm
snědl	sníst	k5eAaPmAgMnS	sníst
s	s	k7c7	s
brambory	brambor	k1gInPc7	brambor
s	s	k7c7	s
máslem	máslo	k1gNnSc7	máslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
<g/>
.	.	kIx.	.
hodin	hodina	k1gFnPc2	hodina
postihlo	postihnout	k5eAaPmAgNnS	postihnout
Salzmanna	Salzmanen	k2eAgNnPc4d1	Salzmanen
zvracení	zvracení	k1gNnSc4	zvracení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
alespoň	alespoň	k9	alespoň
30	[number]	k4	30
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zvracení	zvracení	k1gNnSc1	zvracení
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
ještě	ještě	k9	ještě
oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
tušení	tušený	k2eAgMnPc1d1	tušený
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
houbou	houba	k1gFnSc7	houba
a	a	k8xC	a
Salzmannovu	Salzmannův	k2eAgFnSc4d1	Salzmannova
nevolnost	nevolnost	k1gFnSc4	nevolnost
si	se	k3xPyFc3	se
vysvětlovali	vysvětlovat	k5eAaImAgMnP	vysvětlovat
přejedením	přejedení	k1gNnSc7	přejedení
a	a	k8xC	a
znechucením	znechucení	k1gNnSc7	znechucení
z	z	k7c2	z
Lenzových	Lenzův	k2eAgFnPc2d1	Lenzův
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobné	podobný	k2eAgFnPc1d1	podobná
potíže	potíž	k1gFnPc1	potíž
potkaly	potkat	k5eAaPmAgFnP	potkat
i	i	k9	i
dámu	dáma	k1gFnSc4	dáma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lenzem	Lenz	k1gInSc7	Lenz
večeřela	večeřet	k5eAaImAgFnS	večeřet
i	i	k9	i
služku	služka	k1gFnSc4	služka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
pokrmu	pokrm	k1gInSc2	pokrm
také	také	k9	také
část	část	k1gFnSc1	část
snědla	sníst	k5eAaPmAgFnS	sníst
<g/>
,	,	kIx,	,
spojili	spojit	k5eAaPmAgMnP	spojit
si	se	k3xPyFc3	se
otravu	otrava	k1gFnSc4	otrava
s	s	k7c7	s
houbou	houba	k1gFnSc7	houba
<g/>
.	.	kIx.	.
</s>
<s>
Salzmanna	Salzmanna	k1gFnSc1	Salzmanna
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
stižen	stihnout	k5eAaPmNgInS	stihnout
nejhůře	zle	k6eAd3	zle
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
léčit	léčit	k5eAaImF	léčit
olivovým	olivový	k2eAgInSc7d1	olivový
olejem	olej	k1gInSc7	olej
<g/>
,	,	kIx,	,
týmž	týž	k3xTgInSc7	týž
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
drceného	drcený	k2eAgNnSc2d1	drcené
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpaného	vyčerpaný	k2eAgMnSc2d1	vyčerpaný
pacienta	pacient	k1gMnSc2	pacient
Lenz	Lenz	k1gMnSc1	Lenz
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
ranhojiče	ranhojič	k1gMnSc2	ranhojič
Hauna	Haun	k1gMnSc2	Haun
dále	daleko	k6eAd2	daleko
léčil	léčit	k5eAaImAgMnS	léčit
podáváním	podávání	k1gNnSc7	podávání
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
zvráceno	zvrácen	k2eAgNnSc1d1	zvráceno
<g/>
.	.	kIx.	.
</s>
<s>
Tep	tep	k1gInSc1	tep
byl	být	k5eAaImAgInS	být
sotva	sotva	k6eAd1	sotva
znatelný	znatelný	k2eAgInSc1d1	znatelný
<g/>
,	,	kIx,	,
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
tělo	tělo	k1gNnSc1	tělo
jej	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
bolelo	bolet	k5eAaImAgNnS	bolet
<g/>
.	.	kIx.	.
</s>
<s>
Dostavily	dostavit	k5eAaPmAgInP	dostavit
se	se	k3xPyFc4	se
i	i	k9	i
bolestivé	bolestivý	k2eAgFnPc1d1	bolestivá
křeče	křeč	k1gFnPc1	křeč
<g/>
,	,	kIx,	,
silné	silný	k2eAgInPc1d1	silný
krvavé	krvavý	k2eAgInPc1d1	krvavý
průjmy	průjem	k1gInPc1	průjem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vědomí	vědomí	k1gNnSc4	vědomí
neztratil	ztratit	k5eNaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
dorazil	dorazit	k5eAaPmAgMnS	dorazit
přivolaný	přivolaný	k2eAgMnSc1d1	přivolaný
lékař	lékař	k1gMnSc1	lékař
Richter	Richter	k1gMnSc1	Richter
z	z	k7c2	z
Valtershausenů	Valtershausen	k1gInPc2	Valtershausen
<g/>
,	,	kIx,	,
ráno	ráno	k6eAd1	ráno
ještě	ještě	k9	ještě
člen	člen	k1gMnSc1	člen
lékařské	lékařský	k2eAgFnSc2d1	lékařská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
Kerst	Kerst	k1gInSc4	Kerst
z	z	k7c2	z
Gothy	Gotha	k1gFnSc2	Gotha
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
kúře	kúra	k1gFnSc6	kúra
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
obkládali	obkládat	k5eAaImAgMnP	obkládat
pacientům	pacient	k1gMnPc3	pacient
tělo	tělo	k1gNnSc4	tělo
vařeným	vařený	k2eAgNnSc7d1	vařené
lněným	lněný	k2eAgNnSc7d1	lněné
semenem	semeno	k1gNnSc7	semeno
a	a	k8xC	a
podávali	podávat	k5eAaImAgMnP	podávat
klystýry	klystýr	k1gInPc4	klystýr
z	z	k7c2	z
heřmánku	heřmánek	k1gInSc2	heřmánek
<g/>
,	,	kIx,	,
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
mandlového	mandlový	k2eAgInSc2d1	mandlový
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
ze	z	k7c2	z
slizu	sliz	k1gInSc2	sliz
vařeného	vařený	k2eAgNnSc2d1	vařené
lněného	lněný	k2eAgNnSc2d1	lněné
semene	semeno	k1gNnSc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Lenzovi	Lenz	k1gMnSc3	Lenz
se	se	k3xPyFc4	se
druhého	druhý	k4xOgMnSc4	druhý
dne	den	k1gInSc2	den
dařilo	dařit	k5eAaImAgNnS	dařit
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
Salzmann	Salzmann	k1gMnSc1	Salzmann
stále	stále	k6eAd1	stále
zvracel	zvracet	k5eAaImAgMnS	zvracet
i	i	k8xC	i
medikamenty	medikament	k1gInPc4	medikament
-	-	kIx~	-
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
odvar	odvar	k1gInSc1	odvar
z	z	k7c2	z
ovesných	ovesný	k2eAgFnPc2d1	ovesná
krup	kroupa	k1gFnPc2	kroupa
i	i	k8xC	i
mandlové	mandlový	k2eAgNnSc1d1	mandlové
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
krom	krom	k7c2	krom
mandlového	mandlový	k2eAgNnSc2d1	mandlové
mléka	mléko	k1gNnSc2	mléko
tyto	tento	k3xDgInPc1	tento
léky	lék	k1gInPc1	lék
pomáhaly	pomáhat	k5eAaImAgInP	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
dostával	dostávat	k5eAaImAgMnS	dostávat
lžíci	lžíce	k1gFnSc4	lžíce
emulze	emulze	k1gFnSc2	emulze
z	z	k7c2	z
mandlového	mandlový	k2eAgInSc2d1	mandlový
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgFnSc2d1	arabská
klovatiny	klovatina	k1gFnSc2	klovatina
a	a	k8xC	a
opia	opium	k1gNnSc2	opium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pozvolna	pozvolna	k6eAd1	pozvolna
mírnila	mírnit	k5eAaImAgFnS	mírnit
náchylnost	náchylnost	k1gFnSc1	náchylnost
ke	k	k7c3	k
zvracení	zvracení	k1gNnSc3	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
definitivně	definitivně	k6eAd1	definitivně
přestalo	přestat	k5eAaPmAgNnS	přestat
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
rýže	rýže	k1gFnSc2	rýže
uvařené	uvařený	k2eAgFnSc2d1	uvařená
v	v	k7c6	v
kuřecí	kuřecí	k2eAgFnSc6d1	kuřecí
polévce	polévka	k1gFnSc6	polévka
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Salzmann	Salzmann	k1gMnSc1	Salzmann
vstát	vstát	k5eAaPmF	vstát
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
vydržet	vydržet	k5eAaPmF	vydržet
mimo	mimo	k7c4	mimo
postel	postel	k1gFnSc4	postel
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
začala	začít	k5eAaPmAgFnS	začít
vracet	vracet	k5eAaImF	vracet
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nepotřeboval	potřebovat	k5eNaImAgMnS	potřebovat
ležet	ležet	k5eAaImF	ležet
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
dobře	dobře	k6eAd1	dobře
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byl	být	k5eAaImAgMnS	být
schopný	schopný	k2eAgMnSc1d1	schopný
vyjít	vyjít	k5eAaPmF	vyjít
mimo	mimo	k7c4	mimo
dům	dům	k1gInSc4	dům
a	a	k8xC	a
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
mohl	moct	k5eAaImAgInS	moct
chodit	chodit	k5eAaImF	chodit
i	i	k9	i
po	po	k7c4	po
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
celkovou	celkový	k2eAgFnSc4d1	celková
slabost	slabost	k1gFnSc4	slabost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odezněla	odeznět	k5eAaPmAgFnS	odeznět
až	až	k9	až
po	po	k7c4	po
2	[number]	k4	2
-	-	kIx~	-
3	[number]	k4	3
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Krombholz	Krombholz	k1gMnSc1	Krombholz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
mykologům	mykolog	k1gMnPc3	mykolog
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gMnSc7	satan
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
(	(	kIx(	(
<g/>
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
podle	podle	k7c2	podle
překladu	překlad	k1gInSc2	překlad
Jana	Jan	k1gMnSc2	Jan
Bezděka	Bezděk	k1gMnSc2	Bezděk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prodavač	prodavač	k1gMnSc1	prodavač
nabízel	nabízet	k5eAaImAgMnS	nabízet
tyto	tento	k3xDgFnPc4	tento
plodnice	plodnice	k1gFnPc4	plodnice
společně	společně	k6eAd1	společně
s	s	k7c7	s
hřibem	hřib	k1gInSc7	hřib
královským	královský	k2eAgInSc7d1	královský
a	a	k8xC	a
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
jej	on	k3xPp3gMnSc4	on
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Krombholz	Krombholz	k1gMnSc1	Krombholz
ochutnal	ochutnat	k5eAaPmAgMnS	ochutnat
malý	malý	k2eAgInSc4d1	malý
kousek	kousek	k1gInSc4	kousek
klobouku	klobouk	k1gInSc2	klobouk
a	a	k8xC	a
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
prodavače	prodavač	k1gMnSc2	prodavač
koupil	koupit	k5eAaPmAgMnS	koupit
všechny	všechen	k3xTgFnPc4	všechen
plodnice	plodnice	k1gFnPc4	plodnice
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
jej	on	k3xPp3gInSc4	on
kresliči	kreslič	k1gMnPc1	kreslič
Šírovi	Šírův	k2eAgMnPc1d1	Šírův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
začal	začít	k5eAaPmAgMnS	začít
pozorovat	pozorovat	k5eAaImF	pozorovat
mírnou	mírný	k2eAgFnSc4d1	mírná
závrať	závrať	k1gFnSc4	závrať
a	a	k8xC	a
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
dočasně	dočasně	k6eAd1	dočasně
zahnalo	zahnat	k5eAaPmAgNnS	zahnat
několik	několik	k4yIc4	několik
kapek	kapka	k1gFnPc2	kapka
Hoffmannského	Hoffmannský	k2eAgInSc2d1	Hoffmannský
lihu	líh	k1gInSc2	líh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nepátral	pátrat	k5eNaImAgMnS	pátrat
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
původu	původ	k1gInSc6	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
navštívil	navštívit	k5eAaPmAgMnS	navštívit
kresliče	kreslič	k1gMnSc2	kreslič
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
silné	silný	k2eAgFnPc4d1	silná
bolesti	bolest	k1gFnPc4	bolest
břicha	břicho	k1gNnSc2	břicho
a	a	k8xC	a
zvracel	zvracet	k5eAaImAgMnS	zvracet
krev	krev	k1gFnSc4	krev
-	-	kIx~	-
ochutnal	ochutnat	k5eAaPmAgInS	ochutnat
větší	veliký	k2eAgInSc4d2	veliký
kousek	kousek	k1gInSc4	kousek
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
svého	svůj	k3xOyFgMnSc4	svůj
písaře	písař	k1gMnSc4	písař
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zápasil	zápasit	k5eAaImAgMnS	zápasit
s	s	k7c7	s
mdlobami	mdloba	k1gFnPc7	mdloba
<g/>
,	,	kIx,	,
závratí	závrať	k1gFnSc7	závrať
a	a	k8xC	a
nevolností	nevolnost	k1gFnSc7	nevolnost
-	-	kIx~	-
taktéž	taktéž	k?	taktéž
ochutnal	ochutnat	k5eAaPmAgMnS	ochutnat
kousek	kousek	k1gInSc4	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
hůř	zle	k6eAd2	zle
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
prosektorovi	prosektor	k1gMnSc3	prosektor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
Krombholzově	Krombholzův	k2eAgFnSc6d1	Krombholzův
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
jednu	jeden	k4xCgFnSc4	jeden
houbu	houba	k1gFnSc4	houba
vzal	vzít	k5eAaPmAgMnS	vzít
a	a	k8xC	a
asi	asi	k9	asi
50	[number]	k4	50
gramů	gram	k1gInPc2	gram
připravil	připravit	k5eAaPmAgMnS	připravit
na	na	k7c6	na
másle	máslo	k1gNnSc6	máslo
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
cítil	cítit	k5eAaImAgMnS	cítit
mírnou	mírný	k2eAgFnSc4d1	mírná
bolest	bolest	k1gFnSc4	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k1gInSc1	večer
požil	požít	k5eAaPmAgInS	požít
asi	asi	k9	asi
3	[number]	k4	3
kousky	kousek	k1gInPc4	kousek
syrové	syrový	k2eAgFnSc2d1	syrová
houby	houba	k1gFnSc2	houba
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4	[number]	k4	4
gramy	gram	k1gInPc4	gram
<g/>
)	)	kIx)	)
a	a	k8xC	a
cítil	cítit	k5eAaImAgMnS	cítit
poté	poté	k6eAd1	poté
pálení	pálení	k1gNnSc6	pálení
v	v	k7c6	v
jícnu	jícen	k1gInSc6	jícen
<g/>
,	,	kIx,	,
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
později	pozdě	k6eAd2	pozdě
horkost	horkost	k1gFnSc4	horkost
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
čele	čelo	k1gNnSc6	čelo
a	a	k8xC	a
zádech	zádech	k1gInSc1	zádech
vyvstal	vyvstat	k5eAaPmAgInS	vyvstat
pot	pot	k1gInSc4	pot
a	a	k8xC	a
přidalo	přidat	k5eAaPmAgNnS	přidat
se	se	k3xPyFc4	se
i	i	k9	i
bušení	bušení	k1gNnSc2	bušení
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
těžkost	těžkost	k1gFnSc1	těžkost
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
výraznější	výrazný	k2eAgNnSc1d2	výraznější
škrábání	škrábání	k1gNnSc1	škrábání
a	a	k8xC	a
pálení	pálení	k1gNnSc1	pálení
jícnu	jícen	k1gInSc2	jícen
a	a	k8xC	a
zemdlelost	zemdlelost	k1gFnSc4	zemdlelost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
ani	ani	k9	ani
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
závrať	závrať	k1gFnSc1	závrať
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
mžitky	mžitka	k1gFnPc1	mžitka
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
výpadky	výpadek	k1gInPc7	výpadek
sluchu	sluch	k1gInSc2	sluch
<g/>
,	,	kIx,	,
hučení	hučení	k1gNnSc1	hučení
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
a	a	k8xC	a
prudké	prudký	k2eAgNnSc1d1	prudké
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
Prosektor	prosektor	k1gMnSc1	prosektor
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
pít	pít	k5eAaImF	pít
studenou	studený	k2eAgFnSc4d1	studená
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
bolestivých	bolestivý	k2eAgFnPc6d1	bolestivá
žaludečních	žaludeční	k2eAgFnPc6d1	žaludeční
křečích	křeč	k1gFnPc6	křeč
ji	on	k3xPp3gFnSc4	on
opět	opět	k6eAd1	opět
zvracel	zvracet	k5eAaImAgInS	zvracet
<g/>
,	,	kIx,	,
asi	asi	k9	asi
13	[number]	k4	13
-	-	kIx~	-
15	[number]	k4	15
<g/>
×	×	k?	×
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Objevovalo	objevovat	k5eAaImAgNnS	objevovat
se	se	k3xPyFc4	se
mrazení	mrazení	k1gNnSc1	mrazení
v	v	k7c6	v
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
citlivost	citlivost	k1gFnSc1	citlivost
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
tlukot	tlukot	k1gInSc1	tlukot
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
vysychání	vysychání	k1gNnSc2	vysychání
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
neuhasitelná	uhasitelný	k2eNgFnSc1d1	neuhasitelná
žízeň	žízeň	k1gFnSc1	žízeň
a	a	k8xC	a
prudké	prudký	k2eAgNnSc1d1	prudké
zvracení	zvracení	k1gNnSc1	zvracení
všech	všecek	k3xTgFnPc2	všecek
přijatých	přijatý	k2eAgFnPc2d1	přijatá
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
půlnoci	půlnoc	k1gFnSc3	půlnoc
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
záchvaty	záchvat	k1gInPc1	záchvat
zuřivosti	zuřivost	k1gFnSc2	zuřivost
<g/>
,	,	kIx,	,
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
ve	v	k7c6	v
zvratcích	zvratek	k1gInPc6	zvratek
i	i	k8xC	i
stolici	stolice	k1gFnSc6	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
byl	být	k5eAaImAgMnS	být
léčen	léčit	k5eAaImNgMnS	léčit
potíráním	potírání	k1gNnSc7	potírání
břicha	břicho	k1gNnSc2	břicho
směsí	směs	k1gFnPc2	směs
oleje	olej	k1gInSc2	olej
s	s	k7c7	s
blínovým	blínový	k2eAgInSc7d1	blínový
výtažkem	výtažek	k1gInSc7	výtažek
<g/>
,	,	kIx,	,
obklady	obklad	k1gInPc4	obklad
z	z	k7c2	z
hořčičné	hořčičný	k2eAgFnSc2d1	hořčičná
mouky	mouka	k1gFnSc2	mouka
a	a	k8xC	a
podáván	podáván	k2eAgInSc1d1	podáván
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
ječný	ječný	k2eAgInSc4d1	ječný
odvar	odvar	k1gInSc4	odvar
<g/>
.	.	kIx.	.
</s>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
začaly	začít	k5eAaPmAgFnP	začít
ustupovat	ustupovat	k5eAaImF	ustupovat
a	a	k8xC	a
k	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
dokázal	dokázat	k5eAaPmAgInS	dokázat
na	na	k7c4	na
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
usnout	usnout	k5eAaPmF	usnout
<g/>
.	.	kIx.	.
</s>
<s>
Rekonvalescence	rekonvalescence	k1gFnSc1	rekonvalescence
probíhala	probíhat	k5eAaImAgFnS	probíhat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnPc4d1	silná
bolesti	bolest	k1gFnPc4	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
nápojům	nápoj	k1gInPc3	nápoj
i	i	k8xC	i
studenému	studený	k2eAgInSc3d1	studený
vzduchu	vzduch	k1gInSc3	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
houbový	houbový	k2eAgInSc4d1	houbový
pokrm	pokrm	k1gInSc4	pokrm
vzbuzovala	vzbuzovat	k5eAaImAgFnS	vzbuzovat
nevolnost	nevolnost	k1gFnSc1	nevolnost
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
exempláře	exemplář	k1gInSc2	exemplář
houby	houba	k1gFnSc2	houba
otrávil	otrávit	k5eAaPmAgMnS	otrávit
Krombholzův	Krombholzův	k2eAgMnSc1d1	Krombholzův
posluchač	posluchač	k1gMnSc1	posluchač
chirurgie	chirurgie	k1gFnSc2	chirurgie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ochutnal	ochutnat	k5eAaPmAgInS	ochutnat
asi	asi	k9	asi
2	[number]	k4	2
gramy	gram	k1gInPc4	gram
vážící	vážící	k2eAgInSc1d1	vážící
kousek	kousek	k1gInSc1	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Hodinu	hodina	k1gFnSc4	hodina
poté	poté	k6eAd1	poté
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
svírání	svírání	k1gNnSc1	svírání
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc2	zvracení
a	a	k8xC	a
svírání	svírání	k1gNnSc2	svírání
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
střídala	střídat	k5eAaImAgFnS	střídat
se	se	k3xPyFc4	se
horkost	horkost	k1gFnSc1	horkost
a	a	k8xC	a
mrazení	mrazení	k1gNnSc1	mrazení
<g/>
.	.	kIx.	.
</s>
<s>
Podáván	podáván	k2eAgInSc1d1	podáván
byl	být	k5eAaImAgInS	být
vinný	vinný	k2eAgInSc1d1	vinný
ocet	ocet	k1gInSc1	ocet
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgFnSc4	obojí
zpočátku	zpočátku	k6eAd1	zpočátku
pacient	pacient	k1gMnSc1	pacient
zvracel	zvracet	k5eAaImAgMnS	zvracet
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
přetrvávala	přetrvávat	k5eAaImAgFnS	přetrvávat
malátnost	malátnost	k1gFnSc1	malátnost
a	a	k8xC	a
pouhá	pouhý	k2eAgFnSc1d1	pouhá
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
houby	houba	k1gFnPc4	houba
opět	opět	k6eAd1	opět
působila	působit	k5eAaImAgFnS	působit
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Otravu	otrava	k1gFnSc4	otrava
zažil	zažít	k5eAaPmAgMnS	zažít
i	i	k9	i
český	český	k2eAgMnSc1d1	český
mykolog	mykolog	k1gMnSc1	mykolog
František	František	k1gMnSc1	František
Smotlacha	smotlacha	k1gMnSc1	smotlacha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
po	po	k7c6	po
ochutnání	ochutnání	k1gNnSc6	ochutnání
kousku	kousek	k1gInSc2	kousek
plodnice	plodnice	k1gFnSc2	plodnice
-	-	kIx~	-
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
otravy	otrava	k1gFnSc2	otrava
se	se	k3xPyFc4	se
zotavil	zotavit	k5eAaPmAgMnS	zotavit
večer	večer	k6eAd1	večer
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
co	co	k9	co
houbu	houba	k1gFnSc4	houba
ochutnal	ochutnat	k5eAaPmAgInS	ochutnat
<g/>
.	.	kIx.	.
</s>
<s>
Otravy	otrava	k1gFnPc1	otrava
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
-	-	kIx~	-
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
s	s	k7c7	s
lehčím	lehký	k2eAgInSc7d2	lehčí
průběhem	průběh	k1gInSc7	průběh
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
i	i	k9	i
další	další	k2eAgInPc1d1	další
hřiby	hřib	k1gInPc1	hřib
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
se	s	k7c7	s
satanem	satan	k1gInSc7	satan
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
vzhledově	vzhledově	k6eAd1	vzhledově
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
hřib	hřib	k1gInSc1	hřib
satanovitý	satanovitý	k2eAgInSc1d1	satanovitý
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
satanoides	satanoides	k1gInSc1	satanoides
<g/>
)	)	kIx)	)
hřib	hřib	k1gInSc1	hřib
nachový	nachový	k2eAgInSc1d1	nachový
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
purpureus	purpureus	k1gInSc1	purpureus
<g/>
)	)	kIx)	)
hřib	hřib	k1gInSc1	hřib
Le	Le	k1gFnSc2	Le
Galové	Galová	k1gFnSc2	Galová
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
legaliae	legalia	k1gInSc2	legalia
<g/>
)	)	kIx)	)
-	-	kIx~	-
názory	názor	k1gInPc1	názor
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
jedovatosti	jedovatost	k1gFnSc2	jedovatost
nejsou	být	k5eNaImIp3nP	být
jednotné	jednotný	k2eAgFnPc1d1	jednotná
hřib	hřib	k1gInSc1	hřib
Moserův	Moserův	k2eAgInSc1d1	Moserův
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
rubrosanguineus	rubrosanguineus	k1gInSc1	rubrosanguineus
<g/>
)	)	kIx)	)
hřib	hřib	k1gInSc1	hřib
rudonachový	rudonachový	k2eAgInSc1d1	rudonachový
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gInSc1	Boletus
rhodopurpureus	rhodopurpureus	k1gInSc1	rhodopurpureus
<g/>
)	)	kIx)	)
Prokazatelně	prokazatelně	k6eAd1	prokazatelně
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
a	a	k8xC	a
působící	působící	k2eAgFnPc4d1	působící
otravu	otrava	k1gFnSc4	otrava
typu	typ	k1gInSc2	typ
hřibem	hřib	k1gInSc7	hřib
satanem	satan	k1gInSc7	satan
jsou	být	k5eAaImIp3nP	být
hřib	hřib	k1gInSc4	hřib
satanovitý	satanovitý	k2eAgInSc4d1	satanovitý
a	a	k8xC	a
hřib	hřib	k1gInSc4	hřib
nachový	nachový	k2eAgInSc4d1	nachový
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
otravy	otrava	k1gFnSc2	otrava
hřibem	hřib	k1gInSc7	hřib
nachovým	nachový	k2eAgInSc7d1	nachový
detailně	detailně	k6eAd1	detailně
referoval	referovat	k5eAaBmAgMnS	referovat
Karel	Karel	k1gMnSc1	Karel
Krejčík	Krejčík	k1gMnSc1	Krejčík
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
hodiny	hodina	k1gFnPc1	hodina
po	po	k7c6	po
konzumaci	konzumace	k1gFnSc6	konzumace
kousku	kousek	k1gInSc2	kousek
syrové	syrový	k2eAgFnSc2d1	syrová
dužniny	dužnina	k1gFnSc2	dužnina
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
zhruba	zhruba	k6eAd1	zhruba
poloviny	polovina	k1gFnSc2	polovina
palce	palec	k1gInSc2	palec
se	se	k3xPyFc4	se
dostavila	dostavit	k5eAaPmAgFnS	dostavit
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nastalo	nastat	k5eAaPmAgNnS	nastat
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
asi	asi	k9	asi
20	[number]	k4	20
<g/>
×	×	k?	×
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Otravu	otrava	k1gFnSc4	otrava
provázel	provázet	k5eAaImAgMnS	provázet
studený	studený	k2eAgInSc4d1	studený
pot	pot	k1gInSc4	pot
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
malátnost	malátnost	k1gFnSc1	malátnost
a	a	k8xC	a
silná	silný	k2eAgFnSc1d1	silná
tělesná	tělesný	k2eAgFnSc1d1	tělesná
slabost	slabost	k1gFnSc1	slabost
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
trvaly	trvat	k5eAaImAgInP	trvat
těžké	těžký	k2eAgInPc1d1	těžký
vodnaté	vodnatý	k2eAgInPc1d1	vodnatý
průjmy	průjem	k1gInPc1	průjem
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
po	po	k7c6	po
pěti	pět	k4xCc6	pět
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
identicky	identicky	k6eAd1	identicky
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
symptomů	symptom	k1gInPc2	symptom
do	do	k7c2	do
hodiny	hodina	k1gFnSc2	hodina
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
průběh	průběh	k1gInSc4	průběh
otravy	otrava	k1gFnSc2	otrava
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
15-20	[number]	k4	15-20
g	g	kA	g
houby	houby	k6eAd1	houby
francouzský	francouzský	k2eAgMnSc1d1	francouzský
mykolog	mykolog	k1gMnSc1	mykolog
Victor	Victor	k1gMnSc1	Victor
Piane	Pian	k1gMnSc5	Pian
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
nevolnosti	nevolnost	k1gFnSc2	nevolnost
a	a	k8xC	a
zvracení	zvracení	k1gNnSc2	zvracení
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
nedostatečně	dostatečně	k6eNd1	dostatečně
upravených	upravený	k2eAgFnPc2d1	upravená
plodnic	plodnice	k1gFnPc2	plodnice
bývají	bývat	k5eAaImIp3nP	bývat
rovněž	rovněž	k9	rovněž
uváděné	uváděný	k2eAgInPc1d1	uváděný
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
hřibem	hřib	k1gInSc7	hřib
kovářem	kovář	k1gMnSc7	kovář
(	(	kIx(	(
<g/>
Boletus	Boletus	k1gMnSc1	Boletus
luridiformis	luridiformis	k1gFnSc2	luridiformis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
odborníků	odborník	k1gMnPc2	odborník
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
jednotné	jednotný	k2eAgFnPc1d1	jednotná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
kovář	kovář	k1gMnSc1	kovář
jedovaté	jedovatý	k2eAgFnSc2d1	jedovatá
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
potíže	potíž	k1gFnPc4	potíž
působí	působit	k5eAaImIp3nP	působit
výlučně	výlučně	k6eAd1	výlučně
velmi	velmi	k6eAd1	velmi
hutná	hutný	k2eAgFnSc1d1	hutná
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
stravitelná	stravitelný	k2eAgFnSc1d1	stravitelná
dužina	dužina	k1gFnSc1	dužina
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
důvodem	důvod	k1gInSc7	důvod
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc2d1	nízká
červivosti	červivost	k1gFnSc2	červivost
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
podle	podle	k7c2	podle
starší	starý	k2eAgFnSc2d2	starší
literatury	literatura	k1gFnSc2	literatura
však	však	k9	však
mnohé	mnohý	k2eAgInPc1d1	mnohý
modrající	modrající	k2eAgInPc1d1	modrající
hřiby	hřib	k1gInPc1	hřib
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
podobnou	podobný	k2eAgFnSc4d1	podobná
zvracení	zvracení	k1gNnSc2	zvracení
vyvolávající	vyvolávající	k2eAgFnSc4d1	vyvolávající
látku	látka	k1gFnSc4	látka
jako	jako	k8xC	jako
satan	satan	k1gInSc4	satan
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ve	v	k7c6	v
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc6d2	nižší
koncentraci	koncentrace	k1gFnSc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Řazeny	řazen	k2eAgInPc4d1	řazen
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
bývají	bývat	k5eAaImIp3nP	bývat
hřib	hřib	k1gInSc4	hřib
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
hřib	hřib	k1gInSc4	hřib
koloděj	koloděj	k1gMnSc1	koloděj
<g/>
,	,	kIx,	,
hřib	hřib	k1gInSc1	hřib
kříšť	kříšť	k1gInSc1	kříšť
a	a	k8xC	a
hřib	hřib	k1gInSc1	hřib
medotrpký	medotrpký	k2eAgInSc1d1	medotrpký
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Modráky	modrák	k1gInPc1	modrák
<g/>
.	.	kIx.	.
</s>
