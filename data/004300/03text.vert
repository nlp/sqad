<s>
Verzatilka	verzatilka	k1gFnSc1	verzatilka
(	(	kIx(	(
<g/>
či	či	k8xC	či
versatilka	versatilka	k1gFnSc1	versatilka
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
krajón	krajón	k1gInSc1	krajón
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mechanická	mechanický	k2eAgFnSc1d1	mechanická
padací	padací	k2eAgFnSc1d1	padací
tužka	tužka	k1gFnSc1	tužka
umožňující	umožňující	k2eAgFnSc1d1	umožňující
výměnu	výměna	k1gFnSc4	výměna
tuhy	tuha	k1gFnSc2	tuha
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výrobek	výrobek	k1gInSc4	výrobek
českobudějovické	českobudějovický	k2eAgFnSc2d1	českobudějovická
firmy	firma	k1gFnSc2	firma
Koh-i-noor	Kohoora	k1gFnPc2	Koh-i-noora
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uveden	k2eAgInSc1d1	uveden
roku	rok	k1gInSc3	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
verzatil	verzatit	k5eAaImAgInS	verzatit
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
verzatilní	verzatilní	k2eAgFnSc4d1	verzatilní
=	=	kIx~	=
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
proměnný	proměnný	k2eAgInSc1d1	proměnný
<g/>
,	,	kIx,	,
přizpůsobivý	přizpůsobivý	k2eAgInSc1d1	přizpůsobivý
<g/>
.	.	kIx.	.
</s>
<s>
Lidový	lidový	k2eAgInSc1d1	lidový
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
krajón	krajón	k1gInSc1	krajón
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
crayon	crayon	k1gNnSc1	crayon
=	=	kIx~	=
tužka	tužka	k1gFnSc1	tužka
a	a	k8xC	a
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
dostal	dostat	k5eAaPmAgInS	dostat
jako	jako	k9	jako
označení	označení	k1gNnSc4	označení
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
šestihranné	šestihranný	k2eAgFnSc2d1	šestihranná
tužky	tužka	k1gFnSc2	tužka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kleštinový	kleštinový	k2eAgInSc4d1	kleštinový
držák	držák	k1gInSc4	držák
tuhy	tuha	k1gFnSc2	tuha
<g/>
.	.	kIx.	.
</s>
<s>
Kleština	kleština	k1gFnSc1	kleština
je	být	k5eAaImIp3nS	být
sevřena	sevřít	k5eAaPmNgFnS	sevřít
pomocí	pomocí	k7c2	pomocí
pružiny	pružina	k1gFnSc2	pružina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
vtahuje	vtahovat	k5eAaImIp3nS	vtahovat
do	do	k7c2	do
kuželové	kuželový	k2eAgFnSc2d1	kuželová
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Kleština	kleština	k1gFnSc1	kleština
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
zatlačením	zatlačení	k1gNnSc7	zatlačení
na	na	k7c4	na
zadní	zadní	k2eAgInSc4d1	zadní
konec	konec	k1gInSc4	konec
pouzdra	pouzdro	k1gNnSc2	pouzdro
na	na	k7c4	na
tuhu	tuha	k1gFnSc4	tuha
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zašroubováno	zašroubován	k2eAgNnSc1d1	zašroubováno
hrotítko	hrotítko	k1gNnSc1	hrotítko
(	(	kIx(	(
<g/>
ořezávátko	ořezávátko	k1gNnSc1	ořezávátko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
škála	škála	k1gFnSc1	škála
těchto	tento	k3xDgFnPc2	tento
tužek	tužka	k1gFnPc2	tužka
odstupňovaná	odstupňovaný	k2eAgFnSc1d1	odstupňovaná
dle	dle	k7c2	dle
průměru	průměr	k1gInSc2	průměr
tuhy	tuha	k1gFnSc2	tuha
<g/>
.	.	kIx.	.
</s>
<s>
Tužky	tužka	k1gFnPc1	tužka
pro	pro	k7c4	pro
průměr	průměr	k1gInSc4	průměr
tuhy	tuha	k1gFnSc2	tuha
přes	přes	k7c4	přes
3	[number]	k4	3
mm	mm	kA	mm
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
k	k	k7c3	k
výtvarným	výtvarný	k2eAgInPc3d1	výtvarný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Tuhy	tuha	k1gFnPc1	tuha
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dokoupit	dokoupit	k5eAaPmF	dokoupit
samostatně	samostatně	k6eAd1	samostatně
i	i	k9	i
v	v	k7c6	v
barevných	barevný	k2eAgFnPc6d1	barevná
sadách	sada	k1gFnPc6	sada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trhu	trh	k1gInSc6	trh
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
držáky	držák	k1gInPc1	držák
mazací	mazací	k2eAgFnSc2d1	mazací
gumy	guma	k1gFnSc2	guma
nebo	nebo	k8xC	nebo
rýsovací	rýsovací	k2eAgFnSc2d1	rýsovací
jehly	jehla	k1gFnSc2	jehla
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
