<p>
<s>
Art	Art	k?	Art
Pepper	Pepper	k1gInSc1	Pepper
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Arthur	Arthur	k1gMnSc1	Arthur
Edward	Edward	k1gMnSc1	Edward
Pepper	Pepper	k1gMnSc1	Pepper
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1925	[number]	k4	1925
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
jazzový	jazzový	k2eAgMnSc1d1	jazzový
altsaxofonista	altsaxofonista	k1gMnSc1	altsaxofonista
<g/>
,	,	kIx,	,
tenorsaxofonista	tenorsaxofonista	k1gMnSc1	tenorsaxofonista
<g/>
,	,	kIx,	,
klarinetista	klarinetista	k1gMnSc1	klarinetista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Paul	Paul	k1gMnSc1	Paul
Desmond	Desmond	k1gMnSc1	Desmond
<g/>
,	,	kIx,	,
Chet	Chet	k1gMnSc1	Chet
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
Stan	stan	k1gInSc1	stan
Kenton	Kenton	k1gInSc1	Kenton
<g/>
,	,	kIx,	,
Shorty	Short	k1gInPc1	Short
Rogers	Rogers	k1gInSc1	Rogers
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Gardena	Gardeno	k1gNnSc2	Gardeno
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
čtrnáctiletá	čtrnáctiletý	k2eAgFnSc1d1	čtrnáctiletá
uprchlice	uprchlice	k1gFnSc1	uprchlice
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
námořník	námořník	k1gMnSc1	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
u	u	k7c2	u
babičky	babička	k1gFnSc2	babička
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
devíti	devět	k4xCc6	devět
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
pak	pak	k6eAd1	pak
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
saxofonu	saxofon	k1gInSc3	saxofon
<g/>
.	.	kIx.	.
</s>
<s>
Profesionálně	profesionálně	k6eAd1	profesionálně
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
například	například	k6eAd1	například
s	s	k7c7	s
Bennym	Bennym	k1gInSc1	Bennym
Carterem	Carter	k1gInSc7	Carter
a	a	k8xC	a
Stanem	stan	k1gInSc7	stan
Kentonem	Kenton	k1gInSc7	Kenton
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
opět	opět	k6eAd1	opět
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Kentonem	Kenton	k1gInSc7	Kenton
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nahrál	nahrát	k5eAaBmAgMnS	nahrát
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vlastních	vlastní	k2eAgNnPc2d1	vlastní
alb	album	k1gNnPc2	album
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgMnPc7d1	další
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
strávil	strávit	k5eAaPmAgInS	strávit
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
léčbu	léčba	k1gFnSc4	léčba
metadonem	metadon	k1gInSc7	metadon
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
56	[number]	k4	56
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
