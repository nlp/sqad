<p>
<s>
Blind	Blind	k1gInSc1	Blind
Guardian	Guardiany	k1gInPc2	Guardiany
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Krefeld	Krefeld	k1gInSc4	Krefeld
jako	jako	k8xC	jako
speed	speed	k1gInSc4	speed
metalové	metalový	k2eAgNnSc4d1	metalové
těleso	těleso	k1gNnSc4	těleso
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Heritage	Heritage	k1gFnSc7	Heritage
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgInPc4d1	pojmenovaný
Battalions	Battalions	k1gInSc4	Battalions
Of	Of	k1gFnSc2	Of
Fear	Feara	k1gFnPc2	Feara
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
již	již	k6eAd1	již
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
současným	současný	k2eAgInSc7d1	současný
názvem	název	k1gInSc7	název
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
:	:	kIx,	:
Hansi	Hans	k1gMnPc7	Hans
Kürsch	Kürsch	k1gInSc1	Kürsch
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Olbrich	Olbrich	k1gMnSc1	Olbrich
(	(	kIx(	(
<g/>
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marcus	Marcus	k1gMnSc1	Marcus
Siepen	Siepen	k2eAgMnSc1d1	Siepen
(	(	kIx(	(
<g/>
kytary	kytara	k1gFnPc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Stauch	Stauch	k1gMnSc1	Stauch
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
složení	složení	k1gNnSc6	složení
kapela	kapela	k1gFnSc1	kapela
úspěšně	úspěšně	k6eAd1	úspěšně
fungovala	fungovat	k5eAaImAgFnS	fungovat
téměř	téměř	k6eAd1	téměř
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
alba	album	k1gNnPc4	album
<g/>
:	:	kIx,	:
Follow	Follow	k1gFnSc1	Follow
The	The	k1gMnSc1	The
Blind	Blind	k1gMnSc1	Blind
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tales	Tales	k1gMnSc1	Tales
From	From	k1gMnSc1	From
The	The	k1gMnSc1	The
Twilight	Twilight	k1gMnSc1	Twilight
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Somewhere	Somewher	k1gInSc5	Somewher
Far	fara	k1gFnPc2	fara
Beyond	Beyond	k1gInSc1	Beyond
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
živák	živák	k1gMnSc1	živák
Tokyo	Tokyo	k1gMnSc1	Tokyo
Tales	Tales	k1gMnSc1	Tales
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Imagination	Imagination	k1gInSc4	Imagination
From	Fromo	k1gNnPc2	Fromo
The	The	k1gMnSc2	The
Other	Othra	k1gFnPc2	Othra
Side	Sid	k1gMnSc2	Sid
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nightfall	Nightfall	k1gMnSc1	Nightfall
In	In	k1gMnSc1	In
Middle-Earth	Middle-Earth	k1gMnSc1	Middle-Earth
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A	a	k8xC	a
Night	Night	k1gMnSc1	Night
At	At	k1gFnSc2	At
The	The	k1gFnSc1	The
Opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Live	Liv	k1gFnSc2	Liv
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
DVD	DVD	kA	DVD
Imagination	Imagination	k1gInSc4	Imagination
Through	Througha	k1gFnPc2	Througha
The	The	k1gFnSc2	The
Looking	Looking	k1gInSc1	Looking
Glass	Glass	k1gInSc1	Glass
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
hudba	hudba	k1gFnSc1	hudba
kapely	kapela	k1gFnSc2	kapela
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
power	power	k1gInSc4	power
a	a	k8xC	a
speed	speed	k1gInSc4	speed
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
z	z	k7c2	z
definic	definice	k1gFnPc2	definice
obou	dva	k4xCgInPc2	dva
žánrů	žánr	k1gInPc2	žánr
silně	silně	k6eAd1	silně
vybočuje	vybočovat	k5eAaImIp3nS	vybočovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
bubeníkem	bubeník	k1gMnSc7	bubeník
kapely	kapela	k1gFnSc2	kapela
Frederik	Frederika	k1gFnPc2	Frederika
Ehmke	Ehmk	k1gFnSc2	Ehmk
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Blind	Blind	k1gMnSc1	Blind
Guardian	Guardian	k1gMnSc1	Guardian
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
A	a	k8xC	a
Twist	twist	k1gInSc1	twist
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Myth	Myth	k1gMnSc1	Myth
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
pod	pod	k7c7	pod
distribucí	distribuce	k1gFnSc7	distribuce
firmy	firma	k1gFnSc2	firma
Nuclear	Nuclear	k1gInSc1	Nuclear
Blast	Blast	k1gFnSc4	Blast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
o	o	k7c4	o
která	který	k3yRgNnPc4	který
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
opírá	opírat	k5eAaImIp3nS	opírat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgInSc1d1	Stephen
King	King	k1gInSc1	King
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bájná	bájný	k2eAgFnSc1d1	bájná
historie	historie	k1gFnSc1	historie
(	(	kIx(	(
<g/>
antické	antický	k2eAgInPc1d1	antický
eposy	epos	k1gInPc1	epos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc1	fantas
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
reflexe	reflexe	k1gFnSc1	reflexe
světových	světový	k2eAgFnPc2d1	světová
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnPc4	poznámka
<g/>
,	,	kIx,	,
zajímavosti	zajímavost	k1gFnPc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
alba	album	k1gNnSc2	album
Nightfall	Nightfalla	k1gFnPc2	Nightfalla
In	In	k1gFnSc7	In
Middle-Earth	Middle-Eartha	k1gFnPc2	Middle-Eartha
se	se	k3xPyFc4	se
frontman	frontman	k1gMnSc1	frontman
kapely	kapela	k1gFnSc2	kapela
Hansi	Hans	k1gMnSc3	Hans
Kürsch	Kürsch	k1gMnSc1	Kürsch
soustředí	soustředit	k5eAaPmIp3nS	soustředit
již	již	k6eAd1	již
jen	jen	k9	jen
na	na	k7c4	na
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
hraje	hrát	k5eAaImIp3nS	hrát
hostující	hostující	k2eAgMnSc1d1	hostující
Oliver	Oliver	k1gMnSc1	Oliver
Holzwarth	Holzwarth	k1gMnSc1	Holzwarth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
hrála	hrát	k5eAaImAgFnS	hrát
kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
na	na	k7c4	na
Fantasy	fantas	k1gInPc4	fantas
Metalfest	Metalfest	k1gFnSc4	Metalfest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
opustil	opustit	k5eAaPmAgMnS	opustit
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
bubeník	bubeník	k1gMnSc1	bubeník
Thomas	Thomas	k1gMnSc1	Thomas
Stauch	Stauch	k1gMnSc1	Stauch
kapelu	kapela	k1gFnSc4	kapela
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
Frederik	Frederik	k1gMnSc1	Frederik
Ehmke	Ehmk	k1gInSc2	Ehmk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nightfall	Nightfall	k1gMnSc1	Nightfall
In	In	k1gMnSc1	In
Middle-Earth	Middle-Earth	k1gMnSc1	Middle-Earth
je	být	k5eAaImIp3nS	být
koncepční	koncepční	k2eAgNnSc4d1	koncepční
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zhudebňuje	zhudebňovat	k5eAaImIp3nS	zhudebňovat
příběh	příběh	k1gInSc1	příběh
díla	dílo	k1gNnSc2	dílo
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blind	Blind	k1gMnSc1	Blind
Guardian	Guardian	k1gMnSc1	Guardian
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
kultovní	kultovní	k2eAgFnSc4d1	kultovní
mimo-mainstreamovou	mimoainstreamový	k2eAgFnSc4d1	mimo-mainstreamový
metalovou	metalový	k2eAgFnSc4d1	metalová
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současná	současný	k2eAgFnSc1d1	současná
sestava	sestava	k1gFnSc1	sestava
===	===	k?	===
</s>
</p>
<p>
<s>
Hansi	Hans	k1gMnSc5	Hans
Kürsch	Kürsch	k1gMnSc1	Kürsch
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
pěti	pět	k4xCc7	pět
albech	album	k1gNnPc6	album
(	(	kIx(	(
<g/>
od	od	k7c2	od
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
André	André	k1gMnSc1	André
Olbrich	Olbrich	k1gMnSc1	Olbrich
–	–	k?	–
kytary	kytara	k1gFnSc2	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Siepen	Siepen	k2eAgMnSc1d1	Siepen
–	–	k?	–
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Frederik	Frederik	k1gMnSc1	Frederik
Ehmke	Ehmk	k1gFnSc2	Ehmk
–	–	k?	–
bicí	bicí	k2eAgInPc1d1	bicí
(	(	kIx(	(
<g/>
od	od	k7c2	od
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Stauch	Stauch	k1gMnSc1	Stauch
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Dork	Dork	k1gMnSc1	Dork
–	–	k?	–
kytary	kytara	k1gFnSc2	kytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Christoff	Christoff	k1gInSc1	Christoff
Theissen	Theissen	k1gInSc1	Theissen
–	–	k?	–
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hans-Peter	Hans-Peter	k1gMnSc1	Hans-Peter
Frey	Frea	k1gFnSc2	Frea
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hostující	hostující	k2eAgMnPc1d1	hostující
hudebníci	hudebník	k1gMnPc1	hudebník
===	===	k?	===
</s>
</p>
<p>
<s>
Kai	Kai	k?	Kai
Hansen	Hansen	k1gInSc1	Hansen
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
Holzwarth	Holzwarth	k1gMnSc1	Holzwarth
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alex	Alex	k1gMnSc1	Alex
Holzwarth	Holzwarth	k1gMnSc1	Holzwarth
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
turné	turné	k1gNnSc4	turné
</s>
</p>
<p>
<s>
Mathias	Mathias	k1gMnSc1	Mathias
Wiesner	Wiesner	k1gMnSc1	Wiesner
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Schüren	Schürna	k1gFnPc2	Schürna
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rolf	Rolf	k1gMnSc1	Rolf
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hackmann	Hackmann	k1gMnSc1	Hackmann
<g/>
,	,	kIx,	,
Olaf	Olaf	k1gMnSc1	Olaf
Senkbeil	Senkbeil	k1gMnSc1	Senkbeil
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc4	Bill
King	Kinga	k1gFnPc2	Kinga
–	–	k?	–
sbory	sbor	k1gInPc4	sbor
(	(	kIx(	(
<g/>
od	od	k7c2	od
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Battalions	Battalions	k1gInSc1	Battalions
of	of	k?	of
Fear	Fear	k1gInSc1	Fear
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Follow	Follow	k?	Follow
the	the	k?	the
Blind	Blind	k1gMnSc1	Blind
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tales	Tales	k1gMnSc1	Tales
from	from	k1gMnSc1	from
the	the	k?	the
Twilight	Twilight	k1gMnSc1	Twilight
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Somewhere	Somewhrat	k5eAaPmIp3nS	Somewhrat
Far	fara	k1gFnPc2	fara
Beyond	Beyond	k1gMnSc1	Beyond
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Imaginations	Imaginations	k6eAd1	Imaginations
from	from	k6eAd1	from
the	the	k?	the
Other	Other	k1gInSc1	Other
Side	Side	k1gFnSc1	Side
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nightfall	Nightfall	k1gMnSc1	Nightfall
in	in	k?	in
Middle-Earth	Middle-Earth	k1gMnSc1	Middle-Earth
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Night	Night	k1gMnSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Twist	twist	k1gInSc1	twist
in	in	k?	in
the	the	k?	the
Myth	Myth	k1gInSc1	Myth
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
At	At	k?	At
the	the	k?	the
Edge	Edge	k1gInSc1	Edge
of	of	k?	of
Time	Time	k1gInSc1	Time
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beyond	Beyond	k1gMnSc1	Beyond
the	the	k?	the
Red	Red	k1gMnSc1	Red
Mirror	Mirror	k1gMnSc1	Mirror
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Legacy	Legaca	k1gFnPc1	Legaca
of	of	k?	of
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
Lands	Lands	k1gInSc1	Lands
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Blind	Blinda	k1gFnPc2	Blinda
Guardian	Guardiany	k1gInPc2	Guardiany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Blind	Blinda	k1gFnPc2	Blinda
Guardian	Guardiany	k1gInPc2	Guardiany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
The	The	k?	The
Releases	Releases	k1gMnSc1	Releases
Of	Of	k1gMnSc1	Of
Blind	Blind	k1gMnSc1	Blind
Guardian	Guardian	k1gMnSc1	Guardian
</s>
</p>
