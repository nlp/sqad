<s>
Oxidační	oxidační	k2eAgFnSc1d1
adice	adice	k1gFnSc1
</s>
<s>
Oxidační	oxidační	k2eAgFnSc1d1
adice	adice	k1gFnSc1
je	být	k5eAaImIp3nS
organická	organický	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
oxidační	oxidační	k2eAgNnSc1d1
i	i	k8xC
koordinační	koordinační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
kovového	kovový	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
opačným	opačný	k2eAgInSc7d1
procesem	proces	k1gInSc7
k	k	k7c3
redukční	redukční	k2eAgFnSc3d1
eliminaci	eliminace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
bývá	bývat	k5eAaImIp3nS
součástí	součást	k1gFnSc7
katalytických	katalytický	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
v	v	k7c6
chemii	chemie	k1gFnSc6
přechodných	přechodný	k2eAgInPc2d1
kovů	kov	k1gInPc2
</s>
<s>
U	u	k7c2
přechodných	přechodný	k2eAgInPc2d1
kovů	kov	k1gInPc2
způsobuje	způsobovat	k5eAaImIp3nS
oxidační	oxidační	k2eAgFnSc1d1
adice	adice	k1gFnSc1
snížení	snížení	k1gNnSc2
počtu	počet	k1gInSc2
elektronů	elektron	k1gInPc2
v	v	k7c6
orbitalu	orbital	k1gInSc6
dn	dn	k?
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
o	o	k7c4
dva	dva	k4xCgInPc4
elektrony	elektron	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxidační	oxidační	k2eAgFnPc4d1
adice	adice	k1gFnPc4
dobře	dobře	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
u	u	k7c2
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
atomy	atom	k1gInPc1
jsou	být	k5eAaImIp3nP
zásadité	zásaditý	k2eAgInPc1d1
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
snadno	snadno	k6eAd1
zoxidovat	zoxidovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovy	kov	k1gInPc4
s	s	k7c7
nízkými	nízký	k2eAgInPc7d1
oxidačními	oxidační	k2eAgInPc7d1
čísly	číslo	k1gNnPc7
často	často	k6eAd1
splňují	splňovat	k5eAaImIp3nP
jeden	jeden	k4xCgInSc4
z	z	k7c2
těchto	tento	k3xDgInPc2
požadavků	požadavek	k1gInPc2
<g/>
;	;	kIx,
oxidační	oxidační	k2eAgFnPc1d1
adice	adice	k1gFnPc1
ovšem	ovšem	k9
mohou	moct	k5eAaImIp3nP
probíhat	probíhat	k5eAaImF
i	i	k9
u	u	k7c2
vyšších	vysoký	k2eAgNnPc2d2
oxidačních	oxidační	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Formální	formální	k2eAgNnSc1d1
oxidační	oxidační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
kovu	kov	k1gInSc2
i	i	k8xC
počet	počet	k1gInSc1
elektronů	elektron	k1gInPc2
v	v	k7c6
komplexu	komplex	k1gInSc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
zvýší	zvýšit	k5eAaPmIp3nS
o	o	k7c4
dva	dva	k4xCgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
také	také	k9
docházet	docházet	k5eAaImF
ke	k	k7c3
změnám	změna	k1gFnPc3
o	o	k7c4
jeden	jeden	k4xCgInSc4
elektron	elektron	k1gInSc4
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
oxidační	oxidační	k2eAgFnPc1d1
adice	adice	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
jako	jako	k9
řetězce	řetězec	k1gInPc1
jednoelektronových	jednoelektronový	k2eAgFnPc2d1
reakcí	reakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
oxidační	oxidační	k2eAgFnSc3d1
adici	adice	k1gFnSc3
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
při	při	k7c6
začlenění	začlenění	k1gNnSc6
kovů	kov	k1gInPc2
do	do	k7c2
struktur	struktura	k1gFnPc2
řady	řada	k1gFnSc2
různých	různý	k2eAgInPc2d1
substrátů	substrát	k1gInPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
jde	jít	k5eAaImIp3nS
o	o	k7c6
H	H	kA
<g/>
–	–	k?
<g/>
H	H	kA
<g/>
,	,	kIx,
H	H	kA
<g/>
–	–	k?
<g/>
X	X	kA
a	a	k8xC
C	C	kA
<g/>
–	–	k?
<g/>
X	X	kA
<g/>
,	,	kIx,
protože	protože	k8xS
takové	takový	k3xDgFnPc1
sloučeniny	sloučenina	k1gFnPc1
mají	mít	k5eAaImIp3nP
největší	veliký	k2eAgNnSc4d3
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
provedení	provedení	k1gNnSc3
oxidační	oxidační	k2eAgFnSc2d1
adice	adice	k1gFnSc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měl	mít	k5eAaImAgInS
komplex	komplex	k1gInSc4
volné	volný	k2eAgNnSc4d1
koordinační	koordinační	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
tyto	tento	k3xDgFnPc4
reakce	reakce	k1gFnPc4
nejčastěji	často	k6eAd3
probíhají	probíhat	k5eAaImIp3nP
u	u	k7c2
komplexů	komplex	k1gInPc2
s	s	k7c7
koordinačními	koordinační	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
4	#num#	k4
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Opačnou	opačný	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
je	být	k5eAaImIp3nS
redukční	redukční	k2eAgFnSc1d1
eliminace	eliminace	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
probíhá	probíhat	k5eAaImIp3nS
přednostně	přednostně	k6eAd1
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
případná	případný	k2eAgFnSc1d1
vznikající	vznikající	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
X-Y	X-Y	k1gFnSc1
dostatečně	dostatečně	k6eAd1
silná	silný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
redukční	redukční	k2eAgFnSc2d1
eliminace	eliminace	k1gFnSc2
by	by	k9
dvě	dva	k4xCgFnPc4
skupiny	skupina	k1gFnPc4
(	(	kIx(
<g/>
označené	označený	k2eAgFnPc4d1
X	X	kA
a	a	k8xC
Y	Y	kA
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
na	na	k7c6
sousedních	sousední	k2eAgFnPc6d1
pozicích	pozice	k1gFnPc6
vzhledem	vzhledem	k7c3
ke	k	k7c3
koordinační	koordinační	k2eAgFnSc3d1
sféře	sféra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukční	redukční	k2eAgFnSc1d1
eliminace	eliminace	k1gFnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
mnoha	mnoho	k4c2
reakcí	reakce	k1gFnPc2
vytvářejících	vytvářející	k2eAgFnPc2d1
vazby	vazba	k1gFnPc4
C	C	kA
<g/>
–	–	k?
<g/>
H	H	kA
a	a	k8xC
C	C	kA
<g/>
–	–	k?
<g/>
C	C	kA
krokem	krokem	k6eAd1
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
vzniká	vznikat	k5eAaImIp3nS
výsledný	výsledný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mechanismus	mechanismus	k1gInSc1
</s>
<s>
Oxidační	oxidační	k2eAgFnSc1d1
adice	adice	k1gFnSc1
může	moct	k5eAaImIp3nS
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
vlastnostech	vlastnost	k1gFnPc6
kovového	kovový	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
a	a	k8xC
substrátu	substrát	k1gInSc2
probíhat	probíhat	k5eAaImF
několika	několik	k4yIc7
různými	různý	k2eAgInPc7d1
mechanismy	mechanismus	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Soustředěný	soustředěný	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
</s>
<s>
Oxidační	oxidační	k2eAgFnSc1d1
adice	adice	k1gFnSc1
nepolárních	polární	k2eNgInPc2d1
substrátů	substrát	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
vodík	vodík	k1gInSc4
a	a	k8xC
uhlovodíky	uhlovodík	k1gInPc4
<g/>
,	,	kIx,
probíhají	probíhat	k5eAaImIp3nP
soustředěným	soustředěný	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovéto	takovýto	k3xDgInPc4
substráty	substrát	k1gInPc4
nemají	mít	k5eNaImIp3nP
vazby	vazba	k1gFnPc1
π	π	k?
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yQnSc2,k3yRnSc2
vzniká	vznikat	k5eAaImIp3nS
sigma	sigma	k1gNnSc1
komplex	komplex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
zániku	zánik	k1gInSc3
koordinační	koordinační	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
u	u	k7c2
ligandu	ligand	k1gInSc2
přenosem	přenos	k1gInSc7
elektronů	elektron	k1gInPc2
do	do	k7c2
σ	σ	k?
<g/>
*	*	kIx~
orbitalů	orbital	k1gInPc2
této	tento	k3xDgFnSc2
vazby	vazba	k1gFnSc2
za	za	k7c2
tvorby	tvorba	k1gFnSc2
oxidovaného	oxidovaný	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
<g/>
,	,	kIx,
Výsledné	výsledný	k2eAgInPc1d1
ligandy	ligand	k1gInPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
v	v	k7c6
poloze	poloha	k1gFnSc6
cis	cis	k1gNnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
objevit	objevit	k5eAaPmF
izomerizace	izomerizace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
mechanismus	mechanismus	k1gInSc1
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
při	při	k7c6
adicích	adice	k1gFnPc6
homonukleárních	homonukleární	k2eAgFnPc2d1
dvouatomových	dvouatomový	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
jako	jako	k9
je	být	k5eAaImIp3nS
H	H	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
C	C	kA
<g/>
–	–	k?
<g/>
H	H	kA
aktivačních	aktivační	k2eAgFnPc2d1
reakcí	reakce	k1gFnPc2
má	mít	k5eAaImIp3nS
také	také	k9
soustředěný	soustředěný	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
agostického	agostický	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
(	(	kIx(
<g/>
M	M	kA
<g/>
–	–	k?
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
–	–	k?
<g/>
H	H	kA
<g/>
))	))	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
reakce	reakce	k1gFnSc1
vodíku	vodík	k1gInSc2
s	s	k7c7
Vaskovým	Vaskův	k2eAgInSc7d1
komplexem	komplex	k1gInSc7
(	(	kIx(
<g/>
trans-IrCl	trans-IrCl	k1gInSc1
<g/>
(	(	kIx(
<g/>
CO	co	k3yQnSc1,k3yInSc1,k3yRnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
P	P	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxidační	oxidační	k2eAgInSc4d1
číslo	číslo	k1gNnSc1
iridia	iridium	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
mění	měnit	k5eAaImIp3nS
z	z	k7c2
+1	+1	k4
na	na	k7c4
+3	+3	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Produkt	produkt	k1gInSc1
je	být	k5eAaImIp3nS
navázán	navázán	k2eAgInSc1d1
na	na	k7c4
tři	tři	k4xCgInPc4
anionty	anion	k1gInPc4
<g/>
:	:	kIx,
jeden	jeden	k4xCgInSc1
chloridový	chloridový	k2eAgInSc1d1
a	a	k8xC
dva	dva	k4xCgInPc4
hydridové	hydridový	k2eAgInPc4d1
ligandy	ligand	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
měl	mít	k5eAaImAgInS
16	#num#	k4
elektronů	elektron	k1gInPc2
a	a	k8xC
koordinační	koordinační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
4	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
produkt	produkt	k1gInSc1
má	mít	k5eAaImIp3nS
18	#num#	k4
elektronů	elektron	k1gInPc2
a	a	k8xC
koordinační	koordinační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
tvorbě	tvorba	k1gFnSc6
trigonálně	trigonálně	k6eAd1
bipyramidového	bipyramidový	k2eAgInSc2d1
meziproduktu	meziprodukt	k1gInSc2
následuje	následovat	k5eAaImIp3nS
v	v	k7c6
důsledku	důsledek	k1gInSc6
dodání	dodání	k1gNnSc2
elektronů	elektron	k1gInPc2
do	do	k7c2
orbitalu	orbital	k1gInSc2
σ	σ	k?
<g/>
*	*	kIx~
vazby	vazba	k1gFnSc2
H	H	kA
<g/>
–	–	k?
<g/>
H	H	kA
její	její	k3xOp3gNnSc4
rozštěpení	rozštěpení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
s	s	k7c7
oxidační	oxidační	k2eAgFnSc7d1
adicí	adice	k1gFnSc7
zde	zde	k6eAd1
také	také	k9
probíhá	probíhat	k5eAaImIp3nS
redukční	redukční	k2eAgFnSc1d1
eliminace	eliminace	k1gFnSc1
jejího	její	k3xOp3gInSc2
produktu	produkt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díky	díky	k7c3
dodávání	dodávání	k1gNnSc3
elektronů	elektron	k1gInPc2
do	do	k7c2
orbitalu	orbital	k1gInSc2
σ	σ	k?
<g/>
*	*	kIx~
vazby	vazba	k1gFnSc2
H	H	kA
<g/>
–	–	k?
<g/>
H	H	kA
tato	tento	k3xDgFnSc1
reakce	reakce	k1gFnPc4
převažuje	převažovat	k5eAaImIp3nS
u	u	k7c2
kovů	kov	k1gInPc2
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
elektronovou	elektronový	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Soustředěným	soustředěný	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
vzniká	vznikat	k5eAaImIp3nS
cis	cis	k1gNnSc1
dihydrid	dihydrida	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
jiných	jiný	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
oxidační	oxidační	k2eAgFnSc2d1
adice	adice	k1gFnSc2
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
cis	cis	k1gNnSc2
produkty	produkt	k1gInPc1
netvoří	tvořit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
SN2	SN2	k4
mechanismus	mechanismus	k1gInSc1
</s>
<s>
Některé	některý	k3yIgFnPc1
oxidační	oxidační	k2eAgFnPc1d1
adice	adice	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
podobně	podobně	k6eAd1
jako	jako	k9
bimolekulární	bimolekulární	k2eAgFnSc1d1
nukleofilní	nukleofilní	k2eAgFnSc1d1
substituce	substituce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovové	kovový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
je	být	k5eAaImIp3nS
atakováno	atakovat	k5eAaBmNgNnS
elektropozitivnějším	elektropozitivný	k2eAgInSc7d2
atomem	atom	k1gInSc7
substrátu	substrát	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
zaniká	zanikat	k5eAaImIp3nS
vazba	vazba	k1gFnSc1
R	R	kA
<g/>
–	–	k?
<g/>
X	X	kA
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
se	se	k3xPyFc4
meziprodukt	meziprodukt	k1gInSc1
se	s	k7c7
vzorcem	vzorec	k1gInSc7
[	[	kIx(
<g/>
M	M	kA
<g/>
–	–	k?
<g/>
R	R	kA
<g/>
]	]	kIx)
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
koordinaci	koordinace	k1gFnSc3
aniontu	anion	k1gInSc2
na	na	k7c4
kation	kation	k1gInSc4
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
reakce	reakce	k1gFnSc1
čtvercového	čtvercový	k2eAgInSc2d1
rovinného	rovinný	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
s	s	k7c7
jodmethanem	jodmethan	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
Tento	tento	k3xDgInSc1
mechanismus	mechanismus	k1gInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
u	u	k7c2
polárních	polární	k2eAgInPc2d1
a	a	k8xC
elektrofilních	elektrofilní	k2eAgInPc2d1
substrátů	substrát	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
alkylhalogenidy	alkylhalogenida	k1gFnPc4
a	a	k8xC
halogeny	halogen	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iontový	iontový	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
</s>
<s>
Iontový	iontový	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
oxidační	oxidační	k2eAgFnSc2d1
adice	adice	k1gFnSc2
je	být	k5eAaImIp3nS
podobný	podobný	k2eAgMnSc1d1
SN2	SN2	k1gMnSc1
mechanismu	mechanismus	k1gInSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
při	při	k7c6
něm	on	k3xPp3gMnSc6
postupně	postupně	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
adici	adice	k1gFnSc6
dvou	dva	k4xCgFnPc2
různých	různý	k2eAgFnPc2d1
částí	část	k1gFnPc2
ligandu	ligand	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
iontového	iontový	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
disociaci	disociace	k1gFnSc3
substrátu	substrát	k1gInSc2
ještě	ještě	k9
před	před	k7c7
jakýmikoliv	jakýkoliv	k3yIgFnPc7
interakcemi	interakce	k1gFnPc7
s	s	k7c7
kovovým	kovový	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
například	například	k6eAd1
adice	adice	k1gFnSc1
kyseliny	kyselina	k1gFnSc2
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radikálový	radikálový	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
</s>
<s>
Kromě	kromě	k7c2
SN2	SN2	k1gFnSc2
mechanismu	mechanismus	k1gInSc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
alkylhalogenidy	alkylhalogenida	k1gFnPc1
a	a	k8xC
podobné	podobný	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
adovat	adovat	k5eAaImF,k5eAaPmF,k5eAaBmF
na	na	k7c4
kovová	kovový	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
radikálově	radikálově	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
takové	takový	k3xDgFnPc1
reakce	reakce	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
u	u	k7c2
Pt	Pt	k1gFnSc2
<g/>
0	#num#	k4
<g/>
-RX	-RX	k?
<g/>
,	,	kIx,
Pt	Pt	k1gFnSc1
<g/>
0	#num#	k4
<g/>
-C	-C	k?
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
Br	br	k0
a	a	k8xC
PtII-R	PtII-R	k1gMnSc3
<g/>
′	′	k?
<g/>
SO	So	kA
<g/>
2	#num#	k4
<g/>
X	X	kA
(	(	kIx(
<g/>
R	R	kA
=	=	kIx~
alkyl	alkyl	k1gInSc1
<g/>
,	,	kIx,
R	R	kA
<g/>
′	′	k?
=	=	kIx~
aryl	aryl	k1gMnSc1
<g/>
,	,	kIx,
X	X	kA
=	=	kIx~
halogenid	halogenid	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Iniciace	iniciace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
<g/>
N	N	kA
<g/>
]	]	kIx)
<g/>
2	#num#	k4
→	→	k?
2	#num#	k4
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
•	•	k?
+	+	kIx~
N2	N2	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
•	•	k?
+	+	kIx~
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
Br	br	k0
→	→	k?
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
<g/>
CBr	CBr	k1gMnSc1
+	+	kIx~
Ph	Ph	kA
<g/>
•	•	k?
</s>
<s>
Propagace	propagace	k1gFnSc1
</s>
<s>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
•	•	k?
+	+	kIx~
{	{	kIx(
<g/>
Pt	Pt	k1gMnSc1
<g/>
[	[	kIx(
<g/>
P	P	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
}	}	kIx)
→	→	k?
{	{	kIx(
<g/>
Pt	Pt	k1gMnSc1
<g/>
[	[	kIx(
<g/>
P	P	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
}	}	kIx)
<g/>
•	•	k?
</s>
<s>
{	{	kIx(
<g/>
Pt	Pt	k1gMnSc1
<g/>
[	[	kIx(
<g/>
P	P	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
}	}	kIx)
<g/>
•	•	k?
+	+	kIx~
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
Br	br	k0
→	→	k?
{	{	kIx(
<g/>
Pt	Pt	k1gMnSc1
<g/>
[	[	kIx(
<g/>
P	P	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
Br	br	k0
<g/>
}	}	kIx)
+	+	kIx~
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
•	•	k?
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Oxidační	oxidační	k2eAgFnPc1d1
adice	adice	k1gFnPc1
a	a	k8xC
redukční	redukční	k2eAgFnPc1d1
eliminace	eliminace	k1gFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
mnoha	mnoho	k4c2
procesů	proces	k1gInPc2
při	při	k7c6
homogenní	homogenní	k2eAgFnSc6d1
(	(	kIx(
<g/>
například	například	k6eAd1
hydrogenace	hydrogenace	k1gFnSc1
alkenů	alken	k1gMnPc2
s	s	k7c7
použitím	použití	k1gNnSc7
Wilkinsonova	Wilkinsonův	k2eAgInSc2d1
katalyzátoru	katalyzátor	k1gInSc2
<g/>
)	)	kIx)
i	i	k9
heterogenní	heterogenní	k2eAgFnSc6d1
katalýze	katalýza	k1gFnSc6
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
hydrogenace	hydrogenace	k1gFnSc1
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
platiny	platina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxidační	oxidační	k2eAgFnPc4d1
adice	adice	k1gFnPc4
jsou	být	k5eAaImIp3nP
také	také	k9
nutné	nutný	k2eAgNnSc4d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
nukleofilní	nukleofilní	k2eAgFnSc4d1
adici	adice	k1gFnSc4
na	na	k7c4
alkylové	alkylový	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
významnou	významný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
procesů	proces	k1gInPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Suzukiho	Suzuki	k1gMnSc4
reakce	reakce	k1gFnSc1
<g/>
,	,	kIx,
Negišiho	Negiši	k1gMnSc2
reakce	reakce	k1gFnSc2
a	a	k8xC
Sonogašiho	Sonogaši	k1gMnSc2
reakce	reakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Oxidative	Oxidativ	k1gInSc5
addition	addition	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
J.	J.	kA
F.	F.	kA
Hartwig	Hartwig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organotransition	Organotransition	k1gInSc1
Metal	metat	k5eAaImAgInS
Chemistry	Chemistr	k1gMnPc4
<g/>
,	,	kIx,
from	from	k6eAd1
Bonding	Bonding	k1gInSc1
to	ten	k3xDgNnSc4
Catalysis	Catalysis	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
University	universita	k1gFnPc1
Science	Science	k1gFnSc2
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
891389	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Robert	Robert	k1gMnSc1
Crabtree	Crabtre	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Organometallic	Organometallice	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
of	of	k?
the	the	k?
Transition	Transition	k1gInSc1
Metals	Metals	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wiley-Interscience	Wiley-Interscience	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
66256	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
159	#num#	k4
<g/>
–	–	k?
<g/>
180	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Curtis	Curtis	k1gFnPc2
Johnson	Johnson	k1gMnSc1
<g/>
;	;	kIx,
Richard	Richard	k1gMnSc1
Eisenberg	Eisenberg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stereoselective	Stereoselectiv	k1gInSc5
Oxidative	Oxidativ	k1gInSc5
Addition	Addition	k1gInSc4
of	of	k?
Hydrogen	Hydrogen	k1gInSc1
to	ten	k3xDgNnSc4
Iridium	iridium	k1gNnSc4
<g/>
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
Complexes	Complexes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kinetic	Kinetice	k1gFnPc2
Control	Controla	k1gFnPc2
Based	Based	k1gInSc1
on	on	k3xPp3gMnSc1
Ligand	Ligando	k1gNnPc2
Electronic	Electronice	k1gFnPc2
Effects	Effectsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
1985	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3148	#num#	k4
<g/>
–	–	k?
<g/>
3160	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ja	ja	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
297	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Thomas	Thomas	k1gMnSc1
L.	L.	kA
Hall	Hall	k1gMnSc1
<g/>
;	;	kIx,
Michael	Michael	k1gMnSc1
F.	F.	kA
Lappert	Lappert	k1gMnSc1
<g/>
;	;	kIx,
Peter	Peter	k1gMnSc1
W.	W.	kA
Lednor	Lednor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanistic	Mechanistice	k1gFnPc2
studies	studiesa	k1gFnPc2
of	of	k?
some	somat	k5eAaPmIp3nS
oxidative-addition	oxidative-addition	k1gInSc1
reactions	reactions	k1gInSc1
<g/>
:	:	kIx,
free-radical	free-radicat	k5eAaPmAgInS
pathways	pathways	k1gInSc1
in	in	k?
the	the	k?
Pt	Pt	k1gFnSc7
<g/>
0	#num#	k4
<g/>
-RX	-RX	k?
<g/>
,	,	kIx,
Pt	Pt	k1gFnSc1
<g/>
0	#num#	k4
<g/>
-PhBr	-PhBra	k1gFnPc2
<g/>
,	,	kIx,
and	and	k?
PtII-R	PtII-R	k1gFnSc2
<g/>
′	′	k?
<g/>
SO	So	kA
<g/>
2	#num#	k4
<g/>
X	X	kA
Reactions	Reactionsa	k1gFnPc2
(	(	kIx(
<g/>
R	R	kA
=	=	kIx~
alkyl	alkyl	k1gInSc1
<g/>
,	,	kIx,
R	R	kA
<g/>
′	′	k?
=	=	kIx~
aryl	aryl	k1gMnSc1
<g/>
,	,	kIx,
X	X	kA
=	=	kIx~
halide	halid	k1gInSc5
<g/>
)	)	kIx)
and	and	k?
in	in	k?
the	the	k?
related	related	k1gInSc1
rhodium	rhodium	k1gNnSc1
<g/>
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
or	or	k?
iridium	iridium	k1gNnSc1
<g/>
(	(	kIx(
<g/>
I	i	k9
<g/>
)	)	kIx)
Systems	Systems	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
Chemical	Chemical	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
Dalton	Dalton	k1gInSc1
Transactions	Transactions	k1gInSc1
<g/>
.	.	kIx.
1980	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1448	#num#	k4
<g/>
–	–	k?
<g/>
1456	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
DT	DT	kA
<g/>
9800001448	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
