Oxidační	oxidační	k2eAgFnSc1d1
adice	adice	k1gFnSc1
je	být	k5eAaImIp3nS
organická	organický	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
oxidační	oxidační	k2eAgNnSc1d1
i	i	k8xC
koordinační	koordinační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
kovového	kovový	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
