<s>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1883	#num#	k4
<g/>
Fukov	Fukovo	k1gNnPc2
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
76	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Kempten	Kemptno	k1gNnPc2
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Povolání	povolání	k1gNnSc3
</s>
<s>
celník	celník	k1gMnSc1
a	a	k8xC
horský	horský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Znám	znát	k5eAaImIp1nS
jako	jako	k9
</s>
<s>
alpinista	alpinista	k1gMnSc1
<g/>
,	,	kIx,
horolezec	horolezec	k1gMnSc1
<g/>
,	,	kIx,
sáňkař	sáňkař	k1gMnSc1
a	a	k8xC
lyžař	lyžař	k1gMnSc1
Děti	dítě	k1gFnPc4
</s>
<s>
syn	syn	k1gMnSc1
Manfred	Manfred	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Anton	Anton	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
a	a	k8xC
Marie	Marie	k1gFnSc1
Wünsche	Wünsch	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
1914	#num#	k4
Reichenberg	Reichenberg	k1gInSc1
</s>
<s>
muži	muž	k1gMnPc1
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1914	#num#	k4
Reichenberg	Reichenberg	k1gInSc1
</s>
<s>
muži	muž	k1gMnPc1
dvojice	dvojice	k1gFnSc2
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1928	#num#	k4
Schreiberhau	Schreiberhaus	k1gInSc2
</s>
<s>
muži	muž	k1gMnPc1
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
1929	#num#	k4
Semmering	Semmering	k1gInSc1
</s>
<s>
muži	muž	k1gMnPc1
dvojice	dvojice	k1gFnSc2
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
<g/>
,	,	kIx,
křtěný	křtěný	k2eAgMnSc1d1
Rudolf	Rudolf	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1883	#num#	k4
<g/>
,	,	kIx,
Fukov	Fukov	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
<g/>
,	,	kIx,
Kempten	Kempten	k2eAgInSc1d1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
osobností	osobnost	k1gFnPc2
německých	německý	k2eAgInPc2d1
alpských	alpský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
celé	celý	k2eAgFnPc4d1
severní	severní	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
a	a	k8xC
zejména	zejména	k9
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
je	být	k5eAaImIp3nS
významným	významný	k2eAgMnSc7d1
objevitelem	objevitel	k1gMnSc7
a	a	k8xC
průkopníkem	průkopník	k1gMnSc7
horolezectví	horolezectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
sportovcem	sportovec	k1gMnSc7
<g/>
,	,	kIx,
turistou	turista	k1gMnSc7
<g/>
,	,	kIx,
horolezcem	horolezec	k1gMnSc7
<g/>
,	,	kIx,
publicistu	publicista	k1gMnSc4
a	a	k8xC
spisovatelem	spisovatel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
kniha	kniha	k1gFnSc1
„	„	k?
<g/>
Wandern	Wandern	k1gInSc1
und	und	k?
Klettern	Klettern	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Turistika	turistika	k1gFnSc1
a	a	k8xC
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
základním	základní	k2eAgNnSc7d1
topografickým	topografický	k2eAgNnSc7d1
a	a	k8xC
horolezeckým	horolezecký	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
o	o	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
a	a	k8xC
Lužických	lužický	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1883	#num#	k4
v	v	k7c6
obci	obec	k1gFnSc6
Fukov	Fukovo	k1gNnPc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Fugau	Fuga	k2eAgFnSc4d1
<g/>
)	)	kIx)
ve	v	k7c6
Šluknovském	šluknovský	k2eAgInSc6d1
výběžku	výběžek	k1gInSc6
Antonu	Anton	k1gMnSc3
Kauschkovi	Kauschek	k1gMnSc3
a	a	k8xC
Marii	Maria	k1gFnSc3
Wünsche	Wünsch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
kvůli	kvůli	k7c3
zaměstnání	zaměstnání	k1gNnSc3
celního	celní	k2eAgMnSc2d1
úředníka	úředník	k1gMnSc2
Antona	Anton	k1gMnSc2
Kauschky	Kauschka	k1gMnSc2
stěhovala	stěhovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladý	mladý	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
přestěhování	přestěhování	k1gNnSc6
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1895	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
Bílého	bílý	k2eAgInSc2d1
Potoka	potok	k1gInSc2
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
do	do	k7c2
krajiny	krajina	k1gFnSc2
plné	plný	k2eAgFnSc2d1
skal	skála	k1gFnPc2
zamiloval	zamilovat	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Zde	zde	k6eAd1
jsem	být	k5eAaImIp1nS
našel	najít	k5eAaPmAgMnS
všechno	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k9
mi	já	k3xPp1nSc3
leželo	ležet	k5eAaImAgNnS
na	na	k7c6
srdci	srdce	k1gNnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
jsem	být	k5eAaImIp1nS
našel	najít	k5eAaPmAgMnS
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
studií	studio	k1gNnPc2
se	se	k3xPyFc4
přátelil	přátelit	k5eAaImAgMnS
s	s	k7c7
Otto	Otto	k1gMnSc1
Steppesem	Steppes	k1gMnSc7
<g/>
,	,	kIx,
Rudolfem	Rudolf	k1gMnSc7
Scholzem	Scholz	k1gMnSc7
a	a	k8xC
Rudolfem	Rudolf	k1gMnSc7
Hiebelem	Hiebel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
1902	#num#	k4
odmaturoval	odmaturovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostatek	nedostatek	k1gInSc1
pracovních	pracovní	k2eAgFnPc2d1
příležitostí	příležitost	k1gFnPc2
ho	on	k3xPp3gInSc2
přiměl	přimět	k5eAaPmAgInS
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
armády	armáda	k1gFnSc2
k	k	k7c3
pluku	pluk	k1gInSc3
Feldjäger	Feldjägra	k1gFnPc2
<g/>
–	–	k?
<g/>
Bataillon	Bataillon	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
roce	rok	k1gInSc6
stal	stát	k5eAaPmAgMnS
rezervistou	rezervista	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
února	únor	k1gInSc2
1904	#num#	k4
byl	být	k5eAaImAgInS
zaměstnán	zaměstnat	k5eAaPmNgMnS
ve	v	k7c6
vydavatelství	vydavatelství	k1gNnSc6
Deutsche	Deutsch	k1gFnSc2
Volkszeitung	Volkszeitunga	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mu	on	k3xPp3gInSc3
také	také	k9
zveřejnili	zveřejnit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gInPc1
první	první	k4xOgInPc1
turistické	turistický	k2eAgInPc1d1
články	článek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
tentýž	týž	k3xTgInSc4
rok	rok	k1gInSc4
vydavatelství	vydavatelství	k1gNnSc2
opustil	opustit	k5eAaPmAgMnS
a	a	k8xC
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c6
hlavním	hlavní	k2eAgInSc6d1
celním	celní	k2eAgInSc6d1
úřadě	úřad	k1gInSc6
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
jako	jako	k9
řadový	řadový	k2eAgMnSc1d1
člen	člen	k1gMnSc1
do	do	k7c2
Liberecké	liberecký	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
DuÖAV	DuÖAV	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
později	pozdě	k6eAd2
zastával	zastávat	k5eAaImAgMnS
různé	různý	k2eAgFnPc4d1
vedoucí	vedoucí	k1gFnPc4
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Německého	německý	k2eAgInSc2d1
horského	horský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
pro	pro	k7c4
Ještědské	ještědský	k2eAgFnPc4d1
a	a	k8xC
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
/	/	kIx~
<g/>
Deutscher	Deutschra	k1gFnPc2
Gebirgsverein	Gebirgsverein	k1gInSc1
für	für	k?
das	das	k?
Jeschken	Jeschken	k1gInSc1
und	und	k?
Isergebirge	Isergebirge	k1gInSc1
(	(	kIx(
<g/>
DGJI	DGJI	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
vojenského	vojenský	k2eAgNnSc2d1
cvičení	cvičení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
Jižního	jižní	k2eAgNnSc2d1
Tyrolska	Tyrolsko	k1gNnSc2
a	a	k8xC
vylezl	vylézt	k5eAaPmAgMnS
první	první	k4xOgInPc4
vysokohorské	vysokohorský	k2eAgInPc4d1
výstupy	výstup	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hauptplatz	Hauptplatz	k1gInSc1
v	v	k7c6
St.	st.	kA
Johannu	Johann	k1gInSc2
v	v	k7c6
Tyrolsku	Tyrolsko	k1gNnSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kempten	Kempten	k2eAgMnSc1d1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1909	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
dcerou	dcera	k1gFnSc7
obchodníka	obchodník	k1gMnSc2
<g/>
,	,	kIx,
majitele	majitel	k1gMnSc2
tkalcovské	tkalcovský	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
a	a	k8xC
komerčního	komerční	k2eAgMnSc2d1
rady	rada	k1gMnSc2
Stefana	Stefan	k1gMnSc2
Wenzla	Wenzla	k1gMnSc2
Mathildou	Mathilda	k1gMnSc7
Wenzel	Wenzel	k1gFnSc2
z	z	k7c2
Dolního	dolní	k2eAgInSc2d1
Hanychova	Hanychův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
se	se	k3xPyFc4
manželům	manžel	k1gMnPc3
narodil	narodit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Manfred	Manfred	k1gMnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
dcera	dcera	k1gFnSc1
Elisabeth	Elisabetha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
byl	být	k5eAaImAgMnS
povolán	povolat	k5eAaPmNgMnS
na	na	k7c4
vojenský	vojenský	k2eAgInSc4d1
lyžařský	lyžařský	k2eAgInSc4d1
výcvikový	výcvikový	k2eAgInSc4d1
kurz	kurz	k1gInSc4
do	do	k7c2
St.	st.	kA
Johannu	Johann	k1gInSc2
v	v	k7c6
Tyrolsku	Tyrolsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
popsal	popsat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
nádherné	nádherný	k2eAgInPc4d1
týdny	týden	k1gInPc4
plné	plný	k2eAgFnSc2d1
radosti	radost	k1gFnSc2
z	z	k7c2
lyžování	lyžování	k1gNnSc2
v	v	k7c6
Kitzbühlských	Kitzbühlský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zranění	zranění	k1gNnSc6
kolena	koleno	k1gNnSc2
byl	být	k5eAaImAgMnS
poslán	poslat	k5eAaPmNgMnS
na	na	k7c4
léčení	léčení	k1gNnSc4
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
absolvování	absolvování	k1gNnSc6
nastoupil	nastoupit	k5eAaPmAgInS
vůdcovský	vůdcovský	k2eAgInSc1d1
kurz	kurz	k1gInSc1
v	v	k7c6
Bolzanu	Bolzan	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
své	svůj	k3xOyFgInPc4
vynikající	vynikající	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
si	se	k3xPyFc3
mohl	moct	k5eAaImAgMnS
vybrat	vybrat	k5eAaPmF
frontu	fronta	k1gFnSc4
a	a	k8xC
zvolil	zvolit	k5eAaPmAgMnS
oblast	oblast	k1gFnSc4
Ortleru	Ortler	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frontové	frontový	k2eAgInPc1d1
zážitky	zážitek	k1gInPc1
zpracoval	zpracovat	k5eAaPmAgMnS
do	do	k7c2
přednášky	přednáška	k1gFnSc2
„	„	k?
<g/>
Z	z	k7c2
fronty	fronta	k1gFnSc2
na	na	k7c6
Ortleru	Ortler	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válce	válka	k1gFnSc6
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
vyznamenání	vyznamenání	k1gNnSc4
za	za	k7c4
statečné	statečný	k2eAgNnSc4d1
chování	chování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
neuměl	umět	k5eNaImAgMnS
česky	česky	k6eAd1
a	a	k8xC
to	ten	k3xDgNnSc4
mu	on	k3xPp3gNnSc3
po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
a	a	k8xC
zvláště	zvláště	k6eAd1
po	po	k7c6
jazykovém	jazykový	k2eAgInSc6d1
zákonu	zákon	k1gInSc6
činilo	činit	k5eAaImAgNnS
ve	v	k7c6
služebním	služební	k2eAgInSc6d1
postupu	postup	k1gInSc6
potíže	potíž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jeho	jeho	k3xOp3gNnPc2
vyjádření	vyjádření	k1gNnPc2
se	se	k3xPyFc4
cítil	cítit	k5eAaImAgMnS
jako	jako	k9
člověk	člověk	k1gMnSc1
„	„	k?
<g/>
druhé	druhý	k4xOgFnSc2
kategorie	kategorie	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
byl	být	k5eAaImAgInS
služebně	služebně	k6eAd1
degradován	degradován	k2eAgInSc1d1
a	a	k8xC
poté	poté	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
z	z	k7c2
německého	německý	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Liberce	Liberec	k1gInSc2
přeložen	přeložen	k2eAgInSc4d1
do	do	k7c2
zcela	zcela	k6eAd1
českého	český	k2eAgInSc2d1
Kolína	Kolín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
výslužby	výslužba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kauschka	Kauschek	k1gInSc2
Mnichov	Mnichov	k1gInSc1
přivítal	přivítat	k5eAaPmAgInS
a	a	k8xC
přiřazení	přiřazení	k1gNnSc4
Sudet	Sudety	k1gFnPc2
vnímal	vnímat	k5eAaImAgInS
jako	jako	k9
osvobození	osvobození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc1
německý	německý	k2eAgInSc1d1
nacionální	nacionální	k2eAgInSc1d1
postoj	postoj	k1gInSc1
a	a	k8xC
neustálé	neustálý	k2eAgNnSc1d1
ponižování	ponižování	k1gNnSc1
českým	český	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
popsal	popsat	k5eAaPmAgMnS
v	v	k7c6
článku	článek	k1gInSc6
„	„	k?
<g/>
Befreites	Befreites	k1gInSc1
Sudetenland	Sudetenland	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
básní	báseň	k1gFnPc2
na	na	k7c4
stejné	stejný	k2eAgNnSc4d1
téma	téma	k1gNnSc4
poslal	poslat	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
poděkování	poděkování	k1gNnSc4
„	„	k?
<g/>
vůdci	vůdce	k1gMnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
německému	německý	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Říši	říš	k1gFnSc6
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
očekávané	očekávaný	k2eAgFnSc3d1
politické	politický	k2eAgFnPc4d1
a	a	k8xC
pracovní	pracovní	k2eAgNnSc1d1
zlepšení	zlepšení	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
Liberci	Liberec	k1gInSc6
nedostavilo	dostavit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
byly	být	k5eAaImAgFnP
přidělovány	přidělovat	k5eAaImNgFnP
říšským	říšský	k2eAgMnPc3d1
Němcům	Němec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kauschka	Kauschek	k1gInSc2
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
celního	celní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
názorech	názor	k1gInPc6
vystřízlivěl	vystřízlivět	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nadřízení	nadřízený	k1gMnPc1
z	z	k7c2
Říše	říš	k1gFnSc2
zacházeli	zacházet	k5eAaImAgMnP
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
podřízenými	podřízený	k1gMnPc7
ze	z	k7c2
Sudet	Sudety	k1gFnPc2
úplně	úplně	k6eAd1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
před	před	k7c7
tím	ten	k3xDgNnSc7
Češi	Čech	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kauschka	Kauschka	k1gMnSc1
nadále	nadále	k6eAd1
působil	působit	k5eAaImAgMnS
aktivně	aktivně	k6eAd1
v	v	k7c6
alpském	alpský	k2eAgInSc6d1
spolku	spolek	k1gInSc6
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
řeč	řeč	k1gFnSc4
k	k	k7c3
padesátému	padesátý	k4xOgInSc3
výročí	výročí	k1gNnSc3
Liberecké	liberecký	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
zakončil	zakončit	k5eAaPmAgMnS
pozdravem	pozdrav	k1gInSc7
„	„	k?
<g/>
Berg	Berg	k1gMnSc1
Heil	Heil	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
Hore	Hore	k?
zdar	zdar	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
místo	místo	k7c2
tehdy	tehdy	k6eAd1
předepsaného	předepsaný	k2eAgInSc2d1
„	„	k?
<g/>
Heil	Heil	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
války	válka	k1gFnSc2
pro	pro	k7c4
něj	on	k3xPp3gNnSc4
znamenal	znamenat	k5eAaImAgInS
ztrátu	ztráta	k1gFnSc4
všech	všecek	k3xTgMnPc2
odsunutých	odsunutý	k2eAgMnPc2d1
kamarádů	kamarád	k1gMnPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
rodiny	rodina	k1gFnSc2
Ginzelových	Ginzelová	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syn	syn	k1gMnSc1
Manfred	Manfred	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
americkém	americký	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
rodina	rodina	k1gFnSc1
sice	sice	k8xC
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
rodném	rodný	k2eAgInSc6d1
Dolním	dolní	k2eAgInSc6d1
Hanychově	Hanychův	k2eAgInSc6d1
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
jejich	jejich	k3xOp3gInSc2
domu	dům	k1gInSc2
se	se	k3xPyFc4
nastěhovali	nastěhovat	k5eAaPmAgMnP
Češi	Čech	k1gMnPc1
a	a	k8xC
následovala	následovat	k5eAaImAgFnS
každodenní	každodenní	k2eAgFnSc1d1
šikana	šikana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
Kauschkových	Kauschková	k1gFnPc2
řešila	řešit	k5eAaImAgFnS
bezvýchodnost	bezvýchodnost	k1gFnSc1
situace	situace	k1gFnSc2
vystěhováním	vystěhování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
Kauschkovi	Kauschkův	k2eAgMnPc1d1
vyplatily	vyplatit	k5eAaPmAgInP
jeho	jeho	k3xOp3gInPc1
kontakty	kontakt	k1gInPc1
k	k	k7c3
celnímu	celní	k2eAgInSc3d1
úřadu	úřad	k1gInSc3
<g/>
,	,	kIx,
takže	takže	k8xS
si	se	k3xPyFc3
oproti	oproti	k7c3
jiným	jiný	k2eAgFnPc3d1
německým	německý	k2eAgFnPc3d1
rodinám	rodina	k1gFnPc3
odvážel	odvážet	k5eAaImAgInS
i	i	k9
fotky	fotka	k1gFnSc2
a	a	k8xC
cennosti	cennost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
života	život	k1gInSc2
žil	žít	k5eAaImAgInS
v	v	k7c6
obci	obec	k1gFnSc6
Kempten	Kemptno	k1gNnPc2
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
založil	založit	k5eAaPmAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
DAV-Kempten	DAV-Kemptno	k1gNnPc2
Libereckou	liberecký	k2eAgFnSc4d1
sekci	sekce	k1gFnSc4
a	a	k8xC
vykonával	vykonávat	k5eAaImAgInS
správce	správce	k1gMnSc4
Liberecké	liberecký	k2eAgFnSc2d1
chaty	chata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
vydal	vydat	k5eAaPmAgMnS
sbírku	sbírka	k1gFnSc4
básní	báseň	k1gFnPc2
„	„	k?
<g/>
Sang	Sang	k1gMnSc1
aus	aus	k?
Iserwäldern	Iserwäldern	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Zpěv	zpěv	k1gInSc1
z	z	k7c2
Jizerských	jizerský	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
Leben	Leben	k1gInSc1
als	als	k?
Gedicht	Gedicht	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Život	život	k1gInSc1
jako	jako	k8xC,k8xS
báseň	báseň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
na	na	k7c4
infarkt	infarkt	k1gInSc4
v	v	k7c6
Kemptenu	Kempten	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
náhrobní	náhrobní	k2eAgInSc1d1
kámen	kámen	k1gInSc1
zdobí	zdobit	k5eAaImIp3nS
nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Viele	Viele	k1gFnSc1
Wege	Wege	k1gNnPc2
führen	führno	k1gNnPc2
zu	zu	k?
Gott	Gott	k1gMnSc1
<g/>
,	,	kIx,
einer	einer	k1gMnSc1
führt	führta	k1gFnPc2
über	über	k1gMnSc1
die	die	k?
Berge	Berge	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Mnoho	mnoho	k4c1
cest	cesta	k1gFnPc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
Bohu	bůh	k1gMnSc3
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
vede	vést	k5eAaImIp3nS
přes	přes	k7c4
hory	hora	k1gFnPc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Horolezecká	horolezecký	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
V	v	k7c6
prvních	první	k4xOgInPc6
letech	let	k1gInPc6
své	svůj	k3xOyFgFnSc2
horolezecké	horolezecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
–	–	k?
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
vykonal	vykonat	k5eAaPmAgMnS
Kauschka	Kauschka	k1gMnSc1
řadu	řada	k1gFnSc4
prvovýstupů	prvovýstup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
více	hodně	k6eAd2
lezení	lezení	k1gNnSc3
a	a	k8xC
dlouhému	dlouhý	k2eAgNnSc3d1
putování	putování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
našel	najít	k5eAaPmAgMnS
zálibu	záliba	k1gFnSc4
v	v	k7c6
doprovodu	doprovod	k1gInSc6
mladých	mladý	k2eAgMnPc2d1
horolezců	horolezec	k1gMnPc2
a	a	k8xC
vystupoval	vystupovat	k5eAaImAgMnS
spíše	spíše	k9
v	v	k7c6
roli	role	k1gFnSc6
rádce	rádce	k1gMnSc2
a	a	k8xC
trenéra	trenér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkci	funkce	k1gFnSc4
horského	horský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
vykonával	vykonávat	k5eAaImAgInS
jen	jen	k9
na	na	k7c4
již	již	k6eAd1
dříve	dříve	k6eAd2
vylezených	vylezený	k2eAgInPc6d1
výstupech	výstup	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lezení	lezení	k1gNnSc1
na	na	k7c6
Ještědském	ještědský	k2eAgInSc6d1
hřebenu	hřeben	k1gInSc6
<g/>
,	,	kIx,
pískovcových	pískovcový	k2eAgFnPc6d1
skálách	skála	k1gFnPc6
a	a	k8xC
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
</s>
<s>
Starý	starý	k2eAgInSc1d1
Ještěd	Ještěd	k1gInSc1
před	před	k7c7
vyhořením	vyhoření	k1gNnSc7
(	(	kIx(
<g/>
okolo	okolo	k7c2
r.	r.	kA
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pravčická	Pravčická	k1gFnSc1
brána-	brána-	k?
České	český	k2eAgNnSc4d1
Švýcarsko	Švýcarsko	k1gNnSc4
</s>
<s>
Monte	Monte	k5eAaPmIp2nP
Cristallo-Dolomity	Cristallo-Dolomita	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
uskutečnil	uskutečnit	k5eAaPmAgMnS
Kauschka	Kauschka	k1gMnSc1
přes	přes	k7c4
30	#num#	k4
prvovýstupů	prvovýstup	k1gInPc2
na	na	k7c6
Ještědském	ještědský	k2eAgInSc6d1
hřebenu	hřeben	k1gInSc6
(	(	kIx(
<g/>
například	například	k6eAd1
Krejčík	Krejčík	k1gMnSc1
<g/>
,	,	kIx,
Špehýrka	špehýrka	k1gFnSc1
<g/>
,	,	kIx,
Pudl	pudl	k1gMnSc1
nebo	nebo	k8xC
Dánský	dánský	k2eAgInSc1d1
kámen	kámen	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západně	západně	k6eAd1
od	od	k7c2
Ještědského	ještědský	k2eAgInSc2d1
hřebene	hřeben	k1gInSc2
slézal	slézat	k5eAaImAgMnS
Vajoletky	Vajoletka	k1gFnPc4
(	(	kIx(
<g/>
1906	#num#	k4
Severní	severní	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
Krkavce	krkavec	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
vylezl	vylézt	k5eAaPmAgMnS
významnou	významný	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
výstupu	výstup	k1gInSc2
na	na	k7c4
Kavčí	kavčí	k2eAgFnSc4d1
skálu	skála	k1gFnSc4
v	v	k7c6
Děvínském	Děvínský	k2eAgNnSc6d1
polesí	polesí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnPc4d1
skály	skála	k1gFnPc4
ze	z	k7c2
skupiny	skupina	k1gFnSc2
východního	východní	k2eAgInSc2d1
výběžku	výběžek	k1gInSc2
Žitavského	žitavský	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
téměř	téměř	k6eAd1
neznámé	známý	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kauschka	Kauschek	k1gInSc2
zde	zde	k6eAd1
provedl	provést	k5eAaPmAgInS
pět	pět	k4xCc4
prvovýstupů	prvovýstup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1906	#num#	k4
zdolal	zdolat	k5eAaPmAgInS
s	s	k7c7
kamarády	kamarád	k1gMnPc7
Gahlerovu	Gahlerův	k2eAgFnSc4d1
věž	věž	k1gFnSc4
(	(	kIx(
<g/>
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
podle	podle	k7c2
K.	K.	kA
Gahlera	Gahler	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavítal	zavítat	k5eAaPmAgMnS
i	i	k9
na	na	k7c4
skály	skála	k1gFnPc4
v	v	k7c6
Českém	český	k2eAgInSc6d1
ráji	ráj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Suchých	Suchých	k2eAgFnPc6d1
skálách	skála	k1gFnPc6
lezl	lézt	k5eAaImAgInS
okolo	okolo	k7c2
roku	rok	k1gInSc2
1904	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Hrubé	Hrubé	k2eAgFnSc6d1
Skále	skála	k1gFnSc6
vztyčil	vztyčit	k5eAaPmAgMnS
s	s	k7c7
kamarády	kamarád	k1gMnPc7
v	v	k7c6
červnu	červen	k1gInSc6
1906	#num#	k4
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Dračího	dračí	k2eAgInSc2d1
zubu	zub	k1gInSc2
(	(	kIx(
<g/>
Kauschkova	Kauschkův	k2eAgFnSc1d1
spára	spára	k1gFnSc1
<g/>
)	)	kIx)
plechovou	plechový	k2eAgFnSc4d1
německou	německý	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
tisk	tisk	k1gInSc1
tehdy	tehdy	k6eAd1
ztropil	ztropit	k5eAaPmAgInS
velký	velký	k2eAgInSc4d1
humbuk	humbuk	k1gInSc4
a	a	k8xC
spatřoval	spatřovat	k5eAaImAgMnS
v	v	k7c6
tom	ten	k3xDgNnSc6
německou	německý	k2eAgFnSc4d1
provokaci	provokace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladí	mladý	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
na	na	k7c6
magistrátu	magistrát	k1gInSc6
města	město	k1gNnSc2
omluvit	omluvit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
kamarády	kamarád	k1gMnPc7
Rudolfem	Rudolf	k1gMnSc7
Steinjanem	Steinjan	k1gMnSc7
<g/>
,	,	kIx,
Rudolfem	Rudolf	k1gMnSc7
Thamem	Tham	k1gMnSc7
a	a	k8xC
Ferdinandem	Ferdinand	k1gMnSc7
Gerhardtem	Gerhardt	k1gMnSc7
podnikal	podnikat	k5eAaImAgMnS
pochody	pochod	k1gInPc4
do	do	k7c2
Dubských	Dubských	k2eAgFnPc2d1
skal	skála	k1gFnPc2
a	a	k8xC
Polomených	polomený	k2eAgInPc2d1
hor.	hor.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
...	...	k?
<g/>
vypínavost	vypínavost	k1gFnSc1
a	a	k8xC
sprostota	sprostota	k1gFnSc1
Němců	Němec	k1gMnPc2
jest	být	k5eAaImIp3nS
veliká	veliký	k2eAgFnSc1d1
<g/>
...	...	k?
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
na	na	k7c6
Hrubé	Hrubé	k2eAgFnSc6d1
Skále	skála	k1gFnSc6
<g/>
,	,	kIx,
přesahuje	přesahovat	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
meze	mez	k1gFnPc4
<g/>
...	...	k?
<g/>
Tyto	tento	k3xDgFnPc1
skály	skála	k1gFnPc1
zvolili	zvolit	k5eAaPmAgMnP
sobě	se	k3xPyFc3
minulou	minulý	k2eAgFnSc4d1
neděli	neděle	k1gFnSc4
jacísi	jakýsi	k3yIgMnPc1
němečtí	německý	k2eAgMnPc1d1
mladíci	mladík	k1gMnPc1
za	za	k7c4
předmět	předmět	k1gInSc4
svého	svůj	k3xOyFgInSc2
útoku	útok	k1gInSc2
pro	pro	k7c4
čest	čest	k1gFnSc4
a	a	k8xC
slávu	sláva	k1gFnSc4
národa	národ	k1gInSc2
německého	německý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
železných	železný	k2eAgInPc2d1
klínů	klín	k1gInPc2
vyšplhali	vyšplhat	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
skály	skála	k1gFnPc4
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
těžko	těžko	k6eAd1
přístupné	přístupný	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
zde	zde	k6eAd1
na	na	k7c6
nejvyšším	vysoký	k2eAgNnSc6d3
místě	místo	k1gNnSc6
upevnili	upevnit	k5eAaPmAgMnP
žerď	žerď	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
pověsili	pověsit	k5eAaPmAgMnP
velkoněmeckou	velkoněmecký	k2eAgFnSc4d1
trikoloru	trikolora	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
—	—	k?
<g/>
Hlasy	hlas	k1gInPc1
Pojizerské	pojizerský	k2eAgInPc1d1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
se	se	k3xPyFc4
nejdříve	dříve	k6eAd3
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
oblasti	oblast	k1gFnPc4
na	na	k7c4
západ	západ	k1gInSc4
od	od	k7c2
Oldřichovského	Oldřichovský	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
(	(	kIx(
<g/>
výstupy	výstup	k1gInPc1
na	na	k7c4
Uhlířovu	Uhlířův	k2eAgFnSc4d1
čapku	čapka	k1gFnSc4
<g/>
,	,	kIx,
Kovadlinu	kovadlina	k1gFnSc4
<g/>
,	,	kIx,
první	první	k4xOgInSc4
sportovní	sportovní	k2eAgInSc4d1
výstup	výstup	k1gInSc4
na	na	k7c4
Čertův	čertův	k2eAgInSc4d1
Kámen	kámen	k1gInSc4
<g/>
,	,	kIx,
výstup	výstup	k1gInSc4
na	na	k7c4
Zvon	zvon	k1gInSc4
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
přičítáno	přičítán	k2eAgNnSc4d1
25	#num#	k4
nových	nový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
a	a	k8xC
10	#num#	k4
prvovýstupů	prvovýstup	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
našel	najít	k5eAaPmAgInS
zalíbení	zalíbení	k1gNnSc4
i	i	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Labských	labský	k2eAgInPc2d1
pískovců	pískovec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Horolezecká	horolezecký	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
v	v	k7c6
Alpách	Alpy	k1gFnPc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
začínal	začínat	k5eAaImAgMnS
při	při	k7c6
vojenském	vojenský	k2eAgNnSc6d1
cvičení	cvičení	k1gNnSc6
v	v	k7c6
Dolomitech	Dolomity	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Robertem	Robert	k1gMnSc7
Heuslerem	Heusler	k1gMnSc7
zdolal	zdolat	k5eAaPmAgMnS
severní	severní	k2eAgFnSc4d1
stěnu	stěna	k1gFnSc4
Malé	Malé	k2eAgFnSc2d1
Cimy	Cima	k1gFnSc2
<g/>
,	,	kIx,
pokračoval	pokračovat	k5eAaImAgInS
na	na	k7c4
Libereckou	liberecký	k2eAgFnSc4d1
chatu	chata	k1gFnSc4
u	u	k7c2
Cortiny	Cortina	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Ampezzo	Ampezza	k1gFnSc5
a	a	k8xC
nakonec	nakonec	k6eAd1
překročil	překročit	k5eAaPmAgMnS
vrchol	vrchol	k1gInSc4
Croda	Croda	k1gMnSc1
da	da	k?
Lago	Lago	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sólovým	sólový	k2eAgInSc7d1
výstupem	výstup	k1gInSc7
se	se	k3xPyFc4
pustil	pustit	k5eAaPmAgMnS
na	na	k7c6
Fünffingerspitze	Fünffingerspitza	k1gFnSc6
a	a	k8xC
pak	pak	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
sólo	sólo	k2eAgMnSc7d1
traverzem	traverz	k1gMnSc7
věží	věž	k1gFnPc2
Vajolet	Vajolet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
svou	svůj	k3xOyFgFnSc7
budoucí	budoucí	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
Hildou	Hilda	k1gFnSc7
Wenzelovou	Wenzelová	k1gFnSc7
výstup	výstup	k1gInSc4
zopakoval	zopakovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
pobytu	pobyt	k1gInSc6
v	v	k7c6
Cavalese	Cavales	k1gInSc6
vylezl	vylézt	k5eAaPmAgMnS
na	na	k7c4
Schwarzhorn	Schwarzhorn	k1gNnSc4
a	a	k8xC
Weißhorn	Weißhorn	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
byl	být	k5eAaImAgMnS
Kauschka	Kauschek	k1gMnSc4
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
schopnosti	schopnost	k1gFnPc4
přidělen	přidělit	k5eAaPmNgInS
k	k	k7c3
vůdcovskému	vůdcovský	k2eAgInSc3d1
kurzu	kurz	k1gInSc3
do	do	k7c2
Bolzana	Bolzan	k1gMnSc2
a	a	k8xC
vzdělával	vzdělávat	k5eAaImAgMnS
jako	jako	k9
pomocný	pomocný	k2eAgMnSc1d1
instruktor	instruktor	k1gMnSc1
na	na	k7c4
Regensburger	Regensburger	k1gInSc4
Hütte	Hütt	k1gInSc5
vojáky	voják	k1gMnPc4
pro	pro	k7c4
boj	boj	k1gInSc4
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
vykonal	vykonat	k5eAaPmAgInS
druhý	druhý	k4xOgInSc4
průstup	průstup	k1gInSc4
západní	západní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
Sass	Sassa	k1gFnPc2
di	di	k?
Mesdi	Mesd	k1gMnPc1
<g/>
,	,	kIx,
vystoupil	vystoupit	k5eAaPmAgInS
na	na	k7c4
všech	všecek	k3xTgNnPc2
osm	osm	k4xCc4
vrcholů	vrchol	k1gInPc2
skupiny	skupina	k1gFnSc2
Geislergruppe	Geislergrupp	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznámil	seznámit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
významnými	významný	k2eAgMnPc7d1
alpinisty	alpinista	k1gMnPc7
<g/>
:	:	kIx,
Gustavem	Gustav	k1gMnSc7
Jahnem	Jahn	k1gMnSc7
<g/>
,	,	kIx,
Franzem	Franz	k1gInSc7
Barthem	Barth	k1gInSc7
a	a	k8xC
Angelo	Angela	k1gFnSc5
Dibonou	Dibona	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
Ortleru	Ortler	k1gInSc2
zdolal	zdolat	k5eAaPmAgMnS
například	například	k6eAd1
Königspitze	Königspitza	k1gFnSc3
a	a	k8xC
Zufallspitze	Zufallspitza	k1gFnSc3
(	(	kIx(
<g/>
Cevedale	Cevedala	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
ho	on	k3xPp3gMnSc4
zasáhla	zasáhnout	k5eAaPmAgFnS
ztráta	ztráta	k1gFnSc1
Liberecké	liberecký	k2eAgFnSc2d1
chaty	chata	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
zabraném	zabraný	k2eAgNnSc6d1
Italy	Ital	k1gMnPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
další	další	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Alp	Alpy	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
našel	najít	k5eAaPmAgMnS
místo	místo	k1gNnSc4
pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
chatu	chata	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Nakonec	nakonec	k6eAd1
přijel	přijet	k5eAaPmAgMnS
s	s	k7c7
Rudolfem	Rudolf	k1gMnSc7
Thamem	Tham	k1gInSc7
do	do	k7c2
St.	st.	kA
Jacobu	Jacoba	k1gFnSc4
v	v	k7c6
údolí	údolí	k1gNnSc6
Defereggental	Defereggental	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vrcholů	vrchol	k1gInPc2
Seespitze	Seespitze	k1gFnSc2
a	a	k8xC
Alpsspitze	Alpsspitze	k1gFnSc2
uviděl	uvidět	k5eAaPmAgInS
dno	dno	k1gNnSc4
údolí	údolí	k1gNnSc2
a	a	k8xC
okamžitě	okamžitě	k6eAd1
tušil	tušit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
krajina	krajina	k1gFnSc1
stane	stanout	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc7
vlastí	vlast	k1gFnSc7
<g/>
.	.	kIx.
<g/>
”	”	k?
</s>
<s>
—	—	k?
<g/>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
1883	#num#	k4
<g/>
-	-	kIx~
<g/>
1960	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
okolí	okolí	k1gNnSc6
Nové	Nové	k2eAgFnSc2d1
Liberecké	liberecký	k2eAgFnSc2d1
chaty	chata	k1gFnSc2
pak	pak	k6eAd1
Kauschka	Kauschka	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
prvovýstupy	prvovýstup	k1gInPc4
na	na	k7c4
všechny	všechen	k3xTgFnPc4
okolní	okolní	k2eAgFnPc4d1
dvou	dva	k4xCgFnPc2
a	a	k8xC
třítisícové	třítisícový	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
se	se	k3xPyFc4
pustil	pustit	k5eAaPmAgMnS
na	na	k7c4
čtyřtisícové	čtyřtisícový	k2eAgInPc4d1
vrcholy	vrchol	k1gInPc4
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
(	(	kIx(
<g/>
Matterhorn	Matterhorn	k1gNnSc1
<g/>
,	,	kIx,
Breithorn	Breithorn	k1gNnSc1
<g/>
,	,	kIx,
Pollux	Pollux	k1gInSc1
<g/>
,	,	kIx,
Castor	Castor	k1gInSc1
a	a	k8xC
Lyskamm	Lyskamm	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c4
Mont	Mont	k1gInSc4
Blanc	Blanc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
1931	#num#	k4
se	se	k3xPyFc4
rozjel	rozjet	k5eAaPmAgMnS
do	do	k7c2
Dolomitů	dolomit	k1gInPc2
se	s	k7c7
synem	syn	k1gMnSc7
Manfredem	Manfred	k1gMnSc7
a	a	k8xC
s	s	k7c7
přítelem	přítel	k1gMnSc7
Manfredem	Manfred	k1gMnSc7
Kerteszem	Kertesz	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
těmi	ten	k3xDgMnPc7
vystoupil	vystoupit	k5eAaPmAgMnS
např.	např.	kA
na	na	k7c4
Guglia	Guglius	k1gMnSc4
di	di	k?
Brenta	Brenta	k1gMnSc1
<g/>
,	,	kIx,
Civetta	Civetta	k1gMnSc1
Ostwand	Ostwand	k1gInSc1
<g/>
,	,	kIx,
Campanile	Campanile	k1gFnSc1
di	di	k?
Val	val	k1gInSc4
Montanaia	Montanaius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdním	pozdní	k2eAgInSc6d1
věku	věk	k1gInSc6
(	(	kIx(
<g/>
už	už	k6eAd1
po	po	k7c6
vysídlení	vysídlení	k1gNnSc6
<g/>
)	)	kIx)
lezl	lézt	k5eAaImAgMnS
se	s	k7c7
synem	syn	k1gMnSc7
na	na	k7c4
vrcholy	vrchol	k1gInPc4
Allgäuských	Allgäuský	k2eAgFnPc2d1
a	a	k8xC
Lechtalských	Lechtalský	k2eAgFnPc2d1
Alp	Alpy	k1gFnPc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
vydal	vydat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
na	na	k7c4
výpravu	výprava	k1gFnSc4
do	do	k7c2
Ampezanských	Ampezanský	k2eAgInPc2d1
a	a	k8xC
Sextenských	Sextenský	k2eAgInPc2d1
Dolomit	Dolomity	k1gInPc2
a	a	k8xC
do	do	k7c2
oblasti	oblast	k1gFnSc2
Dachsteinu	Dachstein	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc4d1
pochody	pochod	k1gInPc4
</s>
<s>
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1906	#num#	k4
podnikl	podniknout	k5eAaPmAgInS
s	s	k7c7
přáteli	přítel	k1gMnPc7
pěšky	pěšky	k6eAd1
přes	přes	k7c4
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
a	a	k8xC
po	po	k7c6
hlavním	hlavní	k2eAgInSc6d1
hřebenu	hřeben	k1gInSc6
Krkonoš	Krkonoše	k1gFnPc2
98	#num#	k4
km	km	kA
dlouhý	dlouhý	k2eAgInSc4d1
pochod	pochod	k1gInSc4
až	až	k9
na	na	k7c4
Sněžku	Sněžka	k1gFnSc4
(	(	kIx(
<g/>
už	už	k6eAd1
jen	jen	k9
sám	sám	k3xTgInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
dvou	dva	k4xCgFnPc6
hodinách	hodina	k1gFnPc6
odpočinku	odpočinek	k1gInSc2
pokračoval	pokračovat	k5eAaImAgInS
zpátky	zpátky	k6eAd1
na	na	k7c6
nádraží	nádraží	k1gNnSc6
Zelené	Zelené	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Kořenov	Kořenov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kam	kam	k6eAd1
dorazil	dorazit	k5eAaPmAgMnS
od	od	k7c2
rána	ráno	k1gNnSc2
po	po	k7c6
14	#num#	k4
hodinách	hodina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejnou	stejný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
opakoval	opakovat	k5eAaImAgInS
ještě	ještě	k6eAd1
mnohokrát	mnohokrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
opakovaně	opakovaně	k6eAd1
vystupoval	vystupovat	k5eAaImAgMnS
na	na	k7c4
vrchol	vrchol	k1gInSc4
Ještědu	Ještěd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
vystoupil	vystoupit	k5eAaPmAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
na	na	k7c4
Ještěd	Ještěd	k1gInSc4
celkem	celek	k1gInSc7
1470	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
...	...	k?
<g/>
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
mužského	mužský	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
radost	radost	k1gFnSc4
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
po	po	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
slunce	slunce	k1gNnSc4
nebo	nebo	k8xC
deště	dešť	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
zimě	zima	k1gFnSc6
nebo	nebo	k8xC
horku	horko	k1gNnSc6
<g/>
,	,	kIx,
při	při	k7c6
bouři	bouř	k1gFnSc6
nebo	nebo	k8xC
bezvětří	bezvětří	k1gNnSc6
<g/>
,	,	kIx,
ráno	ráno	k6eAd1
<g/>
,	,	kIx,
v	v	k7c4
poledne	poledne	k1gNnSc4
<g/>
,	,	kIx,
večer	večer	k6eAd1
nebo	nebo	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
noci	noc	k1gFnSc6
vystupovat	vystupovat	k5eAaImF
na	na	k7c4
Ještěd	Ještěd	k1gInSc4
<g/>
...	...	k?
správný	správný	k2eAgInSc4d1
Jeschkentitsch	Jeschkentitsch	k1gInSc4
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
pouze	pouze	k6eAd1
silnice	silnice	k1gFnSc2
a	a	k8xC
pěšiny	pěšina	k1gFnSc2
na	na	k7c4
vrchol	vrchol	k1gInSc4
oblíbené	oblíbený	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vymýšlí	vymýšlet	k5eAaImIp3nS
si	se	k3xPyFc3
s	s	k7c7
neuvěřitelnou	uvěřitelný	k2eNgFnSc7d1
plodností	plodnost	k1gFnSc7
všemožné	všemožný	k2eAgInPc1d1
výstupy	výstup	k1gInPc1
<g/>
...	...	k?
<g/>
od	od	k7c2
7	#num#	k4
hodiny	hodina	k1gFnSc2
večerní	večerní	k2eAgFnSc2d1
do	do	k7c2
¾	¾	k?
na	na	k7c4
6	#num#	k4
večer	večer	k6eAd1
druhého	druhý	k4xOgInSc2
dne	den	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
během	během	k7c2
23	#num#	k4
hodin	hodina	k1gFnPc2
jsme	být	k5eAaImIp1nP
vystoupili	vystoupit	k5eAaPmAgMnP
na	na	k7c4
vrchol	vrchol	k1gInSc4
12	#num#	k4
<g/>
krát	krát	k6eAd1
a	a	k8xC
použili	použít	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
přitom	přitom	k6eAd1
20	#num#	k4
různých	různý	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
...	...	k?
<g/>
,	,	kIx,
celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
cesty	cesta	k1gFnSc2
byla	být	k5eAaImAgFnS
75	#num#	k4
km	km	kA
a	a	k8xC
čistým	čistý	k2eAgInSc7d1
časem	čas	k1gInSc7
18	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
25	#num#	k4
minut	minuta	k1gFnPc2
jsme	být	k5eAaImIp1nP
zdolali	zdolat	k5eAaPmAgMnP
5	#num#	k4
140	#num#	k4
metrů	metr	k1gInPc2
výstupu	výstup	k1gInSc2
a	a	k8xC
4	#num#	k4
690	#num#	k4
metrů	metr	k1gInPc2
sestupu	sestup	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
—	—	k?
<g/>
„	„	k?
<g/>
Jeschkentitsche	Jeschkentitsche	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
definice	definice	k1gFnSc1
Leopolda	Leopold	k1gMnSc2
Tertsche	Tertsch	k1gMnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sáňkování	sáňkování	k1gNnSc1
a	a	k8xC
lyžování	lyžování	k1gNnSc1
</s>
<s>
Vosecká	Vosecká	k1gFnSc1
bouda	bouda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sněžka	Sněžka	k1gFnSc1
s	s	k7c7
hvězdárnou	hvězdárna	k1gFnSc7
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sáňkování	sáňkování	k1gNnSc1
se	se	k3xPyFc4
rozšířilo	rozšířit	k5eAaPmAgNnS
nejdříve	dříve	k6eAd3
v	v	k7c6
německy	německy	k6eAd1
mluvících	mluvící	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještěd	Ještěd	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
centrem	centrum	k1gNnSc7
sáňkařského	sáňkařský	k2eAgInSc2d1
sportu	sport	k1gInSc2
pro	pro	k7c4
celé	celý	k2eAgInPc4d1
Sudety	Sudety	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hoffmann	Hoffmann	k1gMnSc1
zde	zde	k6eAd1
vybudoval	vybudovat	k5eAaPmAgMnS
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
mistrovství	mistrovství	k1gNnSc2
Jizerských	jizerský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
v	v	k7c6
sáňkování	sáňkování	k1gNnSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1910	#num#	k4
<g/>
)	)	kIx)
umělou	umělý	k2eAgFnSc4d1
sáňkařskou	sáňkařský	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
pak	pak	k6eAd1
provozoval	provozovat	k5eAaImAgMnS
DGJI	DGJI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
dráze	dráha	k1gFnSc6
konalo	konat	k5eAaImAgNnS
mistrovství	mistrovství	k1gNnSc1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kauschka	Kauschek	k1gInSc2
si	se	k3xPyFc3
pořídil	pořídit	k5eAaPmAgMnS
závodní	závodní	k2eAgFnPc4d1
saně	saně	k1gFnPc4
a	a	k8xC
sáňkování	sáňkování	k1gNnSc4
doslova	doslova	k6eAd1
propadl	propadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
v	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
byl	být	k5eAaImAgInS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
severních	severní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
druhý	druhý	k4xOgInSc4
<g/>
,	,	kIx,
na	na	k7c6
Mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
na	na	k7c6
Ještědu	Ještěd	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
zvítězil	zvítězit	k5eAaPmAgInS
a	a	k8xC
na	na	k7c6
dvojsaních	dvojsaní	k2eAgFnPc6d1
s	s	k7c7
Hansem	Hans	k1gMnSc7
Gfällerem	Gfäller	k1gMnSc7
byl	být	k5eAaImAgMnS
druhý	druhý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Německa	Německo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgInS
třetí	třetí	k4xOgInSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
na	na	k7c6
závodech	závod	k1gInPc6
na	na	k7c6
Ještědu	Ještěd	k1gInSc6
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
mistrovství	mistrovství	k1gNnSc1
Centrálního	centrální	k2eAgInSc2d1
svazu	svaz	k1gInSc2
německých	německý	k2eAgInPc2d1
spolků	spolek	k1gInPc2
zimních	zimní	k2eAgInPc2d1
sportů	sport	k1gInPc2
<g/>
/	/	kIx~
<g/>
Hauptverband	Hauptverband	k1gInSc1
der	drát	k5eAaImRp2nS
deutschen	deutschna	k1gFnPc2
Wintersportvereine	Wintersportverein	k1gMnSc5
(	(	kIx(
<g/>
HdW	HdW	k1gMnSc6
<g/>
)	)	kIx)
zde	zde	k6eAd1
nevyhrál	vyhrát	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1928	#num#	k4
byl	být	k5eAaImAgMnS
druhý	druhý	k4xOgMnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
zajel	zajet	k5eAaPmAgMnS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
žákem	žák	k1gMnSc7
Fritzem	Fritz	k1gMnSc7
Preißlerem	Preißler	k1gMnSc7
na	na	k7c6
dvojsaních	dvojsaní	k2eAgFnPc6d1
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
a	a	k8xC
mistrovství	mistrovství	k1gNnSc4
HDW	HDW	kA
tentokrát	tentokrát	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
67	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
v	v	k7c6
Iglis	Iglis	k1gFnSc6
u	u	k7c2
Innsbrucku	Innsbruck	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
Liberecké	liberecký	k2eAgFnSc2d1
lyžařské	lyžařský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Reichenberger	Reichenberger	k1gMnSc1
Skizunft	Skizunft	k1gMnSc1
už	už	k6eAd1
před	před	k7c7
první	první	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Vosecké	Vosecké	k2eAgFnSc2d1
boudy	bouda	k1gFnSc2
společně	společně	k6eAd1
se	s	k7c7
Steinjanem	Steinjan	k1gMnSc7
skákal	skákat	k5eAaImAgInS
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
,	,	kIx,
mimo	mimo	k7c4
skoku	skok	k1gInSc6
ovládal	ovládat	k5eAaImAgInS
i	i	k9
další	další	k2eAgFnPc4d1
techniky	technika	k1gFnPc4
lyžování	lyžování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
lyžích	lyže	k1gFnPc6
podnikal	podnikat	k5eAaImAgInS
nejen	nejen	k6eAd1
sjezdy	sjezd	k1gInPc1
na	na	k7c6
Ještědu	Ještěd	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
dlouhé	dlouhý	k2eAgFnPc4d1
túry	túra	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gNnSc2
se	se	k3xPyFc4
také	také	k9
jednomu	jeden	k4xCgMnSc3
ze	z	k7c2
svahů	svah	k1gInPc2
na	na	k7c6
Ještědu	Ještěd	k1gInSc6
říkalo	říkat	k5eAaImAgNnS
Kauschkahang	Kauschkahang	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
války	válka	k1gFnSc2
absolvoval	absolvovat	k5eAaPmAgMnS
lyžařský	lyžařský	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
s	s	k7c7
lyžemi	lyže	k1gFnPc7
do	do	k7c2
pohoří	pohoří	k1gNnSc2
<g/>
:	:	kIx,
Totes	Totesa	k1gFnPc2
Gebirge	Gebirge	k1gInSc1
<g/>
,	,	kIx,
Deferegger	Deferegger	k1gInSc1
Berge	Berg	k1gFnSc2
<g/>
,	,	kIx,
Nízké	nízký	k2eAgFnSc2d1
Taury	Taura	k1gFnSc2
a	a	k8xC
nezapomněl	zapomnět	k5eNaImAgMnS,k5eNaPmAgMnS
navštívit	navštívit	k5eAaPmF
ani	ani	k8xC
oblíbenou	oblíbený	k2eAgFnSc4d1
Neue	Neue	k1gFnSc4
Reichenberger	Reichenbergra	k1gFnPc2
Hütte	Hütt	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
byl	být	k5eAaImAgInS
oceněn	ocenit	k5eAaPmNgInS
Německým	německý	k2eAgInSc7d1
hlavním	hlavní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
pro	pro	k7c4
tělesnou	tělesný	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
/	/	kIx~
<g/>
Deutsche	Deutsche	k1gInSc1
Hauptausschuss	Hauptausschuss	k1gInSc1
für	für	k?
Leibesübungen	Leibesübungen	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
Tschechoslowakei	Tschechoslowake	k1gInSc6
jako	jako	k9
druhý	druhý	k4xOgMnSc1
sportovec	sportovec	k1gMnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
zlatým	zlatý	k2eAgNnSc7d1
Turnerským	turnerský	k2eAgNnSc7d1
a	a	k8xC
sportovním	sportovní	k2eAgNnSc7d1
vyznamenáním	vyznamenání	k1gNnSc7
(	(	kIx(
<g/>
Goldene	Golden	k1gInSc5
Turn-und	Turn-und	k1gInSc4
Sportabzeichen	Sportabzeichen	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
alpských	alpský	k2eAgInPc6d1
spolcích	spolek	k1gInPc6
</s>
<s>
Österreichischer	Österreichischra	k1gFnPc2
Alpenklub	Alpenkluba	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Rakouský	rakouský	k2eAgInSc1d1
alpský	alpský	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Členem	člen	k1gMnSc7
elitního	elitní	k2eAgInSc2d1
Rakouského	rakouský	k2eAgInSc2d1
alpského	alpský	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
/	/	kIx~
<g/>
Österreichischer	Österreichischra	k1gFnPc2
Alpenklub	Alpenkluba	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Kauschka	Kauschek	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
však	však	k9
musel	muset	k5eAaImAgMnS
„	„	k?
<g/>
uskutečnit	uskutečnit	k5eAaPmF
těžké	těžký	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
v	v	k7c6
ledu	led	k1gInSc6
a	a	k8xC
skále	skála	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
byl	být	k5eAaImAgInS
kladen	klást	k5eAaImNgInS
důraz	důraz	k1gInSc1
na	na	k7c4
samostatné	samostatný	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
těchto	tento	k3xDgFnPc2
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Od	od	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
v	v	k7c6
Österreichische	Österreichische	k1gFnSc6
Alpenzeitung	Alpenzeitunga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
DuÖAV-Sektion	DuÖAV-Sektion	k1gInSc1
Reichenberg	Reichenberg	k1gInSc1
<g/>
/	/	kIx~
<g/>
Německý	německý	k2eAgInSc1d1
a	a	k8xC
rakouský	rakouský	k2eAgInSc1d1
alpský	alpský	k2eAgInSc1d1
spolek-Liberecká	spolek-Liberecký	k2eAgFnSc5d1
sekce	sekce	k1gFnSc1
</s>
<s>
Členem	člen	k1gMnSc7
Liberecké	liberecký	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
DuÖAV	DuÖAV	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
působil	působit	k5eAaImAgMnS
ve	v	k7c6
funkci	funkce	k1gFnSc6
účetního	účetní	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
zvolený	zvolený	k2eAgMnSc1d1
člen	člen	k1gMnSc1
ve	v	k7c6
vedení	vedení	k1gNnSc6
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
nově	nově	k6eAd1
založeného	založený	k2eAgInSc2d1
oddílu	oddíl	k1gInSc2
zimních	zimní	k2eAgInPc2d1
sportů	sport	k1gInPc2
při	při	k7c6
DAV-Liberec	DAV-Liberec	k1gMnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
také	také	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
vykonával	vykonávat	k5eAaImAgInS
pro	pro	k7c4
spolek	spolek	k1gInSc4
funkci	funkce	k1gFnSc4
účetního	účetní	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
vzdal	vzdát	k5eAaPmAgMnS
všech	všecek	k3xTgFnPc2
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
nadále	nadále	k6eAd1
zůstal	zůstat	k5eAaPmAgMnS
činný	činný	k2eAgMnSc1d1
ve	v	k7c6
funkci	funkce	k1gFnSc6
vedoucího	vedoucí	k1gMnSc2
výletní	výletní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
pomáhal	pomáhat	k5eAaImAgInS
se	se	k3xPyFc4
správou	správa	k1gFnSc7
Rudolf-Tham-fondu	Rudolf-Tham-fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přesídlení	přesídlení	k1gNnSc6
do	do	k7c2
Německa	Německo	k1gNnSc2
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
aktivně	aktivně	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
vedoucí	vedoucí	k1gMnSc1
místní	místní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
DAV	Dav	k1gInSc1
a	a	k8xC
jako	jako	k8xS,k8xC
správce	správce	k1gMnSc1
Reichenberger	Reichenberger	k1gMnSc1
Hütte	Hütt	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
zlaté	zlatý	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
za	za	k7c4
padesátileté	padesátiletý	k2eAgNnSc4d1
věrné	věrný	k2eAgNnSc4d1
členství	členství	k1gNnSc4
v	v	k7c6
DuÖAV	DuÖAV	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kauschkův	Kauschkův	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
Zvonu	zvon	k1gInSc2
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
vztyčil	vztyčit	k5eAaPmAgMnS
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1922	#num#	k4
horolezec	horolezec	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Fischer	Fischer	k1gMnSc1
tabulku	tabulka	k1gFnSc4
s	s	k7c7
iniciály	iniciála	k1gFnPc1
prvovýstupců	prvovýstupec	k1gMnPc2
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Tham	Tham	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
<g/>
,	,	kIx,
Franz	Franz	k1gMnSc1
Haupt	Haupt	k1gMnSc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
Löppen	Löppen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Obtížnou	obtížný	k2eAgFnSc4d1
horolezeckou	horolezecký	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
na	na	k7c6
Viničné	Viničný	k2eAgFnSc6d1
plotně	plotna	k1gFnSc6
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
vylezli	vylézt	k5eAaPmAgMnP
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1974	#num#	k4
Jan	Jan	k1gMnSc1
Löffler	Löffler	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
Klíma	Klíma	k1gMnSc1
a	a	k8xC
J.	J.	kA
<g/>
Hadrich	Hadrich	k1gInSc4
a	a	k8xC
nazvali	nazvat	k5eAaPmAgMnP,k5eAaBmAgMnP
ji	on	k3xPp3gFnSc4
Kauschkův	Kauschkův	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
Nové	Nové	k2eAgFnSc2d1
Liberecké	liberecký	k2eAgFnSc2d1
chaty	chata	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
pojmenována	pojmenován	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
Reichenberger	Reichenbergra	k1gFnPc2
Hütte	Hütt	k1gInSc5
na	na	k7c4
Drufelder	Drufelder	k1gInSc4
Alm	Alm	k1gFnSc2
„	„	k?
<g/>
Kauschka-Weg	Kauschka-Weg	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
na	na	k7c6
hřebeni	hřeben	k1gInSc6
Panargenkamm	Panargenkamm	k1gMnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
počest	počest	k1gFnSc6
pojmenován	pojmenován	k2eAgInSc4d1
„	„	k?
<g/>
Kauschkahorn	Kauschkahorn	k1gInSc4
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
DAV-Sekce	DAV-Sekce	k1gFnSc1
Reichenberg	Reichenberg	k1gInSc1
na	na	k7c6
jaře	jaro	k1gNnSc6
1961	#num#	k4
zabudovala	zabudovat	k5eAaPmAgFnS
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1
tabuli	tabule	k1gFnSc4
na	na	k7c4
Rudolfa	Rudolf	k1gMnSc4
Kauschku	Kauschka	k1gMnSc4
u	u	k7c2
vchodu	vchod	k1gInSc2
do	do	k7c2
soutěsky	soutěska	k1gFnSc2
Trojerschlucht	Trojerschluchta	k1gFnPc2
u	u	k7c2
St.	st.	kA
Jakob	Jakob	k1gMnSc1
in	in	k?
Osttirol	Osttirol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
je	být	k5eAaImIp3nS
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
počest	počest	k1gFnSc6
pojmenována	pojmenován	k2eAgFnSc1d1
nejobtížněji	obtížně	k6eAd3
dostupná	dostupný	k2eAgFnSc1d1
Kauschkova	Kauschkův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Syn	syn	k1gMnSc1
Rudolfa	Rudolf	k1gMnSc2
Kauschky	Kauschka	k1gMnSc2
společně	společně	k6eAd1
se	s	k7c7
synem	syn	k1gMnSc7
otcova	otcův	k2eAgInSc2d1
přítele	přítel	k1gMnSc4
Gustavem	Gustav	k1gMnSc7
Ginzelem	Ginzel	k1gMnSc7
juniorem	junior	k1gMnSc7
a	a	k8xC
se	s	k7c7
Siegfriedem	Siegfried	k1gMnSc7
Weissem	Weiss	k1gMnSc7
v	v	k7c6
červenci	červenec	k1gInSc6
1961	#num#	k4
splnili	splnit	k5eAaPmAgMnP
poslední	poslední	k2eAgNnPc4d1
přání	přání	k1gNnPc4
Rudolfa	Rudolf	k1gMnSc2
Kauschky	Kauschka	k1gMnSc2
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Poledních	polední	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
umístili	umístit	k5eAaPmAgMnP
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1
tabuli	tabule	k1gFnSc4
s	s	k7c7
nápisem	nápis	k1gInSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1883	#num#	k4
<g/>
,	,	kIx,
zemř	zemř	k1gFnSc1
<g/>
.	.	kIx.
1960	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
památku	památka	k1gFnSc4
horolezci	horolezec	k1gMnSc3
a	a	k8xC
básníkovi	básník	k1gMnSc3
<g/>
.	.	kIx.
<g/>
”	”	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přátelé	přítel	k1gMnPc1
a	a	k8xC
spolulezci	spolulezec	k1gMnPc1
</s>
<s>
August	August	k1gMnSc1
Steinjan	Steinjan	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Tham	Tham	k1gMnSc1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Gerhardt	Gerhardt	k1gMnSc1
jun	jun	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Gustav	Gustav	k1gMnSc1
Ginzel	Ginzel	k1gMnSc1
senior	senior	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
dva	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
Gustav	Gustav	k1gMnSc1
Ginzel	Ginzel	k1gMnSc1
junior	junior	k1gMnSc1
<g/>
,	,	kIx,
Wolfgang	Wolfgang	k1gMnSc1
Ginzel	Ginzel	k1gMnSc1
</s>
<s>
Manfred	Manfred	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
Fukov	Fukov	k1gInSc1
↑	↑	k?
Wandern	Wandern	k1gInSc1
und	und	k?
Klettern	Klettern	k1gInSc1
kopie	kopie	k1gFnSc2
knihy	kniha	k1gFnSc2
Rudolfa	Rudolf	k1gMnSc2
Kauschky	Kauschka	k1gFnSc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
Chaloupská	chaloupský	k2eAgFnSc1d1
Pavlína	Pavlína	k1gFnSc1
<g/>
:	:	kIx,
Vývoj	vývoj	k1gInSc1
německých	německý	k2eAgInPc2d1
alpských	alpský	k2eAgInPc2d1
spolků	spolek	k1gInPc2
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
do	do	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
disertační	disertační	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
↑	↑	k?
Hlasy	hlas	k1gInPc1
Pojizerské	pojizerský	k2eAgInPc1d1
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
21	#num#	k4
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
č.	č.	k?
12	#num#	k4
in	in	k?
KITTLER	KITTLER	kA
A.	A.	kA
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschek	k1gInSc2
1883	#num#	k4
<g/>
-	-	kIx~
<g/>
1960	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
↑	↑	k?
KITTLER	KITTLER	kA
A.	A.	kA
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschek	k1gInSc2
1883	#num#	k4
<g/>
-	-	kIx~
<g/>
1960	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
↑	↑	k?
KITTLER	KITTLER	kA
A.	A.	kA
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschek	k1gInSc2
1883	#num#	k4
<g/>
-	-	kIx~
<g/>
1960	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
111	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FAJGL	FAJGL	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
SIMM	SIMM	kA
<g/>
,	,	kIx,
Otokar	Otokar	k1gMnSc1
<g/>
;	;	kIx,
VRKOSLAV	VRKOSLAV	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jizerské	jizerský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
:	:	kIx,
horolezecký	horolezecký	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
<g/>
:	:	kIx,
Milan	Milan	k1gMnSc1
Vrkoslav	Vrkoslav	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
303	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
–	–	k?
horolezec	horolezec	k1gMnSc1
a	a	k8xC
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
25	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Německé	německý	k2eAgInPc1d1
alpské	alpský	k2eAgInPc1d1
spolky	spolek	k1gInPc1
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
do	do	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
</s>
<s>
Deutscher	Deutschra	k1gFnPc2
Alpenverein	Alpenvereina	k1gFnPc2
</s>
<s>
Oesterreichischer	Oesterreichischra	k1gFnPc2
Alpenverein	Alpenvereina	k1gFnPc2
</s>
<s>
Johann	Johann	k1gMnSc1
Stüdl	Stüdl	k1gMnSc1
</s>
<s>
Sáňkařská	sáňkařský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
na	na	k7c6
Ještědu	Ještěd	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
prvovýstupů	prvovýstup	k1gInPc2
Rudolfa	Rudolf	k1gMnSc2
Kauschky	Kauschka	k1gMnSc2
</s>
<s>
Jizerská	jizerský	k2eAgFnSc1d1
klasika	klasika	k1gFnSc1
<g/>
:	:	kIx,
Kohoutí	kohoutí	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
</s>
<s>
Obří	obří	k2eAgFnSc1d1
věž	věž	k1gFnSc1
Grálu	grál	k1gInSc2
v	v	k7c6
Jizerských	jizerský	k2eAgFnPc6d1
horách	hora	k1gFnPc6
</s>
<s>
Liberecká	liberecký	k2eAgFnSc1d1
chata	chata	k1gFnSc1
–	–	k?
Reichenberger	Reichenberger	k1gInSc1
Hütte	Hütt	k1gInSc5
<g/>
,	,	kIx,
podrobná	podrobný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Časopis	časopis	k1gInSc1
Krkonoše	Krkonoše	k1gFnPc1
–	–	k?
Jizerské	jizerský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
Liberecká	liberecký	k2eAgFnSc1d1
chata	chata	k1gFnSc1
a	a	k8xC
Rudolf	Rudolf	k1gMnSc1
Kauschka	Kauschka	k1gMnSc1
</s>
<s>
Kauschkova	Kauschkův	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Petr	Petr	k1gMnSc1
Hejtmánek	Hejtmánek	k1gMnSc1
<g/>
,	,	kIx,
Hruboskalsko	Hruboskalsko	k1gNnSc1
–	–	k?
Dračí	dračí	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
Zámecká	zámecký	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
|	|	kIx~
Šluknovský	šluknovský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2001100434	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1146847173	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5883	#num#	k4
7402	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84131881	#num#	k4
</s>
