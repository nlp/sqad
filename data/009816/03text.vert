<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
República	Repúblic	k2eAgFnSc1d1	República
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
s	s	k7c7	s
4984	[number]	k4	4984
km	km	kA	km
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
pobřežím	pobřeží	k1gNnSc7	pobřeží
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Bolívií	Bolívie	k1gFnSc7	Bolívie
(	(	kIx(	(
<g/>
832	[number]	k4	832
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Paraguayí	Paraguay	k1gFnSc7	Paraguay
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
(	(	kIx(	(
<g/>
1224	[number]	k4	1224
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Uruguayí	Uruguay	k1gFnSc7	Uruguay
(	(	kIx(	(
<g/>
579	[number]	k4	579
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Chile	Chile	k1gNnSc7	Chile
(	(	kIx(	(
<g/>
5150	[number]	k4	5150
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
2	[number]	k4	2
780	[number]	k4	780
400	[number]	k4	400
km2	km2	k4	km2
osmou	osmý	k4xOgFnSc7	osmý
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
největší	veliký	k2eAgFnSc4d3	veliký
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
největší	veliký	k2eAgFnSc7d3	veliký
španělskojazyčnou	španělskojazyčný	k2eAgFnSc7d1	španělskojazyčný
zemí	zem	k1gFnSc7	zem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
ekonomiku	ekonomika	k1gFnSc4	ekonomika
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
kvalitu	kvalita	k1gFnSc4	kvalita
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
G	G	kA	G
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
sdružení	sdružení	k1gNnSc1	sdružení
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
Mercosur	Mercosura	k1gFnPc2	Mercosura
a	a	k8xC	a
politické	politický	k2eAgNnSc1d1	politické
společenství	společenství	k1gNnSc1	společenství
Unie	unie	k1gFnSc2	unie
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vytvářeno	vytvářit	k5eAaPmNgNnS	vytvářit
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
však	však	k9	však
uvízlo	uvíznout	k5eAaPmAgNnS	uvíznout
na	na	k7c6	na
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
federální	federální	k2eAgInSc1d1	federální
stát	stát	k1gInSc1	stát
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c6	na
23	[number]	k4	23
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
autonomní	autonomní	k2eAgNnSc1d1	autonomní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
provincie	provincie	k1gFnSc1	provincie
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
si	se	k3xPyFc3	se
činí	činit	k5eAaImIp3nS	činit
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
část	část	k1gFnSc4	část
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
vede	vést	k5eAaImIp3nS	vést
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
spor	spor	k1gInSc4	spor
o	o	k7c4	o
Falklandy	Falkland	k1gInPc4	Falkland
(	(	kIx(	(
<g/>
Malvíny	Malvína	k1gFnPc4	Malvína
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Georgii	Georgie	k1gFnSc4	Georgie
a	a	k8xC	a
Jižní	jižní	k2eAgInPc4d1	jižní
Sandwichovy	Sandwichův	k2eAgInPc4d1	Sandwichův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Argentiny	Argentina	k1gFnSc2	Argentina
bylo	být	k5eAaImAgNnS	být
kulturně	kulturně	k6eAd1	kulturně
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonizací	kolonizace	k1gFnSc7	kolonizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
následnými	následný	k2eAgFnPc7d1	následná
vlnami	vlna	k1gFnPc7	vlna
migrace	migrace	k1gFnSc2	migrace
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
v	v	k7c6	v
současných	současný	k2eAgFnPc6d1	současná
hranicích	hranice	k1gFnPc6	hranice
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
Místokrálovství	Místokrálovství	k1gNnSc4	Místokrálovství
Río	Río	k1gFnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
územím	území	k1gNnSc7	území
koloniálním	koloniální	k2eAgNnSc7d1	koloniální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1810-1818	[number]	k4	1810-1818
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Argentinci	Argentinec	k1gMnPc1	Argentinec
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
však	však	k9	však
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
až	až	k9	až
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Argentina	Argentina	k1gFnSc1	Argentina
sedmou	sedmý	k4xOgFnSc7	sedmý
nejbohatší	bohatý	k2eAgFnSc7d3	nejbohatší
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
však	však	k9	však
přišel	přijít	k5eAaPmAgInS	přijít
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
rozvrat	rozvrat	k1gInSc1	rozvrat
následovaný	následovaný	k2eAgInSc1d1	následovaný
politickým	politický	k2eAgInSc7d1	politický
chaosem	chaos	k1gInSc7	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
období	období	k1gNnSc1	období
diktátorských	diktátorský	k2eAgInPc2d1	diktátorský
režimů	režim	k1gInPc2	režim
vojenského	vojenský	k2eAgInSc2d1	vojenský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
a	a	k8xC	a
nejúspěšnějším	úspěšný	k2eAgFnPc3d3	nejúspěšnější
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
diktátorů	diktátor	k1gMnPc2	diktátor
byl	být	k5eAaImAgMnS	být
Juan	Juan	k1gMnSc1	Juan
Perón	perón	k1gInSc4	perón
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
ideologie	ideologie	k1gFnSc1	ideologie
tzv.	tzv.	kA	tzv.
perónismu	perónismus	k1gInSc2	perónismus
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
významným	významný	k2eAgInSc7d1	významný
rysem	rys	k1gInSc7	rys
argentinského	argentinský	k2eAgInSc2d1	argentinský
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
a	a	k8xC	a
hlásí	hlásit	k5eAaImIp3nS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
demokratických	demokratický	k2eAgMnPc2d1	demokratický
politiků	politik	k1gMnPc2	politik
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
diktátorských	diktátorský	k2eAgFnPc2d1	diktátorská
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
doprovázená	doprovázený	k2eAgNnPc1d1	doprovázené
řadou	řada	k1gFnSc7	řada
násilností	násilnost	k1gFnSc7	násilnost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
špinavá	špinavý	k2eAgFnSc1d1	špinavá
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Argentina	Argentina	k1gFnSc1	Argentina
demokracií	demokracie	k1gFnPc2	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
adjektivum	adjektivum	k1gNnSc1	adjektivum
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
argentum	argentum	k1gNnSc4	argentum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
událostech	událost	k1gFnPc6	událost
nezdařené	zdařený	k2eNgFnSc2d1	nezdařená
výpravy	výprava	k1gFnSc2	výprava
(	(	kIx(	(
<g/>
1515	[number]	k4	1515
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
portugalského	portugalský	k2eAgMnSc2d1	portugalský
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
službách	služba	k1gFnPc6	služba
Juana	Juan	k1gMnSc2	Juan
Díaze	Díaze	k1gFnSc2	Díaze
de	de	k?	de
Solís	Solís	k1gInSc1	Solís
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
objevil	objevit	k5eAaPmAgInS	objevit
ohromný	ohromný	k2eAgInSc1d1	ohromný
estuár	estuár	k1gInSc1	estuár
La	la	k1gNnSc2	la
Mar	Mar	k1gFnSc1	Mar
Dulce	Dulce	k1gFnSc1	Dulce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sladké	Sladké	k2eAgNnSc1d1	Sladké
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
s	s	k7c7	s
částí	část	k1gFnSc7	část
posádky	posádka	k1gFnSc2	posádka
zabit	zabít	k5eAaPmNgMnS	zabít
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cestou	cesta	k1gFnSc7	cesta
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
lodí	loď	k1gFnPc2	loď
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
skupinky	skupinka	k1gFnSc2	skupinka
ztroskotanců	ztroskotanec	k1gMnPc2	ztroskotanec
<g/>
,	,	kIx,	,
Portugalec	Portugalec	k1gMnSc1	Portugalec
Alejo	Alejo	k1gMnSc1	Alejo
García	García	k1gMnSc1	García
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
spřátelit	spřátelit	k5eAaPmF	spřátelit
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
Indiány	Indián	k1gMnPc7	Indián
a	a	k8xC	a
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1524	[number]	k4	1524
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
evropsko-indiánskou	evropskondiánský	k2eAgFnSc4d1	evropsko-indiánský
výpravu	výprava	k1gFnSc4	výprava
za	za	k7c7	za
mýtickým	mýtický	k2eAgMnSc7d1	mýtický
"	"	kIx"	"
<g/>
Bílým	bílý	k1gMnSc7	bílý
králem	král	k1gMnSc7	král
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
indiánských	indiánský	k2eAgFnPc2d1	indiánská
pověstí	pověst	k1gFnPc2	pověst
měl	mít	k5eAaImAgInS	mít
sídlit	sídlit	k5eAaImF	sídlit
ve	v	k7c6	v
"	"	kIx"	"
<g/>
Stříbrných	stříbrný	k2eAgFnPc6d1	stříbrná
horách	hora	k1gFnPc6	hora
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Sierra	Sierra	k1gFnSc1	Sierra
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
)	)	kIx)	)
a	a	k8xC	a
oplývat	oplývat	k5eAaImF	oplývat
nesmírným	smírný	k2eNgNnSc7d1	nesmírné
množstvím	množství	k1gNnSc7	množství
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Garcíova	Garcíův	k2eAgFnSc1d1	Garcíův
výprava	výprava	k1gFnSc1	výprava
přešla	přejít	k5eAaPmAgFnS	přejít
kontinent	kontinent	k1gInSc4	kontinent
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
Inckou	incký	k2eAgFnSc4d1	incká
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
obránci	obránce	k1gMnPc7	obránce
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
naloupit	naloupit	k5eAaPmF	naloupit
stříbrné	stříbrný	k2eAgInPc4d1	stříbrný
poklady	poklad	k1gInPc4	poklad
<g/>
;	;	kIx,	;
byť	byť	k8xS	byť
byl	být	k5eAaImAgInS	být
García	Garcíus	k1gMnSc4	Garcíus
na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
zabit	zabit	k2eAgMnSc1d1	zabit
<g/>
,	,	kIx,	,
pověst	pověst	k1gFnSc1	pověst
i	i	k8xC	i
materiální	materiální	k2eAgInPc1d1	materiální
důkazy	důkaz	k1gInPc1	důkaz
se	se	k3xPyFc4	se
donesly	donést	k5eAaPmAgInP	donést
k	k	k7c3	k
sluchu	sluch	k1gInSc3	sluch
dalších	další	k2eAgMnPc2d1	další
evropských	evropský	k2eAgMnPc2d1	evropský
objevitelů	objevitel	k1gMnPc2	objevitel
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1548	[number]	k4	1548
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
několik	několik	k4yIc1	několik
výprav	výprava	k1gFnPc2	výprava
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
najít	najít	k5eAaPmF	najít
ty	ten	k3xDgFnPc4	ten
legendární	legendární	k2eAgFnPc4d1	legendární
Stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
protože	protože	k8xS	protože
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
výpravy	výprava	k1gFnPc1	výprava
měly	mít	k5eAaImAgFnP	mít
počátek	počátek	k1gInSc4	počátek
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
La	la	k1gNnSc2	la
Mar	Mar	k1gMnSc2	Mar
Dulce	Dulce	k1gMnSc2	Dulce
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
Río	Río	k1gFnSc6	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
kraj	kraj	k1gInSc1	kraj
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gInSc2	on
na	na	k7c4	na
Stříbrnou	stříbrná	k1gFnSc4	stříbrná
zemi	zem	k1gFnSc4	zem
–	–	k?	–
ačkoliv	ačkoliv	k8xS	ačkoliv
vytoužená	vytoužený	k2eAgFnSc1d1	vytoužená
hora	hora	k1gFnSc1	hora
plná	plný	k2eAgFnSc1d1	plná
stříbra	stříbro	k1gNnSc2	stříbro
byla	být	k5eAaImAgNnP	být
nakonec	nakonec	k6eAd1	nakonec
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
)	)	kIx)	)
nalezena	nalezen	k2eAgNnPc1d1	Nalezeno
až	až	k6eAd1	až
v	v	k7c6	v
Potosí	Potosý	k2eAgMnPc1d1	Potosý
<g/>
,	,	kIx,	,
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
doloženo	doložen	k2eAgNnSc1d1	doloženo
v	v	k7c6	v
latinském	latinský	k2eAgInSc6d1	latinský
tvaru	tvar	k1gInSc6	tvar
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
Terra	Terro	k1gNnSc2	Terro
Argentea	argenteus	k1gInSc2	argenteus
<g/>
;	;	kIx,	;
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevuje	objevovat	k5eAaImIp3nS	objevovat
jeho	jeho	k3xOp3gInSc4	jeho
španělský	španělský	k2eAgInSc4d1	španělský
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
Tierra	Tierra	k1gFnSc1	Tierra
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
ovšem	ovšem	k9	ovšem
psaný	psaný	k2eAgMnSc1d1	psaný
s	s	k7c7	s
určitým	určitý	k2eAgMnSc7d1	určitý
členem	člen	k1gMnSc7	člen
jako	jako	k8xS	jako
La	la	k1gNnSc7	la
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
toto	tento	k3xDgNnSc1	tento
adjektivum	adjektivum	k1gNnSc1	adjektivum
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
politického	politický	k2eAgNnSc2d1	politické
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
Confederación	Confederación	k1gInSc1	Confederación
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
República	Repúblic	k2eAgFnSc1d1	República
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
i	i	k9	i
současná	současný	k2eAgFnSc1d1	současná
krátká	krátký	k2eAgFnSc1d1	krátká
podoba	podoba	k1gFnSc1	podoba
jména	jméno	k1gNnSc2	jméno
geografického	geografický	k2eAgNnSc2d1	geografické
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
prostřední	prostřední	k2eAgFnSc1d1	prostřední
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
přidáno	přidán	k2eAgNnSc4d1	Přidáno
žluté	žlutý	k2eAgNnSc4d1	žluté
Slunce	slunce	k1gNnSc4	slunce
-	-	kIx~	-
květnové	květnový	k2eAgNnSc4d1	květnové
Slunce	slunce	k1gNnSc4	slunce
-	-	kIx~	-
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
představovat	představovat	k5eAaImF	představovat
boha	bůh	k1gMnSc4	bůh
Inků	Ink	k1gMnPc2	Ink
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Inkové	Ink	k1gMnPc1	Ink
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
kdysi	kdysi	k6eAd1	kdysi
obývali	obývat	k5eAaImAgMnP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
32	[number]	k4	32
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starší	starý	k2eAgFnPc1d2	starší
dějiny	dějiny	k1gFnPc1	dějiny
Argentiny	Argentina	k1gFnSc2	Argentina
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
do	do	k7c2	do
země	zem	k1gFnSc2	zem
pronikli	proniknout	k5eAaPmAgMnP	proniknout
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
oblasti	oblast	k1gFnSc6	oblast
And	Anda	k1gFnPc2	Anda
významné	významný	k2eAgInPc4d1	významný
kulturní	kulturní	k2eAgInPc4d1	kulturní
národy	národ	k1gInPc4	národ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
obývaly	obývat	k5eAaImAgInP	obývat
sídliště	sídliště	k1gNnSc4	sídliště
podobná	podobný	k2eAgNnPc4d1	podobné
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
přebývala	přebývat	k5eAaImAgFnS	přebývat
společenství	společenství	k1gNnSc4	společenství
sběračů	sběrač	k1gMnPc2	sběrač
a	a	k8xC	a
lovců	lovec	k1gMnPc2	lovec
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
obsadili	obsadit	k5eAaPmAgMnP	obsadit
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
části	část	k1gFnSc3	část
Inkové	Ink	k1gMnPc1	Ink
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
přistála	přistát	k5eAaImAgFnS	přistát
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
La	la	k1gNnSc2	la
Platy	plat	k1gInPc4	plat
španělsko-portugalská	španělskoortugalský	k2eAgFnSc1d1	španělsko-portugalská
výprava	výprava	k1gFnSc1	výprava
Solísova	Solísův	k2eAgFnSc1d1	Solísův
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
patřilo	patřit	k5eAaImAgNnS	patřit
území	území	k1gNnSc1	území
Argentiny	Argentina	k1gFnSc2	Argentina
ke	k	k7c3	k
španělskému	španělský	k2eAgNnSc3d1	španělské
místokrálovství	místokrálovství	k1gNnSc3	místokrálovství
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1536	[number]	k4	1536
Španělé	Španěl	k1gMnPc1	Španěl
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
území	území	k1gNnSc4	území
a	a	k8xC	a
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
Arakuánce	Arakuánec	k1gMnSc4	Arakuánec
a	a	k8xC	a
Patagonce	Patagonec	k1gMnSc4	Patagonec
téměř	téměř	k6eAd1	téměř
vyhubili	vyhubit	k5eAaPmAgMnP	vyhubit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
místokrálovství	místokrálovství	k1gNnSc1	místokrálovství
Río	Río	k1gFnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
území	území	k1gNnSc4	území
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
Paraguaye	Paraguay	k1gFnSc2	Paraguay
a	a	k8xC	a
Uruguaye	Uruguay	k1gFnSc2	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
byl	být	k5eAaImAgMnS	být
sesazen	sesazen	k2eAgMnSc1d1	sesazen
místokrál	místokrál	k1gMnSc1	místokrál
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
Spojených	spojený	k2eAgFnPc2d1	spojená
provincií	provincie	k1gFnPc2	provincie
Río	Río	k1gFnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
za	za	k7c2	za
diktatury	diktatura	k1gFnSc2	diktatura
Juana	Juan	k1gMnSc2	Juan
Manuela	Manuel	k1gMnSc2	Manuel
de	de	k?	de
Rosas	Rosas	k1gInSc1	Rosas
<g/>
'	'	kIx"	'
jednotný	jednotný	k2eAgInSc1d1	jednotný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
se	se	k3xPyFc4	se
Argentina	Argentina	k1gFnSc1	Argentina
stala	stát	k5eAaPmAgFnS	stát
federativním	federativní	k2eAgInSc7d1	federativní
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
centralisty	centralista	k1gMnPc7	centralista
a	a	k8xC	a
federalisty	federalista	k1gMnPc7	federalista
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
paraguayské	paraguayský	k2eAgFnPc4d1	paraguayská
války	válka	k1gFnPc4	válka
při	při	k7c6	při
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
zabrala	zabrat	k5eAaPmAgFnS	zabrat
region	region	k1gInSc4	region
Misiones	Misionesa	k1gFnPc2	Misionesa
a	a	k8xC	a
část	část	k1gFnSc1	část
regionu	region	k1gInSc2	region
Chaco	Chaco	k6eAd1	Chaco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
byla	být	k5eAaImAgFnS	být
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
Patagonie	Patagonie	k1gFnSc1	Patagonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Peronismus	Peronismus	k1gInSc4	Peronismus
===	===	k?	===
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
krizí	krize	k1gFnSc7	krize
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
skončila	skončit	k5eAaPmAgFnS	skončit
doba	doba	k1gFnSc1	doba
velkého	velký	k2eAgInSc2d1	velký
rozkvětu	rozkvět	k1gInSc2	rozkvět
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
převzali	převzít	k5eAaPmAgMnP	převzít
vládu	vláda	k1gFnSc4	vláda
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
vlastníci	vlastník	k1gMnPc1	vlastník
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
vojenské	vojenský	k2eAgInPc4d1	vojenský
kruhy	kruh	k1gInPc4	kruh
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
střídaly	střídat	k5eAaImAgFnP	střídat
civilní	civilní	k2eAgFnPc1d1	civilní
vlády	vláda	k1gFnPc1	vláda
s	s	k7c7	s
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
reformního	reformní	k2eAgNnSc2d1	reformní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
populistického	populistický	k2eAgNnSc2d1	populistické
politika	politikum	k1gNnSc2	politikum
platil	platit	k5eAaImAgInS	platit
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
dvakrát	dvakrát	k6eAd1	dvakrát
zvolený	zvolený	k2eAgMnSc1d1	zvolený
Juan	Juan	k1gMnSc1	Juan
Domingo	Domingo	k1gMnSc1	Domingo
Perón	perón	k1gInSc4	perón
<g/>
,	,	kIx,	,
obviňovaný	obviňovaný	k2eAgInSc4d1	obviňovaný
ze	z	k7c2	z
sociální	sociální	k2eAgFnSc2d1	sociální
demagogie	demagogie	k1gFnSc2	demagogie
a	a	k8xC	a
sympatií	sympatie	k1gFnPc2	sympatie
k	k	k7c3	k
fašismu	fašismus	k1gInSc3	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
získala	získat	k5eAaPmAgFnS	získat
jméno	jméno	k1gNnSc4	jméno
politika	politik	k1gMnSc2	politik
peronismu	peronismus	k1gInSc2	peronismus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
projevovala	projevovat	k5eAaImAgFnS	projevovat
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
znárodňováním	znárodňování	k1gNnSc7	znárodňování
<g/>
,	,	kIx,	,
podporou	podpora	k1gFnSc7	podpora
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
sociálního	sociální	k2eAgNnSc2d1	sociální
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc2	rozšíření
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
dělníků	dělník	k1gMnPc2	dělník
i	i	k8xC	i
nejchudších	chudý	k2eAgMnPc2d3	nejchudší
venkovanů	venkovan	k1gMnPc2	venkovan
<g/>
,	,	kIx,	,
posilováním	posilování	k1gNnSc7	posilování
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
autority	autorita	k1gFnSc2	autorita
a	a	k8xC	a
antiklerikalismem	antiklerikalismus	k1gInSc7	antiklerikalismus
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
1955	[number]	k4	1955
argentinskou	argentinský	k2eAgFnSc4d1	Argentinská
vládu	vláda	k1gFnSc4	vláda
exkomunikoval	exkomunikovat	k5eAaBmAgMnS	exkomunikovat
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
uteklo	utéct	k5eAaPmAgNnS	utéct
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
mnoho	mnoho	k4c1	mnoho
nacistických	nacistický	k2eAgMnPc2d1	nacistický
pohlavárů	pohlavár	k1gMnPc2	pohlavár
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Adolf	Adolf	k1gMnSc1	Adolf
Eichmann	Eichmann	k1gMnSc1	Eichmann
nebo	nebo	k8xC	nebo
Josef	Josef	k1gMnSc1	Josef
Mengele	Mengel	k1gInSc2	Mengel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Perón	perón	k1gInSc4	perón
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
a	a	k8xC	a
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
sílící	sílící	k2eAgFnSc1d1	sílící
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
provázely	provázet	k5eAaImAgFnP	provázet
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
generála	generál	k1gMnSc2	generál
Franka	Frank	k1gMnSc2	Frank
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
madridského	madridský	k2eAgInSc2d1	madridský
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
čelila	čelit	k5eAaImAgFnS	čelit
Argentina	Argentina	k1gFnSc1	Argentina
stupňující	stupňující	k2eAgFnSc1d1	stupňující
se	se	k3xPyFc4	se
politické	politický	k2eAgFnSc3d1	politická
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
provázené	provázený	k2eAgInPc1d1	provázený
vojenskými	vojenský	k2eAgInPc7d1	vojenský
puči	puč	k1gInPc7	puč
a	a	k8xC	a
vlnou	vlna	k1gFnSc7	vlna
terorismu	terorismus	k1gInSc2	terorismus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vrcholila	vrcholit	k5eAaImAgFnS	vrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Juan	Juan	k1gMnSc1	Juan
Domingo	Domingo	k1gMnSc1	Domingo
Perón	perón	k1gInSc4	perón
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
potřetí	potřetí	k4xO	potřetí
prezidentem	prezident	k1gMnSc7	prezident
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
kteréžto	kteréžto	k?	kteréžto
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
ukončila	ukončit	k5eAaPmAgFnS	ukončit
hned	hned	k6eAd1	hned
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
diktatura	diktatura	k1gFnSc1	diktatura
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Perónově	perónově	k6eAd1	perónově
smrti	smrt	k1gFnSc2	smrt
svrhly	svrhnout	k5eAaPmAgFnP	svrhnout
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1976	[number]	k4	1976
vojenské	vojenský	k2eAgInPc1d1	vojenský
kruhy	kruh	k1gInPc1	kruh
vládu	vláda	k1gFnSc4	vláda
formálně	formálně	k6eAd1	formálně
vedenou	vedený	k2eAgFnSc4d1	vedená
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
Isabelitou	Isabelitý	k2eAgFnSc4d1	Isabelitý
Maríou	Maríá	k1gFnSc4	Maríá
Estelou	Estelý	k2eAgFnSc4d1	Estelý
(	(	kIx(	(
<g/>
faktickou	faktický	k2eAgFnSc4d1	faktická
moc	moc	k1gFnSc4	moc
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
José	Josá	k1gFnSc2	Josá
López	López	k1gInSc1	López
Rega	Rega	k1gMnSc1	Rega
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
zahájily	zahájit	k5eAaPmAgInP	zahájit
tzv.	tzv.	kA	tzv.
špinavou	špinavý	k2eAgFnSc4d1	špinavá
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
Guerra	Guerra	k1gFnSc1	Guerra
Sucia	Sucia	k1gFnSc1	Sucia
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
odpůrcům	odpůrce	k1gMnPc3	odpůrce
<g/>
;	;	kIx,	;
reagovaly	reagovat	k5eAaBmAgInP	reagovat
tak	tak	k6eAd1	tak
na	na	k7c4	na
vzestup	vzestup	k1gInSc4	vzestup
politického	politický	k2eAgNnSc2d1	politické
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
výrazně	výrazně	k6eAd1	výrazně
přispěly	přispět	k5eAaPmAgFnP	přispět
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
formace	formace	k1gFnPc1	formace
peronistické	peronistický	k2eAgFnSc2d1	peronistická
levice	levice	k1gFnSc2	levice
–	–	k?	–
tzv.	tzv.	kA	tzv.
Montoneros	Montonerosa	k1gFnPc2	Montonerosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
<g/>
,	,	kIx,	,
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
USA	USA	kA	USA
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Henry	Henry	k1gMnSc1	Henry
Kissinger	Kissinger	k1gMnSc1	Kissinger
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
počátečním	počáteční	k2eAgInSc7d1	počáteční
souhlasem	souhlas	k1gInSc7	souhlas
vlivné	vlivný	k2eAgFnSc2d1	vlivná
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
proces	proces	k1gInSc4	proces
tzv.	tzv.	kA	tzv.
národní	národní	k2eAgFnSc2d1	národní
reorganizace	reorganizace	k1gFnSc2	reorganizace
(	(	kIx(	(
<g/>
Proceso	Procesa	k1gFnSc5	Procesa
de	de	k?	de
Reorganización	Reorganización	k1gMnSc1	Reorganización
Nacional	Nacional	k1gMnSc1	Nacional
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
argentinskou	argentinský	k2eAgFnSc4d1	Argentinská
společnost	společnost	k1gFnSc4	společnost
kombinací	kombinace	k1gFnPc2	kombinace
neoliberálních	neoliberální	k2eAgFnPc2d1	neoliberální
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
reforem	reforma	k1gFnPc2	reforma
a	a	k8xC	a
brutální	brutální	k2eAgFnSc2d1	brutální
represe	represe	k1gFnSc2	represe
atomizovat	atomizovat	k5eAaBmF	atomizovat
a	a	k8xC	a
depolitizovat	depolitizovat	k5eAaImF	depolitizovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
junty	junta	k1gFnSc2	junta
a	a	k8xC	a
špinavé	špinavý	k2eAgFnSc2d1	špinavá
války	válka	k1gFnSc2	válka
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
operace	operace	k1gFnSc1	operace
Kondor	kondor	k1gMnSc1	kondor
zmizely	zmizet	k5eAaPmAgInP	zmizet
beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
tisíce	tisíc	k4xCgInPc4	tisíc
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
junty	junta	k1gFnSc2	junta
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
potýkala	potýkat	k5eAaImAgFnS	potýkat
nejenom	nejenom	k6eAd1	nejenom
se	s	k7c7	s
špatnou	špatný	k2eAgFnSc7d1	špatná
vnitropolitickou	vnitropolitický	k2eAgFnSc7d1	vnitropolitická
ale	ale	k8xC	ale
i	i	k8xC	i
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
situací	situace	k1gFnSc7	situace
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
vláda	vláda	k1gFnSc1	vláda
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
upevnila	upevnit	k5eAaPmAgFnS	upevnit
si	se	k3xPyFc3	se
své	své	k1gNnSc4	své
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
válku	válka	k1gFnSc4	válka
o	o	k7c4	o
Falklandy	Falklanda	k1gFnPc4	Falklanda
<g/>
,	,	kIx,	,
Argentinci	Argentinec	k1gMnPc1	Argentinec
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Malvíny	Malvína	k1gFnSc2	Malvína
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
především	především	k6eAd1	především
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
rychlé	rychlý	k2eAgNnSc1d1	rychlé
vítězství	vítězství	k1gNnSc1	vítězství
(	(	kIx(	(
<g/>
neočekávali	očekávat	k5eNaImAgMnP	očekávat
výraznější	výrazný	k2eAgFnSc4d2	výraznější
reakci	reakce	k1gFnSc4	reakce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Británie	Británie	k1gFnSc2	Británie
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
malé	malé	k1gNnSc4	malé
a	a	k8xC	a
od	od	k7c2	od
Británie	Británie	k1gFnSc2	Británie
značně	značně	k6eAd1	značně
odlehlé	odlehlý	k2eAgNnSc4d1	odlehlé
území	území	k1gNnSc4	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jí	on	k3xPp3gFnSc7	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
snadno	snadno	k6eAd1	snadno
si	se	k3xPyFc3	se
upevnit	upevnit	k5eAaPmF	upevnit
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
však	však	k9	však
na	na	k7c4	na
útok	útok	k1gInSc4	útok
Argentiny	Argentina	k1gFnSc2	Argentina
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
nekompromisním	kompromisní	k2eNgInSc7d1	nekompromisní
protiútokem	protiútok	k1gInSc7	protiútok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pohřbil	pohřbít	k5eAaPmAgInS	pohřbít
myšlenku	myšlenka	k1gFnSc4	myšlenka
rychlého	rychlý	k2eAgNnSc2d1	rychlé
a	a	k8xC	a
snadného	snadný	k2eAgNnSc2d1	snadné
vítězství	vítězství	k1gNnSc2	vítězství
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
krátké	krátký	k2eAgFnSc6d1	krátká
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
o	o	k7c4	o
falklandské	falklandský	k2eAgNnSc4d1	falklandský
souostroví	souostroví	k1gNnSc4	souostroví
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1982	[number]	k4	1982
jen	jen	k9	jen
uspíšila	uspíšit	k5eAaPmAgFnS	uspíšit
pád	pád	k1gInSc4	pád
vojenské	vojenský	k2eAgFnSc2d1	vojenská
junty	junta	k1gFnSc2	junta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovení	obnovení	k1gNnSc1	obnovení
demokracie	demokracie	k1gFnSc2	demokracie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1983	[number]	k4	1983
se	se	k3xPyFc4	se
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
opět	opět	k6eAd1	opět
konaly	konat	k5eAaImAgFnP	konat
svobodné	svobodný	k2eAgFnPc1d1	svobodná
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
civilista	civilista	k1gMnSc1	civilista
a	a	k8xC	a
odpůrce	odpůrce	k1gMnSc1	odpůrce
junty	junta	k1gFnSc2	junta
Raúl	Raúl	k1gMnSc1	Raúl
Alfonsín	Alfonsín	k1gMnSc1	Alfonsín
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
demokratizačního	demokratizační	k2eAgInSc2d1	demokratizační
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
dostávaly	dostávat	k5eAaImAgFnP	dostávat
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
mocenské	mocenský	k2eAgInPc4d1	mocenský
prostředky	prostředek	k1gInPc4	prostředek
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vlády	vláda	k1gFnSc2	vláda
<g/>
:	:	kIx,	:
mučení	mučení	k1gNnSc2	mučení
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
masové	masový	k2eAgFnSc2d1	masová
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
lety	let	k1gInPc1	let
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
vuelos	vuelos	k1gInSc1	vuelos
de	de	k?	de
la	la	k1gNnSc1	la
muerte	muert	k1gInSc5	muert
–	–	k?	–
shazování	shazování	k1gNnPc4	shazování
živých	živý	k2eAgMnPc2d1	živý
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
vojenských	vojenský	k2eAgNnPc2d1	vojenské
letadel	letadlo	k1gNnPc2	letadlo
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
)	)	kIx)	)
a	a	k8xC	a
strategie	strategie	k1gFnSc1	strategie
mizení	mizení	k1gNnSc2	mizení
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
desaparecidos	desaparecidos	k1gInSc1	desaparecidos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
porušování	porušování	k1gNnSc6	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
za	za	k7c2	za
vojenské	vojenský	k2eAgFnSc2d1	vojenská
diktatury	diktatura	k1gFnSc2	diktatura
Nuncá	Nuncá	k1gMnSc1	Nuncá
más	más	k?	más
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
víc	hodně	k6eAd2	hodně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
padlo	padnout	k5eAaImAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
kolem	kolem	k7c2	kolem
30	[number]	k4	30
000	[number]	k4	000
Argentinců	Argentinec	k1gMnPc2	Argentinec
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
samotné	samotný	k2eAgInPc1d1	samotný
"	"	kIx"	"
<g/>
lety	let	k1gInPc1	let
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
4000	[number]	k4	4000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
vojenského	vojenský	k2eAgInSc2d1	vojenský
režimu	režim	k1gInSc2	režim
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
k	k	k7c3	k
dlouholetému	dlouholetý	k2eAgNnSc3d1	dlouholeté
vězení	vězení	k1gNnSc3	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Alfonsín	Alfonsín	k1gMnSc1	Alfonsín
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
armády	armáda	k1gFnSc2	armáda
nucen	nutit	k5eAaImNgInS	nutit
dvěma	dva	k4xCgInPc7	dva
spornými	sporný	k2eAgInPc7d1	sporný
zákony	zákon	k1gInPc7	zákon
(	(	kIx(	(
<g/>
Punto	punto	k1gNnSc4	punto
final	final	k1gInSc1	final
a	a	k8xC	a
Ley	Lea	k1gFnSc2	Lea
de	de	k?	de
obediencia	obediencius	k1gMnSc2	obediencius
débita	débit	k1gMnSc2	débit
<g/>
)	)	kIx)	)
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
zločinů	zločin	k1gInPc2	zločin
vojenského	vojenský	k2eAgInSc2d1	vojenský
režimu	režim	k1gInSc2	režim
výrazně	výrazně	k6eAd1	výrazně
omezit	omezit	k5eAaPmF	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
je	být	k5eAaImIp3nS	být
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
prezident	prezident	k1gMnSc1	prezident
Néstor	Néstor	k1gMnSc1	Néstor
Kirchner	Kirchner	k1gMnSc1	Kirchner
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
stoupenec	stoupenec	k1gMnSc1	stoupenec
peronistické	peronistický	k2eAgFnSc2d1	peronistická
levice	levice	k1gFnSc2	levice
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	let	k1gInPc6	let
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
prezidentka	prezidentka	k1gFnSc1	prezidentka
Cristina	Cristin	k2eAgFnSc1d1	Cristina
Fernández	Fernández	k1gInSc4	Fernández
de	de	k?	de
Kirchner	Kirchner	k1gInSc1	Kirchner
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2007	[number]	k4	2007
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
finanční	finanční	k2eAgFnSc1d1	finanční
krize	krize	k1gFnSc1	krize
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
éry	éra	k1gFnSc2	éra
Carlose	Carlosa	k1gFnSc3	Carlosa
Menema	Menemum	k1gNnSc2	Menemum
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
z	z	k7c2	z
Justicialistické	Justicialistický	k2eAgFnSc2d1	Justicialistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
formální	formální	k2eAgFnSc7d1	formální
dědičkou	dědička	k1gFnSc7	dědička
peronismu	peronismus	k1gInSc2	peronismus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
zločinů	zločin	k1gMnPc2	zločin
junty	junta	k1gFnSc2	junta
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
kolej	kolej	k1gFnSc4	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Menemovy	Menemův	k2eAgFnPc1d1	Menemův
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
reformy	reforma	k1gFnPc1	reforma
po	po	k7c6	po
dílčích	dílčí	k2eAgInPc6d1	dílčí
úspěších	úspěch	k1gInPc6	úspěch
(	(	kIx(	(
<g/>
redukce	redukce	k1gFnSc1	redukce
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
inflace	inflace	k1gFnSc2	inflace
<g/>
)	)	kIx)	)
nakonec	nakonec	k6eAd1	nakonec
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
v	v	k7c4	v
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
finanční	finanční	k2eAgFnSc4d1	finanční
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
přispělo	přispět	k5eAaPmAgNnS	přispět
odhalení	odhalení	k1gNnSc2	odhalení
několika	několik	k4yIc2	několik
korupčních	korupční	k2eAgFnPc2d1	korupční
afér	aféra	k1gFnPc2	aféra
i	i	k8xC	i
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
a	a	k8xC	a
která	který	k3yQgFnSc1	který
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
politickou	politický	k2eAgFnSc4d1	politická
nestabilitu	nestabilita	k1gFnSc4	nestabilita
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
se	se	k3xPyFc4	se
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
čtyři	čtyři	k4xCgMnPc1	čtyři
prezidenti	prezident	k1gMnPc1	prezident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
regionu	region	k1gInSc2	region
Cono	Cono	k1gMnSc1	Cono
Sur	Sur	k1gMnSc1	Sur
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
úrodné	úrodný	k2eAgFnPc1d1	úrodná
pampy	pampa	k1gFnPc1	pampa
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
polovině	polovina	k1gFnSc6	polovina
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
roviny	rovina	k1gFnSc2	rovina
Patagonie	Patagonie	k1gFnSc2	Patagonie
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
poloviny	polovina	k1gFnSc2	polovina
až	až	k9	až
k	k	k7c3	k
Ohňové	ohňový	k2eAgFnSc3d1	ohňová
zemi	zem	k1gFnSc3	zem
(	(	kIx(	(
<g/>
Tierra	Tierra	k1gMnSc1	Tierra
de	de	k?	de
Fuego	Fuego	k1gMnSc1	Fuego
<g/>
)	)	kIx)	)
a	a	k8xC	a
horské	horský	k2eAgInPc1d1	horský
masivy	masiv	k1gInPc1	masiv
And	Anda	k1gFnPc2	Anda
na	na	k7c6	na
západě	západ	k1gInSc6	západ
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Chile	Chile	k1gNnSc7	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
And	Anda	k1gFnPc2	Anda
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
,	,	kIx,	,
Aconcagua	Aconcaguum	k1gNnSc2	Aconcaguum
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
právě	právě	k9	právě
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
severních	severní	k2eAgFnPc2d1	severní
provincií	provincie	k1gFnPc2	provincie
Salta	salto	k1gNnSc2	salto
<g/>
,	,	kIx,	,
Formosa	Formosa	k1gFnSc1	Formosa
<g/>
,	,	kIx,	,
Chaco	Chaco	k1gNnSc1	Chaco
<g/>
,	,	kIx,	,
Santiago	Santiago	k1gNnSc1	Santiago
del	del	k?	del
Estero	Ester	k1gFnSc5	Ester
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Fé	Fé	k1gMnSc1	Fé
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
z	z	k7c2	z
Paraguaye	Paraguay	k1gFnSc2	Paraguay
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
planina	planina	k1gFnSc1	planina
Gran	Gran	k1gMnSc1	Gran
Chaco	Chaco	k1gMnSc1	Chaco
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
si	se	k3xPyFc3	se
formálně	formálně	k6eAd1	formálně
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
i	i	k9	i
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
Antarktidy	Antarktida	k1gFnSc2	Antarktida
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
souostroví	souostroví	k1gNnPc2	souostroví
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panuje	panovat	k5eAaImIp3nS	panovat
arktické	arktický	k2eAgNnSc1d1	arktické
a	a	k8xC	a
subarktické	subarktický	k2eAgNnSc1d1	subarktické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Argentiny	Argentina	k1gFnSc2	Argentina
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
10,7	[number]	k4	10,7
%	%	kIx~	%
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
53	[number]	k4	53
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
řeky	řeka	k1gFnPc4	řeka
patří	patřit	k5eAaImIp3nS	patřit
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
Bermejo	Bermejo	k1gNnSc1	Bermejo
<g/>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
Paraná	Paraná	k1gFnSc1	Paraná
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc4	území
mírné	mírný	k2eAgNnSc4d1	mírné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
argentinskými	argentinský	k2eAgMnPc7d1	argentinský
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
(	(	kIx(	(
<g/>
3	[number]	k4	3
043	[number]	k4	043
400	[number]	k4	400
<g/>
,	,	kIx,	,
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Gran	Gran	k1gMnSc1	Gran
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
11	[number]	k4	11
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Córdoba	Córdoba	k1gFnSc1	Córdoba
(	(	kIx(	(
<g/>
1	[number]	k4	1
208	[number]	k4	208
700	[number]	k4	700
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rosario	Rosario	k1gMnSc1	Rosario
(	(	kIx(	(
<g/>
1	[number]	k4	1
119	[number]	k4	119
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mendoza	Mendoza	k1gFnSc1	Mendoza
(	(	kIx(	(
<g/>
773	[number]	k4	773
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc7	la
Plata	plato	k1gNnSc2	plato
(	(	kIx(	(
<g/>
643	[number]	k4	643
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Extrémní	extrémní	k2eAgInPc1d1	extrémní
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Gran	Gran	k1gMnSc1	Gran
Bajo	Bajo	k1gMnSc1	Bajo
de	de	k?	de
San	San	k1gMnSc1	San
Julián	Julián	k1gMnSc1	Julián
−	−	k?	−
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
6959	[number]	k4	6959
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
revidovaná	revidovaný	k2eAgFnSc1d1	revidovaná
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
moc	moc	k6eAd1	moc
na	na	k7c4	na
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
a	a	k8xC	a
regionální	regionální	k2eAgFnSc6d1	regionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
2	[number]	k4	2
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následující	následující	k2eAgNnSc4d1	následující
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Argentinský	argentinský	k2eAgInSc1d1	argentinský
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
komory	komora	k1gFnPc4	komora
<g/>
:	:	kIx,	:
Sněmovnu	sněmovna	k1gFnSc4	sněmovna
poslanců	poslanec	k1gMnPc2	poslanec
s	s	k7c7	s
257	[number]	k4	257
členy	člen	k1gInPc7	člen
a	a	k8xC	a
senát	senát	k1gInSc1	senát
se	s	k7c7	s
72	[number]	k4	72
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jsou	být	k5eAaImIp3nP	být
senátoři	senátor	k1gMnPc1	senátor
voleni	volit	k5eAaImNgMnP	volit
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
provincii	provincie	k1gFnSc4	provincie
tři	tři	k4xCgMnPc1	tři
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
senátorů	senátor	k1gMnPc2	senátor
trvá	trvat	k5eAaImIp3nS	trvat
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
obměňována	obměňován	k2eAgFnSc1d1	obměňována
každé	každý	k3xTgInPc4	každý
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
poslanců	poslanec	k1gMnPc2	poslanec
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
poměrným	poměrný	k2eAgInSc7d1	poměrný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
je	být	k5eAaImIp3nS	být
obměňována	obměňován	k2eAgFnSc1d1	obměňována
polovina	polovina	k1gFnSc1	polovina
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současným	současný	k2eAgInSc7d1	současný
prezdientem	prezdient	k1gInSc7	prezdient
je	být	k5eAaImIp3nS	být
Mauricio	Mauricio	k1gNnSc1	Mauricio
Macri	Macr	k1gFnSc2	Macr
z	z	k7c2	z
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
rozdělení	rozdělení	k1gNnSc1	rozdělení
===	===	k?	===
</s>
</p>
<p>
<s>
23	[number]	k4	23
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
provincia	provincia	k1gFnSc1	provincia
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
provincias	provincias	k1gInSc1	provincias
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
federální	federální	k2eAgInSc4d1	federální
distrikt	distrikt	k1gInSc4	distrikt
(	(	kIx(	(
<g/>
Ciudad	Ciudad	k1gInSc1	Ciudad
Autónoma	Autónom	k1gMnSc2	Autónom
de	de	k?	de
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
využívá	využívat	k5eAaImIp3nS	využívat
velkého	velký	k2eAgNnSc2d1	velké
přírodního	přírodní	k2eAgNnSc2d1	přírodní
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
gramotné	gramotný	k2eAgFnSc2d1	gramotná
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc2	hospodářství
zaměřeného	zaměřený	k2eAgNnSc2d1	zaměřené
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
a	a	k8xC	a
diverzifikovaného	diverzifikovaný	k2eAgInSc2d1	diverzifikovaný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
země	země	k1gFnSc1	země
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
velké	velký	k2eAgInPc4d1	velký
dluhy	dluh	k1gInPc4	dluh
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
1991	[number]	k4	1991
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
inflace	inflace	k1gFnSc1	inflace
někdy	někdy	k6eAd1	někdy
až	až	k9	až
na	na	k7c4	na
200	[number]	k4	200
%	%	kIx~	%
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
boje	boj	k1gInSc2	boj
s	s	k7c7	s
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
liberalizace	liberalizace	k1gFnSc2	liberalizace
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
deregulace	deregulace	k1gFnSc2	deregulace
a	a	k8xC	a
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
zavedla	zavést	k5eAaPmAgFnS	zavést
radikální	radikální	k2eAgFnSc1d1	radikální
peněžní	peněžní	k2eAgFnSc1d1	peněžní
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spojily	spojit	k5eAaPmAgFnP	spojit
peso	peso	k1gNnSc4	peso
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
dolarem	dolar	k1gInSc7	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgInPc1d1	počáteční
úspěchy	úspěch	k1gInPc1	úspěch
v	v	k7c6	v
omezení	omezení	k1gNnSc6	omezení
inflace	inflace	k1gFnSc2	inflace
a	a	k8xC	a
nastolení	nastolení	k1gNnSc2	nastolení
růstu	růst	k1gInSc2	růst
HDP	HDP	kA	HDP
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
opětovné	opětovný	k2eAgNnSc1d1	opětovné
zhoršení	zhoršení	k1gNnSc1	zhoršení
situace	situace	k1gFnSc2	situace
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
kvůli	kvůli	k7c3	kvůli
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
krizím	krize	k1gFnPc3	krize
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
daně	daň	k1gFnPc4	daň
a	a	k8xC	a
snižovala	snižovat	k5eAaImAgFnS	snižovat
výdaje	výdaj	k1gInPc4	výdaj
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
omezovala	omezovat	k5eAaImAgFnS	omezovat
deficit	deficit	k1gInSc4	deficit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
nulový	nulový	k2eAgInSc4d1	nulový
deficit	deficit	k1gInSc4	deficit
<g/>
,	,	kIx,	,
stabilizaci	stabilizace	k1gFnSc4	stabilizace
bankovního	bankovní	k2eAgInSc2d1	bankovní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
nastolení	nastolení	k1gNnSc4	nastolení
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
nedostačujícími	dostačující	k2eNgFnPc7d1	nedostačující
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
De	De	k?	De
La	la	k1gNnSc2	la
Rua	Rua	k1gFnSc2	Rua
zbaven	zbavit	k5eAaPmNgMnS	zbavit
moci	moct	k5eAaImF	moct
po	po	k7c6	po
nepokojích	nepokoj	k1gInPc6	nepokoj
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
;	;	kIx,	;
sněmovna	sněmovna	k1gFnSc1	sněmovna
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Eduarda	Eduard	k1gMnSc4	Eduard
Duhaldeho	Duhalde	k1gMnSc4	Duhalde
za	za	k7c4	za
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Duhalde	Duhalde	k6eAd1	Duhalde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
MMF	MMF	kA	MMF
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
další	další	k2eAgFnSc4d1	další
půjčku	půjčka	k1gFnSc4	půjčka
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
nestačila	stačit	k5eNaBmAgFnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
pesa	peso	k1gNnSc2	peso
s	s	k7c7	s
dolarem	dolar	k1gInSc7	dolar
bylo	být	k5eAaImAgNnS	být
opuštěno	opustit	k5eAaPmNgNnS	opustit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2002	[number]	k4	2002
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
se	se	k3xPyFc4	se
kurz	kurz	k1gInSc1	kurz
pesa	peso	k1gNnSc2	peso
vzdálil	vzdálit	k5eAaPmAgInS	vzdálit
od	od	k7c2	od
dolaru	dolar	k1gInSc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
prezident	prezident	k1gMnSc1	prezident
moratorium	moratorium	k1gNnSc4	moratorium
na	na	k7c4	na
dluhy	dluh	k1gInPc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k8xS	až
2012	[number]	k4	2012
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
spory	spor	k1gInPc1	spor
Argentiny	Argentina	k1gFnSc2	Argentina
s	s	k7c7	s
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
měnovým	měnový	k2eAgInSc7d1	měnový
fondem	fond	k1gInSc7	fond
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Válka	válka	k1gFnSc1	válka
dvou	dva	k4xCgFnPc2	dva
Kristýn	Kristýna	k1gFnPc2	Kristýna
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
prezidentky	prezidentka	k1gFnSc2	prezidentka
Cristiny	Cristina	k1gFnSc2	Cristina
Kirchner	Kirchnra	k1gFnPc2	Kirchnra
a	a	k8xC	a
šéfky	šéfka	k1gFnSc2	šéfka
MMF	MMF	kA	MMF
Christine	Christin	k1gMnSc5	Christin
Lagarde	Lagard	k1gMnSc5	Lagard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
spor	spor	k1gInSc4	spor
o	o	k7c4	o
monetární	monetární	k2eAgFnSc4d1	monetární
suverenitu	suverenita	k1gFnSc4	suverenita
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
o	o	k7c4	o
souboj	souboj	k1gInSc4	souboj
keynesiánského	keynesiánský	k2eAgInSc2d1	keynesiánský
a	a	k8xC	a
neoliberálního	neoliberální	k2eAgInSc2d1	neoliberální
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
činil	činit	k5eAaImAgInS	činit
HDP	HDP	kA	HDP
Argentiny	Argentina	k1gFnSc2	Argentina
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
kupní	kupní	k2eAgFnSc4d1	kupní
sílu	síla	k1gFnSc4	síla
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
17	[number]	k4	17
660	[number]	k4	660
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
stavělo	stavět	k5eAaImAgNnS	stavět
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Litvy	Litva	k1gFnSc2	Litva
nebo	nebo	k8xC	nebo
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetik	k1gMnSc4	energetik
a	a	k8xC	a
jaderný	jaderný	k2eAgInSc4d1	jaderný
program	program	k1gInSc4	program
===	===	k?	===
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
disponuje	disponovat	k5eAaBmIp3nS	disponovat
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
nejrozvinutějším	rozvinutý	k2eAgInSc7d3	nejrozvinutější
jaderným	jaderný	k2eAgInSc7d1	jaderný
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
jaderných	jaderný	k2eAgFnPc2d1	jaderná
technologií	technologie	k1gFnPc2	technologie
navíc	navíc	k6eAd1	navíc
probíhá	probíhat	k5eAaImIp3nS	probíhat
bez	bez	k7c2	bez
výrazné	výrazný	k2eAgFnSc2d1	výrazná
podpory	podpora	k1gFnSc2	podpora
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc4d1	vlastní
koncept	koncept	k1gInSc4	koncept
velkého	velký	k2eAgInSc2d1	velký
reaktoru	reaktor	k1gInSc2	reaktor
ovšem	ovšem	k9	ovšem
Argentina	Argentina	k1gFnSc1	Argentina
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
reaktor	reaktor	k1gInSc1	reaktor
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
a	a	k8xC	a
první	první	k4xOgInSc1	první
komerční	komerční	k2eAgInSc1d1	komerční
reaktor	reaktor	k1gInSc1	reaktor
(	(	kIx(	(
<g/>
elektrárna	elektrárna	k1gFnSc1	elektrárna
Atucha	Atucha	k1gFnSc1	Atucha
I	i	k9	i
<g/>
)	)	kIx)	)
následoval	následovat	k5eAaImAgInS	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
těžkovodní	těžkovodní	k2eAgInSc4d1	těžkovodní
reaktor	reaktor	k1gInSc4	reaktor
(	(	kIx(	(
<g/>
PHWR	PHWR	kA	PHWR
<g/>
)	)	kIx)	)
od	od	k7c2	od
korporace	korporace	k1gFnSc2	korporace
Siemens	siemens	k1gInSc1	siemens
<g/>
.	.	kIx.	.
</s>
<s>
Embalse	Embalse	k1gFnSc1	Embalse
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
těžkovodním	těžkovodní	k2eAgInSc7d1	těžkovodní
reaktorem	reaktor	k1gInSc7	reaktor
(	(	kIx(	(
<g/>
PHWR	PHWR	kA	PHWR
<g/>
)	)	kIx)	)
kanadské	kanadský	k2eAgFnSc2d1	kanadská
společnosti	společnost	k1gFnSc2	společnost
CANDU	CANDU	kA	CANDU
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byly	být	k5eAaImAgFnP	být
argentinské	argentinský	k2eAgFnPc1d1	Argentinská
jaderné	jaderný	k2eAgFnPc1d1	jaderná
technologie	technologie	k1gFnPc1	technologie
využity	využít	k5eAaPmNgFnP	využít
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
možnosti	možnost	k1gFnPc4	možnost
vyrábět	vyrábět	k5eAaImF	vyrábět
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
disponuje	disponovat	k5eAaBmIp3nS	disponovat
potřebnými	potřebný	k2eAgFnPc7d1	potřebná
technologiemi	technologie	k1gFnPc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
guvernérů	guvernér	k1gMnPc2	guvernér
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
MAAE	MAAE	kA	MAAE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
argentinský	argentinský	k2eAgInSc1d1	argentinský
jaderný	jaderný	k2eAgInSc1d1	jaderný
výzkum	výzkum	k1gInSc1	výzkum
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
nízkokapactní	nízkokapactní	k2eAgInPc4d1	nízkokapactní
reaktory	reaktor	k1gInPc4	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
příliš	příliš	k6eAd1	příliš
rozvíjena	rozvíjen	k2eAgFnSc1d1	rozvíjena
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
USA	USA	kA	USA
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
napřed	napřed	k6eAd1	napřed
a	a	k8xC	a
tak	tak	k6eAd1	tak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úspěchu	úspěch	k1gInSc2	úspěch
může	moct	k5eAaImIp3nS	moct
ovládnout	ovládnout	k5eAaPmF	ovládnout
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
zahájeny	zahájen	k2eAgFnPc4d1	zahájena
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
demonstračním	demonstrační	k2eAgInSc6d1	demonstrační
projektu	projekt	k1gInSc6	projekt
CAREM	car	k1gMnSc7	car
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
reaktor	reaktor	k1gInSc1	reaktor
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
výkon	výkon	k1gInSc4	výkon
25	[number]	k4	25
MW	MW	kA	MW
a	a	k8xC	a
původně	původně	k6eAd1	původně
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgFnPc4d1	jaderná
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Palivem	palivo	k1gNnSc7	palivo
bude	být	k5eAaImBp3nS	být
uran	uran	k1gInSc1	uran
obohacený	obohacený	k2eAgInSc1d1	obohacený
na	na	k7c6	na
3,4	[number]	k4	3,4
%	%	kIx~	%
a	a	k8xC	a
reaktor	reaktor	k1gInSc1	reaktor
bude	být	k5eAaImBp3nS	být
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
tlakovodním	tlakovodní	k2eAgInSc6d1	tlakovodní
principu	princip	k1gInSc6	princip
(	(	kIx(	(
<g/>
PWR	PWR	kA	PWR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
pasivních	pasivní	k2eAgInPc2d1	pasivní
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Demonstrační	demonstrační	k2eAgFnSc1d1	demonstrační
elektrárna	elektrárna	k1gFnSc1	elektrárna
bude	být	k5eAaImBp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Formosa	Formosa	k1gFnSc1	Formosa
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
výkon	výkon	k1gInSc1	výkon
systému	systém	k1gInSc2	systém
zvýšit	zvýšit	k5eAaPmF	zvýšit
na	na	k7c4	na
300	[number]	k4	300
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
cena	cena	k1gFnSc1	cena
prototypu	prototyp	k1gInSc2	prototyp
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
350	[number]	k4	350
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
u	u	k7c2	u
případných	případný	k2eAgInPc2d1	případný
sériových	sériový	k2eAgInPc2d1	sériový
výrobků	výrobek	k1gInPc2	výrobek
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
sníží	snížit	k5eAaPmIp3nS	snížit
na	na	k7c4	na
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
asi	asi	k9	asi
36	[number]	k4	36
260	[number]	k4	260
130	[number]	k4	130
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hustotě	hustota	k1gFnSc3	hustota
asi	asi	k9	asi
13	[number]	k4	13
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
40	[number]	k4	40
091	[number]	k4	091
359	[number]	k4	359
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Přibližně	přibližně	k6eAd1	přibližně
87	[number]	k4	87
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
11,5	[number]	k4	11,5
milionu	milion	k4xCgInSc2	milion
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c6	na
aglomeraci	aglomerace	k1gFnSc6	aglomerace
[[	[[	k?	[[
<g/>
Gran	Gran	k1gInSc1	Gran
[	[	kIx(	[
<g/>
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
]]	]]	k?	]]
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
asi	asi	k9	asi
2	[number]	k4	2
989	[number]	k4	989
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
provincie	provincie	k1gFnSc1	provincie
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
16,6	[number]	k4	16,6
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
provincie	provincie	k1gFnSc1	provincie
Córdoba	Córdoba	k1gFnSc1	Córdoba
a	a	k8xC	a
Santa	Santa	k1gFnSc1	Santa
Fe	Fe	k1gFnSc2	Fe
každá	každý	k3xTgFnSc1	každý
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
třech	tři	k4xCgFnPc6	tři
<g/>
,	,	kIx,	,
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
ležících	ležící	k2eAgFnPc6d1	ležící
provinciích	provincie	k1gFnPc6	provincie
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbývající	zbývající	k2eAgFnPc1d1	zbývající
<g/>
,	,	kIx,	,
vzdálenější	vzdálený	k2eAgFnPc1d2	vzdálenější
části	část	k1gFnPc1	část
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
osídleny	osídlit	k5eAaPmNgFnP	osídlit
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
připadají	připadat	k5eAaPmIp3nP	připadat
na	na	k7c4	na
kilometr	kilometr	k1gInSc4	kilometr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
tři	tři	k4xCgMnPc1	tři
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Etnografie	etnografie	k1gFnSc2	etnografie
===	===	k?	===
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
nově	nově	k6eAd1	nově
osídelných	osídelný	k2eAgFnPc6d1	osídelný
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
a	a	k8xC	a
Uruguay	Uruguay	k1gFnSc1	Uruguay
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Argentina	Argentina	k1gFnSc1	Argentina
zemí	zem	k1gFnPc2	zem
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
zejména	zejména	k9	zejména
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Argentina	Argentina	k1gFnSc1	Argentina
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
imigrační	imigrační	k2eAgFnSc7d1	imigrační
vlnou	vlna	k1gFnSc7	vlna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
6,6	[number]	k4	6,6
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
po	po	k7c6	po
USA	USA	kA	USA
(	(	kIx(	(
<g/>
množství	množství	k1gNnSc1	množství
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
asi	asi	k9	asi
27	[number]	k4	27
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
a	a	k8xC	a
před	před	k7c7	před
zeměmi	zem	k1gFnPc7	zem
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
.	.	kIx.	.
<g/>
Pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
populace	populace	k1gFnSc2	populace
každé	každý	k3xTgFnSc2	každý
dvě	dva	k4xCgFnPc4	dva
desetiletí	desetiletí	k1gNnPc2	desetiletí
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
přežil	přežít	k5eAaPmAgMnS	přežít
ve	v	k7c6	v
rčení	rčení	k1gNnSc6	rčení
Los	los	k1gMnSc1	los
Argentinos	Argentinos	k1gMnSc1	Argentinos
descienden	desciendna	k1gFnPc2	desciendna
de	de	k?	de
los	los	k1gInSc1	los
Barcos	Barcos	k1gInSc1	Barcos
(	(	kIx(	(
<g/>
Argentinci	Argentinec	k1gMnPc1	Argentinec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
lodí	loď	k1gFnPc2	loď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
Argentinců	Argentinec	k1gMnPc2	Argentinec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
imigrační	imigrační	k2eAgFnSc2d1	imigrační
vlny	vlna	k1gFnSc2	vlna
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1850	[number]	k4	1850
až	až	k6eAd1	až
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgMnPc2	tento
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
přišla	přijít	k5eAaPmAgFnS	přijít
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
Většina	většina	k1gFnSc1	většina
Argentinců	Argentinec	k1gMnPc2	Argentinec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
několika	několik	k4yIc2	několik
evropských	evropský	k2eAgFnPc2d1	Evropská
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Převažuje	převažovat	k5eAaImIp3nS	převažovat
italská	italský	k2eAgFnSc1d1	italská
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
55	[number]	k4	55
%	%	kIx~	%
Argentinců	Argentinec	k1gMnPc2	Argentinec
má	mít	k5eAaImIp3nS	mít
italský	italský	k2eAgInSc1d1	italský
původ	původ	k1gInSc1	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
španělská	španělský	k2eAgFnSc1d1	španělská
většina	většina	k1gFnSc1	většina
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
17	[number]	k4	17
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
má	mít	k5eAaImIp3nS	mít
francouzský	francouzský	k2eAgInSc4d1	francouzský
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
asi	asi	k9	asi
8	[number]	k4	8
%	%	kIx~	%
německý	německý	k2eAgMnSc1d1	německý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
významnou	významný	k2eAgFnSc4d1	významná
populaci	populace	k1gFnSc4	populace
Arabů	Arab	k1gMnPc2	Arab
či	či	k8xC	či
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
částečným	částečný	k2eAgInSc7d1	částečný
arabským	arabský	k2eAgInSc7d1	arabský
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Libanonu	Libanon	k1gInSc2	Libanon
(	(	kIx(	(
<g/>
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
bělochy	běloch	k1gMnPc4	běloch
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asijská	asijský	k2eAgFnSc1d1	asijská
populace	populace	k1gFnSc1	populace
tvoří	tvořit	k5eAaImIp3nS	tvořit
zhruba	zhruba	k6eAd1	zhruba
180	[number]	k4	180
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
čínskéhoa	čínskéhoa	k6eAd1	čínskéhoa
korejského	korejský	k2eAgInSc2d1	korejský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
existuje	existovat	k5eAaImIp3nS	existovat
významná	významný	k2eAgFnSc1d1	významná
japonská	japonský	k2eAgFnSc1d1	japonská
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
původ	původ	k1gInSc4	původ
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
provedená	provedený	k2eAgFnSc1d1	provedená
argentinským	argentinský	k2eAgMnSc7d1	argentinský
genetikem	genetik	k1gMnSc7	genetik
Danielem	Daniel	k1gMnSc7	Daniel
Corachech	Corachech	k1gInSc4	Corachech
na	na	k7c4	na
218	[number]	k4	218
jedincích	jedinec	k1gMnPc6	jedinec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
genetická	genetický	k2eAgFnSc1d1	genetická
mapa	mapa	k1gFnSc1	mapa
Argentinců	Argentinec	k1gMnPc2	Argentinec
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
79	[number]	k4	79
%	%	kIx~	%
z	z	k7c2	z
různých	různý	k2eAgMnPc2d1	různý
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
především	především	k9	především
italského	italský	k2eAgMnSc2d1	italský
a	a	k8xC	a
španělské	španělský	k2eAgNnSc1d1	španělské
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
18	[number]	k4	18
%	%	kIx~	%
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
domorodých	domorodý	k2eAgNnPc2d1	domorodé
etnik	etnikum	k1gNnPc2	etnikum
a	a	k8xC	a
4,3	[number]	k4	4,3
%	%	kIx~	%
z	z	k7c2	z
afrických	africký	k2eAgFnPc2d1	africká
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
63,6	[number]	k4	63,6
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
testované	testovaný	k2eAgFnSc2d1	testovaná
skupiny	skupina	k1gFnSc2	skupina
mělo	mít	k5eAaImAgNnS	mít
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jednoho	jeden	k4xCgMnSc4	jeden
domorodého	domorodý	k2eAgMnSc4d1	domorodý
předka	předek	k1gMnSc4	předek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
93	[number]	k4	93
%	%	kIx~	%
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
východních	východní	k2eAgMnPc2d1	východní
katolíků	katolík	k1gMnPc2	katolík
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
)	)	kIx)	)
arménští	arménský	k2eAgMnPc1d1	arménský
katolíci	katolík	k1gMnPc1	katolík
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc4	Řehoř
z	z	k7c2	z
Nareku	Narek	k1gInSc2	Narek
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
–	–	k?	–
16	[number]	k4	16
600	[number]	k4	600
věřících	věřící	k2eAgMnPc2d1	věřící
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
–	–	k?	–
16	[number]	k4	16
350	[number]	k4	350
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
i	i	k8xC	i
2000	[number]	k4	2000
–	–	k?	–
16	[number]	k4	16
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
)	)	kIx)	)
maronité	maronitý	k2eAgFnSc2d1	maronitý
<g/>
:	:	kIx,	:
Eparchie	eparchie	k1gFnSc2	eparchie
sv.	sv.	kA	sv.
Šarbela	Šarbela	k1gFnSc1	Šarbela
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
–	–	k?	–
720	[number]	k4	720
000	[number]	k4	000
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
–	–	k?	–
2014	[number]	k4	2014
–	–	k?	–
713	[number]	k4	713
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
i	i	k8xC	i
2000	[number]	k4	2000
–	–	k?	–
700	[number]	k4	700
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
)	)	kIx)	)
řeckokatolíci-melchité	řeckokatolícielchitý	k2eAgInPc1d1	řeckokatolíci-melchitý
<g/>
:	:	kIx,	:
Apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
exarchát	exarchát	k1gInSc1	exarchát
v	v	k7c6	v
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Cordoba	Cordoba	k1gFnSc1	Cordoba
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
–	–	k?	–
310	[number]	k4	310
700	[number]	k4	700
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
–	–	k?	–
305	[number]	k4	305
400	[number]	k4	400
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
–	–	k?	–
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
1	[number]	k4	1
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
protestanti	protestant	k1gMnPc1	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
i	i	k9	i
další	další	k2eAgMnPc1d1	další
křesťané	křesťan	k1gMnPc1	křesťan
arabského	arabský	k2eAgInSc2d1	arabský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
pravoslavných	pravoslavný	k2eAgInPc2d1	pravoslavný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
rodiny	rodina	k1gFnSc2	rodina
syrsko-arménského	syrskorménský	k2eAgInSc2d1	syrsko-arménský
původu	původ	k1gInSc2	původ
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Carlos	Carlos	k1gMnSc1	Carlos
Saúl	Saúl	k1gMnSc1	Saúl
Menem	Menem	k1gInSc4	Menem
<g/>
.	.	kIx.	.
</s>
<s>
Minoritu	minorita	k1gFnSc4	minorita
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2	[number]	k4	2
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
mnohonárodnostním	mnohonárodnostní	k2eAgInSc7d1	mnohonárodnostní
a	a	k8xC	a
multikulturním	multikulturní	k2eAgInSc7d1	multikulturní
charakterem	charakter	k1gInSc7	charakter
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
silnými	silný	k2eAgFnPc7d1	silná
formami	forma	k1gFnPc7	forma
projevy	projev	k1gInPc7	projev
synkretismu	synkretismus	k1gInSc2	synkretismus
a	a	k8xC	a
kladného	kladný	k2eAgNnSc2d1	kladné
hodnocení	hodnocení	k1gNnSc2	hodnocení
pokroku	pokrok	k1gInSc2	pokrok
a	a	k8xC	a
modernity	modernita	k1gFnSc2	modernita
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
bez	bez	k7c2	bez
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
dvojím	dvojí	k4xRgInSc7	dvojí
pocitem	pocit	k1gInSc7	pocit
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
a	a	k8xC	a
latinskoamerickou	latinskoamerický	k2eAgFnSc7d1	latinskoamerická
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mexický	mexický	k2eAgMnSc1d1	mexický
básník	básník	k1gMnSc1	básník
Octavio	Octavio	k1gMnSc1	Octavio
Paz	Paz	k1gMnSc1	Paz
navíc	navíc	k6eAd1	navíc
jednou	jednou	k6eAd1	jednou
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Argentinci	Argentinec	k1gMnPc1	Argentinec
jsou	být	k5eAaImIp3nP	být
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stvořeni	stvořen	k2eAgMnPc1d1	stvořen
po	po	k7c6	po
francouzsku	francouzsko	k1gNnSc6	francouzsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
argentinským	argentinský	k2eAgMnSc7d1	argentinský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
je	být	k5eAaImIp3nS	být
Jorge	Jorge	k1gInSc1	Jorge
Luis	Luisa	k1gFnPc2	Luisa
Borges	Borges	k1gInSc1	Borges
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
k	k	k7c3	k
literatuře	literatura	k1gFnSc3	literatura
fantastické	fantastický	k2eAgInPc4d1	fantastický
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
vrcholného	vrcholný	k2eAgMnSc4d1	vrcholný
představitele	představitel	k1gMnSc4	představitel
magického	magický	k2eAgInSc2d1	magický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
jihoamerickým	jihoamerický	k2eAgInSc7d1	jihoamerický
kulturním	kulturní	k2eAgInSc7d1	kulturní
fenoménem	fenomén	k1gInSc7	fenomén
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Borges	Borges	k1gMnSc1	Borges
Cervantesovu	Cervantesův	k2eAgFnSc4d1	Cervantesova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
nejprestižnější	prestižní	k2eAgNnSc4d3	nejprestižnější
ocenění	ocenění	k1gNnSc4	ocenění
pro	pro	k7c4	pro
španělskojazyčné	španělskojazyčný	k2eAgMnPc4d1	španělskojazyčný
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgMnSc1	první
Argentinec	Argentinec	k1gMnSc1	Argentinec
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
Ernesto	Ernesta	k1gMnSc5	Ernesta
Sábato	Sábat	k2eAgNnSc1d1	Sábato
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
proslul	proslout	k5eAaPmAgMnS	proslout
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgInPc7	svůj
eseji	esej	k1gInPc7	esej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
přišel	přijít	k5eAaPmAgMnS	přijít
Adolfo	Adolfo	k1gMnSc1	Adolfo
Bioy	Bioa	k1gFnSc2	Bioa
Casares	Casares	k1gMnSc1	Casares
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgMnSc1d1	blízký
Borgesův	Borgesův	k2eAgMnSc1d1	Borgesův
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
představitel	představitel	k1gMnSc1	představitel
sci-fi	scii	k1gFnSc2	sci-fi
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Morelův	Morelův	k2eAgInSc4d1	Morelův
vynález	vynález	k1gInSc4	vynález
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
posledním	poslední	k2eAgMnSc7d1	poslední
oceněným	oceněný	k2eAgMnSc7d1	oceněný
Argentincem	Argentinec	k1gMnSc7	Argentinec
je	být	k5eAaImIp3nS	být
Juan	Juan	k1gMnSc1	Juan
Gelman	Gelman	k1gMnSc1	Gelman
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
velkým	velký	k2eAgNnSc7d1	velké
tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
exil	exil	k1gInSc4	exil
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
byl	být	k5eAaImAgInS	být
donucen	donutit	k5eAaPmNgInS	donutit
politickými	politický	k2eAgFnPc7d1	politická
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
tématu	téma	k1gNnSc3	téma
dětí	dítě	k1gFnPc2	dítě
odebraných	odebraný	k2eAgFnPc2d1	odebraná
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
během	během	k7c2	během
špinavé	špinavý	k2eAgFnSc2d1	špinavá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
významným	významný	k2eAgMnSc7d1	významný
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
však	však	k9	však
i	i	k9	i
Julio	Julio	k1gMnSc1	Julio
Cortázar	Cortázar	k1gMnSc1	Cortázar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Borgesem	Borges	k1gInSc7	Borges
a	a	k8xC	a
Cesaresem	Cesares	k1gInSc7	Cesares
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
nové	nový	k2eAgFnSc2d1	nová
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
fantastické	fantastický	k2eAgFnSc2d1	fantastická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
iniciátora	iniciátor	k1gMnSc4	iniciátor
"	"	kIx"	"
<g/>
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
kulturního	kulturní	k2eAgInSc2d1	kulturní
boomu	boom	k1gInSc2	boom
<g/>
"	"	kIx"	"
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
okruhu	okruh	k1gInSc2	okruh
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Victoria	Victorium	k1gNnSc2	Victorium
Ocampová	Ocampová	k1gFnSc1	Ocampová
<g/>
,	,	kIx,	,
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
a	a	k8xC	a
vydavatelka	vydavatelka	k1gFnSc1	vydavatelka
slavného	slavný	k2eAgInSc2d1	slavný
literárního	literární	k2eAgInSc2d1	literární
časopisu	časopis	k1gInSc2	časopis
Sur	Sur	k1gFnSc2	Sur
<g/>
.	.	kIx.	.
</s>
<s>
Prozaik	prozaik	k1gMnSc1	prozaik
Manuel	Manuel	k1gMnSc1	Manuel
Puig	Puig	k1gMnSc1	Puig
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
především	především	k9	především
vliv	vliv	k1gInSc4	vliv
populární	populární	k2eAgFnSc2d1	populární
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
šoubyznysu	šoubyznys	k1gInSc2	šoubyznys
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýraznějším	výrazný	k2eAgMnPc3d3	nejvýraznější
představitelům	představitel	k1gMnPc3	představitel
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
poezie	poezie	k1gFnSc2	poezie
patřili	patřit	k5eAaImAgMnP	patřit
José	José	k1gNnSc4	José
Hernández	Hernándeza	k1gFnPc2	Hernándeza
<g/>
,	,	kIx,	,
Alfonsina	Alfonsina	k1gFnSc1	Alfonsina
Storniová	Storniový	k2eAgFnSc1d1	Storniový
a	a	k8xC	a
Alejandra	Alejandra	k1gFnSc1	Alejandra
Pizarniková	Pizarnikový	k2eAgFnSc1d1	Pizarnikový
<g/>
.	.	kIx.	.
</s>
<s>
Knihami	kniha	k1gFnPc7	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
María	María	k1gFnSc1	María
Elena	Elena	k1gFnSc1	Elena
Walshová	Walshová	k1gFnSc1	Walshová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malířka	malířka	k1gFnSc1	malířka
Leonor	Leonora	k1gFnPc2	Leonora
Finiová	Finiový	k2eAgFnSc1d1	Finiový
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1	proslulý
kreslířem	kreslíř	k1gMnSc7	kreslíř
a	a	k8xC	a
karikaturistou	karikaturista	k1gMnSc7	karikaturista
je	být	k5eAaImIp3nS	být
Quino	Quino	k6eAd1	Quino
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
avantgardním	avantgardní	k2eAgMnSc7d1	avantgardní
sochařem	sochař	k1gMnSc7	sochař
byl	být	k5eAaImAgMnS	být
Lucio	Lucio	k1gMnSc1	Lucio
Fontana	Fontan	k1gMnSc2	Fontan
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
César	César	k1gMnSc1	César
Pelli	Pell	k1gMnSc3	Pell
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
výškovými	výškový	k2eAgFnPc7d1	výšková
stavbami	stavba	k1gFnPc7	stavba
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Petronas	Petronas	k1gInSc4	Petronas
Towers	Towersa	k1gFnPc2	Towersa
v	v	k7c4	v
Kuala	Kualo	k1gNnPc4	Kualo
Lumpur	Lumpura	k1gFnPc2	Lumpura
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Carnegie	Carnegie	k1gFnSc1	Carnegie
Hall	Hallum	k1gNnPc2	Hallum
Tower	Towero	k1gNnPc2	Towero
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Gustavo	Gustava	k1gFnSc5	Gustava
Santaolalla	Santaolall	k1gMnSc2	Santaolall
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmům	film	k1gInPc3	film
Zkrocená	zkrocený	k2eAgFnSc1d1	Zkrocená
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Babel	Babel	k1gMnSc1	Babel
<g/>
.	.	kIx.	.
</s>
<s>
Lalo	Lalo	k6eAd1	Lalo
Schifrin	Schifrin	k1gInSc1	Schifrin
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
proslulého	proslulý	k2eAgInSc2d1	proslulý
hudebního	hudební	k2eAgInSc2d1	hudební
motivu	motiv	k1gInSc2	motiv
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
originální	originální	k2eAgNnSc4d1	originální
spojení	spojení	k1gNnSc4	spojení
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
Mauricio	Mauricio	k1gMnSc1	Mauricio
Kagel	Kagel	k1gMnSc1	Kagel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
klasickým	klasický	k2eAgMnPc3d1	klasický
skladatelům	skladatel	k1gMnPc3	skladatel
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Alberto	Alberta	k1gFnSc5	Alberta
Ginastera	Ginaster	k1gMnSc4	Ginaster
<g/>
,	,	kIx,	,
Ariel	Ariel	k1gMnSc1	Ariel
Ramírez	Ramírez	k1gMnSc1	Ramírez
či	či	k8xC	či
Erich	Erich	k1gMnSc1	Erich
Kleiber	Kleiber	k1gMnSc1	Kleiber
<g/>
.	.	kIx.	.
</s>
<s>
Slavnou	slavný	k2eAgFnSc7d1	slavná
klavíristkou	klavíristka	k1gFnSc7	klavíristka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Martha	Martha	k1gFnSc1	Martha
Argerichová	Argerichová	k1gFnSc1	Argerichová
<g/>
,	,	kIx,	,
dirigentem	dirigent	k1gMnSc7	dirigent
pak	pak	k8xC	pak
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
jazzové	jazzový	k2eAgFnSc2d1	jazzová
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Gato	Gato	k1gMnSc1	Gato
Barbieri	Barbier	k1gFnSc2	Barbier
<g/>
,	,	kIx,	,
ve	v	k7c6	v
folku	folk	k1gInSc6	folk
Facundo	Facundo	k6eAd1	Facundo
Cabral	Cabral	k1gFnSc1	Cabral
<g/>
,	,	kIx,	,
Mercedes	mercedes	k1gInSc1	mercedes
Sosa	Sosa	k1gFnSc1	Sosa
či	či	k8xC	či
Atahualpa	Atahualpa	k1gFnSc1	Atahualpa
Yupanqui	Yupanqu	k1gFnSc2	Yupanqu
<g/>
,	,	kIx,	,
v	v	k7c6	v
rocku	rock	k1gInSc6	rock
Gustavo	Gustava	k1gFnSc5	Gustava
Cerati	Cerat	k1gMnPc5	Cerat
<g/>
,	,	kIx,	,
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
tenor	tenor	k1gMnSc1	tenor
José	Josá	k1gFnSc2	Josá
Cura	Cura	k1gMnSc1	Cura
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
argentinským	argentinský	k2eAgInSc7d1	argentinský
tancem	tanec	k1gInSc7	tanec
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tango	tango	k1gNnSc1	tango
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
taneční	taneční	k2eAgInSc1d1	taneční
a	a	k8xC	a
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
a	a	k8xC	a
Uruguayí	Uruguay	k1gFnSc7	Uruguay
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
však	však	k9	však
s	s	k7c7	s
argentinským	argentinský	k2eAgNnSc7d1	argentinské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
krále	král	k1gMnSc2	král
tanga	tango	k1gNnSc2	tango
je	být	k5eAaImIp3nS	být
připisován	připisovat	k5eAaImNgInS	připisovat
ke	k	k7c3	k
Carlosi	Carlose	k1gFnSc3	Carlose
Gardelovi	Gardelův	k2eAgMnPc1d1	Gardelův
<g/>
.	.	kIx.	.
</s>
<s>
Mistry	mistr	k1gMnPc7	mistr
tanga	tango	k1gNnSc2	tango
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
Astor	Astor	k1gMnSc1	Astor
Piazzolla	Piazzolla	k1gMnSc1	Piazzolla
<g/>
,	,	kIx,	,
Aníbal	Aníbal	k1gInSc1	Aníbal
Troilo	Troila	k1gFnSc5	Troila
nebo	nebo	k8xC	nebo
Osvaldo	Osvaldo	k1gNnSc4	Osvaldo
Pugliese	Pugliese	k1gFnSc2	Pugliese
<g/>
.	.	kIx.	.
</s>
<s>
Argentinský	argentinský	k2eAgInSc1d1	argentinský
folklor	folklor	k1gInSc1	folklor
se	se	k3xPyFc4	se
sbírá	sbírat	k5eAaImIp3nS	sbírat
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
uměleckých	umělecký	k2eAgInPc2d1	umělecký
projektů	projekt	k1gInPc2	projekt
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
dnes	dnes	k6eAd1	dnes
převládají	převládat	k5eAaImIp3nP	převládat
tradiční	tradiční	k2eAgInPc4d1	tradiční
styly	styl	k1gInPc4	styl
například	například	k6eAd1	například
zambas	zambasa	k1gFnPc2	zambasa
<g/>
,	,	kIx,	,
cuecas	cuecasa	k1gFnPc2	cuecasa
<g/>
,	,	kIx,	,
chacarerar	chacarerara	k1gFnPc2	chacarerara
<g/>
,	,	kIx,	,
chamarritas	chamarritasa	k1gFnPc2	chamarritasa
<g/>
,	,	kIx,	,
chamamés	chamamésa	k1gFnPc2	chamamésa
<g/>
,	,	kIx,	,
malambo	malamba	k1gMnSc5	malamba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
produkují	produkovat	k5eAaImIp3nP	produkovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgMnSc7d1	velký
filmovým	filmový	k2eAgMnSc7d1	filmový
provokatérem	provokatér	k1gMnSc7	provokatér
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
Gaspar	Gaspar	k1gMnSc1	Gaspar
Noé	Noé	k1gMnSc1	Noé
<g/>
,	,	kIx,	,
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
jsou	být	k5eAaImIp3nP	být
ceněny	ceněn	k2eAgInPc1d1	ceněn
filmy	film	k1gInPc1	film
Lucrecie	Lucrecie	k1gFnSc2	Lucrecie
Martelové	Martelový	k2eAgInPc1d1	Martelový
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hereckým	herecký	k2eAgFnPc3d1	herecká
hvězdám	hvězda	k1gFnPc3	hvězda
latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
patřila	patřit	k5eAaImAgFnS	patřit
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
Libertad	Libertad	k1gInSc4	Libertad
Lamarqueová	Lamarqueová	k1gFnSc1	Lamarqueová
či	či	k8xC	či
Norma	Norma	k1gFnSc1	Norma
Aleandrová	Aleandrová	k1gFnSc1	Aleandrová
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
například	například	k6eAd1	například
Bérénice	Bérénice	k1gFnSc2	Bérénice
Bejo	Bejo	k6eAd1	Bejo
nebo	nebo	k8xC	nebo
Martina	Martina	k1gFnSc1	Martina
Stoesselová	Stoesselová	k1gFnSc1	Stoesselová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
Argentinci	Argentinec	k1gMnPc1	Argentinec
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
sport	sport	k1gInSc4	sport
považují	považovat	k5eAaImIp3nP	považovat
pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
celkem	celkem	k6eAd1	celkem
neznámý	známý	k2eNgInSc4d1	neznámý
sport	sport	k1gInSc4	sport
"	"	kIx"	"
<g/>
Pato	pata	k1gFnSc5	pata
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kachna	kachna	k1gFnSc1	kachna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
dvě	dva	k4xCgNnPc1	dva
mužstva	mužstvo	k1gNnPc1	mužstvo
jezdců	jezdec	k1gMnPc2	jezdec
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgInPc7d1	sousední
ranči	ranč	k1gInPc7	ranč
<g/>
,	,	kIx,	,
a	a	k8xC	a
vítězí	vítězit	k5eAaImIp3nS	vítězit
družstvo	družstvo	k1gNnSc1	družstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dopravilo	dopravit	k5eAaPmAgNnS	dopravit
svoji	svůj	k3xOyFgFnSc4	svůj
kachnu	kachna	k1gFnSc4	kachna
do	do	k7c2	do
soupeřova	soupeřův	k2eAgNnSc2d1	soupeřovo
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
federace	federace	k1gFnSc1	federace
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
sport	sport	k1gInSc4	sport
národní	národní	k2eAgInSc4d1	národní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
také	také	k9	také
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
organizace	organizace	k1gFnSc2	organizace
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
čtrnácti	čtrnáct	k4xCc2	čtrnáct
z	z	k7c2	z
osmnácti	osmnáct	k4xCc2	osmnáct
fotbalových	fotbalový	k2eAgNnPc2d1	fotbalové
světových	světový	k2eAgNnPc2d1	světové
mistrovství	mistrovství	k1gNnPc2	mistrovství
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
jej	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
-	-	kIx~	-
s	s	k7c7	s
pověstným	pověstný	k2eAgInSc7d1	pověstný
Maradonovým	Maradonův	k2eAgInSc7d1	Maradonův
gólem	gól	k1gInSc7	gól
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
třikrát	třikrát	k6eAd1	třikrát
došla	dojít	k5eAaPmAgFnS	dojít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
také	také	k9	také
Argentinci	Argentinec	k1gMnPc1	Argentinec
kralovali	kralovat	k5eAaImAgMnP	kralovat
fotbalovému	fotbalový	k2eAgInSc3d1	fotbalový
turnaji	turnaj	k1gInSc3	turnaj
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
)	)	kIx)	)
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Copa	Copa	k1gMnSc1	Copa
América	América	k1gMnSc1	América
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
argentinské	argentinský	k2eAgFnPc1d1	Argentinská
fotbalisty	fotbalista	k1gMnPc7	fotbalista
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
ligy	liga	k1gFnSc2	liga
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
Ángel	Ángel	k1gMnSc1	Ángel
Labruna	Labruna	k1gFnSc1	Labruna
<g/>
,	,	kIx,	,
brankář-inovátor	brankářnovátor	k1gMnSc1	brankář-inovátor
Amadeo	Amadeo	k1gMnSc1	Amadeo
Carrizo	Carriza	k1gFnSc5	Carriza
<g/>
,	,	kIx,	,
držitelé	držitel	k1gMnPc1	držitel
Zlatého	zlatý	k2eAgInSc2d1	zlatý
míče	míč	k1gInSc2	míč
Alfredo	Alfredo	k1gNnSc4	Alfredo
di	di	k?	di
Stéfano	Stéfana	k1gFnSc5	Stéfana
a	a	k8xC	a
Omar	Omar	k1gMnSc1	Omar
Sivori	Sivor	k1gMnPc1	Sivor
<g/>
,	,	kIx,	,
brankář	brankář	k1gMnSc1	brankář
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Ubaldo	Ubaldo	k1gNnSc1	Ubaldo
Fillol	Fillola	k1gFnPc2	Fillola
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Mario	Mario	k1gMnSc1	Mario
<g />
.	.	kIx.	.
</s>
<s>
Kempes	Kempes	k1gMnSc1	Kempes
<g/>
,	,	kIx,	,
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
vítěz	vítěz	k1gMnSc1	vítěz
Poháru	pohár	k1gInSc2	pohár
osvoboditelů	osvoboditel	k1gMnPc2	osvoboditel
Ricardo	Ricardo	k1gNnSc4	Ricardo
Bochini	Bochin	k1gMnPc1	Bochin
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
tahoun	tahoun	k1gMnSc1	tahoun
týmu	tým	k1gInSc2	tým
světových	světový	k2eAgMnPc2d1	světový
šampionů	šampion	k1gMnPc2	šampion
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
Diego	Diego	k6eAd1	Diego
Armando	Armanda	k1gFnSc5	Armanda
Maradona	Maradon	k1gMnSc4	Maradon
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
Daniel	Daniel	k1gMnSc1	Daniel
Passarella	Passarella	k1gMnSc1	Passarella
<g/>
,	,	kIx,	,
Gabriel	Gabriel	k1gMnSc1	Gabriel
Batistuta	Batistut	k1gMnSc2	Batistut
<g/>
,	,	kIx,	,
Hernán	Hernán	k2eAgInSc1d1	Hernán
Crespo	Crespa	k1gFnSc5	Crespa
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
Carlos	Carlos	k1gMnSc1	Carlos
Tévez	Tévez	k1gMnSc1	Tévez
<g/>
,	,	kIx,	,
Javier	Javier	k1gMnSc1	Javier
Zanetti	Zanetť	k1gFnSc2	Zanetť
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
Sergio	Sergio	k6eAd1	Sergio
Agüero	Agüero	k1gNnSc4	Agüero
<g/>
,	,	kIx,	,
Ángel	Ángel	k1gMnSc1	Ángel
Di	Di	k1gMnSc1	Di
María	María	k1gMnSc1	María
a	a	k8xC	a
především	především	k9	především
Lionel	Lionel	k1gInSc1	Lionel
Messi	Mess	k1gMnSc3	Mess
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
hvězd	hvězda	k1gFnPc2	hvězda
dějin	dějiny	k1gFnPc2	dějiny
sportu	sport	k1gInSc2	sport
<g/>
:	:	kIx,	:
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
vicemistr	vicemistr	k1gMnSc1	vicemistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
fotbalistou	fotbalista	k1gMnSc7	fotbalista
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
španělské	španělský	k2eAgFnSc2d1	španělská
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
třicet	třicet	k4xCc1	třicet
argentinských	argentinský	k2eAgMnPc2d1	argentinský
boxerů	boxer	k1gMnPc2	boxer
již	již	k6eAd1	již
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
světové	světový	k2eAgInPc4d1	světový
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
mimoto	mimoto	k6eAd1	mimoto
získali	získat	k5eAaPmAgMnP	získat
dalších	další	k2eAgInPc2d1	další
sedm	sedm	k4xCc4	sedm
zlatých	zlatý	k1gInPc2	zlatý
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
a	a	k8xC	a
deset	deset	k4xCc4	deset
bronzových	bronzový	k2eAgFnPc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Carlos	Carlos	k1gMnSc1	Carlos
Monzón	Monzón	k1gMnSc1	Monzón
držel	držet	k5eAaImAgMnS	držet
titul	titul	k1gInSc4	titul
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
váze	váha	k1gFnSc6	váha
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luciana	Luciana	k1gFnSc1	Luciana
Aymarová	Aymarová	k1gFnSc1	Aymarová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
osmkrát	osmkrát	k6eAd1	osmkrát
anketu	anketa	k1gFnSc4	anketa
o	o	k7c4	o
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
pozemní	pozemní	k2eAgFnSc4d1	pozemní
hokejistku	hokejistka	k1gFnSc4	hokejistka
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
volejbalové	volejbalový	k2eAgFnSc2d1	volejbalová
federace	federace	k1gFnSc2	federace
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgMnS	uvést
Hugo	Hugo	k1gMnSc1	Hugo
Conte	Cont	k1gInSc5	Cont
<g/>
.	.	kIx.	.
<g/>
Svou	svůj	k3xOyFgFnSc4	svůj
bohatou	bohatý	k2eAgFnSc4d1	bohatá
minulost	minulost	k1gFnSc4	minulost
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
také	také	k9	také
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
vždyť	vždyť	k9	vždyť
se	se	k3xPyFc4	se
Argentinci	Argentinec	k1gMnPc1	Argentinec
stali	stát	k5eAaPmAgMnP	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
i	i	k9	i
mistry	mistr	k1gMnPc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
však	však	k9	však
popularita	popularita	k1gFnSc1	popularita
košíkové	košíková	k1gFnSc2	košíková
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Zašlou	zašlý	k2eAgFnSc4d1	zašlá
slávu	sláva	k1gFnSc4	sláva
jí	on	k3xPp3gFnSc7	on
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
až	až	k9	až
založení	založení	k1gNnSc4	založení
Národní	národní	k2eAgFnSc2d1	národní
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
Liga	liga	k1gFnSc1	liga
Nacional	Nacional	k1gFnSc2	Nacional
de	de	k?	de
Básquet	Básquet	k1gMnSc1	Básquet
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
impulsem	impuls	k1gInSc7	impuls
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
argentinský	argentinský	k2eAgMnSc1d1	argentinský
basketbalista	basketbalista	k1gMnSc1	basketbalista
v	v	k7c6	v
NBA	NBA	kA	NBA
<g/>
,	,	kIx,	,
Manu	manout	k5eAaImIp1nS	manout
Ginóbili	Ginóbili	k1gMnSc1	Ginóbili
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
titulu	titul	k1gInSc2	titul
pro	pro	k7c4	pro
mistry	mistr	k1gMnPc4	mistr
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
země	země	k1gFnSc1	země
může	moct	k5eAaImIp3nS	moct
pochlubit	pochlubit	k5eAaPmF	pochlubit
finálovou	finálový	k2eAgFnSc7d1	finálová
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
MS	MS	kA	MS
2002	[number]	k4	2002
v	v	k7c6	v
Indianapolis	Indianapolis	k1gFnSc6	Indianapolis
<g/>
,	,	kIx,	,
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
místem	místo	k1gNnSc7	místo
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
2006	[number]	k4	2006
a	a	k8xC	a
zlatou	zlatá	k1gFnSc4	zlatá
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
athénské	athénský	k2eAgFnSc2d1	Athénská
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ceněna	cenit	k5eAaImNgFnS	cenit
nejvýše	vysoce	k6eAd3	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Oscar	Oscar	k1gMnSc1	Oscar
Furlong	Furlong	k1gMnSc1	Furlong
a	a	k8xC	a
Ricardo	Ricardo	k1gNnSc1	Ricardo
González	Gonzáleza	k1gFnPc2	Gonzáleza
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
hvězdami	hvězda	k1gFnPc7	hvězda
argentinského	argentinský	k2eAgInSc2d1	argentinský
tenisu	tenis	k1gInSc2	tenis
jsou	být	k5eAaImIp3nP	být
Guillermo	Guillerma	k1gFnSc5	Guillerma
Vilas	Vilas	k1gInSc1	Vilas
a	a	k8xC	a
Gabriela	Gabriela	k1gFnSc1	Gabriela
Sabatini	Sabatin	k2eAgMnPc1d1	Sabatin
<g/>
.	.	kIx.	.
</s>
<s>
Nynějšími	nynější	k2eAgMnPc7d1	nynější
reprezentanty	reprezentant	k1gMnPc7	reprezentant
Argentiny	Argentina	k1gFnSc2	Argentina
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
jsou	být	k5eAaImIp3nP	být
David	David	k1gMnSc1	David
Nalbandian	Nalbandian	k1gMnSc1	Nalbandian
<g/>
,	,	kIx,	,
šampión	šampión	k1gMnSc1	šampión
Masters	Masters	k1gInSc1	Masters
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Martín	Martína	k1gFnPc2	Martína
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
(	(	kIx(	(
<g/>
vítěz	vítěz	k1gMnSc1	vítěz
US	US	kA	US
Open	Open	k1gMnSc1	Open
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gastón	Gastón	k1gMnSc1	Gastón
Gaudio	Gaudio	k1gMnSc1	Gaudio
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Roland	Rolanda	k1gFnPc2	Rolanda
Garros	Garrosa	k1gFnPc2	Garrosa
2004	[number]	k4	2004
a	a	k8xC	a
také	také	k9	také
argentinští	argentinský	k2eAgMnPc1d1	argentinský
debloví	deblový	k2eAgMnPc1d1	deblový
zabijáci	zabiják	k1gMnPc1	zabiják
Guillermo	Guillerma	k1gFnSc5	Guillerma
Coria	Coria	k1gFnSc1	Coria
a	a	k8xC	a
Paola	Paola	k1gFnSc1	Paola
Suárezová	Suárezová	k1gFnSc1	Suárezová
(	(	kIx(	(
<g/>
triumfy	triumf	k1gInPc4	triumf
z	z	k7c2	z
OH	OH	kA	OH
2004	[number]	k4	2004
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
Roland	Rolanda	k1gFnPc2	Rolanda
Garros	Garrosa	k1gFnPc2	Garrosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Argentinci	Argentinec	k1gMnPc1	Argentinec
byli	být	k5eAaImAgMnP	být
celkem	celkem	k6eAd1	celkem
pětkrát	pětkrát	k6eAd1	pětkrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedno	jeden	k4xCgNnSc1	jeden
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
ragby	ragby	k1gNnSc1	ragby
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
ještě	ještě	k6eAd1	ještě
amatérským	amatérský	k2eAgInSc7d1	amatérský
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
už	už	k6eAd1	už
přes	přes	k7c4	přes
sedmdesát	sedmdesát	k4xCc4	sedmdesát
tisíc	tisíc	k4xCgInPc2	tisíc
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
šestkou	šestka	k1gFnSc7	šestka
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
Los	los	k1gInSc1	los
Pumas	Pumasa	k1gFnPc2	Pumasa
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
cenným	cenný	k2eAgInSc7d1	cenný
kovem	kov	k1gInSc7	kov
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
bronz	bronz	k1gInSc4	bronz
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
MS	MS	kA	MS
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Porta	porto	k1gNnSc2	porto
byl	být	k5eAaImAgMnS	být
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
zástupce	zástupce	k1gMnSc1	zástupce
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
ragbyové	ragbyový	k2eAgFnSc2d1	ragbyová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
<g/>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
argentinským	argentinský	k2eAgMnSc7d1	argentinský
pilotem	pilot	k1gMnSc7	pilot
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
Juan	Juan	k1gMnSc1	Juan
Manuel	Manuel	k1gMnSc1	Manuel
Fangio	Fangio	k1gMnSc1	Fangio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
57	[number]	k4	57
pětkrát	pětkrát	k6eAd1	pětkrát
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
překonáno	překonat	k5eAaPmNgNnS	překonat
až	až	k9	až
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Argentinci	Argentinec	k1gMnPc1	Argentinec
získali	získat	k5eAaPmAgMnP	získat
dosud	dosud	k6eAd1	dosud
tři	tři	k4xCgFnPc4	tři
Nobelovy	Nobelův	k2eAgFnPc4d1	Nobelova
ceny	cena	k1gFnPc4	cena
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Bernardo	Bernardo	k1gNnSc1	Bernardo
Houssay	Houssaum	k1gNnPc7	Houssaum
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgInS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
za	za	k7c4	za
objevy	objev	k1gInPc4	objev
mechanismů	mechanismus	k1gInPc2	mechanismus
hormonální	hormonální	k2eAgFnSc2d1	hormonální
regulace	regulace	k1gFnSc2	regulace
hladiny	hladina	k1gFnSc2	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Argentinský	argentinský	k2eAgMnSc1d1	argentinský
biochemik	biochemik	k1gMnSc1	biochemik
Luis	Luisa	k1gFnPc2	Luisa
Federico	Federico	k1gMnSc1	Federico
Leloir	Leloir	k1gMnSc1	Leloir
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
objev	objev	k1gInSc4	objev
aktivačních	aktivační	k2eAgInPc2d1	aktivační
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
biosyntéze	biosyntéza	k1gFnSc6	biosyntéza
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
oborový	oborový	k2eAgMnSc1d1	oborový
kolega	kolega	k1gMnSc1	kolega
César	César	k1gMnSc1	César
Milstein	Milstein	k1gMnSc1	Milstein
je	být	k5eAaImIp3nS	být
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
monoklonálních	monoklonální	k2eAgFnPc2d1	monoklonální
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Florentino	Florentin	k2eAgNnSc4d1	Florentino
Ameghino	Ameghino	k1gNnSc4	Ameghino
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgMnSc7d1	významný
paleontologem	paleontolog	k1gMnSc7	paleontolog
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
okolo	okolo	k7c2	okolo
šesti	šest	k4xCc2	šest
tisíc	tisíc	k4xCgInPc2	tisíc
prehistorických	prehistorický	k2eAgMnPc2d1	prehistorický
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
argentinský	argentinský	k2eAgMnSc1d1	argentinský
paleontolog	paleontolog	k1gMnSc1	paleontolog
José	José	k1gNnSc2	José
F.	F.	kA	F.
Bonaparte	bonapart	k1gInSc5	bonapart
objevil	objevit	k5eAaPmAgInS	objevit
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
desítek	desítka	k1gFnPc2	desítka
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
herpetologie	herpetologie	k1gFnSc2	herpetologie
a	a	k8xC	a
entomologie	entomologie	k1gFnSc2	entomologie
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
Hermann	Hermann	k1gMnSc1	Hermann
Burmeister	Burmeister	k1gMnSc1	Burmeister
<g/>
.	.	kIx.	.
</s>
<s>
Teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
Juan	Juan	k1gMnSc1	Juan
Maldacena	Maldacen	k2eAgFnSc1d1	Maldacena
popsal	popsat	k5eAaPmAgMnS	popsat
tzv.	tzv.	kA	tzv.
AdS	AdS	k1gMnSc1	AdS
<g/>
/	/	kIx~	/
<g/>
CFT	CFT	kA	CFT
korespondenci	korespondence	k1gFnSc6	korespondence
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
René	René	k1gFnSc5	René
Favaloro	Favalora	k1gFnSc5	Favalora
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
aortokoronární	aortokoronární	k2eAgInSc4d1	aortokoronární
bypass	bypass	k1gInSc4	bypass
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
provedl	provést	k5eAaPmAgMnS	provést
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
voperování	voperování	k1gNnSc4	voperování
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
představitelům	představitel	k1gMnPc3	představitel
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
patří	patřit	k5eAaImIp3nS	patřit
filozof	filozof	k1gMnSc1	filozof
vědy	věda	k1gFnSc2	věda
Mario	Mario	k1gMnSc1	Mario
Bunge	Bunge	k1gFnSc1	Bunge
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Argentina	Argentina	k1gFnSc1	Argentina
postavila	postavit	k5eAaPmAgFnS	postavit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
země	země	k1gFnSc1	země
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
v	v	k7c6	v
latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
otevřela	otevřít	k5eAaPmAgFnS	otevřít
jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
:	:	kIx,	:
Atucha	Atucha	k1gFnSc1	Atucha
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
Embalse	Embalse	k1gFnSc1	Embalse
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
kosmický	kosmický	k2eAgInSc4d1	kosmický
program	program	k1gInSc4	program
a	a	k8xC	a
vyslala	vyslat	k5eAaPmAgFnS	vyslat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
již	již	k6eAd1	již
několik	několik	k4yIc1	několik
satelitů	satelit	k1gMnPc2	satelit
<g/>
:	:	kIx,	:
LUSAT-1	LUSAT-1	k1gMnSc1	LUSAT-1
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Víctor-	Víctor-	k1gFnSc1	Víctor-
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
či	či	k8xC	či
PEHUENSAT-1	PEHUENSAT-1	k1gMnSc1	PEHUENSAT-1
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
Argentina	Argentina	k1gFnSc1	Argentina
43	[number]	k4	43
veřejných	veřejný	k2eAgFnPc2d1	veřejná
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
QS	QS	kA	QS
World	World	k1gMnSc1	World
University	universita	k1gFnSc2	universita
Rankings	Rankings	k1gInSc1	Rankings
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
85	[number]	k4	85
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgFnSc1d1	národní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Córdobě	Córdoba	k1gFnSc6	Córdoba
(	(	kIx(	(
<g/>
Universidad	Universidad	k1gInSc1	Universidad
Nacional	Nacional	k1gFnSc2	Nacional
de	de	k?	de
Córdoba	Córdoba	k1gFnSc1	Córdoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
La	la	k1gNnSc6	la
Plata	plato	k1gNnSc2	plato
(	(	kIx(	(
<g/>
Universidad	Universidad	k1gInSc1	Universidad
Nacional	Nacional	k1gFnSc2	Nacional
de	de	k?	de
La	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
paleontologickou	paleontologický	k2eAgFnSc4d1	paleontologická
sbírku	sbírka	k1gFnSc4	sbírka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CHALUPA	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Argentinské	argentinský	k2eAgNnSc1d1	argentinské
tango	tango	k1gNnSc1	tango
</s>
</p>
<p>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Hispanoamerika	Hispanoamerika	k1gFnSc1	Hispanoamerika
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
krajanské	krajanský	k2eAgInPc1d1	krajanský
spolky	spolek	k1gInPc1	spolek
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Argentina	Argentina	k1gFnSc1	Argentina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Argentina	Argentina	k1gFnSc1	Argentina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Argentina	Argentina	k1gFnSc1	Argentina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Argentina	Argentina	k1gFnSc1	Argentina
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
portál	portál	k1gInSc1	portál
Argentiny	Argentina	k1gFnSc2	Argentina
</s>
</p>
<p>
<s>
Webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
presidentky	presidentka	k1gFnSc2	presidentka
</s>
</p>
<p>
<s>
Webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Economic	Economic	k1gMnSc1	Economic
Collapse	Collapse	k1gFnSc1	Collapse
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
kolapsu	kolaps	k1gInSc6	kolaps
Argentiny	Argentina	k1gFnSc2	Argentina
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Foreign	Foreign	k1gMnSc1	Foreign
&	&	k?	&
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
Office	Office	kA	Office
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2011-08-01	[number]	k4	2011-08-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002-05-16	[number]	k4	2002-05-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Argentina	Argentina	k1gFnSc1	Argentina
Country	country	k2eAgFnSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Western	western	k1gInSc1	western
Hemisphere	Hemispher	k1gInSc5	Hemispher
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Argentina	Argentina	k1gFnSc1	Argentina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-22	[number]	k4	2011-07-22
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Argentina	Argentina	k1gFnSc1	Argentina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Refworld	Refworld	k1gMnSc1	Refworld
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Leader	leader	k1gMnSc1	leader
in	in	k?	in
Refugee	Refugee	k1gInSc1	Refugee
Decision	Decision	k1gInSc1	Decision
Support	support	k1gInSc1	support
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
High	High	k1gMnSc1	High
Commissioner	Commissioner	k1gMnSc1	Commissioner
for	forum	k1gNnPc2	forum
Refugees	Refugees	k1gInSc1	Refugees
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Argentina	Argentina	k1gFnSc1	Argentina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-05-02	[number]	k4	2011-05-02
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CALVERT	CALVERT	kA	CALVERT
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
A.	A.	kA	A.
R	R	kA	R
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Turkey	Turkea	k1gFnPc1	Turkea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
