<s>
deformace	deformace	k1gFnSc2	deformace
povrchu	povrch	k1gInSc2	povrch
oceánu	oceán	k1gInSc2	oceán
vlivem	vlivem	k7c2	vlivem
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
na	na	k7c4	na
vodní	vodní	k2eAgFnSc4d1	vodní
masu	masa	k1gFnSc4	masa
působí	působit	k5eAaImIp3nP	působit
nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
především	především	k9	především
Měsíc	měsíc	k1gInSc1	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
</s>
