<s>
Gelendžik	Gelendžik	k1gInSc1
(	(	kIx(
<g/>
liniový	liniový	k2eAgInSc1d1
maják	maják	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Liniový	liniový	k2eAgInSc1d1
maják	maják	k1gInSc1
Gelendžik	Gelendžik	k1gInSc1
Maják	maják	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
Lokace	lokace	k1gFnSc1
</s>
<s>
Černé	Černé	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
44	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
38	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Konstrukce	konstrukce	k1gFnSc2
</s>
<s>
cihla	cihla	k1gFnSc1
První	první	k4xOgFnSc1
stavba	stavba	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1897	#num#	k4
Označení	označení	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Konfigurace	konfigurace	k1gFnSc1
</s>
<s>
Admiralty	Admiralt	k1gInPc1
N5658	N5658	k1gFnSc2
Výška	výška	k1gFnSc1
</s>
<s>
13	#num#	k4
m	m	kA
Dosah	dosah	k1gInSc1
</s>
<s>
9	#num#	k4
nm	nm	k?
(	(	kIx(
<g/>
16,67	16,67	k4
km	km	kA
<g/>
)	)	kIx)
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Červené	Červené	k2eAgNnSc1d1
a	a	k8xC
zelené	zelený	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
</s>
<s>
Liniový	liniový	k2eAgInSc1d1
maják	maják	k1gInSc1
Gelendžik	Gelendžika	k1gFnPc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
С	С	k?
м	м	k?
Г	Г	k?
<g/>
)	)	kIx)
stojí	stát	k5eAaImIp3nS
na	na	k7c6
břehu	břeh	k1gInSc6
v	v	k7c6
Gelendžijském	Gelendžijský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
Krasnojarský	Krasnojarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
bulváru	bulvár	k1gInSc6
Lermontova	Lermontův	k2eAgFnSc1d1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Lomonosova	Lomonosův	k2eAgNnSc2d1
sanatoria	sanatorium	k1gNnSc2
v	v	k7c6
lázeňském	lázeňský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Gelendžik	Gelendžika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3
dochovaným	dochovaný	k2eAgInSc7d1
majákem	maják	k1gInSc7
v	v	k7c6
městě	město	k1gNnSc6
Gelendžik	Gelendžik	k1gInSc1
je	být	k5eAaImIp3nS
maják	maják	k1gInSc1
jehož	jehož	k3xOyRp3gFnSc1
stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájená	zahájený	k2eAgFnSc1d1
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1897	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektem	architekt	k1gMnSc7
byl	být	k5eAaImAgMnS
Francouz	Francouz	k1gMnSc1
François	François	k1gFnSc2
de	de	k?
Tonde	Tond	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maják	maják	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
při	při	k7c6
ústupu	ústup	k1gInSc6
sovětských	sovětský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
maják	maják	k1gInSc1
zničen	zničit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náčelník	náčelník	k1gInSc1
majáku	maják	k1gInSc2
P.	P.	kA
T.	T.	kA
Sokolov	Sokolov	k1gInSc1
úkol	úkol	k1gInSc1
nesplnil	splnit	k5eNaPmAgInS
a	a	k8xC
maják	maják	k1gInSc1
zachránil	zachránit	k5eAaPmAgInS
pro	pro	k7c4
budoucí	budoucí	k2eAgFnPc4d1
generace	generace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Zděná	zděný	k2eAgFnSc1d1
hranolová	hranolový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
na	na	k7c6
půdorysu	půdorys	k1gInSc6
čtverce	čtverec	k1gInSc2
vysoká	vysoká	k1gFnSc1
13	#num#	k4
m	m	kA
je	být	k5eAaImIp3nS
přistavěna	přistavět	k5eAaPmNgFnS
k	k	k7c3
dvojpodlažní	dvojpodlažní	k2eAgFnSc3d1
budově	budova	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maják	maják	k1gInSc1
je	být	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
se	s	k7c7
svislým	svislý	k2eAgInSc7d1
vodorovným	vodorovný	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Světelný	světelný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
malém	malý	k2eAgInSc6d1
arkýři	arkýř	k1gInSc6
v	v	k7c6
druhém	druhý	k4xOgNnSc6
patře	patro	k1gNnSc6
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
byla	být	k5eAaImAgFnS
naftová	naftový	k2eAgFnSc1d1
lampa	lampa	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
lampou	lampa	k1gFnSc7
elektrickou	elektrický	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zajišťuje	zajišťovat	k5eAaImIp3nS
navigaci	navigace	k1gFnSc4
lodí	loď	k1gFnPc2
do	do	k7c2
přístavu	přístav	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Červený	červený	k2eAgInSc1d1
pruh	pruh	k1gInSc1
na	na	k7c6
majáku	maják	k1gInSc6
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
určení	určení	k1gNnSc3
vzdálenosti	vzdálenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Data	datum	k1gNnPc1
</s>
<s>
výška	výška	k1gFnSc1
věže	věž	k1gFnSc2
13	#num#	k4
m	m	kA
</s>
<s>
výška	výška	k1gFnSc1
světla	světlo	k1gNnSc2
17	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
sektorové	sektorový	k2eAgFnPc1d1
stálé	stálý	k2eAgFnPc1d1
červené	červená	k1gFnPc1
nebo	nebo	k8xC
zelené	zelený	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
směru	směr	k1gInSc6
plavby	plavba	k1gFnSc2
do	do	k7c2
přístavu	přístav	k1gInSc2
(	(	kIx(
<g/>
charakteristika	charakteristika	k1gFnSc1
<g/>
:	:	kIx,
F	F	kA
RG	RG	kA
<g/>
)	)	kIx)
</s>
<s>
dosvit	dosvit	k1gInSc1
16,67	16,67	k4
km	km	kA
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
označení	označení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Admiralty	Admiralt	k1gInPc1
N5658	N5658	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
С	С	k?
м	м	k?
Г	Г	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
М	М	k?
<g/>
.	.	kIx.
shturman-tof	shturman-tof	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Г	Г	k?
с	с	k?
м	м	k?
<g/>
.	.	kIx.
kamaran	kamaran	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
И	И	k?
<g/>
:	:	kIx,
<g/>
Gelendgik	Gelendgik	k1gMnSc1
mayak	mayak	k1gMnSc1
stvornyi	stvorny	k1gFnSc2
<g/>
.	.	kIx.
<g/>
JPG	JPG	kA
—	—	k?
Э	Э	k?
«	«	k?
<g/>
В	В	k?
с	с	k?
<g/>
»	»	k?
<g/>
.	.	kIx.
www.vokrugsveta.ru	www.vokrugsveta.r	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ROWLETT	ROWLETT	kA
<g/>
,	,	kIx,
Russ	Russ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lighthouses	Lighthouses	k1gMnSc1
of	of	k?
Russia	Russia	k1gFnSc1
<g/>
:	:	kIx,
Eastern	Eastern	k1gMnSc1
Black	Black	k1gMnSc1
Sea	Sea	k1gMnSc1
<g/>
.	.	kIx.
www.unc.edu	www.unc.edu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
М	М	k?
Г	Г	k?
|	|	kIx~
Г	Г	k?
<g/>
:	:	kIx,
о	о	k?
К	К	k?
д	д	k?
Т	Т	k?
<g/>
.	.	kIx.
yug-gelendzhik	yug-gelendzhik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Black	Black	k1gMnSc1
sea	sea	k?
/	/	kIx~
Gelendzhik	Gelendzhik	k1gInSc1
front	fronta	k1gFnPc2
range	range	k6eAd1
light	light	k5eAaPmF
(	(	kIx(
<g/>
mitko	mitko	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gMnSc1
of	of	k?
Lighthouses	Lighthouses	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Historická	historický	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
