<s>
Brdce	brdce	k1gNnSc1
</s>
<s>
Brdce	brdce	k1gNnSc1
Vrchol	vrchol	k1gInSc1
</s>
<s>
840	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
23	#num#	k4
m	m	kA
Izolace	izolace	k1gFnSc1
</s>
<s>
1	#num#	k4
km	km	kA
→	→	k?
Hradiště	Hradiště	k1gNnSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Brdské	brdský	k2eAgFnPc1d1
osmistovky	osmistovka	k1gFnPc1
#	#	kIx~
<g/>
5	#num#	k4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Brdská	brdský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Brdy	Brdy	k1gInPc1
/	/	kIx~
Třemošenská	Třemošenský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Tocká	Tocký	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Brdce	brdce	k1gNnSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
slepenec	slepenec	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Litavka	Litavka	k1gFnSc1
<g/>
,	,	kIx,
Berounka	Berounka	k1gFnSc1
<g/>
,	,	kIx,
Vltava	Vltava	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
lokalitě	lokalita	k1gFnSc6
Na	na	k7c6
Brdcích	brdek	k1gInPc6
města	město	k1gNnSc2
Jaroměř	Jaroměř	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Náchod	Náchod	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Brdce	brdce	k1gNnSc2
(	(	kIx(
<g/>
Jaroměř	Jaroměř	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Brdce	brdce	k1gNnSc1
(	(	kIx(
<g/>
840	#num#	k4
m	m	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtvrtá	čtvrtý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Brdské	brdský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
tzv.	tzv.	kA
Středních	střední	k2eAgInPc6d1
Brdech	Brdy	k1gInPc6
<g/>
,	,	kIx,
cca	cca	kA
4	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Láz	Láz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
ležela	ležet	k5eAaImAgFnS
ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
újezdu	újezd	k1gInSc6
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Brdy	Brdy	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Relativně	relativně	k6eAd1
mírným	mírný	k2eAgInSc7d1
svahem	svah	k1gInSc7
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
východně	východně	k6eAd1
položené	položený	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
pramení	pramenit	k5eAaImIp3nS
Litavka	Litavka	k1gFnSc1
a	a	k8xC
leží	ležet	k5eAaImIp3nS
Lázská	Lázský	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
severu	sever	k1gInSc3
se	se	k3xPyFc4
sklání	sklánět	k5eAaImIp3nS
do	do	k7c2
bezejmenného	bezejmenný	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
(	(	kIx(
<g/>
cca	cca	kA
790	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
místními	místní	k2eAgInPc7d1
někdy	někdy	k6eAd1
nazývané	nazývaný	k2eAgNnSc4d1
Borské	Borské	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
(	(	kIx(
<g/>
podle	podle	k7c2
myslivny	myslivna	k1gFnSc2
Bor	bor	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dříve	dříve	k6eAd2
stávala	stávat	k5eAaImAgFnS
těsně	těsně	k6eAd1
pod	pod	k7c7
sedlem	sedlo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
odděluje	oddělovat	k5eAaImIp3nS
toto	tento	k3xDgNnSc4
území	území	k1gNnSc4
od	od	k7c2
oblasti	oblast	k1gFnSc2
Toku	tok	k1gInSc2
(	(	kIx(
<g/>
865	#num#	k4
m	m	kA
<g/>
)	)	kIx)
s	s	k7c7
Korunou	koruna	k1gFnSc7
(	(	kIx(
<g/>
837	#num#	k4
m	m	kA
<g/>
)	)	kIx)
dále	daleko	k6eAd2
na	na	k7c6
severu	sever	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedlem	sedlo	k1gNnSc7
prochází	procházet	k5eAaImIp3nS
asfaltová	asfaltový	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
od	od	k7c2
Bohutína	Bohutín	k1gInSc2
a	a	k8xC
Kozičína	Kozičín	k1gInSc2
na	na	k7c6
východě	východ	k1gInSc6
ke	k	k7c3
Třem	tři	k4xCgFnPc3
Trubkám	trubka	k1gFnPc3
a	a	k8xC
Strašicím	Strašice	k1gFnPc3
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
je	být	k5eAaImIp3nS
veřejnosti	veřejnost	k1gFnSc3
oficiálně	oficiálně	k6eAd1
nepřístupná	přístupný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mnohem	mnohem	k6eAd1
prudšími	prudký	k2eAgInPc7d2
svahy	svah	k1gInPc7
se	se	k3xPyFc4
sklání	sklánět	k5eAaImIp3nS
Brdce	brdce	k1gNnSc1
na	na	k7c4
západ	západ	k1gInSc4
do	do	k7c2
Třitrubeckého	Třitrubecký	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
k	k	k7c3
jihu	jih	k1gInSc3
pokračuje	pokračovat	k5eAaImIp3nS
hřeben	hřeben	k1gInSc1
jen	jen	k9
s	s	k7c7
malými	malý	k2eAgInPc7d1
výškovými	výškový	k2eAgInPc7d1
rozdíly	rozdíl	k1gInPc7
k	k	k7c3
vrchu	vrch	k1gInSc2
Hradiště	Hradiště	k1gNnSc2
(	(	kIx(
<g/>
839	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Brdce	brdce	k1gNnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
vzdálenosti	vzdálenost	k1gFnSc2
poměrně	poměrně	k6eAd1
výrazným	výrazný	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
samotný	samotný	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
plošina	plošina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
níž	nízce	k6eAd2
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
vyznačuje	vyznačovat	k5eAaImIp3nS
uměle	uměle	k6eAd1
navršená	navršený	k2eAgFnSc1d1
hromada	hromada	k1gFnSc1
kamenů	kámen	k1gInPc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
zdůrazněná	zdůrazněný	k2eAgFnSc1d1
i	i	k8xC
zástavou	zástava	k1gFnSc7
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vrcholu	vrchol	k1gInSc2
je	být	k5eAaImIp3nS
dobrý	dobrý	k2eAgInSc4d1
výhled	výhled	k1gInSc4
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
oblasti	oblast	k1gFnSc2
Příbramska	Příbramsko	k1gNnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
vrchy	vrch	k1gInPc4
obklopující	obklopující	k2eAgNnSc4d1
koryto	koryto	k1gNnSc4
Vltavy	Vltava	k1gFnSc2
a	a	k8xC
Středočeskou	středočeský	k2eAgFnSc4d1
pahorkatinu	pahorkatina	k1gFnSc4
až	až	k9
k	k	k7c3
Českomoravské	českomoravský	k2eAgFnSc3d1
vrchovině	vrchovina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrch	vrch	k1gInSc1
Brdce	brdce	k1gNnSc2
byl	být	k5eAaImAgInS
za	za	k7c2
existence	existence	k1gFnSc2
Vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc1
veřejnosti	veřejnost	k1gFnSc3
oficiálně	oficiálně	k6eAd1
nepřístupný	přístupný	k2eNgInSc1d1
<g/>
,	,	kIx,
po	po	k7c6
vzniku	vznik	k1gInSc6
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Brdy	Brdy	k1gInPc1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
je	být	k5eAaImIp3nS
přístupný	přístupný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Nejvhodnějším	vhodný	k2eAgNnSc7d3
východištěm	východiště	k1gNnSc7
jsou	být	k5eAaImIp3nP
obce	obec	k1gFnPc1
Láz	Láz	k?
a	a	k8xC
Bohutín	Bohutín	k1gInSc1
i	i	k8xC
vzdálenější	vzdálený	k2eAgFnSc1d2
Příbram	Příbram	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Vrch	vrch	k1gInSc4
Brdce	brdce	k1gNnSc2
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
„	„	k?
<g/>
horkých	horký	k2eAgInPc2d1
<g/>
“	“	k?
kandidátů	kandidát	k1gMnPc2
na	na	k7c6
umístění	umístění	k1gNnSc6
americké	americký	k2eAgFnSc2d1
radarové	radarový	k2eAgFnSc2d1
základny	základna	k1gFnSc2
PRO	pro	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
dostal	dostat	k5eAaPmAgInS
přednost	přednost	k1gFnSc4
bezejmenný	bezejmenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
Kóta	kóta	k1gFnSc1
718,8	718,8	k4
m	m	kA
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
obcí	obec	k1gFnPc2
Teslíny	Teslína	k1gFnSc2
<g/>
,	,	kIx,
Míšov	Míšov	k1gInSc4
a	a	k8xC
Trokavec	Trokavec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
východním	východní	k2eAgInSc6d1
svahu	svah	k1gInSc6
vrchu	vrch	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
nadm	nadm	k1gMnSc1
<g/>
.	.	kIx.
výšce	výška	k1gFnSc6
cca	cca	kA
715	#num#	k4
m	m	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Skelná	skelný	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zde	zde	k6eAd1
pracovala	pracovat	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1749	#num#	k4
<g/>
–	–	k?
<g/>
1783	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
provozovatelem	provozovatel	k1gMnSc7
byl	být	k5eAaImAgMnS
Tobiáš	Tobiáš	k1gMnSc1
Matěj	Matěj	k1gMnSc1
Adler	Adler	k1gMnSc1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
známý	známý	k2eAgMnSc1d1
hlavně	hlavně	k9
ze	z	k7c2
Šumavy	Šumava	k1gFnSc2
(	(	kIx(
<g/>
i	i	k9
z	z	k7c2
díla	dílo	k1gNnSc2
spisovatele	spisovatel	k1gMnSc2
Karla	Karel	k1gMnSc2
Klostermanna	Klostermann	k1gMnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stávala	stávat	k5eAaImAgFnS
zde	zde	k6eAd1
až	až	k6eAd1
do	do	k7c2
vzniku	vznik	k1gInSc2
brdské	brdský	k2eAgFnSc2d1
střelnice	střelnice	k1gFnSc2
také	také	k6eAd1
myslivna	myslivna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
jen	jen	k9
seník	seník	k1gInSc4
a	a	k8xC
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
louky	louka	k1gFnPc4
<g/>
,	,	kIx,
klesající	klesající	k2eAgNnSc4d1
k	k	k7c3
Lázské	Lázská	k1gFnSc3
nádrži	nádrž	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
blízkém	blízký	k2eAgInSc6d1
vrchu	vrch	k1gInSc6
Závirka	Závirk	k1gInSc2
(	(	kIx(
<g/>
720	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oddělující	oddělující	k2eAgNnPc1d1
údolí	údolí	k1gNnPc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
pramení	pramenit	k5eAaImIp3nS
Litavka	Litavka	k1gFnSc1
a	a	k8xC
údolí	údolí	k1gNnSc1
Pilského	Pilský	k2eAgInSc2d1
potoka	potok	k1gInSc2
(	(	kIx(
<g/>
s	s	k7c7
Pilskou	Pilský	k2eAgFnSc7d1
nádrží	nádrž	k1gFnSc7
<g/>
)	)	kIx)
se	se	k3xPyFc4
snad	snad	k9
nacházelo	nacházet	k5eAaImAgNnS
prehistorické	prehistorický	k2eAgNnSc1d1
hradiště	hradiště	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
původ	původ	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
nejasný	jasný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
odtud	odtud	k6eAd1
<g/>
,	,	kIx,
u	u	k7c2
lesního	lesní	k2eAgNnSc2d1
jezírka	jezírko	k1gNnSc2
<g/>
,	,	kIx,
snad	snad	k9
také	také	k9
žil	žít	k5eAaImAgMnS
poustevník	poustevník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Brdce	brdce	k1gNnSc1
v	v	k7c6
zimě	zima	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Brdce	brdce	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Další	další	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
<g/>
:	:	kIx,
,	,	kIx,
,	,	kIx,
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Brdské	brdský	k2eAgFnSc2d1
osmistovky	osmistovka	k1gFnSc2
</s>
<s>
Tok	tok	k1gInSc1
(	(	kIx(
<g/>
865	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
862	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Malý	malý	k2eAgInSc1d1
Tok	tok	k1gInSc1
(	(	kIx(
<g/>
844	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Brdce	brdce	k1gNnSc4
(	(	kIx(
<g/>
840	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
839	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
837	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Třemšín	Třemšín	k1gInSc1
(	(	kIx(
<g/>
827	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Jordán	Jordán	k1gInSc1
(	(	kIx(
<g/>
826	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Paterák	Paterák	k1gMnSc1
(	(	kIx(
<g/>
814	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nad	nad	k7c7
Marastkem	Marastek	k1gInSc7
(	(	kIx(
<g/>
805	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
