<s>
Pixar	Pixar	k1gInSc1	Pixar
Animation	Animation	k1gInSc1	Animation
Studios	Studios	k?	Studios
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
počítačovou	počítačový	k2eAgFnSc7d1	počítačová
animací	animace	k1gFnSc7	animace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
Pixar	Pixar	k1gInSc4	Pixar
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
The	The	k1gMnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
cen	cena	k1gFnPc2	cena
získalo	získat	k5eAaPmAgNnS	získat
studio	studio	k1gNnSc4	studio
dvacet	dvacet	k4xCc1	dvacet
čtyři	čtyři	k4xCgInPc4	čtyři
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
výdělek	výdělek	k1gInSc1	výdělek
činí	činit	k5eAaImIp3nS	činit
celosvětově	celosvětově	k6eAd1	celosvětově
5.5	[number]	k4	5.5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
ho	on	k3xPp3gMnSc4	on
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
filmových	filmový	k2eAgNnPc2d1	filmové
studií	studio	k1gNnPc2	studio
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
svým	svůj	k3xOyFgInSc7	svůj
softwarem	software	k1gInSc7	software
RenderMan	RenderMan	k1gMnSc1	RenderMan
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
renderování	renderování	k1gNnSc3	renderování
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Pixar	Pixar	k1gMnSc1	Pixar
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
jako	jako	k8xS	jako
Graphics	Graphics	k1gInSc1	Graphics
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
počítačové	počítačový	k2eAgFnSc2d1	počítačová
sekce	sekce	k1gFnSc2	sekce
Lucasfilmu	Lucasfilm	k1gInSc2	Lucasfilm
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
Applu	Appl	k1gInSc2	Appl
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Pixar	Pixara	k1gFnPc2	Pixara
<g/>
.	.	kIx.	.
</s>
<s>
Pixar	Pixar	k1gMnSc1	Pixar
zatím	zatím	k6eAd1	zatím
vydal	vydat	k5eAaPmAgMnS	vydat
patnáct	patnáct	k4xCc4	patnáct
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
s	s	k7c7	s
Příběhem	příběh	k1gInSc7	příběh
hraček	hračka	k1gFnPc2	hračka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
i	i	k8xC	i
kritiků	kritik	k1gMnPc2	kritik
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
Život	život	k1gInSc1	život
brouka	brouk	k1gMnSc2	brouk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Příběh	příběh	k1gInSc4	příběh
hraček	hračka	k1gFnPc2	hračka
2	[number]	k4	2
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Příšerky	příšerka	k1gFnPc1	příšerka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k6eAd1	Nemo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Úžasňákovi	Úžasňákův	k2eAgMnPc1d1	Úžasňákův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Auta	auto	k1gNnPc1	auto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Ratatouille	Ratatouille	k1gFnSc1	Ratatouille
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
VALL-I	VALL-I	k1gFnSc1	VALL-I
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
film	film	k1gInSc1	film
od	od	k7c2	od
Pixaru	Pixar	k1gInSc2	Pixar
prezentovaný	prezentovaný	k2eAgInSc4d1	prezentovaný
ve	v	k7c4	v
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
a	a	k8xC	a
Příběh	příběh	k1gInSc4	příběh
hraček	hračka	k1gFnPc2	hračka
3	[number]	k4	3
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
nejvýdělečnější	výdělečný	k2eAgInSc1d3	nejvýdělečnější
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
vydělal	vydělat	k5eAaPmAgInS	vydělat
celosvětově	celosvětově	k6eAd1	celosvětově
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
film	film	k1gInSc4	film
Pixaru	Pixar	k1gInSc2	Pixar
jsou	být	k5eAaImIp3nP	být
Auta	auto	k1gNnPc1	auto
2	[number]	k4	2
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
rok	rok	k1gInSc1	rok
poté	poté	k6eAd1	poté
následovaný	následovaný	k2eAgInSc1d1	následovaný
filmem	film	k1gInSc7	film
Rebelka	rebelka	k1gFnSc1	rebelka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejdražším	drahý	k2eAgInSc7d3	nejdražší
počinem	počin	k1gInSc7	počin
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
pokračování	pokračování	k1gNnSc4	pokračování
filmu	film	k1gInSc2	film
o	o	k7c6	o
příšerkách	příšerka	k1gFnPc6	příšerka
-	-	kIx~	-
Univerzita	univerzita	k1gFnSc1	univerzita
pro	pro	k7c4	pro
příšerky	příšerka	k1gFnPc4	příšerka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Předposledním	předposlední	k2eAgInSc7d1	předposlední
filmem	film	k1gInSc7	film
Pixaru	Pixar	k1gInSc2	Pixar
je	být	k5eAaImIp3nS	být
V	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
je	být	k5eAaImIp3nS	být
Auta	auto	k1gNnSc2	auto
3	[number]	k4	3
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Akademie	akademie	k1gFnSc1	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
uděluje	udělovat	k5eAaImIp3nS	udělovat
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
filmy	film	k1gInPc4	film
společnosti	společnost	k1gFnSc2	společnost
Pixar	Pixara	k1gFnPc2	Pixara
vždy	vždy	k6eAd1	vždy
mezi	mezi	k7c7	mezi
nominovanými	nominovaný	k2eAgMnPc7d1	nominovaný
<g/>
.	.	kIx.	.
6	[number]	k4	6
z	z	k7c2	z
8	[number]	k4	8
dokonce	dokonce	k9	dokonce
cenu	cena	k1gFnSc4	cena
i	i	k9	i
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
-	-	kIx~	-
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k6eAd1	Nemo
<g/>
,	,	kIx,	,
Úžasňákovi	Úžasňák	k1gMnSc6	Úžasňák
<g/>
,	,	kIx,	,
Ratatouille	Ratatouilla	k1gFnSc6	Ratatouilla
<g/>
,	,	kIx,	,
WALL-E	WALL-E	k1gFnSc6	WALL-E
<g/>
,	,	kIx,	,
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
<g/>
,	,	kIx,	,
Rebelka	rebelka	k1gFnSc1	rebelka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
byl	být	k5eAaImAgMnS	být
film	film	k1gInSc4	film
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
i	i	k9	i
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
filmy	film	k1gInPc7	film
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k6eAd1	Nemo
<g/>
,	,	kIx,	,
Úžasňákovi	Úžasňákův	k2eAgMnPc1d1	Úžasňákův
<g/>
,	,	kIx,	,
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
a	a	k8xC	a
Příběh	příběh	k1gInSc4	příběh
hraček	hračka	k1gFnPc2	hračka
3	[number]	k4	3
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
nejvýdělečnějších	výdělečný	k2eAgInPc2d3	nejvýdělečnější
filmů	film	k1gInPc2	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
vedení	vedení	k1gNnSc4	vedení
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Lasseter	Lassetra	k1gFnPc2	Lassetra
<g/>
,	,	kIx,	,
Brad	brada	k1gFnPc2	brada
Bird	Birda	k1gFnPc2	Birda
<g/>
,	,	kIx,	,
Pete	Pete	k1gFnSc1	Pete
Docter	Docter	k1gMnSc1	Docter
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Stanton	Stanton	k1gInSc1	Stanton
a	a	k8xC	a
Lee	Lea	k1gFnSc3	Lea
Unkrich	Unkricha	k1gFnPc2	Unkricha
<g/>
)	)	kIx)	)
udělen	udělen	k2eAgInSc1d1	udělen
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
Zlatý	zlatý	k2eAgInSc4d1	zlatý
lev	lev	k1gInSc4	lev
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
jim	on	k3xPp3gMnPc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
George	George	k1gFnSc4	George
Lucas	Lucas	k1gMnSc1	Lucas
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Lucasfilm	Lucasfilm	k1gMnSc1	Lucasfilm
<g/>
.	.	kIx.	.
</s>
<s>
Pixar	Pixar	k1gInSc1	Pixar
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k9	jako
Graphics	Graphics	k1gInSc1	Graphics
Group	Group	k1gInSc1	Group
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
počítačové	počítačový	k2eAgFnSc2d1	počítačová
divize	divize	k1gFnSc2	divize
Lucasfilmu	Lucasfilm	k1gInSc2	Lucasfilm
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
pronájmu	pronájem	k1gInSc6	pronájem
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ed	Ed	k1gMnSc1	Ed
Catmull	Catmull	k1gMnSc1	Catmull
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
NYIT	NYIT	kA	NYIT
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
Computer	computer	k1gInSc1	computer
Graphics	Graphics	k1gInSc4	Graphics
Lab	Lab	k1gFnSc2	Lab
(	(	kIx(	(
<g/>
CGL	CGL	kA	CGL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
na	na	k7c4	na
NYIT	NYIT	kA	NYIT
propagovali	propagovat	k5eAaImAgMnP	propagovat
CG	cg	kA	cg
techniky	technika	k1gFnPc1	technika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
samozřejmost	samozřejmost	k1gFnSc4	samozřejmost
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
experimentální	experimentální	k2eAgInSc4d1	experimentální
film	film	k1gInSc4	film
nazvali	nazvat	k5eAaBmAgMnP	nazvat
The	The	k1gMnPc1	The
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
tým	tým	k1gInSc4	tým
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
předchůdci	předchůdce	k1gMnSc6	předchůdce
RenderMana	RenderMana	k1gFnSc1	RenderMana
<g/>
,	,	kIx,	,
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Motion	Motion	k1gInSc1	Motion
Doctor	Doctor	k1gInSc4	Doctor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vytváření	vytváření	k1gNnSc4	vytváření
tradiční	tradiční	k2eAgFnSc2d1	tradiční
cel	clo	k1gNnPc2	clo
animace	animace	k1gFnSc2	animace
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
filmových	filmový	k2eAgFnPc6d1	filmová
epizodách	epizoda	k1gFnPc6	epizoda
produkovaných	produkovaný	k2eAgFnPc6d1	produkovaná
Lucasfilmem	Lucasfilm	k1gInSc7	Lucasfilm
nebo	nebo	k8xC	nebo
pracovali	pracovat	k5eAaImAgMnP	pracovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Industrial	Industrial	k1gMnSc1	Industrial
Light	Light	k1gMnSc1	Light
&	&	k?	&
Magic	Magic	k1gMnSc1	Magic
na	na	k7c6	na
speciálních	speciální	k2eAgInPc6d1	speciální
efektech	efekt	k1gInPc6	efekt
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	let	k1gInPc6	let
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
několika	několik	k4yIc2	několik
zásadních	zásadní	k2eAgFnPc2d1	zásadní
milnících	milník	k1gInPc6	milník
jako	jako	k8xS	jako
např.	např.	kA	např.
efekt	efekt	k1gInSc1	efekt
Genesis	Genesis	k1gFnSc1	Genesis
ve	v	k7c4	v
film	film	k1gInSc4	film
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
koupil	koupit	k5eAaPmAgInS	koupit
skupinu	skupina	k1gFnSc4	skupina
čítající	čítající	k2eAgFnSc4d1	čítající
asi	asi	k9	asi
45	[number]	k4	45
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc1	Jobs
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
opustil	opustit	k5eAaPmAgMnS	opustit
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k1gInSc1	Jobs
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
Lucasovi	Lucas	k1gMnSc3	Lucas
pět	pět	k4xCc1	pět
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc4	pět
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
jako	jako	k9	jako
kapitál	kapitál	k1gInSc1	kapitál
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
řízena	řídit	k5eAaImNgFnS	řídit
prezidentem	prezident	k1gMnSc7	prezident
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edwinem	Edwin	k1gMnSc7	Edwin
Catmullem	Catmull	k1gMnSc7	Catmull
a	a	k8xC	a
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
a	a	k8xC	a
režisérem	režisér	k1gMnSc7	režisér
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Alvym	Alvym	k1gInSc1	Alvym
Rayem	Rayem	k1gInSc1	Rayem
Smithem	Smith	k1gInSc7	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k1gInSc1	Jobs
zastával	zastávat	k5eAaImAgInS	zastávat
funkci	funkce	k1gFnSc4	funkce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Pixaru	Pixar	k1gInSc2	Pixar
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
Pixar	Pixar	k1gInSc1	Pixar
společností	společnost	k1gFnPc2	společnost
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
vysoce	vysoce	k6eAd1	vysoce
výkonný	výkonný	k2eAgInSc1d1	výkonný
počítačový	počítačový	k2eAgInSc1d1	počítačový
hardware	hardware	k1gInSc1	hardware
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
klíčovým	klíčový	k2eAgInSc7d1	klíčový
produktem	produkt	k1gInSc7	produkt
byl	být	k5eAaImAgMnS	být
Pixar	Pixar	k1gMnSc1	Pixar
Image	image	k1gFnSc2	image
Computer	computer	k1gInSc1	computer
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
prodáván	prodávat	k5eAaImNgInS	prodávat
<g/>
,	,	kIx,	,
především	především	k9	především
vládním	vládní	k2eAgFnPc3d1	vládní
organizacím	organizace	k1gFnPc3	organizace
a	a	k8xC	a
lékařským	lékařský	k2eAgFnPc3d1	lékařská
komunitám	komunita	k1gFnPc3	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
takovým	takový	k3xDgInSc7	takový
kupcem	kupec	k1gMnSc7	kupec
byly	být	k5eAaImAgFnP	být
Disney	Disne	k2eAgInPc1d1	Disne
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ho	on	k3xPp3gMnSc4	on
používaly	používat	k5eAaImAgInP	používat
jako	jako	k8xS	jako
část	část	k1gFnSc1	část
jejich	jejich	k3xOp3gInSc2	jejich
tajného	tajný	k2eAgInSc2d1	tajný
projektu	projekt	k1gInSc2	projekt
CAPS	Caps	kA	Caps
<g/>
.	.	kIx.	.
</s>
<s>
Pixar	Pixar	k1gInSc1	Pixar
Image	image	k1gInSc1	image
Computer	computer	k1gInSc1	computer
se	se	k3xPyFc4	se
však	však	k9	však
neprodával	prodávat	k5eNaImAgMnS	prodávat
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lasseter	Lasseter	k1gMnSc1	Lasseter
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reklamy	reklama	k1gFnSc2	reklama
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
několik	několik	k4yIc4	několik
kratších	krátký	k2eAgFnPc2d2	kratší
animací	animace	k1gFnPc2	animace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
následně	následně	k6eAd1	následně
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c6	na
SIGGRAPH	SIGGRAPH	kA	SIGGRAPH
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
počítačů	počítač	k1gMnPc2	počítač
začal	začít	k5eAaPmAgInS	začít
Pixar	Pixar	k1gInSc1	Pixar
prodávat	prodávat	k5eAaImF	prodávat
společnostem	společnost	k1gFnPc3	společnost
Lasseterovy	Lasseterův	k2eAgFnPc4d1	Lasseterův
počítačem	počítač	k1gInSc7	počítač
oživené	oživený	k2eAgFnSc2d1	oživená
animované	animovaný	k2eAgFnSc2d1	animovaná
reklamy	reklama	k1gFnSc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
společnosti	společnost	k1gFnPc4	společnost
Listerine	Listerin	k1gInSc5	Listerin
<g/>
,	,	kIx,	,
Tropicana	Tropicana	k1gFnSc1	Tropicana
nebo	nebo	k8xC	nebo
LifeSavers	LifeSavers	k1gInSc1	LifeSavers
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
prodal	prodat	k5eAaPmAgInS	prodat
Jobs	Jobs	k1gInSc1	Jobs
hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
divizi	divize	k1gFnSc4	divize
Pixaru	Pixar	k1gInSc2	Pixar
společnosti	společnost	k1gFnSc2	společnost
Vicom	Vicom	k1gInSc1	Vicom
Systems	Systems	k1gInSc1	Systems
a	a	k8xC	a
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
ze	z	k7c2	z
San	San	k1gFnSc2	San
Rafael	Rafaela	k1gFnPc2	Rafaela
do	do	k7c2	do
Richmondu	Richmond	k1gInSc2	Richmond
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Pixar	Pixar	k1gMnSc1	Pixar
stále	stále	k6eAd1	stále
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Disney	Disne	k1gMnPc7	Disne
Feature	Featur	k1gMnSc5	Featur
Animation	Animation	k1gInSc4	Animation
a	a	k8xC	a
z	z	k7c2	z
korporačního	korporační	k2eAgMnSc2d1	korporační
společníka	společník	k1gMnSc2	společník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těžkých	těžký	k2eAgInPc6d1	těžký
začátcích	začátek	k1gInPc6	začátek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
muselo	muset	k5eAaImAgNnS	muset
odejít	odejít	k5eAaPmF	odejít
asi	asi	k9	asi
třicet	třicet	k4xCc1	třicet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
počítačového	počítačový	k2eAgNnSc2d1	počítačové
oddělení	oddělení	k1gNnSc2	oddělení
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
prezidenta	prezident	k1gMnSc2	prezident
Chucka	Chucka	k1gFnSc1	Chucka
Kolstada	Kolstada	k1gFnSc1	Kolstada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Pixar	Pixar	k1gInSc1	Pixar
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
Disneyem	Disney	k1gMnSc7	Disney
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vytvořit	vytvořit	k5eAaPmF	vytvořit
tři	tři	k4xCgInPc1	tři
animované	animovaný	k2eAgInPc1d1	animovaný
celovečerní	celovečerní	k2eAgInPc1d1	celovečerní
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
dostal	dostat	k5eAaPmAgMnS	dostat
asi	asi	k9	asi
26	[number]	k4	26
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
však	však	k9	však
společnost	společnost	k1gFnSc1	společnost
prodělávala	prodělávat	k5eAaImAgFnS	prodělávat
a	a	k8xC	a
Jobs	Jobs	k1gInSc4	Jobs
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pixar	Pixar	k1gInSc1	Pixar
prodá	prodat	k5eAaPmIp3nS	prodat
jiné	jiný	k2eAgFnPc4d1	jiná
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
Microsoftu	Microsofta	k1gFnSc4	Microsofta
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
Disney	Disnea	k1gFnPc4	Disnea
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pustí	pustit	k5eAaPmIp3nS	pustit
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
1995	[number]	k4	1995
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jobs	Jobs	k1gInSc1	Jobs
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
dát	dát	k5eAaPmF	dát
Pixaru	Pixara	k1gFnSc4	Pixara
ještě	ještě	k9	ještě
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgInPc1d1	celosvětový
výdělky	výdělek	k1gInPc1	výdělek
se	se	k3xPyFc4	se
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
na	na	k7c4	na
350	[number]	k4	350
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
tak	tak	k9	tak
cena	cena	k1gFnSc1	cena
akcie	akcie	k1gFnSc2	akcie
Pixaru	Pixar	k1gInSc2	Pixar
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
22	[number]	k4	22
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
společnost	společnost	k1gFnSc1	společnost
Disney	Disnea	k1gFnSc2	Disnea
oznámila	oznámit	k5eAaPmAgFnS	oznámit
záměr	záměr	k1gInSc4	záměr
koupit	koupit	k5eAaPmF	koupit
všechny	všechen	k3xTgFnPc4	všechen
akcie	akcie	k1gFnPc4	akcie
společnosti	společnost	k1gFnSc2	společnost
Pixar	Pixara	k1gFnPc2	Pixara
za	za	k7c4	za
7.4	[number]	k4	7.4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
dokončen	dokončit	k5eAaPmNgInS	dokončit
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
ho	on	k3xPp3gNnSc4	on
schválili	schválit	k5eAaPmAgMnP	schválit
akcionáři	akcionář	k1gMnPc1	akcionář
Pixaru	Pixar	k1gInSc2	Pixar
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
transakce	transakce	k1gFnSc1	transakce
vynesla	vynést	k5eAaPmAgFnS	vynést
Steva	Steve	k1gMnSc4	Steve
Jobse	Jobs	k1gMnSc4	Jobs
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
50.1	[number]	k4	50.1
<g/>
%	%	kIx~	%
Pixaru	Pixar	k1gInSc2	Pixar
<g/>
,	,	kIx,	,
na	na	k7c4	na
Disneyho	Disney	k1gMnSc4	Disney
největšího	veliký	k2eAgMnSc4d3	veliký
individuálního	individuální	k2eAgMnSc4d1	individuální
akcionáře	akcionář	k1gMnSc4	akcionář
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
náleží	náležet	k5eAaImIp3nS	náležet
7	[number]	k4	7
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
správní	správní	k2eAgFnSc6d1	správní
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
Pixaru	Pixar	k1gInSc2	Pixar
John	John	k1gMnSc1	John
Lasseter	Lasseter	k1gMnSc1	Lasseter
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
výkonný	výkonný	k2eAgMnSc1d1	výkonný
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
šéfem	šéf	k1gMnSc7	šéf
kreativních	kreativní	k2eAgFnPc2d1	kreativní
oddělení	oddělení	k1gNnSc4	oddělení
jak	jak	k6eAd1	jak
společnosti	společnost	k1gFnSc3	společnost
Pixar	Pixara	k1gFnPc2	Pixara
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
společnosti	společnost	k1gFnSc2	společnost
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
hlavním	hlavní	k2eAgMnSc7d1	hlavní
poradcem	poradce	k1gMnSc7	poradce
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Imagineering	Imagineering	k1gInSc1	Imagineering
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
a	a	k8xC	a
staví	stavit	k5eAaImIp3nS	stavit
tematické	tematický	k2eAgInPc4d1	tematický
zábavní	zábavní	k2eAgInPc4d1	zábavní
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Catmull	Catmull	k1gMnSc1	Catmull
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
místo	místo	k1gNnSc4	místo
prezidenta	prezident	k1gMnSc2	prezident
Pixaru	Pixar	k1gInSc2	Pixar
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
animačních	animační	k2eAgFnPc2d1	animační
studií	studie	k1gFnPc2	studie
Walta	Walt	k1gMnSc2	Walt
Disneyho	Disney	k1gMnSc2	Disney
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
pak	pak	k6eAd1	pak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Boba	Bob	k1gMnSc2	Bob
Igera	Igera	k1gMnSc1	Igera
a	a	k8xC	a
Dicka	Dicka	k1gMnSc1	Dicka
Cooka	Cooka	k1gMnSc1	Cooka
za	za	k7c2	za
předsedy	předseda	k1gMnSc2	předseda
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Studio	studio	k1gNnSc1	studio
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
opustil	opustit	k5eAaPmAgMnS	opustit
místo	místo	k7c2	místo
ředitele	ředitel	k1gMnSc2	ředitel
Pixaru	Pixar	k1gInSc2	Pixar
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
představenstvu	představenstvo	k1gNnSc6	představenstvo
společnosti	společnost	k1gFnSc2	společnost
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Pixar	Pixar	k1gMnSc1	Pixar
nové	nový	k2eAgNnSc4d1	nové
studio	studio	k1gNnSc4	studio
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Vancouveru	Vancouver	k1gInSc2	Vancouver
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ho	on	k3xPp3gInSc4	on
Glenn	Glenn	k1gInSc4	Glenn
McQueen	McQueen	k2eAgInSc4d1	McQueen
Pixar	Pixar	k1gInSc4	Pixar
Animation	Animation	k1gInSc4	Animation
Center	centrum	k1gNnPc2	centrum
po	po	k7c6	po
kanadském	kanadský	k2eAgMnSc6d1	kanadský
rodákovi	rodák	k1gMnSc6	rodák
Glennu	Glenn	k1gMnSc6	Glenn
McQueenovi	McQueen	k1gMnSc6	McQueen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Pixar	Pixar	k1gInSc4	Pixar
a	a	k8xC	a
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
věnován	věnovat	k5eAaImNgInS	věnovat
film	film	k1gInSc1	film
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k1gMnSc1	Nemo
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
2000	[number]	k4	2000
m	m	kA	m
<g/>
2	[number]	k4	2
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
především	především	k6eAd1	především
krátké	krátký	k2eAgInPc4d1	krátký
filmy	film	k1gInPc4	film
a	a	k8xC	a
televizní	televizní	k2eAgInPc4d1	televizní
speciály	speciál	k1gInPc4	speciál
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
od	od	k7c2	od
Pixaru	Pixar	k1gInSc2	Pixar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pořádá	pořádat	k5eAaImIp3nS	pořádat
Pixar	Pixar	k1gInSc4	Pixar
výstavy	výstava	k1gFnSc2	výstava
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
vývoj	vývoj	k1gInSc4	vývoj
animací	animace	k1gFnPc2	animace
a	a	k8xC	a
nejslavnější	slavný	k2eAgMnPc4d3	nejslavnější
animátory	animátor	k1gMnPc4	animátor
za	za	k7c4	za
20	[number]	k4	20
let	léto	k1gNnPc2	léto
jejich	jejich	k3xOp3gFnSc2	jejich
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
představuje	představovat	k5eAaImIp3nS	představovat
práci	práce	k1gFnSc4	práce
Pixaru	Pixar	k1gInSc2	Pixar
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgInPc2d1	různý
pohledů	pohled	k1gInPc2	pohled
náčrty	náčrt	k1gInPc1	náčrt
<g/>
,	,	kIx,	,
hliněné	hliněný	k2eAgFnPc1d1	hliněná
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
3D	[number]	k4	3D
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
Zoetrope	Zoetrop	k1gInSc5	Zoetrop
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc4d1	speciální
způsob	způsob	k1gInSc4	způsob
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
postavičky	postavička	k1gFnPc4	postavička
z	z	k7c2	z
Příběhu	příběh	k1gInSc2	příběh
hraček	hračka	k1gFnPc2	hračka
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Pixar	Pixar	k1gInSc1	Pixar
oslavil	oslavit	k5eAaPmAgInS	oslavit
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
film	film	k1gInSc4	film
Auta	auto	k1gNnSc2	auto
2	[number]	k4	2
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
-	-	kIx~	-
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
1998	[number]	k4	1998
-	-	kIx~	-
Život	život	k1gInSc1	život
brouka	brouk	k1gMnSc2	brouk
(	(	kIx(	(
<g/>
A	a	k9	a
Bug	Bug	k1gFnSc1	Bug
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Life	Life	k1gFnSc7	Life
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
-	-	kIx~	-
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
2	[number]	k4	2
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc4	příběh
hraček	hračka	k1gFnPc2	hračka
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Příšerky	příšerka	k1gFnPc1	příšerka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
Monsters	Monsters	k1gInSc1	Monsters
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k1gMnSc1	Nemo
(	(	kIx(	(
<g/>
Finding	Finding	k1gInSc1	Finding
Nemo	Nemo	k1gNnSc1	Nemo
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
Úžasňákovi	Úžasňákův	k2eAgMnPc1d1	Úžasňákův
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Incredibles	Incredibles	k1gMnSc1	Incredibles
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
-	-	kIx~	-
Auta	auto	k1gNnSc2	auto
(	(	kIx(	(
<g/>
Cars	Cars	k1gInSc1	Cars
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
-	-	kIx~	-
Ratatouille	Ratatouille	k1gFnSc1	Ratatouille
<g />
.	.	kIx.	.
</s>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
VALL-I	VALL-I	k1gMnSc1	VALL-I
(	(	kIx(	(
<g/>
WALL-E	WALL-E	k1gMnSc1	WALL-E
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
(	(	kIx(	(
<g/>
Up	Up	k1gFnSc1	Up
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc2	story
3	[number]	k4	3
2011	[number]	k4	2011
-	-	kIx~	-
Auta	auto	k1gNnSc2	auto
2	[number]	k4	2
(	(	kIx(	(
<g/>
Cars	Cars	k1gInSc1	Cars
2	[number]	k4	2
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
Rebelka	rebelka	k1gFnSc1	rebelka
(	(	kIx(	(
<g/>
Brave	brav	k1gInSc5	brav
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Univerzita	univerzita	k1gFnSc1	univerzita
pro	pro	k7c4	pro
příšerky	příšerka	k1gFnPc4	příšerka
(	(	kIx(	(
<g/>
Monsters	Monsters	k1gInSc1	Monsters
University	universita	k1gFnSc2	universita
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
–	–	k?	–
V	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
(	(	kIx(	(
<g/>
Inside	Insid	k1gMnSc5	Insid
Out	Out	k1gMnSc5	Out
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
-	-	kIx~	-
Hodný	hodný	k2eAgMnSc1d1	hodný
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Good	Good	k1gMnSc1	Good
Dinosaur	Dinosaur	k1gMnSc1	Dinosaur
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
-	-	kIx~	-
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Dory	Dora	k1gFnSc2	Dora
(	(	kIx(	(
<g/>
Finding	Finding	k1gInSc1	Finding
Dory	Dora	k1gFnSc2	Dora
<g/>
)	)	kIx)	)
Auta	auto	k1gNnSc2	auto
3	[number]	k4	3
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Coco	Coco	k6eAd1	Coco
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Přiběh	Přiběh	k1gInSc4	Přiběh
hraček	hračka	k1gFnPc2	hračka
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
Úžasňákovi	Úžasňákův	k2eAgMnPc1d1	Úžasňákův
2	[number]	k4	2
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
-	-	kIx~	-
André	André	k1gMnSc1	André
a	a	k8xC	a
včela	včela	k1gFnSc1	včela
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
André	André	k1gMnSc1	André
and	and	k?	and
Wally	Walla	k1gFnSc2	Walla
B.	B.	kA	B.
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
-	-	kIx~	-
Luxo	Luxo	k1gMnSc1	Luxo
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
-	-	kIx~	-
Sen	sen	k1gInSc1	sen
o	o	k7c6	o
klaunovi	klaun	k1gMnSc6	klaun
(	(	kIx(	(
<g/>
Red	Red	k1gFnSc2	Red
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dream	Dream	k1gInSc1	Dream
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Plechový	plechový	k2eAgMnSc1d1	plechový
panáček	panáček	k1gMnSc1	panáček
(	(	kIx(	(
<g/>
Tin	Tina	k1gFnPc2	Tina
Toy	Toy	k1gFnPc2	Toy
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
-	-	kIx~	-
Suvenýry	suvenýr	k1gInPc1	suvenýr
(	(	kIx(	(
<g/>
Knick	Knick	k1gMnSc1	Knick
Knack	Knack	k1gMnSc1	Knack
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
-	-	kIx~	-
Geriho	Geri	k1gMnSc2	Geri
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
Geri	Geri	k1gNnSc1	Geri
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
–	–	k?	–
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
snímek	snímek	k1gInSc4	snímek
získal	získat	k5eAaPmAgMnS	získat
Jan	Jan	k1gMnSc1	Jan
Pinkava	Pinkava	k?	Pinkava
Oscara	Oscara	k1gFnSc1	Oscara
2000	[number]	k4	2000
-	-	kIx~	-
Pro	pro	k7c4	pro
ptáčky	ptáček	k1gInPc4	ptáček
(	(	kIx(	(
<g/>
For	forum	k1gNnPc2	forum
the	the	k?	the
Birds	Birds	k1gInSc1	Birds
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
2002	[number]	k4	2002
-	-	kIx~	-
Mikeovo	Mikeův	k2eAgNnSc1d1	Mikeovo
nové	nový	k2eAgNnSc1d1	nové
auto	auto	k1gNnSc1	auto
(	(	kIx(	(
<g/>
Mike	Mike	k1gInSc1	Mike
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
Car	car	k1gMnSc1	car
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Zajdalen	Zajdalen	k2eAgMnSc1d1	Zajdalen
(	(	kIx(	(
<g/>
Boundin	Boundin	k2eAgMnSc1d1	Boundin
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Jack-Jack	Jack-Jacko	k1gNnPc2	Jack-Jacko
útočí	útočit	k5eAaImIp3nS	útočit
(	(	kIx(	(
<g/>
Jack-Jack	Jack-Jack	k1gMnSc1	Jack-Jack
Attack	Attack	k1gMnSc1	Attack
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
(	(	kIx(	(
<g/>
One	One	k1gFnSc6	One
Man	mana	k1gFnPc2	mana
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Burák	burák	k1gInSc1	burák
a	a	k8xC	a
Bludička	bludička	k1gFnSc1	bludička
(	(	kIx(	(
<g/>
Mater	mater	k1gFnSc1	mater
and	and	k?	and
the	the	k?	the
Ghostlight	Ghostlight	k1gInSc1	Ghostlight
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
-	-	kIx~	-
Únos	únos	k1gInSc1	únos
(	(	kIx(	(
<g/>
Lifted	Lifted	k1gInSc1	Lifted
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
-	-	kIx~	-
Vaši	váš	k3xOp2gMnPc1	váš
kamarádi	kamarád	k1gMnPc1	kamarád
hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
Your	Your	k1gMnSc1	Your
Friend	Friend	k1gMnSc1	Friend
the	the	k?	the
Rat	Rat	k1gMnSc1	Rat
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Sim	sima	k1gFnPc2	sima
Salabim	Salabim	k1gInSc1	Salabim
(	(	kIx(	(
<g/>
Presto	presto	k1gNnSc1	presto
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Rescue	Rescu	k1gInSc2	Rescu
Squad	Squad	k1gInSc1	Squad
Mater	mater	k1gFnSc1	mater
2008	[number]	k4	2008
-	-	kIx~	-
Mater	Matra	k1gFnPc2	Matra
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Greater	Greater	k1gInSc1	Greater
2008	[number]	k4	2008
-	-	kIx~	-
El	Ela	k1gFnPc2	Ela
Materdor	Materdor	k1gInSc1	Materdor
2008	[number]	k4	2008
-	-	kIx~	-
Tokyo	Tokyo	k1gNnSc4	Tokyo
Mater	mater	k1gFnSc2	mater
2008	[number]	k4	2008
-	-	kIx~	-
BURN-E	BURN-E	k1gFnSc1	BURN-E
2009	[number]	k4	2009
-	-	kIx~	-
Unidentified	Unidentified	k1gInSc1	Unidentified
Flying	Flying	k1gInSc1	Flying
Mater	mater	k1gFnSc1	mater
2009	[number]	k4	2009
-	-	kIx~	-
Polojasno	polojasno	k1gNnSc1	polojasno
(	(	kIx(	(
<g/>
Partly	Partly	k1gFnSc1	Partly
Cloudy	Clouda	k1gFnSc2	Clouda
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Dugova	Dugov	k1gInSc2	Dugov
solo	solo	k1gNnSc1	solo
mise	mise	k1gFnSc2	mise
(	(	kIx(	(
<g/>
Dugs	Dugs	k1gInSc1	Dugs
Special	Special	k1gMnSc1	Special
Mission	Mission	k1gInSc1	Mission
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Monster	monstrum	k1gNnPc2	monstrum
Truck	truck	k1gInSc4	truck
Mater	mater	k1gFnPc4	mater
2010	[number]	k4	2010
-	-	kIx~	-
Heavy	Heava	k1gFnSc2	Heava
Metal	metal	k1gInSc1	metal
Mater	mater	k1gFnSc4	mater
2010	[number]	k4	2010
-	-	kIx~	-
Moon	Moon	k1gInSc1	Moon
Mater	mater	k1gFnSc2	mater
2010	[number]	k4	2010
-	-	kIx~	-
Mater	Matra	k1gFnPc2	Matra
Private	Privat	k1gInSc5	Privat
Eye	Eye	k1gMnSc7	Eye
2010	[number]	k4	2010
-	-	kIx~	-
Den	den	k1gInSc1	den
a	a	k8xC	a
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
Day	Day	k1gMnSc1	Day
&	&	k?	&
Night	Night	k1gMnSc1	Night
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
La	la	k1gNnPc2	la
Luna	luna	k1gFnSc1	luna
2015	[number]	k4	2015
-	-	kIx~	-
Frozen	Frozen	k2eAgInSc1d1	Frozen
Fever	Fever	k1gInSc1	Fever
</s>
