<s>
Jejími	její	k3xOp3gMnPc7
členy	člen	k1gMnPc7
byli	být	k5eAaImAgMnP
John	John	k1gMnSc1
Lennon	Lennon	k1gMnSc1
(	(	kIx(
<g/>
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
doprovodná	doprovodný	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
McCartney	McCartnea	k1gFnSc2
(	(	kIx(
<g/>
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
basová	basový	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
George	George	k1gFnSc1
Harrison	Harrison	k1gMnSc1
(	(	kIx(
<g/>
sólová	sólový	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Ringo	Ringo	k1gMnSc1
Starr	Starr	k1gMnSc1
(	(	kIx(
<g/>
bicí	bicí	k2eAgFnSc1d1
<g/>
,	,	kIx,
příležitostně	příležitostně	k6eAd1
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>