<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hakl	Hakl	k1gMnSc1	Hakl
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jan	Jan	k1gMnSc1	Jan
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
<g/>
,	,	kIx,	,
obor	obor	k1gInSc1	obor
tvorba	tvorba	k1gFnSc1	tvorba
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc1	dva
ročníky	ročník	k1gInPc1	ročník
dramatického	dramatický	k2eAgInSc2d1	dramatický
oboru	obor	k1gInSc2	obor
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
aranžér	aranžér	k1gMnSc1	aranžér
<g/>
,	,	kIx,	,
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
,	,	kIx,	,
skladník	skladník	k1gMnSc1	skladník
<g/>
,	,	kIx,	,
strojník	strojník	k1gMnSc1	strojník
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
zvukař	zvukař	k1gMnSc1	zvukař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
textař	textař	k1gMnSc1	textař
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
reklamních	reklamní	k2eAgFnPc2d1	reklamní
agentur	agentura	k1gFnPc2	agentura
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jako	jako	k8xC	jako
redaktor	redaktor	k1gMnSc1	redaktor
literárního	literární	k2eAgInSc2d1	literární
čtrnáctideníku	čtrnáctideník	k1gInSc2	čtrnáctideník
Tvar	tvar	k1gInSc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
v	v	k7c6	v
týdeníku	týdeník	k1gInSc6	týdeník
Instinkt	instinkt	k1gInSc1	instinkt
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
dramatizaci	dramatizace	k1gFnSc4	dramatizace
literárních	literární	k2eAgFnPc2d1	literární
předloh	předloha	k1gFnPc2	předloha
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
realizaci	realizace	k1gFnSc4	realizace
v	v	k7c6	v
amatérských	amatérský	k2eAgFnPc6d1	amatérská
divadelních	divadelní	k2eAgFnPc6d1	divadelní
formacích	formace	k1gFnPc6	formace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
volného	volný	k2eAgNnSc2d1	volné
literárního	literární	k2eAgNnSc2d1	literární
sdružení	sdružení	k1gNnSc2	sdružení
Moderní	moderní	k2eAgMnSc1d1	moderní
analfabet	analfabet	k1gMnSc1	analfabet
(	(	kIx(	(
<g/>
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžištěm	těžiště	k1gNnSc7	těžiště
aktivity	aktivita	k1gFnSc2	aktivita
sdružení	sdružení	k1gNnSc2	sdružení
bylo	být	k5eAaImAgNnS	být
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
autorské	autorský	k2eAgNnSc4d1	autorské
čtení	čtení	k1gNnSc4	čtení
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
i	i	k8xC	i
mimopražských	mimopražský	k2eAgInPc6d1	mimopražský
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
navázala	navázat	k5eAaPmAgNnP	navázat
autorská	autorský	k2eAgNnPc1d1	autorské
čtení	čtení	k1gNnPc1	čtení
v	v	k7c6	v
Literárním	literární	k2eAgInSc6d1	literární
klubu	klub	k1gInSc6	klub
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
s	s	k7c7	s
literárním	literární	k2eAgInSc7d1	literární
Pant	pant	k1gInSc1	pant
klubem	klub	k1gInSc7	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časopisecky	časopisecky	k6eAd1	časopisecky
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
Iniciálách	iniciála	k1gFnPc6	iniciála
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
Literárních	literární	k2eAgFnPc6d1	literární
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Aluzi	aluze	k1gFnSc6	aluze
<g/>
,	,	kIx,	,
v	v	k7c6	v
Salonu	salon	k1gInSc6	salon
<g/>
,	,	kIx,	,
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Týdnu	týden	k1gInSc6	týden
a	a	k8xC	a
v	v	k7c6	v
internetovém	internetový	k2eAgInSc6d1	internetový
časopise	časopis	k1gInSc6	časopis
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
adresa	adresa	k1gFnSc1	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
výboru	výbor	k1gInSc6	výbor
Anthologie	Anthologie	k1gFnSc2	Anthologie
de	de	k?	de
la	la	k1gNnSc1	la
poésie	poésie	k1gFnSc1	poésie
tchéque	tchéqu	k1gMnSc2	tchéqu
contemporaine	contemporainout	k5eAaPmIp3nS	contemporainout
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
Gallimard	Gallimard	k1gInSc1	Gallimard
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
sbornících	sborník	k1gInPc6	sborník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dílech	díl	k1gInPc6	díl
svého	svůj	k1gMnSc4	svůj
kolegy	kolega	k1gMnSc2	kolega
Václava	Václav	k1gMnSc2	Václav
Kahudy	Kahuda	k1gMnSc2	Kahuda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
novele	novela	k1gFnSc6	novela
Technologie	technologie	k1gFnSc2	technologie
dubnového	dubnový	k2eAgInSc2d1	dubnový
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodil	chodit	k5eAaImAgMnS	chodit
s	s	k7c7	s
hudebnicí	hudebnice	k1gFnSc7	hudebnice
Evou	Eva	k1gFnSc7	Eva
Turnovou	Turnová	k1gFnSc7	Turnová
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
pak	pak	k6eAd1	pak
románově	románově	k6eAd1	románově
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Intimní	intimní	k2eAgFnSc1d1	intimní
schránka	schránka	k1gFnSc1	schránka
Sabriny	Sabrin	k1gInPc4	Sabrin
Black	Blacko	k1gNnPc2	Blacko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Rozpojená	rozpojený	k2eAgNnPc1d1	rozpojené
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
</s>
</p>
<p>
<s>
Zkušební	zkušební	k2eAgInPc1d1	zkušební
trylky	trylek	k1gInPc1	trylek
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Cherm	Cherm	k1gInSc1	Cherm
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
–	–	k?	–
povídky	povídka	k1gFnSc2	povídka
</s>
</p>
<p>
<s>
Intimní	intimní	k2eAgFnSc1d1	intimní
schránka	schránka	k1gFnSc1	schránka
Sabriny	Sabrin	k1gInPc4	Sabrin
Black	Blacko	k1gNnPc2	Blacko
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
–	–	k?	–
román	román	k1gInSc1	román
</s>
</p>
<p>
<s>
O	o	k7c6	o
rodičích	rodič	k1gMnPc6	rodič
a	a	k8xC	a
dětech	dítě	k1gFnPc6	dítě
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
–	–	k?	–
novela	novela	k1gFnSc1	novela
oceněná	oceněný	k2eAgFnSc1d1	oceněná
cenou	cena	k1gFnSc7	cena
Magnesia	magnesium	k1gNnPc5	magnesium
Litera	litera	k1gFnSc1	litera
</s>
</p>
<p>
<s>
O	o	k7c6	o
létajících	létající	k2eAgInPc6d1	létající
objektech	objekt	k1gInPc6	objekt
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
–	–	k?	–
povídky	povídka	k1gFnSc2	povídka
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
–	–	k?	–
román	román	k1gInSc1	román
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
směšného	směšný	k2eAgNnSc2d1	směšné
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Intimní	intimní	k2eAgFnSc1d1	intimní
schránka	schránka	k1gFnSc1	schránka
Sabriny	Sabrin	k1gInPc4	Sabrin
Black	Black	k1gInSc1	Black
(	(	kIx(	(
<g/>
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
–	–	k?	–
zcela	zcela	k6eAd1	zcela
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
staršího	starý	k2eAgInSc2d2	starší
románu	román	k1gInSc2	román
</s>
</p>
<p>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Hovězí	hovězí	k2eAgFnPc1d1	hovězí
kostky	kostka	k1gFnPc1	kostka
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
–	–	k?	–
povídky	povídka	k1gFnSc2	povídka
</s>
</p>
<p>
<s>
Umina	Umien	k2eAgFnSc1d1	Umien
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
Zuzana	Zuzana	k1gFnSc1	Zuzana
Lazarová	Lazarová	k1gFnSc1	Lazarová
</s>
</p>
<p>
<s>
Výsek	výsek	k1gInSc1	výsek
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
časopiseckých	časopisecký	k2eAgInPc2d1	časopisecký
sloupků	sloupek	k1gInPc2	sloupek
z	z	k7c2	z
Čilichili	Čilichili	k1gFnSc2	Čilichili
<g/>
,	,	kIx,	,
rubrika	rubrika	k1gFnSc1	rubrika
PavlačPodle	PavlačPodle	k1gFnSc2	PavlačPodle
novely	novela	k1gFnSc2	novela
O	o	k7c6	o
rodičích	rodič	k1gMnPc6	rodič
a	a	k8xC	a
dětech	dítě	k1gFnPc6	dítě
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Michálek	Michálek	k1gMnSc1	Michálek
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
cena	cena	k1gFnSc1	cena
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
za	za	k7c4	za
novelu	novela	k1gFnSc4	novela
O	o	k7c6	o
rodičích	rodič	k1gMnPc6	rodič
a	a	k8xC	a
dětech	dítě	k1gFnPc6	dítě
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
Pravidla	pravidlo	k1gNnSc2	pravidlo
směšného	směšný	k2eAgNnSc2d1	směšné
chování	chování	k1gNnSc2	chování
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Kniha	kniha	k1gFnSc1	kniha
roku	rok	k1gInSc2	rok
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
literární	literární	k2eAgFnSc1d1	literární
Cena	cena	k1gFnSc1	cena
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgInSc2d1	Škvorecký
za	za	k7c4	za
knihu	kniha	k1gFnSc4	kniha
Pravidla	pravidlo	k1gNnSc2	pravidlo
směšného	směšný	k2eAgNnSc2d1	směšné
chování	chování	k1gNnSc2	chování
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
Pravidla	pravidlo	k1gNnSc2	pravidlo
směšného	směšný	k2eAgNnSc2d1	směšné
chování	chování	k1gNnSc2	chování
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
próza	próza	k1gFnSc1	próza
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
událost	událost	k1gFnSc1	událost
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Magnesia	magnesium	k1gNnSc2	magnesium
litera	litera	k1gFnSc1	litera
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
původní	původní	k2eAgFnSc1d1	původní
česká	český	k2eAgFnSc1d1	Česká
próza	próza	k1gFnSc1	próza
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Culik	Culik	k1gMnSc1	Culik
<g/>
,	,	kIx,	,
J.	J.	kA	J.
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Emil	Emil	k1gMnSc1	Emil
Hakl	Hakl	k1gMnSc1	Hakl
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Serafin	Serafin	k1gMnSc1	Serafin
<g/>
,	,	kIx,	,
S.	S.	kA	S.
and	and	k?	and
Mihailovich	Mihailovich	k1gMnSc1	Mihailovich
<g/>
,	,	kIx,	,
V.D.	V.D.	k1gMnSc1	V.D.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Twenty-first-century	Twentyirstentura	k1gFnPc1	Twenty-first-centura
Central	Central	k1gFnPc1	Central
and	and	k?	and
Eastern	Eastern	k1gInSc1	Eastern
European	European	k1gMnSc1	European
Writers	Writers	k1gInSc1	Writers
<g/>
.	.	kIx.	.
</s>
<s>
Series	Series	k1gInSc1	Series
<g/>
:	:	kIx,	:
Dictionary	Dictionara	k1gFnPc1	Dictionara
of	of	k?	of
Literary	Literara	k1gFnSc2	Literara
Biography	Biographa	k1gFnSc2	Biographa
<g/>
,	,	kIx,	,
353	[number]	k4	353
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Gale	Gale	k1gMnSc1	Gale
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
104	[number]	k4	104
<g/>
-	-	kIx~	-
<g/>
111	[number]	k4	111
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-0-7876-8171-5	[number]	k4	978-0-7876-8171-5
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Emil	Emil	k1gMnSc1	Emil
Hakl	Hakl	k1gMnSc1	Hakl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Emil	Emil	k1gMnSc1	Emil
Hakl	Hakl	k1gMnSc1	Hakl
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Emil	Emil	k1gMnSc1	Emil
Hakl	Hakl	k1gMnSc1	Hakl
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hakl	Hakl	k1gMnSc1	Hakl
ve	v	k7c6	v
Slovníku	slovník	k1gInSc6	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Medailon	medailon	k1gInSc1	medailon
na	na	k7c6	na
Portálu	portál	k1gInSc6	portál
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Čulík	Čulík	k1gMnSc1	Čulík
<g/>
:	:	kIx,	:
Hakl	Hakl	k1gMnSc1	Hakl
a	a	k8xC	a
Viewegh	Viewegh	k1gMnSc1	Viewegh
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
s	s	k7c7	s
banalitou	banalita	k1gFnSc7	banalita
života	život	k1gInSc2	život
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
formát	formát	k1gInSc4	formát
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
