<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgInSc1d1	ústřední
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
ústředních	ústřední	k2eAgInPc2d1	ústřední
správních	správní	k2eAgInPc2d1	správní
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
)	)	kIx)	)
neodlišuje	odlišovat	k5eNaImIp3nS	odlišovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
