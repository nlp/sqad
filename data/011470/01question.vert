<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ústřední	ústřední	k2eAgInSc1d1	ústřední
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
ministr	ministr	k1gMnSc1	ministr
<g/>
?	?	kIx.	?
</s>
