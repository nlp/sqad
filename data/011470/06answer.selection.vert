<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgInSc1d1	ústřední
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
