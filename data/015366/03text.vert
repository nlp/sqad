<s>
Revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
</s>
<s>
Revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ROH	roh	k1gInSc1
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odborů	odbor	k1gInPc2
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Československá	československý	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
Vznik	vznik	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
odborová	odborový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Dům	dům	k1gInSc1
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Žižkov	Žižkov	k1gInSc1
Působnost	působnost	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc4
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
Čechů	Čech	k1gMnPc2
a	a	k8xC
Slováků	Slovák	k1gMnPc2
Dřívější	dřívější	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odborů	odbor	k1gInPc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
ROH	ROH	kA
<g/>
;	;	kIx,
slovensky	slovensky	k6eAd1
Revolučné	Revolučný	k2eAgFnSc2d1
odborové	odborový	k2eAgFnSc2d1
hnutie	hnutie	k1gFnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
monopolní	monopolní	k2eAgFnSc1d1
odborová	odborový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
zároveň	zároveň	k6eAd1
nejmasovější	masový	k2eAgFnSc7d3
společenskou	společenský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
v	v	k7c6
socialistickém	socialistický	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohde	mnohde	k6eAd1
bylo	být	k5eAaImAgNnS
členství	členství	k1gNnSc1
zaměstnanců	zaměstnanec	k1gMnPc2
v	v	k7c4
ROH	roh	k1gInSc4
prakticky	prakticky	k6eAd1
povinné	povinný	k2eAgFnPc1d1
a	a	k8xC
automatické	automatický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Období	období	k1gNnSc1
Protektorátu	protektorát	k1gInSc2
</s>
<s>
Počátkem	počátkem	k7c2
monopolizace	monopolizace	k1gFnSc2
odborového	odborový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
byla	být	k5eAaImAgFnS
likvidace	likvidace	k1gFnSc1
odborového	odborový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
v	v	k7c6
září	září	k1gNnSc6
1938	#num#	k4
<g/>
,	,	kIx,
avšak	avšak	k8xC
jednání	jednání	k1gNnPc1
o	o	k7c6
jednotné	jednotný	k2eAgFnSc6d1
odborové	odborový	k2eAgFnSc6d1
organizaci	organizace	k1gFnSc6
za	za	k7c2
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
nevedla	vést	k5eNaImAgFnS
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
zřízení	zřízení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
protektorátu	protektorát	k1gInSc2
vznikly	vzniknout	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
centrální	centrální	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
odborová	odborový	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
zaměstnanecká	zaměstnanecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
NOÚZ	NOÚZ	kA
<g/>
)	)	kIx)
a	a	k8xC
menší	malý	k2eAgNnSc1d2
Ústředí	ústředí	k1gNnSc1
veřejných	veřejný	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
(	(	kIx(
<g/>
ÚVZ	ÚVZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NOÚZ	NOÚZ	kA
sloužila	sloužit	k5eAaImAgFnS
jak	jak	k6eAd1
jako	jako	k8xS,k8xC
nástroj	nástroj	k1gInSc1
nacistické	nacistický	k2eAgFnSc2d1
propagandy	propaganda	k1gFnSc2
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
pracovního	pracovní	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
jako	jako	k9
semeniště	semeniště	k1gNnSc1
protinacistických	protinacistický	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsně	těsně	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
se	se	k3xPyFc4
v	v	k7c6
NOÚZ	NOÚZ	kA
zformovala	zformovat	k5eAaPmAgFnS
Ústřední	ústřední	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odborů	odbor	k1gInPc2
(	(	kIx(
<g/>
ÚRO	Úro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
ČSR	ČSR	kA
a	a	k8xC
ČSSR	ČSSR	kA
</s>
<s>
ÚRO	Úro	k1gNnSc1
bylo	být	k5eAaImAgNnS
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
Českou	český	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
pověřeno	pověřit	k5eAaPmNgNnS
k	k	k7c3
převzetí	převzetí	k1gNnSc3
všech	všecek	k3xTgInPc2
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
a	a	k8xC
spolků	spolek	k1gInPc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
načež	načež	k6eAd1
ji	on	k3xPp3gFnSc4
výnos	výnos	k1gInSc1
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
IV-	IV-	k1gMnSc1
<g/>
3111	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
45	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
právního	právní	k2eAgMnSc4d1
nástupce	nástupce	k1gMnSc4
všech	všecek	k3xTgFnPc2
odborových	odborový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
č.	č.	k?
144	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
jednotné	jednotný	k2eAgFnSc6d1
odborové	odborový	k2eAgFnSc6d1
organizaci	organizace	k1gFnSc6
<g/>
,	,	kIx,
již	již	k6eAd1
používal	používat	k5eAaImAgMnS
název	název	k1gInSc4
Revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
ROH	roh	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ROH	roh	k1gInSc4
bylo	být	k5eAaImAgNnS
považováno	považován	k2eAgNnSc1d1
za	za	k7c2
právního	právní	k2eAgMnSc2d1
nástupce	nástupce	k1gMnSc2
ÚRO	Úro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
opět	opět	k6eAd1
potvrzoval	potvrzovat	k5eAaImAgInS
převedení	převedení	k1gNnSc4
veškerého	veškerý	k3xTgInSc2
majetku	majetek	k1gInSc2
odborových	odborový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
na	na	k7c4
ROH	roh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
získalo	získat	k5eAaPmAgNnS
ROH	roh	k1gInSc4
velké	velká	k1gFnSc2
pravomoci	pravomoc	k1gFnSc2
i	i	k8xC
velký	velký	k2eAgInSc4d1
neformální	formální	k2eNgInSc4d1
vliv	vliv	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
přes	přes	k7c4
deklarovanou	deklarovaný	k2eAgFnSc4d1
nadstranickost	nadstranickost	k1gFnSc4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
vlivu	vliv	k1gInSc2
KSČ	KSČ	kA
a	a	k8xC
po	po	k7c6
převzetí	převzetí	k1gNnSc6
moci	moc	k1gFnSc2
komunisty	komunista	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
zcela	zcela	k6eAd1
do	do	k7c2
jejího	její	k3xOp3gInSc2
stínu	stín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dům	dům	k1gInSc1
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Žižkově	Žižkov	k1gInSc6
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
Ústřední	ústřední	k2eAgFnSc2d1
rady	rada	k1gFnSc2
odborů	odbor	k1gInPc2
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1951	#num#	k4
se	se	k3xPyFc4
ROH	roh	k1gInSc1
stalo	stát	k5eAaPmAgNnS
vrcholným	vrcholný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
a	a	k8xC
správcem	správce	k1gMnSc7
nemocenského	mocenský	k2eNgNnSc2d1,k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonem	zákon	k1gInSc7
č.	č.	k?
68	#num#	k4
<g/>
/	/	kIx~
<g/>
1951	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
bylo	být	k5eAaImAgNnS
ROH	roh	k1gInSc4
uvedeno	uvést	k5eAaPmNgNnS
jako	jako	k9
dobrovolná	dobrovolný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
systému	systém	k1gInSc2
Národní	národní	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
měla	mít	k5eAaImAgFnS
Ústřední	ústřední	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odborů	odbor	k1gInPc2
na	na	k7c6
Žižkově	Žižkov	k1gInSc6
ve	v	k7c6
funkcionalistickém	funkcionalistický	k2eAgInSc6d1
bývalém	bývalý	k2eAgInSc6d1
paláci	palác	k1gInSc6
Všeobecného	všeobecný	k2eAgInSc2d1
penzijního	penzijní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
kachlíkárna	kachlíkárna	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Dům	dům	k1gInSc1
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
postaveném	postavený	k2eAgInSc6d1
v	v	k7c6
letech	let	k1gInPc6
1932	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
na	na	k7c6
místě	místo	k1gNnSc6
bývalé	bývalý	k2eAgFnSc2d1
plynárny	plynárna	k1gFnSc2
podle	podle	k7c2
návrhů	návrh	k1gInPc2
architekta	architekt	k1gMnSc2
Karla	Karel	k1gMnSc2
Honzíka	Honzík	k1gMnSc2
a	a	k8xC
Josefa	Josef	k1gMnSc2
Havlíčka	Havlíček	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1968	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
se	se	k3xPyFc4
ROH	roh	k1gInSc1
zapojilo	zapojit	k5eAaPmAgNnS
do	do	k7c2
obrodného	obrodný	k2eAgInSc2d1
procesu	proces	k1gInSc2
a	a	k8xC
pokoušelo	pokoušet	k5eAaImAgNnS
se	se	k3xPyFc4
vybudovat	vybudovat	k5eAaPmF
vlastní	vlastní	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
některé	některý	k3yIgNnSc1
z	z	k7c2
tehdy	tehdy	k6eAd1
vytvořených	vytvořený	k2eAgInPc2d1
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
vydržely	vydržet	k5eAaPmAgFnP
poměrně	poměrně	k6eAd1
dlouho	dlouho	k6eAd1
vzdorovat	vzdorovat	k5eAaImF
postupující	postupující	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
v	v	k7c6
listopadu	listopad	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
mnohé	mnohý	k2eAgInPc1d1
podnikové	podnikový	k2eAgInPc1d1
výbory	výbor	k1gInPc1
ROH	roh	k1gInSc4
připojily	připojit	k5eAaPmAgInP
k	k	k7c3
protestům	protest	k1gInPc3
proti	proti	k7c3
brutálnímu	brutální	k2eAgInSc3d1
policejnímu	policejní	k2eAgInSc3d1
zásahu	zásah	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
socialismu	socialismus	k1gInSc2
nebylo	být	k5eNaImAgNnS
konfrontační	konfrontační	k2eAgNnSc1d1
vymezování	vymezování	k1gNnSc1
se	se	k3xPyFc4
vůči	vůči	k7c3
zaměstnavateli	zaměstnavatel	k1gMnSc3
žádoucí	žádoucí	k2eAgMnSc1d1
<g/>
,	,	kIx,
činnost	činnost	k1gFnSc1
odborových	odborový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
ROH	roh	k1gInSc1
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
zaměřovala	zaměřovat	k5eAaImAgFnS
na	na	k7c6
vybírání	vybírání	k1gNnSc6
členských	členský	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
<g/>
,	,	kIx,
organizování	organizování	k1gNnSc3
společenských	společenský	k2eAgFnPc2d1
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
oslavy	oslava	k1gFnPc4
MDŽ	MDŽ	kA
<g/>
)	)	kIx)
a	a	k8xC
rekreačních	rekreační	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
a	a	k8xC
provozování	provozování	k1gNnSc4
rekreačních	rekreační	k2eAgInPc2d1
objektů	objekt	k1gInPc2
pro	pro	k7c4
pracovníky	pracovník	k1gMnPc4
<g/>
,	,	kIx,
příslovečně	příslovečně	k6eAd1
příznačnou	příznačný	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
bylo	být	k5eAaImAgNnS
každoroční	každoroční	k2eAgNnSc1d1
rozdávání	rozdávání	k1gNnSc1
vánočních	vánoční	k2eAgFnPc2d1
kolekcí	kolekce	k1gFnPc2
čokoládových	čokoládový	k2eAgFnPc2d1
figurek	figurka	k1gFnPc2
určených	určený	k2eAgInPc2d1
k	k	k7c3
zavěšování	zavěšování	k1gNnSc3
na	na	k7c4
vánoční	vánoční	k2eAgInPc4d1
stromky	stromek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odborové	odborový	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
stranické	stranický	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
KSČ	KSČ	kA
<g/>
,	,	kIx,
také	také	k9
v	v	k7c6
různých	různý	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
podílely	podílet	k5eAaImAgInP
na	na	k7c6
hodnocení	hodnocení	k1gNnSc6
pracovníků	pracovník	k1gMnPc2
<g/>
,	,	kIx,
mohly	moct	k5eAaImAgFnP
se	se	k3xPyFc4
za	za	k7c4
ně	on	k3xPp3gMnPc4
zaručovat	zaručovat	k5eAaImF
v	v	k7c6
trestním	trestní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
atd.	atd.	kA
Nelze	lze	k6eNd1
však	však	k9
nevzpomenout	vzpomenout	k5eNaPmF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ROH	roh	k1gInSc4
obhospodařovalo	obhospodařovat	k5eAaImAgNnS
veliké	veliký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
odborářských	odborářský	k2eAgFnPc2d1
zotavoven	zotavovna	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
každoročně	každoročně	k6eAd1
trávili	trávit	k5eAaImAgMnP
dovolenou	dovolená	k1gFnSc4
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
rodinami	rodina	k1gFnPc7
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
téměř	téměř	k6eAd1
zadarmo	zadarmo	k6eAd1
–	–	k?
statisíce	statisíce	k1gInPc4
pracujících	pracující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Zákony	zákon	k1gInPc1
a	a	k8xC
vyhlášky	vyhláška	k1gFnPc1
</s>
<s>
Dekret	dekret	k1gInSc1
presidenta	president	k1gMnSc2
republiky	republika	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
104	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
závodních	závodní	k2eAgFnPc6d1
a	a	k8xC
podnikových	podnikový	k2eAgFnPc6d1
radách	rada	k1gFnPc6
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
prováděcí	prováděcí	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
ze	z	k7c2
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1946	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
216	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
144	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
-	-	kIx~
Zákon	zákon	k1gInSc1
o	o	k7c6
jednotné	jednotný	k2eAgFnSc6d1
odborové	odborový	k2eAgFnSc6d1
organisaci	organisace	k1gFnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
37	#num#	k4
<g/>
/	/	kIx~
<g/>
1959	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1959	#num#	k4
o	o	k7c6
postavení	postavení	k1gNnSc6
závodních	závodní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
základních	základní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
Revolučního	revoluční	k2eAgNnSc2d1
odborového	odborový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
122	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
-	-	kIx~
Vyhláška	vyhláška	k1gFnSc1
Ústřední	ústřední	k2eAgFnSc2d1
rady	rada	k1gFnSc2
odborů	odbor	k1gInPc2
a	a	k8xC
ministerstva	ministerstvo	k1gNnSc2
financí	finance	k1gFnPc2
o	o	k7c6
poskytování	poskytování	k1gNnSc6
prostředků	prostředek	k1gInPc2
a	a	k8xC
o	o	k7c4
uhrazování	uhrazování	k1gNnSc4
nákladů	náklad	k1gInPc2
na	na	k7c4
činnost	činnost	k1gFnSc4
základních	základní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
Revolučního	revoluční	k2eAgNnSc2d1
odborového	odborový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
v	v	k7c6
organizacích	organizace	k1gFnPc6
státního	státní	k2eAgInSc2d1
socialistického	socialistický	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
121	#num#	k4
<g/>
/	/	kIx~
<g/>
1965	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
-	-	kIx~
Vyhláška	vyhláška	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
financí	finance	k1gFnPc2
a	a	k8xC
Ústřední	ústřední	k2eAgFnSc2d1
rady	rada	k1gFnSc2
odborů	odbor	k1gInPc2
ze	z	k7c2
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1965	#num#	k4
o	o	k7c6
použití	použití	k1gNnSc6
prostředků	prostředek	k1gInPc2
fondu	fond	k1gInSc2
kulturních	kulturní	k2eAgFnPc2d1
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
a	a	k8xC
financování	financování	k1gNnSc2
některých	některý	k3yIgFnPc2
činností	činnost	k1gFnPc2
státních	státní	k2eAgFnPc2d1
hospodářských	hospodářský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
</s>
<s>
Zánik	zánik	k1gInSc1
organizace	organizace	k1gFnSc2
</s>
<s>
Mimořádný	mimořádný	k2eAgInSc1d1
všeodborový	všeodborový	k2eAgInSc1d1
všesvazový	všesvazový	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
ve	v	k7c6
dnech	den	k1gInPc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c6
zrušení	zrušení	k1gNnSc6
ROH	roh	k1gInSc4
a	a	k8xC
předání	předání	k1gNnSc4
pravomocí	pravomoc	k1gFnPc2
i	i	k8xC
majetku	majetek	k1gInSc2
nástupnickým	nástupnický	k2eAgFnPc3d1
organizacím	organizace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
konstituované	konstituovaný	k2eAgInPc1d1
svazy	svaz	k1gInPc1
se	se	k3xPyFc4
sdružily	sdružit	k5eAaPmAgInP
v	v	k7c4
Československou	československý	k2eAgFnSc4d1
konfederaci	konfederace	k1gFnSc4
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
a	a	k8xC
v	v	k7c4
její	její	k3xOp3gFnSc4
Českomoravskou	českomoravský	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zastřešuje	zastřešovat	k5eAaImIp3nS
odborové	odborový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
doposud	doposud	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
vzniklo	vzniknout	k5eAaPmAgNnS
další	další	k2eAgNnSc1d1
ideově	ideově	k6eAd1
jí	jíst	k5eAaImIp3nS
blízké	blízký	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
-	-	kIx~
Odborové	odborový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
Čech	Čechy	k1gFnPc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SKOVAJSA	SKOVAJSA	kA
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občanský	občanský	k2eAgInSc1d1
sektor	sektor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7367	#num#	k4
<g/>
-	-	kIx~
<g/>
681	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Česká	český	k2eAgFnSc1d1
organizovaná	organizovaný	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
56	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Pražská	pražský	k2eAgFnSc1d1
informační	informační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památky	památka	k1gFnPc1
-	-	kIx~
Dům	dům	k1gInSc1
odborových	odborový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražská	pražský	k2eAgFnSc1d1
informační	informační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2008-01-22	2008-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
RYCHTÁŘ	rychtář	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
.	.	kIx.
www.digitalniknihovna.cz	www.digitalniknihovna.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
INFO@AION.CZ	INFO@AION.CZ	k1gMnSc1
<g/>
,	,	kIx,
AION	AION	kA
CS-	CS-	k1gFnSc1
<g/>
.	.	kIx.
144	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Zákon	zákon	k1gInSc1
o	o	k7c6
jednotné	jednotný	k2eAgFnSc6d1
odborové	odborový	k2eAgFnSc6d1
organisaci	organisace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákony	zákon	k1gInPc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
INFO@AION.CZ	INFO@AION.CZ	k1gMnSc1
<g/>
,	,	kIx,
AION	AION	kA
CS-	CS-	k1gFnSc1
<g/>
.	.	kIx.
37	#num#	k4
<g/>
/	/	kIx~
<g/>
1959	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Zákon	zákon	k1gInSc1
o	o	k7c4
postavení	postavení	k1gNnSc4
závodních	závodní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
základních	základní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
Revolučního	revoluční	k2eAgNnSc2d1
odborového	odborový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákony	zákon	k1gInPc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Revoluční	revoluční	k2eAgFnSc2d1
odborové	odborový	k2eAgFnSc2d1
hnutí	hnutí	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ROH	roh	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Počátky	počátek	k1gInPc1
odborů	odbor	k1gInPc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
nedatovaný	datovaný	k2eNgInSc1d1
text	text	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Českomoravského	českomoravský	k2eAgInSc2d1
odborového	odborový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
civilních	civilní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
armády	armáda	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2004209458	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1088278043	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0816	#num#	k4
6898	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80115493	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
130803214	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80115493	#num#	k4
</s>
