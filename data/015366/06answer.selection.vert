<s>
Revoluční	revoluční	k2eAgNnSc1d1
odborové	odborový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
ROH	ROH	kA
<g/>
;	;	kIx,
slovensky	slovensky	k6eAd1
Revolučné	Revolučný	k2eAgFnSc2d1
odborové	odborový	k2eAgFnSc2d1
hnutie	hnutie	k1gFnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
monopolní	monopolní	k2eAgFnSc1d1
odborová	odborový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
zároveň	zároveň	k6eAd1
nejmasovější	masový	k2eAgFnSc7d3
společenskou	společenský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
v	v	k7c6
socialistickém	socialistický	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>