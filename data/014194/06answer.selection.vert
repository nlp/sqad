<s>
Unikameralismus	Unikameralismus	k1gInSc1
neboli	neboli	k8xC
jednokomorovost	jednokomorovost	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
typ	typ	k1gInSc4
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
jen	jen	k6eAd1
jednou	jeden	k4xCgFnSc7
komorou	komora	k1gFnSc7
–	–	k?
přijetí	přijetí	k1gNnSc1
rozhodnutí	rozhodnutí	k1gNnPc2
<g/>
,	,	kIx,
respektive	respektive	k9
zákona	zákon	k1gInSc2
tedy	tedy	k9
není	být	k5eNaImIp3nS
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
druhé	druhý	k4xOgFnSc6
části	část	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
na	na	k7c4
další	další	k2eAgFnSc6d1
instanci	instance	k1gFnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
podpisu	podpis	k1gInSc6
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>