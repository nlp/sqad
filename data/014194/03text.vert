<s>
Unikameralismus	Unikameralismus	k1gInSc1
</s>
<s>
Státy	stát	k1gInPc1
s	s	k7c7
unikamerálním	unikamerální	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc4
s	s	k7c7
bikamerálním	bikamerální	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc7
bez	bez	k7c2
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Unikameralismus	Unikameralismus	k1gInSc1
neboli	neboli	k8xC
jednokomorovost	jednokomorovost	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
typ	typ	k1gInSc4
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
jen	jen	k6eAd1
jednou	jeden	k4xCgFnSc7
komorou	komora	k1gFnSc7
–	–	k?
přijetí	přijetí	k1gNnSc1
rozhodnutí	rozhodnutí	k1gNnPc2
<g/>
,	,	kIx,
respektive	respektive	k9
zákona	zákon	k1gInSc2
tedy	tedy	k9
není	být	k5eNaImIp3nS
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
druhé	druhý	k4xOgFnSc6
části	část	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
na	na	k7c4
další	další	k2eAgFnSc6d1
instanci	instance	k1gFnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
podpisu	podpis	k1gInSc6
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jistým	jistý	k2eAgInSc7d1
opakem	opak	k1gInSc7
je	být	k5eAaImIp3nS
bikameralismus	bikameralismus	k1gInSc1
(	(	kIx(
<g/>
dvoukomorovost	dvoukomorovost	k1gFnSc1
či	či	k8xC
dvoukomorový	dvoukomorový	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
často	často	k6eAd1
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
a	a	k8xC
senátu	senát	k1gInSc2
jakožto	jakožto	k8xS
kontrolního	kontrolní	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
a	a	k8xC
pojistky	pojistka	k1gFnSc2
kvality	kvalita	k1gFnSc2
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
členění	členění	k1gNnSc1
však	však	k9
není	být	k5eNaImIp3nS
platné	platný	k2eAgNnSc1d1
zdaleka	zdaleka	k6eAd1
všude	všude	k6eAd1
a	a	k8xC
je	být	k5eAaImIp3nS
závislé	závislý	k2eAgNnSc1d1
především	především	k9
na	na	k7c6
politické	politický	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
a	a	k8xC
tradici	tradice	k1gFnSc6
dané	daný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednokomorový	jednokomorový	k2eAgInSc4d1
parlament	parlament	k1gInSc4
mají	mít	k5eAaImIp3nP
například	například	k6eAd1
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
<g/>
,	,	kIx,
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
a	a	k8xC
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc1
unikameralismu	unikameralismus	k1gInSc2
</s>
<s>
Pojem	pojem	k1gInSc1
unikameralismus	unikameralismus	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
latinských	latinský	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
uni	uni	k?
(	(	kIx(
<g/>
jedna	jeden	k4xCgFnSc1
<g/>
)	)	kIx)
a	a	k8xC
camera	camera	k1gFnSc1
(	(	kIx(
<g/>
komora	komora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
označuje	označovat	k5eAaImIp3nS
typ	typ	k1gInSc1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
jedné	jeden	k4xCgFnSc2
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tak	tak	k6eAd1
drží	držet	k5eAaImIp3nS
většinu	většina	k1gFnSc4
zákonodárné	zákonodárný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
s	s	k7c7
pouze	pouze	k6eAd1
slabými	slabý	k2eAgNnPc7d1
omezeními	omezení	k1gNnPc7
(	(	kIx(
<g/>
tím	ten	k3xDgNnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
např.	např.	kA
právo	právo	k1gNnSc4
veta	veto	k1gNnSc2
hlavy	hlava	k1gFnSc2
státu	stát	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určitým	určitý	k2eAgInSc7d1
protikladem	protiklad	k1gInSc7
unikameralismu	unikameralismus	k1gInSc2
je	být	k5eAaImIp3nS
bikameralismus	bikameralismus	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
parlamentní	parlamentní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
naopak	naopak	k6eAd1
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
dvě	dva	k4xCgFnPc4
parlamentní	parlamentní	k2eAgFnPc4d1
komory	komora	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
koncept	koncept	k1gInSc4
politického	politický	k2eAgInSc2d1
systému	systém	k1gInSc2
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
unitárních	unitární	k2eAgFnPc6d1
a	a	k8xC
zároveň	zároveň	k6eAd1
kulturně	kulturně	k6eAd1
homogenních	homogenní	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
z	z	k7c2
pohledu	pohled	k1gInSc2
řízení	řízení	k1gNnSc2
země	zem	k1gFnSc2
neexistuje	existovat	k5eNaImIp3nS
či	či	k8xC
není	být	k5eNaImIp3nS
brána	brát	k5eAaImNgFnS
v	v	k7c4
potaz	potaz	k1gInSc4
silná	silný	k2eAgFnSc1d1
potřeba	potřeba	k1gFnSc1
zastoupení	zastoupení	k1gNnSc2
většího	veliký	k2eAgInSc2d2
počtu	počet	k1gInSc2
zájmových	zájmový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
například	například	k6eAd1
v	v	k7c6
heterogenních	heterogenní	k2eAgInPc6d1
státech	stát	k1gInPc6
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
populací	populace	k1gFnSc7
či	či	k8xC
státech	stát	k1gInPc6
s	s	k7c7
velkým	velký	k2eAgInSc7d1
počtem	počet	k1gInSc7
menšinových	menšinový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
argumentu	argument	k1gInSc3
ale	ale	k8xC
odporuje	odporovat	k5eAaImIp3nS
např.	např.	kA
fungování	fungování	k1gNnSc1
unikamerálního	unikamerální	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
jako	jako	k8xC,k8xS
státě	stát	k1gInSc6
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
ojediněle	ojediněle	k6eAd1
je	být	k5eAaImIp3nS
unikameralismus	unikameralismus	k1gInSc1
viděn	vidět	k5eAaImNgInS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
federalizovanými	federalizovaný	k2eAgInPc7d1
či	či	k8xC
regionálními	regionální	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
vzniká	vznikat	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
zastoupení	zastoupení	k1gNnSc2
skupin	skupina	k1gFnPc2
ze	z	k7c2
všech	všecek	k3xTgFnPc2
oblastí	oblast	k1gFnPc2
dané	daný	k2eAgFnSc2d1
země	zem	k1gFnSc2
v	v	k7c6
centrálním	centrální	k2eAgInSc6d1
parlamentním	parlamentní	k2eAgInSc6d1
orgánu	orgán	k1gInSc6
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
se	se	k3xPyFc4
v	v	k7c6
bikamerálním	bikamerální	k2eAgInSc6d1
systému	systém	k1gInSc6
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
právě	právě	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
komor	komora	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
Senát	senát	k1gInSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
zastupující	zastupující	k2eAgInPc4d1
jednotlivé	jednotlivý	k2eAgInPc4d1
departementy	departement	k1gInPc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
unikameralismu	unikameralismus	k1gInSc2
ve	v	k7c6
federalizovaném	federalizovaný	k2eAgInSc6d1
státu	stát	k1gInSc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
např.	např.	kA
Federativní	federativní	k2eAgInPc1d1
státy	stát	k1gInPc1
Mikronésie	Mikronésie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
těchto	tento	k3xDgMnPc2
faktorů	faktor	k1gInPc2
je	být	k5eAaImIp3nS
typ	typ	k1gInSc1
parlamentního	parlamentní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
ovlivněn	ovlivnit	k5eAaPmNgInS
také	také	k6eAd1
historickým	historický	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
daných	daný	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
historickými	historický	k2eAgFnPc7d1
okolnostmi	okolnost	k1gFnPc7
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
formování	formování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
systém	systém	k1gInSc1
jednokomorového	jednokomorový	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
vyskytuje	vyskytovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
v	v	k7c4
její	její	k3xOp3gFnSc4
severní	severní	k2eAgFnSc4d1
(	(	kIx(
<g/>
např.	např.	kA
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
východní	východní	k2eAgFnSc2d1
(	(	kIx(
<g/>
např.	např.	kA
Ukrajina	Ukrajina	k1gFnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
části	část	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
unikameralismus	unikameralismus	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
a	a	k8xC
Lucembursku	Lucembursko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
unikameralismus	unikameralismus	k1gInSc4
na	na	k7c6
národní	národní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
užíván	užívat	k5eAaImNgInS
v	v	k7c6
68	#num#	k4
%	%	kIx~
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
globálně	globálně	k6eAd1
pak	pak	k6eAd1
v	v	k7c6
cca	cca	kA
60	#num#	k4
%	%	kIx~
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výhody	výhoda	k1gFnPc1
a	a	k8xC
nevýhody	nevýhoda	k1gFnPc1
unikameralismu	unikameralismus	k1gInSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
bikameralismem	bikameralismus	k1gInSc7
</s>
<s>
Otázka	otázka	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
<g/>
,	,	kIx,
zda	zda	k8xS
a	a	k8xC
za	za	k7c2
jakých	jaký	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
podmínek	podmínka	k1gFnPc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
stát	stát	k5eAaPmF,k5eAaImF
výhodnější	výhodný	k2eAgInSc4d2
unikameralismus	unikameralismus	k1gInSc4
než	než	k8xS
bikameralismus	bikameralismus	k1gInSc4
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
jednoznačně	jednoznačně	k6eAd1
zodpovězena	zodpovědět	k5eAaPmNgFnS
a	a	k8xC
vedou	vést	k5eAaImIp3nP
se	se	k3xPyFc4
kolem	kolem	k7c2
ní	on	k3xPp3gFnSc2
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
debaty	debata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významněji	významně	k6eAd2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
zabývali	zabývat	k5eAaImAgMnP
například	například	k6eAd1
J.	J.	kA
S.	S.	kA
Mill	Mill	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
Loewenstein	Loewenstein	k1gMnSc1
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
Blondel	blondel	k1gInSc1
či	či	k8xC
Arend	arenda	k1gFnPc2
Lijphart	Lijpharta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
argumentů	argument	k1gInPc2
pro	pro	k7c4
jednokomorový	jednokomorový	k2eAgInSc4d1
systém	systém	k1gInSc4
používají	používat	k5eAaImIp3nP
jeho	jeho	k3xOp3gMnPc1
zastánci	zastánce	k1gMnPc1
argument	argument	k1gInSc4
nedělitelnosti	nedělitelnost	k1gFnPc1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jedna	jeden	k4xCgFnSc1
komora	komora	k1gFnSc1
parlamentu	parlament	k1gInSc2
zastupuje	zastupovat	k5eAaImIp3nS
národ	národ	k1gInSc4
jako	jako	k8xC,k8xS
celek	celek	k1gInSc4
obývající	obývající	k2eAgInSc4d1
danou	daný	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
příslušnost	příslušnost	k1gFnSc4
k	k	k7c3
danému	daný	k2eAgInSc3d1
státu	stát	k1gInSc3
je	být	k5eAaImIp3nS
důležitější	důležitý	k2eAgInSc4d2
než	než	k8xS
existující	existující	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
občany	občan	k1gMnPc7
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národ	národ	k1gInSc1
je	být	k5eAaImIp3nS
viděn	vidět	k5eAaImNgInS
jako	jako	k8xS,k8xC
jednotná	jednotný	k2eAgFnSc1d1
entita	entita	k1gFnSc1
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
tedy	tedy	k9
smysl	smysl	k1gInSc4
rozdělovat	rozdělovat	k5eAaImF
jeho	jeho	k3xOp3gFnSc4
vůli	vůle	k1gFnSc4
na	na	k7c4
komory	komora	k1gFnPc4
dvě	dva	k4xCgFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Jana	Jan	k1gMnSc2
Kysely	Kysela	k1gMnSc2
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
porozumění	porozumění	k1gNnSc1
nedělitelnosti	nedělitelnost	k1gFnSc2
národa	národ	k1gInSc2
navazováním	navazování	k1gNnSc7
na	na	k7c4
Rousseauovu	Rousseauův	k2eAgFnSc4d1
ideu	idea	k1gFnSc4
obecné	obecný	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
nemožnost	nemožnost	k1gFnSc4
dělitelnosti	dělitelnost	k1gFnSc2
vůle	vůle	k1gFnSc2
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
totiž	totiž	k9
vždy	vždy	k6eAd1
jednotná	jednotný	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgMnSc1d1
politolog	politolog	k1gMnSc1
Jean	Jean	k1gMnSc1
Cluzel	Cluzel	k1gMnSc1
označuje	označovat	k5eAaImIp3nS
toto	tento	k3xDgNnSc4
pojetí	pojetí	k1gNnSc4
demokracie	demokracie	k1gFnSc2
za	za	k7c4
demokracii	demokracie	k1gFnSc4
absolutistickou	absolutistický	k2eAgFnSc4d1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednokomorový	jednokomorový	k2eAgInSc1d1
parlament	parlament	k1gInSc1
je	být	k5eAaImIp3nS
díky	díky	k7c3
větší	veliký	k2eAgFnSc3d2
transparentnosti	transparentnost	k1gFnSc3
také	také	k9
více	hodně	k6eAd2
přímo	přímo	k6eAd1
občanům	občan	k1gMnPc3
odpovědný	odpovědný	k2eAgMnSc1d1
za	za	k7c4
svá	svůj	k3xOyFgNnPc4
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
,	,	kIx,
tato	tento	k3xDgNnPc4
rozhodnutí	rozhodnutí	k1gNnPc4
díky	díky	k7c3
absenci	absence	k1gFnSc3
dalšího	další	k2eAgNnSc2d1
projednávání	projednávání	k1gNnSc2
druhou	druhý	k4xOgFnSc7
komorou	komora	k1gFnSc7
také	také	k6eAd1
činí	činit	k5eAaImIp3nS
rychleji	rychle	k6eAd2
a	a	k8xC
schválené	schválený	k2eAgInPc1d1
zákony	zákon	k1gInPc1
tak	tak	k6eAd1
účinněji	účinně	k6eAd2
vstupují	vstupovat	k5eAaImIp3nP
v	v	k7c4
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
postupem	postup	k1gInSc7
také	také	k9
souvisí	souviset	k5eAaImIp3nS
předpokládaná	předpokládaný	k2eAgFnSc1d1
větší	veliký	k2eAgFnSc1d2
důvěra	důvěra	k1gFnSc1
voličů	volič	k1gMnPc2
ve	v	k7c4
fungování	fungování	k1gNnSc4
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
výsledky	výsledek	k1gInPc4
častěji	často	k6eAd2
a	a	k8xC
rychleji	rychle	k6eAd2
vidí	vidět	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Naopak	naopak	k6eAd1
nejvýraznějším	výrazný	k2eAgInSc7d3
argumentem	argument	k1gInSc7
zastánců	zastánce	k1gMnPc2
bikamerálního	bikamerální	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
kontroly	kontrola	k1gFnSc2
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jedna	jeden	k4xCgFnSc1
komora	komora	k1gFnSc1
má	mít	k5eAaImIp3nS
pravomoc	pravomoc	k1gFnSc4
účinně	účinně	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
činnost	činnost	k1gFnSc4
a	a	k8xC
rozhodování	rozhodování	k1gNnSc1
komory	komora	k1gFnSc2
druhé	druhý	k4xOgFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
pojistka	pojistka	k1gFnSc1
proti	proti	k7c3
zneužití	zneužití	k1gNnSc3
svěřené	svěřený	k2eAgFnSc2d1
zákonodárné	zákonodárný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
souvisí	souviset	k5eAaImIp3nS
kontrola	kontrola	k1gFnSc1
navrhovaných	navrhovaný	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
větším	veliký	k2eAgInSc7d2
počtem	počet	k1gInSc7
poslanců	poslanec	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
účinnější	účinný	k2eAgFnSc4d2
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
pravděpodobnost	pravděpodobnost	k1gFnSc4
zastavení	zastavení	k1gNnSc4
přijetí	přijetí	k1gNnSc2
nedokonalých	dokonalý	k2eNgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
komora	komora	k1gFnSc1
parlamentu	parlament	k1gInSc2
zde	zde	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
institucionální	institucionální	k2eAgFnSc1d1
pojistka	pojistka	k1gFnSc1
proti	proti	k7c3
zneužití	zneužití	k1gNnSc3
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
argument	argument	k1gInSc1
prosazuje	prosazovat	k5eAaImIp3nS
například	například	k6eAd1
slovenský	slovenský	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Lipšic	Lipšic	k1gMnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorový	dvoukomorový	k2eAgInSc1d1
systém	systém	k1gInSc1
má	mít	k5eAaImIp3nS
také	také	k9
větší	veliký	k2eAgFnSc4d2
možnost	možnost	k1gFnSc4
pokrýt	pokrýt	k5eAaPmF
zájmy	zájem	k1gInPc4
více	hodně	k6eAd2
rozdílných	rozdílný	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
uspořádání	uspořádání	k1gNnSc6
se	se	k3xPyFc4
také	také	k9
díky	díky	k7c3
rozdílným	rozdílný	k2eAgInPc3d1
termínům	termín	k1gInPc3
voleb	volba	k1gFnPc2
do	do	k7c2
obou	dva	k4xCgFnPc2
komor	komora	k1gFnPc2
snižuje	snižovat	k5eAaImIp3nS
riziko	riziko	k1gNnSc1
ovládnutí	ovládnutí	k1gNnSc2
parlamentu	parlament	k1gInSc2
jednou	jeden	k4xCgFnSc7
stranou	stranou	k6eAd1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
daném	daný	k2eAgNnSc6d1
období	období	k1gNnSc6
zažívá	zažívat	k5eAaImIp3nS
výrazný	výrazný	k2eAgInSc1d1
nárůst	nárůst	k1gInSc1
v	v	k7c6
preferencích	preference	k1gFnPc6
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
dále	daleko	k6eAd2
posilovalo	posilovat	k5eAaImAgNnS
riziko	riziko	k1gNnSc1
zneužití	zneužití	k1gNnSc1
moci	moc	k1gFnSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Daniela	Daniel	k1gMnSc2
Lipšice	Lipšic	k1gMnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
možná	možný	k2eAgFnSc1d1
účinnější	účinný	k2eAgFnSc1d2
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
mezi	mezi	k7c7
komorami	komora	k1gFnPc7
a	a	k8xC
větší	veliký	k2eAgFnSc4d2
odolnost	odolnost	k1gFnSc4
vůči	vůči	k7c3
tlakům	tlak	k1gInPc3
lobbistů	lobbista	k1gMnPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
unikamerálních	unikamerální	k2eAgFnPc2d1
legislatur	legislatura	k1gFnPc2
</s>
<s>
Konkrétní	konkrétní	k2eAgNnSc1d1
fungování	fungování	k1gNnSc1
unikamerálních	unikamerální	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
ukázáno	ukázat	k5eAaPmNgNnS
na	na	k7c6
příkladech	příklad	k1gInPc6
Slovenska	Slovensko	k1gNnSc2
jako	jako	k8xS,k8xC
zástupce	zástupce	k1gMnSc1
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Finska	Finsko	k1gNnSc2
jako	jako	k8xS,k8xC
představitele	představitel	k1gMnSc2
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Surinamu	Surinam	k1gInSc2
jako	jako	k8xC,k8xS
příkladu	příklad	k1gInSc2
jednokomorového	jednokomorový	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
mimo	mimo	k7c4
území	území	k1gNnSc4
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
funguje	fungovat	k5eAaImIp3nS
unikamerální	unikamerální	k2eAgInSc1d1
systém	systém	k1gInSc1
již	již	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
existence	existence	k1gFnSc2
slovenského	slovenský	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
150	#num#	k4
poslanců	poslanec	k1gMnPc2
volených	volená	k1gFnPc2
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
systémem	systém	k1gInSc7
poměrného	poměrný	k2eAgNnSc2d1
zastoupení	zastoupení	k1gNnSc2
ve	v	k7c6
všeobecných	všeobecný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanci	poslanec	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
stát	stát	k5eAaPmF,k5eAaImF
občané	občan	k1gMnPc1
Slovenska	Slovensko	k1gNnSc2
starší	starý	k2eAgFnSc1d2
21	#num#	k4
let	léto	k1gNnPc2
způsobilí	způsobilý	k2eAgMnPc1d1
k	k	k7c3
právním	právní	k2eAgInPc3d1
úkonům	úkon	k1gInPc3
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
projednávání	projednávání	k1gNnSc2
a	a	k8xC
schvalování	schvalování	k1gNnSc2
zákonů	zákon	k1gInPc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
pravomoci	pravomoc	k1gFnPc4
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
schvalování	schvalování	k1gNnSc2
mezinárodních	mezinárodní	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
<g/>
,	,	kIx,
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
či	či	k8xC
vyhlášení	vyhlášení	k1gNnSc2
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonodárnou	zákonodárný	k2eAgFnSc4d1
iniciativu	iniciativa	k1gFnSc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
mají	mít	k5eAaImIp3nP
výbory	výbor	k1gInPc1
<g/>
,	,	kIx,
vláda	vláda	k1gFnSc1
a	a	k8xC
poslanci	poslanec	k1gMnPc1
<g/>
,	,	kIx,
o	o	k7c6
zákonu	zákon	k1gInSc6
se	se	k3xPyFc4
debatuje	debatovat	k5eAaImIp3nS
ve	v	k7c6
třech	tři	k4xCgNnPc6
čteních	čtení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
zákona	zákon	k1gInSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
souhlasu	souhlas	k1gInSc2
většiny	většina	k1gFnSc2
poslanců	poslanec	k1gMnPc2
<g/>
,	,	kIx,
po	po	k7c6
přijetí	přijetí	k1gNnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ale	ale	k8xC
parlamentu	parlament	k1gInSc3
navrácen	navrácen	k2eAgMnSc1d1
prezidentem	prezident	k1gMnSc7
republiky	republika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
o	o	k7c6
zákonu	zákon	k1gInSc6
hlasuje	hlasovat	k5eAaImIp3nS
znovu	znovu	k6eAd1
<g/>
,	,	kIx,
k	k	k7c3
jeho	jeho	k3xOp3gMnPc3
přijetí	přijetí	k1gNnSc4
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
souhlasu	souhlas	k1gInSc2
nadpoloviční	nadpoloviční	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
všech	všecek	k3xTgMnPc2
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
je	být	k5eAaImIp3nS
parlamentu	parlament	k1gInSc2
odpovědná	odpovědný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
může	moct	k5eAaImIp3nS
jí	on	k3xPp3gFnSc7
či	či	k8xC
jednotlivým	jednotlivý	k2eAgMnPc3d1
poslancům	poslanec	k1gMnPc3
vyslovit	vyslovit	k5eAaPmF
nedůvěru	nedůvěra	k1gFnSc4
<g/>
,	,	kIx,
schvaluje	schvalovat	k5eAaImIp3nS
státní	státní	k2eAgInSc4d1
rozpočet	rozpočet	k1gInSc4
a	a	k8xC
programové	programový	k2eAgNnSc4d1
prohlášení	prohlášení	k1gNnSc4
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Jednokomorový	jednokomorový	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
Finsku	Finsko	k1gNnSc6
nazývaný	nazývaný	k2eAgMnSc1d1
Eduskunta	Eduskunta	k1gMnSc1
<g/>
,	,	kIx,
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
200	#num#	k4
poslanců	poslanec	k1gMnPc2
volených	volená	k1gFnPc2
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
proporcionálním	proporcionální	k2eAgInSc7d1
systémem	systém	k1gInSc7
ve	v	k7c6
všeobecných	všeobecný	k2eAgFnPc6d1
<g/>
,	,	kIx,
rovných	rovný	k2eAgFnPc6d1
a	a	k8xC
tajných	tajný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivní	aktivní	k2eAgInSc4d1
i	i	k8xC
pasivní	pasivní	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
mají	mít	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
finští	finský	k2eAgMnPc1d1
občané	občan	k1gMnPc1
nad	nad	k7c4
18	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
s	s	k7c7
několika	několik	k4yIc7
výjimkami	výjimka	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
např.	např.	kA
nesvéprávné	svéprávný	k2eNgFnPc4d1
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
do	do	k7c2
parlamentu	parlament	k1gInSc2
také	také	k9
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
voleni	volen	k2eAgMnPc1d1
občané	občan	k1gMnPc1
v	v	k7c6
aktivní	aktivní	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
službě	služba	k1gFnSc6
či	či	k8xC
na	na	k7c6
jiných	jiný	k2eAgFnPc6d1
vysokých	vysoký	k2eAgFnPc6d1
pozicích	pozice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finská	finský	k2eAgFnSc1d1
Eduskunta	Eduskunta	k1gFnSc1
má	mít	k5eAaImIp3nS
nad	nad	k7c7
vládou	vláda	k1gFnSc7
silnou	silný	k2eAgFnSc4d1
kontrolní	kontrolní	k2eAgFnSc4d1
pravomoc	pravomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
souhlas	souhlas	k1gInSc4
pouhých	pouhý	k2eAgInPc2d1
10	#num#	k4
%	%	kIx~
z	z	k7c2
celkových	celkový	k2eAgInPc2d1
200	#num#	k4
poslanců	poslanec	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
vláda	vláda	k1gFnSc1
interpelována	interpelovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
spíše	spíše	k9
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
oslabení	oslabení	k1gNnSc3
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
koncentraci	koncentrace	k1gFnSc3
moci	moc	k1gFnSc2
ve	v	k7c6
stranách	strana	k1gFnPc6
a	a	k8xC
charizmatických	charizmatický	k2eAgMnPc6d1
leaderech	leader	k1gMnPc6
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Surinam	Surinam	k6eAd1
</s>
<s>
Surinam	Surinam	k6eAd1
je	být	k5eAaImIp3nS
země	země	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
1667	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1954	#num#	k4
pod	pod	k7c7
koloniální	koloniální	k2eAgFnSc7d1
správou	správa	k1gFnSc7
Nizozemí	Nizozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
Ústava	ústava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zavedla	zavést	k5eAaPmAgFnS
unikamerální	unikamerální	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
výkonné	výkonný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
stojí	stát	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc4d1
shromáždění	shromáždění	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
51	#num#	k4
křesel	křeslo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
má	mít	k5eAaImIp3nS
proporční	proporční	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
10	#num#	k4
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
oblasti	oblast	k1gFnPc1
jsou	být	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
co	co	k8xS
se	se	k3xPyFc4
velikosti	velikost	k1gFnSc2
regionu	region	k1gInSc2
<g/>
,	,	kIx,
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
velikosti	velikost	k1gFnSc2
elektorátu	elektorát	k1gInSc2
týče	týkat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
také	také	k9
znevýhodňuje	znevýhodňovat	k5eAaImIp3nS
menší	malý	k2eAgInPc4d2
regiony	region	k1gInPc4
oproti	oproti	k7c3
těm	ten	k3xDgNnPc3
větším	veliký	k2eAgNnPc3d2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Současný	současný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
od	od	k7c2
konce	konec	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
pozorovat	pozorovat	k5eAaImF
pozvolný	pozvolný	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
počtu	počet	k1gInSc2
unikamerálních	unikamerální	k2eAgNnPc2d1
uspořádání	uspořádání	k1gNnPc2
na	na	k7c4
úkor	úkor	k1gInSc4
systémů	systém	k1gInPc2
bikamerálních	bikamerální	k2eAgInPc2d1
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
obou	dva	k4xCgInPc2
typů	typ	k1gInPc2
parlamentů	parlament	k1gInPc2
ve	v	k7c6
světovém	světový	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
vyrovnaný	vyrovnaný	k2eAgInSc4d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
poměrem	poměr	k1gInSc7
6	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
převládá	převládat	k5eAaImIp3nS
systém	systém	k1gInSc4
jednokomorový	jednokomorový	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc4
směrem	směr	k1gInSc7
od	od	k7c2
bikameralismu	bikameralismus	k1gInSc2
ale	ale	k8xC
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jednoznačný	jednoznačný	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
zhruba	zhruba	k6eAd1
v	v	k7c6
polovině	polovina	k1gFnSc6
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
,	,	kIx,
činil	činit	k5eAaImAgInS
podíl	podíl	k1gInSc1
unikamerálních	unikamerální	k2eAgFnPc2d1
legislatur	legislatura	k1gFnPc2
více	hodně	k6eAd2
než	než	k8xS
65	#num#	k4
%	%	kIx~
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
opětovně	opětovně	k6eAd1
snižoval	snižovat	k5eAaImAgInS
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
změnily	změnit	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
systém	systém	k1gInSc4
z	z	k7c2
dvoukomorového	dvoukomorový	k2eAgMnSc2d1
na	na	k7c4
jednokomorový	jednokomorový	k2eAgInSc4d1
můžeme	moct	k5eAaImIp1nP
nalézt	nalézt	k5eAaPmF,k5eAaBmF
Island	Island	k1gInSc4
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Středoafrickou	středoafrický	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
či	či	k8xC
Chorvatsko	Chorvatsko	k1gNnSc4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
pěti	pět	k4xCc3
státům	stát	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
se	se	k3xPyFc4
vzdaly	vzdát	k5eAaPmAgFnP
bikamerálního	bikamerální	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
uspořádání	uspořádání	k1gNnSc2
unikamerálního	unikamerální	k2eAgNnSc2d1
<g/>
,	,	kIx,
jich	on	k3xPp3gNnPc2
ale	ale	k9
dvanáct	dvanáct	k4xCc1
provedlo	provést	k5eAaPmAgNnS
obrat	obrat	k5eAaPmF
opačným	opačný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
zemí	zem	k1gFnPc2
s	s	k7c7
unikamerálním	unikamerální	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
</s>
<s>
Tabulka	tabulka	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
článku	článek	k1gInSc2
Unicameralism	Unicameralisma	k1gFnPc2
versus	versus	k7c1
Bicameralism	Bicameralism	k1gInSc4
Revisited	Revisited	k1gMnSc1
The	The	k1gMnSc2
Case	Case	k1gNnSc2
of	of	k?
Romania	Romanium	k1gNnSc2
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
stav	stav	k1gInSc1
k	k	k7c3
roku	rok	k1gInSc3
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
AFRIKA	Afrika	k1gFnSc1
</s>
<s>
AMERIKA	Amerika	k1gFnSc1
</s>
<s>
ASIE	Asie	k1gFnSc1
</s>
<s>
EVROPA	Evropa	k1gFnSc1
</s>
<s>
OCEANIE	OCEANIE	kA
</s>
<s>
Unikameralismus	Unikameralismus	k1gInSc1
</s>
<s>
Bikameralismus	Bikameralismus	k1gInSc1
</s>
<s>
Unikameralismus	Unikameralismus	k1gInSc1
</s>
<s>
Bikameralismus	Bikameralismus	k1gInSc1
</s>
<s>
Unikameralismus	Unikameralismus	k1gInSc1
</s>
<s>
Bikameralismus	Bikameralismus	k1gInSc1
</s>
<s>
Unikameralismus	Unikameralismus	k1gInSc1
</s>
<s>
Bikameralismus	Bikameralismus	k1gInSc1
</s>
<s>
Unikameralismus	Unikameralismus	k1gInSc1
</s>
<s>
Bikameralismus	Bikameralismus	k1gInSc1
</s>
<s>
Angola	Angola	k1gFnSc1
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
</s>
<s>
Anguilla	Anguilla	k6eAd1
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Guam	Guam	k6eAd1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
</s>
<s>
Benin	Benin	k1gMnSc1
</s>
<s>
Botswana	Botswana	k1gFnSc1
</s>
<s>
Aruba	Aruba	k1gFnSc1
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Kiribati	Kiribat	k5eAaImF,k5eAaPmF
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
</s>
<s>
Burundi	Burundi	k1gNnSc1
</s>
<s>
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Barbados	Barbados	k1gMnSc1
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
Bhútán	Bhútán	k1gInSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Fidži	Fidzat	k5eAaPmIp1nS
</s>
<s>
Čad	Čad	k1gInSc1
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
</s>
<s>
Kostarika	Kostarika	k1gFnSc1
</s>
<s>
Belize	Belize	k6eAd1
</s>
<s>
Irák	Irák	k1gInSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Mikronésie	Mikronésie	k1gFnSc1
</s>
<s>
Palau	Palau	k6eAd1
</s>
<s>
Džibutsko	Džibutsko	k6eAd1
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Kuba	Kuba	k1gFnSc1
</s>
<s>
Bermudy	Bermudy	k1gFnPc1
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Nauru	Naura	k1gFnSc4
</s>
<s>
Eritrea	Eritrea	k1gFnSc1
</s>
<s>
Etiopie	Etiopie	k1gFnSc1
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1
</s>
<s>
Bolívie	Bolívie	k1gFnSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Gambie	Gambie	k1gFnSc1
</s>
<s>
Gabon	Gabon	k1gMnSc1
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
</s>
<s>
Ghana	Ghana	k1gFnSc1
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Salvador	Salvador	k1gMnSc1
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Katar	katar	k1gMnSc1
</s>
<s>
Jemen	Jemen	k1gInSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Samoa	Samoa	k1gFnSc1
</s>
<s>
Guinea	Guinea	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
</s>
<s>
Grenada	Grenada	k1gFnSc1
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Guinea-Bissau	Guinea-Bissau	k6eAd1
</s>
<s>
Keňa	Keňa	k1gFnSc1
</s>
<s>
Guyana	Guyana	k1gFnSc1
</s>
<s>
Haiti	Haiti	k1gNnSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Tonga	Tonga	k1gFnSc1
</s>
<s>
Kamerun	Kamerun	k1gInSc1
</s>
<s>
Kongo	Kongo	k1gNnSc1
</s>
<s>
Honduras	Honduras	k1gInSc1
</s>
<s>
Chile	Chile	k1gNnSc1
</s>
<s>
Laos	Laos	k1gInSc1
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4
</s>
<s>
Kapverdy	Kapverdy	k6eAd1
</s>
<s>
Lesotho	Lesotze	k6eAd1
</s>
<s>
Nikaragua	Nikaragua	k1gFnSc1
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
</s>
<s>
Libanon	Libanon	k1gInSc1
</s>
<s>
Omán	Omán	k1gInSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Vanuatu	Vanuata	k1gFnSc4
</s>
<s>
Komory	komora	k1gFnPc1
</s>
<s>
Libérie	Libérie	k1gFnSc1
</s>
<s>
Panama	Panama	k1gFnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
Pákistán	Pákistán	k1gInSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Libye	Libye	k1gFnSc1
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1
</s>
<s>
Peru	Peru	k1gNnSc1
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Malawi	Malawi	k1gNnSc1
</s>
<s>
Maroko	Maroko	k1gNnSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Nepál	Nepál	k1gInSc1
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Mali	Mali	k1gNnSc1
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1
</s>
<s>
Surinam	Surinam	k6eAd1
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Mauricius	Mauricius	k1gMnSc1
</s>
<s>
Namibie	Namibie	k1gFnSc1
</s>
<s>
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Mosambik	Mosambik	k1gInSc1
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Singapur	Singapur	k1gInSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Niger	Niger	k1gInSc1
</s>
<s>
Rwanda	Rwanda	k1gFnSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
</s>
<s>
Somálsko	Somálsko	k1gNnSc1
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
</s>
<s>
Srí	Srí	k?
Lanka	lanko	k1gNnPc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Súdán	Súdán	k1gInSc1
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
</s>
<s>
Svazijsko	Svazijsko	k1gNnSc1
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Senegal	Senegal	k1gInSc1
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
Timor	Timor	k1gMnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Tanzanie	Tanzanie	k1gFnSc1
</s>
<s>
Vietnam	Vietnam	k1gInSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Togo	Togo	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Uganda	Uganda	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Zambie	Zambie	k1gFnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
References	References	k1gMnSc1
</s>
<s>
↑	↑	k?
Unicameralism	Unicameralism	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dictionary	Dictionara	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6.1	6.1	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.dictionary.com/browse/unicameralism	http://www.dictionary.com/browse/unicameralism	k1gMnSc1
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Klokočka	Klokočka	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústavní	ústavní	k2eAgInPc1d1
systémy	systém	k1gInPc1
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
334	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Linde	Lind	k1gMnSc5
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7201	#num#	k4
<g/>
-	-	kIx~
<g/>
606	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ústava	ústava	k1gFnSc1
Francouzské	francouzský	k2eAgFnPc1d1
republiky	republika	k1gFnPc1
ze	z	k7c2
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Reflex	reflex	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
155	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
<g/>
↑	↑	k?
Baldwin	Baldwin	k1gMnSc1
<g/>
,	,	kIx,
Nicholas	Nicholas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legislatures	Legislatures	k1gMnSc1
of	of	k?
Small	Small	k1gMnSc1
States	States	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Comparative	Comparativ	k1gInSc5
Study	stud	k1gInPc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
415538335	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
APAHIDEANU	APAHIDEANU	kA
<g/>
,	,	kIx,
IONUŢ	IONUŢ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unicameralism	Unicameralism	k1gInSc1
versus	versus	k7c1
Bicameralism	Bicameralism	k1gInSc4
Revisited	Revisited	k1gMnSc1
The	The	k1gMnSc2
Case	Case	k1gNnSc2
of	of	k?
Romania	Romanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
Politica	Politicum	k1gNnSc2
<g/>
:	:	kIx,
Romanian	Romanian	k1gInSc1
Political	Political	k1gMnSc2
Science	Scienec	k1gMnSc2
Review	Review	k1gMnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
53	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
47-88	47-88	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
15824551	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
25	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
APAHIDEANU	APAHIDEANU	kA
<g/>
,	,	kIx,
IONUŢ	IONUŢ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unicameralism	Unicameralism	k1gInSc1
versus	versus	k7c1
Bicameralism	Bicameralism	k1gInSc4
Revisited	Revisited	k1gMnSc1
The	The	k1gMnSc2
Case	Case	k1gNnSc2
of	of	k?
Romania	Romanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
Politica	Politicum	k1gNnSc2
<g/>
:	:	kIx,
Romanian	Romanian	k1gInSc1
Political	Political	k1gMnSc2
Science	Science	k1gFnSc1
Review	Review	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
47-88	47-88	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
15824551	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kysela	Kysela	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoukomorové	dvoukomorový	k2eAgInPc1d1
systémy	systém	k1gInPc1
:	:	kIx,
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
srovnání	srovnání	k1gNnSc1
dvoukomorových	dvoukomorový	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8086432890	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ústava	ústava	k1gFnSc1
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Článek	článek	k1gInSc1
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poradca	Poradca	k?
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8089213359	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ústava	ústava	k1gFnSc1
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Článek	článek	k1gInSc1
87	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poradca	Poradca	k?
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8089213359	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ústava	ústava	k1gFnSc1
Slovenskej	Slovenskej	k?
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Článek	článek	k1gInSc1
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poradca	Poradca	k?
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8089213359	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnSc1
Constitution	Constitution	k1gInSc1
of	of	k?
Finland	Finland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Section	Section	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ministry	ministr	k1gMnPc4
of	of	k?
Justice	justice	k1gFnSc1
<g/>
,	,	kIx,
Finland	Finland	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://oikeusministerio.fi/en/index/basicprovisions/legislation/constitution.html	http://oikeusministerio.fi/en/index/basicprovisions/legislation/constitution.html	k1gInSc1
<g/>
↑	↑	k?
The	The	k1gFnSc1
Constitution	Constitution	k1gInSc1
of	of	k?
Finland	Finland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Section	Section	k1gInSc1
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ministry	ministr	k1gMnPc4
of	of	k?
Justice	justice	k1gFnSc1
<g/>
,	,	kIx,
Finland	Finland	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://oikeusministerio.fi/en/index/basicprovisions/legislation/constitution.html	http://oikeusministerio.fi/en/index/basicprovisions/legislation/constitution.html	k1gInSc1
<g/>
↑	↑	k?
RAUNIO	RAUNIO	kA
<g/>
,	,	kIx,
T	T	kA
a	a	k8xC
M	M	kA
WIBERG	WIBERG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Eduskunta	Eduskunta	k1gMnSc1
and	and	k?
the	the	k?
parliamentarisation	parliamentarisation	k1gInSc1
of	of	k?
Finnish	Finnish	k1gInSc1
politics	politics	k1gInSc1
<g/>
:	:	kIx,
Formally	Formalla	k1gFnPc1
stronger	strongero	k1gNnPc2
<g/>
,	,	kIx,
politically	politicalla	k1gFnPc1
still	still	k1gMnSc1
weak	weak	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
WEST	WEST	kA
EUROPEAN	EUROPEAN	kA
POLITICS	POLITICS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
581-599	581-599	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
1402382	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CURIEL	CURIEL	kA
<g/>
,	,	kIx,
Imma	Immum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
MULTIFACETED	MULTIFACETED	kA
ANALYSIS	ANALYSIS	kA
OF	OF	kA
THE	THE	kA
ELECTORAL	ELECTORAL	kA
SYSTEM	SYSTEM	kA
OF	OF	kA
THE	THE	kA
REPUBLIC	REPUBLIC	kA
OF	OF	kA
SURINAME	SURINAME	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operations	Operations	k1gInSc1
Research	Research	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
24	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
29-49	29-49	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
10.527	10.527	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
ord	orda	k1gFnPc2
<g/>
140403	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
20818858	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
APAHIDEANU	APAHIDEANU	kA
<g/>
,	,	kIx,
IONUŢ	IONUŢ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unicameralism	Unicameralism	k1gInSc1
versus	versus	k7c1
Bicameralism	Bicameralism	k1gInSc4
Revisited	Revisited	k1gMnSc1
The	The	k1gMnSc2
Case	Case	k1gNnSc2
of	of	k?
Romania	Romanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
Politica	Politicum	k1gNnSc2
<g/>
:	:	kIx,
Romanian	Romanian	k1gInSc1
Political	Political	k1gMnSc2
Science	Scienec	k1gMnSc2
Review	Review	k1gMnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
53	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
47-88	47-88	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
15824551	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CURIEL	CURIEL	kA
<g/>
,	,	kIx,
Imma	Immum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
MULTIFACETED	MULTIFACETED	kA
ANALYSIS	ANALYSIS	kA
OF	OF	kA
THE	THE	kA
ELECTORAL	ELECTORAL	kA
SYSTEM	SYSTEM	kA
OF	OF	kA
THE	THE	kA
REPUBLIC	REPUBLIC	kA
OF	OF	kA
SURINAME	SURINAME	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operations	Operations	k1gInSc1
Research	Research	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
24	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
29-49	29-49	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
10.527	10.527	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
ord	orda	k1gFnPc2
<g/>
140403	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
20818858	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
APAHIDEANU	APAHIDEANU	kA
<g/>
,	,	kIx,
IONUŢ	IONUŢ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unicameralism	Unicameralism	k1gInSc1
versus	versus	k7c1
Bicameralism	Bicameralism	k1gInSc4
Revisited	Revisited	k1gMnSc1
The	The	k1gMnSc2
Case	Case	k1gNnSc2
of	of	k?
Romania	Romanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
Politica	Politicum	k1gNnSc2
<g/>
:	:	kIx,
Romanian	Romanian	k1gInSc1
Political	Political	k1gMnSc2
Science	Science	k1gFnSc1
Review	Review	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
47-88	47-88	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
15824551	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
