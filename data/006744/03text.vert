<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
KLDR	KLDR	kA	KLDR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
v	v	k7c6	v
nejsevernějším	severní	k2eAgInSc6d3	nejsevernější
rohu	roh	k1gInSc6	roh
poloostrova	poloostrov	k1gInSc2	poloostrov
také	také	k9	také
11,87	[number]	k4	11,87
km	km	kA	km
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
vede	vést	k5eAaImIp3nS	vést
podle	podle	k7c2	podle
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
hranice	hranice	k1gFnPc1	hranice
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejmilitarizovanějších	militarizovaný	k2eAgNnPc2d3	militarizovaný
míst	místo	k1gNnPc2	místo
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
23	[number]	k4	23
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Pchjongjang	Pchjongjang	k1gInSc1	Pchjongjang
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc7d1	vládní
stranou	strana	k1gFnSc7	strana
je	být	k5eAaImIp3nS	být
Korejská	korejský	k2eAgFnSc1d1	Korejská
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc7d1	státní
ideologií	ideologie	k1gFnSc7	ideologie
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
čučche	čučche	k1gInSc1	čučche
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
má	mít	k5eAaImIp3nS	mít
třetí	třetí	k4xOgFnSc4	třetí
největší	veliký	k2eAgFnSc4d3	veliký
armádu	armáda	k1gFnSc4	armáda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1,3	[number]	k4	1,3
miliónu	milión	k4xCgInSc2	milión
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozvinutý	rozvinutý	k2eAgInSc4d1	rozvinutý
jaderný	jaderný	k2eAgInSc4d1	jaderný
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
hospodářství	hospodářství	k1gNnSc2	hospodářství
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
rozvoje	rozvoj	k1gInSc2	rozvoj
či	či	k8xC	či
potravinové	potravinový	k2eAgFnSc2d1	potravinová
soběstačnosti	soběstačnost	k1gFnSc2	soběstačnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
velmi	velmi	k6eAd1	velmi
zaostalá	zaostalý	k2eAgFnSc1d1	zaostalá
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Japonska	Japonsko	k1gNnSc2	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
Korea	Korea	k1gFnSc1	Korea
bez	bez	k7c2	bez
konzultace	konzultace	k1gFnSc2	konzultace
s	s	k7c7	s
korejskou	korejský	k2eAgFnSc7d1	Korejská
reprezentací	reprezentace	k1gFnSc7	reprezentace
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
vítěznými	vítězný	k2eAgInPc7d1	vítězný
státy	stát	k1gInPc7	stát
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc4d1	severní
<g/>
,	,	kIx,	,
ovládanou	ovládaný	k2eAgFnSc4d1	ovládaná
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
jižní	jižní	k2eAgFnSc7d1	jižní
<g/>
,	,	kIx,	,
ovládanou	ovládaný	k2eAgFnSc7d1	ovládaná
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
hranici	hranice	k1gFnSc4	hranice
tvořila	tvořit	k5eAaImAgFnS	tvořit
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
okupační	okupační	k2eAgFnPc1d1	okupační
správy	správa	k1gFnPc1	správa
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
dvou	dva	k4xCgInPc2	dva
ideologicky	ideologicky	k6eAd1	ideologicky
antagonistických	antagonistický	k2eAgInPc2d1	antagonistický
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Korejské	korejský	k2eAgFnSc2d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
USA	USA	kA	USA
i	i	k8xC	i
SSSR	SSSR	kA	SSSR
své	své	k1gNnSc4	své
jednotky	jednotka	k1gFnSc2	jednotka
z	z	k7c2	z
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
překročila	překročit	k5eAaPmAgNnP	překročit
vojska	vojsko	k1gNnPc4	vojsko
severokorejské	severokorejský	k2eAgFnSc2d1	severokorejská
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
(	(	kIx(	(
<g/>
demarkační	demarkační	k2eAgFnSc4d1	demarkační
linii	linie	k1gFnSc4	linie
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
sjednotit	sjednotit	k5eAaPmF	sjednotit
Koreu	Korea	k1gFnSc4	Korea
pod	pod	k7c7	pod
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
povolány	povolat	k5eAaPmNgFnP	povolat
jednotky	jednotka	k1gFnPc1	jednotka
OSN	OSN	kA	OSN
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
čelily	čelit	k5eAaImAgFnP	čelit
invazi	invaze	k1gFnSc4	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severokorejské	severokorejský	k2eAgFnSc6d1	severokorejská
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
do	do	k7c2	do
války	válka	k1gFnSc2	válka
později	pozdě	k6eAd2	pozdě
zapojila	zapojit	k5eAaPmAgFnS	zapojit
také	také	k9	také
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
několikrát	několikrát	k6eAd1	několikrát
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příměří	příměří	k1gNnSc3	příměří
mezi	mezi	k7c7	mezi
OSN	OSN	kA	OSN
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
KLDR	KLDR	kA	KLDR
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
na	na	k7c4	na
druhé	druhý	k4xOgMnPc4	druhý
došlo	dojít	k5eAaPmAgNnS	dojít
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
KLDR	KLDR	kA	KLDR
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
stále	stále	k6eAd1	stále
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
nebo	nebo	k8xC	nebo
pohřešováno	pohřešovat	k5eAaImNgNnS	pohřešovat
až	až	k6eAd1	až
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
demarkační	demarkační	k2eAgFnSc1d1	demarkační
linie	linie	k1gFnSc1	linie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstřeženější	střežený	k2eAgFnSc4d3	nejstřeženější
hranici	hranice	k1gFnSc4	hranice
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Demilitarizovanou	demilitarizovaný	k2eAgFnSc4d1	demilitarizovaná
zónu	zóna	k1gFnSc4	zóna
tvoří	tvořit	k5eAaImIp3nS	tvořit
250	[number]	k4	250
kilometrů	kilometr	k1gInPc2	kilometr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
4	[number]	k4	4
kilometry	kilometr	k1gInPc4	kilometr
široký	široký	k2eAgInSc4d1	široký
pás	pás	k1gInSc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
hranici	hranice	k1gFnSc4	hranice
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Pouhých	pouhý	k2eAgInPc2d1	pouhý
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
hranice	hranice	k1gFnSc2	hranice
leží	ležet	k5eAaImIp3nS	ležet
nádraží	nádraží	k1gNnSc1	nádraží
Dorasan	Dorasany	k1gInPc2	Dorasany
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
KLDR	KLDR	kA	KLDR
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příměřím	příměří	k1gNnSc7	příměří
již	již	k6eAd1	již
necítí	cítit	k5eNaImIp3nS	cítit
vázána	vázán	k2eAgFnSc1d1	vázána
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c4	na
jihokorejský	jihokorejský	k2eAgInSc4d1	jihokorejský
ostrov	ostrov	k1gInSc4	ostrov
Jon-pchjong	Jonchjonga	k1gFnPc2	Jon-pchjonga
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
neútočení	neútočení	k1gNnSc6	neútočení
byly	být	k5eAaImAgFnP	být
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
jednostranně	jednostranně	k6eAd1	jednostranně
vypovězeny	vypovědět	k5eAaPmNgFnP	vypovědět
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
uvalila	uvalit	k5eAaPmAgFnS	uvalit
na	na	k7c4	na
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
další	další	k2eAgFnSc2d1	další
sankce	sankce	k1gFnSc2	sankce
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
třetímu	třetí	k4xOgMnSc3	třetí
jadernému	jaderný	k2eAgInSc3d1	jaderný
testu	test	k1gInSc3	test
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
státně	státně	k6eAd1	státně
řízená	řízený	k2eAgFnSc1d1	řízená
ekonomika	ekonomika	k1gFnSc1	ekonomika
KLDR	KLDR	kA	KLDR
vysokého	vysoký	k2eAgInSc2d1	vysoký
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejindustrializovanější	industrializovaný	k2eAgFnSc4d3	industrializovaný
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
po	po	k7c6	po
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
růstu	růst	k1gInSc2	růst
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
k	k	k7c3	k
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
potížím	potíž	k1gFnPc3	potíž
přidaly	přidat	k5eAaPmAgInP	přidat
povodně	povodeň	k1gFnPc4	povodeň
a	a	k8xC	a
sucha	sucho	k1gNnPc4	sucho
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
severokorejskému	severokorejský	k2eAgInSc3d1	severokorejský
hladomoru	hladomor	k1gInSc3	hladomor
v	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nefunkčnost	Nefunkčnost	k1gFnSc1	Nefunkčnost
státního	státní	k2eAgInSc2d1	státní
sektoru	sektor	k1gInSc2	sektor
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
oficiálně	oficiálně	k6eAd1	oficiálně
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
cípu	cíp	k1gInSc6	cíp
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
;	;	kIx,	;
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
téměř	téměř	k6eAd1	téměř
nepropustná	propustný	k2eNgFnSc1d1	nepropustná
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnPc1d1	západní
pobřeží	pobřeží	k1gNnPc1	pobřeží
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Žlutého	žlutý	k2eAgNnSc2d1	žluté
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
Japonské	japonský	k2eAgNnSc1d1	Japonské
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Terén	terén	k1gInSc1	terén
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
,	,	kIx,	,
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
hory	hora	k1gFnPc4	hora
jsou	být	k5eAaImIp3nP	být
proloženy	proložit	k5eAaPmNgFnP	proložit
úzkými	úzký	k2eAgNnPc7d1	úzké
a	a	k8xC	a
hlubokými	hluboký	k2eAgNnPc7d1	hluboké
údolími	údolí	k1gNnPc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Pektusan	Pektusan	k1gInSc1	Pektusan
<g/>
,	,	kIx,	,
vyhaslá	vyhaslý	k2eAgFnSc1d1	vyhaslá
sopka	sopka	k1gFnSc1	sopka
měřící	měřící	k2eAgFnSc1d1	měřící
2	[number]	k4	2
744	[number]	k4	744
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
pláně	pláň	k1gFnPc1	pláň
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
nespojité	spojitý	k2eNgNnSc1d1	nespojité
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
a	a	k8xC	a
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
přes	přes	k7c4	přes
70	[number]	k4	70
procent	procento	k1gNnPc2	procento
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
strmých	strmý	k2eAgInPc6d1	strmý
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Jalu	Jala	k1gFnSc4	Jala
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
790	[number]	k4	790
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
přinášejí	přinášet	k5eAaImIp3nP	přinášet
jasné	jasný	k2eAgNnSc4d1	jasné
počasí	počasí	k1gNnSc4	počasí
<g/>
,	,	kIx,	,
proložené	proložený	k2eAgInPc1d1	proložený
sněhovými	sněhový	k2eAgFnPc7d1	sněhová
bouřemi	bouř	k1gFnPc7	bouř
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
severních	severní	k2eAgInPc2d1	severní
a	a	k8xC	a
severozápadních	severozápadní	k2eAgInPc2d1	severozápadní
větrů	vítr	k1gInPc2	vítr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vanou	vanout	k5eAaImIp3nP	vanout
od	od	k7c2	od
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
bývá	bývat	k5eAaImIp3nS	bývat
zdaleka	zdaleka	k6eAd1	zdaleka
nejteplejší	teplý	k2eAgFnSc4d3	nejteplejší
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
vlhké	vlhký	k2eAgNnSc4d1	vlhké
a	a	k8xC	a
nejdeštivější	deštivý	k2eAgNnSc4d3	nejdeštivější
období	období	k1gNnSc4	období
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jižní	jižní	k2eAgInPc1d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgInPc1d1	jihovýchodní
monzunové	monzunový	k2eAgInPc1d1	monzunový
větry	vítr	k1gInPc1	vítr
přinášejí	přinášet	k5eAaImIp3nP	přinášet
vlhký	vlhký	k2eAgInSc4d1	vlhký
vzduch	vzduch	k1gInSc4	vzduch
od	od	k7c2	od
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Čučche	Čučch	k1gFnSc2	Čučch
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
vláda	vláda	k1gFnSc1	vláda
má	mít	k5eAaImIp3nS	mít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
mnoha	mnoho	k4c7	mnoho
aspekty	aspekt	k1gInPc7	aspekt
národní	národní	k2eAgFnSc2d1	národní
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
kontrola	kontrola	k1gFnSc1	kontrola
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
kultu	kult	k1gInSc2	kult
osobnosti	osobnost	k1gFnSc2	osobnost
okolo	okolo	k7c2	okolo
Kim	Kim	k1gFnSc2	Kim
Ir-Sena	Ir-Seno	k1gNnSc2	Ir-Seno
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
Kim	Kim	k1gMnSc2	Kim
Čong-ila	Čongl	k1gMnSc2	Čong-il
<g/>
.	.	kIx.	.
</s>
<s>
Novinář	novinář	k1gMnSc1	novinář
Bradley	Bradlea	k1gFnSc2	Bradlea
Martin	Martin	k1gMnSc1	Martin
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgFnSc1	všechen
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
"	"	kIx"	"
<g/>
velkého	velký	k2eAgMnSc4d1	velký
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
"	"	kIx"	"
Kim	Kim	k1gMnSc4	Kim
Ir-sena	Iren	k2eAgMnSc4d1	Ir-sen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
kult	kult	k1gInSc1	kult
osobnosti	osobnost	k1gFnSc3	osobnost
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
milovaného	milovaný	k1gMnSc4	milovaný
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
"	"	kIx"	"
Kim	Kim	k1gMnSc4	Kim
Čong-ila	Čongl	k1gMnSc4	Čong-il
<g/>
.	.	kIx.	.
</s>
<s>
Bradley	Bradlea	k1gMnSc2	Bradlea
Martin	Martin	k1gMnSc1	Martin
také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kim	Kim	k1gFnSc1	Kim
Ir-sen	Iren	k1gInSc1	Ir-sen
"	"	kIx"	"
<g/>
stvořil	stvořit	k5eAaPmAgInS	stvořit
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
a	a	k8xC	a
Kim	Kim	k1gMnSc1	Kim
Čong-il	Čongl	k1gMnSc1	Čong-il
může	moct	k5eAaImIp3nS	moct
"	"	kIx"	"
<g/>
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
počasí	počasí	k1gNnSc1	počasí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politické	politický	k2eAgFnSc2d1	politická
čistky	čistka	k1gFnSc2	čistka
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vládních	vládní	k2eAgInPc6d1	vládní
a	a	k8xC	a
nejvyšších	vysoký	k2eAgInPc6d3	Nejvyšší
vojenských	vojenský	k2eAgInPc6d1	vojenský
kruzích	kruh	k1gInPc6	kruh
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
pravidelně	pravidelně	k6eAd1	pravidelně
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
čistkám	čistka	k1gFnPc3	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
státu	stát	k1gInSc2	stát
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
být	být	k5eAaImF	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
např.	např.	kA	např.
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
neloajality	neloajalita	k1gFnSc2	neloajalita
nebo	nebo	k8xC	nebo
kritiky	kritika	k1gFnSc2	kritika
jeho	jeho	k3xOp3gFnSc2	jeho
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čistkám	čistka	k1gFnPc3	čistka
pravidelně	pravidelně	k6eAd1	pravidelně
přistupoval	přistupovat	k5eAaImAgMnS	přistupovat
zakladatel	zakladatel	k1gMnSc1	zakladatel
totalitní	totalitní	k2eAgFnSc2d1	totalitní
KLDR	KLDR	kA	KLDR
Kim	Kim	k1gMnSc1	Kim
Ir-sen	Iren	k2eAgMnSc1d1	Ir-sen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
následovník	následovník	k1gMnSc1	následovník
Kim	Kim	k1gMnSc1	Kim
Čong-il	Čongl	k1gMnSc1	Čong-il
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
převzal	převzít	k5eAaPmAgMnS	převzít
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc1	Kim
Čong-un	Čongn	k1gMnSc1	Čong-un
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
za	za	k7c4	za
vlastizradu	vlastizrada	k1gFnSc4	vlastizrada
popraven	popraven	k2eAgInSc1d1	popraven
velmi	velmi	k6eAd1	velmi
vysoce	vysoce	k6eAd1	vysoce
postavený	postavený	k2eAgMnSc1d1	postavený
Kim	Kim	k1gMnSc1	Kim
Čong-unův	Čongnův	k2eAgMnSc1d1	Čong-unův
strýc	strýc	k1gMnSc1	strýc
Čang	Čang	k1gMnSc1	Čang
Song-tchek	Songchek	k1gMnSc1	Song-tchek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
podle	podle	k7c2	podle
rozsudku	rozsudek	k1gInSc2	rozsudek
provést	provést	k5eAaPmF	provést
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dubnu	duben	k1gInSc3	duben
údajně	údajně	k6eAd1	údajně
popraveno	popravit	k5eAaPmNgNnS	popravit
nejméně	málo	k6eAd3	málo
16	[number]	k4	16
činitelů	činitel	k1gMnPc2	činitel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
vůdcovu	vůdcův	k2eAgFnSc4d1	Vůdcova
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýše	vysoce	k6eAd3	vysoce
postavené	postavený	k2eAgInPc4d1	postavený
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patřil	patřit	k5eAaImAgMnS	patřit
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Hjon	Hjon	k1gMnSc1	Hjon
Jong-čol	Jong-čol	k1gInSc1	Jong-čol
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vlastizrady	vlastizrada	k1gFnSc2	vlastizrada
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
po	po	k7c4	po
zatčení	zatčení	k1gNnSc4	zatčení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
veřejně	veřejně	k6eAd1	veřejně
popraven	popravit	k5eAaPmNgInS	popravit
protiletadlovou	protiletadlový	k2eAgFnSc7d1	protiletadlová
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vážnost	vážnost	k1gFnSc1	vážnost
u	u	k7c2	u
vůdce	vůdce	k1gMnSc2	vůdce
údajně	údajně	k6eAd1	údajně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
když	když	k8xS	když
usnul	usnout	k5eAaPmAgMnS	usnout
během	během	k7c2	během
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přehlídky	přehlídka	k1gFnSc2	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
zmínil	zmínit	k5eAaPmAgInS	zmínit
Státní	státní	k2eAgInSc1d1	státní
odbor	odbor	k1gInSc1	odbor
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
strany	strana	k1gFnSc2	strana
"	"	kIx"	"
<g/>
protistranické	protistranický	k2eAgInPc1d1	protistranický
a	a	k8xC	a
kontrarevoluční	kontrarevoluční	k2eAgInPc1d1	kontrarevoluční
postoje	postoj	k1gInPc1	postoj
<g/>
"	"	kIx"	"
Kim	Kim	k1gFnSc1	Kim
Čong-čina	Čong-čina	k1gFnSc1	Čong-čina
<g/>
,	,	kIx,	,
místopředsedy	místopředseda	k1gMnSc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
ho	on	k3xPp3gMnSc4	on
podle	podle	k7c2	podle
jihokorejského	jihokorejský	k2eAgNnSc2d1	jihokorejské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pro	pro	k7c4	pro
sjednocení	sjednocení	k1gNnSc4	sjednocení
udal	udat	k5eAaPmAgMnS	udat
za	za	k7c4	za
"	"	kIx"	"
<g/>
špatné	špatný	k2eAgNnSc4d1	špatné
držení	držení	k1gNnSc4	držení
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
v	v	k7c6	v
tribuně	tribuna	k1gFnSc6	tribuna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
a	a	k8xC	a
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
Kim	Kim	k1gFnSc1	Kim
Čong-una	Čongna	k1gFnSc1	Čong-una
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
podle	podle	k7c2	podle
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
celkem	celkem	k6eAd1	celkem
popraveno	popravit	k5eAaPmNgNnS	popravit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čistek	čistka	k1gFnPc2	čistka
na	na	k7c4	na
70	[number]	k4	70
státních	státní	k2eAgMnPc2d1	státní
představitelů	představitel	k1gMnPc2	představitel
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
měla	mít	k5eAaImAgFnS	mít
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
až	až	k9	až
1970	[number]	k4	1970
začala	začít	k5eAaPmAgFnS	začít
provádět	provádět	k5eAaImF	provádět
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
navázala	navázat	k5eAaPmAgFnS	navázat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
rozvojovými	rozvojový	k2eAgFnPc7d1	rozvojová
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
připojila	připojit	k5eAaPmAgFnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
země	zem	k1gFnSc2	zem
s	s	k7c7	s
kolapsem	kolaps	k1gInSc7	kolaps
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
uvržena	uvrhnout	k5eAaPmNgFnS	uvrhnout
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
uzavření	uzavření	k1gNnSc4	uzavření
30	[number]	k4	30
<g/>
%	%	kIx~	%
ambasád	ambasáda	k1gFnPc2	ambasáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
162	[number]	k4	162
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
palestinskou	palestinský	k2eAgFnSc7d1	palestinská
samosprávou	samospráva	k1gFnSc7	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Ambasády	ambasáda	k1gFnSc2	ambasáda
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
42	[number]	k4	42
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
má	mít	k5eAaImIp3nS	mít
pokračující	pokračující	k2eAgFnPc4d1	pokračující
silné	silný	k2eAgFnPc4d1	silná
vazby	vazba	k1gFnPc4	vazba
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
socialistickými	socialistický	k2eAgMnPc7d1	socialistický
spojenci	spojenec	k1gMnPc7	spojenec
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
asii	asi	k1gFnSc6	asi
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Vietnam	Vietnam	k1gInSc1	Vietnam
a	a	k8xC	a
Laos	Laos	k1gInSc1	Laos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
i	i	k8xC	i
Čína	Čína	k1gFnSc1	Čína
se	se	k3xPyFc4	se
od	od	k7c2	od
Severní	severní	k2eAgFnSc2d1	severní
Korey	Korea	k1gFnSc2	Korea
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
jaderných	jaderný	k2eAgInPc6d1	jaderný
testech	test	k1gInPc6	test
oficiálně	oficiálně	k6eAd1	oficiálně
distancovaly	distancovat	k5eAaBmAgFnP	distancovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
nabývá	nabývat	k5eAaImIp3nS	nabývat
na	na	k7c6	na
významu	význam	k1gInSc6	význam
politicko-ekonomický	politickokonomický	k2eAgInSc4d1	politicko-ekonomický
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Západních	západní	k2eAgFnPc2d1	západní
sankcí	sankce	k1gFnPc2	sankce
za	za	k7c4	za
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
a	a	k8xC	a
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
Ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
106	[number]	k4	106
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
8	[number]	k4	8
389	[number]	k4	389
000	[number]	k4	000
je	být	k5eAaImIp3nS	být
rezervistů	rezervista	k1gMnPc2	rezervista
nebo	nebo	k8xC	nebo
polovojenských	polovojenský	k2eAgFnPc2d1	polovojenská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
největší	veliký	k2eAgFnSc4d3	veliký
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
20	[number]	k4	20
<g/>
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
mezi	mezi	k7c7	mezi
17	[number]	k4	17
až	až	k8xS	až
54	[number]	k4	54
lety	let	k1gInPc7	let
slouží	sloužit	k5eAaImIp3nP	sloužit
v	v	k7c6	v
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
vojenských	vojenský	k2eAgFnPc6d1	vojenská
jednotkách	jednotka	k1gFnPc6	jednotka
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
25	[number]	k4	25
občanů	občan	k1gMnPc2	občan
je	být	k5eAaImIp3nS	být
vedený	vedený	k2eAgMnSc1d1	vedený
jako	jako	k8xS	jako
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
též	též	k9	též
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
rakety	raketa	k1gFnPc4	raketa
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
členění	členění	k1gNnSc1	členění
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
tříúrovňovou	tříúrovňový	k2eAgFnSc4d1	tříúrovňová
administrativní	administrativní	k2eAgFnSc4d1	administrativní
hierarchii	hierarchie	k1gFnSc4	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
stojí	stát	k5eAaImIp3nS	stát
9	[number]	k4	9
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
přímo	přímo	k6eAd1	přímo
řízená	řízený	k2eAgNnPc1d1	řízené
města	město	k1gNnPc1	město
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
speciální	speciální	k2eAgFnPc1d1	speciální
administrativní	administrativní	k2eAgFnPc1d1	administrativní
oblasti	oblast	k1gFnPc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
úrovni	úroveň	k1gFnSc6	úroveň
stojí	stát	k5eAaImIp3nS	stát
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc1	okres
a	a	k8xC	a
distrikty	distrikt	k1gInPc1	distrikt
<g/>
,	,	kIx,	,
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnPc1	vesnice
a	a	k8xC	a
dělnické	dělnický	k2eAgInPc1d1	dělnický
distrikty	distrikt	k1gInPc1	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Čagang	Čagang	k1gInSc1	Čagang
(	(	kIx(	(
<g/>
자	자	k?	자
<g/>
;	;	kIx,	;
慈	慈	k?	慈
<g/>
)	)	kIx)	)
Severní	severní	k2eAgInSc1d1	severní
Hamgjong	Hamgjong	k1gInSc1	Hamgjong
(	(	kIx(	(
<g/>
함	함	k?	함
<g/>
;	;	kIx,	;
咸	咸	k?	咸
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Hamgjong	Hamgjong	k1gInSc1	Hamgjong
(	(	kIx(	(
<g/>
함	함	k?	함
<g/>
;	;	kIx,	;
咸	咸	k?	咸
<g/>
)	)	kIx)	)
Severní	severní	k2eAgFnSc1d1	severní
Hwanghe	Hwanghe	k1gFnSc1	Hwanghe
(	(	kIx(	(
<g/>
황	황	k?	황
<g/>
;	;	kIx,	;
黃	黃	k?	黃
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgFnSc2d1	jižní
Hwanghe	Hwangh	k1gFnSc2	Hwangh
(	(	kIx(	(
<g/>
황	황	k?	황
<g/>
;	;	kIx,	;
黃	黃	k?	黃
<g/>
)	)	kIx)	)
Kangwon	Kangwon	k1gMnSc1	Kangwon
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
강	강	k?	강
<g/>
;	;	kIx,	;
江	江	k?	江
<g/>
)	)	kIx)	)
Severní	severní	k2eAgInSc1d1	severní
Pchjongan	Pchjongan	k1gInSc1	Pchjongan
(	(	kIx(	(
<g/>
평	평	k?	평
<g/>
;	;	kIx,	;
平	平	k?	平
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Pchjongan	Pchjongan	k1gInSc1	Pchjongan
(	(	kIx(	(
<g/>
평	평	k?	평
<g/>
;	;	kIx,	;
平	平	k?	平
<g/>
)	)	kIx)	)
Rjanggang	Rjanggang	k1gMnSc1	Rjanggang
(	(	kIx(	(
<g/>
량	량	k?	량
<g/>
;	;	kIx,	;
兩	兩	k?	兩
<g/>
)	)	kIx)	)
Pchjongjang	Pchjongjang	k1gInSc1	Pchjongjang
(	(	kIx(	(
<g/>
평	평	k?	평
직	직	k?	직
<g/>
;	;	kIx,	;
平	平	k?	平
<g/>
;	;	kIx,	;
Pchjongjang	Pchjongjang	k1gInSc1	Pchjongjang
čikalši	čikalsat	k5eAaPmIp1nSwK	čikalsat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Rason	Rason	k1gMnSc1	Rason
(	(	kIx(	(
<g/>
Radžin-Sonbong	Radžin-Sonbong	k1gMnSc1	Radžin-Sonbong
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
라	라	k?	라
(	(	kIx(	(
<g/>
라	라	k?	라
<g/>
)	)	kIx)	)
직	직	k?	직
<g/>
;	;	kIx,	;
羅	羅	k?	羅
(	(	kIx(	(
<g/>
羅	羅	k?	羅
<g/>
)	)	kIx)	)
直	直	k?	直
<g/>
;	;	kIx,	;
Rason	Rason	k1gMnSc1	Rason
(	(	kIx(	(
<g/>
Radžin-Sonbong	Radžin-Sonbong	k1gMnSc1	Radžin-Sonbong
<g/>
)	)	kIx)	)
čikalši	čikalsat	k5eAaPmIp1nSwK	čikalsat
<g/>
)	)	kIx)	)
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
oblast	oblast	k1gFnSc1	oblast
Kesong	Kesonga	k1gFnPc2	Kesonga
(	(	kIx(	(
<g/>
개	개	k?	개
공	공	k?	공
지	지	k?	지
<g/>
;	;	kIx,	;
開	開	k?	開
<g/>
;	;	kIx,	;
Kesong	Kesong	k1gMnSc1	Kesong
<g />
.	.	kIx.	.
</s>
<s>
kong-op	kongp	k1gInSc1	kong-op
čigu	čigu	k5eAaPmIp1nS	čigu
<g/>
)	)	kIx)	)
Turistická	turistický	k2eAgFnSc1d1	turistická
oblast	oblast	k1gFnSc1	oblast
Kumgangsan	Kumgangsana	k1gFnPc2	Kumgangsana
(	(	kIx(	(
<g/>
금	금	k?	금
관	관	k?	관
지	지	k?	지
<g/>
;	;	kIx,	;
金	金	k?	金
<g/>
;	;	kIx,	;
Kumgangsan	Kumgangsan	k1gMnSc1	Kumgangsan
kwangwang	kwangwang	k1gMnSc1	kwangwang
čigu	čigu	k5eAaPmIp1nS	čigu
<g/>
)	)	kIx)	)
Speciální	speciální	k2eAgFnSc4d1	speciální
administrativní	administrativní	k2eAgFnSc4d1	administrativní
oblast	oblast	k1gFnSc4	oblast
Sinidžu	Sinidžu	k1gFnSc2	Sinidžu
(	(	kIx(	(
<g/>
신	신	k?	신
특	특	k?	특
행	행	k?	행
<g/>
;	;	kIx,	;
新	新	k?	新
<g/>
;	;	kIx,	;
Sinidžu	Sinidžu	k1gFnPc2	Sinidžu
tchukpjol	tchukpjol	k1gInSc1	tchukpjol
hengdžonggu	hengdžongg	k1gInSc2	hengdžongg
<g/>
)	)	kIx)	)
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
velmi	velmi	k6eAd1	velmi
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
lidově	lidově	k6eAd1	lidově
demokratických	demokratický	k2eAgFnPc2d1	demokratická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
kolapsu	kolaps	k1gInSc6	kolaps
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
režim	režim	k1gInSc1	režim
nezhroutil	zhroutit	k5eNaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
a	a	k8xC	a
staré	starý	k2eAgFnPc1d1	stará
vazby	vazba	k1gFnPc1	vazba
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
přetrhány	přetrhán	k2eAgFnPc1d1	přetrhána
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ohromné	ohromný	k2eAgFnSc3d1	ohromná
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
následkem	následek	k1gInSc7	následek
byl	být	k5eAaImAgInS	být
jak	jak	k8xC	jak
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
úpadek	úpadek	k1gInSc4	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
výkonnost	výkonnost	k1gFnSc1	výkonnost
stále	stále	k6eAd1	stále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
stále	stále	k6eAd1	stále
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
sousedy	soused	k1gMnPc7	soused
enormní	enormní	k2eAgInSc4d1	enormní
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
zbrojení	zbrojení	k1gNnSc4	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
technika	technika	k1gFnSc1	technika
včetně	včetně	k7c2	včetně
vojenské	vojenský	k2eAgFnSc2d1	vojenská
je	být	k5eAaImIp3nS	být
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
izolaci	izolace	k1gFnSc3	izolace
státu	stát	k1gInSc2	stát
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
zastaralou	zastaralý	k2eAgFnSc4d1	zastaralá
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nepočetné	početný	k2eNgFnPc1d1	nepočetná
elitní	elitní	k2eAgFnPc1d1	elitní
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
disponují	disponovat	k5eAaBmIp3nP	disponovat
kvalitními	kvalitní	k2eAgFnPc7d1	kvalitní
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Země	zem	k1gFnPc1	zem
trpí	trpět	k5eAaImIp3nP	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
;	;	kIx,	;
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
radikálně	radikálně	k6eAd1	radikálně
omezena	omezen	k2eAgFnSc1d1	omezena
spotřeba	spotřeba	k1gFnSc1	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
–	–	k?	–
povolen	povolen	k2eAgInSc1d1	povolen
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
spotřebičů	spotřebič	k1gInPc2	spotřebič
<g/>
,	,	kIx,	,
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
energie	energie	k1gFnSc1	energie
odpojená	odpojený	k2eAgFnSc1d1	odpojená
<g/>
.	.	kIx.	.
</s>
<s>
Ochromena	ochromen	k2eAgFnSc1d1	ochromena
je	být	k5eAaImIp3nS	být
též	též	k9	též
doprava	doprava	k6eAd1	doprava
<g/>
;	;	kIx,	;
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
automobilů	automobil	k1gInPc2	automobil
téměř	téměř	k6eAd1	téměř
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
cestují	cestovat	k5eAaImIp3nP	cestovat
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
kolech	kolo	k1gNnPc6	kolo
nebo	nebo	k8xC	nebo
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Enormní	enormní	k2eAgFnPc1d1	enormní
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
obtíže	obtíž	k1gFnPc1	obtíž
se	se	k3xPyFc4	se
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
snaží	snažit	k5eAaImIp3nS	snažit
řešit	řešit	k5eAaImF	řešit
vytvářením	vytváření	k1gNnSc7	vytváření
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
zón	zóna	k1gFnPc2	zóna
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Kesong	Kesong	k1gInSc4	Kesong
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plochy	plocha	k1gFnSc2	plocha
celé	celý	k2eAgFnSc2d1	celá
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
zabírají	zabírat	k5eAaImIp3nP	zabírat
74,5	[number]	k4	74,5
%	%	kIx~	%
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
19,2	[number]	k4	19,2
%	%	kIx~	%
pak	pak	k6eAd1	pak
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
pracuje	pracovat	k5eAaImIp3nS	pracovat
33	[number]	k4	33
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
činného	činný	k2eAgNnSc2d1	činné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nP	těžet
se	se	k3xPyFc4	se
nerostné	nerostný	k2eAgFnPc1d1	nerostná
suroviny	surovina	k1gFnPc1	surovina
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
zpracování	zpracování	k1gNnSc1	zpracování
(	(	kIx(	(
<g/>
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
magnezit	magnezit	k1gInSc1	magnezit
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
drahé	drahý	k2eAgInPc1d1	drahý
kovy	kov	k1gInPc1	kov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
jsou	být	k5eAaImIp3nP	být
přirozeně	přirozeně	k6eAd1	přirozeně
její	její	k3xOp3gMnPc1	její
sousedi	soused	k1gMnPc1	soused
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
–	–	k?	–
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
špatným	špatný	k2eAgInPc3d1	špatný
vztahům	vztah	k1gInPc3	vztah
paradoxně	paradoxně	k6eAd1	paradoxně
–	–	k?	–
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ocel	ocel	k1gFnSc1	ocel
<g/>
,	,	kIx,	,
surové	surový	k2eAgNnSc1d1	surové
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
cement	cement	k1gInSc1	cement
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Vyváží	vyvážet	k5eAaImIp3nP	vyvážet
se	se	k3xPyFc4	se
továrenské	továrenský	k2eAgInPc1d1	továrenský
výrobky	výrobek	k1gInPc1	výrobek
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
a	a	k8xC	a
rybářské	rybářský	k2eAgInPc4d1	rybářský
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
sójové	sójový	k2eAgInPc1d1	sójový
boby	bob	k1gInPc1	bob
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
vepřové	vepřový	k2eAgNnSc1d1	vepřové
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
vejce	vejce	k1gNnSc1	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
vývozu	vývoz	k1gInSc2	vývoz
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Severní	severní	k2eAgFnSc4d1	severní
Koreu	Korea	k1gFnSc4	Korea
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
Ugandy	Uganda	k1gFnSc2	Uganda
<g/>
,	,	kIx,	,
Senegalu	Senegal	k1gInSc2	Senegal
nebo	nebo	k8xC	nebo
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
<g/>
.	.	kIx.	.
</s>
<s>
Dováží	dovážet	k5eAaImIp3nS	dovážet
se	se	k3xPyFc4	se
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
koksovatelné	koksovatelný	k2eAgNnSc1d1	koksovatelné
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnPc1	pšenice
<g/>
,	,	kIx,	,
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
dobrým	dobrý	k2eAgInPc3d1	dobrý
výnosům	výnos	k1gInPc3	výnos
začala	začít	k5eAaPmAgFnS	začít
KLDR	KLDR	kA	KLDR
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
desetiletí	desetiletí	k1gNnSc2	desetiletí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
státem	stát	k1gInSc7	stát
ovládaného	ovládaný	k2eAgInSc2d1	ovládaný
byznysu	byznys	k1gInSc2	byznys
vyrábět	vyrábět	k5eAaImF	vyrábět
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
především	především	k9	především
metamfetamin	metamfetamin	k1gInSc1	metamfetamin
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
<g/>
,	,	kIx,	,
a	a	k8xC	a
opiáty	opiát	k1gInPc1	opiát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
je	být	k5eAaImIp3nS	být
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
diplomatů	diplomat	k1gMnPc2	diplomat
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgMnSc3	každý
bylo	být	k5eAaImAgNnS	být
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
20	[number]	k4	20
kg	kg	kA	kg
nespecifikovaných	specifikovaný	k2eNgFnPc2d1	nespecifikovaná
drog	droga	k1gFnPc2	droga
s	s	k7c7	s
pokynem	pokyn	k1gInSc7	pokyn
prodat	prodat	k5eAaPmF	prodat
je	on	k3xPp3gNnPc4	on
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
alespoň	alespoň	k9	alespoň
za	za	k7c4	za
300	[number]	k4	300
tisíc	tisíc	k4xCgInSc4	tisíc
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Splnění	splnění	k1gNnSc1	splnění
úkolu	úkol	k1gInSc2	úkol
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
úřadů	úřad	k1gInPc2	úřad
KLDR	KLDR	kA	KLDR
projevem	projev	k1gInSc7	projev
loajality	loajalita	k1gFnSc2	loajalita
ke	k	k7c3	k
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
tak	tak	k6eAd1	tak
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
píše	psát	k5eAaImIp3nS	psát
jihokorejský	jihokorejský	k2eAgInSc1d1	jihokorejský
server	server	k1gInSc1	server
chosun	chosuna	k1gFnPc2	chosuna
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
prodejem	prodej	k1gInSc7	prodej
oslavit	oslavit	k5eAaPmF	oslavit
nadcházející	nadcházející	k2eAgFnPc4d1	nadcházející
narozeniny	narozeniny	k1gFnPc4	narozeniny
otce	otec	k1gMnSc2	otec
moderní	moderní	k2eAgFnSc1d1	moderní
KLDR	KLDR	kA	KLDR
Kim	Kim	k1gFnSc1	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
<g/>
.	.	kIx.	.
</s>
<s>
Drogové	drogový	k2eAgFnPc1d1	drogová
operace	operace	k1gFnPc1	operace
probíhaly	probíhat	k5eAaImAgFnP	probíhat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
speciálního	speciální	k2eAgInSc2d1	speciální
Úřadu	úřad	k1gInSc2	úřad
39	[number]	k4	39
s	s	k7c7	s
minimálně	minimálně	k6eAd1	minimálně
mlčenlivým	mlčenlivý	k2eAgInSc7d1	mlčenlivý
souhlasem	souhlas	k1gInSc7	souhlas
vedení	vedení	k1gNnSc2	vedení
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
skandálech	skandál	k1gInPc6	skandál
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
diplomatický	diplomatický	k2eAgInSc1d1	diplomatický
kanál	kanál	k1gInSc1	kanál
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
zavřeny	zavřen	k2eAgFnPc1d1	zavřena
byly	být	k5eAaImAgFnP	být
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
varny	varna	k1gFnPc1	varna
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
ale	ale	k9	ale
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
a	a	k8xC	a
napojila	napojit	k5eAaPmAgFnS	napojit
se	se	k3xPyFc4	se
na	na	k7c4	na
kriminální	kriminální	k2eAgInPc4d1	kriminální
gangy	gang	k1gInPc4	gang
zapojené	zapojený	k2eAgInPc4d1	zapojený
na	na	k7c4	na
asijské	asijský	k2eAgFnPc4d1	asijská
překupníky	překupník	k1gMnPc7	překupník
pašující	pašující	k2eAgFnSc2d1	pašující
drogy	droga	k1gFnSc2	droga
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
prodejem	prodej	k1gInSc7	prodej
drog	droga	k1gFnPc2	droga
snaží	snažit	k5eAaImIp3nP	snažit
zoufale	zoufale	k6eAd1	zoufale
získat	získat	k5eAaPmF	získat
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
měny	měna	k1gFnPc4	měna
v	v	k7c6	v
hotovosti	hotovost	k1gFnSc6	hotovost
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
tak	tak	k9	tak
na	na	k7c6	na
vývozu	vývoz	k1gInSc6	vývoz
KLDR	KLDR	kA	KLDR
vydělá	vydělat	k5eAaPmIp3nS	vydělat
200	[number]	k4	200
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
dotýkat	dotýkat	k5eAaImF	dotýkat
i	i	k9	i
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
často	často	k6eAd1	často
na	na	k7c4	na
léky	lék	k1gInPc4	lék
a	a	k8xC	a
dostupným	dostupný	k2eAgInSc7d1	dostupný
metamfetaminem	metamfetamin	k1gInSc7	metamfetamin
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
říkají	říkat	k5eAaImIp3nP	říkat
familiárně	familiárně	k6eAd1	familiárně
led	led	k1gInSc4	led
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
tak	tak	k9	tak
léčí	léčit	k5eAaImIp3nP	léčit
například	například	k6eAd1	například
i	i	k9	i
chronické	chronický	k2eAgFnPc4d1	chronická
nemoci	nemoc	k1gFnPc4	nemoc
a	a	k8xC	a
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
KLDR	KLDR	kA	KLDR
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
začala	začít	k5eAaPmAgFnS	začít
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
od	od	k7c2	od
užívání	užívání	k1gNnSc2	užívání
této	tento	k3xDgFnSc2	tento
drogy	droga	k1gFnSc2	droga
odrazovat	odrazovat	k5eAaImF	odrazovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
slabosti	slabost	k1gFnSc3	slabost
zneužitelné	zneužitelný	k2eAgNnSc1d1	zneužitelné
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
odbytištěm	odbytiště	k1gNnSc7	odbytiště
severokorejského	severokorejský	k2eAgInSc2d1	severokorejský
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
nemá	mít	k5eNaImIp3nS	mít
jeho	jeho	k3xOp3gFnSc1	jeho
čistota	čistota	k1gFnSc1	čistota
obdoby	obdoba	k1gFnSc2	obdoba
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
98	[number]	k4	98
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
severovýchod	severovýchod	k1gInSc1	severovýchod
Číny	Čína	k1gFnSc2	Čína
je	být	k5eAaImIp3nS	být
nejohroženější	ohrožený	k2eAgFnSc1d3	nejohroženější
a	a	k8xC	a
raketově	raketově	k6eAd1	raketově
zde	zde	k6eAd1	zde
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
závislých	závislý	k2eAgFnPc2d1	závislá
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
říkají	říkat	k5eAaImIp3nP	říkat
ledová	ledový	k2eAgFnSc1d1	ledová
droga	droga	k1gFnSc1	droga
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
drogy	droga	k1gFnSc2	droga
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
mezi	mezi	k7c7	mezi
celebritami	celebrita	k1gFnPc7	celebrita
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
má	mít	k5eAaImIp3nS	mít
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
policisté	policista	k1gMnPc1	policista
ročně	ročně	k6eAd1	ročně
zadrží	zadržet	k5eAaPmIp3nP	zadržet
metamfetamin	metamfetamin	k1gInSc4	metamfetamin
za	za	k7c4	za
desítky	desítka	k1gFnPc4	desítka
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
špička	špička	k1gFnSc1	špička
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
ale	ale	k9	ale
považuje	považovat	k5eAaImIp3nS	považovat
původ	původ	k1gInSc4	původ
zadrženého	zadržený	k2eAgInSc2d1	zadržený
metamfetaminu	metamfetamin	k1gInSc2	metamfetamin
za	za	k7c4	za
citlivou	citlivý	k2eAgFnSc4d1	citlivá
informaci	informace	k1gFnSc4	informace
a	a	k8xC	a
nechce	chtít	k5eNaImIp3nS	chtít
ji	on	k3xPp3gFnSc4	on
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
KLDR	KLDR	kA	KLDR
neodvádějí	odvádět	k5eNaImIp3nP	odvádět
státu	stát	k1gInSc3	stát
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
přímé	přímý	k2eAgFnPc1d1	přímá
daně	daň	k1gFnPc1	daň
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
KLDR	KLDR	kA	KLDR
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgInSc4	první
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
přímé	přímý	k2eAgNnSc4d1	přímé
daňové	daňový	k2eAgNnSc4d1	daňové
zatížení	zatížení	k1gNnSc4	zatížení
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
turistika	turistika	k1gFnSc1	turistika
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
pomocí	pomocí	k7c2	pomocí
skupinových	skupinový	k2eAgInPc2d1	skupinový
zájezdů	zájezd	k1gInPc2	zájezd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
cestovní	cestovní	k2eAgFnPc1d1	cestovní
kanceláře	kancelář	k1gFnPc1	kancelář
nabízejí	nabízet	k5eAaImIp3nP	nabízet
i	i	k9	i
samostatné	samostatný	k2eAgInPc4d1	samostatný
zájezdy	zájezd	k1gInPc4	zájezd
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
návštěvy	návštěva	k1gFnSc2	návštěva
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
svých	svůj	k3xOyFgMnPc2	svůj
průvodců	průvodce	k1gMnPc2	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
organizováno	organizovat	k5eAaBmNgNnS	organizovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
turisté	turist	k1gMnPc1	turist
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
hovoru	hovor	k1gInSc2	hovor
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
čeká	čekat	k5eAaImIp3nS	čekat
postih	postih	k1gInSc1	postih
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
navštěvováno	navštěvován	k2eAgNnSc1d1	navštěvováno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Pchjongjang	Pchjongjang	k1gInSc1	Pchjongjang
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
malé	malý	k2eAgFnSc2d1	malá
čínské	čínský	k2eAgFnSc2d1	čínská
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
několika	několik	k4yIc2	několik
etnických	etnický	k2eAgMnPc2d1	etnický
Japonců	Japonec	k1gMnPc2	Japonec
je	být	k5eAaImIp3nS	být
severokorejské	severokorejský	k2eAgNnSc1d1	severokorejské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
24	[number]	k4	24
852	[number]	k4	852
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
etnicky	etnicky	k6eAd1	etnicky
homogenní	homogenní	k2eAgFnSc1d1	homogenní
<g/>
.	.	kIx.	.
</s>
<s>
Demografové	demograf	k1gMnPc1	demograf
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
narůst	narůst	k5eAaPmF	narůst
na	na	k7c4	na
25,5	[number]	k4	25,5
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
28	[number]	k4	28
milionů	milion	k4xCgInPc2	milion
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hladomoru	hladomor	k1gInSc3	hladomor
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
cca	cca	kA	cca
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
trval	trvat	k5eAaImAgInS	trvat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
smrt	smrt	k1gFnSc4	smrt
300	[number]	k4	300
000	[number]	k4	000
až	až	k9	až
800	[number]	k4	800
000	[number]	k4	000
Severokorejců	Severokorejec	k1gMnPc2	Severokorejec
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtí	úmrtí	k1gNnSc4	úmrtí
byla	být	k5eAaImAgFnS	být
většinou	většinou	k6eAd1	většinou
zapříčiněna	zapříčiněn	k2eAgFnSc1d1	zapříčiněna
nemocemi	nemoc	k1gFnPc7	nemoc
z	z	k7c2	z
podvýživy	podvýživa	k1gFnSc2	podvýživa
jako	jako	k8xC	jako
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
než	než	k8xS	než
z	z	k7c2	z
hladovění	hladovění	k1gNnSc2	hladovění
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgMnPc1d1	mezinárodní
dárci	dárce	k1gMnPc1	dárce
vedení	vedení	k1gNnSc2	vedení
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
zahájili	zahájit	k5eAaPmAgMnP	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
dodávky	dodávka	k1gFnSc2	dodávka
potravin	potravina	k1gFnPc2	potravina
přes	přes	k7c4	přes
světový	světový	k2eAgInSc4d1	světový
potravinový	potravinový	k2eAgInSc4d1	potravinový
program	program	k1gInSc4	program
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
hladomoru	hladomor	k1gInSc3	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
drastickému	drastický	k2eAgNnSc3d1	drastické
snížení	snížení	k1gNnSc3	snížení
podpory	podpora	k1gFnSc2	podpora
během	během	k7c2	během
Bushovy	Bushův	k2eAgFnSc2d1	Bushova
administrativy	administrativa	k1gFnSc2	administrativa
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
postupně	postupně	k6eAd1	postupně
zlepšovala	zlepšovat	k5eAaImAgFnS	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
podvyživených	podvyživený	k2eAgFnPc2d1	podvyživená
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
z	z	k7c2	z
60	[number]	k4	60
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
na	na	k7c4	na
37	[number]	k4	37
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
28	[number]	k4	28
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
produkce	produkce	k1gFnSc1	produkce
potravin	potravina	k1gFnPc2	potravina
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
téměř	téměř	k6eAd1	téměř
zotavila	zotavit	k5eAaPmAgFnS	zotavit
na	na	k7c4	na
doporučenou	doporučený	k2eAgFnSc4d1	Doporučená
roční	roční	k2eAgFnSc4d1	roční
úroveň	úroveň	k1gFnSc4	úroveň
5,37	[number]	k4	5,37
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
obilovin	obilovina	k1gFnPc2	obilovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
světový	světový	k2eAgInSc1d1	světový
potravinový	potravinový	k2eAgInSc1d1	potravinový
program	program	k1gInSc1	program
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
pokračující	pokračující	k2eAgInSc1d1	pokračující
nedostatek	nedostatek	k1gInSc1	nedostatek
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
tukům	tuk	k1gInPc3	tuk
a	a	k8xC	a
bílkovinám	bílkovina	k1gFnPc3	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
jazyk	jazyk	k1gInSc1	jazyk
korejštinu	korejština	k1gFnSc4	korejština
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
zemích	zem	k1gFnPc6	zem
existují	existovat	k5eAaImIp3nP	existovat
některé	některý	k3yIgInPc4	některý
rozdíly	rozdíl	k1gInPc4	rozdíl
a	a	k8xC	a
dialekty	dialekt	k1gInPc4	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejci	Severokorejec	k1gMnPc1	Severokorejec
popisují	popisovat	k5eAaImIp3nP	popisovat
svůj	svůj	k3xOyFgInSc4	svůj
pchjongjangský	pchjongjangský	k2eAgInSc4d1	pchjongjangský
dialekt	dialekt	k1gInSc4	dialekt
slovem	slovem	k6eAd1	slovem
munhwa	munhwa	k6eAd1	munhwa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kultivovaný	kultivovaný	k2eAgInSc1d1	kultivovaný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jihokorejského	jihokorejský	k2eAgMnSc2d1	jihokorejský
dialekt	dialekt	k1gInSc4	dialekt
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
p	p	k?	p
<g/>
'	'	kIx"	'
<g/>
jodžuno	jodžuna	k1gFnSc5	jodžuna
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
průměrný	průměrný	k2eAgInSc1d1	průměrný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k9	jako
dekadentní	dekadentní	k2eAgMnSc1d1	dekadentní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
využívá	využívat	k5eAaImIp3nS	využívat
slova	slovo	k1gNnPc1	slovo
přejatá	přejatý	k2eAgFnSc1d1	přejatá
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
jsou	být	k5eAaImIp3nP	být
ústavně	ústavně	k6eAd1	ústavně
zaručeny	zaručen	k2eAgFnPc1d1	zaručena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
podléhají	podléhat	k5eAaImIp3nP	podléhat
omezením	omezení	k1gNnSc7	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
je	být	k5eAaImIp3nS	být
64,3	[number]	k4	64,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
%	%	kIx~	%
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
korejský	korejský	k2eAgInSc1d1	korejský
šamanismus	šamanismus	k1gInSc1	šamanismus
<g/>
,	,	kIx,	,
13,5	[number]	k4	13,5
<g/>
%	%	kIx~	%
čeondoismus	čeondoismus	k1gInSc1	čeondoismus
<g/>
,	,	kIx,	,
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
buddhisté	buddhista	k1gMnPc1	buddhista
a	a	k8xC	a
1,7	[number]	k4	1,7
<g/>
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
trvá	trvat	k5eAaImIp3nS	trvat
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
zdarma	zdarma	k6eAd1	zdarma
včetně	včetně	k7c2	včetně
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,	,	kIx,	,
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
stravování	stravování	k1gNnSc4	stravování
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
předškolní	předškolní	k2eAgFnSc2d1	předškolní
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
lidové	lidový	k2eAgFnSc2d1	lidová
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
9	[number]	k4	9
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
středoškolského	středoškolský	k2eAgNnSc2d1	středoškolské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
děti	dítě	k1gFnPc1	dítě
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
nebo	nebo	k8xC	nebo
speciálních	speciální	k2eAgFnPc6d1	speciální
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
nepovinné	povinný	k2eNgNnSc1d1	nepovinné
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
akademické	akademický	k2eAgNnSc4d1	akademické
a	a	k8xC	a
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Kim	Kim	k1gFnSc1	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
a	a	k8xC	a
Univerzita	univerzita	k1gFnSc1	univerzita
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
technologie	technologie	k1gFnSc2	technologie
v	v	k7c6	v
Pchjongjangu	Pchjongjang	k1gInSc6	Pchjongjang
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
poslední	poslední	k2eAgFnSc2d1	poslední
zprávy	zpráva	k1gFnSc2	zpráva
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
akutní	akutní	k2eAgInSc4d1	akutní
nedostatek	nedostatek	k1gInSc4	nedostatek
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vůbec	vůbec	k9	vůbec
nějací	nějaký	k3yIgMnPc1	nějaký
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jim	on	k3xPp3gMnPc3	on
chybí	chybět	k5eAaImIp3nS	chybět
patřičné	patřičný	k2eAgNnSc4d1	patřičné
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
operace	operace	k1gFnPc1	operace
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
prováděny	provádět	k5eAaImNgInP	provádět
bez	bez	k7c2	bez
anestetik	anestetikum	k1gNnPc2	anestetikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
přivázání	přivázání	k1gNnPc2	přivázání
pacienta	pacient	k1gMnSc2	pacient
řemeny	řemen	k1gInPc7	řemen
k	k	k7c3	k
operačnímu	operační	k2eAgInSc3d1	operační
stolu	stol	k1gInSc3	stol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
nejsou	být	k5eNaImIp3nP	být
dodržována	dodržován	k2eAgNnPc1d1	dodržováno
ani	ani	k8xC	ani
ta	ten	k3xDgNnPc1	ten
nejzákladnější	základní	k2eAgNnPc1d3	nejzákladnější
hygienická	hygienický	k2eAgNnPc1d1	hygienické
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
injekční	injekční	k2eAgFnPc1d1	injekční
jehly	jehla	k1gFnPc1	jehla
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
nesterilní	sterilní	k2eNgFnPc1d1	nesterilní
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc1	nástroj
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
operacím	operace	k1gFnPc3	operace
se	se	k3xPyFc4	se
rovněž	rovněž	k6eAd1	rovněž
nesterilizují	sterilizovat	k5eNaImIp3nP	sterilizovat
a	a	k8xC	a
operace	operace	k1gFnPc1	operace
se	se	k3xPyFc4	se
dosti	dosti	k6eAd1	dosti
často	často	k6eAd1	často
provádějí	provádět	k5eAaImIp3nP	provádět
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
svíčkách	svíčka	k1gFnPc6	svíčka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
výpadkům	výpadek	k1gInPc3	výpadek
dodávek	dodávka	k1gFnPc2	dodávka
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
a	a	k8xC	a
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
soustavně	soustavně	k6eAd1	soustavně
porušována	porušován	k2eAgNnPc4d1	porušováno
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
mnoha	mnoho	k4c2	mnoho
organizací	organizace	k1gFnPc2	organizace
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
vlád	vláda	k1gFnPc2	vláda
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
severokorejskou	severokorejský	k2eAgFnSc4d1	severokorejská
vládu	vláda	k1gFnSc4	vláda
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
omezování	omezování	k1gNnSc2	omezování
<g/>
;	;	kIx,	;
nejvíce	nejvíce	k6eAd1	nejvíce
kritizováno	kritizován	k2eAgNnSc1d1	kritizováno
je	být	k5eAaImIp3nS	být
omezování	omezování	k1gNnSc1	omezování
práva	právo	k1gNnSc2	právo
Severokorejců	Severokorejec	k1gMnPc2	Severokorejec
na	na	k7c4	na
svobodné	svobodný	k2eAgNnSc4d1	svobodné
opuštění	opuštění	k1gNnSc4	opuštění
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
je	být	k5eAaImIp3nS	být
obviňována	obviňovat	k5eAaImNgFnS	obviňovat
ze	z	k7c2	z
zřizování	zřizování	k1gNnSc2	zřizování
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Jodok	Jodok	k1gInSc1	Jodok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zadržováno	zadržovat	k5eAaImNgNnS	zadržovat
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
"	"	kIx"	"
<g/>
třídními	třídní	k2eAgMnPc7d1	třídní
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
"	"	kIx"	"
zavírány	zavírán	k2eAgFnPc1d1	zavírána
i	i	k8xC	i
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
pořadů	pořad	k1gInPc2	pořad
televize	televize	k1gFnSc2	televize
BBC	BBC	kA	BBC
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
táborů	tábor	k1gInPc2	tábor
testují	testovat	k5eAaImIp3nP	testovat
Severokorejci	Severokorejec	k1gMnPc1	Severokorejec
na	na	k7c6	na
vězních	vězeň	k1gMnPc6	vězeň
chemické	chemický	k2eAgFnSc2d1	chemická
zbraně	zbraň	k1gFnPc1	zbraň
v	v	k7c6	v
plynové	plynový	k2eAgFnSc6d1	plynová
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
média	médium	k1gNnPc1	médium
jsou	být	k5eAaImIp3nP	být
státní	státní	k2eAgFnPc1d1	státní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
cenzurována	cenzurován	k2eAgNnPc1d1	cenzurováno
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
je	být	k5eAaImIp3nS	být
cenzurován	cenzurován	k2eAgInSc1d1	cenzurován
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zamezen	zamezen	k2eAgInSc4d1	zamezen
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
stránky	stránka	k1gFnPc4	stránka
odporující	odporující	k2eAgFnPc4d1	odporující
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
intranet	intranet	k1gInSc4	intranet
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
severokorejskou	severokorejský	k2eAgFnSc7d1	severokorejská
doménou	doména	k1gFnSc7	doména
.	.	kIx.	.
<g/>
kp	kp	k?	kp
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
pouze	pouze	k6eAd1	pouze
28	[number]	k4	28
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Reklama	reklama	k1gFnSc1	reklama
se	se	k3xPyFc4	se
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
dostala	dostat	k5eAaPmAgFnS	dostat
poprvé	poprvé	k6eAd1	poprvé
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Fastfood	Fastfood	k1gInSc1	Fastfood
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
zakázán	zakázat	k5eAaPmNgInS	zakázat
jako	jako	k8xC	jako
západní	západní	k2eAgFnSc1d1	západní
zhýčkanost	zhýčkanost	k1gFnSc1	zhýčkanost
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
obchodě	obchod	k1gInSc6	obchod
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
povoleno	povolen	k2eAgNnSc1d1	povoleno
koupit	koupit	k5eAaPmF	koupit
si	se	k3xPyFc3	se
hot	hot	k0	hot
dog	doga	k1gFnPc2	doga
nebo	nebo	k8xC	nebo
hamburger	hamburger	k1gInSc4	hamburger
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
měsíční	měsíční	k2eAgInSc1d1	měsíční
plat	plat	k1gInSc1	plat
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
dolaru	dolar	k1gInSc2	dolar
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vláda	vláda	k1gFnSc1	vláda
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
ošacení	ošacení	k1gNnSc2	ošacení
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
roka	rok	k1gInSc2	rok
nové	nový	k2eAgFnSc2d1	nová
boty	bota	k1gFnSc2	bota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Potraviny	potravina	k1gFnPc4	potravina
dostávají	dostávat	k5eAaImIp3nP	dostávat
občané	občan	k1gMnPc1	občan
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
příděly	příděl	k1gInPc1	příděl
jsou	být	k5eAaImIp3nP	být
rozdělovány	rozdělovat	k5eAaImNgInP	rozdělovat
podle	podle	k7c2	podle
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
společenské	společenský	k2eAgFnSc2d1	společenská
kategorie	kategorie	k1gFnSc2	kategorie
apod.	apod.	kA	apod.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
nákupu	nákup	k1gInSc3	nákup
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
hodinky	hodinka	k1gFnPc1	hodinka
apod.	apod.	kA	apod.
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
souhlas	souhlas	k1gInSc1	souhlas
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
sledovat	sledovat	k5eAaImF	sledovat
či	či	k8xC	či
poslouchat	poslouchat	k5eAaImF	poslouchat
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
jiné	jiný	k2eAgFnPc4d1	jiná
stanice	stanice	k1gFnPc4	stanice
než	než	k8xS	než
severokorejské	severokorejský	k2eAgInPc4d1	severokorejský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
televizních	televizní	k2eAgInPc6d1	televizní
knoflících	knoflík	k1gInPc6	knoflík
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
papírové	papírový	k2eAgInPc1d1	papírový
plomby	plomb	k1gInPc1	plomb
<g/>
,	,	kIx,	,
tak	tak	k9	tak
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
přepnutí	přepnutí	k1gNnSc3	přepnutí
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
příslušné	příslušný	k2eAgInPc1d1	příslušný
úřady	úřad	k1gInPc1	úřad
provádějí	provádět	k5eAaImIp3nP	provádět
namátkové	namátkový	k2eAgFnPc4d1	namátková
kontroly	kontrola	k1gFnPc4	kontrola
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nejsou	být	k5eNaImIp3nP	být
plomby	plomb	k1gInPc1	plomb
porušeny	porušit	k5eAaPmNgInP	porušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
domácnosti	domácnost	k1gFnSc6	domácnost
musí	muset	k5eAaImIp3nS	muset
viset	viset	k5eAaImF	viset
portréty	portrét	k1gInPc4	portrét
"	"	kIx"	"
<g/>
velkých	velká	k1gFnPc2	velká
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kim	Kim	k1gFnSc1	Kim
Ir-sen	Iren	k1gInSc1	Ir-sen
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc1	Kim
Čong-il	Čongl	k1gMnSc1	Čong-il
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc1	Kim
Čong-un	Čongn	k1gMnSc1	Čong-un
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
tyto	tento	k3xDgInPc4	tento
portréty	portrét	k1gInPc4	portrét
otírány	otírán	k2eAgInPc4d1	otírán
od	od	k7c2	od
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
určen	určen	k2eAgInSc1d1	určen
i	i	k8xC	i
speciální	speciální	k2eAgInSc1d1	speciální
bílý	bílý	k2eAgInSc1d1	bílý
hadřík	hadřík	k1gInSc1	hadřík
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
rovněž	rovněž	k9	rovněž
nosí	nosit	k5eAaImIp3nP	nosit
odznáčky	odznáček	k1gInPc1	odznáček
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgMnPc6	který
jsou	být	k5eAaImIp3nP	být
tito	tento	k3xDgMnPc1	tento
vůdci	vůdce	k1gMnPc1	vůdce
vyobrazeni	vyobrazit	k5eAaPmNgMnP	vyobrazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
domech	dům	k1gInPc6	dům
hlídají	hlídat	k5eAaImIp3nP	hlídat
své	svůj	k3xOyFgMnPc4	svůj
spolubydlící	spolubydlící	k1gMnPc4	spolubydlící
takzvané	takzvaný	k2eAgNnSc1d1	takzvané
"	"	kIx"	"
<g/>
inmimbandžang	inmimbandžang	k1gInSc1	inmimbandžang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
předávat	předávat	k5eAaImF	předávat
zprávy	zpráva	k1gFnPc4	zpráva
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
musí	muset	k5eAaImIp3nS	muset
postarat	postarat	k5eAaPmF	postarat
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
otcově	otcův	k2eAgFnSc6d1	otcova
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Propaganda	propaganda	k1gFnSc1	propaganda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
tak	tak	k6eAd1	tak
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
lidí	člověk	k1gMnPc2	člověk
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
žije	žít	k5eAaImIp3nS	žít
nejlépe	dobře	k6eAd3	dobře
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jsou	být	k5eAaImIp3nP	být
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
vychováváni	vychovávat	k5eAaImNgMnP	vychovávat
k	k	k7c3	k
poslušnosti	poslušnost	k1gFnSc3	poslušnost
a	a	k8xC	a
absolutně	absolutně	k6eAd1	absolutně
odrazování	odrazování	k1gNnSc1	odrazování
od	od	k7c2	od
demokratického	demokratický	k2eAgInSc2d1	demokratický
a	a	k8xC	a
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
používat	používat	k5eAaImF	používat
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
nedostupné	dostupný	k2eNgFnPc4d1	nedostupná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
klasický	klasický	k2eAgInSc4d1	klasický
tradiční	tradiční	k2eAgInSc4d1	tradiční
pokrm	pokrm	k1gInSc4	pokrm
patří	patřit	k5eAaImIp3nP	patřit
korejské	korejský	k2eAgFnPc1d1	Korejská
kimčchi	kimčch	k1gFnPc1	kimčch
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
čínského	čínský	k2eAgNnSc2d1	čínské
zelí	zelí	k1gNnSc2	zelí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
pár	pár	k4xCyI	pár
hodin	hodina	k1gFnPc2	hodina
na	na	k7c4	na
drobno	drobna	k1gFnSc5	drobna
nakrájené	nakrájený	k2eAgFnPc1d1	nakrájená
plavat	plavat	k5eAaImF	plavat
v	v	k7c6	v
solném	solný	k2eAgInSc6d1	solný
nálevu	nálev	k1gInSc6	nálev
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
opláchne	opláchnout	k5eAaPmIp3nS	opláchnout
<g/>
,	,	kIx,	,
prokládá	prokládat	k5eAaImIp3nS	prokládat
se	se	k3xPyFc4	se
s	s	k7c7	s
pastou	pasta	k1gFnSc7	pasta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
chilli	chilli	k1gNnSc2	chilli
<g/>
,	,	kIx,	,
zázvoru	zázvor	k1gInSc2	zázvor
<g/>
,	,	kIx,	,
česneku	česnek	k1gInSc2	česnek
<g/>
,	,	kIx,	,
jarní	jarní	k2eAgFnSc2d1	jarní
cibulky	cibulka	k1gFnSc2	cibulka
a	a	k8xC	a
ústřicovou	ústřicový	k2eAgFnSc4d1	ústřicová
pastu	pasta	k1gFnSc4	pasta
<g/>
.	.	kIx.	.
</s>
<s>
Nechává	nechávat	k5eAaImIp3nS	nechávat
se	s	k7c7	s
14	[number]	k4	14
dní	den	k1gInPc2	den
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
kvasit	kvasit	k5eAaImF	kvasit
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
optimistické	optimistický	k2eAgFnPc4d1	optimistická
melodie	melodie	k1gFnPc4	melodie
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
lidové	lidový	k2eAgFnSc6d1	lidová
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
také	také	k9	také
revolucionářskou	revolucionářský	k2eAgFnSc4d1	revolucionářská
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Ideologické	ideologický	k2eAgFnPc1d1	ideologická
zprávy	zpráva	k1gFnPc1	zpráva
jsou	být	k5eAaImIp3nP	být
sdělovány	sdělovat	k5eAaImNgFnP	sdělovat
přes	přes	k7c4	přes
masivní	masivní	k2eAgFnPc4d1	masivní
orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Pět	pět	k4xCc1	pět
velkých	velký	k2eAgFnPc2d1	velká
revolučních	revoluční	k2eAgFnPc2d1	revoluční
oper	opera	k1gFnPc2	opera
<g/>
,	,	kIx,	,
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
tradičním	tradiční	k2eAgMnSc6d1	tradiční
korejském	korejský	k2eAgMnSc6d1	korejský
changgeuk	changgeuk	k6eAd1	changgeuk
(	(	kIx(	(
<g/>
korejská	korejský	k2eAgFnSc1d1	Korejská
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1	revoluční
opery	opera	k1gFnPc1	opera
se	se	k3xPyFc4	se
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
západních	západní	k2eAgInPc2d1	západní
protějšků	protějšek	k1gInPc2	protějšek
liší	lišit	k5eAaImIp3nS	lišit
přidáním	přidání	k1gNnSc7	přidání
tradičních	tradiční	k2eAgInPc2d1	tradiční
nástrojů	nástroj	k1gInPc2	nástroj
do	do	k7c2	do
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
recitativu	recitativ	k1gInSc2	recitativ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
zde	zde	k6eAd1	zde
neexistuje	existovat	k5eNaImIp3nS	existovat
žádné	žádný	k3yNgNnSc4	žádný
literární	literární	k2eAgNnSc4d1	literární
podzemí	podzemí	k1gNnSc4	podzemí
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
žádní	žádný	k3yNgMnPc1	žádný
disidenti-spisovatelé	disidentipisovatel	k1gMnPc1	disidenti-spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
strany	strana	k1gFnSc2	strana
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
důležitý	důležitý	k2eAgInSc4d1	důležitý
nástroj	nástroj	k1gInSc4	nástroj
propagandy	propaganda	k1gFnSc2	propaganda
a	a	k8xC	a
agitace	agitace	k1gFnSc2	agitace
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
Koreje	Korea	k1gFnSc2	Korea
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
největší	veliký	k2eAgFnSc1d3	veliký
autoritu	autorita	k1gFnSc4	autorita
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
veškeré	veškerý	k3xTgFnPc4	veškerý
práce	práce	k1gFnPc4	práce
Kim	Kim	k1gFnSc1	Kim
Ir-sena	Iren	k2eAgFnSc1d1	Ir-sena
<g/>
,	,	kIx,	,
ideologické	ideologický	k2eAgInPc1d1	ideologický
vzdělávací	vzdělávací	k2eAgInPc1d1	vzdělávací
materiály	materiál	k1gInPc1	materiál
a	a	k8xC	a
dokumentů	dokument	k1gInPc2	dokument
stranické	stranický	k2eAgFnSc2d1	stranická
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
literatura	literatura	k1gFnSc1	literatura
zde	zde	k6eAd1	zde
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
indické	indický	k2eAgInPc1d1	indický
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgFnSc2d1	čínská
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
Tales	Tales	k1gMnSc1	Tales
from	from	k1gMnSc1	from
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
práce	práce	k1gFnPc1	práce
Bertolta	Bertolt	k1gInSc2	Bertolt
Brechta	Brecht	k1gMnSc2	Brecht
a	a	k8xC	a
Ericha	Erich	k1gMnSc2	Erich
Kästnera	Kästner	k1gMnSc2	Kästner
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
Ir-senovy	Irenův	k2eAgFnSc2d1	Ir-senův
osobní	osobní	k2eAgFnSc2d1	osobní
práce	práce	k1gFnSc2	práce
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
"	"	kIx"	"
<g/>
klasická	klasický	k2eAgNnPc1d1	klasické
mistrovská	mistrovský	k2eAgNnPc1d1	mistrovské
díla	dílo	k1gNnPc1	dílo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ta	ten	k3xDgFnSc1	ten
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
instruktáží	instruktáž	k1gFnSc7	instruktáž
jsou	být	k5eAaImIp3nP	být
označena	označit	k5eAaPmNgNnP	označit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
modely	model	k1gInPc1	model
literatury	literatura	k1gFnSc2	literatura
čučche	čučch	k1gFnSc2	čučch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
Osud	osud	k1gInSc4	osud
mužů	muž	k1gMnPc2	muž
obranného	obranný	k2eAgInSc2d1	obranný
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
historie	historie	k1gFnSc1	historie
či	či	k8xC	či
série	série	k1gFnSc1	série
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
zobrazující	zobrazující	k2eAgNnSc1d1	zobrazující
utrpení	utrpení	k1gNnSc1	utrpení
Korejců	Korejec	k1gMnPc2	Korejec
pod	pod	k7c7	pod
japonskou	japonský	k2eAgFnSc7d1	japonská
okupací	okupace	k1gFnSc7	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
po	po	k7c4	po
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
publikováno	publikovat	k5eAaBmNgNnS	publikovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgInPc4	čtyři
miliony	milion	k4xCgInPc4	milion
literárních	literární	k2eAgFnPc2d1	literární
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úzkého	úzký	k2eAgInSc2d1	úzký
výběru	výběr	k1gInSc2	výběr
různých	různý	k2eAgInPc2d1	různý
politických	politický	k2eAgInPc2d1	politický
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc1	první
armádní	armádní	k2eAgFnSc1d1	armádní
revoluční	revoluční	k2eAgFnSc1d1	revoluční
literatura	literatura	k1gFnSc1	literatura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sci-fi	scii	k1gFnSc1	sci-fi
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
druhořadý	druhořadý	k2eAgInSc4d1	druhořadý
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
poněkud	poněkud	k6eAd1	poněkud
odchyluje	odchylovat	k5eAaImIp3nS	odchylovat
od	od	k7c2	od
tradičních	tradiční	k2eAgFnPc2d1	tradiční
norem	norma	k1gFnPc2	norma
podrobných	podrobný	k2eAgInPc2d1	podrobný
popisů	popis	k1gInPc2	popis
a	a	k8xC	a
metafor	metafora	k1gFnPc2	metafora
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
.	.	kIx.	.
</s>
