<s>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
KLDR	KLDR	kA	KLDR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
