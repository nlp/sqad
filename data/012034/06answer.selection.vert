<s>
Státní	státní	k2eAgFnSc1d1	státní
poznávací	poznávací	k2eAgFnSc1d1	poznávací
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SPZ	SPZ	kA	SPZ
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
espézetka	espézetka	k1gFnSc1	espézetka
<g/>
,	,	kIx,	,
též	též	k9	též
rozeznávací	rozeznávací	k2eAgFnSc1d1	rozeznávací
<g/>
,	,	kIx,	,
evidenční	evidenční	k2eAgFnSc1d1	evidenční
<g/>
,	,	kIx,	,
rejstříková	rejstříkový	k2eAgFnSc1d1	rejstříková
nebo	nebo	k8xC	nebo
policejní	policejní	k2eAgFnSc1d1	policejní
značka	značka	k1gFnSc1	značka
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
legislativní	legislativní	k2eAgFnSc2d1	legislativní
zkratky	zkratka	k1gFnSc2	zkratka
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
zákoně	zákon	k1gInSc6	zákon
také	také	k9	také
registrační	registrační	k2eAgFnSc1d1	registrační
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
RZ	RZ	kA	RZ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
písmeno-číselné	písmeno-číselný	k2eAgNnSc1d1	písmeno-číselný
označení	označení	k1gNnSc1	označení
motorového	motorový	k2eAgNnSc2d1	motorové
vozidla	vozidlo	k1gNnSc2	vozidlo
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc2	jeho
přívěsu	přívěs	k1gInSc2	přívěs
či	či	k8xC	či
návěsu	návěs	k1gInSc2	návěs
<g/>
,	,	kIx,	,
zaregistrovaného	zaregistrovaný	k2eAgInSc2d1	zaregistrovaný
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
státu	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
