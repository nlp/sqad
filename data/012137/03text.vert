<p>
<s>
Pulsary	pulsar	k1gInPc1	pulsar
jsou	být	k5eAaImIp3nP	být
rotující	rotující	k2eAgFnPc4d1	rotující
neutronové	neutronový	k2eAgFnPc4d1	neutronová
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
záření	záření	k1gNnSc2	záření
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vzdáleného	vzdálený	k2eAgMnSc4d1	vzdálený
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
pravidelně	pravidelně	k6eAd1	pravidelně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rotací	rotace	k1gFnSc7	rotace
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
majákový	majákový	k2eAgInSc4d1	majákový
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Werner	Werner	k1gMnSc1	Werner
Becker	Becker	k1gMnSc1	Becker
z	z	k7c2	z
Max-Planck-Institut	Max-Planck-Institut	k1gMnSc1	Max-Planck-Institut
für	für	k?	für
extraterrestrische	extraterrestrische	k6eAd1	extraterrestrische
Physik	Physik	k1gMnSc1	Physik
řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pulsary	pulsar	k1gInPc1	pulsar
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
svoji	svůj	k3xOyFgFnSc4	svůj
radiaci	radiace	k1gFnSc4	radiace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
po	po	k7c6	po
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
letech	léto	k1gNnPc6	léto
práce	práce	k1gFnSc2	práce
<g/>
...	...	k?	...
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
přijatá	přijatý	k2eAgFnSc1d1	přijatá
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
...	...	k?	...
Teprve	teprve	k6eAd1	teprve
poslední	poslední	k2eAgInPc4d1	poslední
poznatky	poznatek	k1gInPc4	poznatek
nám	my	k3xPp1nPc3	my
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vytvoření	vytvoření	k1gNnSc2	vytvoření
přesnější	přesný	k2eAgFnSc2d2	přesnější
představy	představa	k1gFnSc2	představa
o	o	k7c4	o
vyzařování	vyzařování	k1gNnSc4	vyzařování
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objev	k1gInSc4	objev
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
pulsar	pulsar	k1gInSc4	pulsar
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
Jocelyn	Jocelyno	k1gNnPc2	Jocelyno
Bellová	Bellová	k1gFnSc1	Bellová
Burnellová	Burnellová	k1gFnSc1	Burnellová
a	a	k8xC	a
Antony	anton	k1gInPc4	anton
Hewish	Hewisha	k1gFnPc2	Hewisha
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
původně	původně	k6eAd1	původně
popletení	popletený	k2eAgMnPc1d1	popletený
nepřirozeně	přirozeně	k6eNd1	přirozeně
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
vyzařováním	vyzařování	k1gNnSc7	vyzařování
pulsaru	pulsar	k1gInSc2	pulsar
<g/>
,	,	kIx,	,
nazvali	nazvat	k5eAaPmAgMnP	nazvat
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
LGM-1	LGM-1	k1gFnSc2	LGM-1
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
little	little	k6eAd1	little
green	green	k1gInSc1	green
men	men	k?	men
=	=	kIx~	=
malí	malý	k1gMnPc1	malý
zelení	zelenit	k5eAaImIp3nP	zelenit
mužíčci	mužíček	k1gMnPc1	mužíček
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
pulsar	pulsar	k1gInSc4	pulsar
nazvali	nazvat	k5eAaPmAgMnP	nazvat
PSR	PSR	kA	PSR
1919	[number]	k4	1919
<g/>
+	+	kIx~	+
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pulsar	pulsar	k1gInSc1	pulsar
je	být	k5eAaImIp3nS	být
složenina	složenina	k1gFnSc1	složenina
z	z	k7c2	z
"	"	kIx"	"
<g/>
pulsating	pulsating	k1gInSc1	pulsating
star	star	k1gFnSc2	star
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pulzující	pulzující	k2eAgFnSc1d1	pulzující
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Úplně	úplně	k6eAd1	úplně
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
...	...	k?	...
začala	začít	k5eAaPmAgFnS	začít
zářit	zářit	k5eAaImF	zářit
loni	loni	k6eAd1	loni
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
<g/>
...	...	k?	...
astronomové	astronom	k1gMnPc1	astronom
jí	on	k3xPp3gFnSc7	on
začali	začít	k5eAaPmAgMnP	začít
říkat	říkat	k5eAaImF	říkat
LGM	LGM	kA	LGM
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
<g/>
...	...	k?	...
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typ	typ	k1gInSc1	typ
hvězdy	hvězda	k1gFnSc2	hvězda
mezi	mezi	k7c7	mezi
bílým	bílý	k1gMnSc7	bílý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
a	a	k8xC	a
neutronovou	neutronový	k2eAgFnSc7d1	neutronová
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pulsar	pulsar	k1gInSc1	pulsar
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
prostě	prostě	k6eAd1	prostě
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
...	...	k?	...
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
A.	A.	kA	A.
Hewish	Hewish	k1gInSc1	Hewish
<g/>
...	...	k?	...
mi	já	k3xPp1nSc3	já
včera	včera	k6eAd1	včera
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
každý	každý	k3xTgInSc1	každý
radioteleskop	radioteleskop	k1gInSc1	radioteleskop
dívá	dívat	k5eAaImIp3nS	dívat
na	na	k7c4	na
pulsary	pulsar	k1gInPc4	pulsar
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
Peter	Peter	k1gMnSc1	Peter
A.	A.	kA	A.
Sturrock	Sturrock	k1gMnSc1	Sturrock
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
objeveny	objevit	k5eAaPmNgInP	objevit
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
radiové	radiový	k2eAgInPc1d1	radiový
signály	signál	k1gInPc1	signál
z	z	k7c2	z
pulsarů	pulsar	k1gInPc2	pulsar
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
vážně	vážně	k6eAd1	vážně
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
od	od	k7c2	od
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Projednali	projednat	k5eAaPmAgMnP	projednat
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
to	ten	k3xDgNnSc1	ten
nesmí	smět	k5eNaImIp3nS	smět
pustit	pustit	k5eAaPmF	pustit
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
vyšších	vysoký	k2eAgFnPc2d2	vyšší
autorit	autorita	k1gFnPc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
zájmu	zájem	k1gInSc6	zájem
lidstva	lidstvo	k1gNnSc2	lidstvo
zničit	zničit	k5eAaPmF	zničit
důkazy	důkaz	k1gInPc4	důkaz
a	a	k8xC	a
na	na	k7c4	na
všechno	všechen	k3xTgNnSc4	všechen
zapomenout	zapomenout	k5eAaPmF	zapomenout
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
CP	CP	kA	CP
1919	[number]	k4	1919
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
rádiové	rádiový	k2eAgFnSc2d1	rádiová
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
také	také	k9	také
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
nebo	nebo	k8xC	nebo
gamma	gamma	k1gNnSc4	gamma
paprsky	paprsek	k1gInPc4	paprsek
nebo	nebo	k8xC	nebo
také	také	k9	také
oboje	oboj	k1gFnPc4	oboj
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Antony	anton	k1gInPc4	anton
Hewish	Hewisha	k1gFnPc2	Hewisha
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgNnSc1d1	spojené
práce	práce	k1gFnPc4	práce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
radioastronomie	radioastronomie	k1gFnSc2	radioastronomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poruchy	porucha	k1gFnPc4	porucha
a	a	k8xC	a
kolísání	kolísání	k1gNnPc4	kolísání
===	===	k?	===
</s>
</p>
<p>
<s>
Pulsy	puls	k1gInPc1	puls
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
přesně	přesně	k6eAd1	přesně
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
náhlé	náhlý	k2eAgFnPc4d1	náhlá
skokové	skokový	k2eAgFnPc4d1	skoková
poruchy	porucha	k1gFnPc4	porucha
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
glitch	glitch	k1gInSc1	glitch
<g/>
)	)	kIx)	)
v	v	k7c6	v
periodách	perioda	k1gFnPc6	perioda
rotace	rotace	k1gFnSc2	rotace
pulsarů	pulsar	k1gInPc2	pulsar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
astronom	astronom	k1gMnSc1	astronom
John	John	k1gMnSc1	John
Middleditch	Middleditch	k1gMnSc1	Middleditch
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tým	tým	k1gInSc4	tým
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
první	první	k4xOgFnSc4	první
předpověď	předpověď	k1gFnSc4	předpověď
poruch	porucha	k1gFnPc2	porucha
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
napozorovanými	napozorovaný	k2eAgNnPc7d1	napozorovaný
z	z	k7c2	z
Rossi	Ross	k1gMnPc7	Ross
X-ray	Xaa	k1gFnSc2	X-raa
Timing	Timing	k1gInSc1	Timing
Explorer	Explorer	k1gInSc1	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Použili	použít	k5eAaPmAgMnP	použít
měření	měření	k1gNnSc4	měření
pulsaru	pulsar	k1gInSc2	pulsar
PSR	PSR	kA	PSR
J	J	kA	J
<g/>
0	[number]	k4	0
<g/>
537	[number]	k4	537
<g/>
-	-	kIx~	-
<g/>
6910	[number]	k4	6910
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
kolísání	kolísání	k1gNnSc1	kolísání
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
wobble	wobble	k6eAd1	wobble
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
odporují	odporovat	k5eAaImIp3nP	odporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
pulsarů	pulsar	k1gInPc2	pulsar
==	==	k?	==
</s>
</p>
<p>
<s>
Astronomové	astronom	k1gMnPc1	astronom
dnes	dnes	k6eAd1	dnes
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
pulsarů	pulsar	k1gInPc2	pulsar
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pohání	pohánět	k5eAaImIp3nS	pohánět
radiaci	radiace	k1gFnSc4	radiace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rotací	rotace	k1gFnSc7	rotace
poháněné	poháněný	k2eAgInPc1d1	poháněný
pulsary	pulsar	k1gInPc1	pulsar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztráta	ztráta	k1gFnSc1	ztráta
rotační	rotační	k2eAgFnSc2d1	rotační
energie	energie	k1gFnSc2	energie
hvězdy	hvězda	k1gFnSc2	hvězda
pohání	pohánět	k5eAaImIp3nS	pohánět
radiaci	radiace	k1gFnSc4	radiace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pulsary	pulsar	k1gInPc1	pulsar
poháněné	poháněný	k2eAgInPc1d1	poháněný
přírůstkem	přírůstek	k1gInSc7	přírůstek
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
rentgenové	rentgenový	k2eAgInPc1d1	rentgenový
pulsary	pulsar	k1gInPc1	pulsar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
gravitační	gravitační	k2eAgFnSc1d1	gravitační
energie	energie	k1gFnSc1	energie
z	z	k7c2	z
přirůstající	přirůstající	k2eAgFnSc2d1	přirůstající
hmoty	hmota	k1gFnSc2	hmota
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
pozorovatelné	pozorovatelný	k2eAgNnSc4d1	pozorovatelné
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetary	Magnetar	k1gInPc1	Magnetar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
radiaci	radiace	k1gFnSc4	radiace
pohání	pohánět	k5eAaImIp3nS	pohánět
rozklad	rozklad	k1gInSc1	rozklad
extrémně	extrémně	k6eAd1	extrémně
silného	silný	k2eAgNnSc2d1	silné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
třech	tři	k4xCgInPc6	tři
případech	případ	k1gInPc6	případ
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
neutronové	neutronový	k2eAgFnPc4d1	neutronová
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
základ	základ	k1gInSc1	základ
se	se	k3xPyFc4	se
dost	dost	k6eAd1	dost
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mají	mít	k5eAaImIp3nP	mít
určitá	určitý	k2eAgNnPc4d1	určité
spojení	spojení	k1gNnPc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rentgenové	rentgenový	k2eAgInPc1d1	rentgenový
pulsary	pulsar	k1gInPc1	pulsar
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
staré	starý	k2eAgInPc1d1	starý
rotační	rotační	k2eAgInPc1d1	rotační
pulsary	pulsar	k1gInPc1	pulsar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
už	už	k6eAd1	už
ztratily	ztratit	k5eAaPmAgInP	ztratit
většinu	většina	k1gFnSc4	většina
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
jen	jen	k9	jen
poté	poté	k6eAd1	poté
co	co	k9	co
jejich	jejich	k3xOp3gMnSc1	jejich
společník	společník	k1gMnSc1	společník
(	(	kIx(	(
<g/>
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
)	)	kIx)	)
naroste	narůst	k5eAaPmIp3nS	narůst
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
předávat	předávat	k5eAaImF	předávat
svou	svůj	k3xOyFgFnSc4	svůj
hmotu	hmota	k1gFnSc4	hmota
neutronové	neutronový	k2eAgFnSc3d1	neutronová
hvězdě	hvězda	k1gFnSc3	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
narůstání	narůstání	k1gNnSc2	narůstání
může	moct	k5eAaImIp3nS	moct
zase	zase	k9	zase
předat	předat	k5eAaPmF	předat
dostatek	dostatek	k1gInSc4	dostatek
úhlové	úhlový	k2eAgFnSc2d1	úhlová
rychlosti	rychlost	k1gFnSc2	rychlost
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdě	hvězda	k1gFnSc3	hvězda
a	a	k8xC	a
ta	ten	k3xDgNnPc1	ten
ji	on	k3xPp3gFnSc4	on
začne	začít	k5eAaPmIp3nS	začít
"	"	kIx"	"
<g/>
recyklovat	recyklovat	k5eAaBmF	recyklovat
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
rotací	rotace	k1gFnSc7	rotace
poháněný	poháněný	k2eAgInSc1d1	poháněný
milisekundový	milisekundový	k2eAgInSc1d1	milisekundový
pulsar	pulsar	k1gInSc1	pulsar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
pulsarů	pulsar	k1gInPc2	pulsar
se	se	k3xPyFc4	se
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
výsledky	výsledek	k1gInPc4	výsledek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
potvrzení	potvrzení	k1gNnSc1	potvrzení
existence	existence	k1gFnSc2	existence
gravitační	gravitační	k2eAgFnSc2d1	gravitační
radiace	radiace	k1gFnSc2	radiace
tak	tak	k6eAd1	tak
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
předpověděla	předpovědět	k5eAaPmAgFnS	předpovědět
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
první	první	k4xOgNnSc1	první
objevení	objevení	k1gNnSc1	objevení
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
mimo	mimo	k7c4	mimo
naši	náš	k3xOp1gFnSc4	náš
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
pulsary	pulsar	k1gInPc1	pulsar
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
rádiový	rádiový	k2eAgInSc1d1	rádiový
pulsar	pulsar	k1gInSc1	pulsar
<g/>
,	,	kIx,	,
CP1919	CP1919	k1gFnSc1	CP1919
(	(	kIx(	(
<g/>
teď	teď	k6eAd1	teď
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
CP	CP	kA	CP
<g/>
1919	[number]	k4	1919
<g/>
+	+	kIx~	+
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
s	s	k7c7	s
pulsační	pulsační	k2eAgFnSc7d1	pulsační
periodou	perioda	k1gFnSc7	perioda
1,337	[number]	k4	1,337
sekundy	sekunda	k1gFnSc2	sekunda
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
pulsu	puls	k1gInSc2	puls
0,04	[number]	k4	0,04
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
pulsar	pulsar	k1gInSc4	pulsar
<g/>
,	,	kIx,	,
PSR	PSR	kA	PSR
1913	[number]	k4	1913
<g/>
+	+	kIx~	+
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgInS	dokázat
existenci	existence	k1gFnSc4	existence
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
milisekundový	milisekundový	k2eAgInSc1d1	milisekundový
pulsar	pulsar	k1gInSc1	pulsar
<g/>
,	,	kIx,	,
PSR	PSR	kA	PSR
B	B	kA	B
<g/>
1937	[number]	k4	1937
<g/>
+	+	kIx~	+
<g/>
21	[number]	k4	21
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
rentgenový	rentgenový	k2eAgInSc1d1	rentgenový
pulsar	pulsar	k1gInSc1	pulsar
<g/>
,	,	kIx,	,
Cen	cena	k1gFnPc2	cena
X-3	X-3	k1gFnPc2	X-3
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
přírůstkový	přírůstkový	k2eAgInSc1d1	přírůstkový
milisekundový	milisekundový	k2eAgInSc1d1	milisekundový
pulsar	pulsar	k1gInSc1	pulsar
<g/>
,	,	kIx,	,
SAX	sax	k1gInSc1	sax
J	J	kA	J
<g/>
1808,4	[number]	k4	1808,4
<g/>
-	-	kIx~	-
<g/>
3658	[number]	k4	3658
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
pulsar	pulsar	k1gInSc1	pulsar
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
<g/>
,	,	kIx,	,
PSR	PSR	kA	PSR
B	B	kA	B
<g/>
1257	[number]	k4	1257
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
pulsar	pulsar	k1gInSc4	pulsar
<g/>
,	,	kIx,	,
PSR	PSR	kA	PSR
J0737-3039	J0737-3039	k1gFnSc1	J0737-3039
</s>
</p>
<p>
<s>
Magnetar	Magnetar	k1gInSc1	Magnetar
SGR	SGR	kA	SGR
1806-20	[number]	k4	1806-20
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
největší	veliký	k2eAgInSc1d3	veliký
výbuch	výbuch	k1gInSc1	výbuch
energie	energie	k1gFnSc2	energie
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
PSR	PSR	kA	PSR
B	B	kA	B
<g/>
1931	[number]	k4	1931
<g/>
+	+	kIx~	+
<g/>
24	[number]	k4	24
"	"	kIx"	"
<g/>
...	...	k?	...
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
normální	normální	k2eAgInSc4d1	normální
pulsar	pulsar	k1gInSc4	pulsar
asi	asi	k9	asi
týden	týden	k1gInSc4	týden
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
vypne	vypnout	k5eAaPmIp3nS	vypnout
asi	asi	k9	asi
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
zase	zase	k9	zase
začne	začít	k5eAaPmIp3nS	začít
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
energii	energie	k1gFnSc4	energie
<g/>
...	...	k?	...
tento	tento	k3xDgInSc4	tento
pulsar	pulsar	k1gInSc4	pulsar
se	se	k3xPyFc4	se
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zapnutý	zapnutý	k2eAgMnSc1d1	zapnutý
<g/>
,	,	kIx,	,
než	než	k8xS	než
když	když	k8xS	když
nefunguje	fungovat	k5eNaImIp3nS	fungovat
<g/>
...	...	k?	...
brzdící	brzdící	k2eAgInSc1d1	brzdící
mechanismus	mechanismus	k1gInSc1	mechanismus
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nějak	nějak	k6eAd1	nějak
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
radiovými	radiový	k2eAgFnPc7d1	radiová
emisemi	emise	k1gFnPc7	emise
a	a	k8xC	a
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	on	k3xPp3gNnSc4	on
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
a	a	k8xC	a
také	také	k9	také
dodatečné	dodatečný	k2eAgNnSc1d1	dodatečné
brzdění	brzdění	k1gNnSc1	brzdění
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
větrem	vítr	k1gInSc7	vítr
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
opouští	opouštět	k5eAaImIp3nS	opouštět
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
pulsaru	pulsar	k1gInSc2	pulsar
a	a	k8xC	a
odnáší	odnášet	k5eAaImIp3nS	odnášet
rotační	rotační	k2eAgFnSc4d1	rotační
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PSR	PSR	kA	PSR
J	J	kA	J
<g/>
1748	[number]	k4	1748
<g/>
-	-	kIx~	-
<g/>
2446	[number]	k4	2446
<g/>
ad	ad	k7c4	ad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
716	[number]	k4	716
Hz	Hz	kA	Hz
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
točícím	točící	k2eAgInSc7d1	točící
známým	známý	k2eAgInSc7d1	známý
pulsarem	pulsar	k1gInSc7	pulsar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRS	prs	k1gInSc1	prs
J	J	kA	J
<g/>
2144	[number]	k4	2144
<g/>
-	-	kIx~	-
<g/>
3933	[number]	k4	3933
<g/>
,	,	kIx,	,
nejpomalejší	pomalý	k2eAgInSc1d3	nejpomalejší
známý	známý	k2eAgInSc1d1	známý
pulsar	pulsar	k1gInSc1	pulsar
<g/>
,	,	kIx,	,
perioda	perioda	k1gFnSc1	perioda
8,51	[number]	k4	8,51
s	s	k7c7	s
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Magnetar	Magnetar	k1gMnSc1	Magnetar
</s>
</p>
<p>
<s>
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pulsar	pulsar	k1gInSc1	pulsar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
pulsaru	pulsar	k1gInSc2	pulsar
-	-	kIx~	-
objevení	objevení	k1gNnSc1	objevení
prvního	první	k4xOgInSc2	první
optického	optický	k2eAgInSc2d1	optický
pulsaru	pulsar	k1gInSc2	pulsar
z	z	k7c2	z
Amerického	americký	k2eAgInSc2d1	americký
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápis	zápis	k1gInSc1	zápis
prvního	první	k4xOgInSc2	první
pulsaru	pulsar	k1gInSc2	pulsar
(	(	kIx(	(
<g/>
PULS	puls	k1gInSc1	puls
CP	CP	kA	CP
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Simbad	Simbad	k1gInSc4	Simbad
</s>
</p>
<p>
<s>
Katalog	katalog	k1gInSc1	katalog
pulsaru	pulsar	k1gInSc2	pulsar
ATNF	ATNF	kA	ATNF
</s>
</p>
<p>
<s>
Objevení	objevení	k1gNnSc1	objevení
pulsarů	pulsar	k1gInPc2	pulsar
na	na	k7c4	na
H2G2	H2G2	k1gFnSc4	H2G2
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
dokážou	dokázat	k5eAaPmIp3nP	dokázat
predvídat	predvídat	k5eAaPmF	predvídat
chování	chování	k1gNnSc4	chování
pulsarů	pulsar	k1gInPc2	pulsar
(	(	kIx(	(
<g/>
SpaceDaily	SpaceDaila	k1gFnSc2	SpaceDaila
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pulsarů	pulsar	k1gInPc2	pulsar
v	v	k7c6	v
binarních	binarní	k2eAgInPc6d1	binarní
systémech	systém	k1gInPc6	systém
</s>
</p>
<p>
<s>
XMM-Newton	XMM-Newton	k1gInSc1	XMM-Newton
Objevuje	objevovat	k5eAaImIp3nS	objevovat
nové	nový	k2eAgFnPc4d1	nová
věci	věc	k1gFnPc4	věc
o	o	k7c6	o
starých	starý	k2eAgInPc6d1	starý
pulsarech	pulsar	k1gInPc6	pulsar
(	(	kIx(	(
<g/>
SpaceDaily	SpaceDaila	k1gFnPc1	SpaceDaila
<g/>
)	)	kIx)	)
27	[number]	k4	27
července	červenec	k1gInSc2	červenec
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
názor	názor	k1gInSc1	názor
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
hvězdy	hvězda	k1gFnPc1	hvězda
chladnou	chladnout	k5eAaImIp3nP	chladnout
Ker	kra	k1gFnPc2	kra
Than	Than	k1gMnSc1	Than
(	(	kIx(	(
<g/>
SPACE	SPACE	kA	SPACE
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
16	[number]	k4	16
</s>
</p>
