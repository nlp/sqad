<s>
Springfield	Springfield	k1gInSc1
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Springfield	Springfield	k6eAd1
City	city	k1gNnSc1
of	of	k?
Springfield	Springfielda	k1gFnPc2
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
72	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
21	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC-	UTC-	k?
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
4	#num#	k4
(	(	kIx(
<g/>
letní	letní	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
)	)	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
federální	federální	k2eAgFnSc2d1
stát	stát	k1gInSc1
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1
okresy	okres	k1gInPc1
</s>
<s>
Hampden	Hampdna	k1gFnPc2
County	Counta	k1gFnSc2
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
86	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
153	#num#	k4
060	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
779,8	779,8	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Domenic	Domenice	k1gFnPc2
J.	J.	kA
Sarno	Sarno	k1gNnSc4
Vznik	vznik	k1gInSc1
</s>
<s>
1636	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.springfieldcityhall.com/COS/	www.springfieldcityhall.com/COS/	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
413	#num#	k4
PSČ	PSČ	kA
</s>
<s>
01101	#num#	k4
01103	#num#	k4
01104	#num#	k4
01105	#num#	k4
01107	#num#	k4
01108	#num#	k4
01118	#num#	k4
01109	#num#	k4
01119	#num#	k4
01128	#num#	k4
01129	#num#	k4
01151	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Springfield	Springfield	k1gInSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
na	na	k7c6
řece	řeka	k1gFnSc6
Connecticut	Connecticut	k1gInSc4
a	a	k8xC
okresní	okresní	k2eAgNnSc4d1
město	město	k1gNnSc4
Hampden	Hampdno	k1gNnPc2
County	Counta	k1gFnSc2
v	v	k7c6
americkém	americký	k2eAgInSc6d1
státě	stát	k1gInSc6
Massachusetts	Massachusetts	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
v	v	k7c6
něm	on	k3xPp3gNnSc6
žilo	žít	k5eAaImAgNnS
153	#num#	k4
060	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
metropolitní	metropolitní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
pak	pak	k6eAd1
žilo	žít	k5eAaImAgNnS
podle	podle	k7c2
odhadu	odhad	k1gInSc2
698	#num#	k4
903	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgNnSc1
město	město	k1gNnSc1
v	v	k7c6
Americe	Amerika	k1gFnSc6
pojmenované	pojmenovaný	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
Springfield	Springfield	k1gInSc1
a	a	k8xC
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
v	v	k7c6
Massachusetts	Massachusetts	k1gNnSc6
a	a	k8xC
čtvrté	čtvrtá	k1gFnSc6
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
(	(	kIx(
<g/>
po	po	k7c6
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
Worcesteru	Worcester	k1gInSc6
a	a	k8xC
Providence	providence	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Springfieldu	Springfield	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
James	James	k1gMnSc1
Naismith	Naismith	k1gMnSc1
vynalezl	vynaleznout	k5eAaPmAgMnS
basketbal	basketbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Oblast	oblast	k1gFnSc1
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
Springfieldem	Springfield	k1gInSc7
a	a	k8xC
Hartfordem	Hartford	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
od	od	k7c2
něho	on	k3xPp3gInSc2
leží	ležet	k5eAaImIp3nS
38,5	38,5	k4
km	km	kA
jižně	jižně	k6eAd1
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
„	„	k?
<g/>
Knowledge	Knowledg	k1gInPc1
Corridor	Corridor	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
nachází	nacházet	k5eAaImIp3nS
32	#num#	k4
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
navštěvuje	navštěvovat	k5eAaImIp3nS
přes	přes	k7c4
160	#num#	k4
000	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Springfield	Springfielda	k1gFnPc2
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Population	Population	k1gInSc1
and	and	k?
Housing	Housing	k1gInSc4
Occupancy	Occupanca	k1gFnSc2
Status	status	k1gInSc1
<g/>
:	:	kIx,
2010	#num#	k4
–	–	k?
State	status	k1gInSc5
–	–	k?
County	Counta	k1gFnPc1
Subdivision	Subdivision	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
Census	census	k1gInSc1
Redistricting	Redistricting	k1gInSc4
Data	datum	k1gNnSc2
(	(	kIx(
<g/>
Public	publicum	k1gNnPc2
Law	Law	k1gFnSc1
94	#num#	k4
<g/>
-	-	kIx~
<g/>
171	#num#	k4
<g/>
)	)	kIx)
Summary	Summara	k1gFnSc2
File	Fil	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
July	Jula	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
estimated	estimated	k1gInSc1
<g/>
:	:	kIx,
Table	tablo	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annual	Annual	k1gMnSc1
Estimates	Estimates	k1gMnSc1
of	of	k?
the	the	k?
Population	Population	k1gInSc1
of	of	k?
Metropolitan	metropolitan	k1gInSc1
and	and	k?
Micropolitan	Micropolitan	k1gInSc1
Statistical	Statistical	k1gMnSc1
Areas	Areas	k1gMnSc1
<g/>
:	:	kIx,
April	April	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
to	ten	k3xDgNnSc1
July	Jula	k1gMnPc7
1	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
(	(	kIx(
<g/>
CBSA-EST	CBSA-EST	k1gFnSc1
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
<g/>
,	,	kIx,
Population	Population	k1gInSc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
2010-03-23	2010-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Firsts	Firsts	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Western	western	k1gInSc1
Massachusetts	Massachusetts	k1gNnSc2
Electric	Electrice	k1gInPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Western	western	k1gInSc1
Massachusetts	Massachusetts	k1gNnSc1
2010-2011	2010-2011	k4
Economic	Economice	k1gFnPc2
Review	Review	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Western	western	k1gInSc1
Massachusetts	Massachusetts	k1gNnSc2
Electric	Electrice	k1gInPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Springfield	Springfielda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
602665	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4130517-6	4130517-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79065976	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
130813774	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79065976	#num#	k4
</s>
