<s>
Český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
neboli	neboli	k8xC	neboli
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
západoslovanský	západoslovanský	k2eAgInSc4d1	západoslovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
polštině	polština	k1gFnSc3	polština
a	a	k8xC	a
lužické	lužický	k2eAgFnSc3d1	Lužická
srbštině	srbština	k1gFnSc3	srbština
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
jazyků	jazyk	k1gInPc2	jazyk
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
nářečí	nářečí	k1gNnPc2	nářečí
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
na	na	k7c6	na
konci	konec	k1gInSc6	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
latinou	latina	k1gFnSc7	latina
a	a	k8xC	a
němčinou	němčina	k1gFnSc7	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
psaná	psaný	k2eAgFnSc1d1	psaná
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
již	již	k6eAd1	již
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgInSc4d1	oficiální
styk	styk	k1gInSc4	styk
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
kodifikována	kodifikovat	k5eAaBmNgFnS	kodifikovat
v	v	k7c6	v
mluvnicích	mluvnice	k1gFnPc6	mluvnice
a	a	k8xC	a
slovnících	slovník	k1gInPc6	slovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
obecnou	obecný	k2eAgFnSc4d1	obecná
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dialekty	dialekt	k1gInPc4	dialekt
(	(	kIx(	(
<g/>
nářečí	nářečí	k1gNnSc2	nářečí
<g/>
)	)	kIx)	)
a	a	k8xC	a
sociolekty	sociolekt	k1gInPc1	sociolekt
(	(	kIx(	(
<g/>
slangy	slang	k1gInPc1	slang
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
vulgarismů	vulgarismus	k1gInPc2	vulgarismus
a	a	k8xC	a
argotu	argot	k1gInSc2	argot
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
podoby	podoba	k1gFnPc4	podoba
<g/>
:	:	kIx,	:
čistě	čistě	k6eAd1	čistě
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
a	a	k8xC	a
hovorovou	hovorový	k2eAgFnSc4d1	hovorová
<g/>
.	.	kIx.	.
</s>
<s>
Hovorovou	hovorový	k2eAgFnSc4d1	hovorová
češtinu	čeština	k1gFnSc4	čeština
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišovat	odlišovat	k5eAaImF	odlišovat
od	od	k7c2	od
substandardní	substandardní	k2eAgFnSc2d1	substandardní
(	(	kIx(	(
<g/>
nespisovné	spisovný	k2eNgFnSc2d1	nespisovná
<g/>
)	)	kIx)	)
češtiny	čeština	k1gFnSc2	čeština
obecné	obecný	k2eAgFnSc2d1	obecná
<g/>
.	.	kIx.	.
</s>
<s>
Koexistence	koexistence	k1gFnSc1	koexistence
spisovné	spisovný	k2eAgFnSc2d1	spisovná
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
autory	autor	k1gMnPc4	autor
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
diglosie	diglosie	k1gFnSc1	diglosie
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
mluví	mluvit	k5eAaImIp3nS	mluvit
zhruba	zhruba	k6eAd1	zhruba
10,6	[number]	k4	10,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přes	přes	k7c4	přes
10,4	[number]	k4	10,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
několika	několik	k4yIc2	několik
vystěhovaleckých	vystěhovalecký	k2eAgFnPc2d1	vystěhovalecká
vln	vlna	k1gFnPc2	vlna
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
150	[number]	k4	150
letech	léto	k1gNnPc6	léto
hovoří	hovořit	k5eAaImIp3nS	hovořit
česky	česky	k6eAd1	česky
i	i	k9	i
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
emigrantů	emigrant	k1gMnPc2	emigrant
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
flektivní	flektivní	k2eAgInSc4d1	flektivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
se	se	k3xPyFc4	se
komplikovaným	komplikovaný	k2eAgInSc7d1	komplikovaný
systémem	systém	k1gInSc7	systém
skloňování	skloňování	k1gNnSc4	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc4	časování
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
písemným	písemný	k2eAgInPc3d1	písemný
záznamům	záznam	k1gInPc3	záznam
používá	používat	k5eAaImIp3nS	používat
latinku	latinka	k1gFnSc4	latinka
<g/>
,	,	kIx,	,
obohacenou	obohacený	k2eAgFnSc4d1	obohacená
o	o	k7c4	o
znaky	znak	k1gInPc4	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výslovnost	výslovnost	k1gFnSc4	výslovnost
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
pevný	pevný	k2eAgInSc1d1	pevný
přízvuk	přízvuk	k1gInSc1	přízvuk
<g/>
,	,	kIx,	,
opozice	opozice	k1gFnSc1	opozice
délky	délka	k1gFnSc2	délka
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
specifická	specifický	k2eAgFnSc1d1	specifická
souhláska	souhláska	k1gFnSc1	souhláska
"	"	kIx"	"
<g/>
ř	ř	k?	ř
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
alveolární	alveolární	k2eAgFnSc1d1	alveolární
vibranta	vibranta	k1gFnSc1	vibranta
=	=	kIx~	=
znělá	znělý	k2eAgFnSc1d1	znělá
dásňová	dásňový	k2eAgFnSc1d1	dásňová
kmitavá	kmitavý	k2eAgFnSc1d1	kmitavá
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
lze	lze	k6eAd1	lze
češtinu	čeština	k1gFnSc4	čeština
používat	používat	k5eAaImF	používat
také	také	k9	také
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Spisovný	spisovný	k2eAgInSc1d1	spisovný
standard	standard	k1gInSc1	standard
žádný	žádný	k3yNgInSc4	žádný
zákon	zákon	k1gInSc4	zákon
neupravuje	upravovat	k5eNaImIp3nS	upravovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kodifikuje	kodifikovat	k5eAaBmIp3nS	kodifikovat
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
svými	svůj	k3xOyFgFnPc7	svůj
obecně	obecně	k6eAd1	obecně
uznávanými	uznávaný	k2eAgFnPc7d1	uznávaná
doporučujícími	doporučující	k2eAgFnPc7d1	doporučující
publikacemi	publikace	k1gFnPc7	publikace
Ústav	ústava	k1gFnPc2	ústava
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
schválením	schválení	k1gNnSc7	schválení
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
i	i	k8xC	i
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
slovanský	slovanský	k2eAgInSc4d1	slovanský
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k6eAd1	tak
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
slovenštinou	slovenština	k1gFnSc7	slovenština
tvoří	tvořit	k5eAaImIp3nP	tvořit
česko-slovenskou	českolovenský	k2eAgFnSc4d1	česko-slovenská
větev	větev	k1gFnSc4	větev
západoslovanských	západoslovanský	k2eAgInPc2d1	západoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
dále	daleko	k6eAd2	daleko
polština	polština	k1gFnSc1	polština
<g/>
,	,	kIx,	,
kašubština	kašubština	k1gFnSc1	kašubština
a	a	k8xC	a
lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc4d1	české
jazykové	jazykový	k2eAgNnSc4d1	jazykové
území	území	k1gNnSc4	území
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
leží	ležet	k5eAaImIp3nS	ležet
nejvíce	hodně	k6eAd3	hodně
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
češtinou	čeština	k1gFnSc7	čeština
a	a	k8xC	a
slovenštinou	slovenština	k1gFnSc7	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
blízká	blízký	k2eAgFnSc1d1	blízká
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
se	s	k7c7	s
slovenštinou	slovenština	k1gFnSc7	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
některými	některý	k3yIgNnPc7	některý
nářečími	nářečí	k1gNnPc7	nářečí
jiných	jiný	k2eAgMnPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
existuje	existovat	k5eAaImIp3nS	existovat
pasivní	pasivní	k2eAgInSc1d1	pasivní
česko-slovenský	českolovenský	k2eAgInSc1d1	česko-slovenský
bilingvismus	bilingvismus	k1gInSc1	bilingvismus
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
díky	díky	k7c3	díky
dřívější	dřívější	k2eAgFnSc3d1	dřívější
existenci	existence	k1gFnSc3	existence
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Češi	Čech	k1gMnPc1	Čech
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
rozumějí	rozumět	k5eAaImIp3nP	rozumět
slovenštině	slovenština	k1gFnSc3	slovenština
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
srozumitelnost	srozumitelnost	k1gFnSc1	srozumitelnost
obou	dva	k4xCgMnPc2	dva
jazyků	jazyk	k1gMnPc2	jazyk
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
95	[number]	k4	95
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
dialekty	dialekt	k1gInPc1	dialekt
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jazykové	jazykový	k2eAgNnSc4d1	jazykové
kontinuum	kontinuum	k1gNnSc4	kontinuum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
je	být	k5eAaImIp3nS	být
plynulý	plynulý	k2eAgInSc1d1	plynulý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
politiky	politika	k1gFnSc2	politika
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
slovenština	slovenština	k1gFnSc1	slovenština
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
spisovné	spisovný	k2eAgFnPc4d1	spisovná
varianty	varianta	k1gFnPc4	varianta
jednoho	jeden	k4xCgInSc2	jeden
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
nářečí	nářečí	k1gNnSc2	nářečí
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pračeském	pračeský	k2eAgNnSc6d1	pračeský
období	období	k1gNnSc6	období
si	se	k3xPyFc3	se
zachovávala	zachovávat	k5eAaImAgFnS	zachovávat
některé	některý	k3yIgInPc4	některý
praslovanské	praslovanský	k2eAgInPc4d1	praslovanský
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byly	být	k5eAaImAgInP	být
jery	jer	k1gInPc1	jer
<g/>
,	,	kIx,	,
nosovky	nosovka	k1gFnPc1	nosovka
(	(	kIx(	(
<g/>
ę	ę	k?	ę
a	a	k8xC	a
ǫ	ǫ	k?	ǫ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palatalizace	palatalizace	k1gFnPc4	palatalizace
či	či	k8xC	či
systém	systém	k1gInSc4	systém
čtyř	čtyři	k4xCgInPc2	čtyři
minulých	minulý	k2eAgInPc2d1	minulý
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
aorist	aorist	k1gInSc1	aorist
<g/>
,	,	kIx,	,
imperfektum	imperfektum	k1gNnSc1	imperfektum
<g/>
,	,	kIx,	,
perfektum	perfektum	k1gNnSc1	perfektum
<g/>
,	,	kIx,	,
plusquamperfektum	plusquamperfektum	k1gNnSc1	plusquamperfektum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
vymizely	vymizet	k5eAaPmAgFnP	vymizet
(	(	kIx(	(
<g/>
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písemné	písemný	k2eAgFnPc4d1	písemná
památky	památka	k1gFnPc4	památka
z	z	k7c2	z
nejstaršího	starý	k2eAgNnSc2d3	nejstarší
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
sporadické	sporadický	k2eAgFnPc1d1	sporadická
<g/>
.	.	kIx.	.
</s>
<s>
Číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
tehdy	tehdy	k6eAd1	tehdy
uměli	umět	k5eAaImAgMnP	umět
většinou	většina	k1gFnSc7	většina
jen	jen	k6eAd1	jen
duchovní	duchovní	k2eAgMnPc1d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
plnila	plnit	k5eAaImAgFnS	plnit
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
česky	česky	k6eAd1	česky
psanou	psaný	k2eAgFnSc7d1	psaná
památkou	památka	k1gFnSc7	památka
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
věty	věta	k1gFnSc2	věta
ze	z	k7c2	z
zakládací	zakládací	k2eAgFnSc2d1	zakládací
listiny	listina	k1gFnSc2	listina
litoměřické	litoměřický	k2eAgFnSc2d1	Litoměřická
kapituly	kapitula	k1gFnSc2	kapitula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1057	[number]	k4	1057
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zřetelně	zřetelně	k6eAd1	zřetelně
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k9	až
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
dal	dát	k5eAaPmAgMnS	dát
jest	být	k5eAaImIp3nS	být
Ploškovicích	Ploškovice	k1gFnPc6	Ploškovice
zemu	zemus	k1gInSc2	zemus
<g/>
.	.	kIx.	.
</s>
<s>
Vlach	Vlach	k1gMnSc1	Vlach
dal	dát	k5eAaPmAgMnS	dát
jest	být	k5eAaImIp3nS	být
Dolas	Dolas	k1gInSc4	Dolas
zemu	zem	k1gMnSc3	zem
Bogu	Bog	k1gMnSc3	Bog
i	i	k8xC	i
svjatemu	svjatem	k1gMnSc3	svjatem
Scepanu	Scepan	k1gMnSc3	Scepan
se	se	k3xPyFc4	se
dvema	dvema	k1gNnSc1	dvema
dušníkoma	dušníkomum	k1gNnSc2	dušníkomum
Bogucos	Bogucosa	k1gFnPc2	Bogucosa
a	a	k8xC	a
Sedlatu	Sedlat	k1gInSc2	Sedlat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
posměšné	posměšný	k2eAgFnPc1d1	posměšná
přípisky	přípiska	k1gFnPc1	přípiska
z	z	k7c2	z
chorální	chorální	k2eAgFnSc2d1	chorální
knihy	kniha	k1gFnSc2	kniha
svatojiřské	svatojiřský	k2eAgFnSc2d1	Svatojiřská
(	(	kIx(	(
<g/>
Svatojiřské	svatojiřský	k2eAgFnSc2d1	Svatojiřská
přípisky	přípiska	k1gFnSc2	přípiska
<g/>
)	)	kIx)	)
z	z	k7c2	z
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Věty	věta	k1gFnPc1	věta
byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
tzv.	tzv.	kA	tzv.
primitivním	primitivní	k2eAgInSc7d1	primitivní
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
používal	používat	k5eAaImAgInS	používat
neupravenou	upravený	k2eNgFnSc4d1	neupravená
latinku	latinka	k1gFnSc4	latinka
i	i	k9	i
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
latině	latina	k1gFnSc3	latina
cizí	cizí	k2eAgFnSc3d1	cizí
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
písmeno	písmeno	k1gNnSc1	písmeno
mohlo	moct	k5eAaImAgNnS	moct
označovat	označovat	k5eAaImF	označovat
více	hodně	k6eAd2	hodně
hlásek	hlásek	k1gInSc4	hlásek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proniká	pronikat	k5eAaImIp3nS	pronikat
čeština	čeština	k1gFnSc1	čeština
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
úředního	úřední	k2eAgInSc2d1	úřední
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
česky	česky	k6eAd1	česky
psané	psaný	k2eAgFnSc2d1	psaná
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nechává	nechávat	k5eAaImIp3nS	nechávat
vyhotovit	vyhotovit	k5eAaPmF	vyhotovit
první	první	k4xOgInSc1	první
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
spřežkový	spřežkový	k2eAgInSc1d1	spřežkový
pravopis	pravopis	k1gInSc1	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
reformu	reforma	k1gFnSc4	reforma
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaváděl	zavádět	k5eAaImAgInS	zavádět
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
používání	používání	k1gNnSc2	používání
diakritických	diakritický	k2eAgNnPc2d1	diakritické
znamének	znaménko	k1gNnPc2	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Propagátorem	propagátor	k1gMnSc7	propagátor
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
také	také	k9	také
jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozvoj	rozvoj	k1gInSc1	rozvoj
zažila	zažít	k5eAaPmAgFnS	zažít
česky	česky	k6eAd1	česky
psaná	psaný	k2eAgFnSc1d1	psaná
literatura	literatura	k1gFnSc1	literatura
zejména	zejména	k9	zejména
po	po	k7c6	po
vynálezu	vynález	k1gInSc6	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
psaná	psaný	k2eAgFnSc1d1	psaná
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
je	být	k5eAaImIp3nS	být
Kronika	kronika	k1gFnSc1	kronika
trojánská	trojánský	k2eAgFnSc1d1	Trojánská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytištěna	vytisknout	k5eAaPmNgFnS	vytisknout
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
nejspíše	nejspíše	k9	nejspíše
roku	rok	k1gInSc2	rok
1468	[number]	k4	1468
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vzor	vzor	k1gInSc1	vzor
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
používána	používán	k2eAgFnSc1d1	používána
tzv.	tzv.	kA	tzv.
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
(	(	kIx(	(
<g/>
1579	[number]	k4	1579
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
úpadku	úpadek	k1gInSc3	úpadek
česky	česky	k6eAd1	česky
psané	psaný	k2eAgFnSc2d1	psaná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčiněn	k2eAgMnSc1d1	zapříčiněn
zejména	zejména	k9	zejména
nucenou	nucený	k2eAgFnSc7d1	nucená
emigrací	emigrace	k1gFnSc7	emigrace
české	český	k2eAgFnSc2d1	Česká
nekatolické	katolický	k2eNgFnSc2d1	nekatolická
inteligence	inteligence	k1gFnSc2	inteligence
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Stránský	Stránský	k1gMnSc1	Stránský
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vycházela	vycházet	k5eAaImAgFnS	vycházet
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovšem	ovšem	k9	ovšem
podléhala	podléhat	k5eAaImAgFnS	podléhat
přísné	přísný	k2eAgFnSc3d1	přísná
cenzuře	cenzura	k1gFnSc3	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Obnovené	obnovený	k2eAgNnSc1d1	obnovené
zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgNnSc1d1	zemské
(	(	kIx(	(
<g/>
1627	[number]	k4	1627
<g/>
,	,	kIx,	,
1628	[number]	k4	1628
<g/>
)	)	kIx)	)
zavedlo	zavést	k5eAaPmAgNnS	zavést
jako	jako	k9	jako
druhý	druhý	k4xOgInSc4	druhý
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zrovnoprávněna	zrovnoprávnit	k5eAaPmNgFnS	zrovnoprávnit
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
však	však	k9	však
díky	díky	k7c3	díky
politickému	politický	k2eAgInSc3d1	politický
tlaku	tlak	k1gInSc3	tlak
získala	získat	k5eAaPmAgFnS	získat
němčina	němčina	k1gFnSc1	němčina
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
staletí	staletí	k1gNnPc2	staletí
navrch	navrch	k6eAd1	navrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
němčiny	němčina	k1gFnSc2	němčina
jako	jako	k8xS	jako
jednotného	jednotný	k2eAgInSc2d1	jednotný
jazyka	jazyk	k1gInSc2	jazyk
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
habsburského	habsburský	k2eAgNnSc2d1	habsburské
soustátí	soustátí	k1gNnSc2	soustátí
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
hlavně	hlavně	k9	hlavně
praktickými	praktický	k2eAgFnPc7d1	praktická
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
než	než	k8xS	než
národnostními	národnostní	k2eAgInPc7d1	národnostní
<g/>
)	)	kIx)	)
důvody	důvod	k1gInPc7	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Ukázala	ukázat	k5eAaPmAgFnS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
jako	jako	k9	jako
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
česky	česky	k6eAd1	česky
mluvící	mluvící	k2eAgNnSc1d1	mluvící
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
početné	početný	k2eAgNnSc1d1	početné
a	a	k8xC	a
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
většiny	většina	k1gFnSc2	většina
území	území	k1gNnSc2	území
poněmčeného	poněmčený	k2eAgNnSc2d1	poněmčené
Slezska	Slezsko	k1gNnSc2	Slezsko
mělo	mít	k5eAaImAgNnS	mít
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
nad	nad	k7c7	nad
německy	německy	k6eAd1	německy
mluvícími	mluvící	k2eAgFnPc7d1	mluvící
procentuálně	procentuálně	k6eAd1	procentuálně
navrch	navrch	k7c2	navrch
<g/>
.	.	kIx.	.
</s>
<s>
Zrušení	zrušení	k1gNnSc1	zrušení
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pak	pak	k6eAd1	pak
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznik	vznik	k1gInSc4	vznik
hnutí	hnutí	k1gNnSc2	hnutí
označovaného	označovaný	k2eAgNnSc2d1	označované
jako	jako	k8xC	jako
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
snahám	snaha	k1gFnPc3	snaha
národních	národní	k2eAgMnPc2d1	národní
buditelů	buditel	k1gMnPc2	buditel
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
opět	opět	k6eAd1	opět
vyzdvižena	vyzdvižen	k2eAgFnSc1d1	vyzdvižena
úroveň	úroveň	k1gFnSc1	úroveň
česky	česky	k6eAd1	česky
psané	psaný	k2eAgFnSc2d1	psaná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
díky	díky	k7c3	díky
povinné	povinný	k2eAgFnSc3d1	povinná
školní	školní	k2eAgFnSc3d1	školní
docházce	docházka	k1gFnSc3	docházka
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc3d1	vysoká
gramotnosti	gramotnost	k1gFnSc3	gramotnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
záležitostí	záležitost	k1gFnPc2	záležitost
úzké	úzký	k2eAgFnSc2d1	úzká
vrstvy	vrstva	k1gFnSc2	vrstva
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
pokusů	pokus	k1gInPc2	pokus
o	o	k7c6	o
kodifikaci	kodifikace	k1gFnSc6	kodifikace
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
všeobecně	všeobecně	k6eAd1	všeobecně
přijímána	přijímán	k2eAgFnSc1d1	přijímána
gramatika	gramatika	k1gFnSc1	gramatika
Josefa	Josef	k1gMnSc2	Josef
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
české	český	k2eAgFnPc4d1	Česká
slovní	slovní	k2eAgFnPc4d1	slovní
zásoby	zásoba	k1gFnPc4	zásoba
přispělo	přispět	k5eAaPmAgNnS	přispět
zejména	zejména	k9	zejména
vydání	vydání	k1gNnSc1	vydání
pětidílného	pětidílný	k2eAgInSc2d1	pětidílný
Slovníku	slovník	k1gInSc2	slovník
česko-německého	českoěmecký	k2eAgInSc2d1	česko-německý
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
Josefa	Josef	k1gMnSc4	Josef
Jungmanna	Jungmann	k1gMnSc4	Jungmann
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
publicistika	publicistika	k1gFnSc1	publicistika
a	a	k8xC	a
umělecká	umělecký	k2eAgFnSc1d1	umělecká
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přiblížit	přiblížit	k5eAaPmF	přiblížit
živému	živý	k1gMnSc3	živý
jazyku	jazyk	k1gMnSc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
některé	některý	k3yIgInPc1	některý
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
zastaralého	zastaralý	k2eAgInSc2d1	zastaralý
pravopisu	pravopis	k1gInSc2	pravopis
Bible	bible	k1gFnSc2	bible
kralické	kralický	k2eAgFnSc2d1	Kralická
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovo	slovo	k1gNnSc4	slovo
její	její	k3xOp3gNnSc1	její
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
psáno	psán	k2eAgNnSc1d1	psáno
jako	jako	k9	jako
gegj	gegj	k1gInSc4	gegj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
získala	získat	k5eAaPmAgFnS	získat
víceméně	víceméně	k9	víceméně
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
slovenština	slovenština	k1gFnSc1	slovenština
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
až	až	k9	až
98	[number]	k4	98
%	%	kIx~	%
praslovanské	praslovanský	k2eAgFnSc2d1	praslovanská
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
slovanskými	slovanský	k2eAgInPc7d1	slovanský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
převzata	převzít	k5eAaPmNgFnS	převzít
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
(	(	kIx(	(
<g/>
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
parabola	parabola	k1gFnSc1	parabola
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
a	a	k8xC	a
latiny	latina	k1gFnSc2	latina
(	(	kIx(	(
<g/>
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
<g/>
,	,	kIx,	,
herbář	herbář	k1gInSc1	herbář
<g/>
,	,	kIx,	,
tabule	tabule	k1gFnSc1	tabule
<g/>
,	,	kIx,	,
kapsa	kapsa	k1gFnSc1	kapsa
<g/>
,	,	kIx,	,
skříň	skříň	k1gFnSc1	skříň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těsným	těsný	k2eAgInPc3d1	těsný
historickým	historický	k2eAgInPc3d1	historický
kontaktům	kontakt	k1gInPc3	kontakt
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
také	také	k6eAd1	také
přejata	přejmout	k5eAaPmNgFnS	přejmout
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
knedlík	knedlík	k1gInSc1	knedlík
<g/>
,	,	kIx,	,
šunka	šunka	k1gFnSc1	šunka
<g/>
,	,	kIx,	,
taška	taška	k1gFnSc1	taška
<g/>
,	,	kIx,	,
brýle	brýle	k1gFnPc1	brýle
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejím	její	k3xOp3gNnSc7	její
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
byla	být	k5eAaImAgFnS	být
přejata	přejmout	k5eAaPmNgFnS	přejmout
slova	slovo	k1gNnPc4	slovo
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
pochází	pocházet	k5eAaImIp3nS	pocházet
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
řemeslnického	řemeslnický	k2eAgNnSc2d1	řemeslnické
názvosloví	názvosloví	k1gNnSc2	názvosloví
i	i	k8xC	i
slangu	slang	k1gInSc2	slang
(	(	kIx(	(
<g/>
hoblík	hoblík	k1gInSc1	hoblík
<g/>
,	,	kIx,	,
klempíř	klempíř	k1gMnSc1	klempíř
<g/>
,	,	kIx,	,
ponk	ponk	k1gInSc1	ponk
<g/>
,	,	kIx,	,
šichta	šichta	k1gFnSc1	šichta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
proniklo	proniknout	k5eAaPmAgNnS	proniknout
do	do	k7c2	do
nespisovných	spisovný	k2eNgFnPc2d1	nespisovná
vrstev	vrstva	k1gFnPc2	vrstva
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
majznout	majznout	k5eAaPmF	majznout
<g/>
,	,	kIx,	,
lajsnout	lajsnout	k5eAaPmF	lajsnout
<g/>
/	/	kIx~	/
<g/>
lajznout	lajznout	k5eAaPmF	lajznout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
luftovat	luftovat	k5eAaImF	luftovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
programově	programově	k6eAd1	programově
přejímána	přejímat	k5eAaImNgFnS	přejímat
ze	z	k7c2	z
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
–	–	k?	–
polštiny	polština	k1gFnPc4	polština
(	(	kIx(	(
<g/>
báje	báje	k1gFnSc1	báje
<g/>
,	,	kIx,	,
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
půvab	půvab	k1gInSc1	půvab
<g/>
,	,	kIx,	,
otvor	otvor	k1gInSc1	otvor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
(	(	kIx(	(
<g/>
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Uměle	uměle	k6eAd1	uměle
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
české	český	k2eAgNnSc1d1	české
odborné	odborný	k2eAgNnSc1d1	odborné
názvosloví	názvosloví	k1gNnSc1	názvosloví
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doslovným	doslovný	k2eAgInSc7d1	doslovný
překladem	překlad	k1gInSc7	překlad
(	(	kIx(	(
<g/>
kalky	kalk	k1gInPc1	kalk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
se	se	k3xPyFc4	se
ujaly	ujmout	k5eAaPmAgInP	ujmout
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
používány	používat	k5eAaImNgFnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
pak	pak	k6eAd1	pak
obohacovala	obohacovat	k5eAaImAgFnS	obohacovat
češtinu	čeština	k1gFnSc4	čeština
zejména	zejména	k9	zejména
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
(	(	kIx(	(
<g/>
sovět	sovět	k1gInSc1	sovět
<g/>
,	,	kIx,	,
kulak	kulak	k1gMnSc1	kulak
<g/>
,	,	kIx,	,
chozrasčot	chozrasčot	k1gInSc1	chozrasčot
<g/>
,	,	kIx,	,
polárník	polárník	k1gMnSc1	polárník
<g/>
,	,	kIx,	,
rozvědka	rozvědka	k1gFnSc1	rozvědka
<g/>
,	,	kIx,	,
celiny	celina	k1gFnPc1	celina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
pochází	pocházet	k5eAaImIp3nS	pocházet
řada	řada	k1gFnSc1	řada
pojmů	pojem	k1gInPc2	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
<g/>
,	,	kIx,	,
soprán	soprán	k1gInSc1	soprán
<g/>
,	,	kIx,	,
forte	forte	k1gNnSc1	forte
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
)	)	kIx)	)
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
(	(	kIx(	(
<g/>
konto	konto	k1gNnSc4	konto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
slova	slovo	k1gNnSc2	slovo
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
módy	móda	k1gFnSc2	móda
(	(	kIx(	(
<g/>
baret	baret	k1gInSc1	baret
<g/>
,	,	kIx,	,
blůza	blůza	k1gFnSc1	blůza
<g/>
,	,	kIx,	,
manžeta	manžeta	k1gFnSc1	manžeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
zdrojem	zdroj	k1gInSc7	zdroj
sportovních	sportovní	k2eAgInPc2d1	sportovní
výrazů	výraz	k1gInPc2	výraz
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc1	tenis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
mnohá	mnohý	k2eAgNnPc4d1	mnohé
slova	slovo	k1gNnPc4	slovo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
hardware	hardware	k1gInSc1	hardware
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
přejímá	přejímat	k5eAaImIp3nS	přejímat
slova	slovo	k1gNnPc4	slovo
i	i	k9	i
z	z	k7c2	z
exotických	exotický	k2eAgInPc2d1	exotický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
pochází	pocházet	k5eAaImIp3nS	pocházet
např.	např.	kA	např.
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
z	z	k7c2	z
turečtiny	turečtina	k1gFnSc2	turečtina
jogurt	jogurt	k1gInSc1	jogurt
<g/>
,	,	kIx,	,
klobouk	klobouk	k1gInSc1	klobouk
<g/>
,	,	kIx,	,
čapka	čapka	k1gFnSc1	čapka
<g/>
,	,	kIx,	,
tasemnice	tasemnice	k1gFnSc1	tasemnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
z	z	k7c2	z
hindštiny	hindština	k1gFnSc2	hindština
džungle	džungle	k1gFnSc2	džungle
či	či	k8xC	či
jóga	jóga	k1gFnSc1	jóga
<g/>
.	.	kIx.	.
</s>
<s>
Pravopis	pravopis	k1gInSc1	pravopis
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
míře	míra	k1gFnSc6	míra
zdomácnění	zdomácnění	k1gNnPc2	zdomácnění
<g/>
:	:	kIx,	:
zdomácnělá	zdomácnělý	k2eAgNnPc1d1	zdomácnělé
slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
standardní	standardní	k2eAgInSc1d1	standardní
pravopis	pravopis	k1gInSc1	pravopis
přizpůsobený	přizpůsobený	k2eAgInSc1d1	přizpůsobený
českému	český	k2eAgInSc3d1	český
<g/>
,	,	kIx,	,
např.	např.	kA	např.
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
rádio	rádio	k1gNnSc1	rádio
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
kriket	kriket	k1gInSc1	kriket
<g/>
,	,	kIx,	,
akvárium	akvárium	k1gNnSc1	akvárium
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
psát	psát	k5eAaImF	psát
i	i	k9	i
televise	televise	k1gFnSc1	televise
<g/>
,	,	kIx,	,
radio	radio	k1gNnSc1	radio
<g/>
,	,	kIx,	,
museum	museum	k1gNnSc1	museum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
;	;	kIx,	;
novější	nový	k2eAgInSc4d2	novější
a	a	k8xC	a
méně	málo	k6eAd2	málo
používaná	používaný	k2eAgNnPc1d1	používané
slova	slovo	k1gNnPc1	slovo
obvykle	obvykle	k6eAd1	obvykle
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
původní	původní	k2eAgInSc4d1	původní
pravopis	pravopis	k1gInSc4	pravopis
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hardware	hardware	k1gInSc4	hardware
<g/>
,	,	kIx,	,
image	image	k1gFnSc4	image
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
představují	představovat	k5eAaImIp3nP	představovat
citátová	citátový	k2eAgNnPc1d1	citátové
slova	slovo	k1gNnPc1	slovo
–	–	k?	–
výrazy	výraz	k1gInPc7	výraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
původní	původní	k2eAgInSc4d1	původní
pravopis	pravopis	k1gInSc4	pravopis
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
i	i	k9	i
výslovnost	výslovnost	k1gFnSc1	výslovnost
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
české	český	k2eAgFnPc4d1	Česká
koncovky	koncovka	k1gFnPc4	koncovka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
lat.	lat.	k?	lat.
<g />
.	.	kIx.	.
</s>
<s>
ecce	ecce	k6eAd1	ecce
homo	homo	k1gNnSc1	homo
<g/>
,	,	kIx,	,
corpus	corpus	k1gNnSc1	corpus
delicti	delicť	k1gFnSc2	delicť
<g/>
,	,	kIx,	,
in	in	k?	in
statu	status	k1gInSc6	status
nascendi	nascend	k1gMnPc1	nascend
<g/>
,	,	kIx,	,
ab	ab	k?	ab
incunabulis	incunabulis	k1gInSc1	incunabulis
<g/>
,	,	kIx,	,
dies	dies	k6eAd1	dies
irae	irae	k6eAd1	irae
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
aperçu	aperçu	k1gNnSc2	aperçu
<g/>
,	,	kIx,	,
à	à	k?	à
propos	propos	k1gInSc1	propos
<g/>
,	,	kIx,	,
enfant	enfant	k1gInSc1	enfant
terrible	terrible	k6eAd1	terrible
<g/>
,	,	kIx,	,
tê	tê	k?	tê
à	à	k?	à
tê	tê	k?	tê
<g/>
,	,	kIx,	,
vis-à	vis-à	k?	vis-à
<g/>
,	,	kIx,	,
potpourri	potpourri	k1gNnPc1	potpourri
<g/>
,	,	kIx,	,
parvenu	parvenu	k1gMnSc1	parvenu
<g/>
;	;	kIx,	;
laissez-faire	laissezair	k1gMnSc5	laissez-fair
<g/>
,	,	kIx,	,
laissez-passer	laissezassrat	k5eAaPmRp2nS	laissez-passrat
<g/>
;	;	kIx,	;
ital	ital	k1gInSc1	ital
<g/>
.	.	kIx.	.
staccato	staccato	k1gNnSc1	staccato
<g/>
,	,	kIx,	,
adagio	adagio	k1gNnSc1	adagio
<g/>
,	,	kIx,	,
dolce	dolec	k1gInPc1	dolec
far	fara	k1gFnPc2	fara
niente	nient	k1gInSc5	nient
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
souběžně	souběžně	k6eAd1	souběžně
původní	původní	k2eAgInSc1d1	původní
i	i	k8xC	i
počeštěný	počeštěný	k2eAgInSc1d1	počeštěný
pravopis	pravopis	k1gInSc1	pravopis
<g/>
,	,	kIx,	,
např.	např.	kA	např.
business	business	k1gInSc1	business
i	i	k8xC	i
byznys	byznys	k1gInSc1	byznys
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
PČP	PČP	kA	PČP
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jen	jen	k9	jen
byznys	byznys	k1gInSc4	byznys
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
čeština	čeština	k1gFnSc1	čeština
jakožto	jakožto	k8xS	jakožto
živý	živý	k2eAgInSc1d1	živý
jazyk	jazyk	k1gInSc1	jazyk
neustále	neustále	k6eAd1	neustále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
má	mít	k5eAaImIp3nS	mít
čeština	čeština	k1gFnSc1	čeština
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
slovních	slovní	k2eAgInPc2d1	slovní
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
Příruční	příruční	k2eAgInSc1d1	příruční
slovník	slovník	k1gInSc1	slovník
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
vydaný	vydaný	k2eAgInSc4d1	vydaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
250	[number]	k4	250
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zná	znát	k5eAaImIp3nS	znát
málokdo	málokdo	k3yInSc1	málokdo
<g/>
,	,	kIx,	,
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
naopak	naopak	k6eAd1	naopak
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
obecně	obecně	k6eAd1	obecně
známá	známý	k2eAgFnSc1d1	známá
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vulgarismy	vulgarismus	k1gInPc1	vulgarismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
na	na	k7c4	na
popis	popis	k1gInSc4	popis
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výtahů	výtah	k1gInPc2	výtah
z	z	k7c2	z
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
,	,	kIx,	,
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
192	[number]	k4	192
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Rozsahem	rozsah	k1gInSc7	rozsah
nejmenší	malý	k2eAgInSc4d3	nejmenší
Slovník	slovník	k1gInSc4	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgInPc1	druhý
<g/>
,	,	kIx,	,
upravené	upravený	k2eAgInPc1d1	upravený
vydání	vydání	k1gNnSc4	vydání
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zhruba	zhruba	k6eAd1	zhruba
48	[number]	k4	48
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
představujících	představující	k2eAgInPc2d1	představující
jádro	jádro	k1gNnSc1	jádro
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
češtinu	čeština	k1gFnSc4	čeština
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
slova	slovo	k1gNnPc1	slovo
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
grafémů	grafém	k1gInPc2	grafém
(	(	kIx(	(
<g/>
grafických	grafický	k2eAgInPc2d1	grafický
nebo	nebo	k8xC	nebo
písemných	písemný	k2eAgInPc2d1	písemný
znaků	znak	k1gInPc2	znak
<g/>
)	)	kIx)	)
–	–	k?	–
slova	slovo	k1gNnSc2	slovo
tvořená	tvořený	k2eAgFnSc1d1	tvořená
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
grafémy	grafém	k1gInPc7	grafém
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
75	[number]	k4	75
%	%	kIx~	%
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psaném	psaný	k2eAgInSc6d1	psaný
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
grafémy	grafém	k1gInPc1	grafém
o	o	k7c4	o
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnSc1	dvojice
grafémů	grafém	k1gInPc2	grafém
st	st	kA	st
<g/>
,	,	kIx,	,
po	po	k7c6	po
<g/>
,	,	kIx,	,
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
ov	ov	k?	ov
<g/>
,	,	kIx,	,
na	na	k7c4	na
a	a	k8xC	a
trojice	trojice	k1gFnSc1	trojice
grafémů	grafém	k1gInPc2	grafém
pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
ost	ost	k?	ost
<g/>
,	,	kIx,	,
ova	ova	k?	ova
<g/>
,	,	kIx,	,
sta	sto	k4xCgNnPc1	sto
<g/>
,	,	kIx,	,
pře	pře	k1gFnSc1	pře
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
skupina	skupina	k1gFnSc1	skupina
slovních	slovní	k2eAgInPc2d1	slovní
kořenů	kořen	k1gInPc2	kořen
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
3	[number]	k4	3
grafémů	grafém	k1gInPc2	grafém
<g/>
,	,	kIx,	,
tvořených	tvořený	k2eAgFnPc2d1	tvořená
kombinací	kombinace	k1gFnPc2	kombinace
souhláska-samohláska-souhláska	souhláskaamohláskaouhláska	k1gFnSc1	souhláska-samohláska-souhláska
(	(	kIx(	(
<g/>
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
cop	cop	k1gInSc4	cop
<g/>
,	,	kIx,	,
jít	jít	k5eAaImF	jít
<g/>
)	)	kIx)	)
–	–	k?	–
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
konkrétních	konkrétní	k2eAgNnPc2d1	konkrétní
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
jazyce	jazyk	k1gInSc6	jazyk
neproporční	proporční	k2eNgMnPc1d1	proporční
–	–	k?	–
kdy	kdy	k6eAd1	kdy
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
slov	slovo	k1gNnPc2	slovo
tvoří	tvořit	k5eAaImIp3nS	tvořit
jádro	jádro	k1gNnSc1	jádro
slovníku	slovník	k1gInSc2	slovník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
tak	tak	k9	tak
10	[number]	k4	10
nejběžnějších	běžný	k2eAgNnPc2d3	nejběžnější
lemmat	lemma	k1gNnPc2	lemma
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
asi	asi	k9	asi
20	[number]	k4	20
%	%	kIx~	%
textu	text	k1gInSc2	text
a	a	k8xC	a
1000	[number]	k4	1000
nejběžnějších	běžný	k2eAgNnPc2d3	nejběžnější
lemmat	lemma	k1gNnPc2	lemma
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
65	[number]	k4	65
%	%	kIx~	%
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
znalost	znalost	k1gFnSc1	znalost
1000	[number]	k4	1000
slov	slovo	k1gNnPc2	slovo
postačuje	postačovat	k5eAaImIp3nS	postačovat
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
každý	každý	k3xTgInSc1	každý
text	text	k1gInSc1	text
(	(	kIx(	(
<g/>
i	i	k9	i
běžný	běžný	k2eAgMnSc1d1	běžný
<g/>
)	)	kIx)	)
operuje	operovat	k5eAaImIp3nS	operovat
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
specifických	specifický	k2eAgNnPc2d1	specifické
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
jádro	jádro	k1gNnSc4	jádro
výpovědi	výpověď	k1gFnSc2	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nářečí	nářečí	k1gNnSc2	nářečí
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
většinou	většinou	k6eAd1	většinou
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
stírají	stírat	k5eAaImIp3nP	stírat
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgNnPc1d1	české
nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
4	[number]	k4	4
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
kterým	který	k3yIgNnSc7	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
4	[number]	k4	4
české	český	k2eAgInPc1d1	český
interdialekty	interdialekt	k1gInPc1	interdialekt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
češtinou	čeština	k1gFnSc7	čeština
jako	jako	k8xS	jako
interdialektem	interdialekt	k1gInSc7	interdialekt
<g/>
)	)	kIx)	)
Středomoravská	středomoravský	k2eAgFnSc1d1	Středomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
<g/>
)	)	kIx)	)
Východomoravská	východomoravský	k2eAgFnSc1d1	východomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
moravskoslovenská	moravskoslovenský	k2eAgFnSc1d1	moravskoslovenská
<g/>
)	)	kIx)	)
Slezská	Slezská	k1gFnSc1	Slezská
nářečí	nářečí	k1gNnSc2	nářečí
(	(	kIx(	(
<g/>
lašská	lašský	k2eAgFnSc1d1	Lašská
<g/>
)	)	kIx)	)
České	český	k2eAgInPc1d1	český
interdialekty	interdialekt	k1gInPc1	interdialekt
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
čtyři	čtyři	k4xCgNnPc1	čtyři
<g/>
:	:	kIx,	:
obecná	obecná	k1gFnSc1	obecná
čeština	čeština	k1gFnSc1	čeština
obecná	obecná	k1gFnSc1	obecná
hanáčtina	hanáčtina	k1gFnSc1	hanáčtina
obecná	obecná	k1gFnSc1	obecná
moravská	moravský	k2eAgFnSc1d1	Moravská
slovenština	slovenština	k1gFnSc1	slovenština
obecná	obecná	k1gFnSc1	obecná
laština	laština	k1gFnSc1	laština
Pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
území	území	k1gNnSc4	území
osídlená	osídlený	k2eAgNnPc4d1	osídlené
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
Němci	Němec	k1gMnPc7	Němec
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
Sudety	Sudety	k1gInPc1	Sudety
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xS	jako
nářečně	nářečně	k6eAd1	nářečně
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
českých	český	k2eAgNnPc2d1	české
nářečí	nářečí	k1gNnPc2	nářečí
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
šestidílný	šestidílný	k2eAgInSc1d1	šestidílný
Český	český	k2eAgInSc1d1	český
jazykový	jazykový	k2eAgInSc1d1	jazykový
atlas	atlas	k1gInSc1	atlas
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
poslední	poslední	k2eAgInSc1d1	poslední
svazek	svazek	k1gInSc1	svazek
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
i	i	k9	i
audionahrávkami	audionahrávka	k1gFnPc7	audionahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Český	český	k2eAgInSc1d1	český
pravopis	pravopis	k1gInSc1	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
abeceda	abeceda	k1gFnSc1	abeceda
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
42	[number]	k4	42
grafémů	grafém	k1gInPc2	grafém
<g/>
/	/	kIx~	/
<g/>
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
grafické	grafický	k2eAgFnSc2d1	grafická
spřežky	spřežka	k1gFnSc2	spřežka
ch	ch	k0	ch
<g/>
;	;	kIx,	;
nejčastějším	častý	k2eAgMnSc6d3	nejčastější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
e	e	k0	e
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
častým	častý	k2eAgMnSc7d1	častý
ó	ó	k0	ó
(	(	kIx(	(
<g/>
41	[number]	k4	41
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
q	q	k?	q
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
.	.	kIx.	.
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
latinku	latinka	k1gFnSc4	latinka
doplněnou	doplněná	k1gFnSc4	doplněná
o	o	k7c4	o
tyto	tento	k3xDgInPc4	tento
znaky	znak	k1gInPc4	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
:	:	kIx,	:
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
spřežku	spřežka	k1gFnSc4	spřežka
ch	ch	k0	ch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
samostatné	samostatný	k2eAgNnSc4d1	samostatné
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc4d1	stojící
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
mezi	mezi	k7c7	mezi
h	h	k?	h
a	a	k8xC	a
i	i	k?	i
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velká	k1gFnSc1	velká
varianta	varianta	k1gFnSc1	varianta
je	být	k5eAaImIp3nS	být
CH	Ch	kA	Ch
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pokud	pokud	k8xS	pokud
stojí	stát	k5eAaImIp3nS	stát
tato	tento	k3xDgFnSc1	tento
spřežka	spřežka	k1gFnSc1	spřežka
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
psaného	psaný	k2eAgNnSc2d1	psané
dále	daleko	k6eAd2	daleko
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
se	se	k3xPyFc4	se
velké	velká	k1gFnSc3	velká
píše	psát	k5eAaImIp3nS	psát
jen	jen	k9	jen
první	první	k4xOgNnSc1	první
písmeno	písmeno	k1gNnSc1	písmeno
spřežky	spřežka	k1gFnSc2	spřežka
<g/>
:	:	kIx,	:
Ch	Ch	kA	Ch
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Chobot	chobot	k1gInSc1	chobot
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ch	ch	k0	ch
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
také	také	k6eAd1	také
používá	používat	k5eAaImIp3nS	používat
spřežka	spřežka	k1gFnSc1	spřežka
dž	dž	k?	dž
(	(	kIx(	(
<g/>
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
znělého	znělý	k2eAgInSc2d1	znělý
protějšku	protějšek	k1gInSc2	protějšek
č	č	k0	č
<g/>
,	,	kIx,	,
např.	např.	kA	např.
původní	původní	k2eAgFnSc2d1	původní
české	český	k2eAgFnPc1d1	Česká
džbán	džbán	k1gInSc4	džbán
a	a	k8xC	a
přejatá	přejatý	k2eAgNnPc4d1	přejaté
slova	slovo	k1gNnPc4	slovo
džus	džus	k1gInSc1	džus
<g/>
,	,	kIx,	,
džem	džem	k1gInSc1	džem
<g/>
,	,	kIx,	,
džíp	džíp	k1gInSc1	džíp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
pak	pak	k6eAd1	pak
dz	dz	k?	dz
(	(	kIx(	(
<g/>
znělý	znělý	k2eAgInSc1d1	znělý
protějšek	protějšek	k1gInSc1	protějšek
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
spřežky	spřežka	k1gFnPc1	spřežka
se	se	k3xPyFc4	se
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
samostatná	samostatný	k2eAgNnPc4d1	samostatné
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
za	za	k7c4	za
písmena	písmeno	k1gNnPc4	písmeno
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
český	český	k2eAgInSc4d1	český
háček	háček	k1gInSc4	háček
(	(	kIx(	(
<g/>
č	č	k0	č
<g/>
,	,	kIx,	,
ž	ž	k?	ž
<g/>
,	,	kIx,	,
š	š	k?	š
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
převzalo	převzít	k5eAaPmAgNnS	převzít
i	i	k9	i
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
kromě	kromě	k7c2	kromě
slovenštiny	slovenština	k1gFnSc2	slovenština
například	například	k6eAd1	například
bosenština	bosenština	k1gFnSc1	bosenština
<g/>
,	,	kIx,	,
chorvatština	chorvatština	k1gFnSc1	chorvatština
<g/>
,	,	kIx,	,
lakotština	lakotština	k1gFnSc1	lakotština
<g/>
,	,	kIx,	,
litevština	litevština	k1gFnSc1	litevština
<g/>
,	,	kIx,	,
lotyština	lotyština	k1gFnSc1	lotyština
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
varianty	varianta	k1gFnPc4	varianta
sámštiny	sámština	k1gFnSc2	sámština
<g/>
,	,	kIx,	,
slovinština	slovinština	k1gFnSc1	slovinština
aj.	aj.	kA	aj.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dominantní	dominantní	k2eAgInSc1d1	dominantní
princip	princip	k1gInSc1	princip
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
je	být	k5eAaImIp3nS	být
princip	princip	k1gInSc1	princip
fonologický	fonologický	k2eAgInSc1d1	fonologický
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
pravopisu	pravopis	k1gInSc2	pravopis
morfologického	morfologický	k2eAgInSc2d1	morfologický
(	(	kIx(	(
<g/>
had	had	k1gMnSc1	had
<g/>
/	/	kIx~	/
<g/>
hadi	had	k1gMnPc1	had
<g/>
)	)	kIx)	)
a	a	k8xC	a
historicko-etymologického	historickotymologický	k2eAgMnSc2d1	historicko-etymologický
(	(	kIx(	(
<g/>
panna	panna	k1gFnSc1	panna
z	z	k7c2	z
pán	pán	k1gMnSc1	pán
+	+	kIx~	+
-na	a	k?	-na
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncovkách	koncovka	k1gFnPc6	koncovka
příčestí	příčestí	k1gNnSc2	příčestí
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pravopis	pravopis	k1gInSc1	pravopis
syntaktický	syntaktický	k2eAgInSc1d1	syntaktický
(	(	kIx(	(
<g/>
dělali	dělat	k5eAaImAgMnP	dělat
<g/>
/	/	kIx~	/
<g/>
dělaly	dělat	k5eAaImAgFnP	dělat
<g/>
/	/	kIx~	/
<g/>
dělala	dělat	k5eAaImAgFnS	dělat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
písmeno	písmeno	k1gNnSc1	písmeno
(	(	kIx(	(
<g/>
grafém	grafém	k1gInSc1	grafém
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zpravidla	zpravidla	k6eAd1	zpravidla
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
jednoho	jeden	k4xCgMnSc2	jeden
fonému	foném	k1gInSc2	foném
(	(	kIx(	(
<g/>
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
i	i	k9	i
každého	každý	k3xTgInSc2	každý
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
alofony	alofon	k1gInPc1	alofon
jako	jako	k9	jako
např.	např.	kA	např.
ŋ	ŋ	k?	ŋ
a	a	k8xC	a
n	n	k0	n
<g/>
)	)	kIx)	)
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přihlíží	přihlížet	k5eAaImIp3nS	přihlížet
k	k	k7c3	k
morfologii	morfologie	k1gFnSc3	morfologie
a	a	k8xC	a
etymologii	etymologie	k1gFnSc3	etymologie
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
staršímu	starý	k2eAgInSc3d2	starší
stavu	stav	k1gInSc3	stav
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
psaní	psaní	k1gNnSc2	psaní
i	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
í	í	k0	í
<g/>
/	/	kIx~	/
<g/>
ý	ý	k?	ý
<g/>
,	,	kIx,	,
ě	ě	k?	ě
<g/>
,	,	kIx,	,
ú	ú	k0	ú
<g/>
/	/	kIx~	/
<g/>
ů	ů	k?	ů
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
ě	ě	k?	ě
a	a	k8xC	a
ů	ů	k?	ů
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
háček	háček	k1gInSc4	háček
na	na	k7c6	na
ě	ě	k?	ě
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
změkčenost	změkčenost	k1gFnSc4	změkčenost
předcházející	předcházející	k2eAgFnSc2d1	předcházející
souhlásky	souhláska	k1gFnSc2	souhláska
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
ů	ů	k?	ů
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
historickým	historický	k2eAgInSc7d1	historický
vývojem	vývoj	k1gInSc7	vývoj
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
hláska	hláska	k1gFnSc1	hláska
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
/	/	kIx~	/
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
ú	ú	k0	ú
<g/>
;	;	kIx,	;
hláska	hláska	k1gFnSc1	hláska
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
:	:	kIx,	:
<g/>
/	/	kIx~	/
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
/	/	kIx~	/
<g/>
o	o	k0	o
<g/>
:	:	kIx,	:
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
/	/	kIx~	/
<g/>
uo	uo	k?	uo
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
ů	ů	k?	ů
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
českých	český	k2eAgNnPc2d1	české
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
grafémů	grafém	k1gInPc2	grafém
ě	ě	k?	ě
<g/>
,	,	kIx,	,
Ě	Ě	kA	Ě
a	a	k8xC	a
ů	ů	k?	ů
<g/>
,	,	kIx,	,
Ů	Ů	kA	Ů
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ani	ani	k8xC	ani
é	é	k0	é
<g/>
,	,	kIx,	,
É	É	kA	É
a	a	k8xC	a
ý	ý	k?	ý
<g/>
,	,	kIx,	,
Ý.	Ý.	kA	Ý.
Řazení	řazení	k1gNnSc1	řazení
Písmena	písmeno	k1gNnSc2	písmeno
s	s	k7c7	s
háčkem	háček	k1gInSc7	háček
č	č	k0	č
<g/>
,	,	kIx,	,
ř	ř	k?	ř
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
,	,	kIx,	,
a	a	k8xC	a
ž	ž	k?	ž
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
kromě	kromě	k7c2	kromě
ě	ě	k?	ě
<g/>
,	,	kIx,	,
ď	ď	k?	ď
<g/>
,	,	kIx,	,
ť	ť	k?	ť
a	a	k8xC	a
ň	ň	k?	ň
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
při	při	k7c6	při
abecedním	abecední	k2eAgNnSc6d1	abecední
řazení	řazení	k1gNnSc6	řazení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
<g/>
,	,	kIx,	,
kladou	klást	k5eAaImIp3nP	klást
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
základní	základní	k2eAgInPc4d1	základní
znaky	znak	k1gInPc4	znak
bez	bez	k7c2	bez
háčku	háček	k1gInSc2	háček
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgNnPc4d1	samostatné
písmena	písmeno	k1gNnPc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
písmena	písmeno	k1gNnPc4	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
–	–	k?	–
samohlásky	samohláska	k1gFnSc2	samohláska
a	a	k8xC	a
také	také	k9	také
písmena	písmeno	k1gNnPc1	písmeno
měkkých	měkký	k2eAgFnPc2d1	měkká
souhlásek	souhláska	k1gFnPc2	souhláska
ď	ď	k?	ď
<g/>
,	,	kIx,	,
ť	ť	k?	ť
a	a	k8xC	a
ň	ň	k?	ň
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
stejnou	stejný	k2eAgFnSc4d1	stejná
prioritu	priorita	k1gFnSc4	priorita
jako	jako	k8xC	jako
znaky	znak	k1gInPc4	znak
bez	bez	k7c2	bez
diakritiky	diakritika	k1gFnSc2	diakritika
<g/>
,	,	kIx,	,
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
řazení	řazení	k1gNnSc1	řazení
typu	typ	k1gInSc2	typ
car	car	k1gMnSc1	car
–	–	k?	–
cár	cár	k1gInSc1	cár
–	–	k?	–
carevič	carevič	k1gMnSc1	carevič
(	(	kIx(	(
<g/>
Pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
5	[number]	k4	5
krátkých	krátká	k1gFnPc2	krátká
a	a	k8xC	a
5	[number]	k4	5
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
27	[number]	k4	27
souhlásek	souhláska	k1gFnPc2	souhláska
a	a	k8xC	a
3	[number]	k4	3
dvojhlásky	dvojhláska	k1gFnPc4	dvojhláska
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
charakteristickým	charakteristický	k2eAgInPc3d1	charakteristický
rysům	rys	k1gInPc3	rys
české	český	k2eAgFnSc2d1	Česká
výslovnosti	výslovnost	k1gFnSc2	výslovnost
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
opozice	opozice	k1gFnSc1	opozice
kvantity	kvantita	k1gFnSc2	kvantita
(	(	kIx(	(
<g/>
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
×	×	k?	×
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
přízvuku	přízvuk	k1gInSc6	přízvuk
<g/>
;	;	kIx,	;
opozice	opozice	k1gFnSc1	opozice
znělosti	znělost	k1gFnSc2	znělost
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
pravých	pravý	k2eAgFnPc2d1	pravá
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
–	–	k?	–
<g/>
z	z	k7c2	z
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
–	–	k?	–
<g/>
b	b	k?	b
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asimilace	asimilace	k1gFnSc1	asimilace
znělosti	znělost	k1gFnSc2	znělost
<g/>
;	;	kIx,	;
ztráta	ztráta	k1gFnSc1	ztráta
znělosti	znělost	k1gFnSc2	znělost
v	v	k7c6	v
koncových	koncový	k2eAgFnPc6d1	koncová
pozicích	pozice	k1gFnPc6	pozice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
led	led	k1gInSc1	led
[	[	kIx(	[
<g/>
lɛ	lɛ	k?	lɛ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pevný	pevný	k2eAgInSc1d1	pevný
přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
(	(	kIx(	(
<g/>
×	×	k?	×
u	u	k7c2	u
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
jednoslabičnou	jednoslabičný	k2eAgFnSc7d1	jednoslabičná
předložkou	předložka	k1gFnSc7	předložka
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
předložce	předložka	k1gFnSc6	předložka
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
les	les	k1gInSc1	les
×	×	k?	×
'	'	kIx"	'
<g/>
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
specificky	specificky	k6eAd1	specificky
český	český	k2eAgInSc1d1	český
foném	foném	k1gInSc1	foném
/	/	kIx~	/
<g/>
ř	ř	k?	ř
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
hláskového	hláskový	k2eAgNnSc2d1	hláskové
okolí	okolí	k1gNnSc2	okolí
vyslovován	vyslovován	k2eAgMnSc1d1	vyslovován
buď	buď	k8xC	buď
zněle	zněle	k6eAd1	zněle
(	(	kIx(	(
<g/>
dři	dřít	k5eAaImRp2nS	dřít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nezněle	zněle	k6eNd1	zněle
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc4	tři
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgFnSc1d1	Česká
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
flektivní	flektivní	k2eAgInSc4d1	flektivní
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
ohebný	ohebný	k2eAgInSc1d1	ohebný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
větnou	větný	k2eAgFnSc4d1	větná
syntax	syntax	k1gFnSc4	syntax
(	(	kIx(	(
<g/>
skladbu	skladba	k1gFnSc4	skladba
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
flexe	flexe	k1gFnSc2	flexe
(	(	kIx(	(
<g/>
skloňování	skloňování	k1gNnSc1	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc1	časování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
taková	takový	k3xDgFnSc1	takový
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
bohatstvím	bohatství	k1gNnSc7	bohatství
slovních	slovní	k2eAgInPc2d1	slovní
tvarů	tvar	k1gInPc2	tvar
ohebných	ohebný	k2eAgNnPc2d1	ohebné
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgMnPc4d1	slovní
druhy	druh	k1gMnPc4	druh
Čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tradičně	tradičně	k6eAd1	tradičně
10	[number]	k4	10
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
ohebné	ohebný	k2eAgFnSc6d1	ohebná
a	a	k8xC	a
neohebné	ohebný	k2eNgFnSc6d1	neohebná
<g/>
:	:	kIx,	:
ohebné	ohebný	k2eAgFnSc6d1	ohebná
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
se	s	k7c7	s
–	–	k?	–
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
číslovky	číslovka	k1gFnPc1	číslovka
<g/>
;	;	kIx,	;
časují	časovat	k5eAaImIp3nP	časovat
se	se	k3xPyFc4	se
–	–	k?	–
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
;	;	kIx,	;
neohebné	ohebný	k2eNgNnSc1d1	neohebné
–	–	k?	–
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnPc1	předložka
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnPc1	spojka
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
citoslovce	citoslovce	k1gNnSc1	citoslovce
<g/>
.	.	kIx.	.
</s>
<s>
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
Čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
3	[number]	k4	3
jmenné	jmenný	k2eAgInPc1d1	jmenný
rody	rod	k1gInPc1	rod
<g/>
:	:	kIx,	:
mužský	mužský	k2eAgInSc1d1	mužský
(	(	kIx(	(
<g/>
maskulinum	maskulinum	k1gNnSc1	maskulinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
(	(	kIx(	(
<g/>
femininum	femininum	k1gNnSc1	femininum
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
(	(	kIx(	(
<g/>
neutrum	neutrum	k1gNnSc4	neutrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
životný	životný	k2eAgInSc4d1	životný
a	a	k8xC	a
neživotný	životný	k2eNgInSc4d1	neživotný
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	s	k7c7	s
dvojí	dvojí	k4xRgNnSc4	dvojí
mluvnické	mluvnický	k2eAgNnSc4d1	mluvnické
číslo	číslo	k1gNnSc4	číslo
<g/>
:	:	kIx,	:
jednotné	jednotný	k2eAgMnPc4d1	jednotný
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
<g/>
)	)	kIx)	)
a	a	k8xC	a
množné	množný	k2eAgFnPc1d1	množná
(	(	kIx(	(
<g/>
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
dvojného	dvojný	k2eAgNnSc2d1	dvojné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
duálu	duál	k1gInSc2	duál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
České	český	k2eAgNnSc1d1	české
skloňování	skloňování	k1gNnSc1	skloňování
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
vokativ	vokativ	k1gInSc1	vokativ
<g/>
,	,	kIx,	,
lokál	lokál	k1gInSc1	lokál
<g/>
,	,	kIx,	,
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
podstatných	podstatný	k2eAgFnPc2d1	podstatná
a	a	k8xC	a
přídavných	přídavný	k2eAgFnPc2d1	přídavná
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
číslovek	číslovka	k1gFnPc2	číslovka
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
tvarem	tvar	k1gInSc7	tvar
(	(	kIx(	(
<g/>
lemmatem	lemma	k1gNnSc7	lemma
<g/>
)	)	kIx)	)
jmen	jméno	k1gNnPc2	jméno
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
nominativ	nominativ	k1gInSc1	nominativ
singuláru	singulár	k1gInSc2	singulár
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc4	pád
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časování	časování	k1gNnSc1	časování
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgNnPc1d1	české
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgNnPc1d1	české
slovesa	sloveso	k1gNnPc1	sloveso
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
3	[number]	k4	3
časy	čas	k1gInPc4	čas
<g/>
:	:	kIx,	:
minulý	minulý	k2eAgInSc1d1	minulý
(	(	kIx(	(
<g/>
préteritum	préteritum	k1gNnSc1	préteritum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc1d1	přítomný
(	(	kIx(	(
<g/>
prézens	prézens	k1gInSc1	prézens
<g/>
)	)	kIx)	)
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
(	(	kIx(	(
<g/>
futurum	futurum	k1gNnSc1	futurum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
schopnost	schopnost	k1gFnSc4	schopnost
rozlišit	rozlišit	k5eAaPmF	rozlišit
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
plynutí	plynutí	k1gNnSc3	plynutí
času	čas	k1gInSc2	čas
a	a	k8xC	a
ukončenosti	ukončenost	k1gFnSc2	ukončenost
děje	děj	k1gInSc2	děj
pomocí	pomocí	k7c2	pomocí
vidu	vid	k1gInSc2	vid
(	(	kIx(	(
<g/>
aspektu	aspekt	k1gInSc2	aspekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dokonavá	dokonavý	k2eAgNnPc1d1	dokonavé
(	(	kIx(	(
<g/>
perfektiva	perfektivum	k1gNnPc1	perfektivum
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedokonavá	dokonavý	k2eNgNnPc1d1	nedokonavé
(	(	kIx(	(
<g/>
imperfektiva	imperfektivum	k1gNnPc1	imperfektivum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokonavá	dokonavý	k2eAgNnPc4d1	dokonavé
slovesa	sloveso	k1gNnPc4	sloveso
nemají	mít	k5eNaImIp3nP	mít
schopnost	schopnost	k1gFnSc1	schopnost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
přítomnost	přítomnost	k1gFnSc4	přítomnost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
přítomné	přítomný	k2eAgInPc1d1	přítomný
tvary	tvar	k1gInPc1	tvar
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
3	[number]	k4	3
slovesné	slovesný	k2eAgInPc4d1	slovesný
způsoby	způsob	k1gInPc4	způsob
<g/>
:	:	kIx,	:
oznamovací	oznamovací	k2eAgInSc4d1	oznamovací
(	(	kIx(	(
<g/>
indikativ	indikativ	k1gInSc4	indikativ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
(	(	kIx(	(
<g/>
kondicionál	kondicionál	k1gInSc1	kondicionál
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
(	(	kIx(	(
<g/>
imperativ	imperativ	k1gInSc1	imperativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
podmětu	podmět	k1gInSc2	podmět
k	k	k7c3	k
ději	děj	k1gInSc3	děj
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
činným	činný	k2eAgNnSc7d1	činné
(	(	kIx(	(
<g/>
aktivum	aktivum	k1gNnSc1	aktivum
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
trpným	trpný	k2eAgNnSc7d1	trpné
(	(	kIx(	(
<g/>
pasivum	pasivum	k1gNnSc1	pasivum
<g/>
)	)	kIx)	)
rodem	rod	k1gInSc7	rod
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
tvarem	tvar	k1gInSc7	tvar
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
je	být	k5eAaImIp3nS	být
infinitiv	infinitiv	k1gInSc1	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Český	český	k2eAgInSc4d1	český
slovosled	slovosled	k1gInSc4	slovosled
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
flexibilní	flexibilní	k2eAgInSc1d1	flexibilní
(	(	kIx(	(
<g/>
volný	volný	k2eAgInSc1d1	volný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
především	především	k9	především
tzv.	tzv.	kA	tzv.
aktuálním	aktuální	k2eAgNnSc7d1	aktuální
větným	větný	k2eAgNnSc7d1	větné
členěním	členění	k1gNnSc7	členění
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
typu	typa	k1gFnSc4	typa
SVO	SVO	kA	SVO
(	(	kIx(	(
<g/>
podmět	podmět	k1gInSc1	podmět
<g/>
–	–	k?	–
<g/>
přísudek	přísudek	k1gInSc1	přísudek
<g/>
–	–	k?	–
<g/>
předmět	předmět	k1gInSc1	předmět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Češtinu	čeština	k1gFnSc4	čeština
používá	používat	k5eAaImIp3nS	používat
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
používání	používání	k1gNnSc1	používání
však	však	k9	však
není	být	k5eNaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
speciálním	speciální	k2eAgInSc7d1	speciální
jazykovým	jazykový	k2eAgInSc7d1	jazykový
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
příslušných	příslušný	k2eAgInPc2d1	příslušný
zákonů	zákon	k1gInPc2	zákon
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
orgány	orgán	k1gInPc1	orgán
činné	činný	k2eAgInPc1d1	činný
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
a	a	k8xC	a
úřady	úřad	k1gInPc1	úřad
vedou	vést	k5eAaImIp3nP	vést
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
vyhotovují	vyhotovovat	k5eAaImIp3nP	vyhotovovat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
finanční	finanční	k2eAgInPc1d1	finanční
úřady	úřad	k1gInPc1	úřad
též	též	k9	též
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
neovládá	ovládat	k5eNaImIp3nS	ovládat
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc4	facto
na	na	k7c4	na
tlumočení	tlumočení	k1gNnSc4	tlumočení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prodávané	prodávaný	k2eAgNnSc1d1	prodávané
zboží	zboží	k1gNnSc1	zboží
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
opatřeno	opatřit	k5eAaPmNgNnS	opatřit
návodem	návod	k1gInSc7	návod
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Listiny	listina	k1gFnSc2	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
ústavního	ústavní	k2eAgInSc2d1	ústavní
pořádku	pořádek	k1gInSc2	pořádek
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
národnostní	národnostní	k2eAgFnPc1d1	národnostní
a	a	k8xC	a
etnické	etnický	k2eAgFnPc1d1	etnická
menšiny	menšina	k1gFnPc1	menšina
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
též	též	k9	též
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
24	[number]	k4	24
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
oficiálních	oficiální	k2eAgInPc2d1	oficiální
(	(	kIx(	(
<g/>
úředních	úřední	k2eAgInPc2d1	úřední
<g/>
)	)	kIx)	)
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
všechny	všechen	k3xTgInPc4	všechen
oficiální	oficiální	k2eAgInPc4d1	oficiální
jazyky	jazyk	k1gInPc4	jazyk
EU	EU	kA	EU
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
autoritu	autorita	k1gFnSc4	autorita
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
pokládán	pokládat	k5eAaImNgInS	pokládat
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vědeckých	vědecký	k2eAgInPc2d1	vědecký
ústavů	ústav	k1gInPc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
i	i	k8xC	i
aplikovaný	aplikovaný	k2eAgInSc1d1	aplikovaný
výzkum	výzkum	k1gInSc1	výzkum
současné	současný	k2eAgFnSc2d1	současná
češtiny	čeština	k1gFnSc2	čeština
i	i	k8xC	i
její	její	k3xOp3gFnSc2	její
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vydává	vydávat	k5eAaPmIp3nS	vydávat
doporučující	doporučující	k2eAgFnPc4d1	doporučující
publikace	publikace	k1gFnPc4	publikace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
Akademický	akademický	k2eAgInSc1d1	akademický
slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
poradnu	poradna	k1gFnSc4	poradna
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
užívání	užívání	k1gNnSc1	užívání
uvedených	uvedený	k2eAgFnPc2d1	uvedená
příruček	příručka	k1gFnPc2	příručka
ke	k	k7c3	k
školní	školní	k2eAgFnSc3d1	školní
výuce	výuka	k1gFnSc3	výuka
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
závazně	závazně	k6eAd1	závazně
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
respektování	respektování	k1gNnSc1	respektování
těchto	tento	k3xDgFnPc2	tento
zásad	zásada	k1gFnPc2	zásada
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
češtiny	čeština	k1gFnSc2	čeština
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
