<s>
Duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
fotometeor	fotometeor	k1gInSc4	fotometeor
(	(	kIx(	(
<g/>
optický	optický	k2eAgInSc4d1	optický
úkaz	úkaz	k1gInSc4	úkaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
jako	jako	k9	jako
skupina	skupina	k1gFnSc1	skupina
soustředných	soustředný	k2eAgInPc2d1	soustředný
barevných	barevný	k2eAgInPc2d1	barevný
oblouků	oblouk	k1gInPc2	oblouk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
lomem	lom	k1gInSc7	lom
a	a	k8xC	a
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
odrazem	odraz	k1gInSc7	odraz
slunečního	sluneční	k2eAgNnSc2d1	sluneční
nebo	nebo	k8xC	nebo
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
vodních	vodní	k2eAgFnPc6d1	vodní
kapkách	kapka	k1gFnPc6	kapka
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
úkaz	úkaz	k1gInSc1	úkaz
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
v	v	k7c6	v
drobných	drobný	k2eAgInPc6d1	drobný
ledových	ledový	k2eAgInPc6d1	ledový
krystalech	krystal	k1gInPc6	krystal
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
deště	dešť	k1gInSc2	dešť
nebo	nebo	k8xC	nebo
mlhy	mlha	k1gFnSc2	mlha
prochází	procházet	k5eAaImIp3nS	procházet
světlo	světlo	k1gNnSc1	světlo
každou	každý	k3xTgFnSc7	každý
jednotlivou	jednotlivý	k2eAgFnSc7d1	jednotlivá
kapkou	kapka	k1gFnSc7	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
voda	voda	k1gFnSc1	voda
větší	veliký	k2eAgFnSc1d2	veliký
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
láme	lámat	k5eAaImIp3nS	lámat
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
povrch	povrch	k1gInSc1	povrch
kapky	kapka	k1gFnSc2	kapka
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barevné	barevný	k2eAgFnPc4d1	barevná
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
odrážejí	odrážet	k5eAaImIp3nP	odrážet
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stěně	stěna	k1gFnSc6	stěna
a	a	k8xC	a
opouštějí	opouštět	k5eAaImIp3nP	opouštět
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
úhly	úhel	k1gInPc7	úhel
kapku	kapka	k1gFnSc4	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Kapky	kapka	k1gFnPc1	kapka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
úhlové	úhlový	k2eAgFnSc6d1	úhlová
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
Slunce	slunce	k1gNnSc2	slunce
či	či	k8xC	či
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
duha	duha	k1gFnSc1	duha
tvar	tvar	k1gInSc1	tvar
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
úhlem	úhel	k1gInSc7	úhel
dopadu	dopad	k1gInSc2	dopad
<g/>
,	,	kIx,	,
úhlem	úhel	k1gInSc7	úhel
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
optického	optický	k2eAgNnSc2d1	optické
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
šíření	šíření	k1gNnSc2	šíření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
Snellův	Snellův	k2eAgInSc1d1	Snellův
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
duha	duha	k1gFnSc1	duha
vzniká	vznikat	k5eAaImIp3nS	vznikat
odrazem	odraz	k1gInSc7	odraz
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
kapkách	kapka	k1gFnPc6	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
duhu	duha	k1gFnSc4	duha
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
v	v	k7c6	v
letech	let	k1gInPc6	let
1635	[number]	k4	1635
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vznik	vznik	k1gInSc1	vznik
barev	barva	k1gFnPc2	barva
duhy	duha	k1gFnSc2	duha
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
Isaacu	Isaac	k1gMnSc3	Isaac
Newtonovi	Newton	k1gMnSc3	Newton
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
paprsek	paprsek	k1gInSc1	paprsek
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
hranolem	hranol	k1gInSc7	hranol
nebo	nebo	k8xC	nebo
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
kapkou	kapka	k1gFnSc7	kapka
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
oblouku	oblouk	k1gInSc2	oblouk
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
výšce	výška	k1gFnSc6	výška
slunce	slunce	k1gNnSc2	slunce
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
–	–	k?	–
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc1	slunce
právě	právě	k6eAd1	právě
na	na	k7c6	na
horizontu	horizont	k1gInSc6	horizont
<g/>
,	,	kIx,	,
duha	duha	k1gFnSc1	duha
bude	být	k5eAaImBp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
půlkruh	půlkruh	k1gInSc4	půlkruh
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
např.	např.	kA	např.
v	v	k7c6	v
balónu	balón	k1gInSc6	balón
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
spatřit	spatřit	k5eAaPmF	spatřit
duhu	duha	k1gFnSc4	duha
i	i	k9	i
jako	jako	k9	jako
celý	celý	k2eAgInSc1d1	celý
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
kruh	kruh	k1gInSc1	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
zřetelnější	zřetelný	k2eAgFnSc1d2	zřetelnější
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
větší	veliký	k2eAgFnPc1d2	veliký
jsou	být	k5eAaImIp3nP	být
dešťové	dešťový	k2eAgFnPc1d1	dešťová
kapky	kapka	k1gFnPc1	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Úhlový	úhlový	k2eAgInSc1d1	úhlový
poloměr	poloměr	k1gInSc1	poloměr
duhy	duha	k1gFnSc2	duha
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
a	a	k8xC	a
fialové	fialový	k2eAgNnSc1d1	fialové
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k7c2	uvnitř
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
vně	vně	k6eAd1	vně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dešťové	dešťový	k2eAgFnSc6d1	dešťová
kapce	kapka	k1gFnSc6	kapka
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
více	hodně	k6eAd2	hodně
odrazům	odraz	k1gInPc3	odraz
světelného	světelný	k2eAgInSc2d1	světelný
paprsku	paprsek	k1gInSc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dvou	dva	k4xCgInPc6	dva
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
odrazech	odraz	k1gInPc6	odraz
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sekundární	sekundární	k2eAgFnSc4d1	sekundární
duhu	duha	k1gFnSc4	duha
neboli	neboli	k8xC	neboli
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
duhu	duha	k1gFnSc4	duha
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
dvojité	dvojitý	k2eAgFnSc6d1	dvojitá
duze	duha	k1gFnSc6	duha
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
duha	duha	k1gFnSc1	duha
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nad	nad	k7c7	nad
první	první	k4xOgFnSc7	první
duhou	duha	k1gFnSc7	duha
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
primární	primární	k2eAgNnSc1d1	primární
nebo	nebo	k8xC	nebo
hlavní	hlavní	k2eAgNnSc1d1	hlavní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
úhlový	úhlový	k2eAgInSc4d1	úhlový
poloměr	poloměr	k1gInSc4	poloměr
než	než	k8xS	než
primární	primární	k2eAgFnSc1d1	primární
duha	duha	k1gFnSc1	duha
(	(	kIx(	(
<g/>
asi	asi	k9	asi
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
–	–	k?	–
<g/>
54	[number]	k4	54
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
posloupnost	posloupnost	k1gFnSc4	posloupnost
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
sekundární	sekundární	k2eAgFnSc6d1	sekundární
duze	duha	k1gFnSc6	duha
je	být	k5eAaImIp3nS	být
opačná	opačný	k2eAgFnSc1d1	opačná
než	než	k8xS	než
v	v	k7c6	v
duze	duha	k1gFnSc6	duha
primární	primární	k2eAgNnSc1d1	primární
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
duha	duha	k1gFnSc1	duha
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
výrazně	výrazně	k6eAd1	výrazně
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
primární	primární	k2eAgFnSc1d1	primární
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
podmínek	podmínka	k1gFnPc2	podmínka
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
také	také	k9	také
třetí	třetí	k4xOgInSc4	třetí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
terciární	terciární	k2eAgFnSc4d1	terciární
duhu	duha	k1gFnSc4	duha
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
o	o	k7c6	o
trojité	trojitý	k2eAgFnSc6d1	trojitá
duze	duha	k1gFnSc6	duha
<g/>
.	.	kIx.	.
</s>
<s>
Terciární	terciární	k2eAgFnSc1d1	terciární
duha	duha	k1gFnSc1	duha
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
úhlový	úhlový	k2eAgInSc4d1	úhlový
poloměr	poloměr	k1gInSc4	poloměr
než	než	k8xS	než
sekundární	sekundární	k2eAgFnSc1d1	sekundární
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Terciární	terciární	k2eAgFnSc1d1	terciární
duha	duha	k1gFnSc1	duha
bývá	bývat	k5eAaImIp3nS	bývat
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
výrazná	výrazný	k2eAgFnSc1d1	výrazná
než	než	k8xS	než
sekundární	sekundární	k2eAgFnSc1d1	sekundární
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
úhlové	úhlový	k2eAgFnSc6d1	úhlová
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
39	[number]	k4	39
<g/>
°	°	k?	°
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
<g/>
°	°	k?	°
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
při	při	k7c6	při
vhodných	vhodný	k2eAgFnPc6d1	vhodná
podmínkách	podmínka	k1gFnPc6	podmínka
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
odcloněna	odclonit	k5eAaPmNgFnS	odclonit
slunečním	sluneční	k2eAgInSc7d1	sluneční
svitem	svit	k1gInSc7	svit
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
duha	duha	k1gFnSc1	duha
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
průměr	průměr	k1gInSc4	průměr
45	[number]	k4	45
<g/>
°	°	k?	°
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
pátá	pátá	k1gFnSc1	pátá
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
opět	opět	k6eAd1	opět
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jako	jako	k8xC	jako
primární	primární	k2eAgFnSc1d1	primární
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
vyšší	vysoký	k2eAgInPc4d2	vyšší
řády	řád	k1gInPc4	řád
duhy	duha	k1gFnSc2	duha
<g/>
.	.	kIx.	.
</s>
<s>
Ohybem	ohyb	k1gInSc7	ohyb
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
dešťových	dešťový	k2eAgFnPc6d1	dešťová
kapkách	kapka	k1gFnPc6	kapka
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
hlavní	hlavní	k2eAgFnSc2d1	hlavní
duhy	duha	k1gFnSc2	duha
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
i	i	k9	i
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
duhy	duha	k1gFnSc2	duha
<g/>
)	)	kIx)	)
vznikat	vznikat	k5eAaImF	vznikat
tzv.	tzv.	kA	tzv.
podružné	podružný	k2eAgInPc4d1	podružný
duhové	duhový	k2eAgInPc4d1	duhový
oblouky	oblouk	k1gInPc4	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
nejzřetelnější	zřetelný	k2eAgInPc1d3	nejzřetelnější
při	při	k7c6	při
vrcholu	vrchol	k1gInSc6	vrchol
duhy	duha	k1gFnSc2	duha
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Kruhová	kruhový	k2eAgFnSc1d1	kruhová
duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
halový	halový	k2eAgInSc4d1	halový
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
lomem	lom	k1gInSc7	lom
slunečního	sluneční	k2eAgNnSc2d1	sluneční
nebo	nebo	k8xC	nebo
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
drobných	drobný	k2eAgInPc6d1	drobný
ledových	ledový	k2eAgInPc6d1	ledový
krystalcích	krystalek	k1gInPc6	krystalek
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Termínem	termín	k1gInSc7	termín
bílá	bílý	k2eAgFnSc1d1	bílá
duha	duha	k1gFnSc1	duha
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
duha	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
minimálně	minimálně	k6eAd1	minimálně
rozlišené	rozlišený	k2eAgFnPc4d1	rozlišená
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
bílý	bílý	k2eAgInSc1d1	bílý
oblouk	oblouk	k1gInSc1	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
buď	buď	k8xC	buď
při	při	k7c6	při
dešti	dešť	k1gInSc6	dešť
s	s	k7c7	s
kapkami	kapka	k1gFnPc7	kapka
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
mikroskopických	mikroskopický	k2eAgFnPc6d1	mikroskopická
kapičkách	kapička	k1gFnPc6	kapička
mlhy	mlha	k1gFnSc2	mlha
(	(	kIx(	(
<g/>
mlžná	mlžný	k2eAgFnSc1d1	mlžná
duha	duha	k1gFnSc1	duha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
duha	duha	k1gFnSc1	duha
vzniká	vznikat	k5eAaImIp3nS	vznikat
lomem	lom	k1gInSc7	lom
měsíčního	měsíční	k2eAgNnSc2d1	měsíční
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
vodních	vodní	k2eAgFnPc6d1	vodní
kapkách	kapka	k1gFnPc6	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
duha	duha	k1gFnSc1	duha
má	mít	k5eAaImIp3nS	mít
nevýrazné	výrazný	k2eNgNnSc4d1	nevýrazné
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
bílou	bílý	k2eAgFnSc4d1	bílá
nebo	nebo	k8xC	nebo
nažloutlou	nažloutlý	k2eAgFnSc4d1	nažloutlá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Převrácená	převrácený	k2eAgFnSc1d1	převrácená
duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tzv.	tzv.	kA	tzv.
cirkumzenitální	cirkumzenitální	k2eAgInSc4d1	cirkumzenitální
oblouk	oblouk	k1gInSc4	oblouk
<g/>
,	,	kIx,	,
odrůda	odrůda	k1gFnSc1	odrůda
halového	halový	k2eAgInSc2d1	halový
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
vzniku	vznik	k1gInSc3	vznik
jsou	být	k5eAaImIp3nP	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
ledové	ledový	k2eAgInPc1d1	ledový
krystaly	krystal	k1gInPc1	krystal
tvaru	tvar	k1gInSc2	tvar
destičky	destička	k1gFnSc2	destička
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
měsíční	měsíční	k2eAgInPc4d1	měsíční
<g/>
)	)	kIx)	)
paprsky	paprsek	k1gInPc4	paprsek
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
vrchní	vrchní	k2eAgFnSc7d1	vrchní
stranou	strana	k1gFnSc7	strana
krystalu	krystal	k1gInSc2	krystal
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
vertikální	vertikální	k2eAgFnSc7d1	vertikální
boční	boční	k2eAgFnSc7d1	boční
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
úhlu	úhel	k1gInSc3	úhel
vycházejících	vycházející	k2eAgInPc2d1	vycházející
paprsků	paprsek	k1gInPc2	paprsek
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
barvy	barva	k1gFnPc4	barva
tohoto	tento	k3xDgInSc2	tento
halového	halový	k2eAgInSc2d1	halový
jevu	jev	k1gInSc2	jev
mnohem	mnohem	k6eAd1	mnohem
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
klasické	klasický	k2eAgFnSc2d1	klasická
duhy	duha	k1gFnSc2	duha
vznikající	vznikající	k2eAgFnSc2d1	vznikající
na	na	k7c6	na
vodních	vodní	k2eAgFnPc6d1	vodní
kapkách	kapka	k1gFnPc6	kapka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
jev	jev	k1gInSc4	jev
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
olejovém	olejový	k2eAgNnSc6d1	olejové
plátně	plátno	k1gNnSc6	plátno
Constantina	Constantin	k1gMnSc2	Constantin
Brumidiho	Brumidi	k1gMnSc2	Brumidi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
s	s	k7c7	s
názvem	název	k1gInSc7	název
Apoteóza	apoteóza	k1gFnSc1	apoteóza
George	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
otce	otec	k1gMnSc2	otec
Ameriky	Amerika	k1gFnSc2	Amerika
výraz	výraz	k1gInSc4	výraz
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
klene	klenout	k5eAaImIp3nS	klenout
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
duha	duha	k1gFnSc1	duha
a	a	k8xC	a
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
ho	on	k3xPp3gNnSc2	on
třináct	třináct	k4xCc1	třináct
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Washingtonem	Washington	k1gInSc7	Washington
je	být	k5eAaImIp3nS	být
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
Lady	lady	k1gFnSc1	lady
Liberty	Libert	k1gInPc4	Libert
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nedopustí	dopustit	k5eNaPmIp3nS	dopustit
pošlapávání	pošlapávání	k1gNnSc4	pošlapávání
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
z	z	k7c2	z
pravomoci	pravomoc	k1gFnSc2	pravomoc
králů	král	k1gMnPc2	král
a	a	k8xC	a
tyranů	tyran	k1gMnPc2	tyran
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olejomalbě	olejomalba	k1gFnSc6	olejomalba
Johna	John	k1gMnSc2	John
Everetta	Everetto	k1gNnSc2	Everetto
Millaise	Millaise	k1gFnSc2	Millaise
Slepá	slepý	k2eAgFnSc1d1	slepá
dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
je	být	k5eAaImIp3nS	být
duha	duha	k1gFnSc1	duha
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
přírodních	přírodní	k2eAgFnPc2d1	přírodní
krás	krása	k1gFnPc2	krása
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nevidomá	vidomý	k2eNgFnSc1d1	nevidomá
dívka	dívka	k1gFnSc1	dívka
nemůže	moct	k5eNaImIp3nS	moct
zažít	zažít	k5eAaPmF	zažít
–	–	k?	–
užita	užít	k5eAaPmNgFnS	užít
ke	k	k7c3	k
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
patosu	patos	k1gInSc2	patos
jejího	její	k3xOp3gInSc2	její
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
uloupené	uloupený	k2eAgFnSc6d1	Uloupená
duze	duha	k1gFnSc6	duha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
pohádka	pohádka	k1gFnSc1	pohádka
Jak	jak	k8xS	jak
Rumcajs	Rumcajs	k1gInSc1	Rumcajs
vysadil	vysadit	k5eAaPmAgInS	vysadit
duhu	duha	k1gFnSc4	duha
na	na	k7c4	na
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Duha	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInSc1d1	přírodní
jev	jev	k1gInSc1	jev
známý	známý	k2eAgMnSc1d1	známý
svou	svůj	k3xOyFgFnSc7	svůj
krásou	krása	k1gFnSc7	krása
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
mytologií	mytologie	k1gFnPc2	mytologie
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Norová	k1gFnSc2	Norová
ji	on	k3xPp3gFnSc4	on
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k8xC	jako
Bifröst	Bifröst	k1gInSc4	Bifröst
–	–	k?	–
most	most	k1gInSc1	most
mezi	mezi	k7c7	mezi
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
židovsko-křesťanské	židovskořesťanský	k2eAgFnPc1d1	židovsko-křesťanská
tradice	tradice	k1gFnPc1	tradice
ji	on	k3xPp3gFnSc4	on
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nemůže	moct	k5eNaImIp3nS	moct
povodní	povodní	k2eAgMnSc1d1	povodní
zničit	zničit	k5eAaPmF	zničit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Najít	najít	k5eAaPmF	najít
mytologii	mytologie	k1gFnSc4	mytologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
duhu	duha	k1gFnSc4	duha
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
opravdovou	opravdový	k2eAgFnSc7d1	opravdová
výzvou	výzva	k1gFnSc7	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
kultury	kultura	k1gFnPc1	kultura
a	a	k8xC	a
kontinenty	kontinent	k1gInPc1	kontinent
vnímají	vnímat	k5eAaImIp3nP	vnímat
všichni	všechen	k3xTgMnPc1	všechen
duhu	duha	k1gFnSc4	duha
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
představivosti	představivost	k1gFnSc2	představivost
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
posla	posel	k1gMnSc4	posel
<g/>
,	,	kIx,	,
luk	luk	k1gInSc4	luk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
duha	duha	k1gFnSc1	duha
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
slouží	sloužit	k5eAaImIp3nS	sloužit
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
chtěli	chtít	k5eAaImAgMnP	chtít
pochopit	pochopit	k5eAaPmF	pochopit
svět	svět	k1gInSc4	svět
kolem	kolem	k6eAd1	kolem
nás	my	k3xPp1nPc4	my
a	a	k8xC	a
pochopit	pochopit	k5eAaPmF	pochopit
význam	význam	k1gInSc4	význam
a	a	k8xC	a
původ	původ	k1gInSc4	původ
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Kultury	kultura	k1gFnPc1	kultura
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
systém	systém	k1gInSc4	systém
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
vysvětlovaly	vysvětlovat	k5eAaImAgFnP	vysvětlovat
historii	historie	k1gFnSc4	historie
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
duha	duha	k1gFnSc1	duha
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
hrála	hrát	k5eAaImAgFnS	hrát
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
nepolapitelnou	polapitelný	k2eNgFnSc4d1	nepolapitelná
<g/>
,	,	kIx,	,
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
<g/>
,	,	kIx,	,
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
tajuplnou	tajuplný	k2eAgFnSc4d1	tajuplná
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přesahovala	přesahovat	k5eAaImAgFnS	přesahovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
starověkých	starověký	k2eAgFnPc2d1	starověká
i	i	k8xC	i
moderních	moderní	k2eAgFnPc6d1	moderní
kulturách	kultura	k1gFnPc6	kultura
do	do	k7c2	do
systému	systém	k1gInSc2	systém
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
dotkne	dotknout	k5eAaPmIp3nS	dotknout
duhy	duha	k1gFnPc4	duha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vytažen	vytáhnout	k5eAaPmNgMnS	vytáhnout
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
Planetnik	Planetnik	k1gMnSc1	Planetnik
–	–	k?	–
napůl	napůl	k6eAd1	napůl
démonické	démonický	k2eAgNnSc1d1	démonické
stvoření	stvoření	k1gNnSc1	stvoření
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yQgInSc7	který
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
bůh	bůh	k1gMnSc1	bůh
hromů	hrom	k1gInPc2	hrom
a	a	k8xC	a
blesků	blesk	k1gInPc2	blesk
Perun	perun	k1gInSc1	perun
<g/>
.	.	kIx.	.
</s>
<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
starověký	starověký	k2eAgMnSc1d1	starověký
Sumerský	sumerský	k2eAgMnSc1d1	sumerský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
ca	ca	kA	ca
<g/>
.	.	kIx.	.
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
nl	nl	k?	nl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
první	první	k4xOgInPc4	první
podrobné	podrobný	k2eAgInPc4d1	podrobný
písemné	písemný	k2eAgInPc4d1	písemný
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
civilizaci	civilizace	k1gFnSc6	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgInSc6d1	viktoriánský
překladu	překlad	k1gInSc6	překlad
Gilgameše	Gilgameše	k1gFnSc1	Gilgameše
Leonida	Leonida	k1gFnSc1	Leonida
Hamiltona	Hamiltona	k1gFnSc1	Hamiltona
Epos	epos	k1gInSc4	epos
Ishtar	Ishtar	k1gInSc1	Ishtar
a	a	k8xC	a
Izdubar	Izdubar	k1gInSc1	Izdubar
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
král	král	k1gMnSc1	král
Izdubar	Izdubar	k1gMnSc1	Izdubar
"	"	kIx"	"
<g/>
barevnou	barevný	k2eAgFnSc4d1	barevná
hmotu	hmota	k1gFnSc4	hmota
jako	jako	k8xS	jako
odstíny	odstín	k1gInPc4	odstín
duhy	duha	k1gFnSc2	duha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
božím	boží	k2eAgNnSc7d1	boží
schválením	schválení	k1gNnSc7	schválení
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Izdubar	Izdubar	k1gInSc1	Izdubar
vidí	vidět	k5eAaImIp3nS	vidět
"	"	kIx"	"
<g/>
jiskřivé	jiskřivý	k2eAgFnPc1d1	jiskřivá
barvy	barva	k1gFnPc1	barva
duhového	duhový	k2eAgInSc2d1	duhový
oblouku	oblouk	k1gInSc2	oblouk
<g/>
"	"	kIx"	"
ve	v	k7c6	v
fontáně	fontána	k1gFnSc6	fontána
života	život	k1gInSc2	život
u	u	k7c2	u
Elamova	Elamův	k2eAgInSc2d1	Elamův
Stromu	strom	k1gInSc2	strom
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Sumerský	sumerský	k2eAgMnSc1d1	sumerský
bůh	bůh	k1gMnSc1	bůh
země	zem	k1gFnSc2	zem
Ninurta	Ninurta	k1gFnSc1	Ninurta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Sumer	Sumer	k1gInSc1	Sumer
brání	bránit	k5eAaImIp3nS	bránit
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
a	a	k8xC	a
šípy	šíp	k1gInPc7	šíp
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
korunu	koruna	k1gFnSc4	koruna
z	z	k7c2	z
duhy	duha	k1gFnSc2	duha
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
Duhový	duhový	k2eAgInSc1d1	duhový
most	most	k1gInSc1	most
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
Bifröst	Bifröst	k1gFnSc1	Bifröst
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
spojuje	spojovat	k5eAaImIp3nS	spojovat
Zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
Ásgardem	Ásgard	k1gInSc7	Ásgard
<g/>
,	,	kIx,	,
domovem	domov	k1gInSc7	domov
norských	norský	k2eAgMnPc2d1	norský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Bifröst	Bifröst	k1gFnSc4	Bifröst
mohou	moct	k5eAaImIp3nP	moct
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
rozbit	rozbít	k5eAaPmNgInS	rozbít
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
Ragnarök	Ragnaröka	k1gFnPc2	Ragnaröka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Götterdämmerung	Götterdämmerung	k1gMnSc1	Götterdämmerung
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Soumrak	soumrak	k1gInSc1	soumrak
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
duhový	duhový	k2eAgInSc1d1	duhový
most	most	k1gInSc1	most
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
je	být	k5eAaImIp3nS	být
dosažitelný	dosažitelný	k2eAgInSc1d1	dosažitelný
pouze	pouze	k6eAd1	pouze
dobrým	dobrý	k2eAgInSc7d1	dobrý
nebo	nebo	k8xC	nebo
ctnostným	ctnostný	k2eAgInSc7d1	ctnostný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
mýtu	mýtus	k1gInSc2	mýtus
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
amatérský	amatérský	k2eAgMnSc1d1	amatérský
etymolog	etymolog	k1gMnSc1	etymolog
Christopher	Christophra	k1gFnPc2	Christophra
Houmann	Houmann	k1gMnSc1	Houmann
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
společnou	společný	k2eAgFnSc4d1	společná
historii	historie	k1gFnSc4	historie
Indo-evropských	Indovropský	k2eAgInPc2d1	Indo-evropský
národů	národ	k1gInPc2	národ
by	by	kYmCp3nS	by
Asgard	Asgard	k1gMnSc1	Asgard
na	na	k7c6	na
konci	konec	k1gInSc6	konec
duhy	duha	k1gFnSc2	duha
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
symbolický	symbolický	k2eAgInSc1d1	symbolický
význam	význam	k1gInSc1	význam
starověké	starověký	k2eAgFnSc2d1	starověká
znalosti	znalost	k1gFnSc2	znalost
čaker	čakra	k1gFnPc2	čakra
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řeckém	řecký	k2eAgInSc6d1	řecký
starověkém	starověký	k2eAgInSc6d1	starověký
pantheonu	pantheon	k1gInSc6	pantheon
je	být	k5eAaImIp3nS	být
význam	význam	k1gInSc4	význam
bohyně	bohyně	k1gFnSc1	bohyně
Iris	iris	k1gFnSc1	iris
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dcera	dcera	k1gFnSc1	dcera
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
bohů	bůh	k1gMnPc2	bůh
Electry	Electr	k1gMnPc4	Electr
a	a	k8xC	a
Thaumáse	Thaumás	k1gMnPc4	Thaumás
<g/>
,	,	kIx,	,
nosila	nosit	k5eAaImAgFnS	nosit
Iris	iris	k1gFnSc1	iris
šaty	šat	k1gInPc1	šat
v	v	k7c6	v
duhových	duhový	k2eAgFnPc6d1	Duhová
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
posel	posel	k1gMnSc1	posel
bohů	bůh	k1gMnPc2	bůh
létala	létat	k5eAaImAgFnS	létat
na	na	k7c6	na
zlatých	zlatý	k2eAgNnPc6d1	Zlaté
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Iris	iris	k1gFnSc1	iris
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
"	"	kIx"	"
<g/>
rychlosti	rychlost	k1gFnSc3	rychlost
větru	vítr	k1gInSc2	vítr
<g/>
"	"	kIx"	"
v	v	k7c6	v
devíti	devět	k4xCc2	devět
ze	z	k7c2	z
čtyřiadvaceti	čtyřiadvacet	k4xCc2	čtyřiadvacet
knih	kniha	k1gFnPc2	kniha
Homerovy	Homerův	k2eAgFnPc1d1	Homerova
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řeckých	řecký	k2eAgFnPc6d1	řecká
bájích	báj	k1gFnPc6	báj
Iris	iris	k1gFnSc2	iris
nosí	nosit	k5eAaImIp3nS	nosit
mír	mír	k1gInSc4	mír
nebo	nebo	k8xC	nebo
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
měla	mít	k5eAaImAgFnS	mít
Iris	iris	k1gFnSc4	iris
pro	pro	k7c4	pro
Dia	Dia	k1gFnSc4	Dia
naplnit	naplnit	k5eAaPmF	naplnit
zlatý	zlatý	k2eAgInSc4d1	zlatý
džbán	džbán	k1gInSc4	džbán
svěcenou	svěcený	k2eAgFnSc7d1	svěcená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mnoho	mnoho	k4c1	mnoho
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
také	také	k9	také
Iris	iris	k1gInSc1	iris
neustále	neustále	k6eAd1	neustále
hledá	hledat	k5eAaImIp3nS	hledat
lidstvo	lidstvo	k1gNnSc1	lidstvo
novou	nový	k2eAgFnSc4d1	nová
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Duha	duha	k1gFnSc1	duha
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Iris	iris	k1gFnSc4	iris
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
duha	duha	k1gFnSc1	duha
sama	sám	k3xTgFnSc1	sám
prchavá	prchavý	k2eAgFnSc1d1	prchavá
a	a	k8xC	a
nepředvídatelná	předvídatelný	k2eNgFnSc1d1	nepředvídatelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
australském	australský	k2eAgNnSc6d1	Australské
domorodém	domorodý	k2eAgNnSc6d1	domorodé
bájesloví	bájesloví	k1gNnSc6	bájesloví
Aboriginců	Aboriginec	k1gInPc2	Aboriginec
je	být	k5eAaImIp3nS	být
hadovitá	hadovitý	k2eAgFnSc1d1	hadovitá
duha	duha	k1gFnSc1	duha
Stvořitel	Stvořitel	k1gMnSc1	Stvořitel
(	(	kIx(	(
<g/>
Kurreah	Kurreah	k1gInSc1	Kurreah
<g/>
,	,	kIx,	,
Andrenjinyi	Andrenjinyi	k1gNnSc1	Andrenjinyi
<g/>
,	,	kIx,	,
Yingarna	Yingarna	k1gFnSc1	Yingarna
<g/>
,	,	kIx,	,
Ngalyod	Ngalyod	k1gInSc1	Ngalyod
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
mytologickém	mytologický	k2eAgInSc6d1	mytologický
světě	svět	k1gInSc6	svět
Snění	snění	k1gNnPc2	snění
–	–	k?	–
nekonečném	konečný	k2eNgNnSc6d1	nekonečné
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
stvořením	stvoření	k1gNnSc7	stvoření
světa	svět	k1gInSc2	svět
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
věčné	věčný	k2eAgFnPc1d1	věčná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
duha	duha	k1gFnSc1	duha
–	–	k?	–
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
"	"	kIx"	"
<g/>
Snění	sněný	k2eAgMnPc1d1	sněný
<g/>
"	"	kIx"	"
a	a	k8xC	a
každodenní	každodenní	k2eAgInSc1d1	každodenní
život	život	k1gInSc1	život
člověka	člověk	k1gMnSc2	člověk
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
snivá	snivý	k2eAgFnSc1d1	snivá
nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgNnSc1	který
zná	znát	k5eAaImIp3nS	znát
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgMnSc1	každý
australský	australský	k2eAgMnSc1d1	australský
domorodec	domorodec	k1gMnSc1	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
kmenech	kmen	k1gInPc6	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
starší	starý	k2eAgInSc4d2	starší
než	než	k8xS	než
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
skutečné	skutečný	k2eAgFnPc4d1	skutečná
duhy	duha	k1gFnPc4	duha
obrovští	obrovský	k2eAgMnPc1d1	obrovský
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zlomyslní	zlomyslný	k2eAgMnPc1d1	zlomyslný
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
obývají	obývat	k5eAaImIp3nP	obývat
oblohu	obloha	k1gFnSc4	obloha
nebo	nebo	k8xC	nebo
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
má	mít	k5eAaImIp3nS	mít
různé	různý	k2eAgInPc4d1	různý
názvy	název	k1gInPc4	název
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
kmenech	kmen	k1gInPc6	kmen
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
jeho	jeho	k3xOp3gInPc1	jeho
znaky	znak	k1gInPc1	znak
z	z	k7c2	z
pokolení	pokolení	k1gNnSc2	pokolení
na	na	k7c4	na
pokolení	pokolení	k1gNnSc4	pokolení
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
australských	australský	k2eAgInPc2d1	australský
kmenů	kmen	k1gInPc2	kmen
však	však	k9	však
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Duhový	duhový	k2eAgMnSc1d1	duhový
had	had	k1gMnSc1	had
je	být	k5eAaImIp3nS	být
stvořitelem	stvořitel	k1gMnSc7	stvořitel
světa	svět	k1gInSc2	svět
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
Duhový	duhový	k2eAgMnSc1d1	duhový
had	had	k1gMnSc1	had
do	do	k7c2	do
hlubokého	hluboký	k2eAgInSc2d1	hluboký
pramene	pramen	k1gInSc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
společným	společný	k2eAgNnSc7d1	společné
tématem	téma	k1gNnSc7	téma
všech	všecek	k3xTgInPc2	všecek
domorodých	domorodý	k2eAgInPc2d1	domorodý
kmenů	kmen	k1gInPc2	kmen
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Duhový	duhový	k2eAgMnSc1d1	duhový
had	had	k1gMnSc1	had
nemá	mít	k5eNaImIp3nS	mít
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Duhový	duhový	k2eAgMnSc1d1	duhový
had	had	k1gMnSc1	had
může	moct	k5eAaImIp3nS	moct
dát	dát	k5eAaPmF	dát
plodnost	plodnost	k1gFnSc4	plodnost
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
úrodu	úroda	k1gFnSc4	úroda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stvoří	stvořit	k5eAaPmIp3nS	stvořit
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
postihovat	postihovat	k5eAaImF	postihovat
lidi	člověk	k1gMnPc4	člověk
slepotou	slepota	k1gFnSc7	slepota
a	a	k8xC	a
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Domorodá	domorodý	k2eAgFnSc1d1	domorodá
duha	duha	k1gFnSc1	duha
znamená	znamenat	k5eAaImIp3nS	znamenat
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
šíří	šířit	k5eAaImIp3nS	šířit
'	'	kIx"	'
<g/>
energii	energie	k1gFnSc4	energie
<g/>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
<g/>
dech	dech	k1gInSc1	dech
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
lidem	člověk	k1gMnPc3	člověk
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ngalyod	Ngalyod	k1gInSc1	Ngalyod
<g/>
,	,	kIx,	,
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
Yingarna	Yingarno	k1gNnSc2	Yingarno
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
Duhového	duhový	k2eAgNnSc2d1	duhové
hada	had	k1gMnSc2	had
Kunwinjku	Kunwinjka	k1gFnSc4	Kunwinjka
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Arnhemu	Arnhem	k1gInSc2	Arnhem
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
vysává	vysávat	k5eAaImIp3nS	vysávat
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
plive	plive	k?	plive
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
déšť	déšť	k1gInSc4	déšť
během	během	k7c2	během
mokré	mokrý	k2eAgFnSc2d1	mokrá
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Iris	iris	k1gFnSc1	iris
<g/>
,	,	kIx,	,
také	také	k9	také
Ngalyod	Ngalyod	k1gInSc1	Ngalyod
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zajistit	zajistit	k5eAaPmF	zajistit
plodnost	plodnost	k1gFnSc1	plodnost
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
zničit	zničit	k5eAaPmF	zničit
i	i	k9	i
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Yingarna	Yingarna	k1gFnSc1	Yingarna
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
lidí	člověk	k1gMnPc2	člověk
Kunwinjku	Kunwinjka	k1gFnSc4	Kunwinjka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
androgyn	androgyn	k1gMnSc1	androgyn
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ngalyod	Ngalyoda	k1gFnPc2	Ngalyoda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rafinovaně	rafinovaně	k6eAd1	rafinovaně
ambivalentní	ambivalentní	k2eAgFnSc4d1	ambivalentní
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
prvků	prvek	k1gInPc2	prvek
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
krokodýla	krokodýl	k1gMnSc4	krokodýl
<g/>
,	,	kIx,	,
sumce	sumec	k1gMnSc4	sumec
<g/>
,	,	kIx,	,
emu	emu	k1gMnSc4	emu
a	a	k8xC	a
klokana	klokan	k1gMnSc4	klokan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biblickém	biblický	k2eAgInSc6d1	biblický
příběhu	příběh	k1gInSc6	příběh
o	o	k7c6	o
potopě	potopa	k1gFnSc6	potopa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
obsaženém	obsažený	k2eAgInSc6d1	obsažený
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Genesis	Genesis	k1gFnSc4	Genesis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
duha	duha	k1gFnSc1	duha
popsána	popsat	k5eAaPmNgFnS	popsat
jako	jako	k8xS	jako
znamení	znamení	k1gNnSc1	znamení
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
Hospodinem	Hospodin	k1gMnSc7	Hospodin
a	a	k8xC	a
Noemem	Noe	k1gMnSc7	Noe
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
potomstvem	potomstvo	k1gNnSc7	potomstvo
a	a	k8xC	a
vším	všecek	k3xTgNnSc7	všecek
živým	živý	k2eAgNnSc7d1	živé
tvorstvem	tvorstvo	k1gNnSc7	tvorstvo
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
Genesis	Genesis	k1gFnSc1	Genesis
9,13	[number]	k4	9,13
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
Duha	duha	k1gFnSc1	duha
jako	jako	k8xS	jako
nebeský	nebeský	k2eAgInSc1d1	nebeský
luk	luk	k1gInSc1	luk
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
starobylých	starobylý	k2eAgFnPc6d1	starobylá
hinduistických	hinduistický	k2eAgFnPc6d1	hinduistická
mytologiích	mytologie	k1gFnPc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Indra	Indra	k1gMnSc1	Indra
–	–	k?	–
hinduistický	hinduistický	k2eAgMnSc1d1	hinduistický
bůh	bůh	k1gMnSc1	bůh
hromu	hrom	k1gInSc2	hrom
a	a	k8xC	a
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
duhu	duha	k1gFnSc4	duha
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
šípů	šíp	k1gInPc2	šíp
jako	jako	k8xS	jako
blesku	blesk	k1gInSc2	blesk
–	–	k?	–
k	k	k7c3	k
zabití	zabití	k1gNnSc3	zabití
Asura	Asuro	k1gNnSc2	Asuro
Vrta	Vrt	k1gInSc2	Vrt
–	–	k?	–
prvotního	prvotní	k2eAgMnSc2d1	prvotní
démonického	démonický	k2eAgMnSc2d1	démonický
hada	had	k1gMnSc2	had
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stvoření	stvoření	k1gNnSc6	stvoření
národa	národ	k1gInSc2	národ
Navajo	Navajo	k6eAd1	Navajo
se	se	k3xPyFc4	se
klenou	klenout	k5eAaImIp3nP	klenout
dvě	dva	k4xCgFnPc1	dva
duhy	duha	k1gFnPc1	duha
v	v	k7c6	v
páru	pár	k1gInSc6	pár
jako	jako	k8xS	jako
krokve	krokev	k1gFnSc2	krokev
na	na	k7c6	na
zenitu	zenit	k1gInSc6	zenit
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
svět	svět	k1gInSc1	svět
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
pata	pata	k1gFnSc1	pata
duhy	duha	k1gFnSc2	duha
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
hlav	hlava	k1gFnPc2	hlava
a	a	k8xC	a
nohou	noha	k1gFnPc2	noha
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínském	čínský	k2eAgNnSc6d1	čínské
bájesloví	bájesloví	k1gNnSc6	bájesloví
jsou	být	k5eAaImIp3nP	být
Hsienpo	Hsienpa	k1gFnSc5	Hsienpa
a	a	k8xC	a
Yingt	Yingtum	k1gNnPc2	Yingtum
<g/>
'	'	kIx"	'
<g/>
ai	ai	k?	ai
milenci	milenec	k1gMnPc1	milenec
zkřížení	zkřížení	k1gNnSc2	zkřížení
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
musí	muset	k5eAaImIp3nP	muset
čekat	čekat	k5eAaImF	čekat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
dokud	dokud	k6eAd1	dokud
se	se	k3xPyFc4	se
neobjeví	objevit	k5eNaPmIp3nS	objevit
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Hsienpo	Hsienpa	k1gFnSc5	Hsienpa
je	on	k3xPp3gInPc4	on
v	v	k7c6	v
duze	duha	k1gFnSc6	duha
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
červeně	červeně	k6eAd1	červeně
a	a	k8xC	a
Yingt	Yingt	k1gInSc1	Yingt
<g/>
'	'	kIx"	'
<g/>
ai	ai	k?	ai
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Sumu	suma	k1gFnSc4	suma
v	v	k7c6	v
Hondurasu	Honduras	k1gInSc6	Honduras
a	a	k8xC	a
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
duhu	duha	k1gFnSc4	duha
jako	jako	k8xS	jako
na	na	k7c4	na
walasa	walas	k1gMnSc4	walas
aniwe	aniw	k1gMnSc4	aniw
–	–	k?	–
ďábel	ďábel	k1gMnSc1	ďábel
je	být	k5eAaImIp3nS	být
rozmrzelý	rozmrzelý	k2eAgMnSc1d1	rozmrzelý
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
schovávají	schovávat	k5eAaImIp3nP	schovávat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
duhu	duha	k1gFnSc4	duha
neviděly	vidět	k5eNaImAgFnP	vidět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
neukazovaly	ukazovat	k5eNaImAgFnP	ukazovat
<g/>
.	.	kIx.	.
</s>
<s>
Fang	Fang	k1gInSc1	Fang
v	v	k7c6	v
africkém	africký	k2eAgInSc6d1	africký
Gabonu	Gabon	k1gInSc6	Gabon
vnímají	vnímat	k5eAaImIp3nP	vnímat
duhu	duha	k1gFnSc4	duha
v	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
jako	jako	k9	jako
"	"	kIx"	"
<g/>
transcendentální	transcendentální	k2eAgInSc1d1	transcendentální
zážitek	zážitek	k1gInSc1	zážitek
<g/>
,	,	kIx,	,
když	když	k8xS	když
dorazí	dorazit	k5eAaPmIp3nP	dorazit
doprostřed	doprostřed	k7c2	doprostřed
duhy	duha	k1gFnSc2	duha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
kruh	kruh	k1gInSc4	kruh
duhy	duha	k1gFnSc2	duha
a	a	k8xC	a
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
úspěch	úspěch	k1gInSc4	úspěch
jejich	jejich	k3xOp3gFnSc2	jejich
vize	vize	k1gFnSc2	vize
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fang	Fang	k1gInSc1	Fang
také	také	k9	také
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
dětem	dítě	k1gFnPc3	dítě
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
duhu	duha	k1gFnSc4	duha
<g/>
.	.	kIx.	.
</s>
<s>
Buddhisté	buddhista	k1gMnPc1	buddhista
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
možné	možný	k2eAgNnSc1d1	možné
dosažitelné	dosažitelný	k2eAgNnSc1d1	dosažitelné
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
Nirvány	nirvána	k1gFnSc2	nirvána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zanikají	zanikat	k5eAaImIp3nP	zanikat
individuální	individuální	k2eAgFnSc2d1	individuální
touhy	touha	k1gFnSc2	touha
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Karens	Karens	k1gInSc1	Karens
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Barma	Barma	k1gFnSc1	Barma
<g/>
)	)	kIx)	)
považují	považovat	k5eAaImIp3nP	považovat
duhu	duha	k1gFnSc4	duha
za	za	k7c4	za
malovaného	malovaný	k2eAgMnSc4d1	malovaný
a	a	k8xC	a
nebezpečného	bezpečný	k2eNgMnSc4d1	nebezpečný
démona	démon	k1gMnSc4	démon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
Izanami	Izana	k1gFnPc7	Izana
a	a	k8xC	a
Izanagi	Izanag	k1gInPc7	Izanag
jsou	být	k5eAaImIp3nP	být
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
–	–	k?	–
tvůrci	tvůrce	k1gMnPc1	tvůrce
světa	svět	k1gInSc2	svět
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
spustili	spustit	k5eAaPmAgMnP	spustit
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
plovoucí	plovoucí	k2eAgInSc4d1	plovoucí
most	most	k1gInSc4	most
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
chaosu	chaos	k1gInSc2	chaos
půdu	půda	k1gFnSc4	půda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
textech	text	k1gInPc6	text
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
most	most	k1gInSc1	most
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
starodávného	starodávný	k2eAgNnSc2d1	starodávné
Japonska	Japonsko	k1gNnSc2	Japonsko
byla	být	k5eAaImAgFnS	být
duha	duha	k1gFnSc1	duha
často	často	k6eAd1	často
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k9	jako
předzvěst	předzvěst	k1gFnSc1	předzvěst
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
představuje	představovat	k5eAaImIp3nS	představovat
hada	had	k1gMnSc4	had
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Gabonu	Gabon	k1gInSc6	Gabon
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předkové	předek	k1gMnPc1	předek
člověka	člověk	k1gMnSc4	člověk
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
po	po	k7c6	po
duze	duha	k1gFnSc6	duha
<g/>
.	.	kIx.	.
</s>
<s>
Šamani	šaman	k1gMnPc1	šaman
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
Burjatů	Burjat	k1gMnPc2	Burjat
mluví	mluvit	k5eAaImIp3nP	mluvit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
duhy	duha	k1gFnSc2	duha
o	o	k7c4	o
stoupání	stoupání	k1gNnSc4	stoupání
k	k	k7c3	k
nebeskému	nebeský	k2eAgInSc3d1	nebeský
duchovnímu	duchovní	k2eAgInSc3d1	duchovní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bulharské	bulharský	k2eAgFnSc6d1	bulharská
legendě	legenda	k1gFnSc6	legenda
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
člověk	člověk	k1gMnSc1	člověk
přijde	přijít	k5eAaPmIp3nS	přijít
pod	pod	k7c4	pod
duhu	duha	k1gFnSc4	duha
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
začít	začít	k5eAaPmF	začít
myslet	myslet	k5eAaImF	myslet
jako	jako	k9	jako
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
začít	začít	k5eAaPmF	začít
myslet	myslet	k5eAaImF	myslet
jako	jako	k9	jako
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Bulhaři	Bulhar	k1gMnPc1	Bulhar
také	také	k9	také
říkají	říkat	k5eAaImIp3nP	říkat
"	"	kIx"	"
<g/>
svítí	svítit	k5eAaImIp3nS	svítit
<g/>
-li	i	k?	-li
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
prší	pršet	k5eAaImIp3nS	pršet
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
se	se	k3xPyFc4	se
žení	ženit	k5eAaImIp3nS	ženit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hrubě	hrubě	k6eAd1	hrubě
přeložená	přeložený	k2eAgFnSc1d1	přeložená
původní	původní	k2eAgFnSc1d1	původní
poezie	poezie	k1gFnSc1	poezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Irská	irský	k2eAgFnSc1d1	irská
legenda	legenda	k1gFnSc1	legenda
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
duhy	duha	k1gFnSc2	duha
nachází	nacházet	k5eAaImIp3nS	nacházet
"	"	kIx"	"
<g/>
hrnec	hrnec	k1gInSc1	hrnec
zlata	zlato	k1gNnSc2	zlato
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
šťastného	šťastný	k2eAgMnSc4d1	šťastný
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gNnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poklad	poklad	k1gInSc1	poklad
však	však	k9	však
střeží	střežit	k5eAaImIp3nS	střežit
leprikón	leprikón	k1gInSc1	leprikón
<g/>
.	.	kIx.	.
</s>
<s>
Estonský	estonský	k2eAgMnSc1d1	estonský
Duhový	duhový	k2eAgMnSc1d1	duhový
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ngalyod	Ngalyod	k1gInSc1	Ngalyod
<g/>
,	,	kIx,	,
nasává	nasávat	k5eAaImIp3nS	nasávat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
plive	plive	k?	plive
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hadím	hadí	k2eAgNnSc6d1	hadí
těle	tělo	k1gNnSc6	tělo
má	mít	k5eAaImIp3nS	mít
hlavu	hlava	k1gFnSc4	hlava
vola	vůl	k1gMnSc2	vůl
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
občas	občas	k6eAd1	občas
nacházeli	nacházet	k5eAaImAgMnP	nacházet
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
keltské	keltský	k2eAgNnSc1d1	keltské
platidlo	platidlo	k1gNnSc1	platidlo
statéry	statér	k1gInPc7	statér
po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
dešti	dešť	k1gInSc6	dešť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mince	mince	k1gFnSc1	mince
omyl	omyl	k1gInSc1	omyl
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
třpytily	třpytit	k5eAaImAgFnP	třpytit
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
mince	mince	k1gFnPc1	mince
padají	padat	k5eAaImIp3nP	padat
z	z	k7c2	z
duhy	duha	k1gFnSc2	duha
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
duha	duha	k1gFnSc1	duha
dotkla	dotknout	k5eAaPmAgFnS	dotknout
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
duhovky	duhovka	k1gFnPc4	duhovka
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přinášejí	přinášet	k5eAaImIp3nP	přinášet
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
sbíraná	sbíraná	k1gFnSc1	sbíraná
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
duha	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
prý	prý	k9	prý
léčí	léčit	k5eAaImIp3nS	léčit
všechny	všechen	k3xTgFnPc4	všechen
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
skřítek	skřítek	k1gMnSc1	skřítek
leprikón	leprikón	k1gMnSc1	leprikón
zakope	zakopat	k5eAaPmIp3nS	zakopat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
duhy	duha	k1gFnSc2	duha
hrnec	hrnec	k1gInSc4	hrnec
zlaťáků	zlaťák	k1gInPc2	zlaťák
<g/>
.	.	kIx.	.
</s>
<s>
Málokomu	málokdo	k3yInSc3	málokdo
se	se	k3xPyFc4	se
však	však	k9	však
podaří	podařit	k5eAaPmIp3nS	podařit
leprikónovu	leprikónův	k2eAgFnSc4d1	leprikónův
skrýš	skrýš	k1gFnSc4	skrýš
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
konce	konec	k1gInSc2	konec
duhy	duha	k1gFnSc2	duha
nelze	lze	k6eNd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Duhová	duhový	k2eAgFnSc1d1	Duhová
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
také	také	k9	také
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
jednoty	jednota	k1gFnSc2	jednota
v	v	k7c6	v
pestrosti	pestrost	k1gFnSc6	pestrost
a	a	k8xC	a
rozličnosti	rozličnost	k1gFnSc6	rozličnost
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
integrace	integrace	k1gFnSc2	integrace
odlišných	odlišný	k2eAgMnPc2d1	odlišný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
příslušníků	příslušník	k1gMnPc2	příslušník
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
jako	jako	k8xS	jako
symbol	symbol	k1gInSc4	symbol
LGBT	LGBT	kA	LGBT
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
integrace	integrace	k1gFnSc2	integrace
mentálně	mentálně	k6eAd1	mentálně
handicapovaných	handicapovaný	k2eAgFnPc2d1	handicapovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Pestrost	pestrost	k1gFnSc1	pestrost
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
života	život	k1gInSc2	život
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
duha	duha	k1gFnSc1	duha
též	též	k9	též
v	v	k7c6	v
symbolice	symbolika	k1gFnSc6	symbolika
ekologických	ekologický	k2eAgNnPc2d1	ekologické
hnutí	hnutí	k1gNnPc2	hnutí
–	–	k?	–
např.	např.	kA	např.
Hnutí	hnutí	k1gNnSc2	hnutí
Duha	duha	k1gFnSc1	duha
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejvlivnější	vlivný	k2eAgNnPc4d3	nejvlivnější
česká	český	k2eAgNnPc4d1	české
ekologistická	ekologistický	k2eAgNnPc4d1	ekologistické
občanská	občanský	k2eAgNnPc4d1	občanské
sdružení	sdružení	k1gNnPc4	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
zjevná	zjevný	k2eAgFnSc1d1	zjevná
je	být	k5eAaImIp3nS	být
souvislost	souvislost	k1gFnSc1	souvislost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sdružení	sdružení	k1gNnSc2	sdružení
Duha	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
volnočasovými	volnočasův	k2eAgFnPc7d1	volnočasův
aktivitami	aktivita	k1gFnPc7	aktivita
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
to	ten	k3xDgNnSc1	ten
operuje	operovat	k5eAaImIp3nS	operovat
s	s	k7c7	s
pestrostí	pestrost	k1gFnSc7	pestrost
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
integrací	integrace	k1gFnPc2	integrace
všech	všecek	k3xTgFnPc2	všecek
skupin	skupina	k1gFnPc2	skupina
dětí	dítě	k1gFnPc2	dítě
i	i	k8xC	i
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
.	.	kIx.	.
</s>
<s>
BEDNÁŘ	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgInPc1d1	pozoruhodný
jevy	jev	k1gInPc1	jev
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
:	:	kIx,	:
Atmosférická	atmosférický	k2eAgFnSc1d1	atmosférická
optika	optika	k1gFnSc1	optika
<g/>
,	,	kIx,	,
akustika	akustika	k1gFnSc1	akustika
a	a	k8xC	a
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
236	[number]	k4	236
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podkapitola	podkapitola	k1gFnSc1	podkapitola
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
odrazy	odraz	k1gInPc1	odraz
na	na	k7c6	na
vodních	vodní	k2eAgFnPc6d1	vodní
kapkách	kapka	k1gFnPc6	kapka
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc1	vznik
duh	duh	k1gInSc1	duh
<g/>
,	,	kIx,	,
s.	s.	k?	s.
155	[number]	k4	155
<g/>
–	–	k?	–
<g/>
162	[number]	k4	162
<g/>
.	.	kIx.	.
</s>
<s>
Halové	halový	k2eAgInPc4d1	halový
jevy	jev	k1gInPc4	jev
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
Odraz	odraz	k1gInSc4	odraz
světla	světlo	k1gNnSc2	světlo
Disperze	disperze	k1gFnSc2	disperze
světla	světlo	k1gNnSc2	světlo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Duha	duha	k1gFnSc1	duha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Duha	duha	k1gFnSc1	duha
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc1	heslo
duha	duha	k1gFnSc1	duha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
