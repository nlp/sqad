<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
duhu	duha	k1gFnSc4	duha
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
v	v	k7c6	v
letech	let	k1gInPc6	let
1635	[number]	k4	1635
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vznik	vznik	k1gInSc1	vznik
barev	barva	k1gFnPc2	barva
duhy	duha	k1gFnSc2	duha
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
