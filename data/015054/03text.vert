<s>
Černý	černý	k2eAgInSc1d1
pátek	pátek	k1gInSc1
(	(	kIx(
<g/>
nakupování	nakupování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
v	v	k7c6
obchodě	obchod	k1gInSc6
v	v	k7c6
USA	USA	kA
</s>
<s>
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Black	Black	k1gMnSc1
Friday	Fridaa	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neformální	formální	k2eNgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
následuje	následovat	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
po	po	k7c6
svátku	svátek	k1gInSc6
Díkůvzdání	díkůvzdání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
čtvrtý	čtvrtý	k4xOgInSc4
pátek	pátek	k1gInSc4
v	v	k7c6
listopadu	listopad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
marketingovou	marketingový	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
obchodníci	obchodník	k1gMnPc1
snaží	snažit	k5eAaImIp3nP
nalákat	nalákat	k5eAaPmF
zákazníky	zákazník	k1gMnPc4
na	na	k7c4
velmi	velmi	k6eAd1
nízké	nízký	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černý	černý	k2eAgInSc1d1
pátek	pátek	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
tradičně	tradičně	k6eAd1
nejrušnějším	rušný	k2eAgInSc7d3
nakupovacím	nakupovací	k2eAgInSc7d1
dnem	den	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
obchodníků	obchodník	k1gMnPc2
však	však	k9
nezačíná	začínat	k5eNaImIp3nS
se	s	k7c7
slevami	sleva	k1gFnPc7
přesně	přesně	k6eAd1
na	na	k7c4
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nabízí	nabízet	k5eAaImIp3nS
je	on	k3xPp3gFnPc4
už	už	k9
o	o	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
také	také	k9
běžné	běžný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
zákazníci	zákazník	k1gMnPc1
kempují	kempovat	k5eAaImIp3nP
před	před	k7c7
obchody	obchod	k1gInPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
zajistili	zajistit	k5eAaPmAgMnP
to	ten	k3xDgNnSc4
nejlepší	dobrý	k2eAgNnSc4d3
místo	místo	k1gNnSc4
ve	v	k7c6
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Termín	termín	k1gInSc1
černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
pravděpodobně	pravděpodobně	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
kolem	kolem	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
ve	v	k7c6
Filadelfii	Filadelfie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
užíván	užívat	k5eAaImNgInS
pro	pro	k7c4
označení	označení	k1gNnSc4
rušného	rušný	k2eAgInSc2d1
silničního	silniční	k2eAgInSc2d1
provozu	provoz	k1gInSc2
den	den	k1gInSc4
po	po	k7c6
Díkůvzdání	díkůvzdání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
více	hodně	k6eAd2
rozšířil	rozšířit	k5eAaPmAgInS
a	a	k8xC
začal	začít	k5eAaPmAgInS
být	být	k5eAaImF
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
pro	pro	k7c4
označování	označování	k1gNnSc4
dne	den	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zvedají	zvedat	k5eAaImIp3nP
tržby	tržba	k1gFnPc1
obchodníků	obchodník	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
se	se	k3xPyFc4
dostávají	dostávat	k5eAaImIp3nP
z	z	k7c2
„	„	k?
<g/>
červených	červené	k1gNnPc2
čísel	číslo	k1gNnPc2
<g/>
“	“	k?
do	do	k7c2
„	„	k?
<g/>
černých	černé	k1gNnPc2
čísel	číslo	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
zejména	zejména	k9
francouzské	francouzský	k2eAgFnPc1d1
oděvní	oděvní	k2eAgFnPc1d1
značky	značka	k1gFnPc1
propagují	propagovat	k5eAaImIp3nP
environmentalistické	environmentalistický	k2eAgInPc4d1
názory	názor	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
zbrojí	zbrojit	k5eAaImIp3nP
proti	proti	k7c3
černému	černý	k2eAgInSc3d1
pátku	pátek	k1gInSc3
vzhledem	vzhledem	k7c3
ekologickému	ekologický	k2eAgInSc3d1
dopadu	dopad	k1gInSc3
tohoto	tento	k3xDgInSc2
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Černý	černý	k2eAgInSc1d1
pátek	pátek	k1gInSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Černý	černý	k2eAgInSc1d1
pátek	pátek	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
oblíbeným	oblíbený	k2eAgNnSc7d1
marketingovým	marketingový	k2eAgNnSc7d1
lákadlem	lákadlo	k1gNnSc7
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
to	ten	k3xDgNnSc1
se	s	k7c7
slevami	sleva	k1gFnPc7
až	až	k8xS
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
osmdesáti	osmdesát	k4xCc2
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odborníci	odborník	k1gMnPc1
ale	ale	k9
varují	varovat	k5eAaImIp3nP
spotřebitele	spotřebitel	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
nenechali	nechat	k5eNaPmAgMnP
nalákat	nalákat	k5eAaPmF
na	na	k7c4
fiktivní	fiktivní	k2eAgFnPc4d1
slevy	sleva	k1gFnPc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Sleva	sleva	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
počítána	počítat	k5eAaImNgFnS
z	z	k7c2
výrazně	výrazně	k6eAd1
vyšší	vysoký	k2eAgFnSc2d2
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
zboží	zboží	k1gNnSc2
v	v	k7c6
běžné	běžný	k2eAgFnSc6d1
tržní	tržní	k2eAgFnSc6d1
síti	síť	k1gFnSc6
<g/>
,	,	kIx,
<g/>
“	“	k?
uvádí	uvádět	k5eAaImIp3nS
vedoucí	vedoucí	k1gFnSc1
právního	právní	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
největší	veliký	k2eAgFnSc2d3
české	český	k2eAgFnSc2d1
spotřebitelské	spotřebitelský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
dTest	dTest	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
prodejců	prodejce	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
odstartovali	odstartovat	k5eAaPmAgMnP
tuto	tento	k3xDgFnSc4
akci	akce	k1gFnSc4
i	i	k9
v	v	k7c6
Česku	Česko	k1gNnSc6
byl	být	k5eAaImAgInS
internetový	internetový	k2eAgInSc1d1
obchod	obchod	k1gInSc1
s	s	k7c7
elektronikou	elektronika	k1gFnSc7
Alza	Alz	k1gInSc2
<g/>
,	,	kIx,
následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
se	se	k3xPyFc4
přidaly	přidat	k5eAaPmAgFnP
i	i	k9
další	další	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
prodejci	prodejce	k1gMnPc1
lákají	lákat	k5eAaImIp3nP
na	na	k7c4
slevy	sleva	k1gFnPc4
Black	Blacka	k1gFnPc2
Friday	Fridaa	k1gFnPc4
i	i	k8xC
mimo	mimo	k7c4
tento	tento	k3xDgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
také	také	k9
často	často	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
podvodům	podvod	k1gInPc3
se	s	k7c7
slevami	sleva	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Čeští	český	k2eAgMnPc1d1
obchodníci	obchodník	k1gMnPc1
si	se	k3xPyFc3
pojem	pojem	k1gInSc4
Black	Black	k1gInSc4
Friday	Fridaa	k1gFnSc2
velmi	velmi	k6eAd1
upravili	upravit	k5eAaPmAgMnP
a	a	k8xC
nabízejí	nabízet	k5eAaImIp3nP
slevy	sleva	k1gFnPc4
prakticky	prakticky	k6eAd1
kdykoliv	kdykoliv	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
a	a	k8xC
někdy	někdy	k6eAd1
dlouhodobě	dlouhodobě	k6eAd1
a	a	k8xC
s	s	k7c7
původní	původní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
souvislost	souvislost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Násilí	násilí	k1gNnSc1
a	a	k8xC
chaos	chaos	k1gInSc1
</s>
<s>
Fronta	fronta	k1gFnSc1
večer	večer	k6eAd1
před	před	k7c4
Black	Black	k1gInSc4
Friday	Fridaa	k1gFnSc2
u	u	k7c2
obchodu	obchod	k1gInSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
</s>
<s>
V	v	k7c4
tento	tento	k3xDgInSc4
den	den	k1gInSc4
se	se	k3xPyFc4
také	také	k9
někdy	někdy	k6eAd1
objevují	objevovat	k5eAaImIp3nP
zprávy	zpráva	k1gFnPc1
o	o	k7c4
násilí	násilí	k1gNnSc4
mezi	mezi	k7c7
zákazníky	zákazník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
zaznamenáno	zaznamenán	k2eAgNnSc1d1
12	#num#	k4
úmrtí	úmrtí	k1gNnPc2
a	a	k8xC
117	#num#	k4
zranění	zranění	k1gNnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
čekal	čekat	k5eAaImAgInS
dav	dav	k1gInSc1
přibližně	přibližně	k6eAd1
2	#num#	k4
000	#num#	k4
nakupujících	nakupující	k2eAgMnPc2d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Valley	Vallea	k1gFnSc2
Stream	Stream	k1gInSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
na	na	k7c6
otevření	otevření	k1gNnSc6
místního	místní	k2eAgInSc2d1
supermarketu	supermarket	k1gInSc2
Walmart	Walmarta	k1gFnPc2
v	v	k7c6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
blížil	blížit	k5eAaImAgInS
otevírací	otevírací	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
,	,	kIx,
rostla	růst	k5eAaImAgFnS
davová	davový	k2eAgFnSc1d1
úzkost	úzkost	k1gFnSc1
a	a	k8xC
když	když	k8xS
byly	být	k5eAaImAgFnP
dveře	dveře	k1gFnPc1
otevřeny	otevřít	k5eAaPmNgFnP
<g/>
,	,	kIx,
dav	dav	k1gInSc1
se	se	k3xPyFc4
tlačil	tlačit	k5eAaImAgInS
kupředu	kupředu	k6eAd1
<g/>
,	,	kIx,
rozbil	rozbít	k5eAaPmAgMnS
dveře	dveře	k1gFnPc4
a	a	k8xC
34	#num#	k4
<g/>
letý	letý	k2eAgMnSc1d1
zaměstnanec	zaměstnanec	k1gMnSc1
byl	být	k5eAaImAgMnS
pošlapán	pošlapán	k2eAgMnSc1d1
k	k	k7c3
smrti	smrt	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezdálo	zdát	k5eNaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
zákazníci	zákazník	k1gMnPc1
zajímali	zajímat	k5eAaImAgMnP
o	o	k7c4
osud	osud	k1gInSc4
oběti	oběť	k1gFnPc4
a	a	k8xC
odmítali	odmítat	k5eAaImAgMnP
zastavit	zastavit	k5eAaPmF
tlačenici	tlačenice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
ostatní	ostatní	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
pokusili	pokusit	k5eAaPmAgMnP
zasáhnout	zasáhnout	k5eAaPmF
a	a	k8xC
pomoci	pomoct	k5eAaPmF
zraněnému	zraněný	k2eAgMnSc3d1
zaměstnanci	zaměstnanec	k1gMnSc3
<g/>
,	,	kIx,
stěžovali	stěžovat	k5eAaImAgMnP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
čekali	čekat	k5eAaImAgMnP
v	v	k7c6
chladu	chlad	k1gInSc6
a	a	k8xC
nebyli	být	k5eNaImAgMnP
ochotni	ochoten	k2eAgMnPc1d1
déle	dlouho	k6eAd2
čekat	čekat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
přijela	přijet	k5eAaPmAgFnS
policie	policie	k1gFnSc1
a	a	k8xC
pokusila	pokusit	k5eAaPmAgFnS
se	se	k3xPyFc4
poskytnout	poskytnout	k5eAaPmF
pomoc	pomoc	k1gFnSc4
zraněnému	zraněný	k1gMnSc3
muži	muž	k1gMnSc3
<g/>
,	,	kIx,
nakupující	nakupující	k2eAgMnPc1d1
strkali	strkat	k5eAaImAgMnP
a	a	k8xC
tlačili	tlačit	k5eAaImAgMnP
do	do	k7c2
důstojníků	důstojník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
utrpělo	utrpět	k5eAaPmAgNnS
drobná	drobný	k2eAgNnPc4d1
zranění	zranění	k1gNnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
těhotné	těhotný	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
odvezena	odvézt	k5eAaPmNgFnS
do	do	k7c2
nemocnice	nemocnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Incident	incident	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
prvním	první	k4xOgInSc7
případem	případ	k1gInSc7
úmrtí	úmrtí	k1gNnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
došlo	dojít	k5eAaPmAgNnS
během	během	k7c2
černého	černý	k2eAgInSc2d1
pátku	pátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
byli	být	k5eAaImAgMnP
smrtelně	smrtelně	k6eAd1
zastřeleni	zastřelit	k5eAaPmNgMnP
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
během	během	k7c2
hádky	hádka	k1gFnSc2
v	v	k7c6
obchodě	obchod	k1gInSc6
Toys	Toysa	k1gFnPc2
'	'	kIx"
<g/>
R	R	kA
<g/>
'	'	kIx"
Us	Us	k1gMnSc1
v	v	k7c4
Palm	Palm	k1gInSc4
Desert	desert	k1gInSc4
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byla	být	k5eAaImAgFnS
zatčena	zatčen	k2eAgFnSc1d1
žena	žena	k1gFnSc1
z	z	k7c2
Indianapolisu	Indianapolis	k1gInSc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
hádat	hádat	k5eAaImF
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
zákazníky	zákazník	k1gMnPc7
Walmartu	Walmart	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
požádána	požádat	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
opustila	opustit	k5eAaPmAgFnS
obchod	obchod	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byl	být	k5eAaImAgInS
v	v	k7c6
Buffalu	Buffal	k1gInSc6
pošlapán	pošlapán	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
se	se	k3xPyFc4
po	po	k7c6
otevření	otevření	k1gNnSc6
dveří	dveře	k1gFnPc2
obchodu	obchod	k1gInSc2
Target	Target	k1gMnSc1
sesypal	sesypat	k5eAaPmAgMnS
dav	dav	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
černém	černý	k2eAgInSc6d1
pátku	pátek	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
žena	žena	k1gFnSc1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
způsobila	způsobit	k5eAaPmAgFnS
zranění	zranění	k1gNnSc6
pepřovým	pepřový	k2eAgInSc7d1
sprejem	sprej	k1gInSc7
dvaceti	dvacet	k4xCc2
ostatním	ostatní	k2eAgMnPc3d1
zákazníkům	zákazník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
čekali	čekat	k5eAaImAgMnP
ve	v	k7c6
frontě	fronta	k1gFnSc6
na	na	k7c4
nově	nově	k6eAd1
zlevněnou	zlevněný	k2eAgFnSc4d1
herní	herní	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dalším	další	k2eAgInSc6d1
incidentu	incident	k1gInSc6
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
také	také	k9
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
byl	být	k5eAaImAgInS
při	při	k7c6
nakupování	nakupování	k1gNnSc6
zastřelen	zastřelen	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byl	být	k5eAaImAgInS
zastřelen	zastřelen	k2eAgInSc4d1
21	#num#	k4
<g/>
letý	letý	k2eAgInSc4d1
Demond	Demond	k1gInSc4
Cottman	Cottman	k1gMnSc1
kolem	kolem	k7c2
jedné	jeden	k4xCgFnSc2
hodiny	hodina	k1gFnSc2
ranní	ranní	k1gFnSc2
v	v	k7c4
pátek	pátek	k1gInSc4
před	před	k7c7
obchodem	obchod	k1gInSc7
Macy	Maca	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
v	v	k7c6
New	New	k1gMnPc6
Jersey	Jersea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střelec	Střelec	k1gMnSc1
vystřelil	vystřelit	k5eAaPmAgMnS
několik	několik	k4yIc4
výstřelů	výstřel	k1gInPc2
a	a	k8xC
zanechal	zanechat	k5eAaPmAgMnS
za	za	k7c7
sebou	se	k3xPyFc7
SUV	SUV	kA
pokryté	pokrytý	k2eAgFnPc4d1
dírami	díra	k1gFnPc7
po	po	k7c6
kulkách	kulka	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
motivy	motiv	k1gInPc1
zůstaly	zůstat	k5eAaPmAgInP
nejasné	jasný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cottmanův	Cottmanův	k2eAgInSc4d1
26	#num#	k4
<g/>
letý	letý	k2eAgInSc4d1
bratr	bratr	k1gMnSc1
byl	být	k5eAaImAgInS
také	také	k9
zraněn	zranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Dalším	další	k2eAgInSc7d1
případem	případ	k1gInSc7
je	být	k5eAaImIp3nS
střelba	střelba	k1gFnSc1
na	na	k7c6
Wolfchase	Wolfchasa	k1gFnSc6
Galleria	Gallerium	k1gNnSc2
Mall	Malla	k1gFnPc2
v	v	k7c6
Memphisu	Memphis	k1gInSc6
v	v	k7c6
Tennessee	Tennessee	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
způsobila	způsobit	k5eAaPmAgFnS
zranění	zranění	k1gNnSc4
jednoho	jeden	k4xCgMnSc2
muže	muž	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devatenáctiletý	devatenáctiletý	k2eAgInSc4d1
mladík	mladík	k1gInSc4
Derrick	Derrick	k1gMnSc1
Blackburn	Blackburn	k1gMnSc1
byl	být	k5eAaImAgMnS
později	pozdě	k6eAd2
zatčen	zatknout	k5eAaPmNgMnS
za	za	k7c4
nezákonné	zákonný	k2eNgInPc4d1
držení	držení	k1gNnSc2
zbraně	zbraň	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
návštěvnost	návštěvnost	k1gFnSc1
obchodních	obchodní	k2eAgInPc2d1
webů	web	k1gInPc2
</s>
<s>
Některé	některý	k3yIgInPc1
e-shopy	e-shop	k1gInPc1
investují	investovat	k5eAaBmIp3nP
mnoho	mnoho	k4c4
peněz	peníze	k1gInPc2
do	do	k7c2
propagačních	propagační	k2eAgFnPc2d1
kampaní	kampaň	k1gFnPc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
populární	populární	k2eAgNnSc1d1
využití	využití	k1gNnSc1
Instagramu	Instagram	k1gInSc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
sociálních	sociální	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
počtem	počet	k1gInSc7
sledujících	sledující	k2eAgMnPc2d1
a	a	k8xC
velkým	velký	k2eAgInSc7d1
mediálním	mediální	k2eAgInSc7d1
dosahem	dosah	k1gInSc7
za	za	k7c4
peníze	peníz	k1gInPc4
a	a	k8xC
produkty	produkt	k1gInPc1
poskytnuté	poskytnutý	k2eAgInPc1d1
zdarma	zdarma	k6eAd1
recenzují	recenzovat	k5eAaImIp3nP
<g/>
,	,	kIx,
vychvalují	vychvalovat	k5eAaImIp3nP
a	a	k8xC
propagují	propagovat	k5eAaImIp3nP
téměř	téměř	k6eAd1
vše	všechen	k3xTgNnSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nalákali	nalákat	k5eAaPmAgMnP
co	co	k9
největší	veliký	k2eAgInSc4d3
možný	možný	k2eAgInSc4d1
počet	počet	k1gInSc4
zákazníků	zákazník	k1gMnPc2
na	na	k7c4
všelijaké	všelijaký	k3yIgFnPc4
slevy	sleva	k1gFnPc4
do	do	k7c2
svých	svůj	k3xOyFgInPc2
obchodů	obchod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
často	často	k6eAd1
zapomínají	zapomínat	k5eAaImIp3nP
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
taková	takový	k3xDgFnSc1
návštěvnost	návštěvnost	k1gFnSc1
může	moct	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
na	na	k7c6
jejich	jejich	k3xOp3gFnPc6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
silný	silný	k2eAgInSc4d1
nápor	nápor	k1gInSc4
a	a	k8xC
výpadek	výpadek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Retail	Retaila	k1gFnPc2
Gazette	Gazett	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
počet	počet	k1gInSc1
obchodních	obchodní	k2eAgFnPc2d1
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
snížil	snížit	k5eAaPmAgInS
v	v	k7c6
důsledku	důsledek	k1gInSc6
velkého	velký	k2eAgInSc2d1
náporu	nápor	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Černého	Černého	k2eAgInSc2d1
pátku	pátek	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc1
jen	jen	k9
podporuje	podporovat	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
spousta	spousta	k1gFnSc1
obchodníků	obchodník	k1gMnPc2
se	se	k3xPyFc4
na	na	k7c4
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
pořádně	pořádně	k6eAd1
nepřipraví	připravit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
selhání	selhání	k1gNnPc4
mohou	moct	k5eAaImIp3nP
způsobit	způsobit	k5eAaPmF
slabší	slabý	k2eAgFnPc4d2
preference	preference	k1gFnPc4
stránek	stránka	k1gFnPc2
<g/>
,	,	kIx,
již	již	k6eAd1
zmíněný	zmíněný	k2eAgInSc4d1
výpadek	výpadek	k1gInSc4
stránky	stránka	k1gFnPc4
a	a	k8xC
ztrátu	ztráta	k1gFnSc4
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
The	The	k1gMnSc1
2017	#num#	k4
Veeam	Veeam	k1gInSc1
Availability	Availabilita	k1gFnSc2
Report	report	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
neplánovaný	plánovaný	k2eNgInSc1d1
výpadek	výpadek	k1gInSc1
stránky	stránka	k1gFnSc2
stojí	stát	k5eAaImIp3nS
organizátory	organizátor	k1gMnPc4
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
v	v	k7c6
průměru	průměr	k1gInSc6
270	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Doporučovací	Doporučovací	k2eAgInPc1d1
weby	web	k1gInPc1
</s>
<s>
Některé	některý	k3yIgFnPc1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
informují	informovat	k5eAaBmIp3nP
o	o	k7c6
tomto	tento	k3xDgInSc6
dnu	den	k1gInSc6
po	po	k7c4
Díkůvzdání	díkůvzdání	k1gNnSc4
i	i	k8xC
měsíc	měsíc	k1gInSc4
předem	předem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznamy	seznam	k1gInPc1
slev	sleva	k1gFnPc2
a	a	k8xC
původních	původní	k2eAgFnPc2d1
cen	cena	k1gFnPc2
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
doplňovány	doplňován	k2eAgInPc1d1
i	i	k9
o	o	k7c4
různé	různý	k2eAgInPc4d1
obrázky	obrázek	k1gInPc4
aktuálních	aktuální	k2eAgFnPc2d1
reklam	reklama	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
mohou	moct	k5eAaImIp3nP
dělat	dělat	k5eAaImF
buď	buď	k8xC
lidé	člověk	k1gMnPc1
interně	interně	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
velcí	velký	k2eAgMnPc1d1
obchodníci	obchodník	k1gMnPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dali	dát	k5eAaPmAgMnP
zákazníkům	zákazník	k1gMnPc3
čas	čas	k1gInSc4
nákupy	nákup	k1gInPc7
naplánovat	naplánovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
někteří	některý	k3yIgMnPc1
velcí	velký	k2eAgMnPc1d1
obchodníci	obchodník	k1gMnPc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
Walmart	Walmart	k1gInSc1
<g/>
,	,	kIx,
Target	Target	k1gInSc1
<g/>
,	,	kIx,
OfficeMax	OfficeMax	k1gInSc1
<g/>
,	,	kIx,
Big	Big	k1gFnSc1
Lots	Lotsa	k1gFnPc2
a	a	k8xC
Staples	Staplesa	k1gFnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
nechali	nechat	k5eAaPmAgMnP
slyšet	slyšet	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
reklamy	reklama	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vypouští	vypouštět	k5eAaImIp3nP
ještě	ještě	k9
před	před	k7c7
samotným	samotný	k2eAgInSc7d1
Černým	černý	k2eAgInSc7d1
pátkem	pátek	k1gInSc7
a	a	k8xC
ceny	cena	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
obsaženy	obsáhnout	k5eAaPmNgInP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vázány	vázat	k5eAaImNgFnP
autorským	autorský	k2eAgNnSc7d1
právem	právo	k1gNnSc7
a	a	k8xC
obchodním	obchodní	k2eAgNnSc7d1
tajemstvím	tajemství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Někteří	některý	k3yIgMnPc1
z	z	k7c2
těchto	tento	k3xDgMnPc2
obchodníků	obchodník	k1gMnPc2
využili	využít	k5eAaPmAgMnP
tzv.	tzv.	kA
take-down	take-down	k1gInSc4
system	syst	k1gInSc7
z	z	k7c2
Digital	Digital	kA
Millennium	millennium	k1gNnSc4
Copyright	copyright	k1gNnSc2
act	act	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
odstranili	odstranit	k5eAaPmAgMnP
pohoršující	pohoršující	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
cen	cena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
strategie	strategie	k1gFnPc4
může	moct	k5eAaImIp3nS
vycházet	vycházet	k5eAaImF
ze	z	k7c2
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
další	další	k2eAgFnSc1d1
“	“	k?
<g/>
soutěžící	soutěžící	k1gFnSc1
<g/>
”	”	k?
sníží	snížit	k5eAaPmIp3nS
své	svůj	k3xOyFgFnPc4
ceny	cena	k1gFnPc4
a	a	k8xC
zákazníci	zákazník	k1gMnPc1
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
další	další	k2eAgInSc4d1
obchod	obchod	k1gInSc4
k	k	k7c3
porovnání	porovnání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuální	aktuální	k2eAgFnSc1d1
validita	validita	k1gFnSc1
tvrzení	tvrzení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
ceny	cena	k1gFnPc1
jsou	být	k5eAaImIp3nP
chráněná	chráněný	k2eAgFnSc1d1
autorská	autorský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nejistá	jistý	k2eNgFnSc1d1
jako	jako	k8xC,k8xS
ceny	cena	k1gFnPc1
samy	sám	k3xTgFnPc1
<g/>
.	.	kIx.
</s>
<s>
Využívání	využívání	k1gNnSc1
doporučovacích	doporučovací	k2eAgInPc2d1
webů	web	k1gInPc2
a	a	k8xC
nakupování	nakupování	k1gNnSc1
v	v	k7c6
kamenných	kamenný	k2eAgFnPc6d1
prodejnách	prodejna	k1gFnPc6
se	se	k3xPyFc4
stát	stát	k1gInSc1
od	od	k7c2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záleží	záležet	k5eAaImIp3nS
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
na	na	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
poštovného	poštovné	k1gNnSc2
a	a	k8xC
jestli	jestli	k8xS
popřípadě	popřípadě	k6eAd1
jakou	jaký	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
má	mít	k5eAaImIp3nS
prodejní	prodejní	k2eAgFnSc4d1
daň	daň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
tento	tento	k3xDgInSc1
komfort	komfort	k1gInSc4
online	onlinout	k5eAaPmIp3nS
nakupování	nakupování	k1gNnSc1
zvýšil	zvýšit	k5eAaPmAgInS
počet	počet	k1gInSc4
přeshraničních	přeshraniční	k2eAgMnPc2d1
zákazníků	zákazník	k1gMnPc2
hledajících	hledající	k2eAgFnPc2d1
co	co	k9
nejvýhodnější	výhodný	k2eAgInSc4d3
nákup	nákup	k1gInSc4
mimo	mimo	k7c4
USA	USA	kA
<g/>
,	,	kIx,
hlavně	hlavně	k9
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistiky	statistik	k1gMnPc7
ukazuji	ukazovat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
online	onlinout	k5eAaPmIp3nS
přeshraniční	přeshraniční	k2eAgInPc4d1
nákupy	nákup	k1gInPc4
se	se	k3xPyFc4
u	u	k7c2
Kanaďanů	Kanaďan	k1gMnPc2
zvýšily	zvýšit	k5eAaPmAgFnP
zhruba	zhruba	k6eAd1
o	o	k7c4
300	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
ročně	ročně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kyber-pondělí	Kyber-pondělit	k5eAaPmIp3nS
(	(	kIx(
<g/>
Cyber	Cyber	k1gInSc1
Monday	Mondaa	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Termín	termín	k1gInSc1
kyber	kybra	k1gFnPc2
pondělí	pondělí	k1gNnSc2
<g/>
,	,	kIx,
neologismus	neologismus	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
vymyslela	vymyslet	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
marketingový	marketingový	k2eAgInSc4d1
pojem	pojem	k1gInSc4
označující	označující	k2eAgNnSc4d1
první	první	k4xOgNnSc4
pondělí	pondělí	k1gNnSc4
po	po	k7c6
Černém	černé	k1gNnSc6
pátku	pátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
trendu	trend	k1gInSc6
přicházejícím	přicházející	k2eAgInSc6d1
z	z	k7c2
let	léto	k1gNnPc2
předcházejících	předcházející	k2eAgInPc2d1
(	(	kIx(
<g/>
2003	#num#	k4
a	a	k8xC
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodníci	obchodník	k1gMnPc1
zaznamenali	zaznamenat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
zákazníků	zákazník	k1gMnPc2
nebylo	být	k5eNaImAgNnS
schopno	schopen	k2eAgNnSc1d1
nakupovat	nakupovat	k5eAaBmF
o	o	k7c6
víkendu	víkend	k1gInSc6
v	v	k7c6
průběhu	průběh	k1gInSc6
oslav	oslava	k1gFnPc2
Díkůvzdání	díkůvzdání	k1gNnSc2
a	a	k8xC
Černého	černé	k1gNnSc2
pátku	pátek	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
byli	být	k5eAaImAgMnP
příliš	příliš	k6eAd1
zaneprázdnění	zaneprázdněný	k2eAgMnPc1d1
nebo	nebo	k8xC
zkrátka	zkrátka	k6eAd1
nenašli	najít	k5eNaPmAgMnP
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
hledali	hledat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
nakupovali	nakupovat	k5eAaBmAgMnP
online	onlinout	k5eAaPmIp3nS
z	z	k7c2
domova	domov	k1gInSc2
až	až	k9
ve	v	k7c6
slevách	sleva	k1gFnPc6
v	v	k7c6
pondělí	pondělí	k1gNnSc6
po	po	k7c6
víkendu	víkend	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
se	se	k3xPyFc4
online	onlinout	k5eAaPmIp3nS
slevy	sleva	k1gFnPc1
a	a	k8xC
výprodeje	výprodej	k1gInPc1
v	v	k7c6
rámci	rámec	k1gInSc6
kyber-pondělí	kyber-pondělit	k5eAaPmIp3nS
zvýšily	zvýšit	k5eAaPmAgFnP
o	o	k7c4
18	#num#	k4
%	%	kIx~
oproti	oproti	k7c3
roku	rok	k1gInSc3
2012	#num#	k4
<g/>
,	,	kIx,
dosáhly	dosáhnout	k5eAaPmAgFnP
rekordních	rekordní	k2eAgFnPc2d1
1,73	1,73	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměr	průměr	k1gInSc1
jedné	jeden	k4xCgFnSc2
objednávky	objednávka	k1gFnSc2
činil	činit	k5eAaImAgInS
128	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
kyber-pondělí	kyber-pondělit	k5eAaPmIp3nS
stalo	stát	k5eAaPmAgNnS
nejrušnějším	rušný	k2eAgInSc7d3
dnem	den	k1gInSc7
roku	rok	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
slevy	sleva	k1gFnPc1
překonávaly	překonávat	k5eAaImAgFnP
2	#num#	k4
miliardy	miliarda	k4xCgFnPc4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
o	o	k7c4
17	#num#	k4
%	%	kIx~
více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kyber-týden	Kyber-týdna	k1gFnPc2
</s>
<s>
V	v	k7c6
magazínu	magazín	k1gInSc6
Forbes	forbes	k1gInSc1
se	s	k7c7
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
objevil	objevit	k5eAaPmAgInS
článek	článek	k1gInSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
kyber-pondělí	kyber-pondělý	k2eAgMnPc1d1
se	se	k3xPyFc4
těší	těšit	k5eAaImIp3nP
obrovské	obrovský	k2eAgFnSc3d1
popularitě	popularita	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
budou	být	k5eAaImBp3nP
kyber-slevy	kyber-slev	k1gInPc1
pokračovat	pokračovat	k5eAaImF
celý	celý	k2eAgInSc4d1
týden	týden	k1gInSc4
po	po	k7c6
Díkůvzdání	díkůvzdání	k1gNnSc6
a	a	k8xC
Černém	černé	k1gNnSc6
pátku	pátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peter	Peter	k1gMnSc1
Greenberg	Greenberg	k1gMnSc1
<g/>
,	,	kIx,
travel	travel	k1gMnSc1
editor	editor	k1gInSc1
z	z	k7c2
CBS	CBS	kA
News	News	k1gInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
:	:	kIx,
“	“	k?
<g/>
Pokud	pokud	k8xS
chcete	chtít	k5eAaImIp2nP
opravdu	opravdu	k6eAd1
obchodovat	obchodovat	k5eAaImF
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
byste	by	kYmCp2nP
se	se	k3xPyFc4
držet	držet	k5eAaImF
dál	daleko	k6eAd2
od	od	k7c2
obchodních	obchodní	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
a	a	k8xC
kyber-pondělí	kyber-pondělý	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
samy	sám	k3xTgFnPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
součástí	součást	k1gFnSc7
kyber-týdne	kyber-týdnout	k5eAaPmIp3nS
<g/>
…	…	k?
<g/>
”	”	k?
</s>
<s>
Dopad	dopad	k1gInSc1
obchodních	obchodní	k2eAgFnPc2d1
slev	sleva	k1gFnPc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Národní	národní	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
zveřejňuje	zveřejňovat	k5eAaImIp3nS
čísla	číslo	k1gNnPc4
pro	pro	k7c4
každý	každý	k3xTgInSc4
víkend	víkend	k1gInSc4
Díkůvzdání	díkůvzdání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federace	federace	k1gFnSc1
definuje	definovat	k5eAaBmIp3nS
víkend	víkend	k1gInSc4
Díkůvzdání	díkůvzdání	k1gNnSc2
jako	jako	k9
čtvrtek	čtvrtek	k1gInSc4
<g/>
,	,	kIx,
pátek	pátek	k1gInSc4
a	a	k8xC
utrácení	utrácení	k1gNnSc1
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
neděli	neděle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průzkum	průzkum	k1gInSc1
odhaduje	odhadovat	k5eAaImIp3nS
počet	počet	k1gInSc1
nakupujících	nakupující	k2eAgFnPc2d1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
počet	počet	k1gInSc1
počet	počet	k1gInSc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
nakupovací	nakupovací	k2eAgFnSc2d1
sezony	sezona	k1gFnSc2
není	být	k5eNaImIp3nS
úplně	úplně	k6eAd1
stejná	stejný	k2eAgFnSc1d1
přes	přes	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnSc2
pro	pro	k7c4
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
jsou	být	k5eAaImIp3nP
od	od	k7c2
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Vánoce	Vánoce	k1gFnPc1
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byla	být	k5eAaImAgFnS
nejdelší	dlouhý	k2eAgFnSc1d3
nákupní	nákupní	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Black	Black	k1gInSc1
Friday	Friday	k1gInPc1
(	(	kIx(
<g/>
shopping	shopping	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
|	|	kIx~
Definition	Definition	k1gInSc1
of	of	k?
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
by	by	kYmCp3nS
Lexico	Lexico	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexico	Lexico	k1gMnSc1
Dictionaries	Dictionaries	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TAYLOR-BLAKE	TAYLOR-BLAKE	k1gFnSc1
<g/>
,	,	kIx,
Bonnie	Bonnie	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1966	#num#	k4
<g/>
]	]	kIx)
"	"	kIx"
<g/>
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
day	day	k?
after	after	k1gInSc1
Thanksgiving	Thanksgiving	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wed	Wed	k1gFnSc1
Apr	Apr	k1gFnSc1
23	#num#	k4
23	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
UTC	UTC	kA
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FOSTER	FOSTER	kA
<g/>
,	,	kIx,
Laura	Laura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
not	nota	k1gFnPc2
'	'	kIx"
<g/>
Green	Green	k1gInSc1
Friday	Fridaa	k1gFnSc2
<g/>
'	'	kIx"
instead	instead	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.bbc.com	www.bbc.com	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Black	Black	k1gInSc1
friday	fridaa	k1gFnSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
jen	jen	k9
iluze	iluze	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
firmy	firma	k1gFnSc2
před	před	k7c7
svátkem	svátek	k1gInSc7
konzumu	konzum	k1gInSc2
navyšují	navyšovat	k5eAaImIp3nP
ceny	cena	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gMnPc2
vypočítávají	vypočítávat	k5eAaImIp3nP
slevy	sleva	k1gFnPc1
|	|	kIx~
Noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-23	2018-11-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DOČEKAL	Dočekal	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Bradbury	Bradbura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kouzla	kouzlo	k1gNnPc4
s	s	k7c7
cenami	cena	k1gFnPc7
na	na	k7c4
Black	Black	k1gInSc4
Friday	Fridaa	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nenechte	nechat	k5eNaPmRp2nP
se	se	k3xPyFc4
napálit	napálit	k5eAaPmF
<g/>
,	,	kIx,
použijte	použít	k5eAaPmRp2nP
Hlídač	hlídač	k1gInSc4
Shopů	shop	k1gInPc2
<g/>
..	..	k?
POOH	POOH	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-11-24	2019-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Velký	velký	k2eAgInSc1d1
test	test	k1gInSc1
Black	Blacka	k1gFnPc2
Friday	Fridaa	k1gFnSc2
u	u	k7c2
e-shopů	e-shop	k1gInPc2
Alza	Alzum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Mall	Mall	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
a	a	k8xC
Datart	Datart	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fixlují	fixlovat	k5eAaImIp3nP
slevy	sleva	k1gFnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikatel	podnikatel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Black	Black	k1gMnSc1
Friday	Fridaa	k1gFnSc2
Death	Death	k1gMnSc1
Count	Count	k1gMnSc1
<g/>
.	.	kIx.
blackfridaydeathcount	blackfridaydeathcount	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Wal-Mart	Wal-Mart	k1gInSc1
worker	worker	k1gMnSc1
dies	diesa	k1gFnPc2
in	in	k?
rush	rusha	k1gFnPc2
<g/>
;	;	kIx,
two	two	k?
killed	killed	k1gInSc1
at	at	k?
toy	toy	k?
store	stor	k1gInSc5
-	-	kIx~
CNN	CNN	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
edition	edition	k1gInSc1
<g/>
.	.	kIx.
<g/>
cnn	cnn	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
shoppers	shoppersa	k1gFnPc2
trampled	trampled	k1gMnSc1
in	in	k?
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
www.cnn.com	www.cnn.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Black	Black	k1gMnSc1
Friday	Fridaa	k1gFnSc2
Shootings	Shootings	k1gInSc1
Kill	Kill	k1gInSc1
2	#num#	k4
and	and	k?
Leave	Leaev	k1gFnSc2
More	mor	k1gInSc5
Injured	Injured	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Černý	černý	k2eAgInSc1d1
pátek	pátek	k1gInSc1
(	(	kIx(
<g/>
Black	Black	k1gInSc1
Friday	Fridaa	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Cyber	Cyber	k1gMnSc1
Monday	Mondaa	k1gFnSc2
<g/>
:	:	kIx,
historie	historie	k1gFnSc2
</s>
