<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
je	být	k5eAaImIp3nS	být
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
delší	dlouhý	k2eAgFnSc7d2	delší
oxidací	oxidace	k1gFnSc7	oxidace
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
nesprávně	správně	k6eNd1	správně
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
fermentaci	fermentace	k1gFnSc6	fermentace
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
než	než	k8xS	než
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
oolong	oolong	k1gInSc1	oolong
(	(	kIx(	(
<g/>
polozelený	polozelený	k2eAgMnSc1d1	polozelený
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
z	z	k7c2	z
lístků	lístek	k1gInPc2	lístek
Camellia	Camellium	k1gNnSc2	Camellium
sinensis	sinensis	k1gFnSc2	sinensis
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
má	mít	k5eAaImIp3nS	mít
silnější	silný	k2eAgFnSc4d2	silnější
vůni	vůně	k1gFnSc4	vůně
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc4d1	ostatní
méně	málo	k6eAd2	málo
oxidované	oxidovaný	k2eAgMnPc4d1	oxidovaný
čaje	čaj	k1gInPc4	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
červený	červený	k2eAgInSc1d1	červený
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
紅	紅	k?	紅
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pchin-jinem	pchinin	k1gInSc7	pchin-jin
hóngchá	hóngchat	k5eAaImIp3nS	hóngchat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
kóča	kóčus	k1gMnSc2	kóčus
紅	紅	k?	紅
<g/>
;	;	kIx,	;
korejsky	korejsky	k6eAd1	korejsky
hongcha	hongch	k1gMnSc2	hongch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
více	hodně	k6eAd2	hodně
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
barvu	barva	k1gFnSc4	barva
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
černý	černý	k2eAgInSc4d1	černý
čaj	čaj	k1gInSc4	čaj
se	se	k3xPyFc4	se
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
spíše	spíše	k9	spíše
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
tradičním	tradiční	k2eAgInSc7d1	tradiční
puerhem	puerh	k1gInSc7	puerh
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
"	"	kIx"	"
<g/>
červeným	červený	k2eAgInSc7d1	červený
čajem	čaj	k1gInSc7	čaj
<g/>
"	"	kIx"	"
rozumí	rozumět	k5eAaImIp3nS	rozumět
spíše	spíše	k9	spíše
jihoafrický	jihoafrický	k2eAgInSc1d1	jihoafrický
rooibos	rooibos	k1gInSc1	rooibos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
většinou	většinou	k6eAd1	většinou
své	svůj	k3xOyFgNnSc4	svůj
aroma	aroma	k1gNnSc4	aroma
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
si	se	k3xPyFc3	se
udrží	udržet	k5eAaPmIp3nS	udržet
svou	svůj	k3xOyFgFnSc4	svůj
vůni	vůně	k1gFnSc4	vůně
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
také	také	k9	také
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
využíván	využívat	k5eAaImNgInS	využívat
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
Tibetu	Tibet	k1gInSc6	Tibet
a	a	k8xC	a
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
byl	být	k5eAaImAgMnS	být
až	až	k9	až
do	do	k7c2	do
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
využíván	využívat	k5eAaPmNgMnS	využívat
jako	jako	k9	jako
měna	měna	k1gFnSc1	měna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
dynastie	dynastie	k1gFnSc2	dynastie
Tchang	Tchang	k1gInSc1	Tchang
byl	být	k5eAaImAgInS	být
černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
namočený	namočený	k2eAgInSc1d1	namočený
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
používán	používat	k5eAaImNgMnS	používat
jako	jako	k8xC	jako
barvivo	barvivo	k1gNnSc4	barvivo
na	na	k7c4	na
látku	látka	k1gFnSc4	látka
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnSc4d2	nižší
třídu	třída	k1gFnSc4	třída
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
nemohla	moct	k5eNaImAgFnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
kvalitnější	kvalitní	k2eAgFnPc4d2	kvalitnější
barvy	barva	k1gFnPc4	barva
na	na	k7c6	na
oblečení	oblečení	k1gNnSc6	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
čaj	čaj	k1gInSc1	čaj
známý	známý	k2eAgInSc1d1	známý
západnímu	západní	k2eAgInSc3d1	západní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
popularita	popularita	k1gFnSc1	popularita
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
stále	stále	k6eAd1	stále
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devadesát	devadesát	k4xCc4	devadesát
procent	procento	k1gNnPc2	procento
čaje	čaj	k1gInSc2	čaj
prodaného	prodaný	k2eAgInSc2d1	prodaný
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
černého	černý	k2eAgInSc2d1	černý
čaje	čaj	k1gInSc2	čaj
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
se	se	k3xPyFc4	se
listy	list	k1gInPc7	list
nejprve	nejprve	k6eAd1	nejprve
nechají	nechat	k5eAaPmIp3nP	nechat
zavadnout	zavadnout	k5eAaPmF	zavadnout
pomocí	pomocí	k7c2	pomocí
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
černé	černý	k2eAgInPc1d1	černý
čaje	čaj	k1gInPc1	čaj
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
CTC	CTC	kA	CTC
(	(	kIx(	(
<g/>
drcení	drcení	k1gNnSc1	drcení
<g/>
,	,	kIx,	,
řezání	řezání	k1gNnSc1	řezání
<g/>
,	,	kIx,	,
stočení	stočení	k1gNnSc1	stočení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
čajové	čajový	k2eAgInPc4d1	čajový
listy	list	k1gInPc4	list
nižší	nízký	k2eAgFnSc2d2	nižší
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
končí	končit	k5eAaImIp3nP	končit
v	v	k7c6	v
čajových	čajový	k2eAgInPc6d1	čajový
sáčcích	sáček	k1gInPc6	sáček
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
strojově	strojově	k6eAd1	strojově
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
zpracování	zpracování	k1gNnSc1	zpracování
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
jak	jak	k6eAd1	jak
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
čaje	čaj	k1gInPc4	čaj
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
vcelku	vcelku	k6eAd1	vcelku
a	a	k8xC	a
přesný	přesný	k2eAgInSc1d1	přesný
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
nechají	nechat	k5eAaPmIp3nP	nechat
čajové	čajový	k2eAgInPc1d1	čajový
listy	list	k1gInPc1	list
za	za	k7c4	za
kontrolované	kontrolovaný	k2eAgFnPc4d1	kontrolovaná
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnPc4	vlhkost
oxidovat	oxidovat	k5eAaBmF	oxidovat
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
oxidace	oxidace	k1gFnSc2	oxidace
určuje	určovat	k5eAaImIp3nS	určovat
kvalitu	kvalita	k1gFnSc4	kvalita
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
usušeny	usušit	k5eAaPmNgInP	usušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
další	další	k2eAgFnSc4d1	další
oxidaci	oxidace	k1gFnSc4	oxidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
roztříděny	roztříděn	k2eAgInPc1d1	roztříděn
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
podle	podle	k7c2	podle
svojí	svůj	k3xOyFgFnSc2	svůj
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
celé	celý	k2eAgInPc1d1	celý
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
zlomené	zlomený	k2eAgInPc1d1	zlomený
a	a	k8xC	a
prach	prach	k1gInSc1	prach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
sít	síto	k1gNnPc2	síto
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poté	poté	k6eAd1	poté
dále	daleko	k6eAd2	daleko
tříděn	třídit	k5eAaImNgInS	třídit
podle	podle	k7c2	podle
dalších	další	k2eAgNnPc2d1	další
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
<g/>
Čaj	čaj	k1gInSc1	čaj
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
připraven	připravit	k5eAaPmNgInS	připravit
k	k	k7c3	k
balení	balení	k1gNnSc3	balení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
