<s>
Traktor	traktor	k1gInSc1	traktor
(	(	kIx(	(
<g/>
internacionalismus	internacionalismus	k1gInSc1	internacionalismus
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
tractor	tractor	k1gInSc1	tractor
'	'	kIx"	'
<g/>
tahač	tahač	k1gInSc1	tahač
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
trahere	trahrat	k5eAaPmIp3nS	trahrat
–	–	k?	–
táhnout	táhnout	k5eAaImF	táhnout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
trakční	trakční	k2eAgInSc1d1	trakční
stroj	stroj	k1gInSc1	stroj
resp.	resp.	kA	resp.
motorové	motorový	k2eAgNnSc1d1	motorové
vozidlo	vozidlo	k1gNnSc1	vozidlo
sloužící	sloužící	k1gFnSc2	sloužící
především	především	k9	především
k	k	k7c3	k
tahu	tah	k1gInSc3	tah
<g/>
.	.	kIx.	.
</s>
