<s>
Damašek	Damašek	k1gInSc1	Damašek
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
oficiálně	oficiálně	k6eAd1	oficiálně
د	د	k?	د
Dimašk	Dimašk	k1gInSc1	Dimašk
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
aš-Šám	aš-Šat	k5eAaBmIp1nS	aš-Šat
ا	ا	k?	ا
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
nejstarší	starý	k2eAgNnSc1d3	nejstarší
trvale	trvale	k6eAd1	trvale
osídlené	osídlený	k2eAgNnSc1d1	osídlené
město	město	k1gNnSc1	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
8	[number]	k4	8
000	[number]	k4	000
lety	let	k1gInPc7	let
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Aramejců	Aramejec	k1gMnPc2	Aramejec
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
město	město	k1gNnSc1	město
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
příchod	příchod	k1gInSc1	příchod
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vybudování	vybudování	k1gNnSc4	vybudování
prvního	první	k4xOgInSc2	první
vodovodního	vodovodní	k2eAgInSc2d1	vodovodní
systému	systém	k1gInSc2	systém
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
již	již	k6eAd1	již
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
se	se	k3xPyFc4	se
Damašek	Damašek	k1gInSc4	Damašek
pyšnil	pyšnit	k5eAaImAgInS	pyšnit
coby	coby	k?	coby
významné	významný	k2eAgFnPc4d1	významná
obchodní	obchodní	k2eAgFnPc4d1	obchodní
a	a	k8xC	a
řemeslné	řemeslný	k2eAgNnSc1d1	řemeslné
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
metropolí	metropol	k1gFnSc7	metropol
silné	silný	k2eAgFnSc2d1	silná
aramejské	aramejský	k2eAgFnSc2d1	aramejská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Aramejcích	Aramejec	k1gMnPc6	Aramejec
přišli	přijít	k5eAaPmAgMnP	přijít
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
,	,	kIx,	,
Babylónci	Babylónek	k1gMnPc1	Babylónek
a	a	k8xC	a
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
64	[number]	k4	64
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
učinila	učinit	k5eAaImAgFnS	učinit
svou	svůj	k3xOyFgFnSc4	svůj
provincii	provincie	k1gFnSc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
římské	římský	k2eAgFnSc6d1	římská
nadvládě	nadvláda	k1gFnSc6	nadvláda
dodnes	dodnes	k6eAd1	dodnes
svědčí	svědčit	k5eAaImIp3nP	svědčit
třeba	třeba	k6eAd1	třeba
ruiny	ruina	k1gFnPc1	ruina
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
zažilo	zažít	k5eAaPmAgNnS	zažít
město	město	k1gNnSc1	město
v	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
661	[number]	k4	661
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rod	rod	k1gInSc1	rod
Ummájovců	Ummájovec	k1gInPc2	Ummájovec
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
Damašek	Damašek	k1gInSc1	Damašek
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nového	nový	k2eAgInSc2d1	nový
islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
následoval	následovat	k5eAaImAgInS	následovat
rychlý	rychlý	k2eAgInSc1d1	rychlý
rozvoj	rozvoj	k1gInSc1	rozvoj
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
výstavba	výstavba	k1gFnSc1	výstavba
-	-	kIx~	-
stavěly	stavět	k5eAaImAgInP	stavět
se	se	k3xPyFc4	se
honosné	honosný	k2eAgInPc1d1	honosný
paláce	palác	k1gInPc1	palác
a	a	k8xC	a
mešity	mešita	k1gFnPc1	mešita
<g/>
,	,	kIx,	,
prostorné	prostorný	k2eAgInPc1d1	prostorný
trhy	trh	k1gInPc1	trh
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnPc1	nemocnice
<g/>
,	,	kIx,	,
proslulé	proslulý	k2eAgFnPc1d1	proslulá
lázně	lázeň	k1gFnPc1	lázeň
a	a	k8xC	a
vodovodní	vodovodní	k2eAgNnSc1d1	vodovodní
a	a	k8xC	a
kanalizační	kanalizační	k2eAgNnSc1d1	kanalizační
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
památkou	památka	k1gFnSc7	památka
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
éru	éra	k1gFnSc4	éra
je	být	k5eAaImIp3nS	být
Ummájovská	Ummájovský	k2eAgFnSc1d1	Ummájovský
mešita	mešita	k1gFnSc1	mešita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
750	[number]	k4	750
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Bagdád	Bagdád	k1gInSc1	Bagdád
a	a	k8xC	a
veliká	veliký	k2eAgFnSc1d1	veliká
sláva	sláva	k1gFnSc1	sláva
Damašku	Damašek	k1gInSc2	Damašek
se	se	k3xPyFc4	se
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
vytratila	vytratit	k5eAaPmAgFnS	vytratit
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
rozvoj	rozvoj	k1gInSc1	rozvoj
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
dostavil	dostavit	k5eAaPmAgMnS	dostavit
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Osmanů	Osman	k1gMnPc2	Osman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
znovu	znovu	k6eAd1	znovu
stavět	stavět	k5eAaImF	stavět
mešity	mešita	k1gFnPc4	mešita
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
citadely	citadela	k1gFnPc4	citadela
a	a	k8xC	a
obestavěli	obestavět	k5eAaPmAgMnP	obestavět
Damašek	Damašek	k1gInSc4	Damašek
pásem	pásmo	k1gNnPc2	pásmo
ochranných	ochranný	k2eAgFnPc2d1	ochranná
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
výstavba	výstavba	k1gFnSc1	výstavba
avšak	avšak	k8xC	avšak
netrvala	trvat	k5eNaImAgFnS	trvat
dlouho	dlouho	k6eAd1	dlouho
-	-	kIx~	-
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
zničeno	zničen	k2eAgNnSc1d1	zničeno
a	a	k8xC	a
spáleno	spálen	k2eAgNnSc1d1	spáleno
Mongolským	mongolský	k2eAgNnSc7d1	mongolské
impériem	impérium	k1gNnSc7	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
Damašek	Damašek	k1gInSc1	Damašek
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
opět	opět	k6eAd1	opět
vzkvétat	vzkvétat	k5eAaImF	vzkvétat
-	-	kIx~	-
navenek	navenek	k6eAd1	navenek
působilo	působit	k5eAaImAgNnS	působit
město	město	k1gNnSc1	město
chudě	chudě	k6eAd1	chudě
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
to	ten	k3xDgNnSc4	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
klenot	klenot	k1gInSc1	klenot
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Damašek	Damašek	k1gInSc1	Damašek
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Damašské	damašský	k2eAgNnSc1d1	damašské
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
ukryto	ukrýt	k5eAaPmNgNnS	ukrýt
za	za	k7c7	za
vnitřními	vnitřní	k2eAgFnPc7d1	vnitřní
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
sem	sem	k6eAd1	sem
vede	vést	k5eAaImIp3nS	vést
několika	několik	k4yIc7	několik
branami	brána	k1gFnPc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
mešit	mešita	k1gFnPc2	mešita
<g/>
,	,	kIx,	,
citadela	citadela	k1gFnSc1	citadela
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgFnSc1d1	vojenská
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc2d1	bývalá
rezidence	rezidence	k1gFnSc2	rezidence
a	a	k8xC	a
paláce	palác	k1gInSc2	palác
vlivných	vlivný	k2eAgMnPc2d1	vlivný
a	a	k8xC	a
bohatých	bohatý	k2eAgMnPc2d1	bohatý
kupců	kupec	k1gMnPc2	kupec
a	a	k8xC	a
správců	správce	k1gMnPc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
Barada	Barada	k1gFnSc1	Barada
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Damašek	Damašek	k1gInSc1	Damašek
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
DAM	dáma	k1gFnPc2	dáma
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
OSDI	OSDI	kA	OSDI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
situované	situovaný	k2eAgFnPc1d1	situovaná
20	[number]	k4	20
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
předních	přední	k2eAgFnPc2d1	přední
světových	světový	k2eAgFnPc2d1	světová
destinací	destinace	k1gFnPc2	destinace
-	-	kIx~	-
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
nebo	nebo	k8xC	nebo
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
linku	linka	k1gFnSc4	linka
z	z	k7c2	z
Ruzyňského	ruzyňský	k2eAgNnSc2d1	ruzyňské
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
do	do	k7c2	do
Damašku	Damašek	k1gInSc2	Damašek
provozovaly	provozovat	k5eAaImAgFnP	provozovat
také	také	k9	také
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Chálid	Chálid	k1gInSc1	Chálid
Chodža	chodža	k1gMnSc1	chodža
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
-	-	kIx~	-
předák	předák	k1gMnSc1	předák
syrské	syrský	k2eAgFnSc2d1	Syrská
opoziční	opoziční	k2eAgFnSc2d1	opoziční
koalice	koalice	k1gFnSc2	koalice
Dubaj	Dubaj	k1gInSc1	Dubaj
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Bukurešť	Bukurešť	k1gFnSc1	Bukurešť
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Ammán	Ammán	k1gInSc1	Ammán
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc4	Jordánsko
Córdoba	Córdoba	k1gFnSc1	Córdoba
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Alexandrie	Alexandrie	k1gFnSc1	Alexandrie
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Málaga	Málaga	k1gFnSc1	Málaga
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Rabat	rabat	k1gInSc1	rabat
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Sã	Sã	k1gFnSc1	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
San	San	k1gFnSc2	San
<g/>
'	'	kIx"	'
<g/>
á	á	k0	á
<g/>
,	,	kIx,	,
Jemen	Jemen	k1gInSc1	Jemen
Navi	Nav	k1gFnSc2	Nav
Mumbai	Mumba	k1gFnSc2	Mumba
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
</s>
