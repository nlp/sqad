<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
nazývá	nazývat	k5eAaImIp3nS	nazývat
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
létá	létat	k5eAaImIp3nS	létat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
?	?	kIx.	?
</s>
