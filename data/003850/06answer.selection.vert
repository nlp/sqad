<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
mělké	mělký	k2eAgNnSc1d1	mělké
stepní	stepní	k2eAgNnSc1d1	stepní
Neziderské	neziderský	k2eAgNnSc1d1	Neziderské
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Burgenlandu	Burgenlando	k1gNnSc6	Burgenlando
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
náleží	náležet	k5eAaImIp3nS	náležet
77	[number]	k4	77
%	%	kIx~	%
svojí	svůj	k3xOyFgFnSc2	svůj
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
315	[number]	k4	315
km2	km2	k4	km2
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
jezera	jezero	k1gNnSc2	jezero
patří	patřit	k5eAaImIp3nP	patřit
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
