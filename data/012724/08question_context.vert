<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
Ulm	Ulm	k1gFnPc2	Ulm
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
vědců	vědec	k1gMnPc2	vědec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
vědce	vědec	k1gMnSc4	vědec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Newtonem	newton	k1gInSc7	newton
za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
fyzika	fyzik	k1gMnSc4	fyzik
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
příspěvky	příspěvek	k1gInPc4	příspěvek
fyzice	fyzika	k1gFnSc3	fyzika
patří	patřit	k5eAaImIp3nS	patřit
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
kvantování	kvantování	k1gNnSc2	kvantování
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
fotoefektu	fotoefekt	k1gInSc2	fotoefekt
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
a	a	k8xC	a
snad	snad	k9	snad
nejvíce	nejvíce	k6eAd1	nejvíce
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doposud	doposud	k6eAd1	doposud
nejlépe	dobře	k6eAd3	dobře
popisuje	popisovat	k5eAaImIp3nS	popisovat
vesmír	vesmír	k1gInSc1	vesmír
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
měřítkách	měřítko	k1gNnPc6	měřítko
<g/>
.	.	kIx.	.
</s>

