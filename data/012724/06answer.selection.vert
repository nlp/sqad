<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
Ulm	Ulm	k1gFnPc2	Ulm
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
vědců	vědec	k1gMnPc2	vědec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
