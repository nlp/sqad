<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Troška	troška	k1gFnSc1	troška
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
u	u	k7c2	u
Bavorova	Bavorův	k2eAgNnSc2d1	Bavorův
<g/>
,	,	kIx,	,
u	u	k7c2	u
Lhotského	Lhotského	k2eAgInSc2d1	Lhotského
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Dobříš	Dobříš	k1gFnSc4	Dobříš
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Nahořany	Nahořan	k1gMnPc4	Nahořan
(	(	kIx(	(
<g/>
Čestice	Čestice	k1gFnSc1	Čestice
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Helfenburk	Helfenburk	k1gInSc4	Helfenburk
u	u	k7c2	u
Bavorova	Bavorův	k2eAgNnSc2d1	Bavorův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
jihočeské	jihočeský	k2eAgFnSc6d1	Jihočeská
vísce	víska	k1gFnSc6	víska
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
tmavých	tmavý	k2eAgInPc2d1	tmavý
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
pohledný	pohledný	k2eAgMnSc1d1	pohledný
mládenec	mládenec	k1gMnSc1	mládenec
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
s	s	k7c7	s
pevným	pevný	k2eAgNnSc7d1	pevné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
vysvobodit	vysvobodit	k5eAaPmF	vysvobodit
zakletou	zakletý	k2eAgFnSc4d1	zakletá
princeznu	princezna	k1gFnSc4	princezna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
mlynářem	mlynář	k1gMnSc7	mlynář
<g/>
,	,	kIx,	,
krásná	krásný	k2eAgFnSc1d1	krásná
Eliška	Eliška	k1gFnSc1	Eliška
<g/>
.	.	kIx.	.
</s>
<s>
Mládenec	mládenec	k1gMnSc1	mládenec
se	se	k3xPyFc4	se
Elišce	Eliška	k1gFnSc3	Eliška
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
namluví	namluvit	k5eAaPmIp3nS	namluvit
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rybníce	rybník	k1gInSc6	rybník
je	být	k5eAaImIp3nS	být
zakletá	zakletý	k2eAgFnSc1d1	zakletá
princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jako	jako	k9	jako
pomocník	pomocník	k1gMnSc1	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nS	líbit
místnímu	místní	k2eAgMnSc3d1	místní
čertovi	čert	k1gMnSc3	čert
a	a	k8xC	a
vodníkovi	vodník	k1gMnSc3	vodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
mlynářovu	mlynářův	k2eAgFnSc4d1	mlynářova
dceru	dcera	k1gFnSc4	dcera
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
seč	seč	k6eAd1	seč
jim	on	k3xPp3gMnPc3	on
síly	síla	k1gFnPc1	síla
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
pomocí	pomocí	k7c2	pomocí
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
intrik	intrika	k1gFnPc2	intrika
mládence	mládenec	k1gMnSc2	mládenec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
ze	z	k7c2	z
mlýna	mlýn	k1gInSc2	mlýn
vypudit	vypudit	k5eAaPmF	vypudit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
třetí	třetí	k4xOgMnSc1	třetí
ženich	ženich	k1gMnSc1	ženich
<g/>
:	:	kIx,	:
starý	starý	k1gMnSc1	starý
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgMnSc1d1	bohatý
knížepán	knížepán	k1gMnSc1	knížepán
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
Eliška	Eliška	k1gFnSc1	Eliška
všechny	všechen	k3xTgMnPc4	všechen
nápadníky	nápadník	k1gMnPc4	nápadník
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
mlynář	mlynář	k1gMnSc1	mlynář
se	se	k3xPyFc4	se
rozzlobí	rozzlobit	k5eAaPmIp3nS	rozzlobit
<g/>
.	.	kIx.	.
</s>
<s>
Určí	určit	k5eAaPmIp3nS	určit
lhůtu	lhůta	k1gFnSc4	lhůta
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
si	se	k3xPyFc3	se
do	do	k7c2	do
úplňku	úplněk	k1gInSc2	úplněk
ženicha	ženich	k1gMnSc2	ženich
sama	sám	k3xTgMnSc4	sám
vybere	vybrat	k5eAaPmIp3nS	vybrat
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
dostane	dostat	k5eAaPmIp3nS	dostat
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
si	se	k3xPyFc3	se
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
první	první	k4xOgMnPc4	první
řekne	říct	k5eAaPmIp3nS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
ženichy	ženich	k1gMnPc7	ženich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žila	žít	k5eAaImAgFnS	žít
Eliška	Eliška	k1gFnSc1	Eliška
s	s	k7c7	s
mlynářem	mlynář	k1gMnSc7	mlynář
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
štáb	štáb	k1gInSc4	štáb
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
natáčení	natáčení	k1gNnSc6	natáčení
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
mlýna	mlýn	k1gInSc2	mlýn
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
dětských	dětský	k2eAgMnPc2d1	dětský
diváků	divák	k1gMnPc2	divák
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
6	[number]	k4	6
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
natočen	natočit	k5eAaBmNgInS	natočit
druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
prvnímu	první	k4xOgInSc3	první
dílu	díl	k1gInSc2	díl
diváci	divák	k1gMnPc1	divák
neuvidí	uvidět	k5eNaPmIp3nP	uvidět
v	v	k7c6	v
roli	role	k1gFnSc6	role
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
Lucii	Lucie	k1gFnSc4	Lucie
Bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Monika	Monika	k1gFnSc1	Monika
Absolonová	Absolonová	k1gFnSc1	Absolonová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Písně	píseň	k1gFnPc1	píseň
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
zazněla	zaznít	k5eAaPmAgFnS	zaznít
řada	řada	k1gFnSc1	řada
písniček	písnička	k1gFnPc2	písnička
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
složil	složit	k5eAaPmAgMnS	složit
Miloš	Miloš	k1gMnSc1	Miloš
Krkoška	krkoška	k1gFnSc1	krkoška
a	a	k8xC	a
texty	text	k1gInPc4	text
Václav	Václav	k1gMnSc1	Václav
Bárta	Bárta	k1gMnSc1	Bárta
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
