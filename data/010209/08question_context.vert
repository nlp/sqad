<s>
Kitchener	Kitchener	k1gInSc1	Kitchener
je	být	k5eAaImIp3nS	být
kanadské	kanadský	k2eAgNnSc4d1	kanadské
město	město	k1gNnSc4	město
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
britském	britský	k2eAgNnSc6d1	Britské
vojenském	vojenský	k2eAgNnSc6d1	vojenské
veliteli	velitel	k1gMnPc7	velitel
Horatio	Horatio	k6eAd1	Horatio
Kitchenerovi	Kitchenerův	k2eAgMnPc1d1	Kitchenerův
<g/>
.	.	kIx.	.
</s>
