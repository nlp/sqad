<p>
<s>
Kitchener	Kitchener	k1gInSc1	Kitchener
je	být	k5eAaImIp3nS	být
kanadské	kanadský	k2eAgNnSc4d1	kanadské
město	město	k1gNnSc4	město
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
britském	britský	k2eAgNnSc6d1	Britské
vojenském	vojenský	k2eAgNnSc6d1	vojenské
veliteli	velitel	k1gMnPc7	velitel
Horatio	Horatio	k6eAd1	Horatio
Kitchenerovi	Kitchenerův	k2eAgMnPc1d1	Kitchenerův
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
městem	město	k1gNnSc7	město
Waterloo	Waterloo	k1gNnSc2	Waterloo
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
často	často	k6eAd1	často
říká	říkat	k5eAaImIp3nS	říkat
Kitchener-Waterloo	Kitchener-Waterloo	k6eAd1	Kitchener-Waterloo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k6eAd1	ještě
přidává	přidávat	k5eAaImIp3nS	přidávat
město	město	k1gNnSc1	město
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
Kitchener	Kitchenra	k1gFnPc2	Kitchenra
<g/>
,	,	kIx,	,
Waterloo	Waterloo	k1gNnSc2	Waterloo
a	a	k8xC	a
Cambridge	Cambridge	k1gFnSc2	Cambridge
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Tri-city	Triit	k1gInPc4	Tri-cit
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Trojměsto	Trojměsta	k1gMnSc5	Trojměsta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
Přejmenováno	přejmenován	k2eAgNnSc4d1	přejmenováno
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
maršála	maršál	k1gMnSc4	maršál
Kitchenera	Kitchener	k1gMnSc4	Kitchener
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kitchener	Kitchenra	k1gFnPc2	Kitchenra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
