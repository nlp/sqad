<s>
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
udělována	udělovat	k5eAaImNgFnS
každoročně	každoročně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1901	[number]	k4
na	na	k7c6
základě	základ	k1gInSc6
poslední	poslední	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
švédského	švédský	k2eAgMnSc2d1
vědce	vědec	k1gMnSc2
a	a	k8xC
průmyslníka	průmyslník	k1gMnSc2
Alfreda	Alfred	k1gMnSc2
Nobela	Nobel	k1gMnSc2
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc2
dynamitu	dynamit	k1gInSc2
<g/>
.	.	kIx.
</s>