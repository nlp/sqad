<s>
Karel	Karel	k1gMnSc1
Kalivoda	Kalivoda	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Kalivoda	Kalivoda	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1916	#num#	k4
<g/>
Brno	Brno	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1980	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Brtníky	brtník	k1gMnPc7
Povolání	povolání	k1gNnPc5
</s>
<s>
policista	policista	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Kalivoda	Kalivoda	k1gMnSc1
<g/>
,	,	kIx,
přezdívaný	přezdívaný	k2eAgMnSc1d1
pražský	pražský	k2eAgMnSc1d1
Maigret	Maigret	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1916	#num#	k4
Brno	Brno	k1gNnSc4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
Brtníky	brtník	k1gMnPc7
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
kriminalista	kriminalista	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1952	#num#	k4
až	až	k9
1966	#num#	k4
ředitel	ředitel	k1gMnSc1
české	český	k2eAgFnSc2d1
kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
mládí	mládí	k1gNnSc6
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
klempířské	klempířský	k2eAgFnSc6d1
dílně	dílna	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kriminalistou	kriminalista	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
Oblastní	oblastní	k2eAgFnSc2d1
kriminální	kriminální	k2eAgFnSc2d1
úřadovny	úřadovna	k1gFnSc2
v	v	k7c6
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínal	začínat	k5eAaImAgMnS
jako	jako	k9
telefonista	telefonista	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
už	už	k6eAd1
patřil	patřit	k5eAaImAgInS
do	do	k7c2
výjezdové	výjezdový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgMnPc7
nadřízenými	nadřízený	k1gMnPc7
byl	být	k5eAaImAgMnS
dobře	dobře	k6eAd1
hodnocen	hodnotit	k5eAaImNgMnS
a	a	k8xC
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
již	již	k6eAd1
řídil	řídit	k5eAaImAgMnS
pražskou	pražský	k2eAgFnSc4d1
kriminální	kriminální	k2eAgFnSc4d1
policii	policie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
vyřešené	vyřešený	k2eAgInPc4d1
případy	případ	k1gInPc4
tehdy	tehdy	k6eAd1
patřily	patřit	k5eAaImAgInP
mj.	mj.	kA
<g/>
:	:	kIx,
</s>
<s>
kauza	kauza	k1gFnSc1
Mědirytina	mědirytina	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
šlo	jít	k5eAaImAgNnS
o	o	k7c6
odhalení	odhalení	k1gNnSc6
padělatelů	padělatel	k1gMnPc2
potravinových	potravinový	k2eAgInPc2d1
lístků	lístek	k1gInPc2
</s>
<s>
případ	případ	k1gInSc1
vraždy	vražda	k1gFnSc2
Herty	Herta	k1gFnSc2
Černínové	Černínová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zmizela	zmizet	k5eAaPmAgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
1951	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
kriminalisté	kriminalista	k1gMnPc1
znovuotevřeli	znovuotevřít	k5eAaPmAgMnP
případ	případ	k1gInSc4
vraždy	vražda	k1gFnSc2
Otýlie	Otýlie	k1gFnSc2
Vranské	vranský	k2eAgFnSc2d1
z	z	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
však	však	k9
ani	ani	k8xC
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
objasnit	objasnit	k5eAaPmF
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
Kalivodovi	Kalivoda	k1gMnSc6
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
přinesly	přinést	k5eAaPmAgInP
posun	posun	k1gInSc4
v	v	k7c6
kariéře	kariéra	k1gFnSc6
<g/>
:	:	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
ředitelem	ředitel	k1gMnSc7
české	český	k2eAgFnSc2d1
kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nebyl	být	k5eNaImAgInS
však	však	k9
ochoten	ochoten	k2eAgInSc1d1
vyšetřovat	vyšetřovat	k5eAaImF
některé	některý	k3yIgInPc4
citlivé	citlivý	k2eAgInPc4d1
případy	případ	k1gInPc4
podle	podle	k7c2
politické	politický	k2eAgFnSc2d1
objednávky	objednávka	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
podivnou	podivný	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
bývalého	bývalý	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
Jindřicha	Jindřich	k1gMnSc2
Veselého	Veselý	k1gMnSc2
nebo	nebo	k8xC
krádež	krádež	k1gFnSc4
tehdy	tehdy	k6eAd1
obrovské	obrovský	k2eAgFnSc2d1
částky	částka	k1gFnSc2
přesahující	přesahující	k2eAgNnSc1d1
800	#num#	k4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
při	při	k7c6
přepadení	přepadení	k1gNnSc6
v	v	k7c6
Čakovicích	Čakovice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
tak	tak	k6eAd1
byl	být	k5eAaImAgMnS
kvůli	kvůli	k7c3
intrikám	intrika	k1gFnPc3
StB	StB	k1gFnSc2
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
vyhozen	vyhozen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Začal	začít	k5eAaPmAgMnS
pak	pak	k6eAd1
opět	opět	k6eAd1
řídit	řídit	k5eAaImF
pražskou	pražský	k2eAgFnSc4d1
kriminální	kriminální	k2eAgFnSc4d1
policii	policie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
byl	být	k5eAaImAgInS
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
důchodu	důchod	k1gInSc2
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
zákaz	zákaz	k1gInSc4
psát	psát	k5eAaImF
a	a	k8xC
publikovat	publikovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
novinách	novina	k1gFnPc6
nevyšel	vyjít	k5eNaPmAgInS
ani	ani	k8xC
malý	malý	k2eAgInSc1d1
nekrolog	nekrolog	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BOHATA	Bohata	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc1d1
Maigret	Maigret	k1gInSc1
bojoval	bojovat	k5eAaImAgInS
s	s	k7c7
galerkou	galerka	k1gFnSc7
i	i	k8xC
Státní	státní	k2eAgFnSc7d1
bezpečností	bezpečnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
MF	MF	kA
DNES	dnes	k6eAd1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
má	mít	k5eAaImIp3nS
českého	český	k2eAgMnSc4d1
Maigreta	Maigret	k1gMnSc4
a	a	k8xC
ten	ten	k3xDgMnSc1
má	mít	k5eAaImIp3nS
teď	teď	k6eAd1
svůj	svůj	k3xOyFgInSc4
životopis	životopis	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KUČERA	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
:	:	kIx,
Pražský	pražský	k2eAgInSc1d1
Maigret	Maigret	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobní	osobní	k2eAgInSc1d1
zápas	zápas	k1gInSc1
legendárního	legendární	k2eAgMnSc2d1
kriminalisty	kriminalista	k1gMnSc2
<g/>
;	;	kIx,
Academia	academia	k1gFnSc1
2009	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-200-1711-6	978-80-200-1711-6	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1052701	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7331	#num#	k4
0262	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2009193441	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83764563	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2009193441	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šluknovský	šluknovský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
</s>
