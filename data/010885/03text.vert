<p>
<s>
Sã	Sã	k?	Sã
Paulo	Paula	k1gFnSc5	Paula
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
nejbohatší	bohatý	k2eAgNnSc1d3	nejbohatší
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
a	a	k8xC	a
sedmou	sedmý	k4xOgFnSc7	sedmý
největší	veliký	k2eAgFnSc7d3	veliký
metropolí	metropol	k1gFnSc7	metropol
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sã	Sã	k?	Sã
Paulo	Paula	k1gFnSc5	Paula
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
<g/>
,	,	kIx,	,
nejlidnatějšího	lidnatý	k2eAgInSc2d3	nejlidnatější
brazilského	brazilský	k2eAgInSc2d1	brazilský
státu	stát	k1gInSc2	stát
Sã	Sã	k1gMnSc2	Sã
Paula	Paul	k1gMnSc2	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
nad	nad	k7c7	nad
10	[number]	k4	10
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1	[number]	k4	1
523	[number]	k4	523
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
biblickou	biblický	k2eAgFnSc4d1	biblická
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
Pavla	Pavel	k1gMnSc4	Pavel
z	z	k7c2	z
Tarsu	Tars	k1gInSc2	Tars
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
silně	silně	k6eAd1	silně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
svůj	svůj	k3xOyFgInSc4	svůj
region	region	k1gInSc4	region
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
také	také	k9	také
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Sã	Sã	k?	Sã
Paulo	Paula	k1gFnSc5	Paula
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
světové	světový	k2eAgNnSc4d1	světové
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Tiete	Tiet	k1gInSc5	Tiet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
město	město	k1gNnSc1	město
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
a	a	k8xC	a
soužití	soužití	k1gNnSc4	soužití
desítek	desítka	k1gFnPc2	desítka
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
22	[number]	k4	22
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc4	potomek
dávných	dávný	k2eAgMnPc2d1	dávný
italských	italský	k2eAgMnPc2d1	italský
či	či	k8xC	či
japonských	japonský	k2eAgMnPc2d1	japonský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Sao	Sao	k?	Sao
Paulo	Paula	k1gFnSc5	Paula
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
městské	městský	k2eAgFnPc1d1	městská
čtvrti	čtvrt	k1gFnPc1	čtvrt
mají	mít	k5eAaImIp3nP	mít
silně	silně	k6eAd1	silně
národnostní	národnostní	k2eAgInSc4d1	národnostní
charakter	charakter	k1gInSc4	charakter
–	–	k?	–
Liberdade	Liberdad	k1gInSc5	Liberdad
je	být	k5eAaImIp3nS	být
asijská	asijský	k2eAgFnSc1d1	asijská
<g/>
,	,	kIx,	,
Bala	Bala	k1gMnSc1	Bala
Vista	vista	k2eAgMnSc2d1	vista
a	a	k8xC	a
Bixiga	Bixig	k1gMnSc2	Bixig
jsou	být	k5eAaImIp3nP	být
italské	italský	k2eAgFnPc1d1	italská
<g/>
,	,	kIx,	,
Bom	Bom	k1gFnSc1	Bom
Retir	Retira	k1gFnPc2	Retira
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
arabská	arabský	k2eAgFnSc1d1	arabská
komunita	komunita	k1gFnSc1	komunita
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
Rua	Rua	k1gFnSc2	Rua
25	[number]	k4	25
de	de	k?	de
Marco	Marco	k1gMnSc1	Marco
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
a	a	k8xC	a
památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
ulice	ulice	k1gFnSc1	ulice
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
–	–	k?	–
Avenida	Avenid	k1gMnSc4	Avenid
Paulista	Paulista	k1gMnSc1	Paulista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přilehlém	přilehlý	k2eAgNnSc6d1	přilehlé
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
koncentrováno	koncentrován	k2eAgNnSc1d1	koncentrováno
nejvíce	nejvíce	k6eAd1	nejvíce
dobrých	dobrý	k2eAgFnPc2d1	dobrá
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
kaváren	kavárna	k1gFnPc2	kavárna
a	a	k8xC	a
nočních	noční	k2eAgInPc2d1	noční
klubů	klub	k1gInPc2	klub
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
bývá	bývat	k5eAaImIp3nS	bývat
Sao	Sao	k1gFnSc4	Sao
Paulo	Paula	k1gFnSc5	Paula
někdy	někdy	k6eAd1	někdy
nazýváno	nazývat	k5eAaImNgNnS	nazývat
"	"	kIx"	"
<g/>
tropickým	tropický	k2eAgNnSc7d1	tropické
New	New	k1gFnSc4	New
Yorkem	York	k1gInSc7	York
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Avenida	Avenid	k1gMnSc4	Avenid
Paulista	Paulista	k1gMnSc1	Paulista
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
Museu	museum	k1gNnSc6	museum
de	de	k?	de
arte	art	k1gInSc2	art
de	de	k?	de
Sao	Sao	k1gMnSc1	Sao
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
sbírku	sbírka	k1gFnSc4	sbírka
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
od	od	k7c2	od
světových	světový	k2eAgMnPc2d1	světový
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Rembrant	Rembrant	k1gMnSc1	Rembrant
<g/>
,	,	kIx,	,
Rubens	Rubens	k1gInSc1	Rubens
<g/>
,	,	kIx,	,
Renoir	Renoir	k1gInSc1	Renoir
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vynikající	vynikající	k2eAgNnPc1d1	vynikající
díla	dílo	k1gNnPc1	dílo
brazilského	brazilský	k2eAgNnSc2d1	brazilské
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zajímavosti	zajímavost	k1gFnPc4	zajímavost
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Parque	Parque	k1gInSc1	Parque
do	do	k7c2	do
Ibirapuera	Ibirapuero	k1gNnSc2	Ibirapuero
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
známý	známý	k2eAgMnSc1d1	známý
architekt	architekt	k1gMnSc1	architekt
Oscar	Oscar	k1gMnSc1	Oscar
Niemeyer	Niemeyer	k1gMnSc1	Niemeyer
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
designerem	designer	k1gMnSc7	designer
Burle	Burle	k1gNnSc7	Burle
Marxem	Marx	k1gMnSc7	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
planetárium	planetárium	k1gNnSc1	planetárium
a	a	k8xC	a
také	také	k9	také
japonský	japonský	k2eAgInSc1d1	japonský
pavilon	pavilon	k1gInSc1	pavilon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
známý	známý	k2eAgInSc1d1	známý
obelisk	obelisk	k1gInSc1	obelisk
a	a	k8xC	a
Monumento	Monumento	k1gNnSc1	Monumento
ás	ás	k?	ás
Bandeiras	Bandeiras	k1gMnSc1	Bandeiras
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Sã	Sã	k?	Sã
Paulo	Paula	k1gFnSc5	Paula
(	(	kIx(	(
<g/>
stát	stát	k5eAaPmF	stát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Sao	Sao	k1gFnSc2	Sao
Paulo	Paula	k1gFnSc5	Paula
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sã	Sã	k1gFnSc2	Sã
Paulo	Paula	k1gFnSc5	Paula
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
