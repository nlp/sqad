<s>
Lajka	lajka	k1gFnSc1	lajka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Л	Л	k?	Л
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Štěkačka	Štěkačka	k1gFnSc1	Štěkačka
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
1954	[number]	k4	1954
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
fena	fena	k1gFnSc1	fena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
živý	živý	k2eAgMnSc1d1	živý
tvor	tvor	k1gMnSc1	tvor
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sovětské	sovětský	k2eAgFnSc2d1	sovětská
družice	družice	k1gFnSc2	družice
Sputnik	sputnik	k1gInSc4	sputnik
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
přípravy	příprava	k1gFnSc2	příprava
jejího	její	k3xOp3gInSc2	její
letu	let	k1gInSc2	let
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
kosmického	kosmický	k2eAgInSc2d1	kosmický
letu	let	k1gInSc2	let
na	na	k7c4	na
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Vyslání	vyslání	k1gNnSc1	vyslání
zvířat	zvíře	k1gNnPc2	zvíře
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
nezbytným	zbytný	k2eNgInSc7d1	zbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
letů	let	k1gInPc2	let
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lajka	lajka	k1gFnSc1	lajka
<g/>
,	,	kIx,	,
potulný	potulný	k2eAgMnSc1d1	potulný
pes	pes	k1gMnSc1	pes
odchycený	odchycený	k2eAgMnSc1d1	odchycený
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
moskevských	moskevský	k2eAgFnPc2d1	Moskevská
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Kudrjavka	Kudrjavka	k1gFnSc1	Kudrjavka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
Kudrnka	kudrnka	k1gFnSc1	kudrnka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
let	let	k1gInSc4	let
na	na	k7c6	na
Sputniku	sputnik	k1gInSc6	sputnik
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
cvičena	cvičen	k2eAgFnSc1d1	cvičena
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
návratu	návrat	k1gInSc2	návrat
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
nebyly	být	k5eNaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nebyl	být	k5eNaImAgMnS	být
Lajčin	lajčin	k2eAgInSc4d1	lajčin
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
plánován	plánovat	k5eAaImNgInS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prožije	prožít	k5eAaPmIp3nS	prožít
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
uhynula	uhynout	k5eAaPmAgFnS	uhynout
následkem	následkem	k7c2	následkem
stresu	stres	k1gInSc2	stres
a	a	k8xC	a
přehřátí	přehřátí	k1gNnSc2	přehřátí
už	už	k6eAd1	už
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
startu	start	k1gInSc6	start
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgMnSc7	první
tvorem	tvor	k1gMnSc7	tvor
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
