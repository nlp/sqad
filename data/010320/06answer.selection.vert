<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Měsíc	měsíc	k1gInSc1	měsíc
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nejlépe	dobře	k6eAd3	dobře
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
na	na	k7c4	na
střídání	střídání	k1gNnSc4	střídání
mořského	mořský	k2eAgInSc2d1	mořský
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
<g/>
.	.	kIx.	.
</s>
