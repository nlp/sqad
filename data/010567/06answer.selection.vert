<s>
Tradičním	tradiční	k2eAgNnSc7d1	tradiční
datem	datum	k1gNnSc7	datum
objevení	objevení	k1gNnSc2	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
Evropany	Evropan	k1gMnPc4	Evropan
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc1	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
břehům	břeh	k1gInPc3	břeh
tohoto	tento	k3xDgInSc2	tento
světadílu	světadíl	k1gInSc2	světadíl
pod	pod	k7c7	pod
španělskými	španělský	k2eAgFnPc7d1	španělská
vlajkami	vlajka	k1gFnPc7	vlajka
přirazila	přirazit	k5eAaPmAgFnS	přirazit
flotila	flotila	k1gFnSc1	flotila
vedená	vedený	k2eAgFnSc1d1	vedená
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
<g/>
.	.	kIx.	.
</s>
