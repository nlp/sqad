<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
je	být	k5eAaImIp3nS	být
model	model	k1gInSc4	model
městského	městský	k2eAgInSc2d1	městský
kloubového	kloubový	k2eAgInSc2d1	kloubový
autobusu	autobus	k1gInSc2	autobus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
společnost	společnost	k1gFnSc1	společnost
Karosa	karosa	k1gFnSc1	karosa
Vysoké	vysoká	k1gFnSc2	vysoká
Mýto	mýto	k1gNnSc1	mýto
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Inovovaná	inovovaný	k2eAgFnSc1d1	inovovaná
varianta	varianta	k1gFnSc1	varianta
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
B	B	kA	B
941	[number]	k4	941
<g/>
E.	E.	kA	E.
Autobus	autobus	k1gInSc4	autobus
B	B	kA	B
941	[number]	k4	941
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
typu	typ	k1gInSc2	typ
B	B	kA	B
741	[number]	k4	741
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
typem	typ	k1gInSc7	typ
B	B	kA	B
961	[number]	k4	961
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
B	B	kA	B
941	[number]	k4	941
je	být	k5eAaImIp3nS	být
třínápravový	třínápravový	k2eAgInSc4d1	třínápravový
autobus	autobus	k1gInSc4	autobus
s	s	k7c7	s
polosamonosnou	polosamonosný	k2eAgFnSc7d1	polosamonosná
karoserií	karoserie	k1gFnSc7	karoserie
panelové	panelový	k2eAgFnSc2d1	panelová
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
spojeny	spojit	k5eAaPmNgInP	spojit
kloubem	kloub	k1gInSc7	kloub
a	a	k8xC	a
krycím	krycí	k2eAgInSc7d1	krycí
měchem	měch	k1gInSc7	měch
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
za	za	k7c7	za
zadní	zadní	k2eAgFnSc7d1	zadní
nápravou	náprava	k1gFnSc7	náprava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
hnací	hnací	k2eAgFnSc1d1	hnací
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc4d1	přední
lichoběžníkovou	lichoběžníkový	k2eAgFnSc4d1	lichoběžníková
nápravu	náprava	k1gFnSc4	náprava
s	s	k7c7	s
nezávisle	závisle	k6eNd1	závisle
zavěšenými	zavěšený	k2eAgFnPc7d1	zavěšená
koly	kola	k1gFnPc4	kola
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
podnik	podnik	k1gInSc1	podnik
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc1d1	střední
a	a	k8xC	a
zadní	zadní	k2eAgNnSc1d1	zadní
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Detva	Detva	k1gFnSc1	Detva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
autobusu	autobus	k1gInSc2	autobus
B	B	kA	B
741	[number]	k4	741
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
B	B	kA	B
941	[number]	k4	941
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
odlišným	odlišný	k2eAgNnSc7d1	odlišné
předním	přední	k2eAgNnSc7d1	přední
i	i	k8xC	i
zadní	zadní	k2eAgFnPc4d1	zadní
čelem	čelo	k1gNnSc7	čelo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zaoblena	zaoblen	k2eAgNnPc1d1	zaobleno
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
sklolaminátový	sklolaminátový	k2eAgInSc1d1	sklolaminátový
panel	panel	k1gInSc1	panel
s	s	k7c7	s
kostrou	kostra	k1gFnSc7	kostra
ze	z	k7c2	z
svařených	svařený	k2eAgInPc2d1	svařený
profilů	profil	k1gInPc2	profil
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
inovovaným	inovovaný	k2eAgInSc7d1	inovovaný
interiérem	interiér	k1gInSc7	interiér
s	s	k7c7	s
protiskluzovou	protiskluzový	k2eAgFnSc7d1	protiskluzová
podlahou	podlaha	k1gFnSc7	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Sedačky	sedačka	k1gFnPc1	sedačka
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
jsou	být	k5eAaImIp3nP	být
čalouněné	čalouněný	k2eAgFnPc1d1	čalouněná
plastové	plastový	k2eAgFnPc1d1	plastová
potažené	potažený	k2eAgFnPc1d1	potažená
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
kočárek	kočárek	k1gInSc4	kočárek
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
u	u	k7c2	u
druhých	druhý	k4xOgFnPc2	druhý
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
bočnici	bočnice	k1gFnSc6	bočnice
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
čtvery	čtvero	k4xRgFnPc1	čtvero
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
dvoje	dvoje	k4xRgFnSc1	dvoje
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
článku	článek	k1gInSc6	článek
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
jsou	být	k5eAaImIp3nP	být
užší	úzký	k2eAgFnPc4d2	užší
než	než	k8xS	než
prostřední	prostřední	k2eAgFnPc4d1	prostřední
dveře	dveře	k1gFnPc4	dveře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
B	B	kA	B
941E	[number]	k4	941E
má	mít	k5eAaImIp3nS	mít
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
přední	přední	k2eAgFnSc4d1	přední
tuhou	tuhý	k2eAgFnSc4d1	tuhá
nápravu	náprava	k1gFnSc4	náprava
LIAZ	liaz	k1gInSc1	liaz
s	s	k7c7	s
kotoučovými	kotoučový	k2eAgFnPc7d1	kotoučová
brzdami	brzda	k1gFnPc7	brzda
<g/>
,	,	kIx,	,
systémy	systém	k1gInPc7	systém
ABS	ABS	kA	ABS
a	a	k8xC	a
ASR	ASR	kA	ASR
<g/>
,	,	kIx,	,
podlahu	podlaha	k1gFnSc4	podlaha
v	v	k7c6	v
předním	přední	k2eAgInSc6d1	přední
článku	článek	k1gInSc6	článek
vozu	vůz	k1gInSc2	vůz
sníženou	snížený	k2eAgFnSc7d1	snížená
o	o	k7c4	o
100	[number]	k4	100
mm	mm	kA	mm
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
menší	malý	k2eAgFnPc4d2	menší
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
a	a	k8xC	a
1998	[number]	k4	1998
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
tři	tři	k4xCgInPc1	tři
prototypy	prototyp	k1gInPc1	prototyp
vozů	vůz	k1gInPc2	vůz
B	B	kA	B
941	[number]	k4	941
označené	označený	k2eAgInPc1d1	označený
jako	jako	k8xC	jako
BK	BK	kA	BK
1	[number]	k4	1
<g/>
,	,	kIx,	,
BK	BK	kA	BK
2	[number]	k4	2
a	a	k8xC	a
BK	BK	kA	BK
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
dodán	dodat	k5eAaPmNgInS	dodat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
s	s	k7c7	s
evidenčním	evidenční	k2eAgNnSc7d1	evidenční
číslem	číslo	k1gNnSc7	číslo
2336	[number]	k4	2336
<g/>
.	.	kIx.	.
<g/>
Výroba	výroba	k1gFnSc1	výroba
městského	městský	k2eAgInSc2d1	městský
kloubového	kloubový	k2eAgInSc2d1	kloubový
autobusu	autobus	k1gInSc2	autobus
řady	řada	k1gFnSc2	řada
900	[number]	k4	900
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
rok	rok	k1gInSc4	rok
po	po	k7c4	po
ukončení	ukončení	k1gNnSc4	ukončení
produkce	produkce	k1gFnSc2	produkce
vozů	vůz	k1gInPc2	vůz
B	B	kA	B
741	[number]	k4	741
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
pak	pak	k6eAd1	pak
Karosa	karosa	k1gFnSc1	karosa
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
pouze	pouze	k6eAd1	pouze
inovovanou	inovovaný	k2eAgFnSc4d1	inovovaná
verzi	verze	k1gFnSc4	verze
B	B	kA	B
941	[number]	k4	941
<g/>
E.	E.	kA	E.
Celkem	celkem	k6eAd1	celkem
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
2001	[number]	k4	2001
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
335	[number]	k4	335
autobusů	autobus	k1gInPc2	autobus
typu	typ	k1gInSc2	typ
B	B	kA	B
941	[number]	k4	941
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
především	především	k9	především
pro	pro	k7c4	pro
dopravní	dopravní	k2eAgInPc4d1	dopravní
podniky	podnik	k1gInPc4	podnik
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
však	však	k9	však
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
postupně	postupně	k6eAd1	postupně
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
věku	věk	k1gInSc3	věk
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
dodávkám	dodávka	k1gFnPc3	dodávka
nových	nový	k2eAgFnPc2d1	nová
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
nízkopodlažních	nízkopodlažní	k2eAgInPc2d1	nízkopodlažní
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
přestaly	přestat	k5eAaPmAgInP	přestat
být	být	k5eAaImF	být
provozovány	provozován	k2eAgInPc1d1	provozován
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
jezdily	jezdit	k5eAaImAgInP	jezdit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Prešově	Prešov	k1gInSc6	Prešov
a	a	k8xC	a
u	u	k7c2	u
několika	několik	k4yIc2	několik
soukromých	soukromý	k2eAgMnPc2d1	soukromý
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podtypy	podtyp	k1gInPc4	podtyp
==	==	k?	==
</s>
</p>
<p>
<s>
Realizované	realizovaný	k2eAgInPc1d1	realizovaný
podtypy	podtyp	k1gInPc1	podtyp
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941.193	[number]	k4	941.193
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
BK	BK	kA	BK
1	[number]	k4	1
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
Voith	Voith	k1gMnSc1	Voith
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941.193	[number]	k4	941.193
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
ZF	ZF	kA	ZF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941.193	[number]	k4	941.193
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
Renault	renault	k1gInSc1	renault
186	[number]	k4	186
kW	kW	kA	kW
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
ZF	ZF	kA	ZF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941.193	[number]	k4	941.193
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
BK	BK	kA	BK
2	[number]	k4	2
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
Voith	Voith	k1gMnSc1	Voith
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941.196	[number]	k4	941.196
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
BK	BK	kA	BK
2	[number]	k4	2
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
Voith	Voith	k1gMnSc1	Voith
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
<g/>
E	E	kA	E
<g/>
.1956	.1956	k4	.1956
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
BK	BK	kA	BK
3	[number]	k4	3
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
ZF	ZF	kA	ZF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
<g/>
E	E	kA	E
<g/>
.1962	.1962	k4	.1962
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
Voith	Voith	k1gMnSc1	Voith
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
<g/>
E	E	kA	E
<g/>
.1964	.1964	k4	.1964
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
Renault	renault	k1gInSc1	renault
217	[number]	k4	217
kW	kW	kA	kW
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
Voith	Voith	k1gMnSc1	Voith
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
<g/>
E	E	kA	E
<g/>
.1966	.1966	k4	.1966
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
1999	[number]	k4	1999
<g/>
;	;	kIx,	;
motor	motor	k1gInSc1	motor
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
převodovka	převodovka	k1gFnSc1	převodovka
ZF	ZF	kA	ZF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
vozy	vůz	k1gInPc1	vůz
==	==	k?	==
</s>
</p>
<p>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
vůz	vůz	k1gInSc1	vůz
ev.	ev.	k?	ev.
č.	č.	k?	č.
374	[number]	k4	374
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
osoba	osoba	k1gFnSc1	osoba
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
DP	DP	kA	DP
Olomouc	Olomouc	k1gFnSc1	Olomouc
ev.	ev.	k?	ev.
č.	č.	k?	č.
314	[number]	k4	314
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
soukromá	soukromý	k2eAgFnSc1d1	soukromá
osoba	osoba	k1gFnSc1	osoba
(	(	kIx(	(
<g/>
ex	ex	k6eAd1	ex
DP	DP	kA	DP
Olomouc	Olomouc	k1gFnSc1	Olomouc
ev.	ev.	k?	ev.
č.	č.	k?	č.
315	[number]	k4	315
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČUMA	Čuma	k1gMnSc1	Čuma
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
;	;	kIx,	;
KOCMAN	Kocman	k1gMnSc1	Kocman
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
;	;	kIx,	;
MRKOS	Mrkos	k1gMnSc1	Mrkos
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
městské	městský	k2eAgFnSc6d1	městská
dopravě	doprava	k1gFnSc6	doprava
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Malkus	Malkus	k1gMnSc1	Malkus
–	–	k?	–
dopravní	dopravní	k2eAgNnSc1d1	dopravní
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
296	[number]	k4	296
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903012	[number]	k4	903012
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
a	a	k8xC	a
B	B	kA	B
941	[number]	k4	941
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
prahamhd	prahamhd	k1gInSc1	prahamhd
<g/>
.	.	kIx.	.
<g/>
vhd	vhd	k?	vhd
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
