<s>
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
válce	válka	k1gFnSc6
mezi	mezi	k7c7
Izraelem	Izrael	k1gInSc7
a	a	k8xC
několika	několik	k4yIc7
arabskými	arabský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
válce	válka	k1gFnSc6
mezi	mezi	k7c7
Gruzií	Gruzie	k1gFnSc7
a	a	k8xC
Abcházií	Abcházie	k1gFnSc7
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Válka	válka	k1gFnSc1
v	v	k7c6
Abcházii	Abcházie	k1gFnSc6
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Arabsko-izraelské	arabsko-izraelský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Příslušníci	příslušník	k1gMnPc1
Izraelské	izraelský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
si	se	k3xPyFc3
prohlížejí	prohlížet	k5eAaImIp3nP
trosky	troska	k1gFnPc4
arabského	arabský	k2eAgInSc2d1
MiGu-	MiGu-	k1gMnSc1
<g/>
21	#num#	k4
zničeného	zničený	k2eAgMnSc2d1
na	na	k7c6
letišti	letiště	k1gNnSc6
Bir	Bir	k1gMnSc2
Gifgafa	Gifgaf	k1gMnSc2
na	na	k7c6
Sinajském	sinajský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1
blokáda	blokáda	k1gFnSc1
Tiranské	tiranský	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
<g/>
,	,	kIx,
přesun	přesun	k1gInSc4
arabských	arabský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c4
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
,	,	kIx,
vypovězení	vypovězení	k1gNnSc1
jednotek	jednotka	k1gFnPc2
OSN	OSN	kA
<g/>
,	,	kIx,
arabský	arabský	k2eAgInSc1d1
pokus	pokus	k1gInSc1
zničit	zničit	k5eAaPmF
Izrael	Izrael	k1gInSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Rozhodující	rozhodující	k2eAgNnSc1d1
izraelské	izraelský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Izrael	Izrael	k1gInSc1
získal	získat	k5eAaPmAgInS
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
<g/>
,	,	kIx,
Sinajský	sinajský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
<g/>
,	,	kIx,
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
(	(	kIx(
<g/>
i	i	k8xC
východní	východní	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Golanské	Golanský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
SýrieJordánsko	SýrieJordánsko	k1gNnSc1
JordánskoArabské	jordánskoarabský	k2eAgFnSc2d1
expediční	expediční	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Irák	Irák	k1gInSc1
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
Maroko	Maroko	k1gNnSc1
Alžírsko	Alžírsko	k1gNnSc1
Libye	Libye	k1gFnSc2
Kuvajt	Kuvajt	k1gInSc1
Tunisko	Tunisko	k1gNnSc1
Súdán	Súdán	k1gInSc1
OOP	OOP	kA
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Moše	mocha	k1gFnSc3
Dajan	Dajan	k1gMnSc1
Jicchak	Jicchak	k1gMnSc1
Rabin	Rabin	k1gMnSc1
Ariel	Ariel	k1gMnSc1
Šaron	Šaron	k1gMnSc1
Uzi	Uzi	k1gMnSc1
Narkis	Narkis	k1gFnPc2
Jisra	Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Tal	Tal	k1gFnSc2
Mordechaj	Mordechaj	k1gMnSc1
Hod	hod	k1gInSc1
</s>
<s>
Abdel	Abdel	k1gMnSc1
Hakim	Hakim	k1gMnSc1
Amer	Amer	k1gMnSc1
Abdul	Abdul	k1gMnSc1
Munim	Munim	k1gMnSc1
Riad	Riad	k1gMnSc1
Zaid	Zaid	k1gMnSc1
ibn	ibn	k?
Shaker	Shaker	k1gMnSc1
Háfiz	Háfiz	k1gMnSc1
al-Asad	al-Asad	k6eAd1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
264	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
50	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
pravidelné	pravidelný	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
197	#num#	k4
letadel	letadlo	k1gNnPc2
</s>
<s>
Egypt	Egypt	k1gInSc1
150	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
;	;	kIx,
Sýrie	Sýrie	k1gFnSc1
75	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
;	;	kIx,
Jordánsko	Jordánsko	k1gNnSc4
55	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
;	;	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
;	;	kIx,
812	#num#	k4
letadel	letadlo	k1gNnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
779	#num#	k4
padlých	padlý	k2eAgInPc2d1
<g/>
,2	,2	k4
563	#num#	k4
zraněných	zraněný	k1gMnPc2
<g/>
,15	,15	k4
zajatců	zajatec	k1gMnPc2
<g/>
,19	,19	k4
letadel	letadlo	k1gNnPc2
<g/>
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgFnSc1d1
ztráty	ztráta	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
21	#num#	k4
000	#num#	k4
padlých	padlý	k2eAgInPc2d1
<g/>
,45	,45	k4
000	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
,6	,6	k4
000	#num#	k4
zajatcůpřes	zajatcůpřesa	k1gFnPc2
400	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
(	(	kIx(
<g/>
odhady	odhad	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
מ	מ	k?
ש	ש	k?
ה	ה	k?
<g/>
,	,	kIx,
milchemet	milchemet	k1gMnSc1
šešet	šešet	k1gMnSc1
ha	ha	kA
<g/>
‑	‑	k?
<g/>
jamim	jamim	k1gInSc1
<g/>
,	,	kIx,
resp.	resp.	kA
třetí	třetí	k4xOgFnSc1
arabsko-izraelská	arabsko-izraelský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
bylo	být	k5eAaImAgNnS
vojenské	vojenský	k2eAgNnSc1d1
střetnutí	střetnutí	k1gNnSc1
mezi	mezi	k7c7
Izraelem	Izrael	k1gInSc7
a	a	k8xC
koalicí	koalice	k1gFnSc7
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc2
a	a	k8xC
Jordánska	Jordánsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotkami	jednotka	k1gFnPc7
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
přispěl	přispět	k5eAaPmAgInS
Irák	Irák	k1gInSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
,	,	kIx,
Súdán	Súdán	k1gInSc1
<g/>
,	,	kIx,
Tunisko	Tunisko	k1gNnSc1
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
a	a	k8xC
Alžírsko	Alžírsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Válka	válka	k1gFnSc1
propukla	propuknout	k5eAaPmAgFnS
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
úderem	úder	k1gInSc7
izraelského	izraelský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
proti	proti	k7c3
leteckým	letecký	k2eAgFnPc3d1
základnám	základna	k1gFnPc3
Egypta	Egypt	k1gInSc2
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Izrael	Izrael	k1gInSc1
ukončil	ukončit	k5eAaPmAgInS
bojové	bojový	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabská	arabský	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
utrpěla	utrpět	k5eAaPmAgFnS
zdrcující	zdrcující	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
–	–	k?
Egypt	Egypt	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
Gazu	Gaza	k1gFnSc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
Sinaj	Sinaj	k1gInSc4
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Sýrie	Sýrie	k1gFnSc2
Golanské	Golanský	k2eAgFnSc2d1
výšiny	výšina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
války	válka	k1gFnSc2
ovlivnil	ovlivnit	k5eAaPmAgInS
geopolitiku	geopolitika	k1gFnSc4
regionu	region	k1gInSc2
až	až	k9
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
četných	četný	k2eAgInPc6d1
hraničních	hraniční	k2eAgInPc6d1
střetech	střet	k1gInPc6
mezi	mezi	k7c7
Izraelem	Izrael	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnPc7
arabskými	arabský	k2eAgMnPc7d1
sousedy	soused	k1gMnPc7
<g/>
,	,	kIx,
zejména	zejména	k9
Sýrií	Sýrie	k1gFnSc7
<g/>
,	,	kIx,
vyzval	vyzvat	k5eAaPmAgMnS
egyptský	egyptský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Gamál	Gamál	k1gMnSc1
Násir	Násir	k1gMnSc1
v	v	k7c6
květnu	květen	k1gInSc6
1967	#num#	k4
k	k	k7c3
odchodu	odchod	k1gInSc3
jednotky	jednotka	k1gFnSc2
OSN	OSN	kA
ze	z	k7c2
Sinaje	Sinaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírové	mírový	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
zde	zde	k6eAd1
byly	být	k5eAaImAgInP
umístěny	umístit	k5eAaPmNgInP
od	od	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
po	po	k7c6
britsko-francouzsko-izraelském	britsko-francouzsko-izraelský	k2eAgInSc6d1
útoku	útok	k1gInSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
ovládnout	ovládnout	k5eAaPmF
Egyptem	Egypt	k1gInSc7
znárodněný	znárodněný	k2eAgInSc4d1
Suezský	suezský	k2eAgInSc4d1
průplav	průplav	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
válce	válka	k1gFnSc3
známé	známý	k2eAgFnSc3d1
jako	jako	k8xC,k8xS
Suezská	suezský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odchodu	odchod	k1gInSc6
mírových	mírový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
shromáždil	shromáždit	k5eAaPmAgInS
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c2
podpory	podpora	k1gFnSc2
ostatních	ostatní	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Izraelem	Izrael	k1gInSc7
na	na	k7c4
1000	#num#	k4
tanků	tank	k1gInPc2
a	a	k8xC
téměř	téměř	k6eAd1
100	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgInS
Tiranskou	tiranský	k2eAgFnSc4d1
úžinu	úžina	k1gFnSc4
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
lodě	loď	k1gFnPc4
plující	plující	k2eAgFnPc4d1
pod	pod	k7c7
izraelskou	izraelský	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
<g/>
,	,	kIx,
či	či	k8xC
přepravující	přepravující	k2eAgInSc4d1
strategický	strategický	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
vyhlásí	vyhlásit	k5eAaPmIp3nS
mobilizaci	mobilizace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
uzavření	uzavření	k1gNnSc1
Tiranské	tiranský	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
bude	být	k5eAaImBp3nS
považovat	považovat	k5eAaImF
za	za	k7c4
válečný	válečný	k2eAgInSc4d1
akt	akt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
služby	služba	k1gFnSc2
bylo	být	k5eAaImAgNnS
povoláno	povolat	k5eAaPmNgNnS
na	na	k7c4
70	#num#	k4
000	#num#	k4
rezervistů	rezervista	k1gMnPc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
Izrael	Izrael	k1gInSc1
spustil	spustit	k5eAaPmAgInS
preemptivní	preemptivní	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordánsko	Jordánsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
s	s	k7c7
Egyptem	Egypt	k1gInSc7
podepsalo	podepsat	k5eAaPmAgNnS
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
<g/>
,	,	kIx,
následně	následně	k6eAd1
zaútočilo	zaútočit	k5eAaPmAgNnS
na	na	k7c4
západní	západní	k2eAgInSc4d1
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
Netanju	Netanju	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
k	k	k7c3
válce	válka	k1gFnSc3
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Izrael	Izrael	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nacházel	nacházet	k5eAaImAgMnS
z	z	k7c2
vojensko-obranného	vojensko-obranný	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
v	v	k7c6
beznadějné	beznadějný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranici	hranice	k1gFnSc4
se	s	k7c7
Sýrií	Sýrie	k1gFnSc7
tvořily	tvořit	k5eAaImAgFnP
Golanské	Golanský	k2eAgFnPc1d1
výšiny	výšina	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
jejichž	jejichž	k3xOyRp3gInPc2
vrcholů	vrchol	k1gInPc2
mohlo	moct	k5eAaImAgNnS
syrské	syrský	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
bez	bez	k7c2
omezení	omezení	k1gNnSc2
ostřelovat	ostřelovat	k5eAaImF
osady	osada	k1gFnPc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syrsko-izraelská	syrsko-izraelský	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
byla	být	k5eAaImAgFnS
zároveň	zároveň	k6eAd1
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
kudy	kudy	k6eAd1
do	do	k7c2
Izraele	Izrael	k1gInSc2
pronikali	pronikat	k5eAaImAgMnP
radikální	radikální	k2eAgMnPc1d1
palestinští	palestinský	k2eAgMnPc1d1
ozbrojenci	ozbrojenec	k1gMnPc1
z	z	k7c2
Fatahu	Fatah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespokojenost	nespokojenost	k1gFnSc1
ze	z	k7c2
syrské	syrský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byla	být	k5eAaImAgFnS
zejména	zejména	k9
s	s	k7c7
dobudováním	dobudování	k1gNnSc7
izraelského	izraelský	k2eAgInSc2d1
zavlažovacího	zavlažovací	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
Národní	národní	k2eAgInSc1d1
rozvaděč	rozvaděč	k1gInSc1
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
odváděl	odvádět	k5eAaImAgMnS
vodu	voda	k1gFnSc4
z	z	k7c2
Galilejského	galilejský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
problematickou	problematický	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
byla	být	k5eAaImAgFnS
hranice	hranice	k1gFnSc1
izraelsko-jordánská	izraelsko-jordánský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
izraelské	izraelský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
a	a	k8xC
polovinu	polovina	k1gFnSc4
ovládalo	ovládat	k5eAaImAgNnS
právě	právě	k9
Jordánsko	Jordánsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
byl	být	k5eAaImAgInS
Jeruzalém	Jeruzalém	k1gInSc1
tzv.	tzv.	kA
demilitarizovanou	demilitarizovaný	k2eAgFnSc7d1
zónou	zóna	k1gFnSc7
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zde	zde	k6eAd1
přítomní	přítomný	k2eAgMnPc1d1
jak	jak	k8xC,k8xS
izraelští	izraelský	k2eAgMnPc1d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
jordánští	jordánský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Relativně	relativně	k6eAd1
nejbezpečnější	bezpečný	k2eAgFnSc4d3
hranici	hranice	k1gFnSc4
měl	mít	k5eAaImAgInS
Izrael	Izrael	k1gInSc1
s	s	k7c7
Egyptem	Egypt	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c6
egyptské	egyptský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
hranice	hranice	k1gFnPc4
byly	být	k5eAaImAgFnP
od	od	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
rozmístěny	rozmístěn	k2eAgFnPc1d1
pohotovostní	pohotovostní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
OSN	OSN	kA
(	(	kIx(
<g/>
UNEF	UNEF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
geopolitického	geopolitický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
byla	být	k5eAaImAgFnS
nejbližším	blízký	k2eAgMnSc7d3
izraelským	izraelský	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
Izraeli	Izrael	k1gInSc6
poskytovala	poskytovat	k5eAaImAgFnS
hospodářskou	hospodářský	k2eAgFnSc4d1
a	a	k8xC
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Izrael	Izrael	k1gInSc1
na	na	k7c4
sebe	sebe	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
bojů	boj	k1gInPc2
Francie	Francie	k1gFnSc2
proti	proti	k7c3
alžírským	alžírský	k2eAgFnPc3d1
snahám	snaha	k1gFnPc3
o	o	k7c4
osamostatnění	osamostatnění	k1gNnSc4
vázal	vázat	k5eAaImAgMnS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
arabských	arabský	k2eAgFnPc2d1
armád	armáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
opětovném	opětovný	k2eAgInSc6d1
nástupu	nástup	k1gInSc6
de	de	k?
Gaulla	Gaullo	k1gNnSc2
k	k	k7c3
moci	moc	k1gFnSc3
a	a	k8xC
zahájení	zahájení	k1gNnSc6
francouzsko-arabského	francouzsko-arabský	k2eAgNnSc2d1
sbližování	sbližování	k1gNnSc2
ale	ale	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
ochlazení	ochlazení	k1gNnSc3
francouzsko-izraelských	francouzsko-izraelský	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Arabské	arabský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
sousedících	sousedící	k2eAgInPc2d1
s	s	k7c7
Izraelem	Izrael	k1gInSc7
byl	být	k5eAaImAgMnS
egyptský	egyptský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Násir	Násir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
již	již	k9
od	od	k7c2
Suezské	suezský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
jednotné	jednotný	k2eAgFnSc2d1
protiizraelské	protiizraelský	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
Sýrií	Sýrie	k1gFnSc7
a	a	k8xC
Egyptem	Egypt	k1gInSc7
byly	být	k5eAaImAgFnP
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
rozporuplné	rozporuplný	k2eAgNnSc1d1
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
vystoupením	vystoupení	k1gNnSc7
Sýrie	Sýrie	k1gFnSc2
ze	z	k7c2
Sjednocené	sjednocený	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
syrská	syrský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Násirovi	Násir	k1gMnSc3
nedůvěřovala	důvěřovat	k5eNaImAgFnS
<g/>
;	;	kIx,
problémem	problém	k1gInSc7
bylo	být	k5eAaImAgNnS
zejména	zejména	k9
založení	založení	k1gNnSc1
OOP	OOP	kA
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
v	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
díky	díky	k7c3
níž	jenž	k3xRgFnSc3
<g/>
,	,	kIx,
dle	dle	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
ovládal	ovládat	k5eAaImAgInS
Palestince	Palestinec	k1gMnPc4
a	a	k8xC
využíval	využívat	k5eAaPmAgMnS,k5eAaImAgMnS
je	být	k5eAaImIp3nS
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
cíle	cíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Sýrie	Sýrie	k1gFnSc1
začala	začít	k5eAaPmAgFnS
podporovat	podporovat	k5eAaImF
LFOP	LFOP	kA
založenou	založený	k2eAgFnSc4d1
též	též	k9
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
egyptsko-syrskému	egyptsko-syrský	k2eAgNnSc3d1
sbližování	sbližování	k1gNnSc3
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc4
země	zem	k1gFnPc4
spojovalo	spojovat	k5eAaImAgNnS
i	i	k9
spojenectví	spojenectví	k1gNnSc4
se	s	k7c7
SSSR	SSSR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1966	#num#	k4
deklarováno	deklarován	k2eAgNnSc1d1
uzavřením	uzavření	k1gNnSc7
egyptsko-syrské	egyptsko-syrský	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
(	(	kIx(
<g/>
díky	díky	k7c3
níž	jenž	k3xRgFnSc3
si	se	k3xPyFc3
Násir	Násir	k1gMnSc1
sliboval	slibovat	k5eAaImAgMnS
větší	veliký	k2eAgInSc4d2
vliv	vliv	k1gInSc4
na	na	k7c4
syrskou	syrský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgMnSc1d1
z	z	k7c2
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc4
<g/>
,	,	kIx,
stálo	stát	k5eAaImAgNnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
víceméně	víceméně	k9
stranou	stranou	k6eAd1
syrsko-egyptského	syrsko-egyptský	k2eAgNnSc2d1
spojenectví	spojenectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordánský	jordánský	k2eAgMnSc1d1
král	král	k1gMnSc1
Husajn	Husajn	k1gMnSc1
I.	I.	kA
netoleroval	tolerovat	k5eNaImAgMnS
útoky	útok	k1gInPc4
Palestinců	Palestinec	k1gMnPc2
proti	proti	k7c3
Izraeli	Izrael	k1gMnSc3
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
ostatním	ostatní	k2eAgFnPc3d1
arabským	arabský	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
nelíbilo	líbit	k5eNaImAgNnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jordánsko	Jordánsko	k1gNnSc1
navíc	navíc	k6eAd1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Sýrie	Sýrie	k1gFnSc2
a	a	k8xC
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
nespolupracovalo	spolupracovat	k5eNaImAgNnS
se	s	k7c7
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
armáda	armáda	k1gFnSc1
používala	používat	k5eAaImAgFnS
západní	západní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
oficiální	oficiální	k2eAgFnSc1d1
jordánská	jordánský	k2eAgFnSc1d1
propaganda	propaganda	k1gFnSc1
říkala	říkat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Násir	Násir	k1gMnSc1
je	být	k5eAaImIp3nS
vůdce	vůdce	k1gMnSc1
arabského	arabský	k2eAgInSc2d1
světa	svět	k1gInSc2
schovaný	schovaný	k2eAgInSc4d1
za	za	k7c2
jednotky	jednotka	k1gFnSc2
UNEF	UNEF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
nechtělo	chtít	k5eNaImAgNnS
Jordánsko	Jordánsko	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
očekávaného	očekávaný	k2eAgInSc2d1
útoku	útok	k1gInSc2
proti	proti	k7c3
Izraeli	Izrael	k1gInSc3
<g/>
,	,	kIx,
zůstat	zůstat	k5eAaPmF
stranou	strana	k1gFnSc7
a	a	k8xC
proto	proto	k8xC
byla	být	k5eAaImAgFnS
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1967	#num#	k4
podepsána	podepsán	k2eAgFnSc1d1
jordánsko-egyptská	jordánsko-egyptský	k2eAgFnSc1d1
obranná	obranný	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
Izrael	Izrael	k1gInSc1
ale	ale	k8xC
s	s	k7c7
účastí	účast	k1gFnSc7
Jordánska	Jordánsko	k1gNnSc2
v	v	k7c6
případné	případný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
nepočítal	počítat	k5eNaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
izraelský	izraelský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Levi	Lev	k1gFnSc2
Eškol	Eškol	k1gInSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Izrael	Izrael	k1gInSc1
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
iniciativy	iniciativa	k1gFnSc2
nedopustí	dopustit	k5eNaPmIp3nS
žádné	žádný	k3yNgFnPc4
nepřátelské	přátelský	k2eNgFnPc4d1
akce	akce	k1gFnPc4
vůči	vůči	k7c3
Jordánsku	Jordánsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nS
však	však	k9
Jordánsko	Jordánsko	k1gNnSc1
zahájilo	zahájit	k5eAaPmAgNnS
válku	válka	k1gFnSc4
proti	proti	k7c3
Izraeli	Izrael	k1gInSc3
<g/>
,	,	kIx,
muselo	muset	k5eAaImAgNnS
by	by	kYmCp3nP
nést	nést	k5eAaImF
všechny	všechen	k3xTgInPc4
důsledky	důsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
koalici	koalice	k1gFnSc3
se	s	k7c7
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
přidal	přidat	k5eAaPmAgMnS
Irák	Irák	k1gInSc4
(	(	kIx(
<g/>
do	do	k7c2
Jordánska	Jordánsko	k1gNnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
irácká	irácký	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
a	a	k8xC
irácké	irácký	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
se	se	k3xPyFc4
soustředilo	soustředit	k5eAaPmAgNnS
u	u	k7c2
jordánsko-irácké	jordánsko-irácký	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
verbálně	verbálně	k6eAd1
Maroko	Maroko	k1gNnSc1
a	a	k8xC
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předehra	předehra	k1gFnSc1
k	k	k7c3
válce	válka	k1gFnSc3
</s>
<s>
Izraelští	izraelský	k2eAgMnPc1d1
civilisté	civilista	k1gMnPc1
hloubí	hloubit	k5eAaImIp3nP
zákopy	zákop	k1gInPc4
poblíž	poblíž	k7c2
Sde	Sde	k1gFnSc2
Warburg	Warburg	k1gInSc1
</s>
<s>
Tiranská	tiranský	k2eAgFnSc1d1
úžina	úžina	k1gFnSc1
</s>
<s>
Přestože	přestože	k8xS
za	za	k7c4
oficiální	oficiální	k2eAgNnSc4d1
datum	datum	k1gNnSc4
zahájení	zahájení	k1gNnSc2
šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1967	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
prvním	první	k4xOgNnPc3
střetům	střet	k1gInPc3
došlo	dojít	k5eAaPmAgNnS
již	již	k9
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Izrael	Izrael	k1gInSc1
odpověděl	odpovědět	k5eAaPmAgInS
na	na	k7c4
syrské	syrský	k2eAgNnSc4d1
ostřelování	ostřelování	k1gNnSc1
leteckým	letecký	k2eAgInSc7d1
úderem	úder	k1gInSc7
proti	proti	k7c3
syrským	syrský	k2eAgInPc3d1
cílům	cíl	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgInSc6
úderu	úder	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
střetům	střet	k1gInPc3
izraelských	izraelský	k2eAgFnPc2d1
a	a	k8xC
syrských	syrský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vzájemných	vzájemný	k2eAgInPc6d1
leteckých	letecký	k2eAgInPc6d1
soubojích	souboj	k1gInPc6
bylo	být	k5eAaImAgNnS
sestřeleno	sestřelit	k5eAaPmNgNnS
šest	šest	k4xCc1
syrských	syrský	k2eAgFnPc2d1
MiG-	MiG-	k1gFnPc2
<g/>
21	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
izraelské	izraelský	k2eAgInPc1d1
stíhací	stíhací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
provedly	provést	k5eAaPmAgInP
demonstrativní	demonstrativní	k2eAgInSc4d1
přelet	přelet	k1gInSc4
nad	nad	k7c7
syrským	syrský	k2eAgNnSc7d1
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Damaškem	Damašek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
incident	incident	k1gInSc1
vyvolal	vyvolat	k5eAaPmAgInS
napětí	napětí	k1gNnSc4
mezi	mezi	k7c7
Sýrií	Sýrie	k1gFnSc7
a	a	k8xC
Egyptem	Egypt	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
Sýrie	Sýrie	k1gFnSc1
obvinila	obvinit	k5eAaPmAgFnS
Egypt	Egypt	k1gInSc4
z	z	k7c2
nedodržení	nedodržení	k1gNnSc2
smlouvy	smlouva	k1gFnSc2
o	o	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
pomoci	pomoc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypt	Egypt	k1gInSc1
nařčení	nařčení	k1gNnSc2
odmítl	odmítnout	k5eAaPmAgInS
a	a	k8xC
odvolal	odvolat	k5eAaPmAgInS
se	se	k3xPyFc4
na	na	k7c6
znění	znění	k1gNnSc6
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
výslovně	výslovně	k6eAd1
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
smluvní	smluvní	k2eAgFnPc1d1
země	zem	k1gFnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
cílem	cíl	k1gInSc7
generálního	generální	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
ale	ale	k8xC
útočníkem	útočník	k1gMnSc7
byla	být	k5eAaImAgFnS
Sýrie	Sýrie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
předala	předat	k5eAaPmAgFnS
Sýrie	Sýrie	k1gFnSc1
Egyptu	Egypt	k1gInSc3
informace	informace	k1gFnSc1
o	o	k7c6
hromadění	hromadění	k1gNnSc6
izraelských	izraelský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c6
syrsko-izraelské	syrsko-izraelský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
sovětskou	sovětský	k2eAgFnSc7d1
rozvědkou	rozvědka	k1gFnSc7
Sýrii	Sýrie	k1gFnSc4
předána	předán	k2eAgFnSc1d1
informace	informace	k1gFnSc1
toto	tento	k3xDgNnSc4
hromadění	hromadění	k1gNnSc4
potvrzující	potvrzující	k2eAgMnPc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
byla	být	k5eAaImAgFnS
zároveň	zároveň	k6eAd1
rezidenturou	rezidentura	k1gFnSc7
KGB	KGB	kA
předána	předat	k5eAaPmNgFnS
i	i	k8xC
Egyptu	Egypt	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
tuto	tento	k3xDgFnSc4
informaci	informace	k1gFnSc4
vzápětí	vzápětí	k6eAd1
popřel	popřít	k5eAaPmAgMnS
(	(	kIx(
<g/>
dokonce	dokonce	k9
pozval	pozvat	k5eAaPmAgMnS
sovětského	sovětský	k2eAgMnSc4d1
velvyslance	velvyslanec	k1gMnSc4
Sergeje	Sergej	k1gMnSc4
Čuvachina	Čuvachin	k1gMnSc4
na	na	k7c4
prohlídku	prohlídka	k1gFnSc4
izraelského	izraelský	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
<g/>
,	,	kIx,
nabídka	nabídka	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
odmítnuta	odmítnout	k5eAaPmNgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byl	být	k5eAaImAgMnS
na	na	k7c4
egyptský	egyptský	k2eAgInSc4d1
popud	popud	k1gInSc4
vyslán	vyslat	k5eAaPmNgMnS
nad	nad	k7c4
syrsko-izraelskou	syrsko-izraelský	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
syrský	syrský	k2eAgInSc4d1
průzkumný	průzkumný	k2eAgInSc4d1
letoun	letoun	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografie	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
pořídil	pořídit	k5eAaPmAgMnS
žádnou	žádný	k3yNgFnSc4
koncentraci	koncentrace	k1gFnSc4
izraelských	izraelský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
nezaznamenaly	zaznamenat	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
náčelník	náčelník	k1gMnSc1
egyptského	egyptský	k2eAgInSc2d1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
generál	generál	k1gMnSc1
Mohammed	Mohammed	k1gMnSc1
Fawzi	Fawze	k1gFnSc4
osobně	osobně	k6eAd1
navštívil	navštívit	k5eAaPmAgMnS
syrsko-izraelsko	syrsko-izraelsko	k6eAd1
hranici	hranice	k1gFnSc4
a	a	k8xC
zvýšenou	zvýšený	k2eAgFnSc4d1
koncentraci	koncentrace	k1gFnSc4
izraelských	izraelský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
také	také	k9
nezaznamenal	zaznamenat	k5eNaPmAgInS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
potvrdil	potvrdit	k5eAaPmAgMnS
i	i	k8xC
americký	americký	k2eAgMnSc1d1
vojenský	vojenský	k2eAgMnSc1d1
atašé	atašé	k1gMnSc1
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
hranici	hranice	k1gFnSc4
navštívil	navštívit	k5eAaPmAgMnS
z	z	k7c2
izraelské	izraelský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
vzniku	vznik	k1gInSc2
dezinformace	dezinformace	k1gFnSc2
kolují	kolovat	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
dohady	dohad	k1gInPc1
<g/>
;	;	kIx,
někteří	některý	k3yIgMnPc1
historici	historik	k1gMnPc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vypustil	vypustit	k5eAaPmAgInS
samotný	samotný	k2eAgInSc1d1
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
eskaloval	eskalovat	k5eAaImAgInS
napětí	napětí	k1gNnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
jiných	jiný	k1gMnPc2
byli	být	k5eAaImAgMnP
autory	autor	k1gMnPc7
Sověti	Sovět	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
do	do	k7c2
počínajícího	počínající	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
hlouběji	hluboko	k6eAd2
zapojit	zapojit	k5eAaPmF
Egypt	Egypt	k1gInSc4
z	z	k7c2
obav	obava	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
pod	pod	k7c7
izraelským	izraelský	k2eAgMnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
tlakem	tlak	k1gInSc7
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
zhroucení	zhroucení	k1gNnSc3
prosovětského	prosovětský	k2eAgInSc2d1
baasistického	baasistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Získáním	získání	k1gNnSc7
zprávy	zpráva	k1gFnSc2
o	o	k7c4
koncentraci	koncentrace	k1gFnSc4
izraelských	izraelský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
o	o	k7c6
síle	síla	k1gFnSc6
10	#num#	k4
-	-	kIx~
12	#num#	k4
brigád	brigáda	k1gFnPc2
<g/>
)	)	kIx)
prezident	prezident	k1gMnSc1
Násir	Násir	k1gMnSc1
zdůvodňoval	zdůvodňovat	k5eAaImAgMnS
vyslání	vyslání	k1gNnSc4
egyptských	egyptský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c4
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pozdějším	pozdní	k2eAgNnSc6d2
vyšetřování	vyšetřování	k1gNnSc6
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
KGB	KGB	kA
skutečně	skutečně	k6eAd1
Egyptu	Egypt	k1gInSc3
zprávu	zpráva	k1gFnSc4
předala	předat	k5eAaPmAgFnS
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
informovala	informovat	k5eAaBmAgFnS
sovětské	sovětský	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
nebylo	být	k5eNaImAgNnS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
předpisy	předpis	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sama	Sam	k1gMnSc2
KGB	KGB	kA
ale	ale	k8xC
zprávu	zpráva	k1gFnSc4
(	(	kIx(
<g/>
i	i	k9
zdroj	zdroj	k1gInSc1
<g/>
)	)	kIx)
z	z	k7c2
Libanonu	Libanon	k1gInSc2
či	či	k8xC
Sýrie	Sýrie	k1gFnSc1
považovala	považovat	k5eAaImAgFnS
za	za	k7c4
nedůvěryhodný	důvěryhodný	k2eNgInSc4d1
(	(	kIx(
<g/>
připouštěla	připouštět	k5eAaImAgFnS
i	i	k9
možnost	možnost	k1gFnSc1
izraelského	izraelský	k2eAgInSc2d1
podvrhu	podvrh	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Násira	Násir	k1gMnSc4
ale	ale	k8xC
získala	získat	k5eAaPmAgFnS
informace	informace	k1gFnPc4
na	na	k7c6
důležitosti	důležitost	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
tu	tu	k6eAd1
samou	samý	k3xTgFnSc4
zprávu	zpráva	k1gFnSc4
obdrželo	obdržet	k5eAaPmAgNnS
egyptské	egyptský	k2eAgNnSc1d1
MZV	MZV	kA
i	i	k9
od	od	k7c2
sovětského	sovětský	k2eAgMnSc2d1
velvyslance	velvyslanec	k1gMnSc2
v	v	k7c6
Káhiře	Káhira	k1gFnSc6
Dimitrije	Dimitrije	k1gMnSc2
Požidajeva	Požidajev	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tu	tu	k6eAd1
samou	samý	k3xTgFnSc4
zprávu	zpráva	k1gFnSc4
následně	následně	k6eAd1
získal	získat	k5eAaPmAgMnS
Násir	Násir	k1gInSc4
i	i	k9
od	od	k7c2
vlastní	vlastní	k2eAgFnSc2d1
parlamentní	parlamentní	k2eAgFnSc2d1
delegace	delegace	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
Anwarem	Anwar	k1gMnSc7
Sadátem	Sadát	k1gMnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
vracela	vracet	k5eAaImAgFnS
z	z	k7c2
návštěvy	návštěva	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
Koreje	Korea	k1gFnSc2
přes	přes	k7c4
Moskvu	Moskva	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
ji	on	k3xPp3gFnSc4
členům	člen	k1gMnPc3
delegace	delegace	k1gFnSc2
poskytl	poskytnout	k5eAaPmAgMnS
náměstek	náměstek	k1gMnSc1
MZV	MZV	kA
SSSR	SSSR	kA
Semjonov	Semjonov	k1gInSc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
zpráva	zpráva	k1gFnSc1
sovětskou	sovětský	k2eAgFnSc4d1
stranou	stranou	k6eAd1
potvrzena	potvrdit	k5eAaPmNgFnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ji	on	k3xPp3gFnSc4
znovu	znovu	k6eAd1
prověřit	prověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
Egypt	Egypt	k1gInSc1
bezpečně	bezpečně	k6eAd1
věděl	vědět	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
koncentrování	koncentrování	k1gNnSc6
izraelských	izraelský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c6
syrsko-izraelské	syrsko-izraelský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
je	být	k5eAaImIp3nS
chybná	chybný	k2eAgFnSc1d1
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
obdržel	obdržet	k5eAaPmAgMnS
od	od	k7c2
sovětských	sovětský	k2eAgMnPc2d1
zpravodajců	zpravodajce	k1gMnPc2
informaci	informace	k1gFnSc4
o	o	k7c6
zvýšené	zvýšený	k2eAgFnSc6d1
koncentraci	koncentrace	k1gFnSc6
izraelských	izraelský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c6
zmíněných	zmíněný	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
citovala	citovat	k5eAaBmAgFnS
agentura	agentura	k1gFnSc1
UPI	UPI	kA
nejmenovaného	nejmenovaný	k1gMnSc2
izraelského	izraelský	k2eAgMnSc2d1
představitele	představitel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
budou	být	k5eAaImBp3nP
syrští	syrský	k2eAgMnPc1d1
teroristé	terorista	k1gMnPc1
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
nájezdech	nájezd	k1gInPc6
a	a	k8xC
sabotážích	sabotáž	k1gFnPc6
proti	proti	k7c3
izraelskému	izraelský	k2eAgNnSc3d1
území	území	k1gNnSc3
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
podnikne	podniknout	k5eAaPmIp3nS
omezenou	omezený	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
vedoucí	vedoucí	k2eAgFnSc4d1
ke	k	k7c3
svržení	svržení	k1gNnSc3
damašského	damašský	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
izraelští	izraelský	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
znovu	znovu	k6eAd1
varovali	varovat	k5eAaImAgMnP
Sýrii	Sýrie	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
provokace	provokace	k1gFnPc4
na	na	k7c6
hranicích	hranice	k1gFnPc6
odpoví	odpovědět	k5eAaPmIp3nS
Izraelské	izraelský	k2eAgFnPc4d1
obranné	obranný	k2eAgFnPc4d1
síly	síla	k1gFnPc4
(	(	kIx(
<g/>
IOS	IOS	kA
<g/>
)	)	kIx)
tvrdě	tvrdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
prohlášení	prohlášení	k1gNnPc4
jsou	být	k5eAaImIp3nP
považována	považován	k2eAgFnSc1d1
za	za	k7c4
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
Násir	Násir	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
rozhodl	rozhodnout	k5eAaPmAgMnS
vyzvat	vyzvat	k5eAaPmF
jednotky	jednotka	k1gFnPc4
OSN	OSN	kA
(	(	kIx(
<g/>
UNEF	UNEF	kA
<g/>
)	)	kIx)
k	k	k7c3
částečnému	částečný	k2eAgNnSc3d1
stažení	stažení	k1gNnSc3
ze	z	k7c2
Sinaje	Sinaj	k1gInSc2
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
Egyptě	Egypt	k1gInSc6
vyhlášen	vyhlásit	k5eAaPmNgInS
mimořádný	mimořádný	k2eAgInSc1d1
stav	stav	k1gInSc1
a	a	k8xC
egyptským	egyptský	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
byl	být	k5eAaImAgInS
vydán	vydán	k2eAgInSc1d1
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
překročení	překročení	k1gNnSc3
Suezu	Suez	k1gInSc2
a	a	k8xC
obsazení	obsazení	k1gNnSc2
Sinaje	Sinaj	k1gInSc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
mobilizace	mobilizace	k1gFnSc1
v	v	k7c6
Jordánsku	Jordánsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgInPc1d1
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
OSN	OSN	kA
U	u	k7c2
Thant	Thanta	k1gFnPc2
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
částečné	částečný	k2eAgNnSc4d1
stažení	stažení	k1gNnSc4
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násir	Násira	k1gFnPc2
proto	proto	k8xC
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
vyzval	vyzvat	k5eAaPmAgMnS
k	k	k7c3
úplnému	úplný	k2eAgNnSc3d1
stažení	stažení	k1gNnSc3
jednotek	jednotka	k1gFnPc2
OSN	OSN	kA
ze	z	k7c2
Sinaje	Sinaj	k1gInSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
to	ten	k3xDgNnSc4
předem	předem	k6eAd1
konzultoval	konzultovat	k5eAaImAgMnS
se	s	k7c7
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
jednotek	jednotka	k1gFnPc2
UNEF	UNEF	kA
zároveň	zároveň	k6eAd1
začali	začít	k5eAaPmAgMnP
obsazovat	obsazovat	k5eAaImF
příslušníci	příslušník	k1gMnPc1
egyptské	egyptský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
Izraeli	Izrael	k1gInSc6
vyhlášena	vyhlásit	k5eAaPmNgFnS
částečná	částečný	k2eAgFnSc1d1
mobilizace	mobilizace	k1gFnSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
egyptská	egyptský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
obsadila	obsadit	k5eAaPmAgFnS
Šarm	šarm	k1gInSc4
aš	aš	k?
Šajch	šajch	k1gMnSc1
a	a	k8xC
den	den	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
Egypt	Egypt	k1gInSc1
uzavřel	uzavřít	k5eAaPmAgInS
Tiranskou	tiranský	k2eAgFnSc4d1
úžinu	úžina	k1gFnSc4
pro	pro	k7c4
izraelské	izraelský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
(	(	kIx(
<g/>
a	a	k8xC
zároveň	zároveň	k6eAd1
pro	pro	k7c4
neizraelské	izraelský	k2eNgInPc4d1
dopravující	dopravující	k2eAgInPc4d1
do	do	k7c2
Izraele	Izrael	k1gInSc2
strategický	strategický	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
vstup	vstup	k1gInSc1
do	do	k7c2
Akabského	Akabský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
odříznut	odříznut	k2eAgInSc1d1
od	od	k7c2
hlavních	hlavní	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odříznutím	odříznutí	k1gNnSc7
přístavu	přístav	k1gInSc2
Ejlat	Ejlat	k1gInSc4
byl	být	k5eAaImAgInS
Izrael	Izrael	k1gInSc1
zároveň	zároveň	k6eAd1
připraven	připravit	k5eAaPmNgInS
o	o	k7c4
přísun	přísun	k1gInSc4
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzavření	uzavření	k1gNnSc1
Tiranské	tiranský	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
bylo	být	k5eAaImAgNnS
aktem	akt	k1gInSc7
agrese	agrese	k1gFnSc2
z	z	k7c2
hlediska	hledisko	k1gNnSc2
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
a	a	k8xC
USA	USA	kA
v	v	k7c6
poselství	poselství	k1gNnSc6
zaslaném	zaslaný	k2eAgNnSc6d1
do	do	k7c2
SSSR	SSSR	kA
upozornily	upozornit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
Izrael	Izrael	k1gInSc1
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
na	na	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izraelské	izraelský	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
s	s	k7c7
dětmi	dítě	k1gFnPc7
v	v	k7c6
protileteckém	protiletecký	k2eAgInSc6d1
krytu	kryt	k1gInSc6
(	(	kIx(
<g/>
osada	osada	k1gFnSc1
Kfar	Kfar	k1gMnSc1
Majmon	Majmon	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
okamžiku	okamžik	k1gInSc6
si	se	k3xPyFc3
žádná	žádný	k3yNgFnSc1
ze	z	k7c2
stran	strana	k1gFnPc2
nepřála	přát	k5eNaImAgFnS
otevřenou	otevřený	k2eAgFnSc4d1
válku	válka	k1gFnSc4
(	(	kIx(
<g/>
Egypt	Egypt	k1gInSc1
Izraeli	Izrael	k1gInSc6
nabídl	nabídnout	k5eAaPmAgInS
obnovení	obnovení	k1gNnSc4
gentlemanské	gentlemanský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
by	by	kYmCp3nS
izraelským	izraelský	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
při	při	k7c6
plavbě	plavba	k1gFnSc6
Tiranskou	tiranský	k2eAgFnSc7d1
úžinou	úžina	k1gFnSc7
stáhly	stáhnout	k5eAaPmAgInP
izraelskou	izraelský	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
a	a	k8xC
vyvěsily	vyvěsit	k5eAaPmAgFnP
vlajku	vlajka	k1gFnSc4
neutrálního	neutrální	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
umožnil	umožnit	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
proplout	proplout	k5eAaPmF
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
však	však	k9
Izrael	Izrael	k1gInSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Nepřál	přát	k5eNaImAgMnS
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
Násir	Násir	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
třetina	třetina	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
armády	armáda	k1gFnSc2
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
Jemenu	Jemen	k1gInSc6
(	(	kIx(
<g/>
kde	kde	k6eAd1
podporovala	podporovat	k5eAaImAgFnS
republikány	republikán	k1gMnPc4
v	v	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
(	(	kIx(
<g/>
politické	politický	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
státu	stát	k1gInSc2
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
čele	čelo	k1gNnSc6
s	s	k7c7
L.	L.	kA
Eškolem	Eškol	k1gInSc7
preferovalo	preferovat	k5eAaImAgNnS
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
velení	velení	k1gNnSc2
armády	armáda	k1gFnSc2
zdrženlivost	zdrženlivost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
ani	ani	k8xC
SSSR	SSSR	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ačkoliv	ačkoliv	k8xS
chtěl	chtít	k5eAaImAgInS
zatáhnout	zatáhnout	k5eAaPmF
Egypt	Egypt	k1gInSc1
do	do	k7c2
izraelsko-syrských	izraelsko-syrský	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
obával	obávat	k5eAaImAgMnS
se	se	k3xPyFc4
přímé	přímý	k2eAgFnSc2d1
konfrontace	konfrontace	k1gFnSc2
s	s	k7c7
USA	USA	kA
a	a	k8xC
ztráty	ztráta	k1gFnPc4
vojenského	vojenský	k2eAgMnSc2d1
a	a	k8xC
politického	politický	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
v	v	k7c6
regionu	region	k1gInSc6
v	v	k7c6
případě	případ	k1gInSc6
pádu	pád	k1gInSc2
spřátelených	spřátelený	k2eAgInPc2d1
režimů	režim	k1gInPc2
v	v	k7c6
arabských	arabský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Uzavření	uzavření	k1gNnSc1
Tiranské	tiranský	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
Izrael	Izrael	k1gInSc1
použil	použít	k5eAaPmAgMnS
jako	jako	k9
casus	casus	k1gMnSc1
belli	bell	k1gMnSc3
pro	pro	k7c4
pozdější	pozdní	k2eAgFnPc4d2
vojenské	vojenský	k2eAgFnPc4d1
akce	akce	k1gFnPc4
(	(	kIx(
<g/>
Egypt	Egypt	k1gInSc1
byl	být	k5eAaImAgMnS
tímto	tento	k3xDgInSc7
aktem	akt	k1gInSc7
klasifikován	klasifikovat	k5eAaImNgInS
jako	jako	k9
agresor	agresor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypt	Egypt	k1gInSc1
jednal	jednat	k5eAaImAgInS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
rezolucí	rezoluce	k1gFnPc2
OSN	OSN	kA
z	z	k7c2
roku	rok	k1gInSc2
1957	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
zakotveno	zakotvit	k5eAaPmNgNnS
právo	právo	k1gNnSc1
Izraele	Izrael	k1gInSc2
na	na	k7c6
používání	používání	k1gNnSc6
úžiny	úžina	k1gFnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
Úmluvou	úmluva	k1gFnSc7
o	o	k7c6
pobřežních	pobřežní	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
a	a	k8xC
pobřežních	pobřežní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
byla	být	k5eAaImAgNnP
přijata	přijmout	k5eAaPmNgNnP
na	na	k7c6
konferenci	konference	k1gFnSc6
o	o	k7c6
mořském	mořský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypt	Egypt	k1gInSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
řídil	řídit	k5eAaImAgInS
podmínkou	podmínka	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
dostal	dostat	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
od	od	k7c2
SSSR	SSSR	kA
<g/>
;	;	kIx,
totiž	totiž	k9
že	že	k8xS
podpora	podpora	k1gFnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
arabským	arabský	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
je	být	k5eAaImIp3nS
možná	možná	k9
pouze	pouze	k6eAd1
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
Egypt	Egypt	k1gInSc1
nezaútočí	zaútočit	k5eNaPmIp3nS
první	první	k4xOgNnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
podpora	podpora	k1gFnSc1
agresora	agresor	k1gMnSc2
odporuje	odporovat	k5eAaImIp3nS
zahraniční	zahraniční	k2eAgInSc1d1
politice	politika	k1gFnSc3
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
výkladů	výklad	k1gInPc2
je	být	k5eAaImIp3nS
ale	ale	k9
uzavření	uzavření	k1gNnSc4
Tiranské	tiranský	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
jako	jako	k8xC,k8xS
válečného	válečný	k2eAgInSc2d1
aktu	akt	k1gInSc2
sporné	sporný	k2eAgNnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
Egypt	Egypt	k1gInSc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nestal	stát	k5eNaPmAgMnS
signatářem	signatář	k1gMnSc7
Úmluvy	úmluva	k1gFnSc2
o	o	k7c6
pobřežních	pobřežní	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
a	a	k8xC
pobřežních	pobřežní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
přes	přes	k7c4
přístav	přístav	k1gInSc4
Ejlat	Ejlat	k1gInSc1
šlo	jít	k5eAaImAgNnS
pouze	pouze	k6eAd1
5	#num#	k4
<g/>
%	%	kIx~
izraelského	izraelský	k2eAgInSc2d1
zahraničního	zahraniční	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
během	během	k7c2
let	léto	k1gNnPc2
1966	#num#	k4
-	-	kIx~
67	#num#	k4
neprošla	projít	k5eNaPmAgFnS
Akabským	Akabský	k2eAgInSc7d1
zálivem	záliv	k1gInSc7
ani	ani	k9
jedna	jeden	k4xCgFnSc1
loď	loď	k1gFnSc1
pod	pod	k7c7
izraelskou	izraelský	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
navštívila	navštívit	k5eAaPmAgFnS
syrská	syrský	k2eAgFnSc1d1
delegace	delegace	k1gFnSc1
Moskvu	Moskva	k1gFnSc4
a	a	k8xC
požadovala	požadovat	k5eAaImAgFnS
záruky	záruka	k1gFnPc4
ochrany	ochrana	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nedosáhla	dosáhnout	k5eNaPmAgFnS
ničeho	nic	k3yNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vedení	vedení	k1gNnSc6
SSSR	SSSR	kA
panoval	panovat	k5eAaImAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Arabové	Arab	k1gMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
případném	případný	k2eAgInSc6d1
konfliktu	konflikt	k1gInSc6
bez	bez	k7c2
perspektivy	perspektiva	k1gFnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
si	se	k3xPyFc3
bylo	být	k5eAaImAgNnS
sovětské	sovětský	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
vědomo	vědom	k2eAgNnSc1d1
faktu	faktum	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
USA	USA	kA
své	svůj	k3xOyFgMnPc4
spojence	spojenec	k1gMnPc4
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
nenechá	nechat	k5eNaPmIp3nS
padnout	padnout	k5eAaImF,k5eAaPmF
a	a	k8xC
zasáhne	zasáhnout	k5eAaPmIp3nS
i	i	k9
vojensky	vojensky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
nucen	nutit	k5eAaImNgMnS
také	také	k9
zasáhnout	zasáhnout	k5eAaPmF
<g/>
,	,	kIx,
čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
konflikt	konflikt	k1gInSc1
rozrostl	rozrůst	k5eAaPmAgInS
do	do	k7c2
celosvětové	celosvětový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
názor	názor	k1gInSc1
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
předán	předat	k5eAaPmNgInS
i	i	k9
egyptské	egyptský	k2eAgFnSc3d1
delegaci	delegace	k1gFnSc3
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
ministrem	ministr	k1gMnSc7
obrany	obrana	k1gFnSc2
Šamsem	Šams	k1gMnSc7
Badránem	Badrán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
oficiálním	oficiální	k2eAgNnSc7d1
stanoviskem	stanovisko	k1gNnSc7
politického	politický	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
SSSR	SSSR	kA
ubezpečili	ubezpečit	k5eAaPmAgMnP
sovětští	sovětský	k2eAgMnPc1d1
vojenští	vojenský	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
maršála	maršál	k1gMnSc4
Grečka	Greček	k1gMnSc4
<g/>
,	,	kIx,
egyptskou	egyptský	k2eAgFnSc4d1
delegaci	delegace	k1gFnSc4
o	o	k7c6
případné	případný	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
podpoře	podpora	k1gFnSc6
SSSR	SSSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poněkud	poněkud	k6eAd1
stranou	strana	k1gFnSc7
sporu	spor	k1gInSc2
stálo	stát	k5eAaImAgNnS
Jordánsko	Jordánsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
mělo	mít	k5eAaImAgNnS
s	s	k7c7
Izraelem	Izrael	k1gInSc7
korektní	korektní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
(	(	kIx(
<g/>
Jordánsko	Jordánsko	k1gNnSc1
zároveň	zároveň	k6eAd1
nepatřilo	patřit	k5eNaImAgNnS
mezi	mezi	k7c4
režimy	režim	k1gInPc4
blízké	blízký	k2eAgFnSc2d1
SSSR	SSSR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
ale	ale	k8xC
jordánská	jordánský	k2eAgFnSc1d1
propaganda	propaganda	k1gFnSc1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
arabskými	arabský	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
kritizovala	kritizovat	k5eAaImAgFnS
Násira	Násira	k1gFnSc1
za	za	k7c2
ustupování	ustupování	k1gNnSc2
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
takže	takže	k8xS
egyptský	egyptský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
byl	být	k5eAaImAgMnS
do	do	k7c2
konfrontace	konfrontace	k1gFnSc2
s	s	k7c7
Izraelem	Izrael	k1gInSc7
tlačen	tlačen	k2eAgInSc1d1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
ztratil	ztratit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
panarabského	panarabský	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
pracně	pracně	k6eAd1
budoval	budovat	k5eAaImAgMnS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
Káhiře	Káhira	k1gFnSc6
proto	proto	k8xC
podepsána	podepsán	k2eAgFnSc1d1
jordánsko-egyptská	jordánsko-egyptský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
o	o	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
a	a	k8xC
jordánská	jordánský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
podřízena	podřídit	k5eAaPmNgFnS
egyptskému	egyptský	k2eAgInSc3d1
velení	velení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
byl	být	k5eAaImAgMnS
na	na	k7c4
post	post	k1gInSc4
izraelského	izraelský	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
Moše	mocha	k1gFnSc3
Dajan	Dajan	k1gMnSc1
<g/>
,	,	kIx,
zastánce	zastánce	k1gMnSc1
politiky	politika	k1gFnSc2
tvrdého	tvrdý	k2eAgInSc2d1
úderu	úder	k1gInSc2
proti	proti	k7c3
Násirovi	Násir	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
do	do	k7c2
vlády	vláda	k1gFnSc2
přizván	přizvat	k5eAaPmNgMnS
i	i	k9
dlouholetý	dlouholetý	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
izraelské	izraelský	k2eAgFnSc2d1
opozice	opozice	k1gFnSc2
Menachem	Menach	k1gInSc7
Begin	Begina	k1gFnPc2
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
izraelská	izraelský	k2eAgFnSc1d1
celonárodní	celonárodní	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diskuse	diskuse	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
koalici	koalice	k1gFnSc6
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
reagovat	reagovat	k5eAaBmF
na	na	k7c4
uzavření	uzavření	k1gNnSc4
Tiranské	tiranský	k2eAgFnSc2d1
úžiny	úžina	k1gFnSc2
vyústily	vyústit	k5eAaPmAgFnP
ve	v	k7c6
vyhlášení	vyhlášení	k1gNnSc6
tzv.	tzv.	kA
Vyčkávacího	vyčkávací	k2eAgNnSc2d1
období	období	k1gNnSc2
(	(	kIx(
<g/>
waiting	waiting	k1gInSc1
period	perioda	k1gFnPc2
<g/>
)	)	kIx)
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
po	po	k7c4
dobu	doba	k1gFnSc4
14	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
chtěl	chtít	k5eAaImAgInS
vyhnout	vyhnout	k5eAaPmF
unáhlené	unáhlený	k2eAgFnSc3d1
vojenské	vojenský	k2eAgFnSc3d1
akci	akce	k1gFnSc3
a	a	k8xC
následné	následný	k2eAgFnSc3d1
protiakci	protiakce	k1gFnSc3
jako	jako	k8xC,k8xS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
při	při	k7c6
Suezské	suezský	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
stály	stát	k5eAaImAgFnP
v	v	k7c6
OSN	OSN	kA
všechny	všechen	k3xTgInPc4
státy	stát	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
USA	USA	kA
proti	proti	k7c3
Izraeli	Izrael	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyčkávací	vyčkávací	k2eAgInSc4d1
období	období	k1gNnSc1
bylo	být	k5eAaImAgNnS
ale	ale	k9
předčasně	předčasně	k6eAd1
ukončeno	ukončit	k5eAaPmNgNnS
na	na	k7c6
zasedání	zasedání	k1gNnSc6
izraelské	izraelský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
v	v	k7c6
noci	noc	k1gFnSc6
ze	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
;	;	kIx,
důvodem	důvod	k1gInSc7
bylo	být	k5eAaImAgNnS
uzavření	uzavření	k1gNnSc1
jordánsko-egyptské	jordánsko-egyptský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
přistoupení	přistoupení	k1gNnSc2
Iráku	Irák	k1gInSc2
do	do	k7c2
arabské	arabský	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
iniciativa	iniciativa	k1gFnSc1
USA	USA	kA
na	na	k7c6
vytvoření	vytvoření	k1gNnSc6
mezinárodní	mezinárodní	k2eAgFnSc2d1
floty	flota	k1gFnSc2
a	a	k8xC
následné	následný	k2eAgNnSc4d1
diplomatické	diplomatický	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
sporu	spor	k1gInSc2
ztroskotala	ztroskotat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
před	před	k7c7
samotným	samotný	k2eAgNnSc7d1
střetnutím	střetnutí	k1gNnSc7
spolu	spolu	k6eAd1
telefonicky	telefonicky	k6eAd1
hovořili	hovořit	k5eAaImAgMnP
egyptský	egyptský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Násir	Násir	k1gMnSc1
a	a	k8xC
jordánský	jordánský	k2eAgMnSc1d1
král	král	k1gMnSc1
Husajn	Husajn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Husajn	Husajn	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
rozhovoru	rozhovor	k1gInSc6
zmínil	zmínit	k5eAaPmAgMnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
očekává	očekávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Izrael	Izrael	k1gInSc1
brzy	brzy	k6eAd1
zahájí	zahájit	k5eAaPmIp3nS
boj	boj	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
nepochybně	pochybně	k6eNd1
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
úder	úder	k1gInSc4
proti	proti	k7c3
egyptskému	egyptský	k2eAgNnSc3d1
letectvu	letectvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násir	Násir	k1gMnSc1
jej	on	k3xPp3gMnSc4
ubezpečil	ubezpečit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
přesně	přesně	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
počítá	počítat	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
připraven	připraven	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypt	Egypt	k1gInSc1
skutečně	skutečně	k6eAd1
očekával	očekávat	k5eAaImAgInS
letecký	letecký	k2eAgInSc1d1
úder	úder	k1gInSc1
Izraele	Izrael	k1gInSc2
a	a	k8xC
podnikl	podniknout	k5eAaPmAgMnS
i	i	k9
určitou	určitý	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
včasné	včasný	k2eAgNnSc4d1
zachycení	zachycení	k1gNnSc4
a	a	k8xC
odražení	odražení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
šlo	jít	k5eAaImAgNnS
o	o	k7c4
amatérská	amatérský	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgNnP
být	být	k5eAaImF
naprosto	naprosto	k6eAd1
neúčinná	účinný	k2eNgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
První	první	k4xOgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
boje	boj	k1gInPc4
o	o	k7c4
Golanské	Golanský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
</s>
<s>
Izraelští	izraelský	k2eAgMnPc1d1
parašutisté	parašutista	k1gMnPc1
při	při	k7c6
střetu	střet	k1gInSc6
s	s	k7c7
jordánskými	jordánský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c4
"	"	kIx"
<g/>
Ammunition	Ammunition	k1gInSc4
Hill	Hilla	k1gFnPc2
<g/>
"	"	kIx"
</s>
<s>
Bojové	bojový	k2eAgFnPc1d1
akce	akce	k1gFnPc1
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
operací	operace	k1gFnSc7
Moked	Moked	k1gMnSc1
–	–	k?
leteckým	letecký	k2eAgInSc7d1
útokem	útok	k1gInSc7
proti	proti	k7c3
Egyptu	Egypt	k1gInSc3
<g/>
,	,	kIx,
Jordánsku	Jordánsko	k1gNnSc3
a	a	k8xC
Sýrii	Sýrie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
sedmou	sedmý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
ranní	ranní	k1gFnSc2
byla	být	k5eAaImAgFnS
skoro	skoro	k6eAd1
všechna	všechen	k3xTgNnPc1
izraelská	izraelský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
(	(	kIx(
<g/>
na	na	k7c4
obranu	obrana	k1gFnSc4
vlastního	vlastní	k2eAgNnSc2d1
území	území	k1gNnSc2
bylo	být	k5eAaImAgNnS
vyčleněno	vyčlenit	k5eAaPmNgNnS
pouhých	pouhý	k2eAgFnPc2d1
12	#num#	k4
stíhaček	stíhačka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letěla	letět	k5eAaImAgFnS
nízko	nízko	k6eAd1
nad	nad	k7c7
hladinou	hladina	k1gFnSc7
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
udržovala	udržovat	k5eAaImAgFnS
úplný	úplný	k2eAgInSc4d1
rádiový	rádiový	k2eAgInSc4d1
klid	klid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvořadé	prvořadý	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
egyptské	egyptský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
překvapit	překvapit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
45	#num#	k4
minutách	minuta	k1gFnPc6
letu	let	k1gInSc2
se	se	k3xPyFc4
letadla	letadlo	k1gNnSc2
dostala	dostat	k5eAaPmAgFnS
k	k	k7c3
cíli	cíl	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
7.45	7.45	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesně	přesně	k6eAd1
chvíle	chvíle	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
egyptské	egyptský	k2eAgFnPc1d1
ranní	ranní	k2eAgFnPc1d1
průzkumné	průzkumný	k2eAgFnPc1d1
hlídky	hlídka	k1gFnPc1
dokončily	dokončit	k5eAaPmAgFnP
své	svůj	k3xOyFgFnSc2
lety	léto	k1gNnPc7
a	a	k8xC
další	další	k2eAgFnSc1d1
směna	směna	k1gFnSc1
ještě	ještě	k6eAd1
neodstartovala	odstartovat	k5eNaPmAgFnS
(	(	kIx(
<g/>
informace	informace	k1gFnPc4
izraelské	izraelský	k2eAgFnSc2d1
špionážní	špionážní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapení	překvapení	k1gNnSc1
bylo	být	k5eAaImAgNnS
dokonalé	dokonalý	k2eAgNnSc1d1
<g/>
.	.	kIx.
419	#num#	k4
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
zničeno	zničit	k5eAaPmNgNnS
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrofa	katastrofa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
potkala	potkat	k5eAaPmAgFnS
arabskou	arabský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
však	však	k9
měla	mít	k5eAaImAgFnS
ještě	ještě	k6eAd1
větší	veliký	k2eAgInSc4d2
rozsah	rozsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násir	Násir	k1gMnSc1
totiž	totiž	k9
podvedl	podvést	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
spojence	spojenec	k1gMnPc4
Jordánsko	Jordánsko	k1gNnSc4
a	a	k8xC
Sýrii	Sýrie	k1gFnSc4
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
útok	útok	k1gInSc1
úspěšně	úspěšně	k6eAd1
odrazil	odrazit	k5eAaPmAgInS
a	a	k8xC
že	že	k8xS
izraelské	izraelský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
utrpělo	utrpět	k5eAaPmAgNnS
těžké	těžký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
státy	stát	k1gInPc1
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
rozhodly	rozhodnout	k5eAaPmAgFnP
zapojit	zapojit	k5eAaPmF
do	do	k7c2
konfliktu	konflikt	k1gInSc2
a	a	k8xC
zahájily	zahájit	k5eAaPmAgInP
nálety	nálet	k1gInPc1
na	na	k7c4
Izrael	Izrael	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
buď	buď	k8xC
s	s	k7c7
těžkými	těžký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
odraženy	odražen	k2eAgMnPc4d1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
mířily	mířit	k5eAaImAgFnP
na	na	k7c4
civilní	civilní	k2eAgInPc4d1
cíle	cíl	k1gInPc4
a	a	k8xC
měly	mít	k5eAaImAgFnP
jen	jen	k9
minimální	minimální	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
následoval	následovat	k5eAaImAgInS
zdrcující	zdrcující	k2eAgInSc1d1
útok	útok	k1gInSc1
na	na	k7c4
syrská	syrský	k2eAgNnPc4d1
a	a	k8xC
jordánská	jordánský	k2eAgNnPc4d1
letiště	letiště	k1gNnSc4
a	a	k8xC
zničení	zničení	k1gNnSc4
prakticky	prakticky	k6eAd1
všech	všecek	k3xTgNnPc2
arabských	arabský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
14.30	14.30	k4
Izrael	Izrael	k1gInSc1
ovládl	ovládnout	k5eAaPmAgInS
vzdušný	vzdušný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
nad	nad	k7c7
celým	celý	k2eAgNnSc7d1
bojištěm	bojiště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
likvidace	likvidace	k1gFnSc2
egyptského	egyptský	k2eAgInSc2d1
<g/>
,	,	kIx,
syrského	syrský	k2eAgMnSc2d1
a	a	k8xC
jordánského	jordánský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
zahájilo	zahájit	k5eAaPmAgNnS
izraelské	izraelský	k2eAgNnSc4d1
pozemní	pozemní	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
postup	postup	k1gInSc4
na	na	k7c4
Sinaj	Sinaj	k1gInSc4
a	a	k8xC
do	do	k7c2
pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
izraelské	izraelský	k2eAgFnSc2d1
obrněné	obrněný	k2eAgFnPc4d1
divize	divize	k1gFnSc2
(	(	kIx(
<g/>
pojmenované	pojmenovaný	k2eAgInPc1d1
podle	podle	k7c2
velitelů	velitel	k1gMnPc2
Tal	Tal	k1gFnSc2
<g/>
,	,	kIx,
Joffe	Joff	k1gInSc5
a	a	k8xC
Šaron	Šarona	k1gFnPc2
<g/>
)	)	kIx)
postupovaly	postupovat	k5eAaImAgFnP
proti	proti	k7c3
sedmi	sedm	k4xCc3
egyptským	egyptský	k2eAgFnPc3d1
divizím	divize	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tankové	tankový	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
generála	generál	k1gMnSc2
Tala	Talus	k1gMnSc2
s	s	k7c7
88	#num#	k4
tanky	tank	k1gInPc1
Centurion	Centurion	k1gInSc1
a	a	k8xC
66	#num#	k4
tanky	tank	k1gInPc7
M48A2C	M48A2C	k1gMnPc2
postupovaly	postupovat	k5eAaImAgFnP
směrem	směr	k1gInSc7
na	na	k7c4
osadu	osada	k1gFnSc4
Chán	chán	k1gMnSc1
Júnis	Júnis	k1gFnPc2
západně	západně	k6eAd1
od	od	k7c2
Gazy	Gaza	k1gFnSc2
a	a	k8xC
po	po	k7c6
půldenních	půldenní	k2eAgInPc6d1
bojích	boj	k1gInPc6
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
hloubky	hloubka	k1gFnSc2
Sinaje	Sinaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
hloubky	hloubka	k1gFnSc2
Sinaje	Sinaj	k1gInSc2
se	se	k3xPyFc4
ve	v	k7c6
večerních	večerní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
dostaly	dostat	k5eAaPmAgFnP
i	i	k8xC
dvě	dva	k4xCgFnPc4
brigády	brigáda	k1gFnPc4
tanků	tank	k1gInPc2
Centurion	Centurion	k1gInSc1
<g/>
,	,	kIx,
Shermann	Shermann	k1gInSc1
a	a	k8xC
Patton	Patton	k1gInSc1
vedených	vedený	k2eAgFnPc2d1
generálem	generál	k1gMnSc7
Šaronem	Šaron	k1gMnSc7
a	a	k8xC
generálem	generál	k1gMnSc7
Joffem	Joff	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
tanková	tankový	k2eAgNnPc1d1
uskupení	uskupení	k1gNnPc1
útočila	útočit	k5eAaImAgNnP
na	na	k7c4
egyptské	egyptský	k2eAgFnPc4d1
opevněné	opevněný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
a	a	k8xC
jednu	jeden	k4xCgFnSc4
po	po	k7c4
druhé	druhý	k4xOgFnSc6
ničila	ničit	k5eAaImAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
křižovatky	křižovatka	k1gFnSc2
Bir	Bir	k1gFnSc2
Lahfan	Lahfana	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
většímu	veliký	k2eAgInSc3d2
střetnutí	střetnutí	k1gNnPc1
izraelských	izraelský	k2eAgMnPc2d1
Centurionů	centurio	k1gMnPc2
a	a	k8xC
egyptských	egyptský	k2eAgFnPc6d1
T-	T-	k1gFnPc6
<g/>
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
pohybu	pohyb	k1gInSc6
svých	svůj	k3xOyFgFnPc2
jednotek	jednotka	k1gFnPc2
po	po	k7c4
celý	celý	k2eAgInSc4d1
první	první	k4xOgInSc4
den	den	k1gInSc4
Izrael	Izrael	k1gMnSc1
mlčel	mlčet	k5eAaImAgMnS
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
egyptský	egyptský	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
vysílal	vysílat	k5eAaImAgInS
zprávy	zpráva	k1gFnPc4
o	o	k7c6
postupu	postup	k1gInSc6
svých	svůj	k3xOyFgFnPc2
jednotek	jednotka	k1gFnPc2
a	a	k8xC
o	o	k7c6
významných	významný	k2eAgNnPc6d1
vítězstvích	vítězství	k1gNnPc6
egyptské	egyptský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
těchto	tento	k3xDgFnPc2
mylných	mylný	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
zahájily	zahájit	k5eAaPmAgInP
své	svůj	k3xOyFgNnSc1
útoky	útok	k1gInPc1
i	i	k8xC
syrská	syrský	k2eAgFnSc1d1
a	a	k8xC
jordánská	jordánský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
očekávajíce	očekávat	k5eAaImSgMnP
podporu	podpora	k1gFnSc4
egyptské	egyptský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
prchala	prchat	k5eAaImAgFnS
ze	z	k7c2
Sinajského	sinajský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syrské	syrský	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
zahájilo	zahájit	k5eAaPmAgNnS
ostřelování	ostřelování	k1gNnSc4
příhraničních	příhraniční	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Izraele	Izrael	k1gInSc2
a	a	k8xC
podniklo	podniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
menších	malý	k2eAgInPc2d2
útoků	útok	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
odraženy	odrazit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvaadvacáté	dvaadvacátý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
byl	být	k5eAaImAgMnS
z	z	k7c2
vrtulníků	vrtulník	k1gInPc2
vysazen	vysazen	k2eAgInSc4d1
prapor	prapor	k1gInSc4
izraelských	izraelský	k2eAgMnPc2d1
výsadkářů	výsadkář	k1gMnPc2
nad	nad	k7c7
egyptským	egyptský	k2eAgNnSc7d1
dělostřeleckým	dělostřelecký	k2eAgNnSc7d1
opevněním	opevnění	k1gNnSc7
Bi	Bi	k1gFnSc2
<g/>
´	´	k?
<g/>
r	r	kA
Džifdžafa	Džifdžaf	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
během	během	k7c2
hodiny	hodina	k1gFnSc2
dobyl	dobýt	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1
a	a	k8xC
jordánská	jordánský	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
byla	být	k5eAaImAgFnS
M.	M.	kA
Dajanem	Dajan	k1gMnSc7
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c7
vedlejší	vedlejší	k2eAgFnSc7d1
a	a	k8xC
prioritou	priorita	k1gFnSc7
zůstala	zůstat	k5eAaPmAgFnS
porážka	porážka	k1gFnSc1
egyptské	egyptský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
USA	USA	kA
se	se	k3xPyFc4
na	na	k7c6
diplomatickém	diplomatický	k2eAgNnSc6d1
poli	pole	k1gNnSc6
snažily	snažit	k5eAaImAgFnP
zajistit	zajistit	k5eAaPmF
neutralitu	neutralita	k1gFnSc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
i	i	k8xC
SSSR	SSSR	kA
uvěřil	uvěřit	k5eAaPmAgMnS
egyptským	egyptský	k2eAgFnPc3d1
zprávám	zpráva	k1gFnPc3
o	o	k7c6
postupu	postup	k1gInSc6
a	a	k8xC
na	na	k7c6
půdě	půda	k1gFnSc6
OSN	OSN	kA
žádal	žádat	k5eAaImAgMnS
nezasahování	nezasahování	k1gNnSc4
do	do	k7c2
konfliktu	konflikt	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dal	dát	k5eAaPmAgMnS
arabským	arabský	k2eAgFnPc3d1
armádám	armáda	k1gFnPc3
čas	čas	k1gInSc1
na	na	k7c4
dokončení	dokončení	k1gNnSc4
vítězství	vítězství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c4
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
zaútočilo	zaútočit	k5eAaPmAgNnS
izraelské	izraelský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
na	na	k7c4
dělostřelecké	dělostřelecký	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
syrské	syrský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c6
Golanských	Golanský	k2eAgFnPc6d1
výšinách	výšina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
poznala	poznat	k5eAaPmAgFnS
Sýrie	Sýrie	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
egyptské	egyptský	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
izraelského	izraelský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
byly	být	k5eAaImAgFnP
nepravdivé	pravdivý	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Izraelské	izraelský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
postupovaly	postupovat	k5eAaImAgFnP
přes	přes	k7c4
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
k	k	k7c3
Suezskému	suezský	k2eAgInSc3d1
průplavu	průplav	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tankové	tankový	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
generála	generál	k1gMnSc2
Tala	Talus	k1gMnSc2
obsadily	obsadit	k5eAaPmAgFnP
egyptské	egyptský	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
al-Aríš	al-Arit	k5eAaImIp2nS,k5eAaBmIp2nS,k5eAaPmIp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
IOS	IOS	kA
dokončily	dokončit	k5eAaPmAgFnP
obklíčení	obklíčení	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odpoledne	odpoledne	k6eAd1
přijala	přijmout	k5eAaPmAgFnS
OSN	OSN	kA
rezoluci	rezoluce	k1gFnSc4
požadující	požadující	k2eAgFnSc7d1
zastavení	zastavení	k1gNnSc4
palby	palba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
Rubinger	Rubinger	k1gMnSc1
<g/>
:	:	kIx,
Výsadkáři	výsadkář	k1gMnPc1
u	u	k7c2
Zdi	zeď	k1gFnSc2
nářků	nářek	k1gInPc2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
</s>
<s>
Ráno	ráno	k6eAd1
po	po	k7c6
těžkých	těžký	k2eAgInPc6d1
bojích	boj	k1gInPc6
vstoupili	vstoupit	k5eAaPmAgMnP
izraelští	izraelský	k2eAgMnPc1d1
výsadkáři	výsadkář	k1gMnPc1
do	do	k7c2
Starého	Starého	k2eAgNnSc2d1
města	město	k1gNnSc2
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
a	a	k8xC
v	v	k7c6
10.15	10.15	k4
se	se	k3xPyFc4
v	v	k7c6
izraelských	izraelský	k2eAgFnPc6d1
rukách	ruka	k1gFnPc6
ocitla	ocitnout	k5eAaPmAgFnS
Chrámová	chrámový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
celý	celý	k2eAgInSc4d1
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
obsadila	obsadit	k5eAaPmAgFnS
celý	celý	k2eAgInSc4d1
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
</s>
<s>
Izraelské	izraelský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
dále	daleko	k6eAd2
obsadily	obsadit	k5eAaPmAgFnP
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Poškozená	poškozený	k2eAgFnSc1d1
loď	loď	k1gFnSc1
USS	USS	kA
Liberty	Libert	k1gInPc4
s	s	k7c7
vrtulníkem	vrtulník	k1gInSc7
americké	americký	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
floty	flota	k1gFnSc2
</s>
<s>
M.	M.	kA
Dajan	Dajan	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
rozkaz	rozkaz	k1gInSc4
k	k	k7c3
obsazení	obsazení	k1gNnSc3
východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Suezského	suezský	k2eAgInSc2d1
průplavu	průplav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Egyptě	Egypt	k1gInSc6
a	a	k8xC
Jordánsku	Jordánsko	k1gNnSc6
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
přijetí	přijetí	k1gNnSc1
rezoluce	rezoluce	k1gFnSc2
požadující	požadující	k2eAgNnSc1d1
zastavení	zastavení	k1gNnSc1
palby	palba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Izraelské	izraelský	k2eAgInPc1d1
vojenské	vojenský	k2eAgInPc1d1
letouny	letoun	k1gInPc1
omylem	omylem	k6eAd1
zaútočily	zaútočit	k5eAaPmAgInP
na	na	k7c4
americkou	americký	k2eAgFnSc4d1
loď	loď	k1gFnSc4
technického	technický	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
USS	USS	kA
Liberty	Libert	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
34	#num#	k4
a	a	k8xC
zraněno	zranit	k5eAaPmNgNnS
117	#num#	k4
amerických	americký	k2eAgMnPc2d1
námořníků	námořník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pátý	pátý	k4xOgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ráno	ráno	k6eAd1
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Golanské	Golanský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
</s>
<s>
Násir	Násir	k1gMnSc1
přijal	přijmout	k5eAaPmAgMnS
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
výsledek	výsledek	k1gInSc4
války	válka	k1gFnSc2
a	a	k8xC
nabídl	nabídnout	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
rezignaci	rezignace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
vypukly	vypuknout	k5eAaPmAgFnP
v	v	k7c6
egyptských	egyptský	k2eAgNnPc6d1
městech	město	k1gNnPc6
demonstrace	demonstrace	k1gFnSc1
žádající	žádající	k2eAgFnSc1d1
jeho	jeho	k3xOp3gNnSc4
setrvání	setrvání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmto	tento	k3xDgInPc3
požadavkům	požadavek	k1gInPc3
Násir	Násir	k1gInSc1
nakonec	nakonec	k6eAd1
vyhověl	vyhovět	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Šestý	šestý	k4xOgInSc1
den	den	k1gInSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přes	přes	k7c4
velké	velký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
ovládl	ovládnout	k5eAaPmAgInS
Izrael	Izrael	k1gInSc1
Golanské	Golanský	k2eAgFnSc2d1
výšiny	výšina	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
obsazena	obsadit	k5eAaPmNgFnS
Kunejtra	Kunejtra	k1gFnSc1
<g/>
,	,	kIx,
nejdůležitější	důležitý	k2eAgNnSc1d3
město	město	k1gNnSc1
tohoto	tento	k3xDgInSc2
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraeli	Izrael	k1gInSc6
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
volná	volný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
směrem	směr	k1gInSc7
na	na	k7c4
Damašek	Damašek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
pohrozil	pohrozit	k5eAaPmAgInS
Izraeli	Izrael	k1gInSc3
vojenskou	vojenský	k2eAgFnSc4d1
intervencí	intervence	k1gFnSc7
pokud	pokud	k8xS
nepřijme	přijmout	k5eNaPmIp3nS
zastavení	zastavení	k1gNnSc4
palby	palba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
doporučily	doporučit	k5eAaPmAgInP
Izraeli	Izrael	k1gInSc6
zastavení	zastavení	k1gNnSc4
palby	palba	k1gFnSc2
přijmout	přijmout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
18.30	18.30	k4
izraelského	izraelský	k2eAgInSc2d1
času	čas	k1gInSc2
oficiálně	oficiálně	k6eAd1
skončila	skončit	k5eAaPmAgFnS
šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Následné	následný	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
akce	akce	k1gFnPc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
obsadil	obsadit	k5eAaPmAgInS
izraelský	izraelský	k2eAgInSc1d1
výsadek	výsadek	k1gInSc1
opuštěnou	opuštěný	k2eAgFnSc4d1
syrskou	syrský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
na	na	k7c6
hoře	hora	k1gFnSc6
Hermon	Hermona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Velmoci	velmoc	k1gFnPc1
během	během	k7c2
války	válka	k1gFnSc2
</s>
<s>
V	v	k7c6
okamžiku	okamžik	k1gInSc6
zahájení	zahájení	k1gNnSc2
bojů	boj	k1gInPc2
měly	mít	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
velmoci	velmoc	k1gFnPc1
<g/>
,	,	kIx,
tedy	tedy	k9
SSSR	SSSR	kA
a	a	k8xC
USA	USA	kA
v	v	k7c6
prostoru	prostor	k1gInSc6
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
přítomny	přítomen	k2eAgFnPc1d1
své	svůj	k3xOyFgFnPc1
námořní	námořní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
vyslal	vyslat	k5eAaPmAgInS
do	do	k7c2
Středomoří	středomoří	k1gNnSc2
eskadru	eskadra	k1gFnSc4
ze	z	k7c2
sestavy	sestava	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
a	a	k8xC
Černomořské	černomořský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
čítající	čítající	k2eAgInSc4d1
40	#num#	k4
lodí	loď	k1gFnPc2
včetně	včetně	k7c2
10	#num#	k4
ponorek	ponorka	k1gFnPc2
a	a	k8xC
doprovodných	doprovodný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
včetně	včetně	k7c2
tankerů	tanker	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
vyslaly	vyslat	k5eAaPmAgInP
eskadru	eskadra	k1gFnSc4
ze	z	k7c2
sestavy	sestava	k1gFnSc2
6	#num#	k4
<g/>
.	.	kIx.
flotily	flotila	k1gFnPc1
čítající	čítající	k2eAgFnPc1d1
2	#num#	k4
letadlové	letadlový	k2eAgFnPc1d1
lodi	loď	k1gFnPc1
(	(	kIx(
<g/>
America	America	k1gFnSc1
a	a	k8xC
Saratoga	Saratoga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
křižníky	křižník	k1gInPc1
<g/>
,	,	kIx,
4	#num#	k4
fregaty	fregata	k1gFnSc2
<g/>
,	,	kIx,
10	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
a	a	k8xC
ponorky	ponorka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgInS
na	na	k7c4
stranu	strana	k1gFnSc4
Egypta	Egypt	k1gInSc2
a	a	k8xC
Sýrie	Sýrie	k1gFnSc2
jako	jako	k8xS,k8xC
obětí	oběť	k1gFnPc2
agrese	agrese	k1gFnSc2
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
přerušil	přerušit	k5eAaPmAgMnS
s	s	k7c7
Izraelem	Izrael	k1gInSc7
veškeré	veškerý	k3xTgInPc4
diplomatické	diplomatický	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
učinily	učinit	k5eAaImAgFnP,k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
země	zem	k1gFnPc1
sovětského	sovětský	k2eAgInSc2d1
bloku	blok	k1gInSc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Rumunska	Rumunsko	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
sovětský	sovětský	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
v	v	k7c6
USA	USA	kA
Alexej	Alexej	k1gMnSc1
Dobrynin	Dobrynin	k2eAgMnSc1d1
hodnotil	hodnotit	k5eAaImAgInS
jako	jako	k9
vážnou	vážný	k2eAgFnSc4d1
chybu	chyba	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
vyloučil	vyloučit	k5eAaPmAgInS
z	z	k7c2
procesu	proces	k1gInSc2
blízkovýchodního	blízkovýchodní	k2eAgNnSc2d1
urovnání	urovnání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Díky	díky	k7c3
Šestidenní	šestidenní	k2eAgFnSc3d1
válce	válka	k1gFnSc3
získal	získat	k5eAaPmAgInS
Izrael	Izrael	k1gInSc4
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
:	:	kIx,
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
(	(	kIx(
<g/>
izraelské	izraelský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
stály	stát	k5eAaImAgFnP
na	na	k7c6
břehu	břeh	k1gInSc6
Suezského	suezský	k2eAgInSc2d1
průplavu	průplav	k1gInSc2
a	a	k8xC
Egypt	Egypt	k1gInSc1
by	by	kYmCp3nS
v	v	k7c6
případě	případ	k1gInSc6
dalšího	další	k2eAgInSc2d1
útoku	útok	k1gInSc2
proti	proti	k7c3
Izraeli	Izrael	k1gInSc3
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
tuto	tento	k3xDgFnSc4
vodní	vodní	k2eAgFnSc4d1
překážku	překážka	k1gFnSc4
překonat	překonat	k5eAaPmF
a	a	k8xC
dobývat	dobývat	k5eAaImF
i	i	k9
Sinajský	sinajský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
<g/>
,	,	kIx,
než	než	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
schopen	schopen	k2eAgInSc1d1
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
jomkipurské	jomkipurský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Golanské	Golanský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
(	(	kIx(
<g/>
skončilo	skončit	k5eAaPmAgNnS
syrské	syrský	k2eAgNnSc1d1
ostřelování	ostřelování	k1gNnSc1
Izraele	Izrael	k1gInSc2
a	a	k8xC
naopak	naopak	k6eAd1
byla	být	k5eAaImAgFnS
Sýrie	Sýrie	k1gFnSc1
z	z	k7c2
Golanských	Golanský	k2eAgFnPc2d1
výšin	výšina	k1gFnPc2
lehce	lehko	k6eAd1
zranitelná	zranitelný	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Zisk	zisk	k1gInSc1
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
byl	být	k5eAaImAgInS
problematický	problematický	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
s	s	k7c7
územím	území	k1gNnSc7
„	„	k?
<g/>
získal	získat	k5eAaPmAgMnS
<g/>
“	“	k?
Izrael	Izrael	k1gInSc4
i	i	k8xC
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
1	#num#	k4
milión	milión	k4xCgInSc4
Arabů	Arab	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Davy	Dav	k1gInPc1
Egypťanů	Egypťan	k1gMnPc2
protestující	protestující	k2eAgInPc1d1
proti	proti	k7c3
Násirově	Násirův	k2eAgFnSc3d1
rezignaci	rezignace	k1gFnSc3
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
všeobecnému	všeobecný	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
důvěry	důvěra	k1gFnSc2
v	v	k7c4
Násirovu	Násirův	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
i	i	k9
k	k	k7c3
poklesu	pokles	k1gInSc3
důvěry	důvěra	k1gFnSc2
v	v	k7c4
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tisku	tisk	k1gInSc6
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nebývalá	bývalý	k2eNgFnSc1d1,k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
vojenského	vojenský	k2eAgNnSc2d1
velení	velení	k1gNnSc2
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
Násir	Násir	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
šestidenní	šestidenní	k2eAgFnSc4d1
válku	válka	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
Egypta	Egypt	k1gInSc2
za	za	k7c4
vyhranou	vyhraná	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
cíle	cíl	k1gInPc1
izraelské	izraelský	k2eAgFnSc2d1
agrese	agrese	k1gFnSc2
<g/>
,	,	kIx,
zničení	zničení	k1gNnSc2
egyptské	egyptský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hospodářského	hospodářský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
byl	být	k5eAaImAgInS
Egypt	Egypt	k1gInSc1
nucen	nucen	k2eAgInSc1d1
se	se	k3xPyFc4
obrátit	obrátit	k5eAaPmF
na	na	k7c4
ostatní	ostatní	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
státy	stát	k1gInPc4
(	(	kIx(
<g/>
zejména	zejména	k9
Saúdskou	saúdský	k2eAgFnSc4d1
Arábii	Arábie	k1gFnSc4
<g/>
)	)	kIx)
s	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
(	(	kIx(
<g/>
z	z	k7c2
důvodu	důvod	k1gInSc2
přítomnosti	přítomnost	k1gFnSc2
izraelské	izraelský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c6
východním	východní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Suezského	suezský	k2eAgInSc2d1
průplavu	průplav	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
zablokování	zablokování	k1gNnSc2
přicházel	přicházet	k5eAaImAgInS
Egypt	Egypt	k1gInSc1
o	o	k7c4
nemalé	malý	k2eNgFnPc4d1
částky	částka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
Egypt	Egypt	k1gInSc1
obrátil	obrátit	k5eAaPmAgInS
na	na	k7c4
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
obdržel	obdržet	k5eAaPmAgMnS
dodávky	dodávka	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
vojenského	vojenský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgMnSc3
byl	být	k5eAaImAgInS
však	však	k9
nucen	nutit	k5eAaImNgMnS
opustit	opustit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
politiku	politika	k1gFnSc4
„	„	k?
<g/>
nezúčastněnosti	nezúčastněnost	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
členem	člen	k1gMnSc7
sovětského	sovětský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
ztráty	ztráta	k1gFnSc2
Golanských	Golanský	k2eAgFnPc2d1
výšin	výšina	k1gFnPc2
a	a	k8xC
v	v	k7c6
podstatě	podstata	k1gFnSc6
likvidace	likvidace	k1gFnSc2
syrského	syrský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vzájemnému	vzájemný	k2eAgNnSc3d1
obviňování	obviňování	k1gNnSc3
mezi	mezi	k7c7
vládnoucí	vládnoucí	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Baas	Baasa	k1gFnPc2
a	a	k8xC
syrskou	syrský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
o	o	k7c6
míře	míra	k1gFnSc6
viny	vina	k1gFnSc2
za	za	k7c4
prohru	prohra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějším	důležitý	k2eAgInSc7d3
argumentem	argument	k1gInSc7
armády	armáda	k1gFnSc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vedení	vedení	k1gNnSc1
státu	stát	k1gInSc2
rozhodlo	rozhodnout	k5eAaPmAgNnS
ponechat	ponechat	k5eAaPmF
nejlepší	dobrý	k2eAgFnPc4d3
jednotky	jednotka	k1gFnPc4
syrské	syrský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c4
obranu	obrana	k1gFnSc4
Damašku	Damašek	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yRnSc2,k3yQnSc2
byly	být	k5eAaImAgInP
Golanské	Golanský	k2eAgInPc1d1
výšiny	výšin	k1gInPc1
hájeny	hájen	k2eAgInPc1d1
hůře	zle	k6eAd2
vyzbrojenými	vyzbrojený	k2eAgFnPc7d1
a	a	k8xC
vycvičenými	vycvičený	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
pro	pro	k7c4
Sýrii	Sýrie	k1gFnSc4
znamenala	znamenat	k5eAaImAgFnS
prohra	prohra	k1gFnSc1
v	v	k7c6
Šestidenní	šestidenní	k2eAgFnSc6d1
válce	válka	k1gFnSc6
ještě	ještě	k9
větší	veliký	k2eAgInSc4d2
příklon	příklon	k1gInSc4
k	k	k7c3
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s>
Velmoci	velmoc	k1gFnPc1
</s>
<s>
Izraelské	izraelský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
znamenalo	znamenat	k5eAaImAgNnS
posílení	posílení	k1gNnSc4
pozice	pozice	k1gFnSc2
USA	USA	kA
v	v	k7c6
regionu	region	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
Izrael	Izrael	k1gInSc1
a	a	k8xC
USA	USA	kA
ještě	ještě	k6eAd1
více	hodně	k6eAd2
upevnily	upevnit	k5eAaPmAgInP
své	svůj	k3xOyFgInPc1
vztahy	vztah	k1gInPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
velmocenským	velmocenský	k2eAgMnSc7d1
garantem	garant	k1gMnSc7
izraelské	izraelský	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
znamenala	znamenat	k5eAaImAgFnS
porážka	porážka	k1gFnSc1
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
prohru	prohra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Izrael	Izrael	k1gInSc1
(	(	kIx(
<g/>
i	i	k8xC
příklonem	příklon	k1gInSc7
k	k	k7c3
USA	USA	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
proti	proti	k7c3
níž	jenž	k3xRgFnSc3
nemá	mít	k5eNaImIp3nS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
žádné	žádný	k3yNgFnSc2
páky	páka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
pokus	pokus	k1gInSc1
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
zničení	zničení	k1gNnSc4
by	by	kYmCp3nS
vedl	vést	k5eAaImAgInS
ke	k	k7c3
konfrontaci	konfrontace	k1gFnSc3
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
války	válka	k1gFnSc2
sice	sice	k8xC
znamenal	znamenat	k5eAaImAgInS
příklon	příklon	k1gInSc1
části	část	k1gFnSc2
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
k	k	k7c3
Sovětskému	sovětský	k2eAgInSc3d1
svazu	svaz	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
představitelé	představitel	k1gMnPc1
SSSR	SSSR	kA
si	se	k3xPyFc3
byli	být	k5eAaImAgMnP
vědomi	vědom	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
vyzbrojují	vyzbrojovat	k5eAaImIp3nP
země	zem	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
z	z	k7c2
politického	politický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
nedokážou	dokázat	k5eNaPmIp3nP
ovládat	ovládat	k5eAaImF
a	a	k8xC
jejichž	jejichž	k3xOyRp3gFnSc1
touha	touha	k1gFnSc1
po	po	k7c6
zničení	zničení	k1gNnSc6
Izraele	Izrael	k1gInSc2
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
vést	vést	k5eAaImF
až	až	k9
k	k	k7c3
nechtěnému	chtěný	k2eNgInSc3d1
americko-sovětskému	americko-sovětský	k2eAgInSc3d1
konfliktu	konflikt	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Poválečná	poválečný	k2eAgNnPc1d1
mírová	mírový	k2eAgNnPc1d1
jednání	jednání	k1gNnPc1
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
mírových	mírový	k2eAgInPc2d1
rozhovorů	rozhovor	k1gInPc2
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgFnP
stěžejními	stěžejní	k2eAgFnPc7d1
dvě	dva	k4xCgFnPc4
události	událost	k1gFnPc4
<g/>
:	:	kIx,
přijetí	přijetí	k1gNnSc4
Rezoluce	rezoluce	k1gFnSc2
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
č.	č.	k?
242	#num#	k4
a	a	k8xC
summit	summit	k1gInSc1
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
Chartúmu	Chartúm	k1gInSc6
v	v	k7c6
srpnu	srpen	k1gInSc6
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezoluce	rezoluce	k1gFnSc1
č.	č.	k?
242	#num#	k4
byla	být	k5eAaImAgFnS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
OSN	OSN	kA
kompromisem	kompromis	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
ukázala	ukázat	k5eAaPmAgFnS
se	se	k3xPyFc4
jako	jako	k9
nepřijatelná	přijatelný	k2eNgNnPc1d1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyklizení	vyklizení	k1gNnSc4
okupovaných	okupovaný	k2eAgNnPc2d1
území	území	k1gNnPc2
podmiňoval	podmiňovat	k5eAaImAgInS
uzavřením	uzavření	k1gNnSc7
mírových	mírový	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
<g/>
,	,	kIx,
rezoluci	rezoluce	k1gFnSc4
odmítal	odmítat	k5eAaImAgMnS
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
ní	on	k3xPp3gFnSc6
ani	ani	k9
jednou	jednou	k9
nebylo	být	k5eNaImAgNnS
zmíněno	zmíněn	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
Izrael	Izrael	k1gInSc1
vyklizení	vyklizení	k1gNnSc2
obsazených	obsazený	k2eAgNnPc2d1
území	území	k1gNnPc2
podmiňoval	podmiňovat	k5eAaImAgInS
bezpečnostními	bezpečnostní	k2eAgFnPc7d1
zárukami	záruka	k1gFnPc7
(	(	kIx(
<g/>
svobodné	svobodný	k2eAgNnSc1d1
proplouvání	proplouvání	k1gNnSc1
Tiranskou	tiranský	k2eAgFnSc7d1
úžinou	úžina	k1gFnSc7
a	a	k8xC
Suezským	suezský	k2eAgInSc7d1
průplavem	průplav	k1gInSc7
a	a	k8xC
nedotknutelnost	nedotknutelnost	k1gFnSc1
hranic	hranice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabské	arabský	k2eAgFnPc4d1
země	zem	k1gFnPc4
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
odmítaly	odmítat	k5eAaImAgFnP
rezoluci	rezoluce	k1gFnSc4
přijmout	přijmout	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
z	z	k7c2
jejího	její	k3xOp3gInSc2
kontextu	kontext	k1gInSc2
vyplývalo	vyplývat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
stran	strana	k1gFnPc2
je	být	k5eAaImIp3nS
právě	právě	k9
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijetí	přijetí	k1gNnSc1
rezoluce	rezoluce	k1gFnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
tedy	tedy	k9
rovnalo	rovnat	k5eAaImAgNnS
uznání	uznání	k1gNnSc1
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
summit	summit	k1gInSc1
arabských	arabský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
Chartúmu	Chartúm	k1gInSc6
výslovně	výslovně	k6eAd1
zakazoval	zakazovat	k5eAaImAgInS
arabským	arabský	k2eAgInPc3d1
státům	stát	k1gInPc3
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
Izraelem	Izrael	k1gInSc7
(	(	kIx(
<g/>
summitu	summit	k1gInSc2
se	se	k3xPyFc4
nezúčastnila	zúčastnit	k5eNaPmAgFnS
Sýrie	Sýrie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1995	#num#	k4
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
izraelský	izraelský	k2eAgInSc4d1
rozhlas	rozhlas	k1gInSc4
obvinil	obvinit	k5eAaPmAgMnS
Arie	Ari	k1gMnSc4
Jicchaki	Jicchaki	k1gNnSc7
z	z	k7c2
Bar-Ilanovy	Bar-Ilanův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
izraelští	izraelský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
z	z	k7c2
průzkumných	průzkumný	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
zabili	zabít	k5eAaPmAgMnP
přibližně	přibližně	k6eAd1
1000	#num#	k4
egyptských	egyptský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jicchaki	Jicchaki	k1gNnPc1
během	během	k7c2
šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
pracoval	pracovat	k5eAaImAgInS
v	v	k7c6
IOS	IOS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozhovoru	rozhovor	k1gInSc6
dále	daleko	k6eAd2
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc4
informace	informace	k1gFnPc4
předložil	předložit	k5eAaPmAgMnS
Jicchaku	Jicchak	k1gMnSc3
Rabinovi	Rabin	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
armádními	armádní	k2eAgFnPc7d1
špičkami	špička	k1gFnPc7
utajil	utajit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
informace	informace	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Jicchaki	Jicchak	k1gFnPc1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
obviněním	obvinění	k1gNnSc7
snaží	snažit	k5eAaImIp3nS
odvést	odvést	k5eAaPmF
pozornost	pozornost	k1gFnSc4
od	od	k7c2
obvinění	obvinění	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgInS
zapleten	zaplést	k5eAaPmNgMnS
do	do	k7c2
zabití	zabití	k1gNnSc2
49	#num#	k4
válečných	válečný	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
během	během	k7c2
suezské	suezský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zprávy	zpráva	k1gFnPc1
o	o	k7c4
zabíjení	zabíjení	k1gNnSc4
egyptských	egyptský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
byly	být	k5eAaImAgInP
prohlášeny	prohlásit	k5eAaPmNgInP
za	za	k7c4
nepodložené	podložený	k2eNgNnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
tisk	tisk	k1gInSc1
následně	následně	k6eAd1
přišel	přijít	k5eAaPmAgInS
se	s	k7c7
svědectvími	svědectví	k1gNnPc7
účastníků	účastník	k1gMnPc2
šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
o	o	k7c6
dalších	další	k2eAgInPc6d1
případech	případ	k1gInPc6
zabíjení	zabíjení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Jicchak	Jicchak	k1gMnSc1
Rabin	Rabin	k1gMnSc1
zprvu	zprvu	k6eAd1
všechny	všechen	k3xTgFnPc4
tyto	tento	k3xDgFnPc4
zprávy	zpráva	k1gFnPc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
později	pozdě	k6eAd2
jeho	jeho	k3xOp3gInSc1
úřad	úřad	k1gInSc1
vydal	vydat	k5eAaPmAgInS
prohlášení	prohlášení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
izolované	izolovaný	k2eAgInPc4d1
incidenty	incident	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenský	vojenský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Uri	Uri	k1gMnSc1
Milstein	Milstein	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
článku	článek	k1gInSc6
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zabíjení	zabíjení	k1gNnSc4
zajatců	zajatec	k1gMnPc2
nebyla	být	k5eNaImAgFnS
oficiální	oficiální	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
armádě	armáda	k1gFnSc6
panovala	panovat	k5eAaImAgFnS
obecná	obecný	k2eAgFnSc1d1
atmosféra	atmosféra	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
zabíjení	zabíjení	k1gNnSc1
zajatců	zajatec	k1gMnPc2
je	být	k5eAaImIp3nS
správné	správný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
velitelé	velitel	k1gMnPc1
to	ten	k3xDgNnSc4
dělali	dělat	k5eAaImAgMnP
<g/>
,	,	kIx,
jiní	jiný	k1gMnPc1
to	ten	k3xDgNnSc4
odmítli	odmítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
věděl	vědět	k5eAaImAgMnS
o	o	k7c6
tom	ten	k3xDgNnSc6
každý	každý	k3xTgMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1995	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
egyptská	egyptský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
nález	nález	k1gInSc4
dvou	dva	k4xCgInPc2
hromadných	hromadný	k2eAgInPc2d1
hrobů	hrob	k1gInPc2
s	s	k7c7
pozůstatky	pozůstatek	k1gInPc7
30-60	30-60	k4
egyptských	egyptský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
zabitých	zabitý	k2eAgFnPc2d1
příslušníky	příslušník	k1gMnPc7
IOS	IOS	kA
na	na	k7c6
Sinaji	Sinaj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupce	zástupce	k1gMnSc1
izraelského	izraelský	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
zahraničí	zahraničí	k1gNnSc2
následně	následně	k6eAd1
jednal	jednat	k5eAaImAgMnS
s	s	k7c7
Egyptem	Egypt	k1gInSc7
o	o	k7c6
finančním	finanční	k2eAgNnSc6d1
odškodnění	odškodnění	k1gNnSc6
příbuzných	příbuzný	k1gMnPc2
zabitých	zabitý	k2eAgInPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
dvacetiletou	dvacetiletý	k2eAgFnSc4d1
promlčecí	promlčecí	k2eAgFnSc4d1
lhůtu	lhůta	k1gFnSc4
platnou	platný	k2eAgFnSc4d1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
odmítl	odmítnout	k5eAaPmAgInS
označit	označit	k5eAaPmF
viníky	viník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Kapitáni	kapitán	k1gMnPc1
Zorc	Zorc	k1gFnSc1
a	a	k8xC
Stočič	Stočič	k1gMnSc1
<g/>
,	,	kIx,
bývalí	bývalý	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
jugoslávského	jugoslávský	k2eAgInSc2d1
kontingentu	kontingent	k1gInSc2
v	v	k7c6
jednotkách	jednotka	k1gFnPc6
UNEF	UNEF	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
tvořil	tvořit	k5eAaImAgInS
nárazník	nárazník	k1gInSc4
mezi	mezi	k7c7
izraelskými	izraelský	k2eAgFnPc7d1
a	a	k8xC
egyptskými	egyptský	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
,	,	kIx,
shodně	shodně	k6eAd1
zpochybnili	zpochybnit	k5eAaPmAgMnP
tvrzení	tvrzení	k1gNnSc4
o	o	k7c6
popravě	poprava	k1gFnSc6
250	#num#	k4
egyptských	egyptský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdělili	sdělit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
by	by	kYmCp3nS
k	k	k7c3
takové	takový	k3xDgFnSc3
akci	akce	k1gFnSc3
v	v	k7c6
prostoru	prostor	k1gInSc6
umístění	umístění	k1gNnSc2
jugoslávské	jugoslávský	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
by	by	kYmCp3nP
o	o	k7c6
tom	ten	k3xDgNnSc6
vědět	vědět	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izraelský	izraelský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Matitjahu	Matitjah	k1gInSc2
Peled	Peled	k1gMnSc1
pět	pět	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1972	#num#	k4
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Teze	teze	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
červnu	červen	k1gInSc6
1967	#num#	k4
nad	nad	k7c7
námi	my	k3xPp1nPc7
viselo	viset	k5eAaImAgNnS
nebezpečí	nebezpečí	k1gNnSc3
genocidy	genocida	k1gFnSc2
a	a	k8xC
že	že	k8xS
Izrael	Izrael	k1gInSc1
bojoval	bojovat	k5eAaImAgInS
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
fyzickou	fyzický	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bluf	bluf	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
zrodil	zrodit	k5eAaPmAgInS
a	a	k8xC
vyvinul	vyvinout	k5eAaPmAgInS
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Six-Day	Six-Daa	k1gMnSc2
War	War	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
KRAUTHAMMER	KRAUTHAMMER	kA
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prelude	Prelud	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Six	Six	k1gMnSc4
Days	Days	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
A	a	k8xC
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
740	#num#	k4
<g/>
-	-	kIx~
<g/>
5421	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pimlott	Pimlott	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
Conflicts	Conflicts	k1gInSc1
<g/>
:	:	kIx,
From	From	k1gInSc1
1945	#num#	k4
to	ten	k3xDgNnSc1
the	the	k?
Present	Present	k1gInSc1
<g/>
,	,	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85613	#num#	k4
<g/>
-	-	kIx~
<g/>
547	#num#	k4
<g/>
-X	-X	k?
<g/>
,	,	kIx,
p.	p.	k?
53	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Six-Day	Six-Daa	k1gMnSc2
War	War	k1gMnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
Encarta	Encarta	k1gFnSc1
Online	Onlin	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnPc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
URL	URL	kA
accessed	accessed	k1gInSc1
April	April	k1gInSc1
10	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GLOSY	glosa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
INFO	INFO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geopolitický	geopolitický	k2eAgInSc1d1
kontext	kontext	k1gInSc1
šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Glosy	glosa	k1gFnSc2
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Glosy	glosa	k1gFnSc2
<g/>
↑	↑	k?
The	The	k1gMnSc1
dogfight	dogfight	k1gMnSc1
of	of	k?
April	April	k1gMnSc1
7	#num#	k4
<g/>
th	th	k?
1967	#num#	k4
-	-	kIx~
The	The	k1gFnSc1
climactic	climactice	k1gFnPc2
end	end	k?
to	ten	k3xDgNnSc4
the	the	k?
'	'	kIx"
<g/>
war	war	k?
for	forum	k1gNnPc2
water	water	k1gInSc1
<g/>
'	'	kIx"
and	and	k?
the	the	k?
start	start	k1gInSc1
of	of	k?
the	the	k?
Six	Six	k1gMnSc1
Day	Day	k1gMnSc1
War	War	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2007-05-26	2007-05-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WASHINGTONPOST	WASHINGTONPOST	kA
<g/>
.	.	kIx.
<g/>
COM	COM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prelude	Prelud	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Six	Six	k1gFnPc1
Days	Daysa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RYBAS	RYBAS	kA
<g/>
,	,	kIx,
Svjatoslav	Svjatoslav	k1gMnSc1
Jurjevič	Jurjevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomat	diplomat	k1gInSc1
války	válka	k1gFnSc2
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rockwood	Rockwood	k1gInSc1
and	and	k?
Partner	partner	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905274	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
332	#num#	k4
<g/>
-	-	kIx~
<g/>
333	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
С	С	k?
В	В	k?
в	в	k?
с	с	k?
и	и	k?
п	п	k?
«	«	k?
<g/>
ш	ш	k?
в	в	k?
<g/>
»	»	k?
в	в	k?
1967	#num#	k4
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
alerozin	alerozin	k1gInSc1
<g/>
.	.	kIx.
<g/>
narod	narod	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NPR	NPR	kA
<g/>
.	.	kIx.
<g/>
ORG	ORG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Mideast	Mideast	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Century	Centura	k1gFnSc2
of	of	k?
Conflict	Conflict	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RYBAS	RYBAS	kA
<g/>
,	,	kIx,
Svjatoslav	Svjatoslav	k1gMnSc1
Jurjevič	Jurjevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomat	diplomat	k1gInSc1
války	válka	k1gFnSc2
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rockwood	Rockwood	k1gInSc1
and	and	k?
Partner	partner	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905274	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
334	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NYTIMES	NYTIMES	kA
<g/>
.	.	kIx.
<g/>
COM	COM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Israelis	Israelis	k1gInSc1
Say	Say	k1gFnSc2
Tape	Tap	k1gFnSc2
Shows	Showsa	k1gFnPc2
Nasser	Nasser	k1gMnSc1
Fabricated	Fabricated	k1gMnSc1
'	'	kIx"
<g/>
Plot	plot	k1gInSc1
<g/>
'	'	kIx"
<g/>
;	;	kIx,
Recording	Recording	k1gInSc1
Said	Saida	k1gFnPc2
to	ten	k3xDgNnSc4
Be	Be	k1gFnSc1
of	of	k?
Phone	Phon	k1gInSc5
Call	Call	k1gInSc1
to	ten	k3xDgNnSc1
Hassein	Hassein	k2eAgMnSc1d1
Gives	Gives	k1gMnSc1
Plan	plan	k1gInSc4
to	ten	k3xDgNnSc4
Accuse	Accuse	k1gFnSc1
U.	U.	kA
<g/>
S.	S.	kA
and	and	k?
Britain	Britain	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VALKA	VALKA	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitka	bitka	k1gFnSc1
o	o	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FORUM	forum	k1gNnSc4
<g/>
.	.	kIx.
<g/>
VALKA	VALKA	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
AGTR	AGTR	kA
-	-	kIx~
5	#num#	k4
USS	USS	kA
Liberty	Libert	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RYBAS	RYBAS	kA
<g/>
,	,	kIx,
Svjatoslav	Svjatoslav	k1gMnSc1
Jurjevič	Jurjevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomat	diplomat	k1gInSc1
války	válka	k1gFnSc2
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rockwood	Rockwood	k1gInSc1
and	and	k?
Partner	partner	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905274	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
337	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RYBAS	RYBAS	kA
<g/>
,	,	kIx,
Svjatoslav	Svjatoslav	k1gMnSc1
Jurjevič	Jurjevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomat	diplomat	k1gInSc1
války	válka	k1gFnSc2
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rockwood	Rockwood	k1gInSc1
and	and	k?
Partner	partner	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905274	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
337	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NEWS	NEWS	kA
<g/>
.	.	kIx.
<g/>
BBC	BBC	kA
<g/>
.	.	kIx.
<g/>
CO	co	k6eAd1
<g/>
.	.	kIx.
<g/>
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Six-Day	Six-Da	k2eAgFnPc4d1
War	War	k1gFnPc4
<g/>
:	:	kIx,
Before	Befor	k1gInSc5
the	the	k?
war	war	k?
-	-	kIx~
Six-Day	Six-Daa	k1gMnSc2
War	War	k1gMnSc2
<g/>
:	:	kIx,
After	After	k1gInSc1
the	the	k?
war	war	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mass	Mass	k1gInSc1
Graves	Graves	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
umassd	umassd	k1gInSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Historians	Historians	k1gInSc1
<g/>
:	:	kIx,
Israeli	Israel	k1gInSc6
troops	troops	k6eAd1
killed	killed	k1gMnSc1
many	mana	k1gFnSc2
Egyptian	Egyptian	k1gMnSc1
POWs	POWs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UN	UN	kA
soldiers	soldiers	k6eAd1
doubt	doubt	k5eAaPmF
1967	#num#	k4
killing	killing	k1gInSc1
of	of	k?
POWs	POWs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
jpost	jpost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ha	ha	kA
<g/>
'	'	kIx"
<g/>
aretz	aretz	k1gMnSc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1972	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BROŽ	Brož	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabsko-izraelské	arabsko-izraelský	k2eAgFnSc2d1
války	válka	k1gFnSc2
:	:	kIx,
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1973	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
dopl	dopla	k1gFnPc2
<g/>
.	.	kIx.
opr	opr	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
362	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7425	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BLACK	BLACK	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
;	;	kIx,
MORRIS	MORRIS	kA
<g/>
,	,	kIx,
Benny	Benn	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mossad	Mossad	k1gInSc1
:	:	kIx,
izraelské	izraelský	k2eAgFnSc2d1
tajné	tajný	k2eAgFnSc2d1
války	válka	k1gFnSc2
:	:	kIx,
dějiny	dějiny	k1gFnPc4
izraelské	izraelský	k2eAgFnSc2d1
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
625	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
145	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GILBERT	Gilbert	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
Art	Art	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
668	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7257	#num#	k4
<g/>
-	-	kIx~
<g/>
740	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HERZOG	HERZOG	kA
<g/>
,	,	kIx,
Chaim	Chaim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabsko-izraelské	arabsko-izraelský	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
válka	válka	k1gFnSc1
a	a	k8xC
mír	mír	k1gInSc1
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
od	od	k7c2
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
617	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
954	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
767	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902484	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SEGEV	SEGEV	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
1967	#num#	k4
:	:	kIx,
Israel	Israel	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
war	war	k?
and	and	k?
the	the	k?
year	year	k1gMnSc1
that	that	k1gMnSc1
transformed	transformed	k1gMnSc1
the	the	k?
Middle	Middle	k1gMnSc1
East	East	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Abacus	Abacus	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
822	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
349	#num#	k4
<g/>
-	-	kIx~
<g/>
11595	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gFnSc1
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Petr	Petr	k1gMnSc1
Žižka	Žižka	k1gMnSc1
-	-	kIx~
Geopolitický	geopolitický	k2eAgInSc1d1
kontext	kontext	k1gInSc1
šestidenní	šestidenní	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
na	na	k7c4
Glosy	glosa	k1gFnPc4
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Reflex	reflex	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
–	–	k?
video	video	k1gNnSc1
z	z	k7c2
cyklu	cyklus	k1gInSc2
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Historický	historický	k2eAgInSc1d1
magazín	magazín	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Výběr	výběr	k1gInSc1
videí	video	k1gNnPc2
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Arabsko-izraelské	arabsko-izraelský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
sinajská	sinajský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
opotřebovací	opotřebovací	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
jomkipurská	jomkipurský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
první	první	k4xOgFnSc1
libanonská	libanonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
druhá	druhý	k4xOgFnSc1
libanonská	libanonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
278606	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4180597-5	4180597-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85068715	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85068715	#num#	k4
</s>
