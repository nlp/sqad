<s>
Šestidenní	šestidenní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
מ	מ	k?
ש	ש	k?
ה	ה	k?
<g/>
,	,	kIx,
milchemet	milchemet	k1gMnSc1
šešet	šešet	k1gMnSc1
ha	ha	kA
<g/>
‑	‑	k?
<g/>
jamim	jamim	k1gInSc1
<g/>
,	,	kIx,
resp.	resp.	kA
třetí	třetí	k4xOgFnSc1
arabsko-izraelská	arabsko-izraelský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1967	#num#	k4
bylo	být	k5eAaImAgNnS
vojenské	vojenský	k2eAgNnSc1d1
střetnutí	střetnutí	k1gNnSc1
mezi	mezi	k7c7
Izraelem	Izrael	k1gInSc7
a	a	k8xC
koalicí	koalice	k1gFnSc7
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc2
a	a	k8xC
Jordánska	Jordánsko	k1gNnSc2
<g/>
.	.	kIx.
</s>