<s desamb="1">
Arabská	arabský	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
utrpěla	utrpět	k5eAaPmAgFnS
zdrcující	zdrcující	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
–	–	k?
Egypt	Egypt	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
Gazu	Gaza	k1gFnSc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
Sinaj	Sinaj	k1gInSc4
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
a	a	k8xC
Sýrie	Sýrie	k1gFnSc2
Golanské	Golanský	k2eAgFnSc2d1
výšiny	výšina	k1gFnSc2
<g/>
.	.	kIx.
</s>