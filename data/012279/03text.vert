<p>
<s>
Colson	Colson	k1gMnSc1	Colson
Baker	Baker	k1gMnSc1	Baker
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
MGK	MGK	kA	MGK
nebo	nebo	k8xC	nebo
Machine	Machin	k1gInSc5	Machin
Gun	Gun	k1gMnSc2	Gun
Kelly	Kell	k1gInPc4	Kell
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
americký	americký	k2eAgMnSc1d1	americký
rapper	rapper	k1gMnSc1	rapper
z	z	k7c2	z
Clevelandu	Cleveland	k1gInSc2	Cleveland
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Bad	Bad	k1gFnSc7	Bad
Boy	boa	k1gFnSc2	boa
a	a	k8xC	a
Interscope	Interscop	k1gInSc5	Interscop
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rychle	rychle	k6eAd1	rychle
umí	umět	k5eAaImIp3nS	umět
rapovat	rapovat	k5eAaImF	rapovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
notoricky	notoricky	k6eAd1	notoricky
známého	známý	k2eAgMnSc4d1	známý
kriminálníka	kriminálník	k1gMnSc4	kriminálník
George	Georg	k1gMnSc4	Georg
"	"	kIx"	"
<g/>
Machine	Machin	k1gMnSc5	Machin
Gun	Gun	k1gMnSc5	Gun
<g/>
"	"	kIx"	"
Kellyho	Kelly	k1gMnSc2	Kelly
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
slávy	sláva	k1gFnPc4	sláva
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgNnPc2	první
čtyř	čtyři	k4xCgNnPc2	čtyři
mixtapeů	mixtape	k1gMnPc2	mixtape
<g/>
,	,	kIx,	,
Stamp	Stamp	k1gMnSc1	Stamp
Of	Of	k1gMnSc1	Of
Approval	Approval	k1gMnSc1	Approval
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Homecoming	Homecoming	k1gInSc1	Homecoming
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
100	[number]	k4	100
Words	Words	k1gInSc1	Words
and	and	k?	and
Running	Running	k1gInSc1	Running
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lace	laka	k1gFnSc3	laka
Up	Up	k1gFnSc2	Up
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
XXL	XXL	kA	XXL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
MGK	MGK	kA	MGK
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Young	Young	k1gMnSc1	Young
and	and	k?	and
Reckless	Reckless	k1gInSc1	Reckless
Clothing	Clothing	k1gInSc1	Clothing
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
MGK	MGK	kA	MGK
jmenován	jmenovat	k5eAaImNgInS	jmenovat
Nejvíce	nejvíce	k6eAd1	nejvíce
sexy	sex	k1gInPc1	sex
průlomovým	průlomový	k2eAgFnPc3d1	průlomová
MC	MC	kA	MC
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
od	od	k7c2	od
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
MTV	MTV	kA	MTV
cenu	cena	k1gFnSc4	cena
Breaking	Breaking	k1gInSc4	Breaking
Woodie	Woodie	k1gFnSc2	Woodie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc1	dětství
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Baker	Baker	k1gMnSc1	Baker
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
misionářským	misionářský	k2eAgMnSc7d1	misionářský
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
norského	norský	k2eAgInSc2d1	norský
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Baker	Baker	k1gMnSc1	Baker
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
procestovala	procestovat	k5eAaPmAgFnS	procestovat
svět	svět	k1gInSc4	svět
a	a	k8xC	a
usadila	usadit	k5eAaPmAgFnS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Egyptu	Egypt	k1gInSc3	Egypt
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
prošla	projít	k5eAaPmAgFnS	projít
i	i	k9	i
USA	USA	kA	USA
a	a	k8xC	a
městy	město	k1gNnPc7	město
jako	jako	k9	jako
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Denver	Denver	k1gInSc1	Denver
a	a	k8xC	a
Cleveland	Cleveland	k1gInSc1	Cleveland
<g/>
.	.	kIx.	.
</s>
<s>
Rap	rap	k1gMnSc1	rap
začal	začít	k5eAaPmAgMnS	začít
poslouchat	poslouchat	k5eAaImF	poslouchat
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
třídě	třída	k1gFnSc6	třída
když	když	k8xS	když
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Hamilton	Hamilton	k1gInSc4	Hamilton
Middle	Middle	k1gFnSc3	Middle
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
školu	škola	k1gFnSc4	škola
s	s	k7c7	s
etnicky	etnicky	k6eAd1	etnicky
různorodým	různorodý	k2eAgInSc7d1	různorodý
studentským	studentský	k2eAgInSc7d1	studentský
rozborem	rozbor	k1gInSc7	rozbor
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Baker	Baker	k1gInSc1	Baker
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
Clevelandu	Clevelanda	k1gFnSc4	Clevelanda
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
Shaker	Shaker	k1gInSc1	Shaker
Heights	Heightsa	k1gFnPc2	Heightsa
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vystěhování	vystěhování	k1gNnSc4	vystěhování
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
trénovat	trénovat	k5eAaImF	trénovat
rap	rap	k1gMnSc1	rap
v	v	k7c4	v
Apollo	Apollo	k1gNnSc4	Apollo
Theater	Theatra	k1gFnPc2	Theatra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
bílý	bílý	k2eAgInSc1d1	bílý
rapper	rapper	k1gInSc1	rapper
<g/>
.	.	kIx.	.
</s>
<s>
Baker	Baker	k1gMnSc1	Baker
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zviditelnil	zviditelnit	k5eAaPmAgMnS	zviditelnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
MTV2	MTV2	k1gFnSc6	MTV2
Sucker	Suckra	k1gFnPc2	Suckra
Free	Fre	k1gFnSc2	Fre
Freestyle	Freestyl	k1gInSc5	Freestyl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
freestyleově	freestyleův	k2eAgNnSc6d1	freestyleův
zareppoval	zareppovat	k5eAaBmAgMnS	zareppovat
předělávky	předělávka	k1gFnPc4	předělávka
slok	sloka	k1gFnPc2	sloka
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Chip	Chip	k1gMnSc1	Chip
Off	Off	k1gMnSc1	Off
The	The	k1gMnSc1	The
Block	Block	k1gMnSc1	Block
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgMnS	vydat
svůj	svůj	k3xOyFgMnSc1	svůj
debutový	debutový	k2eAgMnSc1d1	debutový
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
100	[number]	k4	100
Words	Words	k1gInSc1	Words
and	and	k?	and
Running	Running	k1gInSc1	Running
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
nastal	nastat	k5eAaPmAgInS	nastat
jeho	jeho	k3xOp3gInSc4	jeho
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
debutoval	debutovat	k5eAaBmAgMnS	debutovat
se	s	k7c7	s
singlem	singl	k1gInSc7	singl
a	a	k8xC	a
videoklipem	videoklip	k1gInSc7	videoklip
"	"	kIx"	"
<g/>
Alice	Alice	k1gFnSc1	Alice
In	In	k1gMnSc1	In
Wonderland	Wonderland	k1gInSc1	Wonderland
<g/>
"	"	kIx"	"
na	na	k7c4	na
Midwest	Midwest	k1gInSc4	Midwest
Block	Block	k1gMnSc1	Block
Starz	Starz	k1gMnSc1	Starz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vydal	vydat	k5eAaPmAgMnS	vydat
Block	Block	k1gInSc4	Block
Starz	Starz	k1gMnSc1	Starz
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
druhý	druhý	k4xOgMnSc1	druhý
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
jménem	jméno	k1gNnSc7	jméno
Lace	laka	k1gFnSc3	laka
Up	Up	k1gMnSc1	Up
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
singl	singl	k1gInSc1	singl
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Clevelandu	Cleveland	k1gInSc2	Cleveland
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cleveland	Cleveland	k1gInSc1	Cleveland
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lace	laka	k1gFnSc3	laka
Up	Up	k1gFnPc2	Up
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
song	song	k1gInSc1	song
"	"	kIx"	"
<g/>
Invicible	Invicible	k1gFnSc1	Invicible
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
na	na	k7c4	na
HTC	HTC	kA	HTC
ReZound	ReZound	k1gInSc4	ReZound
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
oficiální	oficiální	k2eAgFnSc7d1	oficiální
znělkou	znělka	k1gFnSc7	znělka
pro	pro	k7c4	pro
WrestleManii	WrestleManie	k1gFnSc4	WrestleManie
XXVIII	XXVIII	kA	XXVIII
a	a	k8xC	a
WWE	WWE	kA	WWE
také	také	k9	také
použila	použít	k5eAaPmAgFnS	použít
tento	tento	k3xDgInSc4	tento
song	song	k1gInSc4	song
pro	pro	k7c4	pro
nejvější	vý	k2eAgInPc4d3	nejvější
okamžiky	okamžik	k1gInPc4	okamžik
ze	z	k7c2	z
zápasu	zápas	k1gInSc2	zápas
mezi	mezi	k7c7	mezi
Johnem	John	k1gMnSc7	John
Cenou	cena	k1gFnSc7	cena
a	a	k8xC	a
The	The	k1gFnSc1	The
Rockem	rock	k1gInSc7	rock
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
znělka	znělka	k1gFnSc1	znělka
pro	pro	k7c4	pro
Thursday	Thursda	k1gMnPc4	Thursda
Night	Night	k2eAgInSc4d1	Night
Football	Football	k1gInSc4	Football
na	na	k7c6	na
NFL	NFL	kA	NFL
Network	network	k1gInSc1	network
<g/>
.	.	kIx.	.
</s>
<s>
Song	song	k1gInSc1	song
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Ester	Ester	k1gFnSc1	Ester
Deanová	Deanová	k1gFnSc1	Deanová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
MGK	MGK	kA	MGK
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Store	Stor	k1gInSc5	Stor
se	se	k3xPyFc4	se
vymklo	vymknout	k5eAaPmAgNnS	vymknout
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Ven	ven	k6eAd1	ven
byl	být	k5eAaImAgInS	být
vyvedem	vyvedem	k?	vyvedem
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
policistů	policista	k1gMnPc2	policista
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
stole	stol	k1gInSc6	stol
a	a	k8xC	a
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
na	na	k7c4	na
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
chtěli	chtít	k5eAaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
slezl	slézt	k5eAaPmAgMnS	slézt
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
prostředníček	prostředníček	k1gInSc1	prostředníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
MGK	MGK	kA	MGK
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c4	na
Aerodrome	aerodrom	k1gInSc5	aerodrom
festivalu	festival	k1gInSc6	festival
a	a	k8xC	a
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Fora	forum	k1gNnSc2	forum
Karlín	Karlín	k1gInSc1	Karlín
,	,	kIx,	,
web	web	k1gInSc1	web
iREPORT	iREPORT	k?	iREPORT
druhé	druhý	k4xOgNnSc4	druhý
vystoupení	vystoupení	k1gNnSc4	vystoupení
pochválil	pochválit	k5eAaPmAgMnS	pochválit
<g/>
,	,	kIx,	,
vytknul	vytknout	k5eAaPmAgMnS	vytknout
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nedostatek	nedostatek	k1gInSc1	nedostatek
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Machine	Machinout	k5eAaPmIp3nS	Machinout
Gun	Gun	k1gFnSc1	Gun
Kelly	Kella	k1gFnSc2	Kella
se	se	k3xPyFc4	se
také	také	k9	také
věnuje	věnovat	k5eAaImIp3nS	věnovat
herectví	herectví	k1gNnSc1	herectví
a	a	k8xC	a
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Beyond	Beyond	k1gMnSc1	Beyond
The	The	k1gMnSc1	The
Lights	Lights	k1gInSc4	Lights
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
roli	role	k1gFnSc4	role
rappera	rappera	k1gFnSc1	rappera
Kida	Kida	k1gFnSc1	Kida
Culprita	Culprita	k1gFnSc1	Culprita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
své	svůj	k3xOyFgFnPc4	svůj
další	další	k2eAgFnPc4d1	další
role	role	k1gFnPc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gMnSc1	The
Land	Land	k1gMnSc1	Land
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
interpret	interpret	k1gMnSc1	interpret
NAS	NAS	kA	NAS
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Roadies	Roadiesa	k1gFnPc2	Roadiesa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
roli	role	k1gFnSc4	role
Wese	Wes	k1gInSc2	Wes
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
ale	ale	k9	ale
neměla	mít	k5eNaImAgFnS	mít
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
seriál	seriál	k1gInSc1	seriál
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Sandry	Sandra	k1gFnSc2	Sandra
Bullock	Bullocka	k1gFnPc2	Bullocka
v	v	k7c6	v
napínavém	napínavý	k2eAgInSc6d1	napínavý
trháku	trhák	k1gInSc6	trhák
Bird	Bird	k1gMnSc1	Bird
Box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
sledovaných	sledovaný	k2eAgInPc2d1	sledovaný
filmů	film	k1gInPc2	film
na	na	k7c6	na
Netflixu	Netflix	k1gInSc6	Netflix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
role	role	k1gFnPc1	role
však	však	k9	však
byly	být	k5eAaImAgFnP	být
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
a	a	k8xC	a
ve	v	k7c6	v
filmech	film	k1gInPc6	film
jim	on	k3xPp3gInPc3	on
nebyla	být	k5eNaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
přiliš	přiliš	k5eAaPmIp2nS	přiliš
velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
ale	ale	k8xC	ale
Netflix	Netflix	k1gInSc4	Netflix
oznámil	oznámit	k5eAaPmAgMnS	oznámit
film	film	k1gInSc4	film
The	The	k1gFnSc3	The
Dirt	Dirt	k1gInSc1	Dirt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
MGK	MGK	kA	MGK
zahrál	zahrát	k5eAaPmAgInS	zahrát
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
bubeníka	bubeník	k1gMnSc2	bubeník
Mötley	Mötlea	k1gMnSc2	Mötlea
Crüe	Crü	k1gMnSc2	Crü
<g/>
,	,	kIx,	,
Tommyho	Tommy	k1gMnSc2	Tommy
Lee	Lea	k1gFnSc6	Lea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rap	rap	k1gMnSc1	rap
Devil	Devil	k1gMnSc1	Devil
(	(	kIx(	(
<g/>
diss	diss	k1gInSc1	diss
na	na	k7c4	na
Eminema	Eminema	k1gNnSc4	Eminema
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
objevil	objevit	k5eAaPmAgMnS	objevit
Kellyho	Kelly	k1gMnSc2	Kelly
tweet	tweet	k1gMnSc1	tweet
"	"	kIx"	"
<g/>
Ok	oka	k1gFnPc2	oka
so	so	k?	so
i	i	k8xC	i
just	just	k6eAd1	just
saw	saw	k?	saw
a	a	k8xC	a
picture	pictur	k1gMnSc5	pictur
of	of	k?	of
Eminem	Emin	k1gInSc7	Emin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
daughter	daughtra	k1gFnPc2	daughtra
<g/>
...	...	k?	...
<g/>
and	and	k?	and
i	i	k9	i
have	havat	k5eAaPmIp3nS	havat
to	ten	k3xDgNnSc1	ten
say	say	k?	say
<g/>
,	,	kIx,	,
she	she	k?	she
is	is	k?	is
hot	hot	k0	hot
as	as	k9	as
fuck	fuck	k6eAd1	fuck
<g/>
,	,	kIx,	,
in	in	k?	in
the	the	k?	the
most	most	k1gInSc1	most
respectful	respectful	k1gInSc1	respectful
way	way	k?	way
possible	possible	k6eAd1	possible
cuz	cuz	k?	cuz
Em	Ema	k1gFnPc2	Ema
is	is	k?	is
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
doslova	doslova	k6eAd1	doslova
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajimávým	Zajimávý	k2eAgInSc7d1	Zajimávý
faktem	fakt	k1gInSc7	fakt
ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
tweet	tweet	k1gInSc1	tweet
začal	začít	k5eAaPmAgInS	začít
řešit	řešit	k5eAaImF	řešit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
když	když	k8xS	když
Eminem	Emino	k1gNnSc7	Emino
překvapivě	překvapivě	k6eAd1	překvapivě
vydal	vydat	k5eAaPmAgInS	vydat
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Kamikaze	kamikaze	k1gMnSc1	kamikaze
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
písnička	písnička	k1gFnSc1	písnička
Not	nota	k1gFnPc2	nota
Alike	Alik	k1gInSc2	Alik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
Eminem	Emin	k1gInSc7	Emin
dissuje	dissovat	k5eAaBmIp3nS	dissovat
Kellse	Kellse	k1gFnSc1	Kellse
za	za	k7c4	za
zmiňovaný	zmiňovaný	k2eAgInSc4d1	zmiňovaný
tweet	tweet	k1gInSc4	tweet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MGK	MGK	kA	MGK
si	se	k3xPyFc3	se
nic	nic	k3yNnSc1	nic
nenechal	nechat	k5eNaPmAgMnS	nechat
líbit	líbit	k5eAaImF	líbit
a	a	k8xC	a
tak	tak	k6eAd1	tak
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2018	[number]	k4	2018
tweetnul	tweetnout	k5eAaPmAgMnS	tweetnout
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
Diss	Diss	k1gInSc4	Diss
na	na	k7c4	na
Eminema	Eminema	k1gNnSc4	Eminema
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
popisek	popisek	k1gInSc4	popisek
"	"	kIx"	"
<g/>
FUCK	FUCK	kA	FUCK
RAP	rapa	k1gFnPc2	rapa
GOD	GOD	kA	GOD
IM	IM	kA	IM
THE	THE	kA	THE
RAP	rapa	k1gFnPc2	rapa
DEVIL	DEVIL	kA	DEVIL
<g/>
.	.	kIx.	.
good	good	k6eAd1	good
morning	morning	k1gInSc1	morning
Eminem	Emino	k1gNnSc7	Emino
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c4	na
Youtube	Youtub	k1gInSc5	Youtub
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
diss	diss	k6eAd1	diss
tracku	track	k1gInSc2	track
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
MGK	MGK	kA	MGK
pojídá	pojídat	k5eAaImIp3nS	pojídat
cereálie	cereálie	k1gFnPc4	cereálie
nebo	nebo	k8xC	nebo
drží	držet	k5eAaImIp3nP	držet
lopatu	lopata	k1gFnSc4	lopata
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nejspíše	nejspíše	k9	nejspíše
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
tímto	tento	k3xDgNnSc7	tento
dissem	disso	k1gNnSc7	disso
svého	svůj	k3xOyFgNnSc2	svůj
bývalého	bývalý	k2eAgNnSc2d1	bývalé
idola	idolo	k1gNnSc2	idolo
pohřbít	pohřbít	k5eAaPmF	pohřbít
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c4	pod
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rap	rap	k1gMnSc1	rap
Devil	Devil	k1gMnSc1	Devil
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
ihned	ihned	k6eAd1	ihned
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
na	na	k7c4	na
první	první	k4xOgFnPc4	první
pozice	pozice	k1gFnPc4	pozice
hitparád	hitparáda	k1gFnPc2	hitparáda
a	a	k8xC	a
během	během	k7c2	během
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
youtube	youtubat	k5eAaPmIp3nS	youtubat
několikamilionová	několikamilionový	k2eAgNnPc4d1	několikamilionové
shlédnutí	shlédnutí	k1gNnPc4	shlédnutí
<g/>
.	.	kIx.	.
</s>
<s>
I	I	kA	I
když	když	k8xS	když
MGK	MGK	kA	MGK
sklidil	sklidit	k5eAaPmAgInS	sklidit
velkou	velký	k2eAgFnSc4d1	velká
kritiku	kritika	k1gFnSc4	kritika
a	a	k8xC	a
fanoušci	fanoušek	k1gMnPc1	fanoušek
Eminema	Eminema	k1gFnSc1	Eminema
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
instagramový	instagramový	k2eAgInSc4d1	instagramový
profil	profil	k1gInSc4	profil
psali	psát	k5eAaImAgMnP	psát
nenávistné	návistný	k2eNgInPc4d1	nenávistný
komentáře	komentář	k1gInPc4	komentář
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
Diss	Dissa	k1gFnPc2	Dissa
tracků	tracek	k1gMnPc2	tracek
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
deseti	deset	k4xCc6	deset
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
fanoušci	fanoušek	k1gMnPc1	fanoušek
Eminema	Eminemum	k1gNnSc2	Eminemum
dočkali	dočkat	k5eAaPmAgMnP	dočkat
jeho	jeho	k3xOp3gFnPc4	jeho
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Eminemův	Eminemův	k2eAgInSc1d1	Eminemův
diss	diss	k1gInSc1	diss
track	track	k1gInSc1	track
"	"	kIx"	"
<g/>
KILLSHOT	KILLSHOT	kA	KILLSHOT
<g/>
"	"	kIx"	"
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
youtube	youtubat	k5eAaPmIp3nS	youtubat
během	během	k7c2	během
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
38.1	[number]	k4	38.1
milionů	milion	k4xCgInPc2	milion
views	viewsa	k1gFnPc2	viewsa
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
osmým	osmý	k4xOgMnSc7	osmý
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
videem	video	k1gNnSc7	video
na	na	k7c4	na
youtube	youtubat	k5eAaPmIp3nS	youtubat
během	během	k7c2	během
prvních	první	k4xOgInPc2	první
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
dissu	diss	k1gInSc2	diss
Killshot	Killshot	k1gInSc1	Killshot
získal	získat	k5eAaPmAgInS	získat
MGK	MGK	kA	MGK
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
tak	tak	k6eAd1	tak
oznámil	oznámit	k5eAaPmAgMnS	oznámit
vydání	vydání	k1gNnSc4	vydání
svého	svůj	k3xOyFgNnSc2	svůj
nejnovějšího	nový	k2eAgNnSc2d3	nejnovější
EP	EP	kA	EP
Binge	Binge	k1gNnSc1	Binge
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
díky	díky	k7c3	díky
beefu	beef	k1gInSc3	beef
s	s	k7c7	s
Eminemem	Eminem	k1gInSc7	Eminem
získalo	získat	k5eAaPmAgNnS	získat
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
reklamu	reklama	k1gFnSc4	reklama
prakticky	prakticky	k6eAd1	prakticky
samo	sám	k3xTgNnSc1	sám
a	a	k8xC	a
zadarmo	zadarmo	k6eAd1	zadarmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
==	==	k?	==
</s>
</p>
<p>
<s>
MGK	MGK	kA	MGK
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
raperů	raper	k1gInPc2	raper
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
s	s	k7c7	s
živou	živý	k2eAgFnSc7d1	živá
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgMnS	začít
aplikovat	aplikovat	k5eAaBmF	aplikovat
prvky	prvek	k1gInPc4	prvek
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
pódia	pódium	k1gNnPc4	pódium
velkých	velký	k2eAgInPc2d1	velký
rockových	rockový	k2eAgInPc2d1	rockový
festivalů	festival	k1gInPc2	festival
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
am	am	k?	am
Ring	ring	k1gInSc1	ring
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
skládání	skládání	k1gNnSc2	skládání
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc2	nahrávání
songů	song	k1gInPc2	song
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
aktivní	aktivní	k2eAgFnSc1d1	aktivní
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
Slim	Slim	k1gInSc1	Slim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
už	už	k6eAd1	už
pro	pro	k7c4	pro
Kellyho	Kelly	k1gMnSc4	Kelly
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
nespočet	nespočet	k1gInSc1	nespočet
beatů	beat	k1gInPc2	beat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledních	poslední	k2eAgNnPc6d1	poslední
albech	album	k1gNnPc6	album
můžeme	moct	k5eAaImIp1nP	moct
také	také	k6eAd1	také
slyšet	slyšet	k5eAaImF	slyšet
živé	živá	k1gFnPc4	živá
bicí	bicí	k2eAgFnPc4d1	bicí
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
většinou	většinou	k6eAd1	většinou
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
Rook	Rook	k1gMnSc1	Rook
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
kamarád	kamarád	k1gMnSc1	kamarád
a	a	k8xC	a
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
Machine	Machin	k1gMnSc5	Machin
Gun	Gun	k1gMnSc5	Gun
Kellyho	Kellyha	k1gMnSc5	Kellyha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slim	Slim	k1gInSc1	Slim
-	-	kIx~	-
Klávesy	kláves	k1gInPc1	kláves
a	a	k8xC	a
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
</s>
</p>
<p>
<s>
AJ	aj	kA	aj
-	-	kIx~	-
Kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Baze	Baze	k1gFnSc1	Baze
-	-	kIx~	-
Baskytara	baskytara	k1gFnSc1	baskytara
a	a	k8xC	a
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Rook	Rook	k1gInSc1	Rook
-	-	kIx~	-
Bicí	bicí	k2eAgFnSc1d1	bicí
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Mixtape	Mixtap	k1gInSc5	Mixtap
===	===	k?	===
</s>
</p>
<p>
<s>
Stamp	Stamp	k1gMnSc1	Stamp
of	of	k?	of
Approval	Approval	k1gMnSc1	Approval
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Homecoming	Homecoming	k1gInSc1	Homecoming
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
100	[number]	k4	100
Words	Words	k1gInSc1	Words
and	and	k?	and
Running	Running	k1gInSc1	Running
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lace	laka	k1gFnSc3	laka
Up	Up	k1gMnSc1	Up
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rage	Rage	k1gFnSc1	Rage
Pack	Packa	k1gFnPc2	Packa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EST	Est	k1gMnSc1	Est
4	[number]	k4	4
Life	Life	k1gInSc1	Life
(	(	kIx(	(
<g/>
s	s	k7c7	s
Dubem	dub	k1gInSc7	dub
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Black	Black	k1gInSc1	Black
Flag	flag	k1gInSc1	flag
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fuck	Fuck	k1gMnSc1	Fuck
It	It	k1gMnSc1	It
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
mtvU	mtvU	k?	mtvU
Woodie	Woodie	k1gFnSc2	Woodie
Awards	Awardsa	k1gFnPc2	Awardsa
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
MGK	MGK	kA	MGK
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Machine	Machin	k1gInSc5	Machin
Gun	Gun	k1gFnPc3	Gun
Kelly	Kell	k1gInPc4	Kell
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
MGK	MGK	kA	MGK
na	na	k7c6	na
Allmusic	Allmusice	k1gFnPc2	Allmusice
</s>
</p>
