<s>
Sedmá	sedmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
IX	IX	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Ludvíkovo	Ludvíkův	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
středověká	středověký	k2eAgFnSc1d1
iluminace	iluminace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Al-Mansura	Al-Mansura	k1gFnSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vítězství	vítězství	k1gNnSc1
muslimů	muslim	k1gMnPc2
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Status	status	k1gInSc1
quo	quo	k?
ante	antat	k5eAaPmIp3nS
bellum	bellum	k1gInSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Poitou	Poiíst	k5eAaPmIp3nP
</s>
<s>
Anjou	Anjou	k6eAd1
</s>
<s>
Artois	Artois	k1gFnSc1
</s>
<s>
Flandry	Flandry	k1gInPc1
</s>
<s>
Burgundsko	Burgundsko	k1gNnSc1
</s>
<s>
Bourbonsko	Bourbonsko	k6eAd1
</s>
<s>
Champagne	Champagnout	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Toulouse	Toulouse	k1gInSc1
</s>
<s>
Provensálsko	Provensálsko	k6eAd1
</s>
<s>
Bretaň	Bretaň	k1gFnSc1
</s>
<s>
Narbonne	Narbonnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
malá	malý	k2eAgNnPc1d1
hrabství	hrabství	k1gNnPc1
či	či	k8xC
panství	panství	k1gNnSc1
</s>
<s>
Achajské	Achajský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc4
templářů	templář	k1gMnPc2
Řád	řád	k1gInSc1
templářů	templář	k1gMnPc2
</s>
<s>
Provensálsko	Provensálsko	k6eAd1
</s>
<s>
Kyperské	kyperský	k2eAgNnSc1d1
královstvíJeruzalémské	královstvíJeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
Řád	řád	k1gInSc4
johanitůŘád	johanitůŘáda	k1gFnPc2
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
Řád	řád	k1gInSc1
německých	německý	k2eAgFnPc2d1
rytířůŘád	rytířůŘáda	k1gFnPc2
sv.	sv.	kA
Lazara	Lazar	k1gMnSc2
Řád	řád	k1gInSc1
sv.	sv.	kA
Lazara	Lazar	k1gMnSc2
</s>
<s>
Ajjúbovský	Ajjúbovský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
</s>
<s>
Bahriové	Bahriový	k2eAgInPc4d1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1250	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mamlúcký	mamlúcký	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Mamlúcký	mamlúcký	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1250	#num#	k4
<g/>
)	)	kIx)
Abbásovský	Abbásovský	k2eAgInSc1d1
chalífát	chalífát	k1gInSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfons	Alfons	k1gMnSc1
z	z	k7c2
Poitiers	Poitiersa	k1gFnPc2
Karel	Karla	k1gFnPc2
z	z	k7c2
Anjou	Anja	k1gMnSc7
Robert	Robert	k1gMnSc1
z	z	k7c2
Artois	Artois	k1gFnSc2
†	†	k?
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Achajský	Achajský	k2eAgMnSc1d1
Vilém	Vilém	k1gMnSc1
ze	z	k7c2
Sonnacu	Sonnacus	k1gInSc2
†	†	k?
Renaud	Renauda	k1gFnPc2
z	z	k7c2
Vichiers	Vichiersa	k1gFnPc2
†	†	k?
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhý	dlouhý	k2eAgInSc1d1
meč	meč	k1gInSc1
†	†	k?
Hugo	Hugo	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
z	z	k7c2
Dreux	Dreux	k1gInSc1
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clément	Clément	k1gMnSc1
Vilém	Vilém	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
z	z	k7c2
Dampierre	Dampierr	k1gInSc5
Jan	Jan	k1gMnSc1
Bretaňský	bretaňský	k2eAgInSc4d1
Raimond	Raimond	k1gInSc4
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raimond	Raimond	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trencavel	Trencavel	k1gMnSc1
Hugo	Hugo	k1gMnSc1
X.	X.	kA
z	z	k7c2
Lusignanu	Lusignan	k1gInSc2
Hugo	Hugo	k1gMnSc1
XI	XI	kA
<g/>
.	.	kIx.
z	z	k7c2
Lusignanu	Lusignan	k1gInSc2
Filip	Filip	k1gMnSc1
z	z	k7c2
Montfortu	Montfort	k1gInSc2
Alfons	Alfons	k1gMnSc1
z	z	k7c2
Brienne	Brienn	k1gInSc5
Jan	Jan	k1gMnSc1
z	z	k7c2
Brienne	Brienn	k1gInSc5
Jan	Jan	k1gMnSc1
Tristan	Tristan	k1gInSc1
z	z	k7c2
Valois	Valois	k1gFnSc2
Jan	Jan	k1gMnSc1
z	z	k7c2
Joinville	Joinville	k1gFnSc2
Humbert	Humbert	k1gMnSc1
V.	V.	kA
z	z	k7c2
Beaujeu	Beaujeus	k1gInSc2
Archambaud	Archambauda	k1gFnPc2
IX	IX	kA
<g/>
.	.	kIx.
†	†	k?
Radulf	Radulf	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
Coucy	Couca	k1gFnSc2
Petr	Petr	k1gMnSc1
z	z	k7c2
Courtenay	Courtenaa	k1gFnSc2
Jindřich	Jindřich	k1gMnSc1
Kyperský	kyperský	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
z	z	k7c2
Ronay	Ronaa	k1gFnSc2
</s>
<s>
As-Sálih	As-Sálih	k1gInSc1
Ajjúb	Ajjúba	k1gFnPc2
Al-Muazzam	Al-Muazzam	k1gInSc1
Turanšáh	Turanšáh	k1gMnSc1
Šagrat	Šagrat	k1gMnSc1
Al	ala	k1gFnPc2
Durr	Durr	k1gMnSc1
An-Násir	An-Násir	k1gMnSc1
Jusúf	Jusúf	k1gMnSc1
Faris	Faris	k1gFnPc2
ad-Dín	ad-Dín	k1gMnSc1
Aktaj	Aktaj	k1gMnSc1
Kutuz	Kutuz	k1gMnSc1
Fakhr-ad-Dín	Fakhr-ad-Dín	k1gMnSc1
Jusuf	Jusuf	k1gMnSc1
†	†	k?
Mu	on	k3xPp3gInSc3
<g/>
‘	‘	k?
<g/>
izzuddín	izzuddín	k1gInSc1
Ajbak	Ajbak	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
Kalavún	Kalavún	k1gMnSc1
Al-Musta	Al-Musta	k1gMnSc1
<g/>
'	'	kIx"
<g/>
sim	sima	k1gFnPc2
Billách	Billách	k?
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
15	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
<g/>
2	#num#	k4
400	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
800	#num#	k4
rytířů	rytíř	k1gMnPc2
<g/>
5	#num#	k4
000	#num#	k4
střelců	střelec	k1gMnPc2
z	z	k7c2
kuše	kuše	k1gFnSc2
</s>
<s>
neznámá	známý	k2eNgFnSc1d1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
těžké	těžký	k2eAgNnSc1d1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
armády	armáda	k1gFnSc2
zničena	zničen	k2eAgFnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
zajat	zajat	k2eAgMnSc1d1
</s>
<s>
lehké	lehký	k2eAgNnSc1d1
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
námořní	námořní	k2eAgFnSc1d1
operace	operace	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
účastníkem	účastník	k1gMnSc7
kruciáty	kruciáta	k1gFnSc2
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
vydal	vydat	k5eAaPmAgMnS
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1248	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
vyplulo	vyplout	k5eAaPmAgNnS
z	z	k7c2
nově	nově	k6eAd1
postaveného	postavený	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
v	v	k7c4
Aigues-Mortes	Aigues-Mortes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
mezipřistání	mezipřistání	k1gNnSc2
na	na	k7c6
Kypru	Kypr	k1gInSc6
byl	být	k5eAaImAgInS
místními	místní	k2eAgFnPc7d1
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
ideálním	ideální	k2eAgInSc7d1
cílem	cíl	k1gInSc7
útoku	útok	k1gInSc2
bude	být	k5eAaImBp3nS
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
a	a	k8xC
přípravy	příprava	k1gFnPc4
výpravy	výprava	k1gFnSc2
</s>
<s>
Po	po	k7c6
vypršení	vypršení	k1gNnSc6
míru	mír	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
vyjednal	vyjednat	k5eAaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
sultán	sultán	k1gMnSc1
Al-Kamil	Al-Kamil	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1244	#num#	k4
Jeruzalém	Jeruzalém	k1gInSc1
opět	opět	k6eAd1
uchvácen	uchvácet	k5eAaImNgInS,k5eAaPmNgInS
muslimy	muslim	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
ztrátě	ztráta	k1gFnSc6
Kristova	Kristův	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
již	již	k6eAd1
nevzbudila	vzbudit	k5eNaPmAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
téměř	téměř	k6eAd1
žádný	žádný	k3yNgInSc4
rozruch	rozruch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štaufský	Štaufský	k2eAgMnSc1d1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
soupeřil	soupeřit	k5eAaImAgMnS
s	s	k7c7
papežem	papež	k1gMnSc7
Inocencem	Inocenc	k1gMnSc7
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
měl	mít	k5eAaImAgInS
i	i	k9
problémy	problém	k1gInPc4
s	s	k7c7
udržením	udržení	k1gNnSc7
císařské	císařský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
zúčastnit	zúčastnit	k5eAaPmF
nemohl	moct	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Anglii	Anglie	k1gFnSc6
měl	mít	k5eAaImAgMnS
král	král	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
velké	velký	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
v	v	k7c6
bojích	boj	k1gInPc6
proti	proti	k7c3
nespokojeným	spokojený	k2eNgMnPc3d1
povstalcům	povstalec	k1gMnPc3
vedených	vedený	k2eAgFnPc2d1
Simonem	Simon	k1gMnSc7
z	z	k7c2
Montfortu	Montfort	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pouze	pouze	k6eAd1
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
ke	k	k7c3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
odhodlal	odhodlat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyžádal	vyžádat	k5eAaPmAgMnS
si	se	k3xPyFc3
nejprve	nejprve	k6eAd1
od	od	k7c2
Jindřicha	Jindřich	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
zatímco	zatímco	k8xS
bude	být	k5eAaImBp3nS
na	na	k7c6
výpravě	výprava	k1gFnSc6
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Francii	Francie	k1gFnSc4
nenapadne	napadnout	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1245	#num#	k4
pak	pak	k6eAd1
oficiálně	oficiálně	k6eAd1
potvrdil	potvrdit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
účast	účast	k1gFnSc4
na	na	k7c6
kruciátě	kruciáta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
získat	získat	k5eAaPmF
k	k	k7c3
výpravě	výprava	k1gFnSc3
také	také	k9
norského	norský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Norska	Norsko	k1gNnSc2
vyrazil	vyrazit	k5eAaPmAgMnS
jako	jako	k9
velvyslanec	velvyslanec	k1gMnSc1
anglický	anglický	k2eAgMnSc1d1
kronikář	kronikář	k1gMnSc1
Matthew	Matthew	k1gMnSc1
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
přesvědčit	přesvědčit	k5eAaPmF
norského	norský	k2eAgMnSc4d1
krále	král	k1gMnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účast	účast	k1gFnSc4
nakonec	nakonec	k6eAd1
přislíbili	přislíbit	k5eAaPmAgMnP
jen	jen	k9
Ludvíkovi	Ludvíkův	k2eAgMnPc1d1
bratři	bratr	k1gMnPc1
Alfons	Alfons	k1gMnSc1
a	a	k8xC
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
následující	následující	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
sháněl	shánět	k5eAaImAgMnS
peníze	peníz	k1gInPc4
a	a	k8xC
prostředky	prostředek	k1gInPc4
na	na	k7c4
financování	financování	k1gNnSc4
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
nakonec	nakonec	k6eAd1
získal	získat	k5eAaPmAgInS
z	z	k7c2
církevních	církevní	k2eAgInPc2d1
desátků	desátek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Socha	Socha	k1gMnSc1
Ludvíka	Ludvík	k1gMnSc2
IX	IX	kA
<g/>
.	.	kIx.
v	v	k7c6
přístavním	přístavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Aigues-Mortes	Aigues-Mortesa	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1248	#num#	k4
nashromáždil	nashromáždit	k5eAaPmAgMnS
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
svou	svůj	k3xOyFgFnSc4
prý	prý	k9
až	až	k9
pětadvacetitisícovou	pětadvacetitisícový	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
v	v	k7c6
přístavech	přístav	k1gInPc6
Aigues-Mortes	Aigues-Mortesa	k1gFnPc2
a	a	k8xC
Marseille	Marseille	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vojsku	vojsko	k1gNnSc6
bylo	být	k5eAaImAgNnS
1500	#num#	k4
rytířů	rytíř	k1gMnPc2
a	a	k8xC
5000	#num#	k4
střelců	střelec	k1gMnPc2
z	z	k7c2
kuší	kuše	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loďstvo	loďstvo	k1gNnSc1
vyplulo	vyplout	k5eAaPmAgNnS
směrem	směr	k1gInSc7
k	k	k7c3
Egyptu	Egypt	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
zimu	zima	k1gFnSc4
strávili	strávit	k5eAaPmAgMnP
vojáci	voják	k1gMnPc1
na	na	k7c6
Kypru	Kypr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
francouzskému	francouzský	k2eAgMnSc3d1
králi	král	k1gMnSc3
přicházeli	přicházet	k5eAaImAgMnP
během	během	k7c2
zimování	zimování	k1gNnSc2
poslové	posel	k1gMnPc1
ze	z	k7c2
všech	všecek	k3xTgNnPc2
křižáckých	křižácký	k2eAgNnPc2d1
panství	panství	k1gNnPc2
s	s	k7c7
prosbami	prosba	k1gFnPc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Latinské	latinský	k2eAgFnPc4d1
císařství	císařství	k1gNnSc3
chtělo	chtít	k5eAaImAgNnS
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
císařství	císařství	k1gNnSc3
Byzantskému	byzantský	k2eAgInSc3d1
a	a	k8xC
templáři	templář	k1gMnSc3
z	z	k7c2
Antiochie	Antiochie	k1gFnSc2
prosili	prosit	k5eAaImAgMnP,k5eAaPmAgMnP
o	o	k7c4
pomoc	pomoc	k1gFnSc4
také	také	k9
<g/>
,	,	kIx,
protože	protože	k8xS
Turci	Turek	k1gMnPc1
se	se	k3xPyFc4
právě	právě	k6eAd1
zmocnili	zmocnit	k5eAaPmAgMnP
Sidonu	Sidona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dobytím	dobytí	k1gNnSc7
Egypta	Egypt	k1gInSc2
by	by	kYmCp3nS
si	se	k3xPyFc3
zajistil	zajistit	k5eAaPmAgMnS
zásobování	zásobování	k1gNnSc4
a	a	k8xC
krytí	krytí	k1gNnSc4
při	při	k7c6
případném	případný	k2eAgInSc6d1
pozdějším	pozdní	k2eAgInSc6d2
útoku	útok	k1gInSc6
na	na	k7c4
samotný	samotný	k2eAgInSc4d1
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Víš	vědět	k5eAaImIp2nS
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
vůdcem	vůdce	k1gMnSc7
komunity	komunita	k1gFnSc2
křesťanské	křesťanský	k2eAgFnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
já	já	k3xPp1nSc1
uznávám	uznávat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
ty	ty	k3xPp2nSc1
jsi	být	k5eAaImIp2nS
vůdcem	vůdce	k1gMnSc7
komunity	komunita	k1gFnSc2
muslimské	muslimský	k2eAgFnSc2d1
<g/>
...	...	k?
Zmocním	zmocnit	k5eAaPmIp1nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
tvé	tvůj	k3xOp2gFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
to	ten	k3xDgNnSc1
dar	dar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
mi	já	k3xPp1nSc3
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
do	do	k7c2
rukou	ruka	k1gFnPc2
<g/>
,	,	kIx,
jestli	jestli	k8xS
zvítězíš	zvítězit	k5eAaPmIp2nS
a	a	k8xC
zůstane	zůstat	k5eAaPmIp3nS
tobě	ty	k3xPp2nSc3
<g/>
,	,	kIx,
budu	být	k5eAaImBp1nS
ti	ten	k3xDgMnPc1
zcela	zcela	k6eAd1
podroben	podrobit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informoval	informovat	k5eAaBmAgMnS
jsem	být	k5eAaImIp1nS
tě	ty	k3xPp2nSc4
a	a	k8xC
varoval	varovat	k5eAaImAgInS
před	před	k7c7
vojsky	vojsko	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mě	já	k3xPp1nSc4
poslouchají	poslouchat	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
zaplňují	zaplňovat	k5eAaImIp3nP
hory	hora	k1gFnPc4
i	i	k8xC
pláně	pláň	k1gFnPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
početná	početný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
oblázky	oblázek	k1gInPc4
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc2
meče	meč	k1gInSc2
proti	proti	k7c3
tobě	ty	k3xPp2nSc3
vede	vést	k5eAaImIp3nS
osud	osud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
(	(	kIx(
<g/>
úryvek	úryvek	k1gInSc1
z	z	k7c2
dopisu	dopis	k1gInSc2
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
egyptskému	egyptský	k2eAgMnSc3d1
sultánovi	sultán	k1gMnSc3
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1249	#num#	k4
se	se	k3xPyFc4
vylodil	vylodit	k5eAaPmAgInS
u	u	k7c2
egyptského	egyptský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
Damietta	Damiett	k1gInSc2
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
Damietta	Damietta	k1gFnSc1
po	po	k7c6
slabém	slabý	k2eAgInSc6d1
odporu	odpor	k1gInSc6
dobyta	dobyt	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
egyptští	egyptský	k2eAgMnPc1d1
obránci	obránce	k1gMnPc1
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
za	za	k7c4
řeku	řeka	k1gFnSc4
Nil	Nil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
však	však	k9
nepočítali	počítat	k5eNaImAgMnP
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Nil	Nil	k1gInSc1
vylije	vylít	k5eAaPmIp3nS
z	z	k7c2
břehů	břeh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Povodeň	povodeň	k1gFnSc4
je	být	k5eAaImIp3nS
uvěznila	uvěznit	k5eAaPmAgFnS
na	na	k7c4
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
také	také	k9
zamítnul	zamítnout	k5eAaPmAgMnS
dohodu	dohoda	k1gFnSc4
z	z	k7c2
páté	pátý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
přidělovala	přidělovat	k5eAaImAgFnS
přístav	přístav	k1gInSc4
pod	pod	k7c4
nadvládu	nadvláda	k1gFnSc4
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
zanechal	zanechat	k5eAaPmAgMnS
ve	v	k7c6
městě	město	k1gNnSc6
svou	svůj	k3xOyFgFnSc4
ženu	žena	k1gFnSc4
Markétu	Markéta	k1gFnSc4
a	a	k8xC
s	s	k7c7
armádou	armáda	k1gFnSc7
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
pochod	pochod	k1gInSc4
ke	k	k7c3
Káhiře	Káhira	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
čas	čas	k1gInSc4
zemřel	zemřít	k5eAaPmAgMnS
Ajjúbovský	Ajjúbovský	k2eAgMnSc1d1
egyptský	egyptský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
as-Sálih	as-Sálih	k1gMnSc1
Ajjúb	Ajjúb	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
vdova	vdova	k1gFnSc1
Šagrat	Šagrat	k1gMnSc1
Al	ala	k1gFnPc2
Durr	Durr	k1gMnSc1
jeho	jeho	k3xOp3gFnSc4
smrt	smrt	k1gFnSc4
zatajila	zatajit	k5eAaPmAgFnS
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
pod	pod	k7c7
jménem	jméno	k1gNnSc7
zemřelého	zemřelý	k1gMnSc2
sultána	sultán	k1gMnSc2
seskupit	seskupit	k5eAaPmF
mamlúckou	mamlúcký	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
vést	vést	k5eAaImF
ji	on	k3xPp3gFnSc4
do	do	k7c2
boje	boj	k1gInSc2
proti	proti	k7c3
křižákům	křižák	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
křižáckého	křižácký	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
vedená	vedený	k2eAgFnSc1d1
Robertem	Robert	k1gMnSc7
z	z	k7c2
Artois	Artois	k1gFnSc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
templáři	templář	k1gMnPc7
<g/>
,	,	kIx,
napadla	napadnout	k5eAaPmAgFnS
nepřátelský	přátelský	k2eNgInSc4d1
tábor	tábor	k1gInSc4
u	u	k7c2
Al-Mansurah	Al-Mansuraha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šagrat	Šagrat	k1gInSc1
Al	ala	k1gFnPc2
Durr	Durra	k1gFnPc2
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
ubránit	ubránit	k5eAaPmF
Al	ala	k1gFnPc2
Mansurah	Mansuraha	k1gFnPc2
za	za	k7c4
každou	každý	k3xTgFnSc4
cenu	cena	k1gFnSc4
a	a	k8xC
mamlúcký	mamlúcký	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Bajbars	Bajbarsa	k1gFnPc2
vymyslel	vymyslet	k5eAaPmAgMnS
lest	lest	k1gFnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
bylo	být	k5eAaImAgNnS
křižácké	křižácký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
poraženo	poražen	k2eAgNnSc1d1
a	a	k8xC
Robert	Robert	k1gMnSc1
z	z	k7c2
Artois	Artois	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
zabit	zabít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
a	a	k8xC
Markéta	Markéta	k1gFnSc1
Provensálská	provensálský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
křižácké	křižácký	k2eAgFnPc1d1
síly	síla	k1gFnPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
samotného	samotný	k2eAgMnSc2d1
Ludvíka	Ludvík	k1gMnSc2
byly	být	k5eAaImAgFnP
mezitím	mezitím	k6eAd1
napadeny	napaden	k2eAgFnPc1d1
Mamlúky	Mamlúka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
vedl	vést	k5eAaImAgMnS
budoucí	budoucí	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
al-Muazzam	al-Muazzam	k6eAd1
Turanšáh	Turanšáh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
byl	být	k5eAaImAgMnS
poražen	porazit	k5eAaPmNgMnS
<g/>
,	,	kIx,
odmítnul	odmítnout	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
stáhnout	stáhnout	k5eAaPmF
zpět	zpět	k6eAd1
k	k	k7c3
Damiettě	Damietta	k1gFnSc3
a	a	k8xC
naopak	naopak	k6eAd1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
dál	daleko	k6eAd2
obléhat	obléhat	k5eAaImF
Al	ala	k1gFnPc2
Mansurah	Mansuraha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obléhání	obléhání	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
katastrofou	katastrofa	k1gFnSc7
a	a	k8xC
křižáci	křižák	k1gMnPc1
trpěli	trpět	k5eAaImAgMnP
strašným	strašný	k2eAgInSc7d1
hladem	hlad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
dal	dát	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
pokyn	pokyn	k1gInSc4
k	k	k7c3
návratu	návrat	k1gInSc3
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
1250	#num#	k4
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
Damiettě	Damietta	k1gFnSc3
byl	být	k5eAaImAgMnS
Ludvík	Ludvík	k1gMnSc1
zajat	zajmout	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
onemocněl	onemocnět	k5eAaPmAgInS
úplavicí	úplavice	k1gFnSc7
a	a	k8xC
zachránili	zachránit	k5eAaPmAgMnP
ho	on	k3xPp3gMnSc4
až	až	k6eAd1
arabští	arabský	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těhotná	těhotná	k1gFnSc1
manželka	manželka	k1gFnSc1
Markéta	Markéta	k1gFnSc1
stále	stále	k6eAd1
čekala	čekat	k5eAaImAgFnS
na	na	k7c4
králův	králův	k2eAgInSc4d1
návrat	návrat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
do	do	k7c2
města	město	k1gNnSc2
došla	dojít	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
o	o	k7c6
zajatcích	zajatec	k1gMnPc6
<g/>
,	,	kIx,
chystala	chystat	k5eAaImAgFnS
se	se	k3xPyFc4
francouzská	francouzský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
k	k	k7c3
návratu	návrat	k1gInSc3
do	do	k7c2
vlasti	vlast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
projevila	projevit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
inteligenci	inteligence	k1gFnSc4
a	a	k8xC
odvahu	odvaha	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
těsně	těsně	k6eAd1
po	po	k7c6
porodu	porod	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podařilo	podařit	k5eAaPmAgNnS
přesvědčit	přesvědčit	k5eAaPmF
své	svůj	k3xOyFgMnPc4
důstojníky	důstojník	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
místo	místo	k1gNnSc4
balení	balení	k1gNnSc2
a	a	k8xC
odchodu	odchod	k1gInSc2
začali	začít	k5eAaPmAgMnP
vyjednávat	vyjednávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
byl	být	k5eAaImAgMnS
Ludvík	Ludvík	k1gMnSc1
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
ženě	žena	k1gFnSc3
ze	z	k7c2
zajetí	zajetí	k1gNnSc2
vykoupen	vykoupit	k5eAaPmNgInS
výměnou	výměna	k1gFnSc7
za	za	k7c4
město	město	k1gNnSc4
Damietta	Damiett	k1gInSc2
a	a	k8xC
400	#num#	k4
000	#num#	k4
liver	livra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžitě	okamžitě	k6eAd1
poté	poté	k6eAd1
odplul	odplout	k5eAaPmAgMnS
z	z	k7c2
Egypta	Egypt	k1gInSc2
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
,	,	kIx,
jedné	jeden	k4xCgFnSc2
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
křesťanských	křesťanský	k2eAgFnPc2d1
držav	država	k1gFnPc2
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1250	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
egyptský	egyptský	k2eAgMnSc1d1
sultán	sultán	k1gMnSc1
al-Muazzam	al-Muazzam	k6eAd1
Turanšáh	Turanšáh	k1gInSc4
vzbouřivšími	vzbouřivší	k2eAgFnPc7d1
se	se	k3xPyFc4
Mamlúky	Mamlúka	k1gFnSc2
a	a	k8xC
moci	moc	k1gFnSc2
se	se	k3xPyFc4
ujala	ujmout	k5eAaPmAgFnS
Šagrat	Šagrat	k1gInSc4
Al	ala	k1gFnPc2
Durr	Durra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
necelých	celý	k2eNgInPc6d1
třech	tři	k4xCgInPc6
měsících	měsíc	k1gInPc6
její	její	k3xOp3gInSc4
vláda	vláda	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
a	a	k8xC
sultánem	sultán	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mamlúcký	mamlúcký	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Mu	on	k3xPp3gMnSc3
<g/>
‘	‘	k?
<g/>
izzuddín	izzuddín	k1gMnSc1
Ajbak	Ajbak	k1gMnSc1
<g/>
,	,	kIx,
za	za	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
Šagrat	Šagrat	k1gMnSc1
Al	ala	k1gFnPc2
Durr	Durr	k1gInSc4
provdala	provdat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
s	s	k7c7
Mamlúky	Mamlúek	k1gMnPc7
spojil	spojit	k5eAaPmAgMnS
a	a	k8xC
pomalu	pomalu	k6eAd1
začal	začít	k5eAaPmAgInS
obnovovat	obnovovat	k5eAaImF
křižácké	křižácký	k2eAgFnPc4d1
državy	država	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
na	na	k7c6
východě	východ	k1gInSc6
objevili	objevit	k5eAaPmAgMnP
Mongolové	Mongol	k1gMnPc1
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
také	také	k9
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
jejich	jejich	k3xOp3gMnSc7
chánem	chán	k1gMnSc7
Möngkem	Möngek	k1gMnSc7
<g/>
,	,	kIx,
vnukem	vnuk	k1gMnSc7
Čingischána	Čingischán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevěděl	vědět	k5eNaImAgMnS
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
Mongoly	Mongol	k1gMnPc7
vyjednávají	vyjednávat	k5eAaImIp3nP
také	také	k9
muslimové	muslim	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
i	i	k8xC
křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
Mongoly	Mongol	k1gMnPc4
získat	získat	k5eAaPmF
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
proti	proti	k7c3
svému	svůj	k3xOyFgMnSc3
soupeři	soupeř	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
netušila	tušit	k5eNaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Mongolové	Mongol	k1gMnPc1
nemají	mít	k5eNaImIp3nP
nejmenší	malý	k2eAgInSc4d3
zájem	zájem	k1gInSc4
některé	některý	k3yIgFnPc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
pomáhat	pomáhat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
spojenectví	spojenectví	k1gNnSc2
sešlo	sejít	k5eAaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
Möngke	Möngke	k1gNnSc7
odmítnul	odmítnout	k5eAaPmAgMnS
Ludvíkem	Ludvík	k1gMnSc7
nabídnutý	nabídnutý	k2eAgInSc1d1
křest	křest	k1gInSc1
a	a	k8xC
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
se	se	k3xPyFc4
odmítnul	odmítnout	k5eAaPmAgMnS
chánovi	chán	k1gMnSc6
podrobit	podrobit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Francouzskému	francouzský	k2eAgMnSc3d1
králi	král	k1gMnSc3
začaly	začít	k5eAaPmAgInP
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
docházet	docházet	k5eAaImF
peníze	peníz	k1gInPc4
a	a	k8xC
navíc	navíc	k6eAd1
ho	on	k3xPp3gMnSc4
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
doma	doma	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
právě	právě	k9
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gMnPc3
matka	matka	k1gFnSc1
regentka	regentka	k1gFnSc1
Blanka	Blanka	k1gFnSc1
Kastilská	kastilský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
Ludvík	Ludvík	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
pro	pro	k7c4
cestu	cesta	k1gFnSc4
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
výprava	výprava	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
neúspěchem	neúspěch	k1gInSc7
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
si	se	k3xPyFc3
tímto	tento	k3xDgInSc7
činem	čin	k1gInSc7
velkou	velký	k2eAgFnSc4d1
úctu	úcta	k1gFnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
křesťanské	křesťanský	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
časem	časem	k6eAd1
byl	být	k5eAaImAgInS
dokonce	dokonce	k9
prohlášen	prohlášen	k2eAgMnSc1d1
za	za	k7c4
svatého	svatý	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HARLANSSON	HARLANSSON	kA
<g/>
,	,	kIx,
Kali	Kali	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crusades	Crusades	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2004-11-24	2004-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TATE	TATE	kA
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížáci	Křížák	k1gMnPc1
v	v	k7c6
Orientu	Orient	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Slovart	Slovarta	k1gFnPc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85871	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
228	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
EHLERS	EHLERS	kA
<g/>
,	,	kIx,
Joachim	Joachim	k1gMnSc1
<g/>
;	;	kIx,
MÜLLER	MÜLLER	kA
<g/>
,	,	kIx,
Heribert	Heribert	k1gMnSc1
<g/>
;	;	kIx,
SCHNEIDMÜLLER	SCHNEIDMÜLLER	kA
<g/>
,	,	kIx,
Bernd	Bernd	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzští	francouzštit	k5eAaImIp3nS
králové	králová	k1gFnPc4
v	v	k7c6
období	období	k1gNnSc6
středověku	středověk	k1gInSc2
:	:	kIx,
od	od	k7c2
Oda	Oda	k1gFnSc2
ke	k	k7c3
Karlu	Karel	k1gMnSc3
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
888	#num#	k4
<g/>
-	-	kIx~
<g/>
1498	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
420	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
465	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FERRO	Ferro	k1gNnSc1
<g/>
,	,	kIx,
Marc	Marc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
692	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
888	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
;	;	kIx,
HROCH	Hroch	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
621	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
</s>
<s>
JOINVILLE	JOINVILLE	kA
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
de	de	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměti	paměť	k1gFnSc2
křižákovy	křižákův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
krásné	krásný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
.	.	kIx.
206	#num#	k4
s.	s.	k?
</s>
<s>
JOINVILLU	JOINVILLU	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
ze	z	k7c2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Ludvíka	Ludvík	k1gMnSc2
Svatého	svatý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
256	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
1270	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LE	LE	kA
GOFF	GOFF	kA
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatý	svatý	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
724	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
685	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
READ	READ	kA
<g/>
,	,	kIx,
Piers	Piers	k1gInSc4
Paul	Paula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Templáři	templář	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
335	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7257	#num#	k4
<g/>
-	-	kIx~
<g/>
406	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
TATE	TATE	kA
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Orientu	Orient	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85871	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLFF	WOLFF	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HAZARD	hazard	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gMnPc4
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
later	later	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
,	,	kIx,
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
871	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jean	Jean	k1gMnSc1
de	de	k?
Joinville	Joinville	k1gInSc1
</s>
<s>
sv.	sv.	kA
Ludvík	Ludvík	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sedmá	sedmý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc1
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc1
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gMnPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgMnSc7d1
Turkům	turek	k1gInPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
543373	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4139113-5	4139113-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034388	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034388	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
