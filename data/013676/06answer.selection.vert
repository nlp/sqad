<s>
Kalifornium	kalifornium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Cf	Cf	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Californium	Californium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
desátý	desátý	k4xOgMnSc1
člen	člen	k1gMnSc1
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
šestý	šestý	k4xOgInSc4
transuran	transuran	k1gInSc4
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
curia	curium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>