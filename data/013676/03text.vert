<s>
Kalifornium	kalifornium	k1gNnSc1
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
251	#num#	k4
</s>
<s>
Cf	Cf	k?
</s>
<s>
98	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
<g/>
,	,	kIx,
Cf	Cf	k1gFnSc1
<g/>
,	,	kIx,
98	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Californium	Californium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
kovově	kovově	k6eAd1
stříbrné	stříbrný	k2eAgFnPc4d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-71-3	7440-71-3	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
251,080	251,080	k4
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
Cf	Cf	k?
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
117	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Cf	Cf	k1gFnSc1
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
99	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
<g/>
Cf	Cf	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
86	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
10	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
6,68	6,68	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
12,6	12,6	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
22,1	22,1	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
hexagonální	hexagonální	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
přibližně	přibližně	k6eAd1
14	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
900	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
173,15	173,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
1470	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
743,15	743,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dy	Dy	k?
<g/>
⋏	⋏	k?
</s>
<s>
Berkelium	Berkelium	k1gNnSc1
≺	≺	k?
<g/>
Cf	Cf	k1gMnSc2
<g/>
≻	≻	k?
Einsteinium	einsteinium	k1gNnSc1
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Cf	Cf	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Californium	Californium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
desátý	desátý	k4xOgMnSc1
člen	člen	k1gMnSc1
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
šestý	šestý	k4xOgInSc4
transuran	transuran	k1gInSc4
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
ozařováním	ozařování	k1gNnSc7
jader	jádro	k1gNnPc2
curia	curium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
kalifornium	kalifornium	k1gNnSc4
za	za	k7c2
běžného	běžný	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
existují	existovat	k5eAaImIp3nP
tři	tři	k4xCgFnPc1
krystalické	krystalický	k2eAgFnPc1d1
formy	forma	k1gFnPc1
<g/>
:	:	kIx,
jedna	jeden	k4xCgFnSc1
nad	nad	k7c4
a	a	k8xC
jedna	jeden	k4xCgFnSc1
pod	pod	k7c4
900	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1650	#num#	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
forma	forma	k1gFnSc1
existuje	existovat	k5eAaImIp3nS
při	při	k7c6
vysokém	vysoký	k2eAgInSc6d1
tlaku	tlak	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalifornium	kalifornium	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c6
vzduchu	vzduch	k1gInSc6
při	při	k7c6
pokojové	pokojový	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
pokrývá	pokrývat	k5eAaImIp3nS
vrstvou	vrstva	k1gFnSc7
oxidu	oxid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloučeninám	sloučenina	k1gFnPc3
kalifornia	kalifornium	k1gNnSc2
dominuje	dominovat	k5eAaImIp3nS
oxidační	oxidační	k2eAgInSc4d1
stav	stav	k1gInSc4
+3	+3	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstabilnějším	stabilní	k2eAgNnSc7d3
z	z	k7c2
dvaceti	dvacet	k4xCc2
známých	známý	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
kalifornia	kalifornium	k1gNnSc2
je	být	k5eAaImIp3nS
kalifornium-	kalifornium-	k?
<g/>
251	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc2
je	být	k5eAaImIp3nS
898	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
krátký	krátký	k2eAgInSc4d1
poločas	poločas	k1gInSc4
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
prvek	prvek	k1gInSc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
nenachází	nacházet	k5eNaImIp3nS
ve	v	k7c6
významném	významný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalifornium-	Kalifornium-	k1gFnSc1
<g/>
252	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
asi	asi	k9
2	#num#	k4
645	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pozorován	pozorovat	k5eAaImNgInS
emisní	emisní	k2eAgFnSc7d1
spektroskopií	spektroskopie	k1gFnSc7
u	u	k7c2
zbytků	zbytek	k1gInPc2
supernov	supernova	k1gFnPc2
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
lze	lze	k6eAd1
opatrně	opatrně	k6eAd1
hypoteticky	hypoteticky	k6eAd1
usoudit	usoudit	k5eAaPmF
že	že	k8xS
exploze	exploze	k1gFnSc1
supernovy	supernova	k1gFnSc2
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
vyvolána	vyvolat	k5eAaPmNgFnS
nahromaděním	nahromadění	k1gNnSc7
kritického	kritický	k2eAgInSc2d1
poměru	poměr	k1gInSc2
štěpného	štěpný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
právě	právě	k6eAd1
252	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
nejběžnějším	běžný	k2eAgInSc7d3
izotopem	izotop	k1gInSc7
kalifornia	kalifornium	k1gNnSc2
vytvořeným	vytvořený	k2eAgMnSc7d1
člověkem	člověk	k1gMnSc7
a	a	k8xC
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
Oak	Oak	k1gFnSc2
Ridge	Ridg	k1gFnSc2
National	National	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laboratoř	laboratoř	k1gFnSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
Výzkumný	výzkumný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
atomových	atomový	k2eAgMnPc2d1
reaktorů	reaktor	k1gInPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mála	málo	k4c2
transuranů	transuran	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
praktické	praktický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
aplikací	aplikace	k1gFnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
schopnosti	schopnost	k1gFnPc4
určitých	určitý	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
kalifornia	kalifornium	k1gNnSc2
k	k	k7c3
emisi	emise	k1gFnSc3
neutronů	neutron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalifornium	kalifornium	k1gNnSc1
lze	lze	k6eAd1
použít	použít	k5eAaPmF
například	například	k6eAd1
k	k	k7c3
nastartování	nastartování	k1gNnSc3
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
zdroj	zdroj	k1gInSc1
neutronů	neutron	k1gInPc2
při	při	k7c6
studiu	studio	k1gNnSc6
materiálů	materiál	k1gInPc2
pomocí	pomocí	k7c2
neutronové	neutronový	k2eAgFnSc2d1
difrakce	difrakce	k1gFnSc2
a	a	k8xC
neutronové	neutronový	k2eAgFnSc2d1
spektroskopie	spektroskopie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalifornium	kalifornium	k1gNnSc1
lze	lze	k6eAd1
také	také	k9
použít	použít	k5eAaPmF
pro	pro	k7c4
jadernou	jaderný	k2eAgFnSc4d1
syntézu	syntéza	k1gFnSc4
těžších	těžký	k2eAgInPc2d2
prvků	prvek	k1gInPc2
<g/>
;	;	kIx,
oganesson	oganesson	k1gInSc1
byl	být	k5eAaImAgInS
syntetizován	syntetizovat	k5eAaImNgInS
bombardováním	bombardování	k1gNnSc7
izotopu	izotop	k1gInSc2
kalifornia-	kalifornia-	k?
<g/>
249	#num#	k4
ionty	ion	k1gInPc7
vápníku-	vápníku-	k?
<g/>
48	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
práci	práce	k1gFnSc6
s	s	k7c7
kaliforniem	kalifornium	k1gNnSc7
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
dbát	dbát	k5eAaImF
na	na	k7c4
dostatečnou	dostatečný	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
má	mít	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
narušit	narušit	k5eAaPmF
tvorbu	tvorba	k1gFnSc4
červených	červený	k2eAgFnPc2d1
krvinek	krvinka	k1gFnPc2
bioakumulací	bioakumulace	k1gFnPc2
v	v	k7c6
kosterní	kosterní	k2eAgFnSc6d1
tkáni	tkáň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fyzikálně-chemické	Fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
je	být	k5eAaImIp3nS
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
aktinidový	aktinidový	k2eAgInSc1d1
kov	kov	k1gInSc1
s	s	k7c7
bodem	bod	k1gInSc7
tání	tání	k1gNnPc2
900	#num#	k4
±	±	k?
30	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
650	#num#	k4
±	±	k?
50	#num#	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
a	a	k8xC
odhadovanou	odhadovaný	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
varu	var	k1gInSc2
1	#num#	k4
745	#num#	k4
K	k	k7c3
(	(	kIx(
<g/>
1	#num#	k4
470	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
;	;	kIx,
2	#num#	k4
680	#num#	k4
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čistý	čistý	k2eAgInSc1d1
kov	kov	k1gInSc1
je	být	k5eAaImIp3nS
tvárný	tvárný	k2eAgInSc1d1
a	a	k8xC
lze	lze	k6eAd1
ho	on	k3xPp3gInSc4
snadno	snadno	k6eAd1
řezat	řezat	k5eAaImF
žiletkou	žiletka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vakuu	vakuum	k1gNnSc6
se	se	k3xPyFc4
kalifornium	kalifornium	k1gNnSc1
začne	začít	k5eAaPmIp3nS
odpařovat	odpařovat	k5eAaImF
nad	nad	k7c7
teplotou	teplota	k1gFnSc7
300	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Pod	pod	k7c4
51	#num#	k4
K	k	k7c3
(	(	kIx(
<g/>
−	−	k?
<g/>
222	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
;	;	kIx,
−	−	k?
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
buď	buď	k8xC
feromagnetický	feromagnetický	k2eAgMnSc1d1
nebo	nebo	k8xC
ferimagnetický	ferimagnetický	k2eAgMnSc1d1
(	(	kIx(
<g/>
chová	chovat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
magnet	magnet	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezi	mezi	k7c7
48	#num#	k4
a	a	k8xC
66	#num#	k4
K	K	kA
je	být	k5eAaImIp3nS
antiferomagnetický	antiferomagnetický	k2eAgInSc1d1
(	(	kIx(
<g/>
mezilehlý	mezilehlý	k2eAgInSc1d1
stav	stav	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
nad	nad	k7c4
160	#num#	k4
K	k	k7c3
(	(	kIx(
<g/>
−	−	k?
<g/>
113	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
;	;	kIx,
−	−	k?
°	°	k?
<g/>
F	F	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
paramagnetický	paramagnetický	k2eAgMnSc1d1
(	(	kIx(
<g/>
vnější	vnější	k2eAgNnSc1d1
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
jej	on	k3xPp3gMnSc4
může	moct	k5eAaImIp3nS
učinit	učinit	k5eAaPmF,k5eAaImF
magnetickým	magnetický	k2eAgNnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
slitiny	slitina	k1gFnPc4
s	s	k7c7
kovy	kov	k1gInPc7
lanthanoidů	lanthanoid	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c6
výsledných	výsledný	k2eAgInPc6d1
materiálech	materiál	k1gInPc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
jen	jen	k9
málo	málo	k4c1
<g/>
.	.	kIx.
</s>
<s>
Prvek	prvek	k1gInSc1
má	mít	k5eAaImIp3nS
při	při	k7c6
standardním	standardní	k2eAgInSc6d1
atmosférickém	atmosférický	k2eAgInSc6d1
tlaku	tlak	k1gInSc6
dvě	dva	k4xCgFnPc4
krystalické	krystalický	k2eAgFnPc4d1
formy	forma	k1gFnPc4
<g/>
:	:	kIx,
dvojitě	dvojitě	k6eAd1
hexagonální	hexagonální	k2eAgFnSc4d1
uzavřenou	uzavřený	k2eAgFnSc4d1
formu	forma	k1gFnSc4
nazvanou	nazvaný	k2eAgFnSc4d1
alfa	alfa	k1gNnSc4
(	(	kIx(
<g/>
α	α	k?
<g/>
)	)	kIx)
a	a	k8xC
kubickou	kubický	k2eAgFnSc4d1
formu	forma	k1gFnSc4
zaměřenou	zaměřený	k2eAgFnSc4d1
na	na	k7c4
označenou	označený	k2eAgFnSc4d1
beta	beta	k1gNnSc1
(	(	kIx(
<g/>
β	β	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forma	forma	k1gFnSc1
α	α	k?
existuje	existovat	k5eAaImIp3nS
pod	pod	k7c7
600	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
°	°	k?
<g/>
C	C	kA
s	s	k7c7
hustotou	hustota	k1gFnSc7
15,10	15,10	k4
g	g	kA
/	/	kIx~
cm	cm	kA
<g/>
3	#num#	k4
a	a	k8xC
forma	forma	k1gFnSc1
β	β	k?
existuje	existovat	k5eAaImIp3nS
nad	nad	k7c7
600	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
°	°	k?
<g/>
C	C	kA
s	s	k7c7
hustotou	hustota	k1gFnSc7
8,74	8,74	k4
g	g	kA
/	/	kIx~
cm	cm	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tlaku	tlak	k1gInSc6
48	#num#	k4
GPa	GPa	k1gFnPc2
se	se	k3xPyFc4
forma	forma	k1gFnSc1
β	β	k?
mění	měnit	k5eAaImIp3nS
na	na	k7c4
orthorombický	orthorombický	k2eAgInSc4d1
krystalický	krystalický	k2eAgInSc4d1
systém	systém	k1gInSc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
delokalizace	delokalizace	k1gFnSc2
atomů	atom	k1gInPc2
5	#num#	k4
<g/>
f	f	k?
elektronů	elektron	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	on	k3xPp3gMnPc4
uvolňuje	uvolňovat	k5eAaImIp3nS
k	k	k7c3
vazbě	vazba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Sypný	sypný	k2eAgInSc1d1
modul	modul	k1gInSc1
materiálu	materiál	k1gInSc2
je	být	k5eAaImIp3nS
měřítkem	měřítko	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
odolnosti	odolnost	k1gFnSc2
vůči	vůči	k7c3
rovnoměrnému	rovnoměrný	k2eAgInSc3d1
tlaku	tlak	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
kalifornia	kalifornium	k1gNnSc2
je	být	k5eAaImIp3nS
objemový	objemový	k2eAgInSc1d1
modul	modul	k1gInSc1
50	#num#	k4
±	±	k?
5	#num#	k4
GPa	GPa	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
podobné	podobný	k2eAgNnSc1d1
trojmocným	trojmocný	k2eAgMnPc3d1
kovům	kov	k1gInPc3
lanthanoidu	lanthanoid	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
menší	malý	k2eAgInPc1d2
než	než	k8xS
užívanější	užívaný	k2eAgInPc1d2
kovy	kov	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
hliník	hliník	k1gInSc1
(	(	kIx(
<g/>
70	#num#	k4
GPa	GPa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1950	#num#	k4
bombardováním	bombardování	k1gNnSc7
izotopu	izotop	k1gInSc2
curia	curium	k1gNnSc2
242	#num#	k4
<g/>
Cm	cm	kA
částicemi	částice	k1gFnPc7
α	α	k?
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
jaderné	jaderný	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
kalifornské	kalifornský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c4
Berkeley	Berkele	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
tak	tak	k9
izotop	izotop	k1gInSc4
245	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
44	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jeho	jeho	k3xOp3gMnPc4
objevitele	objevitel	k1gMnPc4
jsou	být	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
Glenn	Glenna	k1gFnPc2
T.	T.	kA
Seaborg	Seaborg	k1gMnSc1
<g/>
,	,	kIx,
Stanley	Stanlea	k1gMnSc2
G.	G.	kA
Thompson	Thompson	k1gMnSc1
a	a	k8xC
Albert	Albert	k1gMnSc1
Ghiorso	Ghiorsa	k1gFnSc5
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jej	on	k3xPp3gMnSc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
podle	podle	k7c2
federálního	federální	k2eAgInSc2d1
státu	stát	k1gInSc2
USA	USA	kA
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
syntetizováno	syntetizován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
24296	#num#	k4
Cm	cm	kA
+	+	kIx~
42	#num#	k4
He	he	k0
→	→	k?
24598	#num#	k4
Cf	Cf	k1gFnPc2
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Izotopy	izotop	k1gInPc4
kalifornia	kalifornium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
20	#num#	k4
izotopů	izotop	k1gInPc2
kalifornia	kalifornium	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jsou	být	k5eAaImIp3nP
nejstabilnější	stabilní	k2eAgInSc4d3
251	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
898	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
249	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
351	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
250	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
zbývající	zbývající	k2eAgInPc1d1
radioaktivní	radioaktivní	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
mají	mít	k5eAaImIp3nP
poločas	poločas	k1gInSc4
přeměny	přeměna	k1gFnSc2
menší	malý	k2eAgFnSc2d2
než	než	k8xS
3	#num#	k4
roky	rok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1
využití	využití	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
kalifornium	kalifornium	k1gNnSc4
především	především	k6eAd1
jako	jako	k8xC,k8xS
silný	silný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
neutronů	neutron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kalifornium	kalifornium	k1gNnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
nastartování	nastartování	k1gNnSc4
řetězové	řetězový	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
prvním	první	k4xOgNnSc6
uvádění	uvádění	k1gNnSc6
do	do	k7c2
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lékařství	lékařství	k1gNnSc6
je	být	k5eAaImIp3nS
kalifornium	kalifornium	k1gNnSc1
používáno	používat	k5eAaImNgNnS
pro	pro	k7c4
ozařování	ozařování	k1gNnSc4
rakovinných	rakovinný	k2eAgInPc2d1
nádorů	nádor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
kontrole	kontrola	k1gFnSc6
vad	vada	k1gFnPc2
materiálů	materiál	k1gInPc2
je	být	k5eAaImIp3nS
kalifornium	kalifornium	k1gNnSc1
zdrojem	zdroj	k1gInSc7
stabilního	stabilní	k2eAgInSc2d1
neutronového	neutronový	k2eAgInSc2d1
toku	tok	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
po	po	k7c6
průchodu	průchod	k1gInSc6
testovaným	testovaný	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
(	(	kIx(
<g/>
součásti	součást	k1gFnPc1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
turbíny	turbína	k1gFnSc2
<g/>
,	,	kIx,
tlakové	tlakový	k2eAgFnSc2d1
nádoby	nádoba	k1gFnSc2
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
informaci	informace	k1gFnSc4
o	o	k7c6
možných	možný	k2eAgFnPc6d1
skrytých	skrytý	k2eAgFnPc6d1
vadách	vada	k1gFnPc6
jako	jako	k8xS,k8xC
trhliny	trhlina	k1gFnSc2
<g/>
,	,	kIx,
zlomy	zlom	k1gInPc1
<g/>
,	,	kIx,
oslabená	oslabený	k2eAgNnPc1d1
místa	místo	k1gNnPc1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Californium	Californium	k1gNnSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kalifornium	kalifornium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kalifornium	kalifornium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4147208-1	4147208-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
89004338	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
89004338	#num#	k4
</s>
