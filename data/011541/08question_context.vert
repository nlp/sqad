<s>
Lech	Lech	k1gMnSc1	Lech
Wałęsa	Wałęs	k1gMnSc2	Wałęs
[	[	kIx(	[
<g/>
vauensa	vauens	k1gMnSc2	vauens
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1943	[number]	k4	1943
Popowo	Popowo	k6eAd1	Popowo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgMnSc1d1	polský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
nezávislých	závislý	k2eNgInPc2d1	nezávislý
odborů	odbor	k1gInPc2	odbor
Solidarita	solidarita	k1gFnSc1	solidarita
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
byl	být	k5eAaImAgMnS	být
elektrikář	elektrikář	k1gMnSc1	elektrikář
v	v	k7c6	v
Gdaňských	gdaňský	k2eAgFnPc6d1	Gdaňská
loděnicích	loděnice	k1gFnPc6	loděnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
a	a	k8xC	a
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1980	[number]	k4	1980
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stávky	stávka	k1gFnSc2	stávka
a	a	k8xC	a
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
odborová	odborový	k2eAgFnSc1d1	odborová
organizace	organizace	k1gFnSc1	organizace
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>

