<s>
Sádlo	sádlo	k1gNnSc1	sádlo
je	být	k5eAaImIp3nS	být
živočišný	živočišný	k2eAgInSc4d1	živočišný
tuk	tuk	k1gInSc4	tuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžné	běžný	k2eAgNnSc1d1	běžné
je	být	k5eAaImIp3nS	být
i	i	k9	i
husí	husí	k2eAgFnSc1d1	husí
<g/>
,	,	kIx,	,
kachní	kachní	k2eAgFnSc1d1	kachní
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Sádlo	sádlo	k1gNnSc1	sádlo
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kuchyních	kuchyně	k1gFnPc6	kuchyně
na	na	k7c6	na
vaření	vaření	k1gNnSc6	vaření
<g/>
,	,	kIx,	,
smažení	smažení	k1gNnSc6	smažení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
studenou	studený	k2eAgFnSc4d1	studená
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
takzvaně	takzvaně	k6eAd1	takzvaně
zdravý	zdravý	k2eAgInSc4d1	zdravý
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
propaguje	propagovat	k5eAaImIp3nS	propagovat
částečně	částečně	k6eAd1	částečně
hydrogenované	hydrogenovaný	k2eAgInPc4d1	hydrogenovaný
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
chronicky	chronicky	k6eAd1	chronicky
implikované	implikovaný	k2eAgFnPc1d1	implikovaná
v	v	k7c6	v
rostoucím	rostoucí	k2eAgInSc6d1	rostoucí
výskytu	výskyt	k1gInSc6	výskyt
autoimunních	autoimunní	k2eAgNnPc2d1	autoimunní
onemocnění	onemocnění	k1gNnPc2	onemocnění
-	-	kIx~	-
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
trans	trans	k1gInSc1	trans
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Vepřové	vepřový	k2eAgNnSc1d1	vepřové
sádlo	sádlo	k1gNnSc1	sádlo
se	se	k3xPyFc4	se
také	také	k9	také
stále	stále	k6eAd1	stále
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
mýdla	mýdlo	k1gNnSc2	mýdlo
či	či	k8xC	či
mastí	mast	k1gFnPc2	mast
<g/>
.	.	kIx.	.
</s>
<s>
Sádlo	sádlo	k1gNnSc1	sádlo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
kalorické	kalorický	k2eAgNnSc1d1	kalorické
(	(	kIx(	(
<g/>
vepřové	vepřové	k1gNnSc1	vepřové
přes	přes	k7c4	přes
3	[number]	k4	3
600	[number]	k4	600
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
g	g	kA	g
(	(	kIx(	(
<g/>
64	[number]	k4	64
kcal	kcalum	k1gNnPc2	kcalum
na	na	k7c4	na
polévkovou	polévkový	k2eAgFnSc4d1	polévková
lžíci	lžíce	k1gFnSc4	lžíce
<g/>
))	))	k?	))
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
tuků	tuk	k1gInPc2	tuk
(	(	kIx(	(
<g/>
99	[number]	k4	99
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nasycených	nasycený	k2eAgFnPc2d1	nasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
poznatky	poznatek	k1gInPc1	poznatek
nepotvrdily	potvrdit	k5eNaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sádlo	sádlo	k1gNnSc1	sádlo
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
,	,	kIx,	,
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
tak	tak	k6eAd1	tak
srdeční	srdeční	k2eAgNnSc1d1	srdeční
a	a	k8xC	a
cévní	cévní	k2eAgNnSc1d1	cévní
onemocnění	onemocnění	k1gNnSc1	onemocnění
-	-	kIx~	-
tento	tento	k3xDgInSc4	tento
efekt	efekt	k1gInSc4	efekt
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
dáván	dávat	k5eAaImNgInS	dávat
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
nadměrné	nadměrný	k2eAgFnSc3d1	nadměrná
produkci	produkce	k1gFnSc3	produkce
inzulinu	inzulin	k1gInSc2	inzulin
vlivem	vlivem	k7c2	vlivem
vysokého	vysoký	k2eAgInSc2d1	vysoký
obsahu	obsah	k1gInSc2	obsah
cukrů	cukr	k1gInPc2	cukr
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
stravě	strava	k1gFnSc6	strava
<g/>
.	.	kIx.	.
</s>
<s>
Sádlo	sádlo	k1gNnSc4	sádlo
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
měli	mít	k5eAaImAgMnP	mít
jíst	jíst	k5eAaImF	jíst
v	v	k7c6	v
rozumné	rozumný	k2eAgFnSc6d1	rozumná
míře	míra	k1gFnSc6	míra
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
každodenní	každodenní	k2eAgInSc4d1	každodenní
fyzický	fyzický	k2eAgInSc4d1	fyzický
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
svižná	svižný	k2eAgFnSc1d1	svižná
chůze	chůze	k1gFnSc1	chůze
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
<g/>
,	,	kIx,	,
kolo	kolo	k1gNnSc1	kolo
apod.	apod.	kA	apod.
Při	při	k7c6	při
domácí	domácí	k2eAgFnSc6d1	domácí
přípravě	příprava	k1gFnSc6	příprava
sádla	sádlo	k1gNnSc2	sádlo
se	se	k3xPyFc4	se
nevyškvařená	vyškvařený	k2eNgFnSc1d1	vyškvařený
surovina	surovina	k1gFnSc1	surovina
nakrájí	nakrájet	k5eAaPmIp3nS	nakrájet
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
kostky	kostka	k1gFnPc4	kostka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tuk	tuk	k1gInSc1	tuk
snáze	snadno	k6eAd2	snadno
dostával	dostávat	k5eAaImAgInS	dostávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
slabém	slabý	k2eAgInSc6d1	slabý
plameni	plamen	k1gInSc6	plamen
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
,	,	kIx,	,
až	až	k8xS	až
vypustí	vypustit	k5eAaPmIp3nS	vypustit
všechnu	všechen	k3xTgFnSc4	všechen
nazlátlou	nazlátlý	k2eAgFnSc4d1	nazlátlá
šťávu	šťáva	k1gFnSc4	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Tekutý	tekutý	k2eAgInSc1d1	tekutý
tuk	tuk	k1gInSc1	tuk
se	se	k3xPyFc4	se
zcedí	zcedit	k5eAaPmIp3nS	zcedit
od	od	k7c2	od
zbylých	zbylý	k2eAgInPc2d1	zbylý
kousků	kousek	k1gInPc2	kousek
(	(	kIx(	(
<g/>
škvarků	škvarek	k1gInPc2	škvarek
<g/>
)	)	kIx)	)
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
vychladnout	vychladnout	k5eAaPmF	vychladnout
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
přidává	přidávat	k5eAaImIp3nS	přidávat
jen	jen	k9	jen
na	na	k7c4	na
prst	prst	k1gInSc4	prst
mléka	mléko	k1gNnSc2	mléko
kvůli	kvůli	k7c3	kvůli
lepší	dobrý	k2eAgFnSc3d2	lepší
barvě	barva	k1gFnSc3	barva
a	a	k8xC	a
křupavosti	křupavost	k1gFnSc3	křupavost
škvarků	škvarek	k1gInPc2	škvarek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
potřeba	potřeba	k6eAd1	potřeba
být	být	k5eAaImF	být
obezřetný	obezřetný	k2eAgInSc4d1	obezřetný
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
přidaného	přidaný	k2eAgNnSc2d1	přidané
mléka	mléko	k1gNnSc2	mléko
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
rozpálené	rozpálený	k2eAgNnSc1d1	rozpálené
sádlo	sádlo	k1gNnSc1	sádlo
může	moct	k5eAaImIp3nS	moct
vypěnit	vypěnit	k5eAaPmF	vypěnit
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
se	se	k3xPyFc4	se
vylít	vylít	k5eAaPmF	vylít
z	z	k7c2	z
hrnce	hrnec	k1gInSc2	hrnec
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
k	k	k7c3	k
vážnému	vážný	k2eAgNnSc3d1	vážné
popálení	popálení	k1gNnSc3	popálení
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
domácí	domácí	k2eAgNnSc1d1	domácí
sádlo	sádlo	k1gNnSc1	sádlo
slévá	slévat	k5eAaImIp3nS	slévat
do	do	k7c2	do
sklenic	sklenice	k1gFnPc2	sklenice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
dostatečně	dostatečně	k6eAd1	dostatečně
důkladně	důkladně	k6eAd1	důkladně
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
a	a	k8xC	a
otočí	otočit	k5eAaPmIp3nP	otočit
dnem	den	k1gInSc7	den
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
horkem	horko	k1gNnSc7	horko
se	se	k3xPyFc4	se
tak	tak	k9	tak
prostředí	prostředí	k1gNnSc3	prostředí
uvnitř	uvnitř	k6eAd1	uvnitř
samo	sám	k3xTgNnSc1	sám
sterilizuje	sterilizovat	k5eAaImIp3nS	sterilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
částečném	částečný	k2eAgNnSc6d1	částečné
zchladnutí	zchladnutí	k1gNnSc6	zchladnutí
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
za	za	k7c2	za
tekutého	tekutý	k2eAgInSc2d1	tekutý
stavu	stav	k1gInSc2	stav
sklenice	sklenice	k1gFnSc2	sklenice
převrátí	převrátit	k5eAaPmIp3nS	převrátit
opatrně	opatrně	k6eAd1	opatrně
bez	bez	k7c2	bez
nárazů	náraz	k1gInPc2	náraz
zpátky	zpátky	k6eAd1	zpátky
a	a	k8xC	a
sádlo	sádlo	k1gNnSc1	sádlo
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
dochladnout	dochladnout	k5eAaPmF	dochladnout
do	do	k7c2	do
pevné	pevný	k2eAgFnSc2d1	pevná
bílé	bílý	k2eAgFnSc2d1	bílá
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
sádlo	sádlo	k1gNnSc1	sádlo
připraveno	připravit	k5eAaPmNgNnS	připravit
na	na	k7c4	na
dlouhodobější	dlouhodobý	k2eAgNnSc4d2	dlouhodobější
skladování	skladování	k1gNnSc4	skladování
např.	např.	kA	např.
ve	v	k7c6	v
spíži	spíž	k1gFnSc6	spíž
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
chuť	chuť	k1gFnSc4	chuť
v	v	k7c6	v
sádle	sádlo	k1gNnSc6	sádlo
nechává	nechávat	k5eAaImIp3nS	nechávat
i	i	k9	i
část	část	k1gFnSc1	část
škvarků	škvarek	k1gInPc2	škvarek
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
škvarkové	škvarkový	k2eAgNnSc1d1	škvarkové
sádlo	sádlo	k1gNnSc1	sádlo
<g/>
.	.	kIx.	.
</s>
<s>
Kulinářské	kulinářský	k2eAgFnPc1d1	kulinářská
vlastnosti	vlastnost	k1gFnPc1	vlastnost
vepřového	vepřový	k2eAgNnSc2d1	vepřové
sádla	sádlo	k1gNnSc2	sádlo
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
části	část	k1gFnSc6	část
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
tuk	tuk	k1gInSc1	tuk
získán	získán	k2eAgInSc1d1	získán
<g/>
:	:	kIx,	:
řemenové	řemenový	k2eAgNnSc1d1	řemenový
sádlo	sádlo	k1gNnSc1	sádlo
–	–	k?	–
z	z	k7c2	z
podkožního	podkožní	k2eAgInSc2d1	podkožní
tuku	tuk	k1gInSc2	tuk
kromě	kromě	k7c2	kromě
laloku	lalok	k1gInSc2	lalok
plstní	plstní	k2eAgNnSc1d1	plstní
sádlo	sádlo	k1gNnSc1	sádlo
–	–	k?	–
sádlo	sádlo	k1gNnSc4	sádlo
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
plsti	plst	k1gFnSc2	plst
<g/>
"	"	kIx"	"
tj.	tj.	kA	tj.
tuku	tuk	k1gInSc2	tuk
uloženého	uložený	k2eAgInSc2d1	uložený
v	v	k7c6	v
břišní	břišní	k2eAgFnSc6d1	břišní
dutině	dutina	k1gFnSc6	dutina
pod	pod	k7c7	pod
hřbetem	hřbet	k1gInSc7	hřbet
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
)	)	kIx)	)
střevní	střevní	k2eAgNnSc1d1	střevní
sádlo	sádlo	k1gNnSc1	sádlo
–	–	k?	–
sádlo	sádlo	k1gNnSc1	sádlo
z	z	k7c2	z
tukových	tukový	k2eAgInPc2d1	tukový
chuchvalců	chuchvalec	k1gInPc2	chuchvalec
mezi	mezi	k7c7	mezi
střevy	střevo	k1gNnPc7	střevo
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
pachem	pach	k1gInSc7	pach
lalokové	lalokový	k2eAgNnSc1d1	lalokový
sádlo	sádlo	k1gNnSc1	sádlo
–	–	k?	–
sádlo	sádlo	k1gNnSc1	sádlo
z	z	k7c2	z
laloku	lalok	k1gInSc2	lalok
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
pachem	pach	k1gInSc7	pach
Lalokové	lalokový	k2eAgNnSc4d1	lalokový
sádlo	sádlo	k1gNnSc4	sádlo
ani	ani	k8xC	ani
střevní	střevní	k2eAgNnSc1d1	střevní
sádlo	sádlo	k1gNnSc1	sádlo
se	se	k3xPyFc4	se
nemíchá	míchat	k5eNaImIp3nS	míchat
s	s	k7c7	s
ostatním	ostatní	k2eAgNnSc7d1	ostatní
sádlem	sádlo	k1gNnSc7	sádlo
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
(	(	kIx(	(
<g/>
ne	ne	k9	ne
každému	každý	k3xTgMnSc3	každý
příjemný	příjemný	k2eAgInSc1d1	příjemný
<g/>
)	)	kIx)	)
pach	pach	k1gInSc1	pach
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
sádla	sádlo	k1gNnPc1	sádlo
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
horší	zlý	k2eAgFnSc1d2	horší
skladovatelnost	skladovatelnost	k1gFnSc1	skladovatelnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
vaření	vaření	k1gNnSc3	vaření
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
už	už	k6eAd1	už
jako	jako	k8xS	jako
surovina	surovina	k1gFnSc1	surovina
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
zabíjačkových	zabíjačkový	k2eAgFnPc2d1	zabíjačková
specialit	specialita	k1gFnPc2	specialita
(	(	kIx(	(
<g/>
prejt	prejt	k1gInSc1	prejt
<g/>
,	,	kIx,	,
jitrnice	jitrnice	k1gFnSc1	jitrnice
<g/>
,	,	kIx,	,
tlačenka	tlačenka	k1gFnSc1	tlačenka
<g/>
,	,	kIx,	,
ovar	ovar	k1gInSc1	ovar
<g/>
,	,	kIx,	,
zabijačková	zabijačkový	k2eAgFnSc1d1	zabijačková
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
sádlo	sádlo	k1gNnSc1	sádlo
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
mokrou	mokrý	k2eAgFnSc7d1	mokrá
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
extrakce	extrakce	k1gFnSc1	extrakce
tuku	tuk	k1gInSc2	tuk
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
buďto	buďto	k8xC	buďto
varem	var	k1gInSc7	var
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
dekantací	dekantace	k1gFnSc7	dekantace
<g/>
,	,	kIx,	,
umožněnou	umožněný	k2eAgFnSc7d1	umožněná
nerozputitelností	nerozputitelnost	k1gFnSc7	nerozputitelnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
extrakcí	extrakce	k1gFnSc7	extrakce
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
výsledný	výsledný	k2eAgInSc4d1	výsledný
produkt	produkt	k1gInSc4	produkt
více	hodně	k6eAd2	hodně
chuťově	chuťově	k6eAd1	chuťově
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
či	či	k8xC	či
méně	málo	k6eAd2	málo
aromatický	aromatický	k2eAgMnSc1d1	aromatický
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
určitá	určitý	k2eAgNnPc4d1	určité
použití	použití	k1gNnPc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
vyráběné	vyráběný	k2eAgNnSc4d1	vyráběné
sádlo	sádlo	k1gNnSc4	sádlo
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
upravováno	upravovat	k5eAaImNgNnS	upravovat
hydrogenací	hydrogenace	k1gFnSc7	hydrogenace
a	a	k8xC	a
příměsemi	příměse	k1gFnPc7	příměse
antioxidantů	antioxidant	k1gInPc2	antioxidant
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
BHT	BHT	kA	BHT
<g/>
.	.	kIx.	.
</s>
<s>
Labužníci	labužník	k1gMnPc1	labužník
si	se	k3xPyFc3	se
proto	proto	k6eAd1	proto
najdou	najít	k5eAaPmIp3nP	najít
čas	čas	k1gInSc4	čas
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
specializovaného	specializovaný	k2eAgInSc2d1	specializovaný
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
či	či	k8xC	či
domácí	domácí	k2eAgFnSc4d1	domácí
přípravu	příprava	k1gFnSc4	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
chemicky	chemicky	k6eAd1	chemicky
nekonzervované	konzervovaný	k2eNgNnSc4d1	konzervovaný
sádlo	sádlo	k1gNnSc4	sádlo
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
slunečními	sluneční	k2eAgInPc7d1	sluneční
paprsky	paprsek	k1gInPc7	paprsek
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
skladovat	skladovat	k5eAaImF	skladovat
v	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
sádlo	sádlo	k1gNnSc4	sádlo
škvařit	škvařit	k5eAaImF	škvařit
vždy	vždy	k6eAd1	vždy
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
škvařený	škvařený	k2eAgInSc1d1	škvařený
obsah	obsah	k1gInSc1	obsah
znehodnotí	znehodnotit	k5eAaPmIp3nS	znehodnotit
pachem	pach	k1gInSc7	pach
spáleniny	spálenina	k1gFnSc2	spálenina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
škvaření	škvaření	k1gNnSc6	škvaření
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
přidávání	přidávání	k1gNnSc4	přidávání
zbytků	zbytek	k1gInPc2	zbytek
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc4	ten
někteří	některý	k3yIgMnPc1	některý
kuchaři	kuchař	k1gMnPc1	kuchař
při	při	k7c6	při
škvaření	škvaření	k1gNnSc6	škvaření
do	do	k7c2	do
sádla	sádlo	k1gNnSc2	sádlo
přidávají	přidávat	k5eAaImIp3nP	přidávat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
chyba	chyba	k1gFnSc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
také	také	k9	také
použití	použití	k1gNnSc1	použití
sádla	sádlo	k1gNnSc2	sádlo
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
pleťová	pleťový	k2eAgFnSc1d1	pleťová
maska	maska	k1gFnSc1	maska
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hojení	hojení	k1gNnSc4	hojení
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
jizev	jizva	k1gFnPc2	jizva
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
okolí	okolí	k1gNnSc1	okolí
zvláčňovat	zvláčňovat	k5eAaImF	zvláčňovat
přiměřeným	přiměřený	k2eAgNnSc7d1	přiměřené
množstvím	množství	k1gNnSc7	množství
mastného	mastný	k2eAgInSc2d1	mastný
krému	krém	k1gInSc2	krém
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sádlo	sádlo	k1gNnSc4	sádlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sádlo	sádlo	k1gNnSc4	sádlo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Databáze	databáze	k1gFnSc2	databáze
složení	složení	k1gNnSc2	složení
potravin	potravina	k1gFnPc2	potravina
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
Sádlo	sádlo	k1gNnSc1	sádlo
vepřové	vepřový	k2eAgNnSc1d1	vepřové
Postup	postup	k1gInSc4	postup
škvaření	škvaření	k1gNnSc2	škvaření
vepřového	vepřový	k2eAgNnSc2d1	vepřové
sádla	sádlo	k1gNnSc2	sádlo
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
škvaření	škvaření	k1gNnSc1	škvaření
vepřového	vepřový	k2eAgNnSc2d1	vepřové
sádla	sádlo	k1gNnSc2	sádlo
</s>
