<s>
Sádlo	sádlo	k1gNnSc1	sádlo
je	být	k5eAaImIp3nS	být
živočišný	živočišný	k2eAgInSc4d1	živočišný
tuk	tuk	k1gInSc4	tuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžné	běžný	k2eAgNnSc1d1	běžné
je	být	k5eAaImIp3nS	být
i	i	k9	i
husí	husí	k2eAgFnSc1d1	husí
<g/>
,	,	kIx,	,
kachní	kachní	k2eAgFnSc1d1	kachní
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Sádlo	sádlo	k1gNnSc1	sádlo
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kuchyních	kuchyně	k1gFnPc6	kuchyně
na	na	k7c6	na
vaření	vaření	k1gNnSc6	vaření
<g/>
,	,	kIx,	,
smažení	smažení	k1gNnSc6	smažení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
studenou	studený	k2eAgFnSc4d1	studená
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
.	.	kIx.	.
</s>
