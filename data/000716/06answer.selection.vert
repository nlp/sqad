<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
stát	stát	k1gInSc4	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Manila	Manila	k1gFnSc1	Manila
<g/>
.	.	kIx.	.
</s>
