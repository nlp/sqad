<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
stát	stát	k1gInSc4	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Manila	Manila	k1gFnSc1	Manila
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
7107	[number]	k4	7107
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
státem	stát	k1gInSc7	stát
nesdílejí	sdílet	k5eNaImIp3nP	sdílet
pozemní	pozemní	k2eAgFnSc4d1	pozemní
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgFnPc1d1	mořská
úžiny	úžina	k1gFnPc1	úžina
je	on	k3xPp3gMnPc4	on
dělí	dělit	k5eAaImIp3nP	dělit
od	od	k7c2	od
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
od	od	k7c2	od
Malajsie	Malajsie	k1gFnSc2	Malajsie
(	(	kIx(	(
<g/>
Sabah	Sabah	k1gMnSc1	Sabah
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
Sulawesi	Sulawesi	k1gNnSc1	Sulawesi
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
jsou	být	k5eAaImIp3nP	být
dvanáctou	dvanáctý	k4xOgFnSc7	dvanáctý
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
98	[number]	k4	98
miliony	milion	k4xCgInPc7	milion
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
47	[number]	k4	47
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
nominálně	nominálně	k6eAd1	nominálně
168,6	[number]	k4	168,6
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
..	..	k?	..
Dle	dle	k7c2	dle
posledních	poslední	k2eAgFnPc2d1	poslední
analýz	analýza	k1gFnPc2	analýza
mají	mít	k5eAaImIp3nP	mít
Filipíny	Filipíny	k1gFnPc4	Filipíny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2050	[number]	k4	2050
přeskočit	přeskočit	k5eAaPmF	přeskočit
celou	celá	k1gFnSc4	celá
řadu	řad	k1gInSc2	řad
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
zemí	zem	k1gFnPc2	zem
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
a	a	k8xC	a
posunout	posunout	k5eAaPmF	posunout
se	se	k3xPyFc4	se
na	na	k7c4	na
16	[number]	k4	16
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mimo	mimo	k7c4	mimo
Filipíny	Filipíny	k1gFnPc4	Filipíny
žije	žít	k5eAaImIp3nS	žít
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
na	na	k7c4	na
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
Filipínců	Filipínec	k1gMnPc2	Filipínec
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
španělská	španělský	k2eAgFnSc1d1	španělská
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
americká	americký	k2eAgFnSc1d1	americká
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
převážně	převážně	k6eAd1	převážně
katolických	katolický	k2eAgFnPc2d1	katolická
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
druhou	druhý	k4xOgFnSc4	druhý
je	být	k5eAaImIp3nS	být
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
menšinových	menšinový	k2eAgFnPc2d1	menšinová
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
vyznávajících	vyznávající	k2eAgFnPc2d1	vyznávající
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
hinduismus	hinduismus	k1gInSc1	hinduismus
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
víry	vír	k1gInPc1	vír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
filipínských	filipínský	k2eAgInPc6d1	filipínský
ostrovech	ostrov	k1gInPc6	ostrov
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
četná	četný	k2eAgNnPc4d1	četné
etnika	etnikum	k1gNnPc4	etnikum
a	a	k8xC	a
kultury	kultura	k1gFnPc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgFnP	být
Filipíny	Filipíny	k1gFnPc4	Filipíny
rozdrobené	rozdrobený	k2eAgFnPc4d1	rozdrobená
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
drobných	drobný	k2eAgNnPc2d1	drobné
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stáli	stát	k5eAaImAgMnP	stát
rádžové	rádža	k1gMnPc1	rádža
(	(	kIx(	(
<g/>
vesničtí	vesnický	k2eAgMnPc1d1	vesnický
předáci	předák	k1gMnPc1	předák
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
dato	dato	k6eAd1	dato
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Bruneje	Brunej	k1gInSc2	Brunej
šířit	šířit	k5eAaImF	šířit
islám	islám	k1gInSc4	islám
(	(	kIx(	(
<g/>
Suluské	Suluský	k2eAgInPc1d1	Suluský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Mindanao	Mindanao	k1gNnSc1	Mindanao
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
objevil	objevit	k5eAaPmAgMnS	objevit
ostrovy	ostrov	k1gInPc4	ostrov
Fernã	Fernã	k1gMnPc2	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gFnSc7	Magalhã
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1565	[number]	k4	1565
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonií	kolonie	k1gFnSc7	kolonie
Španělská	španělský	k2eAgFnSc1d1	španělská
Východní	východní	k2eAgFnSc1d1	východní
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Marianami	Mariana	k1gFnPc7	Mariana
a	a	k8xC	a
Karolínami	Karolína	k1gFnPc7	Karolína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zesilovaly	zesilovat	k5eAaImAgFnP	zesilovat
snahy	snaha	k1gFnPc1	snaha
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
bylo	být	k5eAaImAgNnS	být
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgMnSc7d1	národní
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
Španělům	Španěl	k1gMnPc3	Španěl
byl	být	k5eAaImAgInS	být
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
José	Josá	k1gFnSc2	Josá
Rizal	Rizal	k1gInSc1	Rizal
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
muslimské	muslimský	k2eAgNnSc1d1	muslimské
Mindanao	Mindanao	k1gNnSc1	Mindanao
nebylo	být	k5eNaImAgNnS	být
Španěly	Španěly	k1gInPc4	Španěly
nikdy	nikdy	k6eAd1	nikdy
plně	plně	k6eAd1	plně
ovládnuto	ovládnut	k2eAgNnSc1d1	ovládnut
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
a	a	k8xC	a
po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
invazi	invaze	k1gFnSc6	invaze
a	a	k8xC	a
filipínsko-americké	filipínskomerický	k2eAgFnSc3d1	filipínsko-americký
válce	válka	k1gFnSc3	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
Filipíny	Filipíny	k1gFnPc4	Filipíny
staly	stát	k5eAaPmAgFnP	stát
kolonií	kolonie	k1gFnPc2	kolonie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
byla	být	k5eAaImAgFnS	být
Filipínám	Filipíny	k1gFnPc3	Filipíny
udělena	udělit	k5eAaPmNgFnS	udělit
tzv.	tzv.	kA	tzv.
omezená	omezený	k2eAgFnSc1d1	omezená
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
domluven	domluven	k2eAgInSc1d1	domluven
desetiletý	desetiletý	k2eAgInSc4d1	desetiletý
přechodný	přechodný	k2eAgInSc4d1	přechodný
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
Filipíny	Filipíny	k1gFnPc1	Filipíny
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jim	on	k3xPp3gMnPc3	on
udělen	udělen	k2eAgInSc4d1	udělen
statut	statut	k1gInSc4	statut
dominia	dominion	k1gNnSc2	dominion
(	(	kIx(	(
<g/>
Philippine	Philippin	k1gInSc5	Philippin
Commonwealth	Commonwealth	k1gInSc1	Commonwealth
<g/>
,	,	kIx,	,
statut	statut	k1gInSc1	statut
podobný	podobný	k2eAgInSc1d1	podobný
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
Portoriku	Portorico	k1gNnSc6	Portorico
<g/>
)	)	kIx)	)
které	který	k3yRgNnSc1	který
ale	ale	k9	ale
přerušila	přerušit	k5eAaPmAgFnS	přerušit
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
počátku	počátek	k1gInSc6	počátek
byly	být	k5eAaImAgFnP	být
Filipíny	Filipíny	k1gFnPc4	Filipíny
obsazeny	obsazen	k2eAgFnPc4d1	obsazena
Japonci	Japonec	k1gMnPc7	Japonec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
zřídili	zřídit	k5eAaPmAgMnP	zřídit
loutkový	loutkový	k2eAgInSc4d1	loutkový
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Američané	Američan	k1gMnPc1	Američan
znovudobývání	znovudobývání	k1gNnSc2	znovudobývání
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Manilu	Manila	k1gFnSc4	Manila
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
zahynul	zahynout	k5eAaPmAgInS	zahynout
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
filipínských	filipínský	k2eAgMnPc2d1	filipínský
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
zemřel	zemřít	k5eAaPmAgMnS	zemřít
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
20	[number]	k4	20
Filipínců	Filipínec	k1gMnPc2	Filipínec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
získala	získat	k5eAaPmAgFnS	získat
země	země	k1gFnSc1	země
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
zde	zde	k6eAd1	zde
diktátorsky	diktátorsky	k6eAd1	diktátorsky
vládl	vládnout	k5eAaImAgMnS	vládnout
prezident	prezident	k1gMnSc1	prezident
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Marcos	Marcos	k1gMnSc1	Marcos
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
svržení	svržení	k1gNnSc6	svržení
jsou	být	k5eAaImIp3nP	být
voleni	volen	k2eAgMnPc1d1	volen
prezidenti	prezident	k1gMnPc1	prezident
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
14	[number]	k4	14
prezidentů	prezident	k1gMnPc2	prezident
byly	být	k5eAaImAgFnP	být
2	[number]	k4	2
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
Corazon	Corazon	k1gInSc1	Corazon
Aquinová	Aquinová	k1gFnSc1	Aquinová
a	a	k8xC	a
Gloria	Gloria	k1gFnSc1	Gloria
Macapagal-Arroyová	Macapagal-Arroyová	k1gFnSc1	Macapagal-Arroyová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Násilné	násilný	k2eAgInPc4d1	násilný
střety	střet	k1gInPc4	střet
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
největším	veliký	k2eAgInSc6d3	veliký
filipínském	filipínský	k2eAgInSc6d1	filipínský
ostrově	ostrov	k1gInSc6	ostrov
Mindanao	Mindanao	k6eAd1	Mindanao
mezi	mezi	k7c7	mezi
vládními	vládní	k2eAgFnPc7d1	vládní
jednotkami	jednotka	k1gFnPc7	jednotka
a	a	k8xC	a
muslimskými	muslimský	k2eAgMnPc7d1	muslimský
povstalci	povstalec	k1gMnPc7	povstalec
z	z	k7c2	z
Fronty	fronta	k1gFnSc2	fronta
islámského	islámský	k2eAgNnSc2d1	islámské
osvobození	osvobození	k1gNnSc2	osvobození
Morů	mor	k1gInPc2	mor
(	(	kIx(	(
<g/>
MILF	MILF	kA	MILF
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc1	Filipíny
jsou	být	k5eAaImIp3nP	být
souostroví	souostroví	k1gNnSc4	souostroví
skládající	skládající	k2eAgNnSc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
7107	[number]	k4	7107
ostrovů	ostrov	k1gInPc2	ostrov
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
116	[number]	k4	116
<g/>
.	.	kIx.	.
a	a	k8xC	a
126	[number]	k4	126
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
stupněm	stupeň	k1gInSc7	stupeň
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
z	z	k7c2	z
východu	východ	k1gInSc2	východ
je	on	k3xPp3gFnPc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Filipínské	filipínský	k2eAgNnSc1d1	filipínské
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Jihočínské	jihočínský	k2eAgNnSc4d1	Jihočínské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Celebeské	celebeský	k2eAgNnSc4d1	celebeský
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ostrov	ostrov	k1gInSc1	ostrov
Borneo	Borneo	k1gNnSc1	Borneo
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
Filipín	Filipíny	k1gFnPc2	Filipíny
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Jiho-jihozápadně	Jihoihozápadně	k6eAd1	Jiho-jihozápadně
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ostrovy	ostrov	k1gInPc1	ostrov
Moluky	Moluky	k1gFnPc4	Moluky
a	a	k8xC	a
Celebes	Celebes	k1gInSc4	Celebes
a	a	k8xC	a
východně	východně	k6eAd1	východně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Palau	Palaus	k1gInSc2	Palaus
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc1	Filipíny
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
ostrovních	ostrovní	k2eAgFnPc2d1	ostrovní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Luzon	Luzon	k1gMnSc1	Luzon
<g/>
,	,	kIx,	,
Visayas	Visayas	k1gMnSc1	Visayas
a	a	k8xC	a
Mindanao	Mindanao	k1gMnSc1	Mindanao
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
Manila	Manila	k1gFnSc1	Manila
v	v	k7c6	v
části	část	k1gFnSc6	část
Luzon	Luzona	k1gFnPc2	Luzona
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
největším	veliký	k2eAgMnPc3d3	veliký
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hornatých	hornatý	k2eAgInPc2d1	hornatý
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
tropickým	tropický	k2eAgInSc7d1	tropický
deštným	deštný	k2eAgInSc7d1	deštný
pralesem	prales	k1gInSc7	prales
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Apo	Apo	k1gFnSc1	Apo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
části	část	k1gFnSc6	část
Mindao	Mindao	k6eAd1	Mindao
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
2954	[number]	k4	2954
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Mayon	Mayon	k1gNnSc1	Mayon
Volcano	Volcana	k1gFnSc5	Volcana
<g/>
,	,	kIx,	,
Pinatubo	Pinatuba	k1gFnSc5	Pinatuba
či	či	k8xC	či
Taal	Taal	k1gInSc1	Taal
Volcano	Volcana	k1gFnSc5	Volcana
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
tajfunovém	tajfunový	k2eAgNnSc6d1	tajfunový
pásmu	pásmo	k1gNnSc6	pásmo
západního	západní	k2eAgNnSc2d1	západní
tichomoří	tichomoří	k1gNnSc2	tichomoří
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zde	zde	k6eAd1	zde
udeří	udeřit	k5eAaPmIp3nP	udeřit
na	na	k7c4	na
devatenáct	devatenáct	k4xCc4	devatenáct
tajfunů	tajfun	k1gInPc2	tajfun
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc1	Filipíny
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
vnějším	vnější	k2eAgInSc6d1	vnější
okraji	okraj	k1gInSc6	okraj
pacifického	pacifický	k2eAgInSc2d1	pacifický
ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
častým	častý	k2eAgNnSc7d1	časté
místem	místo	k1gNnSc7	místo
seismické	seismický	k2eAgFnSc2d1	seismická
a	a	k8xC	a
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
na	na	k7c4	na
20	[number]	k4	20
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pocítit	pocítit	k5eAaPmF	pocítit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
velké	velký	k2eAgNnSc1d1	velké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
filipínskou	filipínský	k2eAgFnSc7d1	filipínská
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Mindanao	Mindanao	k6eAd1	Mindanao
(	(	kIx(	(
<g/>
551	[number]	k4	551
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeky	řeka	k1gFnPc1	řeka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ale	ale	k9	ale
rychlý	rychlý	k2eAgInSc4d1	rychlý
spád	spád	k1gInSc4	spád
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
dobrý	dobrý	k2eAgInSc4d1	dobrý
předpoklad	předpoklad	k1gInSc4	předpoklad
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
využívané	využívaný	k2eAgFnPc1d1	využívaná
k	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
původní	původní	k2eAgInPc4d1	původní
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
například	například	k6eAd1	například
filipínský	filipínský	k2eAgInSc1d1	filipínský
mahagon	mahagon	k1gInSc1	mahagon
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
vzácné	vzácný	k2eAgInPc1d1	vzácný
stromy	strom	k1gInPc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
oblastech	oblast	k1gFnPc6	oblast
převládají	převládat	k5eAaImIp3nP	převládat
borové	borový	k2eAgInPc1d1	borový
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
rostou	růst	k5eAaImIp3nP	růst
mangrovy	mangrovy	k1gInPc1	mangrovy
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
zástupcem	zástupce	k1gMnSc7	zástupce
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
jsou	být	k5eAaImIp3nP	být
opice	opice	k1gFnPc1	opice
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
blízkost	blízkost	k1gFnSc4	blízkost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
a	a	k8xC	a
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
tropickém	tropický	k2eAgNnSc6d1	tropické
podnebném	podnebné	k1gNnSc6	podnebné
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
26,5	[number]	k4	26,5
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
<s>
Na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgNnPc4	tři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tag-init	Tagnit	k1gFnSc1	Tag-init
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Tag-araw	Tagraw	k1gFnSc1	Tag-araw
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
teplé	teplý	k2eAgNnSc4d1	teplé
období	období	k1gNnSc4	období
nebo	nebo	k8xC	nebo
léto	léto	k1gNnSc4	léto
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tag-ulan	Taglan	k1gMnSc1	Tag-ulan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tag-lamig	Tagamig	k1gInSc1	Tag-lamig
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
chladné	chladný	k2eAgNnSc1d1	chladné
období	období	k1gNnSc1	období
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
také	také	k9	také
východoasijské	východoasijský	k2eAgNnSc1d1	východoasijské
monzunové	monzunový	k2eAgNnSc1d1	monzunové
proudění	proudění	k1gNnSc1	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadní	jihozápadní	k2eAgInSc1d1	jihozápadní
monzun	monzun	k1gInSc1	monzun
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Habagat	Habagat	k1gInSc1	Habagat
<g/>
"	"	kIx"	"
a	a	k8xC	a
suché	suchý	k2eAgInPc1d1	suchý
větry	vítr	k1gInPc1	vítr
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
monzunu	monzun	k1gInSc2	monzun
(	(	kIx(	(
<g/>
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Amihan	Amihan	k1gInSc1	Amihan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nejchladnějším	chladný	k2eAgInSc7d3	nejchladnější
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc4	leden
a	a	k8xC	a
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
vlhkost	vlhkost	k1gFnSc1	vlhkost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
maxima	maximum	k1gNnPc1	maximum
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Manila	Manila	k1gFnSc1	Manila
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
nížinatých	nížinatý	k2eAgFnPc2d1	nížinatá
oblastí	oblast	k1gFnPc2	oblast
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
prašná	prašný	k2eAgFnSc1d1	prašná
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
teplým	teplý	k2eAgNnSc7d1	teplé
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
teplota	teplota	k1gFnSc1	teplota
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
klesne	klesnout	k5eAaPmIp3nS	klesnout
pod	pod	k7c7	pod
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
klesne	klesnout	k5eAaPmIp3nS	klesnout
pod	pod	k7c7	pod
27	[number]	k4	27
°	°	k?	°
<g/>
C.	C.	kA	C.
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
východního	východní	k2eAgInSc2d1	východní
pobřeží	pobřeží	k1gNnSc3	pobřeží
5000	[number]	k4	5000
mm	mm	kA	mm
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
údolích	údolí	k1gNnPc6	údolí
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hodnoty	hodnota	k1gFnPc4	hodnota
pod	pod	k7c4	pod
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc4d1	velké
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
představují	představovat	k5eAaImIp3nP	představovat
tajfuny	tajfun	k1gInPc1	tajfun
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
baguio	baguio	k6eAd1	baguio
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
také	také	k9	také
bagio	bagio	k1gNnSc1	bagio
<g/>
,	,	kIx,	,
vaguio	vaguio	k1gNnSc1	vaguio
<g/>
,	,	kIx,	,
bagyo	bagyo	k1gNnSc1	bagyo
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Baguio	Baguio	k6eAd1	Baguio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
bouře	bouř	k1gFnSc2	bouř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
napadlo	napadnout	k5eAaPmAgNnS	napadnout
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
116,8	[number]	k4	116,8
cm	cm	kA	cm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Filipíny	Filipíny	k1gFnPc4	Filipíny
řada	řada	k1gFnSc1	řada
tropických	tropický	k2eAgFnPc2d1	tropická
bouří	bouř	k1gFnPc2	bouř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pokaždé	pokaždé	k6eAd1	pokaždé
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velké	velký	k2eAgFnPc1d1	velká
ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
a	a	k8xC	a
materiální	materiální	k2eAgFnPc4d1	materiální
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1970	[number]	k4	1970
se	se	k3xPyFc4	se
územím	území	k1gNnSc7	území
přehnal	přehnat	k5eAaPmAgInS	přehnat
tajfun	tajfun	k1gInSc1	tajfun
Sening	Sening	k1gInSc1	Sening
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zničil	zničit	k5eAaPmAgInS	zničit
řadu	řada	k1gFnSc4	řada
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
až	až	k9	až
275	[number]	k4	275
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
nejvíc	hodně	k6eAd3	hodně
v	v	k7c6	v
zaznamenané	zaznamenaný	k2eAgFnSc6d1	zaznamenaná
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1991	[number]	k4	1991
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Filipíny	Filipíny	k1gFnPc4	Filipíny
tropická	tropický	k2eAgFnSc1d1	tropická
bouře	bouře	k1gFnSc1	bouře
Uring	Uringa	k1gFnPc2	Uringa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přes	přes	k7c4	přes
relativní	relativní	k2eAgFnSc4d1	relativní
slabost	slabost	k1gFnSc4	slabost
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc4	rychlost
větru	vítr	k1gInSc2	vítr
95	[number]	k4	95
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
způsobila	způsobit	k5eAaPmAgFnS	způsobit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
záplavy	záplava	k1gFnPc4	záplava
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
obětí	oběť	k1gFnPc2	oběť
(	(	kIx(	(
<g/>
odhadem	odhad	k1gInSc7	odhad
až	až	k9	až
8	[number]	k4	8
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
překonala	překonat	k5eAaPmAgFnS	překonat
všechny	všechen	k3xTgFnPc4	všechen
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
bouře	bouř	k1gFnPc4	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
pobřežní	pobřežní	k2eAgNnSc4d1	pobřežní
město	město	k1gNnSc4	město
Basey	Basea	k1gFnSc2	Basea
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Samar	Samara	k1gFnPc2	Samara
tajfun	tajfun	k1gInSc1	tajfun
Haiyan	Haiyan	k1gMnSc1	Haiyan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
přes	přes	k7c4	přes
5000	[number]	k4	5000
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
ihned	ihned	k6eAd1	ihned
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
přispěly	přispět	k5eAaPmAgFnP	přispět
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
,	,	kIx,	,
financemi	finance	k1gFnPc7	finance
a	a	k8xC	a
prostředky	prostředek	k1gInPc7	prostředek
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
humanitární	humanitární	k2eAgFnSc2d1	humanitární
organizace	organizace	k1gFnSc2	organizace
ADRA	ADRA	kA	ADRA
<g/>
,	,	kIx,	,
Charita	charita	k1gFnSc1	charita
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
či	či	k8xC	či
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zatím	zatím	k6eAd1	zatím
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
přes	přes	k7c4	přes
17	[number]	k4	17
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zničenými	zničený	k2eAgNnPc7d1	zničené
městy	město	k1gNnPc7	město
byl	být	k5eAaImAgMnS	být
Tacloban	Tacloban	k1gMnSc1	Tacloban
či	či	k8xC	či
Guiuan	Guiuan	k1gMnSc1	Guiuan
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Leyte	Leyt	k1gInSc5	Leyt
<g/>
,	,	kIx,	,
problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
jsou	být	k5eAaImIp3nP	být
zastupitelskou	zastupitelský	k2eAgFnSc7d1	zastupitelská
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
,	,	kIx,	,
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc1	Filipíny
jsou	být	k5eAaImIp3nP	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
mimo	mimo	k7c4	mimo
Autonomní	autonomní	k2eAgInSc4d1	autonomní
region	region	k1gInSc4	region
Muslimské	muslimský	k2eAgNnSc4d1	muslimské
Mindanao	Mindanao	k1gNnSc4	Mindanao
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
i	i	k9	i
hlava	hlava	k1gFnSc1	hlava
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bikamerální	bikamerální	k2eAgInSc1d1	bikamerální
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
Senátu	senát	k1gInSc2	senát
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
a	a	k8xC	a
komory	komora	k1gFnSc2	komora
dolní	dolní	k2eAgFnSc2d1	dolní
<g/>
,	,	kIx,	,
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
na	na	k7c4	na
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Geograficky	geograficky	k6eAd1	geograficky
se	se	k3xPyFc4	se
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
filipínských	filipínský	k2eAgInPc2d1	filipínský
ostrovů	ostrov	k1gInPc2	ostrov
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
3	[number]	k4	3
skupin	skupina	k1gFnPc2	skupina
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
Luzon	Luzon	k1gMnSc1	Luzon
<g/>
,	,	kIx,	,
Visayas	Visayas	k1gMnSc1	Visayas
a	a	k8xC	a
Mindanao	Mindanao	k1gMnSc1	Mindanao
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
18	[number]	k4	18
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
81	[number]	k4	81
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
144	[number]	k4	144
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
1490	[number]	k4	1490
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
42029	[number]	k4	42029
malých	malý	k2eAgFnPc2d1	malá
obcí	obec	k1gFnPc2	obec
zvaných	zvaný	k2eAgFnPc2d1	zvaná
barangay	barangay	k1gInPc4	barangay
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
oznámila	oznámit	k5eAaPmAgFnS	oznámit
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
filipínská	filipínský	k2eAgFnSc1d1	filipínská
prezidentka	prezidentka	k1gFnSc1	prezidentka
Gloria	Gloria	k1gFnSc1	Gloria
Macapagal-Arroyo	Macapagal-Arroyo	k6eAd1	Macapagal-Arroyo
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
pěti	pět	k4xCc2	pět
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
superregionů	superregion	k1gInPc2	superregion
<g/>
,	,	kIx,	,
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
přednosti	přednost	k1gFnPc4	přednost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Filipíny	Filipíny	k1gFnPc4	Filipíny
velmi	velmi	k6eAd1	velmi
bohaté	bohatý	k2eAgInPc1d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
naleziště	naleziště	k1gNnSc1	naleziště
rud	ruda	k1gFnPc2	ruda
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
chromu	chrom	k1gInSc2	chrom
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
ložiska	ložisko	k1gNnSc2	ložisko
dvou	dva	k4xCgNnPc2	dva
důležitých	důležitý	k2eAgNnPc2d1	důležité
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělstvím	zemědělství	k1gNnSc7	zemědělství
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
asi	asi	k9	asi
34	[number]	k4	34
%	%	kIx~	%
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
plodinou	plodina	k1gFnSc7	plodina
je	být	k5eAaImIp3nS	být
kokosová	kokosový	k2eAgFnSc1d1	kokosová
palma	palma	k1gFnSc1	palma
<g/>
.	.	kIx.	.
</s>
<s>
Zpracováním	zpracování	k1gNnSc7	zpracování
dužiny	dužina	k1gFnSc2	dužina
jejich	jejich	k3xOp3gInPc2	jejich
plodů	plod	k1gInPc2	plod
(	(	kIx(	(
<g/>
kokosové	kokosový	k2eAgInPc4d1	kokosový
ořechy	ořech	k1gInPc4	ořech
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
tzv.	tzv.	kA	tzv.
kopra	kopra	k1gFnSc1	kopra
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
produkci	produkce	k1gFnSc6	produkce
první	první	k4xOgFnSc7	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
ananasy	ananas	k1gInPc1	ananas
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
batáty	batáty	k1gInPc1	batáty
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
kaučukovník	kaučukovník	k1gInSc1	kaučukovník
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
<g/>
,	,	kIx,	,
maniok	maniok	k1gInSc1	maniok
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
převládá	převládat	k5eAaImIp3nS	převládat
nad	nad	k7c7	nad
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc1	Filipíny
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
rozvojovým	rozvojový	k2eAgFnPc3d1	rozvojová
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Donedávna	donedávna	k6eAd1	donedávna
zde	zde	k6eAd1	zde
převládala	převládat	k5eAaImAgFnS	převládat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
odvětví	odvětví	k1gNnSc4	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
-	-	kIx~	-
průmysl	průmysl	k1gInSc4	průmysl
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
dřevařský	dřevařský	k2eAgInSc4d1	dřevařský
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
chemický	chemický	k2eAgInSc1d1	chemický
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
významu	význam	k1gInSc6	význam
nabírat	nabírat	k5eAaImF	nabírat
strojírenství	strojírenství	k1gNnSc4	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgNnPc7d1	významné
odvětvími	odvětví	k1gNnPc7	odvětví
strojírenství	strojírenství	k1gNnSc2	strojírenství
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
výroba	výroba	k1gFnSc1	výroba
videorekordérů	videorekordér	k1gInPc2	videorekordér
<g/>
,	,	kIx,	,
televizorů	televizor	k1gInPc2	televizor
a	a	k8xC	a
radiomagnetofonů	radiomagnetofon	k1gInPc2	radiomagnetofon
a	a	k8xC	a
montáž	montáž	k1gFnSc4	montáž
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
centrem	centr	k1gInSc7	centr
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Manila	Manila	k1gFnSc1	Manila
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
<g/>
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
filipínského	filipínský	k2eAgInSc2d1	filipínský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
populace	populace	k1gFnSc2	populace
ostrovů	ostrov	k1gInPc2	ostrov
tvoří	tvořit	k5eAaImIp3nP	tvořit
Filipínci	Filipínec	k1gMnPc1	Filipínec
(	(	kIx(	(
<g/>
96	[number]	k4	96
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
jazyky	jazyk	k1gInPc4	jazyk
malajsko-polynéské	malajskoolynéský	k2eAgFnSc2d1	malajsko-polynéský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Tagalogové	Tagalogový	k2eAgFnSc2d1	Tagalogový
28,1	[number]	k4	28,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
Cebuanové	Cebuanové	k2eAgFnSc1d1	Cebuanové
13,1	[number]	k4	13,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
Ilokanové	Ilokanové	k2eAgFnSc1d1	Ilokanové
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
Bisajové	Bisajový	k2eAgFnSc2d1	Bisajový
7,6	[number]	k4	7,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
Hiligajnon	Hiligajnon	k1gInSc4	Hiligajnon
Ilongové	Ilongový	k2eAgFnSc2d1	Ilongový
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
Bikolové	Bikolový	k2eAgFnSc2d1	Bikolový
6	[number]	k4	6
%	%	kIx~	%
<g/>
,	,	kIx,	,
Warayové	Warayové	k2eAgFnSc1d1	Warayové
3,4	[number]	k4	3,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
nespočitatelné	spočitatelný	k2eNgFnPc4d1	nespočitatelná
menšiny	menšina	k1gFnPc4	menšina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Negritové	Negritové	k?	Negritové
nebo	nebo	k8xC	nebo
horští	horský	k2eAgMnPc1d1	horský
Ifugaové	Ifugaus	k1gMnPc1	Ifugaus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
obávanými	obávaný	k2eAgMnPc7d1	obávaný
lovci	lovec	k1gMnPc7	lovec
lebek	lebka	k1gFnPc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
žijí	žít	k5eAaImIp3nP	žít
tzv.	tzv.	kA	tzv.
mořští	mořský	k2eAgMnPc1d1	mořský
cikáni	cikán	k1gMnPc1	cikán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hlásí	hlásit	k5eAaImIp3nS	hlásit
80,9	[number]	k4	80,9
%	%	kIx~	%
Filipínců	Filipínec	k1gMnPc2	Filipínec
<g/>
..	..	k?	..
<g/>
Celkem	celkem	k6eAd1	celkem
11,6	[number]	k4	11,6
%	%	kIx~	%
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
jsou	být	k5eAaImIp3nP	být
věřící	věřící	k1gFnPc1	věřící
dalších	další	k2eAgFnPc2d1	další
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
je	být	k5eAaImIp3nS	být
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
filipínská	filipínský	k2eAgFnSc1d1	filipínská
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
IFI	IFI	kA	IFI
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
věřících-národní	věřícíchárodní	k2eAgFnSc1d1	věřících-národní
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svátostném	svátostný	k2eAgNnSc6d1	svátostné
společenství	společenství	k1gNnSc6	společenství
jak	jak	k8xC	jak
s	s	k7c7	s
anglikány	anglikán	k1gMnPc7	anglikán
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
s	s	k7c7	s
Utrechtskou	Utrechtský	k2eAgFnSc7d1	Utrechtská
unií	unie	k1gFnSc7	unie
starokatolických	starokatolický	k2eAgFnPc2d1	Starokatolická
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
jsou	být	k5eAaImIp3nP	být
:	:	kIx,	:
<g/>
Filipínská	filipínský	k2eAgFnSc1d1	filipínská
episkopální	episkopální	k2eAgFnSc1d1	episkopální
c.	c.	k?	c.
<g/>
(	(	kIx(	(
<g/>
anglikáni	anglikán	k1gMnPc1	anglikán
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Muslimští	muslimský	k2eAgMnPc1d1	muslimský
Morové	morový	k2eAgInPc1d1	morový
obývají	obývat	k5eAaImIp3nP	obývat
především	především	k9	především
oblasti	oblast	k1gFnPc1	oblast
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
-	-	kIx~	-
muslimové	muslim	k1gMnPc1	muslim
celkem	celkem	k6eAd1	celkem
tvoří	tvořit	k5eAaImIp3nP	tvořit
5	[number]	k4	5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
víry	vír	k1gInPc1	vír
<g/>
(	(	kIx(	(
<g/>
též	též	k9	též
animisté	animista	k1gMnPc1	animista
<g/>
)	)	kIx)	)
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nezjištěného	zjištěný	k2eNgNnSc2d1	nezjištěné
vyznání	vyznání	k1gNnSc2	vyznání
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
2,5	[number]	k4	2,5
%	%	kIx~	%
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc1	Filipíny
mají	mít	k5eAaImIp3nP	mít
přes	přes	k7c4	přes
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jenom	jenom	k6eAd1	jenom
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990	[number]	k4	1990
a	a	k8xC	a
2008	[number]	k4	2008
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Filipín	Filipíny	k1gFnPc2	Filipíny
o	o	k7c4	o
28	[number]	k4	28
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Filipíny	Filipíny	k1gFnPc4	Filipíny
mají	mít	k5eAaImIp3nP	mít
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
mladou	mladý	k2eAgFnSc4d1	mladá
populaci	populace	k1gFnSc4	populace
a	a	k8xC	a
populační	populační	k2eAgInSc4d1	populační
růst	růst	k1gInSc4	růst
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
liberalizaci	liberalizace	k1gFnSc6	liberalizace
imigračních	imigrační	k2eAgInPc2d1	imigrační
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
USA	USA	kA	USA
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
Filipínců	Filipínec	k1gMnPc2	Filipínec
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
sídla	sídlo	k1gNnPc4	sídlo
především	především	k6eAd1	především
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
65,3	[number]	k4	65,3
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
asi	asi	k9	asi
71,1	[number]	k4	71,1
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Východním	východní	k2eAgInSc7d1	východní
Timorem	Timor	k1gInSc7	Timor
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
převážně	převážně	k6eAd1	převážně
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Manila	Manila	k1gFnSc1	Manila
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
-	-	kIx~	-
11,5	[number]	k4	11,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
i	i	k9	i
s	s	k7c7	s
aglomeracemi	aglomerace	k1gFnPc7	aglomerace
<g/>
)	)	kIx)	)
Quezon	Quezon	k1gInSc1	Quezon
-	-	kIx~	-
2,2	[number]	k4	2,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnSc1	obyvatel
Caloocan	Caloocan	k1gMnSc1	Caloocan
-	-	kIx~	-
1,2	[number]	k4	1,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnSc1	obyvatel
Davao	Davao	k1gMnSc1	Davao
-	-	kIx~	-
1,2	[number]	k4	1,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnSc1	obyvatel
Cebu	Ceba	k1gFnSc4	Ceba
-	-	kIx~	-
730	[number]	k4	730
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Zamboanga	Zamboanga	k1gFnSc1	Zamboanga
-	-	kIx~	-
620	[number]	k4	620
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bacolod	Bacoloda	k1gFnPc2	Bacoloda
-	-	kIx~	-
430	[number]	k4	430
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zábavou	zábava	k1gFnSc7	zábava
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
kohoutí	kohoutí	k2eAgInPc1d1	kohoutí
zápasy	zápas	k1gInPc1	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
sopky	sopka	k1gFnSc2	sopka
Apo	Apo	k1gFnPc2	Apo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
křížovkách	křížovka	k1gFnPc6	křížovka
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
jezulátko	jezulátko	k1gNnSc1	jezulátko
(	(	kIx(	(
<g/>
Bambino	bambino	k1gNnSc1	bambino
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
známo	znám	k2eAgNnSc1d1	známo
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
ctěnou	ctěný	k2eAgFnSc7d1	ctěná
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
ikonou	ikona	k1gFnSc7	ikona
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xS	jako
Santo	Santo	k1gNnSc4	Santo
Niñ	Niñ	k1gFnSc2	Niñ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
velikonoce	velikonoce	k1gFnPc4	velikonoce
se	se	k3xPyFc4	se
na	na	k7c6	na
Luzonu	Luzon	k1gInSc6	Luzon
poblíž	poblíž	k7c2	poblíž
Manily	Manila	k1gFnSc2	Manila
nechávají	nechávat	k5eAaImIp3nP	nechávat
domorodci	domorodec	k1gMnPc1	domorodec
dobrovolně	dobrovolně	k6eAd1	dobrovolně
zaživa	zaživa	k6eAd1	zaživa
ukřižovávat	ukřižovávat	k5eAaImF	ukřižovávat
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
to	ten	k3xDgNnSc4	ten
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Filipín	Filipíny	k1gFnPc2	Filipíny
</s>
