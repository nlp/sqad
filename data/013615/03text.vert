<s>
Mérobert	Mérobert	k1gMnSc1
</s>
<s>
Mérobert	Mérobert	k1gMnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
128-154	128-154	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Île-de-France	Île-de-France	k1gFnSc1
departement	departement	k1gInSc1
</s>
<s>
Essonne	Essonnout	k5eAaImIp3nS,k5eAaPmIp3nS
arrondissement	arrondissement	k1gInSc1
</s>
<s>
Étampes	Étampes	k1gInSc1
kanton	kanton	k1gInSc1
</s>
<s>
Dourdan	Dourdan	k1gMnSc1
</s>
<s>
Mérobert	Mérobert	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
10,71	10,71	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
547	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
51	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Marie-Josè	Marie-Josè	k1gMnSc5
Mazure	Mazur	k1gMnSc5
PSČ	PSČ	kA
</s>
<s>
91780	#num#	k4
INSEE	INSEE	kA
</s>
<s>
91393	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mérobert	Mérobert	k1gInSc1
[	[	kIx(
<g/>
meʁ	meʁ	k?
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
metropolitní	metropolitní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Paříže	Paříž	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
departmentu	department	k1gInSc6
Essonne	Essonne	k1gInSc6
a	a	k8xC
regionu	region	k1gInSc3
Île-de-France	Île-de-France	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
centra	centrum	k1gNnSc2
Paříže	Paříž	k1gFnSc2
je	být	k5eAaImIp3nS
vzdálena	vzdálen	k2eAgFnSc1d1
55	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
<g/>
:	:	kIx,
Saint-Escobille	Saint-Escobille	k1gInSc1
<g/>
,	,	kIx,
Plessis-Saint-Benoist	Plessis-Saint-Benoist	k1gInSc1
<g/>
,	,	kIx,
Chalo-Saint-Mars	Chalo-Saint-Mars	k1gInSc1
<g/>
,	,	kIx,
Oysonville	Oysonville	k1gNnSc1
a	a	k8xC
Congerville-Thionville	Congerville-Thionville	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
departementu	departement	k1gInSc6
Essonne	Essonn	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mérobert	Mérobert	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mérobert	Mérobert	k1gInSc1
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
Dourdan	Dourdan	k1gInSc1
</s>
<s>
Authon-la-Plaine	Authon-la-Plainout	k5eAaPmIp3nS
•	•	k?
Chatignonville	Chatignonville	k1gInSc1
•	•	k?
Corbreuse	Corbreuse	k1gFnSc2
•	•	k?
Dourdan	Dourdan	k1gInSc1
•	•	k?
La	la	k1gNnSc7
Forê	Forê	k1gFnSc2
•	•	k?
Les	les	k1gInSc1
Granges-le-Roi	Granges-le-Ro	k1gFnSc2
•	•	k?
Mérobert	Mérobert	k1gMnSc1
•	•	k?
Plessis-Saint-Benoist	Plessis-Saint-Benoist	k1gMnSc1
•	•	k?
Richarville	Richarville	k1gInSc1
•	•	k?
Roinville	Roinville	k1gInSc1
•	•	k?
Saint-Escobille	Saint-Escobille	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
