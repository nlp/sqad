<s>
Texaská	texaský	k2eAgFnSc1d1	texaská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
University	universita	k1gFnPc1	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
at	at	k?	at
Austin	Austin	k1gInSc1	Austin
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
University	universita	k1gFnSc2	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
UT	UT	kA	UT
nebo	nebo	k8xC	nebo
Texas	Texas	kA	Texas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hlavní	hlavní	k2eAgInSc1d1	hlavní
kampus	kampus	k1gInSc1	kampus
University	universita	k1gFnSc2	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
Systems	Systems	k1gInSc1	Systems
<g/>
.	.	kIx.	.
</s>
