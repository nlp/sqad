<s>
Texaská	texaský	k2eAgFnSc1d1	texaská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
University	universita	k1gFnPc1	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
at	at	k?	at
Austin	Austin	k1gInSc1	Austin
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
University	universita	k1gFnSc2	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
UT	UT	kA	UT
nebo	nebo	k8xC	nebo
Texas	Texas	kA	Texas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hlavní	hlavní	k2eAgInSc1d1	hlavní
kampus	kampus	k1gInSc1	kampus
University	universita	k1gFnSc2	universita
of	of	k?	of
Texas	Texas	k1gInSc1	Texas
Systems	Systems	k1gInSc1	Systems
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ji	on	k3xPp3gFnSc4	on
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
50	[number]	k4	50
201	[number]	k4	201
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
pátou	pátá	k1gFnSc4	pátá
největší	veliký	k2eAgFnSc7d3	veliký
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Texaská	texaský	k2eAgFnSc1d1	texaská
univerzita	univerzita	k1gFnSc1	univerzita
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgFnPc3d3	nejlepší
státním	státní	k2eAgFnPc3d1	státní
školám	škola	k1gFnPc3	škola
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Public	publicum	k1gNnPc2	publicum
Ivy	Ivo	k1gMnSc2	Ivo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Asociace	asociace	k1gFnSc2	asociace
amerických	americký	k2eAgFnPc2d1	americká
univerzit	univerzita	k1gFnPc2	univerzita
(	(	kIx(	(
<g/>
Association	Association	k1gInSc1	Association
of	of	k?	of
American	American	k1gInSc1	American
Universities	Universities	k1gInSc1	Universities
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolku	spolek	k1gInSc2	spolek
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
severoamerických	severoamerický	k2eAgFnPc2d1	severoamerická
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1	sportovní
týmy	tým	k1gInPc1	tým
Texaské	texaský	k2eAgFnSc2d1	texaská
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Longhorns	Longhorns	k1gInSc4	Longhorns
<g/>
.	.	kIx.	.
</s>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Joseph	Joseph	k1gMnSc1	Joseph
Muller	Muller	k1gMnSc1	Muller
-	-	kIx~	-
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
Ilja	Ilja	k1gFnSc1	Ilja
Prigogine	Prigogin	k1gInSc5	Prigogin
-	-	kIx~	-
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Steven	Steven	k2eAgInSc1d1	Steven
Weinberg	Weinberg	k1gInSc1	Weinberg
-	-	kIx~	-
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
Alan	Alan	k1gMnSc1	Alan
Bean	Bean	k1gMnSc1	Bean
-	-	kIx~	-
astronaut	astronaut	k1gMnSc1	astronaut
Jeb	Jeb	k1gMnSc1	Jeb
Bush	Bush	k1gMnSc1	Bush
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
Texas	Texas	k1gInSc1	Texas
Laura	Laura	k1gFnSc1	Laura
Bushová	Bushová	k1gFnSc1	Bushová
-	-	kIx~	-
bývalá	bývalý	k2eAgFnSc1d1	bývalá
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
USA	USA	kA	USA
Ramsey	Ramsea	k1gFnPc1	Ramsea
Clark	Clark	k1gInSc1	Clark
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
mírového	mírový	k2eAgNnSc2d1	Mírové
hnutí	hnutí	k1gNnSc2	hnutí
John	John	k1gMnSc1	John
Maxwell	maxwell	k1gInSc4	maxwell
Coetzee	Coetze	k1gFnSc2	Coetze
-	-	kIx~	-
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Lady	lady	k1gFnPc2	lady
Bird	Birda	k1gFnPc2	Birda
Johnsonová	Johnsonový	k2eAgFnSc1d1	Johnsonová
-	-	kIx~	-
bývalá	bývalý	k2eAgFnSc1d1	bývalá
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
USA	USA	kA	USA
Janis	Janis	k1gFnSc1	Janis
Joplin	Joplin	k1gInSc1	Joplin
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Bruce	Bruce	k1gMnSc1	Bruce
Sterling	sterling	k1gInSc1	sterling
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Uhlířová	Uhlířová	k1gFnSc1	Uhlířová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
tenistka	tenistka	k1gFnSc1	tenistka
Rick	Rick	k1gMnSc1	Rick
Riordan	Riordan	k1gMnSc1	Riordan
-	-	kIx~	-
americký	americký	k2eAgMnSc1d1	americký
spisovateľ	spisovateľ	k?	spisovateľ
Renée	René	k1gMnPc4	René
Zellweger	Zellwegra	k1gFnPc2	Zellwegra
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
</s>
