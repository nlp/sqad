<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Španělské	španělský	k2eAgNnSc1d1	španělské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
galicijsky	galicijsky	k6eAd1	galicijsky
Reino	Rein	k2eAgNnSc4d1	Reino
de	de	k?	de
Españ	Españ	k1gFnSc7	Españ
<g/>
;	;	kIx,	;
katalánsky	katalánsky	k6eAd1	katalánsky
Regne	Regn	k1gInSc5	Regn
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Espanya	Espanya	k1gMnSc1	Espanya
<g/>
;	;	kIx,	;
baskicky	baskicky	k6eAd1	baskicky
Espainiako	Espainiako	k1gNnSc1	Espainiako
Erresuma	Erresumum	k1gNnSc2	Erresumum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
