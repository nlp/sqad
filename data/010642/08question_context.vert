<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Komárov	Komárov	k1gInSc4	Komárov
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
v	v	k7c6	v
Černovické	Černovický	k2eAgFnSc6d1	Černovická
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
funkčních	funkční	k2eAgFnPc2d1	funkční
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Luh	luh	k1gInSc1	luh
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Komárov	Komárov	k1gInSc1	Komárov
<g/>
)	)	kIx)	)
postaven	postavit	k5eAaPmNgMnS	postavit
koncem	koncem	k7c2	koncem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
kaple	kaple	k1gFnSc2	kaple
zasvěcené	zasvěcený	k2eAgFnSc2d1	zasvěcená
svatému	svatý	k1gMnSc3	svatý
Jiljí	Jiljí	k1gMnPc2	Jiljí
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
řádu	řád	k1gInSc3	řád
benediktinů	benediktin	k1gMnPc2	benediktin
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1220	[number]	k4	1220
přistavěli	přistavět	k5eAaPmAgMnP	přistavět
ještě	ještě	k9	ještě
obytnou	obytný	k2eAgFnSc4d1	obytná
věž	věž	k1gFnSc4	věž
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
.	.	kIx.	.
</s>

