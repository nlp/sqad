<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Komárov	Komárov	k1gInSc4	Komárov
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-jih	Brnoiha	k1gFnPc2	Brno-jiha
v	v	k7c6	v
Černovické	Černovický	k2eAgFnSc6d1	Černovická
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
funkčních	funkční	k2eAgFnPc2d1	funkční
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Luh	luh	k1gInSc1	luh
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Komárov	Komárov	k1gInSc1	Komárov
<g/>
)	)	kIx)	)
postaven	postavit	k5eAaPmNgMnS	postavit
koncem	koncem	k7c2	koncem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
kaple	kaple	k1gFnSc2	kaple
zasvěcené	zasvěcený	k2eAgFnSc2d1	zasvěcená
svatému	svatý	k1gMnSc3	svatý
Jiljí	Jiljí	k1gMnPc2	Jiljí
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
řádu	řád	k1gInSc3	řád
benediktinů	benediktin	k1gMnPc2	benediktin
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1220	[number]	k4	1220
přistavěli	přistavět	k5eAaPmAgMnP	přistavět
ještě	ještě	k9	ještě
obytnou	obytný	k2eAgFnSc4d1	obytná
věž	věž	k1gFnSc4	věž
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešních	dnešní	k2eAgFnPc2d1	dnešní
zahrad	zahrada	k1gFnPc2	zahrada
mezi	mezi	k7c7	mezi
kostelem	kostel	k1gInSc7	kostel
a	a	k8xC	a
ulicí	ulice	k1gFnSc7	ulice
Lužnou	Lužna	k1gFnSc7	Lužna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1350	[number]	k4	1350
byla	být	k5eAaImAgFnS	být
stržena	stržen	k2eAgFnSc1d1	stržena
východní	východní	k2eAgFnSc1d1	východní
stěna	stěna	k1gFnSc1	stěna
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
přistavěn	přistavěn	k2eAgInSc1d1	přistavěn
gotický	gotický	k2eAgInSc1d1	gotický
chór	chór	k1gInSc1	chór
a	a	k8xC	a
presbytář	presbytář	k1gInSc1	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
barokně	barokně	k6eAd1	barokně
zaklenuta	zaklenut	k2eAgFnSc1d1	zaklenuta
původní	původní	k2eAgFnSc1d1	původní
románská	románský	k2eAgFnSc1d1	románská
loď	loď	k1gFnSc1	loď
a	a	k8xC	a
proražena	proražen	k2eAgNnPc1d1	proraženo
dnešní	dnešní	k2eAgNnPc1d1	dnešní
velká	velký	k2eAgNnPc1d1	velké
okna	okno	k1gNnPc1	okno
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc4d1	jediné
zachovalé	zachovalý	k2eAgNnSc4d1	zachovalé
románské	románský	k2eAgNnSc4d1	románské
okno	okno	k1gNnSc4	okno
dnes	dnes	k6eAd1	dnes
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
zdi	zeď	k1gFnSc6	zeď
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věž	věž	k1gFnSc1	věž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
zřízení	zřízení	k1gNnSc4	zřízení
komárovské	komárovský	k2eAgFnSc2d1	komárovská
farnosti	farnost	k1gFnSc2	farnost
prošel	projít	k5eAaPmAgInS	projít
kostel	kostel	k1gInSc1	kostel
dalšími	další	k2eAgFnPc7d1	další
stavebními	stavební	k2eAgFnPc7d1	stavební
úpravami	úprava	k1gFnPc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
kaple	kaple	k1gFnSc1	kaple
svátosti	svátost	k1gFnSc2	svátost
smíření	smíření	k1gNnSc2	smíření
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
novogoticky	novogoticky	k6eAd1	novogoticky
upraven	upravit	k5eAaPmNgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
archeologickém	archeologický	k2eAgInSc6d1	archeologický
výzkumu	výzkum	k1gInSc6	výzkum
(	(	kIx(	(
<g/>
provádělo	provádět	k5eAaImAgNnS	provádět
Muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dany	Dana	k1gFnSc2	Dana
Cejnkové	Cejnková	k1gFnSc2	Cejnková
<g/>
)	)	kIx)	)
nalezeny	nalezen	k2eAgInPc1d1	nalezen
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
prošel	projít	k5eAaPmAgInS	projít
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
letech	let	k1gInPc6	let
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
provedeno	proveden	k2eAgNnSc1d1	provedeno
mj.	mj.	kA	mj.
statické	statický	k2eAgNnSc1d1	statické
zajištění	zajištění	k1gNnSc1	zajištění
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
odvlhčení	odvlhčení	k1gNnSc2	odvlhčení
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
krovů	krov	k1gInPc2	krov
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
krytina	krytina	k1gFnSc1	krytina
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
fasády	fasáda	k1gFnPc1	fasáda
<g/>
,	,	kIx,	,
elektroinstalace	elektroinstalace	k1gFnPc1	elektroinstalace
nebo	nebo	k8xC	nebo
restaurování	restaurování	k1gNnSc1	restaurování
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
i	i	k8xC	i
vnějších	vnější	k2eAgInPc2d1	vnější
kamenných	kamenný	k2eAgInPc2d1	kamenný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Obnoven	obnovit	k5eAaPmNgInS	obnovit
byl	být	k5eAaImAgInS	být
i	i	k9	i
mobiliář	mobiliář	k1gInSc4	mobiliář
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
liturgicky	liturgicky	k6eAd1	liturgicky
i	i	k9	i
umělecky	umělecky	k6eAd1	umělecky
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
počátku	počátek	k1gInSc2	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Pozůstatky	pozůstatek	k1gInPc1	pozůstatek
původní	původní	k2eAgFnSc2d1	původní
kaple	kaple	k1gFnSc2	kaple
byly	být	k5eAaImAgFnP	být
odkryty	odkryt	k2eAgFnPc1d1	odkryta
a	a	k8xC	a
opět	opět	k6eAd1	opět
zasypány	zasypán	k2eAgFnPc1d1	zasypána
při	při	k7c6	při
archeologickém	archeologický	k2eAgInSc6d1	archeologický
průzkumu	průzkum	k1gInSc6	průzkum
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
komárovského	komárovský	k2eAgInSc2d1	komárovský
kláštera	klášter	k1gInSc2	klášter
zněl	znět	k5eAaImAgMnS	znět
Porta	porto	k1gNnPc4	porto
Dei	Dei	k1gFnSc2	Dei
(	(	kIx(	(
<g/>
brána	brána	k1gFnSc1	brána
Boží	boží	k2eAgFnSc1d1	boží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
komárovské	komárovský	k2eAgFnSc2d1	komárovská
louky	louka	k1gFnSc2	louka
odcházeli	odcházet	k5eAaImAgMnP	odcházet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
baroka	baroko	k1gNnSc2	baroko
poutníci	poutník	k1gMnPc1	poutník
do	do	k7c2	do
Mariazell	Mariazella	k1gFnPc2	Mariazella
<g/>
;	;	kIx,	;
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gNnSc2	jejich
putování	putování	k1gNnSc2	putování
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
komárovští	komárovský	k2eAgMnPc1d1	komárovský
farníci	farník	k1gMnPc1	farník
shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
k	k	k7c3	k
modlitbám	modlitba	k1gFnPc3	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
farnost	farnost	k1gFnSc1	farnost
scházela	scházet	k5eAaImAgFnS	scházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
uvítání	uvítání	k1gNnSc4	uvítání
chlebem	chléb	k1gInSc7	chléb
a	a	k8xC	a
koláči	koláč	k1gInPc7	koláč
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Koláčovému	koláčový	k2eAgInSc3d1	koláčový
svátku	svátek	k1gInSc3	svátek
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
svátku	svátek	k1gInSc2	svátek
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc1	Jiljí
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pozměněné	pozměněný	k2eAgFnSc6d1	pozměněná
podobě	podoba	k1gFnSc6	podoba
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Betlém	Betlém	k1gInSc4	Betlém
==	==	k?	==
</s>
</p>
<p>
<s>
Betlém	Betlém	k1gInSc1	Betlém
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vánoční	vánoční	k2eAgFnSc1d1	vánoční
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
presbytáře	presbytář	k1gInSc2	presbytář
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
lodi	loď	k1gFnSc6	loď
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gMnSc4	on
sádrové	sádrový	k2eAgFnPc1d1	sádrová
figurky	figurka	k1gFnPc1	figurka
neznámého	známý	k2eNgMnSc2d1	neznámý
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
vždy	vždy	k6eAd1	vždy
od	od	k7c2	od
14	[number]	k4	14
do	do	k7c2	do
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FOLTÝN	Foltýn	k1gMnSc1	Foltýn
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
moravských	moravský	k2eAgInPc2d1	moravský
a	a	k8xC	a
slezských	slezský	k2eAgInPc2d1	slezský
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
878	[number]	k4	878
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Porta	porta	k1gFnSc1	porta
Dei	Dei	k1gFnSc1	Dei
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
v	v	k7c6	v
Brně-Komárově	Brně-Komárův	k2eAgNnSc6d1	Brně-Komárův
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Brno-Komárov	Brno-Komárov	k1gInSc1	Brno-Komárov
<g/>
,	,	kIx,	,
b.d.	b.d.	k?	b.d.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiljí	Jiljí	k1gMnSc2	Jiljí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
dějin	dějiny	k1gFnPc2	dějiny
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
-	-	kIx~	-
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiljí	Jiljí	k1gMnSc2	Jiljí
</s>
</p>
