<p>
<s>
Mykény	Mykény	k1gFnPc1	Mykény
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
Μ	Μ	k?	Μ
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Athén	Athéna	k1gFnPc2	Athéna
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Peloponésu	Peloponés	k1gInSc2	Peloponés
<g/>
.	.	kIx.	.
</s>
<s>
Mykény	Mykény	k1gFnPc1	Mykény
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
center	centrum	k1gNnPc2	centrum
řecké	řecký	k2eAgFnSc2d1	řecká
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dominovala	dominovat	k5eAaImAgFnS	dominovat
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
jižního	jižní	k2eAgNnSc2d1	jižní
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
obdobím	období	k1gNnSc7	období
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Mykény	Mykény	k1gFnPc4	Mykény
bylo	být	k5eAaImAgNnS	být
centrem	centrum	k1gNnSc7	centrum
první	první	k4xOgFnSc2	první
řecké	řecký	k2eAgFnSc2d1	řecká
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
mínojské	mínojský	k2eAgFnSc6d1	mínojská
kultuře	kultura	k1gFnSc6	kultura
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
a	a	k8xC	a
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
probíhají	probíhat	k5eAaImIp3nP	probíhat
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
působil	působit	k5eAaImAgInS	působit
také	také	k6eAd1	také
proslulý	proslulý	k2eAgMnSc1d1	proslulý
amatérský	amatérský	k2eAgMnSc1d1	amatérský
archeolog	archeolog	k1gMnSc1	archeolog
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
<g/>
.	.	kIx.	.
<g/>
Mykéňané	Mykéňan	k1gMnPc1	Mykéňan
(	(	kIx(	(
<g/>
známí	známý	k1gMnPc1	známý
také	také	k9	také
jako	jako	k8xS	jako
Achájci	Achájce	k1gMnPc1	Achájce
<g/>
)	)	kIx)	)
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mykénskou	mykénský	k2eAgFnSc4d1	mykénská
civilizaci	civilizace	k1gFnSc4	civilizace
tvořily	tvořit	k5eAaImAgFnP	tvořit
nejprve	nejprve	k6eAd1	nejprve
vesnice	vesnice	k1gFnPc4	vesnice
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
obývali	obývat	k5eAaImAgMnP	obývat
lidé	člověk	k1gMnPc1	člověk
hovořící	hovořící	k2eAgFnSc7d1	hovořící
starou	starý	k2eAgFnSc7d1	stará
formou	forma	k1gFnSc7	forma
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přerostly	přerůst	k5eAaPmAgFnP	přerůst
mnohé	mnohý	k2eAgFnPc1d1	mnohá
vesnice	vesnice	k1gFnPc1	vesnice
v	v	k7c4	v
opevněná	opevněný	k2eAgNnPc4d1	opevněné
města	město	k1gNnPc4	město
s	s	k7c7	s
bohatými	bohatý	k2eAgInPc7d1	bohatý
paláci	palác	k1gInPc7	palác
a	a	k8xC	a
s	s	k7c7	s
luxusním	luxusní	k2eAgNnSc7d1	luxusní
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
konkurovalo	konkurovat	k5eAaImAgNnS	konkurovat
zboží	zboží	k1gNnSc1	zboží
velmi	velmi	k6eAd1	velmi
zručných	zručný	k2eAgMnPc2d1	zručný
krétských	krétský	k2eAgMnPc2d1	krétský
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Mykénskou	mykénský	k2eAgFnSc4d1	mykénská
civilizaci	civilizace	k1gFnSc4	civilizace
tvořilo	tvořit	k5eAaImAgNnS	tvořit
asi	asi	k9	asi
20	[number]	k4	20
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
ovládali	ovládat	k5eAaImAgMnP	ovládat
Mykéňané	Mykéňan	k1gMnPc1	Mykéňan
celý	celý	k2eAgInSc4d1	celý
jih	jih	k1gInSc4	jih
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sahalo	sahat	k5eAaImAgNnS	sahat
jejich	jejich	k3xOp3gNnSc1	jejich
území	území	k1gNnSc1	území
až	až	k9	až
do	do	k7c2	do
Boiótie	Boiótie	k1gFnSc2	Boiótie
a	a	k8xC	a
Thessalie	Thessalie	k1gFnSc2	Thessalie
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
potom	potom	k6eAd1	potom
až	až	k9	až
k	k	k7c3	k
Iónským	iónský	k2eAgInPc3d1	iónský
ostrovům	ostrov	k1gInPc3	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
Mykén	Mykény	k1gFnPc2	Mykény
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
řeckého	řecký	k2eAgNnSc2d1	řecké
bájesloví	bájesloví	k1gNnSc2	bájesloví
založil	založit	k5eAaPmAgMnS	založit
Mykény	Mykény	k1gFnPc1	Mykény
Perseus	Perseus	k1gMnSc1	Perseus
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
sídlili	sídlit	k5eAaImAgMnP	sídlit
králové	král	k1gMnPc1	král
krvavého	krvavý	k2eAgInSc2d1	krvavý
rodu	rod	k1gInSc2	rod
Pelopovců	Pelopovec	k1gInPc2	Pelopovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
Pelopovcem	Pelopovec	k1gMnSc7	Pelopovec
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
Agamemnon	Agamemnon	k1gMnSc1	Agamemnon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
řeckých	řecký	k2eAgNnPc2d1	řecké
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Trojské	trojský	k2eAgFnSc6d1	Trojská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
město	město	k1gNnSc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Mykénská	mykénský	k2eAgFnSc1d1	mykénská
pevnost	pevnost	k1gFnSc1	pevnost
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
dominantní	dominantní	k2eAgNnSc4d1	dominantní
strategické	strategický	k2eAgNnSc4d1	strategické
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
kopce	kopec	k1gInSc2	kopec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
mohlo	moct	k5eAaImAgNnS	moct
město	město	k1gNnSc1	město
ovládat	ovládat	k5eAaImF	ovládat
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
nížinu	nížina	k1gFnSc4	nížina
i	i	k9	i
horské	horský	k2eAgInPc1d1	horský
průsmyky	průsmyk	k1gInPc1	průsmyk
až	až	k9	až
ke	k	k7c3	k
Korintu	Korint	k1gInSc3	Korint
<g/>
.	.	kIx.	.
</s>
<s>
Mohutné	mohutný	k2eAgNnSc1d1	mohutné
zdivo	zdivo	k1gNnSc1	zdivo
z	z	k7c2	z
nepravidelných	pravidelný	k2eNgInPc2d1	nepravidelný
vápencových	vápencový	k2eAgInPc2d1	vápencový
kvádrů	kvádr	k1gInPc2	kvádr
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
až	až	k9	až
pěti	pět	k4xCc2	pět
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kyklopské	kyklopský	k2eAgInPc4d1	kyklopský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pozdější	pozdní	k2eAgFnPc4d2	pozdější
generace	generace	k1gFnPc4	generace
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gNnPc4	on
mohli	moct	k5eAaImAgMnP	moct
postavit	postavit	k5eAaPmF	postavit
pouze	pouze	k6eAd1	pouze
jednoocí	jednooký	k2eAgMnPc1d1	jednooký
mytičtí	mytický	k2eAgMnPc1d1	mytický
obři	obr	k1gMnPc1	obr
zvaní	zvaný	k2eAgMnPc1d1	zvaný
Kyklopové	Kyklop	k1gMnPc1	Kyklop
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
Lví	lví	k2eAgFnSc1d1	lví
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Nad	nad	k7c7	nad
mohutným	mohutný	k2eAgInSc7d1	mohutný
portálovým	portálový	k2eAgInSc7d1	portálový
překladem	překlad	k1gInSc7	překlad
stojí	stát	k5eAaImIp3nP	stát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
dva	dva	k4xCgMnPc1	dva
kamenní	kamenný	k2eAgMnPc1d1	kamenný
lvi	lev	k1gMnPc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
brány	brána	k1gFnSc2	brána
vede	vést	k5eAaImIp3nS	vést
cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
královskému	královský	k2eAgInSc3d1	královský
paláci	palác	k1gInSc3	palác
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
dva	dva	k4xCgInPc1	dva
trakty	trakt	k1gInPc1	trakt
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
chodbami	chodba	k1gFnPc7	chodba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
zásobárny	zásobárna	k1gFnPc1	zásobárna
<g/>
.	.	kIx.	.
</s>
<s>
Malované	malovaný	k2eAgFnPc1d1	malovaná
podlahy	podlaha	k1gFnPc1	podlaha
a	a	k8xC	a
bohaté	bohatý	k2eAgFnPc1d1	bohatá
fresky	freska	k1gFnPc1	freska
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
palácích	palác	k1gInPc6	palác
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
paláce	palác	k1gInSc2	palác
stojí	stát	k5eAaImIp3nP	stát
domy	dům	k1gInPc1	dům
méně	málo	k6eAd2	málo
významných	významný	k2eAgMnPc2d1	významný
občanů	občan	k1gMnPc2	občan
včetně	včetně	k7c2	včetně
tříposchoďového	tříposchoďový	k2eAgInSc2d1	tříposchoďový
domu	dům	k1gInSc2	dům
zvaného	zvaný	k2eAgInSc2d1	zvaný
Sloupový	sloupový	k2eAgInSc4d1	sloupový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
sám	sám	k3xTgInSc1	sám
byl	být	k5eAaImAgInS	být
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
přestavován	přestavován	k2eAgInSc1d1	přestavován
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
zahrnut	zahrnut	k2eAgInSc1d1	zahrnut
mladší	mladý	k2eAgInSc4d2	mladší
šachtový	šachtový	k2eAgInSc4d1	šachtový
okruh	okruh	k1gInSc4	okruh
a	a	k8xC	a
vybudovány	vybudován	k2eAgFnPc4d1	vybudována
mohutné	mohutný	k2eAgFnPc4d1	mohutná
hradby	hradba	k1gFnPc4	hradba
i	i	k9	i
s	s	k7c7	s
proslulou	proslulý	k2eAgFnSc7d1	proslulá
Lví	lví	k2eAgFnSc7d1	lví
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
máme	mít	k5eAaImIp1nP	mít
celou	celý	k2eAgFnSc4d1	celá
stavbu	stavba	k1gFnSc4	stavba
zachovanou	zachovaný	k2eAgFnSc4d1	zachovaná
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dostala	dostat	k5eAaPmAgFnS	dostat
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1230	[number]	k4	1230
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přistavěn	přistavěn	k2eAgMnSc1d1	přistavěn
z	z	k7c2	z
obranných	obranný	k2eAgInPc2d1	obranný
důvodů	důvod	k1gInPc2	důvod
malý	malý	k2eAgInSc1d1	malý
výběžek	výběžek	k1gInSc1	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
obležen	oblehnout	k5eAaPmNgInS	oblehnout
a	a	k8xC	a
zničeny	zničen	k2eAgFnPc1d1	zničena
palácové	palácový	k2eAgFnPc1d1	palácová
dílny	dílna	k1gFnPc1	dílna
a	a	k8xC	a
skladiště	skladiště	k1gNnSc1	skladiště
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
sám	sám	k3xTgMnSc1	sám
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
definitivní	definitivní	k2eAgFnSc4d1	definitivní
zkáze	zkáza	k1gFnSc3	zkáza
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1125	[number]	k4	1125
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
K	k	k7c3	k
bývalé	bývalý	k2eAgFnSc3d1	bývalá
slávě	sláva	k1gFnSc3	sláva
se	se	k3xPyFc4	se
už	už	k6eAd1	už
Mykény	Mykény	k1gFnPc1	Mykény
nikdy	nikdy	k6eAd1	nikdy
nevzpamatovaly	vzpamatovat	k5eNaPmAgFnP	vzpamatovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zde	zde	k6eAd1	zde
později	pozdě	k6eAd2	pozdě
vznikala	vznikat	k5eAaImAgNnP	vznikat
malá	malý	k2eAgNnPc1d1	malé
sídliště	sídliště	k1gNnPc1	sídliště
a	a	k8xC	a
ve	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dokonce	dokonce	k9	dokonce
malý	malý	k2eAgInSc1d1	malý
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Mykén	Mykény	k1gFnPc2	Mykény
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
také	také	k9	také
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
a	a	k8xC	a
dílny	dílna	k1gFnPc1	dílna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
kopulové	kopulový	k2eAgInPc4d1	kopulový
hroby	hrob	k1gInPc4	hrob
<g/>
,	,	kIx,	,
náhrobní	náhrobní	k2eAgFnPc4d1	náhrobní
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
oltáře	oltář	k1gInPc4	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Malířské	malířský	k2eAgNnSc1d1	malířské
umění	umění	k1gNnSc1	umění
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
ornamenty	ornament	k1gInPc7	ornament
a	a	k8xC	a
stylizacemi	stylizace	k1gFnPc7	stylizace
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
i	i	k8xC	i
vázách	váza	k1gFnPc6	váza
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
i	i	k8xC	i
město	město	k1gNnSc1	město
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
bohatá	bohatý	k2eAgNnPc1d1	bohaté
na	na	k7c4	na
naleziště	naleziště	k1gNnSc4	naleziště
skvostných	skvostný	k2eAgInPc2d1	skvostný
pokladů	poklad	k1gInPc2	poklad
nesmírné	smírný	k2eNgInPc4d1	nesmírný
umělecké	umělecký	k2eAgInPc4d1	umělecký
a	a	k8xC	a
hmotné	hmotný	k2eAgFnPc4d1	hmotná
ceny	cena	k1gFnPc4	cena
doby	doba	k1gFnSc2	doba
mykénské	mykénský	k2eAgFnSc2d1	mykénská
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
těmto	tento	k3xDgFnPc3	tento
památkám	památka	k1gFnPc3	památka
můžeme	moct	k5eAaImIp1nP	moct
obdivovat	obdivovat	k5eAaImF	obdivovat
ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
síni	síň	k1gFnSc6	síň
athénského	athénský	k2eAgNnSc2d1	athénské
Národního	národní	k2eAgNnSc2d1	národní
archeologického	archeologický	k2eAgNnSc2d1	Archeologické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mykénské	mykénský	k2eAgFnSc2d1	mykénská
hrobky	hrobka	k1gFnSc2	hrobka
==	==	k?	==
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
poznání	poznání	k1gNnSc2	poznání
vedle	vedle	k7c2	vedle
vykopávek	vykopávka	k1gFnPc2	vykopávka
samotných	samotný	k2eAgFnPc2d1	samotná
budov	budova	k1gFnPc2	budova
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
šachtové	šachtový	k2eAgInPc1d1	šachtový
hroby	hrob	k1gInPc1	hrob
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
objevil	objevit	k5eAaPmAgMnS	objevit
Schliemann	Schliemann	k1gMnSc1	Schliemann
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
vytesaná	vytesaný	k2eAgNnPc1d1	vytesané
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
mající	mající	k2eAgInSc4d1	mající
tvar	tvar	k1gInSc4	tvar
pravoúhlých	pravoúhlý	k2eAgFnPc2d1	pravoúhlá
studní	studna	k1gFnPc2	studna
o	o	k7c6	o
značné	značný	k2eAgFnSc6d1	značná
hloubce	hloubka	k1gFnSc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
objevil	objevit	k5eAaPmAgMnS	objevit
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
velké	velká	k1gFnSc2	velká
množství	množství	k1gNnSc2	množství
zasypaných	zasypaný	k2eAgFnPc2d1	zasypaná
staveb	stavba	k1gFnPc2	stavba
zarážející	zarážející	k2eAgFnSc2d1	zarážející
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
antický	antický	k2eAgMnSc1d1	antický
autor	autor	k1gMnSc1	autor
Pausaniás	Pausaniása	k1gFnPc2	Pausaniása
pokládal	pokládat	k5eAaImAgMnS	pokládat
tyto	tento	k3xDgFnPc4	tento
stavby	stavba	k1gFnPc4	stavba
nikoli	nikoli	k9	nikoli
za	za	k7c4	za
dílo	dílo	k1gNnSc4	dílo
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
stavby	stavba	k1gFnPc4	stavba
jednookých	jednooký	k2eAgMnPc2d1	jednooký
obrů	obr	k1gMnPc2	obr
Kyklopů	Kyklop	k1gMnPc2	Kyklop
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
divit	divit	k5eAaImF	divit
<g/>
,	,	kIx,	,
vždyť	vždyť	k8xC	vždyť
stavby	stavba	k1gFnPc1	stavba
se	se	k3xPyFc4	se
skládaly	skládat	k5eAaImAgFnP	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
kamenných	kamenný	k2eAgInPc2d1	kamenný
bloků	blok	k1gInPc2	blok
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
3	[number]	k4	3
x	x	k?	x
1	[number]	k4	1
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Schliemann	Schliemann	k1gInSc1	Schliemann
v	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
objevil	objevit	k5eAaPmAgMnS	objevit
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
šachtových	šachtový	k2eAgInPc2d1	šachtový
hrobů	hrob	k1gInPc2	hrob
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
okruh	okruh	k1gInSc1	okruh
hrobů	hrob	k1gInPc2	hrob
A	A	kA	A
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vykopávkách	vykopávka	k1gFnPc6	vykopávka
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
devatenáct	devatenáct	k4xCc4	devatenáct
koster	kostra	k1gFnPc2	kostra
obklopených	obklopený	k2eAgFnPc2d1	obklopená
zlatými	zlatý	k2eAgInPc7d1	zlatý
šperky	šperk	k1gInPc7	šperk
<g/>
,	,	kIx,	,
zlatem	zlato	k1gNnSc7	zlato
a	a	k8xC	a
stříbrem	stříbro	k1gNnSc7	stříbro
vykládanými	vykládaný	k2eAgFnPc7d1	vykládaná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
brněním	brnění	k1gNnSc7	brnění
i	i	k8xC	i
zlatými	zlatá	k1gFnPc7	zlatá
a	a	k8xC	a
stříbrnými	stříbrný	k2eAgInPc7d1	stříbrný
poháry	pohár	k1gInPc7	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Bohatství	bohatství	k1gNnSc1	bohatství
těchto	tento	k3xDgFnPc2	tento
hrobek	hrobka	k1gFnPc2	hrobka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
nebo	nebo	k8xC	nebo
aristokracii	aristokracie	k1gFnSc4	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
měl	mít	k5eAaImAgMnS	mít
až	až	k9	až
400	[number]	k4	400
bronzířů	bronzíř	k1gInPc2	bronzíř
a	a	k8xC	a
stovky	stovka	k1gFnPc4	stovka
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zámožní	zámožný	k2eAgMnPc1d1	zámožný
Mykéňané	Mykéňan	k1gMnPc1	Mykéňan
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
vážili	vážit	k5eAaImAgMnP	vážit
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dováželi	dovážet	k5eAaImAgMnP	dovážet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nález	nález	k1gInSc1	nález
však	však	k9	však
na	na	k7c4	na
Schliemanna	Schliemann	k1gMnSc4	Schliemann
čekal	čekat	k5eAaImAgMnS	čekat
v	v	k7c6	v
hrobce	hrobka	k1gFnSc6	hrobka
číslo	číslo	k1gNnSc1	číslo
pět	pět	k4xCc4	pět
–	–	k?	–
nádherná	nádherný	k2eAgFnSc1d1	nádherná
zlatá	zlatý	k2eAgFnSc1d1	zlatá
maska	maska	k1gFnSc1	maska
<g/>
.	.	kIx.	.
</s>
<s>
Označil	označit	k5eAaPmAgMnS	označit
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
posmrtnou	posmrtný	k2eAgFnSc4d1	posmrtná
masku	maska	k1gFnSc4	maska
Agamemnona	Agamemnon	k1gMnSc2	Agamemnon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Homéra	Homér	k1gMnSc2	Homér
byl	být	k5eAaImAgMnS	být
Agamemnon	Agamemnon	k1gMnSc1	Agamemnon
králem	král	k1gMnSc7	král
Achaie	Achaie	k1gFnPc1	Achaie
(	(	kIx(	(
<g/>
dnešních	dnešní	k2eAgFnPc2d1	dnešní
Mykén	Mykény	k1gFnPc2	Mykény
<g/>
)	)	kIx)	)
a	a	k8xC	a
vůdcem	vůdce	k1gMnSc7	vůdce
řeckých	řecký	k2eAgInPc2d1	řecký
vojsk	vojsko	k1gNnPc2	vojsko
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
na	na	k7c4	na
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poblíž	poblíž	k7c2	poblíž
citadely	citadela	k1gFnSc2	citadela
mimo	mimo	k6eAd1	mimo
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
okruh	okruh	k1gInSc1	okruh
B	B	kA	B
–	–	k?	–
řada	řada	k1gFnSc1	řada
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
než	než	k8xS	než
hroby	hrob	k1gInPc4	hrob
šachtové	šachtový	k2eAgInPc4d1	šachtový
okruhu	okruh	k1gInSc3	okruh
A.	A.	kA	A.
Každý	každý	k3xTgInSc4	každý
hrob	hrob	k1gInSc4	hrob
tvoří	tvořit	k5eAaImIp3nP	tvořit
až	až	k9	až
třicet	třicet	k4xCc4	třicet
pět	pět	k4xCc4	pět
kruhových	kruhový	k2eAgFnPc2d1	kruhová
řad	řada	k1gFnPc2	řada
obrovských	obrovský	k2eAgInPc2d1	obrovský
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Řady	řada	k1gFnPc1	řada
se	se	k3xPyFc4	se
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
postupně	postupně	k6eAd1	postupně
zužují	zužovat	k5eAaImIp3nP	zužovat
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
tradiční	tradiční	k2eAgInPc4d1	tradiční
lidové	lidový	k2eAgInPc4d1	lidový
úly	úl	k1gInPc4	úl
<g/>
.	.	kIx.	.
</s>
<s>
Mykéňané	Mykéňan	k1gMnPc1	Mykéňan
je	on	k3xPp3gMnPc4	on
stavěli	stavět	k5eAaImAgMnP	stavět
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
asi	asi	k9	asi
po	po	k7c4	po
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Přicházelo	přicházet	k5eAaImAgNnS	přicházet
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
chodbou	chodba	k1gFnSc7	chodba
<g/>
,	,	kIx,	,
zakončenou	zakončený	k2eAgFnSc4d1	zakončená
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
dveřmi	dveře	k1gFnPc7	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
pohřbu	pohřeb	k1gInSc6	pohřeb
byly	být	k5eAaImAgFnP	být
dveře	dveře	k1gFnPc1	dveře
zazděny	zazdít	k5eAaPmNgFnP	zazdít
a	a	k8xC	a
chodba	chodba	k1gFnSc1	chodba
zasypána	zasypán	k2eAgFnSc1d1	zasypána
hlínou	hlína	k1gFnSc7	hlína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejhonosnějšími	honosný	k2eAgFnPc7d3	nejhonosnější
hrobkami	hrobka	k1gFnPc7	hrobka
v	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
je	být	k5eAaImIp3nS	být
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Atreova	Atreův	k2eAgFnSc1d1	Atreova
pokladnice	pokladnice	k1gFnSc1	pokladnice
(	(	kIx(	(
<g/>
Atreus	Atreus	k1gInSc1	Atreus
byl	být	k5eAaImAgMnS	být
Agamemnonovým	Agamemnonův	k2eAgMnSc7d1	Agamemnonův
otcem	otec	k1gMnSc7	otec
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrobka	hrobka	k1gFnSc1	hrobka
Klytaimnéstry	Klytaimnéstra	k1gFnSc2	Klytaimnéstra
<g/>
.	.	kIx.	.
</s>
<s>
Átreova	Átreův	k2eAgFnSc1d1	Átreova
pokladnice	pokladnice	k1gFnSc1	pokladnice
měla	mít	k5eAaImAgFnS	mít
téměř	téměř	k6eAd1	téměř
6	[number]	k4	6
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
vchod	vchod	k1gInSc4	vchod
<g/>
,	,	kIx,	,
otvírající	otvírající	k2eAgInPc4d1	otvírající
se	se	k3xPyFc4	se
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
vysoké	vysoký	k2eAgFnSc2d1	vysoká
13	[number]	k4	13
m	m	kA	m
a	a	k8xC	a
široké	široký	k2eAgFnSc2d1	široká
14	[number]	k4	14
m.	m.	k?	m.
Kdysi	kdysi	k6eAd1	kdysi
byla	být	k5eAaImAgFnS	být
obložena	obložit	k5eAaPmNgFnS	obložit
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
hrobkách	hrobka	k1gFnPc6	hrobka
bývaly	bývat	k5eAaImAgFnP	bývat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stejně	stejně	k6eAd1	stejně
bohaté	bohatý	k2eAgInPc1d1	bohatý
poklady	poklad	k1gInPc1	poklad
jako	jako	k8xC	jako
v	v	k7c6	v
hrobech	hrob	k1gInPc6	hrob
šachtových	šachtový	k2eAgInPc6d1	šachtový
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
staletí	staletí	k1gNnPc2	staletí
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
pronikali	pronikat	k5eAaImAgMnP	pronikat
vykrádači	vykrádač	k1gMnPc1	vykrádač
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
z	z	k7c2	z
Atreovy	Atreův	k2eAgFnSc2d1	Atreova
pokladnice	pokladnice	k1gFnSc2	pokladnice
dnes	dnes	k6eAd1	dnes
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
hrubá	hrubý	k2eAgFnSc1d1	hrubá
stavba	stavba	k1gFnSc1	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Mykénsko-argonský	mykénskorgonský	k2eAgInSc1d1	mykénsko-argonský
region	region	k1gInSc1	region
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdéle	dlouho	k6eAd3	dlouho
osídlených	osídlený	k2eAgInPc2d1	osídlený
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
nálezy	nález	k1gInPc1	nález
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
neolitu	neolit	k1gInSc2	neolit
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Řekové	Řek	k1gMnPc1	Řek
nebyli	být	k5eNaImAgMnP	být
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Přišli	přijít	k5eAaPmAgMnP	přijít
sem	sem	k6eAd1	sem
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
splynuli	splynout	k5eAaPmAgMnP	splynout
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
(	(	kIx(	(
<g/>
kmeny	kmen	k1gInPc1	kmen
Achájů	Acháj	k1gInPc2	Acháj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mykénskou	mykénský	k2eAgFnSc4d1	mykénská
společnost	společnost	k1gFnSc4	společnost
tvořili	tvořit	k5eAaImAgMnP	tvořit
hlavně	hlavně	k9	hlavně
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
válečníci	válečník	k1gMnPc1	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
surovin	surovina	k1gFnPc2	surovina
je	být	k5eAaImIp3nS	být
nutil	nutit	k5eAaImAgMnS	nutit
k	k	k7c3	k
dálkovým	dálkový	k2eAgInPc3d1	dálkový
obchodům	obchod	k1gInPc3	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vyváželi	vyvážet	k5eAaImAgMnP	vyvážet
především	především	k6eAd1	především
umělecké	umělecký	k2eAgInPc4d1	umělecký
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
olivový	olivový	k2eAgInSc4d1	olivový
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Dováželi	dovážet	k5eAaImAgMnP	dovážet
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Paláce	palác	k1gInPc1	palác
byly	být	k5eAaImAgInP	být
správními	správní	k2eAgNnPc7d1	správní
středisky	středisko	k1gNnPc7	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
mohutných	mohutný	k2eAgInPc2d1	mohutný
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
"	"	kIx"	"
<g/>
kyklopských	kyklopský	k2eAgMnPc2d1	kyklopský
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
bájného	bájný	k2eAgMnSc2d1	bájný
obra	obr	k1gMnSc2	obr
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
městem	město	k1gNnSc7	město
a	a	k8xC	a
hradem	hrad	k1gInSc7	hrad
Mykény	Mykény	k1gFnPc4	Mykény
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
řeckých	řecký	k2eAgInPc2d1	řecký
kmenů	kmen	k1gInPc2	kmen
Achajové	Achajový	k2eAgNnSc1d1	Achajový
<g/>
.	.	kIx.	.
</s>
<s>
Mykénská	mykénský	k2eAgFnSc1d1	mykénská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
egejsko-krétské	egejskorétský	k2eAgFnSc2d1	egejsko-krétský
kultury	kultura	k1gFnSc2	kultura
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
vlastního	vlastní	k2eAgNnSc2d1	vlastní
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholení	vyvrcholení	k1gNnSc4	vyvrcholení
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
r.	r.	kA	r.
1400	[number]	k4	1400
až	až	k9	až
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Historickou	historický	k2eAgFnSc7d1	historická
osobou	osoba	k1gFnSc7	osoba
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Agamemnon	Agamemnon	k1gMnSc1	Agamemnon
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
spojených	spojený	k2eAgFnPc2d1	spojená
vojsk	vojsko	k1gNnPc2	vojsko
řeckých	řecký	k2eAgMnPc2d1	řecký
knížat	kníže	k1gMnPc2wR	kníže
v	v	k7c6	v
trojské	trojský	k2eAgFnSc6d1	Trojská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mykény	Mykény	k1gFnPc1	Mykény
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Krétu	Kréta	k1gFnSc4	Kréta
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
zakládat	zakládat	k5eAaImF	zakládat
kolonie	kolonie	k1gFnPc1	kolonie
okolo	okolo	k7c2	okolo
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Rhodos	Rhodos	k1gInSc1	Rhodos
a	a	k8xC	a
Kypr	Kypr	k1gInSc1	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
s	s	k7c7	s
Féničany	Féničan	k1gMnPc7	Féničan
<g/>
,	,	kIx,	,
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
její	její	k3xOp3gInSc1	její
zánik	zánik	k1gInSc1	zánik
začíná	začínat	k5eAaImIp3nS	začínat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mykény	Mykény	k1gFnPc1	Mykény
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
kočovným	kočovný	k2eAgMnPc3d1	kočovný
nájezdníkům	nájezdník	k1gMnPc3	nájezdník
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mořským	mořský	k2eAgInPc3d1	mořský
národům	národ	k1gInPc3	národ
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
řeckým	řecký	k2eAgInPc3d1	řecký
kmenům	kmen	k1gInPc3	kmen
Dórů	Dór	k1gMnPc2	Dór
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Archeologické	archeologický	k2eAgFnSc2d1	archeologická
vykopávky	vykopávka	k1gFnSc2	vykopávka
==	==	k?	==
</s>
</p>
<p>
<s>
Naleziště	naleziště	k1gNnSc1	naleziště
v	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
bylo	být	k5eAaImAgNnS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jedinečnosti	jedinečnost	k1gFnSc3	jedinečnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
areálu	areál	k1gInSc2	areál
archeologických	archeologický	k2eAgFnPc2d1	archeologická
vykopávek	vykopávka	k1gFnPc2	vykopávka
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
z	z	k7c2	z
parkoviště	parkoviště	k1gNnSc2	parkoviště
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
staršího	starý	k2eAgInSc2d2	starší
hrobového	hrobový	k2eAgInSc2d1	hrobový
okruhu	okruh	k1gInSc2	okruh
vpravo	vpravo	k6eAd1	vpravo
se	se	k3xPyFc4	se
stoupá	stoupat	k5eAaImIp3nS	stoupat
ke	k	k7c3	k
Lví	lví	k2eAgFnSc3d1	lví
bráně	brána	k1gFnSc3	brána
vysoké	vysoká	k1gFnSc2	vysoká
přes	přes	k7c4	přes
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nahoře	nahoře	k6eAd1	nahoře
na	na	k7c6	na
bráně	brána	k1gFnSc6	brána
na	na	k7c6	na
vápencové	vápencový	k2eAgFnSc6d1	vápencová
desce	deska	k1gFnSc6	deska
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
reliéf	reliéf	k1gInSc1	reliéf
dvou	dva	k4xCgFnPc2	dva
lvic	lvice	k1gFnPc2	lvice
vztyčených	vztyčený	k2eAgFnPc2d1	vztyčená
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
nohách	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
hrobový	hrobový	k2eAgInSc1d1	hrobový
okruh	okruh	k1gInSc1	okruh
se	s	k7c7	s
6	[number]	k4	6
šachtovými	šachtový	k2eAgInPc7d1	šachtový
hroby	hrob	k1gInPc7	hrob
o	o	k7c6	o
hloubce	hloubka	k1gFnSc6	hloubka
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
m	m	kA	m
je	být	k5eAaImIp3nS	být
napravo	napravo	k6eAd1	napravo
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
stoupá	stoupat	k5eAaImIp3nS	stoupat
po	po	k7c6	po
kamenné	kamenný	k2eAgFnSc6d1	kamenná
rampě	rampa	k1gFnSc6	rampa
a	a	k8xC	a
zbytcích	zbytek	k1gInPc6	zbytek
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
do	do	k7c2	do
vladařova	vladařův	k2eAgInSc2d1	vladařův
paláce	palác	k1gInSc2	palác
na	na	k7c6	na
akropoli	akropole	k1gFnSc6	akropole
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nádvoří	nádvoří	k1gNnSc3	nádvoří
s	s	k7c7	s
trůním	trůnit	k5eAaImIp1nS	trůnit
sálem	sál	k1gInSc7	sál
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
obytným	obytný	k2eAgInSc7d1	obytný
palácem	palác	k1gInSc7	palác
zdobeným	zdobený	k2eAgNnSc7d1	zdobené
vstupním	vstupní	k2eAgNnSc7d1	vstupní
sloupořadím	sloupořadí	k1gNnSc7	sloupořadí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
síň	síň	k1gFnSc1	síň
–	–	k?	–
megaron	megaron	k1gInSc1	megaron
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
zbytky	zbytek	k1gInPc4	zbytek
Athénina	Athénin	k2eAgInSc2d1	Athénin
chrámu	chrám	k1gInSc2	chrám
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
též	též	k9	též
dodatečně	dodatečně	k6eAd1	dodatečně
přistavěný	přistavěný	k2eAgInSc1d1	přistavěný
výběžek	výběžek	k1gInSc1	výběžek
opevnění	opevnění	k1gNnSc2	opevnění
z	z	k7c2	z
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
podzemní	podzemní	k2eAgInSc4d1	podzemní
pramen	pramen	k1gInSc4	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
tryská	tryskat	k5eAaImIp3nS	tryskat
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
a	a	k8xC	a
lze	lze	k6eAd1	lze
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
sestoupit	sestoupit	k5eAaPmF	sestoupit
po	po	k7c6	po
90	[number]	k4	90
schodech	schod	k1gInPc6	schod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výběžku	výběžek	k1gInSc2	výběžek
opevnění	opevnění	k1gNnSc2	opevnění
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
bran	brána	k1gFnPc2	brána
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
s.	s.	k?	s.
Eliáše	Eliáš	k1gMnSc2	Eliáš
(	(	kIx(	(
<g/>
Ajios	Ajios	k1gInSc1	Ajios
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
pohled	pohled	k1gInSc4	pohled
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
Mykény	Mykény	k1gFnPc4	Mykény
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
argolskou	argolský	k2eAgFnSc4d1	argolský
kotlinu	kotlina	k1gFnSc4	kotlina
až	až	k9	až
k	k	k7c3	k
arkadským	arkadský	k2eAgFnPc3d1	arkadská
horám	hora	k1gFnPc3	hora
uvnitř	uvnitř	k7c2	uvnitř
Peloponésu	Peloponés	k1gInSc2	Peloponés
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
areál	areál	k1gInSc4	areál
hradu	hrad	k1gInSc2	hrad
leží	ležet	k5eAaImIp3nS	ležet
ještě	ještě	k9	ještě
9	[number]	k4	9
kupolových	kupolový	k2eAgInPc2d1	kupolový
hrobů	hrob	k1gInPc2	hrob
(	(	kIx(	(
<g/>
tholů	thol	k1gInPc2	thol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polorozbořená	polorozbořený	k2eAgFnSc1d1	polorozbořená
hrobka	hrobka	k1gFnSc1	hrobka
Aigisthova	Aigisthova	k1gFnSc1	Aigisthova
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
hrobka	hrobka	k1gFnSc1	hrobka
Klytaiméstřina	Klytaiméstřina	k1gFnSc1	Klytaiméstřina
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
okolo	okolo	k7c2	okolo
1300	[number]	k4	1300
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
poškozená	poškozený	k2eAgFnSc1d1	poškozená
Lví	lví	k2eAgFnSc1d1	lví
hrobka	hrobka	k1gFnSc1	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
hroby	hrob	k1gInPc1	hrob
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
zvlněném	zvlněný	k2eAgInSc6d1	zvlněný
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
-	-	kIx~	-
průvodce	průvodce	k1gMnSc1	průvodce
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mykény	Mykény	k1gFnPc1	Mykény
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fotoalbum	fotoalbum	k1gNnSc4	fotoalbum
sbírek	sbírka	k1gFnPc2	sbírka
Archeologického	archeologický	k2eAgNnSc2d1	Archeologické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
<g/>
)	)	kIx)	)
</s>
</p>
