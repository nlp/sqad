<s>
Esplanade	Esplanást	k5eAaPmIp3nS	Esplanást
des	des	k1gNnSc1	des
Invalides	Invalidesa	k1gFnPc2	Invalidesa
(	(	kIx(	(
<g/>
Esplanáda	esplanáda	k1gFnSc1	esplanáda
Invalidovny	invalidovna	k1gFnSc2	invalidovna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
zelené	zelená	k1gFnSc6	zelená
prostranství	prostranství	k1gNnPc2	prostranství
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Esplanáda	esplanáda	k1gFnSc1	esplanáda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
Invalidovny	invalidovna	k1gFnSc2	invalidovna
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Quai	quai	k1gNnSc2	quai
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gMnSc2	Orsaa
u	u	k7c2	u
Seiny	Seina	k1gFnSc2	Seina
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yIgFnSc4	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
most	most	k1gInSc1	most
Alexandra	Alexandr	k1gMnSc2	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Prostranství	prostranství	k1gNnSc1	prostranství
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Prés-Saint-Germain	Prés-Saint-Germain	k2eAgMnSc1d1	Prés-Saint-Germain
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
architekta	architekt	k1gMnSc2	architekt
Roberta	Robert	k1gMnSc2	Robert
de	de	k?	de
Cotte	Cott	k1gInSc5	Cott
(	(	kIx(	(
<g/>
1656	[number]	k4	1656
<g/>
-	-	kIx~	-
<g/>
1735	[number]	k4	1735
<g/>
)	)	kIx)	)
na	na	k7c4	na
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
obdélníkové	obdélníkový	k2eAgNnSc4d1	obdélníkové
náměstí	náměstí	k1gNnSc4	náměstí
osázené	osázený	k2eAgNnSc4d1	osázené
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
lemované	lemovaný	k2eAgInPc1d1	lemovaný
několika	několik	k4yIc7	několik
řadami	řada	k1gFnPc7	řada
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
promenáda	promenáda	k1gFnSc1	promenáda
táhla	táhnout	k5eAaImAgFnS	táhnout
od	od	k7c2	od
Invalidovny	invalidovna	k1gFnSc2	invalidovna
k	k	k7c3	k
ulici	ulice	k1gFnSc3	ulice
Rue	Rue	k1gFnSc3	Rue
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Université	Universitý	k2eAgFnPc1d1	Universitá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1720	[number]	k4	1720
byla	být	k5eAaImAgFnS	být
esplanáda	esplanáda	k1gFnSc1	esplanáda
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
severně	severně	k6eAd1	severně
až	až	k6eAd1	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
na	na	k7c6	na
Quai	quai	k1gNnSc6	quai
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gMnSc2	Orsaa
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
esplanády	esplanáda	k1gFnSc2	esplanáda
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Rue	Rue	k1gFnSc1	Rue
Saint-Dominique	Saint-Dominique	k1gFnSc1	Saint-Dominique
a	a	k8xC	a
Avenue	avenue	k1gFnSc1	avenue
du	du	k?	du
Maréchal-Gallieni	Maréchal-Gallien	k2eAgMnPc1d1	Maréchal-Gallien
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1804-1840	[number]	k4	1804-1840
nacházela	nacházet	k5eAaImAgFnS	nacházet
fontána	fontána	k1gFnSc1	fontána
Invalidovny	invalidovna	k1gFnSc2	invalidovna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1840	[number]	k4	1840
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehrál	odehrát	k5eAaPmAgInS	odehrát
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
převezeny	převézt	k5eAaPmNgInP	převézt
ostatky	ostatek	k1gInPc1	ostatek
Napoleona	Napoleon	k1gMnSc2	Napoleon
I.	I.	kA	I.
Pozemky	pozemka	k1gFnPc1	pozemka
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
byly	být	k5eAaImAgFnP	být
převedeny	převést	k5eAaPmNgFnP	převést
městu	město	k1gNnSc3	město
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
esplanádě	esplanáda	k1gFnSc6	esplanáda
umístěny	umístěn	k2eAgInPc4d1	umístěn
výstavní	výstavní	k2eAgInPc4d1	výstavní
pavilony	pavilon	k1gInPc4	pavilon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
postaveno	postavit	k5eAaPmNgNnS	postavit
nádraží	nádraží	k1gNnSc1	nádraží
Invalidovny	invalidovna	k1gFnSc2	invalidovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
využívá	využívat	k5eAaImIp3nS	využívat
linka	linka	k1gFnSc1	linka
RER	RER	kA	RER
C.	C.	kA	C.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Invalides	Invalidesa	k1gFnPc2	Invalidesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Esplanade	Esplanad	k1gInSc5	Esplanad
des	des	k1gNnSc3	des
Invalides	Invalides	k1gInSc4	Invalides
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Park	park	k1gInSc1	park
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Ulice	ulice	k1gFnSc1	ulice
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
</s>
