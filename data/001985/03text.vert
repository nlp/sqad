<s>
Cache	Cache	k1gFnSc1	Cache
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
kæ	kæ	k?	kæ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
počeštěle	počeštěle	k6eAd1	počeštěle
[	[	kIx(	[
<g/>
keš	keš	k?	keš
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
též	též	k9	též
mezipaměť	mezipamětit	k5eAaPmRp2nS	mezipamětit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
nebo	nebo	k8xC	nebo
softwarovou	softwarový	k2eAgFnSc4d1	softwarová
součást	součást	k1gFnSc4	součást
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
data	datum	k1gNnPc4	datum
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
následující	následující	k2eAgInSc1d1	následující
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
datům	datum	k1gNnPc3	datum
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
bufferu	buffer	k1gInSc2	buffer
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
cache	cache	k6eAd1	cache
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
data	datum	k1gNnPc1	datum
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uložená	uložený	k2eAgFnSc1d1	uložená
opakovaně	opakovaně	k6eAd1	opakovaně
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
vyrovnávací	vyrovnávací	k2eAgFnSc7d1	vyrovnávací
pamětí	paměť	k1gFnSc7	paměť
data	datum	k1gNnSc2	datum
pouze	pouze	k6eAd1	pouze
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
termínu	termín	k1gInSc2	termín
cache	cach	k1gFnSc2	cach
a	a	k8xC	a
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zaměňováno	zaměňován	k2eAgNnSc1d1	zaměňováno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obě	dva	k4xCgFnPc1	dva
funkce	funkce	k1gFnPc1	funkce
mohou	moct	k5eAaImIp3nP	moct
splývat	splývat	k5eAaImF	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k1gFnSc1	Cache
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
(	(	kIx(	(
<g/>
relativně	relativně	k6eAd1	relativně
<g/>
)	)	kIx)	)
rychlou	rychlý	k2eAgFnSc4d1	rychlá
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
cache	cache	k6eAd1	cache
menší	malý	k2eAgFnSc1d2	menší
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
než	než	k8xS	než
úložný	úložný	k2eAgInSc4d1	úložný
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Optimalizací	optimalizace	k1gFnSc7	optimalizace
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
využití	využití	k1gNnSc2	využití
cache	cachat	k5eAaPmIp3nS	cachat
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vyššího	vysoký	k2eAgInSc2d2	vyšší
výkonu	výkon	k1gInSc2	výkon
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
počítače	počítač	k1gInSc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
cache	cache	k1gFnSc4	cache
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
mikroprocesorech	mikroprocesor	k1gInPc6	mikroprocesor
nebo	nebo	k8xC	nebo
pevných	pevný	k2eAgInPc6d1	pevný
discích	disk	k1gInPc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k1gFnSc1	Cache
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
softwarově	softwarově	k6eAd1	softwarově
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
cache	cach	k1gInSc2	cach
je	být	k5eAaImIp3nS	být
urychlit	urychlit	k5eAaPmF	urychlit
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
používaným	používaný	k2eAgFnPc3d1	používaná
<g/>
)	)	kIx)	)
datům	datum	k1gNnPc3	datum
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
uložena	uložen	k2eAgNnPc1d1	uloženo
na	na	k7c6	na
"	"	kIx"	"
<g/>
pomalých	pomalý	k2eAgNnPc6d1	pomalé
<g/>
"	"	kIx"	"
úložištích	úložiště	k1gNnPc6	úložiště
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
dat	datum	k1gNnPc2	datum
umístěna	umístit	k5eAaPmNgFnS	umístit
do	do	k7c2	do
rychlejšího	rychlý	k2eAgNnSc2d2	rychlejší
úložiště	úložiště	k1gNnSc2	úložiště
(	(	kIx(	(
<g/>
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
současných	současný	k2eAgInPc6d1	současný
mikroprocesorech	mikroprocesor	k1gInPc6	mikroprocesor
funguje	fungovat	k5eAaImIp3nS	fungovat
cache	cache	k6eAd1	cache
automaticky	automaticky	k6eAd1	automaticky
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hardwarová	hardwarový	k2eAgFnSc1d1	hardwarová
cache	cache	k1gFnSc1	cache
<g/>
)	)	kIx)	)
a	a	k8xC	a
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
přístupy	přístup	k1gInPc4	přístup
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
cache	cache	k1gFnSc1	cache
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
jádrem	jádro	k1gNnSc7	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
softwarová	softwarový	k2eAgFnSc1d1	softwarová
cache	cache	k1gFnSc1	cache
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
RAM	RAM	kA	RAM
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
používaných	používaný	k2eAgInPc2d1	používaný
<g/>
)	)	kIx)	)
dat	datum	k1gNnPc2	datum
umístěných	umístěný	k2eAgMnPc2d1	umístěný
na	na	k7c6	na
pevném	pevný	k2eAgInSc6d1	pevný
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
práci	práce	k1gFnSc3	práce
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k1gInSc1	Cache
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dříve	dříve	k6eAd2	dříve
navštívené	navštívený	k2eAgFnPc4d1	navštívená
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
obrázky	obrázek	k1gInPc4	obrázek
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zpět	zpět	k6eAd1	zpět
nebo	nebo	k8xC	nebo
následující	následující	k2eAgFnSc1d1	následující
stránka	stránka	k1gFnSc1	stránka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stejné	stejný	k2eAgInPc4d1	stejný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
se	se	k3xPyFc4	se
stahovat	stahovat	k5eAaImF	stahovat
přes	přes	k7c4	přes
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
poskytnuty	poskytnout	k5eAaPmNgInP	poskytnout
z	z	k7c2	z
cache	cach	k1gInSc2	cach
(	(	kIx(	(
<g/>
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
nebo	nebo	k8xC	nebo
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k1gFnSc1	Cache
byla	být	k5eAaImAgFnS	být
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
požadovaná	požadovaný	k2eAgNnPc4d1	požadované
data	datum	k1gNnPc4	datum
v	v	k7c6	v
cache	cache	k1gFnSc6	cache
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
cache	cache	k1gNnSc4	cache
hit	hit	k1gInSc1	hit
(	(	kIx(	(
<g/>
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
poskytnuta	poskytnout	k5eAaPmNgNnP	poskytnout
z	z	k7c2	z
cache	cach	k1gFnSc2	cach
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
data	datum	k1gNnPc1	datum
v	v	k7c6	v
cache	cache	k1gFnSc6	cache
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
cache	cache	k1gFnSc4	cache
miss	miss	k1gFnSc4	miss
(	(	kIx(	(
<g/>
data	datum	k1gNnPc1	datum
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
získána	získat	k5eAaPmNgFnS	získat
z	z	k7c2	z
pomalejšího	pomalý	k2eAgNnSc2d2	pomalejší
<g />
.	.	kIx.	.
</s>
<s>
úložiště	úložiště	k1gNnSc1	úložiště
-	-	kIx~	-
operační	operační	k2eAgFnSc1d1	operační
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
/	/	kIx~	/
<g/>
O	o	k7c4	o
zařízení	zařízení	k1gNnSc4	zařízení
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
softwarová	softwarový	k2eAgFnSc1d1	softwarová
cache	cache	k1gFnSc1	cache
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
programově	programově	k6eAd1	programově
<g/>
,	,	kIx,	,
vymezením	vymezení	k1gNnSc7	vymezení
určité	určitý	k2eAgFnSc2d1	určitá
části	část	k1gFnSc2	část
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
disková	diskový	k2eAgFnSc1d1	disková
cache	cache	k1gFnSc1	cache
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
)	)	kIx)	)
hardwarová	hardwarový	k2eAgFnSc1d1	hardwarová
cache	cache	k1gFnSc1	cache
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
paměťovými	paměťový	k2eAgInPc7d1	paměťový
obvody	obvod	k1gInPc7	obvod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
procesoru	procesor	k1gInSc2	procesor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
pro	pro	k7c4	pro
pomalé	pomalý	k2eAgMnPc4d1	pomalý
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
procesoru	procesor	k1gInSc2	procesor
<g/>
)	)	kIx)	)
vnější	vnější	k2eAgFnSc2d1	vnější
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
takovým	takový	k3xDgNnSc7	takový
zařízením	zařízení	k1gNnSc7	zařízení
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
disk	disk	k1gInSc1	disk
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
uchovávat	uchovávat	k5eAaImF	uchovávat
v	v	k7c6	v
rychlé	rychlý	k2eAgFnSc6d1	rychlá
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zápisu	zápis	k1gInSc2	zápis
tyto	tento	k3xDgFnPc4	tento
na	na	k7c4	na
disk	disk	k1gInSc4	disk
ukládat	ukládat	k5eAaImF	ukládat
v	v	k7c6	v
co	co	k9	co
nejvýhodnějším	výhodný	k2eAgNnSc6d3	nejvýhodnější
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Nemusí	muset	k5eNaImIp3nP	muset
tak	tak	k6eAd1	tak
provádět	provádět	k5eAaImF	provádět
zbytečné	zbytečný	k2eAgFnPc4d1	zbytečná
čtecí	čtecí	k2eAgFnPc4d1	čtecí
a	a	k8xC	a
zápisové	zápisový	k2eAgFnPc4d1	zápisová
operace	operace	k1gFnPc4	operace
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přemístit	přemístit	k5eAaPmF	přemístit
hlavy	hlava	k1gFnPc4	hlava
a	a	k8xC	a
počkat	počkat	k5eAaPmF	počkat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
disk	disk	k1gInSc1	disk
natočí	natočit	k5eAaBmIp3nS	natočit
požadovaným	požadovaný	k2eAgNnSc7d1	požadované
místem	místo	k1gNnSc7	místo
pod	pod	k7c4	pod
hlavy	hlava	k1gFnPc4	hlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
systémy	systém	k1gInPc1	systém
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
cache	cache	k6eAd1	cache
paměť	paměť	k1gFnSc4	paměť
pro	pro	k7c4	pro
disky	disk	k1gInPc4	disk
dynamicky	dynamicky	k6eAd1	dynamicky
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
volné	volný	k2eAgFnSc2d1	volná
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
potřeb	potřeba	k1gFnPc2	potřeba
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmy	algoritmus	k1gInPc1	algoritmus
pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
diskové	diskový	k2eAgFnSc2d1	disková
cache	cach	k1gFnSc2	cach
mají	mít	k5eAaImIp3nP	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
výkon	výkon	k1gInSc4	výkon
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
používání	používání	k1gNnSc2	používání
cache	cach	k1gInSc2	cach
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc4	riziko
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
neočekávaným	očekávaný	k2eNgInSc7d1	neočekávaný
výpadkem	výpadek	k1gInSc7	výpadek
napájení	napájení	k1gNnSc2	napájení
-	-	kIx~	-
stav	stav	k1gInSc1	stav
datových	datový	k2eAgInPc2d1	datový
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
není	být	k5eNaImIp3nS	být
vždy	vždy	k6eAd1	vždy
aktuální	aktuální	k2eAgFnSc1d1	aktuální
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
cache	cach	k1gInSc2	cach
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
před	před	k7c7	před
vypnutím	vypnutí	k1gNnSc7	vypnutí
počítače	počítač	k1gInSc2	počítač
provést	provést	k5eAaPmF	provést
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
shutdown	shutdown	k1gMnSc1	shutdown
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
korektní	korektní	k2eAgNnSc1d1	korektní
ukončení	ukončení	k1gNnSc1	ukončení
práce	práce	k1gFnSc2	práce
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
)	)	kIx)	)
uloží	uložit	k5eAaPmIp3nS	uložit
obsah	obsah	k1gInSc4	obsah
diskové	diskový	k2eAgFnSc2d1	disková
cache	cach	k1gFnSc2	cach
do	do	k7c2	do
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
některá	některý	k3yIgNnPc4	některý
vyměnitelná	vyměnitelný	k2eAgNnPc4d1	vyměnitelné
média	médium	k1gNnPc4	médium
v	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
před	před	k7c7	před
vyjmutím	vyjmutí	k1gNnSc7	vyjmutí
"	"	kIx"	"
<g/>
odmountovat	odmountovat	k5eAaPmF	odmountovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
médiem	médium	k1gNnSc7	médium
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
dále	daleko	k6eAd2	daleko
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
systém	systém	k1gInSc1	systém
provede	provést	k5eAaPmIp3nS	provést
synchronizaci	synchronizace	k1gFnSc4	synchronizace
cache	cache	k1gFnPc2	cache
se	s	k7c7	s
soubory	soubor	k1gInPc7	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Odpojení	odpojení	k1gNnSc1	odpojení
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
i	i	k9	i
například	například	k6eAd1	například
u	u	k7c2	u
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
zelenou	zelený	k2eAgFnSc7d1	zelená
ikonkou	ikonka	k1gFnSc7	ikonka
vpravo	vpravo	k6eAd1	vpravo
dole	dole	k6eAd1	dole
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
na	na	k7c6	na
výměnném	výměnný	k2eAgNnSc6d1	výměnné
médiu	médium	k1gNnSc6	médium
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
s	s	k7c7	s
médiem	médium	k1gNnSc7	médium
zrovna	zrovna	k6eAd1	zrovna
pracuje	pracovat	k5eAaImIp3nS	pracovat
(	(	kIx(	(
<g/>
kopírování	kopírování	k1gNnSc1	kopírování
<g/>
,	,	kIx,	,
editace	editace	k1gFnSc1	editace
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgInPc1d1	případný
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
nečekaným	čekaný	k2eNgInSc7d1	nečekaný
výpadkem	výpadek	k1gInSc7	výpadek
napájení	napájení	k1gNnSc2	napájení
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
moderní	moderní	k2eAgInPc1d1	moderní
systémy	systém	k1gInPc1	systém
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
eliminovat	eliminovat	k5eAaBmF	eliminovat
pomocí	pomocí	k7c2	pomocí
žurnálu	žurnál	k1gInSc2	žurnál
<g/>
.	.	kIx.	.
</s>
<s>
Paměť	paměť	k1gFnSc1	paměť
cache	cachat	k5eAaPmIp3nS	cachat
realizovaná	realizovaný	k2eAgFnSc1d1	realizovaná
specializovanými	specializovaný	k2eAgInPc7d1	specializovaný
paměťovými	paměťový	k2eAgInPc7d1	paměťový
obvody	obvod	k1gInPc7	obvod
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
řídících	řídící	k2eAgFnPc6d1	řídící
jednotkách	jednotka	k1gFnPc6	jednotka
vnějších	vnější	k2eAgFnPc2d1	vnější
pamětí	paměť	k1gFnPc2	paměť
a	a	k8xC	a
v	v	k7c6	v
procesorech	procesor	k1gInPc6	procesor
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
podpůrných	podpůrný	k2eAgInPc6d1	podpůrný
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k6eAd1	Cache
v	v	k7c6	v
řídících	řídící	k2eAgFnPc6d1	řídící
jednotkách	jednotka	k1gFnPc6	jednotka
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nepravidelným	pravidelný	k2eNgNnSc7d1	nepravidelné
předáváním	předávání	k1gNnSc7	předávání
<g/>
/	/	kIx~	/
<g/>
přebíráním	přebírání	k1gNnSc7	přebírání
dat	datum	k1gNnPc2	datum
počítačem	počítač	k1gInSc7	počítač
(	(	kIx(	(
<g/>
sběrnici	sběrnice	k1gFnSc4	sběrnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
tokem	tok	k1gInSc7	tok
dat	datum	k1gNnPc2	datum
do	do	k7c2	do
<g/>
/	/	kIx~	/
<g/>
z	z	k7c2	z
magnetických	magnetický	k2eAgFnPc2d1	magnetická
hlav	hlava	k1gFnPc2	hlava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rytmus	rytmus	k1gInSc1	rytmus
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
rychlostí	rychlost	k1gFnSc7	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
počítačů	počítač	k1gMnPc2	počítač
je	být	k5eAaImIp3nS	být
cache	cache	k6eAd1	cache
elektronický	elektronický	k2eAgInSc1d1	elektronický
obvod	obvod	k1gInSc1	obvod
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
z	z	k7c2	z
tranzistorů	tranzistor	k1gInPc2	tranzistor
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
tvoří	tvořit	k5eAaImIp3nP	tvořit
bistabilní	bistabilní	k2eAgInPc1d1	bistabilní
klopné	klopný	k2eAgInPc1d1	klopný
obvody	obvod	k1gInPc1	obvod
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
rychlost	rychlost	k1gFnSc4	rychlost
mezi	mezi	k7c7	mezi
procesorem	procesor	k1gInSc7	procesor
a	a	k8xC	a
operační	operační	k2eAgFnSc7d1	operační
pamětí	paměť	k1gFnSc7	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
rychlosti	rychlost	k1gFnSc2	rychlost
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
použitím	použití	k1gNnSc7	použití
kvalitnějších	kvalitní	k2eAgInPc2d2	kvalitnější
tranzistorů	tranzistor	k1gInPc2	tranzistor
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgFnSc1d2	vyšší
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
)	)	kIx)	)
než	než	k8xS	než
u	u	k7c2	u
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
cache	cach	k1gFnSc2	cach
je	být	k5eAaImIp3nS	být
také	také	k9	také
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
procesoru	procesor	k1gInSc2	procesor
než	než	k8xS	než
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
opět	opět	k6eAd1	opět
vyšší	vysoký	k2eAgFnSc4d2	vyšší
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnPc1d1	velká
parazitní	parazitní	k2eAgFnPc1d1	parazitní
kapacity	kapacita	k1gFnPc1	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cache	k6eAd1	Cache
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
disku	disk	k1gInSc6	disk
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc4d1	rychlý
procesor	procesor	k1gInSc4	procesor
od	od	k7c2	od
řádově	řádově	k6eAd1	řádově
pomalejšího	pomalý	k2eAgInSc2d2	pomalejší
pevného	pevný	k2eAgInSc2d1	pevný
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uchovat	uchovat	k5eAaPmF	uchovat
data	datum	k1gNnPc4	datum
určená	určený	k2eAgNnPc4d1	určené
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
mechanická	mechanický	k2eAgFnSc1d1	mechanická
část	část	k1gFnSc1	část
pevného	pevný	k2eAgInSc2d1	pevný
disku	disk	k1gInSc2	disk
zpracuje	zpracovat	k5eAaPmIp3nS	zpracovat
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
přečtená	přečtený	k2eAgNnPc4d1	přečtené
data	datum	k1gNnPc4	datum
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obsluha	obsluha	k1gFnSc1	obsluha
přerušení	přerušení	k1gNnSc2	přerušení
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
k	k	k7c3	k
interní	interní	k2eAgFnSc3d1	interní
reorganizaci	reorganizace	k1gFnSc3	reorganizace
požadavků	požadavek	k1gInPc2	požadavek
kvůli	kvůli	k7c3	kvůli
zvýšení	zvýšení	k1gNnSc3	zvýšení
výkonu	výkon	k1gInSc2	výkon
(	(	kIx(	(
<g/>
NCQ	NCQ	kA	NCQ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
několika	několik	k4yIc2	několik
kB	kb	kA	kb
do	do	k7c2	do
64	[number]	k4	64
MiB	MiB	k1gFnPc2	MiB
(	(	kIx(	(
<g/>
u	u	k7c2	u
současných	současný	k2eAgNnPc2d1	současné
3.5	[number]	k4	3.5
<g/>
"	"	kIx"	"
disků	disk	k1gInPc2	disk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Interní	interní	k2eAgFnPc1d1	interní
cache	cach	k1gFnPc1	cach
používají	používat	k5eAaImIp3nP	používat
také	také	k6eAd1	také
SSD	SSD	kA	SSD
disky	disk	k1gInPc1	disk
<g/>
.	.	kIx.	.
</s>
<s>
Cache	Cachat	k5eAaPmIp3nS	Cachat
paměť	paměť	k1gFnSc4	paměť
v	v	k7c6	v
procesoru	procesor	k1gInSc6	procesor
ukládá	ukládat	k5eAaImIp3nS	ukládat
kopie	kopie	k1gFnPc4	kopie
dat	datum	k1gNnPc2	datum
přečtených	přečtený	k2eAgFnPc2d1	přečtená
z	z	k7c2	z
adresy	adresa	k1gFnSc2	adresa
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
obsahu	obsah	k1gInSc2	obsah
slova	slovo	k1gNnSc2	slovo
z	z	k7c2	z
adresy	adresa	k1gFnSc2	adresa
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
položka	položka	k1gFnSc1	položka
nalezena	naleznout	k5eAaPmNgFnS	naleznout
v	v	k7c6	v
cache	cache	k1gFnSc6	cache
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
přečten	přečíst	k5eAaPmNgInS	přečíst
z	z	k7c2	z
cache	cach	k1gFnSc2	cach
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
z	z	k7c2	z
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
cache	cache	k1gInSc1	cache
hit	hit	k1gInSc1	hit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
procesorem	procesor	k1gInSc7	procesor
a	a	k8xC	a
cache	cache	k1gNnSc7	cache
pamětí	paměť	k1gFnSc7	paměť
se	se	k3xPyFc4	se
přenášejí	přenášet	k5eAaImIp3nP	přenášet
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
cache	cache	k1gFnSc7	cache
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
operační	operační	k2eAgFnSc7d1	operační
pamětí	paměť	k1gFnSc7	paměť
se	se	k3xPyFc4	se
přenášejí	přenášet	k5eAaImIp3nP	přenášet
rámce	rámec	k1gInSc2	rámec
slov	slovo	k1gNnPc2	slovo
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několikanásobku	několikanásobek	k1gInSc2	několikanásobek
velikosti	velikost	k1gFnSc2	velikost
slova	slovo	k1gNnSc2	slovo
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
asi	asi	k9	asi
90	[number]	k4	90
<g/>
%	%	kIx~	%
operací	operace	k1gFnPc2	operace
procesoru	procesor	k1gInSc2	procesor
je	být	k5eAaImIp3nS	být
čtení	čtení	k1gNnSc4	čtení
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
sekvenční	sekvenční	k2eAgFnSc1d1	sekvenční
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
větší	veliký	k2eAgInPc1d2	veliký
propustnosti	propustnost	k1gFnPc4	propustnost
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
do	do	k7c2	do
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vyššího	vysoký	k2eAgInSc2d2	vyšší
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnávací	vyrovnávací	k2eAgFnSc1d1	vyrovnávací
paměť	paměť	k1gFnSc1	paměť
procesoru	procesor	k1gInSc2	procesor
bývá	bývat	k5eAaImIp3nS	bývat
dvojstupňová	dvojstupňový	k2eAgFnSc1d1	dvojstupňová
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
paměti	paměť	k1gFnSc2	paměť
o	o	k7c6	o
malé	malý	k2eAgFnSc6d1	malá
kapacitě	kapacita	k1gFnSc6	kapacita
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
součástí	součást	k1gFnSc7	součást
procesoru	procesor	k1gInSc2	procesor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vlastní	vlastní	k2eAgInSc1d1	vlastní
procesor	procesor	k1gInSc1	procesor
(	(	kIx(	(
<g/>
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
L	L	kA	L
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
procesorem	procesor	k1gInSc7	procesor
a	a	k8xC	a
operační	operační	k2eAgFnSc7d1	operační
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
do	do	k7c2	do
pouzdra	pouzdro	k1gNnSc2	pouzdro
s	s	k7c7	s
procesorem	procesor	k1gInSc7	procesor
(	(	kIx(	(
<g/>
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
L	L	kA	L
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
cena	cena	k1gFnSc1	cena
pamětí	paměť	k1gFnPc2	paměť
stoupá	stoupat	k5eAaImIp3nS	stoupat
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
rychlostí	rychlost	k1gFnSc7	rychlost
(	(	kIx(	(
<g/>
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tímto	tento	k3xDgNnSc7	tento
uspořádáním	uspořádání	k1gNnSc7	uspořádání
najít	najít	k5eAaPmF	najít
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
cenou	cena	k1gFnSc7	cena
a	a	k8xC	a
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
L3	L3	k1gFnSc4	L3
cache	cach	k1gFnSc2	cach
i	i	k9	i
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
procesorech	procesor	k1gInPc6	procesor
(	(	kIx(	(
<g/>
Intel	Intel	kA	Intel
Core	Core	k1gFnSc1	Core
i	i	k9	i
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
AMD	AMD	kA	AMD
Phenom	Phenom	k1gInSc1	Phenom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
jádra	jádro	k1gNnPc4	jádro
společná	společný	k2eAgNnPc4d1	společné
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
několik	několik	k4yIc4	několik
megabajtů	megabajt	k1gInPc2	megabajt
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
paměti	paměť	k1gFnSc2	paměť
cache	cache	k1gFnSc1	cache
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
algoritmus	algoritmus	k1gInSc1	algoritmus
řízení	řízení	k1gNnSc2	řízení
paměti	paměť	k1gFnSc2	paměť
cache	cach	k1gInSc2	cach
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
typů	typ	k1gInPc2	typ
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podstatně	podstatně	k6eAd1	podstatně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Webová	webový	k2eAgFnSc1d1	webová
cache	cache	k1gFnSc1	cache
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
předchozích	předchozí	k2eAgFnPc2d1	předchozí
odpovědí	odpověď	k1gFnPc2	odpověď
od	od	k7c2	od
webových	webový	k2eAgInPc2d1	webový
serverů	server	k1gInPc2	server
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
nebo	nebo	k8xC	nebo
obrázky	obrázek	k1gInPc4	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
uložené	uložený	k2eAgFnPc1d1	uložená
v	v	k7c6	v
cache	cache	k1gFnSc6	cache
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
využity	využit	k2eAgFnPc4d1	využita
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
snížit	snížit	k5eAaPmF	snížit
vytížení	vytížení	k1gNnSc4	vytížení
přenosových	přenosový	k2eAgFnPc2d1	přenosová
cest	cesta	k1gFnPc2	cesta
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
též	též	k9	též
zátěž	zátěž	k1gFnSc1	zátěž
webových	webový	k2eAgInPc2d1	webový
serverů	server	k1gInPc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Webová	webový	k2eAgFnSc1d1	webová
cache	cache	k1gFnSc1	cache
je	být	k5eAaImIp3nS	být
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
součástí	součást	k1gFnSc7	součást
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
<g/>
.	.	kIx.	.
</s>
<s>
Správci	správce	k1gMnPc1	správce
větších	veliký	k2eAgFnPc2d2	veliký
počítačových	počítačový	k2eAgFnPc2d1	počítačová
sítí	síť	k1gFnPc2	síť
nebo	nebo	k8xC	nebo
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
používají	používat	k5eAaImIp3nP	používat
proxy	prox	k1gInPc1	prox
servery	server	k1gInPc4	server
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
cachují	cachovat	k5eAaImIp3nP	cachovat
data	datum	k1gNnPc4	datum
pocházející	pocházející	k2eAgFnSc2d1	pocházející
od	od	k7c2	od
webových	webový	k2eAgInPc2d1	webový
serverů	server	k1gInPc2	server
k	k	k7c3	k
webovým	webový	k2eAgMnPc3d1	webový
klientům	klient	k1gMnPc3	klient
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
velmi	velmi	k6eAd1	velmi
zatížené	zatížený	k2eAgInPc1d1	zatížený
webové	webový	k2eAgInPc1d1	webový
servery	server	k1gInPc1	server
používají	používat	k5eAaImIp3nP	používat
reverzní	reverzní	k2eAgInPc1d1	reverzní
proxy	prox	k1gInPc1	prox
cache	cach	k1gFnSc2	cach
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
záměrně	záměrně	k6eAd1	záměrně
postavena	postaven	k2eAgFnSc1d1	postavena
přímo	přímo	k6eAd1	přímo
před	před	k7c4	před
webový	webový	k2eAgInSc4d1	webový
server	server	k1gInSc4	server
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
před	před	k7c7	před
servery	server	k1gInPc7	server
Wikipedie	Wikipedie	k1gFnSc2	Wikipedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Webový	webový	k2eAgInSc1d1	webový
server	server	k1gInSc1	server
tak	tak	k6eAd1	tak
nemusí	muset	k5eNaImIp3nS	muset
opakovaně	opakovaně	k6eAd1	opakovaně
odesílat	odesílat	k5eAaImF	odesílat
klientům	klient	k1gMnPc3	klient
data	datum	k1gNnSc2	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
odeslal	odeslat	k5eAaPmAgMnS	odeslat
před	před	k7c7	před
kratší	krátký	k2eAgFnSc7d2	kratší
dobou	doba	k1gFnSc7	doba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
snížena	snížit	k5eAaPmNgFnS	snížit
jeho	jeho	k3xOp3gFnSc1	jeho
zátěž	zátěž	k1gFnSc1	zátěž
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
podobou	podoba	k1gFnSc7	podoba
cache	cache	k6eAd1	cache
je	být	k5eAaImIp3nS	být
P2P	P2P	k1gFnSc4	P2P
ukládání	ukládání	k1gNnSc3	ukládání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
soubory	soubor	k1gInPc1	soubor
nejvíce	hodně	k6eAd3	hodně
vyhledávané	vyhledávaný	k2eAgInPc1d1	vyhledávaný
peer-to-peer	peeroeer	k1gInSc1	peer-to-peer
aplikacemi	aplikace	k1gFnPc7	aplikace
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
ISP	ISP	kA	ISP
cache	cachat	k5eAaPmIp3nS	cachat
pro	pro	k7c4	pro
zrychlení	zrychlení	k1gNnSc4	zrychlení
P2P	P2P	k1gFnSc2	P2P
přenosů	přenos	k1gInPc2	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
decentralizované	decentralizovaný	k2eAgInPc1d1	decentralizovaný
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
komunitám	komunita	k1gFnPc3	komunita
provádět	provádět	k5eAaImF	provádět
stejné	stejný	k2eAgFnPc4d1	stejná
úlohy	úloha	k1gFnPc4	úloha
pro	pro	k7c4	pro
P2P	P2P	k1gFnPc4	P2P
přenos	přenos	k1gInSc4	přenos
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Corelli	Corell	k1gMnPc1	Corell
<g/>
.	.	kIx.	.
</s>
<s>
Doménový	doménový	k2eAgInSc1d1	doménový
DNS	DNS	kA	DNS
server	server	k1gInSc1	server
BIND	BIND	kA	BIND
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
cache	cach	k1gFnSc2	cach
mapování	mapování	k1gNnSc2	mapování
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
na	na	k7c4	na
IP	IP	kA	IP
adresy	adresa	k1gFnPc4	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
cache	cache	k1gFnSc6	cache
určuje	určovat	k5eAaImIp3nS	určovat
správce	správce	k1gMnSc1	správce
daného	daný	k2eAgInSc2d1	daný
doménového	doménový	k2eAgInSc2d1	doménový
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
cache	cache	k1gFnSc4	cache
může	moct	k5eAaImIp3nS	moct
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
knihovna	knihovna	k1gFnSc1	knihovna
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
překladu	překlad	k1gInSc2	překlad
doménového	doménový	k2eAgNnSc2d1	doménové
jména	jméno	k1gNnSc2	jméno
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
resolver	resolver	k1gInSc1	resolver
s	s	k7c7	s
konfiguračním	konfigurační	k2eAgInSc7d1	konfigurační
souborem	soubor	k1gInSc7	soubor
resolv	resolv	k1gMnSc1	resolv
<g/>
.	.	kIx.	.
<g/>
conf	conf	k1gMnSc1	conf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
síťové	síťový	k2eAgInPc1d1	síťový
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
urychlují	urychlovat	k5eAaImIp3nP	urychlovat
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
vzdáleným	vzdálený	k2eAgFnPc3d1	vzdálená
souborů	soubor	k1gInPc2	soubor
pomocí	pomoc	k1gFnSc7	pomoc
lokální	lokální	k2eAgInPc1d1	lokální
cache	cach	k1gInPc1	cach
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
celé	celý	k2eAgInPc4d1	celý
soubory	soubor	k1gInPc4	soubor
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
)	)	kIx)	)
ukládány	ukládán	k2eAgFnPc4d1	ukládána
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
klienta	klient	k1gMnSc2	klient
do	do	k7c2	do
cache	cach	k1gFnSc2	cach
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
cache	cache	k1gFnSc1	cache
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
síťové	síťový	k2eAgInPc1d1	síťový
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
musí	muset	k5eAaImIp3nP	muset
řešit	řešit	k5eAaImF	řešit
aktualizace	aktualizace	k1gFnPc4	aktualizace
záznamů	záznam	k1gInPc2	záznam
v	v	k7c6	v
cache	cache	k1gFnSc6	cache
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
změněn	změnit	k5eAaPmNgInS	změnit
jiným	jiný	k2eAgMnSc7d1	jiný
klientem	klient	k1gMnSc7	klient
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
činnost	činnost	k1gFnSc4	činnost
takové	takový	k3xDgFnSc2	takový
lokální	lokální	k2eAgFnSc2d1	lokální
cache	cach	k1gFnSc2	cach
omezena	omezit	k5eAaPmNgNnP	omezit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jen	jen	k9	jen
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
pro	pro	k7c4	pro
soubory	soubor	k1gInPc4	soubor
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
nelze	lze	k6eNd1	lze
zapisovat	zapisovat	k5eAaImF	zapisovat
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgInPc1d1	lokální
cache	cach	k1gInPc1	cach
používají	používat	k5eAaImIp3nP	používat
pokročilé	pokročilý	k2eAgInPc1d1	pokročilý
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
AFS	AFS	kA	AFS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
implementovány	implementován	k2eAgInPc1d1	implementován
i	i	k9	i
pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
běžnějších	běžný	k2eAgNnPc2d2	běžnější
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
NFS	NFS	kA	NFS
nebo	nebo	k8xC	nebo
SMB	SMB	kA	SMB
<g/>
.	.	kIx.	.
</s>
<s>
Webové	webový	k2eAgMnPc4d1	webový
vyhledávače	vyhledávač	k1gMnPc4	vyhledávač
často	často	k6eAd1	často
ukládají	ukládat	k5eAaImIp3nP	ukládat
indexované	indexovaný	k2eAgFnPc4d1	indexovaná
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
cache	cach	k1gFnSc2	cach
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Google	Google	k1gFnSc2	Google
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vedle	vedle	k7c2	vedle
každého	každý	k3xTgInSc2	každý
výsledku	výsledek	k1gInSc2	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
Archiv	archiv	k1gInSc4	archiv
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
z	z	k7c2	z
webového	webový	k2eAgInSc2d1	webový
serveru	server	k1gInSc2	server
dočasně	dočasně	k6eAd1	dočasně
nebo	nebo	k8xC	nebo
permanentně	permanentně	k6eAd1	permanentně
nedostupné	dostupný	k2eNgFnPc4d1	nedostupná
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
cache	cach	k1gFnSc2	cach
je	být	k5eAaImIp3nS	být
ukládání	ukládání	k1gNnSc1	ukládání
vypočítaných	vypočítaný	k2eAgInPc2d1	vypočítaný
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
znovu	znovu	k6eAd1	znovu
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
memoizace	memoizace	k1gFnSc1	memoizace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
Ccache	Ccache	k1gInSc1	Ccache
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
který	který	k3yRgInSc1	který
ukládá	ukládat	k5eAaImIp3nS	ukládat
výstup	výstup	k1gInSc4	výstup
kompilace	kompilace	k1gFnSc2	kompilace
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
příští	příští	k2eAgFnSc2d1	příští
kompilace	kompilace	k1gFnSc2	kompilace
<g/>
.	.	kIx.	.
</s>
<s>
Ukládání	ukládání	k1gNnSc1	ukládání
databáze	databáze	k1gFnSc2	databáze
do	do	k7c2	do
cache	cach	k1gFnSc2	cach
může	moct	k5eAaImIp3nS	moct
podstatně	podstatně	k6eAd1	podstatně
zlepšit	zlepšit	k5eAaPmF	zlepšit
výkonnost	výkonnost	k1gFnSc4	výkonnost
databázových	databázový	k2eAgFnPc2d1	databázová
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
indexů	index	k1gInPc2	index
<g/>
,	,	kIx,	,
datových	datový	k2eAgInPc2d1	datový
repositářů	repositář	k1gInPc2	repositář
a	a	k8xC	a
často	často	k6eAd1	často
používaných	používaný	k2eAgInPc2d1	používaný
sub-sad	subad	k1gInSc4	sub-sad
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Distribuované	distribuovaný	k2eAgNnSc1d1	distribuované
ukládání	ukládání	k1gNnSc1	ukládání
používá	používat	k5eAaImIp3nS	používat
cache	cache	k1gFnSc4	cache
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
na	na	k7c4	na
počítače	počítač	k1gInPc4	počítač
propojené	propojený	k2eAgNnSc4d1	propojené
sítí	sítí	k1gNnSc4	sítí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Corelli	Corell	k1gMnPc1	Corell
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
"	"	kIx"	"
<g/>
buffer	buffer	k1gInSc1	buffer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
cache	cache	k1gFnSc1	cache
<g/>
"	"	kIx"	"
označují	označovat	k5eAaImIp3nP	označovat
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
nevylučující	vylučující	k2eNgFnSc1d1	nevylučující
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
vzájemně	vzájemně	k6eAd1	vzájemně
prolínat	prolínat	k5eAaImF	prolínat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
významech	význam	k1gInPc6	význam
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozdíl	rozdíl	k1gInSc1	rozdíl
(	(	kIx(	(
<g/>
princip	princip	k1gInSc1	princip
cache	cach	k1gInSc2	cach
je	být	k5eAaImIp3nS	být
vysvětlen	vysvětlit	k5eAaPmNgInS	vysvětlit
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Buffer	buffer	k1gInSc1	buffer
je	být	k5eAaImIp3nS	být
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyrovnávání	vyrovnávání	k1gNnSc3	vyrovnávání
různých	různý	k2eAgFnPc2d1	různá
rychlostí	rychlost	k1gFnPc2	rychlost
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tiskárny	tiskárna	k1gFnSc2	tiskárna
a	a	k8xC	a
počítače	počítač	k1gInPc4	počítač
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
vzájemně	vzájemně	k6eAd1	vzájemně
komunikující	komunikující	k2eAgInPc4d1	komunikující
procesy	proces	k1gInPc4	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Buffer	buffer	k1gInSc1	buffer
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
realizován	realizovat	k5eAaBmNgInS	realizovat
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
dočasný	dočasný	k2eAgInSc4d1	dočasný
<g/>
)	)	kIx)	)
úsek	úsek	k1gInSc4	úsek
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
realizován	realizovat	k5eAaBmNgInS	realizovat
i	i	k9	i
hardwarově	hardwarově	k6eAd1	hardwarově
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
pevného	pevný	k2eAgInSc2d1	pevný
disku	disk	k1gInSc2	disk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používán	používán	k2eAgMnSc1d1	používán
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
jako	jako	k9	jako
fronta	fronta	k1gFnSc1	fronta
FIFO	FIFO	kA	FIFO
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k9	jako
kruhový	kruhový	k2eAgInSc4d1	kruhový
buffer	buffer	k1gInSc4	buffer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
jako	jako	k9	jako
zásobník	zásobník	k1gInSc1	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
bufferu	buffer	k1gInSc2	buffer
zapsána	zapsán	k2eAgFnSc1d1	zapsána
<g/>
,	,	kIx,	,
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
přečtena	přečten	k2eAgFnSc1d1	přečtena
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nejsou	být	k5eNaImIp3nP	být
dále	daleko	k6eAd2	daleko
uchovávána	uchovávat	k5eAaImNgFnS	uchovávat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vyrovnávací	vyrovnávací	k2eAgFnSc1d1	vyrovnávací
paměť	paměť	k1gFnSc1	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cache	cachat	k5eAaPmIp3nS	cachat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
cache	cach	k1gFnSc2	cach
<g/>
)	)	kIx)	)
Vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
optimalizace	optimalizace	k1gFnSc2	optimalizace
programů	program	k1gInPc2	program
Návod	návod	k1gInSc1	návod
na	na	k7c6	na
aplikaci	aplikace	k1gFnSc6	aplikace
webové	webový	k2eAgFnSc2d1	webová
cache	cach	k1gFnSc2	cach
Architektury	architektura	k1gFnSc2	architektura
vyrovnávacích	vyrovnávací	k2eAgFnPc2d1	vyrovnávací
pamětí	paměť	k1gFnPc2	paměť
</s>
