<p>
<s>
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Hora	hora	k1gFnSc1	hora
Olymp	Olymp	k1gInSc1	Olymp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
známá	známý	k2eAgFnSc1d1	známá
hora	hora	k1gFnSc1	hora
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
oblasti	oblast	k1gFnSc2	oblast
Tharsis	Tharsis	k1gFnSc2	Tharsis
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klasickou	klasický	k2eAgFnSc4d1	klasická
štítovou	štítový	k2eAgFnSc4d1	štítová
sopku	sopka	k1gFnSc4	sopka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
několikanásobnou	několikanásobný	k2eAgFnSc4d1	několikanásobná
kalderu	kaldera	k1gFnSc4	kaldera
<g/>
.	.	kIx.	.
</s>
<s>
Činí	činit	k5eAaImIp3nS	činit
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
27	[number]	k4	27
km	km	kA	km
nad	nad	k7c7	nad
nulovou	nulový	k2eAgFnSc7d1	nulová
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
Marsu	Mars	k1gInSc2	Mars
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
21	[number]	k4	21
km	km	kA	km
nad	nad	k7c7	nad
gravitačním	gravitační	k2eAgInSc7d1	gravitační
povrchem	povrch	k1gInSc7	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
třikrát	třikrát	k6eAd1	třikrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
nejvýše	nejvýše	k6eAd1	nejvýše
položená	položený	k2eAgFnSc1d1	položená
hora	hora	k1gFnSc1	hora
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
Mount	Mounta	k1gFnPc2	Mounta
Everest	Everest	k1gInSc4	Everest
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sopka	sopka	k1gFnSc1	sopka
Země	zem	k1gFnSc2	zem
Mauna	Maun	k1gMnSc2	Maun
Kea	Kea	k1gMnSc2	Kea
(	(	kIx(	(
<g/>
počítáno	počítat	k5eAaImNgNnS	počítat
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
sopky	sopka	k1gFnSc2	sopka
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
základna	základna	k1gFnSc1	základna
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
přibližně	přibližně	k6eAd1	přibližně
území	území	k1gNnSc4	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
byla	být	k5eAaImAgFnS	být
astronomům	astronom	k1gMnPc3	astronom
známa	znám	k2eAgNnPc1d1	známo
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
lety	let	k1gInPc7	let
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
rozdílného	rozdílný	k2eAgMnSc4d1	rozdílný
albeda	albed	k1gMnSc4	albed
(	(	kIx(	(
<g/>
míry	míra	k1gFnSc2	míra
odrazivosti	odrazivost	k1gFnSc2	odrazivost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
albedový	albedový	k2eAgInSc1d1	albedový
útvar	útvar	k1gInSc1	útvar
dostal	dostat	k5eAaPmAgInS	dostat
pojmenování	pojmenování	k1gNnSc4	pojmenování
Nix	Nix	k1gFnSc2	Nix
Olympica	Olympic	k1gInSc2	Olympic
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Olympský	olympský	k2eAgInSc1d1	olympský
sníh	sníh	k1gInSc1	sníh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
pohoří	pohoří	k1gNnSc4	pohoří
či	či	k8xC	či
horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgNnSc1d1	definitivní
potvrzení	potvrzení	k1gNnSc4	potvrzení
přinesl	přinést	k5eAaPmAgInS	přinést
ale	ale	k8xC	ale
až	až	k6eAd1	až
věk	věk	k1gInSc1	věk
kosmických	kosmický	k2eAgFnPc2d1	kosmická
výprav	výprava	k1gFnPc2	výprava
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
většího	veliký	k2eAgInSc2d2	veliký
komplexu	komplex	k1gInSc2	komplex
Tharsis	Tharsis	k1gFnSc2	Tharsis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
vysokých	vysoký	k2eAgFnPc2d1	vysoká
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
řetězec	řetězec	k1gInSc1	řetězec
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
během	během	k7c2	během
sopečných	sopečný	k2eAgFnPc2d1	sopečná
erupcí	erupce	k1gFnPc2	erupce
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gFnPc7	on
Arsia	Arsia	k1gFnSc1	Arsia
Mons	Mons	k1gInSc1	Mons
<g/>
,	,	kIx,	,
Pavonis	Pavonis	k1gInSc1	Pavonis
Mons	Mons	k1gInSc1	Mons
a	a	k8xC	a
Ascraeus	Ascraeus	k1gInSc1	Ascraeus
Mons	Monsa	k1gFnPc2	Monsa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
až	až	k8xS	až
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
směru	směr	k1gInSc6	směr
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
bájné	bájný	k2eAgFnSc2d1	bájná
hory	hora	k1gFnSc2	hora
Olymp	Olymp	k1gInSc4	Olymp
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
měli	mít	k5eAaImAgMnP	mít
sídlit	sídlit	k5eAaImF	sídlit
řečtí	řecký	k2eAgMnPc1d1	řecký
bohové	bůh	k1gMnPc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
pro	pro	k7c4	pro
sopku	sopka	k1gFnSc4	sopka
byl	být	k5eAaImAgMnS	být
Nix	Nix	k1gMnSc1	Nix
Olympica	Olympica	k1gMnSc1	Olympica
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k8xC	jako
Sněhy	sníh	k1gInPc4	sníh
Olympu	Olymp	k1gInSc2	Olymp
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
při	při	k7c6	při
prvotních	prvotní	k2eAgNnPc6d1	prvotní
pozorováních	pozorování	k1gNnPc6	pozorování
zdál	zdát	k5eAaImAgInS	zdát
bělavý	bělavý	k2eAgInSc1d1	bělavý
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
sněhobílá	sněhobílý	k2eAgFnSc1d1	sněhobílá
barva	barva	k1gFnSc1	barva
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
pozorovanými	pozorovaný	k2eAgNnPc7d1	pozorované
mračny	mračno	k1gNnPc7	mračno
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Základna	základna	k1gFnSc1	základna
kužele	kužel	k1gInSc2	kužel
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
624	[number]	k4	624
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
rozloze	rozloha	k1gFnSc3	rozloha
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
průměr	průměr	k1gInSc1	průměr
kaldery	kaldera	k1gFnSc2	kaldera
(	(	kIx(	(
<g/>
jícnu	jícen	k1gInSc2	jícen
sopky	sopka	k1gFnSc2	sopka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
85	[number]	k4	85
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
sopky	sopka	k1gFnSc2	sopka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
27	[number]	k4	27
km	km	kA	km
(	(	kIx(	(
<g/>
88	[number]	k4	88
600	[number]	k4	600
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
25	[number]	k4	25
km	km	kA	km
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
použité	použitý	k2eAgFnSc2d1	použitá
střední	střední	k2eAgFnSc2d1	střední
hodnoty	hodnota	k1gFnSc2	hodnota
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
Olympu	Olymp	k1gInSc2	Olymp
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
porovnávána	porovnávat	k5eAaImNgFnS	porovnávat
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
největší	veliký	k2eAgFnSc2d3	veliký
pozemské	pozemský	k2eAgFnSc2d1	pozemská
sopky	sopka	k1gFnSc2	sopka
Mauna	Mauna	k1gFnSc1	Mauna
Kea	Kea	k1gMnSc1	Kea
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Havajském	havajský	k2eAgNnSc6d1	havajské
souostroví	souostroví	k1gNnSc6	souostroví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
120	[number]	k4	120
km	km	kA	km
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
2,5	[number]	k4	2,5
×	×	k?	×
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
vrcholku	vrcholek	k1gInSc2	vrcholek
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
by	by	kYmCp3nS	by
případný	případný	k2eAgMnSc1d1	případný
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
neměl	mít	k5eNaImAgMnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
"	"	kIx"	"
<g/>
hoře	hora	k1gFnSc6	hora
<g/>
"	"	kIx"	"
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vlivem	vliv	k1gInSc7	vliv
obrovské	obrovský	k2eAgFnSc2d1	obrovská
rozlohy	rozloha	k1gFnSc2	rozloha
základny	základna	k1gFnSc2	základna
spadají	spadat	k5eAaPmIp3nP	spadat
její	její	k3xOp3gInPc4	její
svahy	svah	k1gInPc4	svah
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
2,5	[number]	k4	2,5
<g/>
°	°	k?	°
až	až	k9	až
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
posledních	poslední	k2eAgInPc2d1	poslední
6	[number]	k4	6
km	km	kA	km
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
téměř	téměř	k6eAd1	téměř
kolmou	kolmý	k2eAgFnSc4d1	kolmá
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
atypické	atypický	k2eAgNnSc4d1	atypické
zakončení	zakončení	k1gNnSc4	zakončení
štítové	štítový	k2eAgFnSc2d1	štítová
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
známý	známý	k2eAgInSc1d1	známý
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
neexistuje	existovat	k5eNaImIp3nS	existovat
žádné	žádný	k3yNgNnSc1	žádný
zcela	zcela	k6eAd1	zcela
přesvědčivé	přesvědčivý	k2eAgNnSc1d1	přesvědčivé
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
hypotéza	hypotéza	k1gFnSc1	hypotéza
například	například	k6eAd1	například
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
výskyt	výskyt	k1gInSc1	výskyt
ledovce	ledovec	k1gInSc2	ledovec
okolo	okolo	k7c2	okolo
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
způsobit	způsobit	k5eAaPmF	způsobit
atypické	atypický	k2eAgNnSc4d1	atypické
zakončení	zakončení	k1gNnSc4	zakončení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
mírně	mírně	k6eAd1	mírně
zvlněnou	zvlněný	k2eAgFnSc4d1	zvlněná
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
by	by	kYmCp3nS	by
nastala	nastat	k5eAaPmAgFnS	nastat
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
kaldery	kaldera	k1gFnSc2	kaldera
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
propadlina	propadlina	k1gFnSc1	propadlina
hluboká	hluboký	k2eAgFnSc1d1	hluboká
až	až	k9	až
3	[number]	k4	3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těleso	těleso	k1gNnSc1	těleso
sopky	sopka	k1gFnSc2	sopka
není	být	k5eNaImIp3nS	být
symetrické	symetrický	k2eAgNnSc1d1	symetrické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
protáhlá	protáhlý	k2eAgNnPc4d1	protáhlé
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
je	být	k5eAaImIp3nS	být
zúžená	zúžený	k2eAgFnSc1d1	zúžená
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
netypické	typický	k2eNgNnSc1d1	netypické
pokřivení	pokřivení	k1gNnSc1	pokřivení
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
magmatickými	magmatický	k2eAgInPc7d1	magmatický
rezervoáry	rezervoár	k1gInPc7	rezervoár
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
by	by	kYmCp3nP	by
teoreticky	teoreticky	k6eAd1	teoreticky
mohly	moct	k5eAaImAgFnP	moct
umožňovat	umožňovat	k5eAaImF	umožňovat
přežívání	přežívání	k1gNnPc4	přežívání
či	či	k8xC	či
případný	případný	k2eAgInSc4d1	případný
výskyt	výskyt	k1gInSc4	výskyt
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Olympu	Olymp	k1gInSc2	Olymp
Mons	Monsa	k1gFnPc2	Monsa
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgInPc1	dva
větší	veliký	k2eAgInPc1d2	veliký
krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
byly	být	k5eAaImAgFnP	být
IAU	IAU	kA	IAU
dočasně	dočasně	k6eAd1	dočasně
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
jako	jako	k8xS	jako
kráter	kráter	k1gInSc1	kráter
Karzok	Karzok	k1gInSc1	Karzok
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
15,6	[number]	k4	15,6
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
kráter	kráter	k1gInSc1	kráter
Pangboche	Pangboch	k1gInSc2	Pangboch
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
10,4	[number]	k4	10,4
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kaldera	Kaldero	k1gNnSc2	Kaldero
===	===	k?	===
</s>
</p>
<p>
<s>
Kaldera	Kaldera	k1gFnSc1	Kaldera
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
Olympu	Olymp	k1gInSc2	Olymp
Mons	Monsa	k1gFnPc2	Monsa
je	být	k5eAaImIp3nS	být
několikanásobně	několikanásobně	k6eAd1	několikanásobně
propadlá	propadlý	k2eAgFnSc1d1	propadlá
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
kolapsem	kolaps	k1gInSc7	kolaps
dna	dno	k1gNnSc2	dno
do	do	k7c2	do
vyprázdněného	vyprázdněný	k2eAgInSc2d1	vyprázdněný
magmatického	magmatický	k2eAgInSc2d1	magmatický
rezervoáru	rezervoár	k1gInSc2	rezervoár
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
šířka	šířka	k1gFnSc1	šířka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
okolo	okolo	k7c2	okolo
80	[number]	k4	80
km	km	kA	km
a	a	k8xC	a
hloubka	hloubka	k1gFnSc1	hloubka
až	až	k9	až
3	[number]	k4	3
km	km	kA	km
(	(	kIx(	(
<g/>
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hloubkou	hloubka	k1gFnSc7	hloubka
4	[number]	k4	4
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc6d3	nejstarší
části	část	k1gFnSc6	část
kaldery	kaldera	k1gFnSc2	kaldera
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tektonické	tektonický	k2eAgInPc1d1	tektonický
zlomy	zlom	k1gInPc1	zlom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc1	výsledek
extenze	extenze	k1gFnSc2	extenze
po	po	k7c6	po
kolapsu	kolaps	k1gInSc6	kolaps
dna	dno	k1gNnSc2	dno
do	do	k7c2	do
vyprázdněných	vyprázdněný	k2eAgFnPc2d1	vyprázdněná
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
detailním	detailní	k2eAgNnSc6d1	detailní
mapování	mapování	k1gNnSc6	mapování
kaldery	kaldera	k1gFnSc2	kaldera
bylo	být	k5eAaImAgNnS	být
rozpoznáno	rozpoznat	k5eAaPmNgNnS	rozpoznat
šest	šest	k4xCc4	šest
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kolapsů	kolaps	k1gInPc2	kolaps
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
odehrály	odehrát	k5eAaPmAgInP	odehrát
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
vrcholku	vrcholek	k1gInSc2	vrcholek
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vulkanismus	vulkanismus	k1gInSc1	vulkanismus
==	==	k?	==
</s>
</p>
<p>
<s>
Olympus	Olympus	k1gInSc1	Olympus
Mons	Monsa	k1gFnPc2	Monsa
je	být	k5eAaImIp3nS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
erupcí	erupce	k1gFnSc7	erupce
málo	málo	k6eAd1	málo
viskózní	viskózní	k2eAgNnSc1d1	viskózní
a	a	k8xC	a
tedy	tedy	k9	tedy
vysoce	vysoce	k6eAd1	vysoce
mobilní	mobilní	k2eAgFnSc2d1	mobilní
lávy	láva	k1gFnSc2	láva
unikající	unikající	k2eAgInSc4d1	unikající
z	z	k7c2	z
kaldery	kaldera	k1gFnSc2	kaldera
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
lávy	láva	k1gFnSc2	láva
umožnila	umožnit	k5eAaPmAgFnS	umožnit
roztečení	roztečení	k1gNnSc4	roztečení
materiálu	materiál	k1gInSc2	materiál
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
útvaru	útvar	k1gInSc2	útvar
s	s	k7c7	s
velice	velice	k6eAd1	velice
nízkými	nízký	k2eAgInPc7d1	nízký
svahy	svah	k1gInPc7	svah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
analogie	analogie	k1gFnSc2	analogie
pozemských	pozemský	k2eAgFnPc2d1	pozemská
nízkoviskózních	nízkoviskózní	k2eAgFnPc2d1	nízkoviskózní
láv	láva	k1gFnPc2	láva
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těleso	těleso	k1gNnSc1	těleso
sopky	sopka	k1gFnSc2	sopka
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
hlavně	hlavně	k9	hlavně
bazickými	bazický	k2eAgFnPc7d1	bazická
až	až	k8xS	až
ultrabazickými	ultrabazický	k2eAgFnPc7d1	ultrabazický
lávami	láva	k1gFnPc7	láva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bazalty	bazalt	k1gInPc1	bazalt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
menší	malý	k2eAgNnSc4d2	menší
gravitační	gravitační	k2eAgNnSc4d1	gravitační
působení	působení	k1gNnSc4	působení
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
těleso	těleso	k1gNnSc1	těleso
sopky	sopka	k1gFnSc2	sopka
i	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
enormních	enormní	k2eAgInPc6d1	enormní
rozměrech	rozměr	k1gInPc6	rozměr
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
aktivita	aktivita	k1gFnSc1	aktivita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
snímkovala	snímkovat	k5eAaImAgFnS	snímkovat
evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
starý	starý	k2eAgInSc1d1	starý
lávový	lávový	k2eAgInSc1d1	lávový
proud	proud	k1gInSc1	proud
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Pozdějším	pozdní	k2eAgInSc7d2	pozdější
průzkumem	průzkum	k1gInSc7	průzkum
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c4	na
sčítání	sčítání	k1gNnSc4	sčítání
množství	množství	k1gNnSc2	množství
kráterů	kráter	k1gInPc2	kráter
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
velikosti	velikost	k1gFnSc2	velikost
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgFnSc1d1	západní
strana	strana	k1gFnSc1	strana
sopky	sopka	k1gFnSc2	sopka
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
před	před	k7c7	před
přibližně	přibližně	k6eAd1	přibližně
115	[number]	k4	115
milióny	milión	k4xCgInPc7	milión
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
menší	malý	k2eAgFnPc1d2	menší
oblasti	oblast	k1gFnPc1	oblast
dokonce	dokonce	k9	dokonce
pouze	pouze	k6eAd1	pouze
před	před	k7c7	před
2	[number]	k4	2
milióny	milión	k4xCgInPc7	milión
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
geologickém	geologický	k2eAgInSc6d1	geologický
pohledu	pohled	k1gInSc6	pohled
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velice	velice	k6eAd1	velice
nedávnou	dávný	k2eNgFnSc4d1	nedávná
erupci	erupce	k1gFnSc4	erupce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
naznačovat	naznačovat	k5eAaImF	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Olympus	Olympus	k1gInSc1	Olympus
Mons	Monsa	k1gFnPc2	Monsa
respektive	respektive	k9	respektive
Mars	Mars	k1gInSc1	Mars
stále	stále	k6eAd1	stále
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důvodem	důvod	k1gInSc7	důvod
extrémního	extrémní	k2eAgInSc2d1	extrémní
rozměru	rozměr	k1gInSc2	rozměr
sopky	sopka	k1gFnSc2	sopka
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
chybějící	chybějící	k2eAgFnSc1d1	chybějící
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
litosférickými	litosférický	k2eAgFnPc7d1	litosférická
deskami	deska	k1gFnPc7	deska
respektive	respektive	k9	respektive
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Marsu	Mars	k1gInSc2	Mars
jedinou	jediný	k2eAgFnSc7d1	jediná
litosférickou	litosférický	k2eAgFnSc7d1	litosférická
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nejspíše	nejspíše	k9	nejspíše
nad	nad	k7c7	nad
horkou	horký	k2eAgFnSc7d1	horká
skvrnou	skvrna	k1gFnSc7	skvrna
a	a	k8xC	a
oproti	oproti	k7c3	oproti
například	například	k6eAd1	například
pozemské	pozemský	k2eAgFnSc3d1	pozemská
Havaji	Havaj	k1gFnSc3	Havaj
nepostupovala	postupovat	k5eNaImAgFnS	postupovat
deska	deska	k1gFnSc1	deska
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
další	další	k2eAgFnSc1d1	další
sopka	sopka	k1gFnSc1	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc1	veškerý
unikající	unikající	k2eAgInSc1d1	unikající
materiál	materiál	k1gInSc1	materiál
ze	z	k7c2	z
skvrny	skvrna	k1gFnSc2	skvrna
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
jediné	jediný	k2eAgFnSc2d1	jediná
masivní	masivní	k2eAgFnSc2d1	masivní
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Románová	románový	k2eAgFnSc1d1	románová
sci-fi	scii	k1gFnSc1	sci-fi
prvotina	prvotina	k1gFnSc1	prvotina
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Neffa	Neff	k1gMnSc2	Neff
Jádro	jádro	k1gNnSc1	jádro
pudla	pudl	k1gMnSc2	pudl
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
Olympus	Olympus	k1gMnSc1	Olympus
Mons	Mons	k1gInSc1	Mons
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc1	Mons
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
Pod	pod	k7c7	pod
kopyty	kopyto	k1gNnPc7	kopyto
bugolů	bugol	k1gInPc2	bugol
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Where	Wher	k1gInSc5	Wher
the	the	k?	the
Buggalo	Buggala	k1gFnSc5	Buggala
Roam	Roamo	k1gNnPc2	Roamo
<g/>
)	)	kIx)	)
vědeckofantastického	vědeckofantastický	k2eAgInSc2d1	vědeckofantastický
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Futurama	Futurama	k?	Futurama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sopku	sopka	k1gFnSc4	sopka
sjížděla	sjíždět	k5eAaImAgFnS	sjíždět
Sabrina	Sabrina	k1gFnSc1	Sabrina
-	-	kIx~	-
mladá	mladý	k2eAgFnSc1d1	mladá
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olympus	Olympus	k1gInSc1	Olympus
Mons	Mons	k1gInSc4	Mons
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Detailní	detailní	k2eAgInPc1d1	detailní
snímky	snímek	k1gInPc1	snímek
kaldery	kaldera	k1gFnSc2	kaldera
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
</s>
</p>
