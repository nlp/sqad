<p>
<s>
Licenciát	licenciát	k1gInSc1	licenciát
teologie	teologie	k1gFnSc2	teologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
theologiae	theologiae	k1gInSc1	theologiae
licentiatus	licentiatus	k1gInSc1	licentiatus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
udělován	udělovat	k5eAaImNgInS	udělovat
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
(	(	kIx(	(
<g/>
bohosloví	bohosloví	k1gNnSc1	bohosloví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
obvykle	obvykle	k6eAd1	obvykle
udělují	udělovat	k5eAaImIp3nP	udělovat
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
zkracován	zkracován	k2eAgMnSc1d1	zkracován
jako	jako	k8xS	jako
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
obdržení	obdržení	k1gNnSc3	obdržení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
získaný	získaný	k2eAgInSc4d1	získaný
titul	titul	k1gInSc4	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
příslušně	příslušně	k6eAd1	příslušně
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fakultativní	fakultativní	k2eAgFnSc4d1	fakultativní
zkoušku	zkouška	k1gFnSc4	zkouška
zpravidla	zpravidla	k6eAd1	zpravidla
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
poplatky	poplatek	k1gInPc7	poplatek
–	–	k?	–
nikoli	nikoli	k9	nikoli
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
dosažené	dosažený	k2eAgFnSc2d1	dosažená
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
<g/>
,	,	kIx,	,
tituly	titul	k1gInPc1	titul
získané	získaný	k2eAgFnSc2d1	získaná
rigorózním	rigorózní	k2eAgNnSc7d1	rigorózní
řízením	řízení	k1gNnSc7	řízení
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
malé	malý	k2eAgInPc4d1	malý
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
licenciát	licenciát	k1gInSc1	licenciát
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
i	i	k8xC	i
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jediným	jediný	k2eAgInSc7d1	jediný
licenciátním	licenciátní	k2eAgInSc7d1	licenciátní
titulem	titul	k1gInSc7	titul
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
"	"	kIx"	"
<g/>
licenciát	licenciát	k1gInSc1	licenciát
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgMnS	být
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
udělován	udělován	k2eAgMnSc1d1	udělován
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
omezení	omezení	k1gNnSc1	omezení
udělování	udělování	k1gNnSc2	udělování
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
od	od	k7c2	od
akademického	akademický	k2eAgInSc2d1	akademický
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
udělován	udělovat	k5eAaImNgInS	udělovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
obecně	obecně	k6eAd1	obecně
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
katolické	katolický	k2eAgInPc1d1	katolický
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
ThDr.	ThDr.	k1gFnSc7	ThDr.
Udělení	udělení	k1gNnSc2	udělení
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
jak	jak	k8xS	jak
předchozím	předchozí	k2eAgNnSc7d1	předchozí
složením	složení	k1gNnSc7	složení
státní	státní	k2eAgFnSc2d1	státní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obhájením	obhájení	k1gNnSc7	obhájení
licenciátní	licenciátní	k2eAgFnSc2d1	licenciátní
či	či	k8xC	či
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
udělení	udělení	k1gNnSc1	udělení
spjato	spjat	k2eAgNnSc1d1	spjato
i	i	k9	i
s	s	k7c7	s
vyznáním	vyznání	k1gNnSc7	vyznání
víry	víra	k1gFnSc2	víra
před	před	k7c7	před
velkým	velký	k2eAgMnSc7d1	velký
kancléřem	kancléř	k1gMnSc7	kancléř
fakulty	fakulta	k1gFnSc2	fakulta
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc7	jeho
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vlastním	vlastní	k2eAgMnSc7d1	vlastní
ordinářem	ordinář	k1gMnSc7	ordinář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
řádném	řádný	k2eAgNnSc6d1	řádné
ukončení	ukončení	k1gNnSc6	ukončení
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
,	,	kIx,	,
event.	event.	k?	event.
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
či	či	k8xC	či
ThDr.	ThDr.	k1gFnSc1	ThDr.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
možné	možný	k2eAgNnSc1d1	možné
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
studiu	studio	k1gNnSc6	studio
–	–	k?	–
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc1d1	velký
doktorát	doktorát	k1gInSc1	doktorát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
udělován	udělován	k2eAgInSc4d1	udělován
standardní	standardní	k2eAgInSc4d1	standardní
vědecký	vědecký	k2eAgInSc4d1	vědecký
titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
již	již	k6eAd1	již
uděluje	udělovat	k5eAaImIp3nS	udělovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
umělecké	umělecký	k2eAgFnSc2d1	umělecká
<g/>
)	)	kIx)	)
i	i	k9	i
náboženské	náboženský	k2eAgFnPc1d1	náboženská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
předpis	předpis	k1gInSc1	předpis
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
příslušný	příslušný	k2eAgInSc1d1	příslušný
rigorózní	rigorózní	k2eAgInSc1d1	rigorózní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
případně	případně	k6eAd1	případně
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
předložená	předložený	k2eAgFnSc1d1	předložená
magisterská	magisterský	k2eAgFnSc1d1	magisterská
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
či	či	k8xC	či
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
uznána	uznán	k2eAgFnSc1d1	uznána
i	i	k8xC	i
jako	jako	k9	jako
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Rigorózum	rigorózum	k1gNnSc1	rigorózum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
finančně	finančně	k6eAd1	finančně
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
–	–	k?	–
poplatky	poplatek	k1gInPc7	poplatek
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spojené	spojený	k2eAgInPc1d1	spojený
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Licenciát	licenciát	k1gInSc1	licenciát
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vyšších	vysoký	k2eAgInPc2d2	vyšší
akademických	akademický	k2eAgInPc2d1	akademický
gradů	grad	k1gInPc2	grad
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
licenciát	licenciát	k1gInSc1	licenciát
získáním	získání	k1gNnSc7	získání
gradu	grad	k1gInSc2	grad
stával	stávat	k5eAaImAgMnS	stávat
členem	člen	k1gMnSc7	člen
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
gradus	gradus	k1gInSc4	gradus
získal	získat	k5eAaPmAgInS	získat
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
svobodně	svobodně	k6eAd1	svobodně
vyučovat	vyučovat	k5eAaImF	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předstupněm	předstupeň	k1gInSc7	předstupeň
doktorátu	doktorát	k1gInSc2	doktorát
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
učitelů	učitel	k1gMnPc2	učitel
univerzity	univerzita	k1gFnSc2	univerzita
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
gradem	grad	k1gInSc7	grad
spokojila	spokojit	k5eAaPmAgFnS	spokojit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jim	on	k3xPp3gMnPc3	on
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
doktorát	doktorát	k1gInSc1	doktorát
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
profesury	profesura	k1gFnSc2	profesura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
teologických	teologický	k2eAgFnPc6d1	teologická
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
vzdělání	vzdělání	k1gNnSc2	vzdělání
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
teologii	teologie	k1gFnSc4	teologie
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
získává	získávat	k5eAaImIp3nS	získávat
licenciát	licenciát	k1gInSc4	licenciát
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
specializovaného	specializovaný	k2eAgNnSc2d1	specializované
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
uvedené	uvedený	k2eAgFnSc6d1	uvedená
oblasti	oblast	k1gFnSc6	oblast
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
udílen	udílen	k2eAgInSc1d1	udílen
doktorát	doktorát	k1gInSc1	doktorát
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
ThDr.	ThDr.	k1gFnSc1	ThDr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vedle	vedle	k7c2	vedle
dalších	další	k2eAgInPc2d1	další
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
přijat	přijat	k2eAgInSc1d1	přijat
Nejedlého	Nejedlého	k2eAgInSc1d1	Nejedlého
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
zrušil	zrušit	k5eAaPmAgInS	zrušit
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
stavovská	stavovský	k2eAgNnPc4d1	Stavovské
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
absolventy	absolvent	k1gMnPc4	absolvent
a	a	k8xC	a
udíleny	udílen	k2eAgFnPc4d1	udílena
tak	tak	k8xS	tak
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
promovaný	promovaný	k2eAgMnSc1d1	promovaný
právník	právník	k1gMnSc1	právník
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
určitá	určitý	k2eAgFnSc1d1	určitá
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgFnP	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgInPc4d1	fakultativní
<g/>
)	)	kIx)	)
malé	malý	k2eAgInPc4d1	malý
doktoráty	doktorát	k1gInPc4	doktorát
zrušeny	zrušen	k2eAgInPc4d1	zrušen
–	–	k?	–
ThDr.	ThDr.	k1gMnSc4	ThDr.
nebyl	být	k5eNaImAgInS	být
udělován	udělován	k2eAgInSc1d1	udělován
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
"	"	kIx"	"
<g/>
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
magisterský	magisterský	k2eAgInSc1d1	magisterský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
udělován	udělován	k2eAgInSc1d1	udělován
titul	titul	k1gInSc1	titul
magistra	magistra	k1gFnSc1	magistra
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
následně	následně	k6eAd1	následně
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebyly	být	k5eNaImAgInP	být
udělovány	udělován	k2eAgInPc1d1	udělován
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgInPc4d1	fakultativní
<g/>
)	)	kIx)	)
malé	malý	k2eAgInPc4d1	malý
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ThDr.	ThDr.	k1gFnSc1	ThDr.
<g/>
)	)	kIx)	)
znovu	znovu	k6eAd1	znovu
udělován	udělován	k2eAgInSc1d1	udělován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
a	a	k8xC	a
zpoplatněné	zpoplatněný	k2eAgFnSc6d1	zpoplatněná
rigorózní	rigorózní	k2eAgFnSc6d1	rigorózní
zkoušce	zkouška	k1gFnSc6	zkouška
–	–	k?	–
jeho	jeho	k3xOp3gNnSc4	jeho
udělení	udělení	k1gNnSc4	udělení
tak	tak	k6eAd1	tak
nepředchází	předcházet	k5eNaImIp3nS	předcházet
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
udělován	udělován	k2eAgInSc1d1	udělován
–	–	k?	–
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
–	–	k?	–
právě	právě	k6eAd1	právě
i	i	k9	i
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
licenciát	licenciát	k1gInSc1	licenciát
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jak	jak	k6eAd1	jak
ThLic	ThLice	k1gFnPc2	ThLice
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
licenciát	licenciát	k1gInSc1	licenciát
teologie	teologie	k1gFnSc2	teologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ThDr.	ThDr.	k1gMnSc1	ThDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
označují	označovat	k5eAaImIp3nP	označovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
udělován	udělován	k2eAgInSc4d1	udělován
i	i	k8xC	i
postgraduální	postgraduální	k2eAgInSc4d1	postgraduální
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
univerzitách	univerzita	k1gFnPc6	univerzita
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
však	však	k9	však
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
titulu	titul	k1gInSc2	titul
ThLic	ThLice	k1gInPc2	ThLice
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
či	či	k8xC	či
PhLic	PhLic	k1gMnSc1	PhLic
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
nutné	nutný	k2eAgNnSc1d1	nutné
studium	studium	k1gNnSc1	studium
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
obvykle	obvykle	k6eAd1	obvykle
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
za	za	k7c4	za
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
či	či	k8xC	či
tři	tři	k4xCgInPc4	tři
možnost	možnost	k1gFnSc4	možnost
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
PhD	PhD	k1gFnSc2	PhD
<g/>
.	.	kIx.	.
či	či	k8xC	či
event.	event.	k?	event.
ThD	ThD	k1gFnSc1	ThD
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
novelizován	novelizován	k2eAgInSc1d1	novelizován
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
lze	lze	k6eAd1	lze
od	od	k7c2	od
září	září	k1gNnSc2	září
2016	[number]	k4	2016
udílet	udílet	k5eAaImF	udílet
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ThDr.	ThDr.	k1gMnPc1	ThDr.
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
užívaný	užívaný	k2eAgInSc1d1	užívaný
postgraduální	postgraduální	k2eAgInSc1d1	postgraduální
titul	titul	k1gInSc1	titul
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
i	i	k9	i
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
