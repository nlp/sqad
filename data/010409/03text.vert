<p>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
,	,	kIx,	,
datum	datum	k1gInSc1	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1109	[number]	k4	1109
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
větve	větev	k1gFnSc2	větev
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
knížetem	kníže	k1gNnSc7wR	kníže
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
údělu	úděl	k1gInSc2	úděl
v	v	k7c6	v
letech	let	k1gInPc6	let
1091	[number]	k4	1091
<g/>
–	–	k?	–
<g/>
1107	[number]	k4	1107
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1107	[number]	k4	1107
<g/>
–	–	k?	–
<g/>
1109	[number]	k4	1109
pak	pak	k6eAd1	pak
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
byl	být	k5eAaImAgMnS	být
nejspíše	nejspíše	k9	nejspíše
prvorozeným	prvorozený	k2eAgMnSc7d1	prvorozený
synem	syn	k1gMnSc7	syn
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
údělného	údělný	k2eAgNnSc2d1	údělné
knížete	kníže	k1gNnSc2wR	kníže
Oty	Ota	k1gMnSc2	Ota
Olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
a	a	k8xC	a
Eufemie	eufemie	k1gFnPc4	eufemie
Uherské	uherský	k2eAgFnPc4d1	uherská
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Arpádovců	Arpádovec	k1gMnPc2	Arpádovec
a	a	k8xC	a
otcovým	otcův	k2eAgMnSc7d1	otcův
nástupcem	nástupce	k1gMnSc7	nástupce
-	-	kIx~	-
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Otou	Ota	k1gMnSc7	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Svatoplukovým	Svatoplukův	k2eAgMnSc7d1	Svatoplukův
dědem	děd	k1gMnSc7	děd
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
strýcem	strýc	k1gMnSc7	strýc
král	král	k1gMnSc1	král
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
údělník	údělník	k1gMnSc1	údělník
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1086	[number]	k4	1086
nebo	nebo	k8xC	nebo
1087	[number]	k4	1087
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
příliš	příliš	k6eAd1	příliš
mladí	mladý	k2eAgMnPc1d1	mladý
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
údělu	úděl	k1gInSc2	úděl
(	(	kIx(	(
<g/>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
narodil	narodit	k5eAaPmAgInS	narodit
až	až	k9	až
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Vratislav	Vratislav	k1gMnSc1	Vratislav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1090	[number]	k4	1090
olomoucký	olomoucký	k2eAgInSc4d1	olomoucký
úděl	úděl	k1gInSc4	úděl
svěřil	svěřit	k5eAaPmAgMnS	svěřit
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eufemie	eufemie	k1gFnSc1	eufemie
Uherská	uherský	k2eAgFnSc1d1	uherská
se	se	k3xPyFc4	se
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
k	k	k7c3	k
brněnskému	brněnský	k2eAgMnSc3d1	brněnský
údělníkovi	údělník	k1gMnSc3	údělník
<g/>
,	,	kIx,	,
Otovu	Otův	k2eAgMnSc3d1	Otův
bratrovi	bratr	k1gMnSc3	bratr
Konrádovi	Konrád	k1gMnSc3	Konrád
<g/>
.	.	kIx.	.
</s>
<s>
Konrád	Konrád	k1gMnSc1	Konrád
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snad	snad	k9	snad
pokusil	pokusit	k5eAaPmAgInS	pokusit
hájit	hájit	k5eAaImF	hájit
práva	právo	k1gNnPc4	právo
Otových	Otův	k2eAgMnPc2d1	Otův
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
získal	získat	k5eAaPmAgMnS	získat
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
Moravou	Morava	k1gFnSc7	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Brna	Brno	k1gNnSc2	Brno
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
svízelné	svízelný	k2eAgFnSc2d1	svízelná
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc1	nástupce
uznal	uznat	k5eAaPmAgMnS	uznat
Konráda	Konrád	k1gMnSc4	Konrád
Brněnského	brněnský	k2eAgMnSc4d1	brněnský
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
1091	[number]	k4	1091
<g/>
–	–	k?	–
<g/>
1092	[number]	k4	1092
<g/>
)	)	kIx)	)
Eufemie	eufemie	k1gFnSc1	eufemie
začala	začít	k5eAaPmAgFnS	začít
spravovat	spravovat	k5eAaImF	spravovat
Olomoucko	Olomoucko	k1gNnSc4	Olomoucko
<g/>
.	.	kIx.	.
</s>
<s>
Svatoplukovi	Svatoplukův	k2eAgMnPc1d1	Svatoplukův
ho	on	k3xPp3gMnSc4	on
předala	předat	k5eAaPmAgFnS	předat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1095	[number]	k4	1095
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1103	[number]	k4	1103
se	se	k3xPyFc4	se
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
účastnil	účastnit	k5eAaImAgMnS	účastnit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
knížetem	kníže	k1gMnSc7	kníže
Bořivojem	Bořivoj	k1gMnSc7	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
tažení	tažení	k1gNnSc1	tažení
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
bojovali	bojovat	k5eAaImAgMnP	bojovat
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Křivoústý	křivoústý	k2eAgMnSc1d1	křivoústý
a	a	k8xC	a
Zbyhněv	Zbyhněv	k1gMnSc1	Zbyhněv
<g/>
.	.	kIx.	.
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
získal	získat	k5eAaPmAgMnS	získat
1000	[number]	k4	1000
hřiven	hřivna	k1gFnPc2	hřivna
od	od	k7c2	od
Křivoústého	křivoústý	k2eAgNnSc2d1	křivoústý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepodnikal	podnikat	k5eNaImAgInS	podnikat
další	další	k2eAgInPc4d1	další
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
sumu	suma	k1gFnSc4	suma
se	se	k3xPyFc4	se
ale	ale	k9	ale
kníže	kníže	k1gMnSc1	kníže
s	s	k7c7	s
bratrancem	bratranec	k1gMnSc7	bratranec
nerozdělil	rozdělit	k5eNaPmAgMnS	rozdělit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
později	pozdě	k6eAd2	pozdě
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
Bořivojovi	Bořivoj	k1gMnSc3	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1105	[number]	k4	1105
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Uherskem	Uhersko	k1gNnSc7	Uhersko
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
odstranit	odstranit	k5eAaPmF	odstranit
Bořivoje	Bořivoj	k1gMnSc4	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
Svatoplukovi	Svatopluk	k1gMnSc3	Svatopluk
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1107	[number]	k4	1107
tedy	tedy	k9	tedy
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
nyní	nyní	k6eAd1	nyní
už	už	k6eAd1	už
nedůvěřivému	důvěřivý	k2eNgMnSc3d1	nedůvěřivý
Bořivoji	Bořivoj	k1gMnSc3	Bořivoj
II	II	kA	II
<g/>
.	.	kIx.	.
svého	svůj	k3xOyFgMnSc4	svůj
vyslance	vyslanec	k1gMnSc4	vyslanec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přeběhl	přeběhnout	k5eAaPmAgMnS	přeběhnout
od	od	k7c2	od
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
<g/>
.	.	kIx.	.
</s>
<s>
Pomluvil	pomluvit	k5eAaPmAgMnS	pomluvit
u	u	k7c2	u
knížete	kníže	k1gMnSc2	kníže
jeho	jeho	k3xOp3gMnSc1	jeho
přední	přední	k2eAgMnSc1d1	přední
rádce	rádce	k1gMnSc1	rádce
a	a	k8xC	a
spojence	spojenec	k1gMnSc4	spojenec
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prý	prý	k9	prý
sami	sám	k3xTgMnPc1	sám
spolčují	spolčovat	k5eAaImIp3nP	spolčovat
se	s	k7c7	s
Svatoplukem	Svatopluk	k1gMnSc7	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
rázem	rázem	k6eAd1	rázem
ztratil	ztratit	k5eAaPmAgMnS	ztratit
oporu	opora	k1gFnSc4	opora
své	svůj	k3xOyFgFnSc3	svůj
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
==	==	k?	==
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
byl	být	k5eAaImAgMnS	být
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
zajat	zajmout	k5eAaPmNgMnS	zajmout
králem	král	k1gMnSc7	král
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
Jindřichem	Jindřich	k1gMnSc7	Jindřich
V.	V.	kA	V.
Jindřich	Jindřich	k1gMnSc1	Jindřich
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
propustil	propustit	k5eAaPmAgInS	propustit
až	až	k9	až
za	za	k7c4	za
slib	slib	k1gInSc4	slib
obrovského	obrovský	k2eAgNnSc2d1	obrovské
výkupného	výkupné	k1gNnSc2	výkupné
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
si	se	k3xPyFc3	se
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
obstaral	obstarat	k5eAaPmAgMnS	obstarat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
vyrabováním	vyrabování	k1gNnSc7	vyrabování
četných	četný	k2eAgInPc2d1	četný
českých	český	k2eAgInPc2d1	český
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
V.	V.	kA	V.
se	se	k3xPyFc4	se
ale	ale	k9	ale
posléze	posléze	k6eAd1	posléze
usmířil	usmířit	k5eAaPmAgMnS	usmířit
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
byla	být	k5eAaImAgFnS	být
mezitím	mezitím	k6eAd1	mezitím
přepadena	přepadnout	k5eAaPmNgFnS	přepadnout
Poláky	Polák	k1gMnPc7	Polák
a	a	k8xC	a
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
se	se	k3xPyFc4	se
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
navrátit	navrátit	k5eAaPmF	navrátit
a	a	k8xC	a
nepřítele	nepřítel	k1gMnSc4	nepřítel
zahnat	zahnat	k5eAaPmF	zahnat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
potom	potom	k6eAd1	potom
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
Uhři	Uhři	k1gNnSc2	Uhři
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
jim	on	k3xPp3gMnPc3	on
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
vstříc	vstříc	k6eAd1	vstříc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
si	se	k3xPyFc3	se
o	o	k7c4	o
větévku	větévka	k1gFnSc4	větévka
vypíchl	vypíchnout	k5eAaPmAgMnS	vypíchnout
oko	oko	k1gNnSc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
také	také	k9	také
vyřešit	vyřešit	k5eAaPmF	vyřešit
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
trvající	trvající	k2eAgFnSc4d1	trvající
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
rivalitu	rivalita	k1gFnSc4	rivalita
mezi	mezi	k7c7	mezi
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
a	a	k8xC	a
rodem	rod	k1gInSc7	rod
Vršovců	Vršovec	k1gMnPc2	Vršovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1108	[number]	k4	1108
nechal	nechat	k5eAaPmAgMnS	nechat
Vršovce	Vršovec	k1gMnPc4	Vršovec
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
příbuznými	příbuzný	k1gMnPc7	příbuzný
povraždit	povraždit	k5eAaPmF	povraždit
a	a	k8xC	a
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc2	jejich
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
Svatoplukovým	Svatoplukův	k2eAgInSc7d1	Svatoplukův
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
odvetné	odvetný	k2eAgNnSc1d1	odvetné
tažení	tažení	k1gNnSc1	tažení
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
roku	rok	k1gInSc2	rok
1109	[number]	k4	1109
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
V.	V.	kA	V.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
byl	být	k5eAaImAgMnS	být
přepaden	přepadnout	k5eAaPmNgMnS	přepadnout
neznámým	známý	k2eNgMnSc7d1	neznámý
jezdcem	jezdec	k1gMnSc7	jezdec
a	a	k8xC	a
probodnut	probodnut	k2eAgInSc1d1	probodnut
kopím	kopit	k5eAaImIp1nS	kopit
<g/>
.	.	kIx.	.
</s>
<s>
Vrah	vrah	k1gMnSc1	vrah
byl	být	k5eAaImAgMnS	být
nejspíše	nejspíše	k9	nejspíše
mstitel	mstitel	k1gMnSc1	mstitel
Vršovců	Vršovec	k1gMnPc2	Vršovec
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyvraždění	vyvražděný	k2eAgMnPc1d1	vyvražděný
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
knížete	kníže	k1gNnSc2wR	kníže
byly	být	k5eAaImAgInP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
v	v	k7c6	v
kladrubském	kladrubský	k2eAgInSc6d1	kladrubský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
neznámou	známý	k2eNgFnSc7d1	neznámá
manželkou	manželka	k1gFnSc7	manželka
měl	mít	k5eAaImAgMnS	mít
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
Jindřicha	Jindřich	k1gMnSc2	Jindřich
(	(	kIx(	(
<g/>
1107	[number]	k4	1107
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
-	-	kIx~	-
1130	[number]	k4	1130
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
kmotra	kmotr	k1gMnSc4	kmotr
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
do	do	k7c2	do
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
1214	[number]	k4	1214
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
1034	[number]	k4	1034
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
712	[number]	k4	712
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
905	[number]	k4	905
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
na	na	k7c4	na
www.e-stredovek.cz	www.etredovek.cz	k1gInSc4	www.e-stredovek.cz
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
