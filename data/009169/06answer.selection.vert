<s>
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
VV	VV	kA	VV
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
období	období	k1gNnSc6	období
2010	[number]	k4	2010
až	až	k9	až
2012	[number]	k4	2012
participovala	participovat	k5eAaImAgFnS	participovat
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
