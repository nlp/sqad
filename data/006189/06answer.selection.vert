<s>
Předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
vlády	vláda	k1gFnSc2	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
se	se	k3xPyFc4	se
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
stala	stát	k5eAaPmAgFnS	stát
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
a	a	k8xC	a
jako	jako	k9	jako
hlavní	hlavní	k2eAgInPc4d1	hlavní
úkoly	úkol	k1gInPc4	úkol
si	se	k3xPyFc3	se
vytyčila	vytyčit	k5eAaPmAgFnS	vytyčit
zastavení	zastavení	k1gNnSc4	zastavení
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
propadu	propad	k1gInSc2	propad
a	a	k8xC	a
zeslabení	zeslabení	k1gNnSc1	zeslabení
role	role	k1gFnSc2	role
státu	stát	k1gInSc2	stát
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
.	.	kIx.	.
</s>
