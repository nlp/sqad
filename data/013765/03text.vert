<s>
Fallout	Fallout	k5eAaPmF
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
rozšířit	rozšířit	k5eAaPmF
úvod	úvod	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
styl	styl	k1gInSc4
dle	dle	k7c2
Wikipedie	Wikipedie	k1gFnSc2
<g/>
:	:	kIx,
<g/>
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
dodat	dodat	k5eAaPmF
další	další	k2eAgFnPc4d1
sekce	sekce	k1gFnPc4
(	(	kIx(
<g/>
charakteristiku	charakteristika	k1gFnSc4
<g/>
,	,	kIx,
hodnocení	hodnocení	k1gNnSc1
<g/>
,	,	kIx,
prodeje	prodej	k1gInSc2
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
FalloutLogo	FalloutLogo	k6eAd1
hryZákladní	hryZákladný	k2eAgMnPc1d1
informaceVývojářInterplay	informaceVývojářInterplaa	k1gFnSc2
EntertainmentVydavatelInterplay	EntertainmentVydavatelInterplaa	k1gFnSc2
EntertainmentRežisérFeargus	EntertainmentRežisérFeargus	k1gMnSc1
UrquhartProducentiTim	UrquhartProducentiTim	k1gMnSc1
Cain	Cain	k1gMnSc1
Brian	Brian	k1gMnSc1
FargoDesignéřiChristopher	FargoDesignéřiChristophra	k1gFnPc2
Taylor	Taylor	k1gMnSc1
David	David	k1gMnSc1
Hendee	Hende	k1gFnSc2
Scott	Scott	k1gMnSc1
EvertsProgramátorChris	EvertsProgramátorChris	k1gFnPc2
JonesSkladatelMark	JonesSkladatelMark	k1gInSc4
MorganHerní	MorganHerní	k2eAgFnSc2d1
sérieFalloutPlatformyMS-DOS	sérieFalloutPlatformyMS-DOS	k?
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
,	,	kIx,
Mac	Mac	kA
OS	OS	kA
<g/>
,	,	kIx,
OS	OS	kA
XDatum	XDatum	k1gNnSc1
vydání	vydání	k1gNnSc2
<g/>
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1997	#num#	k4
<g/>
ŽánrRPGHerní	ŽánrRPGHerní	k2eAgMnSc1d1
módSingle-playerKlasifikacePEGI	módSingle-playerKlasifikacePEGI	k?
ESRB	ESRB	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fallout	Fallout	k1gInSc1
1	#num#	k4
je	být	k5eAaImIp3nS
počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
od	od	k7c2
Interplay	Interplay	k1gInSc1
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
první	první	k4xOgInSc1
díl	díl	k1gInSc1
série	série	k1gFnSc2
Fallout	Fallout	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Gameplay	Gameplaa	k1gFnPc1
</s>
<s>
Fallout	Fallout	k5eAaPmF
je	být	k5eAaImIp3nS
2D	2D	k4
RPG	RPG	kA
s	s	k7c7
otevřeným	otevřený	k2eAgInSc7d1
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
RPG	RPG	kA
prvky	prvek	k1gInPc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgInP
na	na	k7c6
originálním	originální	k2eAgInSc6d1
systému	systém	k1gInSc6
SPECIAL	SPECIAL	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
herní	herní	k2eAgFnSc6d1
sérii	série	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
si	se	k3xPyFc3
na	na	k7c6
začátku	začátek	k1gInSc6
hry	hra	k1gFnSc2
může	moct	k5eAaImIp3nS
vybrat	vybrat	k5eAaPmF
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
tří	tři	k4xCgFnPc2
předvytvořených	předvytvořený	k2eAgFnPc2d1
postav	postava	k1gFnPc2
(	(	kIx(
<g/>
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
odpovídají	odpovídat	k5eAaImIp3nP
klasickým	klasický	k2eAgInSc7d1
RPG	RPG	kA
archetypům	archetyp	k1gInPc3
Bojovník	bojovník	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Lupič	lupič	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Diplomat	diplomat	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
anebo	anebo	k8xC
si	se	k3xPyFc3
pomocí	pomocí	k7c2
přidělení	přidělení	k1gNnSc2
bodů	bod	k1gInPc2
do	do	k7c2
statistik	statistika	k1gFnPc2
vytvořit	vytvořit	k5eAaPmF
vlastní	vlastnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
po	po	k7c6
dvou	dva	k4xCgFnPc6
herních	herní	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
–	–	k?
větší	veliký	k2eAgFnSc1d2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
používána	používán	k2eAgFnSc1d1
k	k	k7c3
přesunu	přesun	k1gInSc3
mezi	mezi	k7c7
městy	město	k1gNnPc7
(	(	kIx(
<g/>
a	a	k8xC
kde	kde	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
náhodným	náhodný	k2eAgNnPc3d1
setkáním	setkání	k1gNnPc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
menší	malý	k2eAgFnSc1d2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
znázorňuje	znázorňovat	k5eAaImIp3nS
samotné	samotný	k2eAgFnPc4d1
ulice	ulice	k1gFnPc4
měst	město	k1gNnPc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
jiné	jiný	k2eAgFnPc4d1
lokace	lokace	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
domy	dům	k1gInPc1
<g/>
,	,	kIx,
farmy	farma	k1gFnPc1
<g/>
,	,	kIx,
jeskyně	jeskyně	k1gFnPc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkušenosti	zkušenost	k1gFnPc1
jsou	být	k5eAaImIp3nP
získávány	získávat	k5eAaImNgFnP
bojem	boj	k1gInSc7
<g/>
,	,	kIx,
konverzacemi	konverzace	k1gFnPc7
<g/>
,	,	kIx,
používáním	používání	k1gNnSc7
schopností	schopnost	k1gFnPc2
a	a	k8xC
plněním	plnění	k1gNnSc7
questů	quest	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
questů	quest	k1gInPc2
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
způsobů	způsob	k1gInPc2
řešení	řešení	k1gNnSc2
a	a	k8xC
hráč	hráč	k1gMnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
si	se	k3xPyFc3
vybere	vybrat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
volby	volba	k1gFnPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
reflektovány	reflektován	k2eAgMnPc4d1
nejen	nejen	k6eAd1
v	v	k7c6
příběhu	příběh	k1gInSc6
(	(	kIx(
<g/>
prostřednictvím	prostřednictvím	k7c2
vyprávění	vyprávění	k1gNnSc2
v	v	k7c6
epilogu	epilog	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
hře	hra	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
dialozích	dialog	k1gInPc6
a	a	k8xC
přístupu	přístup	k1gInSc2
k	k	k7c3
dalším	další	k2eAgInPc3d1
questům	quest	k1gInPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
systém	systém	k1gInSc1
karmy	karma	k1gFnSc2
a	a	k8xC
reputací	reputace	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
upravuje	upravovat	k5eAaImIp3nS
reakce	reakce	k1gFnSc1
NPCs	NPCs	k1gInSc4
na	na	k7c4
hráčovu	hráčův	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
získání	získání	k1gNnSc6
dostatečného	dostatečný	k2eAgInSc2d1
počtu	počet	k1gInSc2
zkušeností	zkušenost	k1gFnPc2
se	se	k3xPyFc4
hlavní	hlavní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
dostává	dostávat	k5eAaImIp3nS
na	na	k7c4
další	další	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zlepšuje	zlepšovat	k5eAaImIp3nS
její	její	k3xOp3gFnPc4
schopnosti	schopnost	k1gFnPc4
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
hráči	hráč	k1gMnSc3
vybírat	vybírat	k5eAaImF
si	se	k3xPyFc3
dodatečné	dodatečný	k2eAgInPc4d1
bonusy	bonus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Boj	boj	k1gInSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
tahů	tah	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
hráč	hráč	k1gMnSc1
používá	používat	k5eAaImIp3nS
omezený	omezený	k2eAgInSc4d1
počet	počet	k1gInSc4
akčních	akční	k2eAgInPc2d1
bodů	bod	k1gInPc2
k	k	k7c3
provádění	provádění	k1gNnSc3
akcí	akce	k1gFnPc2
jako	jako	k9
je	být	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
<g/>
,	,	kIx,
útok	útok	k1gInSc1
<g/>
,	,	kIx,
používání	používání	k1gNnSc1
předmětů	předmět	k1gInPc2
apod.	apod.	kA
Konverzace	konverzace	k1gFnSc1
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
postavami	postava	k1gFnPc7
probíhají	probíhat	k5eAaImIp3nP
prostřednictvím	prostřednictví	k1gNnSc7
dialogových	dialogový	k2eAgNnPc2d1
oken	okno	k1gNnPc2
s	s	k7c7
možností	možnost	k1gFnSc7
vybírání	vybírání	k1gNnSc2
odpovědí	odpovědět	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
jiným	jiný	k2eAgMnPc3d1
soudobým	soudobý	k2eAgMnPc3d1
RPG	RPG	kA
hrám	hra	k1gFnPc3
jsou	být	k5eAaImIp3nP
statistiky	statistika	k1gFnPc1
postavy	postava	k1gFnSc2
v	v	k7c6
konverzacích	konverzace	k1gFnPc6
zohledněny	zohledněn	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
například	například	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
otevírá	otevírat	k5eAaImIp3nS
nové	nový	k2eAgFnPc4d1
dialogové	dialogový	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vysoká	vysoký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
zastrašování	zastrašování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Úvodní	úvodní	k2eAgNnSc1d1
video	video	k1gNnSc1
hry	hra	k1gFnSc2
stručně	stručně	k6eAd1
shrnuje	shrnovat	k5eAaImIp3nS
situaci	situace	k1gFnSc4
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
2077	#num#	k4
proběhl	proběhnout	k5eAaPmAgInS
konflikt	konflikt	k1gInSc1
o	o	k7c4
nerostné	nerostný	k2eAgFnPc4d1
suroviny	surovina	k1gFnPc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nakonec	nakonec	k6eAd1
přerostl	přerůst	k5eAaPmAgInS
v	v	k7c4
jadernou	jaderný	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
uchránili	uchránit	k5eAaPmAgMnP
před	před	k7c7
dopadem	dopad	k1gInSc7
nukleárních	nukleární	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
schovali	schovat	k5eAaPmAgMnP
se	se	k3xPyFc4
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
mohli	moct	k5eAaImAgMnP
<g/>
,	,	kIx,
v	v	k7c6
podzemních	podzemní	k2eAgInPc6d1
krytech	kryt	k1gInPc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
Vaultech	Vault	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
zbylo	zbýt	k5eAaPmAgNnS
samozřejmě	samozřejmě	k6eAd1
jenom	jenom	k9
pro	pro	k7c4
některé	některý	k3yIgNnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byla	být	k5eAaImAgFnS
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
vystavena	vystavit	k5eAaPmNgFnS
nemilosrdným	milosrdný	k2eNgFnPc3d1
podmínkám	podmínka	k1gFnPc3
po	po	k7c6
výbuchu	výbuch	k1gInSc6
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2157	#num#	k4
ve	v	k7c6
Vaultu	Vault	k1gInSc6
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdo	nikdo	k3yNnSc1
nic	nic	k3yNnSc1
netuší	tušit	k5eNaImIp3nS
o	o	k7c6
vnějším	vnější	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
současná	současný	k2eAgFnSc1d1
generace	generace	k1gFnSc1
již	již	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Vaultu	Vault	k1gInSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
z	z	k7c2
okolního	okolní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
porouchá	porouchat	k5eAaPmIp3nS
počítačový	počítačový	k2eAgInSc4d1
čip	čip	k1gInSc4
zajišťující	zajišťující	k2eAgNnSc1d1
čištění	čištění	k1gNnSc1
vody	voda	k1gFnSc2
–	–	k?
bez	bez	k7c2
něj	on	k3xPp3gMnSc2
do	do	k7c2
150	#num#	k4
dnů	den	k1gInPc2
všichni	všechen	k3xTgMnPc1
ve	v	k7c6
Vaultu	Vault	k1gInSc6
zemřou	zemřít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráčova	hráčův	k2eAgFnSc1d1
postava	postava	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
hře	hra	k1gFnSc6
nazývaná	nazývaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Vault	Vault	k2eAgMnSc1d1
Dweller	Dweller	k1gMnSc1
(	(	kIx(
<g/>
Obyvatel	obyvatel	k1gMnSc1
Vaultu	Vault	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pověřena	pověřen	k2eAgFnSc1d1
nalezením	nalezení	k1gNnSc7
náhrady	náhrada	k1gFnSc2
–	–	k?
Správce	správce	k1gMnSc1
Vaultu	Vault	k1gInSc2
mu	on	k3xPp3gMnSc3
předává	předávat	k5eAaImIp3nS
vybavení	vybavení	k1gNnSc4
a	a	k8xC
hermetickými	hermetický	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
jej	on	k3xPp3gMnSc4
propouští	propouštět	k5eAaImIp3nS
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vault	Vault	k2eAgInSc1d1
Dweller	Dweller	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
a	a	k8xC
zjišťuje	zjišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
přes	přes	k7c4
proběhlou	proběhlý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
se	se	k3xPyFc4
lidstvo	lidstvo	k1gNnSc1
vzpamatovalo	vzpamatovat	k5eAaPmAgNnS
a	a	k8xC
v	v	k7c6
pustině	pustina	k1gFnSc6
se	se	k3xPyFc4
nyní	nyní	k6eAd1
nachází	nacházet	k5eAaImIp3nS
několik	několik	k4yIc1
nových	nový	k2eAgFnPc2d1
osad	osada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
Nekropolí	nekropole	k1gFnSc7
<g/>
,	,	kIx,
troskami	troska	k1gFnPc7
města	město	k1gNnSc2
obydlenými	obydlený	k2eAgInPc7d1
Ghúly	Ghúl	k1gInPc7
(	(	kIx(
<g/>
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
kvůli	kvůli	k7c3
radiaci	radiace	k1gFnSc6
prodělali	prodělat	k5eAaPmAgMnP
nesmrtící	smrtící	k2eNgFnPc4d1
mutace	mutace	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nachází	nacházet	k5eAaImIp3nS
další	další	k2eAgInSc4d1
Vault	Vault	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgMnSc2,k3yIgMnSc2,k3yQgMnSc2
získává	získávat	k5eAaImIp3nS
náhradní	náhradní	k2eAgInSc1d1
vodní	vodní	k2eAgInSc1d1
čip	čip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
nalezení	nalezení	k1gNnSc6
čipu	čip	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
dopravení	dopravení	k1gNnSc2
zpět	zpět	k6eAd1
do	do	k7c2
Vaultu	Vault	k1gInSc2
13	#num#	k4
však	však	k9
dostává	dostávat	k5eAaImIp3nS
od	od	k7c2
Správce	správce	k1gMnSc2
druhý	druhý	k4xOgInSc4
úkol	úkol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
svého	svůj	k3xOyFgNnSc2
putování	putování	k1gNnSc2
se	se	k3xPyFc4
totiž	totiž	k9
hráč	hráč	k1gMnSc1
potkal	potkat	k5eAaPmAgMnS
se	se	k3xPyFc4
nepřátelskými	přátelský	k2eNgMnPc7d1
Supermutanty	Supermutant	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
příliš	příliš	k6eAd1
početní	početní	k2eAgMnPc1d1
a	a	k8xC
organizovaní	organizovaný	k2eAgMnPc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vznikli	vzniknout	k5eAaPmAgMnP
přirozeně	přirozeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
proto	proto	k8xC
vyslán	vyslat	k5eAaPmNgInS
zpět	zpět	k6eAd1
do	do	k7c2
Pustiny	pustina	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odhalil	odhalit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc4
zdroj	zdroj	k1gInSc4
a	a	k8xC
eliminoval	eliminovat	k5eAaBmAgMnS
jej	on	k3xPp3gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
začnou	začít	k5eAaPmIp3nP
Supermutanti	Supermutant	k1gMnPc1
představovat	představovat	k5eAaImF
hrozbu	hrozba	k1gFnSc4
i	i	k9
pro	pro	k7c4
Vault	Vault	k1gInSc4
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
katedrále	katedrála	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
poblíž	poblíž	k7c2
bývalého	bývalý	k2eAgMnSc2d1
Los	los	k1gInSc4
Angeles	Angeles	k1gMnSc1
a	a	k8xC
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c6
dalším	další	k2eAgMnSc6d1
z	z	k7c2
Vaultů	Vault	k1gInPc2
<g/>
,	,	kIx,
potkává	potkávat	k5eAaImIp3nS
Vault	Vault	k1gMnSc1
Dveller	Dveller	k1gMnSc1
Vůdce	vůdce	k1gMnSc1
–	–	k?
Vůdce	vůdce	k1gMnSc1
je	být	k5eAaImIp3nS
těžce	těžce	k6eAd1
zmutovaný	zmutovaný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
sloužící	sloužící	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgMnSc1d1
antagonista	antagonista	k1gMnSc1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
plán	plán	k1gInSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
Jednota	jednota	k1gFnSc1
<g/>
,	,	kIx,
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
přeměně	přeměna	k1gFnSc6
všech	všecek	k3xTgMnPc2
lidí	člověk	k1gMnPc2
pomocí	pomocí	k7c2
tzv.	tzv.	kA
F.	F.	kA
<g/>
E.V.	E.V.	k1gMnSc1
(	(	kIx(
<g/>
Forced	Forced	k1gMnSc1
Evolutionary	Evolutionara	k1gFnSc2
Virus	virus	k1gInSc1
–	–	k?
produkt	produkt	k1gInSc4
předválečného	předválečný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
s	s	k7c7
cílem	cíl	k1gInSc7
vytvořit	vytvořit	k5eAaPmF
dokonalého	dokonalý	k2eAgMnSc4d1
vojáka	voják	k1gMnSc4
budoucnosti	budoucnost	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c4
Supermutanty	Supermutant	k1gMnPc4
–	–	k?
tím	ten	k3xDgInSc7
by	by	k9
nejenom	nejenom	k6eAd1
vybavil	vybavit	k5eAaPmAgInS
lidstvo	lidstvo	k1gNnSc4
schopnostmi	schopnost	k1gFnPc7
lepšími	dobrý	k2eAgInPc7d2
k	k	k7c3
přežití	přežití	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
odstranil	odstranit	k5eAaPmAgMnS
veškeré	veškerý	k3xTgInPc4
rozdíly	rozdíl	k1gInPc4
a	a	k8xC
nerovnosti	nerovnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
stály	stát	k5eAaImAgFnP
za	za	k7c7
předchozí	předchozí	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
by	by	kYmCp3nS
však	však	k9
zahrnoval	zahrnovat	k5eAaImAgMnS
zničení	zničení	k1gNnSc2
Vaultu	Vault	k1gInSc2
13	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
Vůdce	vůdce	k1gMnSc1
hráčem	hráč	k1gMnSc7
buď	buď	k8xC
zabit	zabít	k5eAaPmNgMnS
v	v	k7c6
boji	boj	k1gInSc6
<g/>
,	,	kIx,
anebo	anebo	k8xC
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInSc1
plán	plán	k1gInSc1
je	být	k5eAaImIp3nS
odsouzen	odsoudit	k5eAaPmNgInS,k5eAaImNgInS
k	k	k7c3
nezdaru	nezdar	k1gInSc3
(	(	kIx(
<g/>
F.	F.	kA
<g/>
E.V.	E.V.	k1gFnSc1
totiž	totiž	k9
způsobuje	způsobovat	k5eAaImIp3nS
sterilitu	sterilita	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
všichni	všechen	k3xTgMnPc1
Supermutanti	Supermutant	k1gMnPc1
dříve	dříve	k6eAd2
nebo	nebo	k8xC
později	pozdě	k6eAd2
vymřou	vymřít	k5eAaPmIp3nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
načež	načež	k6eAd1
páchá	páchat	k5eAaImIp3nS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xC,k8xS
už	už	k6eAd1
je	být	k5eAaImIp3nS
konfrontace	konfrontace	k1gFnSc1
vyřešena	vyřešit	k5eAaPmNgFnS
jakkoliv	jakkoliv	k6eAd1
<g/>
,	,	kIx,
Vůdcova	vůdcův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
spustí	spustit	k5eAaPmIp3nS
atomovou	atomový	k2eAgFnSc4d1
bombu	bomba	k1gFnSc4
a	a	k8xC
výbuch	výbuch	k1gInSc1
srovná	srovnat	k5eAaPmIp3nS
Vault	Vault	k1gInSc4
i	i	k8xC
katedrálu	katedrála	k1gFnSc4
se	s	k7c7
zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Druhým	druhý	k4xOgInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
zničit	zničit	k5eAaPmF
zdroj	zdroj	k1gInSc4
nových	nový	k2eAgMnPc2d1
mutantů	mutant	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
jsou	být	k5eAaImIp3nP
Supermutantí	Supermutantí	k1gNnPc1
kasárna	kasárna	k1gNnPc4
a	a	k8xC
obří	obří	k2eAgFnPc4d1
kádě	káď	k1gFnPc4
s	s	k7c7
virem	vir	k1gInSc7
na	na	k7c6
vojenské	vojenský	k2eAgFnSc6d1
základně	základna	k1gFnSc6
Mariposa	Mariposa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariposa	Mariposa	k1gFnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Katedrála	katedrála	k1gFnSc1
<g/>
,	,	kIx,
disponuje	disponovat	k5eAaBmIp3nS
autodestrukčním	autodestrukční	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
po	po	k7c6
spuštění	spuštění	k1gNnSc6
zničí	zničit	k5eAaPmIp3nP
kádě	káď	k1gFnPc4
a	a	k8xC
celou	celý	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
zavalí	zavalit	k5eAaPmIp3nP
kamením	kamení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
porážkou	porážka	k1gFnSc7
Vůdce	vůdce	k1gMnSc1
se	se	k3xPyFc4
z	z	k7c2
Vault	Vault	k1gMnSc1
Dwellera	Dweller	k1gMnSc2
stává	stávat	k5eAaImIp3nS
hrdina	hrdina	k1gMnSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
před	před	k7c7
vchodem	vchod	k1gInSc7
do	do	k7c2
Vaultu	Vault	k1gInSc2
13	#num#	k4
na	na	k7c4
něj	on	k3xPp3gMnSc4
čeká	čekat	k5eAaImIp3nS
Správce	správce	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
jej	on	k3xPp3gMnSc4
odmítne	odmítnout	k5eAaPmIp3nS
znovu	znovu	k6eAd1
pustit	pustit	k5eAaPmF
dovnitř	dovnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obává	obávat	k5eAaImIp3nS
se	se	k3xPyFc4
totiž	totiž	k9
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
úspěšné	úspěšný	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
může	moct	k5eAaImIp3nS
inspirovat	inspirovat	k5eAaBmF
i	i	k9
další	další	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
opustili	opustit	k5eAaPmAgMnP
Vault	Vault	k1gInSc4
a	a	k8xC
vypravili	vypravit	k5eAaPmAgMnP
se	se	k3xPyFc4
do	do	k7c2
okolního	okolní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
jej	on	k3xPp3gMnSc4
vyhostí	vyhostit	k5eAaPmIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
Pustiny	pustina	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
také	také	k9
Vault	Vault	k2eAgInSc4d1
Dweller	Dweller	k1gInSc4
v	v	k7c6
závěrečném	závěrečný	k2eAgNnSc6d1
videu	video	k1gNnSc6
odchází	odcházet	k5eAaImIp3nS
(	(	kIx(
<g/>
pokud	pokud	k8xS
hráč	hráč	k1gMnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
hry	hra	k1gFnSc2
nasbíral	nasbírat	k5eAaPmAgInS
negativní	negativní	k2eAgFnSc4d1
karmu	karma	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
tak	tak	k9
sám	sám	k3xTgMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
zabije	zabít	k5eAaPmIp3nS
ještě	ještě	k9
předtím	předtím	k6eAd1
Správce	správka	k1gFnSc3
jedinou	jediný	k2eAgFnSc7d1
ranou	rána	k1gFnSc7
do	do	k7c2
zad	záda	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epilog	epilog	k1gInSc1
hry	hra	k1gFnSc2
popisuje	popisovat	k5eAaImIp3nS
osudy	osud	k1gInPc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
a	a	k8xC
mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
předchozích	předchozí	k2eAgInPc6d1
hráčových	hráčův	k2eAgInPc6d1
činech	čin	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
i	i	k9
nekanonický	kanonický	k2eNgInSc4d1
špatný	špatný	k2eAgInSc4d1
konec	konec	k1gInSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
přidat	přidat	k5eAaPmF
k	k	k7c3
Jednotě	jednota	k1gFnSc3
–	–	k?
v	v	k7c6
něm	on	k3xPp3gInSc6
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
přeměněn	přeměněn	k2eAgMnSc1d1
na	na	k7c4
Supermutanta	Supermutant	k1gMnSc4
<g/>
,	,	kIx,
načež	načež	k6eAd1
vede	vést	k5eAaImIp3nS
úspěšný	úspěšný	k2eAgInSc1d1
útok	útok	k1gInSc1
na	na	k7c4
Vault	Vault	k1gInSc4
13	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
Správce	správce	k1gMnSc1
osobně	osobně	k6eAd1
zabíjí	zabíjet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Vývojářské	vývojářský	k2eAgNnSc1d1
hledisko	hledisko	k1gNnSc1
<g/>
:	:	kIx,
Fallout	Fallout	k1gFnSc1
1	#num#	k4
vytvářela	vytvářet	k5eAaImAgFnS
relativně	relativně	k6eAd1
uzavřená	uzavřený	k2eAgFnSc1d1
(	(	kIx(
<g/>
ze	z	k7c2
dnešního	dnešní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
směšně	směšně	k6eAd1
malá	malý	k2eAgFnSc1d1
<g/>
)	)	kIx)
skupina	skupina	k1gFnSc1
nadšenců	nadšenec	k1gMnPc2
kolem	kolem	k7c2
již	již	k6eAd1
tehdy	tehdy	k6eAd1
zkušeného	zkušený	k2eAgMnSc4d1
vývojáře	vývojář	k1gMnSc4
Tima	Timus	k1gMnSc4
Caina	Cain	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
dalších	další	k2eAgInPc2d1
dílů	díl	k1gInPc2
od	od	k7c2
hry	hra	k1gFnSc2
nikdo	nikdo	k3yNnSc1
nic	nic	k3yNnSc1
nečekal	čekat	k5eNaImAgMnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
poměrně	poměrně	k6eAd1
klidnému	klidný	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
také	také	k9
k	k	k7c3
velké	velký	k2eAgFnSc3d1
kreativní	kreativní	k2eAgFnSc3d1
svobodě	svoboda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
částečné	částečný	k2eAgNnSc4d1
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
vývoj	vývoj	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
ve	v	k7c6
"	"	kIx"
<g/>
100	#num#	k4
<g/>
%	%	kIx~
svobodě	svoboda	k1gFnSc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
autoři	autor	k1gMnPc1
podle	podle	k7c2
svých	svůj	k3xOyFgNnPc2
slov	slovo	k1gNnPc2
proti	proti	k7c3
své	svůj	k3xOyFgFnSc3
vůli	vůle	k1gFnSc3
změnit	změnit	k5eAaPmF
některé	některý	k3yIgInPc4
z	z	k7c2
herních	herní	k2eAgInPc2d1
konců	konec	k1gInPc2
na	na	k7c4
méně	málo	k6eAd2
pesimistické	pesimistický	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fallout	Fallout	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
Fallout	Fallout	k1gFnSc2
Hlavní	hlavní	k2eAgInPc1d1
díly	díl	k1gInPc1
</s>
<s>
Fallout	Fallout	k5eAaPmF
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fallout	Fallout	k5eAaPmF
2	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fallout	Fallout	k5eAaPmF
Tactics	Tactics	k1gInSc4
<g/>
:	:	kIx,
Brotherhood	Brotherhood	k1gInSc1
of	of	k?
Steel	Steel	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fallout	Fallout	k5eAaPmF
3	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fallout	Fallout	k5eAaPmF
<g/>
:	:	kIx,
New	New	k1gFnSc1
Vegas	Vegas	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fallout	Fallout	k5eAaPmF
4	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fallout	Fallout	k5eAaPmF
76	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Vývojáři	vývojář	k1gMnPc1
</s>
<s>
Interplay	Interplay	k1gInPc1
(	(	kIx(
<g/>
Fallout	Fallout	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Black	Black	k1gMnSc1
Isle	Isl	k1gFnSc2
Studios	Studios	k?
(	(	kIx(
<g/>
Fallout	Fallout	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Micro	Micro	k6eAd1
Forté	Fortý	k2eAgNnSc1d1
(	(	kIx(
<g/>
Fallout	Fallout	k1gFnSc1
Tactics	Tacticsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Bethesda	Bethesda	k1gFnSc1
Softworks	Softworksa	k1gFnPc2
(	(	kIx(
<g/>
Fallout	Fallout	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
Modifikace	modifikace	k1gFnSc1
</s>
<s>
Fallout	Fallout	k5eAaPmF
1.5	1.5	k4
<g/>
:	:	kIx,
Resurrection	Resurrection	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
