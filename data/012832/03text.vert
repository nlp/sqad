<p>
<s>
Dipterologie	Dipterologie	k1gFnSc1	Dipterologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
dvoukřídlém	dvoukřídlý	k2eAgInSc6d1	dvoukřídlý
hmyzu	hmyz	k1gInSc6	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
entomologie	entomologie	k1gFnSc2	entomologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dipterologie	Dipterologie	k1gFnSc1	Dipterologie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
a	a	k8xC	a
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
na	na	k7c4	na
forenzní	forenzní	k2eAgFnSc4d1	forenzní
<g/>
,	,	kIx,	,
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
<g/>
,	,	kIx,	,
lesnickou	lesnický	k2eAgFnSc4d1	lesnická
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c6	na
dipterologii	dipterologie	k1gFnSc6	dipterologie
vychází	vycházet	k5eAaImIp3nS	vycházet
německý	německý	k2eAgInSc1d1	německý
časopis	časopis	k1gInSc1	časopis
Zeitschrift	Zeitschrift	k2eAgInSc1d1	Zeitschrift
für	für	k?	für
systematische	systematische	k1gInSc1	systematische
Hymenopterologie	Hymenopterologie	k1gFnSc1	Hymenopterologie
und	und	k?	und
Dipterologie	Dipterologie	k1gFnSc1	Dipterologie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
osobností	osobnost	k1gFnPc2	osobnost
československé	československý	k2eAgFnSc2d1	Československá
dipterologie	dipterologie	k1gFnSc2	dipterologie
byl	být	k5eAaImAgMnS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
Martinek	Martinek	k1gMnSc1	Martinek
<g/>
,	,	kIx,	,
dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
odborníkem	odborník	k1gMnSc7	odborník
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Čepelák	Čepelák	k1gMnSc1	Čepelák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ROZKOŠNÝ	rozkošný	k2eAgMnSc1d1	rozkošný
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Czechoslovak	Czechoslovak	k6eAd1	Czechoslovak
Dipterological	Dipterological	k1gMnSc5	Dipterological
Literature	Literatur	k1gMnSc5	Literatur
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
J.E.	J.E.	k1gFnSc1	J.E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
150	[number]	k4	150
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
