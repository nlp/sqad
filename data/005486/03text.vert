<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
Moby-Dick	Moby-Dick	k1gInSc1	Moby-Dick
<g/>
;	;	kIx,	;
or	or	k?	or
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Whale	Whale	k1gFnSc1	Whale
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hermana	Herman	k1gMnSc2	Herman
Melvilla	Melvill	k1gMnSc2	Melvill
<g/>
.	.	kIx.	.
</s>
<s>
Melville	Melville	k6eAd1	Melville
jej	on	k3xPp3gMnSc4	on
věnoval	věnovat	k5eAaImAgMnS	věnovat
svému	svůj	k3xOyFgMnSc3	svůj
blízkému	blízký	k2eAgMnSc3d1	blízký
příteli	přítel	k1gMnSc3	přítel
Nathanielovi	Nathaniel	k1gMnSc3	Nathaniel
Hawthornemu	Hawthornem	k1gMnSc3	Hawthornem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yIgMnPc3	který
ho	on	k3xPp3gNnSc4	on
pojil	pojit	k5eAaImAgInS	pojit
skepticismus	skepticismus	k1gInSc1	skepticismus
a	a	k8xC	a
neochota	neochota	k1gFnSc1	neochota
podbízet	podbízet	k5eAaImF	podbízet
se	se	k3xPyFc4	se
literárnímu	literární	k2eAgNnSc3d1	literární
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
originální	originální	k2eAgFnSc1d1	originální
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
nenašlo	najít	k5eNaPmAgNnS	najít
pochopení	pochopení	k1gNnSc1	pochopení
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
ocenění	ocenění	k1gNnSc3	ocenění
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
románu	román	k1gInSc2	román
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
kongeniální	kongeniální	k2eAgFnPc1d1	kongeniální
ilustrace	ilustrace	k1gFnPc1	ilustrace
amerického	americký	k2eAgMnSc2d1	americký
malíře	malíř	k1gMnSc2	malíř
Rockwella	Rockwell	k1gMnSc2	Rockwell
Kenta	Kent	k1gMnSc2	Kent
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
popisuje	popisovat	k5eAaImIp3nS	popisovat
plavbu	plavba	k1gFnSc4	plavba
velrybářské	velrybářský	k2eAgFnSc2d1	velrybářská
lodi	loď	k1gFnSc2	loď
Pequod	Pequoda	k1gFnPc2	Pequoda
a	a	k8xC	a
stíhání	stíhání	k1gNnSc2	stíhání
obrovského	obrovský	k2eAgNnSc2d1	obrovské
bílého	bílé	k1gNnSc2	bílé
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
námořníci	námořník	k1gMnPc1	námořník
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
Moby	Moba	k1gFnPc4	Moba
Dick	Dicka	k1gFnPc2	Dicka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
podávaného	podávaný	k2eAgInSc2d1	podávaný
postupy	postup	k1gInPc1	postup
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
Melville	Melville	k1gFnSc1	Melville
řadu	řad	k1gInSc2	řad
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc3	jeho
smyslu	smysl	k1gInSc3	smysl
a	a	k8xC	a
lidském	lidský	k2eAgNnSc6d1	lidské
pachtění	pachtění	k1gNnSc6	pachtění
za	za	k7c7	za
nereálnými	reálný	k2eNgInPc7d1	nereálný
cíli	cíl	k1gInPc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Honba	honba	k1gFnSc1	honba
za	za	k7c4	za
Moby	Mob	k1gMnPc4	Mob
Dickem	Dicko	k1gNnSc7	Dicko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
metaforou	metafora	k1gFnSc7	metafora
lidského	lidský	k2eAgInSc2d1	lidský
boje	boj	k1gInSc2	boj
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
až	až	k9	až
mystický	mystický	k2eAgInSc4d1	mystický
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Titánský	titánský	k2eAgInSc1d1	titánský
souboj	souboj	k1gInSc1	souboj
Achaba	Achab	k1gMnSc2	Achab
s	s	k7c7	s
Moby	Mob	k1gMnPc7	Mob
Dickem	Dicko	k1gNnSc7	Dicko
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
romantickém	romantický	k2eAgNnSc6d1	romantické
pojetí	pojetí	k1gNnSc6	pojetí
heroického	heroický	k2eAgMnSc2d1	heroický
hrdiny	hrdina	k1gMnSc2	hrdina
a	a	k8xC	a
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
často	často	k6eAd1	často
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vyvrcholení	vyvrcholení	k1gNnSc4	vyvrcholení
romantismu	romantismus	k1gInSc2	romantismus
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
roli	role	k1gFnSc4	role
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
"	"	kIx"	"
<g/>
dobyvatelem	dobyvatel	k1gMnSc7	dobyvatel
přírody	příroda	k1gFnSc2	příroda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
nemá	mít	k5eNaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
pouze	pouze	k6eAd1	pouze
literární	literární	k2eAgMnSc1d1	literární
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přináší	přinášet	k5eAaImIp3nS	přinášet
i	i	k9	i
vynikající	vynikající	k2eAgInPc4d1	vynikající
přírodovědné	přírodovědný	k2eAgInPc4d1	přírodovědný
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
životě	život	k1gInSc6	život
velryb	velryba	k1gFnPc2	velryba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
doceňovány	doceňovat	k5eAaImNgFnP	doceňovat
teprve	teprve	k6eAd1	teprve
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
předobraz	předobraz	k1gInSc1	předobraz
Moby	Moba	k1gMnSc2	Moba
Dicka	Dicek	k1gMnSc2	Dicek
skutečně	skutečně	k6eAd1	skutečně
existoval	existovat	k5eAaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
obrovský	obrovský	k2eAgInSc1d1	obrovský
částečně	částečně	k6eAd1	částečně
světle	světle	k6eAd1	světle
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
vorvaní	vorvaní	k2eAgInSc1d1	vorvaní
samec	samec	k1gInSc1	samec
přezdívaný	přezdívaný	k2eAgInSc1d1	přezdívaný
Mocha	mocha	k1gFnSc1	mocha
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
"	"	kIx"	"
<g/>
řádil	řádit	k5eAaImAgInS	řádit
<g/>
"	"	kIx"	"
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
a	a	k8xC	a
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
autor	autor	k1gMnSc1	autor
inspirován	inspirován	k2eAgMnSc1d1	inspirován
tragickým	tragický	k2eAgInSc7d1	tragický
osudem	osud	k1gInSc7	osud
škuneru	škuner	k1gInSc2	škuner
Essex	Essex	k1gInSc1	Essex
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
celkem	celkem	k6eAd1	celkem
ze	z	k7c2	z
135	[number]	k4	135
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
líčí	líčit	k5eAaImIp3nP	líčit
každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
Peqoud	Peqouda	k1gFnPc2	Peqouda
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
události	událost	k1gFnPc1	událost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stíhání	stíhání	k1gNnSc2	stíhání
Moby	Moba	k1gMnSc2	Moba
Dicka	Dicek	k1gMnSc2	Dicek
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
pak	pak	k8xC	pak
charaktery	charakter	k1gInPc1	charakter
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
také	také	k9	také
životní	životní	k2eAgInPc4d1	životní
zvyky	zvyk	k1gInPc4	zvyk
kytovců	kytovec	k1gMnPc2	kytovec
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
popisy	popis	k1gInPc1	popis
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
i	i	k8xC	i
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
na	na	k7c6	na
zobrazeních	zobrazení	k1gNnPc6	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1	vyprávění
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
biblickými	biblický	k2eAgInPc7d1	biblický
motivy	motiv	k1gInPc7	motiv
je	být	k5eAaImIp3nS	být
narušováno	narušovat	k5eAaImNgNnS	narušovat
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
pasážemi	pasáž	k1gFnPc7	pasáž
o	o	k7c6	o
velrybách	velryba	k1gFnPc6	velryba
a	a	k8xC	a
velrybářství	velrybářství	k1gNnSc3	velrybářství
a	a	k8xC	a
především	především	k6eAd1	především
filozofickými	filozofický	k2eAgFnPc7d1	filozofická
reflexemi	reflexe	k1gFnPc7	reflexe
<g/>
,	,	kIx,	,
věnovanými	věnovaný	k2eAgInPc7d1	věnovaný
kořistnickému	kořistnický	k2eAgMnSc3d1	kořistnický
a	a	k8xC	a
destruktivnímu	destruktivní	k2eAgInSc3d1	destruktivní
vztahu	vztah	k1gInSc3	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
pachtěním	pachtění	k1gNnSc7	pachtění
za	za	k7c7	za
nereálnými	reálný	k2eNgInPc7d1	nereálný
cíli	cíl	k1gInPc7	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k6eAd1	také
vývoji	vývoj	k1gInSc3	vývoj
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sociální	sociální	k2eAgInSc1d1	sociální
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
vědecké	vědecký	k2eAgInPc1d1	vědecký
poznatky	poznatek	k1gInPc1	poznatek
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
navyklá	navyklý	k2eAgNnPc4d1	navyklé
mravní	mravní	k2eAgNnPc4d1	mravní
a	a	k8xC	a
ideová	ideový	k2eAgNnPc4d1	ideové
hodnocení	hodnocení	k1gNnPc4	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
lodě	loď	k1gFnPc1	loď
Pequod	Pequoda	k1gFnPc2	Pequoda
<g/>
,	,	kIx,	,
národnostně	národnostně	k6eAd1	národnostně
i	i	k9	i
barvou	barva	k1gFnSc7	barva
pleti	pleť	k1gFnSc2	pleť
různorodé	různorodý	k2eAgFnSc2d1	různorodá
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
snahu	snaha	k1gFnSc4	snaha
autora	autor	k1gMnSc2	autor
o	o	k7c4	o
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
jejich	jejich	k3xOp3gFnSc2	jejich
lidské	lidský	k2eAgFnSc2d1	lidská
(	(	kIx(	(
<g/>
a	a	k8xC	a
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
i	i	k8xC	i
společenské	společenský	k2eAgFnSc2d1	společenská
<g/>
)	)	kIx)	)
rovnoprávnosti	rovnoprávnost	k1gFnSc2	rovnoprávnost
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
svými	svůj	k3xOyFgFnPc7	svůj
jmény	jméno	k1gNnPc7	jméno
paralelou	paralela	k1gFnSc7	paralela
k	k	k7c3	k
postavám	postava	k1gFnPc3	postava
biblickým	biblický	k2eAgFnPc3d1	biblická
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Izmael	Izmael	k1gInSc1	Izmael
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgInSc1d1	symbolizující
v	v	k7c6	v
Bílé	bílý	k2eAgFnSc6d1	bílá
velrybě	velryba	k1gFnSc6	velryba
vyvržence	vyvrženec	k1gMnPc4	vyvrženec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Bibli	bible	k1gFnSc4	bible
zapuzeným	zapuzený	k2eAgNnSc7d1	zapuzené
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Bohem	bůh	k1gMnSc7	bůh
zachráněným	zachráněný	k2eAgMnSc7d1	zachráněný
synem	syn	k1gMnSc7	syn
Abrahamovým	Abrahamův	k2eAgInPc3d1	Abrahamův
(	(	kIx(	(
<g/>
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
potom	potom	k6eAd1	potom
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
vzešel	vzejít	k5eAaPmAgInS	vzejít
arabský	arabský	k2eAgInSc1d1	arabský
národ	národ	k1gInSc1	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
biblického	biblický	k2eAgMnSc2d1	biblický
Achaba	Achab	k1gMnSc2	Achab
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zpronevěřil	zpronevěřit	k5eAaPmAgInS	zpronevěřit
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
a	a	k8xC	a
Moby	Mob	k2eAgInPc1d1	Mob
Dick	Dick	k1gInSc1	Dick
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
biblickému	biblický	k2eAgMnSc3d1	biblický
Leviatanovi	leviatan	k1gMnSc3	leviatan
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
začíná	začínat	k5eAaImIp3nS	začínat
naloděním	nalodění	k1gNnSc7	nalodění
mladíka	mladík	k1gMnSc2	mladík
<g/>
,	,	kIx,	,
nesoucího	nesoucí	k2eAgInSc2d1	nesoucí
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
rysy	rys	k1gInPc4	rys
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pod	pod	k7c7	pod
přijatým	přijatý	k2eAgNnSc7d1	přijaté
jménem	jméno	k1gNnSc7	jméno
Izmael	Izmaela	k1gFnPc2	Izmaela
<g/>
,	,	kIx,	,
po	po	k7c6	po
životním	životní	k2eAgNnSc6d1	životní
zklamání	zklamání	k1gNnSc6	zklamání
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
velrybářskou	velrybářský	k2eAgFnSc4d1	velrybářská
loď	loď	k1gFnSc4	loď
Pequod	Pequoda	k1gFnPc2	Pequoda
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Kvíkvegem	Kvíkveg	k1gMnSc7	Kvíkveg
<g/>
,	,	kIx,	,
harpunářem	harpunář	k1gMnSc7	harpunář
polynéského	polynéský	k2eAgInSc2d1	polynéský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
několika	několik	k4yIc2	několik
varování	varování	k1gNnPc2	varování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
nevyplouval	vyplouvat	k5eNaImAgInS	vyplouvat
(	(	kIx(	(
<g/>
kázání	kázání	k1gNnSc6	kázání
o	o	k7c6	o
prorokovi	prorok	k1gMnSc6	prorok
Jonášovi	Jonáš	k1gMnSc6	Jonáš
v	v	k7c6	v
námořnické	námořnický	k2eAgFnSc6d1	námořnická
kapli	kaple	k1gFnSc6	kaple
<g/>
,	,	kIx,	,
věštba	věštba	k1gFnSc1	věštba
starého	starý	k2eAgMnSc2d1	starý
námořníka	námořník	k1gMnSc2	námořník
se	s	k7c7	s
symbolickým	symbolický	k2eAgNnSc7d1	symbolické
jménem	jméno	k1gNnSc7	jméno
Eliáš	Eliáš	k1gMnSc1	Eliáš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Izmael	Izmael	k1gInSc1	Izmael
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nedbá	nedbat	k5eAaImIp3nS	nedbat
<g/>
.	.	kIx.	.
</s>
<s>
Lodi	loď	k1gFnPc4	loď
velí	velet	k5eAaImIp3nS	velet
kapitán	kapitán	k1gMnSc1	kapitán
Achab	Achab	k1gMnSc1	Achab
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Moby	Moby	k1gInPc7	Moby
Dick	Dick	k1gInSc1	Dick
kdysi	kdysi	k6eAd1	kdysi
připravil	připravit	k5eAaPmAgInS	připravit
o	o	k7c4	o
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Achab	Achab	k1gInSc1	Achab
je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgInSc1d1	posedlý
myšlenkou	myšlenka	k1gFnSc7	myšlenka
na	na	k7c4	na
pomstu	pomsta	k1gFnSc4	pomsta
a	a	k8xC	a
po	po	k7c6	po
vyplutí	vyplutí	k1gNnSc6	vyplutí
z	z	k7c2	z
Nantucketu	Nantucket	k1gInSc2	Nantucket
své	svůj	k3xOyFgFnSc2	svůj
posedlosti	posedlost	k1gFnSc2	posedlost
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
podléhá	podléhat	k5eAaImIp3nS	podléhat
a	a	k8xC	a
nutí	nutit	k5eAaImIp3nS	nutit
svou	svůj	k3xOyFgFnSc4	svůj
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
sebevražedném	sebevražedný	k2eAgNnSc6d1	sebevražedné
úsilí	úsilí	k1gNnSc6	úsilí
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navzdory	navzdory	k6eAd1	navzdory
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
nevýhodnosti	nevýhodnost	k1gFnPc1	nevýhodnost
honby	honba	k1gFnSc2	honba
za	za	k7c7	za
jednou	jeden	k4xCgFnSc7	jeden
konkrétní	konkrétní	k2eAgFnSc7d1	konkrétní
velrybou	velryba	k1gFnSc7	velryba
<g/>
,	,	kIx,	,
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
silným	silný	k2eAgFnPc3d1	silná
bouřím	bouř	k1gFnPc3	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
plavbě	plavba	k1gFnSc6	plavba
potkává	potkávat	k5eAaImIp3nS	potkávat
Pequod	Pequod	k1gInSc4	Pequod
jiné	jiný	k2eAgFnSc2d1	jiná
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
alegorická	alegorický	k2eAgFnSc1d1	alegorická
-	-	kIx~	-
Panna	Panna	k1gFnSc1	Panna
<g/>
,	,	kIx,	,
Růžové	růžový	k2eAgNnSc1d1	růžové
poupě	poupě	k1gNnSc1	poupě
<g/>
,	,	kIx,	,
Mládenec	mládenec	k1gMnSc1	mládenec
<g/>
,	,	kIx,	,
Rozkoš	rozkoš	k1gFnSc1	rozkoš
<g/>
,	,	kIx,	,
Ráchel	Ráchel	k1gFnSc1	Ráchel
(	(	kIx(	(
<g/>
její	její	k3xOp3gMnSc1	její
kapitán	kapitán	k1gMnSc1	kapitán
marně	marně	k6eAd1	marně
prosí	prosit	k5eAaImIp3nS	prosit
Achaba	Achaba	k1gFnSc1	Achaba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
syna	syn	k1gMnSc2	syn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
souboji	souboj	k1gInSc3	souboj
mezi	mezi	k7c7	mezi
Moby	Mob	k1gMnPc7	Mob
Dickem	Dicek	k1gMnSc7	Dicek
a	a	k8xC	a
velrybáři	velrybář	k1gMnPc7	velrybář
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
je	být	k5eAaImIp3nS	být
Achab	Achab	k1gInSc1	Achab
stažen	stáhnout	k5eAaPmNgInS	stáhnout
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
lanem	lano	k1gNnSc7	lano
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
harpuny	harpuna	k1gFnSc2	harpuna
<g/>
,	,	kIx,	,
Pequod	Pequod	k1gInSc1	Pequod
je	být	k5eAaImIp3nS	být
zraněnou	zraněný	k2eAgFnSc7d1	zraněná
velrybou	velryba	k1gFnSc7	velryba
zničena	zničen	k2eAgFnSc1d1	zničena
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
posádka	posádka	k1gFnSc1	posádka
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Izmaela	Izmael	k1gMnSc2	Izmael
<g/>
,	,	kIx,	,
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Izmael	Izmael	k1gInSc1	Izmael
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zachráněn	zachránit	k5eAaPmNgInS	zachránit
lodí	loď	k1gFnSc7	loď
Ráchel	Ráchel	k1gFnPc2	Ráchel
(	(	kIx(	(
<g/>
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
toužící	toužící	k2eAgFnSc1d1	toužící
po	po	k7c6	po
dítěti	dítě	k1gNnSc6	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Sea	Sea	k1gMnSc1	Sea
Beast	Beast	k1gMnSc1	Beast
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Mořská	mořský	k2eAgFnSc1d1	mořská
bestie	bestie	k1gFnSc1	bestie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Millard	Millard	k1gMnSc1	Millard
Webb	Webb	k1gMnSc1	Webb
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
John	John	k1gMnSc1	John
Barrymore	Barrymor	k1gInSc5	Barrymor
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
Dick	Dicka	k1gFnPc2	Dicka
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Lloyd	Lloyd	k1gMnSc1	Lloyd
Bacon	Bacon	k1gMnSc1	Bacon
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
John	John	k1gMnSc1	John
Barrymore	Barrymor	k1gMnSc5	Barrymor
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
Dick	Dicka	k1gFnPc2	Dicka
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Albert	Alberta	k1gFnPc2	Alberta
McCleery	McCleera	k1gFnSc2	McCleera
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
Victor	Victor	k1gMnSc1	Victor
Jory	Jora	k1gMnSc2	Jora
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
Dick	Dicka	k1gFnPc2	Dicka
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
John	John	k1gMnSc1	John
Huston	Huston	k1gInSc1	Huston
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
Ray	Ray	k1gFnSc2	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
Gregory	Gregor	k1gMnPc7	Gregor
Peck	Peck	k1gMnSc1	Peck
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
Dick	Dicka	k1gFnPc2	Dicka
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Franc	Franc	k1gMnSc1	Franc
Roddam	Roddam	k1gInSc1	Roddam
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
Patrick	Patrick	k1gMnSc1	Patrick
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moby	k1gInPc1	Moby
Dick	Dicka	k1gFnPc2	Dicka
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc1d1	německý
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Mike	Mike	k1gNnSc2	Mike
Barker	Barkra	k1gFnPc2	Barkra
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
kapitána	kapitán	k1gMnSc2	kapitán
Achaba	Achab	k1gMnSc2	Achab
William	William	k1gInSc4	William
Hurt	Hurt	k1gMnSc1	Hurt
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Vajtauer	Vajtauer	k1gMnSc1	Vajtauer
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
Školní	školní	k2eAgNnSc1d1	školní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Ondrůj	Ondrůj	k1gMnSc1	Ondrůj
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1945	[number]	k4	1945
a	a	k8xC	a
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
upravil	upravit	k5eAaPmAgMnS	upravit
Karel	Karel	k1gMnSc1	Karel
Vach	Vach	k?	Vach
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Václav	Václav	k1gMnSc1	Václav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Václav	Václav	k1gMnSc1	Václav
Klíma	Klíma	k1gMnSc1	Klíma
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Kornelová	Kornelová	k1gFnSc1	Kornelová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
a	a	k8xC	a
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
Moby	Mobum	k1gNnPc7	Mobum
Dick	Dick	k1gInSc1	Dick
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Václav	Václav	k1gMnSc1	Václav
Klíma	Klíma	k1gMnSc1	Klíma
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Kornelová	Kornelová	k1gFnSc1	Kornelová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc1	KMa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Moby	Moby	k1gInPc4	Moby
Dick	Dick	k1gInSc1	Dick
<g/>
;	;	kIx,	;
or	or	k?	or
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Whale	Whale	k1gFnSc1	Whale
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
text	text	k1gInSc1	text
románu	román	k1gInSc2	román
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Moby	Moby	k1gInPc4	Moby
Dick	Dick	k1gMnSc1	Dick
illustrated	illustrated	k1gInSc4	illustrated
by	by	kYmCp3nS	by
Rockwell	Rockwell	k1gMnSc1	Rockwell
Kent	Kent	k1gMnSc1	Kent
</s>
