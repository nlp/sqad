<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
Moby-Dick	Moby-Dick	k1gInSc1	Moby-Dick
<g/>
;	;	kIx,	;
or	or	k?	or
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Whale	Whale	k1gFnSc1	Whale
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hermana	Herman	k1gMnSc2	Herman
Melvilla	Melvill	k1gMnSc2	Melvill
<g/>
.	.	kIx.	.
</s>
