<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nejznámější	známý	k2eAgInSc1d3	nejznámější
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hermana	Herman	k1gMnSc2	Herman
Melvilla	Melvill	k1gMnSc2	Melvill
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
plavbu	plavba	k1gFnSc4	plavba
velrybářské	velrybářský	k2eAgFnSc2d1	velrybářská
lodi	loď	k1gFnSc2	loď
Pequod	Pequoda	k1gFnPc2	Pequoda
<g/>
?	?	kIx.	?
</s>
