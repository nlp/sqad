<s>
Saturn	Saturn	k1gInSc1	Saturn
(	(	kIx(	(
<g/>
starý	starý	k2eAgInSc1d1	starý
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
Hladolet	hladolet	k1gMnSc1	hladolet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
planet	planeta	k1gFnPc2	planeta
na	na	k7c6	na
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
po	po	k7c6	po
Jupiteru	Jupiter	k1gInSc6	Jupiter
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
již	již	k6eAd1	již
starověkými	starověký	k2eAgMnPc7d1	starověký
astronomy	astronom	k1gMnPc7	astronom
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
pojmenována	pojmenovat	k5eAaPmNgNnP	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
Saturnovi	Saturn	k1gMnSc6	Saturn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
obdobou	obdoba	k1gFnSc7	obdoba
řeckého	řecký	k2eAgMnSc2d1	řecký
boha	bůh	k1gMnSc2	bůh
Krona	Kron	k1gMnSc2	Kron
<g/>
.	.	kIx.	.
</s>
<s>
Astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
pro	pro	k7c4	pro
Saturn	Saturn	k1gInSc4	Saturn
je	být	k5eAaImIp3nS	být
♄	♄	k?	♄
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velké	velký	k2eAgMnPc4d1	velký
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
lehkými	lehký	k2eAgInPc7d1	lehký
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k6eAd1	hlavně
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
96,3	[number]	k4	96,3
%	%	kIx~	%
jejího	její	k3xOp3gInSc2	její
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
Saturnu	Saturn	k1gInSc2	Saturn
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
světle	světle	k6eAd1	světle
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vrstva	vrstva	k1gFnSc1	vrstva
mraků	mrak	k1gInPc2	mrak
s	s	k7c7	s
nejasnými	jasný	k2eNgInPc7d1	nejasný
pásy	pás	k1gInPc7	pás
různých	různý	k2eAgInPc2d1	různý
barevných	barevný	k2eAgInPc2d1	barevný
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
rovnoběžné	rovnoběžný	k2eAgInPc1d1	rovnoběžný
s	s	k7c7	s
rovníkem	rovník	k1gInSc7	rovník
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
oblačné	oblačný	k2eAgFnSc6d1	oblačná
vrstvě	vrstva	k1gFnSc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
-140	-140	k4	-140
°	°	k?	°
<g/>
C.	C.	kA	C.
Objem	objem	k1gInSc4	objem
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
764	[number]	k4	764
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
objem	objem	k1gInSc4	objem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
0,6873	[number]	k4	0,6873
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
planetu	planeta	k1gFnSc4	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnSc4d2	menší
střední	střední	k2eAgFnSc4d1	střední
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
soustavou	soustava	k1gFnSc7	soustava
planetárních	planetární	k2eAgInPc2d1	planetární
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgInPc1d1	viditelný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
i	i	k8xC	i
malým	malý	k2eAgInSc7d1	malý
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nP	značit
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
latinské	latinský	k2eAgNnSc4d1	latinské
abecedy	abeceda	k1gFnPc4	abeceda
<g/>
,	,	kIx,	,
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
také	také	k9	také
početná	početný	k2eAgFnSc1d1	početná
rodina	rodina	k1gFnSc1	rodina
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
oběh	oběh	k1gInSc1	oběh
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
vykoná	vykonat	k5eAaPmIp3nS	vykonat
Saturn	Saturn	k1gInSc1	Saturn
za	za	k7c4	za
29,46	[number]	k4	29,46
pozemského	pozemský	k2eAgInSc2d1	pozemský
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
jako	jako	k9	jako
nažloutlý	nažloutlý	k2eAgInSc1d1	nažloutlý
neblikavý	blikavý	k2eNgInSc1d1	blikavý
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
jasností	jasnost	k1gFnSc7	jasnost
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
nejjasnějšími	jasný	k2eAgFnPc7d3	nejjasnější
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ekliptiky	ekliptika	k1gFnSc2	ekliptika
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdálí	vzdálit	k5eNaPmIp3nS	vzdálit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
než	než	k8xS	než
2,5	[number]	k4	2,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
jedním	jeden	k4xCgNnSc7	jeden
znamením	znamení	k1gNnSc7	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
trvá	trvat	k5eAaImIp3nS	trvat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saturn	Saturn	k1gInSc1	Saturn
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stejným	stejný	k2eAgInSc7d1	stejný
procesem	proces	k1gInSc7	proces
jako	jako	k8xC	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
před	před	k7c7	před
4,6	[number]	k4	4,6
až	až	k9	až
4,7	[number]	k4	4,7
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohly	moct	k5eAaImAgFnP	moct
velké	velký	k2eAgFnPc4d1	velká
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
vzniknout	vzniknout	k5eAaPmF	vzniknout
a	a	k8xC	a
zformovat	zformovat	k5eAaPmF	zformovat
se	se	k3xPyFc4	se
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
teorii	teorie	k1gFnSc4	teorie
akrece	akrece	k1gFnSc2	akrece
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
akrece	akrece	k1gFnSc2	akrece
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
postupně	postupně	k6eAd1	postupně
slepovaly	slepovat	k5eAaImAgFnP	slepovat
drobné	drobný	k2eAgFnPc1d1	drobná
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
větší	veliký	k2eAgFnPc1d2	veliký
částice	částice	k1gFnPc1	částice
až	až	k9	až
posléze	posléze	k6eAd1	posléze
balvany	balvan	k1gInPc4	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgFnPc4d1	neustálá
srážky	srážka	k1gFnPc4	srážka
těles	těleso	k1gNnPc2	těleso
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
narůstání	narůstání	k1gNnSc1	narůstání
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
tělesa	těleso	k1gNnPc1	těleso
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
velká	velký	k2eAgNnPc1d1	velké
železokamenitá	železokamenitý	k2eAgNnPc1d1	železokamenitý
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zárodky	zárodek	k1gInPc7	zárodek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgNnPc1d1	podobné
tělesa	těleso	k1gNnPc1	těleso
mohla	moct	k5eAaImAgNnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
ve	v	k7c6	v
vzdálenějších	vzdálený	k2eAgFnPc6d2	vzdálenější
oblastech	oblast	k1gFnPc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlivem	vliv	k1gInSc7	vliv
velké	velký	k2eAgFnSc2d1	velká
gravitace	gravitace	k1gFnSc2	gravitace
začala	začít	k5eAaPmAgFnS	začít
strhávat	strhávat	k5eAaImF	strhávat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
nabalovat	nabalovat	k5eAaImF	nabalovat
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
planeta	planeta	k1gFnSc1	planeta
dorostla	dorůst	k5eAaPmAgFnS	dorůst
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Saturnu	Saturn	k1gInSc2	Saturn
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
35,49	[number]	k4	35,49
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
daleko	daleko	k6eAd1	daleko
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
rychlost	rychlost	k1gFnSc4	rychlost
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
nejspíše	nejspíše	k9	nejspíše
původní	původní	k2eAgNnPc4d1	původní
složení	složení	k1gNnPc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nabalil	nabalit	k5eAaPmAgMnS	nabalit
už	už	k6eAd1	už
během	během	k7c2	během
vzniku	vznik	k1gInSc2	vznik
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgFnPc1d1	velká
planety	planeta	k1gFnPc1	planeta
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
postupným	postupný	k2eAgNnSc7d1	postupné
slepováním	slepování	k1gNnSc7	slepování
drobných	drobný	k2eAgFnPc2d1	drobná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
rychlým	rychlý	k2eAgNnSc7d1	rychlé
smrštěním	smrštění	k1gNnSc7	smrštění
z	z	k7c2	z
nahuštěného	nahuštěný	k2eAgInSc2d1	nahuštěný
shluku	shluk	k1gInSc2	shluk
v	v	k7c6	v
zárodečném	zárodečný	k2eAgNnSc6d1	zárodečné
disku	disco	k1gNnSc6	disco
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
několika	několik	k4yIc2	několik
gravitačních	gravitační	k2eAgInPc2d1	gravitační
kolapsů	kolaps	k1gInPc2	kolaps
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Alan	Alan	k1gMnSc1	Alan
Boss	boss	k1gMnSc1	boss
z	z	k7c2	z
Carnegie	Carnegie	k1gFnSc2	Carnegie
Institution	Institution	k1gInSc1	Institution
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Saturnu	Saturn	k1gInSc2	Saturn
trval	trvat	k5eAaImAgInS	trvat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
velkých	velký	k2eAgInPc2d1	velký
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
měsíců	měsíc	k1gInPc2	měsíc
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vznikaly	vznikat	k5eAaImAgFnP	vznikat
kamenné	kamenný	k2eAgFnPc4d1	kamenná
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
však	však	k9	však
Saturn	Saturn	k1gInSc1	Saturn
velmi	velmi	k6eAd1	velmi
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
fází	fáze	k1gFnPc2	fáze
vzniku	vznik	k1gInSc2	vznik
měsíců	měsíc	k1gInPc2	měsíc
nevystoupila	vystoupit	k5eNaPmAgFnS	vystoupit
teplota	teplota	k1gFnSc1	teplota
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hodnoty	hodnota	k1gFnPc4	hodnota
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
okolí	okolí	k1gNnSc2	okolí
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
tak	tak	k6eAd1	tak
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
lehce	lehko	k6eAd1	lehko
tavitelných	tavitelný	k2eAgFnPc2d1	tavitelná
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
disku	disk	k1gInSc2	disk
okolo	okolo	k7c2	okolo
vznikající	vznikající	k2eAgFnSc2d1	vznikající
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Saturnově	Saturnův	k2eAgFnSc6d1	Saturnova
měsíční	měsíční	k2eAgFnSc6d1	měsíční
soustavě	soustava	k1gFnSc6	soustava
tak	tak	k6eAd1	tak
vysoké	vysoký	k2eAgNnSc1d1	vysoké
zastoupení	zastoupení	k1gNnSc1	zastoupení
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
zpětně	zpětně	k6eAd1	zpětně
obíhající	obíhající	k2eAgInPc1d1	obíhající
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
nejspíše	nejspíše	k9	nejspíše
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
zachycené	zachycený	k2eAgFnPc1d1	zachycená
planetky	planetka	k1gFnPc1	planetka
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
polární	polární	k2eAgInSc1d1	polární
průměr	průměr	k1gInSc1	průměr
(	(	kIx(	(
<g/>
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
120	[number]	k4	120
536	[number]	k4	536
km	km	kA	km
<g/>
,	,	kIx,	,
polární	polární	k2eAgInSc1d1	polární
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
108	[number]	k4	108
728	[number]	k4	728
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
rychlá	rychlý	k2eAgFnSc1d1	rychlá
rotace	rotace	k1gFnSc1	rotace
a	a	k8xC	a
spíše	spíše	k9	spíše
tekutá	tekutý	k2eAgFnSc1d1	tekutá
než	než	k8xS	než
pevná	pevný	k2eAgFnSc1d1	pevná
fáze	fáze	k1gFnSc1	fáze
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
tlaku	tlak	k1gInSc2	tlak
nevypařuje	vypařovat	k5eNaImIp3nS	vypařovat
až	až	k9	až
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
7000	[number]	k4	7000
K.	K.	kA	K.
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
i	i	k8xC	i
Saturn	Saturn	k1gMnSc1	Saturn
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tepla	teplo	k1gNnSc2	teplo
1,78	[number]	k4	1,78
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
než	než	k8xS	než
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
nejspíše	nejspíše	k9	nejspíše
klesáním	klesání	k1gNnSc7	klesání
hélia	hélium	k1gNnSc2	hélium
do	do	k7c2	do
spodnějších	spodní	k2eAgFnPc2d2	spodnější
vrstev	vrstva	k1gFnPc2	vrstva
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
celkově	celkově	k6eAd1	celkově
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
75	[number]	k4	75
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
25	[number]	k4	25
%	%	kIx~	%
hélia	hélium	k1gNnSc2	hélium
se	se	k3xPyFc4	se
stopami	stopa	k1gFnPc7	stopa
metanu	metan	k1gInSc2	metan
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
složení	složení	k1gNnSc1	složení
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
složení	složení	k1gNnSc4	složení
původní	původní	k2eAgFnSc2d1	původní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
z	z	k7c2	z
kovového	kovový	k2eAgInSc2d1	kovový
vodíku	vodík	k1gInSc2	vodík
či	či	k8xC	či
hélia	hélium	k1gNnSc2	hélium
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
sloučeniny	sloučenina	k1gFnPc1	sloučenina
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
obrovským	obrovský	k2eAgInSc7d1	obrovský
tlakem	tlak	k1gInSc7	tlak
panujícím	panující	k2eAgInSc7d1	panující
uvnitř	uvnitř	k7c2	uvnitř
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
000	[number]	k4	000
K.	K.	kA	K.
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
získaných	získaný	k2eAgInPc2d1	získaný
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
vodíku	vodík	k1gInSc2	vodík
ku	k	k7c3	k
héliu	hélium	k1gNnSc3	hélium
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
hloubkou	hloubka	k1gFnSc7	hloubka
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
ve	v	k7c6	v
vnitřku	vnitřek	k1gInSc6	vnitřek
planety	planeta	k1gFnSc2	planeta
narůstá	narůstat	k5eAaImIp3nS	narůstat
vlivem	vliv	k1gInSc7	vliv
nadložních	nadložní	k2eAgFnPc2d1	nadložní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
jádrem	jádro	k1gNnSc7	jádro
nejsou	být	k5eNaImIp3nP	být
zřetelné	zřetelný	k2eAgFnPc1d1	zřetelná
hranice	hranice	k1gFnPc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
500	[number]	k4	500
km	km	kA	km
pod	pod	k7c4	pod
vrcholky	vrcholek	k1gInPc4	vrcholek
mraků	mrak	k1gInPc2	mrak
vodík	vodík	k1gInSc1	vodík
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
kapalného	kapalný	k2eAgNnSc2d1	kapalné
skupenství	skupenství	k1gNnSc2	skupenství
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
globální	globální	k2eAgInSc1d1	globální
oceán	oceán	k1gInSc1	oceán
tekutého	tekutý	k2eAgInSc2d1	tekutý
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Blíže	blíže	k1gFnSc1	blíže
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
planety	planeta	k1gFnSc2	planeta
získává	získávat	k5eAaImIp3nS	získávat
kapalný	kapalný	k2eAgInSc1d1	kapalný
vodík	vodík	k1gInSc1	vodík
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vlastností	vlastnost	k1gFnSc7	vlastnost
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
25	[number]	k4	25
000	[number]	k4	000
až	až	k9	až
33	[number]	k4	33
000	[number]	k4	000
km	km	kA	km
pod	pod	k7c7	pod
vrchními	vrchní	k2eAgInPc7d1	vrchní
mraky	mrak	k1gInPc7	mrak
začíná	začínat	k5eAaImIp3nS	začínat
vrstva	vrstva	k1gFnSc1	vrstva
tekutého	tekutý	k2eAgInSc2d1	tekutý
kovového	kovový	k2eAgInSc2d1	kovový
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
hloubku	hloubka	k1gFnSc4	hloubka
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
vodíku	vodík	k1gInSc2	vodík
se	s	k7c7	s
zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
velmi	velmi	k6eAd1	velmi
dobrá	dobrý	k2eAgFnSc1d1	dobrá
elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
planety	planeta	k1gFnSc2	planeta
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
000	[number]	k4	000
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
ho	on	k3xPp3gMnSc4	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
směs	směs	k1gFnSc1	směs
skalnatého	skalnatý	k2eAgInSc2d1	skalnatý
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
i	i	k8xC	i
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
12	[number]	k4	12
000	[number]	k4	000
K	K	kA	K
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
8	[number]	k4	8
miliónů	milión	k4xCgInPc2	milión
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
22	[number]	k4	22
násobek	násobek	k1gInSc1	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
má	mít	k5eAaImIp3nS	mít
molekulární	molekulární	k2eAgInSc4d1	molekulární
vodík	vodík	k1gInSc4	vodík
(	(	kIx(	(
<g/>
96,3	[number]	k4	96,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
následován	následovat	k5eAaImNgInS	následovat
héliem	hélium	k1gNnSc7	hélium
(	(	kIx(	(
<g/>
3,25	[number]	k4	3,25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
obsah	obsah	k1gInSc1	obsah
hélia	hélium	k1gNnSc2	hélium
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžší	těžký	k2eAgNnSc1d2	těžší
hélium	hélium	k1gNnSc1	hélium
klesá	klesat	k5eAaImIp3nS	klesat
přes	přes	k7c4	přes
vodíkovou	vodíkový	k2eAgFnSc4d1	vodíková
vrstvu	vrstva	k1gFnSc4	vrstva
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
krystalický	krystalický	k2eAgInSc1d1	krystalický
amoniak	amoniak	k1gInSc1	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
také	také	k9	také
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
vlivem	vliv	k1gInSc7	vliv
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
chladnější	chladný	k2eAgMnSc1d2	chladnější
než	než	k8xS	než
atmosféra	atmosféra	k1gFnSc1	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
komplexnější	komplexní	k2eAgFnSc4d2	komplexnější
molekuly	molekula	k1gFnPc4	molekula
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ethan	ethan	k1gInSc1	ethan
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
deriváty	derivát	k1gInPc1	derivát
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Ionosféra	ionosféra	k1gFnSc1	ionosféra
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
řídká	řídký	k2eAgFnSc1d1	řídká
ionizovaná	ionizovaný	k2eAgFnSc1d1	ionizovaná
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
po	po	k7c4	po
prstenec	prstenec	k1gInSc4	prstenec
C.	C.	kA	C.
Nejvrchnější	vrchní	k2eAgFnSc1d3	nejvrchnější
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
mlžného	mlžný	k2eAgInSc2d1	mlžný
oparu	opar	k1gInSc2	opar
<g/>
.	.	kIx.	.
</s>
<s>
Mlha	mlha	k1gFnSc1	mlha
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgInPc6d1	horní
mracích	mrak	k1gInPc6	mrak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teplota	teplota	k1gFnSc1	teplota
přibližně	přibližně	k6eAd1	přibližně
–	–	k?	–
<g/>
140	[number]	k4	140
°	°	k?	°
<g/>
C.	C.	kA	C.
S	s	k7c7	s
mocností	mocnost	k1gFnSc7	mocnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
nitru	nitro	k1gNnSc3	nitro
planety	planeta	k1gFnSc2	planeta
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
skupenství	skupenství	k1gNnSc4	skupenství
různých	různý	k2eAgFnPc2d1	různá
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc1	vznik
mraků	mrak	k1gInPc2	mrak
různého	různý	k2eAgNnSc2d1	různé
složení	složení	k1gNnSc2	složení
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výškových	výškový	k2eAgFnPc6d1	výšková
hladinách	hladina	k1gFnPc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
vrstvu	vrstva	k1gFnSc4	vrstva
tvoří	tvořit	k5eAaImIp3nP	tvořit
krystalky	krystalka	k1gFnPc1	krystalka
čpavkového	čpavkový	k2eAgInSc2d1	čpavkový
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
mraků	mrak	k1gInPc2	mrak
ze	z	k7c2	z
siřičitanu	siřičitan	k1gInSc2	siřičitan
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
vrstvu	vrstva	k1gFnSc4	vrstva
tvoří	tvořit	k5eAaImIp3nP	tvořit
mraky	mrak	k1gInPc1	mrak
tvořené	tvořený	k2eAgInPc1d1	tvořený
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
planety	planeta	k1gFnSc2	planeta
padají	padat	k5eAaImIp3nP	padat
kapky	kapka	k1gFnPc1	kapka
heliového	heliový	k2eAgInSc2d1	heliový
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
jejich	jejich	k3xOp3gFnSc2	jejich
pohybové	pohybový	k2eAgFnSc2d1	pohybová
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saturn	Saturn	k1gInSc1	Saturn
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
přibližně	přibližně	k6eAd1	přibližně
1,78	[number]	k4	1,78
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařování	vyzařování	k1gNnSc1	vyzařování
energie	energie	k1gFnSc2	energie
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
ještě	ještě	k9	ještě
další	další	k2eAgInSc1d1	další
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgInSc1d1	gravitační
kolaps	kolaps	k1gInSc1	kolaps
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kelvinova-Helmholtzova	Kelvinova-Helmholtzův	k2eAgFnSc1d1	Kelvinova-Helmholtzův
nestabilita	nestabilita	k1gFnSc1	nestabilita
<g/>
)	)	kIx)	)
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
částí	část	k1gFnSc7	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
jsou	být	k5eAaImIp3nP	být
póly	pól	k1gInPc4	pól
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
americké	americký	k2eAgFnSc2d1	americká
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gMnSc1	Voyager
2	[number]	k4	2
překvapivě	překvapivě	k6eAd1	překvapivě
naměřily	naměřit	k5eAaBmAgFnP	naměřit
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
i	i	k9	i
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
rovníkového	rovníkový	k2eAgInSc2d1	rovníkový
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
odrazem	odraz	k1gInSc7	odraz
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
od	od	k7c2	od
horních	horní	k2eAgInPc2d1	horní
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podrobných	podrobný	k2eAgInPc6d1	podrobný
záběrech	záběr	k1gInPc6	záběr
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
se	se	k3xPyFc4	se
však	však	k9	však
atmosféra	atmosféra	k1gFnSc1	atmosféra
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
West	West	k1gMnSc1	West
z	z	k7c2	z
Jet	jet	k5eAaImF	jet
Propulsion	Propulsion	k1gInSc4	Propulsion
Laboratory	Laborator	k1gInPc7	Laborator
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
zobrazovacího	zobrazovací	k2eAgInSc2d1	zobrazovací
týmu	tým	k1gInSc2	tým
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
velmi	velmi	k6eAd1	velmi
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
obloha	obloha	k1gFnSc1	obloha
Saturnu	Saturn	k1gInSc2	Saturn
jevila	jevit	k5eAaImAgFnS	jevit
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobena	způsobit	k5eAaPmNgFnS	způsobit
rozptylem	rozptyl	k1gInSc7	rozptyl
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
vlivem	vliv	k1gInSc7	vliv
tzv.	tzv.	kA	tzv.
Rayleighova	Rayleighův	k2eAgInSc2d1	Rayleighův
rozptylu	rozptyl	k1gInSc2	rozptyl
na	na	k7c6	na
molekulách	molekula	k1gFnPc6	molekula
atmosféry	atmosféra	k1gFnSc2	atmosféra
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
světlo	světlo	k1gNnSc1	světlo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
rozptyluje	rozptylovat	k5eAaImIp3nS	rozptylovat
na	na	k7c6	na
molekulárním	molekulární	k2eAgInSc6d1	molekulární
dusíku	dusík	k1gInSc6	dusík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
se	se	k3xPyFc4	se
rozptyluje	rozptylovat	k5eAaImIp3nS	rozptylovat
na	na	k7c6	na
molekulárním	molekulární	k2eAgInSc6d1	molekulární
vodíku	vodík	k1gInSc6	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
však	však	k9	však
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
severnější	severní	k2eAgFnSc1d2	severnější
polokoule	polokoule	k1gFnSc1	polokoule
mnohem	mnohem	k6eAd1	mnohem
výrazněji	výrazně	k6eAd2	výrazně
modrá	modrý	k2eAgFnSc1d1	modrá
než	než	k8xS	než
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
hypotézy	hypotéza	k1gFnSc2	hypotéza
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
mraků	mrak	k1gInPc2	mrak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
žluté	žlutý	k2eAgFnSc6d1	žlutá
barvě	barva	k1gFnSc6	barva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saturnově	Saturnův	k2eAgFnSc6d1	Saturnova
atmosféře	atmosféra	k1gFnSc6	atmosféra
vanou	vanout	k5eAaImIp3nP	vanout
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
400	[number]	k4	400
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
v	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rychlosti	rychlost	k1gFnPc1	rychlost
až	až	k9	až
1	[number]	k4	1
800	[number]	k4	800
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pětkrát	pětkrát	k6eAd1	pětkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
nejrychlejší	rychlý	k2eAgInPc1d3	nejrychlejší
větry	vítr	k1gInPc1	vítr
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
větrů	vítr	k1gInPc2	vítr
směřuje	směřovat	k5eAaImIp3nS	směřovat
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
předbíhá	předbíhat	k5eAaImIp3nS	předbíhat
rotaci	rotace	k1gFnSc4	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
vanou	vanout	k5eAaImIp3nP	vanout
pouze	pouze	k6eAd1	pouze
slabší	slabý	k2eAgInPc1d2	slabší
větry	vítr	k1gInPc1	vítr
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
šířkách	šířka	k1gFnPc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
pohybem	pohyb	k1gInSc7	pohyb
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
vytvářením	vytváření	k1gNnSc7	vytváření
tmavších	tmavý	k2eAgNnPc2d2	tmavší
pásem	pásmo	k1gNnPc2	pásmo
oblaků	oblak	k1gInPc2	oblak
rovnoběžných	rovnoběžný	k2eAgInPc2d1	rovnoběžný
s	s	k7c7	s
rovníkem	rovník	k1gInSc7	rovník
a	a	k8xC	a
světlejších	světlý	k2eAgNnPc2d2	světlejší
pásem	pásmo	k1gNnPc2	pásmo
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
metanového	metanový	k2eAgInSc2d1	metanový
zákalu	zákal	k1gInSc2	zákal
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
výškách	výška	k1gFnPc6	výška
však	však	k9	však
tyto	tento	k3xDgInPc1	tento
oblačné	oblačný	k2eAgInPc1d1	oblačný
pásy	pás	k1gInPc1	pás
nejsou	být	k5eNaImIp3nP	být
tak	tak	k9	tak
kontrastní	kontrastní	k2eAgMnSc1d1	kontrastní
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgNnSc1d1	polární
zploštění	zploštění	k1gNnSc1	zploštění
působí	působit	k5eAaImIp3nS	působit
střídání	střídání	k1gNnSc1	střídání
světlejších	světlý	k2eAgInPc2d2	světlejší
a	a	k8xC	a
tmavších	tmavý	k2eAgInPc2d2	tmavší
pruhů	pruh	k1gInPc2	pruh
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
rovníkem	rovník	k1gInSc7	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgNnSc1d1	různé
zbarvení	zbarvení	k1gNnSc1	zbarvení
pruhů	pruh	k1gInPc2	pruh
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
rozdílným	rozdílný	k2eAgNnSc7d1	rozdílné
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
a	a	k8xC	a
různě	různě	k6eAd1	různě
silnou	silný	k2eAgFnSc7d1	silná
oblačností	oblačnost	k1gFnSc7	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférické	atmosférický	k2eAgInPc1d1	atmosférický
pásy	pás	k1gInPc1	pás
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
výrazné	výrazný	k2eAgFnPc1d1	výrazná
než	než	k8xS	než
u	u	k7c2	u
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
širší	široký	k2eAgInSc4d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
jsou	být	k5eAaImIp3nP	být
pásy	pás	k1gInPc1	pás
naopak	naopak	k6eAd1	naopak
tenčí	tenčit	k5eAaImIp3nP	tenčit
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
méně	málo	k6eAd2	málo
výraznou	výrazný	k2eAgFnSc4d1	výrazná
strukturu	struktura	k1gFnSc4	struktura
než	než	k8xS	než
pásy	pás	k1gInPc4	pás
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Výraznými	výrazný	k2eAgInPc7d1	výrazný
atmosférickými	atmosférický	k2eAgInPc7d1	atmosférický
útvary	útvar	k1gInPc7	útvar
jsou	být	k5eAaImIp3nP	být
světlé	světlý	k2eAgFnPc4d1	světlá
skvrny	skvrna	k1gFnPc4	skvrna
podobné	podobný	k2eAgFnPc4d1	podobná
tlakovým	tlakový	k2eAgFnPc3d1	tlaková
nížím	níže	k1gFnPc3	níže
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
konvektivními	konvektivní	k2eAgInPc7d1	konvektivní
proudy	proud	k1gInPc7	proud
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
skvrny	skvrna	k1gFnPc1	skvrna
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velké	velký	k2eAgInPc1d1	velký
výbuchy	výbuch	k1gInPc1	výbuch
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
oblastí	oblast	k1gFnPc2	oblast
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
projevy	projev	k1gInPc7	projev
konvekce	konvekce	k1gFnSc2	konvekce
jsou	být	k5eAaImIp3nP	být
vlnové	vlnový	k2eAgInPc4d1	vlnový
řetězce	řetězec	k1gInPc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
objevil	objevit	k5eAaPmAgInS	objevit
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
dalekohled	dalekohled	k1gInSc1	dalekohled
bílé	bílý	k2eAgFnSc2d1	bílá
bouřkové	bouřkový	k2eAgFnSc2d1	bouřková
víry	víra	k1gFnSc2	víra
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
klínu	klín	k1gInSc2	klín
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
útvarů	útvar	k1gInPc2	útvar
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Bouře	bouře	k1gFnSc1	bouře
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
rovníkem	rovník	k1gInSc7	rovník
a	a	k8xC	a
způsoboval	způsobovat	k5eAaImAgMnS	způsobovat
ji	on	k3xPp3gFnSc4	on
proud	proud	k1gInSc1	proud
přehřátých	přehřátý	k2eAgInPc2d1	přehřátý
plynů	plyn	k1gInPc2	plyn
stoupajících	stoupající	k2eAgInPc2d1	stoupající
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
snímky	snímek	k1gInPc1	snímek
zobrazily	zobrazit	k5eAaPmAgFnP	zobrazit
její	její	k3xOp3gInSc4	její
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
detailně	detailně	k6eAd1	detailně
zachytily	zachytit	k5eAaPmAgFnP	zachytit
změny	změna	k1gFnPc1	změna
její	její	k3xOp3gFnSc2	její
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgInPc1d1	bílý
bouřkové	bouřkový	k2eAgInPc1d1	bouřkový
mraky	mrak	k1gInPc1	mrak
byly	být	k5eAaImAgInP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
krystalky	krystalek	k1gInPc1	krystalek
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
až	až	k8xS	až
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
intenzivní	intenzivní	k2eAgFnPc4d1	intenzivní
rádiové	rádiový	k2eAgFnPc4d1	rádiová
emise	emise	k1gFnPc4	emise
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dostaly	dostat	k5eAaPmAgFnP	dostat
jméno	jméno	k1gNnSc4	jméno
Dračí	dračí	k2eAgFnSc2d1	dračí
bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Generátorem	generátor	k1gInSc7	generátor
radiového	radiový	k2eAgNnSc2d1	radiové
záření	záření	k1gNnSc2	záření
byly	být	k5eAaImAgInP	být
silné	silný	k2eAgInPc1d1	silný
výboje	výboj	k1gInPc1	výboj
statické	statický	k2eAgFnSc2d1	statická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
gigantický	gigantický	k2eAgInSc1d1	gigantický
hurikán	hurikán	k1gInSc1	hurikán
byl	být	k5eAaImAgInS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
produkovaly	produkovat	k5eAaImAgInP	produkovat
dynamické	dynamický	k2eAgInPc1d1	dynamický
procesy	proces	k1gInPc1	proces
v	v	k7c6	v
hlubších	hluboký	k2eAgFnPc6d2	hlubší
částech	část	k1gFnPc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Dračí	dračí	k2eAgFnSc1d1	dračí
bouře	bouře	k1gFnSc1	bouře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
páse	pás	k1gInSc6	pás
zvaném	zvaný	k2eAgInSc6d1	zvaný
"	"	kIx"	"
<g/>
Alej	alej	k1gFnSc1	alej
bouřek	bouřka	k1gFnPc2	bouřka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
objevil	objevit	k5eAaPmAgMnS	objevit
Glenn	Glenn	k1gMnSc1	Glenn
S.	S.	kA	S.
Orton	Orton	k1gMnSc1	Orton
a	a	k8xC	a
Padma	Padma	k1gFnSc1	Padma
Yanamandra-Fisherová	Yanamandra-Fisherová	k1gFnSc1	Yanamandra-Fisherová
pomocí	pomocí	k7c2	pomocí
přístroje	přístroj	k1gInSc2	přístroj
Long	Long	k1gMnSc1	Long
Wavelength	Wavelength	k1gMnSc1	Wavelength
Spectrometer	Spectrometer	k1gMnSc1	Spectrometer
na	na	k7c6	na
Keckově	Keckův	k2eAgFnSc6d1	Keckova
observatoři	observatoř	k1gFnSc6	observatoř
relativně	relativně	k6eAd1	relativně
teplý	teplý	k2eAgInSc1d1	teplý
polární	polární	k2eAgInSc1d1	polární
vír	vír	k1gInSc1	vír
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
teplé	teplý	k2eAgFnSc2d1	teplá
polární	polární	k2eAgFnSc2d1	polární
čepičky	čepička	k1gFnSc2	čepička
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
měření	měření	k1gNnSc2	měření
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
nárůst	nárůst	k1gInSc1	nárůst
teploty	teplota	k1gFnSc2	teplota
z	z	k7c2	z
88	[number]	k4	88
K	k	k7c3	k
na	na	k7c4	na
89	[number]	k4	89
K	k	k7c3	k
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
až	až	k9	až
na	na	k7c4	na
91	[number]	k4	91
K	k	k7c3	k
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejteplejší	teplý	k2eAgNnSc4d3	nejteplejší
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgFnPc1d1	polární
víry	víra	k1gFnPc1	víra
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Jupiteru	Jupiter	k1gInSc6	Jupiter
<g/>
,	,	kIx,	,
Marsu	Mars	k1gInSc6	Mars
a	a	k8xC	a
Venuši	Venuše	k1gFnSc6	Venuše
jsou	být	k5eAaImIp3nP	být
chladnější	chladný	k2eAgFnPc1d2	chladnější
než	než	k8xS	než
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
polární	polární	k2eAgInSc1d1	polární
vír	vír	k1gInSc1	vír
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
šířkách	šířka	k1gFnPc6	šířka
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
však	však	k9	však
teplejší	teplý	k2eAgInPc4d2	teplejší
než	než	k8xS	než
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
teplejší	teplý	k2eAgFnSc1d2	teplejší
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
oblast	oblast	k1gFnSc1	oblast
na	na	k7c4	na
póle	pól	k1gInSc5	pól
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
efekt	efekt	k1gInSc4	efekt
pouze	pouze	k6eAd1	pouze
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
Saturnu	Saturn	k1gInSc6	Saturn
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozorování	pozorování	k1gNnPc2	pozorování
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
výrazně	výrazně	k6eAd1	výrazně
stoupá	stoupat	k5eAaImIp3nS	stoupat
na	na	k7c4	na
70	[number]	k4	70
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
opět	opět	k6eAd1	opět
na	na	k7c4	na
87	[number]	k4	87
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
náhlé	náhlý	k2eAgNnSc1d1	náhlé
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
koncentrace	koncentrace	k1gFnSc1	koncentrace
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
záhadná	záhadný	k2eAgFnSc1d1	záhadná
struktura	struktura	k1gFnSc1	struktura
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
šestiúhelníku	šestiúhelník	k1gInSc2	šestiúhelník
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
hexagonal	hexagonat	k5eAaPmAgInS	hexagonat
cloud	cloud	k1gInSc1	cloud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
od	od	k7c2	od
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
podrobnější	podrobný	k2eAgInPc4d2	podrobnější
snímky	snímek	k1gInPc4	snímek
útvaru	útvar	k1gInSc2	útvar
pak	pak	k6eAd1	pak
přinesla	přinést	k5eAaPmAgFnS	přinést
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc5d1	Cassin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
za	za	k7c4	za
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
čas	čas	k1gInSc4	čas
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šestiúhelník	šestiúhelník	k1gInSc1	šestiúhelník
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
25	[number]	k4	25
000	[number]	k4	000
km	km	kA	km
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
úhly	úhel	k1gInPc1	úhel
jsou	být	k5eAaImIp3nP	být
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útvar	útvar	k1gInSc1	útvar
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
připomíná	připomínat	k5eAaImIp3nS	připomínat
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
víry	víra	k1gFnSc2	víra
nad	nad	k7c7	nad
zemskými	zemský	k2eAgInPc7d1	zemský
póly	pól	k1gInPc7	pól
<g/>
,	,	kIx,	,
planetology	planetolog	k1gMnPc7	planetolog
však	však	k8xC	však
zaráží	zarážet	k5eAaImIp3nS	zarážet
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
zaoblený	zaoblený	k2eAgInSc4d1	zaoblený
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Šestiúhelník	Šestiúhelník	k1gInSc1	Šestiúhelník
je	být	k5eAaImIp3nS	být
vnořený	vnořený	k2eAgInSc1d1	vnořený
100	[number]	k4	100
km	km	kA	km
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
75	[number]	k4	75
km	km	kA	km
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
útvar	útvar	k1gInSc1	útvar
zahalený	zahalený	k2eAgInSc1d1	zahalený
do	do	k7c2	do
tmy	tma	k1gFnSc2	tma
polární	polární	k2eAgFnSc2d1	polární
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
může	moct	k5eAaImIp3nS	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
infračerveného	infračervený	k2eAgInSc2d1	infračervený
mapovacího	mapovací	k2eAgInSc2d1	mapovací
spektrometru	spektrometr	k1gInSc2	spektrometr
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
prováděli	provádět	k5eAaImAgMnP	provádět
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
dynamikou	dynamika	k1gFnSc7	dynamika
tekutin	tekutina	k1gFnPc2	tekutina
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
obdobného	obdobný	k2eAgInSc2d1	obdobný
šestiúhelníkového	šestiúhelníkový	k2eAgInSc2d1	šestiúhelníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rotujícím	rotující	k2eAgInSc6d1	rotující
válci	válec	k1gInSc6	válec
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
další	další	k2eAgInPc4d1	další
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
po	po	k7c4	po
obarvení	obarvení	k1gNnSc4	obarvení
tekutin	tekutina	k1gFnPc2	tekutina
viděli	vidět	k5eAaImAgMnP	vidět
podobný	podobný	k2eAgInSc4d1	podobný
šestiúhelník	šestiúhelník	k1gInSc4	šestiúhelník
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hexagon	hexagon	k1gInSc1	hexagon
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
Saturnu	Saturn	k1gInSc2	Saturn
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
dynamické	dynamický	k2eAgFnSc2d1	dynamická
interakce	interakce	k1gFnSc2	interakce
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
proudy	proud	k1gInPc7	proud
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Saturnu	Saturn	k1gInSc6	Saturn
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
dvě	dva	k4xCgNnPc1	dva
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
léto	léto	k1gNnSc1	léto
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Saturn	Saturn	k1gInSc4	Saturn
nakloněný	nakloněný	k2eAgInSc4d1	nakloněný
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
s	s	k7c7	s
prstenci	prstenec	k1gInPc7	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
dopadají	dopadat	k5eAaImIp3nP	dopadat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
pod	pod	k7c7	pod
menším	malý	k2eAgInSc7d2	menší
úhlem	úhel	k1gInSc7	úhel
než	než	k8xS	než
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
méně	málo	k6eAd2	málo
odráží	odrážet	k5eAaImIp3nS	odrážet
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
se	se	k3xPyFc4	se
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
střídají	střídat	k5eAaImIp3nP	střídat
přibližně	přibližně	k6eAd1	přibližně
každých	každý	k3xTgNnPc2	každý
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
se	se	k3xPyFc4	se
však	však	k9	však
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
nijak	nijak	k6eAd1	nijak
neprojevují	projevovat	k5eNaImIp3nP	projevovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vlivem	vlivem	k7c2	vlivem
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
procesy	proces	k1gInPc7	proces
v	v	k7c6	v
Saturnu	Saturn	k1gInSc6	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
mohutných	mohutný	k2eAgInPc2d1	mohutný
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
útvarů	útvar	k1gInPc2	útvar
se	se	k3xPyFc4	se
však	však	k9	však
projevuje	projevovat	k5eAaImIp3nS	projevovat
jistá	jistý	k2eAgFnSc1d1	jistá
periodicita	periodicita	k1gFnSc1	periodicita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
výskytem	výskyt	k1gInSc7	výskyt
třech	tři	k4xCgFnPc6	tři
největších	veliký	k2eAgInPc2d3	veliký
dosud	dosud	k6eAd1	dosud
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
útvarů	útvar	k1gInPc2	útvar
uplynulo	uplynout	k5eAaPmAgNnS	uplynout
vždy	vždy	k6eAd1	vždy
přibližně	přibližně	k6eAd1	přibližně
57	[number]	k4	57
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
oběhy	oběh	k1gInPc4	oběh
Saturnu	Saturn	k1gInSc2	Saturn
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
je	být	k5eAaImIp3nS	být
však	však	k9	však
zatím	zatím	k6eAd1	zatím
příliš	příliš	k6eAd1	příliš
málo	málo	k6eAd1	málo
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
vědci	vědec	k1gMnPc1	vědec
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výskyt	výskyt	k1gInSc1	výskyt
velkých	velký	k2eAgFnPc2d1	velká
bouřek	bouřka	k1gFnPc2	bouřka
je	být	k5eAaImIp3nS	být
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
léta	léto	k1gNnSc2	léto
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
Saturnu	Saturn	k1gInSc2	Saturn
objevila	objevit	k5eAaPmAgFnS	objevit
sonda	sonda	k1gFnSc1	sonda
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc4d2	menší
intenzitu	intenzita	k1gFnSc4	intenzita
než	než	k8xS	než
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejslabší	slabý	k2eAgNnSc4d3	nejslabší
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
21	[number]	k4	21
μ	μ	k?	μ
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
silnější	silný	k2eAgNnSc4d2	silnější
než	než	k8xS	než
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
pozemským	pozemský	k2eAgNnSc7d1	pozemské
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
však	však	k9	však
Saturnovo	Saturnův	k2eAgNnSc1d1	Saturnovo
pole	pole	k1gNnSc1	pole
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
silnější	silný	k2eAgInSc4d2	silnější
dipólový	dipólový	k2eAgInSc4d1	dipólový
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
osa	osa	k1gFnSc1	osa
téměř	téměř	k6eAd1	téměř
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
rotační	rotační	k2eAgFnSc7d1	rotační
osou	osa	k1gFnSc7	osa
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Orientace	orientace	k1gFnSc1	orientace
magnetického	magnetický	k2eAgInSc2d1	magnetický
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
generováno	generovat	k5eAaImNgNnS	generovat
hydromagnetickým	hydromagnetický	k2eAgNnSc7d1	hydromagnetický
dynamem	dynamo	k1gNnSc7	dynamo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
sahá	sahat	k5eAaImIp3nS	sahat
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
není	být	k5eNaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
stejně	stejně	k6eAd1	stejně
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
je	být	k5eAaImIp3nS	být
pole	pole	k1gNnSc4	pole
vlivem	vlivem	k7c2	vlivem
tlaku	tlak	k1gInSc2	tlak
proudících	proudící	k2eAgFnPc2d1	proudící
částic	částice	k1gFnPc2	částice
deformováno	deformovat	k5eAaImNgNnS	deformovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
tvořící	tvořící	k2eAgFnSc2d1	tvořící
plazmový	plazmový	k2eAgInSc4d1	plazmový
torus	torus	k1gInSc4	torus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
1,1	[number]	k4	1,1
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
bude	být	k5eAaImBp3nS	být
protaženo	protažen	k2eAgNnSc1d1	protaženo
do	do	k7c2	do
chvostu	chvost	k1gInSc2	chvost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
za	za	k7c7	za
planetou	planeta	k1gFnSc7	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
Saturnu	Saturn	k1gInSc2	Saturn
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc1	jeho
prstence	prstenec	k1gInPc1	prstenec
a	a	k8xC	a
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgFnPc3d1	jiná
magnetosférám	magnetosféra	k1gFnPc3	magnetosféra
však	však	k9	však
Saturnova	Saturnův	k2eAgNnSc2d1	Saturnovo
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejspíše	nejspíše	k9	nejspíše
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
přítomností	přítomnost	k1gFnSc7	přítomnost
prstenců	prstenec	k1gInPc2	prstenec
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
v	v	k7c6	v
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
obíhají	obíhat	k5eAaImIp3nP	obíhat
planetu	planeta	k1gFnSc4	planeta
stejnou	stejný	k2eAgFnSc7d1	stejná
rychlostí	rychlost	k1gFnSc7	rychlost
jakou	jaký	k3yRgFnSc7	jaký
ona	onen	k3xDgFnSc1	onen
rotuje	rotovat	k5eAaImIp3nS	rotovat
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dráhy	dráha	k1gFnSc2	dráha
Titánu	Titán	k1gMnSc3	Titán
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rychlost	rychlost	k1gFnSc1	rychlost
pohybu	pohyb	k1gInSc2	pohyb
částic	částice	k1gFnPc2	částice
až	až	k9	až
193	[number]	k4	193
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
částice	částice	k1gFnSc1	částice
měsíc	měsíc	k1gInSc4	měsíc
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
oběhu	oběh	k1gInSc6	oběh
dokonce	dokonce	k9	dokonce
předbíhají	předbíhat	k5eAaImIp3nP	předbíhat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ten	ten	k3xDgMnSc1	ten
obíhá	obíhat	k5eAaImIp3nS	obíhat
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
ionizované	ionizovaný	k2eAgFnPc1d1	ionizovaná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nezúčastňují	zúčastňovat	k5eNaImIp3nP	zúčastňovat
pohybu	pohyb	k1gInSc3	pohyb
částic	částice	k1gFnPc2	částice
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
existence	existence	k1gFnSc2	existence
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
příležitostně	příležitostně	k6eAd1	příležitostně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
v	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
části	část	k1gFnSc6	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
nebyly	být	k5eNaImAgFnP	být
polární	polární	k2eAgFnPc1d1	polární
záře	zář	k1gFnPc1	zář
zatím	zatím	k6eAd1	zatím
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
slabší	slabý	k2eAgMnPc1d2	slabší
než	než	k8xS	než
na	na	k7c6	na
Jupiteru	Jupiter	k1gMnSc6	Jupiter
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
pozorování	pozorování	k1gNnSc6	pozorování
je	být	k5eAaImIp3nS	být
rušeno	rušen	k2eAgNnSc1d1	rušeno
odrazy	odraz	k1gInPc7	odraz
a	a	k8xC	a
rozptýleným	rozptýlený	k2eAgNnSc7d1	rozptýlené
světlem	světlo	k1gNnSc7	světlo
na	na	k7c6	na
prstencích	prstenec	k1gInPc6	prstenec
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pozorované	pozorovaný	k2eAgFnPc1d1	pozorovaná
polární	polární	k2eAgFnPc1d1	polární
záře	zář	k1gFnPc1	zář
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
1	[number]	k4	1
600	[number]	k4	600
km	km	kA	km
nad	nad	k7c4	nad
oblačnou	oblačný	k2eAgFnSc4d1	oblačná
vrstvu	vrstva	k1gFnSc4	vrstva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Sledováním	sledování	k1gNnSc7	sledování
zmenšování	zmenšování	k1gNnSc2	zmenšování
a	a	k8xC	a
zvětšování	zvětšování	k1gNnSc1	zvětšování
se	se	k3xPyFc4	se
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
mohou	moct	k5eAaImIp3nP	moct
astronomové	astronom	k1gMnPc1	astronom
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
sledovat	sledovat	k5eAaImF	sledovat
atmosféru	atmosféra	k1gFnSc4	atmosféra
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
její	její	k3xOp3gNnSc1	její
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnPc1d2	novější
studie	studie	k1gFnPc1	studie
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c4	na
pozorování	pozorování	k1gNnPc4	pozorování
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
a	a	k8xC	a
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
polární	polární	k2eAgFnSc1d1	polární
záře	záře	k1gFnSc1	záře
Saturnu	Saturn	k1gInSc2	Saturn
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
polárních	polární	k2eAgFnPc2d1	polární
září	zář	k1gFnPc2	zář
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
polární	polární	k2eAgInSc1d1	polární
prstenec	prstenec	k1gInSc1	prstenec
často	často	k6eAd1	často
není	být	k5eNaImIp3nS	být
spojitý	spojitý	k2eAgInSc1d1	spojitý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
neúplného	úplný	k2eNgInSc2d1	neúplný
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
pozemské	pozemský	k2eAgFnSc3d1	pozemská
polární	polární	k2eAgFnSc3d1	polární
záři	zář	k1gFnSc3	zář
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
po	po	k7c4	po
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přítomnosti	přítomnost	k1gFnSc2	přítomnost
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
radiačních	radiační	k2eAgInPc2d1	radiační
pásů	pás	k1gInPc2	pás
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
oblastí	oblast	k1gFnPc2	oblast
kolem	kolem	k7c2	kolem
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
částice	částice	k1gFnPc1	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Záření	záření	k1gNnSc1	záření
z	z	k7c2	z
radiačních	radiační	k2eAgInPc2d1	radiační
pásů	pás	k1gInPc2	pás
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
slabé	slabý	k2eAgNnSc1d1	slabé
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
záření	záření	k1gNnSc2	záření
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
pásů	pás	k1gInPc2	pás
není	být	k5eNaImIp3nS	být
měřitelné	měřitelný	k2eAgInPc4d1	měřitelný
soudobými	soudobý	k2eAgInPc7d1	soudobý
přístroji	přístroj	k1gInPc7	přístroj
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
prstenců	prstenec	k1gInPc2	prstenec
a	a	k8xC	a
měsíců	měsíc	k1gInPc2	měsíc
nejsou	být	k5eNaImIp3nP	být
radiační	radiační	k2eAgInPc1d1	radiační
pásy	pás	k1gInPc1	pás
spojité	spojitý	k2eAgInPc1d1	spojitý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnPc1	jejich
částice	částice	k1gFnPc1	částice
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
elektricky	elektricky	k6eAd1	elektricky
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc1d3	nejmenší
částice	částice	k1gFnSc1	částice
prstence	prstenec	k1gInSc2	prstenec
však	však	k9	však
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
s	s	k7c7	s
nabitými	nabitý	k2eAgFnPc7d1	nabitá
částicemi	částice	k1gFnPc7	částice
radiačního	radiační	k2eAgInSc2d1	radiační
pásu	pás	k1gInSc2	pás
opouští	opouštět	k5eAaImIp3nS	opouštět
prstenec	prstenec	k1gInSc1	prstenec
a	a	k8xC	a
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
ke	k	k7c3	k
stacionárně	stacionárně	k6eAd1	stacionárně
rotujícím	rotující	k2eAgFnPc3d1	rotující
částicím	částice	k1gFnPc3	částice
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc4	slunce
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1426,9	[number]	k4	1426,9
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Jupitera	Jupiter	k1gMnSc2	Jupiter
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
desetinásobek	desetinásobek	k1gInSc4	desetinásobek
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
eliptická	eliptický	k2eAgFnSc1d1	eliptická
<g/>
,	,	kIx,	,
blízká	blízký	k2eAgFnSc1d1	blízká
kruhové	kruhový	k2eAgFnSc3d1	kruhová
<g/>
.	.	kIx.	.
</s>
<s>
Odklon	odklon	k1gInSc1	odklon
osy	osa	k1gFnSc2	osa
od	od	k7c2	od
kolmice	kolmice	k1gFnSc2	kolmice
na	na	k7c4	na
ekliptiku	ekliptika	k1gFnSc4	ekliptika
je	být	k5eAaImIp3nS	být
26,7	[number]	k4	26,7
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
4	[number]	k4	4
stupně	stupeň	k1gInSc2	stupeň
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
skloněna	skloněn	k2eAgFnSc1d1	skloněna
rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
planety	planeta	k1gFnSc2	planeta
vůči	vůči	k7c3	vůči
oběžné	oběžný	k2eAgFnSc3d1	oběžná
dráze	dráha	k1gFnSc3	dráha
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
viditelnosti	viditelnost	k1gFnSc2	viditelnost
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
prstenců	prstenec	k1gInPc2	prstenec
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
lépe	dobře	k6eAd2	dobře
či	či	k8xC	či
hůře	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
sklonu	sklona	k1gFnSc4	sklona
vůči	vůči	k7c3	vůči
pozemskému	pozemský	k2eAgMnSc3d1	pozemský
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
odráženého	odrážený	k2eAgNnSc2d1	odrážené
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
rychlost	rychlost	k1gFnSc1	rychlost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
9,66	[number]	k4	9,66
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
34	[number]	k4	34
703	[number]	k4	703
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Jedna	jeden	k4xCgFnSc1	jeden
otočka	otočka	k1gFnSc1	otočka
planety	planeta	k1gFnSc2	planeta
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
trvá	trvat	k5eAaImIp3nS	trvat
10,66	[number]	k4	10,66
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
s	s	k7c7	s
nejkratším	krátký	k2eAgInSc7d3	nejkratší
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
rotaci	rotace	k1gFnSc4	rotace
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Rotace	rotace	k1gFnSc1	rotace
je	být	k5eAaImIp3nS	být
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
planety	planeta	k1gFnSc2	planeta
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Klesá	klesat	k5eAaImIp3nS	klesat
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
vykoná	vykonat	k5eAaPmIp3nS	vykonat
jednu	jeden	k4xCgFnSc4	jeden
otočku	otočka	k1gFnSc4	otočka
za	za	k7c4	za
10	[number]	k4	10
h	h	k?	h
a	a	k8xC	a
14	[number]	k4	14
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
například	například	k6eAd1	například
na	na	k7c4	na
57	[number]	k4	57
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
trvá	trvat	k5eAaImIp3nS	trvat
jedna	jeden	k4xCgFnSc1	jeden
otočka	otočka	k1gFnSc1	otočka
již	již	k6eAd1	již
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
7,5	[number]	k4	7,5
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Saturnovy	Saturnův	k2eAgInPc1d1	Saturnův
prstence	prstenec	k1gInPc1	prstenec
mají	mít	k5eAaImIp3nP	mít
celkový	celkový	k2eAgInSc4d1	celkový
průměr	průměr	k1gInSc4	průměr
420	[number]	k4	420
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tlusté	tlustý	k2eAgFnPc1d1	tlustá
jsou	být	k5eAaImIp3nP	být
maximálně	maximálně	k6eAd1	maximálně
jen	jen	k9	jen
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc1d1	tvořen
ledovými	ledový	k2eAgInPc7d1	ledový
úlomky	úlomek	k1gInPc7	úlomek
<g/>
,	,	kIx,	,
prachem	prach	k1gInSc7	prach
<g/>
,	,	kIx,	,
kamením	kamení	k1gNnSc7	kamení
a	a	k8xC	a
balvany	balvan	k1gInPc7	balvan
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
průměr	průměr	k1gInSc1	průměr
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
několik	několik	k4yIc1	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prstenci	prstenec	k1gInPc7	prstenec
leží	ležet	k5eAaImIp3nS	ležet
dráhy	dráha	k1gFnSc2	dráha
nejvnitřnějších	vnitřní	k2eAgInPc2d3	nejvnitřnější
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Pan	Pan	k1gMnSc1	Pan
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
mezeře	mezera	k1gFnSc6	mezera
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
Enckeho	Encke	k1gMnSc4	Encke
dělení	dělení	k1gNnSc2	dělení
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
části	část	k1gFnSc6	část
prstence	prstenec	k1gInSc2	prstenec
A.	A.	kA	A.
Jiný	jiný	k2eAgInSc4d1	jiný
měsíc	měsíc	k1gInSc4	měsíc
Atlas	Atlas	k1gInSc1	Atlas
obíhá	obíhat	k5eAaImIp3nS	obíhat
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
prstence	prstenec	k1gInSc2	prstenec
A	A	kA	A
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Prometheus	Prometheus	k1gMnSc1	Prometheus
a	a	k8xC	a
Pandora	Pandora	k1gFnSc1	Pandora
obíhají	obíhat	k5eAaImIp3nP	obíhat
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
prstence	prstenec	k1gInSc2	prstenec
F.	F.	kA	F.
Některé	některý	k3yIgInPc4	některý
měsíce	měsíc	k1gInPc4	měsíc
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
na	na	k7c6	na
shodných	shodný	k2eAgFnPc6d1	shodná
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
má	mít	k5eAaImIp3nS	mít
nejvýraznější	výrazný	k2eAgFnSc4d3	nejvýraznější
a	a	k8xC	a
nejjasnější	jasný	k2eAgFnSc4d3	nejjasnější
soustavu	soustava	k1gFnSc4	soustava
prstenců	prstenec	k1gInPc2	prstenec
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
známé	známý	k2eAgInPc1d1	známý
jedině	jedině	k6eAd1	jedině
Saturnovy	Saturnův	k2eAgInPc1d1	Saturnův
prstence	prstenec	k1gInPc1	prstenec
a	a	k8xC	a
planeta	planeta	k1gFnSc1	planeta
Saturn	Saturn	k1gInSc1	Saturn
byla	být	k5eAaImAgFnS	být
těmito	tento	k3xDgFnPc7	tento
prstenci	prstenec	k1gInSc3	prstenec
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
nevýrazné	výrazný	k2eNgInPc1d1	nevýrazný
prstence	prstenec	k1gInPc1	prstenec
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
okolo	okolo	k7c2	okolo
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Prstence	prstenec	k1gInPc1	prstenec
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
drobných	drobný	k2eAgFnPc2d1	drobná
částeček	částečka	k1gFnPc2	částečka
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
od	od	k7c2	od
prachových	prachový	k2eAgNnPc2d1	prachové
zrnek	zrnko	k1gNnPc2	zrnko
až	až	k9	až
po	po	k7c4	po
objekty	objekt	k1gInPc4	objekt
velké	velký	k2eAgFnSc2d1	velká
desítky	desítka	k1gFnSc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kousky	kousek	k1gInPc4	kousek
hornin	hornina	k1gFnPc2	hornina
obohacené	obohacený	k2eAgInPc1d1	obohacený
kousky	kousek	k1gInPc1	kousek
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
částice	částice	k1gFnSc1	částice
obíhá	obíhat	k5eAaImIp3nS	obíhat
planetu	planeta	k1gFnSc4	planeta
samostatně	samostatně	k6eAd1	samostatně
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
při	při	k7c6	při
oběhu	oběh	k1gInSc6	oběh
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
Keplerovými	Keplerův	k2eAgInPc7d1	Keplerův
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejbližší	blízký	k2eAgFnPc1d3	nejbližší
částice	částice	k1gFnPc1	částice
obíhají	obíhat	k5eAaImIp3nP	obíhat
Saturn	Saturn	k1gInSc4	Saturn
nejrychleji	rychle	k6eAd3	rychle
(	(	kIx(	(
<g/>
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
4,9	[number]	k4	4,9
hodiny	hodina	k1gFnSc2	hodina
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvzdálenější	vzdálený	k2eAgFnPc1d3	nejvzdálenější
pomaleji	pomale	k6eAd2	pomale
(	(	kIx(	(
<g/>
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
2	[number]	k4	2
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přelety	přelet	k1gInPc1	přelet
sond	sonda	k1gFnPc2	sonda
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgInPc1d1	hlavní
prstence	prstenec	k1gInPc1	prstenec
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
množstvím	množství	k1gNnSc7	množství
malých	malý	k2eAgInPc2d1	malý
jemných	jemný	k2eAgInPc2d1	jemný
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
prstenců	prstenec	k1gInPc2	prstenec
není	být	k5eNaImIp3nS	být
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
teorie	teorie	k1gFnSc1	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zformovaly	zformovat	k5eAaPmAgInP	zformovat
přirozeně	přirozeně	k6eAd1	přirozeně
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
materiálu	materiál	k1gInSc2	materiál
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
rozpadlý	rozpadlý	k2eAgInSc4d1	rozpadlý
vlivem	vliv	k1gInSc7	vliv
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgInSc1d1	poslední
prstenec	prstenec	k1gInSc1	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídký	řídký	k2eAgInSc1d1	řídký
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgInPc4d1	klasický
<g/>
"	"	kIx"	"
prstence	prstenec	k1gInPc4	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Spitzerovým	Spitzerův	k2eAgInSc7d1	Spitzerův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jemných	jemný	k2eAgFnPc2d1	jemná
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jen	jen	k9	jen
málo	málo	k6eAd1	málo
odrážejí	odrážet	k5eAaImIp3nP	odrážet
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
extrémně	extrémně	k6eAd1	extrémně
nízkou	nízký	k2eAgFnSc7d1	nízká
hustotou	hustota	k1gFnSc7	hustota
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
klasických	klasický	k2eAgInPc6d1	klasický
dalekohledech	dalekohled	k1gInPc6	dalekohled
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
materiál	materiál	k1gInSc4	materiál
vytržený	vytržený	k2eAgInSc4d1	vytržený
z	z	k7c2	z
měsíce	měsíc	k1gInSc2	měsíc
Phoebe	Phoeb	k1gInSc5	Phoeb
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgInPc4d1	klasický
<g/>
"	"	kIx"	"
prstence	prstenec	k1gInPc1	prstenec
dělí	dělit	k5eAaImIp3nP	dělit
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
D	D	kA	D
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
a	a	k8xC	a
E.	E.	kA	E.
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prstence	prstenec	k1gInPc1	prstenec
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděleny	oddělit	k5eAaPmNgInP	oddělit
mezerou	mezera	k1gFnSc7	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenější	vzdálený	k2eAgFnSc1d3	nejvzdálenější
část	část	k1gFnSc1	část
systému	systém	k1gInSc2	systém
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
prstenců	prstenec	k1gInPc2	prstenec
viditelných	viditelný	k2eAgInPc2d1	viditelný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
tvoří	tvořit	k5eAaImIp3nS	tvořit
prstenec	prstenec	k1gInSc1	prstenec
A.	A.	kA	A.
Prstenec	prstenec	k1gInSc1	prstenec
A	a	k9	a
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
nejširšího	široký	k2eAgInSc2d3	nejširší
a	a	k8xC	a
nejjasnějšího	jasný	k2eAgInSc2d3	nejjasnější
prstence	prstenec	k1gInSc2	prstenec
B	B	kA	B
oddělen	oddělit	k5eAaPmNgMnS	oddělit
tmavou	tmavý	k2eAgFnSc7d1	tmavá
mezerou	mezera	k1gFnSc7	mezera
širokou	široký	k2eAgFnSc4d1	široká
4500	[number]	k4	4500
km	km	kA	km
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Cassiniho	Cassini	k1gMnSc2	Cassini
dělení	dělení	k1gNnSc2	dělení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
v	v	k7c6	v
dalekohledu	dalekohled	k1gInSc6	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
alespoň	alespoň	k9	alespoň
7,5	[number]	k4	7,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
částečně	částečně	k6eAd1	částečně
průhledný	průhledný	k2eAgInSc1d1	průhledný
prstenec	prstenec	k1gInSc1	prstenec
C.	C.	kA	C.
Slabší	slabý	k2eAgInPc1d2	slabší
prstence	prstenec	k1gInPc1	prstenec
D	D	kA	D
a	a	k8xC	a
F	F	kA	F
leží	ležet	k5eAaImIp3nS	ležet
uvnitř	uvnitř	k6eAd1	uvnitř
a	a	k8xC	a
vně	vně	k6eAd1	vně
viditelných	viditelný	k2eAgInPc2d1	viditelný
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc4d1	jiný
dva	dva	k4xCgInPc4	dva
prstence	prstenec	k1gInPc4	prstenec
G	G	kA	G
a	a	k8xC	a
E	E	kA	E
leží	ležet	k5eAaImIp3nS	ležet
za	za	k7c7	za
prstencem	prstenec	k1gInSc7	prstenec
F.	F.	kA	F.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
objevila	objevit	k5eAaPmAgFnS	objevit
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
náznaky	náznak	k1gInPc1	náznak
dalších	další	k2eAgInPc2d1	další
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dostaly	dostat	k5eAaPmAgInP	dostat
předběžná	předběžný	k2eAgNnPc1d1	předběžné
označení	označení	k1gNnPc1	označení
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
S1	S1	k1gFnPc2	S1
a	a	k8xC	a
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
mezerách	mezera	k1gFnPc6	mezera
mezi	mezi	k7c7	mezi
prstenci	prstenec	k1gInPc7	prstenec
však	však	k9	však
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
prázdný	prázdný	k2eAgInSc1d1	prázdný
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
vyplněny	vyplnit	k5eAaPmNgInP	vyplnit
řadou	řada	k1gFnSc7	řada
slabých	slabý	k2eAgInPc2d1	slabý
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
komplex	komplex	k1gInSc1	komplex
prstenců	prstenec	k1gInPc2	prstenec
zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
000	[number]	k4	000
km	km	kA	km
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
jen	jen	k9	jen
okolo	okolo	k7c2	okolo
stovek	stovka	k1gFnPc2	stovka
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
prstence	prstenec	k1gInPc1	prstenec
tenčí	tenký	k2eAgInPc1d2	tenčí
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc1d1	objevený
řídký	řídký	k2eAgInSc1d1	řídký
prstenec	prstenec	k1gInSc1	prstenec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
6	[number]	k4	6
až	až	k9	až
12	[number]	k4	12
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
od	od	k7c2	od
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
tloušťka	tloušťka	k1gFnSc1	tloušťka
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
<g/>
×	×	k?	×
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
průměr	průměr	k1gInSc4	průměr
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
prstenců	prstenec	k1gInPc2	prstenec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
pozemského	pozemský	k2eAgInSc2d1	pozemský
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíce	měsíc	k1gInSc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
podzimu	podzim	k1gInSc3	podzim
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
známo	znám	k2eAgNnSc1d1	známo
62	[number]	k4	62
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
53	[number]	k4	53
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
a	a	k8xC	a
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
má	mít	k5eAaImIp3nS	mít
provizorní	provizorní	k2eAgNnSc4d1	provizorní
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgInSc1d3	Nejbližší
objevený	objevený	k2eAgInSc1d1	objevený
měsíc	měsíc	k1gInSc1	měsíc
Pan	Pan	k1gMnSc1	Pan
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
133	[number]	k4	133
583	[number]	k4	583
km	km	kA	km
<g/>
,	,	kIx,	,
nejvzdálenější	vzdálený	k2eAgInSc1d3	nejvzdálenější
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
Ymir	Ymir	k1gInSc4	Ymir
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
23	[number]	k4	23
100	[number]	k4	100
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
nebo	nebo	k8xC	nebo
6	[number]	k4	6
největších	veliký	k2eAgInPc2d3	veliký
měsíců	měsíc	k1gInPc2	měsíc
má	mít	k5eAaImIp3nS	mít
kulatý	kulatý	k2eAgInSc1d1	kulatý
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
zlepšování	zlepšování	k1gNnSc2	zlepšování
pozorovacích	pozorovací	k2eAgFnPc2d1	pozorovací
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
neustále	neustále	k6eAd1	neustále
přibývají	přibývat	k5eAaImIp3nP	přibývat
nalezené	nalezený	k2eAgInPc4d1	nalezený
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
lety	let	k1gInPc4	let
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc3	jejich
počet	počet	k1gInSc1	počet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
měsícem	měsíc	k1gInSc7	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
Titan	titan	k1gInSc1	titan
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
5	[number]	k4	5
150	[number]	k4	150
km	km	kA	km
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
Merkur	Merkur	k1gInSc1	Merkur
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
hustou	hustý	k2eAgFnSc7d1	hustá
atmosférou	atmosféra	k1gFnSc7	atmosféra
složenou	složený	k2eAgFnSc4d1	složená
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
molekulárního	molekulární	k2eAgInSc2d1	molekulární
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Ganymedovi	Ganymed	k1gMnSc6	Ganymed
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
měsícem	měsíc	k1gInSc7	měsíc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
měsícem	měsíc	k1gInSc7	měsíc
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
minimálně	minimálně	k6eAd1	minimálně
jedno	jeden	k4xCgNnSc1	jeden
jezero	jezero	k1gNnSc1	jezero
tekutých	tekutý	k2eAgInPc2d1	tekutý
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
asi	asi	k9	asi
–	–	k?	–
<g/>
178	[number]	k4	178
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
160	[number]	k4	160
kPa	kPa	k?	kPa
<g/>
.	.	kIx.	.
</s>
<s>
Titan	titan	k1gInSc1	titan
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
druhým	druhý	k4xOgInSc7	druhý
měsícem	měsíc	k1gInSc7	měsíc
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
přistála	přistát	k5eAaPmAgFnS	přistát
evropsko-americká	evropskomerický	k2eAgFnSc1d1	evropsko-americká
sonda	sonda	k1gFnSc1	sonda
–	–	k?	–
Huygens	Huygens	k1gInSc1	Huygens
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
měsíc	měsíc	k1gInSc1	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
Rhea	Rhea	k1gFnSc1	Rhea
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc4d1	známý
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
bez	bez	k7c2	bez
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
a	a	k8xC	a
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
kamenité	kamenitý	k2eAgNnSc1d1	kamenité
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Japetem	Japet	k1gMnSc7	Japet
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
i	i	k9	i
pomocí	pomoc	k1gFnSc7	pomoc
malých	malý	k2eAgInPc2d1	malý
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Japetus	Japetus	k1gInSc1	Japetus
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
světlá	světlý	k2eAgFnSc1d1	světlá
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
tmavá	tmavý	k2eAgFnSc1d1	tmavá
a	a	k8xC	a
kolem	kolem	k7c2	kolem
rovníku	rovník	k1gInSc2	rovník
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
vysoký	vysoký	k2eAgInSc1d1	vysoký
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
jev	jev	k1gInSc1	jev
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
i	i	k9	i
další	další	k2eAgInSc4d1	další
velký	velký	k2eAgInSc4d1	velký
měsíc	měsíc	k1gInSc4	měsíc
Dione	Dion	k1gInSc5	Dion
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgMnSc2	který
je	být	k5eAaImIp3nS	být
odrazivost	odrazivost	k1gFnSc4	odrazivost
polokoule	polokoule	k1gFnSc2	polokoule
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jeho	on	k3xPp3gInSc2	on
pohybu	pohyb	k1gInSc2	pohyb
o	o	k7c4	o
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
odrazivost	odrazivost	k1gFnSc1	odrazivost
druhé	druhý	k4xOgFnSc2	druhý
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
Mimas	Mimasa	k1gFnPc2	Mimasa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obrovský	obrovský	k2eAgInSc1d1	obrovský
kráter	kráter	k1gInSc1	kráter
Herschel	Herschela	k1gFnPc2	Herschela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
impaktním	impaktní	k2eAgInPc3d1	impaktní
kráterům	kráter	k1gInPc3	kráter
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Enceladus	Enceladus	k1gInSc1	Enceladus
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
512	[number]	k4	512
km	km	kA	km
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgNnSc1d3	veliký
albedo	albedo	k1gNnSc1	albedo
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
měsíců	měsíc	k1gInPc2	měsíc
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
sopky	sopka	k1gFnPc1	sopka
namísto	namísto	k7c2	namísto
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
magmatu	magma	k1gNnSc2	magma
chrlí	chrlit	k5eAaImIp3nP	chrlit
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
vlivem	vliv	k1gInSc7	vliv
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
na	na	k7c4	na
led	led	k1gInSc4	led
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k6eAd1	teplo
způsobující	způsobující	k2eAgInSc1d1	způsobující
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
vzniká	vznikat	k5eAaImIp3nS	vznikat
vlivem	vlivem	k7c2	vlivem
silných	silný	k2eAgInPc2d1	silný
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
okolních	okolní	k2eAgInPc2d1	okolní
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
mateřské	mateřský	k2eAgFnSc2d1	mateřská
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Enceladu	Encelad	k1gInSc2	Encelad
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídká	řídký	k2eAgFnSc1d1	řídká
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
též	též	k9	též
Tethys	Tethys	k1gInSc1	Tethys
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
malými	malý	k2eAgInPc7d1	malý
měsíci	měsíc	k1gInPc7	měsíc
Telesto	Telesta	k1gMnSc5	Telesta
a	a	k8xC	a
Calypso	Calypsa	k1gFnSc5	Calypsa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
malé	malý	k2eAgInPc1d1	malý
měsíce	měsíc	k1gInPc1	měsíc
obíhají	obíhat	k5eAaImIp3nP	obíhat
v	v	k7c6	v
libračních	librační	k2eAgInPc6d1	librační
bodech	bod	k1gInPc6	bod
Tethysu	Tethys	k1gInSc2	Tethys
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
měsíce	měsíc	k1gInPc1	měsíc
Helene	Helen	k1gMnSc5	Helen
a	a	k8xC	a
Dione	Dion	k1gMnSc5	Dion
mají	mít	k5eAaImIp3nP	mít
společnou	společný	k2eAgFnSc4d1	společná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc2	dvojice
měsíců	měsíc	k1gInPc2	měsíc
Prometheus	Prometheus	k1gMnSc1	Prometheus
a	a	k8xC	a
Pandora	Pandora	k1gFnSc1	Pandora
obíhá	obíhat	k5eAaImIp3nS	obíhat
z	z	k7c2	z
opačných	opačný	k2eAgFnPc2d1	opačná
stran	strana	k1gFnPc2	strana
prstence	prstenec	k1gInSc2	prstenec
F	F	kA	F
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
gravitační	gravitační	k2eAgNnSc1d1	gravitační
působení	působení	k1gNnSc1	působení
udržuje	udržovat	k5eAaImIp3nS	udržovat
částice	částice	k1gFnSc1	částice
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
znali	znát	k5eAaImAgMnP	znát
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
historicky	historicky	k6eAd1	historicky
doložené	doložený	k2eAgNnSc1d1	doložené
pozorování	pozorování	k1gNnSc1	pozorování
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
pochází	pocházet	k5eAaImIp3nS	pocházet
do	do	k7c2	do
období	období	k1gNnSc2	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
650	[number]	k4	650
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dochovaném	dochovaný	k2eAgInSc6d1	dochovaný
textu	text	k1gInSc6	text
je	být	k5eAaImIp3nS	být
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
zákrytu	zákryt	k1gInSc6	zákryt
planety	planeta	k1gFnSc2	planeta
Měsícem	měsíc	k1gInSc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
modelech	model	k1gInPc6	model
nebeské	nebeský	k2eAgFnSc2d1	nebeská
sféry	sféra	k1gFnSc2	sféra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
geocentrické	geocentrický	k2eAgFnPc1d1	geocentrická
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejvzdálenější	vzdálený	k2eAgFnSc7d3	nejvzdálenější
planetou	planeta	k1gFnSc7	planeta
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
obíhal	obíhat	k5eAaImAgMnS	obíhat
ji	on	k3xPp3gFnSc4	on
mezi	mezi	k7c7	mezi
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
nejvzdálenější	vzdálený	k2eAgFnSc7d3	nejvzdálenější
sférou	sféra	k1gFnSc7	sféra
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
se	se	k3xPyFc4	se
pozorováním	pozorování	k1gNnSc7	pozorování
Saturnu	Saturn	k1gInSc2	Saturn
zabýval	zabývat	k5eAaImAgMnS	zabývat
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc6	Galilei
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedokonalé	dokonalý	k2eNgFnSc3d1	nedokonalá
optice	optika	k1gFnSc3	optika
použitých	použitý	k2eAgInPc2d1	použitý
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
pouze	pouze	k6eAd1	pouze
32	[number]	k4	32
<g/>
násobné	násobný	k2eAgNnSc1d1	násobné
zvětšení	zvětšení	k1gNnSc3	zvětšení
<g/>
,	,	kIx,	,
neodhalil	odhalit	k5eNaPmAgInS	odhalit
podstatu	podstata	k1gFnSc4	podstata
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
prstenců	prstenec	k1gInPc2	prstenec
a	a	k8xC	a
pokládal	pokládat	k5eAaImAgMnS	pokládat
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
dvě	dva	k4xCgNnPc4	dva
samostatná	samostatný	k2eAgNnPc4d1	samostatné
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
doprovázející	doprovázející	k2eAgFnSc4d1	doprovázející
vlastní	vlastní	k2eAgFnSc4d1	vlastní
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
tedy	tedy	k9	tedy
Saturna	Saturn	k1gMnSc4	Saturn
za	za	k7c2	za
trojplanetu	trojplanet	k1gInSc2	trojplanet
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
pozorováními	pozorování	k1gNnPc7	pozorování
však	však	k9	však
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
oběžnice	oběžnice	k1gFnPc1	oběžnice
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
planety	planeta	k1gFnSc2	planeta
pravidelně	pravidelně	k6eAd1	pravidelně
mizí	mizet	k5eAaImIp3nP	mizet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
měnícím	měnící	k2eAgMnSc7d1	měnící
se	se	k3xPyFc4	se
sklonem	sklon	k1gInSc7	sklon
prstenců	prstenec	k1gInPc2	prstenec
vůči	vůči	k7c3	vůči
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zjištění	zjištění	k1gNnSc1	zjištění
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
Galileo	Galilea	k1gFnSc5	Galilea
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
schopen	schopen	k2eAgMnSc1d1	schopen
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1656	[number]	k4	1656
přinesl	přinést	k5eAaPmAgInS	přinést
správné	správný	k2eAgNnSc4d1	správné
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
jevů	jev	k1gInPc2	jev
holandský	holandský	k2eAgMnSc1d1	holandský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Christiaan	Christiaana	k1gFnPc2	Christiaana
Huygens	Huygens	k1gInSc1	Huygens
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saturn	Saturn	k1gInSc1	Saturn
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
kruhovým	kruhový	k2eAgInSc7d1	kruhový
prstencem	prstenec	k1gInSc7	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
závěr	závěr	k1gInSc1	závěr
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
nových	nový	k2eAgNnPc6d1	nové
pozorováních	pozorování	k1gNnPc6	pozorování
planety	planeta	k1gFnSc2	planeta
započatých	započatý	k2eAgMnPc2d1	započatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1655	[number]	k4	1655
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
s	s	k7c7	s
50	[number]	k4	50
<g/>
násobným	násobný	k2eAgNnSc7d1	násobné
zvětšením	zvětšení	k1gNnSc7	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
největší	veliký	k2eAgInSc1d3	veliký
Saturnův	Saturnův	k2eAgInSc1d1	Saturnův
měsíc	měsíc	k1gInSc1	měsíc
Titan	titan	k1gInSc1	titan
a	a	k8xC	a
určil	určit	k5eAaPmAgInS	určit
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
jeho	jeho	k3xOp3gFnSc4	jeho
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
objeveny	objeven	k2eAgInPc1d1	objeven
další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
měsíce	měsíc	k1gInPc1	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Japetus	Japetus	k1gInSc4	Japetus
<g/>
,	,	kIx,	,
Rhea	Rhea	k1gFnSc1	Rhea
<g/>
,	,	kIx,	,
Tethys	Tethys	k1gInSc1	Tethys
a	a	k8xC	a
Dione	Dion	k1gMnSc5	Dion
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saturnův	Saturnův	k2eAgInSc1d1	Saturnův
prstenec	prstenec	k1gInSc1	prstenec
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
vzájemně	vzájemně	k6eAd1	vzájemně
oddělených	oddělený	k2eAgInPc2d1	oddělený
prstenců	prstenec	k1gInPc2	prstenec
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Giovanni	Giovanň	k1gMnSc3	Giovanň
Domenico	Domenico	k6eAd1	Domenico
Cassini	Cassin	k2eAgMnPc1d1	Cassin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1675	[number]	k4	1675
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
objevená	objevený	k2eAgFnSc1d1	objevená
mezera	mezera	k1gFnSc1	mezera
mezi	mezi	k7c7	mezi
prstenci	prstenec	k1gInPc7	prstenec
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
označuje	označovat	k5eAaImIp3nS	označovat
termínem	termín	k1gInSc7	termín
Cassiniho	Cassini	k1gMnSc2	Cassini
dělení	dělení	k1gNnSc2	dělení
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
změřil	změřit	k5eAaPmAgInS	změřit
William	William	k1gInSc1	William
Herschel	Herschela	k1gFnPc2	Herschela
zploštění	zploštění	k1gNnSc2	zploštění
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
rovníkového	rovníkový	k2eAgInSc2d1	rovníkový
průměru	průměr	k1gInSc2	průměr
k	k	k7c3	k
polárnímu	polární	k2eAgInSc3d1	polární
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
na	na	k7c4	na
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
technika	technika	k1gFnSc1	technika
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
bílé	bílý	k2eAgFnPc4d1	bílá
skvrny	skvrna	k1gFnPc4	skvrna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
všimli	všimnout	k5eAaPmAgMnP	všimnout
Johann	Johann	k1gNnSc4	Johann
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Schröter	Schröter	k1gMnSc1	Schröter
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
asistent	asistent	k1gMnSc1	asistent
Karl	Karl	k1gMnSc1	Karl
Ludwig	Ludwig	k1gMnSc1	Ludwig
Harding	Harding	k1gInSc4	Harding
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Brém	Brémy	k1gFnPc2	Brémy
<g/>
.	.	kIx.	.
</s>
<s>
Skvrny	skvrna	k1gFnPc1	skvrna
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
příležitostně	příležitostně	k6eAd1	příležitostně
pozorovány	pozorován	k2eAgInPc1d1	pozorován
i	i	k9	i
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
novějších	nový	k2eAgNnPc2d2	novější
pozorování	pozorování	k1gNnPc2	pozorování
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
přibližně	přibližně	k6eAd1	přibližně
každých	každý	k3xTgNnPc2	každý
27	[number]	k4	27
až	až	k8xS	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
Saturnu	Saturn	k1gInSc2	Saturn
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
29,5	[number]	k4	29,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
také	také	k6eAd1	také
prokázal	prokázat	k5eAaPmAgMnS	prokázat
J.	J.	kA	J.
E.	E.	kA	E.
Keeler	Keeler	k1gMnSc1	Keeler
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prstence	prstenec	k1gInPc1	prstenec
nejsou	být	k5eNaImIp3nP	být
jednolité	jednolitý	k2eAgInPc1d1	jednolitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
nesmírného	smírný	k2eNgInSc2d1	nesmírný
počtu	počet	k1gInSc2	počet
malých	malý	k2eAgFnPc2d1	malá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
i	i	k9	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
tak	tak	k6eAd1	tak
jasný	jasný	k2eAgMnSc1d1	jasný
jako	jako	k8xC	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
konstelaci	konstelace	k1gFnSc6	konstelace
od	od	k7c2	od
1,4	[number]	k4	1,4
do	do	k7c2	do
–	–	k?	–
<g/>
0,4	[number]	k4	0,4
magnitudy	magnitudy	k6eAd1	magnitudy
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
nejjasnější	jasný	k2eAgFnPc1d3	nejjasnější
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hvězd	hvězda	k1gFnPc2	hvězda
ale	ale	k8xC	ale
Saturn	Saturn	k1gInSc1	Saturn
jako	jako	k8xC	jako
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
nebliká	blikat	k5eNaImIp3nS	blikat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
klidné	klidný	k2eAgNnSc1d1	klidné
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jasnost	jasnost	k1gFnSc1	jasnost
vůči	vůči	k7c3	vůči
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
také	také	k9	také
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
sklon	sklon	k1gInSc1	sklon
prstenců	prstenec	k1gInPc2	prstenec
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
se	se	k3xPyFc4	se
od	od	k7c2	od
ekliptiky	ekliptika	k1gFnSc2	ekliptika
nikdy	nikdy	k6eAd1	nikdy
nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
2,5	[number]	k4	2,5
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
50	[number]	k4	50
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
při	při	k7c6	při
horní	horní	k2eAgFnSc6d1	horní
kulminaci	kulminace	k1gFnSc6	kulminace
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
vystoupat	vystoupat	k5eAaPmF	vystoupat
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
na	na	k7c4	na
66	[number]	k4	66
<g/>
°	°	k?	°
a	a	k8xC	a
klesnout	klesnout	k5eAaPmF	klesnout
méně	málo	k6eAd2	málo
než	než	k8xS	než
14	[number]	k4	14
<g/>
°	°	k?	°
pod	pod	k7c4	pod
obzor	obzor	k1gInSc4	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
nejvzdálenější	vzdálený	k2eAgFnSc4d3	nejvzdálenější
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
Uranu	Uran	k1gInSc2	Uran
se	se	k3xPyFc4	se
však	však	k9	však
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
pozorovatelnosti	pozorovatelnost	k1gFnSc2	pozorovatelnost
a	a	k8xC	a
za	za	k7c2	za
ideálních	ideální	k2eAgFnPc2d1	ideální
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
Saturn	Saturn	k1gInSc4	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Prstence	prstenec	k1gInPc1	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
nejsou	být	k5eNaImIp3nP	být
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
viditelné	viditelný	k2eAgFnSc2d1	viditelná
<g/>
,	,	kIx,	,
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
menším	malý	k2eAgInSc7d2	menší
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
příhodně	příhodně	k6eAd1	příhodně
orientované	orientovaný	k2eAgInPc4d1	orientovaný
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
Saturnem	Saturn	k1gInSc7	Saturn
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dalekohledem	dalekohled	k1gInSc7	dalekohled
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
viditelný	viditelný	k2eAgMnSc1d1	viditelný
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vhodném	vhodný	k2eAgInSc6d1	vhodný
sklonu	sklon	k1gInSc6	sklon
i	i	k9	i
stín	stín	k1gInSc4	stín
prstenců	prstenec	k1gInPc2	prstenec
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
povrchu	povrch	k1gInSc6	povrch
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
atmosférické	atmosférický	k2eAgInPc4d1	atmosférický
pásy	pás	k1gInPc4	pás
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
bílé	bílý	k2eAgFnPc4d1	bílá
jasné	jasný	k2eAgFnPc4d1	jasná
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
nejpomaleji	pomale	k6eAd3	pomale
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
viditelných	viditelný	k2eAgInPc2d1	viditelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důsledek	důsledek	k1gInSc1	důsledek
třetího	třetí	k4xOgInSc2	třetí
Keplerova	Keplerův	k2eAgInSc2d1	Keplerův
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
tak	tak	k9	tak
i	i	k9	i
Saturn	Saturn	k1gMnSc1	Saturn
někdy	někdy	k6eAd1	někdy
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pohybu	pohyb	k1gInSc6	pohyb
na	na	k7c6	na
hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
pozadí	pozadí	k1gNnSc6	pozadí
"	"	kIx"	"
<g/>
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
"	"	kIx"	"
a	a	k8xC	a
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
i	i	k9	i
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
nerovnoměrnosti	nerovnoměrnost	k1gFnPc1	nerovnoměrnost
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
sčítáním	sčítání	k1gNnSc7	sčítání
pohybu	pohyb	k1gInSc2	pohyb
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Saturnova	Saturnův	k2eAgInSc2d1	Saturnův
téměř	téměř	k6eAd1	téměř
rovnoměrného	rovnoměrný	k2eAgInSc2d1	rovnoměrný
oběhu	oběh	k1gInSc2	oběh
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Lva	lev	k1gInSc2	lev
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
2009	[number]	k4	2009
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Panny	Panna	k1gFnSc2	Panna
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
i	i	k8xC	i
oblast	oblast	k1gFnSc1	oblast
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
amatérských	amatérský	k2eAgNnPc2d1	amatérské
a	a	k8xC	a
profesionálních	profesionální	k2eAgNnPc2d1	profesionální
pozorování	pozorování	k1gNnPc2	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Saturn	Saturn	k1gInSc1	Saturn
také	také	k9	také
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
hlavně	hlavně	k9	hlavně
atmosférické	atmosférický	k2eAgFnPc4d1	atmosférická
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
polární	polární	k2eAgFnPc4d1	polární
záře	zář	k1gFnPc4	zář
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
několik	několik	k4yIc4	několik
nových	nový	k2eAgInPc2d1	nový
malých	malý	k2eAgInPc2d1	malý
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
určit	určit	k5eAaPmF	určit
maximální	maximální	k2eAgFnSc4d1	maximální
tloušťku	tloušťka	k1gFnSc4	tloušťka
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zákrytu	zákryt	k1gInSc6	zákryt
hvězdy	hvězda	k1gFnSc2	hvězda
GSCC5249-01240	GSCC5249-01240	k1gFnSc2	GSCC5249-01240
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
až	až	k9	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
určit	určit	k5eAaPmF	určit
podrobnější	podrobný	k2eAgFnSc4d2	podrobnější
strukturu	struktura	k1gFnSc4	struktura
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
maximálním	maximální	k2eAgInSc6d1	maximální
sklonu	sklon	k1gInSc6	sklon
prstenců	prstenec	k1gInPc2	prstenec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
pořídila	pořídit	k5eAaPmAgFnS	pořídit
kamera	kamera	k1gFnSc1	kamera
Wide	Wid	k1gFnSc2	Wid
Field	Fielda	k1gFnPc2	Fielda
Planetary	Planetara	k1gFnSc2	Planetara
Camera	Camero	k1gNnSc2	Camero
2	[number]	k4	2
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
30	[number]	k4	30
snímků	snímek	k1gInPc2	snímek
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
pásmu	pásmo	k1gNnSc6	pásmo
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
snímky	snímek	k1gInPc7	snímek
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
doposud	doposud	k6eAd1	doposud
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
spektrálního	spektrální	k2eAgNnSc2d1	spektrální
pokrytí	pokrytí	k1gNnSc2	pokrytí
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
jejího	její	k3xOp3gNnSc2	její
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
snímky	snímek	k1gInPc1	snímek
umožnily	umožnit	k5eAaPmAgInP	umožnit
vědcům	vědec	k1gMnPc3	vědec
lépe	dobře	k6eAd2	dobře
studovat	studovat	k5eAaImF	studovat
dynamické	dynamický	k2eAgInPc4d1	dynamický
procesy	proces	k1gInPc4	proces
odehrávající	odehrávající	k2eAgInPc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
modely	model	k1gInPc4	model
sezónního	sezónní	k2eAgNnSc2d1	sezónní
chování	chování	k1gNnSc2	chování
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozemských	pozemský	k2eAgFnPc2d1	pozemská
observatoří	observatoř	k1gFnPc2	observatoř
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
Saturn	Saturn	k1gInSc4	Saturn
například	například	k6eAd1	například
Evropská	evropský	k2eAgFnSc1d1	Evropská
jižní	jižní	k2eAgFnSc1d1	jižní
observatoř	observatoř	k1gFnSc1	observatoř
a	a	k8xC	a
observatoř	observatoř	k1gFnSc1	observatoř
na	na	k7c4	na
Mauna	Mauno	k1gNnPc4	Mauno
Kea	Kea	k1gFnPc2	Kea
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2003	[number]	k4	2003
objevily	objevit	k5eAaPmAgInP	objevit
několik	několik	k4yIc4	několik
malých	malý	k2eAgInPc2d1	malý
nepravidelných	pravidelný	k2eNgInPc2d1	nepravidelný
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
astronomie	astronomie	k1gFnSc1	astronomie
čerpá	čerpat	k5eAaImIp3nS	čerpat
většinu	většina	k1gFnSc4	většina
detailních	detailní	k2eAgFnPc2d1	detailní
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
Saturnu	Saturn	k1gInSc6	Saturn
ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
pořízených	pořízený	k2eAgFnPc2d1	pořízená
kosmickými	kosmický	k2eAgFnPc7d1	kosmická
sondami	sonda	k1gFnPc7	sonda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prolétl	prolétnout	k5eAaPmAgInS	prolétnout
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Saturnu	Saturn	k1gInSc2	Saturn
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
dorazil	dorazit	k5eAaPmAgMnS	dorazit
po	po	k7c6	po
čtyř	čtyři	k4xCgFnPc2	čtyři
a	a	k8xC	a
půl	půl	k6eAd1	půl
roční	roční	k2eAgFnSc6d1	roční
cestě	cesta	k1gFnSc6	cesta
meziplanetárním	meziplanetární	k2eAgInSc7d1	meziplanetární
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
okolí	okolí	k1gNnSc2	okolí
začalo	začít	k5eAaPmAgNnS	začít
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
sonda	sonda	k1gFnSc1	sonda
provedla	provést	k5eAaPmAgFnS	provést
riskantní	riskantní	k2eAgFnSc7d1	riskantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
manévr	manévr	k1gInSc1	manévr
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1979	[number]	k4	1979
rovinou	rovina	k1gFnSc7	rovina
Saturnových	Saturnův	k2eAgInPc2d1	Saturnův
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
hrozila	hrozit	k5eAaImAgFnS	hrozit
srážka	srážka	k1gFnSc1	srážka
sondy	sonda	k1gFnSc2	sonda
a	a	k8xC	a
hmoty	hmota	k1gFnSc2	hmota
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
sonda	sonda	k1gFnSc1	sonda
Saturnu	Saturn	k1gInSc2	Saturn
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
na	na	k7c4	na
21	[number]	k4	21
400	[number]	k4	400
km	km	kA	km
nad	nad	k7c4	nad
oblast	oblast	k1gFnSc4	oblast
mraků	mrak	k1gInPc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
planety	planeta	k1gFnSc2	planeta
ukončila	ukončit	k5eAaPmAgFnS	ukončit
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
letu	let	k1gInSc6	let
do	do	k7c2	do
vnějších	vnější	k2eAgFnPc2d1	vnější
oblastí	oblast	k1gFnPc2	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
průzkumníky	průzkumník	k1gMnPc7	průzkumník
Saturnu	Saturn	k1gInSc2	Saturn
byly	být	k5eAaImAgFnP	být
sondy	sonda	k1gFnPc1	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
1	[number]	k4	1
a	a	k8xC	a
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
snímkovaly	snímkovat	k5eAaImAgInP	snímkovat
Saturn	Saturn	k1gInSc4	Saturn
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
Voyager	Voyager	k1gInSc4	Voyager
1	[number]	k4	1
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
přístroje	přístroj	k1gInPc1	přístroj
zkoumaly	zkoumat	k5eAaImAgInP	zkoumat
planetu	planeta	k1gFnSc4	planeta
již	již	k9	již
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přeletu	přelet	k1gInSc2	přelet
bylo	být	k5eAaImAgNnS	být
pořízeno	pořízen	k2eAgNnSc1d1	pořízeno
množství	množství	k1gNnSc1	množství
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přinesly	přinést	k5eAaPmAgFnP	přinést
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
získat	získat	k5eAaPmF	získat
snímky	snímek	k1gInPc4	snímek
měsíců	měsíc	k1gInPc2	měsíc
Mimas	Mimasa	k1gFnPc2	Mimasa
<g/>
,	,	kIx,	,
Tethys	Tethysa	k1gFnPc2	Tethysa
<g/>
,	,	kIx,	,
Dione	Dion	k1gMnSc5	Dion
<g/>
,	,	kIx,	,
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
,	,	kIx,	,
Rhea	Rhea	k1gFnSc1	Rhea
a	a	k8xC	a
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Titanu	titan	k1gInSc2	titan
pak	pak	k6eAd1	pak
sonda	sonda	k1gFnSc1	sonda
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
proletěla	proletět	k5eAaPmAgFnS	proletět
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
pouhých	pouhý	k2eAgInPc2d1	pouhý
6500	[number]	k4	6500
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
nasbírat	nasbírat	k5eAaPmF	nasbírat
základní	základní	k2eAgInPc4d1	základní
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
atmosféře	atmosféra	k1gFnSc6	atmosféra
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
teplotě	teplota	k1gFnSc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nejblíže	blízce	k6eAd3	blízce
dostala	dostat	k5eAaPmAgFnS	dostat
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
začala	začít	k5eAaPmAgFnS	začít
sonda	sonda	k1gFnSc1	sonda
zkoumat	zkoumat	k5eAaImF	zkoumat
horní	horní	k2eAgFnPc4d1	horní
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
radaru	radar	k1gInSc2	radar
<g/>
.	.	kIx.	.
</s>
<s>
Radarové	radarový	k2eAgNnSc1d1	radarové
měření	měření	k1gNnSc1	měření
přineslo	přinést	k5eAaPmAgNnS	přinést
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
hustotě	hustota	k1gFnSc6	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
oblastech	oblast	k1gFnPc6	oblast
atmosféry	atmosféra	k1gFnSc2	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
okolo	okolo	k7c2	okolo
7	[number]	k4	7
kPa	kPa	k?	kPa
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
<g/>
203	[number]	k4	203
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
nejnižších	nízký	k2eAgFnPc6d3	nejnižší
zkoumaných	zkoumaný	k2eAgFnPc6d1	zkoumaná
oblastech	oblast	k1gFnPc6	oblast
byl	být	k5eAaImAgMnS	být
tlaku	tlak	k1gInSc2	tlak
až	až	k8xS	až
120	[number]	k4	120
kPa	kPa	k?	kPa
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
<g/>
130	[number]	k4	130
°	°	k?	°
<g/>
C.	C.	kA	C.
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
chladnější	chladný	k2eAgFnPc1d2	chladnější
než	než	k8xS	než
jižní	jižní	k2eAgFnPc1d1	jižní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
vysvětleno	vysvětlit	k5eAaPmNgNnS	vysvětlit
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
sezónních	sezónní	k2eAgInPc2d1	sezónní
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
odeslat	odeslat	k5eAaPmF	odeslat
okolo	okolo	k7c2	okolo
16	[number]	k4	16
000	[number]	k4	000
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
raketa	raketa	k1gFnSc1	raketa
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
planetární	planetární	k2eAgFnSc4d1	planetární
sondu	sonda	k1gFnSc4	sonda
Cassini-Huygens	Cassini-Huygensa	k1gFnPc2	Cassini-Huygensa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
sonda	sonda	k1gFnSc1	sonda
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
být	být	k5eAaImF	být
navedena	navést	k5eAaPmNgNnP	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
od	od	k7c2	od
sondy	sonda	k1gFnSc2	sonda
oddělil	oddělit	k5eAaPmAgInS	oddělit
přistávací	přistávací	k2eAgInSc1d1	přistávací
modul	modul	k1gInSc1	modul
Huygens	Huygens	k1gInSc1	Huygens
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
a	a	k8xC	a
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
Evropskou	evropský	k2eAgFnSc7d1	Evropská
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
agenturou	agentura	k1gFnSc7	agentura
pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
začal	začít	k5eAaPmAgInS	začít
modul	modul	k1gInSc1	modul
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
třítýdenní	třítýdenní	k2eAgFnSc4d1	třítýdenní
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
úspěšně	úspěšně	k6eAd1	úspěšně
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přistání	přistání	k1gNnSc2	přistání
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
translační	translační	k2eAgFnSc1d1	translační
stanice	stanice	k1gFnSc1	stanice
pro	pro	k7c4	pro
signál	signál	k1gInSc4	signál
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
z	z	k7c2	z
přistávacího	přistávací	k2eAgInSc2d1	přistávací
modulu	modul	k1gInSc2	modul
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Modul	modul	k1gInSc1	modul
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
zmrzlém	zmrzlý	k2eAgInSc6d1	zmrzlý
povrchu	povrch	k1gInSc6	povrch
tvořeném	tvořený	k2eAgInSc6d1	tvořený
směsí	směs	k1gFnPc2	směs
křemičitanových	křemičitanový	k2eAgFnPc2d1	křemičitanová
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
tuhého	tuhý	k2eAgInSc2d1	tuhý
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
očekávání	očekávání	k1gNnSc4	očekávání
dobře	dobře	k6eAd1	dobře
přežil	přežít	k5eAaPmAgMnS	přežít
přistávací	přistávací	k2eAgInSc4d1	přistávací
manévr	manévr	k1gInSc4	manévr
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
fungoval	fungovat	k5eAaImAgInS	fungovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
bylo	být	k5eAaImAgNnS	být
ale	ale	k8xC	ale
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
už	už	k6eAd1	už
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sonda	sonda	k1gFnSc1	sonda
zmizela	zmizet	k5eAaPmAgFnS	zmizet
za	za	k7c7	za
horizontem	horizont	k1gInSc7	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemá	mít	k5eNaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
jako	jako	k8xC	jako
terestrické	terestrický	k2eAgFnSc2d1	terestrická
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
planet	planeta	k1gFnPc2	planeta
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
případný	případný	k2eAgInSc1d1	případný
život	život	k1gInSc1	život
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
vznikat	vznikat	k5eAaImF	vznikat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
kapičky	kapička	k1gFnPc1	kapička
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
dostatek	dostatek	k1gInSc4	dostatek
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
prostředí	prostředí	k1gNnSc6	prostředí
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
žít	žít	k5eAaImF	žít
i	i	k9	i
vícebuněčné	vícebuněčný	k2eAgInPc4d1	vícebuněčný
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nenašly	najít	k5eNaPmAgInP	najít
žádné	žádný	k3yNgInPc1	žádný
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
žít	žít	k5eAaImF	žít
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
mracích	mrak	k1gInPc6	mrak
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
mraky	mrak	k1gInPc1	mrak
přítomny	přítomen	k2eAgInPc1d1	přítomen
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
můžeme	moct	k5eAaImIp1nP	moct
předpokládat	předpokládat	k5eAaImF	předpokládat
podobnou	podobný	k2eAgFnSc4d1	podobná
situaci	situace	k1gFnSc4	situace
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
tělesa	těleso	k1gNnPc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
pro	pro	k7c4	pro
Saturn	Saturn	k1gInSc4	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
za	za	k7c4	za
možné	možný	k2eAgMnPc4d1	možný
kandidáty	kandidát	k1gMnPc4	kandidát
na	na	k7c4	na
mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
život	život	k1gInSc4	život
v	v	k7c6	v
Saturnově	Saturnův	k2eAgFnSc6d1	Saturnova
rodině	rodina	k1gFnSc6	rodina
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
měsíce	měsíc	k1gInPc1	měsíc
Titan	titan	k1gInSc1	titan
a	a	k8xC	a
Enceladus	Enceladus	k1gInSc1	Enceladus
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
Titanu	titan	k1gInSc2	titan
připomíná	připomínat	k5eAaImIp3nS	připomínat
složení	složení	k1gNnSc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
stádiu	stádium	k1gNnSc6	stádium
vzniku	vznik	k1gInSc3	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
též	též	k9	též
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
výskytu	výskyt	k1gInSc2	výskyt
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
zde	zde	k6eAd1	zde
mohly	moct	k5eAaImAgFnP	moct
přežívat	přežívat	k5eAaImF	přežívat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
sondy	sonda	k1gFnSc2	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
však	však	k9	však
Fransois	Fransois	k1gInSc4	Fransois
Raulin	Raulina	k1gFnPc2	Raulina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
expertů	expert	k1gMnPc2	expert
projektu	projekt	k1gInSc2	projekt
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc4	život
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nepravděpodobný	pravděpodobný	k2eNgInSc1d1	nepravděpodobný
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Enceladus	Enceladus	k1gInSc1	Enceladus
vědce	vědec	k1gMnSc2	vědec
překvapil	překvapit	k5eAaPmAgInS	překvapit
přítomností	přítomnost	k1gFnSc7	přítomnost
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
chrlí	chrlit	k5eAaImIp3nP	chrlit
gejzíry	gejzír	k1gInPc1	gejzír
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
dovolovat	dovolovat	k5eAaImF	dovolovat
existenci	existence	k1gFnSc4	existence
primitivního	primitivní	k2eAgInSc2d1	primitivní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Saturnus	Saturnus	k1gMnSc1	Saturnus
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
starý	starý	k2eAgMnSc1d1	starý
římský	římský	k2eAgMnSc1d1	římský
bůh	bůh	k1gMnSc1	bůh
rolnictví	rolnictví	k1gNnSc2	rolnictví
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ztotožňoval	ztotožňovat	k5eAaImAgMnS	ztotožňovat
s	s	k7c7	s
řeckým	řecký	k2eAgMnSc7d1	řecký
Kronem	Kron	k1gMnSc7	Kron
<g/>
,	,	kIx,	,
bohem	bůh	k1gMnSc7	bůh
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Krona	Kron	k1gMnSc2	Kron
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pro	pro	k7c4	pro
požírání	požírání	k1gNnSc4	požírání
vlastních	vlastní	k2eAgFnPc2d1	vlastní
dětí	dítě	k1gFnPc2	dítě
nebyl	být	k5eNaImAgInS	být
u	u	k7c2	u
starověkých	starověký	k2eAgMnPc2d1	starověký
Řeků	Řek	k1gMnPc2	Řek
příliš	příliš	k6eAd1	příliš
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Saturnus	Saturnus	k1gMnSc1	Saturnus
u	u	k7c2	u
Římanů	Říman	k1gMnPc2	Říman
velkou	velký	k2eAgFnSc4d1	velká
vážnost	vážnost	k1gFnSc4	vážnost
a	a	k8xC	a
úctu	úcta	k1gFnSc4	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mýtu	mýtus	k1gInSc2	mýtus
naučil	naučit	k5eAaPmAgMnS	naučit
lidi	člověk	k1gMnPc4	člověk
obdělávat	obdělávat	k5eAaImF	obdělávat
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
pěstovat	pěstovat	k5eAaImF	pěstovat
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
stavět	stavět	k5eAaImF	stavět
obydlí	obydlet	k5eAaPmIp3nS	obydlet
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
údajného	údajný	k2eAgNnSc2d1	údajné
panování	panování	k1gNnSc2	panování
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
zlatém	zlatý	k2eAgInSc6d1	zlatý
věku	věk	k1gInSc6	věk
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
slavnosti	slavnost	k1gFnPc1	slavnost
zvané	zvaný	k2eAgFnPc1d1	zvaná
saturnálie	saturnálie	k1gFnPc1	saturnálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
těchto	tento	k3xDgFnPc2	tento
slavností	slavnost	k1gFnPc2	slavnost
dostávali	dostávat	k5eAaImAgMnP	dostávat
otroci	otrok	k1gMnPc1	otrok
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
zlatém	zlatý	k2eAgInSc6d1	zlatý
věku	věk	k1gInSc6	věk
nebylo	být	k5eNaImAgNnS	být
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
ani	ani	k9	ani
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Saturnovi	Saturn	k1gMnSc3	Saturn
se	se	k3xPyFc4	se
po	po	k7c6	po
ztotožnění	ztotožnění	k1gNnSc6	ztotožnění
s	s	k7c7	s
Kronem	Kron	k1gMnSc7	Kron
začaly	začít	k5eAaPmAgFnP	začít
připisovat	připisovat	k5eAaImF	připisovat
Kronovy	Kronův	k2eAgFnPc1d1	Kronova
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
i	i	k9	i
Zeus	Zeus	k1gInSc1	Zeus
<g/>
,	,	kIx,	,
Římany	Říman	k1gMnPc4	Říman
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
nakonec	nakonec	k6eAd1	nakonec
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
mytologii	mytologie	k1gFnSc6	mytologie
Saturn	Saturn	k1gMnSc1	Saturn
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
bůh	bůh	k1gMnSc1	bůh
Šani	Šan	k1gFnSc2	Šan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Saturn	Saturn	k1gInSc1	Saturn
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
planetu	planeta	k1gFnSc4	planeta
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
nejpomalejší	pomalý	k2eAgMnSc1d3	nejpomalejší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
tradiční	tradiční	k2eAgFnSc2d1	tradiční
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
formování	formování	k1gNnSc4	formování
a	a	k8xC	a
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
zákony	zákon	k1gInPc4	zákon
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
starobu	staroba	k1gFnSc4	staroba
<g/>
,	,	kIx,	,
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
kladné	kladný	k2eAgFnPc1d1	kladná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
připisuje	připisovat	k5eAaImIp3nS	připisovat
stálost	stálost	k1gFnSc4	stálost
<g/>
,	,	kIx,	,
praktičnost	praktičnost	k1gFnSc4	praktičnost
<g/>
,	,	kIx,	,
hospodárnost	hospodárnost	k1gFnSc4	hospodárnost
<g/>
,	,	kIx,	,
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
a	a	k8xC	a
systematičnost	systematičnost	k1gFnSc4	systematičnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
nepříznivým	příznivý	k2eNgInPc3d1	nepříznivý
patří	patřit	k5eAaImIp3nP	patřit
chlad	chlad	k1gInSc1	chlad
<g/>
,	,	kIx,	,
izolace	izolace	k1gFnSc1	izolace
<g/>
,	,	kIx,	,
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
<g/>
,	,	kIx,	,
pesimismus	pesimismus	k1gInSc1	pesimismus
<g/>
,	,	kIx,	,
frustrace	frustrace	k1gFnSc1	frustrace
a	a	k8xC	a
deprese	deprese	k1gFnSc1	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
astrologie	astrologie	k1gFnSc1	astrologie
se	se	k3xPyFc4	se
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
dívá	dívat	k5eAaImIp3nS	dívat
jako	jako	k9	jako
na	na	k7c6	na
"	"	kIx"	"
<g/>
otce	otka	k1gFnSc6	otka
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidský	lidský	k2eAgInSc1d1	lidský
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
obězích	oběh	k1gInPc6	oběh
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
zvěrokruhem	zvěrokruh	k1gInSc7	zvěrokruh
naplněn	naplnit	k5eAaPmNgInS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
životního	životní	k2eAgMnSc4d1	životní
učitele	učitel	k1gMnSc4	učitel
a	a	k8xC	a
symbol	symbol	k1gInSc4	symbol
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
též	též	k9	též
symbolem	symbol	k1gInSc7	symbol
zkušenosti	zkušenost	k1gFnSc2	zkušenost
a	a	k8xC	a
zodpovědnosti	zodpovědnost	k1gFnSc2	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
astrologii	astrologie	k1gFnSc6	astrologie
vládne	vládnout	k5eAaImIp3nS	vládnout
Saturn	Saturn	k1gMnSc1	Saturn
znamení	znamení	k1gNnSc4	znamení
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
i	i	k8xC	i
znamení	znamení	k1gNnSc6	znamení
Vodnáře	vodnář	k1gMnSc2	vodnář
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
moderní	moderní	k2eAgMnPc1d1	moderní
astrologové	astrolog	k1gMnPc1	astrolog
však	však	k9	však
Vodnáře	vodnář	k1gMnPc4	vodnář
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
planetě	planeta	k1gFnSc3	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
,	,	kIx,	,
objevené	objevený	k2eAgInPc1d1	objevený
r.	r.	kA	r.
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
patřila	patřit	k5eAaImAgFnS	patřit
planeta	planeta	k1gFnSc1	planeta
Saturn	Saturn	k1gInSc4	Saturn
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Merkurem	Merkur	k1gInSc7	Merkur
<g/>
,	,	kIx,	,
Venuší	Venuše	k1gFnSc7	Venuše
<g/>
,	,	kIx,	,
Marsem	Mars	k1gInSc7	Mars
<g/>
,	,	kIx,	,
Jupiterem	Jupiter	k1gInSc7	Jupiter
<g/>
,	,	kIx,	,
Měsícem	měsíc	k1gInSc7	měsíc
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
další	další	k2eAgFnPc1d1	další
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
námětem	námět	k1gInSc7	námět
některých	některý	k3yIgFnPc2	některý
sci-fi	scii	k1gFnPc2	sci-fi
knížek	knížka	k1gFnPc2	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyjma	vyjma	k7c2	vyjma
Saturnu	Saturn	k1gInSc2	Saturn
objevuje	objevovat	k5eAaImIp3nS	objevovat
děj	děj	k1gInSc1	děj
situovaný	situovaný	k2eAgInSc1d1	situovaný
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
měsíc	měsíc	k1gInSc4	měsíc
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc4d1	tvořený
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
často	často	k6eAd1	často
měsíc	měsíc	k1gInSc4	měsíc
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
čerpací	čerpací	k2eAgFnPc1d1	čerpací
stanice	stanice	k1gFnPc1	stanice
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInPc4d1	budoucí
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
či	či	k8xC	či
jako	jako	k9	jako
surovinová	surovinový	k2eAgFnSc1d1	surovinová
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
dobývání	dobývání	k1gNnSc4	dobývání
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
částí	část	k1gFnPc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gMnSc1	Saturn
se	se	k3xPyFc4	se
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
například	například	k6eAd1	například
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
bratrů	bratr	k1gMnPc2	bratr
Strugackých	Strugacký	k2eAgInPc2d1	Strugacký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
publikovali	publikovat	k5eAaBmAgMnP	publikovat
svoji	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
Tachmasib	Tachmasib	k1gMnSc1	Tachmasib
letí	letět	k5eAaImIp3nS	letět
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
okolo	okolo	k7c2	okolo
Saturna	Saturn	k1gMnSc2	Saturn
hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
Arthura	Arthura	k1gFnSc1	Arthura
C.	C.	kA	C.
Clarka	Clarka	k1gFnSc1	Clarka
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
výpravě	výprava	k1gFnSc6	výprava
k	k	k7c3	k
Saturnově	Saturnův	k2eAgFnSc3d1	Saturnova
měsíci	měsíc	k1gInPc7	měsíc
Japetusu	Japetus	k1gInSc2	Japetus
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nacházet	nacházet	k5eAaImF	nacházet
tajemný	tajemný	k2eAgInSc1d1	tajemný
černý	černý	k2eAgInSc1d1	černý
monolit	monolit	k1gInSc1	monolit
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
českého	český	k2eAgMnSc2d1	český
autora	autor	k1gMnSc2	autor
Jiřího	Jiří	k1gMnSc2	Jiří
Kulhánka	Kulhánek	k1gMnSc2	Kulhánek
Stroncium	stroncium	k1gNnSc4	stroncium
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
literárního	literární	k2eAgNnSc2d1	literární
zpracování	zpracování	k1gNnSc2	zpracování
se	se	k3xPyFc4	se
Saturn	Saturn	k1gInSc1	Saturn
stává	stávat	k5eAaImIp3nS	stávat
i	i	k9	i
námětem	námět	k1gInSc7	námět
filmu	film	k1gInSc2	film
Saturn	Saturn	k1gInSc1	Saturn
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
malé	malý	k2eAgFnSc6d1	malá
vědecké	vědecký	k2eAgFnSc6d1	vědecká
stanici	stanice	k1gFnSc6	stanice
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
Titan	titan	k1gInSc4	titan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dvojice	dvojice	k1gFnSc1	dvojice
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
osobou	osoba	k1gFnSc7	osoba
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
robotem	robot	k1gInSc7	robot
<g/>
.	.	kIx.	.
</s>
