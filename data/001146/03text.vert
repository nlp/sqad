<s>
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1838	[number]	k4	1838
Úbislavice	Úbislavice	k1gFnSc1	Úbislavice
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1907	[number]	k4	1907
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
bohemista	bohemista	k1gMnSc1	bohemista
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
přední	přední	k2eAgMnSc1d1	přední
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
českých	český	k2eAgMnPc2d1	český
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vědecká	vědecký	k2eAgFnSc1d1	vědecká
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
pozitivismem	pozitivismus	k1gInSc7	pozitivismus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
mladogramatismu	mladogramatismus	k1gInSc2	mladogramatismus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
zkoumáním	zkoumání	k1gNnSc7	zkoumání
jazyka	jazyk	k1gInSc2	jazyk
toliko	toliko	k6eAd1	toliko
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
diachronního	diachronní	k2eAgNnSc2d1	diachronní
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
(	(	kIx(	(
<g/>
synchronní	synchronní	k2eAgInSc1d1	synchronní
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgInS	být
brán	brát	k5eAaImNgInS	brát
vůbec	vůbec	k9	vůbec
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
vydal	vydat	k5eAaPmAgInS	vydat
příručku	příručka	k1gFnSc4	příručka
Pravidel	pravidlo	k1gNnPc2	pravidlo
hledících	hledící	k2eAgNnPc2d1	hledící
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
pravopisu	pravopis	k1gInSc3	pravopis
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc6	tvarosloví
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešních	dnešní	k2eAgNnPc2d1	dnešní
Pravidel	pravidlo	k1gNnPc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
osadě	osada	k1gFnSc6	osada
Úbislavice	Úbislavice	k1gFnSc2	Úbislavice
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
do	do	k7c2	do
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
místního	místní	k2eAgMnSc2d1	místní
faráře	farář	k1gMnSc2	farář
dostal	dostat	k5eAaPmAgMnS	dostat
nadaný	nadaný	k2eAgMnSc1d1	nadaný
chlapec	chlapec	k1gMnSc1	chlapec
možnost	možnost	k1gFnSc4	možnost
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
nejdřív	dříve	k6eAd3	dříve
studoval	studovat	k5eAaImAgMnS	studovat
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
studium	studium	k1gNnSc4	studium
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
filozofie	filozofie	k1gFnSc2	filozofie
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
studií	studio	k1gNnPc2	studio
trpěl	trpět	k5eAaImAgInS	trpět
stálým	stálý	k2eAgInSc7d1	stálý
nedostatkem	nedostatek	k1gInSc7	nedostatek
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studií	studie	k1gFnPc2	studie
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
-	-	kIx~	-
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zase	zase	k9	zase
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc4	doktorát
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
jako	jako	k9	jako
docent	docent	k1gMnSc1	docent
češtiny	čeština	k1gFnSc2	čeština
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
pověřen	pověřit	k5eAaPmNgInS	pověřit
výkonem	výkon	k1gInSc7	výkon
funkce	funkce	k1gFnSc2	funkce
prvního	první	k4xOgMnSc2	první
ředitele	ředitel	k1gMnPc4	ředitel
semináře	seminář	k1gInPc1	seminář
pro	pro	k7c4	pro
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
filologii	filologie	k1gFnSc4	filologie
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
profesorem	profesor	k1gMnSc7	profesor
řádným	řádný	k2eAgMnSc7d1	řádný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
byl	být	k5eAaImAgInS	být
děkanem	děkan	k1gMnSc7	děkan
FF	ff	kA	ff
UK	UK	kA	UK
<g/>
,	,	kIx,	,
rektorem	rektor	k1gMnSc7	rektor
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
pak	pak	k6eAd1	pak
v	v	k7c6	v
období	období	k1gNnSc6	období
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zásluhy	zásluha	k1gFnPc1	zásluha
ve	v	k7c6	v
vědních	vědní	k2eAgInPc6d1	vědní
oborech	obor	k1gInPc6	obor
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
filologie	filologie	k1gFnSc1	filologie
a	a	k8xC	a
literární	literární	k2eAgFnPc1d1	literární
historie	historie	k1gFnPc1	historie
byly	být	k5eAaImAgFnP	být
oceněny	oceněn	k2eAgFnPc1d1	oceněna
členstvím	členství	k1gNnSc7	členství
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
české	český	k2eAgFnSc6d1	Česká
společnosti	společnost	k1gFnSc6	společnost
nauk	nauka	k1gFnPc2	nauka
(	(	kIx(	(
<g/>
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
členem	člen	k1gMnSc7	člen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
řádným	řádný	k2eAgMnSc7d1	řádný
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1890	[number]	k4	1890
jej	on	k3xPp3gInSc4	on
císař	císař	k1gMnSc1	císař
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
akademií	akademie	k1gFnPc2	akademie
polské	polský	k2eAgFnSc2d1	polská
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
<g/>
,	,	kIx,	,
Jednoty	jednota	k1gFnSc2	jednota
českých	český	k2eAgMnPc2d1	český
filologů	filolog	k1gMnPc2	filolog
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
Listů	list	k1gInPc2	list
filologických	filologický	k2eAgInPc2d1	filologický
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
Riegrova	Riegrův	k2eAgInSc2d1	Riegrův
slovníku	slovník	k1gInSc2	slovník
naučného	naučný	k2eAgInSc2d1	naučný
přispěl	přispět	k5eAaPmAgInS	přispět
řadou	řada	k1gFnSc7	řada
hesel	heslo	k1gNnPc2	heslo
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
jazykovědy	jazykověda	k1gFnSc2	jazykověda
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Gollem	Goll	k1gMnSc7	Goll
měl	mít	k5eAaImAgMnS	mít
největší	veliký	k2eAgFnSc4d3	veliký
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
prokázání	prokázání	k1gNnSc4	prokázání
nepravosti	nepravost	k1gFnSc2	nepravost
Rukopisů	rukopis	k1gInPc2	rukopis
královédvorského	královédvorský	k2eAgNnSc2d1	královédvorské
a	a	k8xC	a
zelenohorského	zelenohorský	k2eAgNnSc2d1	zelenohorský
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
jejich	jejich	k3xOp3gMnSc7	jejich
zastáncem	zastánce	k1gMnSc7	zastánce
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poctivě	poctivě	k6eAd1	poctivě
bádal	bádat	k5eAaImAgInS	bádat
nad	nad	k7c4	nad
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
,	,	kIx,	,
než	než	k8xS	než
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jazykového	jazykový	k2eAgInSc2d1	jazykový
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
padělek	padělek	k1gInSc4	padělek
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
byl	být	k5eAaImAgMnS	být
tiskem	tisek	k1gMnSc7	tisek
napadán	napadat	k5eAaPmNgMnS	napadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zrádcem	zrádce	k1gMnSc7	zrádce
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
kruhu	kruh	k1gInSc6	kruh
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hanka	Hanka	k1gFnSc1	Hanka
byl	být	k5eAaImAgMnS	být
šejdíř	šejdíř	k1gMnSc1	šejdíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mě	já	k3xPp1nSc4	já
stál	stát	k5eAaImAgMnS	stát
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Helena	Helena	k1gFnSc1	Helena
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
manželkou	manželka	k1gFnSc7	manželka
profesora	profesor	k1gMnSc2	profesor
brněnské	brněnský	k2eAgFnSc2d1	brněnská
techniky	technika	k1gFnSc2	technika
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Lista	lista	k1gFnSc1	lista
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Eva	Eva	k1gFnSc1	Eva
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Josefa	Josef	k1gMnSc4	Josef
Schiezsla	Schiezsla	k1gMnSc4	Schiezsla
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k1gMnSc4	vedoucí
politického	politický	k2eAgInSc2d1	politický
odboru	odbor	k1gInSc2	odbor
kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
mluvnice	mluvnice	k1gFnSc1	mluvnice
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
-	-	kIx~	-
díl	díl	k1gInSc4	díl
I.	I.	kA	I.
Hláskosloví	hláskosloví	k1gNnSc1	hláskosloví
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Časování	časování	k1gNnSc1	časování
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díl	díl	k1gInSc4	díl
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
upravil	upravit	k5eAaPmAgMnS	upravit
František	František	k1gMnSc1	František
Trávníček	Trávníček	k1gMnSc1	Trávníček
<g/>
)	)	kIx)	)
Slovník	slovník	k1gInSc1	slovník
staročeský	staročeský	k2eAgInSc1d1	staročeský
díl	díl	k1gInSc1	díl
I.	I.	kA	I.
A-J	A-J	k1gFnSc2	A-J
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
on-line	onin	k1gInSc5	on-lin
<g/>
)	)	kIx)	)
a	a	k8xC	a
díl	díl	k1gInSc4	díl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
K-netbalivost	Ketbalivost	k1gFnSc1	K-netbalivost
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
dokončen	dokončen	k2eAgInSc1d1	dokončen
Emilem	Emil	k1gMnSc7	Emil
Smetánkou	smetánka	k1gFnSc7	smetánka
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
Mluvnice	mluvnice	k1gFnSc1	mluvnice
česká	český	k2eAgFnSc1d1	Česká
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
ústavy	ústava	k1gFnSc2	ústava
učitelské	učitelský	k2eAgInPc1d1	učitelský
-	-	kIx~	-
díl	díl	k1gInSc1	díl
I.	I.	kA	I.
Nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
slově	slovo	k1gNnSc6	slovo
<g/>
,	,	kIx,	,
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
Skladba	skladba	k1gFnSc1	skladba
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
Krátká	krátký	k2eAgFnSc1d1	krátká
mluvnice	mluvnice	k1gFnSc1	mluvnice
česká	český	k2eAgFnSc1d1	Česká
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
škol	škola	k1gFnPc2	škola
středních	střední	k2eAgInPc2d1	střední
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Poučení	poučení	k1gNnSc1	poučení
o	o	k7c6	o
padělaných	padělaný	k2eAgFnPc6d1	padělaná
rukopisích	rukopise	k1gFnPc6	rukopise
Královédvorském	královédvorský	k2eAgInSc6d1	královédvorský
a	a	k8xC	a
Zelenohorském	zelenohorský	k2eAgInSc6d1	zelenohorský
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
</s>
