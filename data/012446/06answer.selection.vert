<s>
Cizinec	cizinec	k1gMnSc1	cizinec
přichází	přicházet	k5eAaImIp3nS	přicházet
(	(	kIx(	(
<g/>
finský	finský	k2eAgInSc1d1	finský
titul	titul	k1gInSc1	titul
zní	znět	k5eAaImIp3nS	znět
Vieras	Vieras	k1gInSc4	Vieras
mies	mies	k6eAd1	mies
tuli	tuli	k6eAd1	tuli
taloon	taloona	k1gFnPc2	taloona
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
psychologicky	psychologicky	k6eAd1	psychologicky
laděná	laděný	k2eAgFnSc1d1	laděná
novela	novela	k1gFnSc1	novela
finského	finský	k2eAgMnSc2d1	finský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Miky	Mik	k1gMnPc4	Mik
Waltariho	Waltariha	k1gFnSc5	Waltariha
vydaná	vydaný	k2eAgNnPc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
