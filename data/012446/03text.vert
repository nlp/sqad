<p>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
přichází	přicházet	k5eAaImIp3nS	přicházet
(	(	kIx(	(
<g/>
finský	finský	k2eAgInSc1d1	finský
titul	titul	k1gInSc1	titul
zní	znět	k5eAaImIp3nS	znět
Vieras	Vieras	k1gInSc4	Vieras
mies	mies	k6eAd1	mies
tuli	tuli	k6eAd1	tuli
taloon	taloona	k1gFnPc2	taloona
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
psychologicky	psychologicky	k6eAd1	psychologicky
laděná	laděný	k2eAgFnSc1d1	laděná
novela	novela	k1gFnSc1	novela
finského	finský	k2eAgMnSc2d1	finský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Miky	Mik	k1gMnPc4	Mik
Waltariho	Waltariha	k1gFnSc5	Waltariha
vydaná	vydaný	k2eAgNnPc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc6	nenávist
situovaný	situovaný	k2eAgInSc1d1	situovaný
na	na	k7c4	na
odlehlý	odlehlý	k2eAgInSc4d1	odlehlý
finský	finský	k2eAgInSc4d1	finský
venkovský	venkovský	k2eAgInSc4d1	venkovský
statek	statek	k1gInSc4	statek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
překonávala	překonávat	k5eAaImAgFnS	překonávat
mnohá	mnohý	k2eAgFnSc1d1	mnohá
dobová	dobový	k2eAgFnSc1d1	dobová
tabu	tabu	k1gNnSc7	tabu
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zde	zde	k6eAd1	zde
zazněla	zaznít	k5eAaPmAgFnS	zaznít
obhajoba	obhajoba	k1gFnSc1	obhajoba
mimomanželského	mimomanželský	k2eAgInSc2d1	mimomanželský
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
nepotrestaná	potrestaný	k2eNgFnSc1d1	nepotrestaná
vražda	vražda	k1gFnSc1	vražda
<g/>
,	,	kIx,	,
silné	silný	k2eAgInPc1d1	silný
erotické	erotický	k2eAgInPc1d1	erotický
momenty	moment	k1gInPc1	moment
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Panu	Pan	k1gMnSc3	Pan
Rajaly	Rajal	k1gInPc4	Rajal
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnPc4	předseda
klubu	klub	k1gInSc2	klub
Miky	Mik	k1gMnPc4	Mik
Waltariho	Waltari	k1gMnSc2	Waltari
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
díly	díl	k1gInPc1	díl
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
D.	D.	kA	D.
H.	H.	kA	H.
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
autorovou	autorův	k2eAgFnSc7d1	autorova
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
venkovskými	venkovský	k2eAgInPc7d1	venkovský
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Waltari	Waltari	k6eAd1	Waltari
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
anonymní	anonymní	k2eAgFnPc4d1	anonymní
soutěže	soutěž	k1gFnPc4	soutěž
vypsané	vypsaný	k2eAgNnSc1d1	vypsané
největším	veliký	k2eAgNnSc7d3	veliký
finským	finský	k2eAgNnSc7d1	finské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Werner	Werner	k1gMnSc1	Werner
Söderström	Söderström	k1gMnSc1	Söderström
Osakeyhtiö	Osakeyhtiö	k1gMnSc1	Osakeyhtiö
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
identifikován	identifikovat	k5eAaBmNgMnS	identifikovat
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
porotu	porota	k1gFnSc4	porota
to	ten	k3xDgNnSc1	ten
překvapilo	překvapit	k5eAaPmAgNnS	překvapit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
Waltari	Waltari	k1gNnSc4	Waltari
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
městského	městský	k2eAgMnSc4d1	městský
spisovatele	spisovatel	k1gMnSc4	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
obrovský	obrovský	k2eAgInSc1d1	obrovský
skandál	skandál	k1gInSc1	skandál
a	a	k8xC	a
Waltari	Waltari	k1gNnSc1	Waltari
pod	pod	k7c7	pod
veřejným	veřejný	k2eAgInSc7d1	veřejný
tlakem	tlak	k1gInSc7	tlak
napsal	napsat	k5eAaPmAgInS	napsat
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
novelu	novela	k1gFnSc4	novela
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dohra	dohra	k1gFnSc1	dohra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
vzbouřené	vzbouřený	k2eAgFnPc4d1	vzbouřená
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
<g/>
Česky	česky	k6eAd1	česky
kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Skaličky	Skalička	k1gMnSc2	Skalička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Aaltonen	Aaltonen	k1gInSc1	Aaltonen
-	-	kIx~	-
protagonista	protagonista	k1gMnSc1	protagonista
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Herman	Herman	k1gMnSc1	Herman
-	-	kIx~	-
starý	starý	k2eAgMnSc1d1	starý
hospodář	hospodář	k1gMnSc1	hospodář
</s>
</p>
<p>
<s>
hospodyně	hospodyně	k1gFnSc1	hospodyně
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
komory	komora	k1gFnSc2	komora
<g/>
"	"	kIx"	"
-	-	kIx~	-
manžel	manžel	k1gMnSc1	manžel
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
finský	finský	k2eAgInSc4d1	finský
venkovský	venkovský	k2eAgInSc4d1	venkovský
statek	statek	k1gInSc4	statek
přichází	přicházet	k5eAaImIp3nS	přicházet
"	"	kIx"	"
<g/>
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
"	"	kIx"	"
Aaltonen	Aaltonen	k1gInSc1	Aaltonen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odešel	odejít	k5eAaPmAgInS	odejít
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nevěrná	věrný	k2eNgFnSc1d1	nevěrná
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
sblíží	sblížit	k5eAaPmIp3nS	sblížit
s	s	k7c7	s
hospodyní	hospodyně	k1gFnSc7	hospodyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
muže	muž	k1gMnSc4	muž
alkoholika	alkoholik	k1gMnSc4	alkoholik
a	a	k8xC	a
hulváta	hulvát	k1gMnSc4	hulvát
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
její	její	k3xOp3gNnSc4	její
city	city	k1gNnSc4	city
po	po	k7c6	po
čase	čas	k1gInSc6	čas
opětuje	opětovat	k5eAaImIp3nS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
Podváděný	podváděný	k2eAgMnSc1d1	podváděný
manžel	manžel	k1gMnSc1	manžel
se	se	k3xPyFc4	se
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
vztahu	vztah	k1gInSc6	vztah
později	pozdě	k6eAd2	pozdě
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
a	a	k8xC	a
spřádá	spřádat	k5eAaImIp3nS	spřádat
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Aaltonena	Aaltonena	k1gFnSc1	Aaltonena
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gMnSc4	on
zastřelí	zastřelit	k5eAaPmIp3nP	zastřelit
v	v	k7c6	v
lese	les	k1gInSc6	les
při	při	k7c6	při
štípání	štípání	k1gNnSc6	štípání
dříví	dříví	k1gNnSc2	dříví
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
zaslechne	zaslechnout	k5eAaPmIp3nS	zaslechnout
výstřely	výstřel	k1gInPc4	výstřel
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
dorazí	dorazit	k5eAaPmIp3nS	dorazit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
činu	čin	k1gInSc2	čin
a	a	k8xC	a
sezná	seznat	k5eAaPmIp3nS	seznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
jejímu	její	k3xOp3gNnSc3	její
srdci	srdce	k1gNnSc3	srdce
milovanou	milovaný	k2eAgFnSc4d1	milovaná
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
pocítí	pocítit	k5eAaPmIp3nS	pocítit
palčivou	palčivý	k2eAgFnSc4d1	palčivá
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
Aaltonenovu	Aaltonenův	k2eAgFnSc4d1	Aaltonenův
sekeru	sekera	k1gFnSc4	sekera
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
tyrana	tyran	k1gMnSc2	tyran
ubíjí	ubíjet	k5eAaImIp3nS	ubíjet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
hospodář	hospodář	k1gMnSc1	hospodář
Herman	Herman	k1gMnSc1	Herman
jí	on	k3xPp3gFnSc7	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
odstranit	odstranit	k5eAaPmF	odstranit
obě	dva	k4xCgNnPc1	dva
těla	tělo	k1gNnPc1	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žena	žena	k1gFnSc1	žena
pak	pak	k6eAd1	pak
zažívá	zažívat	k5eAaImIp3nS	zažívat
psychické	psychický	k2eAgNnSc4d1	psychické
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
žít	žít	k5eAaImF	žít
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
čeká	čekat	k5eAaImIp3nS	čekat
Aaltonenovo	Aaltonenův	k2eAgNnSc1d1	Aaltonenův
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vladimír	Vladimír	k1gMnSc1	Vladimír
Skalička	Skalička	k1gMnSc1	Skalička
<g/>
,	,	kIx,	,
148	[number]	k4	148
stran	strana	k1gFnPc2	strana
</s>
</p>
<p>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
1465	[number]	k4	1465
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Petr	Petr	k1gMnSc1	Petr
Velkoborský	Velkoborský	k2eAgMnSc1d1	Velkoborský
<g/>
,	,	kIx,	,
144	[number]	k4	144
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Vieras	Vieras	k1gMnSc1	Vieras
mies	mies	k6eAd1	mies
tuli	tul	k1gMnSc3	tul
taloon	taloona	k1gFnPc2	taloona
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Wilho	Wil	k1gMnSc2	Wil
Ilmari	Ilmar	k1gFnSc2	Ilmar
</s>
</p>
<p>
<s>
Vieras	Vieras	k1gInSc1	Vieras
mies	miesa	k1gFnPc2	miesa
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Hannu	Hann	k1gInSc2	Hann
Leminen	Leminno	k1gNnPc2	Leminno
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
Databáze	databáze	k1gFnSc1	databáze
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
bibliografická	bibliografický	k2eAgFnSc1d1	bibliografická
databáze	databáze	k1gFnSc1	databáze
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
