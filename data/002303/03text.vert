<s>
Nekrofilie	nekrofilie	k1gFnSc1	nekrofilie
(	(	kIx(	(
<g/>
též	též	k9	též
nekromanie	nekromanie	k1gFnSc1	nekromanie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
parafilie	parafilie	k1gFnSc1	parafilie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
sexuální	sexuální	k2eAgFnSc1d1	sexuální
úchylka	úchylka	k1gFnSc1	úchylka
<g/>
)	)	kIx)	)
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
erotickou	erotický	k2eAgFnSc7d1	erotická
náklonností	náklonnost	k1gFnSc7	náklonnost
k	k	k7c3	k
mrtvolám	mrtvola	k1gFnPc3	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
složením	složení	k1gNnSc7	složení
starořeckého	starořecký	k2eAgMnSc2d1	starořecký
ν	ν	k?	ν
(	(	kIx(	(
<g/>
nekrós	nekrós	k1gInSc1	nekrós
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
,	,	kIx,	,
zemřelý	zemřelý	k1gMnSc1	zemřelý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
φ	φ	k?	φ
(	(	kIx(	(
<g/>
filia	filia	k1gFnSc1	filia
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
obliba	obliba	k1gFnSc1	obliba
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
něčemu	něco	k3yInSc3	něco
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
samotný	samotný	k2eAgInSc1d1	samotný
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
Psychopathia	Psychopathium	k1gNnSc2	Psychopathium
Sexualis	Sexualis	k1gFnSc2	Sexualis
německého	německý	k2eAgMnSc2d1	německý
sexuologa	sexuolog	k1gMnSc2	sexuolog
Richarda	Richarda	k1gFnSc1	Richarda
von	von	k1gInSc1	von
Krafft-Ebing	Krafft-Ebing	k1gInSc1	Krafft-Ebing
<g/>
.	.	kIx.	.
</s>
<s>
Nekrofilní	nekrofilní	k2eAgNnSc1d1	nekrofilní
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nekrofilní	nekrofilní	k2eAgNnSc1d1	nekrofilní
chování	chování	k1gNnSc1	chování
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
mezi	mezi	k7c7	mezi
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
pohřebního	pohřební	k2eAgInSc2d1	pohřební
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
popisovaný	popisovaný	k2eAgInSc1d1	popisovaný
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Sexuologové	sexuolog	k1gMnPc1	sexuolog
u	u	k7c2	u
nekrofilie	nekrofilie	k1gFnSc2	nekrofilie
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
formě	forma	k1gFnSc3	forma
parafilie	parafilie	k1gFnSc2	parafilie
<g/>
.	.	kIx.	.
</s>
<s>
Uznávají	uznávat	k5eAaImIp3nP	uznávat
spíše	spíše	k9	spíše
termín	termín	k1gInSc4	termín
nekrofilní	nekrofilní	k2eAgNnSc4d1	nekrofilní
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
s	s	k7c7	s
mrtvolami	mrtvola	k1gFnPc7	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Nekrofilní	nekrofilní	k2eAgNnSc1d1	nekrofilní
chování	chování	k1gNnSc1	chování
bývá	bývat	k5eAaImIp3nS	bývat
náhražkovým	náhražkový	k2eAgNnSc7d1	náhražkové
chováním	chování	k1gNnSc7	chování
u	u	k7c2	u
těch	ten	k3xDgFnPc2	ten
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jinak	jinak	k6eAd1	jinak
zaměřených	zaměřený	k2eAgMnPc2d1	zaměřený
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
uspokojit	uspokojit	k5eAaPmF	uspokojit
svoji	svůj	k3xOyFgFnSc4	svůj
sexuální	sexuální	k2eAgFnSc4d1	sexuální
potřebu	potřeba	k1gFnSc4	potřeba
s	s	k7c7	s
živými	živý	k2eAgFnPc7d1	živá
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nekrofilního	nekrofilní	k2eAgNnSc2d1	nekrofilní
chování	chování	k1gNnSc2	chování
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
trestu	trest	k1gInSc2	trest
hanobení	hanobení	k1gNnSc2	hanobení
lidských	lidský	k2eAgInPc2d1	lidský
ostatků	ostatek	k1gInPc2	ostatek
(	(	kIx(	(
<g/>
§	§	k?	§
202	[number]	k4	202
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Kdo	kdo	k3yQnSc1	kdo
...	...	k?	...
<g/>
s	s	k7c7	s
lidskými	lidský	k2eAgInPc7d1	lidský
ostatky	ostatek	k1gInPc7	ostatek
nakládá	nakládat	k5eAaImIp3nS	nakládat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
odnětím	odnětí	k1gNnSc7	odnětí
svobody	svoboda	k1gFnSc2	svoboda
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
léta	léto	k1gNnPc4	léto
nebo	nebo	k8xC	nebo
peněžitým	peněžitý	k2eAgInSc7d1	peněžitý
trestem	trest	k1gInSc7	trest
<g/>
.	.	kIx.	.
</s>
<s>
Zločinec	zločinec	k1gMnSc1	zločinec
není	být	k5eNaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
společensky	společensky	k6eAd1	společensky
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
nekrofilní	nekrofilní	k2eAgNnSc1d1	nekrofilní
chování	chování	k1gNnSc1	chování
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
vraždou	vražda	k1gFnSc7	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Nekrofilní	nekrofilní	k2eAgNnSc1d1	nekrofilní
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
běžné	běžný	k2eAgNnSc1d1	běžné
u	u	k7c2	u
sadistů	sadista	k1gMnPc2	sadista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nad	nad	k7c7	nad
mrtvými	mrtvý	k1gMnPc7	mrtvý
prožívají	prožívat	k5eAaImIp3nP	prožívat
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
pocit	pocit	k1gInSc1	pocit
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
a	a	k8xC	a
bezmoci	bezmoc	k1gFnSc2	bezmoc
oběti	oběť	k1gFnSc2	oběť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
WEISS	Weiss	k1gMnSc1	Weiss
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgFnSc1d1	sexuální
deviace	deviace	k1gFnSc1	deviace
<g/>
:	:	kIx,	:
Klasifikace	klasifikace	k1gFnSc1	klasifikace
<g/>
,	,	kIx,	,
diagnostika	diagnostika	k1gFnSc1	diagnostika
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
351	[number]	k4	351
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7178	[number]	k4	7178
<g/>
-	-	kIx~	-
<g/>
634	[number]	k4	634
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
