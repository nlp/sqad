<s>
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
(	(	kIx(
<g/>
ČNK	ČNK	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc1
jazykových	jazykový	k2eAgInPc2d1
korpusů	korpus	k1gInPc2
<g/>
,	,	kIx,
různě	různě	k6eAd1
vybraných	vybraný	k2eAgFnPc2d1
a	a	k8xC
uspořádaných	uspořádaný	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
elektronicky	elektronicky	k6eAd1
zaznamenaných	zaznamenaný	k2eAgInPc2d1
textů	text	k1gInPc2
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
<g/>
.	.	kIx.
</s>