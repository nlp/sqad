<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
Johna	John	k1gMnSc2	John
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
rukavičkáře	rukavičkář	k1gMnSc2	rukavičkář
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
radního	radní	k2eAgNnSc2d1	radní
města	město	k1gNnSc2	město
Stratfordu	Stratford	k1gInSc2	Stratford
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
ze	z	k7c2	z
Snitterfieldu	Snitterfield	k1gInSc2	Snitterfield
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
Ardenové	Ardenová	k1gFnSc2	Ardenová
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
bohatého	bohatý	k2eAgMnSc2d1	bohatý
velkostatkáře	velkostatkář	k1gMnSc2	velkostatkář
<g/>
.	.	kIx.	.
</s>
