<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Österreich-Ungarn	Österreich-Ungarn	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Österreichisch-Ungarische	Österreichisch-Ungarische	k1gFnSc1
Monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Osztrák-Magyar	Osztrák-Magyar	k1gInSc1
Monarchia	Monarchium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
<g/>
,	,	kIx,
reálná	reálný	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Království	království	k1gNnPc2
a	a	k8xC
zemí	zem	k1gFnPc2
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
zastoupených	zastoupený	k2eAgFnPc2d1
neboli	neboli	k8xC
Předlitavska	Předlitavsko	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Cisleithanien	Cisleithanien	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Zemí	zem	k1gFnSc7
svaté	svatý	k2eAgFnSc2d1
Štěpánské	štěpánský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
uherské	uherský	k2eAgFnSc2d1
neboli	neboli	k8xC
Zalitavska	Zalitavsko	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Transleithanien	Transleithanien	k2eAgInSc1d1
<g/>
,	,	kIx,
nepřesně	přesně	k6eNd1
Uherska	Uhersko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
existující	existující	k2eAgInSc4d1
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1867	#num#	k4
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>