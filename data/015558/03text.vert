<s>
Rakousko-Uhersko	Rakousko-Uhersko	k6eAd1
</s>
<s>
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
monarchieÖsterreichisch-UngarischeMonarchie	monarchieÖsterreichisch-UngarischeMonarchie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
Osztrák-Magyar	Osztrák-Magyar	k1gInSc1
Monarchia	Monarchium	k1gNnSc2
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↓	↓	k?
</s>
<s>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
↓	↓	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Hymna	hymna	k1gFnSc1
<g/>
:	:	kIx,
Gott	Gott	k1gInSc4
erhalte	erhalit	k5eAaPmRp2nP
<g/>
,	,	kIx,
Gott	Gott	k2eAgInSc4d1
beschütze	beschütze	k6eAd1
</s>
<s>
Motto	motto	k1gNnSc1
<g/>
:	:	kIx,
Indivisibiliter	Indivisibiliter	k1gInSc1
ac	ac	k?
inseparabiliter	inseparabiliter	k1gInSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
Předlitavsko	Předlitavsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
Budapešť	Budapešť	k1gFnSc1
(	(	kIx(
<g/>
Zalitavsko	Zalitavsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
676	#num#	k4
615	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Ortler	Ortler	k1gInSc1
(	(	kIx(
<g/>
3905	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Dunaj	Dunaj	k1gInSc1
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
51	#num#	k4
390	#num#	k4
223	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
Maďaři	Maďar	k1gMnPc1
<g/>
,	,	kIx,
Češi	Čech	k1gMnPc1
<g/>
,	,	kIx,
Poláci	Polák	k1gMnPc1
<g/>
,	,	kIx,
Ukrajinci	Ukrajinec	k1gMnPc1
(	(	kIx(
<g/>
Rusíni	Rusín	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rumuni	Rumun	k1gMnPc1
<g/>
,	,	kIx,
Srbové	Srb	k1gMnPc1
<g/>
,	,	kIx,
Chorvati	Chorvat	k1gMnPc1
<g/>
,	,	kIx,
Slováci	Slovák	k1gMnPc1
<g/>
,	,	kIx,
Slovinci	Slovinec	k1gMnPc1
<g/>
,	,	kIx,
Italové	Ital	k1gMnPc1
<g/>
,	,	kIx,
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
Romové	Rom	k1gMnPc1
(	(	kIx(
<g/>
Cikáni	cikán	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aj.	aj.	kA
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
maďarština	maďarština	k1gFnSc1
<g/>
,	,	kIx,
čeština	čeština	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
srbština-chorvatština	srbština-chorvatština	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
polština	polština	k1gFnSc1
<g/>
,	,	kIx,
slovenština	slovenština	k1gFnSc1
<g/>
,	,	kIx,
ukrajinština	ukrajinština	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
rumunština	rumunština	k1gFnSc1
<g/>
,	,	kIx,
slovinština	slovinština	k1gFnSc1
<g/>
,	,	kIx,
italština	italština	k1gFnSc1
<g/>
,	,	kIx,
jidiš	jidiš	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
latina	latina	k1gFnSc1
(	(	kIx(
<g/>
státně-oficiální	státně-oficiální	k2eAgFnSc1d1
<g/>
,	,	kIx,
církevní	církevní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aj.	aj.	kA
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
údaje	údaj	k1gInPc1
ze	z	k7c2
sčítání	sčítání	k1gNnSc2
r.	r.	kA
1910	#num#	k4
<g/>
:	:	kIx,
<g/>
64	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
%	%	kIx~
římskokatolické	římskokatolický	k2eAgFnSc2d1
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
10	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
%	%	kIx~
východní	východní	k2eAgFnSc2d1
katolické	katolický	k2eAgFnSc2d1
<g/>
,	,	kIx,
8,9	8,9	k4
%	%	kIx~
protestantské	protestantský	k2eAgFnSc2d1
(	(	kIx(
<g/>
tj.	tj.	kA
luteránské	luteránský	k2eAgFnPc1d1
<g/>
,	,	kIx,
kalvínské	kalvínský	k2eAgFnPc1d1
<g/>
,	,	kIx,
unitářské	unitářský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
8,7	8,7	k4
%	%	kIx~
srbské	srbský	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
<g/>
,	,	kIx,
4,4	4,4	k4
%	%	kIx~
judaistické	judaistický	k2eAgInPc1d1
<g/>
,	,	kIx,
1,3	1,3	k4
%	%	kIx~
sunnitský	sunnitský	k2eAgInSc1d1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
romské	romský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
<g/>
,	,	kIx,
synkretistické	synkretistický	k2eAgFnSc2d1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
dualistická	dualistický	k2eAgFnSc1d1
konstituční	konstituční	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
</s>
<s>
měna	měna	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
zlatý	zlatý	k1gInSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1867	#num#	k4
–	–	k?
po	po	k7c6
rakousko-uherském	rakousko-uherský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
–	–	k?
vystoupením	vystoupení	k1gNnSc7
Uherska	Uhersko	k1gNnSc2
z	z	k7c2
unie	unie	k1gFnSc2
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Německé	německý	k2eAgNnSc1d1
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
Maďarská	maďarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
Československá	československý	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Západoukrajinská	Západoukrajinský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Zakopanská	Zakopanský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Lemko-rusínská	Lemko-rusínský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Tarnobřežská	Tarnobřežský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Rumunské	rumunský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
Slovinců	Slovinec	k1gMnPc2
<g/>
,	,	kIx,
Chorvatů	Chorvat	k1gMnPc2
a	a	k8xC
Srbů	Srb	k1gMnPc2
</s>
<s>
Italské	italský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Italské	italský	k2eAgNnSc1d1
regentství	regentství	k1gNnSc1
Carnara	Carnar	k1gMnSc2
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Österreich-Ungarn	Österreich-Ungarn	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Österreichisch-Ungarische	Österreichisch-Ungarische	k1gFnSc1
Monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Osztrák-Magyar	Osztrák-Magyar	k1gInSc1
Monarchia	Monarchium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
<g/>
,	,	kIx,
reálná	reálný	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Království	království	k1gNnPc2
a	a	k8xC
zemí	zem	k1gFnPc2
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
zastoupených	zastoupený	k2eAgFnPc2d1
neboli	neboli	k8xC
Předlitavska	Předlitavsko	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Cisleithanien	Cisleithanien	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Zemí	zem	k1gFnSc7
svaté	svatý	k2eAgFnSc2d1
Štěpánské	štěpánský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
uherské	uherský	k2eAgFnSc2d1
neboli	neboli	k8xC
Zalitavska	Zalitavsko	k1gNnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Transleithanien	Transleithanien	k2eAgInSc1d1
<g/>
,	,	kIx,
nepřesně	přesně	k6eNd1
Uherska	Uhersko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
existující	existující	k2eAgInSc4d1
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1867	#num#	k4
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
soustátí	soustátí	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
přeměnou	přeměna	k1gFnSc7
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
nástupníka	nástupník	k1gMnSc2
Habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
rakousko-uherského	rakousko-uherský	k2eAgNnSc2d1
vyrovnání	vyrovnání	k1gNnSc2
v	v	k7c6
únoru	únor	k1gInSc6
1867	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Rakousko-uherské	rakousko-uherský	k2eAgNnSc1d1
vyrovnání	vyrovnání	k1gNnSc1
</s>
<s>
Císař	Císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
vládl	vládnout	k5eAaImAgMnS
Rakouskému	rakouský	k2eAgNnSc3d1
císařství	císařství	k1gNnSc3
od	od	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1916	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
plných	plný	k2eAgNnPc2d1
68	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Po	po	k7c6
katastrofální	katastrofální	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
rakouské	rakouský	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
ve	v	k7c6
válce	válka	k1gFnSc6
proti	proti	k7c3
Prusku	Prusko	k1gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
1866	#num#	k4
se	se	k3xPyFc4
znovu	znovu	k6eAd1
objevily	objevit	k5eAaPmAgFnP
obavy	obava	k1gFnPc1
z	z	k7c2
dalšího	další	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k6eAd1
se	se	k3xPyFc4
tomu	ten	k3xDgNnSc3
již	již	k6eAd1
v	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
minulosti	minulost	k1gFnSc6
několikrát	několikrát	k6eAd1
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomeňme	připomenout	k5eAaPmRp1nP
<g/>
,	,	kIx,
že	že	k8xS
evropská	evropský	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
revolucí	revoluce	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
nakonec	nakonec	k6eAd1
vyústila	vyústit	k5eAaPmAgFnS
v	v	k7c4
otevřenou	otevřený	k2eAgFnSc4d1
válku	válka	k1gFnSc4
mezi	mezi	k7c7
císařskými	císařský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
a	a	k8xC
povstaleckou	povstalecký	k2eAgFnSc7d1
uherskou	uherský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
relativně	relativně	k6eAd1
rychle	rychle	k6eAd1
zpacifikovat	zpacifikovat	k5eAaPmF
jen	jen	k9
díky	díky	k7c3
pomoci	pomoc	k1gFnSc3
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
následné	následný	k2eAgFnSc6d1
době	doba	k1gFnSc6
Bachova	Bachův	k2eAgInSc2d1
absolutismu	absolutismus	k1gInSc2
rakousko-uherský	rakousko-uherský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
dál	daleko	k6eAd2
bublal	bublat	k5eAaImAgInS
pod	pod	k7c7
povrchem	povrch	k1gInSc7
a	a	k8xC
projevoval	projevovat	k5eAaImAgMnS
se	se	k3xPyFc4
občasnými	občasný	k2eAgFnPc7d1
projevy	projev	k1gInPc7
občanské	občanský	k2eAgFnSc2d1
neposlušnosti	neposlušnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
potácela	potácet	k5eAaImAgFnS
od	od	k7c2
jednoho	jeden	k4xCgInSc2
neúspěchu	neúspěch	k1gInSc2
k	k	k7c3
druhému	druhý	k4xOgInSc3
a	a	k8xC
o	o	k7c6
úspěších	úspěch	k1gInPc6
na	na	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
se	se	k3xPyFc4
rozhodně	rozhodně	k6eAd1
také	také	k9
nedalo	dát	k5eNaPmAgNnS
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
po	po	k7c6
prohrané	prohraný	k2eAgFnSc6d1
Prusko-rakouské	prusko-rakouský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	s	k7c7
vlivným	vlivný	k2eAgInSc7d1
politickým	politický	k2eAgInSc7d1
kruhům	kruh	k1gInPc3
zdálo	zdát	k5eAaImAgNnS
výhodné	výhodný	k2eAgNnSc1d1
uklidnit	uklidnit	k5eAaPmF
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
rozsáhlou	rozsáhlý	k2eAgFnSc7d1
autonomií	autonomie	k1gFnSc7
Uherska	Uhersko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národnostní	národnostní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
obou	dva	k4xCgFnPc2
polovin	polovina	k1gFnPc2
říše	říš	k1gFnSc2
byla	být	k5eAaImAgFnS
podobná	podobný	k2eAgFnSc1d1
–	–	k?
v	v	k7c6
obou	dva	k4xCgFnPc6
existoval	existovat	k5eAaImAgInS
politicky	politicky	k6eAd1
dominantní	dominantní	k2eAgMnSc1d1
a	a	k8xC
početně	početně	k6eAd1
nejsilnější	silný	k2eAgInSc1d3
národ	národ	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
Maďaři	Maďar	k1gMnPc1
a	a	k8xC
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
těchto	tento	k3xDgInPc2
národů	národ	k1gInPc2
ve	v	k7c4
„	„	k?
<g/>
své	své	k1gNnSc4
<g/>
“	“	k?
části	část	k1gFnSc2
říše	říš	k1gFnSc2
neměl	mít	k5eNaImAgInS
nadpoloviční	nadpoloviční	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
rozdělení	rozdělení	k1gNnSc2
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
nazývat	nazývat	k5eAaImF
dualismem	dualismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
dualistické	dualistický	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
se	se	k3xPyFc4
tyto	tento	k3xDgInPc1
dva	dva	k4xCgInPc1
národní	národní	k2eAgInPc1d1
živly	živel	k1gInPc1
mohly	moct	k5eAaImAgInP
podporovat	podporovat	k5eAaImF
bez	bez	k7c2
vzájemné	vzájemný	k2eAgFnSc2d1
animozity	animozita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1867	#num#	k4
uherský	uherský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
schválil	schválit	k5eAaPmAgInS
všech	všecek	k3xTgInPc2
69	#num#	k4
článků	článek	k1gInPc2
vyrovnání	vyrovnání	k1gNnSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
říše	říše	k1gFnSc1
tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
tzv.	tzv.	kA
Země	zem	k1gFnSc2
v	v	k7c6
říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
zastoupené	zastoupený	k2eAgFnSc6d1
(	(	kIx(
<g/>
neoficiálně	neoficiálně	k6eAd1,k6eNd1
Předlitavsko	Předlitavsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Země	země	k1gFnSc1
koruny	koruna	k1gFnSc2
svatoštěpánské	svatoštěpánský	k2eAgFnSc2d1
(	(	kIx(
<g/>
neoficiálně	neoficiálně	k6eAd1,k6eNd1
Zalitavsko	Zalitavsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
]	]	kIx)
Vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
potvrzeno	potvrzen	k2eAgNnSc1d1
korunovací	korunovace	k1gFnSc7
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
za	za	k7c4
uherského	uherský	k2eAgMnSc4d1
krále	král	k1gMnSc4
v	v	k7c6
Budíně	Budín	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1867	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1868	#num#	k4
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
monarchie	monarchie	k1gFnSc1
nazývala	nazývat	k5eAaImAgFnS
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyrovnání	vyrovnání	k1gNnSc1
bylo	být	k5eAaImAgNnS
veřejností	veřejnost	k1gFnSc7
v	v	k7c6
monarchii	monarchie	k1gFnSc6
přijato	přijmout	k5eAaPmNgNnS
rozporuplně	rozporuplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
občanů	občan	k1gMnPc2
maďarské	maďarský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
byla	být	k5eAaImAgFnS
autonomie	autonomie	k1gFnSc1
Uher	uher	k1gInSc1
nedostatečná	nedostatečná	k1gFnSc1
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
si	se	k3xPyFc3
totiž	totiž	k9
představovali	představovat	k5eAaImAgMnP
úplné	úplný	k2eAgNnSc4d1
odtržení	odtržení	k1gNnSc4
od	od	k7c2
rakouské	rakouský	k2eAgFnSc2d1
části	část	k1gFnSc2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
další	další	k2eAgFnSc4d1
koexistenci	koexistence	k1gFnSc4
formou	forma	k1gFnSc7
konfederace	konfederace	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
mnohým	mnohý	k2eAgMnPc3d1
politikům	politik	k1gMnPc3
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
mysli	mysl	k1gFnSc3
samotné	samotný	k2eAgNnSc1d1
dualistické	dualistický	k2eAgNnSc1d1
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
zánik	zánik	k1gInSc4
relativně	relativně	k6eAd1
centralizovaného	centralizovaný	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
slovanskými	slovanský	k2eAgInPc7d1
národy	národ	k1gInPc7
vyrovnání	vyrovnání	k1gNnSc1
zase	zase	k9
oživilo	oživit	k5eAaPmAgNnS
naděje	naděje	k1gFnPc4
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1868	#num#	k4
bylo	být	k5eAaImAgNnS
přijato	přijmout	k5eAaPmNgNnS
chorvatsko-maďarské	chorvatsko-maďarský	k2eAgNnSc1d1
vyrovnání	vyrovnání	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
zajistilo	zajistit	k5eAaPmAgNnS
částečnou	částečný	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
Chorvatsko-Slavonska	Chorvatsko-Slavonsko	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
připravováno	připravován	k2eAgNnSc4d1
i	i	k8xC
rakousko-české	rakousko-český	k2eAgNnSc4d1
vyrovnání	vyrovnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
při	při	k7c6
své	svůj	k3xOyFgFnSc6
návštěvě	návštěva	k1gFnSc6
Prahy	Praha	k1gFnSc2
roku	rok	k1gInSc2
1868	#num#	k4
osobně	osobně	k6eAd1
pokoušel	pokoušet	k5eAaImAgInS
o	o	k7c4
dohodu	dohoda	k1gFnSc4
s	s	k7c7
českými	český	k2eAgMnPc7d1
zástupci	zástupce	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
pověřil	pověřit	k5eAaPmAgInS
hraběte	hrabě	k1gMnSc4
Karla	Karel	k1gMnSc4
Siegmunda	Siegmund	k1gMnSc4
von	von	k1gInSc1
Hohenwarta	Hohenwart	k1gMnSc2
vytvořením	vytvoření	k1gNnSc7
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
vláda	vláda	k1gFnSc1
Karla	Karla	k1gFnSc1
von	von	k1gInSc1
Hohenwarta	Hohenwarta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
pokoušela	pokoušet	k5eAaImAgFnS
o	o	k7c4
dohodu	dohoda	k1gFnSc4
s	s	k7c7
Čechy	Čech	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
političtí	politický	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
sestavili	sestavit	k5eAaPmAgMnP
fundamentální	fundamentální	k2eAgInPc4d1
články	článek	k1gInPc4
jako	jako	k8xS,k8xC
základ	základ	k1gInSc4
pozdějšího	pozdní	k2eAgNnSc2d2
vyrovnání	vyrovnání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požadavky	požadavek	k1gInPc4
v	v	k7c6
nich	on	k3xPp3gInPc6
obsažené	obsažený	k2eAgInPc1d1
však	však	k9
vyvolaly	vyvolat	k5eAaPmAgFnP
odpor	odpor	k1gInSc4
jak	jak	k6eAd1
u	u	k7c2
uherského	uherský	k2eAgMnSc2d1
ministerského	ministerský	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Andrássyho	Andrássy	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
snížení	snížení	k1gNnSc3
významu	význam	k1gInSc2
rakousko-uherského	rakousko-uherský	k2eAgNnSc2d1
vyrovnání	vyrovnání	k1gNnSc2
a	a	k8xC
podobně	podobně	k6eAd1
představitelé	představitel	k1gMnPc1
rakouských	rakouský	k2eAgFnPc2d1
Němců	Němec	k1gMnPc2
se	se	k3xPyFc4
cítili	cítit	k5eAaImAgMnP
ohroženi	ohrozit	k5eAaPmNgMnP
růstem	růst	k1gInSc7
vlivu	vliv	k1gInSc2
slovanských	slovanský	k2eAgMnPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
panovník	panovník	k1gMnSc1
jednání	jednání	k1gNnSc2
o	o	k7c6
česko-rakouském	česko-rakouský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
zastavil	zastavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vídeňská	vídeňský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
oslabená	oslabený	k2eAgFnSc1d1
po	po	k7c6
prohrané	prohraný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
vyšla	vyjít	k5eAaPmAgFnS
vstříc	vstříc	k7c3
polským	polský	k2eAgMnPc3d1
obyvatelům	obyvatel	k1gMnPc3
Haliče	Halič	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
autonomie	autonomie	k1gFnSc1
země	země	k1gFnSc1
<g/>
,	,	kIx,
polonizováno	polonizován	k2eAgNnSc1d1
školství	školství	k1gNnSc1
a	a	k8xC
soudnictví	soudnictví	k1gNnSc1
a	a	k8xC
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
rozrostla	rozrůst	k5eAaPmAgFnS
o	o	k7c4
jednoho	jeden	k4xCgMnSc4
ministra	ministr	k1gMnSc4
pro	pro	k7c4
haličské	haličský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zakladatelské	zakladatelský	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Rozestavěná	rozestavěný	k2eAgFnSc1d1
vídeňská	vídeňský	k2eAgFnSc1d1
Ringstraße	Ringstraße	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
</s>
<s>
Po	po	k7c6
vyrovnání	vyrovnání	k1gNnSc6
nastoupila	nastoupit	k5eAaPmAgFnS
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
liberální	liberální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
ministerským	ministerský	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
Gyulou	Gyula	k1gMnSc7
Andrássym	Andrássym	k1gInSc4
starším	starší	k1gMnPc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
stála	stát	k5eAaImAgFnS
před	před	k7c7
úkolem	úkol	k1gInSc7
modernizovat	modernizovat	k5eAaBmF
politicky	politicky	k6eAd1
i	i	k8xC
ekonomicky	ekonomicky	k6eAd1
značně	značně	k6eAd1
zaostalý	zaostalý	k2eAgInSc4d1
region	region	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovuzavedla	Znovuzavedlo	k1gNnPc1
některé	některý	k3yIgInPc4
zákony	zákon	k1gInPc4
pocházející	pocházející	k2eAgInPc4d1
ještě	ještě	k9
z	z	k7c2
období	období	k1gNnSc2
uherské	uherský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
posílila	posílit	k5eAaPmAgFnS
charakter	charakter	k1gInSc4
občanského	občanský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
usnadnila	usnadnit	k5eAaPmAgFnS
vydávání	vydávání	k1gNnSc4
novin	novina	k1gFnPc2
a	a	k8xC
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
byla	být	k5eAaImAgFnS
nadále	nadále	k6eAd1
znemožněna	znemožněn	k2eAgFnSc1d1
účast	účast	k1gFnSc1
v	v	k7c6
obecních	obecní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
spadaly	spadat	k5eAaPmAgFnP,k5eAaImAgFnP
do	do	k7c2
moci	moc	k1gFnSc2
nadřízeného	nadřízený	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byli	být	k5eAaImAgMnP
např.	např.	kA
sluhové	sluha	k1gMnPc1
<g/>
,	,	kIx,
zemědělští	zemědělský	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
nebo	nebo	k8xC
nádeníci	nádeník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžko	těžko	k6eAd1
vymahatelné	vymahatelný	k2eAgFnSc2d1
bylo	být	k5eAaImAgNnS
i	i	k9
spolčovací	spolčovací	k2eAgNnSc1d1
a	a	k8xC
shromažďovací	shromažďovací	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
nedemokratické	demokratický	k2eNgInPc4d1
rysy	rys	k1gInPc4
znevýhodňovaly	znevýhodňovat	k5eAaImAgFnP
především	především	k6eAd1
národnostní	národnostní	k2eAgFnPc1d1
menšiny	menšina	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
tvořily	tvořit	k5eAaImAgFnP
nejpočetnější	početní	k2eAgFnSc4d3
část	část	k1gFnSc4
chudiny	chudina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
nutnou	nutný	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
byla	být	k5eAaImAgFnS
modernizace	modernizace	k1gFnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
negramotnost	negramotnost	k1gFnSc1
u	u	k7c2
mužské	mužský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
k	k	k7c3
roku	rok	k1gInSc3
1869	#num#	k4
dosahovala	dosahovat	k5eAaImAgFnS
59	#num#	k4
procent	procento	k1gNnPc2
a	a	k8xC
u	u	k7c2
ženské	ženská	k1gFnSc2
dokonce	dokonce	k9
79	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Země	země	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
prohrané	prohraný	k2eAgFnSc6d1
prusko-rakouské	prusko-rakouský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
rychle	rychle	k6eAd1
vzpamatovala	vzpamatovat	k5eAaPmAgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
bismarkovské	bismarkovský	k2eAgNnSc1d1
Prusko	Prusko	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
zájem	zájem	k1gInSc4
na	na	k7c6
nestabilitě	nestabilita	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
jižního	jižní	k2eAgMnSc2d1
souseda	soused	k1gMnSc2
<g/>
,	,	kIx,
vyžadovalo	vyžadovat	k5eAaImAgNnS
proto	proto	k8xC
jen	jen	k9
nízké	nízký	k2eAgFnPc1d1
válečné	válečný	k2eAgFnPc1d1
reparace	reparace	k1gFnPc1
a	a	k8xC
dokonce	dokonce	k9
si	se	k3xPyFc3
v	v	k7c6
mírové	mírový	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
vynutilo	vynutit	k5eAaPmAgNnS
lepší	dobrý	k2eAgNnSc1d2
železniční	železniční	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
ekonomické	ekonomický	k2eAgFnSc6d1
i	i	k8xC
společenské	společenský	k2eAgFnSc6d1
vyspělosti	vyspělost	k1gFnSc6
byl	být	k5eAaImAgInS
obecně	obecně	k6eAd1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
monarchie	monarchie	k1gFnSc2
značný	značný	k2eAgInSc1d1
nepoměr	nepoměr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Sčítání	sčítání	k1gNnSc4
lidu	lid	k1gInSc2
1910	#num#	k4
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
odhalilo	odhalit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
zatímco	zatímco	k8xS
v	v	k7c6
Čechách	Čechy	k1gFnPc6
tvořili	tvořit	k5eAaImAgMnP
negramotní	gramotný	k2eNgMnPc1d1
jen	jen	k9
3	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
plných	plný	k2eAgNnPc2d1
60	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rakouských	rakouský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
Slezsku	Slezsko	k1gNnSc6
a	a	k8xC
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
prudkému	prudký	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
průmyslu	průmysl	k1gInSc2
i	i	k8xC
nové	nový	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
železnic	železnice	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
říční	říční	k2eAgFnSc2d1
nákladní	nákladní	k2eAgFnSc2d1
plavby	plavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Uhrách	Uhry	k1gFnPc6
se	se	k3xPyFc4
průmyslový	průmyslový	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
soustředil	soustředit	k5eAaPmAgInS
především	především	k9
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
v	v	k7c6
okolí	okolí	k1gNnSc6
Budapešti	Budapešť	k1gFnSc2
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
nynějšího	nynější	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
se	se	k3xPyFc4
průmysl	průmysl	k1gInSc1
rychle	rychle	k6eAd1
dral	drát	k5eAaImAgInS
na	na	k7c4
místo	místo	k1gNnSc4
rozhodujícího	rozhodující	k2eAgNnSc2d1
ekonomického	ekonomický	k2eAgNnSc2d1
odvětví	odvětví	k1gNnSc2
<g/>
,	,	kIx,
Zalitavsko	Zalitavsko	k1gNnSc1
nadále	nadále	k6eAd1
hrálo	hrát	k5eAaImAgNnS
roli	role	k1gFnSc4
zejména	zejména	k6eAd1
producenta	producent	k1gMnSc4
a	a	k8xC
vývozce	vývozce	k1gMnPc4
agrárních	agrární	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
i	i	k9
většina	většina	k1gFnSc1
uherského	uherský	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
spadala	spadat	k5eAaImAgFnS,k5eAaPmAgFnS
do	do	k7c2
výroby	výroba	k1gFnSc2
zemědělských	zemědělský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
nástrojů	nástroj	k1gInPc2
a	a	k8xC
potravinářství	potravinářství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
1869	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
dále	daleko	k6eAd2
akceleroval	akcelerovat	k5eAaImAgInS
po	po	k7c6
skončené	skončený	k2eAgFnSc6d1
prusko-francouzské	prusko-francouzský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
si	se	k3xPyFc3
v	v	k7c6
mírové	mírový	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
Prusko	Prusko	k1gNnSc1
vymohlo	vymoct	k5eAaPmAgNnS
na	na	k7c6
Francii	Francie	k1gFnSc6
válečné	válečný	k2eAgFnSc2d1
reparace	reparace	k1gFnSc2
v	v	k7c6
astronomické	astronomický	k2eAgFnSc6d1
výši	výše	k1gFnSc6,k1gFnSc6wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příliv	příliv	k1gInSc1
těchto	tento	k3xDgInPc2
levných	levný	k2eAgInPc2d1
peněz	peníze	k1gInPc2
ovlivňoval	ovlivňovat	k5eAaImAgInS
též	též	k9
situaci	situace	k1gFnSc4
v	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léta	léto	k1gNnSc2
1867	#num#	k4
<g/>
–	–	k?
<g/>
1873	#num#	k4
bývají	bývat	k5eAaImIp3nP
nazývána	nazývat	k5eAaImNgFnS
gründerským	gründerský	k2eAgNnSc7d1
(	(	kIx(
<g/>
zakladatelským	zakladatelský	k2eAgMnSc7d1
<g/>
)	)	kIx)
obdobím	období	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
monarchii	monarchie	k1gFnSc6
byly	být	k5eAaImAgFnP
zakládány	zakládán	k2eAgFnPc1d1
stovky	stovka	k1gFnPc1
akciových	akciový	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
a	a	k8xC
desítky	desítka	k1gFnPc4
bank	banka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
též	též	k9
vrcholila	vrcholit	k5eAaImAgFnS
doba	doba	k1gFnSc1
objevitelských	objevitelský	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
k	k	k7c3
dosud	dosud	k6eAd1
neznámým	známý	k2eNgFnPc3d1
končinám	končina	k1gFnPc3
severských	severský	k2eAgFnPc2d1
a	a	k8xC
jižních	jižní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Země	zem	k1gFnSc2
a	a	k8xC
u	u	k7c2
nových	nový	k2eAgInPc2d1
objevů	objev	k1gInPc2
stálo	stát	k5eAaImAgNnS
též	též	k9
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1871	#num#	k4
a	a	k8xC
1872	#num#	k4
byly	být	k5eAaImAgFnP
vyslány	vyslat	k5eAaPmNgFnP
dvě	dva	k4xCgFnPc1
průzkumné	průzkumný	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
do	do	k7c2
Arktidy	Arktida	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ta	ten	k3xDgFnSc1
druhá	druhý	k4xOgFnSc1
objevila	objevit	k5eAaPmAgNnP
v	v	k7c6
létě	léto	k1gNnSc6
1873	#num#	k4
nejsevernější	severní	k2eAgFnSc4d3
souostroví	souostroví	k1gNnSc3
<g/>
,	,	kIx,
kterému	který	k3yQgNnSc3,k3yRgNnSc3,k3yIgNnSc3
objevitelé	objevitel	k1gMnPc1
dali	dát	k5eAaPmAgMnP
název	název	k1gInSc4
Země	zem	k1gFnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
palubě	paluba	k1gFnSc6
paro-plachetního	paro-plachetní	k2eAgInSc2d1
škuneru	škuner	k1gInSc2
Admiral	Admiral	k1gMnSc1
Tegetthoff	Tegetthoff	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
k	k	k7c3
severu	sever	k1gInSc3
vyslán	vyslat	k5eAaPmNgInS
(	(	kIx(
<g/>
a	a	k8xC
sama	sám	k3xTgFnSc1
loď	loď	k1gFnSc1
byla	být	k5eAaImAgFnS
při	při	k7c6
plavbě	plavba	k1gFnSc6
zničena	zničen	k2eAgFnSc1d1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
řada	řada	k1gFnSc1
Čechů	Čech	k1gMnPc2
a	a	k8xC
českých	český	k2eAgMnPc2d1
Němců	Němec	k1gMnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
dnes	dnes	k6eAd1
na	na	k7c6
mapě	mapa	k1gFnSc6
souostroví	souostroví	k1gNnSc2
dodnes	dodnes	k6eAd1
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
několik	několik	k4yIc4
českých	český	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrovy	ostrov	k1gInPc7
se	se	k3xPyFc4
nestaly	stát	k5eNaPmAgFnP
součástí	součást	k1gFnSc7
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
výprava	výprava	k1gFnSc1
byla	být	k5eAaImAgFnS
financována	financovat	k5eAaBmNgFnS
ze	z	k7c2
soukromých	soukromý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1873	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zhroucení	zhroucení	k1gNnSc3
akciového	akciový	k2eAgInSc2d1
trhu	trh	k1gInSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krach	krach	k1gInSc1
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
burze	burza	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
počátkem	počátkem	k7c2
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
drtivě	drtivě	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
většinu	většina	k1gFnSc4
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
zejména	zejména	k9
strojírenství	strojírenství	k1gNnSc4
a	a	k8xC
těžký	těžký	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Čechách	Čechy	k1gFnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
propadu	propad	k1gInSc3
výroby	výroba	k1gFnSc2
surového	surový	k2eAgNnSc2d1
železa	železo	k1gNnSc2
a	a	k8xC
litiny	litina	k1gFnSc2
o	o	k7c4
65	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Uhry	Uhry	k1gFnPc1
odvrátily	odvrátit	k5eAaPmAgFnP
hrozbu	hrozba	k1gFnSc4
státního	státní	k2eAgInSc2d1
bankrotu	bankrot	k1gInSc2
jen	jen	k9
díky	díky	k7c3
půjčce	půjčka	k1gFnSc3
od	od	k7c2
Rothschildova	Rothschildův	k2eAgNnSc2d1
bankovního	bankovní	k2eAgNnSc2d1
konsorcia	konsorcium	k1gNnSc2
za	za	k7c2
značně	značně	k6eAd1
nevýhodných	výhodný	k2eNgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Krize	krize	k1gFnSc1
v	v	k7c6
monarchii	monarchie	k1gFnSc6
doznívala	doznívat	k5eAaImAgFnS
až	až	k6eAd1
koncem	koncem	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
krize	krize	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Velká	velký	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1875	#num#	k4
vypuklo	vypuknout	k5eAaPmAgNnS
v	v	k7c6
Hercegovině	Hercegovina	k1gFnSc6
Hercegovské	hercegovský	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
křesťanského	křesťanský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
proti	proti	k7c3
osmanské	osmanský	k2eAgFnSc3d1
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
Balkáně	Balkán	k1gInSc6
ve	v	k7c6
stádiu	stádium	k1gNnSc6
postupného	postupný	k2eAgInSc2d1
rozkladu	rozklad	k1gInSc2
již	již	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
využilo	využít	k5eAaPmAgNnS
Srbsko	Srbsko	k1gNnSc4
s	s	k7c7
Černou	černý	k2eAgFnSc7d1
Horou	hora	k1gFnSc7
a	a	k8xC
obě	dva	k4xCgFnPc1
země	zem	k1gFnPc1
vyhlásily	vyhlásit	k5eAaPmAgFnP
Osmanské	osmanský	k2eAgFnPc4d1
říši	říše	k1gFnSc4
válku	válek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
začala	začít	k5eAaPmAgFnS
Velká	velká	k1gFnSc1
východní	východní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
velkou	velký	k2eAgFnSc4d1
materiální	materiální	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
Ruska	Rusko	k1gNnSc2
stálo	stát	k5eAaImAgNnS
vojenské	vojenský	k2eAgNnSc4d1
štěstí	štěstí	k1gNnSc4
spíš	spíš	k9
na	na	k7c6
straně	strana	k1gFnSc6
Turků	turek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplné	úplný	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
Srbska	Srbsko	k1gNnSc2
zabránilo	zabránit	k5eAaPmAgNnS
diplomatické	diplomatický	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
evropských	evropský	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
sice	sice	k8xC
formálně	formálně	k6eAd1
spolupracovaly	spolupracovat	k5eAaImAgFnP
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
hájily	hájit	k5eAaImAgInP
především	především	k9
své	svůj	k3xOyFgFnPc4
imperiální	imperiální	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
a	a	k8xC
šly	jít	k5eAaImAgInP
tak	tak	k9
v	v	k7c6
podstatě	podstata	k1gFnSc6
proti	proti	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
Rusko	Rusko	k1gNnSc1
ze	z	k7c2
zištných	zištný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
po	po	k7c6
tajné	tajný	k2eAgFnSc6d1
dohodě	dohoda	k1gFnSc6
s	s	k7c7
Rakousko-Uherskem	Rakousko-Uhersko	k1gNnSc7
vyhlásilo	vyhlásit	k5eAaPmAgNnS
válku	válka	k1gFnSc4
Turecku	Turecko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výměnou	výměna	k1gFnSc7
za	za	k7c4
rakouskou	rakouský	k2eAgFnSc4d1
přátelskou	přátelský	k2eAgFnSc4d1
neutralitu	neutralita	k1gFnSc4
souhlasilo	souhlasit	k5eAaImAgNnS
Rusko	Rusko	k1gNnSc1
s	s	k7c7
rakousko-uherskými	rakousko-uherský	k2eAgInPc7d1
zájmy	zájem	k1gInPc7
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
mezi	mezi	k7c7
Tureckem	Turecko	k1gNnSc7
a	a	k8xC
Ruskem	Rusko	k1gNnSc7
tak	tak	k9
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
do	do	k7c2
správy	správa	k1gFnSc2
Bosnu	Bosna	k1gFnSc4
a	a	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
od	od	k7c2
roku	rok	k1gInSc2
1878	#num#	k4
vojensky	vojensky	k6eAd1
okupovalo	okupovat	k5eAaBmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
nadále	nadále	k6eAd1
formálně	formálně	k6eAd1
součástí	součást	k1gFnSc7
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
faktickým	faktický	k2eAgMnSc7d1
suverénem	suverén	k1gMnSc7
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
právě	právě	k9
habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
krize	krize	k1gFnSc1
a	a	k8xC
následná	následný	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
vzbudila	vzbudit	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
u	u	k7c2
ostatních	ostatní	k2eAgFnPc2d1
slovanských	slovanský	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
v	v	k7c6
monarchii	monarchie	k1gFnSc6
a	a	k8xC
např.	např.	kA
český	český	k2eAgInSc1d1
tisk	tisk	k1gInSc1
vyvolával	vyvolávat	k5eAaImAgInS
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
vlny	vlna	k1gFnSc2
národních	národní	k2eAgFnPc2d1
vášní	vášeň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekriticky	kriticky	k6eNd1
byl	být	k5eAaImAgInS
podporován	podporovat	k5eAaImNgInS
bosenský	bosenský	k2eAgInSc1d1
slovanský	slovanský	k2eAgInSc1d1
odboj	odboj	k1gInSc1
a	a	k8xC
intervenující	intervenující	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Národností	národnost	k1gFnPc2
požadavky	požadavek	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
odmítání	odmítání	k1gNnSc1
jinými	jiný	k2eAgFnPc7d1
zájmovými	zájmový	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
v	v	k7c6
rakousko-uherské	rakousko-uherský	k2eAgFnSc6d1
politice	politika	k1gFnSc6
se	se	k3xPyFc4
stávaly	stávat	k5eAaImAgInP
hlavním	hlavní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
vnitřních	vnitřní	k2eAgInPc2d1
rozporů	rozpor	k1gInPc2
v	v	k7c6
monarchii	monarchie	k1gFnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
spory	spor	k1gInPc1
česko-německé	česko-německý	k2eAgFnSc2d1
byly	být	k5eAaImAgFnP
nejvážnějším	vážní	k2eAgMnSc6d3
z	z	k7c2
těchto	tento	k3xDgInPc2
konfliktů	konflikt	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
rakousko-uherských	rakousko-uherský	k2eAgInPc2d1
národů	národ	k1gInPc2
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1
kroje	kroj	k1gInPc1
obyvatel	obyvatel	k1gMnPc2
Uherska	Uhersko	k1gNnSc2
z	z	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
se	se	k3xPyFc4
situace	situace	k1gFnSc1
neněmeckých	německý	k2eNgInPc2d1
národů	národ	k1gInPc2
sice	sice	k8xC
pomalu	pomalu	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
zcela	zcela	k6eAd1
zřetelně	zřetelně	k6eAd1
postupně	postupně	k6eAd1
zlepšovala	zlepšovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1880	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
Stremayrova	Stremayrův	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
nařízení	nařízení	k1gNnSc1
(	(	kIx(
<g/>
Karl	Karla	k1gFnPc2
von	von	k1gInSc1
Stremayr	Stremayr	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
předlitavským	předlitavský	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nařizovala	nařizovat	k5eAaImAgFnS
státním	státní	k2eAgInPc3d1
úřadům	úřad	k1gInPc3
na	na	k7c4
česká	český	k2eAgNnPc4d1
podání	podání	k1gNnPc4
odpovídat	odpovídat	k5eAaImF
též	též	k6eAd1
česky	česky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
vnitřní	vnitřní	k2eAgFnSc1d1
agenda	agenda	k1gFnSc1
zůstávala	zůstávat	k5eAaImAgFnS
dál	daleko	k6eAd2
jen	jen	k9
německá	německý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1897	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgMnSc4d1
Badeniho	Badeni	k1gMnSc4
jazyková	jazykový	k2eAgNnPc4d1
nařízení	nařízení	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
nařizovala	nařizovat	k5eAaImAgFnS
státním	státní	k2eAgMnPc3d1
úředníkům	úředník	k1gMnPc3
vést	vést	k5eAaImF
i	i	k9
vnitřní	vnitřní	k2eAgFnSc4d1
agendu	agenda	k1gFnSc4
v	v	k7c6
jazyce	jazyk	k1gInSc6
prvního	první	k4xOgNnSc2
podání	podání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
bylo	být	k5eAaImAgNnS
vyžadováno	vyžadovat	k5eAaImNgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
úředníci	úředník	k1gMnPc1
museli	muset	k5eAaImAgMnP
ovládat	ovládat	k5eAaImF
oba	dva	k4xCgInPc4
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reforma	reforma	k1gFnSc1
vyvolala	vyvolat	k5eAaPmAgFnS
ostrý	ostrý	k2eAgInSc4d1
odpor	odpor	k1gInSc4
německých	německý	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
a	a	k8xC
národovců	národovec	k1gMnPc2
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
jejichž	jejichž	k3xOyRp3gInSc4
nátlak	nátlak	k1gInSc4
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
vláda	vláda	k1gFnSc1
Kazimíra	Kazimír	k1gMnSc2
Badeniho	Badeni	k1gMnSc2
odvolána	odvolat	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Následující	následující	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Paula	Paul	k1gMnSc2
Gautsche	Gautsch	k1gMnSc2
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgNnP
zmírnit	zmírnit	k5eAaPmF
spory	spor	k1gInPc7
Gautschovými	Gautschův	k2eAgInPc7d1
jazykovými	jazykový	k2eAgNnPc7d1
nařízeními	nařízení	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yQgNnPc4,k3yIgNnPc4
jazyk	jazyk	k1gInSc1
vnitřní	vnitřní	k2eAgFnSc2d1
agendy	agenda	k1gFnSc2
určovala	určovat	k5eAaImAgNnP
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
jazyk	jazyk	k1gInSc1
měl	mít	k5eAaImAgInS
v	v	k7c6
daném	daný	k2eAgInSc6d1
okresu	okres	k1gInSc6
mezi	mezi	k7c7
obyvatelstvem	obyvatelstvo	k1gNnSc7
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
i	i	k9
tato	tento	k3xDgNnPc1
nařízení	nařízení	k1gNnPc1
odvolána	odvolán	k2eAgNnPc1d1
v	v	k7c6
platnosti	platnost	k1gFnSc6
zůstala	zůstat	k5eAaPmAgFnS
zákonná	zákonný	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
rokem	rok	k1gInSc7
1897	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Spor	spor	k1gInSc1
kolem	kolem	k7c2
jazykových	jazykový	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
vedl	vést	k5eAaImAgInS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
ochlazení	ochlazení	k1gNnSc3
vzájemných	vzájemný	k2eAgInPc2d1
česko-německých	česko-německý	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Situace	situace	k1gFnSc1
menších	malý	k2eAgInPc2d2
národů	národ	k1gInPc2
v	v	k7c6
Zalitavsku	Zalitavsko	k1gNnSc6
byla	být	k5eAaImAgFnS
o	o	k7c6
poznání	poznání	k1gNnSc6
horší	zlý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Andrássyho	Andrássy	k1gMnSc2
vládě	vláda	k1gFnSc6
(	(	kIx(
<g/>
skončila	skončit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1871	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
vystřídalo	vystřídat	k5eAaPmAgNnS
několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
slabších	slabý	k2eAgFnPc2d2
vlád	vláda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krizovém	krizový	k2eAgInSc6d1
roku	rok	k1gInSc6
1873	#num#	k4
se	se	k3xPyFc4
politická	politický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
stabilizovala	stabilizovat	k5eAaBmAgFnS
a	a	k8xC
roku	rok	k1gInSc2
1875	#num#	k4
nastoupila	nastoupit	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
ministerského	ministerský	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Kálmána	Kálmán	k1gMnSc2
Tiszy	Tisza	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
vydržel	vydržet	k5eAaPmAgMnS
celých	celý	k2eAgNnPc2d1
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
sice	sice	k8xC
úspěšně	úspěšně	k6eAd1
snažila	snažit	k5eAaImAgFnS
stabilizovat	stabilizovat	k5eAaBmF
veřejné	veřejný	k2eAgFnPc4d1
finance	finance	k1gFnPc4
a	a	k8xC
systém	systém	k1gInSc4
dualismu	dualismus	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
maďarizačním	maďarizační	k2eAgInSc6d1
tlaku	tlak	k1gInSc6
na	na	k7c4
ostatní	ostatní	k2eAgInPc4d1
národy	národ	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
roku	rok	k1gInSc2
1874	#num#	k4
byla	být	k5eAaImAgFnS
např.	např.	kA
zahájena	zahájen	k2eAgFnSc1d1
likvidace	likvidace	k1gFnSc1
slovenských	slovenský	k2eAgNnPc2d1
gymnázií	gymnázium	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
zastavena	zastaven	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
omezován	omezovat	k5eAaImNgInS
národnostní	národnostní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1868	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
se	se	k3xPyFc4
vytvářelo	vytvářet	k5eAaImAgNnS
zákonodárství	zákonodárství	k1gNnSc1
namířené	namířený	k2eAgNnSc1d1
proti	proti	k7c3
služebnictvu	služebnictvo	k1gNnSc3
a	a	k8xC
dělnictvu	dělnictvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
ústavní	ústavní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
o	o	k7c6
jednotném	jednotný	k2eAgInSc6d1
Uherském	uherský	k2eAgInSc6d1
politickém	politický	k2eAgInSc6d1
národě	národ	k1gInSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
maďarizováno	maďarizován	k2eAgNnSc1d1
školství	školství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učitelé	učitel	k1gMnPc1
museli	muset	k5eAaImAgMnP
maďarštinu	maďarština	k1gFnSc4
ovládat	ovládat	k5eAaImF
a	a	k8xC
vyučovat	vyučovat	k5eAaImF
na	na	k7c6
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
často	často	k6eAd1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
učitelům	učitel	k1gMnPc3
prakticky	prakticky	k6eAd1
nezbýval	zbývat	k5eNaImAgMnS
čas	čas	k1gInSc4
na	na	k7c4
výuku	výuka	k1gFnSc4
ostatních	ostatní	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
útlak	útlak	k1gInSc1
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgInS
i	i	k9
autonomního	autonomní	k2eAgNnSc2d1
Království	království	k1gNnSc2
chorvatsko-slavonského	chorvatsko-slavonský	k2eAgNnSc2d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
míra	míra	k1gFnSc1
autonomie	autonomie	k1gFnSc2
nebyla	být	k5eNaImAgFnS
příliš	příliš	k6eAd1
široká	široký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
násilném	násilný	k2eAgNnSc6d1
potlačení	potlačení	k1gNnSc6
chorvatského	chorvatský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
roku	rok	k1gInSc2
1883	#num#	k4
zde	zde	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
bána	bán	k1gMnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
Károly	Károla	k1gFnPc1
Khuen-Héderváry	Khuen-Hédervár	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zde	zde	k6eAd1
vládl	vládnout	k5eAaImAgInS
s	s	k7c7
pomocí	pomoc	k1gFnSc7
volebních	volební	k2eAgFnPc2d1
machinací	machinace	k1gFnPc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1903	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Khuen	Khuen	k1gInSc1
využíval	využívat	k5eAaPmAgInS,k5eAaImAgInS
sporů	spor	k1gInPc2
mezi	mezi	k7c7
Chorvaty	Chorvat	k1gMnPc7
a	a	k8xC
Srby	Srb	k1gMnPc7
a	a	k8xC
vládl	vládnout	k5eAaImAgInS
zde	zde	k6eAd1
tvrdou	tvrdý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
v	v	k7c6
prakticky	prakticky	k6eAd1
absolutistickém	absolutistický	k2eAgInSc6d1
režimu	režim	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Jistých	jistý	k2eAgInPc2d1
politických	politický	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
dosáhl	dosáhnout	k5eAaPmAgInS
krátkodobě	krátkodobě	k6eAd1
v	v	k7c6
uherském	uherský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
i	i	k9
antisemitismus	antisemitismus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Méně	málo	k6eAd2
početné	početný	k2eAgInPc1d1
národy	národ	k1gInPc1
měly	mít	k5eAaImAgInP
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
po	po	k7c6
právní	právní	k2eAgFnSc6d1
i	i	k8xC
faktické	faktický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
stále	stále	k6eAd1
více	hodně	k6eAd2
postavení	postavení	k1gNnSc4
pouhé	pouhý	k2eAgFnSc2d1
etnické	etnický	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
odsunuté	odsunutý	k2eAgFnSc2d1
na	na	k7c4
politický	politický	k2eAgInSc4d1
i	i	k8xC
ekonomický	ekonomický	k2eAgInSc4d1
okraj	okraj	k1gInSc4
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ačkoliv	ačkoliv	k8xS
v	v	k7c6
početním	početní	k2eAgInSc6d1
součtu	součet	k1gInSc6
tvořily	tvořit	k5eAaImAgFnP
větší	veliký	k2eAgInSc4d2
podíl	podíl	k1gInSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
než	než	k8xS
lidé	člověk	k1gMnPc1
hlásící	hlásící	k2eAgMnSc1d1
se	se	k3xPyFc4
k	k	k7c3
maďarské	maďarský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
zde	zde	k6eAd1
hrály	hrát	k5eAaImAgFnP
výchozí	výchozí	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
z	z	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
zastihl	zastihnout	k5eAaPmAgMnS
např.	např.	kA
Slováky	Slováky	k1gInPc4
na	na	k7c6
podstatně	podstatně	k6eAd1
nižším	nízký	k2eAgInSc6d2
stupni	stupeň	k1gInSc6
národního	národní	k2eAgNnSc2d1
uvědomění	uvědomění	k1gNnSc2
a	a	k8xC
politického	politický	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
než	než	k8xS
Čechy	Čechy	k1gFnPc4
v	v	k7c6
předlitavské	předlitavský	k2eAgFnSc6d1
části	část	k1gFnSc6
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
národnostních	národnostní	k2eAgInPc6d1
sporech	spor	k1gInPc6
se	se	k3xPyFc4
negativně	negativně	k6eAd1
projevoval	projevovat	k5eAaImAgInS
vliv	vliv	k1gInSc1
státního	státní	k2eAgInSc2d1
dualismu	dualismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivné	vlivný	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
německorakouských	německorakouský	k2eAgMnPc2d1
a	a	k8xC
maďarských	maďarský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
si	se	k3xPyFc3
dovedly	dovést	k5eAaPmAgFnP
dlouhodobě	dlouhodobě	k6eAd1
zajišťovat	zajišťovat	k5eAaImF
majoritní	majoritní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
ve	v	k7c6
„	„	k?
<g/>
svých	svůj	k3xOyFgNnPc6
<g/>
“	“	k?
částech	část	k1gFnPc6
monarchie	monarchie	k1gFnSc2
a	a	k8xC
na	na	k7c6
tomto	tento	k3xDgInSc6
stavu	stav	k1gInSc6
neměly	mít	k5eNaImAgFnP
zájem	zájem	k1gInSc4
mnoho	mnoho	k6eAd1
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vnitropolitickém	vnitropolitický	k2eAgNnSc6d1
pnutí	pnutí	k1gNnSc6
hrálo	hrát	k5eAaImAgNnS
též	též	k6eAd1
roli	role	k1gFnSc4
soupeření	soupeření	k1gNnSc3
mezi	mezi	k7c7
Budapeští	Budapešť	k1gFnSc7
a	a	k8xC
Vídní	Vídeň	k1gFnSc7
o	o	k7c4
dominantní	dominantní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
v	v	k7c6
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
dualismus	dualismus	k1gInSc4
vnímali	vnímat	k5eAaImAgMnP
jako	jako	k9
brzdu	brzda	k1gFnSc4
vývoje	vývoj	k1gInSc2
jak	jak	k8xC,k8xS
později	pozdě	k6eAd2
zavražděný	zavražděný	k2eAgMnSc1d1
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
skutečný	skutečný	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
Karel	Karel	k1gMnSc1
I.	I.	kA
Oba	dva	k4xCgMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
plánu	plán	k1gInSc6
přebudovat	přebudovat	k5eAaPmF
politické	politický	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
státu	stát	k1gInSc2
na	na	k7c6
federaci	federace	k1gFnSc6
autonomních	autonomní	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
federalizováno	federalizován	k2eAgNnSc1d1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
i	i	k9
dosud	dosud	k6eAd1
státoprávně	státoprávně	k6eAd1
celistvé	celistvý	k2eAgNnSc4d1
území	území	k1gNnSc4
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neměli	mít	k5eNaImAgMnP
však	však	k8xC
příležitost	příležitost	k1gFnSc1
své	svůj	k3xOyFgInPc4
plány	plán	k1gInPc4
zrealizovat	zrealizovat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vnímat	vnímat	k5eAaImF
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
coby	coby	k?
„	„	k?
<g/>
vězení	vězení	k1gNnSc2
národů	národ	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ale	ale	k9
přehnané	přehnaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavení	postavení	k1gNnSc1
menších	malý	k2eAgInPc2d2
národů	národ	k1gInPc2
v	v	k7c6
monarchii	monarchie	k1gFnSc6
bylo	být	k5eAaImAgNnS
proto	proto	k8xC
nesrovnatelně	srovnatelně	k6eNd1
lepší	dobrý	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
mnohonárodních	mnohonárodní	k2eAgInPc6d1
státech	stát	k1gInPc6
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
arménská	arménský	k2eAgFnSc1d1
genocida	genocida	k1gFnSc1
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Osmanské	osmanský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
polského	polský	k2eAgNnSc2d1
etnika	etnikum	k1gNnSc2
se	se	k3xPyFc4
města	město	k1gNnSc2
Lvov	Lvov	k1gInSc1
a	a	k8xC
Krakov	Krakov	k1gInSc1
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
Haliči	Halič	k1gFnSc6
stala	stát	k5eAaPmAgFnS
relativně	relativně	k6eAd1
svobodnými	svobodný	k2eAgInPc7d1
duchovními	duchovní	k2eAgInPc7d1
centry	centr	k1gInPc7
polského	polský	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
částech	část	k1gFnPc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
byly	být	k5eAaImAgInP
zabrány	zabrat	k5eAaPmNgInP
Německem	Německo	k1gNnSc7
či	či	k8xC
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
Poláci	Polák	k1gMnPc1
vzdorovat	vzdorovat	k5eAaImF
tvrdému	tvrdé	k1gNnSc3
germanizačnímu	germanizační	k2eAgNnSc3d1
<g/>
,	,	kIx,
resp.	resp.	kA
rusifikačnímu	rusifikační	k2eAgInSc3d1
nátlaku	nátlak	k1gInSc3
a	a	k8xC
cenzuře	cenzura	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Poláci	Polák	k1gMnPc1
na	na	k7c6
počátku	počátek	k1gInSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
nejprve	nejprve	k6eAd1
inklinovali	inklinovat	k5eAaImAgMnP
k	k	k7c3
rakouské	rakouský	k2eAgFnSc3d1
straně	strana	k1gFnSc3
proti	proti	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
měli	mít	k5eAaImAgMnP
z	z	k7c2
uplynulého	uplynulý	k2eAgNnSc2d1
století	století	k1gNnSc2
velmi	velmi	k6eAd1
špatné	špatný	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Nakonec	nakonec	k6eAd1
podstatná	podstatný	k2eAgFnSc1d1
část	část	k1gFnSc1
příslušníků	příslušník	k1gMnPc2
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
brala	brát	k5eAaImAgFnS
existenci	existence	k1gFnSc4
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
jako	jako	k8xS,k8xC
danou	daný	k2eAgFnSc4d1
věc	věc	k1gFnSc4
a	a	k8xC
místo	místo	k1gNnSc4
aktivního	aktivní	k2eAgInSc2d1
odporu	odpor	k1gInSc2
proti	proti	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
tomuto	tento	k3xDgInSc3
státu	stát	k1gInSc3
<g/>
,	,	kIx,
volili	volit	k5eAaImAgMnP
spíš	spíš	k9
pasivitu	pasivita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Situace	situace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6
se	se	k3xPyFc4
navíc	navíc	k6eAd1
lišila	lišit	k5eAaImAgFnS
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k9
kvůli	kvůli	k7c3
odlišnému	odlišný	k2eAgInSc3d1
politickému	politický	k2eAgInSc3d1
systému	systém	k1gInSc3
v	v	k7c6
předlitavské	předlitavský	k2eAgFnSc6d1
a	a	k8xC
zalitavské	zalitavský	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
kvůli	kvůli	k7c3
vzájemné	vzájemný	k2eAgFnSc3d1
animozitě	animozita	k1gFnSc3
mezi	mezi	k7c7
některými	některý	k3yIgInPc7
menšími	malý	k2eAgInPc7d2
národy	národ	k1gInPc7
samotnými	samotný	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemnou	vzájemný	k2eAgFnSc4d1
nedůvěru	nedůvěra	k1gFnSc4
k	k	k7c3
sobě	se	k3xPyFc3
pociťovali	pociťovat	k5eAaImAgMnP
Srbové	Srb	k1gMnPc1
a	a	k8xC
Chorvati	Chorvat	k1gMnPc1
<g/>
,	,	kIx,
špatné	špatný	k2eAgInPc1d1
byly	být	k5eAaImAgInP
vztahy	vztah	k1gInPc1
jihoslovanských	jihoslovanský	k2eAgMnPc2d1
národů	národ	k1gInPc2
k	k	k7c3
Italům	Ital	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zvláště	zvláště	k6eAd1
v	v	k7c6
Istrii	Istrie	k1gFnSc6
a	a	k8xC
Dalmácii	Dalmácie	k1gFnSc4
patřili	patřit	k5eAaImAgMnP
k	k	k7c3
ekonomicky	ekonomicky	k6eAd1
dominantním	dominantní	k2eAgFnPc3d1
vrstvám	vrstva	k1gFnPc3
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
také	také	k9
mnoho	mnoho	k4c1
případů	případ	k1gInPc2
napadání	napadání	k1gNnPc2
židů	žid	k1gMnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
Čechů	Čech	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
vůči	vůči	k7c3
Srbsku	Srbsko	k1gNnSc3
</s>
<s>
Odpor	odpor	k1gInSc1
muslimských	muslimský	k2eAgFnPc2d1
Bosňáků	Bosňáků	k?
během	běh	k1gInSc7
bitvy	bitva	k1gFnSc2
o	o	k7c4
Sarajevo	Sarajevo	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1878	#num#	k4
proti	proti	k7c3
rakousko-uherské	rakousko-uherský	k2eAgFnSc3d1
okupaci	okupace	k1gFnSc3
</s>
<s>
Konečným	konečný	k2eAgInSc7d1
momentem	moment	k1gInSc7
Velké	velký	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
létě	léto	k1gNnSc6
1878	#num#	k4
svolaný	svolaný	k2eAgInSc4d1
Berlínský	berlínský	k2eAgInSc4d1
kongres	kongres	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
upravoval	upravovat	k5eAaImAgInS
předchozí	předchozí	k2eAgFnSc4d1
Sanstefanskou	Sanstefanský	k2eAgFnSc4d1
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ukončila	ukončit	k5eAaPmAgFnS
válku	válka	k1gFnSc4
mezi	mezi	k7c7
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
Tureckem	Turecko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
kongresu	kongres	k1gInSc2
byla	být	k5eAaImAgFnS
stabilizace	stabilizace	k1gFnSc1
nových	nový	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
nemalé	malý	k2eNgInPc4d1
územní	územní	k2eAgInPc4d1
zisky	zisk	k1gInPc4
vnímalo	vnímat	k5eAaImAgNnS
Rusko	Rusko	k1gNnSc1
výsledky	výsledek	k1gInPc4
kongresu	kongres	k1gInSc2
jako	jako	k8xS,k8xC
svou	svůj	k3xOyFgFnSc4
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongres	kongres	k1gInSc4
ale	ale	k8xC
pomohl	pomoct	k5eAaPmAgMnS
zajistit	zajistit	k5eAaPmF
relativní	relativní	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
v	v	k7c6
regionu	region	k1gInSc6
na	na	k7c4
dalších	další	k2eAgNnPc2d1
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
postupně	postupně	k6eAd1
na	na	k7c6
Balkáně	Balkán	k1gInSc6
uvolňovala	uvolňovat	k5eAaImAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
stával	stávat	k5eAaImAgInS
místem	místo	k1gNnSc7
skrytého	skrytý	k2eAgNnSc2d1
soupeření	soupeření	k1gNnSc2
evropských	evropský	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
hrálo	hrát	k5eAaImAgNnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
klíčových	klíčový	k2eAgFnPc2d1
rolí	role	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Centrem	centr	k1gInSc7
tohoto	tento	k3xDgNnSc2
soupeření	soupeření	k1gNnSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Srbské	srbský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
mladý	mladý	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
si	se	k3xPyFc3
těžce	těžce	k6eAd1
vydobýval	vydobývat	k5eAaImAgMnS
samostatnost	samostatnost	k1gFnSc4
již	již	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
tradičně	tradičně	k6eAd1
silně	silně	k6eAd1
ekonomicky	ekonomicky	k6eAd1
vázán	vázat	k5eAaImNgMnS
na	na	k7c4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
do	do	k7c2
této	tento	k3xDgFnSc2
země	zem	k1gFnSc2
směřovala	směřovat	k5eAaImAgFnS
většina	většina	k1gFnSc1
srbského	srbský	k2eAgInSc2d1
vývozu	vývoz	k1gInSc2
a	a	k8xC
odtud	odtud	k6eAd1
pocházela	pocházet	k5eAaImAgFnS
i	i	k9
většina	většina	k1gFnSc1
dovozu	dovoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srbský	srbský	k2eAgMnSc1d1
král	král	k1gMnSc1
Milan	Milan	k1gMnSc1
Obrenović	Obrenović	k1gFnSc4
uzavřel	uzavřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1881	#num#	k4
s	s	k7c7
Rakouskem-Uherskem	Rakouskem-Uhersek	k1gInSc7
tajnou	tajný	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
a	a	k8xC
obchodní	obchodní	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
Srbsko	Srbsko	k1gNnSc4
vzdal	vzdát	k5eAaPmAgMnS
nároku	nárok	k1gInSc2
na	na	k7c4
Bosnu	Bosna	k1gFnSc4
a	a	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
a	a	k8xC
též	též	k9
se	se	k3xPyFc4
zavázal	zavázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Srbsko	Srbsko	k1gNnSc1
nebude	být	k5eNaImBp3nS
uzavírat	uzavírat	k5eAaImF,k5eAaPmF
žádné	žádný	k3yNgFnPc4
smlouvy	smlouva	k1gFnPc4
s	s	k7c7
dalšími	další	k2eAgInPc7d1
státy	stát	k1gInPc7
bez	bez	k7c2
předběžného	předběžný	k2eAgInSc2d1
souhlasu	souhlas	k1gInSc2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
dohody	dohoda	k1gFnPc1
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
tajné	tajný	k2eAgFnPc1d1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
srbské	srbský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
ve	v	k7c6
většině	většina	k1gFnSc6
k	k	k7c3
rakouské	rakouský	k2eAgFnSc3d1
monarchii	monarchie	k1gFnSc3
negativní	negativní	k2eAgInSc4d1
vztah	vztah	k1gInSc4
a	a	k8xC
cítilo	cítit	k5eAaImAgNnS
silnou	silný	k2eAgFnSc4d1
náklonnost	náklonnost	k1gFnSc4
k	k	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zveřejnění	zveřejnění	k1gNnSc1
těchto	tento	k3xDgFnPc2
dohod	dohoda	k1gFnPc2
o	o	k7c4
12	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
vyvolalo	vyvolat	k5eAaPmAgNnS
mohutnou	mohutný	k2eAgFnSc4d1
bouři	bouře	k1gFnSc4
nevole	nevole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrofilská	austrofilský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
dál	daleko	k6eAd2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1903	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
spáchán	spáchat	k5eAaPmNgInS
atentát	atentát	k1gInSc1
na	na	k7c4
krále	král	k1gMnSc4
Alexandra	Alexandr	k1gMnSc4
Obrenoviće	Obrenović	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
ženu	žena	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Srbské	srbský	k2eAgNnSc1d1
království	království	k1gNnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
nového	nový	k2eAgMnSc2d1
krále	král	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Karađorđeviće	Karađorđeviće	k1gInSc1
se	se	k3xPyFc4
zahraničněpoliticky	zahraničněpoliticky	k6eAd1
přeorientovalo	přeorientovat	k5eAaPmAgNnS
zejména	zejména	k9
na	na	k7c4
Rusko	Rusko	k1gNnSc4
a	a	k8xC
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6
vyvolalo	vyvolat	k5eAaPmAgNnS
nevoli	nevole	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
stupňovala	stupňovat	k5eAaImAgFnS
kolem	kolem	k7c2
otázky	otázka	k1gFnSc2
budování	budování	k1gNnSc2
nových	nový	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
svým	svůj	k3xOyFgNnSc7
trasováním	trasování	k1gNnSc7
posílit	posílit	k5eAaPmF
vliv	vliv	k1gInSc4
podunajské	podunajský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Rusko	Rusko	k1gNnSc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
francouzského	francouzský	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
prosazovalo	prosazovat	k5eAaImAgNnS
zcela	zcela	k6eAd1
jiné	jiný	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
nových	nový	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
dalšímu	další	k2eAgNnSc3d1
zhoršení	zhoršení	k1gNnSc3
vzájemných	vzájemný	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
vedl	vést	k5eAaImAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Srbsko	Srbsko	k1gNnSc1
rozhodlo	rozhodnout	k5eAaPmAgNnS
odebírat	odebírat	k5eAaImF
vojenský	vojenský	k2eAgInSc4d1
materiál	materiál	k1gInSc4
z	z	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
dříve	dříve	k6eAd2
bylo	být	k5eAaImAgNnS
hlavním	hlavní	k2eAgMnSc7d1
dodavatelem	dodavatel	k1gMnSc7
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spor	spor	k1gInSc1
vedl	vést	k5eAaImAgInS
až	až	k9
tak	tak	k6eAd1
daleko	daleko	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
roku	rok	k1gInSc2
1906	#num#	k4
prasečí	prasečí	k2eAgFnSc4d1
válku	válka	k1gFnSc4
(	(	kIx(
<g/>
obchodní	obchodní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
Srbsko	Srbsko	k1gNnSc1
překvapivě	překvapivě	k6eAd1
uspělo	uspět	k5eAaPmAgNnS
a	a	k8xC
vzájemné	vzájemný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
pak	pak	k6eAd1
byly	být	k5eAaImAgInP
normalizovány	normalizovat	k5eAaBmNgInP
až	až	k9
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konzervativní	konzervativní	k2eAgFnSc1d1
éra	éra	k1gFnSc1
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
a	a	k8xC
období	období	k1gNnSc6
politické	politický	k2eAgFnSc2d1
stability	stabilita	k1gFnSc2
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
</s>
<s>
Vláda	vláda	k1gFnSc1
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
přinesla	přinést	k5eAaPmAgFnS
kromě	kromě	k7c2
stabilizace	stabilizace	k1gFnSc2
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
politiky	politika	k1gFnSc2
též	též	k9
zlepšení	zlepšení	k1gNnSc4
postavení	postavení	k1gNnSc2
češtiny	čeština	k1gFnSc2
u	u	k7c2
rakousko-uherských	rakousko-uherský	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
</s>
<s>
V	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
se	se	k3xPyFc4
se	s	k7c7
zpožděním	zpoždění	k1gNnSc7
dostavily	dostavit	k5eAaPmAgInP
politické	politický	k2eAgInPc4d1
následky	následek	k1gInPc4
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
započaté	započatý	k2eAgFnSc2d1
krachem	krach	k1gInSc7
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
burze	burza	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1873	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
a	a	k8xC
následná	následný	k2eAgFnSc1d1
stagnace	stagnace	k1gFnSc1
hospodářství	hospodářství	k1gNnSc2
podlomila	podlomit	k5eAaPmAgFnS
důvěru	důvěra	k1gFnSc4
v	v	k7c4
liberální	liberální	k2eAgInSc4d1
kapitalismus	kapitalismus	k1gInSc4
předchozí	předchozí	k2eAgFnSc2d1
éry	éra	k1gFnSc2
všeobecné	všeobecný	k2eAgFnSc2d1
konjunktury	konjunktura	k1gFnSc2
a	a	k8xC
s	s	k7c7
koncem	konec	k1gInSc7
důvěry	důvěra	k1gFnSc2
skončila	skončit	k5eAaPmAgFnS
též	též	k9
dominance	dominance	k1gFnSc1
liberálních	liberální	k2eAgFnPc2d1
německých	německý	k2eAgFnPc2d1
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
Ústavní	ústavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1879	#num#	k4
nastoupila	nastoupit	k5eAaPmAgFnS
konzervativně-federalistická	konzervativně-federalistický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vydržela	vydržet	k5eAaPmAgFnS
u	u	k7c2
moci	moc	k1gFnSc2
14	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taaffe	Taaff	k1gInSc5
se	se	k3xPyFc4
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
opíral	opírat	k5eAaImAgMnS
též	též	k9
o	o	k7c4
podporu	podpora	k1gFnSc4
staročeské	staročeský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
dalších	další	k2eAgInPc2d1
českých	český	k2eAgInPc2d1
státoprávně	státoprávně	k6eAd1
orientovaných	orientovaný	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
,	,	kIx,
sdružených	sdružený	k2eAgInPc2d1
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
do	do	k7c2
Českého	český	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnSc1
zástupce	zástupce	k1gMnSc1
po	po	k7c6
jejich	jejich	k3xOp3gInSc6
mnohaletém	mnohaletý	k2eAgInSc6d1
bojkotu	bojkot	k1gInSc6
přivedl	přivést	k5eAaPmAgInS
do	do	k7c2
poslaneckých	poslanecký	k2eAgFnPc2d1
lavic	lavice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejich	jejich	k3xOp3gInSc6
žádost	žádost	k1gFnSc1
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
Stremayrova	Stremayrův	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
nařízení	nařízení	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
používání	používání	k1gNnSc4
Češtiny	čeština	k1gFnSc2
v	v	k7c6
rakouských	rakouský	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
roku	rok	k1gInSc2
1882	#num#	k4
byl	být	k5eAaImAgInS
snížen	snížit	k5eAaPmNgInS
volební	volební	k2eAgInSc1d1
cenzus	cenzus	k1gInSc1
ve	v	k7c6
dvou	dva	k4xCgFnPc6
kuriích	kurie	k1gFnPc6
na	na	k7c6
5	#num#	k4
zlatých	zlatá	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
rozšířen	rozšířen	k2eAgInSc1d1
okruh	okruh	k1gInSc1
oprávněných	oprávněný	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
o	o	k7c4
malopodnikatelské	malopodnikatelský	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
též	též	k9
dosahovalo	dosahovat	k5eAaImAgNnS
prvních	první	k4xOgNnPc6
úspěchů	úspěch	k1gInPc2
dělnické	dělnický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
institucionalizovalo	institucionalizovat	k5eAaImAgNnS
v	v	k7c6
podobě	podoba	k1gFnSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
národních	národní	k2eAgFnPc2d1
odnoží	odnož	k1gFnPc2
rakouské	rakouský	k2eAgFnSc2d1
Sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
položeny	položen	k2eAgInPc1d1
základy	základ	k1gInPc1
sociálního	sociální	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
úrazového	úrazový	k2eAgNnSc2d1
a	a	k8xC
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
zákonných	zákonný	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
k	k	k7c3
ochraně	ochrana	k1gFnSc3
dělnictva	dělnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vídeňský	vídeňský	k2eAgInSc1d1
císařský	císařský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
ale	ale	k8xC
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
–	–	k?
roku	rok	k1gInSc2
1889	#num#	k4
–	–	k?
zasáhla	zasáhnout	k5eAaPmAgFnS
též	též	k9
chmurná	chmurný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
sebevraždě	sebevražda	k1gFnSc6
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
Rudolfa	Rudolf	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc2
I.	I.	kA
Éra	éra	k1gFnSc1
vládnutí	vládnutí	k1gNnSc4
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
politickou	politický	k2eAgFnSc7d1
i	i	k8xC
hospodářskou	hospodářský	k2eAgFnSc7d1
stabilitou	stabilita	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
ale	ale	k8xC
na	na	k7c6
přelomu	přelom	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vytratila	vytratit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
byla	být	k5eAaImAgFnS
neschopnost	neschopnost	k1gFnSc1
zásadně	zásadně	k6eAd1
vyřešit	vyřešit	k5eAaPmF
spory	spor	k1gInPc4
mezi	mezi	k7c7
centralistickým	centralistický	k2eAgNnSc7d1
a	a	k8xC
federalistickým	federalistický	k2eAgNnSc7d1
pojetím	pojetí	k1gNnSc7
státu	stát	k1gInSc2
a	a	k8xC
spory	spor	k1gInPc1
mezi	mezi	k7c7
Čechy	Čech	k1gMnPc7
a	a	k8xC
Němci	Němec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzervativně-liberální	Konzervativně-liberální	k2eAgFnSc1d1
staročeská	staročeský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1891	#num#	k4
drtivě	drtivě	k6eAd1
poražena	poražen	k2eAgFnSc1d1
<g/>
,	,	kIx,
zato	zato	k6eAd1
velký	velký	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
měli	mít	k5eAaImAgMnP
radikálnější	radikální	k2eAgMnPc1d2
národně-demokratičtí	národně-demokratický	k2eAgMnPc1d1
mladočeši	mladočech	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
demisí	demise	k1gFnSc7
Eduarda	Eduard	k1gMnSc2
Taaffeho	Taaffe	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
tak	tak	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
klidné	klidný	k2eAgNnSc1d1
období	období	k1gNnSc1
a	a	k8xC
předlitavská	předlitavský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
se	se	k3xPyFc4
propadla	propadnout	k5eAaPmAgFnS
do	do	k7c2
opakovaných	opakovaný	k2eAgFnPc2d1
krizí	krize	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Uhrách	Uhry	k1gFnPc6
zatím	zatím	k6eAd1
vrcholila	vrcholit	k5eAaImAgFnS
opožděná	opožděný	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
letech	let	k1gInPc6
1867	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
z	z	k7c2
původních	původní	k2eAgInPc2d1
2200	#num#	k4
km	km	kA
zpětinásobila	zpětinásobit	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nakonec	nakonec	k6eAd1
k	k	k7c3
roku	rok	k1gInSc3
1913	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
22	#num#	k4
tisíc	tisíc	k4xCgInPc2
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrostla	vyrůst	k5eAaPmAgFnS
zde	zde	k6eAd1
řada	řada	k1gFnSc1
průmyslových	průmyslový	k2eAgInPc2d1
závodů	závod	k1gInPc2
<g/>
,	,	kIx,
Uhry	Uhry	k1gFnPc4
přesto	přesto	k8xC
zůstávaly	zůstávat	k5eAaImAgFnP
především	především	k6eAd1
agrární	agrární	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
–	–	k?
průmysl	průmysl	k1gInSc4
zde	zde	k6eAd1
k	k	k7c3
roku	rok	k1gInSc3
1910	#num#	k4
zaměstnával	zaměstnávat	k5eAaImAgInS
18	#num#	k4
%	%	kIx~
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
vytvářel	vytvářet	k5eAaImAgInS
ale	ale	k9
čtvrtinu	čtvrtina	k1gFnSc4
národního	národní	k2eAgInSc2d1
důchodu	důchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
až	až	k6eAd1
šestiprocentní	šestiprocentní	k2eAgInSc4d1
roční	roční	k2eAgInSc4d1
růst	růst	k1gInSc4
průmyslu	průmysl	k1gInSc2
a	a	k8xC
dvouprocentní	dvouprocentní	k2eAgInSc4d1
růst	růst	k1gInSc4
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
se	se	k3xPyFc4
vize	vize	k1gFnSc1
rychlého	rychlý	k2eAgNnSc2d1
dohnání	dohnání	k1gNnSc2
tehdejších	tehdejší	k2eAgFnPc2d1
vyspělých	vyspělý	k2eAgFnPc2d1
ekonomik	ekonomika	k1gFnPc2
nedařila	dařit	k5eNaImAgFnS
naplnit	naplnit	k5eAaPmF
a	a	k8xC
Uhry	Uhry	k1gFnPc1
zůstávaly	zůstávat	k5eAaImAgFnP
na	na	k7c6
úrovni	úroveň	k1gFnSc6
40	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
%	%	kIx~
britských	britský	k2eAgMnPc2d1
a	a	k8xC
80	#num#	k4
%	%	kIx~
předlitavských	předlitavský	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
ukazatelů	ukazatel	k1gMnPc2
národního	národní	k2eAgInSc2d1
důchodu	důchod	k1gInSc2
na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1880	#num#	k4
založena	založit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
potýkat	potýkat	k5eAaImF
s	s	k7c7
nepříznivou	příznivý	k2eNgFnSc7d1
legislativou	legislativa	k1gFnSc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
které	který	k3yRgFnSc3,k3yIgFnSc3,k3yQgFnSc3
např.	např.	kA
pracovní	pracovní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
dosahovala	dosahovat	k5eAaImAgFnS
i	i	k9
16	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1890	#num#	k4
rezignoval	rezignovat	k5eAaBmAgInS
na	na	k7c4
post	post	k1gInSc4
ministerského	ministerský	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Kálmán	Kálmán	k2eAgMnSc1d1
Tisza	Tisz	k1gMnSc4
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gFnSc6
rezignaci	rezignace	k1gFnSc6
následovalo	následovat	k5eAaImAgNnS
období	období	k1gNnSc1
permanentní	permanentní	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
však	však	k9
Tiszovou	Tiszový	k2eAgFnSc7d1
rezignací	rezignace	k1gFnSc7
končilo	končit	k5eAaImAgNnS
období	období	k1gNnSc1
liberální	liberální	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k6eAd1
v	v	k7c6
systému	systém	k1gInSc6
evropských	evropský	k2eAgInPc2d1
mezistátních	mezistátní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
</s>
<s>
SS	SS	kA
Wien	Wien	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
357	#num#	k4
BRT	BRT	k?
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jednou	jednou	k6eAd1
z	z	k7c2
největší	veliký	k2eAgFnSc2d3
rakouských	rakouský	k2eAgFnPc2d1
osobních	osobní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
výletům	výlet	k1gInPc3
do	do	k7c2
Orientu	Orient	k1gInSc2
</s>
<s>
Roku	rok	k1gInSc2
1879	#num#	k4
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
obranná	obranný	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
s	s	k7c7
Německým	německý	k2eAgNnSc7d1
císařstvím	císařství	k1gNnSc7
nazývaná	nazývaný	k2eAgFnSc1d1
Dvojspolek	dvojspolek	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
patřila	patřit	k5eAaImAgFnS
do	do	k7c2
řetězce	řetězec	k1gInSc2
Bismarckových	Bismarckův	k2eAgMnPc2d1
tajných	tajný	k1gMnPc2
i	i	k8xC
veřejných	veřejný	k2eAgMnPc2d1
smluvních	smluvní	k2eAgMnPc2d1
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
evropskými	evropský	k2eAgFnPc7d1
mocnostmi	mocnost	k1gFnPc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
měly	mít	k5eAaImAgFnP
zajistit	zajistit	k5eAaPmF
dlouhodobý	dlouhodobý	k2eAgInSc4d1
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
těmto	tento	k3xDgFnPc3
smlouvám	smlouva	k1gFnPc3
též	též	k6eAd1
patřila	patřit	k5eAaImAgFnS
Aliance	aliance	k1gFnSc1
tří	tři	k4xCgMnPc2
císařů	císař	k1gMnPc2
o	o	k7c6
společném	společný	k2eAgInSc6d1
postupu	postup	k1gInSc6
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
a	a	k8xC
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Balkánu	Balkán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
aliance	aliance	k1gFnSc1
se	se	k3xPyFc4
ale	ale	k9
roku	rok	k1gInSc2
1887	#num#	k4
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojspolek	dvojspolek	k1gInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1882	#num#	k4
rozšířen	rozšířit	k5eAaPmNgMnS
přistoupením	přistoupení	k1gNnSc7
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vznikl	vzniknout	k5eAaPmAgInS
Trojspolek	trojspolek	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterým	který	k3yIgFnPc3,k3yQgFnPc3,k3yRgFnPc3
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
přechodně	přechodně	k6eAd1
provázáno	provázat	k5eAaPmNgNnS
též	též	k9
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Španělsko	Španělsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
Bismarck	Bismarck	k1gMnSc1
uzavřel	uzavřít	k5eAaPmAgMnS
různé	různý	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
též	též	k9
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
velkými	velký	k2eAgMnPc7d1
evropskými	evropský	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Bismarckově	Bismarckův	k2eAgInSc6d1
odchodu	odchod	k1gInSc6
z	z	k7c2
politiky	politika	k1gFnSc2
roku	rok	k1gInSc2
1890	#num#	k4
kvůli	kvůli	k7c3
neshodám	neshoda	k1gFnPc3
s	s	k7c7
novým	nový	k2eAgMnSc7d1
německým	německý	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Vilémem	Vilém	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
smluvní	smluvní	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
začaly	začít	k5eAaPmAgInP
postupně	postupně	k6eAd1
rozpadat	rozpadat	k5eAaImF,k5eAaPmF
a	a	k8xC
polarizovat	polarizovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původní	původní	k2eAgFnSc1d1
nepřehledná	přehledný	k2eNgFnSc1d1
směsice	směsice	k1gFnSc1
nejrůznějších	různý	k2eAgFnPc2d3
mezinárodních	mezinárodní	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
různými	různý	k2eAgNnPc7d1
způsoby	způsob	k1gInPc4
propojovaly	propojovat	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
významné	významný	k2eAgFnPc1d1
evropské	evropský	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
postupně	postupně	k6eAd1
redukovala	redukovat	k5eAaBmAgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
základní	základní	k2eAgInPc4d1
vojensko-politické	vojensko-politický	k2eAgInPc4d1
bloky	blok	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c4
nesourodou	sourodý	k2eNgFnSc4d1
Dohodu	dohoda	k1gFnSc4
na	na	k7c6
straně	strana	k1gFnSc6
jedné	jeden	k4xCgFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
sdružovala	sdružovat	k5eAaImAgFnS
zejména	zejména	k9
Rusko	Rusko	k1gNnSc4
<g/>
,	,	kIx,
Velkou	velký	k2eAgFnSc4d1
Británii	Británie	k1gFnSc4
a	a	k8xC
Francii	Francie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
Dohoda	dohoda	k1gFnSc1
stála	stát	k5eAaImAgFnS
proti	proti	k7c3
původně	původně	k6eAd1
obrannému	obranný	k2eAgInSc3d1
Trojspolku	trojspolek	k1gInSc3
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Bosenská	bosenský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
hrálo	hrát	k5eAaImAgNnS
v	v	k7c6
tomto	tento	k3xDgInSc6
procesu	proces	k1gInSc6
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgFnPc3
aspiracím	aspirace	k1gFnPc3
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1878	#num#	k4
se	se	k3xPyFc4
monarchie	monarchie	k1gFnSc1
chystala	chystat	k5eAaImAgFnS
okupovanou	okupovaný	k2eAgFnSc4d1
Bosnu	Bosna	k1gFnSc4
a	a	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
přímo	přímo	k6eAd1
anektovat	anektovat	k5eAaBmF
a	a	k8xC
po	po	k7c6
územních	územní	k2eAgFnPc6d1
ztrátách	ztráta	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
císař	císař	k1gMnSc1
musel	muset	k5eAaImAgMnS
podstoupit	podstoupit	k5eAaPmF
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
by	by	kYmCp3nS
tak	tak	k9
jeho	jeho	k3xOp3gFnSc1
země	země	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
ke	k	k7c3
značnému	značný	k2eAgInSc3d1
územnímu	územní	k2eAgInSc3d1
zisku	zisk	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
rakouské	rakouský	k2eAgFnPc4d1
aspirace	aspirace	k1gFnPc4
však	však	k9
narážely	narážet	k5eAaPmAgFnP,k5eAaImAgFnP
na	na	k7c4
zájmy	zájem	k1gInPc4
Ruska	Rusko	k1gNnSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
nového	nový	k2eAgMnSc2d1
chráněnce	chráněnec	k1gMnSc2
–	–	k?
Srbska	Srbsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
se	se	k3xPyFc4
k	k	k7c3
anexi	anexe	k1gFnSc3
Bosny	Bosna	k1gFnSc2
rozhodlo	rozhodnout	k5eAaPmAgNnS
roku	rok	k1gInSc2
1908	#num#	k4
–	–	k?
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Turecko	Turecko	k1gNnSc1
bylo	být	k5eAaImAgNnS
oslabeno	oslabit	k5eAaPmNgNnS
mladotureckou	mladoturecký	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
využilo	využít	k5eAaPmAgNnS
i	i	k9
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stalo	stát	k5eAaPmAgNnS
významným	významný	k2eAgMnSc7d1
rakousko-uherským	rakousko-uherský	k2eAgMnSc7d1
vojenským	vojenský	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
by	by	kYmCp3nS
sice	sice	k8xC
s	s	k7c7
anexí	anexe	k1gFnSc7
souhlasilo	souhlasit	k5eAaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
kladlo	klást	k5eAaImAgNnS
si	se	k3xPyFc3
své	svůj	k3xOyFgFnPc4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
jenže	jenže	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
tento	tento	k3xDgInSc4
akt	akt	k1gInSc4
vyhlásilo	vyhlásit	k5eAaPmAgNnS
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyjednávání	vyjednávání	k1gNnSc1
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
teprve	teprve	k6eAd1
začínala	začínat	k5eAaImAgFnS
a	a	k8xC
carský	carský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
rovnou	rovnou	k6eAd1
před	před	k7c4
hotovou	hotový	k2eAgFnSc4d1
věc	věc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-rakouské	rusko-rakouský	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
silně	silně	k6eAd1
ochladly	ochladnout	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1912	#num#	k4
vyhlásily	vyhlásit	k5eAaPmAgInP
malé	malý	k2eAgInPc1d1
balkánské	balkánský	k2eAgInPc1d1
státy	stát	k1gInPc1
válku	válka	k1gFnSc4
Osmanské	osmanský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
s	s	k7c7
cílem	cíl	k1gInSc7
získat	získat	k5eAaPmF
další	další	k2eAgNnSc4d1
území	území	k1gNnSc4
na	na	k7c4
úkor	úkor	k1gInSc4
tohoto	tento	k3xDgNnSc2
slábnoucího	slábnoucí	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koalice	koalice	k1gFnSc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
,	,	kIx,
Srbska	Srbsko	k1gNnSc2
<g/>
,	,	kIx,
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
a	a	k8xC
Řecka	Řecko	k1gNnSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
rychlého	rychlý	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
a	a	k8xC
všechny	všechen	k3xTgInPc1
státy	stát	k1gInPc1
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
územně	územně	k6eAd1
obohatily	obohatit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
zde	zde	k6eAd1
ale	ale	k8xC
podporou	podpora	k1gFnSc7
vzniku	vznik	k1gInSc2
albánského	albánský	k2eAgInSc2d1
státu	stát	k1gInSc2
zabránilo	zabránit	k5eAaPmAgNnS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Srbsko	Srbsko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
přístup	přístup	k1gInSc4
k	k	k7c3
Jaderskému	jaderský	k2eAgNnSc3d1
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
uzavření	uzavření	k1gNnSc6
míru	mír	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
mezi	mezi	k7c7
dřívějšími	dřívější	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
k	k	k7c3
roztržce	roztržka	k1gFnSc3
o	o	k7c6
rozdělení	rozdělení	k1gNnSc6
území	území	k1gNnSc2
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1913	#num#	k4
vyhlášena	vyhlásit	k5eAaPmNgFnS
druhá	druhý	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
Bulharsko	Bulharsko	k1gNnSc1
snažilo	snažit	k5eAaImAgNnS
získat	získat	k5eAaPmF
větší	veliký	k2eAgInSc4d2
díl	díl	k1gInSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
válce	válka	k1gFnSc6
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
podporovalo	podporovat	k5eAaImAgNnS
právě	právě	k9
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
ale	ale	k9
ve	v	k7c6
válce	válka	k1gFnSc6
proti	proti	k7c3
Srbsku	Srbsko	k1gNnSc3
a	a	k8xC
dalším	další	k2eAgMnPc3d1
spojencům	spojenec	k1gMnPc3
brzy	brzy	k6eAd1
prohrálo	prohrát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Balkánském	balkánský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
vytvořil	vytvořit	k5eAaPmAgInS
další	další	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
mezistátního	mezistátní	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Itálie	Itálie	k1gFnSc1
nakonec	nakonec	k6eAd1
z	z	k7c2
Trojspolku	trojspolek	k1gInSc2
vystoupila	vystoupit	k5eAaPmAgFnS
a	a	k8xC
tento	tento	k3xDgInSc4
pakt	pakt	k1gInSc4
se	se	k3xPyFc4
pak	pak	k6eAd1
přejmenoval	přejmenovat	k5eAaPmAgMnS
na	na	k7c4
Centrální	centrální	k2eAgMnPc4d1
(	(	kIx(
<g/>
či	či	k8xC
Ústřední	ústřední	k2eAgFnSc2d1
<g/>
)	)	kIx)
mocnosti	mocnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
mnozí	mnohý	k2eAgMnPc1d1
evropští	evropský	k2eAgMnPc1d1
armádní	armádní	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
někteří	některý	k3yIgMnPc1
politici	politik	k1gMnPc1
<g/>
,	,	kIx,
mluvili	mluvit	k5eAaImAgMnP
o	o	k7c6
nevyhnutelnosti	nevyhnutelnost	k1gFnSc6
velké	velký	k2eAgFnSc2d1
evropské	evropský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
země	zem	k1gFnSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
kavárenská	kavárenský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
</s>
<s>
I	i	k9
Budapešť	Budapešť	k1gFnSc1
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
jako	jako	k9
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
světových	světový	k2eAgFnPc2d1
metropolí	metropol	k1gFnPc2
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Ortleru	Ortler	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
3	#num#	k4
905	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
</s>
<s>
Období	období	k1gNnSc1
permanentní	permanentní	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
v	v	k7c6
obou	dva	k4xCgFnPc6
částech	část	k1gFnPc6
monarchie	monarchie	k1gFnSc2
nepřerušilo	přerušit	k5eNaPmAgNnS
ekonomický	ekonomický	k2eAgMnSc1d1
i	i	k8xC
společenský	společenský	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomika	ekonomika	k1gFnSc1
rostla	růst	k5eAaImAgFnS
politice	politika	k1gFnSc3
navzdory	navzdory	k7c3
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
i	i	k9
lidská	lidský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostla	růst	k5eAaImAgNnP
především	především	k9
průmyslová	průmyslový	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
mířilo	mířit	k5eAaImAgNnS
množství	množství	k1gNnSc1
venkovského	venkovský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
za	za	k7c7
výdělkem	výdělek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měla	mít	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
budoucí	budoucí	k2eAgFnSc2d1
ostravské	ostravský	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
jen	jen	k9
12	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
roztroušených	roztroušený	k2eAgMnPc2d1
do	do	k7c2
23	#num#	k4
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
k	k	k7c3
roku	rok	k1gInSc3
1880	#num#	k4
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
zvýšil	zvýšit	k5eAaPmAgInS
na	na	k7c4
32	#num#	k4
tisíc	tisíc	k4xCgInPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
40	#num#	k4
tisíc	tisíc	k4xCgInSc4
osob	osoba	k1gFnPc2
zde	zde	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
zaměstnání	zaměstnání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgInPc1d1
Brno	Brno	k1gNnSc4
na	na	k7c6
přelomu	přelom	k1gInSc6
letopočtu	letopočet	k1gInSc2
překročilo	překročit	k5eAaPmAgNnS
stotisícovou	stotisícový	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
a	a	k8xC
Praha	Praha	k1gFnSc1
dvousettisícovou	dvousettisícový	k2eAgFnSc4d1
(	(	kIx(
<g/>
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
početnými	početný	k2eAgFnPc7d1
předměstími	předměstí	k1gNnPc7
dosahovala	dosahovat	k5eAaImAgFnS
pražská	pražský	k2eAgFnSc1d1
aglomerace	aglomerace	k1gFnSc1
již	již	k6eAd1
půlmilionové	půlmilionový	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Nic	nic	k3yNnSc1
se	se	k3xPyFc4
však	však	k9
nevyrovnalo	vyrovnat	k5eNaPmAgNnS
růstu	růst	k1gInSc2
obou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
metropolí	metropol	k1gFnPc2
–	–	k?
Vídně	Vídeň	k1gFnSc2
a	a	k8xC
Budapešti	Budapešť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
Budapešti	Budapešť	k1gFnSc2
vzrostl	vzrůst	k5eAaPmAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1869	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
z	z	k7c2
270	#num#	k4
tisíc	tisíc	k4xCgInPc2
na	na	k7c4
880	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Vídně	Vídeň	k1gFnSc2
ze	z	k7c2
431	#num#	k4
tisíc	tisíc	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
na	na	k7c4
2,2	2,2	k4
milionu	milion	k4xCgInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Vídeň	Vídeň	k1gFnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
patřila	patřit	k5eAaImAgFnS
mezi	mezi	k7c4
šest	šest	k4xCc4
největších	veliký	k2eAgFnPc2d3
<g />
.	.	kIx.
</s>
<s hack="1">
měst	město	k1gNnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
9	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
početní	početní	k2eAgInSc1d1
stav	stav	k1gInSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
v	v	k7c6
letech	let	k1gInPc6
1869	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
zvýšil	zvýšit	k5eAaPmAgInS
z	z	k7c2
35	#num#	k4
812	#num#	k4
000	#num#	k4
na	na	k7c4
51	#num#	k4
390	#num#	k4
000	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1870	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
rostlo	růst	k5eAaImAgNnS
HDP	HDP	kA
ročně	ročně	k6eAd1
zhruba	zhruba	k6eAd1
o	o	k7c4
1,76	1,76	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
růst	růst	k1gInSc4
byl	být	k5eAaImAgInS
větší	veliký	k2eAgMnSc1d2
než	než	k8xS
v	v	k7c6
Británii	Británie	k1gFnSc6
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Francii	Francie	k1gFnSc4
(	(	kIx(
<g/>
1,06	1,06	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
1,51	1,51	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
kulturních	kulturní	k2eAgNnPc2d1
a	a	k8xC
vědeckých	vědecký	k2eAgNnPc2d1
center	centrum	k1gNnPc2
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
zde	zde	k6eAd1
psycholog	psycholog	k1gMnSc1
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
Gustav	Gustav	k1gMnSc1
Mahler	Mahler	k1gMnSc1
nebo	nebo	k8xC
Johannes	Johannes	k1gMnSc1
Brahms	Brahms	k1gMnSc1
a	a	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
význačných	význačný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vídeň	Vídeň	k1gFnSc1
již	již	k9
od	od	k7c2
dob	doba	k1gFnPc2
oživení	oživení	k1gNnSc2
po	po	k7c6
prusko-rakouské	prusko-rakouský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
přetvářeli	přetvářet	k5eAaImAgMnP
vynikající	vynikající	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
urbanizační	urbanizační	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
podpořila	podpořit	k5eAaPmAgFnS
kulturní	kulturní	k2eAgNnSc4d1
vzepětí	vzepětí	k1gNnSc4
města	město	k1gNnSc2
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgInSc3
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Vídeň	Vídeň	k1gFnSc1
přelomu	přelom	k1gInSc2
století	století	k1gNnSc2
dodnes	dodnes	k6eAd1
zajímavým	zajímavý	k2eAgInSc7d1
fenoménem	fenomén	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ani	ani	k8xC
Budapešť	Budapešť	k1gFnSc1
nezůstávala	zůstávat	k5eNaImAgFnS
pozadu	pozadu	k6eAd1
a	a	k8xC
v	v	k7c6
jejích	její	k3xOp3gFnPc6
nesčetných	sčetný	k2eNgFnPc6d1
kavárnách	kavárna	k1gFnPc6
se	se	k3xPyFc4
scházeli	scházet	k5eAaImAgMnP
k	k	k7c3
debatám	debata	k1gFnPc3
o	o	k7c6
umění	umění	k1gNnSc6
či	či	k8xC
politice	politika	k1gFnSc6
členové	člen	k1gMnPc1
místní	místní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
působila	působit	k5eAaImAgFnS
řada	řada	k1gFnSc1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
od	od	k7c2
národního	národní	k2eAgInSc2d1
romantismu	romantismus	k1gInSc2
přecházeli	přecházet	k5eAaImAgMnP
k	k	k7c3
uměleckým	umělecký	k2eAgFnPc3d1
formám	forma	k1gFnPc3
ve	v	k7c6
stylu	styl	k1gInSc6
fin	fin	k?
de	de	k?
siè	siè	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
příležitosti	příležitost	k1gFnSc2
tisíciletého	tisíciletý	k2eAgNnSc2d1
výročí	výročí	k1gNnSc2
příchodů	příchod	k1gInPc2
Maďarů	Maďar	k1gMnPc2
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
metro	metro	k1gNnSc1
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
elektrická	elektrický	k2eAgFnSc1d1
podzemní	podzemní	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
druhá	druhý	k4xOgFnSc1
trasa	trasa	k1gFnSc1
metra	metro	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přelom	přelom	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
také	také	k9
obdobím	období	k1gNnSc7
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
spolkového	spolkový	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spousty	spousta	k1gFnSc2
členů	člen	k1gInPc2
nabíraly	nabírat	k5eAaImAgInP
spolky	spolek	k1gInPc1
okrašlovací	okrašlovací	k2eAgInPc1d1
<g/>
,	,	kIx,
muzejní	muzejní	k2eAgNnPc1d1
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgNnPc1d1
<g/>
,	,	kIx,
tělovýchovné	tělovýchovný	k2eAgNnSc1d1
a	a	k8xC
další	další	k2eAgNnSc1d1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
mnohé	mnohý	k2eAgInPc4d1
již	již	k9
roky	rok	k1gInPc4
existovaly	existovat	k5eAaImAgFnP
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
dalších	další	k2eAgMnPc2d1
byla	být	k5eAaImAgFnS
zakládána	zakládat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
též	též	k9
sdružovali	sdružovat	k5eAaImAgMnP
v	v	k7c6
četných	četný	k2eAgFnPc6d1
neformálních	formální	k2eNgFnPc6d1
stolních	stolní	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
nebo	nebo	k8xC
různých	různý	k2eAgNnPc6d1
sportovních	sportovní	k2eAgNnPc6d1
družstvech	družstvo	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Badeniho	Badeni	k1gMnSc4
volební	volební	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1896	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
možnost	možnost	k1gFnSc1
volit	volit	k5eAaImF
rozšířila	rozšířit	k5eAaPmAgFnS
na	na	k7c4
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
zkonsolidovala	zkonsolidovat	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
odnož	odnož	k1gFnSc1
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
získávat	získávat	k5eAaImF
značný	značný	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc3
sílily	sílit	k5eAaImAgFnP
i	i	k9
profesní	profesní	k2eAgFnPc1d1
odborové	odborový	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českoslovanská	českoslovanský	k2eAgFnSc1d1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
dělnická	dělnický	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
provázána	provázat	k5eAaPmNgFnS
s	s	k7c7
rakouskou	rakouský	k2eAgFnSc7d1
sociální	sociální	k2eAgFnSc7d1
demokracií	demokracie	k1gFnSc7
se	se	k3xPyFc4
sídlem	sídlo	k1gNnSc7
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1893	#num#	k4
byla	být	k5eAaImAgFnS
českoslovanská	českoslovanský	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
samostatnou	samostatný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
se	se	k3xPyFc4
sdružovaly	sdružovat	k5eAaImAgFnP
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
s	s	k7c7
šesti	šest	k4xCc7
členy	člen	k1gInPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
německých	německý	k2eAgFnPc2d1
odnoží	odnož	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Uhrách	Uhry	k1gFnPc6
byla	být	k5eAaImAgFnS
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1890	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
neněmeckých	německý	k2eNgFnPc6d1
sociálních	sociální	k2eAgFnPc6d1
demokraciích	demokracie	k1gFnPc6
<g />
.	.	kIx.
</s>
<s hack="1">
draly	drát	k5eAaImAgInP
napovrch	napovrch	k6eAd1
národnostní	národnostní	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
zůstávaly	zůstávat	k5eAaImAgInP
jedněmi	jeden	k4xCgFnPc7
z	z	k7c2
mála	málo	k4c2
celorakouských	celorakouský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nevnímaly	vnímat	k5eNaImAgFnP
německý	německý	k2eAgInSc4d1
živel	živel	k1gInSc4
apriori	apriori	k6eAd1
negativně	negativně	k6eAd1
a	a	k8xC
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
existenci	existence	k1gFnSc6
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
nenalézaly	nalézat	k5eNaImAgInP
příčinu	příčina	k1gFnSc4
nějakého	nějaký	k3yIgInSc2
útisku	útisk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
šly	jít	k5eAaImAgInP
ale	ale	k9
značně	značně	k6eAd1
proti	proti	k7c3
proudu	proud	k1gInSc3
myšlení	myšlení	k1gNnSc2
podstatné	podstatný	k2eAgFnSc2d1
části	část	k1gFnSc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
odštěpení	odštěpení	k1gNnSc2
nacionálně	nacionálně	k6eAd1
radikálnější	radikální	k2eAgFnSc2d2
socialistické	socialistický	k2eAgFnSc2d1
frakce	frakce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1905	#num#	k4
se	se	k3xPyFc4
zintenzivnil	zintenzivnit	k5eAaPmAgInS
boj	boj	k1gInSc1
o	o	k7c4
všeobecné	všeobecný	k2eAgNnSc4d1
a	a	k8xC
rovné	rovný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yRgNnSc2,k3yIgNnSc2,k3yQgNnSc2
bylo	být	k5eAaImAgNnS
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
nakonec	nakonec	k6eAd1
poprvé	poprvé	k6eAd1
voleno	volit	k5eAaImNgNnS
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládané	předpokládaný	k2eAgNnSc4d1
zklidnění	zklidnění	k1gNnSc4
politické	politický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
nekonalo	konat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dlouho	dlouho	k6eAd1
připravované	připravovaný	k2eAgFnSc3d1
a	a	k8xC
dlouho	dlouho	k6eAd1
odkládané	odkládaný	k2eAgFnSc6d1
anexi	anexe	k1gFnSc6
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
kromě	kromě	k7c2
velkého	velký	k2eAgNnSc2d1
mezistátního	mezistátní	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
rozbouřila	rozbouřit	k5eAaPmAgFnS
i	i	k8xC
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
nové	nový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
totiž	totiž	k9
nacházela	nacházet	k5eAaImAgFnS
pestrá	pestrý	k2eAgFnSc1d1
směsice	směsice	k1gFnSc1
národností	národnost	k1gFnPc2
a	a	k8xC
náboženství	náboženství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chorvati	Chorvat	k1gMnPc1
anexi	anexe	k1gFnSc4
přivítali	přivítat	k5eAaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
bosenští	bosenský	k2eAgMnPc1d1
Chorvati	Chorvat	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
společné	společný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Srbové	Srbové	k2eAgFnPc1d1
nikoliv	nikoliv	k9
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
samostatné	samostatný	k2eAgNnSc1d1
Srbsko	Srbsko	k1gNnSc1
již	již	k6eAd1
za	za	k7c7
hranicemi	hranice	k1gFnPc7
monarchie	monarchie	k1gFnSc2
existovalo	existovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitele	představitel	k1gMnPc4
dalších	další	k2eAgInPc2d1
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
dráždila	dráždit	k5eAaImAgFnS
nerozhodnost	nerozhodnost	k1gFnSc1
císařské	císařský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
nevěděla	vědět	k5eNaImAgFnS
rady	rada	k1gFnPc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kam	kam	k6eAd1
Bosnu	Bosna	k1gFnSc4
zařadit	zařadit	k5eAaPmF
<g/>
,	,	kIx,
jestli	jestli	k8xS
k	k	k7c3
Předlitavsku	Předlitavsko	k1gNnSc3
nebo	nebo	k8xC
Zalitavsku	Zalitavsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgFnPc6
částech	část	k1gFnPc6
monarchie	monarchie	k1gFnSc2
totiž	totiž	k9
panovala	panovat	k5eAaImAgFnS
obava	obava	k1gFnSc1
z	z	k7c2
růstu	růst	k1gInSc2
slovanského	slovanský	k2eAgInSc2d1
živlu	živel	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
se	s	k7c7
přičleněním	přičlenění	k1gNnSc7
k	k	k7c3
jedné	jeden	k4xCgFnSc2
nebo	nebo	k8xC
druhé	druhý	k4xOgFnSc2
části	část	k1gFnSc2
státu	stát	k1gInSc2
stal	stát	k5eAaPmAgMnS
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
početně	početně	k6eAd1
dominantním	dominantní	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občané	občan	k1gMnPc1
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
zůstávali	zůstávat	k5eAaImAgMnP
v	v	k7c6
postavení	postavení	k1gNnSc6
neplnoprávných	plnoprávný	k2eNgMnPc2d1
zemských	zemský	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
značné	značný	k2eAgFnPc4d1
investice	investice	k1gFnPc4
do	do	k7c2
nové	nový	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
růst	růst	k1gInSc4
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
snahu	snaha	k1gFnSc4
přiblížit	přiblížit	k5eAaPmF
tento	tento	k3xDgInSc4
orientální	orientální	k2eAgInSc4d1
svět	svět	k1gInSc4
západním	západní	k2eAgInPc3d1
životním	životní	k2eAgInPc3d1
standardům	standard	k1gInPc3
<g/>
,	,	kIx,
rostlo	růst	k5eAaImAgNnS
i	i	k9
zde	zde	k6eAd1
nacionální	nacionální	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
To	ten	k3xDgNnSc1
nakonec	nakonec	k6eAd1
rostlo	růst	k5eAaImAgNnS
i	i	k9
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
částech	část	k1gFnPc6
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
anexi	anexe	k1gFnSc6
se	se	k3xPyFc4
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
smysluplnosti	smysluplnost	k1gFnSc2
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
rozloučil	rozloučit	k5eAaPmAgInS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
posledních	poslední	k2eAgMnPc2d1
významných	významný	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
austroslavismu	austroslavismus	k1gInSc2
–	–	k?
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gFnSc2
Masaryk	Masaryk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němečtí	německý	k2eAgMnPc1d1
a	a	k8xC
čeští	český	k2eAgMnPc1d1
nacionalisté	nacionalista	k1gMnPc1
se	se	k3xPyFc4
předháněli	předhánět	k5eAaImAgMnP
v	v	k7c6
útocích	útok	k1gInPc6
na	na	k7c4
druhou	druhý	k4xOgFnSc4
národnost	národnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
příručce	příručka	k1gFnSc6
pro	pro	k7c4
členy	člen	k1gMnPc4
tělovýchovného	tělovýchovný	k2eAgMnSc4d1
Sokola	Sokol	k1gMnSc4
z	z	k7c2
přelomu	přelom	k1gInSc2
století	století	k1gNnSc2
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
doporučení	doporučení	k1gNnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
Sokolové	Sokol	k1gMnPc1
úplně	úplně	k6eAd1
odmítali	odmítat	k5eAaImAgMnP
jakoukoliv	jakýkoliv	k3yIgFnSc4
komunikaci	komunikace	k1gFnSc4
v	v	k7c6
němčině	němčina	k1gFnSc6
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
nakonec	nakonec	k6eAd1
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
odmítali	odmítat	k5eAaImAgMnP
veškeré	veškerý	k3xTgNnSc4
nečeské	český	k2eNgNnSc4d1
zboží	zboží	k1gNnSc4
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Naproti	naproti	k7c3
tomu	ten	k3xDgInSc3
němečtí	německý	k2eAgMnPc1d1
nacionalisté	nacionalista	k1gMnPc1
přednášeli	přednášet	k5eAaImAgMnP
v	v	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
takřka	takřka	k6eAd1
vlastizrádné	vlastizrádný	k2eAgInPc1d1
projevy	projev	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jeden	jeden	k4xCgMnSc1
dokonce	dokonce	k9
skončil	skončit	k5eAaPmAgMnS
provoláním	provolání	k1gNnSc7
„	„	k?
<g/>
Pryč	pryč	k1gFnSc1
s	s	k7c7
Habsburky	Habsburk	k1gMnPc7
<g/>
“	“	k?
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
zfanatizovaní	zfanatizovaný	k2eAgMnPc1d1
němečtí	německý	k2eAgMnPc1d1
radikální	radikální	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
povstávali	povstávat	k5eAaImAgMnP
a	a	k8xC
hrdě	hrdě	k6eAd1
se	se	k3xPyFc4
prohlašovali	prohlašovat	k5eAaImAgMnP
za	za	k7c4
vlastizrádce	vlastizrádce	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
slovenské	slovenský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
Černová	Černový	k2eAgFnSc1d1
se	se	k3xPyFc4
při	při	k7c6
vysvěcování	vysvěcování	k1gNnSc6
kostela	kostel	k1gInSc2
obyvatelstvo	obyvatelstvo	k1gNnSc1
dožadovalo	dožadovat	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mši	mše	k1gFnSc4
vedl	vést	k5eAaImAgMnS
místní	místní	k2eAgMnSc1d1
rodák	rodák	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
však	však	k9
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
za	za	k7c4
pobuřování	pobuřování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nepokojům	nepokoj	k1gInPc3
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
uherští	uherský	k2eAgMnPc1d1
četníci	četník	k1gMnPc1
zastřelili	zastřelit	k5eAaPmAgMnP
15	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
řadu	řada	k1gFnSc4
dalších	další	k2eAgFnPc2d1
zranili	zranit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neštěstí	neštěstí	k1gNnSc1
vzbudilo	vzbudit	k5eAaPmAgNnS
pobouření	pobouření	k1gNnSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
přispělo	přispět	k5eAaPmAgNnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
sblížení	sblížení	k1gNnSc3
českého	český	k2eAgInSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
slovanští	slovanský	k2eAgMnPc1d1
myslitelé	myslitel	k1gMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
též	též	k9
blouznili	blouznit	k5eAaImAgMnP
o	o	k7c6
spojení	spojení	k1gNnSc6
slovanských	slovanský	k2eAgNnPc2d1
území	území	k1gNnPc2
s	s	k7c7
carským	carský	k2eAgNnSc7d1
Ruskem	Rusko	k1gNnSc7
(	(	kIx(
<g/>
panslavismus	panslavismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
snili	snít	k5eAaImAgMnP
především	především	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
neměli	mít	k5eNaImAgMnP
možnost	možnost	k1gFnSc4
se	se	k3xPyFc4
seznámit	seznámit	k5eAaPmF
s	s	k7c7
realitou	realita	k1gFnSc7
ruského	ruský	k2eAgNnSc2d1
samoděržaví	samoděržaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
spojení	spojení	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
s	s	k7c7
Německým	německý	k2eAgNnSc7d1
císařstvím	císařství	k1gNnSc7
snili	snít	k5eAaImAgMnP
zase	zase	k9
pangermanisté	pangermanista	k1gMnPc1
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
Maďaři	Maďar	k1gMnPc1
snili	snít	k5eAaImAgMnP
o	o	k7c6
samostatných	samostatný	k2eAgFnPc6d1
Uhrách	Uhry	k1gFnPc6
<g/>
,	,	kIx,
Poláci	Polák	k1gMnPc1
o	o	k7c6
obnově	obnova	k1gFnSc6
svého	svůj	k3xOyFgInSc2
historického	historický	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
Rumuni	Rumun	k1gMnPc1
a	a	k8xC
Jihoslované	Jihoslovan	k1gMnPc1
o	o	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
již	již	k6eAd1
existujícím	existující	k2eAgInPc3d1
národním	národní	k2eAgInPc3d1
státům	stát	k1gInPc3
za	za	k7c7
jižními	jižní	k2eAgFnPc7d1
hranicemi	hranice	k1gFnPc7
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
předvečer	předvečer	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
víc	hodně	k6eAd2
elementů	element	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
si	se	k3xPyFc3
další	další	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
nepřály	přát	k5eNaImAgInP
<g/>
,	,	kIx,
než	než	k8xS
těch	ten	k3xDgFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
si	se	k3xPyFc3
přály	přát	k5eAaImAgFnP
jeho	jeho	k3xOp3gNnSc4
zachování	zachování	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
říše	říše	k1gFnSc1
se	se	k3xPyFc4
zmítala	zmítat	k5eAaImAgFnS
v	v	k7c6
nekončících	končící	k2eNgInPc6d1
bezcílných	bezcílný	k2eAgInPc6d1
bouřlivých	bouřlivý	k2eAgInPc6d1
národnostních	národnostní	k2eAgInPc6d1
svárech	svár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Zánik	zánik	k1gInSc1
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
a	a	k8xC
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
za	za	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Místní	místní	k2eAgInSc1d1
trh	trh	k1gInSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
česká	český	k2eAgFnSc1d1
šlechtična	šlechtična	k1gFnSc1
Žofie	Žofie	k1gFnSc1
Chotková	Chotková	k1gFnSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
torpédovka	torpédovka	k1gFnSc1
74T	74T	k4
někdy	někdy	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1914	#num#	k4
až	až	k9
1917	#num#	k4
</s>
<s>
Rakousko-uherští	rakousko-uherský	k2eAgMnPc1d1
odvedenci	odvedenec	k1gMnPc1
v	v	k7c6
Chomutově	Chomutov	k1gInSc6
nastupují	nastupovat	k5eAaImIp3nP
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
mnozí	mnohý	k2eAgMnPc1d1
s	s	k7c7
nadšením	nadšení	k1gNnSc7
<g/>
,	,	kIx,
po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
už	už	k6eAd1
měli	mít	k5eAaImAgMnP
zabíjení	zabíjení	k1gNnSc2
<g/>
,	,	kIx,
hladu	hlad	k1gInSc2
a	a	k8xC
nedostatku	nedostatek	k1gInSc2
dost	dost	k6eAd1
téměř	téměř	k6eAd1
všichni	všechen	k3xTgMnPc1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
národnost	národnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maďary	maďar	k1gInPc7
čekal	čekat	k5eAaImAgMnS
po	po	k7c6
válce	válka	k1gFnSc6
ještě	ještě	k9
další	další	k2eAgInSc4d1
prohraný	prohraný	k2eAgInSc4d1
boj	boj	k1gInSc4
o	o	k7c4
zachování	zachování	k1gNnSc4
svého	svůj	k3xOyFgInSc2
historického	historický	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1914	#num#	k4
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
na	na	k7c4
cestu	cesta	k1gFnSc4
z	z	k7c2
Chlumu	chlum	k1gInSc2
u	u	k7c2
Třeboně	Třeboň	k1gFnSc2
na	na	k7c4
císařské	císařský	k2eAgInPc4d1
manévry	manévr	k1gInPc4
v	v	k7c6
Bosně	Bosna	k1gFnSc6
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Žofie	Žofie	k1gFnSc1
Chotková	Chotková	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
návštěvě	návštěva	k1gFnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
země	zem	k1gFnSc2
Sarajeva	Sarajevo	k1gNnSc2
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
byli	být	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
zabiti	zabit	k2eAgMnPc1d1
při	při	k7c6
atentátu	atentát	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
provedla	provést	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
srbských	srbský	k2eAgMnPc2d1
nacionalistů	nacionalista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
11	#num#	k4
<g/>
]	]	kIx)
Atentát	atentát	k1gInSc1
způsobil	způsobit	k5eAaPmAgInS
šok	šok	k1gInSc4
nejen	nejen	k6eAd1
v	v	k7c6
samotném	samotný	k2eAgInSc6d1
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
celé	celý	k2eAgFnSc6d1
evropské	evropský	k2eAgFnSc6d1
politické	politický	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německý	německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
považoval	považovat	k5eAaImAgMnS
Františka	František	k1gMnSc4
Ferdinanda	Ferdinand	k1gMnSc4
za	za	k7c4
přítele	přítel	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
ho	on	k3xPp3gInSc4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
smrt	smrt	k1gFnSc1
osobně	osobně	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
německé	německý	k2eAgFnSc6d1
i	i	k8xC
rakousko-uherské	rakousko-uherský	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
ovšem	ovšem	k9
existovalo	existovat	k5eAaImAgNnS
tzv.	tzv.	kA
válečné	válečný	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
propojené	propojený	k2eAgNnSc1d1
s	s	k7c7
armádou	armáda	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
tito	tento	k3xDgMnPc1
lidé	člověk	k1gMnPc1
hledali	hledat	k5eAaImAgMnP
vhodnou	vhodný	k2eAgFnSc4d1
záminku	záminka	k1gFnSc4
pro	pro	k7c4
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
existovalo	existovat	k5eAaImAgNnS
důvodné	důvodný	k2eAgNnSc4d1
podezření	podezření	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
atentátníci	atentátník	k1gMnPc1
byli	být	k5eAaImAgMnP
řízeni	řídit	k5eAaImNgMnP
z	z	k7c2
nepřátelského	přátelský	k2eNgNnSc2d1
Srbska	Srbsko	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
proto	proto	k8xC
srbské	srbský	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
23	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
odesláno	odeslán	k2eAgNnSc1d1
červencové	červencový	k2eAgNnSc1d1
ultimátum	ultimátum	k1gNnSc1
s	s	k7c7
velmi	velmi	k6eAd1
tvrdými	tvrdý	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
většinu	většina	k1gFnSc4
se	se	k3xPyFc4
srbská	srbský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
splnit	splnit	k5eAaPmF
–	–	k?
kromě	kromě	k7c2
požadavku	požadavek	k1gInSc2
rakouskou	rakouský	k2eAgFnSc7d1
policií	policie	k1gFnSc7
vyšetřovat	vyšetřovat	k5eAaImF
osoby	osoba	k1gFnPc4
podezřelé	podezřelý	k2eAgFnPc4d1
z	z	k7c2
vraždy	vražda	k1gFnSc2
manželského	manželský	k2eAgInSc2d1
páru	pár	k1gInSc2
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
považovalo	považovat	k5eAaImAgNnS
ultimátum	ultimátum	k1gNnSc4
za	za	k7c4
nesplněné	splněný	k2eNgFnPc4d1
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
vyhlásilo	vyhlásit	k5eAaPmAgNnS
Srbsku	Srbsko	k1gNnSc6
válku	válek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
hybateli	hybatel	k1gMnPc7
událostí	událost	k1gFnSc7
byly	být	k5eAaImAgInP
v	v	k7c6
pozadí	pozadí	k1gNnSc6
stojící	stojící	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
a	a	k8xC
generální	generální	k2eAgInPc1d1
štáby	štáb	k1gInPc1
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
sám	sám	k3xTgMnSc1
nebyl	být	k5eNaImAgMnS
přímo	přímo	k6eAd1
válečným	válečný	k2eAgMnSc7d1
štváčem	štváč	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
spíš	spíš	k9
diplomatickým	diplomatický	k2eAgMnSc7d1
hazardérem	hazardér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko-uherský	rakousko-uherský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
byl	být	k5eAaImAgMnS
v	v	k7c6
pokročilém	pokročilý	k2eAgInSc6d1
věku	věk	k1gInSc6
okolnostmi	okolnost	k1gFnPc7
postaven	postavit	k5eAaPmNgInS
před	před	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
svým	svůj	k3xOyFgInSc7
podpisem	podpis	k1gInSc7
pod	pod	k7c4
známý	známý	k2eAgInSc4d1
manifest	manifest	k1gInSc4
Mým	můj	k3xOp1gInPc3
národům	národ	k1gInPc3
<g/>
!	!	kIx.
</s>
<s desamb="1">
zapříčinil	zapříčinit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
moci	moc	k1gFnSc3
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
tlačit	tlačit	k5eAaImF
důstojníci	důstojník	k1gMnPc1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
během	během	k7c2
červencové	červencový	k2eAgFnSc2d1
krize	krize	k1gFnSc2
byla	být	k5eAaImAgFnS
pozastavena	pozastavit	k5eAaPmNgFnS
činnost	činnost	k1gFnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
řízena	řídit	k5eAaImNgFnS
pomocí	pomocí	k7c2
nouzových	nouzový	k2eAgNnPc2d1
nařízení	nařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušena	zrušen	k2eAgFnSc1d1
byla	být	k5eAaImAgNnP
ústavní	ústavní	k2eAgNnPc1d1
občanská	občanský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
:	:	kIx,
svoboda	svoboda	k1gFnSc1
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
shromažďování	shromažďování	k1gNnSc2
<g/>
,	,	kIx,
spolčování	spolčování	k1gNnSc2
<g/>
,	,	kIx,
nedotknutelnost	nedotknutelnost	k1gFnSc1
poštovních	poštovní	k2eAgFnPc2d1
zásilek	zásilka	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
září	září	k1gNnSc6
pak	pak	k6eAd1
i	i	k9
imunita	imunita	k1gFnSc1
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečná	částečný	k2eAgFnSc1d1
mobilizace	mobilizace	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
již	již	k6eAd1
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1914	#num#	k4
a	a	k8xC
úplná	úplný	k2eAgFnSc1d1
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
souhlasem	souhlas	k1gInSc7
s	s	k7c7
vyhlášením	vyhlášení	k1gNnSc7
války	válka	k1gFnSc2
otálel	otálet	k5eAaImAgMnS
uherský	uherský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
István	István	k2eAgMnSc1d1
Tisza	Tisz	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
souhlasil	souhlasit	k5eAaImAgMnS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
velkou	velký	k2eAgFnSc7d1
částí	část	k1gFnSc7
uherské	uherský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
přivítáno	přivítán	k2eAgNnSc1d1
s	s	k7c7
nadšením	nadšení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
podobným	podobný	k2eAgNnSc7d1
nadšením	nadšení	k1gNnSc7
bylo	být	k5eAaImAgNnS
přivítáno	přivítán	k2eAgNnSc1d1
i	i	k8xC
radikálnějšími	radikální	k2eAgMnPc7d2
Němci	Němec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	Válka	k1gMnSc1
se	se	k3xPyFc4
totiž	totiž	k9
zdála	zdát	k5eAaImAgFnS
vhodnou	vhodný	k2eAgFnSc7d1
příležitostí	příležitost	k1gFnSc7
k	k	k7c3
odvrácení	odvrácení	k1gNnSc3
pozornosti	pozornost	k1gFnSc2
od	od	k7c2
vnitřního	vnitřní	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
rozkladu	rozklad	k1gInSc2
říše	říš	k1gFnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
slovanským	slovanský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
se	se	k3xPyFc4
podobné	podobný	k2eAgNnSc1d1
nadšení	nadšení	k1gNnSc1
nekonalo	konat	k5eNaImAgNnS
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
zde	zde	k6eAd1
byli	být	k5eAaImAgMnP
vyhlášením	vyhlášení	k1gNnSc7
války	válka	k1gFnSc2
šokováni	šokován	k2eAgMnPc1d1
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
žádným	žádný	k3yNgFnPc3
podstatným	podstatný	k2eAgFnPc3d1
nepokojům	nepokoj	k1gInPc3
nedocházelo	docházet	k5eNaImAgNnS
a	a	k8xC
mobilizaci	mobilizace	k1gFnSc4
se	se	k3xPyFc4
nevyhýbali	vyhýbat	k5eNaImAgMnP
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
se	se	k3xPyFc4
totiž	totiž	k9
většina	většina	k1gFnSc1
lidí	člověk	k1gMnPc2
domnívala	domnívat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
půjde	jít	k5eAaImIp3nS
o	o	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
krátkých	krátký	k2eAgInPc2d1
<g/>
,	,	kIx,
byť	byť	k8xS
krvavých	krvavý	k2eAgInPc2d1
válečných	válečný	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
měsíce	měsíc	k1gInSc2
se	se	k3xPyFc4
však	však	k9
ve	v	k7c6
válečném	válečný	k2eAgInSc6d1
stavu	stav	k1gInSc6
ocitly	ocitnout	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
velké	velký	k2eAgFnPc1d1
evropské	evropský	k2eAgFnPc1d1
země	zem	k1gFnPc1
(	(	kIx(
<g/>
kromě	kromě	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
mobilizovalo	mobilizovat	k5eAaBmAgNnS
36	#num#	k4
ročníků	ročník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
směřovala	směřovat	k5eAaImAgFnS
proti	proti	k7c3
Srbsku	Srbsko	k1gNnSc3
pod	pod	k7c7
vedením	vedení	k1gNnSc7
generála	generál	k1gMnSc2
Oskara	Oskar	k1gMnSc2
Potiorka	Potiorka	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
předtím	předtím	k6eAd1
selhal	selhat	k5eAaPmAgInS
při	při	k7c6
zajištění	zajištění	k1gNnSc6
bezpečnosti	bezpečnost	k1gFnSc2
návštěvy	návštěva	k1gFnSc2
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
12	#num#	k4
<g/>
]	]	kIx)
Selhal	selhat	k5eAaPmAgInS
i	i	k9
při	při	k7c6
této	tento	k3xDgFnSc6
ofenzivě	ofenziva	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
skončila	skončit	k5eAaPmAgFnS
fiaskem	fiasko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
byl	být	k5eAaImAgMnS
penzionován	penzionovat	k5eAaBmNgMnS
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
jiným	jiný	k2eAgInSc7d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
již	již	k6eAd1
byl	být	k5eAaImAgMnS
úspěšnější	úspěšný	k2eAgMnSc1d2
–	–	k?
Srbsko	Srbsko	k1gNnSc1
i	i	k8xC
jeho	jeho	k3xOp3gFnSc4
spojenec	spojenec	k1gMnSc1
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
byly	být	k5eAaImAgFnP
dobyty	dobýt	k5eAaPmNgFnP
a	a	k8xC
okupovány	okupován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
v	v	k7c6
regionu	region	k1gInSc6
se	se	k3xPyFc4
roku	rok	k1gInSc2
1915	#num#	k4
stalo	stát	k5eAaPmAgNnS
Bulharsko	Bulharsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
krvavé	krvavý	k2eAgInPc1d1
boje	boj	k1gInPc1
byly	být	k5eAaImAgInP
vedeny	vést	k5eAaImNgInP
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
schopnost	schopnost	k1gFnSc4
mobilizace	mobilizace	k1gFnSc2
a	a	k8xC
celková	celkový	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
armády	armáda	k1gFnSc2
byly	být	k5eAaImAgFnP
Rakousko-Uherskem	Rakousko-Uhersko	k1gNnSc7
i	i	k8xC
Německem	Německo	k1gNnSc7
podceněny	podcenit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
stálo	stát	k5eAaImAgNnS
neúspěšné	úspěšný	k2eNgNnSc1d1
válčení	válčení	k1gNnSc1
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
vybití	vybití	k1gNnSc2
téměř	téměř	k6eAd1
poloviny	polovina	k1gFnSc2
předválečného	předválečný	k2eAgInSc2d1
důstojnického	důstojnický	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přesunu	přesun	k1gInSc3
fronty	fronta	k1gFnSc2
hluboko	hluboko	k6eAd1
na	na	k7c4
rakousko-uherské	rakousko-uherský	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Haliči	Halič	k1gFnSc6
a	a	k8xC
východních	východní	k2eAgFnPc6d1
Uhrách	Uhry	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Zde	zde	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
projevovat	projevovat	k5eAaImF
národnostní	národnostní	k2eAgFnSc4d1
roztříštěnost	roztříštěnost	k1gFnSc4
císařské	císařský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
noví	nový	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
<g/>
,	,	kIx,
rekrutovaní	rekrutovaný	k2eAgMnPc1d1
z	z	k7c2
branců	branec	k1gMnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
nezvládali	zvládat	k5eNaImAgMnP
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
mužstvem	mužstvo	k1gNnSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italská	italský	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
původní	původní	k2eAgMnSc1d1
spojenec	spojenec	k1gMnSc1
centrálních	centrální	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
od	od	k7c2
počátku	počátek	k1gInSc2
války	válka	k1gFnSc2
udržoval	udržovat	k5eAaImAgMnS
neutralitu	neutralita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
slibu	slib	k1gInSc6
rozsáhlých	rozsáhlý	k2eAgInPc2d1
územních	územní	k2eAgInPc2d1
zisků	zisk	k1gInPc2
v	v	k7c6
případě	případ	k1gInSc6
vítězství	vítězství	k1gNnSc2
Dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
1915	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
Itálie	Itálie	k1gFnSc1
Rakousko-Uhersku	Rakousko-Uherska	k1gFnSc4
válku	válek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
monarchie	monarchie	k1gFnSc1
úspěšnější	úspěšný	k2eAgFnSc1d2
a	a	k8xC
tuto	tento	k3xDgFnSc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
obrannou	obranný	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
hájila	hájit	k5eAaImAgFnS
bez	bez	k7c2
podstatného	podstatný	k2eAgInSc2d1
italského	italský	k2eAgInSc2d1
průlomu	průlom	k1gInSc2
a	a	k8xC
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
technologickou	technologický	k2eAgFnSc7d1
i	i	k8xC
vojenskou	vojenský	k2eAgFnSc7d1
převahou	převaha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
tyto	tento	k3xDgInPc4
neúspěchy	neúspěch	k1gInPc4
v	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
války	válka	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1916	#num#	k4
a	a	k8xC
1917	#num#	k4
ke	k	k7c3
konsolidaci	konsolidace	k1gFnSc3
situace	situace	k1gFnSc2
a	a	k8xC
vítězství	vítězství	k1gNnSc2
Centrálních	centrální	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
na	na	k7c4
dohled	dohled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společným	společný	k2eAgNnSc7d1
úsilím	úsilí	k1gNnSc7
Německa	Německo	k1gNnSc2
a	a	k8xC
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
byla	být	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
úspěšná	úspěšný	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vytlačena	vytlačit	k5eAaPmNgFnS
z	z	k7c2
Haliče	Halič	k1gFnSc2
a	a	k8xC
dovedena	dovést	k5eAaPmNgFnS
na	na	k7c4
pokraj	pokraj	k1gInSc4
zhroucení	zhroucení	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
celá	celý	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
italské	italský	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
byla	být	k5eAaImAgNnP
italská	italský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
rozdrcena	rozdrcen	k2eAgNnPc1d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Caporetta	Caporetto	k1gNnSc2
a	a	k8xC
prakticky	prakticky	k6eAd1
vyřazena	vyřadit	k5eAaPmNgFnS
z	z	k7c2
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německá	německý	k2eAgFnSc1d1
západní	západní	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
přešla	přejít	k5eAaPmAgFnS
k	k	k7c3
vyčerpávající	vyčerpávající	k2eAgFnSc3d1
opotřebovávací	opotřebovávací	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nějaký	nějaký	k3yIgInSc1
zásadní	zásadní	k2eAgInSc1d1
průlom	průlom	k1gInSc1
nedal	dát	k5eNaPmAgInS
čekat	čekat	k5eAaImF
od	od	k7c2
žádné	žádný	k3yNgFnSc2
z	z	k7c2
bojujících	bojující	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1916	#num#	k4
vyhlásilo	vyhlásit	k5eAaPmAgNnS
Centrálním	centrální	k2eAgInSc7d1
mocnostem	mocnost	k1gFnPc3
válku	válka	k1gFnSc4
Rumunsko	Rumunsko	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
po	po	k7c6
případném	případný	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
chtělo	chtít	k5eAaImAgNnS
získat	získat	k5eAaPmF
uherské	uherský	k2eAgNnSc1d1
Sedmihradsko	Sedmihradsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rumunsko	Rumunsko	k1gNnSc1
sice	sice	k8xC
nebylo	být	k5eNaImAgNnS
zcela	zcela	k6eAd1
dobyto	dobýt	k5eAaPmNgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
zahnána	zahnat	k5eAaPmNgFnS
do	do	k7c2
obranného	obranný	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
v	v	k7c6
rumunské	rumunský	k2eAgFnSc6d1
Moldávii	Moldávie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Císař	Císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
Zitou	Zita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
nejtěžších	těžký	k2eAgFnPc6d3
chvílích	chvíle	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
jeho	jeho	k3xOp3gFnSc1
země	země	k1gFnSc1
kdy	kdy	k6eAd1
zažila	zažít	k5eAaPmAgFnS
a	a	k8xC
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
odejít	odejít	k5eAaPmF
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
pokoušel	pokoušet	k5eAaImAgMnS
ukončit	ukončit	k5eAaPmF
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
frontách	fronta	k1gFnPc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
to	ten	k3xDgNnSc1
tedy	tedy	k9
na	na	k7c4
brzkou	brzký	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
nevypadalo	vypadat	k5eNaImAgNnS,k5eNaPmAgNnS
<g/>
,	,	kIx,
uvnitř	uvnitř	k7c2
země	zem	k1gFnSc2
ale	ale	k8xC
vše	všechen	k3xTgNnSc1
vypadalo	vypadat	k5eAaPmAgNnS,k5eAaImAgNnS
trochu	trochu	k6eAd1
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
brzy	brzy	k6eAd1
po	po	k7c6
začátku	začátek	k1gInSc6
válčení	válčení	k1gNnSc2
nastaly	nastat	k5eAaPmAgFnP
zásobovací	zásobovací	k2eAgFnPc1d1
potíže	potíž	k1gFnPc1
zejména	zejména	k9
u	u	k7c2
potravin	potravina	k1gFnPc2
a	a	k8xC
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vedly	vést	k5eAaImAgFnP
k	k	k7c3
extrémnímu	extrémní	k2eAgInSc3d1
růstu	růst	k1gInSc3
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
státem	stát	k1gInSc7
regulovány	regulovat	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
dubnu	duben	k1gInSc6
1915	#num#	k4
zaveden	zaveden	k2eAgInSc1d1
přídělový	přídělový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
dobových	dobový	k2eAgFnPc2d1
výpovědí	výpověď	k1gFnPc2
přesto	přesto	k8xC
můžeme	moct	k5eAaImIp1nP
vyčíst	vyčíst	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
lidem	člověk	k1gMnPc3
dostávalo	dostávat	k5eAaImAgNnS
lístků	lístek	k1gInPc2
na	na	k7c4
životní	životní	k2eAgFnPc4d1
potřeby	potřeba	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
samotných	samotný	k2eAgFnPc2d1
životních	životní	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostávalo	dostávat	k5eNaImAgNnS
se	se	k3xPyFc4
ani	ani	k8xC
petroleje	petrolej	k1gInPc1
na	na	k7c4
svícení	svícení	k1gNnPc4
<g/>
,	,	kIx,
kůží	kůže	k1gFnPc2
<g/>
,	,	kIx,
textilu	textil	k1gInSc2
a	a	k8xC
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
záminkou	záminka	k1gFnSc7
vědeckého	vědecký	k2eAgNnSc2d1
zkoumání	zkoumání	k1gNnSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1916	#num#	k4
proveden	proveden	k2eAgInSc1d1
soupis	soupis	k1gInSc1
kostelních	kostelní	k2eAgInPc2d1
zvonů	zvon	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
napřesrok	napřesrok	k6eAd1
zrekvírovány	zrekvírovat	k5eAaPmNgInP
a	a	k8xC
roztaveny	roztavit	k5eAaPmNgInP
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
válečné	válečný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
velké	velký	k2eAgNnSc1d1
pobouření	pobouření	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černý	černý	k2eAgInSc1d1
trh	trh	k1gInSc1
s	s	k7c7
potravinami	potravina	k1gFnPc7
bujel	bujet	k5eAaImAgInS
a	a	k8xC
z	z	k7c2
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
mířily	mířit	k5eAaImAgFnP
každý	každý	k3xTgInSc4
den	den	k1gInSc4
tisíce	tisíc	k4xCgInPc1
žen	žena	k1gFnPc2
za	za	k7c7
nákupem	nákup	k1gInSc7
potravin	potravina	k1gFnPc2
na	na	k7c4
venkov	venkov	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
pak	pak	k6eAd1
stály	stát	k5eAaImAgFnP
nekonečné	konečný	k2eNgFnPc1d1
fronty	fronta	k1gFnPc1
na	na	k7c4
brambory	brambora	k1gFnPc4
a	a	k8xC
zeleninu	zelenina	k1gFnSc4
<g/>
,	,	kIx,
masa	masa	k1gFnSc1
se	se	k3xPyFc4
vůbec	vůbec	k9
nedostávalo	dostávat	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Uhrách	Uhry	k1gFnPc6
bylo	být	k5eAaImAgNnS
ale	ale	k9
potravin	potravina	k1gFnPc2
relativně	relativně	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
dostatek	dostatek	k1gInSc1
díky	díky	k7c3
větším	veliký	k2eAgFnPc3d2
plochám	plocha	k1gFnPc3
orné	orný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
smluvní	smluvní	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
obou	dva	k4xCgFnPc2
polovin	polovina	k1gFnPc2
říše	říš	k1gFnSc2
umožňovaly	umožňovat	k5eAaImAgInP
uherským	uherský	k2eAgMnPc3d1
úřadům	úřada	k1gMnPc3
autonomní	autonomní	k2eAgNnSc4d1
hospodaření	hospodaření	k1gNnSc4
s	s	k7c7
potravinami	potravina	k1gFnPc7
a	a	k8xC
Uhry	Uhry	k1gFnPc1
tak	tak	k9
nebyly	být	k5eNaImAgFnP
nuceny	nucen	k2eAgFnPc1d1
dodávat	dodávat	k5eAaImF
obživu	obživa	k1gFnSc4
do	do	k7c2
hladovějící	hladovějící	k2eAgFnSc2d1
západní	západní	k2eAgFnSc2d1
poloviny	polovina	k1gFnSc2
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
byl	být	k5eAaImAgInS
jízdní	jízdní	k2eAgInSc4d1
řád	řád	k1gInSc4
osobní	osobní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
s	s	k7c7
válkou	válka	k1gFnSc7
značně	značně	k6eAd1
zredukován	zredukován	k2eAgMnSc1d1
<g/>
,	,	kIx,
přesto	přesto	k8xC
stály	stát	k5eAaImAgFnP
rakousko-uherské	rakousko-uherský	k2eAgFnPc1d1
železnice	železnice	k1gFnPc1
trvale	trvale	k6eAd1
na	na	k7c6
pokraji	pokraj	k1gInSc6
zhroucení	zhroucení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobní	osobní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
měly	mít	k5eAaImAgFnP
každý	každý	k3xTgInSc4
den	den	k1gInSc4
mnohahodinová	mnohahodinový	k2eAgNnPc4d1
zpoždění	zpoždění	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
mnohdy	mnohdy	k6eAd1
vůbec	vůbec	k9
nepřijely	přijet	k5eNaPmAgFnP
a	a	k8xC
nedávno	nedávno	k6eAd1
zestátněné	zestátněný	k2eAgFnPc1d1
nevýkonné	výkonný	k2eNgFnPc1d1
hlavní	hlavní	k2eAgFnPc1d1
magistrály	magistrála	k1gFnPc1
monarchie	monarchie	k1gFnSc2
byly	být	k5eAaImAgFnP
zacpány	zacpat	k5eAaPmNgFnP
pomalu	pomalu	k6eAd1
se	s	k7c7
šinoucími	šinoucí	k2eAgInPc7d1
vojenskými	vojenský	k2eAgInPc7d1
transporty	transport	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
daleko	daleko	k6eAd1
zaostávaly	zaostávat	k5eAaImAgInP
za	za	k7c4
přesně	přesně	k6eAd1
organizovanou	organizovaný	k2eAgFnSc7d1
dopravou	doprava	k1gFnSc7
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
tedy	tedy	k9
po	po	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
nebylo	být	k5eNaImAgNnS
vůbec	vůbec	k9
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
připraveno	připravit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělské	zemědělský	k2eAgInPc1d1
produkty	produkt	k1gInPc1
byly	být	k5eAaImAgInP
přednostně	přednostně	k6eAd1
dodávány	dodávat	k5eAaImNgInP
na	na	k7c4
frontu	fronta	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
o	o	k7c4
to	ten	k3xDgNnSc4
víc	hodně	k6eAd2
nedostávalo	dostávat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
byla	být	k5eAaImAgFnS
hospodářská	hospodářský	k2eAgFnSc1d1
situace	situace	k1gFnSc1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
frontách	fronta	k1gFnPc6
i	i	k8xC
ve	v	k7c6
vojenských	vojenský	k2eAgInPc6d1
útvarech	útvar	k1gInPc6
natolik	natolik	k6eAd1
kritická	kritický	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
četným	četný	k2eAgFnPc3d1
vzpourám	vzpoura	k1gFnPc3
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
byl	být	k5eAaImAgInS
u	u	k7c2
nás	my	k3xPp1nPc2
později	pozdě	k6eAd2
přisuzován	přisuzován	k2eAgInSc1d1
nacionální	nacionální	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastňovali	zúčastňovat	k5eAaImAgMnP
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
však	však	k9
vojáci	voják	k1gMnPc1
i	i	k8xC
civilisté	civilista	k1gMnPc1
všech	všecek	k3xTgFnPc2
národností	národnost	k1gFnPc2
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc7
motivem	motiv	k1gInSc7
byl	být	k5eAaImAgInS
především	především	k9
hlad	hlad	k1gInSc4
a	a	k8xC
odpor	odpor	k1gInSc4
k	k	k7c3
nekončícímu	končící	k2eNgNnSc3d1
zabíjení	zabíjení	k1gNnSc3
na	na	k7c6
bojištích	bojiště	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomník	pomník	k1gInSc4
padlým	padlý	k1gMnPc3
v	v	k7c6
obci	obec	k1gFnSc6
Vidice	Vidice	k1gFnSc2
u	u	k7c2
Kutné	kutný	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1
na	na	k7c6
Václavském	václavský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
počátku	počátek	k1gInSc6
stál	stát	k5eAaImAgMnS
protest	protest	k1gInSc4
proti	proti	k7c3
vyvážení	vyvážení	k1gNnSc3
obilí	obilí	k1gNnSc2
z	z	k7c2
města	město	k1gNnSc2
</s>
<s>
Generální	generální	k2eAgInSc1d1
štáb	štáb	k1gInSc1
i	i	k8xC
vláda	vláda	k1gFnSc1
proválečného	proválečný	k2eAgMnSc2d1
ministerského	ministerský	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Karla	Karel	k1gMnSc2
Stürgkha	Stürgkh	k1gMnSc2
byly	být	k5eAaImAgInP
ke	k	k7c3
kritické	kritický	k2eAgFnSc3d1
hospodářské	hospodářský	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
i	i	k8xC
odporu	odpor	k1gInSc2
k	k	k7c3
válčení	válčení	k1gNnSc3
u	u	k7c2
většiny	většina	k1gFnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
hluší	hluš	k1gFnPc2wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zřejmě	zřejmě	k6eAd1
dohnalo	dohnat	k5eAaPmAgNnS
německého	německý	k2eAgMnSc2d1
sociálního	sociální	k2eAgMnSc2d1
demokrata	demokrat	k1gMnSc2
Friedricha	Friedrich	k1gMnSc2
Adlera	Adler	k1gMnSc2
k	k	k7c3
atentátu	atentát	k1gInSc3
na	na	k7c4
Stürgkha	Stürgkh	k1gMnSc2
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1916	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
byl	být	k5eAaImAgMnS
ihned	ihned	k6eAd1
zadržen	zadržet	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
zachycující	zachycující	k2eAgFnSc1d1
rozpad	rozpad	k1gInSc4
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
s	s	k7c7
vyznačenými	vyznačený	k2eAgInPc7d1
nástupnickými	nástupnický	k2eAgInPc7d1
státy	stát	k1gInPc7
</s>
<s>
Přesně	přesně	k6eAd1
měsíc	měsíc	k1gInSc4
po	po	k7c6
Stürgkhově	Stürgkhův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1916	#num#	k4
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gInSc6
na	na	k7c4
trůn	trůn	k1gInSc4
usedl	usednout	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
člen	člen	k1gMnSc1
evropských	evropský	k2eAgFnPc2d1
královských	královský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
přímo	přímo	k6eAd1
účastnil	účastnit	k5eAaImAgMnS
bojů	boj	k1gInPc2
na	na	k7c6
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
své	svůj	k3xOyFgFnSc2
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
zlepšit	zlepšit	k5eAaPmF
situaci	situace	k1gFnSc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obnovil	obnovit	k5eAaPmAgMnS
činnost	činnost	k1gFnSc4
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
další	další	k2eAgNnPc4d1
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
vylepšovala	vylepšovat	k5eAaImAgFnS
též	též	k9
sociální	sociální	k2eAgNnSc4d1
zákonodárství	zákonodárství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokoušel	pokoušet	k5eAaImAgMnS
se	se	k3xPyFc4
také	také	k9
od	od	k7c2
jara	jaro	k1gNnSc2
1917	#num#	k4
tajně	tajně	k6eAd1
vyjednat	vyjednat	k5eAaPmF
separátní	separátní	k2eAgInSc4d1
mír	mír	k1gInSc4
mezi	mezi	k7c7
Rakousko-Uherskem	Rakousko-Uhersek	k1gInSc7
a	a	k8xC
Dohodou	dohoda	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
kterýžto	kterýžto	k?
jednáních	jednání	k1gNnPc6
dělali	dělat	k5eAaImAgMnP
prostředníky	prostředník	k1gInPc4
bratři	bratr	k1gMnPc1
Karlovy	Karlův	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
Zity	Zita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednání	jednání	k1gNnPc1
se	se	k3xPyFc4
též	též	k9
účastnil	účastnit	k5eAaImAgMnS
Karlův	Karlův	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Otakar	Otakar	k1gMnSc1
Černín	Černín	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3,k3yIgNnSc3,k3yQgNnSc3
císař	císař	k1gMnSc1
plně	plně	k6eAd1
důvěřoval	důvěřovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
však	však	k9
ukázal	ukázat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
osoba	osoba	k1gFnSc1
nevhodná	vhodný	k2eNgFnSc1d1
pro	pro	k7c4
tak	tak	k6eAd1
delikátní	delikátní	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírová	mírový	k2eAgNnPc1d1
jednání	jednání	k1gNnPc1
ztroskotala	ztroskotat	k5eAaPmAgNnP
na	na	k7c6
neúměrných	úměrný	k2eNgInPc6d1
územních	územní	k2eAgInPc6d1
požadavcích	požadavek	k1gInPc6
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
postavila	postavit	k5eAaPmAgFnS
Francie	Francie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1918	#num#	k4
se	se	k3xPyFc4
Černín	Černín	k1gMnSc1
před	před	k7c7
vídeňskou	vídeňský	k2eAgFnSc7d1
obecní	obecní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
vyjádřil	vyjádřit	k5eAaPmAgInS
o	o	k7c6
francouzském	francouzský	k2eAgMnSc6d1
ministerském	ministerský	k2eAgMnSc6d1
předsedovi	předseda	k1gMnSc6
Georgesovi	Georges	k1gMnSc6
Clemenceauovi	Clemenceau	k1gMnSc6
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgMnS
on	on	k3xPp3gMnSc1
–	–	k?
Clemenceau	Clemencea	k1gMnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
mírovou	mírový	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
předložil	předložit	k5eAaPmAgMnS
a	a	k8xC
Černín	Černín	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
sám	sám	k3xTgMnSc1
odmítl	odmítnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clemenceau	Clemenceaus	k1gInSc2
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
zveřejnil	zveřejnit	k5eAaPmAgInS
Karlovy	Karlův	k2eAgInPc4d1
dopisy	dopis	k1gInPc4
a	a	k8xC
vypukla	vypuknout	k5eAaPmAgFnS
tak	tak	k6eAd1
Sixtova	Sixtův	k2eAgFnSc1d1
aféra	aféra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černín	Černín	k1gMnSc1
nejprve	nejprve	k6eAd1
tlačil	tlačit	k5eAaImAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
císař	císař	k1gMnSc1
abdikoval	abdikovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
však	však	k9
musel	muset	k5eAaImAgInS
odejít	odejít	k5eAaPmF
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
pak	pak	k6eAd1
musel	muset	k5eAaImAgMnS
podstoupit	podstoupit	k5eAaPmF
ponižující	ponižující	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
s	s	k7c7
císařem	císař	k1gMnSc7
Vilémem	Vilém	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
bylo	být	k5eAaImAgNnS
jasno	jasno	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
je	být	k5eAaImIp3nS
již	již	k6eAd1
vojensky	vojensky	k6eAd1
<g/>
,	,	kIx,
politicky	politicky	k6eAd1
i	i	k9
hospodářsky	hospodářsky	k6eAd1
zcela	zcela	k6eAd1
ve	v	k7c6
vleku	vlek	k1gInSc6
Německa	Německo	k1gNnSc2
a	a	k8xC
i	i	k9
vliv	vliv	k1gInSc1
císaře	císař	k1gMnSc2
na	na	k7c4
vnitřní	vnitřní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
země	zem	k1gFnSc2
se	se	k3xPyFc4
značně	značně	k6eAd1
oslabil	oslabit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyzrazení	vyzrazení	k1gNnSc1
tajného	tajný	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
o	o	k7c6
separátním	separátní	k2eAgInSc6d1
míru	mír	k1gInSc6
nepřišlo	přijít	k5eNaPmAgNnS
ve	v	k7c4
vhodný	vhodný	k2eAgInSc4d1
okamžik	okamžik	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
centrální	centrální	k2eAgFnSc3d1
mocnosti	mocnost	k1gFnSc3
ovládalo	ovládat	k5eAaImAgNnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
totiž	totiž	k9
opět	opět	k6eAd1
obnovila	obnovit	k5eAaPmAgFnS
víra	víra	k1gFnSc1
v	v	k7c4
konečné	konečný	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
:	:	kIx,
generální	generální	k2eAgFnSc1d1
stávka	stávka	k1gFnSc1
milionu	milion	k4xCgInSc2
dělníků	dělník	k1gMnPc2
z	z	k7c2
počátku	počátek	k1gInSc2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
zapomenuta	zapomenout	k5eAaPmNgFnS,k5eAaImNgFnS
pod	pod	k7c7
dojmem	dojem	k1gInSc7
mohutného	mohutný	k2eAgInSc2d1
průlomu	průlom	k1gInSc2
na	na	k7c6
západní	západní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
byly	být	k5eAaImAgFnP
přesouvány	přesouván	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
uvolněné	uvolněný	k2eAgFnPc1d1
na	na	k7c6
východě	východ	k1gInSc6
po	po	k7c6
uzavření	uzavření	k1gNnSc6
brestlitevského	brestlitevský	k2eAgInSc2d1
míru	mír	k1gInSc2
a	a	k8xC
počet	počet	k1gInSc1
nově	nově	k6eAd1
příchozích	příchozí	k1gMnPc2
vojáků	voják	k1gMnPc2
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
vstoupily	vstoupit	k5eAaPmAgInP
do	do	k7c2
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
<g/>
,	,	kIx,
mohly	moct	k5eAaImAgFnP
sotva	sotva	k6eAd1
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
početní	početní	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
v	v	k7c6
britské	britský	k2eAgFnSc6d1
<g/>
,	,	kIx,
francouzské	francouzský	k2eAgFnSc3d1
a	a	k8xC
italské	italský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
1918	#num#	k4
se	se	k3xPyFc4
ale	ale	k9
ukazovalo	ukazovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
pesimismus	pesimismus	k1gInSc4
Karla	Karel	k1gMnSc2
I.	I.	kA
ohledně	ohledně	k7c2
možnosti	možnost	k1gFnSc2
vítězství	vítězství	k1gNnSc2
centrálních	centrální	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
mnohem	mnohem	k6eAd1
reálnější	reální	k2eAgInSc1d2
základ	základ	k1gInSc1
<g/>
,	,	kIx,
než	než	k8xS
si	se	k3xPyFc3
důstojníci	důstojník	k1gMnPc1
generálních	generální	k2eAgInPc2d1
štábů	štáb	k1gInPc2
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pracovnách	pracovna	k1gFnPc6
představovali	představovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obnovení	obnovení	k1gNnSc1
činnosti	činnost	k1gFnSc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
odhalilo	odhalit	k5eAaPmAgNnS
hloubku	hloubka	k1gFnSc4
politického	politický	k2eAgInSc2d1
rozkladu	rozklad	k1gInSc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exiloví	exilový	k2eAgMnPc1d1
i	i	k8xC
domácí	domácí	k2eAgMnPc1d1
politici	politik	k1gMnPc1
menších	malý	k2eAgInPc2d2
národů	národ	k1gInPc2
otevřeně	otevřeně	k6eAd1
mluvili	mluvit	k5eAaImAgMnP
o	o	k7c6
vzniku	vznik	k1gInSc6
budoucích	budoucí	k2eAgInPc2d1
nových	nový	k2eAgInPc2d1
nástupnických	nástupnický	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
konfrontovaní	konfrontovaný	k2eAgMnPc1d1
s	s	k7c7
každodenní	každodenní	k2eAgFnSc7d1
realitou	realita	k1gFnSc7
všeobecného	všeobecný	k2eAgInSc2d1
nedostatku	nedostatek	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zvláště	zvláště	k6eAd1
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
žilo	žít	k5eAaImAgNnS
na	na	k7c4
hranici	hranice	k1gFnSc4
hladomoru	hladomor	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
bouřili	bouřit	k5eAaImAgMnP
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byla	být	k5eAaImAgFnS
zorganizována	zorganizován	k2eAgFnSc1d1
generální	generální	k2eAgFnSc1d1
stávka	stávka	k1gFnSc1
kvůli	kvůli	k7c3
vyvážení	vyvážení	k1gNnSc3
potravin	potravina	k1gFnPc2
z	z	k7c2
Čech	Čechy	k1gFnPc2
na	na	k7c4
frontu	fronta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
umírání	umírání	k1gNnSc3
v	v	k7c6
důsledku	důsledek	k1gInSc6
hladu	hlad	k1gInSc2
se	se	k3xPyFc4
navíc	navíc	k6eAd1
přidala	přidat	k5eAaPmAgFnS
španělská	španělský	k2eAgFnSc1d1
chřipka	chřipka	k1gFnSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
vyhlásil	vyhlásit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
záměr	záměr	k1gInSc4
federalizace	federalizace	k1gFnSc2
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
měl	mít	k5eAaImAgInS
v	v	k7c6
plánu	plán	k1gInSc6
již	již	k6eAd1
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
už	už	k6eAd1
ale	ale	k8xC
takový	takový	k3xDgInSc1
akt	akt	k1gInSc1
téměř	téměř	k6eAd1
nikoho	nikdo	k3yNnSc4
neuspokojil	uspokojit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uhersko	Uhersko	k1gNnSc1
vypovědělo	vypovědět	k5eAaPmAgNnS
Rakousko-uherské	rakousko-uherský	k2eAgNnSc1d1
vyrovnání	vyrovnání	k1gNnSc1
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
vyhlášena	vyhlásit	k5eAaPmNgFnS
samostatnost	samostatnost	k1gFnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byla	být	k5eAaImAgFnS
zveřejněna	zveřejněn	k2eAgFnSc1d1
odpověď	odpověď	k1gFnSc1
rakousko-uherského	rakousko-uherský	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
zahraničí	zahraničí	k1gNnSc2
na	na	k7c4
Wilsonovy	Wilsonův	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
příměří	příměří	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
byly	být	k5eAaImAgFnP
mylně	mylně	k6eAd1
vykládány	vykládán	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
vojenská	vojenský	k2eAgFnSc1d1
kapitulace	kapitulace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
novému	nový	k2eAgInSc3d1
státu	stát	k1gInSc3
se	s	k7c7
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
přidalo	přidat	k5eAaPmAgNnS
Slovensko	Slovensko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
demonstracím	demonstrace	k1gFnPc3
vedoucím	vedoucí	k2eAgMnSc7d1
ke	k	k7c3
vzniku	vznik	k1gInSc3
republiky	republika	k1gFnSc2
docházelo	docházet	k5eAaImAgNnS
i	i	k9
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitulace	kapitulace	k1gFnSc1
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
po	po	k7c6
předchozím	předchozí	k2eAgNnSc6d1
příměří	příměří	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
italská	italský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
nerespektovala	respektovat	k5eNaImAgNnP
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1918	#num#	k4
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
podepsal	podepsat	k5eAaPmAgMnS
rezignaci	rezignace	k1gFnSc4
na	na	k7c4
řízení	řízení	k1gNnSc4
státních	státní	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
a	a	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
tak	tak	k6eAd1
definitivně	definitivně	k6eAd1
zmizelo	zmizet	k5eAaPmAgNnS
z	z	k7c2
mapy	mapa	k1gFnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Válka	válka	k1gFnSc1
stála	stát	k5eAaImAgFnS
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
ve	v	k7c6
vojenských	vojenský	k2eAgFnPc6d1
i	i	k8xC
civilních	civilní	k2eAgFnPc6d1
ztrátách	ztráta	k1gFnPc6
1,2	1,2	k4
milionu	milion	k4xCgInSc2
životů	život	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
propadla	propadnout	k5eAaPmAgFnS
do	do	k7c2
chaosu	chaos	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
poznamenával	poznamenávat	k5eAaImAgInS
evropské	evropský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
po	po	k7c4
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
a	a	k8xC
správní	správní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
roku	rok	k1gInSc2
1914	#num#	k4
<g/>
:	:	kIx,
Předlitavsko	Předlitavsko	k1gNnSc4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čechy	Čech	k1gMnPc7
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bukovina	Bukovina	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korutany	Korutany	k1gInPc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraňsko	Kraňsko	k1gNnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalmácie	Dalmácie	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Halič	halič	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouské	rakouský	k2eAgInPc1d1
přímoří	přímoří	k1gNnSc1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salcbursko	Salcbursko	k1gNnSc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slezsko	Slezsko	k1gNnSc1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štýrsko	Štýrsko	k1gNnSc1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolsko	Tyrolsko	k1gNnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vorarlbersko	Vorarlbersko	k1gNnSc1
<g/>
;	;	kIx,
Zalitavsko	Zalitavsko	k1gNnSc1
<g/>
:	:	kIx,
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uhersko	Uhersko	k1gNnSc1
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chorvatsko-Slavonsko	Chorvatsko-Slavonsko	k1gNnSc1
<g/>
;	;	kIx,
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
;	;	kIx,
s	s	k7c7
vyznačením	vyznačení	k1gNnSc7
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
bylo	být	k5eAaImAgNnS
unií	unie	k1gFnSc7
dvou	dva	k4xCgInPc2
státních	státní	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Předlitavska	Předlitavsko	k1gNnPc1
–	–	k?
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
„	„	k?
<g/>
Království	království	k1gNnSc1
a	a	k8xC
země	zem	k1gFnPc1
v	v	k7c6
říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
zastoupené	zastoupený	k2eAgInPc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
neoficiální	neoficiální	k2eAgInSc4d1,k2eNgInSc4d1
název	název	k1gInSc4
Předlitavsko	Předlitavsko	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1915	#num#	k4
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
„	„	k?
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
“	“	k?
</s>
<s>
Zalitavska	Zalitavsko	k1gNnSc2
–	–	k?
též	též	k9
nazýváno	nazýván	k2eAgNnSc4d1
Země	zem	k1gFnPc4
Koruny	koruna	k1gFnSc2
svatoštěpánské	svatoštěpánský	k2eAgFnPc4d1
<g/>
,	,	kIx,
tedy	tedy	k9
Uhersko	Uhersko	k1gNnSc1
a	a	k8xC
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
s	s	k7c7
ním	on	k3xPp3gNnSc7
spojených	spojený	k2eAgFnPc2d1
personální	personální	k2eAgFnSc7d1
unií	unie	k1gFnSc7
</s>
<s>
Obě	dva	k4xCgFnPc1
poloviny	polovina	k1gFnPc1
soustátí	soustátí	k1gNnSc2
se	se	k3xPyFc4
vydaly	vydat	k5eAaPmAgFnP
odlišnou	odlišný	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
vnitřního	vnitřní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Uhersko	Uhersko	k1gNnSc1
</s>
<s>
Korunovace	korunovace	k1gFnSc1
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
a	a	k8xC
Alžběty	Alžběta	k1gFnSc2
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
a	a	k8xC
královnou	královna	k1gFnSc7
</s>
<s>
V	v	k7c6
Zalitavsku	Zalitavsko	k1gNnSc6
zůstal	zůstat	k5eAaPmAgInS
zakonzervován	zakonzervován	k2eAgInSc1d1
systém	systém	k1gInSc1
konstituční	konstituční	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
jako	jako	k8xS,k8xC
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
Uherském	uherský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
byla	být	k5eAaImAgFnS
jen	jen	k9
polovina	polovina	k1gFnSc1
poslanců	poslanec	k1gMnPc2
volena	volen	k2eAgFnSc1d1
ve	v	k7c6
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voleb	volba	k1gFnPc2
části	část	k1gFnSc2
poslanců	poslanec	k1gMnPc2
se	se	k3xPyFc4
též	též	k9
mohla	moct	k5eAaImAgFnS
zúčastnit	zúčastnit	k5eAaPmF
jen	jen	k9
část	část	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vykazovala	vykazovat	k5eAaImAgFnS
dostatečný	dostatečný	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
především	především	k9
o	o	k7c4
pozemkové	pozemkový	k2eAgMnPc4d1
vlastníky	vlastník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc4d1
právo	právo	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k9
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
vztahovalo	vztahovat	k5eAaImAgNnS
na	na	k7c4
přibližně	přibližně	k6eAd1
šest	šest	k4xCc4
procent	procento	k1gNnPc2
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
srovnání	srovnání	k1gNnSc6
ještě	ještě	k6eAd1
přijatelné	přijatelný	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
pozdějším	pozdní	k2eAgNnSc6d2
období	období	k1gNnSc6
se	se	k3xPyFc4
z	z	k7c2
takového	takový	k3xDgNnSc2
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
stal	stát	k5eAaPmAgInS
anachronismus	anachronismus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Uherský	uherský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Uherský	uherský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
byl	být	k5eAaImAgInS
dvoukomorový	dvoukomorový	k2eAgMnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
horní	horní	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
(	(	kIx(
<g/>
Sněmovna	sněmovna	k1gFnSc1
magnátů	magnát	k1gMnPc2
<g/>
)	)	kIx)
tvořili	tvořit	k5eAaImAgMnP
dědiční	dědičný	k2eAgMnPc1d1
nevolení	volený	k2eNgMnPc1d1
členové	člen	k1gMnPc1
a	a	k8xC
dolní	dolní	k2eAgMnPc1d1
pak	pak	k6eAd1
volení	volený	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc1d1
systém	systém	k1gInSc1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
znevýhodňoval	znevýhodňovat	k5eAaImAgInS
nemaďarské	maďarský	k2eNgInPc4d1
národy	národ	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
tvořily	tvořit	k5eAaImAgInP
přes	přes	k7c4
50	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
národy	národ	k1gInPc7
v	v	k7c6
počátečním	počáteční	k2eAgNnSc6d1
období	období	k1gNnSc6
nebylo	být	k5eNaImAgNnS
z	z	k7c2
historických	historický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
tolik	tolik	k6eAd1
dostatečně	dostatečně	k6eAd1
bohatých	bohatý	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdějším	pozdní	k2eAgNnSc6d2
období	období	k1gNnSc6
docházelo	docházet	k5eAaImAgNnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
všeobecným	všeobecný	k2eAgInSc7d1
nárůstem	nárůst	k1gInSc7
evropských	evropský	k2eAgNnPc2d1
nacionalistických	nacionalistický	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
též	též	k9
k	k	k7c3
manipulování	manipulování	k1gNnSc3
s	s	k7c7
volebními	volební	k2eAgInPc7d1
výsledky	výsledek	k1gInPc7
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
účelovým	účelový	k2eAgInPc3d1
zákrokům	zákrok	k1gInPc3
proti	proti	k7c3
přílivu	příliv	k1gInSc3
investic	investice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
by	by	kYmCp3nP
vedly	vést	k5eAaImAgInP
k	k	k7c3
růstu	růst	k1gInSc3
vlivu	vliv	k1gInSc2
nemaďarských	maďarský	k2eNgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zalitavsko	Zalitavsko	k1gNnSc1
se	se	k3xPyFc4
politicky	politicky	k6eAd1
členilo	členit	k5eAaImAgNnS
pouze	pouze	k6eAd1
na	na	k7c4
samotné	samotný	k2eAgNnSc4d1
Uhersko	Uhersko	k1gNnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
na	na	k7c4
Království	království	k1gNnSc4
chorvatsko-slavonské	chorvatsko-slavonský	k2eAgNnSc4d1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
určitou	určitý	k2eAgFnSc4d1
míru	míra	k1gFnSc4
autonomie	autonomie	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Uher	uher	k1gInSc1
a	a	k8xC
přístavní	přístavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Rijeka	Rijeka	k1gFnSc1
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
Fiume	Fium	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
zvláštním	zvláštní	k2eAgInSc7d1
statutem	statut	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
už	už	k6eAd1
stát	stát	k1gInSc1
dělil	dělit	k5eAaImAgInS
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
župy	župa	k1gFnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vesnice	vesnice	k1gFnPc1
podléhaly	podléhat	k5eAaImAgFnP
župní	župní	k2eAgFnSc2d1
správě	správa	k1gFnSc3
a	a	k8xC
neměly	mít	k5eNaImAgInP
tedy	tedy	k9
vlastní	vlastní	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autonomie	autonomie	k1gFnSc1
Chorvatsko-Slavonska	Chorvatsko-Slavonsko	k1gNnSc2
byla	být	k5eAaImAgFnS
zajištěna	zajistit	k5eAaPmNgFnS
po	po	k7c6
chorvatsko-uherském	chorvatsko-uherský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1868	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
ještě	ještě	k9
roku	rok	k1gInSc2
1873	#num#	k4
znovu	znovu	k6eAd1
revidováno	revidován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chorvatsko	Chorvatsko	k1gNnSc1
mělo	mít	k5eAaImAgNnS
svojí	svůj	k3xOyFgFnSc7
vládu	vláda	k1gFnSc4
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
bánem	bán	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
panovník	panovník	k1gMnSc1
na	na	k7c4
návrh	návrh	k1gInSc4
Uherského	uherský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
měla	mít	k5eAaImAgFnS
vlastní	vlastní	k2eAgInSc4d1
malý	malý	k2eAgInSc4d1
rozpočet	rozpočet	k1gInSc4
a	a	k8xC
řešila	řešit	k5eAaImAgFnS
pouze	pouze	k6eAd1
autonomní	autonomní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
společné	společný	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
řešila	řešit	k5eAaImAgFnS
uherská	uherský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
,	,	kIx,
u	u	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
bylo	být	k5eAaImAgNnS
Chorvatsko	Chorvatsko	k1gNnSc1
zastoupeno	zastoupit	k5eAaPmNgNnS
jedním	jeden	k4xCgInSc7
ministrem	ministr	k1gMnSc7
bez	bez	k7c2
portfeje	portfej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
uherském	uherský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
mělo	mít	k5eAaImAgNnS
pak	pak	k6eAd1
zastoupení	zastoupení	k1gNnSc4
40	#num#	k4
poslanci	poslanec	k1gMnPc1
(	(	kIx(
<g/>
z	z	k7c2
celkových	celkový	k2eAgInPc2d1
413	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autonomie	autonomie	k1gFnSc1
Velké	velký	k2eAgFnSc2d1
a	a	k8xC
Malé	Malé	k2eAgFnSc2d1
Kumánie	Kumánie	k1gFnSc2
byla	být	k5eAaImAgFnS
zrušena	zrušen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1876	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
existovalo	existovat	k5eAaImAgNnS
ještě	ještě	k9
území	území	k1gNnSc4
se	s	k7c7
zvláštním	zvláštní	k2eAgInSc7d1
režimem	režim	k1gInSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
nazývalo	nazývat	k5eAaImAgNnS
Vojenská	vojenský	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
táhlo	táhnout	k5eAaImAgNnS
od	od	k7c2
Jaderského	jaderský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
až	až	k9
do	do	k7c2
Sedmihradska	Sedmihradsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nepodléhalo	podléhat	k5eNaImAgNnS
správě	správa	k1gFnSc3
korunních	korunní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
sloužilo	sloužit	k5eAaImAgNnS
jako	jako	k8xS,k8xC
obranná	obranný	k2eAgFnSc1d1
linie	linie	k1gFnSc1
proti	proti	k7c3
tureckému	turecký	k2eAgNnSc3d1
nebezpečí	nebezpečí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1881	#num#	k4
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
postupně	postupně	k6eAd1
přičleněno	přičlenit	k5eAaPmNgNnS
k	k	k7c3
sousedícím	sousedící	k2eAgInPc3d1
regionům	region	k1gInPc3
a	a	k8xC
byla	být	k5eAaImAgFnS
na	na	k7c6
něm	on	k3xPp3gNnSc6
zavedena	zaveden	k2eAgFnSc1d1
normální	normální	k2eAgFnSc1d1
župní	župní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
V	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
se	se	k3xPyFc4
politika	politika	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
cestou	cestou	k7c2
postupné	postupný	k2eAgFnSc2d1
demokratizace	demokratizace	k1gFnSc2
společnosti	společnost	k1gFnSc2
a	a	k8xC
národnostního	národnostní	k2eAgNnSc2d1
zrovnoprávnění	zrovnoprávnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Předlitavský	předlitavský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
)	)	kIx)
sestával	sestávat	k5eAaImAgMnS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
komor	komora	k1gFnPc2
–	–	k?
Panská	panský	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
a	a	k8xC
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
členy	člen	k1gMnPc4
Panské	panský	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
plnoletí	plnoletý	k2eAgMnPc1d1
arcivévodové	arcivévoda	k1gMnPc1
<g/>
,	,	kIx,
církevní	církevní	k2eAgMnPc1d1
hodnostáři	hodnostář	k1gMnPc1
s	s	k7c7
knížecím	knížecí	k2eAgInSc7d1
titulem	titul	k1gInSc7
<g/>
,	,	kIx,
dědiční	dědičný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
šlechtických	šlechtický	k2eAgInPc2d1
rodů	rod	k1gInPc2
a	a	k8xC
císařem	císař	k1gMnSc7
doživotně	doživotně	k6eAd1
jmenovaní	jmenovaný	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
mužové	muž	k1gMnPc1
politického	politický	k2eAgInSc2d1
<g/>
,	,	kIx,
vědeckého	vědecký	k2eAgInSc2d1
a	a	k8xC
společenského	společenský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
komory	komora	k1gFnSc2
vysílaly	vysílat	k5eAaImAgFnP
své	svůj	k3xOyFgFnPc1
zástupce	zástupce	k1gMnSc2
zemské	zemský	k2eAgInPc1d1
sněmy	sněm	k1gInPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
korunních	korunní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
dubnová	dubnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
roku	rok	k1gInSc2
1873	#num#	k4
zavedla	zavést	k5eAaPmAgFnS
přímou	přímý	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
poslanců	poslanec	k1gMnPc2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc1d1
systém	systém	k1gInSc1
byl	být	k5eAaImAgMnS
tzv.	tzv.	kA
kuriální	kuriální	k2eAgMnSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
existovaly	existovat	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc1
volební	volební	k2eAgFnPc1d1
kurie	kurie	k1gFnPc1
<g/>
:	:	kIx,
velkostatkářská	velkostatkářský	k2eAgFnSc1d1
<g/>
,	,	kIx,
obchodních	obchodní	k2eAgFnPc2d1
komor	komora	k1gFnPc2
<g/>
,	,	kIx,
měst	město	k1gNnPc2
a	a	k8xC
venkovských	venkovský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
mandátů	mandát	k1gInPc2
pro	pro	k7c4
každou	každý	k3xTgFnSc4
z	z	k7c2
kurií	kurie	k1gFnPc2
neodpovídal	odpovídat	k5eNaImAgInS
počtu	počet	k1gInSc3
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
přiřazených	přiřazený	k2eAgFnPc2d1
do	do	k7c2
této	tento	k3xDgFnSc2
kurie	kurie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc4d1
právo	právo	k1gNnSc1
získal	získat	k5eAaPmAgMnS
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
platil	platit	k5eAaImAgInS
minimálně	minimálně	k6eAd1
10	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
přímých	přímý	k2eAgFnPc2d1
daní	daň	k1gFnPc2
ročně	ročně	k6eAd1
(	(	kIx(
<g/>
volební	volební	k2eAgInSc1d1
cenzus	cenzus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
váha	váha	k1gFnSc1
hlasu	hlas	k1gInSc2
odpovídala	odpovídat	k5eAaImAgFnS
daňové	daňový	k2eAgFnSc6d1
zátěži	zátěž	k1gFnSc3
voliče	volič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
census	census	k1gInSc1
byl	být	k5eAaImAgInS
postupně	postupně	k6eAd1
snižován	snižovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1882	#num#	k4
ho	on	k3xPp3gMnSc4
redukovala	redukovat	k5eAaBmAgFnS
na	na	k7c4
5	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
,	,	kIx,
následná	následný	k2eAgFnSc1d1
Badeniho	Badeni	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1896	#num#	k4
pak	pak	k6eAd1
na	na	k7c4
4	#num#	k4
zlaté	zlatá	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Badeniho	Badeni	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
ale	ale	k8xC
hlavně	hlavně	k9
přidala	přidat	k5eAaPmAgFnS
k	k	k7c3
dosavadním	dosavadní	k2eAgFnPc3d1
čtyřem	čtyři	k4xCgFnPc3
kuriím	kurie	k1gFnPc3
i	i	k8xC
kurii	kurie	k1gFnSc3
pátou	pátá	k1gFnSc4
<g/>
,	,	kIx,
všeobecnou	všeobecný	k2eAgFnSc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
bylo	být	k5eAaImAgNnS
volební	volební	k2eAgNnSc1d1
právo	právo	k1gNnSc1
rozšířeno	rozšířen	k2eAgNnSc1d1
na	na	k7c4
celou	celý	k2eAgFnSc4d1
mužskou	mužský	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
24	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kurie	kurie	k1gFnPc4
měla	mít	k5eAaImAgFnS
přiděleno	přidělit	k5eAaPmNgNnS
72	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
byla	být	k5eAaImAgFnS
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
rozšířena	rozšířit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
volební	volební	k2eAgInSc1d1
právo	právo	k1gNnSc4
žen	žena	k1gFnPc2
nevylučoval	vylučovat	k5eNaImAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
plátci	plátce	k1gMnPc1
daní	daň	k1gFnPc2
byli	být	k5eAaImAgMnP
obvykle	obvykle	k6eAd1
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
ženy	žena	k1gFnPc1
k	k	k7c3
volbám	volba	k1gFnPc3
dostaly	dostat	k5eAaPmAgInP
jen	jen	k9
výjimečně	výjimečně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
však	však	k9
žena	žena	k1gFnSc1
jako	jako	k8xC,k8xS
dědička	dědička	k1gFnSc1
nebo	nebo	k8xC
vdova	vdova	k1gFnSc1
přesto	přesto	k6eAd1
k	k	k7c3
majetku	majetek	k1gInSc2
přišla	přijít	k5eAaPmAgFnS
<g/>
,	,	kIx,
dokázala	dokázat	k5eAaPmAgFnS
si	se	k3xPyFc3
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
vymoci	vymoct	k5eAaPmF
výjimku	výjimek	k1gInSc3
a	a	k8xC
volit	volit	k5eAaImF
pak	pak	k6eAd1
mohla	moct	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
14	#num#	k4
<g/>
]	]	kIx)
Nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
prosazeno	prosazen	k2eAgNnSc4d1
všeobecné	všeobecný	k2eAgNnSc4d1
a	a	k8xC
rovné	rovný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
volilo	volit	k5eAaImAgNnS
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecné	všeobecný	k2eAgFnPc4d1
<g/>
,	,	kIx,
přímé	přímý	k2eAgFnPc4d1
<g/>
,	,	kIx,
rovné	rovný	k2eAgFnPc4d1
a	a	k8xC
tajné	tajný	k2eAgNnSc1d1
volební	volební	k2eAgNnSc1d1
právo	právo	k1gNnSc1
se	se	k3xPyFc4
týkalo	týkat	k5eAaImAgNnS
zletilých	zletilý	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tyto	tento	k3xDgFnPc1
postupné	postupný	k2eAgFnPc1d1
a	a	k8xC
dalekosáhlé	dalekosáhlý	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
se	se	k3xPyFc4
netýkaly	týkat	k5eNaImAgFnP
dalších	další	k2eAgInPc2d1
volených	volený	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
–	–	k?
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
<g/>
,	,	kIx,
okresních	okresní	k2eAgNnPc2d1
a	a	k8xC
obecních	obecní	k2eAgNnPc2d1
zastupitelstev	zastupitelstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
sněmy	sněm	k1gInPc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
různý	různý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgFnPc4d1
kurie	kurie	k1gFnPc4
zde	zde	k6eAd1
sice	sice	k8xC
zůstávaly	zůstávat	k5eAaImAgInP
<g/>
,	,	kIx,
ale	ale	k8xC
výše	výše	k1gFnSc1
censu	census	k1gInSc2
a	a	k8xC
počet	počet	k1gInSc1
kurií	kurie	k1gFnPc2
se	se	k3xPyFc4
lišily	lišit	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
pro	pro	k7c4
volby	volba	k1gFnPc4
na	na	k7c4
Český	český	k2eAgInSc4d1
zemský	zemský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
cenzus	cenzus	k1gInSc1
činil	činit	k5eAaImAgInS
4	#num#	k4
zlaté	zlatý	k1gInPc1
a	a	k8xC
kurie	kurie	k1gFnPc1
byly	být	k5eAaImAgFnP
tři	tři	k4xCgFnPc1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
se	se	k3xPyFc4
po	po	k7c6
moravském	moravský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
roku	rok	k1gInSc2
1905	#num#	k4
rozšířil	rozšířit	k5eAaPmAgInS
o	o	k7c4
všeobecnou	všeobecný	k2eAgFnSc4d1
kurii	kurie	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
mohlo	moct	k5eAaImAgNnS
volit	volit	k5eAaImF
veškeré	veškerý	k3xTgNnSc4
mužské	mužský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
odváděných	odváděný	k2eAgFnPc2d1
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
zato	zato	k6eAd1
tu	tu	k6eAd1
ale	ale	k8xC
byla	být	k5eAaImAgFnS
stanovena	stanoven	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
se	se	k3xPyFc4
voleb	volba	k1gFnPc2
zúčastnit	zúčastnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
sněmu	sněm	k1gInSc6
bylo	být	k5eAaImAgNnS
několik	několik	k4yIc1
poslanců	poslanec	k1gMnPc2
nevolených	volený	k2eNgMnPc2d1
(	(	kIx(
<g/>
virilisté	virilista	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tam	tam	k6eAd1
byli	být	k5eAaImAgMnP
z	z	k7c2
titulu	titul	k1gInSc2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
obecních	obecní	k2eAgInPc2d1
a	a	k8xC
okresních	okresní	k2eAgNnPc2d1
zastupitelstev	zastupitelstvo	k1gNnPc2
byl	být	k5eAaImAgMnS
systém	systém	k1gInSc4
ještě	ještě	k6eAd1
složitější	složitý	k2eAgMnSc1d2
a	a	k8xC
udržel	udržet	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
nezměněné	změněný	k2eNgFnSc6d1
podobě	podoba	k1gFnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Společné	společný	k2eAgFnPc1d1
záležitosti	záležitost	k1gFnPc1
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
paráda	paráda	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
</s>
<s>
Každý	každý	k3xTgMnSc1
z	z	k7c2
obou	dva	k4xCgInPc2
celků	celek	k1gInPc2
měl	mít	k5eAaImAgInS
tedy	tedy	k9
svůj	svůj	k3xOyFgInSc4
parlament	parlament	k1gInSc4
(	(	kIx(
<g/>
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
v	v	k7c6
Zalitavsku	Zalitavsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládu	vláda	k1gFnSc4
každého	každý	k3xTgInSc2
z	z	k7c2
celků	celek	k1gInPc2
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
a	a	k8xC
odvolával	odvolávat	k5eAaImAgMnS
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
odpovědná	odpovědný	k2eAgFnSc1d1
jemu	on	k3xPp3gMnSc3
a	a	k8xC
ne	ne	k9
parlamentům	parlament	k1gInPc3
<g/>
,	,	kIx,
neexistovalo	existovat	k5eNaImAgNnS
tedy	tedy	k9
žádné	žádný	k3yNgNnSc1
hlasování	hlasování	k1gNnSc1
o	o	k7c6
důvěře	důvěra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
měl	mít	k5eAaImAgMnS
též	též	k9
pravomoc	pravomoc	k1gFnSc4
vetovat	vetovat	k5eAaBmF
jakýkoliv	jakýkoliv	k3yIgInSc4
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
vynášet	vynášet	k5eAaImF
rozsudky	rozsudek	k1gInPc4
<g/>
,	,	kIx,
milosti	milost	k1gFnPc4
a	a	k8xC
amnestie	amnestie	k1gFnPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
též	též	k6eAd1
nejvyšším	vysoký	k2eAgMnSc7d3
velitelem	velitel	k1gMnSc7
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
delegace	delegace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Společné	společný	k2eAgFnPc1d1
záležitosti	záležitost	k1gFnPc1
soustátí	soustátí	k1gNnSc2
projednávaly	projednávat	k5eAaImAgFnP
rakousko-uherské	rakousko-uherský	k2eAgFnPc1d1
delegace	delegace	k1gFnPc1
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgFnPc1d1
paritně	paritně	k6eAd1
šedesáti	šedesát	k4xCc7
poslanci	poslanec	k1gMnPc7
delegovanými	delegovaný	k2eAgMnPc7d1
předlitavskou	předlitavský	k2eAgFnSc7d1
Říšskou	říšský	k2eAgFnSc7d1
radou	rada	k1gFnSc7
a	a	k8xC
šedesáti	šedesát	k4xCc7
poslanci	poslanec	k1gMnPc7
Uherského	uherský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delegace	delegace	k1gFnSc1
se	se	k3xPyFc4
scházely	scházet	k5eAaImAgInP
jednou	jednou	k6eAd1
ročně	ročně	k6eAd1
střídavě	střídavě	k6eAd1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
Budapešti	Budapešť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouvy	smlouva	k1gFnSc2
o	o	k7c6
rakousko-uherském	rakousko-uherský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
každých	každý	k3xTgNnPc2
deset	deset	k4xCc4
let	léto	k1gNnPc2
obnovovány	obnovován	k2eAgInPc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
především	především	k9
zástupci	zástupce	k1gMnPc1
Uherska	Uhersko	k1gNnSc2
vždy	vždy	k6eAd1
snažili	snažit	k5eAaImAgMnP
tlačit	tlačit	k5eAaImF
na	na	k7c4
prohloubení	prohloubení	k1gNnSc4
vlastní	vlastní	k2eAgFnSc2d1
suverenity	suverenita	k1gFnSc2
a	a	k8xC
snížení	snížení	k1gNnSc2
vlastních	vlastní	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
do	do	k7c2
společných	společný	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
obou	dva	k4xCgFnPc2
polovin	polovina	k1gFnPc2
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
opatrné	opatrný	k2eAgFnSc3d1
politice	politika	k1gFnSc3
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
se	se	k3xPyFc4
vždy	vždy	k6eAd1
domohli	domoct	k5eAaPmAgMnP
dalších	další	k2eAgMnPc2d1
ústupků	ústupek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
smluvním	smluvní	k2eAgMnPc3d1
závazkům	závazek	k1gInPc3
vzešlým	vzešlý	k2eAgInPc3d1
z	z	k7c2
těchto	tento	k3xDgNnPc2
jednání	jednání	k1gNnPc2
se	se	k3xPyFc4
nemohl	moct	k5eNaImAgInS
postavit	postavit	k5eAaPmF
ani	ani	k8xC
sám	sám	k3xTgMnSc1
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
politické	politický	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
země	zem	k1gFnSc2
bylo	být	k5eAaImAgNnS
trnem	trn	k1gInSc7
v	v	k7c6
oku	oko	k1gNnSc6
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
s	s	k7c7
dualismem	dualismus	k1gInSc7
chtěl	chtít	k5eAaImAgMnS
skoncovat	skoncovat	k5eAaPmF
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
i	i	k9
jeho	jeho	k3xOp3gMnSc1
následník	následník	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
Opakující	opakující	k2eAgInPc4d1
se	se	k3xPyFc4
jednání	jednání	k1gNnPc1
o	o	k7c6
rakousko-uherském	rakousko-uherský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
vnášela	vnášet	k5eAaImAgFnS
další	další	k2eAgFnSc4d1
nestabilitu	nestabilita	k1gFnSc4
do	do	k7c2
již	již	k6eAd1
tak	tak	k6eAd1
rozjitřené	rozjitřený	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
společná	společný	k2eAgNnPc1d1
ministerstva	ministerstvo	k1gNnPc1
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
osoby	osoba	k1gFnSc2
císaře	císař	k1gMnSc2
spojovala	spojovat	k5eAaImAgFnS
obě	dva	k4xCgFnPc4
části	část	k1gFnPc4
monarchie	monarchie	k1gFnSc2
i	i	k8xC
společná	společný	k2eAgNnPc1d1
ministerstva	ministerstvo	k1gNnPc1
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
úřady	úřad	k1gInPc1
společného	společný	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
financí	finance	k1gFnPc2
a	a	k8xC
zahraničí	zahraničí	k1gNnSc6
(	(	kIx(
<g/>
společný	společný	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
byl	být	k5eAaImAgInS
zároveň	zároveň	k6eAd1
předsedou	předseda	k1gMnSc7
ministerské	ministerský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pro	pro	k7c4
společné	společný	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
a	a	k8xC
tudíž	tudíž	k8xC
rakousko-uherským	rakousko-uherský	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
fakticky	fakticky	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
jeho	jeho	k3xOp3gInSc1
vliv	vliv	k1gInSc1
omezoval	omezovat	k5eAaImAgInS
na	na	k7c4
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgInPc1
ostatní	ostatní	k2eAgInPc1d1
rezorty	rezort	k1gInPc1
existovaly	existovat	k5eAaImAgInP
samostatně	samostatně	k6eAd1
v	v	k7c6
Předlitavsku	Předlitavsko	k1gNnSc6
a	a	k8xC
Zalitavsku	Zalitavsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlády	vláda	k1gFnSc2
Předlitavska	Předlitavsko	k1gNnSc2
měly	mít	k5eAaImAgInP
navíc	navíc	k6eAd1
i	i	k9
vlastní	vlastní	k2eAgInSc4d1
post	post	k1gInSc4
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
(	(	kIx(
<g/>
kompetence	kompetence	k1gFnPc1
společného	společný	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
byly	být	k5eAaImAgInP
značně	značně	k6eAd1
omezené	omezený	k2eAgInPc1d1
<g/>
)	)	kIx)
a	a	k8xC
ministra	ministr	k1gMnSc2
zeměbrany	zeměbrana	k1gFnSc2
(	(	kIx(
<g/>
zodpovídal	zodpovídat	k5eAaPmAgInS,k5eAaImAgInS
za	za	k7c4
předlitavské	předlitavský	k2eAgFnPc4d1
ozbrojené	ozbrojený	k2eAgFnPc4d1
síly	síla	k1gFnPc4
(	(	kIx(
<g/>
zeměbrana	zeměbrana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
doplňovaly	doplňovat	k5eAaImAgFnP
společnou	společný	k2eAgFnSc4d1
rakousko-uherskou	rakousko-uherský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společné	společný	k2eAgInPc1d1
úřady	úřad	k1gInPc1
nesly	nést	k5eAaImAgInP
označení	označení	k1gNnSc4
c.	c.	k?
a	a	k8xC
k.	k.	k?
(	(	kIx(
<g/>
císařský	císařský	k2eAgMnSc1d1
a	a	k8xC
královský	královský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
předlitavské	předlitavský	k2eAgNnSc1d1
c.	c.	k?
k.	k.	k?
(	(	kIx(
<g/>
císařský	císařský	k2eAgMnSc1d1
královský	královský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zalitavské	zalitavský	k2eAgFnSc2d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
uher	uher	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
královský	královský	k2eAgMnSc1d1
uherský	uherský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
měla	mít	k5eAaImAgFnS
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
Rakousko-Uherskem	Rakousko-Uhersek	k1gInSc7
okupována	okupován	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1878	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1908	#num#	k4
anektována	anektovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
podřízena	podřídit	k5eAaPmNgFnS
přímo	přímo	k6eAd1
císařské	císařský	k2eAgFnSc3d1
správě	správa	k1gFnSc3
<g/>
,	,	kIx,
resp.	resp.	kA
spravovalo	spravovat	k5eAaImAgNnS
ji	on	k3xPp3gFnSc4
společné	společný	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
však	však	k9
nebyli	být	k5eNaImAgMnP
v	v	k7c6
plně	plně	k6eAd1
rovnoprávném	rovnoprávný	k2eAgNnSc6d1
postavení	postavení	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
zemí	zem	k1gFnPc2
R-U	R-U	k1gFnSc2
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
(	(	kIx(
<g/>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Rozloha	rozloha	k1gFnSc1
v	v	k7c6
km²	km²	k?
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Království	království	k1gNnPc2
české	český	k2eAgFnPc1d1
(	(	kIx(
<g/>
Čechy	Čechy	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
668	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51948.00000051	51948.00000051	k4
948	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6769000.0000006	6769000.0000006	k4
769	#num#	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Dalmácie	Dalmácie	k1gFnSc1
Království	království	k1gNnSc1
dalmatské	dalmatský	k2eAgFnSc2d1
(	(	kIx(
<g/>
Dalmácie	Dalmácie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Zadar	Zadar	k1gInSc1
(	(	kIx(
<g/>
14	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
12833.00000012	12833.00000012	k4
833	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
646000.000000646	646000.000000646	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Halič	Halič	k1gFnSc1
Království	království	k1gNnSc2
haličsko-vladiměřské	haličsko-vladiměřský	k2eAgNnSc1d1
(	(	kIx(
<g/>
Halič	Halič	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Lvov	Lvov	k1gInSc1
(	(	kIx(
<g/>
206	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
78493.00000078	78493.00000078	k4
493	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8025000.0000008	8025000.0000008	k4
025	#num#	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
Arcivévodství	arcivévodství	k1gNnSc2
Rakousy	Rakousy	k1gInPc4
pod	pod	k7c4
Enží	Enží	k1gFnSc4
(	(	kIx(
<g/>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
031	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
19822.00000019	19822.00000019	k4
822	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3532000.0000003	3532000.0000003	k4
532	#num#	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Horní	horní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
Arcivévodství	arcivévodství	k1gNnSc2
Rakousy	Rakousy	k1gInPc4
nad	nad	k7c4
Enží	Enží	k1gFnSc4
(	(	kIx(
<g/>
Horní	horní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Linec	Linec	k1gInSc1
(	(	kIx(
<g/>
71	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11981.00000011	11981.00000011	k4
981	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
853000.000000853	853000.000000853	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Bukovina	Bukovina	k1gFnSc1
Vévodství	vévodství	k1gNnSc2
bukovinské	bukovinský	k2eAgFnSc2d1
</s>
<s>
Černovice	Černovice	k1gFnSc1
(	(	kIx(
<g/>
87	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10442.00000010	10442.00000010	k4
442	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
800000.000000800	800000.000000800	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Korutany	Korutany	k1gInPc1
Vévodství	vévodství	k1gNnSc2
korutanské	korutanský	k2eAgFnSc2d1
</s>
<s>
Celovec	Celovec	k1gInSc1
(	(	kIx(
<g/>
29	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10327.00000010	10327.00000010	k4
327	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
396000.000000396	396000.000000396	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Kraňsko	Kraňsko	k1gNnSc1
Vévodství	vévodství	k1gNnSc2
kraňské	kraňský	k2eAgFnSc2d1
</s>
<s>
Lublaň	Lublaň	k1gFnSc1
(	(	kIx(
<g/>
47	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9955.0000009	9955.0000009	k4
955	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
526000.000000526	526000.000000526	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Salcbursko	Salcbursko	k1gNnSc1
Vévodství	vévodství	k1gNnSc2
salcburské	salcburský	k2eAgFnSc2d1
</s>
<s>
Salzburg	Salzburg	k1gInSc1
(	(	kIx(
<g/>
36	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7153.0000007	7153.0000007	k4
153	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
215000.000000215	215000.000000215	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
Vévodství	vévodství	k1gNnSc2
slezské	slezský	k2eAgFnSc2d1
</s>
<s>
Opava	Opava	k1gFnSc1
(	(	kIx(
<g/>
31	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5147.0000005	5147.0000005	k4
147	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
757000.000000757	757000.000000757	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Štýrsko	Štýrsko	k1gNnSc1
Vévodství	vévodství	k1gNnSc2
štýrské	štýrský	k2eAgFnSc2d1
</s>
<s>
Štýrský	štýrský	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
(	(	kIx(
<g/>
152	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22426.00000022	22426.00000022	k4
426	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1444000.0000001	1444000.0000001	k4
444	#num#	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Markrabství	markrabství	k1gNnSc2
moravské	moravský	k2eAgNnSc1d1
(	(	kIx(
<g/>
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
126	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22222.00000022	22222.00000022	k4
222	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2622000.0000002	2622000.0000002	k4
622	#num#	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Tyrolské	tyrolský	k2eAgNnSc1d1
Okněžněné	Okněžněný	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
tyrolské	tyrolský	k2eAgFnSc2d1
</s>
<s>
Innsbruck	Innsbruck	k1gInSc1
(	(	kIx(
<g/>
53	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
26683.00000026	26683.00000026	k4
683	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
946000.000000946	946000.000000946	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Přímoří	Přímoří	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
přímoří	přímoří	k1gNnSc1
(	(	kIx(
<g/>
Město	město	k1gNnSc1
a	a	k8xC
území	území	k1gNnSc1
Terst	Terst	k1gInSc1
<g/>
,	,	kIx,
Okněžněné	Okněžněný	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
Gorice	Goric	k1gMnSc2
a	a	k8xC
Gradiška	Gradišek	k1gMnSc2
<g/>
,	,	kIx,
Markrabství	markrabství	k1gNnSc1
Istrie	Istrie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Terst	Terst	k1gInSc1
(	(	kIx(
<g/>
161	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7969.0000007	7969.0000007	k4
969	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
895000.000000895	895000.000000895	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Vorarlbersko	Vorarlbersko	k1gNnSc1
Vorarlbersko	Vorarlbersko	k1gNnSc1
</s>
<s>
Bregenz	Bregenz	k1gInSc1
(	(	kIx(
<g/>
9	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2601.0000002	2601.0000002	k4
601	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
145000.000000145	145000.000000145	k4
000	#num#	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
</s>
<s>
Uhersko	Uhersko	k1gNnSc1
Království	království	k1gNnSc2
uherské	uherský	k2eAgFnSc2d1
</s>
<s>
Budapešť	Budapešť	k1gFnSc1
(	(	kIx(
<g/>
882	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
282297.000000282	282297.000000282	k4
297	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18265000.00000018	18265000.00000018	k4
265	#num#	k4
000	#num#	k4
</s>
<s>
Zalitavsko	Zalitavsko	k1gNnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
Království	království	k1gNnSc2
chorvatsko-slavonské	chorvatsko-slavonský	k2eAgFnSc2d1
</s>
<s>
Záhřeb	Záhřeb	k1gInSc1
(	(	kIx(
<g/>
80	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
42534.00000042	42534.00000042	k4
534	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2622000.0000002	2622000.0000002	k4
622	#num#	k4
000	#num#	k4
</s>
<s>
Zalitavsko	Zalitavsko	k1gNnSc1
</s>
<s>
Rijeka	Rijeka	k1gFnSc1
</s>
<s>
Rijeka	Rijeka	k1gFnSc1
(	(	kIx(
<g/>
39	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
21.000	21.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
21	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
48800.00000048	48800.00000048	k4
800	#num#	k4
</s>
<s>
Zalitavsko	Zalitavsko	k1gNnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1
(	(	kIx(
<g/>
52	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51082.00000051	51082.00000051	k4
082	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1932000.0000001	1932000.0000001	k4
932	#num#	k4
000	#num#	k4
</s>
<s>
společná	společný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Předlitavska	Předlitavsko	k1gNnSc2
a	a	k8xC
Zalitavska	Zalitavsko	k1gNnSc2
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
</s>
<s>
Jazyky	jazyk	k1gInPc1
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
němčina	němčina	k1gFnSc1
</s>
<s>
maďarština	maďarština	k1gFnSc1
</s>
<s>
čeština	čeština	k1gFnSc1
</s>
<s>
srbština	srbština	k1gFnSc1
a	a	k8xC
chorvatština	chorvatština	k1gFnSc1
</s>
<s>
polština	polština	k1gFnSc1
</s>
<s>
ukrajinština	ukrajinština	k1gFnSc1
(	(	kIx(
<g/>
rusínština	rusínština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
rumunština	rumunština	k1gFnSc1
</s>
<s>
slovenština	slovenština	k1gFnSc1
</s>
<s>
slovinština	slovinština	k1gFnSc1
</s>
<s>
italština	italština	k1gFnSc1
</s>
<s>
ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
23,36	23,36	k4
%	%	kIx~
</s>
<s>
19,57	19,57	k4
%	%	kIx~
</s>
<s>
12,54	12,54	k4
%	%	kIx~
</s>
<s>
9,68	9,68	k4
%	%	kIx~
</s>
<s>
8,52	8,52	k4
%	%	kIx~
</s>
<s>
7,78	7,78	k4
%	%	kIx~
</s>
<s>
6,27	6,27	k4
%	%	kIx~
</s>
<s>
3,83	3,83	k4
%	%	kIx~
</s>
<s>
2,44	2,44	k4
%	%	kIx~
</s>
<s>
1,5	1,5	k4
%	%	kIx~
</s>
<s>
4,51	4,51	k4
%	%	kIx~
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
potažmo	potažmo	k6eAd1
národů	národ	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
</s>
<s>
O	o	k7c6
obyvatelstvu	obyvatelstvo	k1gNnSc6
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
máme	mít	k5eAaImIp1nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
přesné	přesný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
z	z	k7c2
pravidelných	pravidelný	k2eAgNnPc2d1
sčítání	sčítání	k1gNnPc2
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
bylo	být	k5eAaImAgNnS
sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
1869	#num#	k4
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
a	a	k8xC
od	od	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
1880	#num#	k4
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
pak	pak	k6eAd1
probíhalo	probíhat	k5eAaImAgNnS
vždy	vždy	k6eAd1
po	po	k7c6
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
údaje	údaj	k1gInPc1
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
ze	z	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
1910	#num#	k4
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sčítacích	sčítací	k2eAgInPc6d1
formulářích	formulář	k1gInPc6
byla	být	k5eAaImAgFnS
otázka	otázka	k1gFnSc1
na	na	k7c4
obcovací	obcovací	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
nepřímo	přímo	k6eNd1
dala	dát	k5eAaPmAgFnS
určit	určit	k5eAaPmF
skladba	skladba	k1gFnSc1
národností	národnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c4
celé	celý	k2eAgNnSc4d1
období	období	k1gNnSc4
dualismu	dualismus	k1gInSc2
v	v	k7c6
letech	let	k1gInPc6
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
můžeme	moct	k5eAaImIp1nP
sledovat	sledovat	k5eAaImF
trvalý	trvalý	k2eAgInSc4d1
růst	růst	k1gInSc4
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
navíc	navíc	k6eAd1
roku	rok	k1gInSc2
1908	#num#	k4
umocněn	umocnit	k5eAaPmNgInS
anexí	anexe	k1gFnSc7
dvoumilionové	dvoumilionový	k2eAgFnSc2d1
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
podle	podle	k7c2
údajů	údaj	k1gInPc2
sčítání	sčítání	k1gNnSc2
z	z	k7c2
let	léto	k1gNnPc2
1869	#num#	k4
a	a	k8xC
1910	#num#	k4
zvýšil	zvýšit	k5eAaPmAgInS
z	z	k7c2
35,812	35,812	k4
milionů	milion	k4xCgInPc2
na	na	k7c4
51,39	51,39	k4
milionu	milion	k4xCgInSc2
<g/>
.	.	kIx.
54	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
monarchie	monarchie	k1gFnSc2
žilo	žít	k5eAaImAgNnS
v	v	k7c6
obcích	obec	k1gFnPc6
menších	malý	k2eAgFnPc2d2
než	než	k8xS
2000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
tento	tento	k3xDgInSc4
podíl	podíl	k1gInSc4
po	po	k7c4
celé	celý	k2eAgNnSc4d1
období	období	k1gNnSc4
neustále	neustále	k6eAd1
klesal	klesat	k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
51,39	51,39	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
nejvíce	nejvíce	k6eAd1,k6eAd3
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
používání	používání	k1gNnSc3
německého	německý	k2eAgInSc2d1
a	a	k8xC
maďarského	maďarský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádný	žádný	k1gMnSc1
z	z	k7c2
národů	národ	k1gInPc2
však	však	k9
nebyl	být	k5eNaImAgInS
početně	početně	k6eAd1
zcela	zcela	k6eAd1
dominantní	dominantní	k2eAgNnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
způsobovalo	způsobovat	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
prakticky	prakticky	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgNnPc6
národnostních	národnostní	k2eAgNnPc6d1
společenstvích	společenství	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
nepřímé	přímý	k2eNgFnSc3d1
metodě	metoda	k1gFnSc3
zjišťování	zjišťování	k1gNnSc2
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
tyto	tento	k3xDgMnPc4
odrážejí	odrážet	k5eAaImIp3nP
jen	jen	k9
částečně	částečně	k6eAd1
v	v	k7c6
číslech	číslo	k1gNnPc6
<g/>
,	,	kIx,
např.	např.	kA
židé	žid	k1gMnPc1
a	a	k8xC
státní	státní	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
různých	různý	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
se	se	k3xPyFc4
často	často	k6eAd1
hlásili	hlásit	k5eAaImAgMnP
k	k	k7c3
používání	používání	k1gNnSc3
němčiny	němčina	k1gFnSc2
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
objevily	objevit	k5eAaPmAgInP
případy	případ	k1gInPc1
vzájemného	vzájemný	k2eAgNnSc2d1
obviňování	obviňování	k1gNnSc2
mezi	mezi	k7c7
Čechy	Čech	k1gMnPc7
a	a	k8xC
Němci	Němec	k1gMnPc7
ze	z	k7c2
zmanipulování	zmanipulování	k1gNnSc2
výsledků	výsledek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Badeniho	Badenize	k6eAd1
jazyková	jazykový	k2eAgNnPc1d1
nařízení	nařízení	k1gNnPc1
z	z	k7c2
dubna	duben	k1gInSc2
1897	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zrovnoprávnila	zrovnoprávnit	k5eAaPmAgFnS
češtinu	čeština	k1gFnSc4
s	s	k7c7
němčinou	němčina	k1gFnSc7
<g/>
,	,	kIx,
vedla	vést	k5eAaImAgFnS
k	k	k7c3
pádu	pád	k1gInSc3
Badeniho	Badeni	k1gMnSc2
vlády	vláda	k1gFnSc2
a	a	k8xC
k	k	k7c3
národnostním	národnostní	k2eAgInPc3d1
nepokojům	nepokoj	k1gInPc3
mezi	mezi	k7c7
Čechy	Čech	k1gMnPc7
a	a	k8xC
Němci	Němec	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Ani	ani	k8xC
další	další	k2eAgNnPc1d1
jazyková	jazykový	k2eAgNnPc1d1
nařízení	nařízení	k1gNnPc1
neprošla	projít	k5eNaPmAgNnP
a	a	k8xC
posílení	posílení	k1gNnSc3
česko-německého	česko-německý	k2eAgInSc2d1
antagonismu	antagonismus	k1gInSc2
usnadnilo	usnadnit	k5eAaPmAgNnS
rozpad	rozpad	k1gInSc4
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tzv.	tzv.	kA
Svatodušním	svatodušní	k2eAgInSc6d1
programu	program	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1899	#num#	k4
německé	německý	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
z	z	k7c2
českých	český	k2eAgFnPc2d1
i	i	k8xC
alpských	alpský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
odmítly	odmítnout	k5eAaPmAgInP
jazykové	jazykový	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
Čechů	Čech	k1gMnPc2
i	i	k8xC
federalizaci	federalizace	k1gFnSc4
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ekonomika	ekonomika	k1gFnSc1
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1
<g/>
.20	.20	k4
korun	koruna	k1gFnPc2
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
se	se	k3xPyFc4
během	běh	k1gInSc7
existence	existence	k1gFnSc2
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
výrazně	výrazně	k6eAd1
změnila	změnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
urychlily	urychlit	k5eAaPmAgFnP
industrializaci	industrializace	k1gFnSc4
a	a	k8xC
urbanizaci	urbanizace	k1gFnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemi	zem	k1gFnSc6
během	během	k7c2
její	její	k3xOp3gFnSc2
padesátileté	padesátiletý	k2eAgFnSc2d1
existence	existence	k1gFnSc2
vystřídaly	vystřídat	k5eAaPmAgInP
kapitalistické	kapitalistický	k2eAgInPc1d1
výrobní	výrobní	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
tradiční	tradiční	k2eAgFnSc4d1
řemeslnou	řemeslný	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomický	ekonomický	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
byl	být	k5eAaImAgInS
soustředěn	soustředit	k5eAaPmNgInS
okolo	okolo	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
rakouských	rakouský	k2eAgFnPc6d1
<g/>
,	,	kIx,
alpských	alpský	k2eAgFnPc6d1
a	a	k8xC
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
rychle	rychle	k6eAd1
ekonomicky	ekonomicky	k6eAd1
rozvíjely	rozvíjet	k5eAaImAgFnP
oblasti	oblast	k1gFnPc1
v	v	k7c6
centrálním	centrální	k2eAgNnSc6d1
Uhersku	Uhersko	k1gNnSc6
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Karpat	Karpaty	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
byly	být	k5eAaImAgInP
velké	velký	k2eAgInPc1d1
regionální	regionální	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
ve	v	k7c6
stupni	stupeň	k1gInSc6
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnPc1d1
části	část	k1gFnPc1
impéria	impérium	k1gNnSc2
byly	být	k5eAaImAgFnP
všeobecně	všeobecně	k6eAd1
rozvinutější	rozvinutý	k2eAgFnPc4d2
než	než	k8xS
východní	východní	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
ovšem	ovšem	k9
ekonomické	ekonomický	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
postupně	postupně	k6eAd1
zmenšovaly	zmenšovat	k5eAaImAgInP
<g/>
,	,	kIx,
protože	protože	k8xS
růst	růst	k1gInSc1
ekonomiky	ekonomika	k1gFnSc2
ve	v	k7c6
východních	východní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
byl	být	k5eAaImAgMnS
rychlejší	rychlý	k2eAgMnSc1d2
než	než	k8xS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silná	silný	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
a	a	k8xC
potravinářský	potravinářský	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
hrály	hrát	k5eAaImAgInP
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
jak	jak	k8xC,k8xS
v	v	k7c6
samotné	samotný	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
tak	tak	k6eAd1
v	v	k7c6
exportu	export	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Mezitím	mezitím	k6eAd1
západní	západní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
především	především	k6eAd1
oblasti	oblast	k1gFnPc1
okolo	okolo	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
vynikly	vyniknout	k5eAaPmAgFnP
v	v	k7c6
několika	několik	k4yIc6
výrobních	výrobní	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
mezi	mezi	k7c7
východní	východní	k2eAgFnSc7d1
a	a	k8xC
západní	západní	k2eAgFnSc7d1
částí	část	k1gFnSc7
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
společné	společný	k2eAgFnSc2d1
monetární	monetární	k2eAgFnSc2d1
a	a	k8xC
ekonomické	ekonomický	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vedla	vést	k5eAaImAgFnS
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
rychlému	rychlý	k2eAgInSc3d1
ekonomickému	ekonomický	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
HDP	HDP	kA
na	na	k7c4
hlavu	hlava	k1gFnSc4
rostlo	růst	k5eAaImAgNnS
ročně	ročně	k6eAd1
přibližně	přibližně	k6eAd1
o	o	k7c4
1,76	1,76	k4
%	%	kIx~
v	v	k7c6
letech	léto	k1gNnPc6
1870	#num#	k4
až	až	k9
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
míra	míra	k1gFnSc1
růstu	růst	k1gInSc2
převyšovala	převyšovat	k5eAaImAgFnS
míru	míra	k1gFnSc4
růstu	růst	k1gInSc2
ostatních	ostatní	k2eAgInPc2d1
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
(	(	kIx(
<g/>
1,00	1,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
1,06	1,06	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
1,51	1,51	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
celková	celkový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
ekonomiky	ekonomika	k1gFnSc2
stále	stále	k6eAd1
znatelně	znatelně	k6eAd1
zaostávala	zaostávat	k5eAaImAgFnS
za	za	k7c7
ekonomikami	ekonomika	k1gFnPc7
nejvyspělejších	vyspělý	k2eAgFnPc2d3
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
systematická	systematický	k2eAgFnSc1d1
modernizace	modernizace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
mnohem	mnohem	k6eAd1
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
věnovalo	věnovat	k5eAaImAgNnS,k5eAaPmAgNnS
velkou	velký	k2eAgFnSc4d1
péči	péče	k1gFnSc4
rozvoji	rozvoj	k1gInSc3
infrastruktury	infrastruktura	k1gFnSc2
(	(	kIx(
<g/>
výstavbě	výstavba	k1gFnSc3
železnic	železnice	k1gFnPc2
<g/>
,	,	kIx,
silnic	silnice	k1gFnPc2
<g/>
,	,	kIx,
telegrafních	telegrafní	k2eAgFnPc2d1
a	a	k8xC
telefonních	telefonní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
pošt	pošta	k1gFnPc2
<g/>
,	,	kIx,
veřejných	veřejný	k2eAgInPc2d1
vodovodů	vodovod	k1gInPc2
a	a	k8xC
kanalizací	kanalizace	k1gFnPc2
<g/>
,	,	kIx,
školství	školství	k1gNnSc1
<g/>
,	,	kIx,
zakládání	zakládání	k1gNnSc1
vědeckých	vědecký	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
fungující	fungující	k2eAgFnSc3d1
státní	státní	k2eAgFnSc3d1
správě	správa	k1gFnSc3
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc6
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Krach	krach	k1gInSc1
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
burze	burza	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1873	#num#	k4
odstartoval	odstartovat	k5eAaPmAgMnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
největší	veliký	k2eAgFnSc4d3
ekonomickou	ekonomický	k2eAgFnSc4d1
recesi	recese	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
zemí	zem	k1gFnPc2
Předlitavska	Předlitavsko	k1gNnSc2
</s>
<s>
České	český	k2eAgFnPc1d1
země	zem	k1gFnPc1
vytvářely	vytvářet	k5eAaImAgFnP
zhruba	zhruba	k6eAd1
45	#num#	k4
%	%	kIx~
národního	národní	k2eAgInSc2d1
důchodu	důchod	k1gInSc2
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Předlitavska	Předlitavsko	k1gNnSc2
v	v	k7c6
letech	let	k1gInPc6
1911	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
celkem	celkem	k6eAd1
</s>
<s>
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
na	na	k7c4
hlavu	hlava	k1gFnSc4
</s>
<s>
v	v	k7c6
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
korun	koruna	k1gFnPc2
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
v	v	k7c6
korunách	koruna	k1gFnPc6
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
Čechy	Čechy	k1gFnPc1
</s>
<s>
5	#num#	k4
158	#num#	k4
</s>
<s>
31,6	31,6	k4
</s>
<s>
761	#num#	k4
</s>
<s>
133,7	133,7	k4
</s>
<s>
Morava	Morava	k1gFnSc1
</s>
<s>
1	#num#	k4
703	#num#	k4
</s>
<s>
10,4	10,4	k4
</s>
<s>
648	#num#	k4
</s>
<s>
113,9	113,9	k4
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
</s>
<s>
472	#num#	k4
</s>
<s>
3,0	3,0	k4
</s>
<s>
619	#num#	k4
</s>
<s>
108,8	108,8	k4
</s>
<s>
České	český	k2eAgFnPc1d1
země	zem	k1gFnPc1
celkem	celkem	k6eAd1
</s>
<s>
7	#num#	k4
333	#num#	k4
</s>
<s>
45,0	45,0	k4
</s>
<s>
676	#num#	k4
</s>
<s>
118,8	118,8	k4
</s>
<s>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
vč.	vč.	k?
Vídně	Vídeň	k1gFnSc2
</s>
<s>
3	#num#	k4
011	#num#	k4
</s>
<s>
18,5	18,5	k4
</s>
<s>
850	#num#	k4
</s>
<s>
149,4	149,4	k4
</s>
<s>
Horní	horní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
</s>
<s>
532	#num#	k4
</s>
<s>
3,2	3,2	k4
</s>
<s>
626	#num#	k4
</s>
<s>
110,0	110,0	k4
</s>
<s>
Salcbursko	Salcbursko	k6eAd1
</s>
<s>
139	#num#	k4
</s>
<s>
0,9	0,9	k4
</s>
<s>
641	#num#	k4
</s>
<s>
112,6	112,6	k4
</s>
<s>
Štýrsko	Štýrsko	k1gNnSc1
</s>
<s>
750	#num#	k4
</s>
<s>
4,6	4,6	k4
</s>
<s>
519	#num#	k4
</s>
<s>
91,2	91,2	k4
</s>
<s>
Korutany	Korutany	k1gInPc1
</s>
<s>
220	#num#	k4
</s>
<s>
1,3	1,3	k4
</s>
<s>
556	#num#	k4
</s>
<s>
97,7	97,7	k4
</s>
<s>
Tyrolsko	Tyrolsko	k1gNnSc1
<g/>
,	,	kIx,
Vorarlbersko	Vorarlbersko	k1gNnSc1
</s>
<s>
655	#num#	k4
</s>
<s>
4,0	4,0	k4
</s>
<s>
660	#num#	k4
</s>
<s>
116,0	116,0	k4
</s>
<s>
Rakouské	rakouský	k2eAgFnPc1d1
země	zem	k1gFnPc1
celkem	celkem	k6eAd1
</s>
<s>
5	#num#	k4
307	#num#	k4
</s>
<s>
32,5	32,5	k4
</s>
<s>
632	#num#	k4
</s>
<s>
111,1	111,1	k4
</s>
<s>
Kraňsko	Kraňsko	k1gNnSc1
</s>
<s>
230	#num#	k4
</s>
<s>
1,4	1,4	k4
</s>
<s>
439	#num#	k4
</s>
<s>
77,2	77,2	k4
</s>
<s>
Rakouské	rakouský	k2eAgNnSc1d1
přímoří	přímoří	k1gNnSc1
</s>
<s>
464	#num#	k4
</s>
<s>
2,8	2,8	k4
</s>
<s>
522	#num#	k4
</s>
<s>
91,7	91,7	k4
</s>
<s>
Dalmácie	Dalmácie	k1gFnSc1
</s>
<s>
171	#num#	k4
</s>
<s>
1,1	1,1	k4
</s>
<s>
264	#num#	k4
</s>
<s>
46,4	46,4	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
země	země	k1gFnSc1
celkem	celkem	k6eAd1
</s>
<s>
865	#num#	k4
</s>
<s>
5,3	5,3	k4
</s>
<s>
408	#num#	k4
</s>
<s>
71,7	71,7	k4
</s>
<s>
Halič	Halič	k1gFnSc1
</s>
<s>
2	#num#	k4
546	#num#	k4
</s>
<s>
15,6	15,6	k4
</s>
<s>
316	#num#	k4
</s>
<s>
55,5	55,5	k4
</s>
<s>
Bukovina	Bukovina	k1gFnSc1
</s>
<s>
250	#num#	k4
</s>
<s>
1,6	1,6	k4
</s>
<s>
310	#num#	k4
</s>
<s>
54,5	54,5	k4
</s>
<s>
Karpatské	karpatský	k2eAgFnPc1d1
země	zem	k1gFnPc1
celkem	celkem	k6eAd1
</s>
<s>
2	#num#	k4
796	#num#	k4
</s>
<s>
17,2	17,2	k4
</s>
<s>
313	#num#	k4
</s>
<s>
55,0	55,0	k4
</s>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1
celkem	celkem	k6eAd1
</s>
<s>
16	#num#	k4
301	#num#	k4
</s>
<s>
100,0	100,0	k4
</s>
<s>
569	#num#	k4
</s>
<s>
100,0	100,0	k4
</s>
<s>
Státní	státní	k2eAgInPc1d1
symboly	symbol	k1gInPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vlajky	vlajka	k1gFnSc2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
univerzální	univerzální	k2eAgFnSc4d1
společnou	společný	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
jejím	její	k3xOp3gNnSc6
místě	místo	k1gNnSc6
byly	být	k5eAaImAgFnP
užívány	užíván	k2eAgFnPc1d1
různé	různý	k2eAgFnPc1d1
vlajky	vlajka	k1gFnPc1
služební	služební	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Civilní	civilní	k2eAgInSc4d1
loďstvo	loďstvo	k1gNnSc1
a	a	k8xC
zahraniční	zahraniční	k2eAgInPc1d1
zastupitelské	zastupitelský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
používaly	používat	k5eAaImAgInP
tzv.	tzv.	kA
obchodní	obchodní	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
se	s	k7c7
znaky	znak	k1gInPc7
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
vojenské	vojenský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
se	se	k3xPyFc4
identifikovalo	identifikovat	k5eAaBmAgNnS
vlajkou	vlajka	k1gFnSc7
s	s	k7c7
rakouským	rakouský	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
již	již	k6eAd1
roku	rok	k1gInSc2
1786	#num#	k4
<g/>
;	;	kIx,
její	její	k3xOp3gFnSc1
změna	změna	k1gFnSc1
<g/>
,	,	kIx,
nařízená	nařízený	k2eAgFnSc1d1
v	v	k7c6
r.	r.	kA
1915	#num#	k4
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
realizována	realizován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
mimoto	mimoto	k6eAd1
zároveň	zároveň	k6eAd1
používala	používat	k5eAaImAgFnS
i	i	k9
vlajky	vlajka	k1gFnPc4
se	s	k7c7
symbolem	symbol	k1gInSc7
dvouhlavého	dvouhlavý	k2eAgMnSc2d1
orla	orel	k1gMnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
tyto	tento	k3xDgInPc1
prapory	prapor	k1gInPc1
v	v	k7c6
řadě	řada	k1gFnSc6
případů	případ	k1gInPc2
měly	mít	k5eAaImAgInP
bohatou	bohatý	k2eAgFnSc4d1
historii	historie	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
vojsko	vojsko	k1gNnSc1
nechtělo	chtít	k5eNaImAgNnS
vzdát	vzdát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
státních	státní	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
pak	pak	k6eAd1
byly	být	k5eAaImAgInP
používány	používat	k5eAaImNgInP
žluto-černé	žluto-černý	k2eAgInPc1d1
rakouské	rakouský	k2eAgInPc1d1
vlajky	vlajka	k1gFnPc4
a	a	k8xC
uherská	uherský	k2eAgFnSc1d1
trikolora	trikolora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Předlitavska	Předlitavsko	k1gNnSc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Uherského	uherský	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
Zalitavska	Zalitavsko	k1gNnSc2
<g/>
)	)	kIx)
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
užívaná	užívaný	k2eAgFnSc1d1
v	v	k7c6
Chorvatsko-slavonském	chorvatsko-slavonský	k2eAgNnSc6d1
království	království	k1gNnSc6
</s>
<s>
Společná	společný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
plukovních	plukovní	k2eAgFnPc2d1
vlajek	vlajka	k1gFnPc2
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
1869	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
1786	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Dvouhlavý	dvouhlavý	k2eAgMnSc1d1
císařský	císařský	k2eAgMnSc1d1
orel	orel	k1gMnSc1
byl	být	k5eAaImAgMnS
symbolem	symbol	k1gInSc7
celé	celá	k1gFnSc2
monarchie	monarchie	k1gFnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
společný	společný	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
však	však	k9
vzhledem	vzhledem	k7c3
k	k	k7c3
válečným	válečný	k2eAgFnPc3d1
událostem	událost	k1gFnPc3
nebyl	být	k5eNaImAgInS
fakticky	fakticky	k6eAd1
realizován	realizovat	k5eAaBmNgInS
a	a	k8xC
byl	být	k5eAaImAgInS
ostatně	ostatně	k6eAd1
i	i	k9
heraldicky	heraldicky	k6eAd1
a	a	k8xC
ústavně	ústavně	k6eAd1
nesprávný	správný	k2eNgMnSc1d1
<g/>
;	;	kIx,
Josef	Josef	k1gMnSc1
Pekař	Pekař	k1gMnSc1
dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
císařský	císařský	k2eAgInSc1d1
rod	rod	k1gInSc1
vzdal	vzdát	k5eAaPmAgInS
české	český	k2eAgFnPc4d1
koruny	koruna	k1gFnPc4
ve	v	k7c6
znaku	znak	k1gInSc6
<g/>
,	,	kIx,
vzdal	vzdát	k5eAaPmAgMnS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
i	i	k9
vlády	vláda	k1gFnSc2
na	na	k7c4
českými	český	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Střední	střední	k2eAgInSc1d1
znak	znak	k1gInSc1
Rakousko-Uherska	Rakousko-Uherska	k1gFnSc1
1867	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
</s>
<s>
Malý	malý	k2eAgInSc1d1
znak	znak	k1gInSc1
Rakousko-Uherska	Rakousko-Uherska	k1gFnSc1
1867	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
</s>
<s>
Velký	velký	k2eAgInSc1d1
znak	znak	k1gInSc1
Rakousko-Uherska	Rakousko-Uherska	k1gFnSc1
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Malý	malý	k2eAgInSc1d1
znak	znak	k1gInSc1
Rakousko-Uherska	Rakousko-Uherska	k1gFnSc1
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Obě	dva	k4xCgFnPc1
části	část	k1gFnPc1
monarchie	monarchie	k1gFnSc2
si	se	k3xPyFc3
však	však	k9
stále	stále	k6eAd1
ponechávaly	ponechávat	k5eAaImAgFnP
i	i	k9
své	svůj	k3xOyFgInPc4
symboly	symbol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
byly	být	k5eAaImAgFnP
změněny	změnit	k5eAaPmNgInP
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
se	s	k7c7
společným	společný	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
znaku	znak	k1gInSc2
Uher	Uhry	k1gFnPc2
nebyly	být	k5eNaImAgFnP
změny	změna	k1gFnPc1
tak	tak	k6eAd1
radikální	radikální	k2eAgInPc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gInSc1
předlitavský	předlitavský	k2eAgInSc1d1
protějšek	protějšek	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
nově	nově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
byly	být	k5eAaImAgInP
navrženy	navrhnout	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
spojením	spojení	k1gNnSc7
vytvořily	vytvořit	k5eAaPmAgInP
společný	společný	k2eAgInSc4d1
velký	velký	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
znaků	znak	k1gInPc2
nepřímo	přímo	k6eNd1
vycházely	vycházet	k5eAaImAgFnP
i	i	k9
jejich	jejich	k3xOp3gFnPc1
malé	malý	k2eAgFnPc1d1
verze	verze	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
níže	nízce	k6eAd2
též	též	k9
zobrazeny	zobrazit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc4d1
znak	znak	k1gInSc4
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Malý	malý	k2eAgInSc1d1
znak	znak	k1gInSc1
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Velký	velký	k2eAgInSc1d1
znak	znak	k1gInSc1
Uher	uher	k1gInSc1
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Malý	malý	k2eAgInSc1d1
znak	znak	k1gInSc1
Uher	uher	k1gInSc1
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Ve	v	k7c6
sčítáních	sčítání	k1gNnPc6
lidu	lid	k1gInSc2
označována	označovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
čeština-moravština-slováčtina	čeština-moravština-slováčtina	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
Böhmisch-mährisch-slowakisch	Böhmisch-mährisch-slowakisch	k1gInSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Od	od	k7c2
roku	rok	k1gInSc2
1850	#num#	k4
byly	být	k5eAaImAgFnP
srbskými	srbský	k2eAgMnPc7d1
a	a	k8xC
chorvatskými	chorvatský	k2eAgMnPc7d1
vzdělanci	vzdělanec	k1gMnPc7
oba	dva	k4xCgInPc1
jazyky	jazyk	k1gInPc1
považovány	považován	k2eAgInPc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
dvojím	dvojí	k4xRgInSc7
písemným	písemný	k2eAgInSc7d1
zápisem	zápis	k1gInSc7
(	(	kIx(
<g/>
cyrilicí	cyrilice	k1gFnSc7
a	a	k8xC
latinkou	latinka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Dobově	dobově	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
„	„	k?
<g/>
rusínština	rusínština	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
Ruthenisch	Ruthenisch	k1gInSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Tento	tento	k3xDgInSc4
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
či	či	k8xC
spíše	spíše	k9
soubor	soubor	k1gInSc1
nářečí	nářečí	k1gNnSc2
<g/>
,	,	kIx,
neměl	mít	k5eNaImAgInS
oficiální	oficiální	k2eAgInSc4d1
status	status	k1gInSc4
a	a	k8xC
Židé	Žid	k1gMnPc1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
hlásit	hlásit	k5eAaImF
ve	v	k7c6
sčítáních	sčítání	k1gNnPc6
lidu	lid	k1gInSc2
k	k	k7c3
některému	některý	k3yIgInSc3
z	z	k7c2
uznaných	uznaný	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
↑	↑	k?
Vyrovnání	vyrovnání	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
maďarštině	maďarština	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
kompromis	kompromis	k1gInSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Parlamenty	parlament	k1gInPc1
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
se	se	k3xPyFc4
neshodly	shodnout	k5eNaPmAgFnP
na	na	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
jménu	jméno	k1gNnSc6
nových	nový	k2eAgInPc2d1
státoprávních	státoprávní	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
tato	tento	k3xDgNnPc1
zdlouhavá	zdlouhavý	k2eAgNnPc1d1
označení	označení	k1gNnPc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gyula	Gyula	k1gMnSc1
Andrássy	Andrássa	k1gFnSc2
starší	starší	k1gMnSc1
byl	být	k5eAaImAgMnS
po	po	k7c6
porážce	porážka	k1gFnSc6
Uherské	uherský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
odsouzen	odsouzen	k2eAgMnSc1d1
k	k	k7c3
smrti	smrt	k1gFnSc3
<g/>
,	,	kIx,
z	z	k7c2
emigrace	emigrace	k1gFnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
po	po	k7c6
amnestii	amnestie	k1gFnSc6
roku	rok	k1gInSc2
1858	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nenávist	nenávist	k1gFnSc4
mnohých	mnohý	k2eAgMnPc2d1
Čechů	Čech	k1gMnPc2
k	k	k7c3
Židům	Žid	k1gMnPc3
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
nebyla	být	k5eNaImAgFnS
jen	jen	k9
známá	známý	k2eAgFnSc1d1
Hilsneriáda	hilsneriáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobový	dobový	k2eAgInSc4d1
tisk	tisk	k1gInSc4
o	o	k7c6
nich	on	k3xPp3gMnPc6
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
otiskoval	otiskovat	k5eAaImAgInS
značně	značně	k6eAd1
nevybíravé	vybíravý	k2eNgFnPc4d1
narážky	narážka	k1gFnPc4
a	a	k8xC
např.	např.	kA
během	během	k7c2
převratu	převrat	k1gInSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1918	#num#	k4
nebyl	být	k5eNaImAgInS
daleko	daleko	k6eAd1
od	od	k7c2
zlynčování	zlynčování	k1gNnSc2
davem	dav	k1gInSc7
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
někdo	někdo	k3yInSc1
z	z	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
slavících	slavící	k2eAgMnPc2d1
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
ulicích	ulice	k1gFnPc6
<g/>
,	,	kIx,
označil	označit	k5eAaPmAgMnS
za	za	k7c4
Žida	Žid	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachránilo	zachránit	k5eAaPmAgNnS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
až	až	k9
když	když	k8xS
vyšlo	vyjít	k5eAaPmAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
písničkáře	písničkář	k1gMnSc4
Karla	Karel	k1gMnSc4
Hašlera	Hašler	k1gMnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vídeň	Vídeň	k1gFnSc1
mohutněla	mohutnět	k5eAaImAgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
i	i	k9
přílivem	příliv	k1gInSc7
přistěhovalců	přistěhovalec	k1gMnPc2
z	z	k7c2
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
počet	počet	k1gInSc1
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Vídeň	Vídeň	k1gFnSc1
byla	být	k5eAaImAgFnS
de	de	k?
facto	facta	k1gFnSc5
druhým	druhý	k4xOgNnSc7
největším	veliký	k2eAgNnSc7d3
českým	český	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Již	již	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
dělnického	dělnický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
na	na	k7c6
začátku	začátek	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
charakteristická	charakteristický	k2eAgFnSc1d1
vzájemná	vzájemný	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
a	a	k8xC
solidarita	solidarita	k1gFnSc1
mezi	mezi	k7c7
německými	německý	k2eAgMnPc7d1
a	a	k8xC
českými	český	k2eAgMnPc7d1
aktivisty	aktivista	k1gMnPc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vztahy	vztah	k1gInPc1
následníka	následník	k1gMnSc2
Františka	František	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
s	s	k7c7
vládnoucím	vládnoucí	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Františkem	František	k1gMnSc7
Josefem	Josef	k1gMnSc7
I.	I.	kA
měly	mít	k5eAaImAgInP
značně	značně	k6eAd1
výbušný	výbušný	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
můžeme	moct	k5eAaImIp1nP
<g />
.	.	kIx.
</s>
<s hack="1">
atentát	atentát	k1gInSc4
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
synovce	synovka	k1gFnSc6
přiřadit	přiřadit	k5eAaPmF
k	k	k7c3
dalším	další	k2eAgFnPc3d1
jeho	jeho	k3xOp3gFnPc3
osobním	osobní	k2eAgFnPc3d1
tragédiím	tragédie	k1gFnPc3
<g/>
:	:	kIx,
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
mexický	mexický	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
byl	být	k5eAaImAgMnS
roku	rok	k1gInSc2
1867	#num#	k4
po	po	k7c6
státním	státní	k2eAgInSc6d1
převratu	převrat	k1gInSc6
popraven	popravit	k5eAaPmNgInS
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
spáchal	spáchat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1889	#num#	k4
sebevraždu	sebevražda	k1gFnSc4
a	a	k8xC
manželka	manželka	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1898	#num#	k4
zabita	zabit	k2eAgFnSc1d1
při	při	k7c6
atentátu	atentát	k1gInSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zajištění	zajištění	k1gNnSc2
bezpečnosti	bezpečnost	k1gFnSc2
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1914	#num#	k4
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
zcela	zcela	k6eAd1
selhalo	selhat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Františka	František	k1gMnSc4
Ferdinanda	Ferdinand	k1gMnSc4
byl	být	k5eAaImAgMnS
během	během	k7c2
cesty	cesta	k1gFnSc2
městem	město	k1gNnSc7
spáchán	spáchat	k5eAaPmNgInS
bombový	bombový	k2eAgInSc1d1
útok	útok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ale	ale	k8xC
nikoho	nikdo	k3yNnSc4
nezabil	zabít	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potiorek	Potiorek	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
nijak	nijak	k6eAd1
nereagoval	reagovat	k5eNaBmAgInS
a	a	k8xC
dopustil	dopustit	k5eAaPmAgInS
tak	tak	k6eAd1
zastřelení	zastřelení	k1gNnSc4
manželského	manželský	k2eAgInSc2d1
páru	pár	k1gInSc2
při	při	k7c6
zpáteční	zpáteční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovaly	existovat	k5eAaImAgInP
proto	proto	k6eAd1
spekulace	spekulace	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
Potiorek	Potiorek	k1gInSc1
o	o	k7c6
chystaném	chystaný	k2eAgInSc6d1
atentátu	atentát	k1gInSc6
věděl	vědět	k5eAaImAgMnS
a	a	k8xC
úmyslně	úmyslně	k6eAd1
ho	on	k3xPp3gMnSc4
umožnil	umožnit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Friedrich	Friedrich	k1gMnSc1
Adler	Adler	k1gMnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
vyhnul	vyhnout	k5eAaPmAgInS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
Gavrilo	Gavrila	k1gFnSc5
Princip	princip	k1gInSc4
–	–	k?
sarajevský	sarajevský	k2eAgMnSc1d1
atentátník	atentátník	k1gMnSc1
z	z	k7c2
června	červen	k1gInSc2
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
mu	on	k3xPp3gMnSc3
udělil	udělit	k5eAaPmAgMnS
milost	milost	k1gFnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
dožil	dožít	k5eAaPmAgMnS
vysokého	vysoký	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princip	princip	k1gInSc1
nebyl	být	k5eNaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
plnoletý	plnoletý	k2eAgInSc1d1
<g/>
,	,	kIx,
nemohl	moct	k5eNaImAgInS
mu	on	k3xPp3gNnSc3
tedy	tedy	k9
být	být	k5eAaImF
udělen	udělen	k2eAgInSc4d1
nejvyšší	vysoký	k2eAgInSc4d3
trest	trest	k1gInSc4
a	a	k8xC
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
na	na	k7c4
tuberkulózu	tuberkulóza	k1gFnSc4
v	v	k7c6
terezínské	terezínský	k2eAgFnSc6d1
věznici	věznice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Volební	volební	k2eAgInSc1d1
řád	řád	k1gInSc1
českého	český	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
nevylučoval	vylučovat	k5eNaImAgMnS
kandidaturu	kandidatura	k1gFnSc4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
době	doba	k1gFnSc6
kdy	kdy	k6eAd1
vznikal	vznikat	k5eAaImAgInS
–	–	k?
roku	rok	k1gInSc2
1861	#num#	k4
<g/>
,	,	kIx,
něco	něco	k3yInSc1
takového	takový	k3xDgMnSc4
nikoho	nikdo	k3yNnSc4
nenapadlo	napadnout	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
pokus	pokus	k1gInSc1
o	o	k7c4
kandidaturu	kandidatura	k1gFnSc4
žen	žena	k1gFnPc2
byl	být	k5eAaImAgInS
učiněn	učiněn	k2eAgInSc1d1
roku	rok	k1gInSc2
1908	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
žádná	žádný	k3yNgFnSc1
žena	žena	k1gFnSc1
tehdy	tehdy	k6eAd1
zvolena	zvolit	k5eAaPmNgFnS
nebyla	být	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1912	#num#	k4
za	za	k7c4
obvod	obvod	k1gInSc4
Mladá	mladá	k1gFnSc1
Boleslav-Nymburk	Boleslav-Nymburk	k1gInSc1
kandidovalo	kandidovat	k5eAaImAgNnS
hned	hned	k6eAd1
několik	několik	k4yIc1
žen	žena	k1gFnPc2
a	a	k8xC
za	za	k7c4
poslankyni	poslankyně	k1gFnSc4
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
nakonec	nakonec	k6eAd1
byla	být	k5eAaImAgFnS
zvolena	zvolit	k5eAaPmNgFnS
spisovatelka	spisovatelka	k1gFnSc1
Božena	Božena	k1gFnSc1
Viková	Viková	k1gFnSc1
Kunětická	kunětický	k2eAgFnSc1d1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vůbec	vůbec	k9
první	první	k4xOgInPc1
poslankyní	poslankyně	k1gFnPc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KONTLER	KONTLER	kA
<g/>
,	,	kIx,
László	László	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
x.	x.	k?
Kapitola	kapitola	k1gFnSc1
Neoabsolutismus	Neoabsolutismus	k1gInSc1
a	a	k8xC
vyrovnání	vyrovnání	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
253	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SVATUŠKA	SVATUŠKA	kA
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
1790	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
488	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KONTLER	KONTLER	kA
<g/>
,	,	kIx,
László	László	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
x.	x.	k?
Kapitola	kapitola	k1gFnSc1
Šťastné	Šťastná	k1gFnSc2
mírové	mírový	k2eAgInPc4d1
časy	čas	k1gInPc4
aneb	aneb	k?
přelud	přelud	k1gInSc4
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
254	#num#	k4
<g/>
-	-	kIx~
<g/>
255	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ŠESTÁK	Šesták	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
jihoslovanských	jihoslovanský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
mezi	mezi	k7c7
Vídní	Vídeň	k1gFnSc7
a	a	k8xC
Peští	Pešť	k1gFnSc7
<g/>
,	,	kIx,
s.	s.	k?
253	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WEISSENSTEINER	WEISSENSTEINER	kA
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouští	rakouský	k2eAgMnPc1d1
císařové	císař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ikar	Ikar	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
249	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
464	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
s.	s.	k?
140	#num#	k4
<g/>
-	-	kIx~
<g/>
141	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOSMAN	kosman	k1gMnSc1
<g/>
,	,	kIx,
Marceli	Marcel	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
1842	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Hmotná	hmotný	k2eAgFnSc1d1
a	a	k8xC
duchovní	duchovní	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
236	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KONTLER	KONTLER	kA
<g/>
,	,	kIx,
László	László	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
x.	x.	k?
Kapitola	kapitola	k1gFnSc1
Šťastné	Šťastná	k1gFnSc2
mírové	mírový	k2eAgInPc4d1
časy	čas	k1gInPc4
<g/>
,	,	kIx,
aneb	aneb	k?
přelud	přelud	k1gInSc4
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
254	#num#	k4
<g/>
-	-	kIx~
<g/>
279	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCHREIER	SCHREIER	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběhy	příběh	k1gInPc7
z	z	k7c2
dějin	dějiny	k1gFnPc2
našich	náš	k3xOp1gFnPc2
drah	draha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1505	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Železnice	železnice	k1gFnSc1
soukromá	soukromý	k2eAgFnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
107	#num#	k4
-	-	kIx~
108	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MOTÝL	motýl	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
Chorvatů	Chorvat	k1gMnPc2
neumí	umět	k5eNaImIp3nS
číst	číst	k5eAaImF
a	a	k8xC
psát	psát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
267	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Proces	proces	k1gInSc4
modernizace	modernizace	k1gFnSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
proměny	proměna	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
140	#num#	k4
<g/>
-	-	kIx~
<g/>
145	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Julius	Julius	k1gMnSc1
Johannes	Johannes	k1gMnSc1
Ludovicus	Ludovicus	k1gMnSc1
von	von	k1gInSc4
Payer	Payer	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VOLEK	Volek	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gründerský	Gründerský	k2eAgInSc1d1
boom	boom	k1gInSc1
a	a	k8xC
Vídeňský	vídeňský	k2eAgInSc1d1
krach	krach	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1873	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
2217	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HLADKÝ	Hladký	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
jihoslovanských	jihoslovanský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Velká	velká	k1gFnSc1
východní	východní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
1875	#num#	k4
-	-	kIx~
1878	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BACHMAN	BACHMAN	kA
<g/>
,	,	kIx,
Kryštof	Kryštof	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
krize	krize	k1gFnSc1
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
-	-	kIx~
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FLIEGER	FLIEGER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
myšlenky	myšlenka	k1gFnSc2
austroslavismu	austroslavismus	k1gInSc2
k	k	k7c3
samostatnému	samostatný	k2eAgNnSc3d1
Československu	Československo	k1gNnSc3
-	-	kIx~
den	den	k1gInSc4
po	po	k7c6
dni	den	k1gInSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
díl	díl	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
4306	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Badeniho	Badeni	k1gMnSc2
jazyková	jazykový	k2eAgNnPc4d1
nařízení	nařízení	k1gNnPc4
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnSc1
Leccos	leccos	k3yInSc1
<g/>
↑	↑	k?
Gautschova	Gautschův	k2eAgNnPc4d1
jazyková	jazykový	k2eAgNnPc4d1
nařízení	nařízení	k1gNnPc4
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnSc1
Leccos	leccos	k3yInSc1
<g/>
↑	↑	k?
Kalmán	Kalmán	k2eAgInSc1d1
Tisza	Tisza	k1gFnSc1
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnPc1
Leccos	leccos	k3yInSc4
<g/>
↑	↑	k?
KOVAČIČ	KOVAČIČ	kA
<g/>
,	,	kIx,
Ján	Ján	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maďarizácia	Maďarizácium	k1gNnPc1
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
4306	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠESTÁK	Šesták	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
jihoslovanských	jihoslovanský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Režim	režim	k1gInSc1
bána	bán	k1gMnSc2
Khuena-Héderváryho	Khuena-Héderváry	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
319	#num#	k4
<g/>
-	-	kIx~
<g/>
322	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FRANKL	FRANKL	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
liberalismu	liberalismus	k1gInSc2
k	k	k7c3
antisemitismu	antisemitismus	k1gInSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LUKŠÍČEK	LUKŠÍČEK	kA
<g/>
,	,	kIx,
M.	M.	kA
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
;	;	kIx,
Politik	politik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FranzFerdinand	FranzFerdinand	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
WEISSENSTEINER	WEISSENSTEINER	kA
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouští	rakouský	k2eAgMnPc1d1
císařové	císař	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ikar	Ikar	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
249	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
464	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Karel	Karel	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
<g/>
-	-	kIx~
<g/>
194	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOSMAN	kosman	k1gMnSc1
<g/>
,	,	kIx,
Marceli	Marcel	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
1842	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
246	#num#	k4
<g/>
-	-	kIx~
<g/>
252	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BRÜGEL	BRÜGEL	kA
<g/>
,	,	kIx,
Johann	Johann	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	česat	k5eAaImIp1nS
a	a	k8xC
Němci	Němec	k1gMnSc3
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1440	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1914	#num#	k4
<g/>
:	:	kIx,
válka	válka	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
<g/>
,	,	kIx,
s.	s.	k?
64	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOSATÍK	KOSATÍK	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
snění	snění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Torst	Torst	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7215	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Sen	sena	k1gFnPc2
o	o	k7c6
vyhraných	vyhraný	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
<g/>
,	,	kIx,
s.	s.	k?
99	#num#	k4
<g/>
-	-	kIx~
<g/>
126	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŽALOUDEK	Žaloudek	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
HLADKÝ	Hladký	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
jihoslovanských	jihoslovanský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
266	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Srbsko	Srbsko	k1gNnSc4
v	v	k7c6
konfliktu	konflikt	k1gInSc6
s	s	k7c7
habsburskou	habsburský	k2eAgFnSc7d1
a	a	k8xC
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
s.	s.	k?
277	#num#	k4
<g/>
-	-	kIx~
<g/>
291	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Prasečí	prasečí	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnSc1
CoJeCo	CoJeCo	k1gMnSc1
<g/>
↑	↑	k?
OUBRECHT	OUBRECHT	kA
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celní	celní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
rakousko-uhersko	rakousko-uhersko	k6eAd1
-	-	kIx~
srbská	srbský	k2eAgFnSc1d1
1906-1911	1906-1911	k4
a	a	k8xC
povědomí	povědomí	k1gNnSc1
o	o	k7c6
ní	on	k3xPp3gFnSc6
v	v	k7c6
české	český	k2eAgFnSc6d1
veřejnosti	veřejnost	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
491	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Konzervativní	konzervativní	k2eAgFnSc1d1
éra	éra	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
425	#num#	k4
<g/>
-	-	kIx~
<g/>
427	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KONTLER	KONTLER	kA
<g/>
,	,	kIx,
László	László	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
x.	x.	k?
Kapitola	kapitola	k1gFnSc1
Pokrok	pokrok	k1gInSc1
<g/>
,	,	kIx,
blahobyt	blahobyt	k1gInSc1
a	a	k8xC
květy	květ	k1gInPc1
rozkladu	rozklad	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
279	#num#	k4
<g/>
-	-	kIx~
<g/>
295	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kalmán	Kalmán	k2eAgInSc4d1
Tisza	Tisza	k1gFnSc1
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnPc1
Leccos	leccos	k3yInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Dvojspolek	dvojspolek	k1gInSc1
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnPc1
leccos	leccos	k3yInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
LIDDELL	LIDDELL	kA
HART	HART	kA
<g/>
,	,	kIx,
Basil	Basil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
První	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
164	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Počátky	počátek	k1gInPc4
války	válka	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
-	-	kIx~
<g/>
38	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtení	čtení	k1gNnPc1
o	o	k7c6
Severní	severní	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
Ferdinandově	Ferdinandův	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nadas	Nadas	k1gMnSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7030	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
94	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Co	co	k9
dala	dát	k5eAaPmAgFnS
Severní	severní	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Moravě	Morava	k1gFnSc3
a	a	k8xC
Slezsku	Slezsko	k1gNnSc6
<g/>
,	,	kIx,
s.	s.	k?
207	#num#	k4
<g/>
-	-	kIx~
<g/>
209	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
491	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Proměna	proměna	k1gFnSc1
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
448	#num#	k4
<g/>
-	-	kIx~
<g/>
450	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
informace	informace	k1gFnSc1
o	o	k7c6
městě	město	k1gNnSc6
<g/>
1	#num#	k4
2	#num#	k4
Good	Gooda	k1gFnPc2
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economic	Economic	k1gMnSc1
Rise	Ris	k1gFnSc2
of	of	k?
the	the	k?
Habsburg	Habsburg	k1gInSc1
Empire	empir	k1gInSc5
<g/>
↑	↑	k?
-RED-	-RED-	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budapešť	Budapešť	k1gFnSc1
<g/>
:	:	kIx,
první	první	k4xOgNnSc1
metro	metro	k1gNnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
mycentrope	mycentrop	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Heil	Heil	k1gInSc1
und	und	k?
Sieg	Sieg	k1gInSc1
<g/>
...	...	k?
<g/>
aneb	aneb	k?
Srdečné	srdečný	k2eAgInPc4d1
pozdravy	pozdrav	k1gInPc4
do	do	k7c2
Duchcova	Duchcův	k2eAgNnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
města	město	k1gNnSc2
Duchcova	Duchcův	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
FIDLER	FIDLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
a	a	k8xC
česká	český	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
do	do	k7c2
léta	léto	k1gNnSc2
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valka	Valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
4306	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
historie	historie	k1gFnSc2
české	český	k2eAgFnSc2d1
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSSD	ČSSD	kA
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HOLÍKOVÁ	Holíková	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balkán	Balkán	k1gInSc4
a	a	k8xC
válka	válka	k1gFnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valka	Valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
4306	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KOSATÍK	KOSATÍK	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
snění	snění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Torst	Torst	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7215	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Sen	sena	k1gFnPc2
o	o	k7c6
tělesné	tělesný	k2eAgFnSc6d1
zdatnosti	zdatnost	k1gFnSc6
<g/>
,	,	kIx,
s.	s.	k?
67	#num#	k4
<g/>
-	-	kIx~
<g/>
98	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
,	,	kIx,
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
325	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Hnutí	hnutí	k1gNnSc2
Los	los	k1gInSc4
von	von	k1gInSc4
Rom	Rom	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
228	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Černovská	Černovský	k2eAgFnSc1d1
masakra	masakra	k1gFnSc1
<g/>
;	;	kIx,
cernova	cernův	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
.	.	kIx.
www.cernova.sk	www.cernova.sk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KOSATÍK	KOSATÍK	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
snění	snění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Torst	Torst	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7215	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Sen	sena	k1gFnPc2
o	o	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOSOS	losos	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
<g/>
;	;	kIx,
MAHEL	Mahel	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osudy	osud	k1gInPc4
salonního	salonní	k2eAgInSc2d1
vozu	vůz	k1gInSc2
Aza	Aza	k1gFnSc2
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
35	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
-	-	kIx~
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
7219	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://ceskapozice.lidovky.cz/rakousko-uhersko-vyhlasilo-srbsku-valku-opravnene-fha-/tema.aspx?c=A140626_154416_pozice-tema_lube	http://ceskapozice.lidovky.cz/rakousko-uhersko-vyhlasilo-srbsku-valku-opravnene-fha-/tema.aspx?c=A140626_154416_pozice-tema_lubat	k5eAaPmIp3nS
<g/>
↑	↑	k?
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wacht	Wacht	k1gMnSc1
am	am	k?
Rhein	Rhein	k1gMnSc1
a	a	k8xC
Kde	kde	k6eAd1
domov	domov	k1gInSc1
můj	můj	k3xOp1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgInPc1d1
nadšení	nadšení	k1gNnSc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
v	v	k7c6
létě	léto	k1gNnSc6
1914	#num#	k4
<g/>
..	..	k?
Historie	historie	k1gFnSc1
a	a	k8xC
vojenství	vojenství	k1gNnSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
45	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
22	#num#	k4
-	-	kIx~
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
2583	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FIDLER	FIDLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
domácí	domácí	k2eAgFnSc1d1
politika	politika	k1gFnSc1
za	za	k7c2
Velké	velký	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1803	#num#	k4
<g/>
-	-	kIx~
<g/>
4306	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LIDDELL	LIDDELL	kA
HART	HART	kA
<g/>
,	,	kIx,
Basil	Basil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
První	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
164	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
V	v	k7c6
zaklínění	zaklínění	k1gNnSc6
<g/>
,	,	kIx,
s.	s.	k?
62	#num#	k4
<g/>
-	-	kIx~
<g/>
86	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Češi	česat	k5eAaImIp1nS
na	na	k7c6
frontách	fronta	k1gFnPc6
1	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
;	;	kIx,
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cs	cs	k?
<g/>
;	;	kIx,
videoarchiv	videoarchivit	k5eAaPmRp2nS
ČT	ČT	kA
<g/>
↑	↑	k?
-LK-	-LK-	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Militaria	Militarium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
6034	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČAPKA	Čapka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
v	v	k7c6
datech	datum	k1gNnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libri	Libri	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kronika	kronika	k1gFnSc1
města	město	k1gNnSc2
Čelákovic	Čelákovice	k1gFnPc2
I.	I.	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903461	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
1	#num#	k4
2	#num#	k4
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
Habsburkové	Habsburk	k1gMnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85947	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Posledním	poslední	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
115	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SCHREIER	SCHREIER	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gInPc4
dráhy	dráha	k1gFnPc1
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
2312	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Zachovej	zachovat	k5eAaPmRp2nS
nám	my	k3xPp1nPc3
<g/>
,	,	kIx,
Hospodine	Hospodin	k1gMnSc5
<g/>
,	,	kIx,
s.	s.	k?
27	#num#	k4
-	-	kIx~
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČAPKA	Čapka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
v	v	k7c6
datech	datum	k1gNnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libri	Libri	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LIDDELL	LIDDELL	kA
HART	HART	kA
<g/>
,	,	kIx,
Basil	Basil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
První	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
164	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Zlom	zlom	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
367	#num#	k4
<g/>
-	-	kIx~
<g/>
388	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Generální	generální	k2eAgFnSc1d1
stávka	stávka	k1gFnSc1
14	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1918	#num#	k4
<g/>
;	;	kIx,
encyklopedie	encyklopedie	k1gFnSc1
CoJeCo	CoJeCo	k1gMnSc1
<g/>
↑	↑	k?
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
267	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Politický	politický	k2eAgInSc1d1
zápas	zápas	k1gInSc4
před	před	k7c7
první	první	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
s.	s.	k?
153	#num#	k4
<g/>
-	-	kIx~
<g/>
161	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KONTLER	KONTLER	kA
<g/>
,	,	kIx,
László	László	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
405	#num#	k4
<g/>
-	-	kIx~
<g/>
x.	x.	k?
Kapitola	kapitola	k1gFnSc1
Šťastné	Šťastná	k1gFnSc2
mírové	mírový	k2eAgInPc4d1
časy	čas	k1gInPc4
<g/>
,	,	kIx,
aneb	aneb	k?
přelud	přelud	k1gInSc4
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
260	#num#	k4
<g/>
-	-	kIx~
<g/>
261	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vojenská	vojenský	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
<g/>
;	;	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
Leccos	leccos	k3yInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Volby	volba	k1gFnSc2
a	a	k8xC
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
za	za	k7c2
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
<g/>
;	;	kIx,
přepis	přepis	k1gInSc1
rozhlasové	rozhlasový	k2eAgFnSc2d1
relace	relace	k1gFnSc2
ČRO	ČRO	kA
<g/>
↑	↑	k?
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
<g/>
,	,	kIx,
následník	následník	k1gMnSc1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
325	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
O	o	k7c4
reformu	reforma	k1gFnSc4
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
263	#num#	k4
<g/>
-	-	kIx~
<g/>
278	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LUKŠÍČEK	LUKŠÍČEK	kA
<g/>
,	,	kIx,
M.	M.	kA
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FranzFerdinand	FranzFerdinand	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
v	v	k7c6
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ČSÚ	ČSÚ	kA
<g/>
↑	↑	k?
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
<g/>
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
491	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Základní	základní	k2eAgInPc1d1
trendy	trend	k1gInPc1
hospodářského	hospodářský	k2eAgMnSc2d1
a	a	k8xC
sociálního	sociální	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
446	#num#	k4
<g/>
-	-	kIx~
<g/>
447	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sčítání	sčítání	k1gNnSc1
lidu	lid	k1gInSc2
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
stát	stát	k1gInSc1
má	mít	k5eAaImIp3nS
51	#num#	k4
314	#num#	k4
271	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
;	;	kIx,
Tyden	Tyden	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Pražské	pražský	k2eAgFnSc2d1
bouře	bouř	k1gFnSc2
roku	rok	k1gInSc2
1897	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
též	též	k9
SKŘIVAN	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Role	role	k1gFnSc1
of	of	k?
Export	export	k1gInSc1
in	in	k?
the	the	k?
Economy	Econom	k1gInPc1
of	of	k?
the	the	k?
Habsburg	Habsburg	k1gInSc1
Monarchy	monarcha	k1gMnSc2
before	befor	k1gInSc5
the	the	k?
First	First	k1gMnSc1
World	World	k1gMnSc1
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
SKŘIVAN	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
;	;	kIx,
SUPPAN	SUPPAN	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc1
International	International	k1gFnSc1
Relations	Relations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
<g/>
:	:	kIx,
Institute	institut	k1gInSc5
of	of	k?
World	Worldo	k1gNnPc2
History	Histor	k1gInPc7
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
575	#num#	k4
<g/>
-	-	kIx~
<g/>
580	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
PRAVEC	PRAVEC	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
budoucnost	budoucnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přežívalo	přežívat	k5eAaImAgNnS
z	z	k7c2
českých	český	k2eAgFnPc2d1
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
upozorňuje	upozorňovat	k5eAaImIp3nS
historik	historik	k1gMnSc1
Prokš	Prokš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonom	ekonom	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-15	2015-10-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
v	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
proměny	proměna	k1gFnSc2
společnosti	společnost	k1gFnSc2
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7286	#num#	k4
<g/>
-	-	kIx~
<g/>
219	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ekonomika	ekonomika	k1gFnSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
389	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schwarzenberg	Schwarzenberg	k1gInSc1
<g/>
,	,	kIx,
Znaky	znak	k1gInPc1
rakouské	rakouský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
in	in	k?
Torzo	torzo	k1gNnSc1
díla	dílo	k1gNnSc2
str	str	kA
<g/>
.	.	kIx.
836	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Torst	Torst	k1gFnSc1
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7215-316-9	978-80-7215-316-9	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
EFMERTOVÁ	EFMERTOVÁ	kA
<g/>
,	,	kIx,
Marcela	Marcel	k1gMnSc2
C.	C.	kA
<g/>
;	;	kIx,
SAVICKÝ	SAVICKÝ	kA
<g/>
,	,	kIx,
Nikolaj	Nikolaj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
v	v	k7c6
letech	léto	k1gNnPc6
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
březnové	březnový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
do	do	k7c2
požáru	požár	k1gInSc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
456	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
171	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GALANDAUER	GALANDAUER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
I.	I.	kA
Poslední	poslední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HANISCH	HANISCH	kA
<g/>
,	,	kIx,
Ernst	Ernst	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
lange	lang	k1gFnSc2
Schatten	Schatten	k2eAgInSc1d1
des	des	k1gNnPc6
Staates	Staates	k1gInSc4
:	:	kIx,
Österreichische	Österreichischus	k1gMnSc5
Gesellschaftsgeschichte	Gesellschaftsgeschicht	k1gMnSc5
im	im	k?
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jahrhundert	Jahrhundert	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Österreichische	Österreichisch	k1gInSc2
Geschichte	Geschicht	k1gInSc5
1890	#num#	k4
<g/>
-	-	kIx~
<g/>
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gMnSc1
<g/>
:	:	kIx,
Ueberreuter	Ueberreuter	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
599	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
80003980	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
v	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
proměny	proměna	k1gFnSc2
společnosti	společnost	k1gFnSc2
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
478	#num#	k4
a	a	k8xC
476	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7286	#num#	k4
<g/>
-	-	kIx~
<g/>
219	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KONTLER	KONTLER	kA
<g/>
,	,	kIx,
László	László	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
613	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
616	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOŘALKA	kořalka	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	česat	k5eAaImIp1nS
v	v	k7c6
habsburské	habsburský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
a	a	k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
1815-1914	1815-1914	k4
:	:	kIx,
sociálněhistorické	sociálněhistorický	k2eAgFnSc2d1
souvislosti	souvislost	k1gFnSc2
vytváření	vytváření	k1gNnSc2
novodobého	novodobý	k2eAgInSc2d1
národa	národ	k1gInSc2
a	a	k8xC
národnostní	národnostní	k2eAgFnSc2d1
otázky	otázka	k1gFnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
354	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOVÁČ	Kováč	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejina	k1gFnSc2
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
opr	opr	k?
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
432	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KŘEN	křen	k1gInSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
století	století	k1gNnPc2
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
1109	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
612	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
katolicismus	katolicismus	k1gInSc1
1890	#num#	k4
<g/>
-	-	kIx~
<g/>
1914	#num#	k4
<g/>
:	:	kIx,
Kapitoly	kapitola	k1gFnPc4
z	z	k7c2
dějin	dějiny	k1gFnPc2
českého	český	k2eAgInSc2d1
katolického	katolický	k2eAgInSc2d1
tábora	tábor	k1gInSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Rosice	Rosice	k1gFnPc1
<g/>
:	:	kIx,
Gloria	gloria	k1gNnSc1
Rosice	Rosice	k1gFnPc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
649	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86200	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
:	:	kIx,
nikdy	nikdy	k6eAd1
nekorunovaný	korunovaný	k2eNgMnSc1d1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
495	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
217	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
habsburským	habsburský	k2eAgMnSc7d1
orlem	orel	k1gMnSc7
:	:	kIx,
České	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
205	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
290	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
Habsburkové	Habsburk	k1gMnPc1
:	:	kIx,
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
Zita	Zita	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
a	a	k8xC
snahy	snaha	k1gFnPc1
o	o	k7c4
záchranu	záchrana	k1gFnSc4
císařského	císařský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gInSc1
&	&	k?
Principal	Principal	k1gFnSc1
;	;	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
286	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85947	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PRAŽÁK	Pražák	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Uher	Uhry	k1gFnPc2
a	a	k8xC
Maďarska	Maďarsko	k1gNnSc2
v	v	k7c6
datech	datum	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
536	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
391	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PRECLÍK	preclík	k1gInSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
legie	legie	k1gFnSc1
<g/>
,	,	kIx,
váz	váza	k1gFnPc2
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
219	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Paris	Paris	k1gMnSc1
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Žižkova	Žižkův	k2eAgFnSc1d1
2379	#num#	k4
(	(	kIx(
<g/>
734	#num#	k4
01	#num#	k4
Karviná	Karviná	k1gFnSc1
<g/>
)	)	kIx)
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Masarykovým	Masarykův	k2eAgNnSc7d1
demokratickým	demokratický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87173	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
5	#num#	k4
-	-	kIx~
15	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
-	-	kIx~
25	#num#	k4
<g/>
,	,	kIx,
33	#num#	k4
-	-	kIx~
45	#num#	k4
<g/>
,	,	kIx,
70	#num#	k4
–	–	k?
76	#num#	k4
</s>
<s>
RUMPLER	RUMPLER	kA
<g/>
,	,	kIx,
Helmut	Helmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eine	Eine	k1gNnSc1
Chance	Chanec	k1gInSc2
für	für	k?
Mitteleuropa	Mitteleurop	k1gMnSc2
:	:	kIx,
Bürgerliche	Bürgerlich	k1gMnSc2
Emanzipation	Emanzipation	k1gInSc1
und	und	k?
Staatsverfall	Staatsverfall	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
Habsburgermonarchie	Habsburgermonarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Österreichische	Österreichisch	k1gInSc2
Geschichte	Geschicht	k1gInSc5
1804	#num#	k4
<g/>
-	-	kIx~
<g/>
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gMnSc1
<g/>
:	:	kIx,
Ueberreuter	Ueberreuter	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
672	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
80003979	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SKED	SKED	kA
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úpadek	úpadek	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
habsburské	habsburský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panevropa	Panevropa	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
372	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85846	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SKŘIVAN	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Untergang	Untergang	k1gInSc4
des	des	k1gNnSc7
alten	altna	k1gFnPc2
Großmächtekonzerts	Großmächtekonzerts	k1gInSc4
–	–	k?
zur	zur	k?
Außenpolitik	Außenpolitikum	k1gNnPc2
Deutschlands	Deutschlands	k1gInSc1
und	und	k?
Österreich-Ungarns	Österreich-Ungarns	k1gInSc1
am	am	k?
Vorabend	Vorabend	k1gInSc1
des	des	k1gNnSc2
Ersten	Ersten	k2eAgInSc1d1
Weltkrieges	Weltkrieges	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
75	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
John	John	k1gMnSc1
Percivale	Percivala	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
století	století	k1gNnSc1
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
:	:	kIx,
Rakousko	Rakousko	k1gNnSc1
a	a	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1809	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
371	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85947	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
690	#num#	k4
s.	s.	k?
</s>
<s>
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
297	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
203	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VEBER	VEBER	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
doplněné	doplněný	k2eAgInPc4d1
a	a	k8xC
aktualizované	aktualizovaný	k2eAgInPc4d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Česka	Česko	k1gNnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Rakouska	Rakousko	k1gNnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Rakousko-uherské	rakousko-uherský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
ministerských	ministerský	k2eAgMnPc2d1
předsedů	předseda	k1gMnPc2
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
</s>
<s>
Zánik	zánik	k1gInSc1
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnPc1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Správní	správní	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
Předlitavska	Předlitavsko	k1gNnSc2
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
právní	právní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
Rakouská	rakouský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
apod.	apod.	kA
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
a	a	k8xC
poštovní	poštovní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Rakousko	Rakousko	k1gNnSc1
–	–	k?
Uherska	Uhersko	k1gNnSc2
1894	#num#	k4
</s>
<s>
Atlas	Atlas	k1gInSc1
statistický	statistický	k2eAgInSc1d1
rakousko-uherské	rakousko-uherský	k2eAgFnPc4d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Korunní	korunní	k2eAgFnSc1d1
země	země	k1gFnSc1
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1
Předlitavsko	Předlitavsko	k1gNnSc4
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc2
Předlitavsko	Předlitavsko	k1gNnSc4
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2
<g/>
(	(	kIx(
<g/>
země	zem	k1gFnPc1
zastoupené	zastoupený	k2eAgFnPc1d1
v	v	k7c6
říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Dalmatské	dalmatský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Bukovinské	bukovinský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Korutanské	korutanský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Kraňské	kraňský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Haličsko-vladiměřské	Haličsko-vladiměřský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Moravské	moravský	k2eAgNnSc1d1
markrabství	markrabství	k1gNnSc1
<g/>
,	,	kIx,
Rakouské	rakouský	k2eAgNnSc1d1
arcivévodství	arcivévodství	k1gNnSc1
(	(	kIx(
<g/>
Rakousy	Rakousy	k1gInPc1
pod	pod	k7c4
Enží	Enží	k1gFnPc4
<g/>
,	,	kIx,
Rakousy	Rakousy	k1gInPc4
nad	nad	k7c4
Enží	Enží	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vévodství	vévodství	k1gNnSc1
Horní	horní	k2eAgNnSc1d1
a	a	k8xC
Dolní	dolní	k2eAgNnSc1d1
Slezsko	Slezsko	k1gNnSc1
<g/>
,	,	kIx,
Salcburské	salcburský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Štýrské	štýrský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Tyrolské	tyrolský	k2eAgNnSc1d1
okněžněné	okněžněný	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
a	a	k8xC
Vorarlbersko	Vorarlbersko	k1gNnSc1
<g/>
,	,	kIx,
Rakouské	rakouský	k2eAgNnSc1d1
přímoří	přímoří	k1gNnSc1
(	(	kIx(
<g/>
okněžněné	okněžněný	k2eAgNnSc1d1
hrabství	hrabství	k1gNnSc1
Gorice	Gorice	k1gFnSc2
a	a	k8xC
Gradiška	Gradišek	k1gInSc2
<g/>
,	,	kIx,
Markrabství	markrabství	k1gNnSc2
Istrie	Istrie	k1gFnSc2
<g/>
,	,	kIx,
Terst	Terst	k1gInSc1
<g/>
)	)	kIx)
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Zalitavsko	Zalitavsko	k1gNnSc1
<g/>
(	(	kIx(
<g/>
země	země	k1gFnSc1
svatošpěpánské	svatošpěpánský	k2eAgFnSc2d1
uherské	uherský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
apoštolské	apoštolský	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
Sedmihradské	sedmihradský	k2eAgNnSc1d1
velkoknížectví	velkoknížectví	k1gNnSc1
<g/>
1	#num#	k4
a	a	k8xC
Srbské	srbský	k2eAgNnSc4d1
vojvodsví	vojvodsví	k1gNnSc4
a	a	k8xC
Banát	Banát	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chorvatsko-slavonské	chorvatsko-slavonský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
město	město	k1gNnSc1
Rijeka	Rijeka	k1gFnSc1
(	(	kIx(
<g/>
corpus	corpus	k1gInSc1
separatum	separatum	k1gNnSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Kondominium	kondominium	k1gNnSc4
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
3	#num#	k4
1	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
,	,	kIx,
2	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1868	#num#	k4
,	,	kIx,
3	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1
a	a	k8xC
politický	politický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
a	a	k8xC
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
Ústavní	ústavní	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
</s>
<s>
dubnová	dubnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
kroměřížská	kroměřížský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
březnová	březnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
silvestrovské	silvestrovský	k2eAgInPc1d1
patenty	patent	k1gInPc1
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
)	)	kIx)
•	•	k?
říjnový	říjnový	k2eAgInSc4d1
diplom	diplom	k1gInSc4
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
)	)	kIx)
•	•	k?
únorová	únorový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
)	)	kIx)
•	•	k?
zářijový	zářijový	k2eAgInSc4d1
manifest	manifest	k1gInSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1865	#num#	k4
<g/>
)	)	kIx)
•	•	k?
rakousko-uherské	rakousko-uherský	k2eAgNnSc4d1
vyrovnání	vyrovnání	k1gNnSc4
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
prosincová	prosincový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
chorvatsko-uherské	chorvatsko-uherský	k2eAgNnSc4d1
vyrovnání	vyrovnání	k1gNnSc4
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
•	•	k?
fundamentální	fundamentální	k2eAgInPc4d1
články	článek	k1gInPc4
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
dubnová	dubnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Taaffeho	Taaffe	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Badeniho	Badeni	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
•	•	k?
všeobecné	všeobecný	k2eAgNnSc4d1
a	a	k8xC
rovné	rovný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
Legislativní	legislativní	k2eAgNnPc1d1
a	a	k8xC
exekutivní	exekutivní	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Frankfurtský	frankfurtský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Uherský	uherský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
zemské	zemský	k2eAgInPc1d1
sněmy	sněm	k1gInPc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
delegace	delegace	k1gFnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
vláda	vláda	k1gFnSc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
vláda	vláda	k1gFnSc1
Předlitavska	Předlitavsko	k1gNnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
vláda	vláda	k1gFnSc1
Uherska	Uhersko	k1gNnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
společná	společný	k2eAgFnSc1d1
ministerstva	ministerstvo	k1gNnSc2
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
zemské	zemský	k2eAgInPc1d1
výbory	výbor	k1gInPc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
130613	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4075613-0	4075613-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1845	#num#	k4
5087	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50057161	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
2413157416853816710008	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50057161	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
