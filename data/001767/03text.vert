<s>
Timothy	Timotha	k1gFnPc1	Timotha
William	William	k1gInSc1	William
Burton	Burton	k1gInSc4	Burton
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Burbank	Burbank	k1gInSc1	Burbank
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
výtvarník	výtvarník	k1gMnSc1	výtvarník
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
nekonvenčním	konvenční	k2eNgInSc7d1	nekonvenční
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
povědomí	povědomí	k1gNnSc2	povědomí
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
zejména	zejména	k9	zejména
po	po	k7c4	po
natočení	natočení	k1gNnSc4	natočení
kasovního	kasovní	k2eAgInSc2d1	kasovní
trháku	trhák	k1gInSc2	trhák
Batman	Batman	k1gMnSc1	Batman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
později	pozdě	k6eAd2	pozdě
natočil	natočit	k5eAaBmAgMnS	natočit
i	i	k9	i
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
filmy	film	k1gInPc4	film
patřil	patřit	k5eAaImAgMnS	patřit
Střihoruký	Střihoruký	k2eAgMnSc1d1	Střihoruký
Edward	Edward	k1gMnSc1	Edward
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
točil	točit	k5eAaImAgInS	točit
poprvé	poprvé	k6eAd1	poprvé
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
hercem	herec	k1gMnSc7	herec
Johnny	Johnna	k1gFnSc2	Johnna
Deppem	Depp	k1gInSc7	Depp
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jeho	jeho	k3xOp3gInPc6	jeho
úspěšných	úspěšný	k2eAgInPc6d1	úspěšný
filmech	film	k1gInPc6	film
jako	jako	k8xS	jako
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
Ospalá	ospalý	k2eAgFnSc1d1	ospalá
díra	díra	k1gFnSc1	díra
<g/>
,	,	kIx,	,
či	či	k8xC	či
Karlík	Karlík	k1gMnSc1	Karlík
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Depp	Depp	k1gMnSc1	Depp
taky	taky	k6eAd1	taky
namluvil	namluvit	k5eAaPmAgMnS	namluvit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
jeho	jeho	k3xOp3gFnSc2	jeho
animované	animovaný	k2eAgFnSc2d1	animovaná
Mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Johnnyho	Johnny	k1gMnSc2	Johnny
Deppa	Depp	k1gMnSc2	Depp
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
častým	častý	k2eAgMnPc3d1	častý
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
i	i	k8xC	i
skladatel	skladatel	k1gMnSc1	skladatel
Danny	Danna	k1gFnSc2	Danna
Elfman	Elfman	k1gMnSc1	Elfman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
také	také	k9	také
kniha	kniha	k1gFnSc1	kniha
Burtonových	Burtonový	k2eAgFnPc2d1	Burtonová
básní	báseň	k1gFnPc2	báseň
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Trudný	trudný	k2eAgInSc1d1	trudný
konec	konec	k1gInSc1	konec
Ústřičného	ústřičný	k2eAgMnSc2d1	ústřičný
chlapečka	chlapeček	k1gMnSc2	chlapeček
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c4	v
Burbanku	Burbanka	k1gFnSc4	Burbanka
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ateliéry	ateliér	k1gInPc1	ateliér
Disneyovy	Disneyův	k2eAgFnSc2d1	Disneyova
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
a	a	k8xC	a
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c4	na
California	Californium	k1gNnPc4	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
tamějším	tamější	k2eAgInSc6d1	tamější
programu	program	k1gInSc6	program
trikových	trikový	k2eAgInPc2d1	trikový
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gNnSc4	on
brzy	brzy	k6eAd1	brzy
přivedlo	přivést	k5eAaPmAgNnS	přivést
do	do	k7c2	do
Disneyova	Disneyův	k2eAgInSc2d1	Disneyův
ateliéru	ateliér	k1gInSc2	ateliér
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nabíral	nabírat	k5eAaImAgInS	nabírat
nové	nový	k2eAgMnPc4d1	nový
pracovníky	pracovník	k1gMnPc4	pracovník
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
ročníku	ročník	k1gInSc2	ročník
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
tam	tam	k6eAd1	tam
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
Disneyův	Disneyův	k2eAgMnSc1d1	Disneyův
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
na	na	k7c6	na
filmu	film	k1gInSc6	film
Liška	liška	k1gFnSc1	liška
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
musel	muset	k5eAaImAgInS	muset
kreslit	kreslit	k5eAaImF	kreslit
především	především	k9	především
scény	scéna	k1gFnPc4	scéna
s	s	k7c7	s
malým	malý	k2eAgMnSc7d1	malý
lišákem	lišák	k1gMnSc7	lišák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stal	stát	k5eAaPmAgInS	stát
jeho	jeho	k3xOp3gInSc7	jeho
osobním	osobní	k2eAgInSc7d1	osobní
předmětem	předmět	k1gInSc7	předmět
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Pocta	pocta	k1gFnSc1	pocta
Vincentu	Vincent	k1gMnSc3	Vincent
Priceovi	Priceus	k1gMnSc3	Priceus
Vincent	Vincent	k1gMnSc1	Vincent
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
kreslený	kreslený	k2eAgInSc1d1	kreslený
a	a	k8xC	a
trikový	trikový	k2eAgInSc1d1	trikový
loutkový	loutkový	k2eAgInSc1d1	loutkový
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vypravěčem	vypravěč	k1gMnSc7	vypravěč
mimo	mimo	k7c4	mimo
obraz	obraz	k1gInSc4	obraz
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgMnSc1	tento
elegantní	elegantní	k2eAgMnSc1d1	elegantní
hororový	hororový	k2eAgMnSc1d1	hororový
představitel	představitel	k1gMnSc1	představitel
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
další	další	k2eAgInPc1d1	další
na	na	k7c4	na
černobílý	černobílý	k2eAgInSc4d1	černobílý
materiál	materiál	k1gInSc4	materiál
natočený	natočený	k2eAgInSc4d1	natočený
film	film	k1gInSc4	film
Frankenweenie	Frankenweenie	k1gFnSc2	Frankenweenie
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc1	jeho
vlastní	vlastní	k2eAgInPc1d1	vlastní
první	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
natočené	natočený	k2eAgInPc1d1	natočený
pro	pro	k7c4	pro
studio	studio	k1gNnSc4	studio
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
obdivovány	obdivován	k2eAgInPc1d1	obdivován
<g/>
,	,	kIx,	,
než	než	k8xS	než
uznávány	uznáván	k2eAgFnPc1d1	uznávána
a	a	k8xC	a
bez	bez	k7c2	bez
okolků	okolek	k1gInPc2	okolek
zmizely	zmizet	k5eAaPmAgInP	zmizet
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Frankenweenie	Frankenweenie	k1gFnSc1	Frankenweenie
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svého	svůj	k3xOyFgMnSc4	svůj
přejetého	přejetý	k2eAgMnSc4d1	přejetý
psa	pes	k1gMnSc4	pes
oživí	oživit	k5eAaPmIp3nS	oživit
ránou	rána	k1gFnSc7	rána
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Laskavou	laskavý	k2eAgFnSc7d1	laskavá
parafrází	parafráze	k1gFnSc7	parafráze
na	na	k7c4	na
film	film	k1gInSc4	film
Frankensteinova	Frankensteinův	k2eAgFnSc1d1	Frankensteinova
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
produkoval	produkovat	k5eAaImAgMnS	produkovat
Burton	Burton	k1gInSc4	Burton
pro	pro	k7c4	pro
Disney	Disne	k1gMnPc4	Disne
dětský	dětský	k2eAgInSc4d1	dětský
televizní	televizní	k2eAgInSc4d1	televizní
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pedagogicky	pedagogicky	k6eAd1	pedagogicky
povážlivý	povážlivý	k2eAgMnSc1d1	povážlivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
ponurý	ponurý	k2eAgMnSc1d1	ponurý
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
tehdejšími	tehdejší	k2eAgMnPc7d1	tehdejší
odpovědnými	odpovědný	k2eAgMnPc7d1	odpovědný
činiteli	činitel	k1gMnPc7	činitel
tento	tento	k3xDgInSc4	tento
půlhodinový	půlhodinový	k2eAgInSc4d1	půlhodinový
film	film	k1gInSc4	film
prominentně	prominentně	k6eAd1	prominentně
obsazený	obsazený	k2eAgInSc4d1	obsazený
Danielem	Daniel	k1gMnSc7	Daniel
Sternem	sternum	k1gNnSc7	sternum
a	a	k8xC	a
Shelley	Shellea	k1gFnPc1	Shellea
Duvalovou	Duvalová	k1gFnSc4	Duvalová
<g/>
.	.	kIx.	.
</s>
<s>
Pee-Weeho	Pee-Weeze	k6eAd1	Pee-Weeze
velké	velký	k2eAgNnSc4d1	velké
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vydán	vydat	k5eAaPmNgInS	vydat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
videu	video	k1gNnSc6	video
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
stal	stát	k5eAaPmAgInS	stát
překvapivým	překvapivý	k2eAgInSc7d1	překvapivý
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
následujících	následující	k2eAgFnPc2d1	následující
komedií	komedie	k1gFnPc2	komedie
Beetlejuice	Beetlejuice	k1gFnSc2	Beetlejuice
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k6eAd1	především
filmu	film	k1gInSc2	film
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hollywoodská	hollywoodský	k2eAgNnPc1d1	hollywoodské
studia	studio	k1gNnPc1	studio
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
volný	volný	k2eAgInSc4d1	volný
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc4d1	umožňující
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
letech	léto	k1gNnPc6	léto
realizaci	realizace	k1gFnSc3	realizace
filmů	film	k1gInPc2	film
jako	jako	k8xC	jako
Střihoruký	Střihoruký	k2eAgMnSc1d1	Střihoruký
Edward	Edward	k1gMnSc1	Edward
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Planeta	planeta	k1gFnSc1	planeta
opic	opice	k1gFnPc2	opice
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Danny	Danna	k1gMnSc2	Danna
Elfman	Elfman	k1gMnSc1	Elfman
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Richarda	Richard	k1gMnSc4	Richard
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
stejném	stejné	k1gNnSc6	stejné
uskupení	uskupení	k1gNnSc2	uskupení
<g/>
,	,	kIx,	,
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
první	první	k4xOgInSc1	první
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
Zone	Zone	k1gFnPc2	Zone
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
propadl	propadnout	k5eAaPmAgMnS	propadnout
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
podobnosti	podobnost	k1gFnSc3	podobnost
s	s	k7c7	s
The	The	k1gFnPc7	The
Rocky	rock	k1gInPc4	rock
Horror	horror	k1gInSc1	horror
Pictureshow	Pictureshow	k1gMnPc2	Pictureshow
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Líbila	líbit	k5eAaImAgFnS	líbit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
nevedlo	vést	k5eNaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Danny	Danna	k1gFnPc1	Danna
Elfman	Elfman	k1gMnSc1	Elfman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
ve	v	k7c6	v
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
Zone	Zon	k1gInSc2	Zon
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
Satana	Satan	k1gMnSc2	Satan
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
komponování	komponování	k1gNnSc6	komponování
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
letech	léto	k1gNnPc6	léto
pracoval	pracovat	k5eAaImAgMnS	pracovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Oingo-Boingo	Oingo-Boingo	k1gMnSc1	Oingo-Boingo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
legendou	legenda	k1gFnSc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Oingo-Boingo	Oingo-Boingo	k6eAd1	Oingo-Boingo
byl	být	k5eAaImAgMnS	být
upozorněn	upozornit	k5eAaPmNgMnS	upozornit
na	na	k7c4	na
Elfmana	Elfman	k1gMnSc4	Elfman
a	a	k8xC	a
angažoval	angažovat	k5eAaBmAgMnS	angažovat
ho	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
filmovou	filmový	k2eAgFnSc4d1	filmová
hudbu	hudba	k1gFnSc4	hudba
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
prvnímu	první	k4xOgInSc3	první
hranému	hraný	k2eAgInSc3d1	hraný
celovečernímu	celovečerní	k2eAgInSc3d1	celovečerní
filmu	film	k1gInSc3	film
Pee-Weeho	Pee-Wee	k1gMnSc2	Pee-Wee
velké	velký	k2eAgNnSc4d1	velké
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
Elfmanovy	Elfmanův	k2eAgFnSc2d1	Elfmanova
skladby	skladba	k1gFnSc2	skladba
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Elfman	Elfman	k1gMnSc1	Elfman
skládal	skládat	k5eAaImAgMnS	skládat
nejen	nejen	k6eAd1	nejen
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmům	film	k1gInPc3	film
Dobrý	dobrý	k2eAgInSc1d1	dobrý
Will	Will	k1gInSc1	Will
Hunting	Hunting	k1gInSc1	Hunting
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
filmové	filmový	k2eAgFnPc4d1	filmová
série	série	k1gFnPc4	série
jako	jako	k8xS	jako
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Terminátor	terminátor	k1gInSc1	terminátor
3	[number]	k4	3
<g/>
:	:	kIx,	:
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
The	The	k1gFnPc2	The
Island	Island	k1gInSc1	Island
of	of	k?	of
Doctor	Doctor	k1gInSc4	Doctor
Agor	agora	k1gFnPc2	agora
1979	[number]	k4	1979
Stalk	Stalka	k1gFnPc2	Stalka
of	of	k?	of
the	the	k?	the
Celery	celer	k1gInPc7	celer
1979	[number]	k4	1979
Doctor	Doctor	k1gInSc1	Doctor
of	of	k?	of
Doom	Doom	k1gInSc4	Doom
1982	[number]	k4	1982
Vincent	Vincent	k1gMnSc1	Vincent
1982	[number]	k4	1982
Hansel	Hansel	k1gInSc1	Hansel
and	and	k?	and
Gretel	Gretel	k1gInSc1	Gretel
1982	[number]	k4	1982
Luau	Luaus	k1gInSc2	Luaus
1982	[number]	k4	1982
Aladdin	Aladdin	k2eAgMnSc1d1	Aladdin
and	and	k?	and
His	his	k1gNnSc2	his
Wonderful	Wonderfula	k1gFnPc2	Wonderfula
Lamp	lampa	k1gFnPc2	lampa
1984	[number]	k4	1984
Frankenweenie	Frankenweenie	k1gFnSc2	Frankenweenie
1985	[number]	k4	1985
Pee-Weeho	Pee-Wee	k1gMnSc2	Pee-Wee
velké	velký	k2eAgNnSc4d1	velké
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
1985	[number]	k4	1985
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
uvádí	uvádět	k5eAaImIp3nS	uvádět
1988	[number]	k4	1988
Beetlejuice	Beetlejuice	k1gFnSc2	Beetlejuice
1989	[number]	k4	1989
Batman	Batman	k1gMnSc1	Batman
1990	[number]	k4	1990
Střihoruký	Střihoruký	k2eAgMnSc1d1	Střihoruký
Edward	Edward	k1gMnSc1	Edward
1992	[number]	k4	1992
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
1993	[number]	k4	1993
Ukradené	ukradený	k2eAgFnPc1d1	ukradená
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Henry	Henry	k1gMnSc1	Henry
Selick	Selick	k1gMnSc1	Selick
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
1996	[number]	k4	1996
Mars	Mars	k1gInSc1	Mars
útočí	útočit	k5eAaImIp3nP	útočit
<g/>
!	!	kIx.	!
</s>
<s>
1999	[number]	k4	1999
Ospalá	ospalý	k2eAgFnSc1d1	ospalá
díra	díra	k1gFnSc1	díra
2000	[number]	k4	2000
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
of	of	k?	of
Stainboy	Stainboa	k1gFnSc2	Stainboa
2001	[number]	k4	2001
Planeta	planeta	k1gFnSc1	planeta
opic	opice	k1gFnPc2	opice
2003	[number]	k4	2003
Velká	velký	k2eAgFnSc1d1	velká
ryba	ryba	k1gFnSc1	ryba
2005	[number]	k4	2005
Mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
nevěsta	nevěsta	k1gFnSc1	nevěsta
Tima	Tima	k1gFnSc1	Tima
Burtona	Burtona	k1gFnSc1	Burtona
2005	[number]	k4	2005
Karlík	Karlík	k1gMnSc1	Karlík
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
2007	[number]	k4	2007
Sweeney	Sweenea	k1gFnSc2	Sweenea
Todd	Todda	k1gFnPc2	Todda
<g/>
:	:	kIx,	:
Ďábelský	ďábelský	k2eAgMnSc1d1	ďábelský
holič	holič	k1gMnSc1	holič
z	z	k7c2	z
Fleet	Fleet	k1gMnSc1	Fleet
Street	Street	k1gInSc4	Street
2010	[number]	k4	2010
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
2011	[number]	k4	2011
Frankenweenie	Frankenweenie	k1gFnSc2	Frankenweenie
<g/>
:	:	kIx,	:
Domácí	domácí	k2eAgMnSc1d1	domácí
mazlíček	mazlíček	k1gMnSc1	mazlíček
2011	[number]	k4	2011
Temné	temný	k2eAgInPc1d1	temný
stíny	stín	k1gInPc1	stín
2014	[number]	k4	2014
<g />
.	.	kIx.	.
</s>
<s>
Big	Big	k?	Big
Eyes	Eyes	k1gInSc1	Eyes
2015	[number]	k4	2015
Pinocchio	Pinocchio	k6eAd1	Pinocchio
2016	[number]	k4	2016
Miss	miss	k1gFnPc2	miss
Peregrine	Peregrin	k1gInSc5	Peregrin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Home	Home	k1gFnSc7	Home
for	forum	k1gNnPc2	forum
Peculiar	Peculiara	k1gFnPc2	Peculiara
Children	Childrno	k1gNnPc2	Childrno
2016	[number]	k4	2016
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
:	:	kIx,	:
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
Helmut	Helmut	k1gMnSc1	Helmut
Merchmann	Merchmann	k1gMnSc1	Merchmann
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
The	The	k1gFnSc1	The
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
on-line	onin	k1gInSc5	on-lin
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
