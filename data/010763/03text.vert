<p>
<s>
Elbait	Elbait	k1gInSc1	Elbait
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
turmalínů	turmalín	k1gInPc2	turmalín
a	a	k8xC	a
též	též	k9	též
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
barevný	barevný	k2eAgInSc1d1	barevný
turmalín	turmalín	k1gInSc1	turmalín
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
podle	podle	k7c2	podle
ostrova	ostrov	k1gInSc2	ostrov
Elba	Elb	k1gInSc2	Elb
u	u	k7c2	u
italských	italský	k2eAgInPc2d1	italský
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
puklinách	puklina	k1gFnPc6	puklina
žuly	žula	k1gFnSc2	žula
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
mnoho	mnoho	k4c1	mnoho
nádherných	nádherný	k2eAgInPc2d1	nádherný
krystalů	krystal	k1gInPc2	krystal
tohoto	tento	k3xDgInSc2	tento
nerostu	nerost	k1gInSc2	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Barevnost	barevnost	k1gFnSc1	barevnost
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
téměř	téměř	k6eAd1	téměř
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
barvu	barva	k1gFnSc4	barva
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
skvělé	skvělý	k2eAgFnSc3d1	skvělá
chemické	chemický	k2eAgFnSc3d1	chemická
mísitelnosti	mísitelnost	k1gFnSc3	mísitelnost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
odrůdami	odrůda	k1gFnPc7	odrůda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
elbaitu	elbait	k1gInSc2	elbait
je	být	k5eAaImIp3nS	být
striktně	striktně	k6eAd1	striktně
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
lithium	lithium	k1gNnSc4	lithium
bohaté	bohatý	k2eAgNnSc1d1	bohaté
granitické	granitický	k2eAgInPc1d1	granitický
pegmatity	pegmatit	k1gInPc1	pegmatit
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
asociaci	asociace	k1gFnSc6	asociace
s	s	k7c7	s
minerály	minerál	k1gInPc7	minerál
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
lepidolit	lepidolit	k1gInSc4	lepidolit
a	a	k8xC	a
albit	albit	k1gInSc4	albit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
také	také	k9	také
v	v	k7c6	v
rozsypech	rozsyp	k1gInPc6	rozsyp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Krystaly	krystal	k1gInPc1	krystal
elbaitu	elbait	k1gInSc2	elbait
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
trojboké	trojboký	k2eAgInPc1d1	trojboký
nebo	nebo	k8xC	nebo
šestiboké	šestiboký	k2eAgInPc1d1	šestiboký
podélně	podélně	k6eAd1	podélně
rýhované	rýhovaný	k2eAgInPc4d1	rýhovaný
sloupce	sloupec	k1gInPc4	sloupec
<g/>
,	,	kIx,	,
ukončené	ukončený	k2eAgInPc4d1	ukončený
plochou	plocha	k1gFnSc7	plocha
spodovou	spodový	k2eAgFnSc7d1	spodová
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
plochami	plocha	k1gFnPc7	plocha
nízkých	nízký	k2eAgFnPc2d1	nízká
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
oboustranně	oboustranně	k6eAd1	oboustranně
ukončených	ukončený	k2eAgInPc2d1	ukončený
krystalů	krystal	k1gInPc2	krystal
bývá	bývat	k5eAaImIp3nS	bývat
každý	každý	k3xTgInSc1	každý
konec	konec	k1gInSc1	konec
krystalu	krystal	k1gInSc2	krystal
jinak	jinak	k6eAd1	jinak
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
strukturní	strukturní	k2eAgFnSc1d1	strukturní
polarita	polarita	k1gFnSc1	polarita
elbaitu	elbait	k1gInSc2	elbait
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
elbaitu	elbait	k1gInSc2	elbait
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
turmalínů	turmalín	k1gInPc2	turmalín
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
krystal	krystal	k1gInSc1	krystal
zahříván	zahříván	k2eAgInSc1d1	zahříván
nebo	nebo	k8xC	nebo
podroben	podroben	k2eAgInSc1d1	podroben
tření	tření	k1gNnSc2	tření
či	či	k8xC	či
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
se	se	k3xPyFc4	se
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
konce	konec	k1gInSc2	konec
začnou	začít	k5eAaPmIp3nP	začít
přitahovat	přitahovat	k5eAaImF	přitahovat
jemné	jemný	k2eAgFnPc4d1	jemná
částečky	částečka	k1gFnPc4	částečka
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
konec	konec	k1gInSc1	konec
se	se	k3xPyFc4	se
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
kladně	kladně	k6eAd1	kladně
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
záporně	záporně	k6eAd1	záporně
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
výborné	výborný	k2eAgFnSc3d1	výborná
chemické	chemický	k2eAgFnSc3d1	chemická
mísitelnosti	mísitelnost	k1gFnSc3	mísitelnost
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
odrůdami	odrůda	k1gFnPc7	odrůda
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
také	také	k9	také
se	s	k7c7	s
zonálností	zonálnost	k1gFnSc7	zonálnost
elbaitu	elbait	k1gInSc2	elbait
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
střed	střed	k1gInSc1	střed
krystalu	krystal	k1gInSc2	krystal
má	mít	k5eAaImIp3nS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
barvu	barva	k1gFnSc4	barva
než	než	k8xS	než
jeho	jeho	k3xOp3gInSc4	jeho
zevnějšek	zevnějšek	k1gInSc4	zevnějšek
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
vrchní	vrchní	k2eAgFnSc1d1	vrchní
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
krystalu	krystal	k1gInSc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnPc1	kombinace
těchto	tento	k3xDgFnPc2	tento
zonálností	zonálnost	k1gFnPc2	zonálnost
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohdy	mnohdy	k6eAd1	mnohdy
nelze	lze	k6eNd1	lze
zcela	zcela	k6eAd1	zcela
bezpečně	bezpečně	k6eAd1	bezpečně
odlišit	odlišit	k5eAaPmF	odlišit
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
variety	varieta	k1gFnPc4	varieta
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
je	on	k3xPp3gNnSc4	on
určit	určit	k5eAaPmF	určit
teprve	teprve	k6eAd1	teprve
až	až	k9	až
podrobný	podrobný	k2eAgInSc4d1	podrobný
rozbor	rozbor	k1gInSc4	rozbor
na	na	k7c6	na
elektronové	elektronový	k2eAgFnSc6d1	elektronová
mikrosondě	mikrosonda	k1gFnSc6	mikrosonda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Elbait	Elbait	k1gInSc1	Elbait
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
klenotnictví	klenotnictví	k1gNnSc6	klenotnictví
jako	jako	k8xS	jako
hodnotné	hodnotný	k2eAgInPc4d1	hodnotný
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
velice	velice	k6eAd1	velice
překrásnou	překrásný	k2eAgFnSc4d1	překrásná
sbírkovou	sbírkový	k2eAgFnSc4d1	sbírková
záležitost	záležitost	k1gFnSc4	záležitost
a	a	k8xC	a
nejkrásnější	krásný	k2eAgInPc1d3	nejkrásnější
kousky	kousek	k1gInPc1	kousek
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
ve	v	k7c6	v
světových	světový	k2eAgNnPc6d1	světové
muzeích	muzeum	k1gNnPc6	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Achroit	Achroit	k2eAgMnSc1d1	Achroit
===	===	k?	===
</s>
</p>
<p>
<s>
Achroit	Achroit	k1gInSc1	Achroit
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
odrůda	odrůda	k1gFnSc1	odrůda
elbaitu	elbait	k1gInSc2	elbait
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
achroos	achroosa	k1gFnPc2	achroosa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velice	velice	k6eAd1	velice
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
odrůdu	odrůda	k1gFnSc4	odrůda
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
drahokamy	drahokam	k1gInPc7	drahokam
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
závratná	závratný	k2eAgFnSc1d1	závratná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
většímu	veliký	k2eAgInSc3d2	veliký
zájmu	zájem	k1gInSc3	zájem
u	u	k7c2	u
sběratelů	sběratel	k1gMnPc2	sběratel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
cení	cenit	k5eAaImIp3nP	cenit
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Achroit	Achroit	k1gInSc4	Achroit
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
pegmatitech	pegmatit	k1gInPc6	pegmatit
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
Saint	Saint	k1gMnSc1	Saint
Austell	Austell	k1gMnSc1	Austell
<g/>
,	,	kIx,	,
Cornwall	Cornwall	k1gMnSc1	Cornwall
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadu	jihozápad	k1gInSc2	jihozápad
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
většina	většina	k1gFnSc1	většina
nalezených	nalezený	k2eAgInPc2d1	nalezený
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Indigolit	Indigolit	k1gInSc4	Indigolit
===	===	k?	===
</s>
</p>
<p>
<s>
Indigolit	Indigolit	k1gInSc1	Indigolit
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
elbaitu	elbait	k1gInSc2	elbait
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
téměř	téměř	k6eAd1	téměř
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
barvivu	barvivo	k1gNnSc3	barvivo
indigo	indigo	k1gNnSc1	indigo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
získala	získat	k5eAaPmAgFnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
indigolitu	indigolit	k1gInSc2	indigolit
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
krásné	krásný	k2eAgInPc4d1	krásný
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
sloupce	sloupec	k1gInPc4	sloupec
drahokamové	drahokamový	k2eAgFnSc2d1	drahokamová
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
českou	český	k2eAgFnSc7d1	Česká
lokalitou	lokalita	k1gFnSc7	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
indigolit	indigolit	k1gInSc1	indigolit
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
je	on	k3xPp3gNnPc4	on
Rožná	Rožné	k1gNnPc4	Rožné
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
asociaci	asociace	k1gFnSc6	asociace
s	s	k7c7	s
rubelitem	rubelit	k1gInSc7	rubelit
a	a	k8xC	a
lepidolitem	lepidolit	k1gInSc7	lepidolit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
známé	známý	k2eAgFnPc4d1	známá
světové	světový	k2eAgFnPc4d1	světová
lokality	lokalita	k1gFnPc4	lokalita
odkud	odkud	k6eAd1	odkud
pocházejí	pocházet	k5eAaImIp3nP	pocházet
velmi	velmi	k6eAd1	velmi
čisté	čistý	k2eAgInPc1d1	čistý
kusy	kus	k1gInPc1	kus
je	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
Brazílie	Brazílie	k1gFnSc1	Brazílie
nebo	nebo	k8xC	nebo
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Melounový	melounový	k2eAgInSc1d1	melounový
turmalín	turmalín	k1gInSc1	turmalín
===	===	k?	===
</s>
</p>
<p>
<s>
Melounový	melounový	k2eAgInSc1d1	melounový
turmalín	turmalín	k1gInSc1	turmalín
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
akceptovanou	akceptovaný	k2eAgFnSc7d1	akceptovaná
odrůdou	odrůda	k1gFnSc7	odrůda
elbaitu	elbait	k1gInSc2	elbait
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c4	o
šperkařský	šperkařský	k2eAgInSc4d1	šperkařský
termín	termín	k1gInSc4	termín
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
elbait	elbait	k1gInSc1	elbait
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
červeno-zelenou	červenoelený	k2eAgFnSc4d1	červeno-zelená
zonálnost	zonálnost	k1gFnSc4	zonálnost
velice	velice	k6eAd1	velice
podobnou	podobný	k2eAgFnSc4d1	podobná
melounu	meloun	k1gInSc2	meloun
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
melounový	melounový	k2eAgInSc1d1	melounový
turmalín	turmalín	k1gInSc1	turmalín
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
velice	velice	k6eAd1	velice
hezká	hezký	k2eAgFnSc1d1	hezká
anomálie	anomálie	k1gFnSc1	anomálie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
broušena	brousit	k5eAaImNgFnS	brousit
a	a	k8xC	a
leštěna	leštit	k5eAaImNgFnS	leštit
na	na	k7c4	na
destičky	destička	k1gFnPc4	destička
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
okraje	okraj	k1gInPc1	okraj
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
neupravují	upravovat	k5eNaImIp3nP	upravovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paraíba	Paraíba	k1gFnSc1	Paraíba
(	(	kIx(	(
<g/>
turmalín	turmalín	k1gInSc1	turmalín
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Paraíba	Paraíba	k1gFnSc1	Paraíba
(	(	kIx(	(
<g/>
turmalín	turmalín	k1gInSc1	turmalín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
neonově	neonově	k6eAd1	neonově
modře	modro	k6eAd1	modro
zabarvená	zabarvený	k2eAgFnSc1d1	zabarvená
odrůda	odrůda	k1gFnSc1	odrůda
elbaitu	elbait	k1gInSc2	elbait
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byly	být	k5eAaImAgInP	být
u	u	k7c2	u
města	město	k1gNnSc2	město
Sao	Sao	k1gMnPc2	Sao
José	José	k1gNnSc2	José
de	de	k?	de
Batalha	Batalha	k1gMnSc1	Batalha
ve	v	k7c6	v
východobrazilském	východobrazilský	k2eAgInSc6d1	východobrazilský
státě	stát	k1gInSc6	stát
Paraíba	Paraíb	k1gMnSc2	Paraíb
nalezeny	nalezen	k2eAgInPc1d1	nalezen
krystaly	krystal	k1gInPc1	krystal
tohoto	tento	k3xDgInSc2	tento
drahokamu	drahokam	k1gInSc2	drahokam
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
světovou	světový	k2eAgFnSc7d1	světová
senzací	senzace	k1gFnSc7	senzace
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
turmalín	turmalín	k1gInSc1	turmalín
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
zařadil	zařadit	k5eAaPmAgMnS	zařadit
po	po	k7c4	po
bok	bok	k1gInSc4	bok
těch	ten	k3xDgMnPc2	ten
nejdražších	drahý	k2eAgMnPc2d3	nejdražší
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Svítivě	svítivě	k6eAd1	svítivě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
až	až	k8xS	až
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
paraíba	paraíb	k1gMnSc2	paraíb
turmalínu	turmalín	k1gInSc2	turmalín
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
příměsí	příměs	k1gFnSc7	příměs
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
4000	[number]	k4	4000
$	$	kIx~	$
za	za	k7c4	za
karát	karát	k1gInSc4	karát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
krystaly	krystal	k1gInPc1	krystal
nad	nad	k7c4	nad
dva	dva	k4xCgInPc4	dva
karáty	karát	k1gInPc4	karát
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rubelit	rubelit	k1gInSc4	rubelit
===	===	k?	===
</s>
</p>
<p>
<s>
Rubelit	rubelit	k1gInSc1	rubelit
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
elbaitu	elbait	k1gInSc2	elbait
se	s	k7c7	s
sytě	sytě	k6eAd1	sytě
růžovou	růžový	k2eAgFnSc7d1	růžová
až	až	k8xS	až
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
rubellus	rubellus	k1gInSc4	rubellus
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
narůžovělý	narůžovělý	k2eAgInSc4d1	narůžovělý
nebo	nebo	k8xC	nebo
podobný	podobný	k2eAgInSc4d1	podobný
rubínu	rubín	k1gInSc3	rubín
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
krystal	krystal	k1gInSc1	krystal
rubelitu	rubelit	k1gInSc2	rubelit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
kdy	kdy	k6eAd1	kdy
nalezen	naleznout	k5eAaPmNgInS	naleznout
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
"	"	kIx"	"
<g/>
Raketa	raketa	k1gFnSc1	raketa
<g/>
"	"	kIx"	"
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
109	[number]	k4	109
cm	cm	kA	cm
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Rubelity	rubelit	k1gInPc1	rubelit
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
brousit	brousit	k5eAaImF	brousit
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
růžové	růžový	k2eAgInPc1d1	růžový
krystaly	krystal	k1gInPc1	krystal
tvoří	tvořit	k5eAaImIp3nP	tvořit
překrásné	překrásný	k2eAgInPc1d1	překrásný
sbírkové	sbírkový	k2eAgInPc1d1	sbírkový
exponáty	exponát	k1gInPc1	exponát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Verdelit	Verdelit	k1gMnPc5	Verdelit
===	===	k?	===
</s>
</p>
<p>
<s>
Verdelit	Verdelit	k5eAaImF	Verdelit
(	(	kIx(	(
<g/>
též	též	k9	též
emeraldit	emeraldit	k5eAaPmF	emeraldit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
elbaitu	elbait	k1gInSc2	elbait
listově	listově	k6eAd1	listově
zelené	zelený	k2eAgFnPc1d1	zelená
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejběžnější	běžný	k2eAgFnSc4d3	nejběžnější
varietu	varieta	k1gFnSc4	varieta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
naleziště	naleziště	k1gNnSc1	naleziště
tohoto	tento	k3xDgInSc2	tento
minerálu	minerál	k1gInSc2	minerál
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
dolů	dolů	k6eAd1	dolů
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Pala	Pala	k1gMnSc1	Pala
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
anebo	anebo	k8xC	anebo
Dunton	Dunton	k1gInSc4	Dunton
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Maine	Main	k1gInSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
až	až	k9	až
27	[number]	k4	27
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
krystaly	krystal	k1gInPc4	krystal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Rožná	Rožná	k1gFnSc1	Rožná
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
rubelity	rubelit	k1gInPc1	rubelit
<g/>
,	,	kIx,	,
indigolity	indigolit	k1gInPc1	indigolit
<g/>
,	,	kIx,	,
verdelity	verdelit	k2eAgInPc1d1	verdelit
společně	společně	k6eAd1	společně
s	s	k7c7	s
lepidolitem	lepidolit	k1gInSc7	lepidolit
na	na	k7c6	na
Li-pegmatitovém	Liegmatitový	k2eAgNnSc6d1	Li-pegmatitový
ložisku	ložisko	k1gNnSc6	ložisko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gilgit	Gilgit	k1gInSc1	Gilgit
(	(	kIx(	(
<g/>
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kunar	Kunar	k1gInSc1	Kunar
(	(	kIx(	(
<g/>
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
vzorky	vzorek	k1gInPc4	vzorek
drahokamové	drahokamový	k2eAgFnSc2d1	drahokamová
kvality	kvalita	k1gFnSc2	kvalita
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Utö	Utö	k?	Utö
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
</s>
</p>
<p>
<s>
Malchan	Malchan	k1gInSc1	Malchan
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
elbait	elbaita	k1gFnPc2	elbaita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Elbait	Elbait	k2eAgMnSc1d1	Elbait
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Elbait	Elbait	k2eAgMnSc1d1	Elbait
na	na	k7c6	na
webu	web	k1gInSc6	web
webmineral	webminerat	k5eAaImAgMnS	webminerat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Paraíba	Paraíba	k1gFnSc1	Paraíba
turmalín	turmalín	k1gInSc1	turmalín
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaPmF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rubelit	rubelit	k1gInSc1	rubelit
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaPmF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Indigolit	Indigolit	k1gInSc1	Indigolit
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaImF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Melounový	melounový	k2eAgInSc1d1	melounový
turmalín	turmalín	k1gInSc1	turmalín
na	na	k7c6	na
webu	web	k1gInSc6	web
gemdat	gemdat	k5eAaImF	gemdat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
