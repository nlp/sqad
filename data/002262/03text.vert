<s>
Wales	Wales	k1gInSc1	Wales
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
[	[	kIx(	[
<g/>
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
Vels	Velsa	k1gFnPc2	Velsa
nebo	nebo	k8xC	nebo
Vejls	Vejls	k1gInSc1	Vejls
<g/>
]	]	kIx)	]
převzatý	převzatý	k2eAgMnSc1d1	převzatý
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
;	;	kIx,	;
velšsky	velšsky	k6eAd1	velšsky
Cymru	Cymra	k1gFnSc4	Cymra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
autonomií	autonomie	k1gFnSc7	autonomie
<g/>
,	,	kIx,	,
rozprostírající	rozprostírající	k2eAgNnPc1d1	rozprostírající
se	se	k3xPyFc4	se
západně	západně	k6eAd1	západně
od	od	k7c2	od
Anglie	Anglie	k1gFnSc2	Anglie
na	na	k7c6	na
západě	západ	k1gInSc6	západ
ostrova	ostrov	k1gInSc2	ostrov
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
obývá	obývat	k5eAaImIp3nS	obývat
keltský	keltský	k2eAgInSc1d1	keltský
národ	národ	k1gInSc1	národ
Velšanů	Velšan	k1gMnPc2	Velšan
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgInSc1d1	formální
název	název	k1gInSc1	název
Knížectví	knížectví	k1gNnSc1	knížectví
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Principality	principalita	k1gFnSc2	principalita
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
velšsky	velšsky	k6eAd1	velšsky
Tywysogaeth	Tywysogaeth	k1gMnSc1	Tywysogaeth
Cymru	Cymr	k1gInSc2	Cymr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzácně	vzácně	k6eAd1	vzácně
a	a	k8xC	a
termín	termín	k1gInSc4	termín
Velšské	velšský	k2eAgNnSc4d1	Velšské
knížectví	knížectví	k1gNnSc4	knížectví
nepatří	patřit	k5eNaImIp3nS	patřit
zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Pembrokeshiru	Pembrokeshir	k1gInSc2	Pembrokeshir
<g/>
,	,	kIx,	,
Cardiganshiru	Cardiganshir	k1gInSc2	Cardiganshir
a	a	k8xC	a
Gwyneddu	Gwynedd	k1gInSc2	Gwynedd
k	k	k7c3	k
nejoblíbenějším	oblíbený	k2eAgFnPc3d3	nejoblíbenější
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
současných	současný	k2eAgFnPc6d1	současná
hranicích	hranice	k1gFnPc6	hranice
nikdy	nikdy	k6eAd1	nikdy
netvořil	tvořit	k5eNaImAgInS	tvořit
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získal	získat	k5eAaPmAgMnS	získat
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gNnSc7	jeho
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
o	o	k7c6	o
téměř	téměř	k6eAd1	téměř
dnešním	dnešní	k2eAgInSc6d1	dnešní
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
Gruffydd	Gruffydd	k1gMnSc1	Gruffydd
ap	ap	kA	ap
Llywelyn	Llywelyn	k1gMnSc1	Llywelyn
a	a	k8xC	a
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1057	[number]	k4	1057
až	až	k9	až
do	do	k7c2	do
Gruffuddovy	Gruffuddův	k2eAgFnSc2d1	Gruffuddův
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1063	[number]	k4	1063
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
Wales	Wales	k1gInSc1	Wales
jednoho	jeden	k4xCgMnSc2	jeden
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
suverenitu	suverenita	k1gFnSc4	suverenita
uznala	uznat	k5eAaPmAgFnS	uznat
i	i	k9	i
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
potom	potom	k6eAd1	potom
neopakovalo	opakovat	k5eNaImAgNnS	opakovat
a	a	k8xC	a
než	než	k8xS	než
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1282	[number]	k4	1282
dobyl	dobýt	k5eAaPmAgMnS	dobýt
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
I.	I.	kA	I.
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
malých	malý	k2eAgNnPc2d1	malé
království	království	k1gNnPc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1301	[number]	k4	1301
tradičně	tradičně	k6eAd1	tradičně
náleží	náležet	k5eAaImIp3nS	náležet
následníkovi	následník	k1gMnSc3	následník
anglického	anglický	k2eAgNnSc2d1	anglické
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
britského	britský	k2eAgInSc2d1	britský
trůnu	trůn	k1gInSc2	trůn
titul	titul	k1gInSc1	titul
knížete	kníže	k1gMnSc2	kníže
velšského	velšský	k2eAgMnSc2d1	velšský
(	(	kIx(	(
<g/>
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
-	-	kIx~	-
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
často	často	k6eAd1	často
chybně	chybně	k6eAd1	chybně
překládán	překládán	k2eAgInSc1d1	překládán
jako	jako	k9	jako
"	"	kIx"	"
<g/>
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnSc7	metropol
Walesu	Wales	k1gInSc2	Wales
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
Cardiff	Cardiff	k1gInSc1	Cardiff
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
je	být	k5eAaImIp3nS	být
Caernarfon	Caernarfon	k1gInSc4	Caernarfon
místem	místem	k6eAd1	místem
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
udělován	udělován	k2eAgInSc4d1	udělován
titul	titul	k1gInSc4	titul
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Machynllethu	Machynlleth	k1gInSc6	Machynlleth
sídlil	sídlit	k5eAaImAgInS	sídlit
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
svolal	svolat	k5eAaPmAgMnS	svolat
velšský	velšský	k2eAgMnSc1d1	velšský
vůdce	vůdce	k1gMnSc1	vůdce
Owain	Owain	k1gMnSc1	Owain
Glyndŵ	Glyndŵ	k1gMnSc1	Glyndŵ
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
revolty	revolta	k1gFnSc2	revolta
počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
má	mít	k5eAaImIp3nS	mít
Wales	Wales	k1gInSc1	Wales
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
období	období	k1gNnSc6	období
přímé	přímý	k2eAgFnSc2d1	přímá
anglické	anglický	k2eAgFnSc2d1	anglická
a	a	k8xC	a
britské	britský	k2eAgFnSc2d1	britská
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
trvající	trvající	k2eAgInPc1d1	trvající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Prvním	první	k4xOgInSc7	první
aktem	akt	k1gInSc7	akt
o	o	k7c6	o
unii	unie	k1gFnSc6	unie
právně	právně	k6eAd1	právně
i	i	k8xC	i
administrativně	administrativně	k6eAd1	administrativně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Walesu	Wales	k1gInSc2	Wales
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Velšské	velšský	k2eAgNnSc1d1	Velšské
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Assembly	Assembly	k1gFnSc2	Assembly
for	forum	k1gNnPc2	forum
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
omezené	omezený	k2eAgFnPc4d1	omezená
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
Wales	Wales	k1gInSc4	Wales
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
germánského	germánský	k2eAgNnSc2d1	germánské
slova	slovo	k1gNnSc2	slovo
Walha	Walh	k1gMnSc2	Walh
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
cizí	cizí	k2eAgFnSc3d1	cizí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
etymologie	etymologie	k1gFnPc4	etymologie
je	být	k5eAaImIp3nS	být
i	i	k9	i
Valašsko	Valašsko	k1gNnSc1	Valašsko
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Valonsko	Valonsko	k1gNnSc1	Valonsko
či	či	k8xC	či
Galie	Galie	k1gFnSc1	Galie
(	(	kIx(	(
<g/>
kelt	kelt	k1gInSc1	kelt
<g/>
/	/	kIx~	/
<g/>
Keltoi	Keltoi	k1gNnSc1	Keltoi
<g/>
/	/	kIx~	/
<g/>
Galatai	Galatai	k1gNnSc1	Galatai
<g/>
,	,	kIx,	,
romanizovaný	romanizovaný	k2eAgMnSc1d1	romanizovaný
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velšané	Velšan	k1gMnPc1	Velšan
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
nazývají	nazývat	k5eAaImIp3nP	nazývat
Cymry	Cymra	k1gFnPc1	Cymra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
velštině	velština	k1gFnSc6	velština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
krajan	krajan	k1gMnSc1	krajan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
ve	v	k7c6	v
velšské	velšský	k2eAgFnSc6d1	velšská
literatuře	literatura	k1gFnSc6	literatura
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
používal	používat	k5eAaImAgInS	používat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Brythoniaid	Brythoniaida	k1gFnPc2	Brythoniaida
(	(	kIx(	(
<g/>
Britové	Brit	k1gMnPc1	Brit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
na	na	k7c6	na
středozápadě	středozápad	k1gInSc6	středozápad
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
779	[number]	k4	779
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
274	[number]	k4	274
km	km	kA	km
a	a	k8xC	a
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
asi	asi	k9	asi
97	[number]	k4	97
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
třech	tři	k4xCgFnPc6	tři
stranách	strana	k1gFnPc6	strana
<g/>
:	:	kIx,	:
s	s	k7c7	s
Bristolským	bristolský	k2eAgInSc7d1	bristolský
průlivem	průliv	k1gInSc7	průliv
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
se	s	k7c7	s
Svatojiřským	svatojiřský	k2eAgInSc7d1	svatojiřský
průlivem	průliv	k1gInSc7	průliv
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
a	a	k8xC	a
Irským	irský	k2eAgNnSc7d1	irské
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Velšské	velšský	k2eAgNnSc1d1	Velšské
pobřeží	pobřeží	k1gNnSc1	pobřeží
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
965	[number]	k4	965
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Walesu	Wales	k1gInSc2	Wales
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnPc4d3	veliký
Anglesey	Anglese	k1gMnPc4	Anglese
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
koncentrace	koncentrace	k1gFnSc1	koncentrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
Walesu	Wales	k1gInSc6	Wales
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Cardiff	Cardiff	k1gInSc1	Cardiff
<g/>
,	,	kIx,	,
Swansea	Swansea	k1gFnSc1	Swansea
a	a	k8xC	a
Newport	Newport	k1gInSc1	Newport
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnPc1	hora
byly	být	k5eAaImAgFnP	být
vymodelovány	vymodelovat	k5eAaPmNgFnP	vymodelovat
během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hory	hora	k1gFnPc1	hora
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
Snowdonii	Snowdonie	k1gFnSc6	Snowdonie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Walesu	Wales	k1gInSc2	Wales
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgFnSc1d1	zdejší
hora	hora	k1gFnSc1	hora
Snowdon	Snowdon	k1gMnSc1	Snowdon
(	(	kIx(	(
<g/>
1085	[number]	k4	1085
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Walesu	Wales	k1gInSc6	Wales
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Kambrijské	Kambrijský	k2eAgNnSc4d1	Kambrijský
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
navazuje	navazovat	k5eAaImIp3nS	navazovat
pohoří	pohoří	k1gNnSc1	pohoří
Brecon	Brecona	k1gFnPc2	Brecona
Beacons	Beacons	k1gInSc4	Beacons
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Pen	Pen	k1gFnPc2	Pen
y	y	k?	y
Fan	Fana	k1gFnPc2	Fana
(	(	kIx(	(
<g/>
886	[number]	k4	886
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
hranice	hranice	k1gFnSc1	hranice
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
jsou	být	k5eAaImIp3nP	být
hodně	hodně	k6eAd1	hodně
umělé	umělý	k2eAgFnPc1d1	umělá
<g/>
;	;	kIx,	;
byly	být	k5eAaImAgFnP	být
definovány	definovat	k5eAaBmNgFnP	definovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
základě	základ	k1gInSc6	základ
feudálních	feudální	k2eAgFnPc2d1	feudální
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
potvrzení	potvrzení	k1gNnSc2	potvrzení
Monmouthshiru	Monmouthshir	k1gInSc2	Monmouthshir
jako	jako	k8xS	jako
části	část	k1gFnSc2	část
Walesu	Wales	k1gInSc2	Wales
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
schváleny	schválit	k5eAaPmNgFnP	schválit
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
referendu	referendum	k1gNnSc6	referendum
a	a	k8xC	a
neurčila	určit	k5eNaPmAgFnS	určit
je	být	k5eAaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
hraniční	hraniční	k2eAgFnSc1d1	hraniční
komise	komise	k1gFnSc1	komise
a	a	k8xC	a
kopírují	kopírovat	k5eAaImIp3nP	kopírovat
Ofův	Ofův	k1gInSc4	Ofův
val	val	k1gInSc4	val
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
různými	různý	k2eAgInPc7d1	různý
krkolomnými	krkolomný	k2eAgInPc7d1	krkolomný
názvy	název	k1gInPc7	název
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
např	např	kA	např
Cwmbran	Cwmbrana	k1gFnPc2	Cwmbrana
Town	Town	k1gMnSc1	Town
Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	k1gMnSc1	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Walesu	Wales	k1gInSc2	Wales
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
osídlena	osídlen	k2eAgFnSc1d1	osídlena
minimálně	minimálně	k6eAd1	minimálně
29	[number]	k4	29
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nepřetržité	přetržitý	k2eNgNnSc1d1	nepřetržité
osídlení	osídlení	k1gNnSc1	osídlení
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k9	až
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnPc1d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
památek	památka	k1gFnPc2	památka
z	z	k7c2	z
období	období	k1gNnSc2	období
neolitu	neolit	k1gInSc2	neolit
(	(	kIx(	(
<g/>
především	především	k9	především
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
a	a	k8xC	a
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
psané	psaný	k2eAgInPc4d1	psaný
historické	historický	k2eAgInPc4d1	historický
záznamy	záznam	k1gInPc4	záznam
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
dobývat	dobývat	k5eAaImF	dobývat
Wales	Wales	k1gInSc4	Wales
v	v	k7c6	v
roce	rok	k1gInSc6	rok
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
současného	současný	k2eAgInSc2d1	současný
Walesu	Wales	k1gInSc2	Wales
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
klanových	klanový	k2eAgNnPc2d1	klanové
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
řetěz	řetěz	k1gInSc4	řetěz
pevností	pevnost	k1gFnPc2	pevnost
napříč	napříč	k7c7	napříč
jižním	jižní	k2eAgInSc7d1	jižní
Walesem	Wales	k1gInSc7	Wales
a	a	k8xC	a
těžili	těžit	k5eAaImAgMnP	těžit
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
Dolaucothi	Dolaucoth	k1gFnSc6	Dolaucoth
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
také	také	k9	také
legionářskou	legionářský	k2eAgFnSc4d1	legionářská
pevnost	pevnost	k1gFnSc4	pevnost
Caerleon	Caerleona	k1gFnPc2	Caerleona
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Isca	Isca	k1gFnSc1	Isca
Silurum	Silurum	k1gInSc1	Silurum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
nejzachovalejší	zachovalý	k2eAgInSc1d3	nejzachovalejší
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přinesli	přinést	k5eAaPmAgMnP	přinést
Římané	Říman	k1gMnPc1	Říman
do	do	k7c2	do
Walesu	Wales	k1gInSc2	Wales
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
410	[number]	k4	410
Římané	Říman	k1gMnPc1	Říman
opustili	opustit	k5eAaPmAgMnP	opustit
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
nížin	nížina	k1gFnPc2	nížina
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
ovládly	ovládnout	k5eAaPmAgInP	ovládnout
různé	různý	k2eAgInPc1d1	různý
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
nezávislých	závislý	k2eNgInPc2d1	nezávislý
velšských	velšský	k2eAgInPc2d1	velšský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Gwynedd	Gwynedd	k1gInSc1	Gwynedd
<g/>
,	,	kIx,	,
Powys	Powys	k1gInSc1	Powys
<g/>
,	,	kIx,	,
Dyfed	Dyfed	k1gMnSc1	Dyfed
a	a	k8xC	a
Seisyllg	Seisyllg	k1gMnSc1	Seisyllg
<g/>
,	,	kIx,	,
Morgannwg	Morgannwg	k1gMnSc1	Morgannwg
a	a	k8xC	a
Gwent	Gwent	k1gMnSc1	Gwent
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
především	především	k9	především
díky	díky	k7c3	díky
vhodným	vhodný	k2eAgFnPc3d1	vhodná
zeměpisným	zeměpisný	k2eAgFnPc3d1	zeměpisná
podmínkám	podmínka	k1gFnPc3	podmínka
(	(	kIx(	(
<g/>
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnPc1	řeka
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
tak	tak	k6eAd1	tak
základ	základ	k1gInSc4	základ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
známe	znát	k5eAaImIp1nP	znát
jako	jako	k8xC	jako
dnešní	dnešní	k2eAgInSc1d1	dnešní
Wales	Wales	k1gInSc1	Wales
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
budovaly	budovat	k5eAaImAgFnP	budovat
státy	stát	k1gInPc4	stát
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
opevněné	opevněný	k2eAgInPc1d1	opevněný
valy	val	k1gInPc1	val
(	(	kIx(	(
<g/>
Wat	Wat	k1gFnSc1	Wat
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dyke	Dyke	k1gInSc1	Dyke
<g/>
,	,	kIx,	,
Offa	Offa	k1gFnSc1	Offa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dyke	Dyke	k1gNnSc7	Dyke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zbytcích	zbytek	k1gInPc6	zbytek
Offa	Off	k2eAgFnSc1d1	Off
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dyke	Dyke	k1gFnSc7	Dyke
vede	vést	k5eAaImIp3nS	vést
dnes	dnes	k6eAd1	dnes
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
napříč	napříč	k7c7	napříč
celým	celý	k2eAgInSc7d1	celý
poloostrovem	poloostrov	k1gInSc7	poloostrov
z	z	k7c2	z
jižního	jižní	k2eAgInSc2d1	jižní
až	až	k9	až
na	na	k7c4	na
severní	severní	k2eAgNnSc4d1	severní
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
šarvátek	šarvátka	k1gFnPc2	šarvátka
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
i	i	k9	i
vikinští	vikinský	k2eAgMnPc1d1	vikinský
nájezdníci	nájezdník	k1gMnPc1	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
Velšané	Velšan	k1gMnPc1	Velšan
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
s	s	k7c7	s
Vikingy	Viking	k1gMnPc7	Viking
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
společném	společný	k2eAgInSc6d1	společný
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
anglo-saské	angloaský	k2eAgFnSc3d1	anglo-saská
Mercii	Mercie	k1gFnSc3	Mercie
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
tak	tak	k6eAd1	tak
zabránili	zabránit	k5eAaPmAgMnP	zabránit
expanzi	expanze	k1gFnSc4	expanze
do	do	k7c2	do
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
středověk	středověk	k1gInSc1	středověk
je	být	k5eAaImIp3nS	být
provázen	provázet	k5eAaImNgInS	provázet
boji	boj	k1gInSc3	boj
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
Anglosasy	Anglosas	k1gMnPc7	Anglosas
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
Normany	Norman	k1gMnPc7	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Obsadit	obsadit	k5eAaPmF	obsadit
Wales	Wales	k1gInSc4	Wales
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k6eAd1	až
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Eduardu	Eduard	k1gMnSc3	Eduard
I.	I.	kA	I.
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
spoutal	spoutat	k5eAaPmAgMnS	spoutat
Wales	Wales	k1gInSc4	Wales
kruhem	kruh	k1gInSc7	kruh
královských	královský	k2eAgInPc2d1	královský
hradů	hrad	k1gInPc2	hrad
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
titu	titu	k6eAd1	titu
kníže	kníže	k1gMnSc1	kníže
Velšský	velšský	k2eAgMnSc1d1	velšský
(	(	kIx(	(
<g/>
Prince	princa	k1gFnSc3	princa
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
držitelem	držitel	k1gMnSc7	držitel
je	být	k5eAaImIp3nS	být
následník	následník	k1gMnSc1	následník
anglického	anglický	k2eAgNnSc2d1	anglické
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
britského	britský	k2eAgInSc2d1	britský
<g/>
)	)	kIx)	)
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
anglické	anglický	k2eAgFnSc3d1	anglická
nadvládě	nadvláda	k1gFnSc3	nadvláda
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
zvedl	zvednout	k5eAaPmAgMnS	zvednout
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
povstání	povstání	k1gNnSc4	povstání
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Owain	Owain	k1gMnSc1	Owain
Glyndŵ	Glyndŵ	k1gMnSc1	Glyndŵ
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
nad	nad	k7c7	nad
Walesem	Wales	k1gInSc7	Wales
kontrolu	kontrola	k1gFnSc4	kontrola
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
byl	být	k5eAaImAgInS	být
Wales	Wales	k1gInSc1	Wales
plně	plně	k6eAd1	plně
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
,	,	kIx,	,
velšská	velšský	k2eAgFnSc1d1	velšská
legislativa	legislativa	k1gFnSc1	legislativa
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
parlamentem	parlament	k1gInSc7	parlament
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začínala	začínat	k5eAaImAgFnS	začínat
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Wales	Wales	k1gInSc4	Wales
hluboce	hluboko	k6eAd1	hluboko
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
jihovýchodní	jihovýchodní	k2eAgInSc1d1	jihovýchodní
Wales	Wales	k1gInSc1	Wales
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
velmi	velmi	k6eAd1	velmi
industrializován	industrializovat	k5eAaBmNgInS	industrializovat
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dramaticky	dramaticky	k6eAd1	dramaticky
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
původně	původně	k6eAd1	původně
velšsky	velšsky	k6eAd1	velšsky
mluvící	mluvící	k2eAgFnPc1d1	mluvící
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
poangličtěny	poangličtěn	k2eAgFnPc1d1	poangličtěn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začal	začít	k5eAaPmAgInS	začít
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
úpadek	úpadek	k1gInSc1	úpadek
těžby	těžba	k1gFnSc2	těžba
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
železářství	železářství	k1gNnSc2	železářství
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvořil	tvořit	k5eAaImAgInS	tvořit
základ	základ	k1gInSc4	základ
zdejšího	zdejší	k2eAgInSc2d1	zdejší
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Plaid	Plaid	k1gInSc1	Plaid
Cymru	Cymr	k1gInSc2	Cymr
(	(	kIx(	(
<g/>
národní	národní	k2eAgFnSc1d1	národní
velšská	velšský	k2eAgFnSc1d1	velšská
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgNnPc4	první
křesla	křeslo	k1gNnPc4	křeslo
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
a	a	k8xC	a
zavedení	zavedení	k1gNnSc6	zavedení
velšské	velšský	k2eAgFnSc2d1	velšská
samosprávy	samospráva	k1gFnSc2	samospráva
(	(	kIx(	(
<g/>
devolution	devolution	k1gInSc1	devolution
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
aktuálním	aktuální	k2eAgNnSc7d1	aktuální
politickým	politický	k2eAgNnSc7d1	politické
tématem	téma	k1gNnSc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
domácí	domácí	k2eAgFnSc6d1	domácí
samosprávě	samospráva	k1gFnSc6	samospráva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
bylo	být	k5eAaImAgNnS	být
neúspěšné	úspěšný	k2eNgNnSc1d1	neúspěšné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
voliči	volič	k1gMnPc1	volič
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
svůj	svůj	k3xOyFgInSc4	svůj
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
referenda	referendum	k1gNnSc2	referendum
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
Velšského	velšský	k2eAgNnSc2d1	Velšské
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Cardiffu	Cardiff	k1gInSc6	Cardiff
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vývoj	vývoj	k1gInSc1	vývoj
správního	správní	k2eAgNnSc2d1	správní
členění	členění	k1gNnSc2	členění
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
konstituční	konstituční	k2eAgFnPc1d1	konstituční
části	část	k1gFnPc1	část
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
britský	britský	k2eAgMnSc1d1	britský
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
královny	královna	k1gFnSc2	královna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uskutečňována	uskutečňovat	k5eAaImNgFnS	uskutečňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Parlamentu	parlament	k1gInSc2	parlament
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
pravomoci	pravomoc	k1gFnPc1	pravomoc
jsou	být	k5eAaImIp3nP	být
delegovány	delegovat	k5eAaBmNgFnP	delegovat
na	na	k7c4	na
Národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Assembly	Assembly	k1gFnSc2	Assembly
for	forum	k1gNnPc2	forum
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
v	v	k7c6	v
Cardiffu	Cardiff	k1gInSc6	Cardiff
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
vydává	vydávat	k5eAaPmIp3nS	vydávat
primární	primární	k2eAgInPc4d1	primární
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
rámci	rámec	k1gInSc6	rámec
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
sekundární	sekundární	k2eAgFnSc4d1	sekundární
legislativu	legislativa	k1gFnSc4	legislativa
platnou	platný	k2eAgFnSc4d1	platná
na	na	k7c4	na
území	území	k1gNnSc4	území
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
má	mít	k5eAaImIp3nS	mít
Wales	Wales	k1gInSc1	Wales
vyhrazen	vyhrazen	k2eAgInSc1d1	vyhrazen
40	[number]	k4	40
poslaneckých	poslanecký	k2eAgNnPc2d1	poslanecké
křesel	křeslo	k1gNnPc2	křeslo
(	(	kIx(	(
<g/>
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
646	[number]	k4	646
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
vlády	vláda	k1gFnSc2	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
je	být	k5eAaImIp3nS	být
Státní	státní	k2eAgMnSc1d1	státní
sekretář	sekretář	k1gMnSc1	sekretář
pro	pro	k7c4	pro
Wales	Wales	k1gInSc4	Wales
(	(	kIx(	(
<g/>
Secretary	Secretar	k1gInPc1	Secretar
of	of	k?	of
State	status	k1gInSc5	status
for	forum	k1gNnPc2	forum
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
záležitosti	záležitost	k1gFnPc4	záležitost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Walesu	Wales	k1gInSc2	Wales
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
červené	červený	k2eAgFnSc2d1	červená
saně	saně	k1gFnSc2	saně
prince	princ	k1gMnSc2	princ
Calwaladera	Calwalader	k1gMnSc2	Calwalader
a	a	k8xC	a
tudorovských	tudorovský	k2eAgFnPc2d1	Tudorovská
barev	barva	k1gFnPc2	barva
-	-	kIx~	-
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Bosworthu	Bosworth	k1gInSc2	Bosworth
a	a	k8xC	a
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
přidán	přidán	k2eAgInSc4d1	přidán
červený	červený	k2eAgInSc4d1	červený
drak	drak	k1gInSc4	drak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
velšského	velšský	k2eAgInSc2d1	velšský
původu	původ	k1gInSc2	původ
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
<g/>
.	.	kIx.	.
</s>
<s>
Velšskou	velšský	k2eAgFnSc7d1	velšská
národní	národní	k2eAgFnSc7d1	národní
vlajkou	vlajka	k1gFnSc7	vlajka
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
britská	britský	k2eAgFnSc1d1	britská
vlajka	vlajka	k1gFnSc1	vlajka
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádný	žádný	k3yNgInSc1	žádný
velšský	velšský	k2eAgInSc1d1	velšský
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
Walesu	Wales	k1gInSc2	Wales
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
národním	národní	k2eAgInSc7d1	národní
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nejstarší	starý	k2eAgInPc1d3	nejstarší
doklady	doklad	k1gInPc1	doklad
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
tohoto	tento	k3xDgInSc2	tento
symbolu	symbol	k1gInSc2	symbol
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Historia	Historium	k1gNnSc2	Historium
Brittonum	Brittonum	k1gNnSc4	Brittonum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
populární	populární	k2eAgFnSc2d1	populární
legendy	legenda	k1gFnSc2	legenda
byl	být	k5eAaImAgInS	být
drak	drak	k1gInSc1	drak
již	již	k6eAd1	již
na	na	k7c6	na
standartě	standarta	k1gFnSc6	standarta
krále	král	k1gMnSc2	král
Artuše	Artuš	k1gMnSc2	Artuš
<g/>
.	.	kIx.	.
</s>
<s>
Pórek	pórek	k1gInSc1	pórek
je	být	k5eAaImIp3nS	být
také	také	k9	také
národním	národní	k2eAgInSc7d1	národní
symbolem	symbol	k1gInSc7	symbol
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
poručil	poručit	k5eAaPmAgMnS	poručit
svatý	svatý	k2eAgMnSc1d1	svatý
David	David	k1gMnSc1	David
svým	svůj	k3xOyFgMnPc3	svůj
velšským	velšský	k2eAgMnPc3d1	velšský
vojákům	voják	k1gMnPc3	voják
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
bitev	bitva	k1gFnPc2	bitva
konající	konající	k2eAgFnSc2d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
pórkovém	pórkový	k2eAgNnSc6d1	pórkový
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
na	na	k7c6	na
helmy	helma	k1gFnSc2	helma
připevnili	připevnit	k5eAaPmAgMnP	připevnit
tuto	tento	k3xDgFnSc4	tento
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Pórek	pórek	k1gInSc1	pórek
potom	potom	k6eAd1	potom
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
rozpoznávací	rozpoznávací	k2eAgNnSc1d1	rozpoznávací
znamení	znamení	k1gNnSc1	znamení
vojáků	voják	k1gMnPc2	voják
bojujících	bojující	k2eAgMnPc2d1	bojující
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
reverzu	reverz	k1gInSc6	reverz
britských	britský	k2eAgFnPc2d1	britská
jednolibrových	jednolibrový	k2eAgFnPc2d1	jednolibrový
mincí	mince	k1gFnPc2	mince
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1985	[number]	k4	1985
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Narcis	narcis	k1gInSc1	narcis
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
květinou	květina	k1gFnSc7	květina
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Davida	David	k1gMnSc2	David
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
připevňuje	připevňovat	k5eAaImIp3nS	připevňovat
na	na	k7c4	na
oděv	oděv	k1gInSc4	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
svatého	svatý	k2eAgMnSc2d1	svatý
Davida	David	k1gMnSc2	David
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
používá	používat	k5eAaImIp3nS	používat
místo	místo	k1gNnSc4	místo
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
se	se	k3xPyFc4	se
na	na	k7c4	na
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Davida	David	k1gMnSc2	David
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
znak	znak	k1gInSc1	znak
Walesu	Wales	k1gInSc2	Wales
používá	používat	k5eAaImIp3nS	používat
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
velšský	velšský	k2eAgMnSc1d1	velšský
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
osobní	osobní	k2eAgInSc4d1	osobní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wales	Wales	k1gInSc1	Wales
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Wales	Wales	k1gInSc1	Wales
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Wales	Wales	k1gInSc1	Wales
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
není	být	k5eNaImIp3nS	být
princem	princ	k1gMnSc7	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
knížetem	kníže	k1gNnSc7wR	kníže
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
</s>
