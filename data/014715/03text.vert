<s>
Děrná	děrný	k2eAgFnSc1d1
páska	páska	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
5-	5-	k4
a	a	k8xC
8	#num#	k4
<g/>
-stopá	-stopý	k2eAgFnSc1d1
děrná	děrný	k2eAgFnSc1d1
páska	páska	k1gFnSc1
</s>
<s>
Děrná	děrný	k2eAgFnSc1d1
páska	páska	k1gFnSc1
je	být	k5eAaImIp3nS
historické	historický	k2eAgNnSc4d1
paměťové	paměťový	k2eAgNnSc4d1
médium	médium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používala	používat	k5eAaImAgFnS
se	se	k3xPyFc4
v	v	k7c6
dálnopisech	dálnopis	k1gInPc6
a	a	k8xC
též	též	k9
v	v	k7c6
terminálech	terminál	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nahradila	nahradit	k5eAaPmAgFnS
děrný	děrný	k2eAgInSc4d1
štítek	štítek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
papírový	papírový	k2eAgInSc4d1
nosič	nosič	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
jako	jako	k9
nosič	nosič	k1gInSc4
dat	datum	k1gNnPc2
pro	pro	k7c4
řízení	řízení	k1gNnSc4
činnosti	činnost	k1gFnSc2
pořizovačů	pořizovač	k1gInPc2
dat	datum	k1gNnPc2
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
trvanlivější	trvanlivý	k2eAgFnSc1d2
kovová	kovový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dálnopisná	dálnopisný	k2eAgFnSc1d1
páska	páska	k1gFnSc1
byla	být	k5eAaImAgFnS
pětistopá	pětistopý	k2eAgFnSc1d1
(	(	kIx(
<g/>
pět	pět	k4xCc1
stop	stopa	k1gFnPc2
pro	pro	k7c4
záznam	záznam	k1gInSc4
dat	datum	k1gNnPc2
[	[	kIx(
<g/>
velké	velký	k2eAgInPc1d1
otvory	otvor	k1gInPc1
<g/>
]	]	kIx)
a	a	k8xC
vodicí	vodicí	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
[	[	kIx(
<g/>
malé	malý	k2eAgInPc1d1
otvory	otvor	k1gInPc1
-	-	kIx~
na	na	k7c6
obrázku	obrázek	k1gInSc6
čtvrtá	čtvrtý	k4xOgFnSc1
řada	řada	k1gFnSc1
od	od	k7c2
spodu	spod	k1gInSc2
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Páska	páska	k1gFnSc1
pro	pro	k7c4
pořízení	pořízení	k1gNnSc4
dat	datum	k1gNnPc2
do	do	k7c2
počítače	počítač	k1gInSc2
byla	být	k5eAaImAgFnS
osmistopá	osmistopý	k2eAgFnSc1d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
obrázek	obrázek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrola	kontrola	k1gFnSc1
bezchybného	bezchybný	k2eAgNnSc2d1
děrování	děrování	k1gNnSc2
byla	být	k5eAaImAgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
prováděna	provádět	k5eAaImNgNnP
pomocí	pomocí	k7c2
liché	lichý	k2eAgFnSc2d1
nebo	nebo	k8xC
sudé	sudý	k2eAgFnSc2d1
parity	parita	k1gFnSc2
<g/>
,	,	kIx,
tzn.	tzn.	kA
<g/>
,	,	kIx,
že	že	k8xS
veškeré	veškerý	k3xTgInPc1
znaky	znak	k1gInPc1
měly	mít	k5eAaImAgInP
buď	buď	k8xC
lichý	lichý	k2eAgMnSc1d1
nebo	nebo	k8xC
sudý	sudý	k2eAgInSc1d1
počet	počet	k1gInSc1
děrovaných	děrovaný	k2eAgInPc2d1
otvorů	otvor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Čtení	čtení	k1gNnSc1
děrné	děrný	k2eAgFnSc2d1
pásky	páska	k1gFnSc2
probíhalo	probíhat	k5eAaImAgNnS
buď	buď	k8xC
mechanicky	mechanicky	k6eAd1
nebo	nebo	k8xC
opticky	opticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
mechanického	mechanický	k2eAgInSc2d1
principu	princip	k1gInSc2
byla	být	k5eAaImAgFnS
pod	pod	k7c7
děrnou	děrný	k2eAgFnSc7d1
páskou	páska	k1gFnSc7
kovová	kovový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
a	a	k8xC
nad	nad	k7c7
děrnou	děrný	k2eAgFnSc7d1
páskou	páska	k1gFnSc7
byly	být	k5eAaImAgInP
umístěny	umístit	k5eAaPmNgInP
kovové	kovový	k2eAgInPc1d1
kartáčky	kartáček	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
klouzaly	klouzat	k5eAaImAgInP
po	po	k7c6
povrchu	povrch	k1gInSc6
pásky	páska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
pohyb	pohyb	k1gInSc1
pásky	páska	k1gFnSc2
a	a	k8xC
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
měla	mít	k5eAaImAgFnS
páska	páska	k1gFnSc1
dírku	dírka	k1gFnSc4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vodivému	vodivý	k2eAgNnSc3d1
propojení	propojení	k1gNnSc3
kartáčků	kartáček	k1gInPc2
s	s	k7c7
podkladem	podklad	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vydalo	vydat	k5eAaPmAgNnS
elektrický	elektrický	k2eAgInSc4d1
impulz	impulz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
optického	optický	k2eAgInSc2d1
principu	princip	k1gInSc2
byla	být	k5eAaImAgFnS
páska	páska	k1gFnSc1
prosvětlována	prosvětlovat	k5eAaImNgFnS
tenkým	tenký	k2eAgInSc7d1
paprskem	paprsek	k1gInSc7
světla	světlo	k1gNnSc2
a	a	k8xC
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
dírky	dírka	k1gFnPc1
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
světelnému	světelný	k2eAgInSc3d1
impulzu	impulz	k1gInSc3
na	na	k7c6
snímací	snímací	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
pod	pod	k7c7
páskou	páska	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vydalo	vydat	k5eAaPmAgNnS
elektrický	elektrický	k2eAgInSc4d1
impulz	impulz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Oba	dva	k4xCgInPc1
způsoby	způsob	k1gInPc1
čtení	čtení	k1gNnSc2
byly	být	k5eAaImAgFnP
náchylné	náchylný	k2eAgFnPc1d1
k	k	k7c3
chybám	chyba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
mechanického	mechanický	k2eAgInSc2d1
principu	princip	k1gInSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
opotřebení	opotřebení	k1gNnSc3
kartáčků	kartáček	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
pak	pak	k6eAd1
při	při	k7c6
vyšší	vysoký	k2eAgFnSc6d2
rychlosti	rychlost	k1gFnSc6
pohybu	pohyb	k1gInSc2
pásky	páska	k1gFnSc2
nestačily	stačit	k5eNaBmAgFnP
reagovat	reagovat	k5eAaBmF
a	a	k8xC
signál	signál	k1gInSc1
oznamující	oznamující	k2eAgInSc1d1
dírku	dírka	k1gFnSc4
v	v	k7c6
pásce	páska	k1gFnSc6
byl	být	k5eAaImAgInS
někdy	někdy	k6eAd1
vynechán	vynechat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
optického	optický	k2eAgInSc2d1
principu	princip	k1gInSc2
se	se	k3xPyFc4
zase	zase	k9
stávalo	stávat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
méně	málo	k6eAd2
kvalitní	kvalitní	k2eAgInSc1d1
papír	papír	k1gInSc1
pásky	páska	k1gFnSc2
propouštěl	propouštět	k5eAaImAgInS
světlo	světlo	k1gNnSc4
i	i	k8xC
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebyla	být	k5eNaImAgFnS
dírka	dírka	k1gFnSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
vydán	vydán	k2eAgInSc4d1
falešný	falešný	k2eAgInSc4d1
signál	signál	k1gInSc4
o	o	k7c6
poloze	poloha	k1gFnSc6
dírky	dírka	k1gFnSc2
na	na	k7c6
pásce	páska	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
děrné	děrný	k2eAgFnSc2d1
pásky	páska	k1gFnSc2
běžně	běžně	k6eAd1
sloužily	sloužit	k5eAaImAgInP
k	k	k7c3
dálkovému	dálkový	k2eAgNnSc3d1
přenosu	přenos	k1gInSc2
binárních	binární	k2eAgNnPc2d1
dat	datum	k1gNnPc2
pomocí	pomocí	k7c2
dálnopisu	dálnopis	k1gInSc2
a	a	k8xC
ke	k	k7c3
vkládání	vkládání	k1gNnSc3
dat	datum	k1gNnPc2
do	do	k7c2
sálových	sálový	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
mezinárodní	mezinárodní	k2eAgInSc1d1
telegrafní	telegrafní	k2eAgInSc1d1
kód	kód	k1gInSc1
na	na	k7c6
pětistopé	pětistopý	k2eAgFnSc6d1
děrné	děrný	k2eAgFnSc6d1
pásce	páska	k1gFnSc6
</s>
<s>
Děrná	děrný	k2eAgFnSc1d1
páska	páska	k1gFnSc1
<g/>
,	,	kIx,
nerozbalená	rozbalený	k2eNgFnSc1d1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dálnopis	dálnopis	k1gInSc1
</s>
<s>
Děrný	děrný	k2eAgInSc1d1
štítek	štítek	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4122264-7	4122264-7	k4
</s>
