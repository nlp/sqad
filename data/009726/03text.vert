<p>
<s>
Ethylcinnamát	Ethylcinnamát	k1gInSc1	Ethylcinnamát
(	(	kIx(	(
<g/>
též	též	k9	též
ethylester	ethylester	k1gInSc1	ethylester
kyseliny	kyselina	k1gFnSc2	kyselina
skořicové	skořicová	k1gFnSc2	skořicová
či	či	k8xC	či
ethyl-	ethyl-	k?	ethyl-
<g/>
3	[number]	k4	3
<g/>
-fenyl-	enyl-	k?	-fenyl-
<g/>
2	[number]	k4	2
<g/>
-propenát	ropenát	k1gInSc1	-propenát
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ester	ester	k1gInSc1	ester
kyseliny	kyselina	k1gFnSc2	kyselina
skořicové	skořicová	k1gFnSc2	skořicová
a	a	k8xC	a
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
sumární	sumární	k2eAgInSc4d1	sumární
vzorec	vzorec	k1gInSc4	vzorec
je	být	k5eAaImIp3nS	být
C	C	kA	C
<g/>
11	[number]	k4	11
<g/>
H	H	kA	H
<g/>
12	[number]	k4	12
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
esenciálním	esenciální	k2eAgInSc6d1	esenciální
oleji	olej	k1gInSc6	olej
skořice	skořice	k1gFnSc2	skořice
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
ethylcinnamát	ethylcinnamát	k1gInSc1	ethylcinnamát
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
ovocnou	ovocný	k2eAgFnSc4d1	ovocná
a	a	k8xC	a
balzámovou	balzámový	k2eAgFnSc4d1	balzámová
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
skořici	skořice	k1gFnSc4	skořice
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
jantaru	jantar	k1gInSc2	jantar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
p-methoxy	pethox	k1gInPc4	p-methox
substitut	substitut	k1gInSc1	substitut
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
inhibitor	inhibitor	k1gInSc4	inhibitor
monoaminoxidázy	monoaminoxidáza	k1gFnSc2	monoaminoxidáza
(	(	kIx(	(
<g/>
MAOI	MAOI	kA	MAOI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
skořicová	skořicový	k2eAgFnSc1d1	skořicová
</s>
</p>
<p>
<s>
Cinnamaldehyd	Cinnamaldehyd	k6eAd1	Cinnamaldehyd
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ethyl	ethyl	k1gInSc1	ethyl
cinnamate	cinnamat	k1gInSc5	cinnamat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
