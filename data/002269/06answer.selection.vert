<s>
Kanadský	kanadský	k2eAgInSc1d1	kanadský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
z	z	k7c2	z
volené	volený	k2eAgFnSc2d1	volená
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmenovaného	jmenovaný	k2eAgInSc2d1	jmenovaný
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
