<s>
Buzola	buzola	k1gFnSc1	buzola
(	(	kIx(	(
<g/>
také	také	k9	také
busola	busola	k1gFnSc1	busola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
přístroj	přístroj	k1gInSc4	přístroj
pro	pro	k7c4	pro
orientaci	orientace	k1gFnSc4	orientace
a	a	k8xC	a
navigaci	navigace	k1gFnSc4	navigace
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
buzoly	buzola	k1gFnSc2	buzola
je	být	k5eAaImIp3nS	být
kompas	kompas	k1gInSc4	kompas
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
doplněný	doplněný	k2eAgInSc1d1	doplněný
otočným	otočný	k2eAgInSc7d1	otočný
úhloměrem	úhloměr	k1gInSc7	úhloměr
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
azimutu	azimut	k1gInSc2	azimut
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
buzolu	buzola	k1gFnSc4	buzola
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
český	český	k2eAgMnSc1d1	český
vynálezce	vynálezce	k1gMnSc1	vynálezce
Josef	Josef	k1gMnSc1	Josef
Ressel	Ressel	k1gMnSc1	Ressel
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnSc1d1	univerzální
buzola	buzola	k1gFnSc1	buzola
má	mít	k5eAaImIp3nS	mít
otočný	otočný	k2eAgInSc4d1	otočný
kompas	kompas	k1gInSc4	kompas
upevněný	upevněný	k2eAgInSc4d1	upevněný
na	na	k7c6	na
průhledné	průhledný	k2eAgFnSc6d1	průhledná
plastové	plastový	k2eAgFnSc6d1	plastová
destičce	destička	k1gFnSc6	destička
a	a	k8xC	a
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
výklopným	výklopný	k2eAgNnSc7d1	výklopné
zrcátkem	zrcátko	k1gNnSc7	zrcátko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
speciální	speciální	k2eAgFnPc1d1	speciální
buzoly	buzola	k1gFnPc1	buzola
či	či	k8xC	či
kompasy	kompas	k1gInPc1	kompas
pro	pro	k7c4	pro
specifické	specifický	k2eAgNnSc4d1	specifické
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1	speciální
buzoly	buzola	k1gFnPc1	buzola
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
orientační	orientační	k2eAgInSc4d1	orientační
běh	běh	k1gInSc4	běh
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
orientační	orientační	k2eAgInPc4d1	orientační
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rychlé	rychlý	k2eAgNnSc4d1	rychlé
ustálení	ustálení	k1gNnSc4	ustálení
střelky	střelka	k1gFnSc2	střelka
kompasu	kompas	k1gInSc2	kompas
<g/>
.	.	kIx.	.
</s>
<s>
Specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
buzola	buzola	k1gFnSc1	buzola
má	mít	k5eAaImIp3nS	mít
úchyt	úchyt	k1gInSc4	úchyt
na	na	k7c4	na
palec	palec	k1gInSc4	palec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
přidržuje	přidržovat	k5eAaImIp3nS	přidržovat
mapu	mapa	k1gFnSc4	mapa
<g/>
,	,	kIx,	,
a	a	k8xC	a
postrádá	postrádat	k5eAaImIp3nS	postrádat
azimutální	azimutální	k2eAgFnSc4d1	azimutální
stupnici	stupnice	k1gFnSc4	stupnice
<g/>
,	,	kIx,	,
azimut	azimut	k1gInSc1	azimut
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
mapy	mapa	k1gFnSc2	mapa
do	do	k7c2	do
terénu	terén	k1gInSc2	terén
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
odečítání	odečítání	k1gNnSc2	odečítání
stupňové	stupňový	k2eAgFnSc2d1	stupňová
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
jsou	být	k5eAaImIp3nP	být
buzoly	buzola	k1gFnPc4	buzola
konstruovány	konstruován	k2eAgFnPc4d1	konstruována
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
odolnost	odolnost	k1gFnSc4	odolnost
a	a	k8xC	a
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
jiné	jiný	k2eAgNnSc4d1	jiné
dělení	dělení	k1gNnSc4	dělení
obzoru	obzor	k1gInSc2	obzor
než	než	k8xS	než
azimutové	azimutový	k2eAgInPc4d1	azimutový
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
6	[number]	k4	6
400	[number]	k4	400
mils	milsa	k1gFnPc2	milsa
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
používané	používaný	k2eAgInPc1d1	používaný
pro	pro	k7c4	pro
zaměřování	zaměřování	k1gNnSc4	zaměřování
cílů	cíl	k1gInPc2	cíl
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
a	a	k8xC	a
odvozené	odvozený	k2eAgInPc4d1	odvozený
od	od	k7c2	od
miliradiánů	miliradián	k1gInPc2	miliradián
<g/>
,	,	kIx,	,
tisícin	tisícina	k1gFnPc2	tisícina
radiánu	radián	k1gInSc2	radián
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1	speciální
buzoly	buzola	k1gFnPc1	buzola
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
geodeti	geodet	k1gMnPc1	geodet
a	a	k8xC	a
kartografové	kartograf	k1gMnPc1	kartograf
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
precizní	precizní	k2eAgNnSc4d1	precizní
provedení	provedení	k1gNnSc4	provedení
s	s	k7c7	s
optickým	optický	k2eAgInSc7d1	optický
zaměřovačem	zaměřovač	k1gInSc7	zaměřovač
a	a	k8xC	a
přesností	přesnost	k1gFnSc7	přesnost
0,5	[number]	k4	0,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dělení	dělení	k1gNnSc2	dělení
azimutu	azimut	k1gInSc2	azimut
na	na	k7c4	na
360	[number]	k4	360
<g/>
°	°	k?	°
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
400	[number]	k4	400
gradů	grad	k1gInPc2	grad
(	(	kIx(	(
<g/>
400	[number]	k4	400
gon	gon	k?	gon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
buzoly	buzola	k1gFnSc2	buzola
je	být	k5eAaImIp3nS	být
kompas	kompas	k1gInSc4	kompas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
střelkou	střelka	k1gFnSc7	střelka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
severní	severní	k2eAgInSc4d1	severní
magnetický	magnetický	k2eAgInSc4d1	magnetický
pól	pól	k1gInSc4	pól
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Střelka	střelka	k1gFnSc1	střelka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyvážena	vyvážit	k5eAaPmNgFnS	vyvážit
podle	podle	k7c2	podle
magnetické	magnetický	k2eAgFnSc2d1	magnetická
inklinace	inklinace	k1gFnSc2	inklinace
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
bude	být	k5eAaImBp3nS	být
používána	používán	k2eAgFnSc1d1	používána
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
optimální	optimální	k2eAgFnSc4d1	optimální
funkci	funkce	k1gFnSc4	funkce
střelky	střelka	k1gFnSc2	střelka
mají	mít	k5eAaImIp3nP	mít
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
buzoly	buzola	k1gFnPc1	buzola
její	její	k3xOp3gFnSc4	její
osu	osa	k1gFnSc4	osa
uloženou	uložený	k2eAgFnSc4d1	uložená
do	do	k7c2	do
obvykle	obvykle	k6eAd1	obvykle
safírových	safírový	k2eAgNnPc2d1	safírové
ložisek	ložisko	k1gNnPc2	ložisko
a	a	k8xC	a
pouzdro	pouzdro	k1gNnSc1	pouzdro
naplněné	naplněný	k2eAgNnSc1d1	naplněné
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
pohyb	pohyb	k1gInSc4	pohyb
střelky	střelka	k1gFnSc2	střelka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
její	její	k3xOp3gNnPc4	její
ustálení	ustálení	k1gNnPc4	ustálení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
otočného	otočný	k2eAgNnSc2d1	otočné
pouzdra	pouzdro	k1gNnSc2	pouzdro
kompasu	kompas	k1gInSc2	kompas
je	být	k5eAaImIp3nS	být
branka	branka	k1gFnSc1	branka
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
střelka	střelka	k1gFnSc1	střelka
natočením	natočení	k1gNnSc7	natočení
buzoly	buzola	k1gFnSc2	buzola
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
používání	používání	k1gNnSc6	používání
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
branky	branka	k1gFnSc2	branka
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
stupnice	stupnice	k1gFnSc1	stupnice
magnetické	magnetický	k2eAgFnSc2d1	magnetická
deklinace	deklinace	k1gFnSc2	deklinace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
se	s	k7c7	s
znalostí	znalost	k1gFnSc7	znalost
aktuální	aktuální	k2eAgFnSc2d1	aktuální
magnetické	magnetický	k2eAgFnSc2d1	magnetická
deklinace	deklinace	k1gFnSc2	deklinace
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
geografickou	geografický	k2eAgFnSc4d1	geografická
polohu	poloha	k1gFnSc4	poloha
určení	určení	k1gNnSc2	určení
skutečného	skutečný	k2eAgNnSc2d1	skutečné
(	(	kIx(	(
<g/>
geografického	geografický	k2eAgMnSc2d1	geografický
<g/>
)	)	kIx)	)
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
buzoly	buzola	k1gFnPc1	buzola
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
nastavení	nastavení	k1gNnSc4	nastavení
korekce	korekce	k1gFnSc2	korekce
deklinace	deklinace	k1gFnSc2	deklinace
pootočením	pootočení	k1gNnSc7	pootočení
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přibližnou	přibližný	k2eAgFnSc4d1	přibližná
orientaci	orientace	k1gFnSc4	orientace
(	(	kIx(	(
<g/>
s	s	k7c7	s
odchylkou	odchylka	k1gFnSc7	odchylka
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
několika	několik	k4yIc2	několik
málo	málo	k4c1	málo
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
lze	lze	k6eAd1	lze
deklinaci	deklinace	k1gFnSc4	deklinace
pominout	pominout	k5eAaPmF	pominout
<g/>
.	.	kIx.	.
</s>
<s>
Otočné	otočný	k2eAgNnSc1d1	otočné
pouzdro	pouzdro	k1gNnSc1	pouzdro
kompasu	kompas	k1gInSc2	kompas
je	být	k5eAaImIp3nS	být
opatřeno	opatřit	k5eAaPmNgNnS	opatřit
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
stupnicí	stupnice	k1gFnSc7	stupnice
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
odečíst	odečíst	k5eAaPmF	odečíst
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
svírají	svírat	k5eAaImIp3nP	svírat
rysky	ryska	k1gFnPc4	ryska
na	na	k7c6	na
otočném	otočný	k2eAgInSc6d1	otočný
kotouči	kotouč	k1gInSc6	kotouč
s	s	k7c7	s
hranou	hrana	k1gFnSc7	hrana
pevné	pevný	k2eAgFnSc2d1	pevná
destičky	destička	k1gFnSc2	destička
buzoly	buzola	k1gFnSc2	buzola
<g/>
.	.	kIx.	.
</s>
<s>
Azimut	azimut	k1gInSc1	azimut
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
buzoly	buzola	k1gFnSc2	buzola
natočí	natočit	k5eAaBmIp3nS	natočit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
startovního	startovní	k2eAgMnSc2d1	startovní
do	do	k7c2	do
cílového	cílový	k2eAgInSc2d1	cílový
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
kotouč	kotouč	k1gInSc1	kotouč
pootočí	pootočit	k5eAaPmIp3nS	pootočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rysky	ryska	k1gFnPc1	ryska
směřovaly	směřovat	k5eAaImAgFnP	směřovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mapě	mapa	k1gFnSc6	mapa
se	se	k3xPyFc4	se
rysky	ryska	k1gFnPc1	ryska
srovnávají	srovnávat	k5eAaImIp3nP	srovnávat
s	s	k7c7	s
poledníky	poledník	k1gInPc7	poledník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
vyznačené	vyznačený	k2eAgFnSc6d1	vyznačená
<g/>
,	,	kIx,	,
a	a	k8xC	a
hrana	hrana	k1gFnSc1	hrana
destičky	destička	k1gFnSc2	destička
se	s	k7c7	s
spojnicí	spojnice	k1gFnSc7	spojnice
startu	start	k1gInSc2	start
a	a	k8xC	a
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terénu	terén	k1gInSc6	terén
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
azimut	azimut	k1gInSc1	azimut
obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kotouč	kotouč	k1gInSc1	kotouč
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nasměrovat	nasměrovat	k5eAaPmF	nasměrovat
dle	dle	k7c2	dle
kompasové	kompasový	k2eAgFnSc2d1	kompasová
střelky	střelka	k1gFnSc2	střelka
a	a	k8xC	a
destička	destička	k1gFnSc1	destička
buzoly	buzola	k1gFnSc2	buzola
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
cíli	cíl	k1gInSc3	cíl
(	(	kIx(	(
<g/>
orientačnímu	orientační	k2eAgNnSc3d1	orientační
bodu	bod	k1gInSc3	bod
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
dle	dle	k7c2	dle
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
azimutu	azimut	k1gInSc2	azimut
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zjištěného	zjištěný	k2eAgInSc2d1	zjištěný
v	v	k7c6	v
mapě	mapa	k1gFnSc6	mapa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
změřit	změřit	k5eAaPmF	změřit
azimut	azimut	k1gInSc4	azimut
orientačního	orientační	k2eAgInSc2d1	orientační
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
azimutu	azimut	k1gInSc2	azimut
mezi	mezi	k7c7	mezi
mapou	mapa	k1gFnSc7	mapa
a	a	k8xC	a
terénem	terén	k1gInSc7	terén
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zohlednit	zohlednit	k5eAaPmF	zohlednit
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
deklinaci	deklinace	k1gFnSc4	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
buzoly	buzola	k1gFnPc1	buzola
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
kalibraci	kalibrace	k1gFnSc4	kalibrace
otočného	otočný	k2eAgInSc2d1	otočný
kotouče	kotouč	k1gInSc2	kotouč
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
korekci	korekce	k1gFnSc4	korekce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesnější	přesný	k2eAgNnSc4d2	přesnější
určení	určení	k1gNnSc4	určení
azimutu	azimut	k1gInSc2	azimut
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
buzoly	buzola	k1gFnPc1	buzola
opatřeny	opatřen	k2eAgFnPc1d1	opatřena
průzorovým	průzorův	k2eAgInSc7d1	průzorův
hledáčkem	hledáček	k1gInSc7	hledáček
a	a	k8xC	a
výklopným	výklopný	k2eAgNnSc7d1	výklopné
zrcátkem	zrcátko	k1gNnSc7	zrcátko
s	s	k7c7	s
ryskou	ryska	k1gFnSc7	ryska
<g/>
.	.	kIx.	.
</s>
<s>
Hledáček	hledáček	k1gInSc1	hledáček
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přesněji	přesně	k6eAd2	přesně
zaměřit	zaměřit	k5eAaPmF	zaměřit
cílový	cílový	k2eAgInSc4d1	cílový
bod	bod	k1gInSc4	bod
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
současně	současně	k6eAd1	současně
při	při	k7c6	při
zaměřování	zaměřování	k1gNnSc6	zaměřování
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
udržení	udržení	k1gNnSc1	udržení
střelky	střelka	k1gFnSc2	střelka
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
správné	správný	k2eAgNnSc1d1	správné
natočení	natočení	k1gNnSc1	natočení
otočného	otočný	k2eAgInSc2d1	otočný
kotouče	kotouč	k1gInSc2	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Buzola	buzola	k1gFnSc1	buzola
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
hranách	hrana	k1gFnPc6	hrana
opatřena	opatřit	k5eAaPmNgFnS	opatřit
pravítky	pravítko	k1gNnPc7	pravítko
pro	pro	k7c4	pro
odčítání	odčítání	k1gNnSc4	odčítání
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
různých	různý	k2eAgFnPc2d1	různá
měřítek	měřítko	k1gNnPc2	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
obvyklým	obvyklý	k2eAgFnPc3d1	obvyklá
součástem	součást	k1gFnPc3	součást
patří	patřit	k5eAaImIp3nS	patřit
zvětšovací	zvětšovací	k2eAgFnSc1d1	zvětšovací
lupa	lupa	k1gFnSc1	lupa
a	a	k8xC	a
šablony	šablona	k1gFnPc1	šablona
pro	pro	k7c4	pro
vyznačování	vyznačování	k1gNnSc4	vyznačování
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Buzoly	buzola	k1gFnPc1	buzola
také	také	k6eAd1	také
bývají	bývat	k5eAaImIp3nP	bývat
opatřeny	opatřit	k5eAaPmNgFnP	opatřit
popruhem	popruh	k1gInSc7	popruh
k	k	k7c3	k
zavěšení	zavěšení	k1gNnSc3	zavěšení
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
a	a	k8xC	a
fosforescenčními	fosforescenční	k2eAgInPc7d1	fosforescenční
prvky	prvek	k1gInPc7	prvek
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
za	za	k7c2	za
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
Buzola	buzola	k1gFnSc1	buzola
se	se	k3xPyFc4	se
nasměruje	nasměrovat	k5eAaPmIp3nS	nasměrovat
k	k	k7c3	k
měřenému	měřený	k2eAgInSc3d1	měřený
bodu	bod	k1gInSc3	bod
<g/>
.	.	kIx.	.
</s>
<s>
Otočným	otočný	k2eAgInSc7d1	otočný
kotoučem	kotouč	k1gInSc7	kotouč
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
střelka	střelka	k1gFnSc1	střelka
byla	být	k5eAaImAgFnS	být
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
ryskami	ryska	k1gFnPc7	ryska
kotouče	kotouč	k1gInPc4	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Pevný	pevný	k2eAgInSc1d1	pevný
ukazatel	ukazatel	k1gInSc1	ukazatel
buzoly	buzola	k1gFnSc2	buzola
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
stupnici	stupnice	k1gFnSc4	stupnice
otočného	otočný	k2eAgInSc2d1	otočný
kotouče	kotouč	k1gInSc2	kotouč
hodnotu	hodnota	k1gFnSc4	hodnota
azimutu	azimut	k1gInSc2	azimut
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
buzol	buzola	k1gFnPc2	buzola
s	s	k7c7	s
průzorem	průzor	k1gInSc7	průzor
a	a	k8xC	a
zrcátkem	zrcátko	k1gNnSc7	zrcátko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přesněji	přesně	k6eAd2	přesně
zaměřit	zaměřit	k5eAaPmF	zaměřit
bod	bod	k1gInSc4	bod
pomocí	pomocí	k7c2	pomocí
průzoru	průzor	k1gInSc2	průzor
a	a	k8xC	a
přesněji	přesně	k6eAd2	přesně
natočit	natočit	k5eAaBmF	natočit
kotouč	kotouč	k1gInSc4	kotouč
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
ryskou	ryska	k1gFnSc7	ryska
pohledem	pohled	k1gInSc7	pohled
do	do	k7c2	do
zrcátka	zrcátko	k1gNnSc2	zrcátko
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
zorientována	zorientován	k2eAgFnSc1d1	zorientována
<g/>
.	.	kIx.	.
</s>
<s>
Buzola	buzola	k1gFnSc1	buzola
se	se	k3xPyFc4	se
na	na	k7c4	na
mapu	mapa	k1gFnSc4	mapa
přiloží	přiložit	k5eAaPmIp3nS	přiložit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
okraj	okraj	k1gInSc1	okraj
spojoval	spojovat	k5eAaImAgInS	spojovat
výchozí	výchozí	k2eAgNnSc4d1	výchozí
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Otočný	otočný	k2eAgInSc1d1	otočný
kotouč	kotouč	k1gInSc1	kotouč
na	na	k7c6	na
buzole	buzola	k1gFnSc6	buzola
se	se	k3xPyFc4	se
natočí	natočit	k5eAaBmIp3nS	natočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rysky	ryska	k1gFnSc2	ryska
kotouče	kotouč	k1gInSc2	kotouč
shodovaly	shodovat	k5eAaImAgInP	shodovat
s	s	k7c7	s
mřížkou	mřížka	k1gFnSc7	mřížka
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
a	a	k8xC	a
sever	sever	k1gInSc1	sever
na	na	k7c6	na
otočném	otočný	k2eAgInSc6d1	otočný
kotouči	kotouč	k1gInSc6	kotouč
mířil	mířit	k5eAaImAgMnS	mířit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Pevný	pevný	k2eAgInSc1d1	pevný
ukazatel	ukazatel	k1gInSc1	ukazatel
buzoly	buzola	k1gFnSc2	buzola
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
stupnici	stupnice	k1gFnSc4	stupnice
otočného	otočný	k2eAgInSc2d1	otočný
kotouče	kotouč	k1gInSc2	kotouč
hodnotu	hodnota	k1gFnSc4	hodnota
azimutu	azimut	k1gInSc2	azimut
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
krátké	krátký	k2eAgFnSc3d1	krátká
délce	délka	k1gFnSc3	délka
rysek	ryska	k1gFnPc2	ryska
kotouče	kotouč	k1gInSc2	kotouč
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
méně	málo	k6eAd2	málo
přesnému	přesný	k2eAgNnSc3d1	přesné
nastavení	nastavení	k1gNnSc3	nastavení
severu	sever	k1gInSc2	sever
na	na	k7c6	na
buzole	buzola	k1gFnSc6	buzola
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
poněkud	poněkud	k6eAd1	poněkud
přesnějšího	přesný	k2eAgInSc2d2	přesnější
odečtu	odečet	k1gInSc2	odečet
azimutu	azimut	k1gInSc2	azimut
z	z	k7c2	z
mapy	mapa	k1gFnSc2	mapa
jejím	její	k3xOp3gNnSc7	její
zorientováním	zorientování	k1gNnSc7	zorientování
na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
podložce	podložka	k1gFnSc6	podložka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sever	sever	k1gInSc1	sever
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
severu	sever	k1gInSc2	sever
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
přiloží	přiložit	k5eAaPmIp3nS	přiložit
buzola	buzola	k1gFnSc1	buzola
na	na	k7c4	na
mapu	mapa	k1gFnSc4	mapa
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
k	k	k7c3	k
trase	trasa	k1gFnSc3	trasa
pochodu	pochod	k1gInSc2	pochod
a	a	k8xC	a
kotouč	kotouč	k1gInSc1	kotouč
natočí	natočit	k5eAaBmIp3nS	natočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sever	sever	k1gInSc1	sever
na	na	k7c6	na
kotouči	kotouč	k1gInSc6	kotouč
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
severu	sever	k1gInSc2	sever
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
a	a	k8xC	a
severu	sever	k1gInSc6	sever
na	na	k7c6	na
rysce	ryska	k1gFnSc6	ryska
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgInSc2	tento
pracnějšího	pracný	k2eAgInSc2d2	pracnější
postupu	postup	k1gInSc2	postup
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zorientování	zorientování	k1gNnSc6	zorientování
mapy	mapa	k1gFnSc2	mapa
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloha	poloha	k1gFnSc1	poloha
terénních	terénní	k2eAgInPc2d1	terénní
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
jejich	jejich	k3xOp3gInSc2	jejich
směru	směr	k1gInSc2	směr
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
se	se	k3xPyFc4	se
změří	změřit	k5eAaPmIp3nS	změřit
azimut	azimut	k1gInSc1	azimut
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Kotouč	kotouč	k1gInSc1	kotouč
buzoly	buzola	k1gFnSc2	buzola
se	se	k3xPyFc4	se
ponechá	ponechat	k5eAaPmIp3nS	ponechat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pochodu	pochod	k1gInSc2	pochod
nastaven	nastavit	k5eAaPmNgInS	nastavit
na	na	k7c4	na
změřený	změřený	k2eAgInSc4d1	změřený
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Nastavená	nastavený	k2eAgFnSc1d1	nastavená
buzola	buzola	k1gFnSc1	buzola
se	se	k3xPyFc4	se
vezme	vzít	k5eAaPmIp3nS	vzít
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc7d1	celá
buzolou	buzola	k1gFnSc7	buzola
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
střelka	střelka	k1gFnSc1	střelka
byla	být	k5eAaImAgFnS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
ryskami	ryska	k1gFnPc7	ryska
kotouče	kotouč	k1gInPc4	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nastaví	nastavět	k5eAaBmIp3nS	nastavět
<g/>
,	,	kIx,	,
podélná	podélný	k2eAgFnSc1d1	podélná
osa	osa	k1gFnSc1	osa
buzoly	buzola	k1gFnSc2	buzola
(	(	kIx(	(
<g/>
a	a	k8xC	a
průzor	průzor	k1gInSc1	průzor
<g/>
)	)	kIx)	)
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směru	směr	k1gInSc6	směr
buzoly	buzola	k1gFnSc2	buzola
se	se	k3xPyFc4	se
v	v	k7c6	v
průzoru	průzor	k1gInSc6	průzor
najde	najít	k5eAaPmIp3nS	najít
tzv.	tzv.	kA	tzv.
dílčí	dílčí	k2eAgInSc1d1	dílčí
cíl	cíl	k1gInSc1	cíl
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
nastavenou	nastavený	k2eAgFnSc7d1	nastavená
buzolou	buzola	k1gFnSc7	buzola
určí	určit	k5eAaPmIp3nS	určit
další	další	k2eAgInSc1d1	další
dílčí	dílčí	k2eAgInSc1d1	dílčí
cíl	cíl	k1gInSc1	cíl
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hustá	hustý	k2eAgFnSc1d1	hustá
mlha	mlha	k1gFnSc1	mlha
a	a	k8xC	a
dílčí	dílčí	k2eAgInPc1d1	dílčí
cíle	cíl	k1gInPc1	cíl
nejsou	být	k5eNaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pochodovat	pochodovat	k5eAaImF	pochodovat
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
neustále	neustále	k6eAd1	neustále
sledovat	sledovat	k5eAaImF	sledovat
buzolu	buzola	k1gFnSc4	buzola
a	a	k8xC	a
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
střelka	střelka	k1gFnSc1	střelka
stále	stále	k6eAd1	stále
udržovala	udržovat	k5eAaImAgFnS	udržovat
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
ryskami	ryska	k1gFnPc7	ryska
kotouče	kotouč	k1gInSc2	kotouč
a	a	k8xC	a
hlídat	hlídat	k5eAaImF	hlídat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
buzola	buzola	k1gFnSc1	buzola
byla	být	k5eAaImAgFnS	být
vodorovně	vodorovně	k6eAd1	vodorovně
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zorientuje	zorientovat	k5eAaPmIp3nS	zorientovat
k	k	k7c3	k
severu	sever	k1gInSc3	sever
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kompasem	kompas	k1gInSc7	kompas
zjistí	zjistit	k5eAaPmIp3nS	zjistit
sever	sever	k1gInSc4	sever
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
nastaví	nastavit	k5eAaPmIp3nP	nastavit
i	i	k9	i
sever	sever	k1gInSc4	sever
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
vrchní	vrchní	k2eAgInSc1d1	vrchní
okraj	okraj	k1gInSc1	okraj
<g/>
)	)	kIx)	)
mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozhledem	rozhled	k1gInSc7	rozhled
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
zvolí	zvolit	k5eAaPmIp3nS	zvolit
nápadné	nápadný	k2eAgInPc4d1	nápadný
krajinné	krajinný	k2eAgInPc4d1	krajinný
body	bod	k1gInPc4	bod
(	(	kIx(	(
<g/>
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejdou	jít	k5eNaImIp3nP	jít
zaměnit	zaměnit	k5eAaPmF	zaměnit
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
buzola	buzola	k1gFnSc1	buzola
nasměruje	nasměrovat	k5eAaPmIp3nS	nasměrovat
průzorem	průzor	k1gInSc7	průzor
k	k	k7c3	k
nápadnému	nápadný	k2eAgInSc3d1	nápadný
bodu	bod	k1gInSc3	bod
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Otočný	otočný	k2eAgInSc1d1	otočný
kotouč	kotouč	k1gInSc1	kotouč
se	se	k3xPyFc4	se
natočí	natočit	k5eAaBmIp3nS	natočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
střelka	střelka	k1gFnSc1	střelka
byla	být	k5eAaImAgFnS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
ryskami	ryska	k1gFnPc7	ryska
kotouče	kotouč	k1gInPc4	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
buzola	buzola	k1gFnSc1	buzola
položí	položit	k5eAaPmIp3nS	položit
na	na	k7c4	na
mapu	mapa	k1gFnSc4	mapa
<g/>
,	,	kIx,	,
rysky	ryska	k1gFnPc4	ryska
otočného	otočný	k2eAgInSc2d1	otočný
kotouče	kotouč	k1gInSc2	kotouč
se	se	k3xPyFc4	se
musejí	muset	k5eAaImIp3nP	muset
krýt	krýt	k5eAaImF	krýt
s	s	k7c7	s
mřížkou	mřížka	k1gFnSc7	mřížka
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
sever	sever	k1gInSc4	sever
na	na	k7c6	na
otočném	otočný	k2eAgInSc6d1	otočný
kotouči	kotouč	k1gInSc6	kotouč
musí	muset	k5eAaImIp3nS	muset
směřovat	směřovat	k5eAaImF	směřovat
k	k	k7c3	k
severu	sever	k1gInSc3	sever
mapy	mapa	k1gFnSc2	mapa
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
okraj	okraj	k1gInSc4	okraj
buzoly	buzola	k1gFnSc2	buzola
musí	muset	k5eAaImIp3nS	muset
protínat	protínat	k5eAaImF	protínat
nápadný	nápadný	k2eAgInSc1d1	nápadný
krajinný	krajinný	k2eAgInSc1d1	krajinný
bod	bod	k1gInSc1	bod
zobrazený	zobrazený	k2eAgInSc1d1	zobrazený
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
tohoto	tento	k3xDgInSc2	tento
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
okraje	okraj	k1gInSc2	okraj
buzoly	buzola	k1gFnSc2	buzola
se	se	k3xPyFc4	se
tužkou	tužka	k1gFnSc7	tužka
narýsuje	narýsovat	k5eAaPmIp3nS	narýsovat
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
nápadným	nápadný	k2eAgInSc7d1	nápadný
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přímky	přímka	k1gFnPc1	přímka
protínají	protínat	k5eAaImIp3nP	protínat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
je	být	k5eAaImIp3nS	být
zjišťování	zjišťování	k1gNnSc1	zjišťování
polohy	poloha	k1gFnSc2	poloha
prováděno	prováděn	k2eAgNnSc1d1	prováděno
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
busola	busola	k1gFnSc1	busola
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Návod	návod	k1gInSc1	návod
na	na	k7c6	na
webu	web	k1gInSc6	web
KVH	KVH	kA	KVH
Návod	návod	k1gInSc1	návod
na	na	k7c6	na
webu	web	k1gInSc6	web
hasičů	hasič	k1gMnPc2	hasič
</s>
