<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
však	však	k9	však
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
jazyk	jazyk	k1gInSc1	jazyk
Ainu	Ainus	k1gInSc2	Ainus
nezná	neznat	k5eAaImIp3nS	neznat
psanou	psaný	k2eAgFnSc4d1	psaná
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
lid	lid	k1gInSc4	lid
Ainu	Ainus	k1gInSc2	Ainus
nepoužíval	používat	k5eNaImAgMnS	používat
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ústní	ústní	k2eAgFnSc4d1	ústní
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
heslo	heslo	k1gNnSc4	heslo
Yukar	Yukara	k1gFnPc2	Yukara
<g/>
.	.	kIx.	.
</s>
