<s>
Polka	Polka	k1gFnSc1	Polka
je	být	k5eAaImIp3nS	být
společenský	společenský	k2eAgInSc1d1	společenský
tanec	tanec	k1gInSc1	tanec
v	v	k7c6	v
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
taktu	takt	k1gInSc2	takt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
výrazem	výraz	k1gInSc7	výraz
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rytmu	rytmus	k1gInSc6	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
polky	polka	k1gFnSc2	polka
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasno	jasno	k6eAd1	jasno
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
národopisce	národopisec	k1gMnSc2	národopisec
Čeňka	Čeněk	k1gMnSc2	Čeněk
Zíbrta	Zíbrta	k1gFnSc1	Zíbrta
polku	polka	k1gFnSc4	polka
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Strejček	Strejček	k1gMnSc1	Strejček
Nimra	nimra	k1gFnSc1	nimra
děvečka	děvečka	k1gFnSc1	děvečka
Anna	Anna	k1gFnSc1	Anna
Chadimová	Chadimová	k1gFnSc1	Chadimová
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Slezáková	Slezáková	k1gFnSc1	Slezáková
<g/>
,	,	kIx,	,
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
sloužila	sloužit	k5eAaImAgFnS	sloužit
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
u	u	k7c2	u
měšťana	měšťan	k1gMnSc2	měšťan
Klášterského	klášterský	k2eAgMnSc2d1	klášterský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
jde	jít	k5eAaImIp3nS	jít
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
nepravděpodobnou	pravděpodobný	k2eNgFnSc4d1	nepravděpodobná
legendu	legenda	k1gFnSc4	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
tanec	tanec	k1gInSc4	tanec
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
maděra	maděra	k1gMnSc1	maděra
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
polovičního	poloviční	k2eAgInSc2d1	poloviční
rytmu	rytmus	k1gInSc2	rytmus
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c6	na
půlka	půlka	k1gFnSc1	půlka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
polka	polka	k1gFnSc1	polka
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
jako	jako	k9	jako
výraz	výraz	k1gInSc1	výraz
sympatií	sympatie	k1gFnPc2	sympatie
k	k	k7c3	k
národně	národně	k6eAd1	národně
utlačovaným	utlačovaný	k2eAgMnPc3d1	utlačovaný
Polákům	Polák	k1gMnPc3	Polák
(	(	kIx(	(
<g/>
povstání	povstání	k1gNnSc4	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
pramene	pramen	k1gInSc2	pramen
však	však	k9	však
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
polské	polský	k2eAgFnSc2d1	polská
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Esmeraldy	Esmeralda	k1gFnSc2	Esmeralda
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
polky	polka	k1gFnSc2	polka
Esmeralda	Esmeralda	k1gFnSc1	Esmeralda
i	i	k8xC	i
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnPc4	první
tištěné	tištěný	k2eAgFnPc4d1	tištěná
polky	polka	k1gFnPc4	polka
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Matěj	Matěj	k1gMnSc1	Matěj
Hilmar	Hilmar	k1gMnSc1	Hilmar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
i	i	k9	i
některé	některý	k3yIgNnSc1	některý
další	další	k2eAgNnSc1d1	další
<g/>
:	:	kIx,	:
Prachovská	prachovský	k2eAgFnSc1d1	Prachovská
<g/>
,	,	kIx,	,
Anenská	anenský	k2eAgFnSc1d1	Anenská
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
František	František	k1gMnSc1	František
Matěj	Matěj	k1gMnSc1	Matěj
Hilmar	Hilmar	k1gMnSc1	Hilmar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
tanec	tanec	k1gInSc1	tanec
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
polka	polka	k1gFnSc1	polka
celkem	celkem	k6eAd1	celkem
běžně	běžně	k6eAd1	běžně
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
uchvácené	uchvácený	k2eAgNnSc1d1	uchvácené
rytmem	rytmus	k1gInSc7	rytmus
polky	polka	k1gFnSc2	polka
zavítal	zavítat	k5eAaPmAgInS	zavítat
i	i	k9	i
Johann	Johann	k1gInSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pak	pak	k9	pak
sám	sám	k3xTgMnSc1	sám
složil	složit	k5eAaPmAgMnS	složit
několik	několik	k4yIc4	několik
skladeb	skladba	k1gFnPc2	skladba
v	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
polky	polka	k1gFnSc2	polka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
známým	známý	k2eAgMnPc3d1	známý
autorům	autor	k1gMnPc3	autor
polek	polka	k1gFnPc2	polka
patří	patřit	k5eAaImIp3nS	patřit
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Polka	Polka	k1gFnSc1	Polka
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
společenských	společenský	k2eAgInPc2d1	společenský
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
hlavně	hlavně	k6eAd1	hlavně
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
zejména	zejména	k9	zejména
jako	jako	k9	jako
Beer	Beer	k1gMnSc1	Beer
barrel	barrel	k1gMnSc1	barrel
polka	polka	k1gFnSc1	polka
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
skladba	skladba	k1gFnSc1	skladba
od	od	k7c2	od
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Vejvody	Vejvoda	k1gMnSc2	Vejvoda
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
jako	jako	k8xS	jako
Modřanská	modřanský	k2eAgFnSc1d1	Modřanská
polka	polka	k1gFnSc1	polka
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Polka	Polka	k1gFnSc1	Polka
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
tanec	tanec	k1gInSc1	tanec
známa	známo	k1gNnSc2	známo
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
Německo	Německo	k1gNnSc1	Německo
Polsko	Polsko	k1gNnSc1	Polsko
Rakousko	Rakousko	k1gNnSc1	Rakousko
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
Slovensko	Slovensko	k1gNnSc1	Slovensko
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k9	také
v	v	k7c4	v
<g/>
:	:	kIx,	:
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
Itálie	Itálie	k1gFnSc2	Itálie
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
Litva	Litva	k1gFnSc1	Litva
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
Rusko	Rusko	k1gNnSc1	Rusko
Místní	místní	k2eAgFnPc1d1	místní
formy	forma	k1gFnPc1	forma
tohoto	tento	k3xDgInSc2	tento
tance	tanec	k1gInSc2	tanec
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c4	v
<g/>
:	:	kIx,	:
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
vč.	vč.	k?	vč.
Skotska	Skotsko	k1gNnSc2	Skotsko
Irsko	Irsko	k1gNnSc4	Irsko
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
USA	USA	kA	USA
Filipíny	Filipíny	k1gFnPc4	Filipíny
Polka	Polka	k1gFnSc1	Polka
je	být	k5eAaImIp3nS	být
párový	párový	k2eAgInSc4d1	párový
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kolové	kolový	k2eAgInPc4d1	kolový
tance	tanec	k1gInPc4	tanec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
dokola	dokola	k6eAd1	dokola
kolem	kolem	k7c2	kolem
sálu	sál	k1gInSc2	sál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgInSc2	tento
tance	tanec	k1gInSc2	tanec
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
kombinace	kombinace	k1gFnSc2	kombinace
kroků	krok	k1gInPc2	krok
s	s	k7c7	s
tanci	tanec	k1gInPc7	tanec
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
a	a	k8xC	a
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
figur	figura	k1gFnPc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
polka	polka	k1gFnSc1	polka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polka	Polka	k1gFnSc1	Polka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
