<s>
Dněstr	Dněstr	k1gInSc1	Dněstr
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Д	Д	k?	Д
/	/	kIx~	/
Dnister	Dnister	k1gMnSc1	Dnister
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
Nistru	Nistr	k1gInSc2	Nistr
<g/>
,	,	kIx,	,
moldavsky	moldavsky	k6eAd1	moldavsky
Н	Н	k?	Н
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
latinsky	latinsky	k6eAd1	latinsky
Tyras	Tyrasa	k1gFnPc2	Tyrasa
a	a	k8xC	a
starořecky	starořecky	k6eAd1	starořecky
Danastris	Danastris	k1gFnSc1	Danastris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
částečně	částečně	k6eAd1	částečně
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
