<p>
<s>
Dněstr	Dněstr	k1gInSc1	Dněstr
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Д	Д	k?	Д
/	/	kIx~	/
Dnister	Dnister	k1gMnSc1	Dnister
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
Nistru	Nistr	k1gInSc2	Nistr
<g/>
,	,	kIx,	,
moldavsky	moldavsky	k6eAd1	moldavsky
Н	Н	k?	Н
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
latinsky	latinsky	k6eAd1	latinsky
Tyras	Tyrasa	k1gFnPc2	Tyrasa
a	a	k8xC	a
starořecky	starořecky	k6eAd1	starořecky
Danastris	Danastris	k1gFnSc1	Danastris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
částečně	částečně	k6eAd1	částečně
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
se	se	k3xPyFc4	se
východní	východní	k2eAgInSc1d1	východní
břeh	břeh	k1gInSc1	břeh
mezi	mezi	k7c7	mezi
řekou	řeka	k1gFnSc7	řeka
a	a	k8xC	a
hranicí	hranice	k1gFnSc7	hranice
nazývá	nazývat	k5eAaImIp3nS	nazývat
Transnistrie	Transnistrie	k1gFnSc1	Transnistrie
nebo	nebo	k8xC	nebo
Podněstří	Podněstří	k1gFnSc1	Podněstří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Chotyn	Chotyno	k1gNnPc2	Chotyno
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
historické	historický	k2eAgInPc4d1	historický
regiony	region	k1gInPc4	region
Podolí	Podolí	k1gNnSc2	Podolí
a	a	k8xC	a
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
1352	[number]	k4	1352
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
72	[number]	k4	72
100	[number]	k4	100
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
severních	severní	k2eAgInPc6d1	severní
svazích	svah	k1gInPc6	svah
Karpat	Karpaty	k1gInPc2	Karpaty
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
m	m	kA	m
v	v	k7c6	v
Haliči	Halič	k1gFnSc6	Halič
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
poblíž	poblíž	k6eAd1	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
Dněsterského	dněsterský	k2eAgInSc2d1	dněsterský
limanu	liman	k1gInSc2	liman
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Halyčem	Halyč	k1gInSc7	Halyč
<g/>
)	)	kIx)	)
teče	téct	k5eAaImIp3nS	téct
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
úzkou	úzký	k2eAgFnSc7d1	úzká
dolinou	dolina	k1gFnSc7	dolina
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
horské	horský	k2eAgFnSc2d1	horská
bystřiny	bystřina	k1gFnSc2	bystřina
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
přijímá	přijímat	k5eAaImIp3nS	přijímat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
přítoků	přítok	k1gInPc2	přítok
především	především	k9	především
zprava	zprava	k6eAd1	zprava
z	z	k7c2	z
karpatských	karpatský	k2eAgInPc2d1	karpatský
svahů	svah	k1gInPc2	svah
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnSc4d3	veliký
je	být	k5eAaImIp3nS	být
Stryj	Stryj	k1gFnSc4	Stryj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Halyčem	Halyč	k1gInSc7	Halyč
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
klidnější	klidný	k2eAgFnSc1d2	klidnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dolina	dolina	k1gFnSc1	dolina
je	být	k5eAaImIp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
úzká	úzký	k2eAgFnSc1d1	úzká
a	a	k8xC	a
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
přijímá	přijímat	k5eAaImIp3nS	přijímat
přítoky	přítok	k1gInPc4	přítok
pouze	pouze	k6eAd1	pouze
zleva	zleva	k6eAd1	zleva
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Zolota	Zolot	k1gMnSc4	Zolot
Lipa	Lipus	k1gMnSc4	Lipus
<g/>
,	,	kIx,	,
Strypa	Stryp	k1gMnSc4	Stryp
<g/>
,	,	kIx,	,
Seret	Seret	k1gMnSc1	Seret
<g/>
,	,	kIx,	,
Zbruč	Zbruč	k1gMnSc1	Zbruč
<g/>
,	,	kIx,	,
Smotryč	Smotryč	k1gMnSc1	Smotryč
<g/>
,	,	kIx,	,
Murafa	Muraf	k1gMnSc2	Muraf
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
Mohyliv-Podilskyj	Mohyliv-Podilskyj	k1gInSc1	Mohyliv-Podilskyj
se	se	k3xPyFc4	se
dolina	dolina	k1gFnSc1	dolina
mírně	mírně	k6eAd1	mírně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
jen	jen	k9	jen
v	v	k7c6	v
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
mezi	mezi	k7c7	mezi
výběžky	výběžek	k1gInPc7	výběžek
Podolské	podolský	k2eAgFnSc2d1	Podolská
vysočiny	vysočina	k1gFnSc2	vysočina
dosahujícími	dosahující	k2eAgFnPc7d1	dosahující
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
jsou	být	k5eAaImIp3nP	být
nevelké	velký	k2eNgInPc1d1	nevelký
slapy	slap	k1gInPc1	slap
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
města	město	k1gNnSc2	město
Dubăsari	Dubăsar	k1gFnSc2	Dubăsar
vybudována	vybudován	k2eAgFnSc1d1	vybudována
hráz	hráz	k1gFnSc1	hráz
Dubăsarské	Dubăsarský	k2eAgFnSc2d1	Dubăsarský
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Dubăsarská	Dubăsarský	k2eAgFnSc1d1	Dubăsarský
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
Tiraspol	Tiraspola	k1gFnPc2	Tiraspola
řeka	řeka	k1gFnSc1	řeka
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
Černomořské	černomořský	k2eAgFnSc2d1	černomořská
nížiny	nížina	k1gFnSc2	nížina
a	a	k8xC	a
dolina	dolina	k1gFnSc1	dolina
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
8	[number]	k4	8
až	až	k9	až
16	[number]	k4	16
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
přijímá	přijímat	k5eAaImIp3nS	přijímat
zprava	zprava	k6eAd1	zprava
řeky	řeka	k1gFnPc1	řeka
Răut	Răutum	k1gNnPc2	Răutum
<g/>
,	,	kIx,	,
Byk	Byk	k1gMnSc4	Byk
<g/>
,	,	kIx,	,
Botna	Botn	k1gMnSc4	Botn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgNnPc4d2	veliký
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
Dněstr	Dněstr	k1gInSc1	Dněstr
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Sambir	Sambira	k1gFnPc2	Sambira
<g/>
,	,	kIx,	,
Mykolajiv	Mykolajiva	k1gFnPc2	Mykolajiva
<g/>
,	,	kIx,	,
Novyj	Novyj	k1gFnSc1	Novyj
Rozdil	Rozdil	k1gFnSc1	Rozdil
<g/>
,	,	kIx,	,
Halyč	Halyč	k1gFnSc1	Halyč
<g/>
,	,	kIx,	,
Zališčyky	Zališčyka	k1gFnPc1	Zališčyka
<g/>
,	,	kIx,	,
Chotyn	Chotyn	k1gMnSc1	Chotyn
<g/>
,	,	kIx,	,
Mohyliv-Podilskyj	Mohyliv-Podilskyj	k1gMnSc1	Mohyliv-Podilskyj
<g/>
,	,	kIx,	,
Jampil	Jampil	k1gMnSc1	Jampil
<g/>
,	,	kIx,	,
Soroca	Soroca	k1gMnSc1	Soroca
<g/>
,	,	kIx,	,
Rîbniț	Rîbniț	k1gMnSc1	Rîbniț
<g/>
,	,	kIx,	,
Bendery	Bendera	k1gFnPc1	Bendera
<g/>
,	,	kIx,	,
Tiraspol	Tiraspol	k1gInSc1	Tiraspol
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Dněsterského	dněsterský	k2eAgInSc2d1	dněsterský
limanu	liman	k1gInSc2	liman
leží	ležet	k5eAaImIp3nS	ležet
Bilhorod-Dnistrovskyj	Bilhorod-Dnistrovskyj	k1gFnSc1	Bilhorod-Dnistrovskyj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
sněhový	sněhový	k2eAgInSc4d1	sněhový
a	a	k8xC	a
dešťový	dešťový	k2eAgInSc4d1	dešťový
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prudkým	prudký	k2eAgInPc3d1	prudký
vzestupům	vzestup	k1gInPc3	vzestup
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
způsobeny	způsoben	k2eAgInPc1d1	způsoben
letními	letní	k2eAgInPc7d1	letní
přívalovými	přívalový	k2eAgInPc7d1	přívalový
dešti	dešť	k1gInPc7	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Nezřídka	nezřídka	k6eAd1	nezřídka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
povodním	povodeň	k1gFnPc3	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bendery	Bendera	k1gFnSc2	Bendera
činí	činit	k5eAaImIp3nS	činit
310	[number]	k4	310
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgNnSc1d1	maximální
až	až	k6eAd1	až
2500	[number]	k4	2500
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
minimální	minimální	k2eAgInSc4d1	minimální
14,7	[number]	k4	14,7
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Celkově	celkově	k6eAd1	celkově
řekou	řeka	k1gFnSc7	řeka
ročně	ročně	k6eAd1	ročně
proteče	protéct	k5eAaPmIp3nS	protéct
8,7	[number]	k4	8,7
km3	km3	k4	km3
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
nestálý	stálý	k2eNgInSc1d1	nestálý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
plují	plout	k5eAaImIp3nP	plout
kry	kra	k1gFnPc1	kra
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zima	zima	k1gFnSc1	zima
teplá	teplý	k2eAgFnSc1d1	teplá
<g/>
,	,	kIx,	,
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
zavedená	zavedený	k2eAgFnSc1d1	zavedená
od	od	k7c2	od
města	město	k1gNnSc2	město
Soroca	Soroc	k1gInSc2	Soroc
do	do	k7c2	do
Dubăsari	Dubăsar	k1gFnSc2	Dubăsar
a	a	k8xC	a
od	od	k7c2	od
Dubăsari	Dubăsar	k1gFnSc2	Dubăsar
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Mohyliv-Podilskyj	Mohyliv-Podilskyj	k1gInSc1	Mohyliv-Podilskyj
<g/>
,	,	kIx,	,
Soroca	Sorocum	k1gNnPc1	Sorocum
<g/>
,	,	kIx,	,
Bendery	Bender	k1gInPc1	Bender
<g/>
,	,	kIx,	,
Tiraspol	Tiraspol	k1gInSc1	Tiraspol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Д	Д	k?	Д
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Dněstr	Dněstr	k1gInSc1	Dněstr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dněstr	Dněstr	k1gInSc1	Dněstr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
