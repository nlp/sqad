<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
slova	slovo	k1gNnPc1	slovo
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
nebo	nebo	k8xC	nebo
podobným	podobný	k2eAgInSc7d1	podobný
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
lze	lze	k6eAd1	lze
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
<g/>
?	?	kIx.	?
</s>
