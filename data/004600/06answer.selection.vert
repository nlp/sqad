<s>
Synonyma	synonymum	k1gNnPc1	synonymum
též	též	k9	též
slova	slovo	k1gNnPc1	slovo
souznačná	souznačný	k2eAgNnPc1d1	souznačné
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc4	slovo
nebo	nebo	k8xC	nebo
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
vzájemně	vzájemně	k6eAd1	vzájemně
stejným	stejný	k2eAgInSc7d1	stejný
nebo	nebo	k8xC	nebo
podobným	podobný	k2eAgInSc7d1	podobný
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
lze	lze	k6eAd1	lze
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
<g/>
.	.	kIx.	.
</s>
