<s>
Synonyma	synonymum	k1gNnPc1	synonymum
též	též	k9	též
slova	slovo	k1gNnPc1	slovo
souznačná	souznačný	k2eAgNnPc1d1	souznačné
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc4	slovo
nebo	nebo	k8xC	nebo
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
vzájemně	vzájemně	k6eAd1	vzájemně
stejným	stejný	k2eAgInSc7d1	stejný
nebo	nebo	k8xC	nebo
podobným	podobný	k2eAgInSc7d1	podobný
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
lze	lze	k6eAd1	lze
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dodávají	dodávat	k5eAaImIp3nP	dodávat
jazyku	jazyk	k1gInSc3	jazyk
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
jemné	jemný	k2eAgNnSc4d1	jemné
odstínění	odstínění	k1gNnSc4	odstínění
významů	význam	k1gInPc2	význam
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
kontextovém	kontextový	k2eAgNnSc6d1	kontextové
a	a	k8xC	a
stylistickém	stylistický	k2eAgNnSc6d1	stylistické
zabarvení	zabarvení	k1gNnSc6	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
jevu	jev	k1gInSc3	jev
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
synonymních	synonymní	k2eAgInPc2d1	synonymní
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tzv.	tzv.	kA	tzv.
synonymickou	synonymický	k2eAgFnSc4d1	synonymická
řadu	řada	k1gFnSc4	řada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jádro	jádro	k1gNnSc4	jádro
takové	takový	k3xDgFnSc2	takový
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
nejvíce	hodně	k6eAd3	hodně
stylisticky	stylisticky	k6eAd1	stylisticky
i	i	k8xC	i
emocionálně	emocionálně	k6eAd1	emocionálně
neutrální	neutrální	k2eAgInSc1d1	neutrální
výraz	výraz	k1gInSc1	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Synonyma	synonymum	k1gNnPc1	synonymum
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
,	,	kIx,	,
neúplná	úplný	k2eNgFnSc1d1	neúplná
<g/>
.	.	kIx.	.
</s>
<s>
Úplná	úplný	k2eAgFnSc1d1	úplná
(	(	kIx(	(
<g/>
čistá	čistý	k2eAgFnSc1d1	čistá
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc1d1	absolutní
<g/>
)	)	kIx)	)
synonyma	synonymum	k1gNnPc1	synonymum
jsou	být	k5eAaImIp3nP	být
ta	ten	k3xDgNnPc1	ten
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
zcela	zcela	k6eAd1	zcela
totožný	totožný	k2eAgInSc4d1	totožný
význam	význam	k1gInSc4	význam
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
zaměnit	zaměnit	k5eAaPmF	zaměnit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
kontextech	kontext	k1gInPc6	kontext
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
ustálených	ustálený	k2eAgInPc2d1	ustálený
obratů	obrat	k1gInPc2	obrat
(	(	kIx(	(
<g/>
přísloví	přísloví	k1gNnSc1	přísloví
<g/>
,	,	kIx,	,
pořekadel	pořekadlo	k1gNnPc2	pořekadlo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgNnPc2	takový
synonym	synonymum	k1gNnPc2	synonymum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
velice	velice	k6eAd1	velice
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
hezký	hezký	k2eAgInSc1d1	hezký
–	–	k?	–
pěkný	pěkný	k2eAgInSc1d1	pěkný
<g/>
,	,	kIx,	,
chlapec	chlapec	k1gMnSc1	chlapec
–	–	k?	–
hoch	hoch	k1gMnSc1	hoch
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nP	jevit
jako	jako	k9	jako
absolutní	absolutní	k2eAgFnSc4d1	absolutní
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
úzu	úzus	k1gInSc6	úzus
spojovat	spojovat	k5eAaImF	spojovat
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
oddalování	oddalování	k1gNnSc3	oddalování
jejich	jejich	k3xOp3gInPc2	jejich
významů	význam	k1gInPc2	význam
–	–	k?	–
např.	např.	kA	např.
slova	slovo	k1gNnSc2	slovo
statečný	statečný	k2eAgMnSc1d1	statečný
a	a	k8xC	a
odvážný	odvážný	k2eAgMnSc1d1	odvážný
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
absolutní	absolutní	k2eAgFnSc4d1	absolutní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výraz	výraz	k1gInSc1	výraz
statečný	statečný	k2eAgMnSc1d1	statečný
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
spíše	spíše	k9	spíše
s	s	k7c7	s
osobami	osoba	k1gFnPc7	osoba
(	(	kIx(	(
<g/>
statečný	statečný	k2eAgMnSc1d1	statečný
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvážný	odvážný	k2eAgMnSc1d1	odvážný
spíše	spíše	k9	spíše
s	s	k7c7	s
abstraktními	abstraktní	k2eAgInPc7d1	abstraktní
výrazy	výraz	k1gInPc7	výraz
(	(	kIx(	(
<g/>
odvážný	odvážný	k2eAgInSc1d1	odvážný
kousek	kousek	k1gInSc1	kousek
<g/>
,	,	kIx,	,
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kategorií	kategorie	k1gFnSc7	kategorie
jsou	být	k5eAaImIp3nP	být
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
pouze	pouze	k6eAd1	pouze
stylovou	stylový	k2eAgFnSc7d1	stylová
platností	platnost	k1gFnSc7	platnost
(	(	kIx(	(
<g/>
táta	táta	k1gMnSc1	táta
–	–	k?	–
tatínek	tatínek	k1gMnSc1	tatínek
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvojice	dvojice	k1gFnSc1	dvojice
českého	český	k2eAgInSc2d1	český
pojmu	pojem	k1gInSc2	pojem
s	s	k7c7	s
přejatým	přejatý	k2eAgInSc7d1	přejatý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
jazykové	jazykový	k2eAgInPc4d1	jazykový
ekvivalenty	ekvivalent	k1gInPc4	ekvivalent
(	(	kIx(	(
<g/>
přeprava	přeprava	k1gFnSc1	přeprava
–	–	k?	–
transport	transport	k1gInSc1	transport
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
běžnější	běžný	k2eAgMnPc1d2	běžnější
jsou	být	k5eAaImIp3nP	být
synonyma	synonymum	k1gNnPc4	synonymum
neúplná	úplný	k2eNgNnPc4d1	neúplné
(	(	kIx(	(
<g/>
částečná	částečný	k2eAgNnPc4d1	částečné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
významy	význam	k1gInPc1	význam
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
blízké	blízký	k2eAgFnPc1d1	blízká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
totožné	totožný	k2eAgInPc1d1	totožný
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgNnPc4d1	částečné
synonyma	synonymum	k1gNnPc4	synonymum
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
kontextovým	kontextový	k2eAgNnSc7d1	kontextové
a	a	k8xC	a
stylovým	stylový	k2eAgNnSc7d1	stylové
užitím	užití	k1gNnSc7	užití
<g/>
,	,	kIx,	,
obsahem	obsah	k1gInSc7	obsah
i	i	k8xC	i
rozsahem	rozsah	k1gInSc7	rozsah
významu	význam	k1gInSc2	význam
(	(	kIx(	(
<g/>
červený	červený	k2eAgMnSc1d1	červený
–	–	k?	–
rudý	rudý	k1gMnSc1	rudý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intenzitou	intenzita	k1gFnSc7	intenzita
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc1	práce
–	–	k?	–
dřina	dřina	k1gFnSc1	dřina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobovým	dobový	k2eAgNnSc7d1	dobové
užitím	užití	k1gNnSc7	užití
(	(	kIx(	(
<g/>
krmě	krmě	k1gFnSc2	krmě
–	–	k?	–
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
frekvencí	frekvence	k1gFnSc7	frekvence
užití	užití	k1gNnSc2	užití
(	(	kIx(	(
<g/>
málokdy	málokdy	k6eAd1	málokdy
–	–	k?	–
zřídkavě	zřídkavě	k6eAd1	zřídkavě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
synonyma	synonymum	k1gNnSc2	synonymum
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
i	i	k9	i
slova	slovo	k1gNnPc4	slovo
odvozená	odvozený	k2eAgNnPc4d1	odvozené
nebo	nebo	k8xC	nebo
slovotvorné	slovotvorný	k2eAgFnPc4d1	slovotvorná
varianty	varianta	k1gFnPc4	varianta
(	(	kIx(	(
<g/>
dítě	dítě	k1gNnSc1	dítě
–	–	k?	–
děcko	děcko	k1gNnSc1	děcko
<g/>
,	,	kIx,	,
jevit	jevit	k5eAaImF	jevit
–	–	k?	–
projevit	projevit	k5eAaPmF	projevit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Synonymy	synonymum	k1gNnPc7	synonymum
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
výrazy	výraz	k1gInPc4	výraz
nářeční	nářeční	k2eAgInPc4d1	nářeční
<g/>
,	,	kIx,	,
argotické	argotický	k2eAgInPc4d1	argotický
<g/>
,	,	kIx,	,
slangové	slangový	k2eAgInPc4d1	slangový
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
neutrální	neutrální	k2eAgInSc4d1	neutrální
výraz	výraz	k1gInSc4	výraz
(	(	kIx(	(
<g/>
chlapec	chlapec	k1gMnSc1	chlapec
–	–	k?	–
ogar	ogar	k?	ogar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
varianty	varianta	k1gFnPc4	varianta
tvaroslovné	tvaroslovný	k2eAgNnSc4d1	tvaroslovné
a	a	k8xC	a
hláskové	hláskový	k2eAgNnSc4d1	hláskové
se	se	k3xPyFc4	se
za	za	k7c2	za
synonyma	synonymum	k1gNnSc2	synonymum
nepovažují	považovat	k5eNaImIp3nP	považovat
(	(	kIx(	(
<g/>
brambor	brambor	k1gInSc1	brambor
–	–	k?	–
brambora	brambora	k1gFnSc1	brambora
<g/>
,	,	kIx,	,
vzlítnout	vzlítnout	k5eAaPmF	vzlítnout
–	–	k?	–
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
sousloví	sousloví	k1gNnPc1	sousloví
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pořekadla	pořekadlo	k1gNnPc1	pořekadlo
a	a	k8xC	a
ustálené	ustálený	k2eAgInPc1d1	ustálený
obraty	obrat	k1gInPc1	obrat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
někdy	někdy	k6eAd1	někdy
svůj	svůj	k3xOyFgInSc4	svůj
slovní	slovní	k2eAgInSc4d1	slovní
synonymní	synonymní	k2eAgInSc4d1	synonymní
protějšek	protějšek	k1gInSc4	protějšek
(	(	kIx(	(
<g/>
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c4	na
ocet	ocet	k1gInSc4	ocet
–	–	k?	–
neprovdat	provdat	k5eNaPmF	provdat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
ustálené	ustálený	k2eAgInPc1d1	ustálený
obraty	obrat	k1gInPc1	obrat
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
synonymní	synonymní	k2eAgInSc4d1	synonymní
protějšek	protějšek	k1gInSc4	protějšek
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
vzít	vzít	k5eAaPmF	vzít
nohy	noha	k1gFnPc4	noha
na	na	k7c4	na
ramena	rameno	k1gNnPc4	rameno
–	–	k?	–
prásknout	prásknout	k5eAaPmF	prásknout
do	do	k7c2	do
bot	bota	k1gFnPc2	bota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
synonym	synonymum	k1gNnPc2	synonymum
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
synonyma	synonymum	k1gNnPc4	synonymum
idiografická	idiografický	k2eAgNnPc4d1	idiografický
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
ve	v	k7c6	v
věcném	věcný	k2eAgInSc6d1	věcný
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
označují	označovat	k5eAaImIp3nP	označovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
hlediska	hledisko	k1gNnSc2	hledisko
(	(	kIx(	(
<g/>
moudrý	moudrý	k2eAgInSc1d1	moudrý
–	–	k?	–
chytrý	chytrý	k2eAgInSc1d1	chytrý
<g/>
,	,	kIx,	,
smělý	smělý	k2eAgInSc1d1	smělý
–	–	k?	–
nebojácný	bojácný	k2eNgInSc1d1	nebojácný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
synonyma	synonymum	k1gNnPc1	synonymum
můžeme	moct	k5eAaImIp1nP	moct
dále	daleko	k6eAd2	daleko
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
:	:	kIx,	:
specifikační	specifikační	k2eAgFnSc4d1	specifikační
–	–	k?	–
spojením	spojení	k1gNnSc7	spojení
slov	slovo	k1gNnPc2	slovo
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
rudý	rudý	k2eAgInSc1d1	rudý
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
sytě	sytě	k6eAd1	sytě
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
intenzifikační	intenzifikační	k2eAgFnSc1d1	intenzifikační
–	–	k?	–
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
emocí	emoce	k1gFnPc2	emoce
(	(	kIx(	(
<g/>
studený	studený	k2eAgInSc1d1	studený
–	–	k?	–
ledový	ledový	k2eAgInSc1d1	ledový
<g/>
)	)	kIx)	)
Slova	slovo	k1gNnPc4	slovo
přejatá	přejatý	k2eAgNnPc4d1	přejaté
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
českým	český	k2eAgInPc3d1	český
protějškům	protějšek	k1gInPc3	protějšek
zpravidla	zpravidla	k6eAd1	zpravidla
pociťována	pociťovat	k5eAaImNgFnS	pociťovat
jako	jako	k8xC	jako
stylově	stylově	k6eAd1	stylově
vytříbenější	vytříbený	k2eAgInSc1d2	vytříbenější
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
restaurant	restaurant	k1gInSc1	restaurant
–	–	k?	–
hostinec	hostinec	k1gInSc1	hostinec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
eufemizující	eufemizující	k2eAgFnSc1d1	eufemizující
(	(	kIx(	(
<g/>
impertinence	impertinence	k1gFnSc1	impertinence
–	–	k?	–
drzost	drzost	k1gFnSc1	drzost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kromě	kromě	k7c2	kromě
většiny	většina	k1gFnSc2	většina
výrazů	výraz	k1gInPc2	výraz
přejatých	přejatý	k2eAgInPc2d1	přejatý
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
lágr	lágr	k1gInSc1	lágr
–	–	k?	–
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
štucl	štucl	k?	štucl
–	–	k?	–
rukávník	rukávník	k1gInSc1	rukávník
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
výjimek	výjimka	k1gFnPc2	výjimka
(	(	kIx(	(
<g/>
lavor	lavor	k?	lavor
–	–	k?	–
umyvadlo	umyvadlo	k1gNnSc1	umyvadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
užívají	užívat	k5eAaImIp3nP	užívat
výrazy	výraz	k1gInPc1	výraz
přejaté	přejatý	k2eAgInPc1d1	přejatý
<g/>
,	,	kIx,	,
v	v	k7c6	v
popularizační	popularizační	k2eAgFnSc6d1	popularizační
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
preferovat	preferovat	k5eAaImF	preferovat
jejich	jejich	k3xOp3gInPc4	jejich
české	český	k2eAgInPc4d1	český
protějšky	protějšek	k1gInPc4	protějšek
(	(	kIx(	(
<g/>
marginální	marginální	k2eAgInSc1d1	marginální
–	–	k?	–
okrajový	okrajový	k2eAgInSc1d1	okrajový
<g/>
,	,	kIx,	,
binom	binom	k1gInSc1	binom
–	–	k?	–
dvojčlen	dvojčlen	k1gInSc1	dvojčlen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
už	už	k6eAd1	už
nejsou	být	k5eNaImIp3nP	být
málo	málo	k6eAd1	málo
užívané	užívaný	k2eAgInPc1d1	užívaný
(	(	kIx(	(
<g/>
azbest	azbest	k1gInSc1	azbest
–	–	k?	–
osinek	osinek	k1gInSc1	osinek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Synonymie	synonymie	k1gFnSc1	synonymie
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
týkat	týkat	k5eAaImF	týkat
pouze	pouze	k6eAd1	pouze
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vět	věta	k1gFnPc2	věta
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
delších	dlouhý	k2eAgInPc2d2	delší
textových	textový	k2eAgInPc2d1	textový
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
výpovědi	výpověď	k1gFnPc1	výpověď
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
parafráze	parafráze	k1gFnPc1	parafráze
(	(	kIx(	(
<g/>
výpověď	výpověď	k1gFnSc1	výpověď
opakuje	opakovat	k5eAaImIp3nS	opakovat
totéž	týž	k3xTgNnSc4	týž
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
perifráze	perifráze	k1gFnSc1	perifráze
(	(	kIx(	(
<g/>
výpověď	výpověď	k1gFnSc1	výpověď
je	být	k5eAaImIp3nS	být
přesnější	přesný	k2eAgFnSc7d2	přesnější
specifikací	specifikace	k1gFnSc7	specifikace
nebo	nebo	k8xC	nebo
slovním	slovní	k2eAgInSc7d1	slovní
opisem	opis	k1gInSc7	opis
původní	původní	k2eAgFnSc2d1	původní
výpovědi	výpověď	k1gFnSc2	výpověď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
synonymní	synonymní	k2eAgFnPc4d1	synonymní
výpovědi	výpověď	k1gFnPc4	výpověď
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
takové	takový	k3xDgInPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc4d1	stejná
presupozici	presupozice	k1gFnSc4	presupozice
(	(	kIx(	(
<g/>
předpoklad	předpoklad	k1gInSc1	předpoklad
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejné	stejný	k2eAgFnPc4d1	stejná
pravdivostní	pravdivostní	k2eAgFnPc4d1	pravdivostní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
zaměněné	zaměněný	k2eAgNnSc4d1	zaměněné
téma	téma	k1gNnSc4	téma
a	a	k8xC	a
réma	réma	k1gNnSc4	réma
<g/>
,	,	kIx,	,
intonaci	intonace	k1gFnSc4	intonace
<g/>
,	,	kIx,	,
podmět	podmět	k1gInSc4	podmět
a	a	k8xC	a
předmět	předmět	k1gInSc4	předmět
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vyplývat	vyplývat	k5eAaImF	vyplývat
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
kontextu	kontext	k1gInSc2	kontext
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
kontext	kontext	k1gInSc1	kontext
vytvářet	vytvářet	k5eAaImF	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Větná	větný	k2eAgFnSc1d1	větná
synonymie	synonymie	k1gFnSc1	synonymie
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
:	:	kIx,	:
slovní	slovní	k2eAgFnSc1d1	slovní
synonymií	synonymie	k1gFnSc7	synonymie
<g/>
:	:	kIx,	:
tatínek	tatínek	k1gMnSc1	tatínek
je	být	k5eAaImIp3nS	být
lingvista	lingvista	k1gMnSc1	lingvista
–	–	k?	–
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
záměnou	záměna	k1gFnSc7	záměna
slovosledu	slovosled	k1gInSc2	slovosled
<g/>
:	:	kIx,	:
Pod	pod	k7c7	pod
stolem	stol	k1gInSc7	stol
leží	ležet	k5eAaImIp3nS	ležet
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
–	–	k?	–
Kniha	kniha	k1gFnSc1	kniha
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
stolem	stol	k1gInSc7	stol
<g/>
.	.	kIx.	.
záměnou	záměna	k1gFnSc7	záměna
věty	věta	k1gFnSc2	věta
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
za	za	k7c4	za
větný	větný	k2eAgInSc4d1	větný
člen	člen	k1gInSc4	člen
<g/>
:	:	kIx,	:
Ten	ten	k3xDgInSc1	ten
zápas	zápas	k1gInSc1	zápas
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
ten	ten	k3xDgInSc4	ten
zápas	zápas	k1gInSc4	zápas
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
.	.	kIx.	.
univerbizací	univerbizace	k1gFnSc7	univerbizace
nebo	nebo	k8xC	nebo
multiverbizací	multiverbizace	k1gFnSc7	multiverbizace
<g/>
:	:	kIx,	:
Odpoledne	odpoledne	k6eAd1	odpoledne
musím	muset	k5eAaImIp1nS	muset
navštívit	navštívit	k5eAaPmF	navštívit
zubaře	zubař	k1gMnPc4	zubař
<g/>
.	.	kIx.	.
–	–	k?	–
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
odpoledne	odpoledne	k1gNnSc2	odpoledne
musím	muset	k5eAaImIp1nS	muset
vykonat	vykonat	k5eAaPmF	vykonat
návštěvu	návštěva	k1gFnSc4	návštěva
u	u	k7c2	u
zubního	zubní	k2eAgMnSc2d1	zubní
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
záměnou	záměna	k1gFnSc7	záměna
podmětu	podmět	k1gInSc2	podmět
a	a	k8xC	a
předmětu	předmět	k1gInSc2	předmět
<g/>
:	:	kIx,	:
Tento	tento	k3xDgInSc4	tento
dům	dům	k1gInSc4	dům
postavil	postavit	k5eAaPmAgMnS	postavit
můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
–	–	k?	–
Tento	tento	k3xDgInSc4	tento
dům	dům	k1gInSc4	dům
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
mým	můj	k1gMnSc7	můj
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
záměnou	záměna	k1gFnSc7	záměna
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
synonymie	synonymie	k1gFnSc1	synonymie
ztratit	ztratit	k5eAaPmF	ztratit
–	–	k?	–
např.	např.	kA	např.
u	u	k7c2	u
záporných	záporný	k2eAgFnPc2d1	záporná
vět	věta	k1gFnPc2	věta
může	moct	k5eAaImIp3nS	moct
slovosled	slovosled	k1gInSc1	slovosled
změnit	změnit	k5eAaPmF	změnit
rozsah	rozsah	k1gInSc4	rozsah
negace	negace	k1gFnSc2	negace
<g/>
:	:	kIx,	:
Maminka	maminka	k1gFnSc1	maminka
nečte	číst	k5eNaImIp3nS	číst
za	za	k7c7	za
domem	dům	k1gInSc7	dům
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Maminka	maminka	k1gFnSc1	maminka
tam	tam	k6eAd1	tam
čte	číst	k5eAaImIp3nS	číst
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
)	)	kIx)	)
x	x	k?	x
Za	za	k7c7	za
domem	dům	k1gInSc7	dům
nečte	číst	k5eNaImIp3nS	číst
knihu	kniha	k1gFnSc4	kniha
maminka	maminka	k1gFnSc1	maminka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Knihu	kniha	k1gFnSc4	kniha
tam	tam	k6eAd1	tam
čte	číst	k5eAaImIp3nS	číst
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
x	x	k?	x
Maminka	maminka	k1gFnSc1	maminka
nečte	číst	k5eNaImIp3nS	číst
knihu	kniha	k1gFnSc4	kniha
za	za	k7c7	za
domem	dům	k1gInSc7	dům
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Knihu	kniha	k1gFnSc4	kniha
čte	číst	k5eAaImIp3nS	číst
někde	někde	k6eAd1	někde
jinde	jinde	k6eAd1	jinde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Synonyma	synonymum	k1gNnPc1	synonymum
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nejběžnější	běžný	k2eAgMnSc1d3	Nejběžnější
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
plnovýznamových	plnovýznamový	k2eAgNnPc2d1	plnovýznamové
(	(	kIx(	(
<g/>
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seu	Seu	k?	Seu
neboli	neboli	k8xC	neboli
sive	sive	k1gFnSc1	sive
znamená	znamenat	k5eAaImIp3nS	znamenat
latinsky	latinsky	k6eAd1	latinsky
nebo	nebo	k8xC	nebo
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
synonym	synonymum	k1gNnPc2	synonymum
v	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kost	kost	k1gFnSc4	kost
stehenní	stehenní	k2eAgInSc1d1	stehenní
–	–	k?	–
os	osa	k1gFnPc2	osa
femoris	femoris	k1gFnSc1	femoris
seu	seu	k?	seu
femur	femura	k1gFnPc2	femura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
používá	používat	k5eAaImIp3nS	používat
zkratka	zkratka	k1gFnSc1	zkratka
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
synonymum	synonymum	k1gNnSc4	synonymum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
synonymie	synonymie	k1gFnSc1	synonymie
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
absolutní	absolutní	k2eAgFnSc1d1	absolutní
tautonomie	tautonomie	k1gFnSc1	tautonomie
<g/>
.	.	kIx.	.
</s>
<s>
Synonyma	synonymum	k1gNnPc1	synonymum
se	se	k3xPyFc4	se
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
objektivní	objektivní	k2eAgMnPc1d1	objektivní
(	(	kIx(	(
<g/>
o	o	k7c6	o
synonymii	synonymie	k1gFnSc6	synonymie
pojmenování	pojmenování	k1gNnSc2	pojmenování
není	být	k5eNaImIp3nS	být
sporu	spor	k1gInSc2	spor
<g/>
)	)	kIx)	)
a	a	k8xC	a
subjektivní	subjektivní	k2eAgInPc1d1	subjektivní
(	(	kIx(	(
<g/>
taxony	taxon	k1gInPc1	taxon
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
tytéž	týž	k3xTgInPc4	týž
jen	jen	k8xS	jen
některými	některý	k3yIgMnPc7	některý
autory	autor	k1gMnPc7	autor
<g/>
)	)	kIx)	)
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
a	a	k8xC	a
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Synonymum	synonymum	k1gNnSc1	synonymum
(	(	kIx(	(
<g/>
taxonomie	taxonomie	k1gFnSc1	taxonomie
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
nomenklatuře	nomenklatura	k1gFnSc6	nomenklatura
jiné	jiný	k2eAgNnSc4d1	jiné
jméno	jméno	k1gNnSc4	jméno
téhož	týž	k3xTgInSc2	týž
taxonu	taxon	k1gInSc2	taxon
používané	používaný	k2eAgInPc4d1	používaný
jinými	jiný	k2eAgMnPc7d1	jiný
autory	autor	k1gMnPc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Platný	platný	k2eAgMnSc1d1	platný
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
ten	ten	k3xDgInSc4	ten
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yQgMnSc7	který
byl	být	k5eAaImAgInS	být
taxon	taxon	k1gInSc4	taxon
popsán	popsán	k2eAgInSc1d1	popsán
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
synonyma	synonymum	k1gNnPc1	synonymum
existují	existovat	k5eAaImIp3nP	existovat
k	k	k7c3	k
přibližně	přibližně	k6eAd1	přibližně
23	[number]	k4	23
000	[number]	k4	000
českým	český	k2eAgMnSc7d1	český
slovům	slovo	k1gNnPc3	slovo
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
synonym	synonymum	k1gNnPc2	synonymum
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
KOLEKTIV	kolektiv	k1gInSc1	kolektiv
AUTORŮ	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgNnPc2d1	české
synonym	synonymum	k1gNnPc2	synonymum
a	a	k8xC	a
antonym	antonymum	k1gNnPc2	antonymum
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Lingea	Lingea	k1gFnSc1	Lingea
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Fortuna	Fortuna	k1gFnSc1	Fortuna
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
<g/>
,	,	kIx,	,
s.	s.	k?	s.
112	[number]	k4	112
<g/>
-	-	kIx~	-
<g/>
113	[number]	k4	113
<g/>
.	.	kIx.	.
antonymum	antonymum	k1gNnSc1	antonymum
eufemismus	eufemismus	k1gInSc1	eufemismus
homonymum	homonymum	k1gNnSc1	homonymum
frazém	frazém	k1gInSc1	frazém
kontext	kontext	k1gInSc1	kontext
přenesený	přenesený	k2eAgInSc4d1	přenesený
význam	význam	k1gInSc4	význam
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
synonymum	synonymum	k1gNnSc4	synonymum
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
