<p>
<s>
Stanisław	Stanisław	k?	Stanisław
Herman	Herman	k1gMnSc1	Herman
Lem	lem	k1gInSc1	lem
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1921	[number]	k4	1921
Lvov	Lvov	k1gInSc1	Lvov
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
Krakov	Krakov	k1gInSc1	Krakov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
futurolog	futurolog	k1gMnSc1	futurolog
a	a	k8xC	a
satirik	satirik	k1gMnSc1	satirik
<g/>
,	,	kIx,	,
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
autor	autor	k1gMnSc1	autor
sci-fi	scii	k1gNnPc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
40	[number]	k4	40
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
27	[number]	k4	27
milionů	milion	k4xCgInPc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
nejen	nejen	k6eAd1	nejen
polskou	polský	k2eAgFnSc4d1	polská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
světovou	světový	k2eAgFnSc4d1	světová
vědecko-fantastickou	vědeckoantastický	k2eAgFnSc4d1	vědecko-fantastická
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Lvov	Lvov	k1gInSc4	Lvov
-	-	kIx~	-
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
dospívání	dospívání	k1gNnSc2	dospívání
===	===	k?	===
</s>
</p>
<p>
<s>
Stanisław	Stanisław	k?	Stanisław
Lem	lem	k1gInSc1	lem
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
synem	syn	k1gMnSc7	syn
zámožného	zámožný	k2eAgMnSc2d1	zámožný
lvovského	lvovský	k2eAgMnSc2d1	lvovský
lékaře	lékař	k1gMnSc2	lékař
otorhinolaryngologa	otorhinolaryngolog	k1gMnSc2	otorhinolaryngolog
Samuela	Samuel	k1gMnSc2	Samuel
Lema	Lemus	k1gMnSc2	Lemus
a	a	k8xC	a
Sabiny	Sabina	k1gMnSc2	Sabina
Lemové	lemový	k2eAgFnSc2d1	lemová
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Wollnerové	Wollnerová	k1gFnSc2	Wollnerová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgMnSc1d1	malý
Stanisław	Stanisław	k1gMnSc1	Stanisław
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
bystrým	bystrý	k2eAgNnSc7d1	bystré
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
číst	číst	k5eAaImF	číst
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgInS	naučit
již	již	k9	již
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
4	[number]	k4	4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
si	se	k3xPyFc3	se
prohlížel	prohlížet	k5eAaImAgMnS	prohlížet
otcovy	otcův	k2eAgInPc4d1	otcův
anatomické	anatomický	k2eAgInPc4d1	anatomický
atlasy	atlas	k1gInPc4	atlas
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
četl	číst	k5eAaImAgMnS	číst
knihy	kniha	k1gFnPc4	kniha
známých	známý	k2eAgMnPc2d1	známý
autorů	autor	k1gMnPc2	autor
jako	jako	k9	jako
Karl	Karl	k1gMnSc1	Karl
May	May	k1gMnSc1	May
<g/>
,	,	kIx,	,
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
a	a	k8xC	a
Rudyard	Rudyard	k1gMnSc1	Rudyard
Kipling	Kipling	k1gInSc1	Kipling
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Stanisława	Stanisławus	k1gMnSc2	Stanisławus
Zołkiewského	Zołkiewský	k1gMnSc2	Zołkiewský
a	a	k8xC	a
poté	poté	k6eAd1	poté
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
Karla	Karel	k1gMnSc2	Karel
Szajnochy	Szajnoch	k1gMnPc4	Szajnoch
<g/>
.	.	kIx.	.
</s>
<s>
Stanisław	Stanisław	k?	Stanisław
Lem	lem	k1gInSc1	lem
byl	být	k5eAaImAgMnS	být
spíše	spíše	k9	spíše
samotář	samotář	k1gMnSc1	samotář
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
silnější	silný	k2eAgFnSc4d2	silnější
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souviselo	souviset	k5eAaImAgNnS	souviset
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
náklonností	náklonnost	k1gFnSc7	náklonnost
k	k	k7c3	k
cukroví	cukroví	k1gNnSc3	cukroví
-	-	kIx~	-
za	za	k7c4	za
bonbony	bonbon	k1gInPc4	bonbon
utrácel	utrácet	k5eAaImAgInS	utrácet
většinu	většina	k1gFnSc4	většina
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
příležitostně	příležitostně	k6eAd1	příležitostně
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgMnS	začít
kupovat	kupovat	k5eAaImF	kupovat
různá	různý	k2eAgNnPc1d1	různé
mechanická	mechanický	k2eAgNnPc1d1	mechanické
a	a	k8xC	a
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
vášeň	vášeň	k1gFnSc1	vášeň
pro	pro	k7c4	pro
konstruktérství	konstruktérství	k1gNnSc4	konstruktérství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
jej	on	k3xPp3gMnSc4	on
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
otcův	otcův	k2eAgMnSc1d1	otcův
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
mu	on	k3xPp3gMnSc3	on
půjčoval	půjčovat	k5eAaImAgMnS	půjčovat
technické	technický	k2eAgFnPc4d1	technická
encyklopedie	encyklopedie	k1gFnPc4	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
letech	léto	k1gNnPc6	léto
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
psací	psací	k2eAgInSc4d1	psací
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaPmAgMnS	napsat
svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
dostal	dostat	k5eAaPmAgMnS	dostat
originální	originální	k2eAgInSc4d1	originální
nápad	nápad	k1gInSc4	nápad
-	-	kIx~	-
vytvářet	vytvářet	k5eAaImF	vytvářet
si	se	k3xPyFc3	se
legitimace	legitimace	k1gFnPc4	legitimace
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
vyrábět	vyrábět	k5eAaImF	vyrábět
všelijaké	všelijaký	k3yIgInPc4	všelijaký
průkazy	průkaz	k1gInPc4	průkaz
a	a	k8xC	a
doklady	doklad	k1gInPc4	doklad
<g/>
,	,	kIx,	,
opatřoval	opatřovat	k5eAaImAgMnS	opatřovat
je	být	k5eAaImIp3nS	být
razítky	razítko	k1gNnPc7	razítko
a	a	k8xC	a
pečetěmi	pečeť	k1gFnPc7	pečeť
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
si	se	k3xPyFc3	se
tak	tak	k8xC	tak
svůj	svůj	k3xOyFgInSc4	svůj
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vymýšlet	vymýšlet	k5eAaImF	vymýšlet
neexistující	existující	k2eNgFnPc4d1	neexistující
skutečnosti	skutečnost	k1gFnPc4	skutečnost
a	a	k8xC	a
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
momentě	moment	k1gInSc6	moment
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
počátky	počátek	k1gInPc4	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
dobře	dobře	k6eAd1	dobře
prospíval	prospívat	k5eAaImAgInS	prospívat
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
slohové	slohový	k2eAgFnSc2d1	slohová
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
mu	on	k3xPp3gNnSc3	on
činila	činit	k5eAaImAgFnS	činit
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tu	tu	k6eAd1	tu
krátce	krátce	k6eAd1	krátce
učil	učít	k5eAaPmAgMnS	učít
i	i	k9	i
známý	známý	k2eAgMnSc1d1	známý
filosof	filosof	k1gMnSc1	filosof
Roman	Roman	k1gMnSc1	Roman
Ingarden	Ingardno	k1gNnPc2	Ingardno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
během	během	k7c2	během
okupace	okupace	k1gFnSc2	okupace
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
oblíbenými	oblíbený	k2eAgMnPc7d1	oblíbený
básníky	básník	k1gMnPc7	básník
byli	být	k5eAaImAgMnP	být
Bolesław	Bolesław	k1gMnSc1	Bolesław
Leśmian	Leśmian	k1gMnSc1	Leśmian
a	a	k8xC	a
Rainer	Rainer	k1gMnSc1	Rainer
Maria	Mario	k1gMnSc2	Mario
Rilke	Rilke	k1gNnSc2	Rilke
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Sověty	Sovět	k1gMnPc7	Sovět
okupovaném	okupovaný	k2eAgInSc6d1	okupovaný
Lvově	Lvov	k1gInSc6	Lvov
se	se	k3xPyFc4	se
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
lvovskou	lvovský	k2eAgFnSc4d1	Lvovská
polytechniku	polytechnika	k1gFnSc4	polytechnika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přestože	přestože	k8xS	přestože
složil	složit	k5eAaPmAgMnS	složit
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
buržoaznímu	buržoazní	k2eAgInSc3d1	buržoazní
původu	původ	k1gInSc3	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
otcovým	otcův	k2eAgFnPc3d1	otcova
známostem	známost	k1gFnPc3	známost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zahájit	zahájit	k5eAaPmF	zahájit
studia	studio	k1gNnPc4	studio
na	na	k7c6	na
Lvovské	lvovský	k2eAgFnSc6d1	Lvovská
lékařské	lékařský	k2eAgFnSc6d1	lékařská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
přerušila	přerušit	k5eAaPmAgFnS	přerušit
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
automechanik	automechanik	k1gMnSc1	automechanik
a	a	k8xC	a
svářeč	svářeč	k1gMnSc1	svářeč
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Lvova	Lvov	k1gInSc2	Lvov
opět	opět	k6eAd1	opět
vkročila	vkročit	k5eAaPmAgFnS	vkročit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přesun	přesun	k1gInSc1	přesun
do	do	k7c2	do
Krakowa	Krakow	k1gInSc2	Krakow
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lvov	Lvov	k1gInSc1	Lvov
bude	být	k5eAaImBp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
Lemovi	Lemův	k2eAgMnPc1d1	Lemův
rodiče	rodič	k1gMnPc1	rodič
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
zvolili	zvolit	k5eAaPmAgMnP	zvolit
odjezd	odjezd	k1gInSc4	odjezd
do	do	k7c2	do
Krakowa	Krakow	k1gInSc2	Krakow
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k1gMnSc1	mladý
Stanisław	Stanisław	k1gFnSc2	Stanisław
Lem	lem	k1gInSc1	lem
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
medicínu	medicína	k1gFnSc4	medicína
na	na	k7c6	na
Jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
však	však	k9	však
nepřistoupil	přistoupit	k5eNaPmAgMnS	přistoupit
k	k	k7c3	k
závěrečné	závěrečný	k2eAgFnSc3d1	závěrečná
zkoušce	zkouška	k1gFnSc3	zkouška
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
službě	služba	k1gFnSc6	služba
vojenského	vojenský	k2eAgMnSc4d1	vojenský
lékaře	lékař	k1gMnSc4	lékař
<g/>
,	,	kIx,	,
a	a	k8xC	a
obdržel	obdržet	k5eAaPmAgMnS	obdržet
pouze	pouze	k6eAd1	pouze
diplom	diplom	k1gInSc4	diplom
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukončil	ukončit	k5eAaPmAgInS	ukončit
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
lékařský	lékařský	k2eAgInSc4d1	lékařský
diplom	diplom	k1gInSc4	diplom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
beletrii	beletrie	k1gFnSc4	beletrie
<g/>
,	,	kIx,	,
vyšly	vyjít	k5eAaPmAgFnP	vyjít
mu	on	k3xPp3gMnSc3	on
první	první	k4xOgFnSc2	první
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
novela	novela	k1gFnSc1	novela
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
Marťan	Marťan	k1gMnSc1	Marťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
mladší	mladý	k2eAgMnSc1d2	mladší
asistent	asistent	k1gMnSc1	asistent
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
semináři	seminář	k1gInSc6	seminář
vedeném	vedený	k2eAgInSc6d1	vedený
doktorem	doktor	k1gMnSc7	doktor
Mieczysławem	Mieczysławum	k1gNnSc7	Mieczysławum
Choynowskim	Choynowskim	k1gMnSc1	Choynowskim
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
Lema	Lema	k1gMnSc1	Lema
nutil	nutit	k5eAaImAgMnS	nutit
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
půjčoval	půjčovat	k5eAaImAgMnS	půjčovat
mu	on	k3xPp3gMnSc3	on
knihy	kniha	k1gFnSc2	kniha
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Stanisława	Stanisława	k6eAd1	Stanisława
velmi	velmi	k6eAd1	velmi
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
dílo	dílo	k1gNnSc1	dílo
Kybernetika	kybernetik	k1gMnSc2	kybernetik
aneb	aneb	k?	aneb
Řízení	řízení	k1gNnSc1	řízení
a	a	k8xC	a
sdělování	sdělování	k1gNnSc1	sdělování
u	u	k7c2	u
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
strojů	stroj	k1gInPc2	stroj
Norberta	Norbert	k1gMnSc2	Norbert
Wienera	Wiener	k1gMnSc2	Wiener
<g/>
.	.	kIx.	.
</s>
<s>
Lemův	Lemův	k2eAgInSc1d1	Lemův
výrazný	výrazný	k2eAgInSc1d1	výrazný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
kybernetiku	kybernetika	k1gFnSc4	kybernetika
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c6	v
napsání	napsání	k1gNnSc6	napsání
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
v	v	k7c6	v
nepatrném	patrný	k2eNgInSc6d1	patrný
nákladu	náklad	k1gInSc6	náklad
s	s	k7c7	s
plným	plný	k2eAgInSc7d1	plný
titulem	titul	k1gInSc7	titul
<g/>
:	:	kIx,	:
Dialogy	dialog	k1gInPc1	dialog
o	o	k7c6	o
atomovém	atomový	k2eAgNnSc6d1	atomové
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc6	zmrtvýchvstání
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc6	teorie
nemožnosti	nemožnost	k1gFnSc6	nemožnost
<g/>
,	,	kIx,	,
filosofických	filosofický	k2eAgInPc6d1	filosofický
přínosech	přínos	k1gInPc6	přínos
kanibalismu	kanibalismus	k1gInSc2	kanibalismus
<g/>
,	,	kIx,	,
smutku	smutek	k1gInSc2	smutek
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
,	,	kIx,	,
kybernetické	kybernetický	k2eAgFnSc6d1	kybernetická
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnSc6d1	elektrická
reinkarnaci	reinkarnace	k1gFnSc4	reinkarnace
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
zpětných	zpětný	k2eAgFnPc6d1	zpětná
vazbách	vazba	k1gFnPc6	vazba
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
kybernetické	kybernetický	k2eAgFnSc6d1	kybernetická
eschatologii	eschatologie	k1gFnSc6	eschatologie
<g/>
,	,	kIx,	,
charakteru	charakter	k1gInSc6	charakter
elektrických	elektrický	k2eAgFnPc2d1	elektrická
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
falešnosti	falešnost	k1gFnPc1	falešnost
elektromozků	elektromozek	k1gInPc2	elektromozek
<g/>
,	,	kIx,	,
věčném	věčný	k2eAgInSc6d1	věčný
životě	život	k1gInSc6	život
v	v	k7c6	v
bedně	bedna	k1gFnSc6	bedna
<g/>
,	,	kIx,	,
konstruování	konstruování	k1gNnSc1	konstruování
géniů	génius	k1gMnPc2	génius
<g/>
,	,	kIx,	,
epilepsii	epilepsie	k1gFnSc4	epilepsie
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
řídících	řídící	k2eAgInPc6d1	řídící
strojích	stroj	k1gInPc6	stroj
<g/>
,	,	kIx,	,
projektování	projektování	k1gNnSc6	projektování
společenských	společenský	k2eAgInPc2d1	společenský
systémů	systém	k1gInPc2	systém
od	od	k7c2	od
Stanisława	Stanisław	k1gInSc2	Stanisław
Lema	Lem	k1gInSc2	Lem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Barbarou	Barbara	k1gFnSc7	Barbara
Leśniakovou	Leśniakový	k2eAgFnSc7d1	Leśniakový
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
začali	začít	k5eAaPmAgMnP	začít
jezdit	jezdit	k5eAaImF	jezdit
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
Stanisław	Stanisław	k1gFnSc2	Stanisław
Lem	lem	k1gInSc1	lem
byl	být	k5eAaImAgInS	být
řadovým	řadový	k2eAgInSc7d1	řadový
členem	člen	k1gInSc7	člen
Svazu	svaz	k1gInSc2	svaz
polských	polský	k2eAgMnPc2d1	polský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
během	během	k7c2	během
výjezdů	výjezd	k1gInPc2	výjezd
pořádaných	pořádaný	k2eAgNnPc2d1	pořádané
Svazem	svaz	k1gInSc7	svaz
navštívili	navštívit	k5eAaPmAgMnP	navštívit
manželé	manžel	k1gMnPc1	manžel
např.	např.	kA	např.
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
NDR	NDR	kA	NDR
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lem	lem	k1gInSc1	lem
trpěl	trpět	k5eAaImAgMnS	trpět
sennou	senný	k2eAgFnSc7d1	senná
rýmou	rýma	k1gFnSc7	rýma
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jezdíval	jezdívat	k5eAaImAgInS	jezdívat
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
do	do	k7c2	do
polského	polský	k2eAgNnSc2d1	polské
podtatranského	podtatranský	k2eAgNnSc2d1	Podtatranské
města	město	k1gNnSc2	město
Zakopane	Zakopan	k1gMnSc5	Zakopan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
věnoval	věnovat	k5eAaPmAgMnS	věnovat
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
sem	sem	k6eAd1	sem
jezdil	jezdit	k5eAaImAgMnS	jezdit
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
lyžovat	lyžovat	k5eAaImF	lyžovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Lemovým	lemový	k2eAgFnPc3d1	lemová
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Tomasz	Tomasz	k1gMnSc1	Tomasz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
navázal	navázat	k5eAaPmAgInS	navázat
Stanisław	Stanisław	k1gFnSc2	Stanisław
Lem	lem	k1gInSc1	lem
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
vydavatelem	vydavatel	k1gMnSc7	vydavatel
časopisu	časopis	k1gInSc2	časopis
Quarber	Quarber	k1gMnSc1	Quarber
Merkur	Merkur	k1gMnSc1	Merkur
Franzem	Franz	k1gMnSc7	Franz
Rottensteinerem	Rottensteiner	k1gMnSc7	Rottensteiner
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
úzká	úzký	k2eAgFnSc1d1	úzká
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
Rottensteiner	Rottensteiner	k1gInSc1	Rottensteiner
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Lemovým	lemový	k2eAgMnSc7d1	lemový
významným	významný	k2eAgMnSc7d1	významný
propagátorem	propagátor	k1gMnSc7	propagátor
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
jazykovém	jazykový	k2eAgNnSc6d1	jazykové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
<g/>
Lem	lem	k1gInSc1	lem
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
populárním	populární	k2eAgMnSc6d1	populární
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
např.	např.	kA	např.
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
vyšel	vyjít	k5eAaPmAgInS	vyjít
román	román	k1gInSc4	román
Solaris	Solaris	k1gFnSc2	Solaris
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Polské	polský	k2eAgFnSc2d1	polská
astronautické	astronautický	k2eAgFnSc2d1	astronautická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Polské	polský	k2eAgFnSc2d1	polská
kybernetické	kybernetický	k2eAgFnSc2d1	kybernetická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
probíhala	probíhat	k5eAaImAgNnP	probíhat
Polskem	Polsko	k1gNnSc7	Polsko
vlna	vlna	k1gFnSc1	vlna
stávek	stávka	k1gFnPc2	stávka
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
odborovým	odborový	k2eAgNnSc7d1	odborové
hnutím	hnutí	k1gNnSc7	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1981	[number]	k4	1981
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgNnPc1d1	občanské
práva	právo	k1gNnPc1	právo
byla	být	k5eAaImAgNnP	být
potlačena	potlačit	k5eAaPmNgNnP	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Lem	lem	k1gInSc1	lem
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
opustil	opustit	k5eAaPmAgMnS	opustit
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
literárním	literární	k2eAgInSc6d1	literární
institutu	institut	k1gInSc6	institut
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
sci-fi	scii	k1gNnPc2	sci-fi
příběhům	příběh	k1gInPc3	příběh
prokládaným	prokládaný	k2eAgFnPc3d1	prokládaná
filosofickými	filosofický	k2eAgFnPc7d1	filosofická
úvahami	úvaha	k1gFnPc7	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
publicista	publicista	k1gMnSc1	publicista
Stanisław	Stanisław	k1gMnSc1	Stanisław
Bereś	Bereś	k1gMnSc1	Bereś
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
s	s	k7c7	s
Lemem	lem	k1gInSc7	lem
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
později	pozdě	k6eAd2	pozdě
shrnul	shrnout	k5eAaPmAgInS	shrnout
do	do	k7c2	do
publikace	publikace	k1gFnSc2	publikace
Hovory	hovor	k1gInPc1	hovor
se	s	k7c7	s
Stanisławem	Stanisław	k1gMnSc7	Stanisław
Lemem	lem	k1gInSc7	lem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lem	lem	k1gInSc1	lem
se	se	k3xPyFc4	se
nezřekl	zřeknout	k5eNaPmAgInS	zřeknout
své	svůj	k3xOyFgFnPc4	svůj
vlasti	vlast	k1gFnPc4	vlast
nadobro	nadobro	k6eAd1	nadobro
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Krakowa	Krakow	k1gInSc2	Krakow
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
84	[number]	k4	84
let	léto	k1gNnPc2	léto
na	na	k7c4	na
srdeční	srdeční	k2eAgNnSc4d1	srdeční
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
hřbitově	hřbitov	k1gInSc6	hřbitov
Cmentarz	Cmentarz	k1gMnSc1	Cmentarz
Salwatorski	Salwatorsk	k1gFnSc2	Salwatorsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
Lemova	Lemův	k2eAgNnSc2d1	Lemův
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
prvotinu	prvotina	k1gFnSc4	prvotina
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
dílo	dílo	k1gNnSc4	dílo
své	svůj	k3xOyFgFnSc2	svůj
rané	raný	k2eAgFnSc2d1	raná
tvorby	tvorba	k1gFnSc2	tvorba
považoval	považovat	k5eAaImAgMnS	považovat
Stanisław	Stanisław	k1gMnSc1	Stanisław
Lem	lem	k1gInSc4	lem
Dům	dům	k1gInSc1	dům
proměnění	proměnění	k1gNnSc1	proměnění
-	-	kIx~	-
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
třídílného	třídílný	k2eAgInSc2d1	třídílný
románu	román	k1gInSc2	román
Nepromarněný	promarněný	k2eNgInSc4d1	Nepromarněný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
u	u	k7c2	u
vážných	vážný	k2eAgNnPc2d1	vážné
soudobých	soudobý	k2eAgNnPc2d1	soudobé
témat	téma	k1gNnPc2	téma
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
,	,	kIx,	,
nebýt	být	k5eNaImF	být
peripetií	peripetie	k1gFnSc7	peripetie
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
<g/>
Příklonu	příklon	k1gInSc2	příklon
k	k	k7c3	k
sci-fi	scii	k1gFnSc3	sci-fi
literatuře	literatura	k1gFnSc3	literatura
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Czytelnik	Czytelnik	k1gMnSc1	Czytelnik
J.	J.	kA	J.
Panskim	Panskim	k1gMnSc1	Panskim
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vydání	vydání	k1gNnSc4	vydání
knihy	kniha	k1gFnSc2	kniha
Astronauti	astronaut	k1gMnPc1	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Lem	lem	k1gInSc1	lem
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
knižním	knižní	k2eAgInSc6d1	knižní
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
mezera	mezera	k1gFnSc1	mezera
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
absencí	absence	k1gFnSc7	absence
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
literární	literární	k2eAgFnSc2d1	literární
odezvy	odezva	k1gFnSc2	odezva
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
nových	nový	k2eAgInPc2d1	nový
technologických	technologický	k2eAgInPc2d1	technologický
objevů	objev	k1gInPc2	objev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
atomová	atomový	k2eAgFnSc1d1	atomová
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
raketová	raketový	k2eAgFnSc1d1	raketová
technika	technika	k1gFnSc1	technika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
motiv	motiv	k1gInSc4	motiv
atomového	atomový	k2eAgInSc2d1	atomový
konfliktu	konflikt	k1gInSc2	konflikt
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
několika	několik	k4yIc6	několik
autorových	autorův	k2eAgInPc6d1	autorův
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
možného	možný	k2eAgNnSc2d1	možné
zneužití	zneužití	k1gNnSc2	zneužití
těchto	tento	k3xDgFnPc2	tento
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
výrazným	výrazný	k2eAgInSc7d1	výrazný
rysem	rys	k1gInSc7	rys
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
je	být	k5eAaImIp3nS	být
užití	užití	k1gNnSc1	užití
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
četnosti	četnost	k1gFnSc2	četnost
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
motiv	motiv	k1gInSc1	motiv
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
důkladných	důkladný	k2eAgFnPc6d1	důkladná
znalostech	znalost	k1gFnPc6	znalost
datujících	datující	k2eAgFnPc6d1	datující
se	se	k3xPyFc4	se
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
seznámení	seznámení	k1gNnSc2	seznámení
se	se	k3xPyFc4	se
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
Kybernetika	kybernetik	k1gMnSc2	kybernetik
aneb	aneb	k?	aneb
Řízení	řízení	k1gNnSc1	řízení
a	a	k8xC	a
sdělování	sdělování	k1gNnSc1	sdělování
u	u	k7c2	u
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
strojů	stroj	k1gInPc2	stroj
Norberta	Norbert	k1gMnSc2	Norbert
Wienera	Wiener	k1gMnSc2	Wiener
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
kybernetické	kybernetický	k2eAgInPc1d1	kybernetický
mozky	mozek	k1gInPc1	mozek
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
předčí	předčit	k5eAaBmIp3nP	předčit
lidské	lidský	k2eAgFnPc1d1	lidská
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
konstantám	konstanta	k1gFnPc3	konstanta
Lemových	lemový	k2eAgMnPc2d1	lemový
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gNnSc1	jeho
nejslavnější	slavný	k2eAgNnSc1d3	nejslavnější
díla	dílo	k1gNnPc1	dílo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvotního	prvotní	k2eAgNnSc2d1	prvotní
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
i	i	k9	i
vynuceného	vynucený	k2eAgInSc2d1	vynucený
komunistickými	komunistický	k2eAgInPc7d1	komunistický
programy	program	k1gInPc7	program
<g/>
)	)	kIx)	)
socialistickým	socialistický	k2eAgInSc7d1	socialistický
realismem	realismus	k1gInSc7	realismus
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
postupně	postupně	k6eAd1	postupně
stávala	stávat	k5eAaImAgFnS	stávat
kombinací	kombinace	k1gFnSc7	kombinace
čistokrevné	čistokrevný	k2eAgFnSc2d1	čistokrevná
technologické	technologický	k2eAgFnSc2d1	technologická
sci-fi	scii	k1gFnSc2	sci-fi
a	a	k8xC	a
filozofických	filozofický	k2eAgFnPc2d1	filozofická
úvah	úvaha	k1gFnPc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
Lemova	Lemov	k1gInSc2	Lemov
díla	dílo	k1gNnSc2	dílo
jsou	být	k5eAaImIp3nP	být
sci-fi	scii	k1gFnPc4	sci-fi
bajky	bajka	k1gFnPc4	bajka
a	a	k8xC	a
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
kontrastují	kontrastovat	k5eAaImIp3nP	kontrastovat
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
vážnými	vážný	k2eAgInPc7d1	vážný
a	a	k8xC	a
přemítavými	přemítavý	k2eAgInPc7d1	přemítavý
romány	román	k1gInPc7	román
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
s	s	k7c7	s
žánrem	žánr	k1gInSc7	žánr
vědecké	vědecký	k2eAgFnSc2d1	vědecká
beletrie	beletrie	k1gFnSc2	beletrie
sdílí	sdílet	k5eAaImIp3nS	sdílet
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jenom	jenom	k6eAd1	jenom
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
podstatou	podstata	k1gFnSc7	podstata
a	a	k8xC	a
též	též	k9	též
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
Lem	lem	k1gInSc4	lem
psal	psát	k5eAaImAgInS	psát
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
bude	být	k5eAaImBp3nS	být
autorova	autorův	k2eAgFnSc1d1	autorova
potřeba	potřeba	k1gFnSc1	potřeba
odpočinutí	odpočinutí	k1gNnSc2	odpočinutí
<g/>
,	,	kIx,	,
uvolnění	uvolnění	k1gNnSc3	uvolnění
a	a	k8xC	a
odlehčení	odlehčení	k1gNnSc3	odlehčení
jeho	jeho	k3xOp3gFnPc2	jeho
často	často	k6eAd1	často
tíživých	tíživý	k2eAgFnPc2d1	tíživá
<g/>
,	,	kIx,	,
přemítavých	přemítavý	k2eAgFnPc2d1	přemítavá
<g/>
,	,	kIx,	,
filosoficky	filosoficky	k6eAd1	filosoficky
laděných	laděný	k2eAgNnPc2d1	laděné
sci-fi	scii	k1gNnPc2	sci-fi
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Lemových	lemový	k2eAgNnPc6d1	lemový
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevují	objevovat	k5eAaImIp3nP	objevovat
dvě	dva	k4xCgFnPc1	dva
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
později	pozdě	k6eAd2	pozdě
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
pilot	pilot	k1gMnSc1	pilot
Pirx	Pirx	k1gInSc1	Pirx
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
čtenáři	čtenář	k1gMnPc1	čtenář
překonávají	překonávat	k5eAaImIp3nP	překonávat
různé	různý	k2eAgFnPc4d1	různá
technické	technický	k2eAgFnPc4d1	technická
nástrahy	nástraha	k1gFnPc4	nástraha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ijon	Ijon	k1gMnSc1	Ijon
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
kosmických	kosmický	k2eAgFnPc6d1	kosmická
výpravách	výprava	k1gFnPc6	výprava
navazuje	navazovat	k5eAaImIp3nS	navazovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
mimozemskými	mimozemský	k2eAgFnPc7d1	mimozemská
civilizacemi	civilizace	k1gFnPc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
deníků	deník	k1gInPc2	deník
Ijona	Ijon	k1gMnSc2	Ijon
Tichého	Tichý	k1gMnSc2	Tichý
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
baronu	baron	k1gMnSc3	baron
Prášilovi	Prášil	k1gMnSc3	Prášil
<g/>
)	)	kIx)	)
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
autor	autor	k1gMnSc1	autor
ironické	ironický	k2eAgNnSc1d1	ironické
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
lidské	lidský	k2eAgNnSc1d1	lidské
civilizaci	civilizace	k1gFnSc3	civilizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
několika	několik	k4yIc6	několik
Lemových	lemový	k2eAgNnPc6d1	lemový
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vysledovat	vysledovat	k5eAaPmF	vysledovat
společný	společný	k2eAgInSc4d1	společný
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
téma	téma	k1gNnSc1	téma
kontaktu	kontakt	k1gInSc2	kontakt
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
mimozemskou	mimozemský	k2eAgFnSc7d1	mimozemská
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
obtížnost	obtížnost	k1gFnSc1	obtížnost
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
neschopnost	neschopnost	k1gFnSc1	neschopnost
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
nemožnost	nemožnost	k1gFnSc1	nemožnost
jeho	jeho	k3xOp3gNnPc2	jeho
navázání	navázání	k1gNnSc2	navázání
<g/>
,	,	kIx,	,
dané	daný	k2eAgNnSc1d1	dané
přílišnou	přílišný	k2eAgFnSc7d1	přílišná
odlišností	odlišnost	k1gFnSc7	odlišnost
mezi	mezi	k7c7	mezi
pozemským	pozemský	k2eAgInSc7d1	pozemský
a	a	k8xC	a
mimozemským	mimozemský	k2eAgInSc7d1	mimozemský
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
televizních	televizní	k2eAgFnPc2d1	televizní
inscenací	inscenace	k1gFnPc2	inscenace
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgInPc2d1	filmový
scénářů	scénář	k1gInPc2	scénář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Romány	román	k1gInPc4	román
===	===	k?	===
</s>
</p>
<p>
<s>
Marťan	Marťan	k1gMnSc1	Marťan
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Człowiek	Człowiek	k6eAd1	Człowiek
z	z	k7c2	z
Marsa	Mars	k1gMnSc2	Mars
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
raná	raný	k2eAgFnSc1d1	raná
novela	novela	k1gFnSc1	novela
o	o	k7c4	o
přistání	přistání	k1gNnSc4	přistání
mimozemského	mimozemský	k2eAgMnSc2d1	mimozemský
návštěvníka	návštěvník	k1gMnSc2	návštěvník
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Astronauti	astronaut	k1gMnPc1	astronaut
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Astronauci	Astronauk	k1gMnPc1	Astronauk
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
/	/	kIx~	/
<g/>
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
nalezen	nalezen	k2eAgInSc1d1	nalezen
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
explodovala	explodovat	k5eAaBmAgFnS	explodovat
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
tunguzskou	tunguzský	k2eAgFnSc4d1	Tunguzská
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
je	být	k5eAaImIp3nS	být
dešifrován	dešifrován	k2eAgInSc1d1	dešifrován
a	a	k8xC	a
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
raketa	raketa	k1gFnSc1	raketa
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
z	z	k7c2	z
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
vyslána	vyslán	k2eAgFnSc1d1	vyslána
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
pilot	pilot	k1gMnSc1	pilot
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
lidé	člověk	k1gMnPc1	člověk
naleznou	nalézt	k5eAaBmIp3nP	nalézt
stopy	stopa	k1gFnPc4	stopa
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chtěla	chtít	k5eAaImAgFnS	chtít
provést	provést	k5eAaPmF	provést
invazi	invaze	k1gFnSc3	invaze
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kvůli	kvůli	k7c3	kvůli
atomovému	atomový	k2eAgInSc3d1	atomový
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepromarněný	promarněný	k2eNgInSc1d1	Nepromarněný
čas	čas	k1gInSc1	čas
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Czas	Czas	k1gInSc1	Czas
nieutracony	nieutracona	k1gFnSc2	nieutracona
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třídílný	třídílný	k2eAgInSc1d1	třídílný
román	román	k1gInSc1	román
z	z	k7c2	z
válečného	válečný	k2eAgNnSc2d1	válečné
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
Dům	dům	k1gInSc1	dům
proměnění	proměnění	k1gNnSc2	proměnění
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobývá	pobývat	k5eAaImIp3nS	pobývat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Třinecký	třinecký	k2eAgMnSc1d1	třinecký
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
ústav	ústava	k1gFnPc2	ústava
zničí	zničit	k5eAaPmIp3nP	zničit
a	a	k8xC	a
pacienty	pacient	k1gMnPc7	pacient
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
Mezi	mezi	k7c7	mezi
mrtvými	mrtvý	k1gMnPc7	mrtvý
začíná	začínat	k5eAaImIp3nS	začínat
biografií	biografie	k1gFnSc7	biografie
venkovského	venkovský	k2eAgMnSc4d1	venkovský
chlapce	chlapec	k1gMnSc4	chlapec
Wilka	Wilek	k1gMnSc4	Wilek
s	s	k7c7	s
geniálním	geniální	k2eAgNnSc7d1	geniální
nadáním	nadání	k1gNnSc7	nadání
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zapojí	zapojit	k5eAaPmIp3nS	zapojit
do	do	k7c2	do
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Třinecký	třinecký	k2eAgMnSc1d1	třinecký
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
při	při	k7c6	při
protižidovském	protižidovský	k2eAgInSc6d1	protižidovský
pogromu	pogrom	k1gInSc6	pogrom
je	být	k5eAaImIp3nS	být
též	též	k9	též
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
Návrat	návrat	k1gInSc1	návrat
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
do	do	k7c2	do
bezprostředního	bezprostřední	k2eAgNnSc2d1	bezprostřední
poválečného	poválečný	k2eAgNnSc2d1	poválečné
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
protagonisté	protagonista	k1gMnPc1	protagonista
předchozích	předchozí	k2eAgInPc2d1	předchozí
dílů	díl	k1gInPc2	díl
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
divokého	divoký	k2eAgNnSc2d1	divoké
poválečného	poválečný	k2eAgNnSc2d1	poválečné
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
Mrakům	mrak	k1gInPc3	mrak
Magellanovým	Magellanův	k2eAgInPc3d1	Magellanův
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Obłok	Obłok	k1gInSc1	Obłok
Magellana	Magellana	k1gFnSc1	Magellana
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
románu	román	k1gInSc2	román
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
filmu	film	k1gInSc6	film
Ikarie	Ikarie	k1gFnSc2	Ikarie
XB	XB	kA	XB
1	[number]	k4	1
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
Eden	Eden	k1gInSc1	Eden
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Eden	Eden	k1gInSc1	Eden
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cizí	cizí	k2eAgFnSc6d1	cizí
planetě	planeta	k1gFnSc6	planeta
nouzově	nouzově	k6eAd1	nouzově
přistane	přistat	k5eAaPmIp3nS	přistat
pozemská	pozemský	k2eAgFnSc1d1	pozemská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
posádka	posádka	k1gFnSc1	posádka
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
nepochopitelnými	pochopitelný	k2eNgInPc7d1	nepochopitelný
jevy	jev	k1gInPc7	jev
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
navázání	navázání	k1gNnSc4	navázání
kontaktu	kontakt	k1gInSc2	kontakt
se	se	k3xPyFc4	se
zdaří	zdařit	k5eAaPmIp3nS	zdařit
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Sledzstwo	Sledzstwo	k6eAd1	Sledzstwo
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
s	s	k7c7	s
detektivními	detektivní	k2eAgInPc7d1	detektivní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Inspektor	inspektor	k1gMnSc1	inspektor
Gregory	Gregor	k1gMnPc4	Gregor
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
záhadné	záhadný	k2eAgInPc4d1	záhadný
přesuny	přesun	k1gInPc4	přesun
mrtvol	mrtvola	k1gFnPc2	mrtvola
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
veškeré	veškerý	k3xTgInPc4	veškerý
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
logické	logický	k2eAgNnSc4d1	logické
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
selhávají	selhávat	k5eAaImIp3nP	selhávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Deník	deník	k1gInSc1	deník
nalezený	nalezený	k2eAgInSc1d1	nalezený
ve	v	k7c6	v
vaně	vana	k1gFnSc6	vana
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Pamiętnik	Pamiętnik	k1gInSc1	Pamiętnik
znaleziony	znalezion	k1gInPc1	znalezion
w	w	k?	w
wanie	wanie	k1gFnSc2	wanie
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
globální	globální	k2eAgFnSc6d1	globální
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgFnSc6d1	způsobená
zánikem	zánik	k1gInSc7	zánik
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
protiatomovém	protiatomový	k2eAgInSc6d1	protiatomový
krytu	kryt	k1gInSc6	kryt
nalezeny	naleznout	k5eAaPmNgInP	naleznout
nějaké	nějaký	k3yIgInPc1	nějaký
zápisky	zápisek	k1gInPc1	zápisek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krytu	kryt	k1gInSc6	kryt
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
mikrosvět	mikrosvět	k1gInSc1	mikrosvět
s	s	k7c7	s
paranoidní	paranoidní	k2eAgFnSc7d1	paranoidní
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Powrót	Powrót	k2eAgInSc1d1	Powrót
z	z	k7c2	z
gwiazd	gwiazda	k1gFnPc2	gwiazda
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
pilota	pilot	k1gMnSc2	pilot
hvězdoletu	hvězdolet	k1gInSc2	hvězdolet
vrátivšího	vrátivší	k2eAgInSc2d1	vrátivší
se	se	k3xPyFc4	se
z	z	k7c2	z
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
mise	mise	k1gFnSc2	mise
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
líčí	líčit	k5eAaImIp3nP	líčit
jeho	jeho	k3xOp3gInPc4	jeho
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
nezvyklé	zvyklý	k2eNgNnSc4d1	nezvyklé
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Solaris	Solaris	k1gFnSc1	Solaris
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Solaris	Solaris	k1gFnSc2	Solaris
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
lidé	člověk	k1gMnPc1	člověk
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
inteligentní	inteligentní	k2eAgInSc4d1	inteligentní
oceán	oceán	k1gInSc4	oceán
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
kontaktu	kontakt	k1gInSc2	kontakt
naráží	narážet	k5eAaPmIp3nS	narážet
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
bariéry	bariéra	k1gFnPc4	bariéra
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
k	k	k7c3	k
duševnímu	duševní	k2eAgInSc3d1	duševní
rozvratu	rozvrat	k1gInSc3	rozvrat
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
třikrát	třikrát	k6eAd1	třikrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepřemožitelný	přemožitelný	k2eNgMnSc1d1	nepřemožitelný
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Niezwyciężony	Niezwyciężona	k1gFnPc4	Niezwyciężona
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdolet	Hvězdolet	k1gInSc1	Hvězdolet
Nepřemožitelný	přemožitelný	k2eNgInSc1d1	nepřemožitelný
je	být	k5eAaImIp3nS	být
vyslán	vyslán	k2eAgInSc1d1	vyslán
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
Regis	Regis	k1gFnSc2	Regis
III	III	kA	III
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
pátral	pátrat	k5eAaImAgMnS	pátrat
po	po	k7c6	po
ztracené	ztracený	k2eAgFnSc6d1	ztracená
sesterské	sesterský	k2eAgFnSc6d1	sesterská
lodi	loď	k1gFnSc6	loď
Kondor	kondor	k1gMnSc1	kondor
<g/>
.	.	kIx.	.
</s>
<s>
Nalezne	nalézt	k5eAaBmIp3nS	nalézt
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
pátrání	pátrání	k1gNnSc2	pátrání
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
lodě	loď	k1gFnSc2	loď
jsou	být	k5eAaImIp3nP	být
nepoškozené	poškozený	k2eNgInPc1d1	nepoškozený
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
posádka	posádka	k1gFnSc1	posádka
je	být	k5eAaImIp3nS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Zahynula	zahynout	k5eAaPmAgFnS	zahynout
za	za	k7c2	za
podivných	podivný	k2eAgFnPc2d1	podivná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
i	i	k9	i
místní	místní	k2eAgInSc4d1	místní
život	život	k1gInSc4	život
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
zničil	zničit	k5eAaPmAgInS	zničit
podivný	podivný	k2eAgInSc4d1	podivný
kovový	kovový	k2eAgInSc4d1	kovový
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
neorganickou	organický	k2eNgFnSc7d1	neorganická
evolucí	evoluce	k1gFnSc7	evoluce
z	z	k7c2	z
robotů	robot	k1gInPc2	robot
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
prioritou	priorita	k1gFnSc7	priorita
je	být	k5eAaImIp3nS	být
zničení	zničení	k1gNnSc4	zničení
všeho	všecek	k3xTgInSc2	všecek
organického	organický	k2eAgInSc2d1	organický
života	život	k1gInSc2	život
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pánův	pánův	k2eAgInSc1d1	pánův
hlas	hlas	k1gInSc1	hlas
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Glos	glosa	k1gFnPc2	glosa
Pana	Pan	k1gMnSc2	Pan
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Hogarth	Hogarth	k1gMnSc1	Hogarth
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
utajeném	utajený	k2eAgInSc6d1	utajený
výzkumu	výzkum	k1gInSc6	výzkum
zachycených	zachycený	k2eAgInPc2d1	zachycený
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
objevům	objev	k1gInPc3	objev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podstata	podstata	k1gFnSc1	podstata
stále	stále	k6eAd1	stále
uniká	unikat	k5eAaImIp3nS	unikat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rýma	rýma	k1gFnSc1	rýma
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Katar	katar	k1gInSc1	katar
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
s	s	k7c7	s
detektivními	detektivní	k2eAgInPc7d1	detektivní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
astronaut	astronaut	k1gMnSc1	astronaut
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
příčinách	příčina	k1gFnPc6	příčina
záhadných	záhadný	k2eAgFnPc2d1	záhadná
úmrtí	úmrť	k1gFnPc2	úmrť
amerických	americký	k2eAgMnPc2d1	americký
turistů	turist	k1gMnPc2	turist
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
imitaci	imitace	k1gFnSc6	imitace
jejich	jejich	k3xOp3gNnSc2	jejich
chování	chování	k1gNnSc2	chování
se	se	k3xPyFc4	se
málem	málem	k6eAd1	málem
sám	sám	k3xTgMnSc1	sám
stane	stanout	k5eAaPmIp3nS	stanout
obětí	oběť	k1gFnSc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Pachatel	pachatel	k1gMnSc1	pachatel
se	se	k3xPyFc4	se
nenajde	najít	k5eNaPmIp3nS	najít
<g/>
,	,	kIx,	,
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
je	být	k5eAaImIp3nS	být
označena	označen	k2eAgFnSc1d1	označena
kombinace	kombinace	k1gFnSc1	kombinace
řady	řada	k1gFnSc2	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wizja	Wizj	k2eAgFnSc1d1	Wizj
lokalna	lokalna	k1gFnSc1	lokalna
-	-	kIx~	-
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevyšla	vyjít	k5eNaPmAgFnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
název	název	k1gInSc4	název
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Místní	místní	k2eAgFnSc1d1	místní
vize	vize	k1gFnSc1	vize
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ijon	Ijon	k1gMnSc1	Ijon
Tichý	Tichý	k1gMnSc1	Tichý
navštíví	navštívit	k5eAaPmIp3nS	navštívit
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
ústav	ústav	k1gInSc4	ústav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
počítače	počítač	k1gInPc1	počítač
modelují	modelovat	k5eAaImIp3nP	modelovat
historický	historický	k2eAgInSc4d1	historický
i	i	k8xC	i
jazykový	jazykový	k2eAgInSc4d1	jazykový
vývoj	vývoj	k1gInSc4	vývoj
planety	planeta	k1gFnSc2	planeta
Encie	Encie	k1gFnSc2	Encie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Tichý	Tichý	k1gMnSc1	Tichý
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Vydá	vydat	k5eAaPmIp3nS	vydat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
ověřit	ověřit	k5eAaPmF	ověřit
tyto	tento	k3xDgFnPc4	tento
hypotézy	hypotéza	k1gFnPc4	hypotéza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
symbióze	symbióza	k1gFnSc6	symbióza
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
potomci	potomek	k1gMnPc1	potomek
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
zvířaty	zvíře	k1gNnPc7	zvíře
zvanými	zvaný	k2eAgFnPc7d1	zvaná
Kurdlové	Kurdlový	k2eAgFnSc3d1	Kurdlový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mír	mír	k1gInSc4	mír
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Pokój	Pokój	k1gInSc1	Pokój
na	na	k7c4	na
Ziemi	Zie	k1gFnPc7	Zie
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
odzbrojena	odzbrojen	k2eAgFnSc1d1	odzbrojen
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
zbraní	zbraň	k1gFnPc2	zbraň
je	být	k5eAaImIp3nS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
automaticky	automaticky	k6eAd1	automaticky
bez	bez	k7c2	bez
lidské	lidský	k2eAgFnSc2d1	lidská
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Ijon	Ijon	k1gMnSc1	Ijon
Tichý	Tichý	k1gMnSc1	Tichý
je	být	k5eAaImIp3nS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkontroloval	zkontrolovat	k5eAaPmAgMnS	zkontrolovat
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
však	však	k9	však
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
nemůže	moct	k5eNaImIp3nS	moct
vybavit	vybavit	k5eAaPmF	vybavit
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Začnou	začít	k5eAaPmIp3nP	začít
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
zajímat	zajímat	k5eAaImF	zajímat
tajné	tajný	k2eAgFnPc4d1	tajná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
samovývoji	samovývoj	k1gInSc3	samovývoj
počítačových	počítačový	k2eAgInPc2d1	počítačový
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Tichý	Tichý	k1gMnSc1	Tichý
zavlekl	zavleknout	k5eAaPmAgMnS	zavleknout
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
způsobily	způsobit	k5eAaPmAgFnP	způsobit
pořádný	pořádný	k2eAgInSc4d1	pořádný
chaos	chaos	k1gInSc4	chaos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fiasko	fiasko	k1gNnSc1	fiasko
-	-	kIx~	-
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Saturnově	Saturnův	k2eAgInSc6d1	Saturnův
měsíci	měsíc	k1gInSc6	měsíc
Titanu	titan	k1gInSc2	titan
přistane	přistat	k5eAaPmIp3nS	přistat
pilot	pilot	k1gMnSc1	pilot
Parvis	Parvis	k1gFnSc4	Parvis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
pátral	pátrat	k5eAaImAgMnS	pátrat
po	po	k7c6	po
zmizelých	zmizelý	k2eAgInPc6d1	zmizelý
lidech	lid	k1gInPc6	lid
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
po	po	k7c6	po
pilotu	pilot	k1gMnSc6	pilot
Pirxovi	Pirxa	k1gMnSc6	Pirxa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
plnění	plnění	k1gNnSc2	plnění
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
zmrazen	zmrazit	k5eAaPmNgInS	zmrazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	let	k1gInPc6	let
je	být	k5eAaImIp3nS	být
oživen	oživit	k5eAaPmNgInS	oživit
na	na	k7c4	na
kosmické	kosmický	k2eAgFnPc4d1	kosmická
lodi	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
letí	letět	k5eAaImIp3nS	letět
na	na	k7c4	na
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
planetu	planeta	k1gFnSc4	planeta
navázat	navázat	k5eAaPmF	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
nedávno	nedávno	k6eAd1	nedávno
zjištěnou	zjištěný	k2eAgFnSc7d1	zjištěná
mimozemskou	mimozemský	k2eAgFnSc7d1	mimozemská
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
kontakt	kontakt	k1gInSc4	kontakt
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vynucují	vynucovat	k5eAaImIp3nP	vynucovat
s	s	k7c7	s
katastrofálními	katastrofální	k2eAgInPc7d1	katastrofální
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sbírky	sbírka	k1gFnPc1	sbírka
povídek	povídka	k1gFnPc2	povídka
===	===	k?	===
</s>
</p>
<p>
<s>
Sezam	sezam	k1gInSc1	sezam
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Sezam	sezam	k1gInSc1	sezam
i	i	k8xC	i
inne	inne	k1gInSc1	inne
opowiadania	opowiadanium	k1gNnSc2	opowiadanium
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
6	[number]	k4	6
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
originálu	originál	k1gInSc6	originál
je	být	k5eAaImIp3nS	být
povídek	povídka	k1gFnPc2	povídka
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězdné	hvězdný	k2eAgInPc1d1	hvězdný
deníky	deník	k1gInPc1	deník
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Dzienniki	Dzienniki	k1gNnSc1	Dzienniki
gwiazdowe	gwiazdow	k1gFnSc2	gwiazdow
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
15	[number]	k4	15
cest	cesta	k1gFnPc2	cesta
vesmírného	vesmírný	k2eAgMnSc2d1	vesmírný
dobrodruha	dobrodruh	k1gMnSc2	dobrodruh
Ijona	Ijon	k1gMnSc4	Ijon
Tichého	Tichý	k1gMnSc4	Tichý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
naznačováno	naznačován	k2eAgNnSc1d1	naznačováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
omylem	omylem	k6eAd1	omylem
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
neopatrnosti	neopatrnost	k1gFnSc2	neopatrnost
mimozemšťanů	mimozemšťan	k1gMnPc2	mimozemšťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Invaze	invaze	k1gFnSc1	invaze
z	z	k7c2	z
Aldebaranu	Aldebaran	k1gInSc2	Aldebaran
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
Ijona	Ijon	k1gMnSc2	Ijon
Tichého	Tichý	k1gMnSc2	Tichý
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Księga	Księg	k1gMnSc2	Księg
robotów	robotów	k?	robotów
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
9	[number]	k4	9
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
polský	polský	k2eAgInSc1d1	polský
originál	originál	k1gInSc1	originál
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgFnPc4	některý
povídky	povídka	k1gFnPc4	povídka
navíc	navíc	k6eAd1	navíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bajky	bajka	k1gFnPc4	bajka
robotů	robot	k1gInPc2	robot
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Bajki	Bajki	k1gNnSc1	Bajki
robotów	robotów	k?	robotów
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
14	[number]	k4	14
povídek	povídka	k1gFnPc2	povídka
<g/>
:	:	kIx,	:
Tři	tři	k4xCgMnPc1	tři
elektrytíři	elektrytíř	k1gMnPc1	elektrytíř
<g/>
;	;	kIx,	;
Uranové	Uran	k1gMnPc1	Uran
uši	ucho	k1gNnPc1	ucho
<g/>
;	;	kIx,	;
Jak	jak	k8xS	jak
Erg	erg	k1gInSc1	erg
Transformátor	transformátor	k1gMnSc1	transformátor
bleďocha	bleďoch	k1gMnSc4	bleďoch
přemohl	přemoct	k5eAaPmAgMnS	přemoct
<g/>
;	;	kIx,	;
Dva	dva	k4xCgMnPc1	dva
netvoři	netvor	k1gMnPc1	netvor
<g/>
;	;	kIx,	;
Bílá	bílý	k2eAgFnSc1d1	bílá
smrt	smrt	k1gFnSc1	smrt
<g/>
;	;	kIx,	;
Jak	jak	k8xS	jak
Mikromil	Mikromil	k1gMnSc1	Mikromil
a	a	k8xC	a
Gigantin	Gigantina	k1gFnPc2	Gigantina
počali	počnout	k5eAaPmAgMnP	počnout
úprk	úprk	k1gInSc4	úprk
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
;	;	kIx,	;
bajka	bajka	k1gFnSc1	bajka
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bojoval	bojovat	k5eAaImAgInS	bojovat
s	s	k7c7	s
drakem	drak	k1gInSc7	drak
<g/>
;	;	kIx,	;
Rádci	rádce	k1gMnPc1	rádce
krále	král	k1gMnSc2	král
Hydropse	Hydrops	k1gMnSc2	Hydrops
<g/>
;	;	kIx,	;
Automatoušův	Automatoušův	k2eAgMnSc1d1	Automatoušův
přítel	přítel	k1gMnSc1	přítel
<g/>
;	;	kIx,	;
Král	Král	k1gMnSc1	Král
Globares	Globares	k1gMnSc1	Globares
a	a	k8xC	a
mudrci	mudrc	k1gMnPc1	mudrc
<g/>
;	;	kIx,	;
Bajka	bajka	k1gFnSc1	bajka
o	o	k7c6	o
králi	král	k1gMnSc6	král
Murdasovi	Murdas	k1gMnSc6	Murdas
<g/>
;	;	kIx,	;
Poklady	poklad	k1gInPc7	poklad
krále	král	k1gMnSc2	král
Biskalara	Biskalar	k1gMnSc2	Biskalar
<g/>
;	;	kIx,	;
Záhada	záhada	k1gFnSc1	záhada
<g/>
;	;	kIx,	;
Z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Cifrotikon	Cifrotikona	k1gFnPc2	Cifrotikona
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
o	o	k7c6	o
srdečních	srdeční	k2eAgFnPc6d1	srdeční
deviacích	deviace	k1gFnPc6	deviace
<g/>
,	,	kIx,	,
superfixacích	superfixak	k1gInPc6	superfixak
a	a	k8xC	a
variacích	variace	k1gFnPc6	variace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
princi	princ	k1gMnSc6	princ
Ferritovi	Ferrita	k1gMnSc6	Ferrita
a	a	k8xC	a
princezně	princezna	k1gFnSc3	princezna
Krystalce	krystalka	k1gFnSc3	krystalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyberiáda	Kyberiáda	k1gFnSc1	Kyberiáda
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Cyberiada	Cyberiada	k1gFnSc1	Cyberiada
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Groteskní	groteskní	k2eAgInPc4d1	groteskní
příběhy	příběh	k1gInPc4	příběh
dvou	dva	k4xCgInPc2	dva
robotů-konstruktérů	robotůonstruktér	k1gInPc2	robotů-konstruktér
<g/>
,	,	kIx,	,
Trurla	Trurlo	k1gNnSc2	Trurlo
a	a	k8xC	a
Klapaciuse	Klapaciuse	k1gFnSc2	Klapaciuse
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
byl	být	k5eAaImAgInS	být
zachráněn	zachráněn	k2eAgInSc1d1	zachráněn
svět	svět	k1gInSc1	svět
<g/>
;	;	kIx,	;
Trurlův	Trurlův	k2eAgInSc1d1	Trurlův
stroj	stroj	k1gInSc1	stroj
<g/>
;	;	kIx,	;
Velký	velký	k2eAgInSc1d1	velký
výprask	výprask	k1gInSc1	výprask
<g/>
;	;	kIx,	;
Sedm	sedm	k4xCc4	sedm
výprav	výprava	k1gFnPc2	výprava
Trurla	Trurlo	k1gNnSc2	Trurlo
a	a	k8xC	a
Klapaciuse	Klapaciuse	k1gFnSc2	Klapaciuse
<g/>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
první	první	k4xOgFnSc1	první
čili	čili	k8xC	čili
Gargantuova	Gargantuův	k2eAgFnSc1d1	Gargantuův
lest	lest	k1gFnSc1	lest
<g/>
;	;	kIx,	;
Výprava	výprava	k1gMnSc1	výprava
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
Trurlův	Trurlův	k2eAgInSc1d1	Trurlův
elektrobard	elektrobard	k1gInSc1	elektrobard
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
druhá	druhý	k4xOgFnSc1	druhý
čili	čili	k8xC	čili
nabídka	nabídka	k1gFnSc1	nabídka
krále	král	k1gMnSc2	král
Kruťáka	Kruťáka	k?	Kruťáka
<g/>
;	;	kIx,	;
Výprava	výprava	k1gMnSc1	výprava
třetí	třetí	k4xOgMnSc1	třetí
čili	čili	k8xC	čili
draci	drak	k1gMnPc1	drak
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
čili	čili	k8xC	čili
kterak	kterak	k8xS	kterak
Trurl	Trurl	k1gMnSc1	Trurl
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
feminotron	feminotron	k1gInSc4	feminotron
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prince	princ	k1gMnSc4	princ
Pantarktika	Pantarktik	k1gMnSc4	Pantarktik
zbavil	zbavit	k5eAaPmAgInS	zbavit
milostného	milostný	k2eAgNnSc2d1	milostné
soužení	soužení	k1gNnSc2	soužení
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xC	jak
použil	použít	k5eAaPmAgMnS	použít
později	pozdě	k6eAd2	pozdě
děckometu	děckomet	k1gInSc2	děckomet
<g/>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
pátá	pátá	k1gFnSc1	pátá
čili	čili	k8xC	čili
nezbedné	nezbedný	k2eAgInPc1d1	nezbedný
kousky	kousek	k1gInPc1	kousek
krále	král	k1gMnSc2	král
Baleriona	Balerion	k1gMnSc2	Balerion
<g/>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
<g />
.	.	kIx.	.
</s>
<s>
pátá	pátá	k1gFnSc1	pátá
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
čili	čili	k8xC	čili
Trurlova	Trurlův	k2eAgFnSc1d1	Trurlův
rada	rada	k1gFnSc1	rada
<g/>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
šestá	šestý	k4xOgFnSc1	šestý
čili	čili	k8xC	čili
jak	jak	k8xC	jak
Trurl	Trurl	k1gMnSc1	Trurl
a	a	k8xC	a
Klapacius	Klapacius	k1gMnSc1	Klapacius
stvořili	stvořit	k5eAaPmAgMnP	stvořit
démona	démon	k1gMnSc2	démon
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
piráta	pirát	k1gMnSc4	pirát
Tlamsu	Tlamsa	k1gFnSc4	Tlamsa
přemohli	přemoct	k5eAaPmAgMnP	přemoct
<g/>
;	;	kIx,	;
Výprava	výprava	k1gFnSc1	výprava
sedmá	sedmý	k4xOgFnSc1	sedmý
čili	čili	k8xC	čili
jak	jak	k8xS	jak
vlastní	vlastní	k2eAgFnSc4d1	vlastní
dokonalost	dokonalost	k1gFnSc4	dokonalost
byla	být	k5eAaImAgFnS	být
Trurlovi	Trurl	k1gMnSc3	Trurl
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
<g/>
;	;	kIx,	;
Bajka	bajka	k1gFnSc1	bajka
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
strojích	stroj	k1gInPc6	stroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyprávěly	vyprávět	k5eAaImAgInP	vyprávět
králi	král	k1gMnSc3	král
Genialonovi	Genialon	k1gMnSc3	Genialon
<g/>
;	;	kIx,	;
Altruisin	Altruisin	k1gMnSc1	Altruisin
<g/>
;	;	kIx,	;
Kobyště	Kobyště	k1gNnSc1	Kobyště
<g/>
;	;	kIx,	;
Cifranova	Cifranův	k2eAgFnSc1d1	Cifranův
výchova	výchova	k1gFnSc1	výchova
<g/>
;	;	kIx,	;
Opakování	opakování	k1gNnSc1	opakování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lov	lov	k1gInSc1	lov
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Polowanie	Polowanie	k1gFnSc1	Polowanie
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
5	[number]	k4	5
povídek	povídka	k1gFnPc2	povídka
<g/>
:	:	kIx,	:
Lov	lov	k1gInSc1	lov
<g/>
;	;	kIx,	;
Neštěstí	neštěstí	k1gNnSc1	neštěstí
<g/>
;	;	kIx,	;
Pirxovo	Pirxův	k2eAgNnSc1d1	Pirxův
vyprávění	vyprávění	k1gNnSc1	vyprávění
<g/>
;	;	kIx,	;
Altruisin	Altruisin	k1gMnSc1	Altruisin
<g/>
;	;	kIx,	;
Dva	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zachraňme	zachránit	k5eAaPmRp1nP	zachránit
vesmír	vesmír	k1gInSc4	vesmír
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Ratujmy	Ratujm	k1gInPc1	Ratujm
kosmos	kosmos	k1gInSc1	kosmos
i	i	k8xC	i
inne	inne	k1gInSc1	inne
opowiadania	opowiadanium	k1gNnSc2	opowiadanium
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
9	[number]	k4	9
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběhy	příběh	k1gInPc1	příběh
pilota	pilot	k1gMnSc2	pilot
Pirxe	Pirxe	k1gFnSc2	Pirxe
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Přelíčení	přelíčení	k1gNnSc2	přelíčení
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zkouška	zkouška	k1gFnSc1	zkouška
pilota	pilota	k1gFnSc1	pilota
Pirxe	Pirxe	k1gFnSc1	Pirxe
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
polsko-sovětsko-estonský	polskoovětskostonský	k2eAgInSc4d1	polsko-sovětsko-estonský
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Marek	Marek	k1gMnSc1	Marek
Piestrak	Piestrak	k1gMnSc1	Piestrak
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgFnPc1d1	literární
hříčky	hříčka	k1gFnPc1	hříčka
===	===	k?	===
</s>
</p>
<p>
<s>
Pomyslná	pomyslný	k2eAgFnSc1d1	pomyslná
velikost	velikost	k1gFnSc1	velikost
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Wielkość	Wielkość	k1gMnSc2	Wielkość
urojona	urojon	k1gMnSc2	urojon
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úvody	úvod	k1gInPc4	úvod
k	k	k7c3	k
neexistujícím	existující	k2eNgFnPc3d1	neexistující
knihám	kniha	k1gFnPc3	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Lem	lem	k1gInSc1	lem
hýřil	hýřit	k5eAaImAgInS	hýřit
originálními	originální	k2eAgFnPc7d1	originální
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
nestačil	stačit	k5eNaBmAgMnS	stačit
všechny	všechen	k3xTgInPc4	všechen
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
konkrétně	konkrétně	k6eAd1	konkrétně
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Obdobným	obdobný	k2eAgInSc7d1	obdobný
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
prázdnota	prázdnota	k1gFnSc1	prázdnota
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
2	[number]	k4	2
knihy	kniha	k1gFnPc1	kniha
vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k9	i
ve	v	k7c6	v
společném	společný	k2eAgNnSc6d1	společné
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
prázdnota	prázdnota	k1gFnSc1	prázdnota
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Doskonala	Doskonala	k1gFnSc1	Doskonala
próżnia	próżnium	k1gNnSc2	próżnium
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
neexistujících	existující	k2eNgFnPc2d1	neexistující
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Golem	Golem	k1gMnSc1	Golem
XIV	XIV	kA	XIV
-	-	kIx~	-
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gMnSc1	počítač
Golem	Golem	k1gMnSc1	Golem
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
s	s	k7c7	s
filosofy	filosof	k1gMnPc7	filosof
o	o	k7c6	o
Rozumu	rozum	k1gInSc6	rozum
a	a	k8xC	a
světě	svět	k1gInSc6	svět
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
odmlčí	odmlčet	k5eAaPmIp3nS	odmlčet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nonfiction	Nonfiction	k1gInSc4	Nonfiction
===	===	k?	===
</s>
</p>
<p>
<s>
Dialogy	dialog	k1gInPc1	dialog
o	o	k7c6	o
atomovém	atomový	k2eAgNnSc6d1	atomové
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc6	zmrtvýchvstání
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc6	teorie
nemožnosti	nemožnost	k1gFnSc6	nemožnost
<g/>
,	,	kIx,	,
filosofických	filosofický	k2eAgInPc6d1	filosofický
přínosech	přínos	k1gInPc6	přínos
kanibalismu	kanibalismus	k1gInSc2	kanibalismus
<g/>
,	,	kIx,	,
smutku	smutek	k1gInSc2	smutek
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
,	,	kIx,	,
kybernetické	kybernetický	k2eAgFnSc6d1	kybernetická
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnSc6d1	elektrická
reinkarnaci	reinkarnace	k1gFnSc4	reinkarnace
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
zpětných	zpětný	k2eAgFnPc6d1	zpětná
vazbách	vazba	k1gFnPc6	vazba
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
kybernetické	kybernetický	k2eAgFnSc6d1	kybernetická
eschatologii	eschatologie	k1gFnSc6	eschatologie
<g/>
,	,	kIx,	,
charakteru	charakter	k1gInSc6	charakter
elektrických	elektrický	k2eAgFnPc2d1	elektrická
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
falešnosti	falešnost	k1gFnPc1	falešnost
elektromozků	elektromozek	k1gInPc2	elektromozek
<g/>
,	,	kIx,	,
věčném	věčný	k2eAgInSc6d1	věčný
životě	život	k1gInSc6	život
v	v	k7c6	v
bedně	bedna	k1gFnSc6	bedna
<g/>
,	,	kIx,	,
konstruování	konstruování	k1gNnSc1	konstruování
géniů	génius	k1gMnPc2	génius
<g/>
,	,	kIx,	,
epilepsii	epilepsie	k1gFnSc4	epilepsie
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
řídících	řídící	k2eAgInPc6d1	řídící
strojích	stroj	k1gInPc6	stroj
<g/>
,	,	kIx,	,
projektování	projektování	k1gNnSc6	projektování
společenských	společenský	k2eAgInPc2d1	společenský
systémů	systém	k1gInPc2	systém
od	od	k7c2	od
Stanisława	Stanisław	k1gInSc2	Stanisław
Lema	Lem	k1gInSc2	Lem
-	-	kIx~	-
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozpravy	rozprava	k1gFnPc1	rozprava
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
kybernetiky	kybernetika	k1gFnSc2	kybernetika
formou	forma	k1gFnSc7	forma
platónského	platónský	k2eAgInSc2d1	platónský
dialogu	dialog	k1gInSc2	dialog
dvou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
Hylase	Hylasa	k1gFnSc6	Hylasa
a	a	k8xC	a
Filonouse	Filonous	k1gInSc6	Filonous
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vydání	vydání	k1gNnSc1	vydání
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dialogy	dialog	k1gInPc4	dialog
po	po	k7c6	po
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
Ztracené	ztracený	k2eAgFnPc4d1	ztracená
iluze	iluze	k1gFnPc4	iluze
čili	čili	k8xC	čili
od	od	k7c2	od
intelektroniky	intelektronika	k1gFnSc2	intelektronika
k	k	k7c3	k
informatice	informatika	k1gFnSc3	informatika
<g/>
;	;	kIx,	;
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
kybernetika	kybernetika	k1gFnSc1	kybernetika
<g/>
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
;	;	kIx,	;
Etika	etika	k1gFnSc1	etika
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
etiky	etika	k1gFnSc2	etika
<g/>
;	;	kIx,	;
Biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Wejście	Wejście	k1gFnSc1	Wejście
na	na	k7c4	na
orbite	orbit	k1gInSc5	orbit
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
fejetonů	fejeton	k1gInPc2	fejeton
a	a	k8xC	a
článků	článek	k1gInPc2	článek
publikovaných	publikovaný	k2eAgInPc2d1	publikovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
-	-	kIx~	-
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Summa	Summa	k1gFnSc1	Summa
technologiae	technologiae	k1gFnSc1	technologiae
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
-	-	kIx~	-
souhrn	souhrn	k1gInSc1	souhrn
Lemových	lemový	k2eAgFnPc2d1	lemová
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
budoucím	budoucí	k2eAgInSc6d1	budoucí
vývoji	vývoj	k1gInSc6	vývoj
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
existence	existence	k1gFnSc2	existence
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Kosmogonické	kosmogonický	k2eAgNnSc1d1	kosmogonický
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Napodobení	napodobení	k1gNnSc1	napodobení
přírodní	přírodní	k2eAgFnSc2d1	přírodní
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnPc1	rekonstrukce
člověka	člověk	k1gMnSc2	člověk
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
zámek	zámek	k1gInSc1	zámek
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Wysoki	Wysoki	k1gNnSc1	Wysoki
zamek	zamky	k1gFnPc2	zamky
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lemovy	Lemovy	k?	Lemovy
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
z	z	k7c2	z
rodného	rodný	k2eAgInSc2d1	rodný
Lvova	Lvov	k1gInSc2	Lvov
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dětských	dětský	k2eAgFnPc2d1	dětská
až	až	k9	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
vyšly	vyjít	k5eAaPmAgFnP	vyjít
i	i	k8xC	i
autorovy	autorův	k2eAgFnPc1d1	autorova
lyrické	lyrický	k2eAgFnPc1d1	lyrická
básně	báseň	k1gFnPc1	báseň
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Verše	verš	k1gInPc4	verš
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
náhody	náhoda	k1gFnSc2	náhoda
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Filozofija	Filozofija	k1gFnSc1	Filozofija
przypadku	przypadek	k1gInSc2	przypadek
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podtitul	podtitul	k1gInSc1	podtitul
<g/>
:	:	kIx,	:
Literatura	literatura	k1gFnSc1	literatura
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
empirie	empirie	k1gFnSc2	empirie
<g/>
.	.	kIx.	.
</s>
<s>
Pojetí	pojetí	k1gNnSc1	pojetí
literárního	literární	k2eAgInSc2d1	literární
útvaru	útvar	k1gInSc2	útvar
jako	jako	k8xS	jako
sémantického	sémantický	k2eAgInSc2d1	sémantický
jevu	jev	k1gInSc2	jev
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
čtenářské	čtenářský	k2eAgFnSc2d1	čtenářská
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Fenomenologická	fenomenologický	k2eAgFnSc1d1	fenomenologická
teorie	teorie	k1gFnSc1	teorie
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
sémantika	sémantika	k1gFnSc1	sémantika
<g/>
,	,	kIx,	,
strukturální	strukturální	k2eAgFnPc4d1	strukturální
a	a	k8xC	a
selektivní	selektivní	k2eAgFnPc4d1	selektivní
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnPc4	aplikace
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc4	jazyk
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
biologické	biologický	k2eAgInPc1d1	biologický
a	a	k8xC	a
stochastické	stochastický	k2eAgInPc1d1	stochastický
modely	model	k1gInPc1	model
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
strukturalismus	strukturalismus	k1gInSc1	strukturalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fantastika	fantastika	k1gFnSc1	fantastika
a	a	k8xC	a
futurologie	futurologie	k1gFnSc1	futurologie
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Fantastyka	Fantastyka	k1gFnSc1	Fantastyka
i	i	k8xC	i
futurologia	futurologia	k1gFnSc1	futurologia
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
monografie	monografie	k1gFnSc1	monografie
o	o	k7c6	o
sci-fi	scii	k1gFnSc6	sci-fi
především	především	k9	především
z	z	k7c2	z
anglosaské	anglosaský	k2eAgFnSc2d1	anglosaská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Kritický	kritický	k2eAgInSc1d1	kritický
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
sci-fi	scii	k1gFnSc3	sci-fi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpravy	rozprava	k1gFnPc1	rozprava
a	a	k8xC	a
náčrty	náčrt	k1gInPc1	náčrt
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Rozprawy	Rozpraw	k2eAgInPc1d1	Rozpraw
i	i	k8xC	i
szkice	szkice	k1gInPc1	szkice
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
článků	článek	k1gInPc2	článek
publikovaných	publikovaný	k2eAgInPc2d1	publikovaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
-	-	kIx~	-
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc4	tajemství
čínského	čínský	k2eAgInSc2d1	čínský
pokoje	pokoj	k1gInSc2	pokoj
-	-	kIx~	-
polsky	polsky	k6eAd1	polsky
Tajemnica	Tajemnicus	k1gMnSc2	Tajemnicus
chinskieho	chinskie	k1gMnSc2	chinskie
pokoju	pokoju	k5eAaPmIp1nS	pokoju
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
esejů	esej	k1gInPc2	esej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Specifická	specifický	k2eAgNnPc1d1	specifické
česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
===	===	k?	===
</s>
</p>
<p>
<s>
Futurologický	futurologický	k2eAgInSc1d1	futurologický
kongres	kongres	k1gInSc1	kongres
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
8	[number]	k4	8
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězdné	hvězdný	k2eAgInPc1d1	hvězdný
deníky	deník	k1gInPc1	deník
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Baronet	baroneta	k1gFnPc2	baroneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sex	sex	k1gInSc1	sex
Wars	Wars	k1gInSc1	Wars
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
esejů	esej	k1gInPc2	esej
<g/>
.	.	kIx.	.
</s>
<s>
Vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Invaze	invaze	k1gFnSc1	invaze
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
sestavený	sestavený	k2eAgInSc1d1	sestavený
Pavlem	Pavel	k1gMnSc7	Pavel
Weigelem	Weigel	k1gMnSc7	Weigel
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídky	povídka	k1gFnPc4	povídka
z	z	k7c2	z
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
Lemovy	Lemovy	k?	Lemovy
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
česky	česky	k6eAd1	česky
doposud	doposud	k6eAd1	doposud
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WEIGEL	WEIGEL	kA	WEIGEL
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Stanisław	Stanisław	k?	Stanisław
Lem	lem	k1gInSc1	lem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Magnet-Press	Magnet-Press	k1gInSc1	Magnet-Press
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
112	[number]	k4	112
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85847	[number]	k4	85847
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ijon	Ijon	k1gMnSc1	Ijon
Tichý	Tichý	k1gMnSc1	Tichý
</s>
</p>
<p>
<s>
Science	Science	k1gFnSc1	Science
fiction	fiction	k1gInSc1	fiction
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
polských	polský	k2eAgMnPc2d1	polský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stanisław	Stanisław	k1gFnSc2	Stanisław
Lem	lem	k1gInSc1	lem
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Stanisław	Stanisław	k1gFnSc2	Stanisław
Lem	lem	k1gInSc1	lem
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Stanisław	Stanisław	k1gFnSc1	Stanisław
Lem	lem	k1gInSc4	lem
</s>
</p>
<p>
<s>
Stanisław	Stanisław	k?	Stanisław
Lem	lem	k1gInSc1	lem
na	na	k7c4	na
sci-fi	scii	k1gFnSc4	sci-fi
databázi	databáze	k1gFnSc4	databáze
LEGIE	legie	k1gFnSc2	legie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stanisław	Stanisław	k1gFnSc1	Stanisław
Lem	lem	k1gInSc4	lem
na	na	k7c4	na
sci-fi	scii	k1gFnSc4	sci-fi
databázi	databáze	k1gFnSc4	databáze
Isfdb	Isfdba	k1gFnPc2	Isfdba
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
