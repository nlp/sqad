<s>
Městečko	městečko	k1gNnSc1	městečko
South	Southa	k1gFnPc2	Southa
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
South	South	k1gMnSc1	South
Park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
komediální	komediální	k2eAgInSc1d1	komediální
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Trey	Trey	k1gInPc7	Trey
Parker	Parker	k1gMnSc1	Parker
a	a	k8xC	a
Matt	Matt	k1gMnSc1	Matt
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
vysílá	vysílat	k5eAaImIp3nS	vysílat
stanice	stanice	k1gFnSc1	stanice
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
-	-	kIx~	-
2004	[number]	k4	2004
vysílaly	vysílat	k5eAaImAgFnP	vysílat
stanice	stanice	k1gFnPc1	stanice
HBO	HBO	kA	HBO
a	a	k8xC	a
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
až	až	k9	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
navázala	navázat	k5eAaPmAgFnS	navázat
Nova	nova	k1gFnSc1	nova
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
kanále	kanál	k1gInSc6	kanál
Fanda	Fanda	k1gFnSc1	Fanda
7	[number]	k4	7
<g/>
.	.	kIx.	.
sérií	série	k1gFnSc7	série
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
seriál	seriál	k1gInSc1	seriál
vysílá	vysílat	k5eAaImIp3nS	vysílat
Prima	primo	k1gNnPc4	primo
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc4	život
v	v	k7c6	v
zapadlém	zapadlý	k2eAgNnSc6d1	zapadlé
coloradském	coloradský	k2eAgNnSc6d1	coloradský
městečku	městečko	k1gNnSc6	městečko
South	Southa	k1gFnPc2	Southa
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgMnPc1	čtyři
žáci	žák	k1gMnPc1	žák
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
agresivním	agresivní	k2eAgMnPc3d1	agresivní
<g/>
,	,	kIx,	,
vulgárním	vulgární	k2eAgMnPc3d1	vulgární
a	a	k8xC	a
často	často	k6eAd1	často
značně	značně	k6eAd1	značně
kontroverzním	kontroverzní	k2eAgInSc7d1	kontroverzní
humorem	humor	k1gInSc7	humor
se	se	k3xPyFc4	se
tvůrci	tvůrce	k1gMnPc1	tvůrce
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
k	k	k7c3	k
různým	různý	k2eAgNnPc3d1	různé
politickým	politický	k2eAgNnPc3d1	politické
a	a	k8xC	a
společenským	společenský	k2eAgNnPc3d1	společenské
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tabuizovaným	tabuizovaný	k2eAgMnPc3d1	tabuizovaný
<g/>
,	,	kIx,	,
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
např.	např.	kA	např.
euthanasii	euthanasie	k1gFnSc4	euthanasie
<g/>
,	,	kIx,	,
potratům	potrat	k1gInPc3	potrat
<g/>
,	,	kIx,	,
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aféře	aféra	k1gFnSc3	aféra
s	s	k7c7	s
karikaturami	karikatura	k1gFnPc7	karikatura
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
poběží	běžet	k5eAaImIp3nS	běžet
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
a	a	k8xC	a
s	s	k7c7	s
nynějšími	nynější	k2eAgMnPc7d1	nynější
20	[number]	k4	20
sériemi	série	k1gFnPc7	série
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
druhým	druhý	k4xOgInSc7	druhý
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
americkým	americký	k2eAgInSc7d1	americký
animovaným	animovaný	k2eAgInSc7d1	animovaný
seriálem	seriál	k1gInSc7	seriál
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
epizod	epizoda	k1gFnPc2	epizoda
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vysílala	vysílat	k5eAaImAgFnS	vysílat
seriál	seriál	k1gInSc4	seriál
HBO	HBO	kA	HBO
a	a	k8xC	a
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
sérií	série	k1gFnPc2	série
<g/>
,	,	kIx,	,
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
jej	on	k3xPp3gInSc4	on
začala	začít	k5eAaPmAgFnS	začít
opět	opět	k6eAd1	opět
vysílat	vysílat	k5eAaImF	vysílat
stanice	stanice	k1gFnSc1	stanice
Fanda	Fanda	k1gFnSc1	Fanda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
jej	on	k3xPp3gInSc4	on
opětovné	opětovný	k2eAgNnSc1d1	opětovné
nasadila	nasadit	k5eAaPmAgNnP	nasadit
stanice	stanice	k1gFnSc2	stanice
Prima	prima	k2eAgMnSc2d1	prima
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oficiálním	oficiální	k2eAgInSc6d1	oficiální
webu	web	k1gInSc6	web
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
volně	volně	k6eAd1	volně
sledovat	sledovat	k5eAaImF	sledovat
všechny	všechen	k3xTgInPc4	všechen
díly	díl	k1gInPc4	díl
v	v	k7c6	v
originálním	originální	k2eAgNnSc6d1	originální
znění	znění	k1gNnSc6	znění
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
čtyř	čtyři	k4xCgInPc2	čtyři
cenzurovaných	cenzurovaný	k2eAgInPc2d1	cenzurovaný
dílů	díl	k1gInPc2	díl
s	s	k7c7	s
Mohamedem	Mohamed	k1gMnSc7	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
South	Southa	k1gFnPc2	Southa
Park	park	k1gInSc4	park
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2005	[number]	k4	2005
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
100	[number]	k4	100
Greatest	Greatest	k1gFnSc1	Greatest
Cartoons	Cartoonsa	k1gFnPc2	Cartoonsa
třetím	třetí	k4xOgInSc7	třetí
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
animovaným	animovaný	k2eAgInSc7d1	animovaný
seriálem	seriál	k1gInSc7	seriál
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
porazili	porazit	k5eAaPmAgMnP	porazit
jej	on	k3xPp3gMnSc4	on
pouze	pouze	k6eAd1	pouze
druzí	druhý	k4xOgMnPc1	druhý
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
a	a	k8xC	a
první	první	k4xOgMnPc1	první
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
Městečko	městečko	k1gNnSc1	městečko
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
Peabody	Peaboda	k1gFnSc2	Peaboda
Award	Awarda	k1gFnPc2	Awarda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
televizním	televizní	k2eAgInSc7d1	televizní
a	a	k8xC	a
rozhlasovým	rozhlasový	k2eAgInPc3d1	rozhlasový
pořadům	pořad	k1gInPc3	pořad
<g/>
.	.	kIx.	.
</s>
<s>
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velice	velice	k6eAd1	velice
kontroverzní	kontroverzní	k2eAgInSc4d1	kontroverzní
seriál	seriál	k1gInSc4	seriál
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
používání	používání	k1gNnSc3	používání
vulgárního	vulgární	k2eAgInSc2d1	vulgární
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zesměšňování	zesměšňování	k1gNnSc1	zesměšňování
mnohých	mnohý	k2eAgInPc2d1	mnohý
aspektů	aspekt	k1gInPc2	aspekt
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
společenského	společenský	k2eAgNnSc2d1	společenské
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
epizody	epizoda	k1gFnPc4	epizoda
Navěky	navěky	k6eAd1	navěky
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
spor	spor	k1gInSc4	spor
o	o	k7c4	o
odpojení	odpojení	k1gNnSc4	odpojení
Terri	Terr	k1gFnSc2	Terr
Schiavo	Schiava	k1gFnSc5	Schiava
od	od	k7c2	od
přístrojů	přístroj	k1gInPc2	přístroj
udržujících	udržující	k2eAgInPc2d1	udržující
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
epizody	epizoda	k1gFnPc4	epizoda
Animákové	Animákové	k2eAgFnSc2d1	Animákové
války	válka	k1gFnSc2	válka
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
aféru	aféra	k1gFnSc4	aféra
s	s	k7c7	s
karikaturami	karikatura	k1gFnPc7	karikatura
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
vlnu	vlna	k1gFnSc4	vlna
kontroverze	kontroverze	k1gFnSc2	kontroverze
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
epizoda	epizoda	k1gFnSc1	epizoda
Uvězněný	uvězněný	k2eAgMnSc1d1	uvězněný
v	v	k7c6	v
komoře	komora	k1gFnSc6	komora
zesměšňující	zesměšňující	k2eAgFnSc4d1	zesměšňující
scientologii	scientologie	k1gFnSc4	scientologie
a	a	k8xC	a
její	její	k3xOp3gMnPc4	její
představitele	představitel	k1gMnPc4	představitel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Tomem	Tom	k1gMnSc7	Tom
Cruisem	Cruis	k1gInSc7	Cruis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
seriál	seriál	k1gInSc1	seriál
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
značné	značný	k2eAgNnSc1d1	značné
rozhořčení	rozhořčení	k1gNnSc1	rozhořčení
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
značnou	značný	k2eAgFnSc4d1	značná
vulgárnost	vulgárnost	k1gFnSc4	vulgárnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
médiích	médium	k1gNnPc6	médium
dosud	dosud	k6eAd1	dosud
nevídanou	vídaný	k2eNgFnSc4d1	nevídaná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
"	"	kIx"	"
<g/>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
rodiče	rodič	k1gMnSc2	rodič
proti	proti	k7c3	proti
South	South	k1gInSc1	South
Parku	park	k1gInSc2	park
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
iniciátorka	iniciátorka	k1gFnSc1	iniciátorka
<g/>
,	,	kIx,	,
Mgr.	Mgr.	kA	Mgr.
Hana	Hana	k1gFnSc1	Hana
Králová	Králová	k1gFnSc1	Králová
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
narušuje	narušovat	k5eAaImIp3nS	narušovat
dětskou	dětský	k2eAgFnSc4d1	dětská
psychiku	psychika	k1gFnSc4	psychika
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
nevhodné	vhodný	k2eNgFnSc2d1	nevhodná
normy	norma	k1gFnSc2	norma
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
je	být	k5eAaImIp3nS	být
používat	používat	k5eAaImF	používat
vulgarismy	vulgarismus	k1gInPc4	vulgarismus
a	a	k8xC	a
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
sex	sex	k1gInSc4	sex
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
normální	normální	k2eAgFnSc4d1	normální
součást	součást	k1gFnSc4	součást
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
iniciativy	iniciativa	k1gFnSc2	iniciativa
byly	být	k5eAaImAgFnP	být
aktualizovány	aktualizovat	k5eAaBmNgFnP	aktualizovat
naposledy	naposledy	k6eAd1	naposledy
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
již	již	k6eAd1	již
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
archivovaná	archivovaný	k2eAgFnSc1d1	archivovaná
verze	verze	k1gFnSc1	verze
na	na	k7c4	na
archive	archiv	k1gInSc5	archiv
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Stanley	Stanlea	k1gFnPc1	Stanlea
"	"	kIx"	"
<g/>
Stan	stan	k1gInSc1	stan
<g/>
"	"	kIx"	"
Marsh	Marsh	k1gInSc1	Marsh
Často	často	k6eAd1	často
nejupřímnější	upřímný	k2eAgMnSc1d3	nejupřímnější
člen	člen	k1gMnSc1	člen
party	parta	k1gFnSc2	parta
<g/>
.	.	kIx.	.
</s>
<s>
Stan	stan	k1gInSc1	stan
často	často	k6eAd1	často
přichází	přicházet	k5eAaImIp3nS	přicházet
s	s	k7c7	s
logickým	logický	k2eAgNnSc7d1	logické
řešením	řešení	k1gNnSc7	řešení
netradičních	tradiční	k2eNgFnPc2d1	netradiční
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
chlapci	chlapec	k1gMnPc1	chlapec
dostávají	dostávat	k5eAaImIp3nP	dostávat
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Stana	Stan	k1gInSc2	Stan
je	být	k5eAaImIp3nS	být
alter	alter	k1gMnSc1	alter
ego	ego	k1gNnSc4	ego
tvůrce	tvůrce	k1gMnSc2	tvůrce
seriálu	seriál	k1gInSc2	seriál
Treye	Trey	k1gMnSc2	Trey
Parkera	Parker	k1gMnSc2	Parker
a	a	k8xC	a
často	často	k6eAd1	často
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
morální	morální	k2eAgNnSc4d1	morální
poselství	poselství	k1gNnSc4	poselství
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
je	být	k5eAaImIp3nS	být
Kyle	Kyle	k1gFnSc1	Kyle
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
přátelství	přátelství	k1gNnSc1	přátelství
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
motivem	motiv	k1gInSc7	motiv
mnoha	mnoho	k4c2	mnoho
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Kyle	Kyle	k1gNnSc1	Kyle
Broflovski	Broflovsk	k1gFnSc2	Broflovsk
Skeptický	skeptický	k2eAgMnSc1d1	skeptický
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
pokrytecký	pokrytecký	k2eAgMnSc1d1	pokrytecký
a	a	k8xC	a
také	také	k9	také
nejsnáze	snadno	k6eAd3	snadno
ovlivnitelný	ovlivnitelný	k2eAgMnSc1d1	ovlivnitelný
člen	člen	k1gMnSc1	člen
party	parta	k1gFnSc2	parta
<g/>
.	.	kIx.	.
</s>
<s>
Kyle	Kyle	k1gFnSc1	Kyle
je	být	k5eAaImIp3nS	být
alter-ego	altergo	k6eAd1	alter-ego
dalšího	další	k2eAgMnSc4d1	další
tvůrce	tvůrce	k1gMnSc4	tvůrce
<g/>
,	,	kIx,	,
Matta	Matt	k1gMnSc4	Matt
Stonea	Stoneus	k1gMnSc4	Stoneus
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Stanem	stan	k1gInSc7	stan
přichází	přicházet	k5eAaImIp3nS	přicházet
Kyle	Kyle	k1gFnSc1	Kyle
s	s	k7c7	s
rozumným	rozumný	k2eAgInSc7d1	rozumný
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
bláznivé	bláznivý	k2eAgNnSc4d1	bláznivé
chování	chování	k1gNnSc4	chování
světa	svět	k1gInSc2	svět
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Kyle	Kyle	k1gFnSc1	Kyle
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejčestnějšího	čestný	k2eAgMnSc4d3	nejčestnější
člena	člen	k1gMnSc4	člen
party	parta	k1gFnSc2	parta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
terčem	terč	k1gInSc7	terč
Cartmanova	Cartmanův	k2eAgInSc2d1	Cartmanův
rasismu	rasismus	k1gInSc2	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
Theodore	Theodor	k1gMnSc5	Theodor
Cartman	Cartman	k1gMnSc1	Cartman
Postava	postava	k1gFnSc1	postava
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
Archiem	Archius	k1gMnSc7	Archius
Bunkerem	Bunker	k1gMnSc7	Bunker
<g/>
.	.	kIx.	.
</s>
<s>
Cartman	Cartman	k1gMnSc1	Cartman
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
původcem	původce	k1gMnSc7	původce
zápletky	zápletka	k1gFnSc2	zápletka
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Cartman	Cartman	k1gMnSc1	Cartman
je	být	k5eAaImIp3nS	být
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
,	,	kIx,	,
sadistický	sadistický	k2eAgMnSc1d1	sadistický
<g/>
,	,	kIx,	,
fanatický	fanatický	k2eAgMnSc1d1	fanatický
<g/>
,	,	kIx,	,
rozmazlený	rozmazlený	k2eAgMnSc1d1	rozmazlený
<g/>
,	,	kIx,	,
obézní	obézní	k2eAgMnSc1d1	obézní
<g/>
,	,	kIx,	,
drzý	drzý	k2eAgInSc1d1	drzý
<g/>
,	,	kIx,	,
manipulativní	manipulativní	k2eAgInSc1d1	manipulativní
a	a	k8xC	a
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
uráží	urážet	k5eAaImIp3nS	urážet
Kyla	Kyl	k2eAgFnSc1d1	Kyla
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
a	a	k8xC	a
Kennyho	Kenny	k1gMnSc4	Kenny
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
také	také	k9	také
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c4	po
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
ochoten	ochoten	k2eAgInSc1d1	ochoten
udělat	udělat	k5eAaPmF	udělat
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
.	.	kIx.	.
</s>
<s>
Kenneth	Kenneth	k1gInSc1	Kenneth
"	"	kIx"	"
<g/>
Kenny	Kenna	k1gFnPc1	Kenna
<g/>
"	"	kIx"	"
McCormick	McCormick	k1gInSc1	McCormick
Kenny	Kenna	k1gFnSc2	Kenna
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
extrémně	extrémně	k6eAd1	extrémně
chudé	chudý	k2eAgFnSc2d1	chudá
a	a	k8xC	a
hulvátské	hulvátský	k2eAgFnSc2d1	hulvátská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgInSc4d1	posedlý
sexem	sex	k1gInSc7	sex
a	a	k8xC	a
toaletním	toaletní	k2eAgInSc7d1	toaletní
humorem	humor	k1gInSc7	humor
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
často	často	k6eAd1	často
poučuje	poučovat	k5eAaImIp3nS	poučovat
své	svůj	k3xOyFgMnPc4	svůj
kamarády	kamarád	k1gMnPc4	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
těžké	těžký	k2eAgNnSc1d1	těžké
rozumět	rozumět	k5eAaImF	rozumět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
kapuci	kapuce	k1gFnSc4	kapuce
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mu	on	k3xPp3gMnSc3	on
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
kamarádi	kamarád	k1gMnPc1	kamarád
bezvadně	bezvadně	k6eAd1	bezvadně
rozumí	rozumět	k5eAaImIp3nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
pěti	pět	k4xCc2	pět
sezón	sezóna	k1gFnPc2	sezóna
zahynul	zahynout	k5eAaPmAgMnS	zahynout
Kenny	Kenna	k1gFnSc2	Kenna
takřka	takřka	k6eAd1	takřka
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
epizodě	epizoda	k1gFnSc6	epizoda
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
velice	velice	k6eAd1	velice
komickou	komický	k2eAgFnSc7d1	komická
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
objevil	objevit	k5eAaPmAgMnS	objevit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Legendární	legendární	k2eAgMnSc1d1	legendární
je	být	k5eAaImIp3nS	být
také	také	k9	také
hláška	hláška	k1gFnSc1	hláška
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Cartman	Cartman	k1gMnSc1	Cartman
nebo	nebo	k8xC	nebo
Stan	stan	k1gInSc1	stan
vykřikne	vykřiknout	k5eAaPmIp3nS	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Oh	oh	k0	oh
my	my	k3xPp1nPc1	my
God	God	k1gFnPc4	God
<g/>
,	,	kIx,	,
they	the	k2eAgInPc4d1	the
killed	killed	k1gInSc4	killed
Kenny	Kenna	k1gFnSc2	Kenna
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Kyle	Kyl	k1gInPc4	Kyl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
bastards	bastardsa	k1gFnPc2	bastardsa
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
páté	pátý	k4xOgFnSc2	pátý
sezóny	sezóna	k1gFnSc2	sezóna
Kenny	Kenna	k1gMnSc2	Kenna
definitivně	definitivně	k6eAd1	definitivně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
v	v	k7c6	v
partě	parta	k1gFnSc6	parta
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
Butters	Butters	k1gInSc1	Butters
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Tweek	Tweek	k6eAd1	Tweek
<g/>
.	.	kIx.	.
</s>
<s>
Kenny	Kenn	k1gInPc1	Kenn
se	se	k3xPyFc4	se
do	do	k7c2	do
seriálu	seriál	k1gInSc2	seriál
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sérii	série	k1gFnSc6	série
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k6eAd1	už
ale	ale	k8xC	ale
neumírá	umírat	k5eNaImIp3nS	umírat
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
vzácné	vzácný	k2eAgFnPc4d1	vzácná
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jerome	Jerom	k1gInSc5	Jerom
"	"	kIx"	"
<g/>
Chef	Chef	k1gInSc4	Chef
<g/>
"	"	kIx"	"
McElroy	McElroa	k1gMnSc2	McElroa
Postava	postava	k1gFnSc1	postava
Chefa	Chef	k1gMnSc2	Chef
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
dabingu	dabing	k1gInSc6	dabing
Šéfa	šéf	k1gMnSc2	šéf
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šéfkuchařem	šéfkuchař	k1gMnSc7	šéfkuchař
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
jídelně	jídelna	k1gFnSc6	jídelna
southparské	southparský	k2eAgFnSc2d1	southparský
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
naši	náš	k3xOp1gMnPc1	náš
hrdinové	hrdina	k1gMnPc1	hrdina
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
charakterů	charakter	k1gInPc2	charakter
černé	černý	k2eAgFnSc2d1	černá
pleti	pleť	k1gFnSc2	pleť
v	v	k7c6	v
show	show	k1gFnSc6	show
<g/>
,	,	kIx,	,
typickým	typický	k2eAgNnSc7d1	typické
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
postavu	postava	k1gFnSc4	postava
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
velký	velký	k2eAgInSc1d1	velký
sexuální	sexuální	k2eAgInSc1d1	sexuální
apetit	apetit	k1gInSc1	apetit
a	a	k8xC	a
úspěchy	úspěch	k1gInPc1	úspěch
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
zdrojem	zdroj	k1gInSc7	zdroj
rozličných	rozličný	k2eAgFnPc2d1	rozličná
informací	informace	k1gFnPc2	informace
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnSc4d1	hlavní
čtveřici	čtveřice	k1gFnSc4	čtveřice
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
spojka	spojka	k1gFnSc1	spojka
mezi	mezi	k7c7	mezi
světem	svět	k1gInSc7	svět
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
chlapci	chlapec	k1gMnPc7	chlapec
začínal	začínat	k5eAaImAgMnS	začínat
rituálem	rituál	k1gInSc7	rituál
složeným	složený	k2eAgInSc7d1	složený
z	z	k7c2	z
vět	věta	k1gFnPc2	věta
<g/>
:	:	kIx,	:
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
Hi	hi	k0	hi
<g/>
,	,	kIx,	,
children	childrna	k1gFnPc2	childrna
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
Hi	hi	k0	hi
<g/>
,	,	kIx,	,
Chef	Chef	k1gMnSc1	Chef
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
How	How	k1gFnSc1	How
are	ar	k1gInSc5	ar
you	you	k?	you
today	todaum	k1gNnPc7	todaum
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc1	Bad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
<g/>
"	"	kIx"	"
<g/>
Why	Why	k1gFnSc1	Why
bad	bad	k?	bad
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Chefa	Chef	k1gMnSc2	Chef
ale	ale	k8xC	ale
přestala	přestat	k5eAaPmAgFnS	přestat
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
Isaac	Isaac	k1gFnSc1	Isaac
Hayes	Hayes	k1gMnSc1	Hayes
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
soulový	soulový	k2eAgMnSc1d1	soulový
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
kultovní	kultovní	k2eAgFnSc4d1	kultovní
postavu	postava	k1gFnSc4	postava
v	v	k7c6	v
originále	originál	k1gInSc6	originál
daboval	dabovat	k5eAaBmAgMnS	dabovat
<g/>
.	.	kIx.	.
</s>
<s>
Hayes	Hayes	k1gMnSc1	Hayes
coby	coby	k?	coby
aktivní	aktivní	k2eAgMnSc1d1	aktivní
člen	člen	k1gMnSc1	člen
Scientologické	scientologický	k2eAgFnSc2d1	Scientologická
církve	církev	k1gFnSc2	církev
neunesl	unést	k5eNaPmAgMnS	unést
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Scientologie	scientologie	k1gFnSc1	scientologie
stala	stát	k5eAaPmAgFnS	stát
terčem	terč	k1gInSc7	terč
ostré	ostrý	k2eAgFnSc2d1	ostrá
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
zesměšnění	zesměšnění	k1gNnSc2	zesměšnění
v	v	k7c6	v
show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Leopold	Leopold	k1gMnSc1	Leopold
"	"	kIx"	"
<g/>
Butters	Butters	k1gInSc1	Butters
<g/>
"	"	kIx"	"
Stotch	Stotch	k1gInSc1	Stotch
Butters	Buttersa	k1gFnPc2	Buttersa
nahradil	nahradit	k5eAaPmAgInS	nahradit
v	v	k7c6	v
partě	parta	k1gFnSc6	parta
Kennyho	Kenny	k1gMnSc2	Kenny
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
šesté	šestý	k4xOgFnSc2	šestý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Butters	Butters	k1gInSc1	Butters
je	být	k5eAaImIp3nS	být
nervózní	nervózní	k2eAgMnSc1d1	nervózní
<g/>
,	,	kIx,	,
naivní	naivní	k2eAgMnSc1d1	naivní
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
manipulovatelný	manipulovatelný	k2eAgInSc4d1	manipulovatelný
a	a	k8xC	a
skrývající	skrývající	k2eAgNnSc4d1	skrývající
své	své	k1gNnSc4	své
emoce	emoce	k1gFnSc2	emoce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
paradoxně	paradoxně	k6eAd1	paradoxně
optimistický	optimistický	k2eAgInSc4d1	optimistický
a	a	k8xC	a
bystrý	bystrý	k2eAgInSc4d1	bystrý
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
bezohledně	bezohledně	k6eAd1	bezohledně
trestán	trestat	k5eAaImNgMnS	trestat
svými	svůj	k3xOyFgMnPc7	svůj
extrémně	extrémně	k6eAd1	extrémně
starostlivými	starostlivý	k2eAgMnPc7d1	starostlivý
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
necitlivě	citlivě	k6eNd1	citlivě
urážen	urážen	k2eAgMnSc1d1	urážen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zneužíván	zneužíván	k2eAgInSc1d1	zneužíván
svými	svůj	k3xOyFgMnPc7	svůj
spolužáky	spolužák	k1gMnPc7	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
tragickému	tragický	k2eAgInSc3d1	tragický
charakteru	charakter	k1gInSc3	charakter
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
narozeniny	narozeniny	k1gFnPc4	narozeniny
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Buttersovo	Buttersův	k2eAgNnSc1d1	Buttersův
alter-ego	altergo	k1gNnSc1	alter-ego
je	být	k5eAaImIp3nS	být
Profesor	profesor	k1gMnSc1	profesor
Chaos	chaos	k1gInSc1	chaos
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
převléká	převlékat	k5eAaImIp3nS	převlékat
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
škodit	škodit	k5eAaImF	škodit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
South	South	k1gInSc4	South
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Tweek	Tweek	k1gMnSc1	Tweek
Tweak	Tweak	k1gMnSc1	Tweak
V	v	k7c6	v
partě	parta	k1gFnSc6	parta
nahradil	nahradit	k5eAaPmAgMnS	nahradit
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
sezóně	sezóna	k1gFnSc6	sezóna
Butterse	Butterse	k1gFnSc1	Butterse
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
díl	díl	k1gInSc1	díl
6	[number]	k4	6
<g/>
x	x	k?	x
<g/>
6	[number]	k4	6
Profesor	profesor	k1gMnSc1	profesor
Chaos	chaos	k1gInSc1	chaos
<g/>
)	)	kIx)	)
Tweek	Tweek	k1gInSc1	Tweek
je	být	k5eAaImIp3nS	být
nervově	nervově	k6eAd1	nervově
nevyrovnaný	vyrovnaný	k2eNgInSc1d1	nevyrovnaný
<g/>
,	,	kIx,	,
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nP	trpět
PPD	PPD	kA	PPD
(	(	kIx(	(
<g/>
Porucha	porucha	k1gFnSc1	porucha
Pozornostního	pozornostní	k2eAgInSc2d1	pozornostní
Deficitu	deficit	k1gInSc2	deficit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původci	původce	k1gMnPc1	původce
problémů	problém	k1gInPc2	problém
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jej	on	k3xPp3gMnSc4	on
naučili	naučit	k5eAaPmAgMnP	naučit
pít	pít	k5eAaImF	pít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
také	také	k9	také
skřítci	skřítek	k1gMnPc1	skřítek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Tweekovi	Tweekův	k2eAgMnPc1d1	Tweekův
kradou	krást	k5eAaImIp3nP	krást
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
epizodách	epizoda	k1gFnPc6	epizoda
se	se	k3xPyFc4	se
z	z	k7c2	z
Tweeka	Tweeek	k1gInSc2	Tweeek
opět	opět	k6eAd1	opět
stala	stát	k5eAaPmAgFnS	stát
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
série	série	k1gFnSc1	série
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
gay	gay	k1gMnSc1	gay
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
6	[number]	k4	6
dílu	díl	k1gInSc2	díl
dá	dát	k5eAaPmIp3nS	dát
dohromady	dohromady	k6eAd1	dohromady
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
spolužákem	spolužák	k1gMnSc7	spolužák
Craigem	Craig	k1gMnSc7	Craig
<g/>
.	.	kIx.	.
</s>
<s>
Timmy	Timma	k1gFnPc1	Timma
Burch	Burcha	k1gFnPc2	Burcha
Tělesně	tělesně	k6eAd1	tělesně
postižený	postižený	k2eAgMnSc1d1	postižený
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
používat	používat	k5eAaImF	používat
vozíček	vozíček	k1gInSc4	vozíček
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
slovník	slovník	k1gInSc1	slovník
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
vykřikuje	vykřikovat	k5eAaImIp3nS	vykřikovat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
různé	různý	k2eAgInPc4d1	různý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
řekne	říct	k5eAaPmIp3nS	říct
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
(	(	kIx(	(
<g/>
nejčastější	častý	k2eAgNnPc4d3	nejčastější
slova	slovo	k1gNnPc4	slovo
sou	sou	k1gInSc2	sou
Timmyyy	Timmyya	k1gFnSc2	Timmyya
<g/>
,	,	kIx,	,
Bublaaa	Bublaaa	k1gMnSc1	Bublaaa
a	a	k8xC	a
Shiiit	Shiiit	k1gMnSc1	Shiiit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
postižený	postižený	k2eAgMnSc1d1	postižený
<g/>
,	,	kIx,	,
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
kladeny	klást	k5eAaImNgInP	klást
stejné	stejný	k2eAgInPc1d1	stejný
nároky	nárok	k1gInPc1	nárok
jako	jako	k9	jako
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
žáky	žák	k1gMnPc4	žák
Pan	Pan	k1gMnSc1	Pan
Mackey	Mackea	k1gFnSc2	Mackea
Pan	Pan	k1gMnSc1	Pan
Mackey	Mackea	k1gFnSc2	Mackea
je	být	k5eAaImIp3nS	být
studijní	studijní	k2eAgMnSc1d1	studijní
poradce	poradce	k1gMnSc1	poradce
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
v	v	k7c4	v
South	South	k1gInSc4	South
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
za	za	k7c7	za
každou	každý	k3xTgFnSc7	každý
větou	věta	k1gFnSc7	věta
řekne	říct	k5eAaPmIp3nS	říct
ÁNO	ÁNO	kA	ÁNO
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
si	se	k3xPyFc3	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
studenti	student	k1gMnPc1	student
dělají	dělat	k5eAaImIp3nP	dělat
při	při	k7c6	při
hodinách	hodina	k1gFnPc6	hodina
srandu	sranda	k1gFnSc4	sranda
<g/>
,	,	kIx,	,
že	že	k8xS	že
říkají	říkat	k5eAaImIp3nP	říkat
např.	např.	kA	např.
Už	už	k6eAd1	už
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Mackey	Macke	k2eAgMnPc4d1	Macke
<g/>
,	,	kIx,	,
áno	áno	k?	áno
<g/>
.	.	kIx.	.
</s>
<s>
Randy	rand	k1gInPc1	rand
<g/>
,	,	kIx,	,
Sharon	Sharon	k1gInSc1	Sharon
<g/>
,	,	kIx,	,
Shelley	Shelley	k1gInPc1	Shelley
a	a	k8xC	a
Děda	děda	k1gMnSc1	děda
Marshovi	Marsh	k1gMnSc3	Marsh
Gerald	Gerald	k1gInSc4	Gerald
<g/>
,	,	kIx,	,
Sheila	Sheila	k1gFnSc1	Sheila
a	a	k8xC	a
Ike	Ike	k1gFnSc1	Ike
Moisha	Moish	k1gMnSc2	Moish
Broflovski	Broflovsk	k1gFnSc2	Broflovsk
Liane	Lian	k1gInSc5	Lian
Cartmanová	Cartmanová	k1gFnSc1	Cartmanová
Stuart	Stuart	k1gInSc1	Stuart
McCormick	McCormick	k1gInSc1	McCormick
<g/>
,	,	kIx,	,
Paní	paní	k1gFnSc1	paní
McCormicková	McCormicková	k1gFnSc1	McCormicková
<g/>
,	,	kIx,	,
<g/>
Kennyho	Kenny	k1gMnSc2	Kenny
bratr	bratr	k1gMnSc1	bratr
Kevin	Kevin	k1gMnSc1	Kevin
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
sestra	sestra	k1gFnSc1	sestra
Stephen	Stephna	k1gFnPc2	Stephna
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Chris	Chris	k1gFnSc1	Chris
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lynda	Lynda	k1gMnSc1	Lynda
Stotchovi	Stotchův	k2eAgMnPc1d1	Stotchův
Pan	Pan	k1gMnSc1	Pan
a	a	k8xC	a
Paní	paní	k1gFnSc1	paní
Tweekovi	Tweekovi	k1gRnPc1	Tweekovi
Pan	Pan	k1gMnSc1	Pan
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
í	í	k0	í
<g/>
)	)	kIx)	)
Herbert	Herbert	k1gMnSc1	Herbert
Garrison	Garrison	k1gMnSc1	Garrison
Korektní	korektní	k2eAgMnSc1d1	korektní
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
korektní	korektní	k2eAgMnPc1d1	korektní
Chefovi	Chefův	k2eAgMnPc1d1	Chefův
rodiče	rodič	k1gMnPc1	rodič
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
Paní	paní	k1gFnSc5	paní
Diane	Dian	k1gInSc5	Dian
Choksondiková	Choksondikový	k2eAgFnSc1d1	Choksondikový
Ředitelka	ředitelka	k1gFnSc1	ředitelka
Victoria	Victorium	k1gNnSc2	Victorium
Paní	paní	k1gFnSc1	paní
Veronica	Veronica	k1gFnSc1	Veronica
Crabtreeová	Crabtreeová	k1gFnSc1	Crabtreeová
Pan	Pan	k1gMnSc1	Pan
Mackey	Mackea	k1gFnSc2	Mackea
Token	Token	k2eAgMnSc1d1	Token
Black	Black	k1gMnSc1	Black
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Token	Token	k2eAgInSc1d1	Token
Williams	Williams	k1gInSc1	Williams
<g/>
)	)	kIx)	)
Wendy	Wend	k1gInPc1	Wend
Testaburgerová	Testaburgerová	k1gFnSc1	Testaburgerová
Bebe	Bebe	k1gInSc1	Bebe
Stevensová	Stevensová	k1gFnSc1	Stevensová
Philip	Philip	k1gMnSc1	Philip
"	"	kIx"	"
<g/>
Pip	pipa	k1gFnPc2	pipa
<g/>
"	"	kIx"	"
Pirrup	Pirrup	k1gInSc1	Pirrup
Clyde	Clyd	k1gInSc5	Clyd
Donovan	Donovan	k1gMnSc1	Donovan
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dříve	dříve	k6eAd2	dříve
Clyde	Clyd	k1gInSc5	Clyd
Goodman	Goodman	k1gMnSc1	Goodman
<g/>
)	)	kIx)	)
Craig	Craig	k1gMnSc1	Craig
Tucker	Tucker	k1gMnSc1	Tucker
Jimmy	Jimma	k1gFnSc2	Jimma
Vulmer	Vulmer	k1gMnSc1	Vulmer
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Jimmy	Jimm	k1gInPc1	Jimm
Swanson	Swanson	k1gNnSc1	Swanson
<g/>
)	)	kIx)	)
Dog	doga	k1gFnPc2	doga
Poo	Poo	k1gFnSc2	Poo
Goth	Gotha	k1gFnPc2	Gotha
Kids	Kids	k1gInSc1	Kids
Pan	Pan	k1gMnSc1	Pan
Hankey	Hankea	k1gFnSc2	Hankea
<g/>
,	,	kIx,	,
vánoční	vánoční	k2eAgFnSc2d1	vánoční
hovínko	hovínko	k?	hovínko
Pan	Pan	k1gMnSc1	Pan
Otrok	otrok	k1gMnSc1	otrok
Jimbo	Jimba	k1gFnSc5	Jimba
Kern	Kern	k1gMnSc1	Kern
a	a	k8xC	a
Ned	Ned	k1gMnSc1	Ned
Gerblansky	Gerblansky	k1gMnPc2	Gerblansky
Policista	policista	k1gMnSc1	policista
Barbrady	Barbrada	k1gFnSc2	Barbrada
Starostka	starostka	k1gFnSc1	starostka
McDanielsová	McDanielsová	k1gFnSc1	McDanielsová
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Alphonse	Alphons	k1gMnSc5	Alphons
Mephisto	Mephista	k1gMnSc5	Mephista
a	a	k8xC	a
Kevin	Kevin	k2eAgInSc4d1	Kevin
Ručníček	ručníček	k1gInSc4	ručníček
Otec	otec	k1gMnSc1	otec
Maxi	maxi	k6eAd1	maxi
Tuong	Tuong	k1gMnSc1	Tuong
Lu	Lu	k1gFnSc2	Lu
<g />
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
(	(	kIx(	(
<g/>
majitel	majitel	k1gMnSc1	majitel
čínské	čínský	k2eAgFnSc2d1	čínská
restaurace	restaurace	k1gFnSc2	restaurace
City	city	k1gNnSc1	city
Wok	Wok	k1gMnPc2	Wok
<g/>
)	)	kIx)	)
Ježíš	Ježíš	k1gMnSc1	Ježíš
Satan	Satan	k1gMnSc1	Satan
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
Scott	Scott	k1gMnSc1	Scott
Tenorman	Tenorman	k1gMnSc1	Tenorman
Pan	Pan	k1gMnSc1	Pan
Klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
maňásek	maňásek	k1gInSc1	maňásek
Pana	Pan	k1gMnSc2	Pan
Garrisona	Garrison	k1gMnSc2	Garrison
<g/>
)	)	kIx)	)
Pan	Pan	k1gMnSc1	Pan
Větvička	větvička	k1gFnSc1	větvička
(	(	kIx(	(
<g/>
náhradník	náhradník	k1gMnSc1	náhradník
Pana	Pan	k1gMnSc2	Pan
Klobouka	Klobouk	k1gMnSc2	Klobouk
<g/>
)	)	kIx)	)
Bůh	bůh	k1gMnSc1	bůh
Terrance	Terrance	k1gFnSc2	Terrance
a	a	k8xC	a
Phillip	Phillip	k1gInSc1	Phillip
obrko	obrko	k1gNnSc1	obrko
<g/>
*	*	kIx~	*
<g/>
ot	ot	k1gMnSc1	ot
Scott	Scott	k1gMnSc1	Scott
Dougie	Dougie	k1gFnSc1	Dougie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Generál	generál	k1gMnSc1	generál
Zmatek	zmatek	k1gInSc1	zmatek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Kyle	Kyle	k1gFnSc1	Kyle
Schwartz	Schwartz	k1gMnSc1	Schwartz
(	(	kIx(	(
<g/>
Kyleův	Kyleův	k2eAgMnSc1d1	Kyleův
bratranec	bratranec	k1gMnSc1	bratranec
<g/>
)	)	kIx)	)
Šesťáci	Šesťák	k1gMnPc1	Šesťák
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
Smrt	smrt	k1gFnSc1	smrt
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Kitty	Kitt	k1gInPc1	Kitt
<g/>
/	/	kIx~	/
<g/>
Číča	Číča	k1gFnSc1	Číča
(	(	kIx(	(
<g/>
Cartmanova	Cartmanův	k2eAgFnSc1d1	Cartmanova
kočka	kočka	k1gFnSc1	kočka
<g/>
)	)	kIx)	)
Fluffy	Fluff	k1gInPc1	Fluff
(	(	kIx(	(
<g/>
Cartmanovo	Cartmanův	k2eAgNnSc1d1	Cartmanovo
prase	prase	k1gNnSc1	prase
<g/>
)	)	kIx)	)
Sparky	Sparek	k1gInPc1	Sparek
(	(	kIx(	(
<g/>
Stanův	Stanův	k2eAgMnSc1d1	Stanův
teplý	teplý	k2eAgMnSc1d1	teplý
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
Gobbles	Gobbles	k1gMnSc1	Gobbles
<g/>
/	/	kIx~	/
<g/>
Bubla	Bubla	k1gMnSc1	Bubla
(	(	kIx(	(
<g/>
Timmyho	Timmy	k1gMnSc4	Timmy
ochočený	ochočený	k2eAgMnSc1d1	ochočený
krocan	krocan	k1gMnSc1	krocan
<g/>
)	)	kIx)	)
...	...	k?	...
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Městečko	městečko	k1gNnSc1	městečko
South	South	k1gMnSc1	South
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
seriálu	seriál	k1gInSc2	seriál
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Bigger	Bigger	k1gMnSc1	Bigger
<g/>
,	,	kIx,	,
Longer	Longer	k1gMnSc1	Longer
&	&	k?	&
Uncut	Uncut	k1gInSc1	Uncut
(	(	kIx(	(
<g/>
přesný	přesný	k2eAgInSc1d1	přesný
překlad	překlad	k1gInSc1	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
Delší	dlouhý	k2eAgNnSc1d2	delší
&	&	k?	&
Nesestříhaný	sestříhaný	k2eNgMnSc1d1	nesestříhaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
uveden	uvést	k5eAaPmNgMnS	uvést
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
South	South	k1gMnSc1	South
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
Peklo	peklo	k1gNnSc1	peklo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Blackwell	Blackwella	k1gFnPc2	Blackwella
kniha	kniha	k1gFnSc1	kniha
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
a	a	k8xC	a
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
tváře	tvář	k1gFnPc1	tvář
současné	současný	k2eAgFnSc2d1	současná
světové	světový	k2eAgFnSc2d1	světová
filosofie	filosofie	k1gFnSc2	filosofie
zde	zde	k6eAd1	zde
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
seriál	seriál	k1gInSc1	seriál
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
směs	směs	k1gFnSc1	směs
fekálního	fekální	k2eAgInSc2d1	fekální
humoru	humor	k1gInSc2	humor
s	s	k7c7	s
rasovým	rasový	k2eAgInSc7d1	rasový
podtextem	podtext	k1gInSc7	podtext
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
autorská	autorský	k2eAgFnSc1d1	autorská
dvojka	dvojka	k1gFnSc1	dvojka
Trey	Trea	k1gFnSc2	Trea
Parker	Parker	k1gMnSc1	Parker
a	a	k8xC	a
Matt	Matt	k1gMnSc1	Matt
Stone	ston	k1gInSc5	ston
tvrdě	tvrdě	k6eAd1	tvrdě
otírají	otírat	k5eAaImIp3nP	otírat
o	o	k7c4	o
problémy	problém	k1gInPc4	problém
světové	světový	k2eAgFnSc2d1	světová
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
etiky	etika	k1gFnSc2	etika
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
u	u	k7c2	u
toho	ten	k3xDgNnSc2	ten
dobře	dobře	k6eAd1	dobře
pobavíte	pobavit	k5eAaPmIp2nP	pobavit
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
South	South	k1gInSc1	South
Park	park	k1gInSc4	park
<g/>
:	:	kIx,	:
Chef	Chef	k1gInSc1	Chef
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Luv	Luv	k1gFnSc7	Luv
Shack	Shack	k1gMnSc1	Shack
-	-	kIx~	-
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
Nintendo	Nintendo	k1gNnSc4	Nintendo
64	[number]	k4	64
<g/>
,	,	kIx,	,
Dreamcast	Dreamcast	k1gFnSc1	Dreamcast
<g/>
,	,	kIx,	,
PlayStation	PlayStation	k1gInSc1	PlayStation
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
/	/	kIx~	/
<g/>
PC	PC	kA	PC
<g/>
/	/	kIx~	/
1999	[number]	k4	1999
South	South	k1gInSc1	South
Park	park	k1gInSc4	park
Rally	Ralla	k1gFnSc2	Ralla
-	-	kIx~	-
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
PlayStation	PlayStation	k1gInSc4	PlayStation
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
/	/	kIx~	/
<g/>
PC	PC	kA	PC
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
Nintendo	Nintendo	k1gNnSc1	Nintendo
64	[number]	k4	64
a	a	k8xC	a
Dreamcast	Dreamcast	k1gMnSc1	Dreamcast
2012	[number]	k4	2012
South	South	k1gInSc1	South
Park	park	k1gInSc4	park
<g/>
:	:	kIx,	:
Tenorman	Tenorman	k1gMnSc1	Tenorman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Revenge	Revenge	k1gFnSc7	Revenge
-	-	kIx~	-
hra	hra	k1gFnSc1	hra
Xbox	Xbox	k1gInSc1	Xbox
Live	Live	k1gFnSc1	Live
Arcade	Arcad	k1gInSc5	Arcad
2014	[number]	k4	2014
South	South	k1gInSc1	South
Park	park	k1gInSc4	park
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Stick	Stick	k1gMnSc1	Stick
of	of	k?	of
Truth	Truth	k1gMnSc1	Truth
-	-	kIx~	-
RPG	RPG	kA	RPG
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
/	/	kIx~	/
<g/>
PC	PC	kA	PC
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
PlayStation	PlayStation	k1gInSc1	PlayStation
3	[number]	k4	3
<g/>
,	,	kIx,	,
Xbox	Xbox	k1gInSc1	Xbox
360	[number]	k4	360
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Městečko	městečko	k1gNnSc1	městečko
South	South	k1gInSc4	South
Park	park	k1gInSc1	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
Studios	Studios	k?	Studios
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Městečko	městečko	k1gNnSc1	městečko
South	Southa	k1gFnPc2	Southa
Park	park	k1gInSc1	park
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc6	Databas
Městečko	městečko	k1gNnSc1	městečko
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
South-Park	South-Park	k1gInSc1	South-Park
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
fanouškovská	fanouškovský	k2eAgFnSc1d1	fanouškovská
stránka	stránka	k1gFnSc1	stránka
</s>
