<s>
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Echo	echo	k1gNnSc1
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
2014	#num#	k4
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
Periodicita	periodicita	k1gFnSc1
</s>
<s>
týdeník	týdeník	k1gInSc1
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Sídlo	sídlo	k1gNnSc1
redakce	redakce	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
Cena	cena	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
,	,	kIx,
<g/>
-Kč	-Kč	k?
Klíčové	klíčový	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
Majitel	majitel	k1gMnSc1
</s>
<s>
Dalibor	Dalibor	k1gMnSc1
Balšínek	Balšínek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Klenor	Klenor	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
Ovčaří	Ovčarý	k2eAgMnPc5d1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Echo	echo	k1gNnSc1
Media	medium	k1gNnSc2
a.s.	a.s.	k?
Šéfredaktor	šéfredaktor	k1gMnSc1
</s>
<s>
Dalibor	Dalibor	k1gMnSc1
Balšínek	Balšínek	k1gMnSc1
Editor	editor	k1gMnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Janík	Janík	k1gMnSc1
Odkazy	odkaz	k1gInPc7
ISSN	ISSN	kA
</s>
<s>
2336-4971	2336-4971	k4
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
zpravodajský	zpravodajský	k2eAgInSc1d1
server	server	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Majitelem	majitel	k1gMnSc7
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
Echo	echo	k1gNnSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
většinově	většinově	k6eAd1
vlastní	vlastní	k2eAgMnSc1d1
šéfredaktor	šéfredaktor	k1gMnSc1
Dalibor	Dalibor	k1gMnSc1
Balšínek	Balšínek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
je	být	k5eAaImIp3nS
dle	dle	k7c2
vlastních	vlastní	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
pravicové	pravicový	k2eAgNnSc1d1
a	a	k8xC
konzervativní	konzervativní	k2eAgNnSc1d1
médium	médium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
koupil	koupit	k5eAaPmAgMnS
politik	politik	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
vydavatelství	vydavatelství	k1gNnSc2
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
a	a	k8xC
Mladou	mladý	k2eAgFnSc4d1
frontu	fronta	k1gFnSc4
DNES	dnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
několika	několik	k4yIc2
měsíců	měsíc	k1gInPc2
od	od	k7c2
oznámení	oznámení	k1gNnSc2
této	tento	k3xDgFnSc2
akvizice	akvizice	k1gFnSc2
opustila	opustit	k5eAaPmAgFnS
oba	dva	k4xCgInPc4
deníky	deník	k1gInPc4
část	část	k1gFnSc1
novinářů	novinář	k1gMnPc2
–	–	k?
z	z	k7c2
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
odešel	odejít	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
šéfredaktor	šéfredaktor	k1gMnSc1
Balšínek	Balšínek	k1gMnSc1
<g/>
,	,	kIx,
komentátoři	komentátor	k1gMnPc1
Lenka	Lenka	k1gFnSc1
Zlámalová	Zlámalová	k1gFnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Weiss	Weiss	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Štindl	Štindl	k1gMnSc1
<g/>
,	,	kIx,
šéfeditorka	šéfeditorka	k1gFnSc1
Kamila	Kamila	k1gFnSc1
Klausová	Klausová	k1gFnSc1
<g/>
,	,	kIx,
zástupce	zástupce	k1gMnSc2
šéfredaktora	šéfredaktor	k1gMnSc2
Jan	Jan	k1gMnSc1
Dražan	Dražan	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tito	tento	k3xDgMnPc1
novináři	novinář	k1gMnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
podíleli	podílet	k5eAaImAgMnP
na	na	k7c6
projektu	projekt	k1gInSc6
Echa	echo	k1gNnSc2
<g/>
24	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
oznámeno	oznámit	k5eAaPmNgNnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
a	a	k8xC
spuštěno	spuštěn	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Redaktoři	redaktor	k1gMnPc1
si	se	k3xPyFc3
vytkli	vytknout	k5eAaPmAgMnP
za	za	k7c4
cíl	cíl	k1gInSc4
„	„	k?
<g/>
být	být	k5eAaImF
protiváhou	protiváha	k1gFnSc7
oligarchizovaným	oligarchizovaný	k2eAgNnPc3d1
českým	český	k2eAgNnPc3d1
médiím	médium	k1gNnPc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
vychází	vycházet	k5eAaImIp3nS
každý	každý	k3xTgInSc1
pátek	pátek	k1gInSc1
také	také	k9
týdeník	týdeník	k1gInSc1
Echo	echo	k1gNnSc1
pro	pro	k7c4
tablety	tableta	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
v	v	k7c6
tištěné	tištěný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
náklad	náklad	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c4
30	#num#	k4
000	#num#	k4
kusech	kus	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
společnosti	společnost	k1gFnSc6
Echo	echo	k1gNnSc1
Media	medium	k1gNnSc2
vlastnil	vlastnit	k5eAaImAgMnS
Dalibor	Dalibor	k1gMnSc1
Balšínek	Balšínek	k1gMnSc1
95	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc1
<g/>
,	,	kIx,
zbývajících	zbývající	k2eAgNnPc2d1
5	#num#	k4
%	%	kIx~
vlastnila	vlastnit	k5eAaImAgFnS
obchodní	obchodní	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
Petra	Petra	k1gFnSc1
Feřtová	Feřtová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Hlavním	hlavní	k2eAgMnSc7d1
investorem	investor	k1gMnSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Klenor	Klenor	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Patria	Patrium	k1gNnSc2
Finance	finance	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
2020	#num#	k4
spoluvlastní	spoluvlastnit	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
Balšínek	Balšínko	k1gNnPc2
s	s	k7c7
bývalými	bývalý	k2eAgMnPc7d1
věřiteli	věřitel	k1gMnPc7
Klenorem	Klenor	k1gMnSc7
a	a	k8xC
Rudolfem	Rudolf	k1gMnSc7
Ovčařím	Ovčařím	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
2020	#num#	k4
společnost	společnost	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
nabídne	nabídnout	k5eAaPmIp3nS
51	#num#	k4
%	%	kIx~
svých	svůj	k3xOyFgFnPc2
akcí	akce	k1gFnPc2
předplatitelům	předplatitel	k1gMnPc3
a	a	k8xC
zbytek	zbytek	k1gInSc4
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
ovládnout	ovládnout	k5eAaPmF
Ovčaří	Ovčarý	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
týdeníku	týdeník	k1gInSc2
dále	daleko	k6eAd2
přispívají	přispívat	k5eAaImIp3nP
mj.	mj.	kA
Petr	Petr	k1gMnSc1
Holub	Holub	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Peňás	Peňás	k1gInSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Šmigol	Šmigol	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Durčák	Durčák	k1gMnSc1
<g/>
,	,	kIx,
Lucie	Lucie	k1gFnSc1
Sulovská	Sulovská	k1gFnSc1
či	či	k8xC
Marian	Marian	k1gMnSc1
Kechlibar	Kechlibar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Spor	spor	k1gInSc1
s	s	k7c7
Andrejem	Andrej	k1gMnSc7
Babišem	Babiš	k1gMnSc7
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
server	server	k1gInSc1
zveřejnil	zveřejnit	k5eAaPmAgInS
rozhovor	rozhovor	k1gInSc1
redaktora	redaktor	k1gMnSc2
Daniela	Daniel	k1gMnSc2
Kaisera	Kaiser	k1gMnSc2
s	s	k7c7
Helenou	Helena	k1gFnSc7
Válkovou	Válková	k1gFnSc7
<g/>
,	,	kIx,
ministryní	ministryně	k1gFnSc7
spravedlnosti	spravedlnost	k1gFnSc2
za	za	k7c4
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgInS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
Babišova	Babišův	k2eAgNnSc2d1
zbohatnutí	zbohatnutí	k1gNnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
údajné	údajný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
se	s	k7c7
Státní	státní	k2eAgFnSc7d1
bezpečností	bezpečnost	k1gFnSc7
a	a	k8xC
možného	možný	k2eAgNnSc2d1
ovlivňování	ovlivňování	k1gNnSc2
obsahu	obsah	k1gInSc2
novin	novina	k1gFnPc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
rozhovoru	rozhovor	k1gInSc2
bylo	být	k5eAaImAgNnS
postavení	postavení	k1gNnSc1
Čechů	Čech	k1gMnPc2
za	za	k7c2
Protektorátu	protektorát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Babišova	Babišův	k2eAgNnSc2d1
následného	následný	k2eAgNnSc2d1
vyjádření	vyjádření	k1gNnSc2
byl	být	k5eAaImAgInS
rozhovor	rozhovor	k1gInSc4
tendeční	tendeční	k2eAgInSc4d1
<g/>
,	,	kIx,
provokativní	provokativní	k2eAgInSc4d1
a	a	k8xC
vedený	vedený	k2eAgInSc4d1
za	za	k7c7
účelem	účel	k1gInSc7
ministryni	ministryně	k1gFnSc4
poškodit	poškodit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
byl	být	k5eAaImAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
vytvořen	vytvořen	k2eAgMnSc1d1
za	za	k7c7
účelem	účel	k1gInSc7
napadat	napadat	k5eAaBmF,k5eAaPmF,k5eAaImF
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
samotného	samotný	k2eAgNnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
hnutí	hnutí	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
redaktory	redaktor	k1gMnPc4
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
za	za	k7c4
„	„	k?
<g/>
tuneláře	tunelář	k1gMnSc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vytunelovali	vytunelovat	k5eAaPmAgMnP
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
udělali	udělat	k5eAaPmAgMnP
tam	tam	k6eAd1
50	#num#	k4
milionů	milion	k4xCgInPc2
ztrátu	ztráta	k1gFnSc4
a	a	k8xC
fandili	fandit	k5eAaImAgMnP
ODS	ODS	kA
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
celou	celý	k2eAgFnSc7d1
věcí	věc	k1gFnSc7
měli	mít	k5eAaImAgMnP
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
stát	stát	k5eAaPmF,k5eAaImF
místopředseda	místopředseda	k1gMnSc1
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Miroslav	Miroslav	k1gMnSc1
Kalousek	Kalousek	k1gMnSc1
a	a	k8xC
ředitel	ředitel	k1gMnSc1
ČEZ	ČEZ	kA
Martin	Martin	k1gMnSc1
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
dále	daleko	k6eAd2
prohlásil	prohlásit	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Doufám	doufat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
váš	váš	k3xOp2gMnSc1
bílý	bílý	k1gMnSc1
kůň	kůň	k1gMnSc1
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
Klenor	Klenor	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
velké	velký	k2eAgNnSc4d1
majetkové	majetkový	k2eAgNnSc4d1
přiznání	přiznání	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
prokázal	prokázat	k5eAaPmAgInS
potom	potom	k6eAd1
ty	ten	k3xDgInPc4
vaše	váš	k3xOp2gInPc4
náklady	náklad	k1gInPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Adama	Adam	k1gMnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
Drdy	Drda	k1gMnSc2
z	z	k7c2
Revolver	revolver	k1gInSc4
Revue	revue	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Jiřího	Jiří	k1gMnSc4
X.	X.	kA
Doležala	Doležal	k1gMnSc4
z	z	k7c2
Reflexu	reflex	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
bývalého	bývalý	k2eAgMnSc2d1
politika	politik	k1gMnSc2
Miroslava	Miroslav	k1gMnSc2
Macka	Macek	k1gMnSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
serveru	server	k1gInSc2
MediaGuru	MediaGur	k1gInSc2
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
tím	ten	k3xDgNnSc7
z	z	k7c2
pozice	pozice	k1gFnSc2
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
Janu	Jan	k1gMnSc6
Klenorovi	Klenor	k1gMnSc6
pohrozil	pohrozit	k5eAaPmAgMnS
finanční	finanční	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Drdy	Drda	k1gMnSc2
tím	ten	k3xDgNnSc7
navíc	navíc	k6eAd1
vyhrožoval	vyhrožovat	k5eAaImAgMnS
potenciálním	potenciální	k2eAgMnSc7d1
konkurentům	konkurent	k1gMnPc3
svých	svůj	k3xOyFgNnPc2
periodik	periodikum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Šéfredaktor	šéfredaktor	k1gMnSc1
Balšínek	Balšínek	k1gMnSc1
všechna	všechen	k3xTgNnPc4
Babišova	Babišův	k2eAgNnPc4d1
nařčení	nařčení	k1gNnPc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Babiš	Babiš	k1gInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
za	za	k7c4
svá	svůj	k3xOyFgNnPc4
slova	slovo	k1gNnPc4
omluvil	omluvit	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nemá	mít	k5eNaImIp3nS
a	a	k8xC
nikdy	nikdy	k6eAd1
neměl	mít	k5eNaImAgMnS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
prověřovat	prověřovat	k5eAaImF
hospodaření	hospodaření	k1gNnSc4
zpravodajského	zpravodajský	k2eAgInSc2d1
serveru	server	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
problém	problém	k1gInSc4
si	se	k3xPyFc3
zvyknout	zvyknout	k5eAaPmF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k8xS,k8xC
politik	politik	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
říkat	říkat	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
si	se	k3xPyFc3
doopravdy	doopravdy	k6eAd1
myslí	myslet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
května	květen	k1gInSc2
2014	#num#	k4
v	v	k7c6
redakci	redakce	k1gFnSc6
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
skutečně	skutečně	k6eAd1
proběhla	proběhnout	k5eAaPmAgFnS
kontrola	kontrola	k1gFnSc1
z	z	k7c2
finančního	finanční	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
redakce	redakce	k1gFnSc2
nebyly	být	k5eNaImAgInP
při	při	k7c6
kontrole	kontrola	k1gFnSc6
nalezeny	nalézt	k5eAaBmNgFnP,k5eAaPmNgFnP
žádné	žádný	k3yNgFnPc1
nesrovnalosti	nesrovnalost	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BALŠÍNEK	BALŠÍNEK	kA
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spouštíme	spouštět	k5eAaImIp1nP
Echo	echo	k1gNnSc4
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
nový	nový	k2eAgInSc4d1
internetový	internetový	k2eAgInSc4d1
deník	deník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-16	2014-03-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://echo24.cz/a/ifvhZ/nevite-si-rady-a-nechcete-medvidka-myvala-darujte-tydenik-echo	http://echo24.cz/a/ifvhZ/nevite-si-rady-a-nechcete-medvidka-myvala-darujte-tydenik-echo	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Výzkum	výzkum	k1gInSc1
<g/>
:	:	kIx,
Echo	echo	k1gNnSc1
má	mít	k5eAaImIp3nS
vysokoškolské	vysokoškolský	k2eAgInPc4d1
a	a	k8xC
pravicové	pravicový	k2eAgMnPc4d1
čtenáře	čtenář	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
MediaGuru	MediaGur	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.ceskatelevize.cz/ct24/ekonomika/232763-babis-koupil-mafru-do-obsahu-novin-zasahovat-nebude-rika-jeho-mluvci/	http://www.ceskatelevize.cz/ct24/ekonomika/232763-babis-koupil-mafru-do-obsahu-novin-zasahovat-nebude-rika-jeho-mluvci/	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://hlidacipes.org/babisuv-seznam/	http://hlidacipes.org/babisuv-seznam/	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://www.mediar.cz/byvaly-sef-ln-balsinek-chysta-echo-novy-online-denik-a-tabletovy-tydenik/	http://www.mediar.cz/byvaly-sef-ln-balsinek-chysta-echo-novy-online-denik-a-tabletovy-tydenik/	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
BALŠÍNEK	BALŠÍNEK	kA
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
:	:	kIx,
Chceme	chtít	k5eAaImIp1nP
být	být	k5eAaImF
protiváhou	protiváha	k1gFnSc7
oligarchizovaným	oligarchizovaný	k2eAgNnPc3d1
českým	český	k2eAgNnPc3d1
médiím	médium	k1gNnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
nedatováno	datován	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://echo24.cz/a/i5U9M/spoustime-tydenik-echo	http://echo24.cz/a/i5U9M/spoustime-tydenik-echo	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://www.rozhlas.cz/zpravy/politika/_zprava/vychazi-novy-tydenik-echo-chce-ziskat-narocne-ctenare-a-jit-proti-vsem-trendum--1417782	http://www.rozhlas.cz/zpravy/politika/_zprava/vychazi-novy-tydenik-echo-chce-ziskat-narocne-ctenare-a-jit-proti-vsem-trendum--1417782	k4
<g/>
↑	↑	k?
http://www.mediaguru.cz/2014/01/podil-ve-firme-echo-media-si-rozdeli-dva-spolecnici/#.UyY9adzcGd4	http://www.mediaguru.cz/2014/01/podil-ve-firme-echo-media-si-rozdeli-dva-spolecnici/#.UyY9adzcGd4	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
http://www.mediaguru.cz/2014/01/podil-ve-firme-echo-media-si-rozdeli-dva-spolecnici/#.UyY9adzcGd4	http://www.mediaguru.cz/2014/01/podil-ve-firme-echo-media-si-rozdeli-dva-spolecnici/#.UyY9adzcGd4	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
https://www.mediar.cz/echo-media-ma-nove-akcionare-klenor-a-ovcari-kapitalizovali-sve-pohledavky	https://www.mediar.cz/echo-media-ma-nove-akcionare-klenor-a-ovcari-kapitalizovali-sve-pohledavka	k1gFnSc2
<g/>
↑	↑	k?
http://www.ceskamedia.cz/clanek/286204/echo-nabidne-51-svych-akcii-ctenarum-zbytek-ziska-rudolf-ovcari	http://www.ceskamedia.cz/clanek/286204/echo-nabidne-51-svych-akcii-ctenarum-zbytek-ziska-rudolf-ovcar	k1gFnSc2
<g/>
↑	↑	k?
https://echo24.cz/a/SB2Ge/pokusime-se-o-svetovy-prulom-51--akcii-echa-pujde-mezi-ctenare-rika-rudolf-ovcari	https://echo24.cz/a/SB2Ge/pokusime-se-o-svetovy-prulom-51--akcii-echa-pujde-mezi-ctenare-rika-rudolf-ovcar	k1gFnSc2
<g/>
↑	↑	k?
KAISER	KAISER	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nP
byl	být	k5eAaImAgMnS
Babiš	Babiš	k1gMnSc1
agent	agent	k1gMnSc1
<g/>
,	,	kIx,
mrzelo	mrzet	k5eAaImAgNnS
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
to	ten	k3xDgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministryní	ministryně	k1gFnPc2
by	by	kYmCp3nP
Válková	Válková	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
<g/>
.	.	kIx.
echo	echo	k1gNnSc1
<g/>
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-22	2014-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Babiš	Babiš	k1gInSc1
<g/>
:	:	kIx,
Celý	celý	k2eAgInSc1d1
rozhovor	rozhovor	k1gInSc1
byl	být	k5eAaImAgInS
veden	vést	k5eAaImNgInS
za	za	k7c7
účelem	účel	k1gInSc7
ministryni	ministryně	k1gFnSc4
poškodit	poškodit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
DRDA	Drda	k1gMnSc1
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
<g/>
,	,	kIx,
média	médium	k1gNnPc1
<g/>
,	,	kIx,
souznění	souznění	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revolver	revolver	k1gInSc1
Revue	revue	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-31	2014-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JIŘÍ	Jiří	k1gMnSc1
X.	X.	kA
<g/>
,	,	kIx,
Doležal	Doležal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
vyhrožuje	vyhrožovat	k5eAaImIp3nS
novinářům	novinář	k1gMnPc3
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
mlčí	mlčet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-25	2014-03-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Páteční	páteční	k2eAgFnPc4d1
glosy	glosa	k1gFnPc4
28	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macek	Macek	k1gMnSc1
v	v	k7c6
botách	bota	k1gFnPc6
<g/>
,	,	kIx,
2014-03-28	2014-03-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Babiš	Babiš	k1gMnSc1
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
Echo	echo	k1gNnSc4
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
deník	deník	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
MediaGuru	MediaGur	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-24	2014-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BALŠÍNEK	BALŠÍNEK	kA
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
s	s	k7c7
kým	kdo	k3yQnSc7,k3yInSc7,k3yRnSc7
máme	mít	k5eAaImIp1nP
tu	tu	k6eAd1
čest	čest	k1gFnSc4
<g/>
,	,	kIx,
pane	pan	k1gMnSc5
Babiši	Babiš	k1gMnSc5
<g/>
.	.	kIx.
echo	echo	k1gNnSc4
<g/>
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-24	2014-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gMnSc1
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
novinách	novina	k1gFnPc6
omluvil	omluvit	k5eAaPmAgMnS
za	za	k7c4
výroky	výrok	k1gInPc4
o	o	k7c6
serveru	server	k1gInSc6
Echo	echo	k1gNnSc1
<g/>
.	.	kIx.
aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-27	2014-03-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalousek	Kalousek	k1gMnSc1
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c4
finanční	finanční	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
serveru	server	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
kritizoval	kritizovat	k5eAaImAgInS
Babiš	Babiš	k1gInSc4
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-06	2014-05-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ghostbuster	Ghostbuster	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrola	kontrola	k1gFnSc1
z	z	k7c2
finančního	finanční	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
dopadla	dopadnout	k5eAaPmAgFnS
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
echo	echo	k1gNnSc4
<g/>
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-19	2014-05-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
</s>
<s>
Týdeník	týdeník	k1gInSc1
Echo	echo	k1gNnSc1
(	(	kIx(
<g/>
placený	placený	k2eAgInSc1d1
přístup	přístup	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
</s>
