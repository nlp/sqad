<s>
Tunel	tunel	k1gInSc1
Sitina	Sitina	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
Tunel	tunel	k1gInSc1
Sitiny	Sitina	k1gFnPc1
<g/>
,	,	kIx,
či	či	k8xC
Tunel	tunel	k1gInSc1
Františka	František	k1gMnSc2
je	být	k5eAaImIp3nS
dálniční	dálniční	k2eAgInSc4d1
tunel	tunel	k1gInSc4
na	na	k7c6
dálnici	dálnice	k1gFnSc6
D	D	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
její	její	k3xOp3gFnSc6
části	část	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
prochází	procházet	k5eAaImIp3nS
slovenskou	slovenský	k2eAgFnSc7d1
metropolí	metropol	k1gFnSc7
Bratislavou	Bratislava	k1gFnSc7
(	(	kIx(
<g/>
úsek	úsek	k1gInSc1
Lamačská	lamačský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
–	–	k?
Staré	Staré	k2eAgInPc1d1
grunty	grunt	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>