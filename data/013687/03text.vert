<s>
Tunel	tunel	k1gInSc1
Sitina	Sitino	k1gNnSc2
</s>
<s>
Tunel	tunel	k1gInSc1
Sitina	Sitino	k1gNnSc2
Severní	severní	k2eAgInSc1d1
portál	portál	k1gInSc1
tunelu	tunel	k1gInSc2
při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
otevření	otevření	k1gNnSc6
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
Výstavba	výstavba	k1gFnSc1
</s>
<s>
2007	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
46,37	46,37	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
29,64	29,64	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tunel	tunel	k1gInSc1
Sitina	Sitina	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
Tunel	tunel	k1gInSc1
Sitiny	Sitina	k1gFnPc1
<g/>
,	,	kIx,
či	či	k8xC
Tunel	tunel	k1gInSc1
Františka	František	k1gMnSc2
je	být	k5eAaImIp3nS
dálniční	dálniční	k2eAgInSc4d1
tunel	tunel	k1gInSc4
na	na	k7c6
dálnici	dálnice	k1gFnSc6
D	D	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
její	její	k3xOp3gFnSc6
části	část	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
prochází	procházet	k5eAaImIp3nS
slovenskou	slovenský	k2eAgFnSc7d1
metropolí	metropol	k1gFnSc7
Bratislavou	Bratislava	k1gFnSc7
(	(	kIx(
<g/>
úsek	úsek	k1gInSc1
Lamačská	lamačský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
–	–	k?
Staré	Staré	k2eAgInPc1d1
grunty	grunt	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tunel	tunel	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
dva	dva	k4xCgInPc4
menší	malý	k2eAgInPc4d2
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
pro	pro	k7c4
dva	dva	k4xCgInPc4
pruhy	pruh	k1gInPc4
a	a	k8xC
jeden	jeden	k4xCgInSc4
směr	směr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
tunelu	tunel	k1gInSc2
je	být	k5eAaImIp3nS
ražená	ražený	k2eAgFnSc1d1
(	(	kIx(
<g/>
západní	západní	k2eAgFnSc1d1
dlouhá	dlouhý	k2eAgFnSc1d1
1189	#num#	k4
m	m	kA
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc1d1
pak	pak	k6eAd1
1159	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
část	část	k1gFnSc1
hloubená	hloubený	k2eAgFnSc1d1
(	(	kIx(
<g/>
západní	západní	k2eAgFnSc1d1
206	#num#	k4
m	m	kA
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc2d1
216	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohromady	dohromady	k6eAd1
je	být	k5eAaImIp3nS
tak	tak	k9
tunel	tunel	k1gInSc1
dlouhý	dlouhý	k2eAgInSc1d1
1440	#num#	k4
m.	m.	k?
</s>
<s>
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
budována	budovat	k5eAaImNgFnS
tzv.	tzv.	kA
novou	nový	k2eAgFnSc7d1
rakouskou	rakouský	k2eAgFnSc7d1
tunelovací	tunelovací	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
(	(	kIx(
<g/>
NRTM	NRTM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodů	důvod	k1gInPc2
bezpečnosti	bezpečnost	k1gFnSc2
byly	být	k5eAaImAgFnP
vyraženy	vyražen	k2eAgFnPc1d1
i	i	k8xC
únikové	únikový	k2eAgFnPc1d1
chodby	chodba	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
celkem	celkem	k6eAd1
na	na	k7c6
pěti	pět	k4xCc6
místech	místo	k1gNnPc6
<g/>
;	;	kIx,
součástí	součást	k1gFnSc7
stavby	stavba	k1gFnSc2
jsou	být	k5eAaImIp3nP
též	též	k9
i	i	k9
SOS	SOS	kA
budky	budka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
jsou	být	k5eAaImIp3nP
chodníky	chodník	k1gInPc1
<g/>
,	,	kIx,
široké	široký	k2eAgFnPc1d1
1	#num#	k4
m	m	kA
<g/>
,	,	kIx,
vozovka	vozovka	k1gFnSc1
sama	sám	k3xTgMnSc4
pak	pak	k9
byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
v	v	k7c6
šířce	šířka	k1gFnSc6
7,5	7,5	k4
m.	m.	k?
</s>
<s>
Projektová	projektový	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
tunelu	tunel	k1gInSc2
začala	začít	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1996	#num#	k4
a	a	k8xC
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ražba	ražba	k1gFnSc1
tunelu	tunel	k1gInSc2
začala	začít	k5eAaPmAgFnS
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
probíhala	probíhat	k5eAaImAgFnS
montáž	montáž	k1gFnSc1
technologických	technologický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
byly	být	k5eAaImAgInP
3,5	3,5	k4
miliardy	miliarda	k4xCgFnSc2
Sk	Sk	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tunel	tunel	k1gInSc1
Sitina	Sitina	k1gMnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c6
tunelu	tunel	k1gInSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
národní	národní	k2eAgFnSc2d1
dálniční	dálniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
www.ndsas.sk	www.ndsas.sk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bratislava	Bratislava	k1gFnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
