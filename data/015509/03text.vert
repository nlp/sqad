<s>
Insomnie	insomnie	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
poruše	porucha	k1gFnSc6
spánku	spánek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Insomnie	insomnie	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Insomnie	insomnie	k1gFnSc1
(	(	kIx(
<g/>
nespavost	nespavost	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
porucha	porucha	k1gFnSc1
spánku	spánek	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
jedinec	jedinec	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
usnout	usnout	k5eAaPmF
nebo	nebo	k8xC
se	se	k3xPyFc4
v	v	k7c6
spánku	spánek	k1gInSc6
často	často	k6eAd1
probouzí	probouzet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Insomniak	Insomniak	k1gMnSc1
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
trpící	trpící	k2eAgInSc4d1
insomnií	insomnie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Příznaky	příznak	k1gInPc1
insomnie	insomnie	k1gFnSc2
</s>
<s>
Insomnie	insomnie	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
projevovat	projevovat	k5eAaImF
několika	několik	k4yIc7
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
obtížné	obtížný	k2eAgNnSc1d1
noční	noční	k2eAgNnSc1d1
usínání	usínání	k1gNnSc1
</s>
<s>
časné	časný	k2eAgNnSc1d1
ranní	ranní	k2eAgNnSc1d1
probouzení	probouzení	k1gNnSc1
bez	bez	k7c2
schopnosti	schopnost	k1gFnSc2
znovu	znovu	k6eAd1
usnout	usnout	k5eAaPmF
</s>
<s>
časté	častý	k2eAgNnSc1d1
noční	noční	k2eAgNnSc1d1
probouzení	probouzení	k1gNnSc1
</s>
<s>
noční	noční	k2eAgFnSc1d1
bdělost	bdělost	k1gFnSc1
<g/>
,	,	kIx,
záměna	záměna	k1gFnSc1
dne	den	k1gInSc2
a	a	k8xC
noci	noc	k1gFnSc2
(	(	kIx(
<g/>
paradoxní	paradoxní	k2eAgFnSc2d1
insomnie	insomnie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Typy	typ	k1gInPc1
insomnie	insomnie	k1gFnSc2
</s>
<s>
Insomnie	insomnie	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
několik	několik	k4yIc4
typů	typ	k1gInPc2
především	především	k6eAd1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
délky	délka	k1gFnSc2
trvání	trvání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přechodná	přechodný	k2eAgFnSc1d1
insomnie	insomnie	k1gFnSc1
je	být	k5eAaImIp3nS
stav	stav	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
trvá	trvat	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
do	do	k7c2
jednoho	jeden	k4xCgInSc2
týdne	týden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
bývá	bývat	k5eAaImIp3nS
způsobený	způsobený	k2eAgInSc4d1
stresem	stres	k1gInSc7
a	a	k8xC
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
považovat	považovat	k5eAaImF
za	za	k7c4
patologický	patologický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
insomnie	insomnie	k1gFnPc4
zpravidla	zpravidla	k6eAd1
po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
odezní	odeznět	k5eAaPmIp3nS
sama	sám	k3xTgFnSc1
(	(	kIx(
<g/>
často	často	k6eAd1
po	po	k7c6
zmizení	zmizení	k1gNnSc6
stresoru	stresor	k1gInSc2
–	–	k?
vykonání	vykonání	k1gNnSc1
zkoušky	zkouška	k1gFnSc2
<g/>
,	,	kIx,
pohovoru	pohovor	k1gInSc2
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Akutní	akutní	k2eAgFnSc1d1
insomnie	insomnie	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
stress-related	stress-related	k1gMnSc1
insomnia	insomnium	k1gNnSc2
<g/>
,	,	kIx,
trvání	trvání	k1gNnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
1	#num#	k4
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
zásah	zásah	k1gInSc1
lékaře	lékař	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
stav	stav	k1gInSc1
insomniaka	insomniak	k1gMnSc2
zkontroluje	zkontrolovat	k5eAaPmIp3nS
a	a	k8xC
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
nutná	nutný	k2eAgFnSc1d1
léčba	léčba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1
insomnie	insomnie	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
primární	primární	k2eAgFnSc1d1
(	(	kIx(
<g/>
vznikla	vzniknout	k5eAaPmAgFnS
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
např.	např.	kA
kvůli	kvůli	k7c3
organickému	organický	k2eAgNnSc3d1
poškození	poškození	k1gNnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
anebo	anebo	k8xC
sekundární	sekundární	k2eAgMnSc1d1
(	(	kIx(
<g/>
vzniká	vznikat	k5eAaImIp3nS
vlivem	vliv	k1gInSc7
jiné	jiný	k2eAgFnSc2d1
nemoci	nemoc	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
deprese	deprese	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhotrvající	dlouhotrvající	k2eAgFnSc1d1
insomnie	insomnie	k1gFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
vážné	vážný	k2eAgInPc4d1
zdravotní	zdravotní	k2eAgInPc4d1
následky	následek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Fatální	fatální	k2eAgFnSc1d1
familiární	familiární	k2eAgFnSc1d1
insomnie	insomnie	k1gFnSc1
tento	tento	k3xDgInSc4
typ	typ	k1gInSc4
insomnie	insomnie	k1gFnSc2
je	být	k5eAaImIp3nS
smrtelný	smrtelný	k2eAgInSc1d1
a	a	k8xC
nedá	dát	k5eNaPmIp3nS
se	se	k3xPyFc4
léčit	léčit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
s	s	k7c7
touto	tento	k3xDgFnSc7
insomnií	insomnie	k1gFnSc7
postupně	postupně	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
usnout	usnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
dosáhne	dosáhnout	k5eAaPmIp3nS
stavu	stav	k1gInSc3
naprosté	naprostý	k2eAgFnSc2d1
insomnie	insomnie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
za	za	k7c2
odlišných	odlišný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
není	být	k5eNaImIp3nS
uskutečnitelná	uskutečnitelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
insomnií	insomnie	k1gFnSc7
trpí	trpět	k5eAaImIp3nS
pouze	pouze	k6eAd1
pár	pár	k4xCyI
set	set	k1gInSc4
lidí	člověk	k1gMnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
jsou	být	k5eAaImIp3nP
priony	prion	k1gInPc1
<g/>
,	,	kIx,
napadající	napadající	k2eAgNnPc1d1
určitá	určitý	k2eAgNnPc1d1
thalamická	thalamický	k2eAgNnPc1d1
jádra	jádro	k1gNnPc1
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
onemocnění	onemocnění	k1gNnSc1
je	být	k5eAaImIp3nS
dědičné	dědičný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
insomnie	insomnie	k1gFnSc2
</s>
<s>
Nespavost	nespavost	k1gFnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
velmi	velmi	k6eAd1
častým	častý	k2eAgInPc3d1
zdravotním	zdravotní	k2eAgInPc3d1
problémům	problém	k1gInPc3
současné	současný	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
pokusy	pokus	k1gInPc1
prokázaly	prokázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
mnozí	mnohý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
domnívající	domnívající	k2eAgMnSc1d1
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
trpí	trpět	k5eAaImIp3nS
insomnií	insomnie	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
spí	spát	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
jako	jako	k8xS,k8xC
ostatní	ostatní	k2eAgFnPc4d1
<g/>
,	,	kIx,
ovšem	ovšem	k9
často	často	k6eAd1
se	se	k3xPyFc4
probouzejí	probouzet	k5eAaImIp3nP
<g/>
,	,	kIx,
neupadají	upadat	k5eNaPmIp3nP,k5eNaImIp3nP
do	do	k7c2
hlubokého	hluboký	k2eAgInSc2d1
spánku	spánek	k1gInSc2
<g/>
,	,	kIx,
špatně	špatně	k6eAd1
odpočívají	odpočívat	k5eAaImIp3nP
atd.	atd.	kA
<g/>
,	,	kIx,
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
parasomnii	parasomnie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Určitou	určitý	k2eAgFnSc7d1
občasnou	občasný	k2eAgFnSc7d1
nespavostí	nespavost	k1gFnSc7
trpí	trpět	k5eAaImIp3nS
téměř	téměř	k6eAd1
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noční	noční	k2eAgInSc4d1
bdění	bdění	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
spojeno	spojit	k5eAaPmNgNnS
s	s	k7c7
psychickými	psychický	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
,	,	kIx,
člověk	člověk	k1gMnSc1
podléhá	podléhat	k5eAaImIp3nS
chmurným	chmurný	k2eAgFnPc3d1
představám	představa	k1gFnPc3
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
podrážděný	podrážděný	k2eAgMnSc1d1
<g/>
,	,	kIx,
zmatený	zmatený	k2eAgMnSc1d1
<g/>
,	,	kIx,
mívá	mívat	k5eAaImIp3nS
sklon	sklon	k1gInSc4
ke	k	k7c3
smyslovým	smyslový	k2eAgInPc3d1
klamům	klam	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nespavost	nespavost	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
doprovodným	doprovodný	k2eAgInSc7d1
symptomem	symptom	k1gInSc7
tělesných	tělesný	k2eAgInPc2d1
nebo	nebo	k8xC
duševních	duševní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
můžeme	moct	k5eAaImIp1nP
hovořit	hovořit	k5eAaImF
o	o	k7c6
nespavosti	nespavost	k1gFnSc6
jako	jako	k8xS,k8xC
o	o	k7c6
primárním	primární	k2eAgNnSc6d1
onemocnění	onemocnění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postihuje	postihovat	k5eAaImIp3nS
asi	asi	k9
15	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
</s>
<s>
Mezi	mezi	k7c4
nejčastější	častý	k2eAgFnPc4d3
příčiny	příčina	k1gFnPc4
patří	patřit	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
stres	stres	k1gInSc1
<g/>
,	,	kIx,
nadměrná	nadměrný	k2eAgFnSc1d1
psychická	psychický	k2eAgFnSc1d1
zátěž	zátěž	k1gFnSc1
</s>
<s>
nepravidelný	pravidelný	k2eNgInSc1d1
spánkový	spánkový	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
změna	změna	k1gFnSc1
přirozeného	přirozený	k2eAgInSc2d1
biologického	biologický	k2eAgInSc2d1
rytmu	rytmus	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
při	při	k7c6
změně	změna	k1gFnSc6
časového	časový	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
–	–	k?
pásmová	pásmový	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
<g/>
,	,	kIx,
přechod	přechod	k1gInSc1
z	z	k7c2
letního	letní	k2eAgInSc2d1
na	na	k7c4
standardní	standardní	k2eAgInSc4d1
čas	čas	k1gInSc4
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
deprese	deprese	k1gFnSc1
</s>
<s>
kofein	kofein	k1gInSc1
a	a	k8xC
alkohol	alkohol	k1gInSc1
</s>
<s>
užívání	užívání	k1gNnSc1
některých	některý	k3yIgInPc2
léků	lék	k1gInPc2
</s>
<s>
špatné	špatný	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
pro	pro	k7c4
spánek	spánek	k1gInSc4
</s>
<s>
nemoc	nemoc	k1gFnSc1
(	(	kIx(
<g/>
zdravotní	zdravotní	k2eAgFnPc1d1
obtíže	obtíž	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
nedostatek	nedostatek	k1gInSc1
pohybu	pohyb	k1gInSc2
během	během	k7c2
dne	den	k1gInSc2
</s>
<s>
nadměrná	nadměrný	k2eAgFnSc1d1
konzumace	konzumace	k1gFnSc1
jídla	jídlo	k1gNnSc2
nebo	nebo	k8xC
pití	pití	k1gNnSc2
večer	večer	k6eAd1
</s>
<s>
syndrom	syndrom	k1gInSc1
neklidných	klidný	k2eNgFnPc2d1
nohou	noha	k1gFnPc2
–	–	k?
člověk	člověk	k1gMnSc1
při	při	k7c6
usínání	usínání	k1gNnSc6
cuká	cukat	k5eAaImIp3nS
nohama	noha	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ho	on	k3xPp3gMnSc4
probouzí	probouzet	k5eAaImIp3nS
a	a	k8xC
znesnadňuje	znesnadňovat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
usnutí	usnutí	k1gNnSc1
</s>
<s>
tělesná	tělesný	k2eAgNnPc1d1
onemocnění	onemocnění	k1gNnPc1
–	–	k?
srdeční	srdeční	k2eAgNnSc1d1
selhání	selhání	k1gNnSc1
<g/>
,	,	kIx,
hypertenze	hypertenze	k1gFnSc1
</s>
<s>
Důsledky	důsledek	k1gInPc1
insomnie	insomnie	k1gFnSc2
</s>
<s>
Nespavost	nespavost	k1gFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
během	během	k7c2
dne	den	k1gInSc2
unavený	unavený	k2eAgMnSc1d1
<g/>
,	,	kIx,
malátný	malátný	k2eAgInSc1d1
<g/>
,	,	kIx,
podrážděný	podrážděný	k2eAgInSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
zhoršenou	zhoršený	k2eAgFnSc4d1
koncentraci	koncentrace	k1gFnSc4
<g/>
,	,	kIx,
sníženou	snížený	k2eAgFnSc4d1
pracovní	pracovní	k2eAgFnSc4d1
výkonnost	výkonnost	k1gFnSc4
<g/>
,	,	kIx,
špatnou	špatný	k2eAgFnSc4d1
náladu	nálada	k1gFnSc4
<g/>
,	,	kIx,
sklon	sklon	k1gInSc1
k	k	k7c3
depresím	deprese	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
problémy	problém	k1gInPc4
souvisí	souviset	k5eAaImIp3nS
hodně	hodně	k6eAd1
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
kortizolu	kortizol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1
spánku	spánek	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
jistém	jistý	k2eAgInSc6d1
čase	čas	k1gInSc6
projevuje	projevovat	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k9
opilost	opilost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dlouhotrvající	dlouhotrvající	k2eAgFnSc6d1
nespavosti	nespavost	k1gFnSc6
může	moct	k5eAaImIp3nS
dokonce	dokonce	k9
docházet	docházet	k5eAaImF
k	k	k7c3
psychickým	psychický	k2eAgFnPc3d1
poruchám	porucha	k1gFnPc3
<g/>
,	,	kIx,
poruchám	porucha	k1gFnPc3
vnímání	vnímání	k1gNnSc2
<g/>
,	,	kIx,
úsudku	úsudek	k1gInSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
kognitivních	kognitivní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Léčba	léčba	k1gFnSc1
nespavosti	nespavost	k1gFnSc2
</s>
<s>
Spánková	spánkový	k2eAgFnSc1d1
hygiena	hygiena	k1gFnSc1
</s>
<s>
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
způsobů	způsob	k1gInPc2
nefarmakologické	farmakologický	k2eNgFnSc2d1
léčby	léčba	k1gFnSc2
je	být	k5eAaImIp3nS
úprava	úprava	k1gFnSc1
spánkové	spánkový	k2eAgFnSc2d1
hygieny	hygiena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
přehled	přehled	k1gInSc4
obecných	obecný	k2eAgNnPc2d1
doporučení	doporučení	k1gNnPc2
jedincům	jedinec	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
mají	mít	k5eAaImIp3nP
problémy	problém	k1gInPc4
s	s	k7c7
nedostatečným	dostatečný	k2eNgInSc7d1
a	a	k8xC
nekvalitním	kvalitní	k2eNgInSc7d1
spánkem	spánek	k1gInSc7
a	a	k8xC
u	u	k7c2
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
bylo	být	k5eAaImAgNnS
vyloučeno	vyloučit	k5eAaPmNgNnS
onemocnění	onemocnění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
nespavost	nespavost	k1gFnSc1
může	moct	k5eAaImIp3nS
způsobovat	způsobovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Prostředí	prostředí	k1gNnSc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
co	co	k9
možná	možná	k9
nejpohodlnější	pohodlný	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokoj	pokoj	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
spí	spát	k5eAaImIp3nS
<g/>
,	,	kIx,
by	by	kYmCp3nS
neměl	mít	k5eNaImAgInS
zároveň	zároveň	k6eAd1
sloužit	sloužit	k5eAaImF
jako	jako	k9
pracovna	pracovna	k1gFnSc1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
by	by	kYmCp3nS
se	se	k3xPyFc4
užívat	užívat	k5eAaImF
pouze	pouze	k6eAd1
na	na	k7c6
spaní	spaní	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostředí	prostředí	k1gNnSc1
by	by	kYmCp3nP
nemělo	mít	k5eNaImAgNnS
být	být	k5eAaImF
příliš	příliš	k6eAd1
hlučné	hlučný	k2eAgNnSc4d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
tento	tento	k3xDgInSc1
faktor	faktor	k1gInSc1
lze	lze	k6eAd1
málokdy	málokdy	k6eAd1
ovlivnit	ovlivnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučená	doporučený	k2eAgFnSc1d1
pokojová	pokojový	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
pro	pro	k7c4
zdravý	zdravý	k2eAgInSc4d1
spánek	spánek	k1gInSc4
je	být	k5eAaImIp3nS
16	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Příliš	příliš	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
či	či	k8xC
příliš	příliš	k6eAd1
nízká	nízký	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
může	moct	k5eAaImIp3nS
spánek	spánek	k1gInSc4
narušovat	narušovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ložnici	ložnice	k1gFnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
udržovat	udržovat	k5eAaImF
tma	tma	k1gFnSc1
–	–	k?
světlo	světlo	k1gNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
tělo	tělo	k1gNnSc4
signálem	signál	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zůstalo	zůstat	k5eAaPmAgNnS
vzhůru	vzhůru	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Načasování	načasování	k1gNnSc1
spánku	spánek	k1gInSc2
<g/>
:	:	kIx,
Dobré	dobrý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
chodit	chodit	k5eAaImF
spát	spát	k5eAaImF
každou	každý	k3xTgFnSc4
noc	noc	k1gFnSc4
a	a	k8xC
vstávat	vstávat	k5eAaImF
každé	každý	k3xTgNnSc4
ráno	ráno	k1gNnSc4
ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zdřímnutí	zdřímnutí	k1gNnSc1
<g/>
:	:	kIx,
Krátká	krátký	k2eAgNnPc1d1
zdřímnutí	zdřímnutí	k1gNnPc1
během	během	k7c2
dne	den	k1gInSc2
jsou	být	k5eAaImIp3nP
normální	normální	k2eAgFnSc7d1
zdravou	zdravý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
denního	denní	k2eAgInSc2d1
režimu	režim	k1gInSc2
mnoha	mnoho	k4c2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
<g/>
,	,	kIx,
u	u	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
potíže	potíž	k1gFnPc4
se	s	k7c7
spánkem	spánek	k1gInSc7
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
tato	tento	k3xDgNnPc1
krátká	krátký	k2eAgNnPc1d1
zdřímnutí	zdřímnutí	k1gNnPc1
během	během	k7c2
dne	den	k1gInSc2
nahrazovat	nahrazovat	k5eAaImF
noční	noční	k2eAgInSc4d1
spánek	spánek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denní	denní	k2eAgInSc1d1
spánek	spánek	k1gInSc1
tak	tak	k6eAd1
muže	muž	k1gMnPc4
nespavost	nespavost	k1gFnSc1
zhoršit	zhoršit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Cvičení	cvičení	k1gNnSc1
<g/>
:	:	kIx,
Ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
pravidelné	pravidelný	k2eAgNnSc4d1
ranní	ranní	k2eAgNnSc4d1
cvičení	cvičení	k1gNnSc4
zlepšuje	zlepšovat	k5eAaImIp3nS
spánek	spánek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
cvičení	cvičení	k1gNnPc1
před	před	k7c7
spaním	spaní	k1gNnSc7
může	moct	k5eAaImIp3nS
znesnadnit	znesnadnit	k5eAaPmF
usínání	usínání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Nikotin	nikotin	k1gInSc1
<g/>
:	:	kIx,
Cigarety	cigareta	k1gFnPc1
a	a	k8xC
ostatní	ostatní	k2eAgInPc1d1
nikotinové	nikotinový	k2eAgInPc1d1
produkty	produkt	k1gInPc1
působí	působit	k5eAaImIp3nP
povzbudivě	povzbudivě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
před	před	k7c7
spaním	spaní	k1gNnSc7
a	a	k8xC
při	při	k7c6
probuzení	probuzení	k1gNnSc6
v	v	k7c6
průběhu	průběh	k1gInSc6
noci	noc	k1gFnSc2
kouření	kouření	k1gNnSc2
vyvarovat	vyvarovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Kofein	kofein	k1gInSc1
<g/>
:	:	kIx,
Káva	káva	k1gFnSc1
<g/>
,	,	kIx,
čaj	čaj	k1gInSc1
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
nápojů	nápoj	k1gInPc2
a	a	k8xC
potravin	potravina	k1gFnPc2
obsahující	obsahující	k2eAgInSc1d1
kofein	kofein	k1gInSc1
působí	působit	k5eAaImIp3nS
povzbudivě	povzbudivě	k6eAd1
a	a	k8xC
může	moct	k5eAaImIp3nS
udržovat	udržovat	k5eAaImF
déle	dlouho	k6eAd2
v	v	k7c6
bdělém	bdělý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
trpící	trpící	k2eAgMnPc1d1
nespavostí	nespavost	k1gFnPc2
by	by	kYmCp3nP
neměli	mít	k5eNaImAgMnP
konzumovat	konzumovat	k5eAaBmF
kofein	kofein	k1gInSc4
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
hodin	hodina	k1gFnPc2
před	před	k7c7
ulehnutím	ulehnutí	k1gNnSc7
do	do	k7c2
postele	postel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alkohol	alkohol	k1gInSc1
<g/>
:	:	kIx,
Alkohol	alkohol	k1gInSc1
urychluje	urychlovat	k5eAaImIp3nS
nástup	nástup	k1gInSc4
spánku	spánek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
lidí	člověk	k1gMnPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
alkohol	alkohol	k1gInSc4
jako	jako	k8xS,k8xC
první	první	k4xOgInSc4
prostředek	prostředek	k1gInSc4
k	k	k7c3
řešení	řešení	k1gNnSc3
nespavosti	nespavost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhodobé	dlouhodobý	k2eAgInPc1d1
užívání	užívání	k1gNnSc4
alkoholu	alkohol	k1gInSc2
však	však	k9
narušuje	narušovat	k5eAaImIp3nS
spánkový	spánkový	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
konzumace	konzumace	k1gFnSc1
alkoholu	alkohol	k1gInSc2
problém	problém	k1gInSc4
se	s	k7c7
spánkem	spánek	k1gInSc7
neřeší	řešit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Těžká	těžký	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
<g/>
:	:	kIx,
Těžká	těžký	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
pozdě	pozdě	k6eAd1
večer	večer	k6eAd1
mohou	moct	k5eAaImIp3nP
spánek	spánek	k1gInSc4
narušit	narušit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
lehká	lehký	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
anebo	anebo	k8xC
jiný	jiný	k2eAgInSc1d1
rituál	rituál	k1gInSc1
před	před	k7c7
usínáním	usínání	k1gNnSc7
může	moct	k5eAaImIp3nS
pomoci	pomoct	k5eAaPmF
spánek	spánek	k1gInSc1
navodit	navodit	k5eAaPmF,k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1
<g/>
:	:	kIx,
Při	při	k7c6
usínání	usínání	k1gNnSc6
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
potlačit	potlačit	k5eAaPmF
negativní	negativní	k2eAgFnPc4d1
myšlenky	myšlenka	k1gFnPc4
rušící	rušící	k2eAgInSc1d1
spánek	spánek	k1gInSc1
a	a	k8xC
usínání	usínání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Léky	lék	k1gInPc1
na	na	k7c4
spaní	spaní	k1gNnSc4
</s>
<s>
Léky	lék	k1gInPc1
jsou	být	k5eAaImIp3nP
předepisovány	předepisovat	k5eAaImNgInP
lidem	člověk	k1gMnPc3
se	s	k7c7
stálou	stálý	k2eAgFnSc7d1
nespavostí	nespavost	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
jim	on	k3xPp3gMnPc3
ztěžuje	ztěžovat	k5eAaImIp3nS
každodenní	každodenní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
léky	lék	k1gInPc4
způsobují	způsobovat	k5eAaImIp3nP
útlum	útlum	k1gInSc4
té	ten	k3xDgFnSc2
části	část	k1gFnSc2
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nás	my	k3xPp1nPc4
drží	držet	k5eAaImIp3nS
v	v	k7c6
bdělosti	bdělost	k1gFnSc6
a	a	k8xC
navozuje	navozovat	k5eAaImIp3nS
spánek	spánek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
použití	použití	k1gNnSc6
převládá	převládat	k5eAaImIp3nS
lehký	lehký	k2eAgInSc1d1
spánek	spánek	k1gInSc1
(	(	kIx(
<g/>
NREM	NREM	kA
stadia	stadion	k1gNnPc4
1	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
méně	málo	k6eAd2
pak	pak	k6eAd1
hluboký	hluboký	k2eAgInSc1d1
spánek	spánek	k1gInSc1
(	(	kIx(
<g/>
NREM	NREM	kA
stadia	stadion	k1gNnPc4
3	#num#	k4
a	a	k8xC
4	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ještě	ještě	k9
méně	málo	k6eAd2
spánek	spánek	k1gInSc1
se	s	k7c7
sny	sen	k1gInPc7
(	(	kIx(
<g/>
REM	REM	kA
spánek	spánek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
těchto	tento	k3xDgInPc2
léků	lék	k1gInPc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
vhodné	vhodný	k2eAgNnSc1d1
jen	jen	k9
pro	pro	k7c4
obnovení	obnovení	k1gNnSc4
spánkového	spánkový	k2eAgInSc2d1
rytmu	rytmus	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
všechny	všechen	k3xTgInPc1
předchozí	předchozí	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
(	(	kIx(
<g/>
metody	metoda	k1gFnPc1
<g/>
)	)	kIx)
selhaly	selhat	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podávání	podávání	k1gNnPc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
velmi	velmi	k6eAd1
opatrné	opatrný	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
dlouhodobé	dlouhodobý	k2eAgNnSc1d1
užívání	užívání	k1gNnSc1
může	moct	k5eAaImIp3nS
vyvolat	vyvolat	k5eAaPmF
lékovou	lékový	k2eAgFnSc4d1
závislost	závislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
některých	některý	k3yIgInPc2
léků	lék	k1gInPc2
na	na	k7c6
spaní	spaní	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
Chloralhydrát	Chloralhydrát	k1gInSc1
</s>
<s>
Nitrazepam	Nitrazepam	k6eAd1
</s>
<s>
Chlormethiazol	Chlormethiazol	k1gInSc1
</s>
<s>
Promethazin	Promethazin	k1gMnSc1
</s>
<s>
Dichloralphenazon	Dichloralphenazon	k1gMnSc1
</s>
<s>
Temazepam	Temazepam	k6eAd1
</s>
<s>
Flurazepam	Flurazepam	k6eAd1
</s>
<s>
Triazolam	Triazolam	k6eAd1
</s>
<s>
Stilnox	Stilnox	k1gInSc1
</s>
<s>
Dormicum	Dormicum	k1gNnSc1
(	(	kIx(
<g/>
Midazolamum	Midazolamum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VONDRÁČEK	Vondráček	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fantastické	fantastický	k2eAgInPc1d1
a	a	k8xC
magické	magický	k2eAgInPc1d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
psychiatrie	psychiatrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Columbus	Columbus	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
-	-	kIx~
<g/>
182	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HUGHES	HUGHES	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
obrazová	obrazový	k2eAgFnSc1d1
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Svojtka	Svojtka	k1gFnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7237	#num#	k4
<g/>
-	-	kIx~
<g/>
256	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Potraviny	potravina	k1gFnSc2
a	a	k8xC
výživa	výživa	k1gFnSc1
-	-	kIx~
spánek	spánek	k1gInSc1
a	a	k8xC
sny	sen	k1gInPc1
<g/>
,	,	kIx,
s.	s.	k?
168	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KASSIN	KASSIN	kA
<g/>
,	,	kIx,
Saul	Saul	k1gMnSc1
M.	M.	kA
Psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1716	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
143	#num#	k4
-	-	kIx~
144	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
insomnie	insomnie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Nespavost	nespavost	k1gFnSc4
na	na	k7c4
Jsme	být	k5eAaImIp1nP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1
neručí	ručit	k5eNaImIp3nS
za	za	k7c4
správnost	správnost	k1gFnSc4
lékařských	lékařský	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
vyhledejte	vyhledat	k5eAaPmRp2nP
lékaře	lékař	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Přečtěte	přečíst	k5eAaPmRp2nP
si	se	k3xPyFc3
prosím	prosit	k5eAaImIp1nS
pokyny	pokyn	k1gInPc4
pro	pro	k7c4
využití	využití	k1gNnSc4
článků	článek	k1gInPc2
o	o	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4052584-3	4052584-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85066717	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85066717	#num#	k4
</s>
