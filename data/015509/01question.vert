<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
volá	volat	k5eAaImIp3nS
porucha	porucha	k1gFnSc1
spánku	spánek	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
jedinec	jedinec	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
usnout	usnout	k5eAaPmF
nebo	nebo	k8xC
se	se	k3xPyFc4
v	v	k7c6
spánku	spánek	k1gInSc6
často	často	k6eAd1
probouzí	probouzet	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>