<s>
Čtvrtek	čtvrtek	k1gInSc1	čtvrtek
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
den	den	k1gInSc4	den
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
židovském	židovský	k2eAgMnSc6d1	židovský
a	a	k8xC	a
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
dnem	den	k1gInSc7	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
neděle	neděle	k1gFnSc2	neděle
<g/>
,	,	kIx,	,
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgNnSc7	pátý
dnem	dno	k1gNnSc7	dno
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
byzantském	byzantský	k2eAgInSc6d1	byzantský
ritu	rit	k1gInSc6	rit
je	být	k5eAaImIp3nS	být
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
dnem	den	k1gInSc7	den
připomínky	připomínka	k1gFnSc2	připomínka
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInPc1	mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
"	"	kIx"	"
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
týden	týden	k1gInSc1	týden
začíná	začínat	k5eAaImIp3nS	začínat
pondělkem	pondělek	k1gInSc7	pondělek
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
týden	týden	k1gInSc4	týden
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
od	od	k7c2	od
neděle	neděle	k1gFnSc2	neděle
<g/>
;	;	kIx,	;
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
čtvrtek	čtvrtek	k1gInSc1	čtvrtek
pátým	pátý	k4xOgInSc7	pátý
dnem	den	k1gInSc7	den
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vysvítá	vysvítat	k5eAaImIp3nS	vysvítat
ze	z	k7c2	z
středověkého	středověký	k2eAgInSc2d1	středověký
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
feria	ferius	k1gMnSc2	ferius
quinta	quint	k1gMnSc2	quint
či	či	k8xC	či
z	z	k7c2	z
portugalského	portugalský	k2eAgInSc2d1	portugalský
quinta-feira	quintaeir	k1gInSc2	quinta-feir
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ISO	ISO	kA	ISO
8601	[number]	k4	8601
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čtvrtek	čtvrtek	k1gInSc1	čtvrtek
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
kromě	kromě	k7c2	kromě
češtiny	čeština	k1gFnSc2	čeština
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
czwartek	czwartek	k1gInSc1	czwartek
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
ч	ч	k?	ч
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
latině	latina	k1gFnSc6	latina
se	se	k3xPyFc4	se
čtvrtek	čtvrtek	k1gInSc1	čtvrtek
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
Dies	Dies	k1gInSc1	Dies
Jovis	Jovis	k1gInSc1	Jovis
(	(	kIx(	(
<g/>
Jovův	Jovův	k2eAgInSc1d1	Jovův
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
názvy	název	k1gInPc7	název
čtvrtku	čtvrtka	k1gFnSc4	čtvrtka
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
it.	it.	k?	it.
Giovedì	Giovedì	k1gMnSc1	Giovedì
<g/>
,	,	kIx,	,
špan.	špan.	k?	špan.
jueves	jueves	k1gInSc1	jueves
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
jeudi	jeudit	k5eAaPmRp2nS	jeudit
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
Thursday	Thursdaa	k1gFnPc1	Thursdaa
či	či	k8xC	či
německé	německý	k2eAgInPc1d1	německý
Donnerstag	Donnerstag	k1gInSc1	Donnerstag
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
germánského	germánský	k2eAgMnSc2d1	germánský
boha	bůh	k1gMnSc2	bůh
Thóra	Thór	k1gMnSc2	Thór
<g/>
;	;	kIx,	;
znamená	znamenat	k5eAaImIp3nS	znamenat
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Thorův	Thorův	k2eAgInSc4d1	Thorův
den	den	k1gInSc4	den
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bavorském	bavorský	k2eAgNnSc6d1	bavorské
nářečí	nářečí	k1gNnSc6	nářečí
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
také	také	k9	také
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Pfinztag	Pfinztag	k1gInSc1	Pfinztag
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
"	"	kIx"	"
<g/>
pempte	pempat	k5eAaPmRp2nP	pempat
hemera	hemero	k1gNnPc4	hemero
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pátý	pátý	k4xOgInSc1	pátý
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
měl	mít	k5eAaImAgInS	mít
eliminovat	eliminovat	k5eAaBmF	eliminovat
název	název	k1gInSc4	název
vycházející	vycházející	k2eAgInPc4d1	vycházející
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
pohanského	pohanský	k2eAgMnSc2d1	pohanský
boha	bůh	k1gMnSc2	bůh
Thóra	Thór	k1gMnSc2	Thór
<g/>
.	.	kIx.	.
</s>
<s>
Hindský	hindský	k2eAgInSc1d1	hindský
název	název	k1gInSc1	název
pro	pro	k7c4	pro
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
je	být	k5eAaImIp3nS	být
Guruvar	Guruvar	k1gInSc1	Guruvar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Guru	guru	k1gMnSc1	guru
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
název	název	k1gInSc1	název
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
spojenou	spojený	k2eAgFnSc4d1	spojená
se	s	k7c7	s
čtvrtkem	čtvrtek	k1gInSc7	čtvrtek
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
Čtvrtek	čtvrtka	k1gFnPc2	čtvrtka
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
protagonisty	protagonista	k1gMnSc2	protagonista
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
australský	australský	k2eAgMnSc1d1	australský
sci-fi	scii	k1gFnPc4	sci-fi
autor	autor	k1gMnSc1	autor
Garth	Garth	k1gMnSc1	Garth
Nix	Nix	k1gMnSc1	Nix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hinduistickém	hinduistický	k2eAgNnSc6d1	hinduistické
náboženství	náboženství	k1gNnSc6	náboženství
je	být	k5eAaImIp3nS	být
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
guruvar	guruvar	k1gInSc1	guruvar
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
postním	postní	k2eAgInSc7d1	postní
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
severní	severní	k2eAgFnSc2d1	severní
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Kvakeři	kvaker	k1gMnPc1	kvaker
tradičně	tradičně	k6eAd1	tradičně
označují	označovat	k5eAaImIp3nP	označovat
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
pátý	pátý	k4xOgInSc1	pátý
den	den	k1gInSc1	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyvarovali	vyvarovat	k5eAaPmAgMnP	vyvarovat
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
Thursday	Thursdaa	k1gFnSc2	Thursdaa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vycházejícím	vycházející	k2eAgMnSc7d1	vycházející
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
pohanského	pohanský	k2eAgMnSc2d1	pohanský
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
jako	jako	k8xC	jako
pátého	pátý	k4xOgInSc2	pátý
dne	den	k1gInSc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
v	v	k7c6	v
islandštině	islandština	k1gFnSc6	islandština
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc6d1	moderní
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
,	,	kIx,	,
portugalštině	portugalština	k1gFnSc6	portugalština
a	a	k8xC	a
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
semitských	semitský	k2eAgInPc6d1	semitský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
tradici	tradice	k1gFnSc6	tradice
zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
je	být	k5eAaImIp3nS	být
čtvrtkem	čtvrtek	k1gInSc7	čtvrtek
před	před	k7c7	před
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
-	-	kIx~	-
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtek	čtvrtek	k1gInSc1	čtvrtek
Nanebevstoupení	nanebevstoupení	k1gNnSc2	nanebevstoupení
Páně	páně	k2eAgFnSc2d1	páně
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
po	po	k7c6	po
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
Den	den	k1gInSc1	den
díkůvzdání	díkůvzdání	k1gNnSc2	díkůvzdání
každoročním	každoroční	k2eAgInSc7d1	každoroční
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
spadá	spadat	k5eAaPmIp3nS	spadat
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
v	v	k7c6	v
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
určuje	určovat	k5eAaImIp3nS	určovat
ho	on	k3xPp3gNnSc4	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
řeči	řeč	k1gFnSc6	řeč
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
čtvrtek	čtvrtek	k1gInSc1	čtvrtek
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc4d1	malý
pátek	pátek	k1gInSc4	pátek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
zřejmě	zřejmě	k6eAd1	zřejmě
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
blížícího	blížící	k2eAgMnSc2d1	blížící
se	se	k3xPyFc4	se
konce	konec	k1gInSc2	konec
pracovního	pracovní	k2eAgInSc2d1	pracovní
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Astrologické	astrologický	k2eAgNnSc1d1	astrologické
znamení	znamení	k1gNnSc1	znamení
pro	pro	k7c4	pro
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
pro	pro	k7c4	pro
planetu	planeta	k1gFnSc4	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
i	i	k9	i
z	z	k7c2	z
názvu	název	k1gInSc2	název
čtvrtku	čtvrtek	k1gInSc2	čtvrtek
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
čtvrtek	čtvrtek	k1gInSc1	čtvrtek
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
datum	datum	k1gNnSc4	datum
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prudce	prudko	k6eAd1	prudko
klesly	klesnout	k5eAaPmAgFnP	klesnout
ceny	cena	k1gFnPc1	cena
rekordního	rekordní	k2eAgInSc2d1	rekordní
objemu	objem	k1gInSc2	objem
témě	témě	k1gNnSc4	témě
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
akcií	akcie	k1gFnPc2	akcie
na	na	k7c6	na
Newyorské	newyorský	k2eAgFnSc6d1	newyorská
burze	burza	k1gFnSc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
klesly	klesnout	k5eAaPmAgFnP	klesnout
ceny	cena	k1gFnPc1	cena
objemu	objem	k1gInSc2	objem
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nebylo	být	k5eNaImAgNnS	být
překonáno	překonat	k5eAaPmNgNnS	překonat
plných	plný	k2eAgNnPc2d1	plné
39	[number]	k4	39
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obrazných	obrazný	k2eAgFnPc6d1	obrazná
metaforách	metafora	k1gFnPc6	metafora
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
krach	krach	k1gInSc4	krach
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
počátek	počátek	k1gInSc4	počátek
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Špinavý	špinavý	k2eAgInSc4d1	špinavý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
tlustý	tlustý	k2eAgInSc4d1	tlustý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
masopustu	masopust	k1gInSc2	masopust
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Páně	páně	k2eAgInSc4d1	páně
Dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
prodloužení	prodloužení	k1gNnSc6	prodloužení
otvírací	otvírací	k2eAgFnSc2d1	otvírací
doby	doba	k1gFnSc2	doba
obchodů	obchod	k1gInPc2	obchod
každý	každý	k3xTgInSc4	každý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
až	až	k6eAd1	až
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgNnSc1d1	boží
Tělo	tělo	k1gNnSc1	tělo
(	(	kIx(	(
<g/>
Corpus	corpus	k1gNnSc1	corpus
Cristi	Crist	k1gFnSc2	Crist
<g/>
)	)	kIx)	)
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čtvrtek	čtvrtka	k1gFnPc2	čtvrtka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
