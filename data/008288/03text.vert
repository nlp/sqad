<p>
<s>
Romština	romština	k1gFnSc1	romština
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
cikánština	cikánština	k1gFnSc1	cikánština
<g/>
,	,	kIx,	,
romsky	romsky	k6eAd1	romsky
romaňi	romaňi	k6eAd1	romaňi
čhib	čhiba	k1gFnPc2	čhiba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
indoárijský	indoárijský	k2eAgInSc1d1	indoárijský
jazyk	jazyk	k1gInSc1	jazyk
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
řazena	řazen	k2eAgFnSc1d1	řazena
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Ethnologue	Ethnologu	k1gInPc4	Ethnologu
<g/>
)	)	kIx)	)
do	do	k7c2	do
centrální	centrální	k2eAgFnSc2d1	centrální
podskupiny	podskupina	k1gFnSc2	podskupina
indoárijských	indoárijský	k2eAgInPc2d1	indoárijský
jazyků	jazyk	k1gInPc2	jazyk
spolu	spolu	k6eAd1	spolu
např.	např.	kA	např.
s	s	k7c7	s
hindštinou	hindština	k1gFnSc7	hindština
<g/>
,	,	kIx,	,
paňdžábštinou	paňdžábština	k1gFnSc7	paňdžábština
<g/>
,	,	kIx,	,
rádžasthánštinou	rádžasthánština	k1gFnSc7	rádžasthánština
nebo	nebo	k8xC	nebo
gudžarátštinou	gudžarátština	k1gFnSc7	gudžarátština
<g/>
;	;	kIx,	;
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
jazyky	jazyk	k1gInPc7	jazyk
ovšem	ovšem	k9	ovšem
ztratila	ztratit	k5eAaPmAgFnS	ztratit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
migrace	migrace	k1gFnSc1	migrace
Romů	Rom	k1gMnPc2	Rom
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
a	a	k8xC	a
vývojově	vývojově	k6eAd1	vývojově
tedy	tedy	k9	tedy
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
přechodu	přechod	k1gInSc2	přechod
k	k	k7c3	k
novoindickým	novoindický	k2eAgInPc3d1	novoindický
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Romština	romština	k1gFnSc1	romština
je	být	k5eAaImIp3nS	být
flektivní	flektivní	k2eAgInSc4d1	flektivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
8	[number]	k4	8
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
vokativ	vokativ	k1gInSc1	vokativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
lokativ	lokativ	k1gInSc1	lokativ
<g/>
,	,	kIx,	,
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc1	ablativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
rody	rod	k1gInPc4	rod
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
slovesné	slovesný	k2eAgFnPc1d1	slovesná
třídy	třída	k1gFnPc1	třída
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
koncovky	koncovka	k1gFnPc4	koncovka
v	v	k7c6	v
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
má	mít	k5eAaImIp3nS	mít
tvary	tvar	k1gInPc4	tvar
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
infinitiv	infinitiv	k1gInSc4	infinitiv
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
jedn	jedn	k1gNnSc1	jedn
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
množ	množit	k5eAaImRp2nS	množit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovesné	slovesný	k2eAgInPc1d1	slovesný
časy	čas	k1gInPc1	čas
<g/>
,	,	kIx,	,
způsoby	způsob	k1gInPc1	způsob
<g/>
,	,	kIx,	,
rody	rod	k1gInPc1	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovesné	slovesný	k2eAgInPc4d1	slovesný
časy	čas	k1gInPc4	čas
<g/>
:	:	kIx,	:
přítomný	přítomný	k2eAgInSc4d1	přítomný
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgInSc4d1	budoucí
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
nedokonavý	dokonavý	k2eNgInSc4d1	nedokonavý
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
dokonavý	dokonavý	k2eAgInSc4d1	dokonavý
</s>
</p>
<p>
<s>
Způsoby	způsob	k1gInPc1	způsob
<g/>
:	:	kIx,	:
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
<g/>
,	,	kIx,	,
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
<g/>
,	,	kIx,	,
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
přítomný	přítomný	k2eAgInSc1d1	přítomný
<g/>
,	,	kIx,	,
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
minulý	minulý	k2eAgInSc4d1	minulý
</s>
</p>
<p>
<s>
Rody	rod	k1gInPc1	rod
<g/>
:	:	kIx,	:
činný	činný	k2eAgMnSc1d1	činný
<g/>
,	,	kIx,	,
trpný	trpný	k2eAgMnSc1d1	trpný
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
<g/>
:	:	kIx,	:
opětovnost	opětovnost	k1gFnSc1	opětovnost
<g/>
,	,	kIx,	,
slovesa	sloveso	k1gNnPc1	sloveso
mají	mít	k5eAaImIp3nP	mít
kauzativní	kauzativní	k2eAgNnPc1d1	kauzativní
a	a	k8xC	a
pasivní	pasivní	k2eAgNnPc1d1	pasivní
tvaryMnoho	tvaryMnoze	k6eAd1	tvaryMnoze
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
přejatých	přejatý	k2eAgMnPc2d1	přejatý
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
územími	území	k1gNnPc7	území
Romové	Rom	k1gMnPc1	Rom
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
putovali	putovat	k5eAaImAgMnP	putovat
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc1	slovo
indického	indický	k2eAgInSc2d1	indický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
výpůjčky	výpůjčka	k1gFnPc4	výpůjčka
patří	patřit	k5eAaImIp3nS	patřit
slova	slovo	k1gNnPc4	slovo
z	z	k7c2	z
perštiny	perština	k1gFnSc2	perština
a	a	k8xC	a
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
dialekty	dialekt	k1gInPc1	dialekt
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
množstvím	množství	k1gNnSc7	množství
těchto	tento	k3xDgNnPc2	tento
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
celkovou	celkový	k2eAgFnSc7d1	celková
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
<g/>
,	,	kIx,	,
fonetikou	fonetika	k1gFnSc7	fonetika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dialekty	dialekt	k1gInPc4	dialekt
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
jazyků	jazyk	k1gInPc2	jazyk
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
následující	následující	k2eAgInPc4d1	následující
hlavní	hlavní	k2eAgInPc4d1	hlavní
dialekty	dialekt	k1gInPc4	dialekt
romštiny	romština	k1gFnSc2	romština
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
balkánský	balkánský	k2eAgMnSc1d1	balkánský
</s>
</p>
<p>
<s>
baltský	baltský	k2eAgInSc1d1	baltský
</s>
</p>
<p>
<s>
finský	finský	k2eAgMnSc1d1	finský
</s>
</p>
<p>
<s>
karpatský	karpatský	k2eAgInSc4d1	karpatský
</s>
</p>
<p>
<s>
ruský	ruský	k2eAgMnSc1d1	ruský
</s>
</p>
<p>
<s>
sinti	sinti	k1gNnSc1	sinti
</s>
</p>
<p>
<s>
velšský	velšský	k2eAgInSc1d1	velšský
</s>
</p>
<p>
<s>
českýLze	českýLze	k6eAd1	českýLze
však	však	k9	však
použít	použít	k5eAaPmF	použít
i	i	k9	i
přesnější	přesný	k2eAgNnSc4d2	přesnější
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
dialekty	dialekt	k1gInPc4	dialekt
např.	např.	kA	např.
slovenský	slovenský	k2eAgMnSc1d1	slovenský
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
atd.	atd.	kA	atd.
Jak	jak	k8xC	jak
hlavní	hlavní	k2eAgInPc1d1	hlavní
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
regionální	regionální	k2eAgFnSc2d1	regionální
varianty	varianta	k1gFnSc2	varianta
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
fonetické	fonetický	k2eAgFnPc1d1	fonetická
<g/>
,	,	kIx,	,
lexikální	lexikální	k2eAgFnPc1d1	lexikální
i	i	k8xC	i
gramatické	gramatický	k2eAgFnPc1d1	gramatická
odlišnosti	odlišnost	k1gFnPc1	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
spisovná	spisovný	k2eAgFnSc1d1	spisovná
romština	romština	k1gFnSc1	romština
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Pravopis	pravopis	k1gInSc1	pravopis
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
nebo	nebo	k8xC	nebo
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
majoritními	majoritní	k2eAgInPc7d1	majoritní
jazyky	jazyk	k1gInPc7	jazyk
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
slovenský	slovenský	k2eAgInSc1d1	slovenský
pravopis	pravopis	k1gInSc1	pravopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k6eAd1	již
našel	najít	k5eAaPmAgInS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
jak	jak	k8xC	jak
při	při	k7c6	při
původní	původní	k2eAgFnSc6d1	původní
literární	literární	k2eAgFnSc6d1	literární
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
sestavování	sestavování	k1gNnSc6	sestavování
slovníků	slovník	k1gInPc2	slovník
a	a	k8xC	a
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
romština	romština	k1gFnSc1	romština
je	být	k5eAaImIp3nS	být
indický	indický	k2eAgInSc4d1	indický
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
István	István	k2eAgMnSc1d1	István
Vályi	Vály	k1gFnPc4	Vály
(	(	kIx(	(
<g/>
†	†	k?	†
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
romština	romština	k1gFnSc1	romština
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
již	již	k9	již
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Puchmajerem	Puchmajer	k1gInSc7	Puchmajer
<g/>
,	,	kIx,	,
autorkou	autorka	k1gFnSc7	autorka
moderních	moderní	k2eAgFnPc2d1	moderní
publikací	publikace	k1gFnPc2	publikace
o	o	k7c6	o
romštině	romština	k1gFnSc6	romština
je	být	k5eAaImIp3nS	být
Milena	Milena	k1gFnSc1	Milena
Hübschmannová	Hübschmannová	k1gFnSc1	Hübschmannová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
lze	lze	k6eAd1	lze
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
romštinu	romština	k1gFnSc4	romština
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
Pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
fakultě	fakulta	k1gFnSc3	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
Pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
fakultě	fakulta	k1gFnSc3	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
ČR	ČR	kA	ČR
o	o	k7c6	o
zavedení	zavedení	k1gNnSc6	zavedení
romštiny	romština	k1gFnSc2	romština
i	i	k9	i
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
jako	jako	k8xS	jako
volitelný	volitelný	k2eAgInSc4d1	volitelný
předmět	předmět	k1gInSc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgNnP	mít
učit	učit	k5eAaImF	učit
i	i	k9	i
romská	romský	k2eAgFnSc1d1	romská
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
lze	lze	k6eAd1	lze
jen	jen	k6eAd1	jen
odhadovat	odhadovat	k5eAaImF	odhadovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
obtížně	obtížně	k6eAd1	obtížně
slučitelných	slučitelný	k2eAgNnPc2d1	slučitelné
statistických	statistický	k2eAgNnPc2d1	statistické
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
4,6	[number]	k4	4,6
miliony	milion	k4xCgInPc7	milion
a	a	k8xC	a
12	[number]	k4	12
miliony	milion	k4xCgInPc7	milion
s	s	k7c7	s
realistickým	realistický	k2eAgInSc7d1	realistický
středem	střed	k1gInSc7	střed
okolo	okolo	k7c2	okolo
6,6	[number]	k4	6,6
milionu	milion	k4xCgInSc2	milion
mluvčích	mluvčí	k1gFnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
romštiny	romština	k1gFnSc2	romština
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
abeceda	abeceda	k1gFnSc1	abeceda
karpatské	karpatský	k2eAgFnSc2d1	Karpatská
(	(	kIx(	(
<g/>
slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
)	)	kIx)	)
romštiny	romština	k1gFnSc2	romština
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k9	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
,	,	kIx,	,
čh	čh	k?	čh
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
ď	ď	k?	ď
<g/>
,	,	kIx,	,
dz	dz	k?	dz
<g/>
,	,	kIx,	,
dž	dž	k?	dž
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
h	h	k?	h
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
kh	kh	k0	kh
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
ľ	ľ	k?	ľ
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
ň	ň	k?	ň
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
p	p	k?	p
<g/>
,	,	kIx,	,
ph	ph	kA	ph
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
ť	ť	k?	ť
<g/>
,	,	kIx,	,
th	th	k?	th
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
ž	ž	k?	ž
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Devla	Devlo	k1gNnSc2	Devlo
amaro	amaro	k1gNnSc1	amaro
(	(	kIx(	(
<g/>
Otče	otec	k1gMnSc5	otec
náš	náš	k3xOp1gMnSc1	náš
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Přísloví	přísloví	k1gNnSc2	přísloví
===	===	k?	===
</s>
</p>
<p>
<s>
Ko	Ko	k?	Ko
kamel	kamel	k1gMnSc1	kamel
ča	ča	k?	ča
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
na	na	k7c4	na
džanel	džanel	k1gInSc4	džanel
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
oda	oda	k?	oda
bacht	bacht	k1gMnSc1	bacht
<g/>
.	.	kIx.	.
<g/>
Kdo	kdo	k3yRnSc1	kdo
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
jenom	jenom	k6eAd1	jenom
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
.	.	kIx.	.
<g/>
Dilino	Dilino	k1gNnSc1	Dilino
phenel	phenela	k1gFnPc2	phenela
<g/>
,	,	kIx,	,
so	so	k?	so
džanel	džanel	k1gMnSc1	džanel
<g/>
,	,	kIx,	,
goďaver	goďaver	k1gMnSc1	goďaver
džanel	džanel	k1gMnSc1	džanel
<g/>
,	,	kIx,	,
so	so	k?	so
phenel	phenel	k1gMnSc1	phenel
<g/>
.	.	kIx.	.
<g/>
Hlupák	hlupák	k1gMnSc1	hlupák
<g />
.	.	kIx.	.
</s>
<s>
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
moudrý	moudrý	k2eAgMnSc1d1	moudrý
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
říká	říkat	k5eAaImIp3nS	říkat
<g/>
.	.	kIx.	.
<g/>
Feder	Feder	k1gMnSc1	Feder
goďavereha	goďavereha	k1gMnSc1	goďavereha
bara	bara	k1gMnSc1	bara
te	te	k?	te
phagerel	phagerel	k1gMnSc1	phagerel
<g/>
,	,	kIx,	,
sar	sar	k?	sar
dilineha	dilineha	k1gMnSc1	dilineha
bokheľa	bokheľa	k1gMnSc1	bokheľa
te	te	k?	te
chal	chal	k1gMnSc1	chal
<g/>
.	.	kIx.	.
<g/>
Lépe	dobře	k6eAd2	dobře
s	s	k7c7	s
moudrým	moudrý	k2eAgInSc7d1	moudrý
roztloukat	roztloukat	k5eAaImF	roztloukat
kamení	kamení	k1gNnSc1	kamení
<g/>
,	,	kIx,	,
než	než	k8xS	než
s	s	k7c7	s
hlupákem	hlupák	k1gMnSc7	hlupák
jíst	jíst	k5eAaImF	jíst
buchty	buchta	k1gFnPc4	buchta
<g/>
.	.	kIx.	.
<g/>
Nane	Nane	k1gNnSc4	Nane
čhave	čhaev	k1gFnSc2	čhaev
<g/>
,	,	kIx,	,
nane	nane	k6eAd1	nane
bacht	bacht	k2eAgMnSc1d1	bacht
<g/>
.	.	kIx.	.
<g/>
Nejsou	být	k5eNaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Romové	Rom	k1gMnPc1	Rom
</s>
</p>
<p>
<s>
Romská	romský	k2eAgFnSc1d1	romská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
romština	romština	k1gFnSc1	romština
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Šebková	Šebková	k1gFnSc1	Šebková
<g/>
:	:	kIx,	:
Nástin	nástin	k1gInSc1	nástin
mluvnice	mluvnice	k1gFnSc2	mluvnice
slovenské	slovenský	k2eAgFnSc2d1	slovenská
romštiny	romština	k1gFnSc2	romština
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
pedagogické	pedagogický	k2eAgInPc4d1	pedagogický
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Research	Research	k1gInSc1	Research
Support	support	k1gInSc1	support
Scheme	Schem	k1gInSc5	Schem
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
romština	romština	k1gFnSc1	romština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnPc1	téma
Romská	romský	k2eAgNnPc1d1	romské
přísloví	přísloví	k1gNnPc1	přísloví
ve	v	k7c6	v
WikicitátechRomská	WikicitátechRomský	k2eAgNnPc4d1	WikicitátechRomský
Wikipedie	Wikipedie	k1gFnPc1	Wikipedie
</s>
</p>
<p>
<s>
Kódy	kód	k1gInPc4	kód
dialektů	dialekt	k1gInPc2	dialekt
romštiny	romština	k1gFnSc2	romština
</s>
</p>
<p>
<s>
ROMLEX	ROMLEX	kA	ROMLEX
Lexical	Lexical	k1gFnSc1	Lexical
Database	Databasa	k1gFnSc3	Databasa
-	-	kIx~	-
databáze	databáze	k1gFnSc1	databáze
(	(	kIx(	(
<g/>
slovník	slovník	k1gInSc1	slovník
<g/>
)	)	kIx)	)
různých	různý	k2eAgInPc2d1	různý
romských	romský	k2eAgInPc2d1	romský
dialektů	dialekt	k1gInPc2	dialekt
</s>
</p>
<p>
<s>
Ukázky	ukázka	k1gFnPc1	ukázka
romských	romský	k2eAgInPc2d1	romský
dialektů	dialekt	k1gInPc2	dialekt
</s>
</p>
