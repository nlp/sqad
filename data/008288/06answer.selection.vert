<s>
Romština	romština	k1gFnSc1	romština
je	být	k5eAaImIp3nS	být
flektivní	flektivní	k2eAgInSc4d1	flektivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
8	[number]	k4	8
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
vokativ	vokativ	k1gInSc1	vokativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
lokativ	lokativ	k1gInSc1	lokativ
<g/>
,	,	kIx,	,
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc1	ablativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
rody	rod	k1gInPc4	rod
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
slovesné	slovesný	k2eAgFnPc1d1	slovesná
třídy	třída	k1gFnPc1	třída
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
koncovky	koncovka	k1gFnPc4	koncovka
v	v	k7c6	v
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
