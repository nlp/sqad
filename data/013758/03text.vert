<s>
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgInSc1d1
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánský	dánský	k2eAgInSc1d1
</s>
<s>
král	král	k1gMnSc1
dánský	dánský	k2eAgMnSc1d1
<g/>
,	,	kIx,
švédský	švédský	k2eAgMnSc1d1
a	a	k8xC
norský	norský	k2eAgMnSc1d1
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánský	dánský	k2eAgInSc1d1
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1440	#num#	k4
–	–	k?
1448	#num#	k4
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
<g/>
1442	#num#	k4
–	–	k?
1448	#num#	k4
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1440	#num#	k4
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
<g/>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1441	#num#	k4
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
1443	#num#	k4
v	v	k7c6
Norsku	Norsko	k1gNnSc6
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1416	#num#	k4
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1448	#num#	k4
</s>
<s>
Helsingborg	Helsingborg	k1gMnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Roskilde	Roskild	k1gInSc5
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Kristián	Kristián	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
Dánsko	Dánsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Karel	Karel	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Norsko	Norsko	k1gNnSc1
a	a	k8xC
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Braniborská	braniborský	k2eAgFnSc1d1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Wittelsbachové	Wittelsbachové	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Falcko-Neumarktský	Falcko-Neumarktský	k2eAgMnSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Pomořanská	pomořanský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgMnSc1d1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1416	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1448	#num#	k4
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
letech	let	k1gInPc6
1440	#num#	k4
<g/>
–	–	k?
<g/>
1448	#num#	k4
dánský	dánský	k2eAgMnSc1d1
a	a	k8xC
švédský	švédský	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1442	#num#	k4
<g/>
–	–	k?
<g/>
1448	#num#	k4
norský	norský	k2eAgMnSc1d1
král	král	k1gMnSc1
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Wittelsbachů	Wittelsbach	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Erb	erb	k1gInSc1
Kryštofa	Kryštof	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorského	bavorský	k2eAgInSc2d1
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
jako	jako	k9
poslední	poslední	k2eAgNnSc4d1
<g/>
,	,	kIx,
sedmý	sedmý	k4xOgInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
jediný	jediný	k2eAgMnSc1d1
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
přeživší	přeživší	k2eAgMnSc1d1
potomek	potomek	k1gMnSc1
hornofalckého	hornofalcký	k2eAgMnSc2d1
kurfiřta	kurfiřt	k1gMnSc2
Jana	Jan	k1gMnSc2
Wittelsbacha	Wittelsbach	k1gMnSc2
(	(	kIx(
<g/>
†	†	k?
1443	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
první	první	k4xOgFnPc1
manželky	manželka	k1gFnPc1
Kateřiny	Kateřina	k1gFnSc2
Pomořanské	pomořanský	k2eAgInPc4d1
(	(	kIx(
<g/>
†	†	k?
1426	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dcery	dcera	k1gFnSc2
pomořanského	pomořanský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Vartislava	Vartislav	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Greifen	Greifna	k1gFnPc2
<g/>
,	,	kIx,
sestry	sestra	k1gFnSc2
dánského	dánský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Erika	Erik	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
Neumarktu	Neumarkt	k1gInSc6
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Falci	Falc	k1gFnSc6
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
prameny	pramen	k1gInPc1
však	však	k9
uvádějí	uvádět	k5eAaImIp3nP
i	i	k9
Helsingborg	Helsingborg	k1gInSc4
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychován	vychován	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
na	na	k7c6
dvoře	dvůr	k1gInSc6
římského	římský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Zikmunda	Zikmund	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
spojencem	spojenec	k1gMnSc7
Zikmundovým	Zikmundův	k2eAgMnSc7d1
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
válkách	válka	k1gFnPc6
s	s	k7c7
českými	český	k2eAgMnPc7d1
husity	husita	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Kryštofova	Kryštofův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
sestrou	sestra	k1gFnSc7
Erika	Erik	k1gMnSc2
Pomořanského	pomořanský	k2eAgMnSc2d1
<g/>
,	,	kIx,
pomořanského	pomořanský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
jako	jako	k9
Erik	Erika	k1gFnPc2
I.	I.	kA
panoval	panovat	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1449	#num#	k4
v	v	k7c6
Pomořansku	Pomořansko	k1gNnSc6
a	a	k8xC
před	před	k7c7
tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgMnS
králem	král	k1gMnSc7
Kalmarské	Kalmarský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
Erik	Erik	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
Erik	Erik	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Norsku	Norsko	k1gNnSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
Erik	Erik	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Stejně	stejně	k6eAd1
jako	jako	k9
Erik	Erik	k1gMnSc1
i	i	k8xC
Kryštofova	Kryštofův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
přeslici	přeslice	k1gFnSc6
pravnučkou	pravnučka	k1gFnSc7
Valdemara	Valdemara	k1gFnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atterdaga	Atterdaga	k1gFnSc1
(	(	kIx(
<g/>
†	†	k?
1375	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
posledního	poslední	k2eAgMnSc2d1
dánského	dánský	k2eAgMnSc2d1
krále	král	k1gMnSc2
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Estridsenů	Estridsen	k1gInPc2
(	(	kIx(
<g/>
nepočítaje	nepočítaje	k7c4
v	v	k7c6
to	ten	k3xDgNnSc4
královnu	královna	k1gFnSc4
Markétu	Markéta	k1gFnSc4
I.	I.	kA
Dánskou	dánský	k2eAgFnSc7d1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
panovala	panovat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1412	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
Když	když	k8xS
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
<g/>
,	,	kIx,
Švédsku	Švédsko	k1gNnSc6
a	a	k8xC
Norsku	Norsko	k1gNnSc6
-	-	kIx~
zemích	zem	k1gFnPc6
Kalmarské	Kalmarský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
-	-	kIx~
došlo	dojít	k5eAaPmAgNnS
pro	pro	k7c4
všeobecnou	všeobecný	k2eAgFnSc4d1
nespokojenost	nespokojenost	k1gFnSc4
s	s	k7c7
vládou	vláda	k1gFnSc7
Erika	Erik	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
sesazení	sesazení	k1gNnSc3
s	s	k7c7
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
sněm	sněm	k1gInSc1
dánského	dánský	k2eAgNnSc2d1
království	království	k1gNnSc2
se	se	k3xPyFc4
obrátil	obrátit	k5eAaPmAgInS
na	na	k7c4
Kryštofa	Kryštof	k1gMnSc4
jako	jako	k8xC,k8xS
na	na	k7c4
dalšího	další	k2eAgMnSc4d1
pokračovatele	pokračovatel	k1gMnSc4
dánské	dánský	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
linie	linie	k1gFnSc2
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
královské	královský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
a	a	k8xC
on	on	k3xPp3gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1439	#num#	k4
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
Dánska	Dánsko	k1gNnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc7
guvernérem	guvernér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1440	#num#	k4
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
králem	král	k1gMnSc7
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
roku	rok	k1gInSc2
1440	#num#	k4
korunován	korunován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dánské	dánský	k2eAgFnSc6d1
volbě	volba	k1gFnSc6
poslal	poslat	k5eAaPmAgMnS
vyslance	vyslanec	k1gMnSc2
do	do	k7c2
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tam	tam	k6eAd1
navázali	navázat	k5eAaPmAgMnP
vztahy	vztah	k1gInPc4
se	s	k7c7
švédským	švédský	k2eAgInSc7d1
sněmem	sněm	k1gInSc7
<g/>
;	;	kIx,
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
byl	být	k5eAaImAgInS
guvernérem	guvernér	k1gMnSc7
Karel	Karel	k1gMnSc1
Knutsson	Knutsson	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
to	ten	k3xDgNnSc4
akceptoval	akceptovat	k5eAaBmAgMnS
a	a	k8xC
Kryštof	Kryštof	k1gMnSc1
byl	být	k5eAaImAgMnS
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
1441	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
králem	král	k1gMnSc7
Švédska	Švédsko	k1gNnSc2
a	a	k8xC
den	den	k1gInSc1
nato	nato	k6eAd1
v	v	k7c6
Uppsale	Uppsala	k1gFnSc6
korunován	korunován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
1442	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
i	i	k8xC
králem	král	k1gMnSc7
Norska	Norsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
překonání	překonání	k1gNnSc6
odporu	odpor	k1gInSc2
posledních	poslední	k2eAgInPc2d1
stoupenců	stoupenec	k1gMnPc2
Erika	Erik	k1gMnSc2
a	a	k8xC
potlačení	potlačení	k1gNnSc1
povstání	povstání	k1gNnSc2
v	v	k7c6
Jutsku	Jutsko	k1gNnSc6
(	(	kIx(
<g/>
1441	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1443	#num#	k4
korunován	korunovat	k5eAaBmNgInS
i	i	k8xC
králem	král	k1gMnSc7
Norska	Norsko	k1gNnSc2
<g/>
,	,	kIx,
přijav	přijmout	k5eAaPmDgInS
současně	současně	k6eAd1
titul	titul	k1gInSc1
nordic	nordice	k1gFnPc2
archrex	archrex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomtéž	týž	k3xTgInSc6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
převzal	převzít	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Falci	Falc	k1gFnSc6
(	(	kIx(
<g/>
Pfalzgraf	Pfalzgraf	k1gInSc1
von	von	k1gInSc1
Neumarkt	Neumarkt	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
Oberpfalz	Oberpfalz	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
převzetí	převzetí	k1gNnSc6
vlády	vláda	k1gFnSc2
narovnal	narovnat	k5eAaPmAgMnS
Kryštof	Kryštof	k1gMnSc1
vztahy	vztah	k1gInPc1
s	s	k7c7
Hanzou	hanza	k1gFnSc7
<g/>
,	,	kIx,
potvrdiv	potvrdit	k5eAaPmDgInS
její	její	k3xOp3gNnPc4
dřívější	dřívější	k2eAgNnPc4d1
privilegia	privilegium	k1gNnPc4
<g/>
,	,	kIx,
i	i	k8xC
s	s	k7c7
církví	církev	k1gFnSc7
a	a	k8xC
s	s	k7c7
panovníkem	panovník	k1gMnSc7
Holštýnského	holštýnský	k2eAgNnSc2d1
vévodství	vévodství	k1gNnSc2
hrabětem	hrabě	k1gMnSc7
Adolfem	Adolf	k1gMnSc7
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
udělil	udělit	k5eAaPmAgInS
v	v	k7c4
léno	léno	k1gNnSc4
šlesvické	šlesvický	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
krále	král	k1gMnSc4
byla	být	k5eAaImAgFnS
omezená	omezený	k2eAgFnSc1d1
<g/>
,	,	kIx,
oslabená	oslabený	k2eAgFnSc1d1
nesnadnou	snadný	k2eNgFnSc7d1
spoluprací	spolupráce	k1gFnSc7
s	s	k7c7
mocnými	mocný	k2eAgInPc7d1
rody	rod	k1gInPc7
všech	všecek	k3xTgInPc2
tří	tři	k4xCgInPc2
království	království	k1gNnPc2
<g/>
;	;	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
situaci	situace	k1gFnSc6
se	se	k3xPyFc4
král	král	k1gMnSc1
v	v	k7c6
počátcích	počátek	k1gInPc6
svého	svůj	k3xOyFgNnSc2
panování	panování	k1gNnSc2
snažil	snažit	k5eAaImAgMnS
o	o	k7c6
zachování	zachování	k1gNnSc6
rovnováhy	rovnováha	k1gFnSc2
mezi	mezi	k7c7
všemi	všecek	k3xTgFnPc7
třemi	tři	k4xCgFnPc7
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
však	však	k9
podporoval	podporovat	k5eAaImAgInS
dominanci	dominance	k1gFnSc4
Dánska	Dánsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napomáhal	napomáhat	k5eAaImAgMnS,k5eAaBmAgMnS
rovněž	rovněž	k9
rozvoji	rozvoj	k1gInSc6
měst	město	k1gNnPc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
Kodaně	Kodaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
bylo	být	k5eAaImAgNnS
nově	nově	k6eAd1
kodifikováno	kodifikován	k2eAgNnSc1d1
švédské	švédský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Kristoffers	Kristoffers	k1gInSc1
Landslag	Landslag	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
konci	konec	k1gInSc3
jeho	jeho	k3xOp3gFnSc3
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
znovu	znovu	k6eAd1
projevily	projevit	k5eAaPmAgFnP
diskrepance	diskrepance	k1gFnPc1
s	s	k7c7
hanzovní	hanzovní	k2eAgFnSc7d1
ligou	liga	k1gFnSc7
a	a	k8xC
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Kryštof	Kryštof	k1gMnSc1
plánoval	plánovat	k5eAaImAgMnS
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
různými	různý	k2eAgMnPc7d1
německými	německý	k2eAgMnPc7d1
knížaty	kníže	k1gMnPc7wR
se	se	k3xPyFc4
záměrem	záměr	k1gInSc7
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Lübeck	Lübeck	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
plánoval	plánovat	k5eAaImAgMnS
i	i	k9
válku	válka	k1gFnSc4
proti	proti	k7c3
Rusku	Rusko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
nabídl	nabídnout	k5eAaPmAgMnS
Švédům	Švéd	k1gMnPc3
vrátit	vrátit	k5eAaPmF
ostrov	ostrov	k1gInSc4
Gotland	Gotlanda	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
usadil	usadit	k5eAaPmAgMnS
Erik	Erik	k1gMnSc1
Pomořanský	pomořanský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
odtud	odtud	k6eAd1
podnikal	podnikat	k5eAaImAgMnS
pirátské	pirátský	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
a	a	k8xC
nikdy	nikdy	k6eAd1
s	s	k7c7
králem	král	k1gMnSc7
Kryštofem	Kryštof	k1gMnSc7
nesouhlasil	souhlasit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
se	se	k3xPyFc4
opakovaně	opakovaně	k6eAd1
musel	muset	k5eAaImAgMnS
vyrovnávat	vyrovnávat	k5eAaImF
s	s	k7c7
komplikovanou	komplikovaný	k2eAgFnSc7d1
vnitrostátní	vnitrostátní	k2eAgFnSc7d1
situací	situace	k1gFnSc7
<g/>
;	;	kIx,
kvůli	kvůli	k7c3
vzrůstající	vzrůstající	k2eAgFnSc3d1
nespokojenosti	nespokojenost	k1gFnSc3
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
svolal	svolat	k5eAaPmAgMnS
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1448	#num#	k4
sněm	sněm	k1gInSc1
do	do	k7c2
Jönköpingu	Jönköping	k1gInSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
v	v	k7c6
Helsingborgu	Helsingborg	k1gInSc6
nenadále	nenadále	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
nezůstaviv	zůstavit	k5eNaPmDgInS
dědiců	dědic	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
posledním	poslední	k2eAgMnSc7d1
potomkem	potomek	k1gMnSc7
Valdemara	Valdemara	k1gFnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atterdaga	Atterdaga	k1gFnSc1
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Kristián	Kristián	k1gMnSc1
I.	I.	kA
položil	položit	k5eAaPmAgMnS
základy	základ	k1gInPc4
nové	nový	k2eAgFnSc2d1
Oldenburské	oldenburský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc4
Wittelsbachů	Wittelsbach	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
Kryštof	Kryštof	k1gMnSc1
náležel	náležet	k5eAaImAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
Švédska	Švédsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1654	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
1445	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Kodani	Kodaň	k1gFnSc6
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Doroteou	Dorotea	k1gFnSc7
Braniborskou	braniborský	k2eAgFnSc7d1
<g/>
,	,	kIx,
nejmladší	mladý	k2eAgFnSc7d3
ze	z	k7c2
tří	tři	k4xCgFnPc2
dcer	dcera	k1gFnPc2
(	(	kIx(
<g/>
jediný	jediný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
při	při	k7c6
narození	narození	k1gNnSc6
<g/>
)	)	kIx)
braniborského	braniborský	k2eAgMnSc2d1
markraběte	markrabě	k1gMnSc2
Jana	Jan	k1gMnSc2
<g/>
,	,	kIx,
zvaného	zvaný	k2eAgMnSc2d1
Alchymista	alchymista	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
vévodkyně	vévodkyně	k1gFnSc2
Barbary	Barbara	k1gFnSc2
Anhaltské	Anhaltský	k2eAgFnSc2d1
<g/>
;	;	kIx,
její	její	k3xOp3gFnSc1
nejstarší	starý	k2eAgFnSc1d3
sestra	sestra	k1gFnSc1
Barbara	Barbara	k1gFnSc1
(	(	kIx(
<g/>
1423	#num#	k4
<g/>
-	-	kIx~
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
provdaná	provdaný	k2eAgFnSc1d1
vévodkyně	vévodkyně	k1gFnSc1
milánská	milánský	k2eAgFnSc1d1
a	a	k8xC
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1425	#num#	k4
<g/>
-	-	kIx~
<g/>
1465	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
provdaná	provdaný	k2eAgFnSc1d1
vévodkyně	vévodkyně	k1gFnSc1
pomořská	pomořský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
krátkého	krátký	k2eAgInSc2d1
(	(	kIx(
<g/>
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
a	a	k8xC
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
)	)	kIx)
manželství	manželství	k1gNnSc2
se	se	k3xPyFc4
nenarodil	narodit	k5eNaPmAgMnS
žádný	žádný	k3yNgMnSc1
potomek	potomek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
roku	rok	k1gInSc2
1448	#num#	k4
v	v	k7c6
Helsingborgu	Helsingborg	k1gInSc6
a	a	k8xC
byl	být	k5eAaImAgInS
pochován	pochovat	k5eAaPmNgInS
v	v	k7c6
katedrále	katedrála	k1gFnSc6
v	v	k7c6
Roskilde	Roskild	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
vdova	vdova	k1gFnSc1
z	z	k7c2
ekonomických	ekonomický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
na	na	k7c6
straně	strana	k1gFnSc6
Dánska	Dánsko	k1gNnSc2
(	(	kIx(
<g/>
byla	být	k5eAaImAgFnS
obava	obava	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
sňatku	sňatek	k1gInSc2
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
nápadníkem	nápadník	k1gMnSc7
by	by	kYmCp3nS
Dánsko	Dánsko	k1gNnSc1
muselo	muset	k5eAaImAgNnS
vrátit	vrátit	k5eAaPmF
její	její	k3xOp3gNnSc4
věno	věno	k1gNnSc4
<g/>
)	)	kIx)
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
nového	nový	k2eAgMnSc4d1
krále	král	k1gMnSc4
<g/>
,	,	kIx,
Kryštofova	Kryštofův	k2eAgMnSc4d1
nástupce	nástupce	k1gMnSc4
Kristiána	Kristián	k1gMnSc2
I.	I.	kA
z	z	k7c2
Oldenburské	oldenburský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vládne	vládnout	k5eAaImIp3nS
na	na	k7c6
dánském	dánský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Předkové	předek	k1gMnPc1
</s>
<s>
Ruprecht	Ruprecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcký	falcký	k2eAgInSc1d1
</s>
<s>
Ruprecht	Ruprecht	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcký	falcký	k2eAgInSc1d1
</s>
<s>
Beatrix	Beatrix	k1gInSc1
Sicilská	sicilský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
Falcko-Neumarktský	Falcko-Neumarktský	k2eAgMnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
Norimberský	norimberský	k2eAgInSc1d1
</s>
<s>
Eliška	Eliška	k1gFnSc1
Norimberská	norimberský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Míšeňská	míšeňský	k2eAgFnSc1d1
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgInSc1d1
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1
V.	V.	kA
Pomořanský	pomořanský	k2eAgInSc1d1
</s>
<s>
Vartislav	Vartislav	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomořanský	pomořanský	k2eAgInSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Polská	polský	k2eAgFnSc1d1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Pomořanská	pomořanský	k2eAgFnSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meklenburský	meklenburský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Meklenburská	meklenburský	k2eAgFnSc1d1
</s>
<s>
Ingeborg	Ingeborg	k1gInSc1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20101010083600/http://www.historicum.net/themen/reformation/glossar/c/	https://web.archive.org/web/20101010083600/http://www.historicum.net/themen/reformation/glossar/c/	k4
</s>
<s>
http://genealogy.euweb.cz/wittel/wittel2.html#C3	http://genealogy.euweb.cz/wittel/wittel2.html#C3	k4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Erik	Erik	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomořanský	pomořanský	k2eAgInSc1d1
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1
král	král	k1gMnSc1
1440	#num#	k4
<g/>
–	–	k?
<g/>
1448	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
I.	I.	kA
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Erik	Erik	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomořanský	pomořanský	k2eAgInSc1d1
</s>
<s>
Norský	norský	k2eAgMnSc1d1
král	král	k1gMnSc1
1442	#num#	k4
<g/>
–	–	k?
<g/>
1448	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Karel	Karel	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Erik	Erik	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomořanský	pomořanský	k2eAgInSc1d1
</s>
<s>
Švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
1440	#num#	k4
<g/>
–	–	k?
<g/>
1448	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Karel	Karel	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
Dánští	dánský	k2eAgMnPc1d1
králové	král	k1gMnPc1
Vikinští	vikinský	k2eAgMnPc1d1
králové	král	k1gMnPc1
(	(	kIx(
<g/>
c.	c.	k?
934	#num#	k4
<g/>
–	–	k?
<g/>
1047	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gorm	Gorm	k1gMnSc1
Starý	Starý	k1gMnSc1
•	•	k?
Harald	Harald	k1gMnSc1
I	i	k9
<g/>
.1	.1	k4
•	•	k?
Sven	Svena	k1gFnPc2
I	i	k9
<g/>
.1	.1	k4
<g/>
,3	,3	k4
•	•	k?
Harald	Harald	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Knut	knuta	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliký	veliký	k2eAgInSc1d1
<g/>
1,3	1,3	k4
•	•	k?
Knut	knuta	k1gFnPc2
III	III	kA
<g/>
.3	.3	k4
•	•	k?
Magnus	Magnus	k1gInSc1
I	i	k9
<g/>
.1	.1	k4
Estridsenové	Estridsenové	k2eAgInPc2d1
(	(	kIx(
<g/>
1047	#num#	k4
<g/>
–	–	k?
<g/>
1157	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sven	Sven	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Harald	Harald	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Knut	knuta	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Olaf	Olaf	k1gMnSc1
I.	I.	kA
•	•	k?
Erik	Erik	k1gMnSc1
I.	I.	kA
•	•	k?
Niels	Niels	k1gInSc1
•	•	k?
Erik	Erika	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Erik	Erik	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Sven	Sveno	k1gNnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valdemarové	Valdemarové	k2eAgMnPc2d1
(	(	kIx(
<g/>
1157	#num#	k4
<g/>
–	–	k?
<g/>
1375	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Valdemar	Valdemara	k1gFnPc2
I.	I.	kA
•	•	k?
Knut	knuta	k1gFnPc2
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Valdemar	Valdemara	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Erik	Erik	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
•	•	k?
Abel	Abel	k1gMnSc1
•	•	k?
Kryštof	Kryštof	k1gMnSc1
I.	I.	kA
•	•	k?
Erik	Erik	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
•	•	k?
Kryštof	Kryštof	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
•	•	k?
Valdemar	Valdemara	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Valdemar	Valdemara	k1gFnPc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Folkungové	Folkung	k1gMnPc1
(	(	kIx(
<g/>
1376	#num#	k4
<g/>
–	–	k?
<g/>
1387	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olaf	Olaf	k1gInSc1
II	II	kA
<g/>
.1	.1	k4
Valdemarové	Valdemarová	k1gFnPc1
(	(	kIx(
<g/>
1387	#num#	k4
<g/>
–	–	k?
<g/>
1412	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Markéta	Markéta	k1gFnSc1
I	i	k9
<g/>
.1	.1	k4
<g/>
,2	,2	k4
Greifenové	Greifenové	k2eAgMnPc2d1
(	(	kIx(
<g/>
1412	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Erik	Erik	k1gMnSc1
VII	VII	kA
<g/>
.1	.1	k4
<g/>
,2	,2	k4
Wittelsbachové	Wittelsbachové	k2eAgMnPc2d1
(	(	kIx(
<g/>
1440	#num#	k4
<g/>
–	–	k?
<g/>
1448	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kryštof	Kryštof	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgInSc4d1
<g/>
1,2	1,2	k4
Oldenburkové	Oldenburková	k1gFnPc1
(	(	kIx(
<g/>
1448	#num#	k4
<g/>
–	–	k?
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kristián	Kristián	k1gMnSc1
I	i	k9
<g/>
.1	.1	k4
<g/>
,2	,2	k4
•	•	k?
Jan	Jan	k1gMnSc1
I	i	k9
<g/>
.1	.1	k4
<g/>
,2	,2	k4
•	•	k?
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.1	.1	k4
<g/>
,2	,2	k4
•	•	k?
Frederik	Frederika	k1gFnPc2
I	i	k9
<g/>
.1	.1	k4
<g/>
,2	,2	k4
•	•	k?
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.1	.1	k4
•	•	k?
Frederik	Frederika	k1gFnPc2
II	II	kA
<g/>
.1	.1	k4
•	•	k?
Kristián	Kristián	k1gMnSc1
IV	IV	kA
<g/>
.1	.1	k4
•	•	k?
Frederik	Frederika	k1gFnPc2
III	III	kA
<g/>
.1	.1	k4
•	•	k?
Kristián	Kristián	k1gMnSc1
V	v	k7c6
<g/>
.1	.1	k4
•	•	k?
Frederik	Frederika	k1gFnPc2
IV	IV	kA
<g/>
.1	.1	k4
•	•	k?
<g/>
Kristián	Kristián	k1gMnSc1
VI	VI	kA
<g/>
.1	.1	k4
•	•	k?
Kristián	Kristián	k1gMnSc1
VII	VII	kA
<g/>
.1	.1	k4
•	•	k?
Kristián	Kristián	k1gMnSc1
VIII	VIII	kA
<g/>
.1	.1	k4
•	•	k?
Frederik	Frederika	k1gFnPc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glücksburkové	Glücksburkové	k2eAgMnPc2d1
(	(	kIx(
<g/>
od	od	k7c2
1863	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kristián	Kristián	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
•	•	k?
Frederik	Frederik	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
•	•	k?
Kristián	Kristián	k1gMnSc1
X	X	kA
<g/>
.4	.4	k4
•	•	k?
Frederik	Frederika	k1gFnPc2
IX	IX	kA
<g/>
.	.	kIx.
•	•	k?
Markéta	Markéta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
také	také	k9
král	král	k1gMnSc1
norský	norský	k2eAgMnSc1d1
<g/>
,	,	kIx,
2	#num#	k4
také	také	k9
král	král	k1gMnSc1
švédský	švédský	k2eAgMnSc1d1
<g/>
,	,	kIx,
3	#num#	k4
také	také	k9
král	král	k1gMnSc1
anglický	anglický	k2eAgMnSc1d1
<g/>
,	,	kIx,
4	#num#	k4
také	také	k9
král	král	k1gMnSc1
islandský	islandský	k2eAgMnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118871099	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1597	#num#	k4
5503	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2007048976	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
13105437	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2007048976	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Středověk	středověk	k1gInSc1
|	|	kIx~
Dánsko	Dánsko	k1gNnSc1
</s>
