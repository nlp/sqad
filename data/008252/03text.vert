<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
ha-Bima	ha-Bimum	k1gNnSc2	ha-Bimum
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
ה	ה	k?	ה
ה	ה	k?	ה
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
scéna	scéna	k1gFnSc1	scéna
<g/>
)	)	kIx)	)
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Avivo	k1gNnSc6	Avivo
je	být	k5eAaImIp3nS	být
izraelské	izraelský	k2eAgNnSc4d1	izraelské
národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
hebrejských	hebrejský	k2eAgNnPc2d1	hebrejské
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
ha-Bima	ha-Bimum	k1gNnSc2	ha-Bimum
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Nachumem	Nachum	k1gInSc7	Nachum
Zemachem	Zemach	k1gInSc7	Zemach
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
muselo	muset	k5eAaImAgNnS	muset
divadlo	divadlo	k1gNnSc1	divadlo
opustit	opustit	k5eAaPmF	opustit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
pětiletého	pětiletý	k2eAgNnSc2d1	pětileté
celosvětového	celosvětový	k2eAgNnSc2d1	celosvětové
turné	turné	k1gNnSc2	turné
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
i	i	k8xC	i
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
mandátní	mandátní	k2eAgFnSc2d1	mandátní
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vplynulo	vplynout	k5eAaPmAgNnS	vplynout
dosud	dosud	k6eAd1	dosud
samostatné	samostatný	k2eAgNnSc1d1	samostatné
Divadlo	divadlo	k1gNnSc1	divadlo
Zavit	zavit	k2eAgMnSc1d1	zavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
divadlo	divadlo	k1gNnSc1	divadlo
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
kvůli	kvůli	k7c3	kvůli
zásadní	zásadní	k2eAgFnSc3d1	zásadní
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
budovy	budova	k1gFnSc2	budova
zachován	zachovat	k5eAaPmNgInS	zachovat
pouze	pouze	k6eAd1	pouze
skelet	skelet	k1gInSc1	skelet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
necelých	celý	k2eNgNnPc6d1	necelé
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
oprav	oprava	k1gFnPc2	oprava
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Habima	Habimum	k1gNnSc2	Habimum
Theatre	Theatr	k1gInSc5	Theatr
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ha-Bima	Ha-Bimum	k1gNnSc2	Ha-Bimum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Habima	Habima	k1gFnSc1	Habima
spojí	spojit	k5eAaPmIp3nS	spojit
dva	dva	k4xCgInPc4	dva
festivaly	festival	k1gInPc4	festival
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
