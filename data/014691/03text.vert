<s>
Antické	antický	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
ve	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Antikové	Antik	k1gMnPc1
byli	být	k5eAaImAgMnP
fiktivní	fiktivní	k2eAgFnSc7d1
rasou	rasa	k1gFnSc7
vyspělých	vyspělý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
původními	původní	k2eAgMnPc7d1
tvůrci	tvůrce	k1gMnPc7
sítě	síť	k1gFnSc2
hvězdných	hvězdný	k2eAgFnPc2d1
bran	brána	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
rasa	rasa	k1gFnSc1
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
naší	náš	k3xOp1gFnSc2
části	část	k1gFnSc2
vesmíru	vesmír	k1gInSc2
již	již	k6eAd1
dávno	dávno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antikové	Antik	k1gMnPc1
se	se	k3xPyFc4
naučili	naučit	k5eAaPmAgMnP
povznést	povznést	k5eAaPmF
na	na	k7c4
vyšší	vysoký	k2eAgFnSc4d2
úroveň	úroveň	k1gFnSc4
existence	existence	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
galaxii	galaxie	k1gFnSc6
přehnala	přehnat	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
epidemie	epidemie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
nich	on	k3xPp3gMnPc6
zemřelo	zemřít	k5eAaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
naučili	naučit	k5eAaPmAgMnP
povznést	povznést	k5eAaPmF
získali	získat	k5eAaPmAgMnP
nový	nový	k2eAgInSc4d1
smysl	smysl	k1gInSc4
existence	existence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vesmírné	vesmírný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vesmírné	vesmírný	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
ve	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
<g/>
#	#	kIx~
<g/>
Antické	antický	k2eAgFnPc1d1
vesmírné	vesmírný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Lodě	loď	k1gFnPc1
třídy	třída	k1gFnSc2
Aurora	Aurora	k1gFnSc1
</s>
<s>
Lodě	loď	k1gFnPc1
třídy	třída	k1gFnSc2
Aurora	Aurora	k1gFnSc1
patří	patřit	k5eAaImIp3nP
k	k	k7c3
antickým	antický	k2eAgFnPc3d1
válečným	válečný	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
jedny	jeden	k4xCgFnPc1
z	z	k7c2
nejmocnějších	mocný	k2eAgFnPc2d3
lodí	loď	k1gFnPc2
v	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
souboji	souboj	k1gInSc6
jeden	jeden	k4xCgMnSc1
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
dokáží	dokázat	k5eAaPmIp3nP
bez	bez	k7c2
problému	problém	k1gInSc2
porazit	porazit	k5eAaPmF
wraithský	wraithský	k2eAgInSc4d1
Úl	úl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
těchto	tento	k3xDgFnPc2
lodí	loď	k1gFnPc2
jsou	být	k5eAaImIp3nP
Drony	Dron	k1gInPc1
ovládané	ovládaný	k2eAgInPc1d1
pomocí	pomocí	k7c2
Křesla	křeslo	k1gNnSc2
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
Atlantidě	Atlantida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jejich	jejich	k3xOp3gInSc2
energetického	energetický	k2eAgInSc2d1
systému	systém	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
připojeno	připojit	k5eAaPmNgNnS
ZPM	ZPM	kA
(	(	kIx(
<g/>
Zero	Zero	k1gMnSc1
Power	Power	k1gMnSc1
Module	modul	k1gInSc5
<g/>
)	)	kIx)
neboli	neboli	k8xC
"	"	kIx"
<g/>
modul	modul	k1gInSc1
nulového	nulový	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
lodě	loď	k1gFnPc1
mají	mít	k5eAaImIp3nP
dva	dva	k4xCgInPc4
typy	typ	k1gInPc4
hyperpohonu	hyperpohon	k1gInSc2
–	–	k?
buď	buď	k8xC
mezihvězdný	mezihvězdný	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
výkonnější	výkonný	k2eAgInSc1d2
<g/>
,	,	kIx,
mezigalaktický	mezigalaktický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
několik	několik	k4yIc1
lodí	loď	k1gFnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Aurora	Aurora	k1gFnSc1
(	(	kIx(
<g/>
epizoda	epizoda	k1gFnSc1
Aurora	Aurora	k1gFnSc1
<g/>
,	,	kIx,
zničena	zničen	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Hipoforalkus	Hipoforalkus	k1gInSc1
(	(	kIx(
<g/>
překřtěna	překřtěn	k2eAgFnSc1d1
na	na	k7c4
Orion	orion	k1gInSc4
<g/>
,	,	kIx,
epizody	epizoda	k1gFnPc1
Peklo	peklo	k1gNnSc1
<g/>
,	,	kIx,
Spojenci	spojenec	k1gMnPc1
a	a	k8xC
Země	země	k1gFnSc1
nikoho	nikdo	k3yNnSc4
<g/>
,	,	kIx,
zničena	zničen	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Tria	trio	k1gNnPc1
(	(	kIx(
<g/>
epizoda	epizoda	k1gFnSc1
Návrat	návrat	k1gInSc1
<g/>
,	,	kIx,
<g/>
zničena	zničen	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Nejmenovaná	jmenovaný	k2eNgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
nalezli	naleznout	k5eAaPmAgMnP,k5eAaBmAgMnP
Cestovatelé	cestovatel	k1gMnPc1
(	(	kIx(
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
epizody	epizoda	k1gFnPc4
Cestovatelé	cestovatel	k1gMnPc1
(	(	kIx(
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Všechny	všechen	k3xTgInPc1
mé	můj	k3xOp1gInPc1
hříchy	hřích	k1gInPc1
budou	být	k5eAaImBp3nP
vzpomenuty	vzpomenut	k2eAgInPc1d1
a	a	k8xC
Ztracený	ztracený	k2eAgInSc1d1
kmen	kmen	k1gInSc1
<g/>
,	,	kIx,
nejspíše	nejspíše	k9
zničena	zničen	k2eAgFnSc1d1
<g/>
,	,	kIx,
neupřesněno	upřesněn	k2eNgNnSc1d1
<g/>
,	,	kIx,
cestovatelé	cestovatel	k1gMnPc1
měli	mít	k5eAaImAgMnP
několik	několik	k4yIc4
antických	antický	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
těchto	tento	k3xDgFnPc2
lodí	loď	k1gFnPc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
seriálu	seriál	k1gInSc6
ještě	ještě	k9
několik	několik	k4yIc4
nejmenovaných	nejmenovaná	k1gFnPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
Asuranských	Asuranský	k2eAgInPc2d1
<g/>
,	,	kIx,
lodí	loď	k1gFnSc7
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Puddle	Puddle	k6eAd1
Jumper	jumper	k1gInSc1
(	(	kIx(
<g/>
dále	daleko	k6eAd2
Jumper	jumper	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
válcovité	válcovitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
víceúčelové	víceúčelový	k2eAgNnSc1d1
plavidlo	plavidlo	k1gNnSc1
vytvořené	vytvořený	k2eAgFnSc2d1
Antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
pro	pro	k7c4
přepravu	přeprava	k1gFnSc4
skrze	skrze	k?
hvězdnou	hvězdný	k2eAgFnSc4d1
bránu	brána	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
minimálně	minimálně	k6eAd1
osmi	osm	k4xCc7
střelami	střela	k1gFnPc7
drone	dronout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
ukryté	ukrytý	k2eAgFnPc1d1
ve	v	k7c6
výsuvných	výsuvný	k2eAgNnPc6d1
pouzdrech	pouzdro	k1gNnPc6
po	po	k7c6
stranách	strana	k1gFnPc6
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc4
Puddle	Puddle	k1gFnSc2
Jumper	jumper	k1gInSc1
vymyslel	vymyslet	k5eAaPmAgMnS
major	major	k1gMnSc1
John	John	k1gMnSc1
Sheppard	Sheppard	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
skutečný	skutečný	k2eAgInSc1d1
Antický	antický	k2eAgInSc1d1
název	název	k1gInSc1
není	být	k5eNaImIp3nS
znám	znám	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
vybavena	vybavit	k5eAaPmNgFnS
maskováním	maskování	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
plavidlo	plavidlo	k1gNnSc1
učiní	učinit	k5eAaPmIp3nS,k5eAaImIp3nS
neviditelné	viditelný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
přední	přední	k2eAgFnSc4d1
a	a	k8xC
zadní	zadní	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vpředu	vpředu	k6eAd1
jsou	být	k5eAaImIp3nP
čtyři	čtyři	k4xCgNnPc4
křesla	křeslo	k1gNnPc4
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc4
pro	pro	k7c4
piloty	pilota	k1gFnPc4
a	a	k8xC
dvě	dva	k4xCgFnPc1
pro	pro	k7c4
vědce	vědec	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
vzadu	vzadu	k6eAd1
je	být	k5eAaImIp3nS
místo	místo	k1gNnSc4
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzole	konzola	k1gFnSc6
umístěná	umístěný	k2eAgFnSc1d1
mezi	mezi	k7c7
pilotem	pilot	k1gMnSc7
a	a	k8xC
druhým	druhý	k4xOgInSc7
pilotem	pilot	k1gInSc7
je	být	k5eAaImIp3nS
modifikované	modifikovaný	k2eAgNnSc4d1
vytáčecí	vytáčecí	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
přímý	přímý	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
nejbližší	blízký	k2eAgFnSc3d3
hvězdné	hvězdný	k2eAgFnSc3d1
bráně	brána	k1gFnSc3
a	a	k8xC
vytočení	vytočení	k1gNnSc6
adresy	adresa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
dosahovat	dosahovat	k5eAaImF
vysokých	vysoký	k2eAgFnPc2d1
rychlostí	rychlost	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
musí	muset	k5eAaImIp3nP
mít	mít	k5eAaImF
pokročilé	pokročilý	k2eAgInPc1d1
inerciální	inerciální	k2eAgInPc1d1
tlumiče	tlumič	k1gInPc1
kvůli	kvůli	k7c3
bezpečnosti	bezpečnost	k1gFnSc3
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Stroj	stroj	k1gInSc1
času	čas	k1gInSc2
</s>
<s>
Stroj	stroj	k1gInSc1
času	čas	k1gInSc2
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
vylepšený	vylepšený	k2eAgInSc1d1
Jumper	jumper	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledově	vzhledově	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
normálního	normální	k2eAgInSc2d1
Jumperu	jumper	k1gInSc2
moc	moc	k6eAd1
neliší	lišit	k5eNaImIp3nP
–	–	k?
má	mít	k5eAaImIp3nS
jiný	jiný	k2eAgInSc4d1
řídící	řídící	k2eAgInSc4d1
panel	panel	k1gInSc4
a	a	k8xC
uprostřed	uprostřed	k6eAd1
mezi	mezi	k7c7
sedadly	sedadlo	k1gNnPc7
pro	pro	k7c4
pasažéry	pasažér	k1gMnPc4
leží	ležet	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
oválná	oválný	k2eAgFnSc1d1
trubice	trubice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
přesně	přesně	k6eAd1
tento	tento	k3xDgInSc4
vynález	vynález	k1gInSc4
funguje	fungovat	k5eAaImIp3nS
nikdo	nikdo	k3yNnSc1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
Sheppard	Sheppard	k1gMnSc1
zmáčknul	zmáčknout	k5eAaPmAgMnS
"	"	kIx"
<g/>
nějaké	nějaký	k3yIgNnSc4
<g/>
"	"	kIx"
tlačítko	tlačítko	k1gNnSc4
<g/>
,	,	kIx,
přesunul	přesunout	k5eAaPmAgInS
se	se	k3xPyFc4
stroj	stroj	k1gInSc1
času	čas	k1gInSc2
10	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
do	do	k7c2
minulosti	minulost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
okamžitě	okamžitě	k6eAd1
sestřelili	sestřelit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynalezl	vynaleznout	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
antický	antický	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
Janus	Janus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janus	Janus	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nejspíš	nejspíš	k9
sestrojil	sestrojit	k5eAaPmAgMnS
ještě	ještě	k9
jeden	jeden	k4xCgInSc4
stroj	stroj	k1gInSc4
času	čas	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
SG-1	SG-1	k1gFnPc4
na	na	k7c6
planetě	planeta	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Maybourne	Maybourn	k1gInSc5
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
za	za	k7c4
krále	král	k1gMnSc4
<g/>
,	,	kIx,
našli	najít	k5eAaPmAgMnP
zde	zde	k6eAd1
stroj	stroj	k1gInSc4
času	čas	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
poté	poté	k6eAd1
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Ra	ra	k0
vládl	vládnout	k5eAaImAgInS
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
získali	získat	k5eAaPmAgMnP
ZPM	ZPM	kA
<g/>
.	.	kIx.
</s>
<s>
Destiny	Destina	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Destiny	Destina	k1gFnSc2
(	(	kIx(
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Destiny	Destina	k1gFnPc4
je	být	k5eAaImIp3nS
antická	antický	k2eAgFnSc1d1
průzkumná	průzkumný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
vyslaná	vyslaný	k2eAgFnSc1d1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
cestu	cesta	k1gFnSc4
před	před	k7c7
miliony	milion	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antikové	Antik	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
objevovat	objevovat	k5eAaImF
nové	nový	k2eAgFnPc4d1
galaxie	galaxie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
by	by	kYmCp3nS
jim	on	k3xPp3gMnPc3
trvala	trvat	k5eAaImAgFnS
cesta	cesta	k1gFnSc1
na	na	k7c4
Destiny	Destina	k1gFnPc4
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
vyslali	vyslat	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
samotnou	samotný	k2eAgFnSc4d1
a	a	k8xC
plánovali	plánovat	k5eAaImAgMnP
na	na	k7c4
ni	on	k3xPp3gFnSc4
přijít	přijít	k5eAaPmF
Hvězdnou	hvězdný	k2eAgFnSc7d1
branou	brána	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
neuskutečnilo	uskutečnit	k5eNaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
Antikové	Antik	k1gMnPc1
dříve	dříve	k6eAd2
povznesli	povznést	k5eAaPmAgMnP
nebo	nebo	k8xC
zemřeli	zemřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Atlantida	Atlantida	k1gFnSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Atlantida	Atlantida	k1gFnSc1
(	(	kIx(
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Atlantida	Atlantida	k1gFnSc1
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgNnSc4d1
město	město	k1gNnSc4
v	v	k7c6
televizních	televizní	k2eAgInPc6d1
seriálech	seriál	k1gInPc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
a	a	k8xC
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
ztracené	ztracený	k2eAgNnSc4d1
město	město	k1gNnSc4
Antiků	Antik	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
původně	původně	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
na	na	k7c6
Zemi	zem	k1gFnSc6
a	a	k8xC
bylo	být	k5eAaImAgNnS
poslední	poslední	k2eAgNnSc1d1
z	z	k7c2
měst	město	k1gNnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnPc4
Antikové	Antik	k1gMnPc1
postavili	postavit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
pěti	pět	k4xCc7
až	až	k8xS
deseti	deset	k4xCc7
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
se	se	k3xPyFc4
však	však	k9
Antikové	Antik	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
kvůli	kvůli	k7c3
epidemii	epidemie	k1gFnSc3
Zemi	zem	k1gFnSc4
opustit	opustit	k5eAaPmF
a	a	k8xC
s	s	k7c7
městem	město	k1gNnSc7
odletěli	odletět	k5eAaPmAgMnP
do	do	k7c2
galaxie	galaxie	k1gFnSc2
Pegasus	Pegasus	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tam	tam	k6eAd1
začali	začít	k5eAaPmAgMnP
budovat	budovat	k5eAaImF
nové	nový	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
mezihvězdným	mezihvězdný	k2eAgNnSc7d1
plavidlem	plavidlo	k1gNnSc7
a	a	k8xC
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
má	mít	k5eAaImIp3nS
motory	motor	k1gInPc4
pro	pro	k7c4
cestování	cestování	k1gNnSc4
hyperprostorem	hyperprostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
napájeno	napájet	k5eAaImNgNnS
třemi	tři	k4xCgNnPc7
ZPM	ZPM	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
potřebné	potřebný	k2eAgInPc1d1
pro	pro	k7c4
napájení	napájení	k1gNnSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
systémů	systém	k1gInPc2
města	město	k1gNnSc2
(	(	kIx(
<g/>
energetické	energetický	k2eAgInPc4d1
štíty	štít	k1gInPc4
<g/>
,	,	kIx,
obranné	obranný	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
hyperpohon	hyperpohon	k1gInSc4
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
galaxii	galaxie	k1gFnSc6
Pegasus	Pegasus	k1gMnSc1
Antikové	Antikový	k2eAgFnSc6d1
vedli	vést	k5eAaImAgMnP
válku	válka	k1gFnSc4
s	s	k7c7
Wraighty	Wraighto	k1gNnPc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
díky	díky	k7c3
jejich	jejich	k3xOp3gFnSc3
početní	početní	k2eAgFnSc3d1
převaze	převaha	k1gFnSc3
prohráli	prohrát	k5eAaPmAgMnP
<g/>
,	,	kIx,
potopili	potopit	k5eAaPmAgMnP
město	město	k1gNnSc4
do	do	k7c2
moře	moře	k1gNnSc2
aby	aby	kYmCp3nP
zmírnili	zmírnit	k5eAaPmAgMnP
účinek	účinek	k1gInSc4
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
expedice	expedice	k1gFnSc1
doktorky	doktorka	k1gFnSc2
Weirové	Weirová	k1gFnSc2
<g/>
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Carterové	Carterové	k2eAgInSc1d1
a	a	k8xC
poté	poté	k6eAd1
pana	pan	k1gMnSc4
Woosleyho	Woosley	k1gMnSc4
<g/>
)	)	kIx)
Atlantis	Atlantis	k1gFnSc1
našla	najít	k5eAaPmAgFnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
stále	stále	k6eAd1
pod	pod	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInSc7
příchodem	příchod	k1gInSc7
město	město	k1gNnSc1
probudilo	probudit	k5eAaPmAgNnS
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zatížení	zatížení	k1gNnSc3
již	již	k6eAd1
tak	tak	k6eAd1
vybitého	vybitý	k2eAgMnSc2d1
ZPM	ZPM	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
pod	pod	k7c4
kritickou	kritický	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
a	a	k8xC
město	město	k1gNnSc4
se	se	k3xPyFc4
vynořilo	vynořit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
vynořením	vynoření	k1gNnSc7
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
spotřebě	spotřeba	k1gFnSc3
veškeré	veškerý	k3xTgFnSc2
zbylé	zbylý	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
běžný	běžný	k2eAgInSc4d1
chod	chod	k1gInSc4
města	město	k1gNnSc2
dále	daleko	k6eAd2
zásoben	zásobna	k1gFnPc2
energií	energie	k1gFnPc2
z	z	k7c2
naquadahových	naquadahový	k2eAgInPc2d1
generátorů	generátor	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
však	však	k9
nestačila	stačit	k5eNaBmAgFnS
pro	pro	k7c4
obranu	obrana	k1gFnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
ochraně	ochrana	k1gFnSc3
města	město	k1gNnSc2
slouží	sloužit	k5eAaImIp3nS
energetický	energetický	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
napájen	napájen	k2eAgInSc1d1
ZPM	ZPM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štít	štít	k1gInSc1
dokáže	dokázat	k5eAaPmIp3nS
odolat	odolat	k5eAaPmF
jakémukoliv	jakýkoliv	k3yIgInSc3
útoku	útok	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
doba	doba	k1gFnSc1
po	po	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vztyčený	vztyčený	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
úměrná	úměrný	k2eAgNnPc4d1
množství	množství	k1gNnPc4
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
ZPM	ZPM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
zapojena	zapojit	k5eAaPmNgFnS
maximálně	maximálně	k6eAd1
tři	tři	k4xCgInPc4
ZPM	ZPM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obranný	obranný	k2eAgInSc1d1
systém	systém	k1gInSc1
má	mít	k5eAaImIp3nS
město	město	k1gNnSc1
podobný	podobný	k2eAgInSc4d1
<g/>
,	,	kIx,
jaký	jaký	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
na	na	k7c6
základně	základna	k1gFnSc6
Antiků	Antik	k1gInPc2
na	na	k7c6
Antarktidě	Antarktida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
kontrolního	kontrolní	k2eAgNnSc2d1
křesla	křeslo	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
pomocí	pomoc	k1gFnSc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vystřelovány	vystřelován	k2eAgInPc4d1
drony	dron	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3
částí	část	k1gFnSc7
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
ústřední	ústřední	k2eAgFnSc1d1
část	část	k1gFnSc1
s	s	k7c7
hlavní	hlavní	k2eAgFnSc7d1
věží	věž	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
řídící	řídící	k2eAgFnSc1d1
místnost	místnost	k1gFnSc1
s	s	k7c7
bránou	brána	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
města	město	k1gNnSc2
se	se	k3xPyFc4
rovněž	rovněž	k9
nachází	nacházet	k5eAaImIp3nS
hangár	hangár	k1gInSc1
pro	pro	k7c4
puddle	puddle	k6eAd1
jumpery	jumper	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
malé	malý	k2eAgFnPc1d1
antické	antický	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
schopné	schopný	k2eAgFnPc1d1
cestovat	cestovat	k5eAaImF
hvězdnou	hvězdný	k2eAgFnSc7d1
bránou	brána	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hangáru	hangár	k1gInSc2
lze	lze	k6eAd1
tunelem	tunel	k1gInSc7
vyletět	vyletět	k5eAaPmF
vrchní	vrchní	k1gFnSc4
částí	část	k1gFnPc2
z	z	k7c2
města	město	k1gNnSc2
nebo	nebo	k8xC
se	se	k3xPyFc4
lze	lze	k6eAd1
dostat	dostat	k5eAaPmF
do	do	k7c2
kontrolní	kontrolní	k2eAgFnSc2d1
místnosti	místnost	k1gFnSc2
s	s	k7c7
bránou	brána	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
Brána	brána	k1gFnSc1
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c6
Mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c4
PegasuHvězdná	PegasuHvězdný	k2eAgNnPc4d1
brána	brán	k2eAgNnPc4d1
na	na	k7c4
Destiny	Destin	k2eAgInPc4d1
a	a	k8xC
vzdálených	vzdálený	k2eAgFnPc6d1
galaxiích	galaxie	k1gFnPc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
(	(	kIx(
<g/>
zařízení	zařízení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c6
seriálu	seriál	k1gInSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
stabilní	stabilní	k2eAgFnSc4d1
umělou	umělý	k2eAgFnSc4d1
červí	červí	k2eAgFnSc4d1
díru	díra	k1gFnSc4
(	(	kIx(
<g/>
Einstein-Rosenův	Einstein-Rosenův	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
)	)	kIx)
po	po	k7c6
zadání	zadání	k1gNnSc6
nejméně	málo	k6eAd3
sedmi	sedm	k4xCc2
symbolů	symbol	k1gInPc2
pomocí	pomocí	k7c2
zařízení	zařízení	k1gNnSc2
zvaného	zvaný	k2eAgInSc2d1
DHD	DHD	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bývalo	bývat	k5eAaImAgNnS
v	v	k7c6
seriálu	seriál	k1gInSc6
interpretováno	interpretován	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
ovládací	ovládací	k2eAgInSc1d1
panel	panel	k1gInSc1
kruhové	kruhový	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
osazené	osazený	k2eAgFnSc2d1
devětatřiceti	devětatřicet	k4xCc2
tlačítky	tlačítko	k1gNnPc7
zasazených	zasazený	k2eAgFnPc2d1
ve	v	k7c6
třech	tři	k4xCgInPc6
kruzích	kruh	k1gInPc6
rovnoměrně	rovnoměrně	k6eAd1
umístěných	umístěný	k2eAgMnPc2d1
od	od	k7c2
středu	střed	k1gInSc2
panelu	panel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
zadávaných	zadávaný	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
určuje	určovat	k5eAaImIp3nS
adresu	adresa	k1gFnSc4
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgInPc4
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
neboli	neboli	k8xC
adresu	adresa	k1gFnSc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
vypočítat	vypočítat	k5eAaPmF
pomocí	pomocí	k7c2
planetárního	planetární	k2eAgInSc2d1
posunu	posun	k1gInSc2
okolo	okolo	k7c2
cílové	cílový	k2eAgFnSc2d1
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
průsečíku	průsečík	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmý	sedmý	k4xOgInSc1
symbol	symbol	k1gInSc1
označuje	označovat	k5eAaImIp3nS
výchozí	výchozí	k2eAgFnSc4d1
planetu	planeta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bráně	brána	k1gFnSc6
lze	lze	k6eAd1
však	však	k9
zadat	zadat	k5eAaPmF
i	i	k9
osmý	osmý	k4xOgInSc4
symbol	symbol	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
označuje	označovat	k5eAaImIp3nS
galaxii	galaxie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devátý	devátý	k4xOgInSc1
symbol	symbol	k1gInSc1
vede	vést	k5eAaImIp3nS
na	na	k7c4
antickou	antický	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Destiny	Destina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
tedy	tedy	k9
umožňuje	umožňovat	k5eAaImIp3nS
cestování	cestování	k1gNnSc1
mezi	mezi	k7c7
planetami	planeta	k1gFnPc7
a	a	k8xC
galaxiemi	galaxie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
přenos	přenos	k1gInSc1
zjednodušeně	zjednodušeně	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
předmět	předmět	k1gInSc1
či	či	k8xC
organismus	organismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
překročí	překročit	k5eAaPmIp3nS
horizont	horizont	k1gInSc1
událostí	událost	k1gFnPc2
(	(	kIx(
<g/>
vstoupí	vstoupit	k5eAaPmIp3nP
do	do	k7c2
červí	červí	k2eAgFnSc2d1
díry	díra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
molekulárně	molekulárně	k6eAd1
rozložen	rozložen	k2eAgInSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
u	u	k7c2
živého	živý	k2eAgInSc2d1
organismu	organismus	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
těla	tělo	k1gNnSc2
odděleno	oddělen	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
a	a	k8xC
posláno	poslán	k2eAgNnSc1d1
na	na	k7c4
zadanou	zadaný	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
do	do	k7c2
cílové	cílový	k2eAgFnSc2d1
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
znovu	znovu	k6eAd1
předmět	předmět	k1gInSc1
či	či	k8xC
organismus	organismus	k1gInSc1
zkompletován	zkompletován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
rozložené	rozložený	k2eAgInPc1d1
organizmy	organizmus	k1gInPc1
či	či	k8xC
předměty	předmět	k1gInPc1
nejsou	být	k5eNaImIp3nP
po	po	k7c4
dobu	doba	k1gFnSc4
pobytu	pobyt	k1gInSc2
v	v	k7c6
horizontu	horizont	k1gInSc6
událostí	událost	k1gFnPc2
(	(	kIx(
<g/>
jež	jenž	k3xRgNnSc1
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nS
se	s	k7c7
vzdáleností	vzdálenost	k1gFnSc7
obou	dva	k4xCgFnPc2
planet	planeta	k1gFnPc2
<g/>
)	)	kIx)
vystaveny	vystaven	k2eAgFnPc1d1
vlivu	vliv	k1gInSc2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pomocí	pomocí	k7c2
brány	brána	k1gFnSc2
mohou	moct	k5eAaImIp3nP
předměty	předmět	k1gInPc4
či	či	k8xC
organismy	organismus	k1gInPc4
cestovat	cestovat	k5eAaImF
jen	jen	k6eAd1
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
a	a	k8xC
to	ten	k3xDgNnSc1
od	od	k7c2
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
byla	být	k5eAaImAgFnS
červí	červí	k2eAgFnSc1d1
díra	díra	k1gFnSc1
vytvořena	vytvořen	k2eAgFnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
cílové	cílový	k2eAgFnSc3d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
různé	různý	k2eAgInPc4d1
signály	signál	k1gInPc4
či	či	k8xC
impulsy	impuls	k1gInPc4
mohou	moct	k5eAaImIp3nP
cestovat	cestovat	k5eAaImF
obousměrně	obousměrně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
vytvoření	vytvoření	k1gNnSc1
červí	červí	k2eAgFnSc2d1
díry	díra	k1gFnSc2
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
energeticky	energeticky	k6eAd1
náročné	náročný	k2eAgNnSc1d1
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
za	za	k7c2
normálních	normální	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
daří	dařit	k5eAaImIp3nS
udržet	udržet	k5eAaPmF
takovouto	takovýto	k3xDgFnSc4
červí	červí	k2eAgFnSc4d1
díru	díra	k1gFnSc4
otevřenou	otevřený	k2eAgFnSc4d1
po	po	k7c4
dobu	doba	k1gFnSc4
osmatřiceti	osmatřicet	k4xCc2
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brána	brána	k1gFnSc1
byla	být	k5eAaImAgFnS
sestrojena	sestrojit	k5eAaPmNgFnS
přibližně	přibližně	k6eAd1
před	před	k7c7
deseti	deset	k4xCc7
až	až	k8xS
jedenácti	jedenáct	k4xCc7
miliony	milion	k4xCgInPc7
lety	léto	k1gNnPc7
rasou	rasa	k1gFnSc7
zvanou	zvaný	k2eAgFnSc4d1
Antikové	Antikový	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bran	brána	k1gFnPc2
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
typů	typ	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
seriálech	seriál	k1gInPc6
byly	být	k5eAaImAgFnP
zatím	zatím	k6eAd1
představeny	představit	k5eAaPmNgFnP
jen	jen	k9
tři	tři	k4xCgFnPc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
brána	brána	k1gFnSc1
z	z	k7c2
naší	náš	k3xOp1gFnSc2
galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
vytvořena	vytvořen	k2eAgFnSc1d1
z	z	k7c2
vysoce	vysoce	k6eAd1
vodivého	vodivý	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
zvaného	zvaný	k2eAgInSc2d1
naquadah	naquadah	k1gMnSc1
vážící	vážící	k2eAgMnSc1d1
přibližně	přibližně	k6eAd1
30	#num#	k4
tun	tuna	k1gFnPc2
<g/>
,	,	kIx,
brána	brána	k1gFnSc1
z	z	k7c2
galaxie	galaxie	k1gFnSc2
Pegasus	Pegasus	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
zatím	zatím	k6eAd1
mnoho	mnoho	k4c4
podrobností	podrobnost	k1gFnPc2
neví	vědět	k5eNaImIp3nS
a	a	k8xC
brána	brána	k1gFnSc1
na	na	k7c4
Destiny	Destina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
brány	brána	k1gFnPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
kompatibilní	kompatibilní	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brány	brána	k1gFnPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
skoro	skoro	k6eAd1
na	na	k7c6
všech	všecek	k3xTgFnPc6
planetách	planeta	k1gFnPc6
v	v	k7c6
obou	dva	k4xCgFnPc6
galaxiích	galaxie	k1gFnPc6
(	(	kIx(
<g/>
Mléčná	mléčný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
a	a	k8xC
Pegasus	Pegasus	k1gMnSc1
<g/>
)	)	kIx)
nezávisle	závisle	k6eNd1
na	na	k7c6
obyvatelnosti	obyvatelnost	k1gFnSc6
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
typ	typ	k1gInSc1
bran	brána	k1gFnPc2
je	být	k5eAaImIp3nS
rozmístěn	rozmístit	k5eAaPmNgInS
pomocí	pomoc	k1gFnSc7
konstrukčních	konstrukční	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
vyslaných	vyslaný	k2eAgFnPc2d1
před	před	k7c7
Destiny	Destin	k1gInPc7
do	do	k7c2
vzdáleného	vzdálený	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
bran	brána	k1gFnPc2
</s>
<s>
Doposud	doposud	k6eAd1
bylo	být	k5eAaImAgNnS
nalezeny	nalezen	k2eAgInPc4d1
tři	tři	k4xCgInPc4
druhy	druh	k1gInPc4
bran	brána	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Brána	brána	k1gFnSc1
z	z	k7c2
Mléčné	mléčný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
–	–	k?
má	mít	k5eAaImIp3nS
vnitřní	vnitřní	k2eAgInSc1d1
otočný	otočný	k2eAgInSc1d1
kruh	kruh	k1gInSc1
<g/>
,	,	kIx,
lze	lze	k6eAd1
manuálně	manuálně	k6eAd1
vytáčet	vytáčet	k5eAaImF
<g/>
,	,	kIx,
červené	červený	k2eAgInPc4d1
zámky	zámek	k1gInPc4
a	a	k8xC
krystal	krystal	k1gInSc1
na	na	k7c4
DHD	DHD	kA
</s>
<s>
Brána	brána	k1gFnSc1
z	z	k7c2
galaxie	galaxie	k1gFnSc2
Pegasus	Pegasus	k1gMnSc1
–	–	k?
nejmodernější	moderní	k2eAgInSc1d3
typ	typ	k1gInSc1
brány	brána	k1gFnPc1
<g/>
,	,	kIx,
modré	modrý	k2eAgInPc1d1
zámky	zámek	k1gInPc1
a	a	k8xC
krystal	krystal	k1gInSc1
na	na	k7c4
DHD	DHD	kA
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
vnitřní	vnitřní	k2eAgInSc1d1
otočný	otočný	k2eAgInSc1d1
kruh	kruh	k1gInSc1
</s>
<s>
Brána	brána	k1gFnSc1
na	na	k7c4
Destiny	Destina	k1gFnPc4
a	a	k8xC
vzdálených	vzdálený	k2eAgFnPc6d1
galaxiích	galaxie	k1gFnPc6
–	–	k?
nejstarší	starý	k2eAgInSc1d3
druh	druh	k1gInSc1
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
omezená	omezený	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
spojení	spojení	k1gNnSc2
s	s	k7c7
další	další	k2eAgFnSc7d1
bránou	brána	k1gFnSc7
<g/>
,	,	kIx,
odlišná	odlišný	k2eAgFnSc1d1
od	od	k7c2
předchozích	předchozí	k2eAgFnPc2d1
<g/>
,	,	kIx,
při	při	k7c6
zadávání	zadávání	k1gNnSc6
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
,	,	kIx,
odlišné	odlišný	k2eAgInPc4d1
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
DHD	DHD	kA
zadává	zadávat	k5eAaImIp3nS
se	se	k3xPyFc4
pomocí	pomocí	k7c2
dálkového	dálkový	k2eAgNnSc2d1
ovládání	ovládání	k1gNnSc2
"	"	kIx"
<g/>
KINA	kino	k1gNnSc2
<g/>
"	"	kIx"
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c6
novém	nový	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgInSc1d1
kruh	kruh	k1gInSc1
není	být	k5eNaImIp3nS
otočný	otočný	k2eAgInSc1d1
<g/>
,	,	kIx,
zadávání	zadávání	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
jako	jako	k9
"	"	kIx"
<g/>
problikávání	problikávání	k1gNnSc1
<g/>
"	"	kIx"
jednotlivých	jednotlivý	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámky	zámek	k1gInPc1
také	také	k9
nezapadávají	zapadávat	k5eNaImIp3nP
<g/>
,	,	kIx,
jenom	jenom	k9
pod	pod	k7c7
nimi	on	k3xPp3gMnPc7
zůstanou	zůstat	k5eAaPmIp3nP
svítit	svítit	k5eAaImF
příslušné	příslušný	k2eAgInPc4d1
symboly	symbol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Červí	červí	k2eAgFnSc1d1
díra	díra	k1gFnSc1
rovněž	rovněž	k9
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
jinak	jinak	k6eAd1
–	–	k?
jako	jako	k8xS,k8xC
zelený	zelený	k2eAgInSc1d1
tunel	tunel	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
řadě	řada	k1gFnSc6
SG-1	SG-1	k1gFnPc2
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
cestování	cestování	k1gNnSc1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
SGA	SGA	kA
<g/>
,	,	kIx,
akorát	akorát	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
modrý	modrý	k2eAgInSc4d1
tunel	tunel	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
rasy	rasa	k1gFnPc4
<g/>
,	,	kIx,
i	i	k9
ty	ten	k3xDgInPc1
primitivní	primitivní	k2eAgInPc4d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
vědí	vědět	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
bránu	brána	k1gFnSc4
používat	používat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
světy	svět	k1gInPc1
spolu	spolu	k6eAd1
běžně	běžně	k6eAd1
obchodují	obchodovat	k5eAaImIp3nP
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
s	s	k7c7
jídlem	jídlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
lze	lze	k6eAd1
cestovat	cestovat	k5eAaImF
z	z	k7c2
Pegasu	Pegas	k1gMnSc3
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
na	na	k7c6
Atlantidě	Atlantida	k1gFnSc6
(	(	kIx(
<g/>
díky	díky	k7c3
ZPM	ZPM	kA
a	a	k8xC
speciálnímu	speciální	k2eAgInSc3d1
krystalu	krystal	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mini	mini	k2eAgFnSc1d1
Hvězdná	hvězdný	k2eAgFnSc1d1
Brána	brána	k1gFnSc1
(	(	kIx(
<g/>
Mini	mini	k2eAgFnSc1d1
StarGate	StarGat	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Mini	mini	k2eAgFnSc1d1
Stargate	Stargat	k1gMnSc5
</s>
<s>
Mini	mini	k2eAgFnSc1d1
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc1d2
a	a	k8xC
primitivnější	primitivní	k2eAgFnSc1d2
verze	verze	k1gFnSc1
Hvězdné	hvězdný	k2eAgFnSc2d1
brány	brána	k1gFnSc2
sestavené	sestavený	k2eAgFnPc1d1
pouze	pouze	k6eAd1
z	z	k7c2
pozemských	pozemský	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
45,36	45,36	k4
kg	kg	kA
čistého	čistý	k2eAgInSc2d1
titanu	titan	k1gInSc2
<g/>
,	,	kIx,
200	#num#	k4
metrů	metr	k1gInPc2
optických	optický	k2eAgInPc2d1
kabelů	kabel	k1gInPc2
<g/>
,	,	kIx,
sedm	sedm	k4xCc4
100	#num#	k4
kilowattových	kilowattový	k2eAgInPc2d1
kondenzátorů	kondenzátor	k1gInPc2
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
a	a	k8xC
toustovač	toustovač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
asi	asi	k9
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
v	v	k7c6
průměru	průměr	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
vešly	vejít	k5eAaPmAgFnP
jeden	jeden	k4xCgMnSc1
nebo	nebo	k8xC
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
schopná	schopný	k2eAgNnPc4d1
připojení	připojení	k1gNnPc4
k	k	k7c3
běžné	běžný	k2eAgFnSc3d1
Hvězdné	hvězdný	k2eAgFnSc3d1
bráně	brána	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
vytočit	vytočit	k5eAaPmF
jen	jen	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
vyhoří	vyhořet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
postrádá	postrádat	k5eAaImIp3nS
běžné	běžný	k2eAgNnSc4d1
DHD	DHD	kA
jaké	jaký	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
je	být	k5eAaImIp3nS
u	u	k7c2
normální	normální	k2eAgFnSc2d1
Hvězdné	hvězdný	k2eAgFnSc2d1
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
Mini	mini	k2eAgFnSc1d1
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
speciálně	speciálně	k6eAd1
pro	pro	k7c4
vytáčení	vytáčení	k1gNnSc4
planety	planeta	k1gFnSc2
Velona	Velona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
DHD	DHD	kA
</s>
<s>
Schematické	schematický	k2eAgNnSc4d1
znázornění	znázornění	k1gNnSc4
ovládacího	ovládací	k2eAgInSc2d1
panelu	panel	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
(	(	kIx(
<g/>
zařízení	zařízení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
#	#	kIx~
<g/>
Ovládací	ovládací	k2eAgInSc1d1
panel	panel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
DHD	DHD	kA
–	–	k?
anglicky	anglicky	k6eAd1
Dial	Dial	k1gInSc4
Home	Hom	k1gFnSc2
Device	device	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
ovládací	ovládací	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
"	"	kIx"
<g/>
vytáčení	vytáčení	k1gNnSc3
adresy	adresa	k1gFnSc2
<g/>
"	"	kIx"
na	na	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Transportní	transportní	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
</s>
<s>
Transportní	transportní	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
jsou	být	k5eAaImIp3nP
zařízení	zařízení	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yIgNnPc4,k3yRgNnPc4
jsme	být	k5eAaImIp1nP
mohli	moct	k5eAaImAgMnP
poprvé	poprvé	k6eAd1
spatřit	spatřit	k5eAaPmF
již	již	k9
v	v	k7c6
původním	původní	k2eAgInSc6d1
filmu	film	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
technologii	technologie	k1gFnSc4
rozkládající	rozkládající	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
na	na	k7c4
atomy	atom	k1gInPc4
a	a	k8xC
pomocí	pomocí	k7c2
proudu	proud	k1gInSc2
hmoty	hmota	k1gFnSc2
jej	on	k3xPp3gMnSc4
přenese	přenést	k5eAaPmIp3nS
ke	k	k7c3
kruhům	kruh	k1gInPc3
druhým	druhý	k4xOgNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
daný	daný	k2eAgInSc1d1
objekt	objekt	k1gInSc1
(	(	kIx(
<g/>
věci	věc	k1gFnPc1
<g/>
,	,	kIx,
osoby	osoba	k1gFnPc1
<g/>
)	)	kIx)
re-materializuje	re-materializovat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kruhy	kruh	k1gInPc7
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
například	například	k6eAd1
nastavit	nastavit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vyhledali	vyhledat	k5eAaPmAgMnP
automaticky	automaticky	k6eAd1
nejbližší	blízký	k2eAgInPc4d3
kruhy	kruh	k1gInPc4
<g/>
,	,	kIx,
či	či	k8xC
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
při	při	k7c6
transportu	transport	k1gInSc6
proud	proud	k1gInSc1
hmoty	hmota	k1gFnSc2
zachytit	zachytit	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
do	do	k7c2
proudu	proud	k1gInSc2
hmoty	hmota	k1gFnSc2
naletí	naletět	k5eAaPmIp3nS
například	například	k6eAd1
s	s	k7c7
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
taktéž	taktéž	k?
má	mít	k5eAaImIp3nS
na	na	k7c6
palubě	paluba	k1gFnSc6
kruhovou	kruhový	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Archiv	archiv	k1gInSc1
Antiků	Antik	k1gMnPc2
</s>
<s>
Archiv	archiv	k1gInSc1
Antiků	Antik	k1gInPc2
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc4
uchovávající	uchovávající	k2eAgInSc1d1
archiv	archiv	k1gInSc1
veškerých	veškerý	k3xTgFnPc2
antických	antický	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zařízení	zařízení	k1gNnSc1
nahrálo	nahrát	k5eAaPmAgNnS,k5eAaBmAgNnS
do	do	k7c2
mozku	mozek	k1gInSc2
Jackovi	Jackův	k2eAgMnPc1d1
O	O	kA
<g/>
'	'	kIx"
<g/>
Neillovi	Neill	k1gMnSc3
všechny	všechen	k3xTgFnPc1
vědomosti	vědomost	k1gFnPc1
Antiků	Antik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jack	Jack	k1gInSc1
zadal	zadat	k5eAaPmAgInS
do	do	k7c2
počítače	počítač	k1gInSc2
SGC	SGC	kA
všechny	všechen	k3xTgFnPc4
brány	brána	k1gFnPc1
v	v	k7c6
galaxii	galaxie	k1gFnSc6
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
mluvit	mluvit	k5eAaImF
Anticky	anticky	k6eAd1
a	a	k8xC
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
vzorec	vzorec	k1gInSc4
pro	pro	k7c4
vypočítání	vypočítání	k1gNnSc4
vzdálenosti	vzdálenost	k1gFnSc2
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
generátor	generátor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dodal	dodat	k5eAaPmAgInS
bráně	brána	k1gFnSc3
10	#num#	k4
<g/>
x	x	k?
více	hodně	k6eAd2
energie	energie	k1gFnSc2
a	a	k8xC
odešel	odejít	k5eAaPmAgMnS
za	za	k7c4
Asgardy	Asgarda	k1gFnPc4
do	do	k7c2
jejich	jejich	k3xOp3gFnSc2
galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jej	on	k3xPp3gMnSc4
zbavili	zbavit	k5eAaPmAgMnP
antických	antický	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ovládací	ovládací	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
Obdélníkový	obdélníkový	k2eAgInSc1d1
panel	panel	k1gInSc1
s	s	k7c7
několika	několik	k4yIc7
krystaly	krystal	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
konzole	konzola	k1gFnSc6
je	být	k5eAaImIp3nS
přiřazena	přiřazen	k2eAgFnSc1d1
k	k	k7c3
jinému	jiný	k2eAgInSc3d1
systému	systém	k1gInSc3
(	(	kIx(
<g/>
podpora	podpora	k1gFnSc1
života	život	k1gInSc2
<g/>
,	,	kIx,
štíty	štít	k1gInPc1
<g/>
,	,	kIx,
brána	brána	k1gFnSc1
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
jí	jíst	k5eAaImIp3nS
aktivovat	aktivovat	k5eAaBmF
antickým	antický	k2eAgInSc7d1
genem	gen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
konzole	konzola	k1gFnSc6
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgInP
na	na	k7c6
Atlantidě	Atlantida	k1gFnSc6
<g/>
,	,	kIx,
antických	antický	k2eAgFnPc6d1
základnách	základna	k1gFnPc6
<g/>
...	...	k?
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
jakoby	jakoby	k8xS
lidské	lidský	k2eAgInPc4d1
počítače	počítač	k1gInPc4
s	s	k7c7
klávesnicí	klávesnice	k1gFnSc7
bez	bez	k7c2
monitorů	monitor	k1gInPc2
(	(	kIx(
<g/>
lze	lze	k6eAd1
k	k	k7c3
nim	on	k3xPp3gMnPc3
připojit	připojit	k5eAaPmF
obrazovku	obrazovka	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expedice	expedice	k1gFnSc1
Atlantis	Atlantis	k1gFnSc1
používá	používat	k5eAaImIp3nS
k	k	k7c3
těmto	tento	k3xDgInPc3
konzolím	konzolit	k5eAaImIp1nS,k5eAaPmIp1nS
pozemské	pozemský	k2eAgInPc4d1
notebooky	notebook	k1gInPc4
(	(	kIx(
<g/>
důvod	důvod	k1gInSc4
neznámý	známý	k2eNgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kontrolní	kontrolní	k2eAgNnSc1d1
křeslo	křeslo	k1gNnSc1
</s>
<s>
Křeslo	křeslo	k1gNnSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
ovládací	ovládací	k2eAgInSc1d1
terminál	terminál	k1gInSc1
k	k	k7c3
některým	některý	k3yIgInPc3
systémům	systém	k1gInPc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
odpalovačů	odpalovač	k1gMnPc2
Dronů	Dron	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
fungovalo	fungovat	k5eAaImAgNnS
<g/>
,	,	kIx,
potřebuje	potřebovat	k5eAaImIp3nS
silný	silný	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládá	ovládat	k5eAaImIp3nS
se	se	k3xPyFc4
myšlenkami	myšlenka	k1gFnPc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
abyste	aby	kYmCp2nP
ho	on	k3xPp3gMnSc4
dokázali	dokázat	k5eAaPmAgMnP
vůbec	vůbec	k9
zapnout	zapnout	k5eAaPmF
<g/>
,	,	kIx,
musíte	muset	k5eAaImIp2nP
mít	mít	k5eAaImF
antický	antický	k2eAgInSc4d1
gen.	gen.	kA
</s>
<s>
Detektor	detektor	k1gInSc1
známek	známka	k1gFnPc2
života	život	k1gInSc2
</s>
<s>
Toto	tento	k3xDgNnSc1
jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
zavádějící	zavádějící	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
tento	tento	k3xDgInSc1
přístroj	přístroj	k1gInSc1
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c4
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
když	když	k8xS
byl	být	k5eAaImAgInS
lidmi	člověk	k1gMnPc7
poprvé	poprvé	k6eAd1
objeven	objeven	k2eAgInSc1d1
<g/>
,	,	kIx,
zastával	zastávat	k5eAaImAgMnS
funkci	funkce	k1gFnSc4
právě	právě	k6eAd1
detektoru	detektor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
takovým	takový	k3xDgInPc3
"	"	kIx"
<g/>
antickým	antický	k2eAgInSc7d1
PDA	PDA	kA
<g/>
"	"	kIx"
a	a	k8xC
dokáže	dokázat	k5eAaPmIp3nS
objevit	objevit	k5eAaPmF
například	například	k6eAd1
také	také	k9
zdroje	zdroj	k1gInPc1
energie	energie	k1gFnSc2
či	či	k8xC
antický	antický	k2eAgInSc1d1
gen	gen	k1gInSc1
v	v	k7c6
lidském	lidský	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
ZPM	ZPM	kA
</s>
<s>
ZPM	ZPM	kA
(	(	kIx(
<g/>
Zero	Zero	k1gInSc1
Point	pointa	k1gInSc1
Module	modul	k1gInSc1
–	–	kIx~
modul	modul	k1gInSc1
nulového	nulový	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZPM	ZPM	kA
čerpá	čerpat	k5eAaImIp3nS
energii	energie	k1gFnSc4
z	z	k7c2
vakuové	vakuový	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
čerpá	čerpat	k5eAaImIp3nS
ze	z	k7c2
subprostoru	subprostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SG-1	SG-1	k1gFnSc1
našla	najít	k5eAaPmAgFnS
několik	několik	k4yIc4
ZPM	ZPM	kA
<g/>
,	,	kIx,
ale	ale	k8xC
většina	většina	k1gFnSc1
jich	on	k3xPp3gInPc2
už	už	k6eAd1
byla	být	k5eAaImAgFnS
vybitá	vybitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
sedmé	sedmý	k4xOgFnSc2
řady	řada	k1gFnSc2
našla	najít	k5eAaPmAgFnS
SG-1	SG-1	k1gFnSc1
ZPM	ZPM	kA
na	na	k7c6
planetě	planeta	k1gFnSc6
Praclarush	Praclarush	k1gMnSc1
Taonas	Taonas	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
jehož	jehož	k3xOyRp3gFnSc7
pomocí	pomoc	k1gFnSc7
mohli	moct	k5eAaImAgMnP
napájet	napájet	k5eAaImF
křeslo	křeslo	k1gNnSc4
v	v	k7c6
Antarktidě	Antarktida	k1gFnSc6
a	a	k8xC
ubránit	ubránit	k5eAaPmF
se	se	k3xPyFc4
tak	tak	k6eAd1
před	před	k7c7
Anubisem	Anubis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
toho	ten	k3xDgNnSc2
samého	samý	k3xTgNnSc2
ZPM	ZPM	kA
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
uskutečnit	uskutečnit	k5eAaPmF
expedice	expedice	k1gFnSc1
Atlantis	Atlantis	k1gFnSc1
do	do	k7c2
galaxie	galaxie	k1gFnSc2
Pegasus	Pegasus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
v	v	k7c6
díle	díl	k1gInSc6
Moebius	Moebius	k1gInSc1
získal	získat	k5eAaPmAgMnS
dr	dr	kA
<g/>
.	.	kIx.
Daniel	Daniel	k1gMnSc1
Jackson	Jackson	k1gMnSc1
knihy	kniha	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
mu	on	k3xPp3gMnSc3
před	před	k7c7
svou	svůj	k3xOyFgFnSc7
smrtí	smrt	k1gFnSc7
odkázala	odkázat	k5eAaPmAgFnS
Catherine	Catherin	k1gInSc5
Langfordová	Langfordový	k2eAgNnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
nim	on	k3xPp3gFnPc3
zjistil	zjistit	k5eAaPmAgMnS
že	že	k9
3000	#num#	k4
let	léto	k1gNnPc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
když	když	k8xS
vládl	vládnout	k5eAaImAgMnS
na	na	k7c6
Zemi	zem	k1gFnSc6
goa	goa	k?
<g/>
'	'	kIx"
<g/>
uld	uld	k?
Ra	ra	k0
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
u	u	k7c2
sebe	sebe	k3xPyFc4
ZPM	ZPM	kA
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
věděl	vědět	k5eAaImAgMnS
k	k	k7c3
čemu	co	k3yInSc3,k3yQnSc3,k3yRnSc3
slouží	sloužit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
na	na	k7c6
Zemi	zem	k1gFnSc6
toto	tento	k3xDgNnSc1
ZPM	ZPM	kA
nebylo	být	k5eNaImAgNnS
nalezeno	nalezen	k2eAgNnSc1d1
<g/>
,	,	kIx,
přesvědčil	přesvědčit	k5eAaPmAgMnS
Daniel	Daniel	k1gMnSc1
Jackson	Jackson	k1gMnSc1
SG-1	SG-1	k1gMnSc1
včetně	včetně	k7c2
generála	generál	k1gMnSc2
O	O	kA
<g/>
'	'	kIx"
<g/>
Neilla	Neill	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gInSc4
vrátili	vrátit	k5eAaPmAgMnP
pomocí	pomocí	k7c2
stroje	stroj	k1gInSc2
času	čas	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
ZPM	ZPM	kA
mohlo	moct	k5eAaImAgNnS
velitelství	velitelství	k1gNnSc1
brány	brána	k1gFnSc2
vyslat	vyslat	k5eAaPmF
pomoc	pomoc	k1gFnSc4
expedici	expedice	k1gFnSc4
Atlantis	Atlantis	k1gFnSc1
a	a	k8xC
také	také	k9
podpořit	podpořit	k5eAaPmF
hyperpohon	hyperpohon	k1gInSc4
na	na	k7c6
právě	právě	k6eAd1
dokončeném	dokončený	k2eAgInSc6d1
bitevním	bitevní	k2eAgInSc6d1
křižníku	křižník	k1gInSc6
Daedalus	Daedalus	k1gInSc4
a	a	k8xC
vyslat	vyslat	k5eAaPmF
ho	on	k3xPp3gMnSc4
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Atlantidě	Atlantida	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
třetí	třetí	k4xOgFnSc2
řady	řada	k1gFnSc2
seriálu	seriál	k1gInSc2
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
získali	získat	k5eAaPmAgMnP
lidé	člověk	k1gMnPc1
zpátky	zpátky	k6eAd1
Atlantidu	Atlantida	k1gFnSc4
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
rukou	ruka	k1gFnPc2
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgFnP
na	na	k7c6
Atlantidě	Atlantida	k1gFnSc6
3	#num#	k4
plně	plně	k6eAd1
nabitá	nabitý	k2eAgFnSc1d1
ZPM	ZPM	kA
po	po	k7c6
replikátorech	replikátor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
protesty	protest	k1gInPc4
dr	dr	kA
<g/>
.	.	kIx.
McKaye	McKay	k1gMnSc2
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
na	na	k7c6
Atlantidě	Atlantida	k1gFnSc6
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc1
ZPM	ZPM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
bylo	být	k5eAaImAgNnS
posláno	poslat	k5eAaPmNgNnS
na	na	k7c4
základnu	základna	k1gFnSc4
antiků	antik	k1gInPc2
v	v	k7c6
Antarktidě	Antarktida	k1gFnSc6
na	na	k7c6
Zemi	zem	k1gFnSc6
k	k	k7c3
obraně	obrana	k1gFnSc3
Země	zem	k1gFnSc2
a	a	k8xC
třetí	třetí	k4xOgFnSc7
na	na	k7c4
Odysseu	Odyssea	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jí	on	k3xPp3gFnSc3
pomohlo	pomoct	k5eAaPmAgNnS
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Oriům	Ori	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Projekt	projekt	k1gInSc1
Arcturus	Arcturus	k1gInSc1
</s>
<s>
Projekt	projekt	k1gInSc1
Arcturus	Arcturus	k1gInSc1
byl	být	k5eAaImAgInS
pokusem	pokus	k1gInSc7
Antiků	Antik	k1gInPc2
zvrátit	zvrátit	k5eAaPmF
nepříznový	příznový	k2eNgInSc4d1
vývoj	vývoj	k1gInSc4
války	válka	k1gFnSc2
s	s	k7c7
Wraithy	Wraith	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tím	ten	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
nahradí	nahradit	k5eAaPmIp3nS
stávající	stávající	k2eAgFnSc3d1
ZPM	ZPM	kA
mnohem	mnohem	k6eAd1
<g/>
,	,	kIx,
mnohem	mnohem	k6eAd1
výkonnějším	výkonný	k2eAgInSc7d2
zdrojem	zdroj	k1gInSc7
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
obdobný	obdobný	k2eAgInSc4d1
princip	princip	k1gInSc4
jako	jako	k8xC,k8xS
u	u	k7c2
ZPM	ZPM	kA
<g/>
,	,	kIx,
ale	ale	k8xC
získává	získávat	k5eAaImIp3nS
energii	energie	k1gFnSc4
z	z	k7c2
vlastního	vlastní	k2eAgInSc2d1
časoprostoru	časoprostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
by	by	kYmCp3nS
zdroj	zdroj	k1gInSc1
energie	energie	k1gFnSc2
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
stejný	stejný	k2eAgInSc1d1
výkon	výkon	k1gInSc1
jako	jako	k8xC,k8xS
samotný	samotný	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pravdou	pravda	k1gFnSc7
zůstává	zůstávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
projekt	projekt	k1gInSc4
se	se	k3xPyFc4
Antikům	Antik	k1gMnPc3
nepodařilo	podařit	k5eNaPmAgNnS
dotáhnout	dotáhnout	k5eAaPmF
do	do	k7c2
konečné	konečný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
a	a	k8xC
dle	dle	k7c2
Zelenkových	Zelenkových	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
ani	ani	k9
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
vytváření	vytváření	k1gNnSc3
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yQgNnSc6,k3yRgNnSc6
nefungují	fungovat	k5eNaImIp3nP
fyzikální	fyzikální	k2eAgInPc1d1
zákony	zákon	k1gInPc1
a	a	k8xC
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
nemožné	možný	k2eNgNnSc1d1
předpovědět	předpovědět	k5eAaPmF
vzájemné	vzájemný	k2eAgFnPc4d1
interakce	interakce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
McKayovi	McKaya	k1gMnSc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
dokončit	dokončit	k5eAaPmF
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zničit	zničit	k5eAaPmF
5	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
EMP	EMP	kA
generátor	generátor	k1gInSc1
</s>
<s>
Generátor	generátor	k1gInSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
(	(	kIx(
<g/>
EMP	EMP	kA
generátor	generátor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
generuje	generovat	k5eAaImIp3nS
silné	silný	k2eAgNnSc1d1
elektromagnetické	elektromagnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
má	mít	k5eAaImIp3nS
několik	několik	k4yIc4
důsledků	důsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
prvé	prvý	k4xOgInPc4
v	v	k7c6
dosahu	dosah	k1gInSc6
pole	pole	k1gNnSc2
nefunguje	fungovat	k5eNaImIp3nS
kompas	kompas	k1gInSc4
a	a	k8xC
za	za	k7c4
druhé	druhý	k4xOgNnSc4
v	v	k7c6
něm	on	k3xPp3gNnSc6
nefunguje	fungovat	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
elektronika	elektronika	k1gFnSc1
včetně	včetně	k7c2
Antických	antický	k2eAgInPc2d1
a	a	k8xC
Wraithských	Wraithský	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generátor	generátor	k1gInSc1
je	být	k5eAaImIp3nS
napájen	napájen	k2eAgInSc1d1
jedním	jeden	k4xCgInSc7
ZPM	ZPM	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komunikační	komunikační	k2eAgInPc1d1
kameny	kámen	k1gInPc1
</s>
<s>
Antické	antický	k2eAgInPc1d1
komunikační	komunikační	k2eAgInPc1d1
kameny	kámen	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
individuální	individuální	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
pomocí	pomocí	k7c2
přenesení	přenesení	k1gNnSc2
vědomí	vědomí	k1gNnSc2
do	do	k7c2
druhé	druhý	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
může	moct	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
na	na	k7c4
obrovské	obrovský	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
převzetí	převzetí	k1gNnSc4
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
tělem	tělo	k1gNnSc7
druhé	druhý	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
zanechalo	zanechat	k5eAaPmAgNnS
jakékoliv	jakýkoliv	k3yIgFnPc4
stopy	stopa	k1gFnPc4
v	v	k7c6
její	její	k3xOp3gFnSc6
paměti	paměť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zařízení	zařízení	k1gNnSc1
je	být	k5eAaImIp3nS
běžně	běžně	k6eAd1
používáno	používat	k5eAaImNgNnS
v	v	k7c6
seriálu	seriál	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
posádky	posádka	k1gFnSc2
lodi	loď	k1gFnSc2
Destiny	Destina	k1gFnSc2
s	s	k7c7
Velitelstvím	velitelství	k1gNnSc7
hvězdné	hvězdný	k2eAgFnSc2d1
brány	brána	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zbraně	zbraň	k1gFnPc1
</s>
<s>
Drone	Dronout	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
střela	střela	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Drone	Dronout	k5eAaImIp3nS,k5eAaPmIp3nS
je	on	k3xPp3gFnPc4
antická	antický	k2eAgFnSc1d1
střela	střela	k1gFnSc1
(	(	kIx(
<g/>
velmi	velmi	k6eAd1
vyspělá	vyspělý	k2eAgFnSc1d1
obdoba	obdoba	k1gFnSc1
pozemských	pozemský	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
odpalovaná	odpalovaný	k2eAgFnSc1d1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
antického	antický	k2eAgNnSc2d1
křesla	křeslo	k1gNnSc2
nebo	nebo	k8xC
ovládací	ovládací	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
v	v	k7c6
Puddle	Puddle	k1gFnSc6
Jumperu	jumper	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
jako	jako	k9
chobotnička	chobotnička	k1gFnSc1
se	s	k7c7
žlutou	žlutý	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
a	a	k8xC
černými	černý	k2eAgNnPc7d1
chapadly	chapadlo	k1gNnPc7
<g/>
,	,	kIx,
po	po	k7c6
aktivaci	aktivace	k1gFnSc6
září	září	k1gNnSc2
jako	jako	k8xS,k8xC
oheň	oheň	k1gInSc1
<g/>
,	,	kIx,
její	její	k3xOp3gInSc4
povrch	povrch	k1gInSc4
se	se	k3xPyFc4
totiž	totiž	k9
rozžhaví	rozžhavit	k5eAaPmIp3nS
do	do	k7c2
teploty	teplota	k1gFnSc2
plazmatu	plazma	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
dokáže	dokázat	k5eAaPmIp3nS
proniknout	proniknout	k5eAaPmF
většinou	většina	k1gFnSc7
energetických	energetický	k2eAgInPc2d1
štítů	štít	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
střela	střela	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
dalšími	další	k2eAgNnPc7d1
drony	drono	k1gNnPc7
a	a	k8xC
nebo	nebo	k8xC
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
na	na	k7c4
dálku	dálka	k1gFnSc4
ovládat	ovládat	k5eAaImF
z	z	k7c2
jumperu	jumper	k1gInSc2
nebo	nebo	k8xC
antického	antický	k2eAgNnSc2d1
křesla	křeslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
přesná	přesný	k2eAgFnSc1d1
a	a	k8xC
účinná	účinný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ke	k	k7c3
zničení	zničení	k1gNnSc3
Wraithské	Wraithský	k2eAgFnSc2d1
šipky	šipka	k1gFnSc2
stačí	stačit	k5eAaBmIp3nS
jedna	jeden	k4xCgFnSc1
jediná	jediný	k2eAgFnSc1d1
střela	střela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zničení	zničení	k1gNnSc3
lodě	loď	k1gFnSc2
třídy	třída	k1gFnSc2
Ha	ha	kA
<g/>
'	'	kIx"
<g/>
tak	tak	k6eAd1
stačí	stačit	k5eAaBmIp3nS
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
střely	střela	k1gFnPc4
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
když	když	k8xS
jsou	být	k5eAaImIp3nP
zapnuté	zapnutý	k2eAgInPc4d1
štíty	štít	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zničení	zničení	k1gNnSc3
lodí	loď	k1gFnPc2
třídy	třída	k1gFnSc2
Aurora	Aurora	k1gFnSc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
stačit	stačit	k5eAaBmF
i	i	k9
pouze	pouze	k6eAd1
3	#num#	k4
střely	střela	k1gFnPc4
Drone	Dron	k1gInSc5
(	(	kIx(
<g/>
pokud	pokud	k8xS
nejsou	být	k5eNaImIp3nP
zapnuté	zapnutý	k2eAgInPc1d1
štíty	štít	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obranný	obranný	k2eAgInSc4d1
satelit	satelit	k1gInSc4
</s>
<s>
Obranná	obranný	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
antiků	antik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdysi	kdysi	k6eAd1
jich	on	k3xPp3gMnPc2
byly	být	k5eAaImAgFnP
desítky	desítka	k1gFnPc4
<g/>
,	,	kIx,
expedice	expedice	k1gFnSc1
však	však	k9
narazila	narazit	k5eAaPmAgFnS
jen	jen	k9
na	na	k7c4
poslední	poslední	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zůstal	zůstat	k5eAaPmAgInS
po	po	k7c6
válce	válka	k1gFnSc6
s	s	k7c7
Wraithy	Wraith	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediný	jediný	k2eAgInSc1d1
plně	plně	k6eAd1
funkční	funkční	k2eAgInSc1d1
satelit	satelit	k1gInSc1
dokáže	dokázat	k5eAaPmIp3nS
sestřelit	sestřelit	k5eAaPmF
několik	několik	k4yIc4
Wraithských	Wraithská	k1gFnPc2
mateřských	mateřský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
takové	takový	k3xDgNnSc1
štěstí	štěstí	k1gNnSc1
však	však	k9
expedice	expedice	k1gFnSc1
neměla	mít	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satelit	satelit	k1gInSc4
našli	najít	k5eAaPmAgMnP
"	"	kIx"
<g/>
mrtvý	mrtvý	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
bez	bez	k7c2
náznaku	náznak	k1gInSc2
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
také	také	k9
proto	proto	k8xC
ho	on	k3xPp3gMnSc4
zřejmě	zřejmě	k6eAd1
Wraithové	Wraith	k1gMnPc1
ignorovali	ignorovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
McKayovi	McKaya	k1gMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
připojit	připojit	k5eAaPmF
satelit	satelit	k1gInSc4
na	na	k7c4
naqadahový	naqadahový	k2eAgInSc4d1
reaktor	reaktor	k1gInSc4
a	a	k8xC
přemostit	přemostit	k5eAaPmF
spojení	spojení	k1gNnSc4
mezi	mezi	k7c7
bufferem	buffer	k1gInSc7
(	(	kIx(
<g/>
zásobníkem	zásobník	k1gInSc7
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
satelit	satelit	k1gInSc4
si	se	k3xPyFc3
vezme	vzít	k5eAaPmIp3nS
energii	energie	k1gFnSc4
z	z	k7c2
reaktoru	reaktor	k1gInSc2
<g/>
,	,	kIx,
nabije	nabít	k5eAaBmIp3nS,k5eAaPmIp3nS
s	s	k7c7
ní	on	k3xPp3gFnSc7
buffer	buffer	k1gInSc1
a	a	k8xC
poté	poté	k6eAd1
jí	jíst	k5eAaImIp3nS
z	z	k7c2
bufferu	buffer	k1gInSc2
předává	předávat	k5eAaImIp3nS
zbraňovým	zbraňový	k2eAgInPc3d1
systémům	systém	k1gInPc3
<g/>
)	)	kIx)
a	a	k8xC
zbraňovými	zbraňový	k2eAgInPc7d1
systémy	systém	k1gInPc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
satelit	satelit	k1gMnSc1
sestřelil	sestřelit	k5eAaPmAgMnS
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
Wraithskou	Wraithský	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
obvody	obvod	k1gInPc1
přetížily	přetížit	k5eAaPmAgInP
a	a	k8xC
zbylé	zbylý	k2eAgFnSc2d1
Wraithské	Wraithský	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
ho	on	k3xPp3gNnSc4
zničily	zničit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
štít	štít	k1gInSc1
</s>
<s>
Pokud	pokud	k8xS
máte	mít	k5eAaImIp2nP
antický	antický	k2eAgInSc4d1
gen	gen	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
přiložení	přiložení	k1gNnSc6
k	k	k7c3
hrudi	hruď	k1gFnSc3
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
malá	malý	k2eAgFnSc1d1
věcička	věcička	k1gFnSc1
rozsvítí	rozsvítit	k5eAaPmIp3nS
a	a	k8xC
nadále	nadále	k6eAd1
vás	vy	k3xPp2nPc4
chrání	chránit	k5eAaImIp3nS
před	před	k7c7
vlivy	vliv	k1gInPc7
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pád	pád	k1gInSc4
z	z	k7c2
velké	velký	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
nebo	nebo	k8xC
o	o	k7c4
pokus	pokus	k1gInSc4
vás	vy	k3xPp2nPc4
zastřelit	zastřelit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štítem	štít	k1gInSc7
nepronikne	proniknout	k5eNaPmIp3nS
vůbec	vůbec	k9
nic	nic	k3yNnSc1
ani	ani	k8xC
jídlo	jídlo	k1gNnSc1
a	a	k8xC
pití	pití	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
aktivaci	aktivace	k1gFnSc6
si	se	k3xPyFc3
štít	štít	k1gInSc1
zapamatuje	zapamatovat	k5eAaPmIp3nS
svého	svůj	k3xOyFgMnSc2
nositele	nositel	k1gMnSc2
a	a	k8xC
nikdo	nikdo	k3yNnSc1
jiný	jiný	k1gMnSc1
jej	on	k3xPp3gMnSc4
pak	pak	k6eAd1
nemůže	moct	k5eNaImIp3nS
použít	použít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypínání	vypínání	k1gNnSc1
se	se	k3xPyFc4
ovládá	ovládat	k5eAaImIp3nS
myšlenkami	myšlenka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Ruční	ruční	k2eAgFnSc1d1
omračovací	omračovací	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
jako	jako	k9
hranatý	hranatý	k2eAgInSc1d1
signalizační	signalizační	k2eAgInSc1d1
majáček	majáček	k1gInSc1
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc1d1
část	část	k1gFnSc1
má	mít	k5eAaImIp3nS
design	design	k1gInSc1
podobný	podobný	k2eAgInSc1d1
antickému	antický	k2eAgInSc3d1
osobnímu	osobní	k2eAgInSc3d1
štítu	štít	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
účinná	účinný	k2eAgFnSc1d1
omračující	omračující	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
výstřel	výstřel	k1gInSc1
člověka	člověk	k1gMnSc2
omráčí	omráčet	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Past	past	k1gFnSc1
na	na	k7c4
temnotu	temnota	k1gFnSc4
</s>
<s>
Tato	tento	k3xDgFnSc1
past	past	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
přilákání	přilákání	k1gNnSc3
a	a	k8xC
uvěznění	uvěznění	k1gNnSc4
jakési	jakýsi	k3yIgFnSc2
energetické	energetický	k2eAgFnSc2d1
bytosti	bytost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
mezníkem	mezník	k1gInSc7
při	při	k7c6
vývoji	vývoj	k1gInSc6
Povznesení	povznesení	k1gNnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Temnota	temnota	k1gFnSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nS
energií	energie	k1gFnSc7
<g/>
,	,	kIx,
past	past	k1gFnSc1
tedy	tedy	k9
po	po	k7c6
zapnutí	zapnutí	k1gNnSc6
generuje	generovat	k5eAaImIp3nS
silné	silný	k2eAgNnSc4d1
energetické	energetický	k2eAgNnSc4d1
pole	pole	k1gNnSc4
pro	pro	k7c4
přilákání	přilákání	k1gNnSc4
této	tento	k3xDgFnSc2
bytosti	bytost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
potom	potom	k6eAd1
nikdo	nikdo	k3yNnSc1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
v	v	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
bytost	bytost	k1gFnSc1
nenechala	nechat	k5eNaPmAgFnS
nachytat	nachytat	k5eAaPmF,k5eAaBmF
a	a	k8xC
odplazila	odplazit	k5eAaPmAgFnS
se	se	k3xPyFc4
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zbraň	zbraň	k1gFnSc1
na	na	k7c6
Dakaře	Dakara	k1gFnSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
zbraň	zbraň	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
planetě	planeta	k1gFnSc6
Dakara	Dakar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokáže	dokázat	k5eAaPmIp3nS
účinně	účinně	k6eAd1
ničit	ničit	k5eAaImF
replikátory	replikátor	k1gInPc4
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
Disruptor	Disruptor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jacob	Jacoba	k1gFnPc2
Carter	Cartra	k1gFnPc2
ji	on	k3xPp3gFnSc4
použil	použít	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zničil	zničit	k5eAaPmAgInS
všechny	všechen	k3xTgInPc4
replikátory	replikátor	k1gInPc4
v	v	k7c6
galaxii	galaxie	k1gFnSc6
Mléčná	mléčný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
ale	ale	k8xC
nemělo	mít	k5eNaImAgNnS
takový	takový	k3xDgInSc4
dosah	dosah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Samantha	Samantha	k1gFnSc1
Carterová	Carterová	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Ba	ba	k9
<g/>
'	'	kIx"
<g/>
ala	ala	k1gFnSc1
<g/>
)	)	kIx)
upravila	upravit	k5eAaPmAgFnS
Hvězdnou	hvězdný	k2eAgFnSc4d1
bránu	brána	k1gFnSc4
na	na	k7c6
Dakaře	Dakara	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vytočila	vytočit	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
brány	brána	k1gFnPc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
galaxii	galaxie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Energetická	energetický	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
přenášela	přenášet	k5eAaImAgFnS
bránou	brána	k1gFnSc7
tak	tak	k6eAd1
zničila	zničit	k5eAaPmAgFnS
všechny	všechen	k3xTgInPc4
replikátory	replikátor	k1gInPc4
v	v	k7c6
galaxii	galaxie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
zařízení	zařízení	k1gNnSc1
původně	původně	k6eAd1
sloužilo	sloužit	k5eAaImAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
života	život	k1gInSc2
v	v	k7c6
Mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
může	moct	k5eAaImIp3nS
zničit	zničit	k5eAaPmF
veškerý	veškerý	k3xTgInSc4
život	život	k1gInSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
galaxii	galaxie	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
povznesených	povznesený	k2eAgFnPc2d1
bytostí	bytost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc2
pokoušel	pokoušet	k5eAaImAgMnS
zmocnit	zmocnit	k5eAaPmF
Anubis	Anubis	k1gInSc4
a	a	k8xC
také	také	k9
proto	proto	k8xC
ho	on	k3xPp3gMnSc4
chtěli	chtít	k5eAaImAgMnP
zničit	zničit	k5eAaPmF
replikátoři	replikátor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
když	když	k8xS
se	se	k3xPyFc4
pomocí	pomocí	k7c2
této	tento	k3xDgFnSc2
zbraně	zbraň	k1gFnSc2
a	a	k8xC
náhodného	náhodný	k2eAgNnSc2d1
vytáčení	vytáčení	k1gNnSc2
bran	brána	k1gFnPc2
snažili	snažit	k5eAaImAgMnP
svobodní	svobodný	k2eAgMnPc1d1
jaffové	jaff	k1gMnPc1
ničit	ničit	k5eAaImF
orijské	orijský	k2eAgMnPc4d1
převory	převor	k1gMnPc4
a	a	k8xC
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
přiletěla	přiletět	k5eAaPmAgFnS
nad	nad	k7c7
Dakaru	Dakar	k1gInSc6
Adria	Adrius	k1gMnSc2
a	a	k8xC
orijským	orijský	k2eAgInSc7d1
paprskem	paprsek	k1gInSc7
z	z	k7c2
mateřské	mateřský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
zbraň	zbraň	k1gFnSc1
na	na	k7c6
Dakaře	Dakara	k1gFnSc6
zničila	zničit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Léčebná	léčebný	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Stázová	Stázový	k2eAgFnSc1d1
komora	komora	k1gFnSc1
</s>
<s>
Komora	komora	k1gFnSc1
pro	pro	k7c4
stázi	stáze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvede	uvést	k5eAaPmIp3nS
vás	vy	k3xPp2nPc4
do	do	k7c2
umělého	umělý	k2eAgInSc2d1
spánku	spánek	k1gInSc2
a	a	k8xC
sníží	snížit	k5eAaPmIp3nS
činnost	činnost	k1gFnSc4
metabolismu	metabolismus	k1gInSc2
na	na	k7c4
minimum	minimum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stárnutí	stárnutí	k1gNnPc4
je	být	k5eAaImIp3nS
zpomaleno	zpomalen	k2eAgNnSc1d1
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
zcela	zcela	k6eAd1
zastaveno	zastavit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stázová	Stázový	k2eAgFnSc1d1
komora	komora	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
Atlantidě	Atlantida	k1gFnSc6
a	a	k8xC
také	také	k9
na	na	k7c6
všech	všecek	k3xTgFnPc6
Antických	antický	k2eAgFnPc6d1
základnách	základna	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
nalezeny	nalézt	k5eAaBmNgFnP,k5eAaPmNgFnP
na	na	k7c4
Zemi	zem	k1gFnSc4
a	a	k8xC
Praclarush	Praclarush	k1gInSc4
Taonas	Taonasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vylepšená	vylepšený	k2eAgFnSc1d1
stázová	stázový	k2eAgFnSc1d1
kapsle	kapsle	k1gFnSc1
</s>
<s>
Zpomaluje	zpomalovat	k5eAaImIp3nS
stárnutí	stárnutí	k1gNnSc4
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
udržuje	udržovat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
ve	v	k7c6
stázi	stáze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mozková	mozkový	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
však	však	k9
zůstává	zůstávat	k5eAaImIp3nS
zachována	zachovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
lůžek	lůžko	k1gNnPc2
víc	hodně	k6eAd2
a	a	k8xC
jsou	být	k5eAaImIp3nP
propojené	propojený	k2eAgFnPc1d1
<g/>
,	,	kIx,
hibernující	hibernující	k2eAgFnPc1d1
osoby	osoba	k1gFnPc1
spolu	spolu	k6eAd1
mohou	moct	k5eAaImIp3nP
komunikovat	komunikovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
zařízení	zařízení	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
některých	některý	k3yIgFnPc6
lodích	loď	k1gFnPc6
třídy	třída	k1gFnSc2
Aurora	Aurora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Antikové	Antik	k1gMnPc1
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
</s>
<s>
Technologie	technologie	k1gFnSc1
ve	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
</s>
<s>
Orijské	Orijský	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
ve	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
</s>
<s>
Asuranské	Asuranský	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
ve	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
</s>
<s>
Goa	Goa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
uldské	uldské	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
ve	v	k7c6
Hvězdné	hvězdný	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ancient_technology_in_Stargate	Ancient_technology_in_Stargat	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gMnSc5
Stargate	Stargat	k1gMnSc5
Omnipedia	Omnipedium	k1gNnSc2
–	–	k?
Puddle	Puddle	k1gFnSc1
Jumper	jumper	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
1	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
1	#num#	k4
Vynoření	vynoření	k1gNnSc6
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
SG-1	SG-1	k1gFnSc1
7	#num#	k4
<g/>
x	x	k?
<g/>
21	#num#	k4
a	a	k8xC
7	#num#	k4
<g/>
x	x	k?
<g/>
22	#num#	k4
Ztracené	ztracený	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
SG-1	SG-1	k1gFnSc1
8	#num#	k4
<g/>
x	x	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
19	#num#	k4
a	a	k8xC
8	#num#	k4
<g/>
x	x	k?
<g/>
20	#num#	k4
Moebius	Moebius	k1gInSc1
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
1	#num#	k4
<g/>
x	x	k?
<g/>
20	#num#	k4
V	v	k7c6
obležení	obležení	k1gNnSc6
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
3	#num#	k4
<g/>
x	x	k?
<g/>
12	#num#	k4
Ozvěny	ozvěna	k1gFnSc2
<g/>
↑	↑	k?
www.sg1.cz/technologie/zpm	www.sg1.cz/technologie/zpm	k6eAd1
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
3	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
8	#num#	k4
McKay	McKay	k1gInPc7
a	a	k8xC
paní	paní	k1gFnSc7
Miller	Miller	k1gMnSc1
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantid	Atlantida	k1gFnPc2
1	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
6	#num#	k4
Konec	konec	k1gInSc1
dětství	dětství	k1gNnSc2
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
4	#num#	k4
<g/>
x	x	k?
<g/>
11	#num#	k4
Všechny	všechen	k3xTgInPc1
mé	můj	k3xOp1gInPc1
hříchy	hřích	k1gInPc1
budou	být	k5eAaImBp3nP
vzpomenuty	vzpomenut	k2eAgInPc1d1
<g/>
↑	↑	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
„	„	k?
<g/>
2	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
9	#num#	k4
Aurora	Aurora	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
Díla	dílo	k1gNnSc2
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
Archa	archa	k1gFnSc1
pravdy	pravda	k1gFnSc2
•	•	k?
Návrat	návrat	k1gInSc1
Seriály	seriál	k1gInPc5
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
(	(	kIx(
<g/>
SG-	SG-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlantida	Atlantida	k1gFnSc1
•	•	k?
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
•	•	k?
Třetí	třetí	k4xOgFnSc2
říše	říš	k1gFnSc2
•	•	k?
Infinity	Infinita	k1gFnSc2
(	(	kIx(
<g/>
animovaný	animovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
SG-1	SG-1	k4
</s>
<s>
epizody	epizoda	k1gFnPc4
•	•	k?
postavy	postava	k1gFnSc2
Samantha	Samantha	k1gFnSc1
Carterová	Carterová	k1gFnSc1
•	•	k?
George	George	k1gNnSc1
Hammond	Hammond	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Jackson	Jackson	k1gMnSc1
•	•	k?
Hank	Hank	k1gInSc1
Landry	Landr	k1gInPc1
•	•	k?
Vala	Vala	k1gMnSc1
Mal	málit	k5eAaImRp2nS
Doran	Doran	k1gInSc1
•	•	k?
Cameron	Cameron	k1gInSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
Jack	Jack	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Neill	Neill	k1gMnSc1
•	•	k?
Jonas	Jonas	k1gMnSc1
Quinn	Quinn	k1gMnSc1
•	•	k?
Teal	Teal	k1gMnSc1
<g/>
'	'	kIx"
<g/>
c	c	k0
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
epizody	epizoda	k1gFnPc4
•	•	k?
postavy	postava	k1gFnSc2
Carson	Carson	k1gMnSc1
Beckett	Beckett	k1gMnSc1
•	•	k?
Samantha	Samantha	k1gFnSc1
Carterová	Carterová	k1gFnSc1
•	•	k?
Ronon	Ronon	k1gMnSc1
Dex	Dex	k1gMnSc1
•	•	k?
Teyla	Teyla	k1gMnSc1
Emmagan	Emmagan	k1gMnSc1
•	•	k?
Aiden	Aiden	k1gInSc1
Ford	ford	k1gInSc1
•	•	k?
Jennifer	Jennifer	k1gInSc1
Kellerová	Kellerová	k1gFnSc1
•	•	k?
Rodney	Rodne	k2eAgFnPc4d1
McKay	McKaa	k1gFnPc4
•	•	k?
John	John	k1gMnSc1
Sheppard	Sheppard	k1gMnSc1
•	•	k?
Elizabeth	Elizabeth	k1gFnSc1
Weirová	Weirová	k1gFnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Woolsey	Woolsea	k1gFnSc2
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
</s>
<s>
epizody	epizoda	k1gFnPc4
•	•	k?
postavy	postava	k1gFnSc2
Chloe	Chloe	k1gFnSc1
Armstrongová	Armstrongová	k1gFnSc1
•	•	k?
Ronald	Ronald	k1gMnSc1
Greer	Greer	k1gMnSc1
•	•	k?
Tamara	Tamara	k1gFnSc1
Johansenová	Johansenová	k1gFnSc1
•	•	k?
Nicholas	Nicholas	k1gMnSc1
Rush	Rush	k1gMnSc1
•	•	k?
Matthew	Matthew	k1gMnSc1
Scott	Scott	k1gMnSc1
•	•	k?
Eli	Eli	k1gMnSc1
Wallace	Wallace	k1gFnSc2
•	•	k?
Everett	Everett	k1gMnSc1
Young	Young	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Telford	Telford	k1gMnSc1
•	•	k?
Camile	Camila	k1gFnSc6
Wrayová	Wrayová	k1gFnSc1
Infinity	Infinita	k1gFnSc2
</s>
<s>
epizody	epizoda	k1gFnPc4
Rasy	rasa	k1gFnSc2
</s>
<s>
SG-1	SG-1	k4
</s>
<s>
Abydosané	Abydosaný	k2eAgFnPc1d1
•	•	k?
Antikové	Antikový	k2eAgFnPc1d1
(	(	kIx(
<g/>
postavy	postava	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Asgardi	Asgard	k1gMnPc1
•	•	k?
Furlingové	Furlingový	k2eAgNnSc1d1
•	•	k?
Goa	Goa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
uldi	uldi	k1gNnSc1
(	(	kIx(
<g/>
vládci	vládce	k1gMnPc1
soustavy	soustava	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Jaffové	Jaffový	k2eAgFnSc2d1
•	•	k?
Kull	Kull	k1gMnSc1
bojovníci	bojovník	k1gMnPc1
•	•	k?
Langarané	Langaraný	k2eAgNnSc1d1
•	•	k?
Luciánská	Luciánský	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
•	•	k?
Noxové	Noxová	k1gFnSc2
•	•	k?
Oriové	Oriová	k1gFnSc2
•	•	k?
Replikátoři	Replikátor	k1gMnPc1
•	•	k?
Tau	tau	k1gNnSc2
<g/>
'	'	kIx"
<g/>
riové	riový	k2eAgNnSc1d1
•	•	k?
Tok	tok	k1gInSc4
<g/>
'	'	kIx"
<g/>
rové	rové	k1gNnSc4
•	•	k?
Tolláni	Tollán	k2eAgMnPc1d1
•	•	k?
Unasové	Unasové	k2eAgFnSc1d1
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
Athosiané	Athosian	k1gMnPc1
•	•	k?
Asurané	Asuraný	k2eAgNnSc1d1
•	•	k?
Cestovatelé	cestovatel	k1gMnPc1
•	•	k?
Geniiové	Geniiový	k2eAgFnSc2d1
•	•	k?
Sateďané	Sateďaná	k1gFnSc2
•	•	k?
Wraithové	Wraithové	k2eAgInSc1d1
Hluboký	hluboký	k2eAgInSc1d1
vesmír	vesmír	k1gInSc1
</s>
<s>
Nakaiové	Nakaius	k1gMnPc1
•	•	k?
Nované	Novaný	k2eAgInPc4d1
Seznamy	seznam	k1gInPc4
</s>
<s>
lidské	lidský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
lidské	lidský	k2eAgFnPc4d1
civilizace	civilizace	k1gFnPc4
v	v	k7c6
seriálu	seriál	k1gInSc6
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
:	:	kIx,
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
fiktivní	fiktivní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
•	•	k?
planety	planeta	k1gFnSc2
•	•	k?
mytologie	mytologie	k1gFnSc2
(	(	kIx(
<g/>
povznesení	povznesení	k1gNnSc3
<g/>
)	)	kIx)
•	•	k?
technologie	technologie	k1gFnSc1
(	(	kIx(
<g/>
hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
pozemské	pozemský	k2eAgNnSc4d1
•	•	k?
antické	antický	k2eAgFnSc2d1
•	•	k?
asgardské	asgardský	k2eAgNnSc1d1
•	•	k?
asuranské	asuranský	k2eAgNnSc1d1
•	•	k?
goa	goa	k?
<g/>
'	'	kIx"
<g/>
uldské	uldské	k2eAgInSc4d1
•	•	k?
orijské	orijský	k2eAgNnSc1d1
•	•	k?
tokránské	tokránský	k2eAgNnSc1d1
•	•	k?
tollánské	tollánský	k2eAgNnSc1d1
•	•	k?
wraithské	wraithské	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Program	program	k1gInSc1
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
Velitelství	velitelství	k1gNnSc2
Hvězdné	hvězdný	k2eAgFnSc2d1
brány	brána	k1gFnSc2
•	•	k?
vesmírné	vesmírný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
(	(	kIx(
<g/>
Atlantida	Atlantida	k1gFnSc1
•	•	k?
Destiny	Destina	k1gFnSc2
•	•	k?
pozemské	pozemský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Hvězdná	hvězdný	k2eAgFnSc1d1
brána	brána	k1gFnSc1
</s>
