<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
kapalná	kapalný	k2eAgFnSc1d1	kapalná
<g/>
,	,	kIx,	,
vazká	vazký	k2eAgFnSc1d1	vazká
a	a	k8xC	a
viskózní	viskózní	k2eAgFnSc1d1	viskózní
cirkulující	cirkulující	k2eAgFnSc1d1	cirkulující
tkáň	tkáň	k1gFnSc1	tkáň
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
tekuté	tekutý	k2eAgFnSc2d1	tekutá
plazmy	plazma	k1gFnSc2	plazma
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
