<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
kapalná	kapalný	k2eAgFnSc1d1	kapalná
<g/>
,	,	kIx,	,
vazká	vazký	k2eAgFnSc1d1	vazká
a	a	k8xC	a
viskózní	viskózní	k2eAgFnSc1d1	viskózní
cirkulující	cirkulující	k2eAgFnSc1d1	cirkulující
tkáň	tkáň	k1gFnSc1	tkáň
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
tekuté	tekutý	k2eAgFnSc2d1	tekutá
plazmy	plazma	k1gFnSc2	plazma
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medicínské	medicínský	k2eAgInPc1d1	medicínský
termíny	termín	k1gInPc1	termín
souvisící	souvisící	k2eAgInPc1d1	souvisící
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
často	často	k6eAd1	často
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c4	na
hemo-	hemo-	k?	hemo-
nebo	nebo	k8xC	nebo
hemato-	hemato-	k?	hemato-
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
haema	haemum	k1gNnSc2	haemum
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc4	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
funkci	funkce	k1gFnSc3	funkce
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
často	často	k6eAd1	často
mluví	mluvit	k5eAaImIp3nS	mluvit
též	též	k9	též
jako	jako	k9	jako
o	o	k7c6	o
trofické	trofický	k2eAgFnSc6d1	trofická
tkáni	tkáň	k1gFnSc6	tkáň
nebo	nebo	k8xC	nebo
trofickém	trofický	k2eAgNnSc6d1	trofický
pojivu	pojivo	k1gNnSc6	pojivo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
funkce	funkce	k1gFnSc1	funkce
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
dopravovat	dopravovat	k5eAaImF	dopravovat
živiny	živina	k1gFnPc4	živina
(	(	kIx(	(
<g/>
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
stopové	stopový	k2eAgInPc4d1	stopový
prvky	prvek	k1gInPc4	prvek
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
odvádět	odvádět	k5eAaImF	odvádět
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
a	a	k8xC	a
kyselinu	kyselina	k1gFnSc4	kyselina
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
též	též	k9	též
transportuje	transportovat	k5eAaBmIp3nS	transportovat
buňky	buňka	k1gFnPc4	buňka
(	(	kIx(	(
<g/>
leukocyty	leukocyt	k1gInPc4	leukocyt
<g/>
,	,	kIx,	,
abnormální	abnormální	k2eAgFnPc4d1	abnormální
nádorové	nádorový	k2eAgFnPc4d1	nádorová
buňky	buňka	k1gFnPc4	buňka
<g/>
)	)	kIx)	)
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
substance	substance	k1gFnPc1	substance
(	(	kIx(	(
<g/>
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
lipidy	lipid	k1gInPc1	lipid
<g/>
,	,	kIx,	,
hormony	hormon	k1gInPc1	hormon
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
tkáně	tkáň	k1gFnPc4	tkáň
a	a	k8xC	a
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oběhová	oběhový	k2eAgFnSc1d1	oběhová
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
kapalná	kapalný	k2eAgFnSc1d1	kapalná
červená	červený	k2eAgFnSc1d1	červená
tkáň	tkáň	k1gFnSc1	tkáň
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
transport	transport	k1gInSc1	transport
kyslíku	kyslík	k1gInSc2	kyslík
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
–	–	k?	–
okysličování	okysličování	k1gNnSc2	okysličování
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
<s>
Též	též	k6eAd1	též
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
tkáně	tkáň	k1gFnPc4	tkáň
živinami	živina	k1gFnPc7	živina
(	(	kIx(	(
<g/>
cukry	cukr	k1gInPc1	cukr
<g/>
,	,	kIx,	,
tuky	tuk	k1gInPc1	tuk
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc1	vitamín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvádí	odvádět	k5eAaImIp3nS	odvádět
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
různé	různý	k2eAgInPc4d1	různý
komponenty	komponent	k1gInPc4	komponent
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
jsou	být	k5eAaImIp3nP	být
leukocyty	leukocyt	k1gInPc1	leukocyt
(	(	kIx(	(
<g/>
bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
tělo	tělo	k1gNnSc4	tělo
před	před	k7c7	před
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Krví	krev	k1gFnSc7	krev
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
také	také	k9	také
hormony	hormon	k1gInPc1	hormon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
orgány	orgán	k1gInPc4	orgán
–	–	k?	–
stimulují	stimulovat	k5eAaImIp3nP	stimulovat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
litrů	litr	k1gInPc2	litr
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
8	[number]	k4	8
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
asi	asi	k9	asi
60	[number]	k4	60
ml	ml	kA	ml
krve	krev	k1gFnSc2	krev
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
mívají	mívat	k5eAaImIp3nP	mívat
méně	málo	k6eAd2	málo
krve	krev	k1gFnSc2	krev
než	než	k8xS	než
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
objem	objem	k1gInSc1	objem
krve	krev	k1gFnSc2	krev
u	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
normovolémie	normovolémie	k1gFnSc1	normovolémie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
objemu	objem	k1gInSc2	objem
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hypervolémii	hypervolémie	k1gFnSc3	hypervolémie
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
objemu	objem	k1gInSc2	objem
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
hypovolémie	hypovolémie	k1gFnSc1	hypovolémie
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
poměr	poměr	k1gInSc1	poměr
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
8	[number]	k4	8
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc3	hmotnost
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
od	od	k7c2	od
světle	světle	k6eAd1	světle
červené	červený	k2eAgFnSc2d1	červená
–	–	k?	–
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
okysličená	okysličený	k2eAgFnSc1d1	okysličená
<g/>
,	,	kIx,	,
po	po	k7c4	po
tmavě	tmavě	k6eAd1	tmavě
červenou	červený	k2eAgFnSc4d1	červená
–	–	k?	–
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
metaloproteinová	metaloproteinový	k2eAgFnSc1d1	metaloproteinový
sloučenina	sloučenina	k1gFnSc1	sloučenina
obsahující	obsahující	k2eAgNnSc4d1	obsahující
železo	železo	k1gNnSc4	železo
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
–	–	k?	–
kofaktoru	kofaktor	k1gInSc2	kofaktor
hemu	hem	k1gInSc2	hem
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
kyslík	kyslík	k1gInSc1	kyslík
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
populární	populární	k2eAgFnPc1d1	populární
mylné	mylný	k2eAgFnPc1d1	mylná
představy	představa	k1gFnPc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
odkysličená	odkysličený	k2eAgFnSc1d1	odkysličená
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
že	že	k8xS	že
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
červenou	červená	k1gFnSc7	červená
<g/>
,	,	kIx,	,
jen	jen	k9	jen
když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žíly	žíla	k1gFnPc1	žíla
jsou	být	k5eAaImIp3nP	být
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
rozptýleno	rozptýlen	k2eAgNnSc4d1	rozptýleno
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
krev	krev	k1gFnSc1	krev
uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
slabý	slabý	k2eAgInSc4d1	slabý
světelný	světelný	k2eAgInSc4d1	světelný
odraz	odraz	k1gInSc4	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
nebo	nebo	k8xC	nebo
operaci	operace	k1gFnSc6	operace
vypadají	vypadat	k5eAaPmIp3nP	vypadat
tepny	tepna	k1gFnPc4	tepna
a	a	k8xC	a
žíly	žíla	k1gFnPc4	žíla
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
cévách	céva	k1gFnPc6	céva
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
oběh	oběh	k1gInSc4	oběh
je	být	k5eAaImIp3nS	být
zajišťován	zajišťovat	k5eAaImNgInS	zajišťovat
srdcem	srdce	k1gNnSc7	srdce
<g/>
,	,	kIx,	,
svalovou	svalový	k2eAgFnSc7d1	svalová
pumpou	pumpa	k1gFnSc7	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Proudí	proudit	k5eAaPmIp3nS	proudit
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
pro	pro	k7c4	pro
okysličení	okysličení	k1gNnSc4	okysličení
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
cirkulována	cirkulován	k2eAgFnSc1d1	cirkulována
tělem	tělo	k1gNnSc7	tělo
přes	přes	k7c4	přes
tepny	tepna	k1gFnPc4	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Rozptyluje	rozptylovat	k5eAaImIp3nS	rozptylovat
svůj	svůj	k3xOyFgInSc4	svůj
obsažený	obsažený	k2eAgInSc4d1	obsažený
kyslík	kyslík	k1gInSc4	kyslík
přechodem	přechod	k1gInSc7	přechod
přes	přes	k7c4	přes
tenké	tenký	k2eAgFnPc4d1	tenká
krevní	krevní	k2eAgFnPc4d1	krevní
cévy	céva	k1gFnPc4	céva
zvané	zvaný	k2eAgFnSc2d1	zvaná
kapiláry	kapilára	k1gFnSc2	kapilára
(	(	kIx(	(
<g/>
vlásečnice	vlásečnice	k1gFnSc2	vlásečnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
žílami	žíla	k1gFnPc7	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
transportuje	transportovat	k5eAaBmIp3nS	transportovat
metabolické	metabolický	k2eAgInPc4d1	metabolický
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
léky	lék	k1gInPc4	lék
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
cizí	cizí	k2eAgFnPc4d1	cizí
chemikálie	chemikálie	k1gFnPc4	chemikálie
do	do	k7c2	do
jater	játra	k1gNnPc2	játra
na	na	k7c6	na
eliminaci	eliminace	k1gFnSc6	eliminace
a	a	k8xC	a
do	do	k7c2	do
ledvin	ledvina	k1gFnPc2	ledvina
na	na	k7c4	na
vyloučení	vyloučení	k1gNnSc4	vyloučení
močí	moč	k1gFnPc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Tlumivý	tlumivý	k2eAgInSc1d1	tlumivý
roztok	roztok	k1gInSc1	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
bikarbonátu	bikarbonát	k1gInSc2	bikarbonát
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přítomný	přítomný	k2eAgMnSc1d1	přítomný
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
pH	ph	kA	ph
mezi	mezi	k7c4	mezi
7,35	[number]	k4	7,35
a	a	k8xC	a
7,45	[number]	k4	7,45
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anatomie	anatomie	k1gFnSc2	anatomie
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
jako	jako	k8xS	jako
např.	např.	kA	např.
hmyz	hmyz	k1gInSc4	hmyz
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc1	kyslík
jednoduše	jednoduše	k6eAd1	jednoduše
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
v	v	k7c6	v
plazmě	plazma	k1gFnSc6	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
zvířata	zvíře	k1gNnPc1	zvíře
používají	používat	k5eAaImIp3nP	používat
respirační	respirační	k2eAgInPc1d1	respirační
proteiny	protein	k1gInPc1	protein
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
kapacity	kapacita	k1gFnSc2	kapacita
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Hemoglobin	hemoglobin	k1gInSc1	hemoglobin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nejčastější	častý	k2eAgNnSc1d3	nejčastější
takový	takový	k3xDgInSc4	takový
protein	protein	k1gInSc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Hemocyanin	Hemocyanin	k2eAgInSc1d1	Hemocyanin
(	(	kIx(	(
<g/>
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
měď	měď	k1gFnSc4	měď
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
u	u	k7c2	u
korýšů	korýš	k1gMnPc2	korýš
a	a	k8xC	a
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
"	"	kIx"	"
<g/>
vanadiny	vanadin	k2eAgInPc4d1	vanadin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
proteiny	protein	k1gInPc4	protein
obsahující	obsahující	k2eAgInPc4d1	obsahující
vanad	vanad	k1gInSc4	vanad
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
nebo	nebo	k8xC	nebo
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
bezobratlovců	bezobratlovec	k1gMnPc2	bezobratlovec
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
proteiny	protein	k1gInPc4	protein
přenášející	přenášející	k2eAgInPc4d1	přenášející
kyslík	kyslík	k1gInSc4	kyslík
volně	volně	k6eAd1	volně
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
koncentraci	koncentrace	k1gFnSc4	koncentrace
respiračních	respirační	k2eAgInPc2d1	respirační
pigmentů	pigment	k1gInPc2	pigment
bez	bez	k7c2	bez
zvýšení	zvýšení	k1gNnSc2	zvýšení
viskozity	viskozita	k1gFnSc2	viskozita
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
krevních	krevní	k2eAgInPc2d1	krevní
filtračních	filtrační	k2eAgInPc2d1	filtrační
orgánů	orgán	k1gInPc2	orgán
jako	jako	k8xC	jako
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
není	být	k5eNaImIp3nS	být
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
hemolymfa	hemolymf	k1gMnSc2	hemolymf
<g/>
)	)	kIx)	)
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
do	do	k7c2	do
transportu	transport	k1gInSc2	transport
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Otvory	otvor	k1gInPc1	otvor
zvané	zvaný	k2eAgFnSc2d1	zvaná
vzdušnice	vzdušnice	k1gFnSc2	vzdušnice
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
kyslíku	kyslík	k1gInSc3	kyslík
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Krev	krev	k1gFnSc1	krev
hmyzu	hmyz	k1gInSc2	hmyz
přenáší	přenášet	k5eAaImIp3nS	přenášet
živiny	živina	k1gFnPc4	živina
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
odnáší	odnášet	k5eAaImIp3nS	odnášet
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
několika	několik	k4yIc2	několik
typů	typ	k1gInPc2	typ
krvinek	krvinka	k1gFnPc2	krvinka
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
formované	formovaný	k2eAgInPc1d1	formovaný
elementy	element	k1gInPc1	element
krve	krev	k1gFnSc2	krev
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
45	[number]	k4	45
%	%	kIx~	%
celé	celý	k2eAgFnSc2d1	celá
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ostatních	ostatní	k2eAgInPc2d1	ostatní
55	[number]	k4	55
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
–	–	k?	–
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
kapalným	kapalný	k2eAgNnSc7d1	kapalné
mediem	medium	k1gNnSc7	medium
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgFnSc1d1	normální
pH	ph	kA	ph
lidské	lidský	k2eAgFnSc2d1	lidská
arteriální	arteriální	k2eAgFnSc2d1	arteriální
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
7,40	[number]	k4	7,40
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
:	:	kIx,	:
Červené	Červené	k2eAgFnPc1d1	Červené
krvinky	krvinka	k1gFnPc1	krvinka
neboli	neboli	k8xC	neboli
erytrocyty	erytrocyt	k1gInPc1	erytrocyt
(	(	kIx(	(
<g/>
96	[number]	k4	96
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
tyto	tento	k3xDgFnPc4	tento
krvinky	krvinka	k1gFnPc1	krvinka
nemají	mít	k5eNaImIp3nP	mít
buněčné	buněčný	k2eAgNnSc4d1	buněčné
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
organely	organela	k1gFnPc4	organela
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
neplnohodnotné	plnohodnotný	k2eNgFnPc1d1	neplnohodnotná
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hemoglobin	hemoglobin	k1gInSc4	hemoglobin
(	(	kIx(	(
<g/>
červené	červený	k2eAgNnSc4d1	červené
krevní	krevní	k2eAgNnSc4d1	krevní
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
distribuují	distribuovat	k5eAaBmIp3nP	distribuovat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
životnost	životnost	k1gFnSc1	životnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
120	[number]	k4	120
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
endotelními	endotelní	k2eAgFnPc7d1	endotelní
cévními	cévní	k2eAgFnPc7d1	cévní
buňkami	buňka	k1gFnPc7	buňka
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
jinými	jiný	k2eAgFnPc7d1	jiná
buňkami	buňka	k1gFnPc7	buňka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
též	též	k6eAd1	též
označeny	označit	k5eAaPmNgInP	označit
proteiny	protein	k1gInPc7	protein
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
definují	definovat	k5eAaBmIp3nP	definovat
různé	různý	k2eAgInPc1d1	různý
krevní	krevní	k2eAgInPc1d1	krevní
typy	typ	k1gInPc1	typ
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
neboli	neboli	k8xC	neboli
leukocyty	leukocyt	k1gInPc1	leukocyt
(	(	kIx(	(
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
částí	část	k1gFnSc7	část
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
a	a	k8xC	a
eliminují	eliminovat	k5eAaBmIp3nP	eliminovat
původce	původce	k1gMnSc4	původce
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
neboli	neboli	k8xC	neboli
trombocyty	trombocyt	k1gInPc1	trombocyt
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
odpovědny	odpověden	k2eAgFnPc1d1	odpovědna
za	za	k7c4	za
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
neboli	neboli	k8xC	neboli
koagulaci	koagulace	k1gFnSc4	koagulace
(	(	kIx(	(
<g/>
sraženinu	sraženina	k1gFnSc4	sraženina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vodní	vodní	k2eAgInSc1d1	vodní
roztok	roztok	k1gInSc1	roztok
obsahující	obsahující	k2eAgInSc1d1	obsahující
90	[number]	k4	90
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
7	[number]	k4	7
%	%	kIx~	%
plazmatických	plazmatický	k2eAgInPc2d1	plazmatický
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
0,9	[number]	k4	0,9
%	%	kIx~	%
anorganických	anorganický	k2eAgFnPc2d1	anorganická
solí	sůl	k1gFnPc2	sůl
a	a	k8xC	a
roznášené	roznášený	k2eAgFnSc2d1	roznášená
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
a	a	k8xC	a
krvinky	krvinka	k1gFnPc1	krvinka
spolu	spolu	k6eAd1	spolu
tvoří	tvořit	k5eAaImIp3nP	tvořit
nenewtonskou	newtonský	k2eNgFnSc4d1	newtonský
tekutinu	tekutina	k1gFnSc4	tekutina
<g/>
,	,	kIx,	,
proudní	proudní	k2eAgFnPc4d1	proudní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jsou	být	k5eAaImIp3nP	být
unikátně	unikátně	k6eAd1	unikátně
adaptovány	adaptovat	k5eAaBmNgInP	adaptovat
do	do	k7c2	do
architektury	architektura	k1gFnSc2	architektura
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc4d1	krevní
buňky	buňka	k1gFnPc4	buňka
produkuje	produkovat	k5eAaImIp3nS	produkovat
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
<g/>
.	.	kIx.	.
</s>
<s>
Proteinová	proteinový	k2eAgFnSc1d1	proteinová
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
produkována	produkovat	k5eAaImNgFnS	produkovat
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hormony	hormon	k1gInPc1	hormon
produkují	produkovat	k5eAaImIp3nP	produkovat
endokrinní	endokrinní	k2eAgFnPc4d1	endokrinní
žlázy	žláza	k1gFnPc4	žláza
a	a	k8xC	a
vodní	vodní	k2eAgFnPc4d1	vodní
frakce	frakce	k1gFnPc4	frakce
udržované	udržovaný	k2eAgFnPc4d1	udržovaná
v	v	k7c6	v
zažívacím	zažívací	k2eAgInSc6d1	zažívací
traktu	trakt	k1gInSc6	trakt
a	a	k8xC	a
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
v	v	k7c6	v
slezině	slezina	k1gFnSc6	slezina
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
(	(	kIx(	(
<g/>
vychytávané	vychytávaný	k2eAgInPc1d1	vychytávaný
Kupfferovými	Kupfferův	k2eAgFnPc7d1	Kupfferův
buňkami	buňka	k1gFnPc7	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Játra	játra	k1gNnPc1	játra
též	též	k9	též
čistí	čistit	k5eAaImIp3nP	čistit
proteiny	protein	k1gInPc1	protein
a	a	k8xC	a
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
(	(	kIx(	(
<g/>
ledviny	ledvina	k1gFnPc1	ledvina
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
mnoho	mnoho	k4c4	mnoho
malých	malý	k2eAgInPc2d1	malý
proteinů	protein	k1gInPc2	protein
do	do	k7c2	do
moči	moč	k1gFnSc2	moč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrné	úměrný	k2eAgNnSc1d1	úměrné
parciálnímu	parciální	k2eAgInSc3d1	parciální
tlaku	tlak	k1gInSc3	tlak
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
–	–	k?	–
zkratka	zkratka	k1gFnSc1	zkratka
PO2	PO2	k1gFnSc2	PO2
–	–	k?	–
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
je	být	k5eAaImIp3nS	být
primární	primární	k2eAgInSc1d1	primární
transportér	transportér	k1gInSc1	transportér
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
98,5	[number]	k4	98,5
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
kombinováno	kombinovat	k5eAaImNgNnS	kombinovat
s	s	k7c7	s
hemoglobinem	hemoglobin	k1gInSc7	hemoglobin
(	(	kIx(	(
<g/>
Hb	Hb	k1gFnSc1	Hb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
1,5	[number]	k4	1,5
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
fyzikálně	fyzikálně	k6eAd1	fyzikálně
rozpuštěno	rozpustit	k5eAaPmNgNnS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
arteriální	arteriální	k2eAgFnSc1d1	arteriální
(	(	kIx(	(
<g/>
tepenná	tepenný	k2eAgFnSc1d1	tepenná
<g/>
)	)	kIx)	)
krev	krev	k1gFnSc1	krev
proudí	proudit	k5eAaImIp3nS	proudit
přes	přes	k7c4	přes
kapiláry	kapilára	k1gFnPc4	kapilára
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
z	z	k7c2	z
tkáně	tkáň	k1gFnSc2	tkáň
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc1	nějaký
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
hemoglobinem	hemoglobin	k1gInSc7	hemoglobin
pro	pro	k7c4	pro
formování	formování	k1gNnSc4	formování
karbaminohemoglobinu	karbaminohemoglobina	k1gFnSc4	karbaminohemoglobina
<g/>
.	.	kIx.	.
</s>
<s>
Zbylý	zbylý	k2eAgInSc1d1	zbylý
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
konvertován	konvertován	k2eAgInSc1d1	konvertován
na	na	k7c4	na
bikarbonát	bikarbonát	k1gInSc4	bikarbonát
a	a	k8xC	a
vodíkové	vodíkový	k2eAgInPc4d1	vodíkový
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
je	být	k5eAaImIp3nS	být
transportována	transportován	k2eAgFnSc1d1	transportována
krví	krvit	k5eAaImIp3nS	krvit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
iontů	ion	k1gInPc2	ion
bikarbonátu	bikarbonát	k1gInSc2	bikarbonát
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
oxyhemoglobinu	oxyhemoglobina	k1gFnSc4	oxyhemoglobina
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
kyslík	kyslík	k1gInSc1	kyslík
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
deoxyhemoglobin	deoxyhemoglobin	k2eAgMnSc1d1	deoxyhemoglobin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
afinitu	afinita	k1gFnSc4	afinita
k	k	k7c3	k
vodíkovým	vodíkový	k2eAgInPc3d1	vodíkový
iontům	ion	k1gInPc3	ion
H	H	kA	H
<g/>
+	+	kIx~	+
než	než	k8xS	než
oxyhemoglobin	oxyhemoglobin	k2eAgMnSc1d1	oxyhemoglobin
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
váže	vázat	k5eAaImIp3nS	vázat
většinu	většina	k1gFnSc4	většina
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
sání	sání	k1gNnSc2	sání
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
krevsající	krevsající	k2eAgInSc4d1	krevsající
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
roztoči	roztoč	k1gMnPc1	roztoč
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
živočichové	živočich	k1gMnPc1	živočich
se	se	k3xPyFc4	se
vyživují	vyživovat	k5eAaImIp3nP	vyživovat
pouze	pouze	k6eAd1	pouze
či	či	k8xC	či
převážně	převážně	k6eAd1	převážně
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Darování	darování	k1gNnSc1	darování
krve	krev	k1gFnSc2	krev
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
krevní	krevní	k2eAgFnSc1d1	krevní
transfuze	transfuze	k1gFnSc1	transfuze
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
tedy	tedy	k9	tedy
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
oddalovat	oddalovat	k5eAaImF	oddalovat
stárnutí	stárnutí	k1gNnSc4	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgFnSc1d1	specifická
hmotnost	hmotnost	k1gFnSc1	hmotnost
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
hmotnosti	hmotnost	k1gFnSc2	hmotnost
krve	krev	k1gFnSc2	krev
k	k	k7c3	k
objemu	objem	k1gInSc3	objem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tato	tento	k3xDgFnSc1	tento
krev	krev	k1gFnSc4	krev
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
<g/>
.	.	kIx.	.
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
a	a	k8xC	a
velikosti	velikost	k1gFnSc3	velikost
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
<g/>
,	,	kIx,	,
koncentraci	koncentrace	k1gFnSc4	koncentrace
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
a	a	k8xC	a
koncentraci	koncentrace	k1gFnSc4	koncentrace
plasmatických	plasmatický	k2eAgInPc2d1	plasmatický
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Normativní	normativní	k2eAgFnPc1d1	normativní
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
plná	plný	k2eAgFnSc1d1	plná
krev	krev	k1gFnSc1	krev
<g/>
:	:	kIx,	:
1052	[number]	k4	1052
-	-	kIx~	-
1063	[number]	k4	1063
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
plasma	plasma	k1gFnSc1	plasma
<g/>
:	:	kIx,	:
1027	[number]	k4	1027
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
erytrocyty	erytrocyt	k1gInPc7	erytrocyt
<g/>
:	:	kIx,	:
1090	[number]	k4	1090
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
</s>
