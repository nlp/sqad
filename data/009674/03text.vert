<p>
<s>
SIG	SIG	kA	SIG
P220	P220	k1gFnSc1	P220
je	být	k5eAaImIp3nS	být
poloautomatická	poloautomatický	k2eAgFnSc1d1	poloautomatická
pistole	pistole	k1gFnSc1	pistole
navržená	navržený	k2eAgFnSc1d1	navržená
a	a	k8xC	a
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
společností	společnost	k1gFnSc7	společnost
Swiss	Swiss	k1gInSc1	Swiss
Arms	Arms	k1gInSc4	Arms
AG	AG	kA	AG
známou	známá	k1gFnSc7	známá
jako	jako	k8xC	jako
SIG	SIG	kA	SIG
Sauer	Sauer	k1gInSc1	Sauer
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Pistole	pistole	k1gFnSc1	pistole
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Eckernforde	Eckernford	k1gInSc5	Eckernford
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
společností	společnost	k1gFnSc7	společnost
J.	J.	kA	J.
<g/>
P.	P.	kA	P.
Sauer	Sauer	k1gMnSc1	Sauer
und	und	k?	und
Sohn	Sohn	k1gMnSc1	Sohn
GmbH	GmbH	k1gMnSc1	GmbH
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
model	model	k1gInSc1	model
a	a	k8xC	a
model	model	k1gInSc1	model
R	R	kA	R
má	mít	k5eAaImIp3nS	mít
hliníkové	hliníkový	k2eAgNnSc4d1	hliníkové
tělo	tělo	k1gNnSc4	tělo
pokryté	pokrytý	k2eAgNnSc4d1	pokryté
ocelí	ocel	k1gFnSc7	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
modely	model	k1gInPc1	model
bývají	bývat	k5eAaImIp3nP	bývat
celoocelové	celoocelový	k2eAgInPc1d1	celoocelový
<g/>
.	.	kIx.	.
</s>
<s>
Pistole	pistole	k1gFnSc1	pistole
je	být	k5eAaImIp3nS	být
nabízena	nabízet	k5eAaImNgFnS	nabízet
v	v	k7c6	v
ráži	ráže	k1gFnSc6	ráže
9	[number]	k4	9
mm	mm	kA	mm
Luger	Lugra	k1gFnPc2	Lugra
<g/>
,	,	kIx,	,
.38	.38	k4	.38
Super	super	k1gInSc2	super
a	a	k8xC	a
.45	.45	k4	.45
ACP	ACP	kA	ACP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
pouze	pouze	k6eAd1	pouze
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
ráží	ráže	k1gFnSc7	ráže
.45	.45	k4	.45
ACP	ACP	kA	ACP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pistole	pistole	k1gFnSc1	pistole
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
Browningově	browningově	k6eAd1	browningově
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
funkce	funkce	k1gFnPc4	funkce
ověřila	ověřit	k5eAaPmAgFnS	ověřit
již	již	k9	již
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
armádě	armáda	k1gFnSc6	armáda
je	být	k5eAaImIp3nS	být
pistole	pistole	k1gFnSc1	pistole
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
Pistole	pistole	k1gFnSc1	pistole
75	[number]	k4	75
(	(	kIx(	(
P75	P75	k1gFnSc2	P75
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zavedla	zavést	k5eAaPmAgFnS	zavést
tuto	tento	k3xDgFnSc4	tento
pistoli	pistole	k1gFnSc4	pistole
do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
bylo	být	k5eAaImAgNnS	být
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Zavedlo	zavést	k5eAaPmAgNnS	zavést
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
pistole	pistol	k1gFnSc2	pistol
SIG	SIG	kA	SIG
P	P	kA	P
<g/>
226	[number]	k4	226
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pistole	pistol	k1gFnSc2	pistol
je	být	k5eAaImIp3nS	být
198	[number]	k4	198
mm	mm	kA	mm
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
samotné	samotný	k2eAgNnSc4d1	samotné
hlavně	hlavně	k6eAd1	hlavně
je	být	k5eAaImIp3nS	být
112	[number]	k4	112
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
pistole	pistol	k1gFnSc2	pistol
je	být	k5eAaImIp3nS	být
800	[number]	k4	800
gramů	gram	k1gInPc2	gram
v	v	k7c6	v
normálním	normální	k2eAgInSc6d1	normální
a	a	k8xC	a
1130	[number]	k4	1130
gramů	gram	k1gInPc2	gram
v	v	k7c6	v
ocelovém	ocelový	k2eAgNnSc6d1	ocelové
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Ráže	ráže	k1gFnPc1	ráže
zbraně	zbraň	k1gFnPc1	zbraň
jsou	být	k5eAaImIp3nP	být
9	[number]	k4	9
mm	mm	kA	mm
Luger	Lugra	k1gFnPc2	Lugra
<g/>
,	,	kIx,	,
.38	.38	k4	.38
Super	super	k1gInSc2	super
a	a	k8xC	a
.45	.45	k4	.45
ACP	ACP	kA	ACP
a	a	k8xC	a
zásobníky	zásobník	k1gInPc1	zásobník
mají	mít	k5eAaImIp3nP	mít
kapacitu	kapacita	k1gFnSc4	kapacita
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
nebo	nebo	k8xC	nebo
9	[number]	k4	9
nábojů	náboj	k1gInPc2	náboj
v	v	k7c6	v
ráži	ráže	k1gFnSc6	ráže
.45	.45	k4	.45
ACP	ACP	kA	ACP
nebo	nebo	k8xC	nebo
9	[number]	k4	9
nábojů	náboj	k1gInPc2	náboj
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
rážích	ráže	k1gFnPc6	ráže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
Pistole	pistole	k1gFnSc1	pistole
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
variantách	varianta	k1gFnPc6	varianta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
civilní	civilní	k2eAgNnSc4d1	civilní
využití	využití	k1gNnSc4	využití
a	a	k8xC	a
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
SIG	SIG	kA	SIG
Sauer	Sauer	k1gInSc4	Sauer
P220	P220	k1gFnSc7	P220
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
výrobce	výrobce	k1gMnSc2	výrobce
</s>
</p>
<p>
<s>
world	world	k1gInSc1	world
<g/>
.	.	kIx.	.
<g/>
guns	guns	k1gInSc1	guns
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
</s>
</p>
<p>
<s>
The	The	k?	The
SIG	SIG	kA	SIG
P220	P220	k1gFnSc2	P220
in	in	k?	in
film	film	k1gInSc1	film
na	na	k7c6	na
Internetové	internetový	k2eAgFnSc6d1	internetová
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Chris	Chris	k1gInSc1	Chris
Bishop	Bishop	k1gInSc1	Bishop
-	-	kIx~	-
Příruční	příruční	k2eAgFnPc1d1	příruční
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
pěchotních	pěchotní	k2eAgFnPc2d1	pěchotní
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
-	-	kIx~	-
Jota	jota	k1gFnSc1	jota
Military	Militara	k1gFnSc2	Militara
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
-	-	kIx~	-
ISBN	ISBN	kA	ISBN
80-7217-064-3	[number]	k4	80-7217-064-3
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
SIG	SIG	kA	SIG
Sauer	Sauer	k1gInSc4	Sauer
P220	P220	k1gFnSc7	P220
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
