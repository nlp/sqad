<s>
Nůžky	nůžky	k1gFnPc1	nůžky
jsou	být	k5eAaImIp3nP	být
nástroj	nástroj	k1gInSc4	nástroj
nebo	nebo	k8xC	nebo
i	i	k9	i
stroj	stroj	k1gInSc1	stroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
oddělování	oddělování	k1gNnSc4	oddělování
(	(	kIx(	(
<g/>
povětšinou	povětšinou	k6eAd1	povětšinou
plochých	plochý	k2eAgInPc2d1	plochý
či	či	k8xC	či
tenkých	tenký	k2eAgInPc2d1	tenký
<g/>
)	)	kIx)	)
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
(	(	kIx(	(
<g/>
textil	textil	k1gInSc1	textil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plastická	plastický	k2eAgFnSc1d1	plastická
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
nehty	nehet	k1gInPc1	nehet
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
ale	ale	k8xC	ale
i	i	k9	i
plechy	plech	k1gInPc4	plech
či	či	k8xC	či
maso	maso	k1gNnSc4	maso
apod.	apod.	kA	apod.
</s>
