<p>
<s>
Nůžky	nůžky	k1gFnPc1	nůžky
jsou	být	k5eAaImIp3nP	být
nástroj	nástroj	k1gInSc4	nástroj
nebo	nebo	k8xC	nebo
i	i	k9	i
stroj	stroj	k1gInSc1	stroj
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
oddělování	oddělování	k1gNnSc4	oddělování
(	(	kIx(	(
<g/>
povětšinou	povětšinou	k6eAd1	povětšinou
plochých	plochý	k2eAgInPc2d1	plochý
či	či	k8xC	či
tenkých	tenký	k2eAgInPc2d1	tenký
<g/>
)	)	kIx)	)
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
(	(	kIx(	(
<g/>
textil	textil	k1gInSc1	textil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plastická	plastický	k2eAgFnSc1d1	plastická
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
nehty	nehet	k1gInPc1	nehet
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
ale	ale	k8xC	ale
i	i	k9	i
plechy	plech	k1gInPc4	plech
či	či	k8xC	či
maso	maso	k1gNnSc4	maso
apod.	apod.	kA	apod.
</s>
<s>
Rozdělování	rozdělování	k1gNnSc1	rozdělování
materiálu	materiál	k1gInSc2	materiál
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
střihem	střih	k1gInSc7	střih
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
stříhání	stříhání	k1gNnSc4	stříhání
<g/>
.	.	kIx.	.
</s>
<s>
Nůžky	nůžky	k1gFnPc1	nůžky
byly	být	k5eAaImAgFnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vynalezeny	vynaleznout	k5eAaPmNgFnP	vynaleznout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nůžky	nůžky	k1gFnPc4	nůžky
pružinové	pružinový	k2eAgFnPc4d1	pružinová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
moderní	moderní	k2eAgFnPc4d1	moderní
otočné	otočný	k2eAgFnPc4d1	otočná
nůžky	nůžky	k1gFnPc4	nůžky
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
100	[number]	k4	100
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sériově	sériově	k6eAd1	sériově
z	z	k7c2	z
lité	litý	k2eAgFnSc2d1	litá
kalené	kalený	k2eAgFnSc2d1	kalená
oceli	ocel	k1gFnSc2	ocel
je	být	k5eAaImIp3nS	být
začal	začít	k5eAaPmAgMnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
Robert	Robert	k1gMnSc1	Robert
Hinchliffe	Hinchliff	k1gInSc5	Hinchliff
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1761.	[number]	k4	1761.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Mechanika	mechanika	k1gFnSc1	mechanika
nůžek	nůžky	k1gFnPc2	nůžky
==	==	k?	==
</s>
</p>
<p>
<s>
Nůžky	nůžky	k1gFnPc1	nůžky
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
mechanického	mechanický	k2eAgNnSc2d1	mechanické
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc4	nůžky
pružinové	pružinový	k2eAgFnPc4d1	pružinová
–	–	k?	–
čepele	čepel	k1gFnSc2	čepel
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
pružinou	pružina	k1gFnSc7	pružina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
deformace	deformace	k1gFnSc1	deformace
určuje	určovat	k5eAaImIp3nS	určovat
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
pohyb	pohyb	k1gInSc4	pohyb
čepelí	čepel	k1gFnPc2	čepel
</s>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc4	nůžky
otočné	otočný	k2eAgFnPc4d1	otočná
–	–	k?	–
čepele	čepel	k1gFnPc4	čepel
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
otočným	otočný	k2eAgInSc7d1	otočný
čepem	čep	k1gInSc7	čep
</s>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc4	nůžky
suvné	suvný	k2eAgFnPc4d1	suvný
–	–	k?	–
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
pohyb	pohyb	k1gInSc1	pohyb
čepelí	čepel	k1gFnPc2	čepel
je	být	k5eAaImIp3nS	být
přímočarý	přímočarý	k2eAgInSc1d1	přímočarý
</s>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc1	nůžky
padací	padací	k2eAgFnSc2d1	padací
(	(	kIx(	(
<g/>
tabulové	tabulový	k2eAgFnSc2d1	Tabulová
<g/>
)	)	kIx)	)
–	–	k?	–
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
posuvnou	posuvný	k2eAgFnSc7d1	posuvná
čepelí	čepel	k1gFnSc7	čepel
</s>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc4	nůžky
lištové	lištový	k2eAgFnPc4d1	lištová
–	–	k?	–
s	s	k7c7	s
posuvnou	posuvný	k2eAgFnSc7d1	posuvná
ozubenou	ozubený	k2eAgFnSc7d1	ozubená
lištou	lišta	k1gFnSc7	lišta
</s>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc4	nůžky
kruhové	kruhový	k2eAgFnPc4d1	kruhová
–	–	k?	–
čepele	čepel	k1gFnPc4	čepel
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
hranami	hrana	k1gFnPc7	hrana
rotujících	rotující	k2eAgFnPc2d1	rotující
kotoučů	kotouč	k1gInPc2	kotouč
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ruční	ruční	k2eAgFnPc1d1	ruční
nůžky	nůžky	k1gFnPc1	nůžky
==	==	k?	==
</s>
</p>
<p>
<s>
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
čepelí	čepel	k1gFnPc2	čepel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
k	k	k7c3	k
sobě	se	k3xPyFc3	se
těsně	těsně	k6eAd1	těsně
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bývají	bývat	k5eAaImIp3nP	bývat
obě	dva	k4xCgFnPc1	dva
čepele	čepel	k1gFnPc1	čepel
hladké	hladký	k2eAgFnPc1d1	hladká
a	a	k8xC	a
rovné	rovný	k2eAgFnPc1d1	rovná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
vroubkované	vroubkovaný	k2eAgInPc1d1	vroubkovaný
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
nůžky	nůžky	k1gFnPc1	nůžky
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
kuřecí	kuřecí	k2eAgFnSc1d1	kuřecí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
k	k	k7c3	k
oddělování	oddělování	k1gNnSc3	oddělování
šlach	šlacha	k1gFnPc2	šlacha
a	a	k8xC	a
organických	organický	k2eAgInPc2d1	organický
spojů	spoj	k1gInPc2	spoj
od	od	k7c2	od
živočišných	živočišný	k2eAgFnPc2d1	živočišná
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
běžně	běžně	k6eAd1	běžně
používáme	používat	k5eAaImIp1nP	používat
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
nůžky	nůžky	k1gFnPc4	nůžky
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ale	ale	k9	ale
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
masivnější	masivní	k2eAgFnPc1d2	masivnější
a	a	k8xC	a
ostřejší	ostrý	k2eAgFnPc1d2	ostřejší
nůžky	nůžky	k1gFnPc1	nůžky
krejčovské	krejčovský	k2eAgFnPc1d1	krejčovská
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1	speciální
nůžtičky	nůžtičky	k1gFnPc1	nůžtičky
stříhají	stříhat	k5eAaImIp3nP	stříhat
nehty	nehet	k1gInPc4	nehet
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
manikúry	manikúra	k1gFnSc2	manikúra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
speciální	speciální	k2eAgFnPc1d1	speciální
nůžky	nůžky	k1gFnPc1	nůžky
jsou	být	k5eAaImIp3nP	být
nůžky	nůžky	k1gFnPc4	nůžky
kadeřnické	kadeřnický	k2eAgFnPc4d1	kadeřnická
resp.	resp.	kA	resp.
vlasové	vlasový	k2eAgFnPc4d1	vlasová
<g/>
.	.	kIx.	.
</s>
<s>
Zahradnické	zahradnický	k2eAgFnPc1d1	zahradnická
nůžky	nůžky	k1gFnPc1	nůžky
zase	zase	k9	zase
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
oddělování	oddělování	k1gNnSc3	oddělování
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
stonků	stonek	k1gInPc2	stonek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zahradnické	zahradnický	k2eAgFnPc1d1	zahradnická
nůžky	nůžky	k1gFnPc1	nůžky
a	a	k8xC	a
nůžky	nůžky	k1gFnPc1	nůžky
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
pružinovou	pružinový	k2eAgFnSc7d1	pružinová
spirálou	spirála	k1gFnSc7	spirála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
protitlak	protitlak	k1gInSc4	protitlak
vůči	vůči	k7c3	vůči
stisku	stisk	k1gInSc3	stisk
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
větší	veliký	k2eAgFnSc1d2	veliký
síla	síla	k1gFnSc1	síla
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
ocelová	ocelový	k2eAgFnSc1d1	ocelová
spirála	spirála	k1gFnSc1	spirála
také	také	k9	také
udržuje	udržovat	k5eAaImIp3nS	udržovat
tyto	tento	k3xDgFnPc4	tento
nůžky	nůžky	k1gFnPc4	nůžky
otevřené	otevřený	k2eAgFnPc4d1	otevřená
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
pojistku	pojistka	k1gFnSc4	pojistka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
liší	lišit	k5eAaImIp3nP	lišit
provedením	provedení	k1gNnSc7	provedení
zajištění	zajištění	k1gNnSc4	zajištění
od	od	k7c2	od
výrobce	výrobce	k1gMnSc2	výrobce
k	k	k7c3	k
výrobci	výrobce	k1gMnSc3	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
druhem	druh	k1gInSc7	druh
zahradnických	zahradnický	k2eAgFnPc2d1	zahradnická
nůžek	nůžky	k1gFnPc2	nůžky
jsou	být	k5eAaImIp3nP	být
nůžky	nůžky	k1gFnPc1	nůžky
pro	pro	k7c4	pro
zarovnávání	zarovnávání	k1gNnSc4	zarovnávání
živých	živý	k2eAgInPc2d1	živý
plotů	plot	k1gInPc2	plot
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
pákové	pákový	k2eAgFnPc1d1	páková
nůžky	nůžky	k1gFnPc1	nůžky
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
stříhání	stříhání	k1gNnSc3	stříhání
drátů	drát	k1gInPc2	drát
<g/>
,	,	kIx,	,
plechů	plech	k1gInPc2	plech
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
kovových	kovový	k2eAgFnPc2d1	kovová
součástí	součást	k1gFnPc2	součást
(	(	kIx(	(
<g/>
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
ručním	ruční	k2eAgNnSc6d1	ruční
i	i	k8xC	i
stolním	stolní	k2eAgNnSc6d1	stolní
<g/>
)	)	kIx)	)
kupř.	kupř.	kA	kupř.
v	v	k7c6	v
zámečnických	zámečnický	k2eAgFnPc6d1	zámečnická
a	a	k8xC	a
klempířských	klempířský	k2eAgFnPc6d1	klempířská
dílnách	dílna	k1gFnPc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
typy	typ	k1gInPc1	typ
nůžek	nůžky	k1gFnPc2	nůžky
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnPc1d1	speciální
nůžky	nůžky	k1gFnPc1	nůžky
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
zkracoval	zkracovat	k5eAaImAgInS	zkracovat
hořící	hořící	k2eAgInSc1d1	hořící
knot	knot	k1gInSc1	knot
svíčky	svíčka	k1gFnSc2	svíčka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgInP	nazývat
štipec	štipec	k1gInSc4	štipec
či	či	k8xC	či
kratiknot	kratiknot	k1gInSc4	kratiknot
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
opatřené	opatřený	k2eAgInPc1d1	opatřený
malou	malý	k2eAgFnSc7d1	malá
kovovou	kovový	k2eAgFnSc7d1	kovová
skříňkou	skříňka	k1gFnSc7	skříňka
na	na	k7c4	na
hořící	hořící	k2eAgInPc4d1	hořící
ústřižky	ústřižek	k1gInPc4	ústřižek
knotu	knot	k1gInSc2	knot
proti	proti	k7c3	proti
propálení	propálení	k1gNnSc3	propálení
ubrusu	ubrus	k1gInSc2	ubrus
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
malé	malý	k2eAgFnPc4d1	malá
nožičky	nožička	k1gFnPc4	nožička
proti	proti	k7c3	proti
ušpinění	ušpinění	k1gNnSc3	ušpinění
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
svíčky	svíčka	k1gFnPc1	svíčka
představovaly	představovat	k5eAaImAgFnP	představovat
hlavní	hlavní	k2eAgFnPc1d1	hlavní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgInSc1d2	kratší
knot	knot	k1gInSc1	knot
znamenal	znamenat	k5eAaImAgInS	znamenat
menší	malý	k2eAgInSc4d2	menší
plamen	plamen	k1gInSc4	plamen
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
hoření	hoření	k1gNnSc2	hoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nůžky	nůžky	k1gFnPc1	nůžky
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
velikostech	velikost	k1gFnPc6	velikost
i	i	k8xC	i
tvarech	tvar	k1gInPc6	tvar
-	-	kIx~	-
od	od	k7c2	od
miniaturních	miniaturní	k2eAgFnPc2d1	miniaturní
nůžtiček	nůžtičky	k1gFnPc2	nůžtičky
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
a	a	k8xC	a
modelářské	modelářský	k2eAgFnPc4d1	modelářská
nůžky	nůžky	k1gFnPc4	nůžky
až	až	k9	až
po	po	k7c4	po
velké	velký	k2eAgFnPc4d1	velká
dílenské	dílenský	k2eAgFnPc4d1	dílenská
stolní	stolní	k2eAgFnPc4d1	stolní
pákové	pákový	k2eAgFnPc4d1	páková
nůžky	nůžky	k1gFnPc4	nůžky
či	či	k8xC	či
velké	velký	k2eAgFnPc4d1	velká
zahradnické	zahradnický	k2eAgFnPc4d1	zahradnická
nůžky	nůžky	k1gFnPc4	nůžky
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
pracovními	pracovní	k2eAgInPc7d1	pracovní
stroji	stroj	k1gInPc7	stroj
a	a	k8xC	a
nástroji	nástroj	k1gInPc7	nástroj
nůžek	nůžky	k1gFnPc2	nůžky
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
třeba	třeba	k6eAd1	třeba
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
řezačky	řezačka	k1gFnPc4	řezačka
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
ořezávačky	ořezávačka	k1gFnPc4	ořezávačka
fotopapírů	fotopapír	k1gInPc2	fotopapír
či	či	k8xC	či
skartovací	skartovací	k2eAgInPc1d1	skartovací
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
principu	princip	k1gInSc6	princip
oboustranné	oboustranný	k2eAgFnSc2d1	oboustranná
páky	páka	k1gFnSc2	páka
pracují	pracovat	k5eAaImIp3nP	pracovat
též	též	k9	též
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
pracovní	pracovní	k2eAgInPc1d1	pracovní
nástroje	nástroj	k1gInPc1	nástroj
zvané	zvaný	k2eAgFnSc2d1	zvaná
kleště	kleště	k1gFnPc1	kleště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Strojní	strojní	k2eAgFnPc1d1	strojní
nůžky	nůžky	k1gFnPc1	nůžky
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
nůžky	nůžky	k1gFnPc1	nůžky
===	===	k?	===
</s>
</p>
<p>
<s>
Strojní	strojní	k2eAgFnPc1d1	strojní
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
nůžky	nůžky	k1gFnPc1	nůžky
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tvářecích	tvářecí	k2eAgInPc2d1	tvářecí
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc1d1	velké
strojní	strojní	k2eAgNnSc1d1	strojní
(	(	kIx(	(
<g/>
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
pákové	pákový	k2eAgFnPc1d1	páková
<g/>
)	)	kIx)	)
nůžky	nůžky	k1gFnPc1	nůžky
na	na	k7c4	na
stříhání	stříhání	k1gNnSc4	stříhání
plechů	plech	k1gInPc2	plech
(	(	kIx(	(
<g/>
příbuzným	příbuzný	k2eAgNnSc7d1	příbuzné
strojním	strojní	k2eAgNnSc7d1	strojní
zařízením	zařízení	k1gNnSc7	zařízení
je	být	k5eAaImIp3nS	být
lis	lis	k1gInSc1	lis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
stříhat	stříhat	k5eAaImF	stříhat
různé	různý	k2eAgFnPc4d1	různá
součásti	součást	k1gFnPc4	součást
-	-	kIx~	-
vhodný	vhodný	k2eAgInSc1d1	vhodný
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
prostřihování	prostřihování	k1gNnSc4	prostřihování
otvorů	otvor	k1gInPc2	otvor
<g/>
)	)	kIx)	)
<g/>
Kotoučové	kotoučový	k2eAgFnPc4d1	kotoučová
nůžky	nůžky	k1gFnPc4	nůžky
pro	pro	k7c4	pro
tvarové	tvarový	k2eAgNnSc4d1	tvarové
stříhání	stříhání	k1gNnSc4	stříhání
plechu	plech	k1gInSc2	plech
<g/>
,	,	kIx,	,
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
<g />
.	.	kIx.	.
</s>
<s>
protiběžných	protiběžný	k2eAgFnPc2d1	protiběžná
nástrojových	nástrojový	k2eAgFnPc2d1	nástrojová
kol	kola	k1gFnPc2	kola
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
komolého	komolý	k2eAgInSc2d1	komolý
kužele	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
břitem	břit	k1gInSc7	břit
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
(	(	kIx(	(
<g/>
zkosení	zkosení	k1gNnSc4	zkosení
kuželů	kužel	k1gInPc2	kužel
zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
ostrém	ostrý	k2eAgInSc6d1	ostrý
úhlu	úhel	k1gInSc6	úhel
-	-	kIx~	-
břit	břit	k1gInSc1	břit
nástroje	nástroj	k1gInSc2	nástroj
tvoří	tvořit	k5eAaImIp3nS	tvořit
spodní	spodní	k2eAgFnSc1d1	spodní
hrana	hrana	k1gFnSc1	hrana
komolého	komolý	k2eAgInSc2d1	komolý
kužele	kužel	k1gInSc2	kužel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střih	střih	k1gInSc1	střih
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
prováděn	provádět	k5eAaImNgInS	provádět
odvalováním	odvalování	k1gNnSc7	odvalování
stříhaného	stříhaný	k2eAgInSc2d1	stříhaný
materiálu	materiál	k1gInSc2	materiál
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
otočnými	otočný	k2eAgInPc7d1	otočný
břity	břit	k1gInPc7	břit
obou	dva	k4xCgFnPc6	dva
kolBourací	kolBourace	k1gFnSc7	kolBourace
hydraulické	hydraulický	k2eAgFnPc1d1	hydraulická
nůžky	nůžky	k1gFnPc1	nůžky
v	v	k7c6	v
mobilním	mobilní	k2eAgNnSc6d1	mobilní
provedení	provedení	k1gNnSc6	provedení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
např.	např.	kA	např.
při	při	k7c6	při
demolici	demolice	k1gFnSc6	demolice
kovových	kovový	k2eAgFnPc2d1	kovová
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
železobetonových	železobetonový	k2eAgFnPc2d1	železobetonová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
apod.	apod.	kA	apod.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Stříhací	stříhací	k2eAgInPc1d1	stříhací
stroje	stroj	k1gInPc1	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
strojní	strojní	k2eAgFnPc4d1	strojní
nůžky	nůžky	k1gFnPc4	nůžky
resp.	resp.	kA	resp.
střihací	střihací	k2eAgInPc1d1	střihací
stroje	stroj	k1gInPc1	stroj
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
strojní	strojní	k2eAgNnPc1d1	strojní
zařízení	zařízení	k1gNnPc1	zařízení
pracující	pracující	k2eAgNnPc1d1	pracující
na	na	k7c6	na
principu	princip	k1gInSc6	princip
vícebřitých	vícebřitý	k2eAgFnPc2d1	vícebřitý
nůžek	nůžky	k1gFnPc2	nůžky
resp.	resp.	kA	resp.
dvou	dva	k4xCgFnPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
ozubených	ozubený	k2eAgFnPc2d1	ozubená
protiběžných	protiběžný	k2eAgFnPc2d1	protiběžná
stříhacích	stříhací	k2eAgFnPc2d1	stříhací
lišt	lišta	k1gFnPc2	lišta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c4	o
tato	tento	k3xDgNnPc4	tento
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
lištové	lištový	k2eAgFnPc4d1	lištová
sekačky	sekačka	k1gFnPc4	sekačka
na	na	k7c4	na
trávu	tráva	k1gFnSc4	tráva
-	-	kIx~	-
stroje	stroj	k1gInPc1	stroj
bývají	bývat	k5eAaImIp3nP	bývat
konstruované	konstruovaný	k2eAgInPc1d1	konstruovaný
pro	pro	k7c4	pro
pokos	pokos	k1gInSc4	pokos
obilovin	obilovina	k1gFnPc2	obilovina
a	a	k8xC	a
travin	travina	k1gFnPc2	travina
(	(	kIx(	(
<g/>
na	na	k7c6	na
principu	princip	k1gInSc6	princip
lištové	lištový	k2eAgFnSc2d1	lištová
sekačky	sekačka	k1gFnSc2	sekačka
pracují	pracovat	k5eAaImIp3nP	pracovat
i	i	k9	i
samovazací	samovazací	k2eAgInPc1d1	samovazací
stroje	stroj	k1gInPc1	stroj
či	či	k8xC	či
obilní	obilní	k2eAgInPc1d1	obilní
kombajny	kombajn	k1gInPc1	kombajn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lištové	lištový	k2eAgFnPc1d1	lištová
sekačky	sekačka	k1gFnPc1	sekačka
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
lidově	lidově	k6eAd1	lidově
označovány	označovat	k5eAaImNgFnP	označovat
termínem	termín	k1gInSc7	termín
motorová	motorový	k2eAgFnSc1d1	motorová
kosa	kosa	k1gFnSc1	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Lištové	lištový	k2eAgFnPc4d1	lištová
sekačky	sekačka	k1gFnPc4	sekačka
na	na	k7c4	na
trávu	tráva	k1gFnSc4	tráva
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
mívají	mívat	k5eAaImIp3nP	mívat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
pohon	pohon	k1gInSc4	pohon
buďto	buďto	k8xC	buďto
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
dvoutaktní	dvoutaktní	k2eAgInSc1d1	dvoutaktní
zážehový	zážehový	k2eAgInSc1d1	zážehový
motor	motor	k1gInSc1	motor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bývají	bývat	k5eAaImIp3nP	bývat
poháněny	pohánět	k5eAaImNgFnP	pohánět
elektromotorem	elektromotor	k1gInSc7	elektromotor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
sekačky	sekačka	k1gFnPc1	sekačka
hojně	hojně	k6eAd1	hojně
používaly	používat	k5eAaImAgFnP	používat
i	i	k9	i
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
sekačky	sekačka	k1gFnPc1	sekačka
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
poháněny	pohánět	k5eAaImNgInP	pohánět
silou	síla	k1gFnSc7	síla
tažných	tažný	k2eAgNnPc2d1	tažné
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
pomocí	pomocí	k7c2	pomocí
traktorulištové	traktorulištová	k1gFnSc2	traktorulištová
křovinořezy	křovinořez	k1gInPc1	křovinořez
-	-	kIx~	-
stroje	stroj	k1gInPc1	stroj
konstruované	konstruovaný	k2eAgInPc1d1	konstruovaný
pro	pro	k7c4	pro
tvarové	tvarový	k2eAgNnSc4d1	tvarové
zarovnávání	zarovnávání	k1gNnSc4	zarovnávání
či	či	k8xC	či
hromadný	hromadný	k2eAgInSc4d1	hromadný
řez	řez	k1gInSc4	řez
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
<g/>
)	)	kIx)	)
živých	živý	k2eAgInPc2d1	živý
plotů	plot	k1gInPc2	plot
a	a	k8xC	a
větších	veliký	k2eAgInPc2d2	veliký
křovních	křovní	k2eAgInPc2d1	křovní
porostů	porost	k1gInPc2	porost
(	(	kIx(	(
<g/>
kupř.	kupř.	kA	kupř.
podél	podél	k7c2	podél
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
dopravních	dopravní	k2eAgFnPc2d1	dopravní
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
horizontální	horizontální	k2eAgFnSc6d1	horizontální
či	či	k8xC	či
vertikální	vertikální	k2eAgFnSc6d1	vertikální
poloze	poloha	k1gFnSc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
jak	jak	k6eAd1	jak
o	o	k7c4	o
čistě	čistě	k6eAd1	čistě
ruční	ruční	k2eAgInSc4d1	ruční
stroj	stroj	k1gInSc4	stroj
tak	tak	k6eAd1	tak
i	i	k9	i
o	o	k7c4	o
mobilní	mobilní	k2eAgInSc4d1	mobilní
mechanismus	mechanismus	k1gInSc4	mechanismus
namontovaný	namontovaný	k2eAgInSc4d1	namontovaný
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
automobilu	automobil	k1gInSc6	automobil
či	či	k8xC	či
na	na	k7c4	na
traktorustříhací	traktorustříhací	k2eAgInPc4d1	traktorustříhací
strojky	strojek	k1gInPc4	strojek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
v	v	k7c4	v
holičství	holičství	k1gNnSc4	holičství
a	a	k8xC	a
kadeřnictví	kadeřnictví	k1gNnSc4	kadeřnictví
pro	pro	k7c4	pro
stříhání	stříhání	k1gNnSc4	stříhání
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
pro	pro	k7c4	pro
stříhání	stříhání	k1gNnSc4	stříhání
ochlupení	ochlupení	k1gNnSc2	ochlupení
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
strojky	strojek	k1gInPc1	strojek
bývají	bývat	k5eAaImIp3nP	bývat
poháněny	poháněn	k2eAgFnPc4d1	poháněna
vždy	vždy	k6eAd1	vždy
malým	malý	k2eAgInSc7d1	malý
elektromotorkem	elektromotorek	k1gInSc7	elektromotorek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bourací	bourací	k2eAgFnPc1d1	bourací
hydraulické	hydraulický	k2eAgFnPc1d1	hydraulická
nůžky	nůžky	k1gFnPc1	nůžky
</s>
</p>
<p>
<s>
Kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
nůžky	nůžky	k1gFnPc1	nůžky
<g/>
,	,	kIx,	,
papír	papír	k1gInSc1	papír
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nůžky	nůžky	k1gFnPc1	nůžky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nůžky	nůžky	k1gFnPc1	nůžky
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
