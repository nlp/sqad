<s>
Diamant	diamant	k1gInSc1	diamant
je	být	k5eAaImIp3nS	být
nejtvrdší	tvrdý	k2eAgInSc1d3	nejtvrdší
známý	známý	k2eAgInSc1d1	známý
přírodní	přírodní	k2eAgInSc1d1	přírodní
minerál	minerál	k1gInSc1	minerál
(	(	kIx(	(
<g/>
nerost	nerost	k1gInSc1	nerost
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgFnPc2d3	nejtvrdší
látek	látka	k1gFnPc2	látka
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
tvrdší	tvrdý	k2eAgInPc1d2	tvrdší
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
fullerit	fullerit	k1gInSc1	fullerit
<g/>
,	,	kIx,	,
romboedrická	romboedrický	k2eAgFnSc1d1	romboedrická
modifikace	modifikace	k1gFnSc1	modifikace
diamantu	diamant	k1gInSc2	diamant
či	či	k8xC	či
nanokrystalická	nanokrystalický	k2eAgFnSc1d1	nanokrystalický
forma	forma	k1gFnSc1	forma
diamantu	diamant	k1gInSc2	diamant
-	-	kIx~	-
ADNR	ADNR	kA	ADNR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
