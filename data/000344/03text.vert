<s>
Diamant	diamant	k1gInSc1	diamant
je	být	k5eAaImIp3nS	být
nejtvrdší	tvrdý	k2eAgInSc1d3	nejtvrdší
známý	známý	k2eAgInSc1d1	známý
přírodní	přírodní	k2eAgInSc1d1	přírodní
minerál	minerál	k1gInSc1	minerál
(	(	kIx(	(
<g/>
nerost	nerost	k1gInSc1	nerost
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgFnPc2d3	nejtvrdší
látek	látka	k1gFnPc2	látka
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
tvrdší	tvrdý	k2eAgInPc1d2	tvrdší
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
fullerit	fullerit	k1gInSc1	fullerit
<g/>
,	,	kIx,	,
romboedrická	romboedrický	k2eAgFnSc1d1	romboedrická
modifikace	modifikace	k1gFnSc1	modifikace
diamantu	diamant	k1gInSc2	diamant
či	či	k8xC	či
nanokrystalická	nanokrystalický	k2eAgFnSc1d1	nanokrystalický
forma	forma	k1gFnSc1	forma
diamantu	diamant	k1gInSc2	diamant
-	-	kIx~	-
ADNR	ADNR	kA	ADNR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
formu	forma	k1gFnSc4	forma
uhlíku	uhlík	k1gInSc2	uhlík
C.	C.	kA	C.
Tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavně	hlavně	k9	hlavně
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
krystaly	krystal	k1gInPc4	krystal
oktaedrického	oktaedrický	k2eAgNnSc2d1	oktaedrický
<g/>
,	,	kIx,	,
dodekaedrického	dodekaedrický	k2eAgMnSc2d1	dodekaedrický
nebo	nebo	k8xC	nebo
krychlového	krychlový	k2eAgInSc2d1	krychlový
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
je	být	k5eAaImIp3nS	být
vypouklý	vypouklý	k2eAgInSc1d1	vypouklý
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
až	až	k9	až
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
srostlice	srostlice	k1gFnPc1	srostlice
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
šperku	šperk	k1gInSc6	šperk
je	být	k5eAaImIp3nS	být
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
výbrus	výbrus	k1gInSc1	výbrus
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
briliant	briliant	k1gInSc1	briliant
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
diamant	diamant	k1gInSc1	diamant
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ἀ	ἀ	k?	ἀ
[	[	kIx(	[
<g/>
adámas	adámas	k1gMnSc1	adámas
<g/>
]	]	kIx)	]
znamenající	znamenající	k2eAgInSc1d1	znamenající
nezničitelný	zničitelný	k2eNgInSc1d1	nezničitelný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradiční	tradiční	k2eAgFnSc2d1	tradiční
teorie	teorie	k1gFnSc2	teorie
vznikají	vznikat	k5eAaImIp3nP	vznikat
diamanty	diamant	k1gInPc1	diamant
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
zemském	zemský	k2eAgInSc6d1	zemský
plášti	plášť	k1gInSc6	plášť
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
900	[number]	k4	900
<g/>
-	-	kIx~	-
<g/>
1300	[number]	k4	1300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
GPa	GPa	k1gFnPc2	GPa
<g/>
)	)	kIx)	)
v	v	k7c6	v
ultrabazických	ultrabazický	k2eAgFnPc6d1	ultrabazický
vyvřelinách	vyvřelina	k1gFnPc6	vyvřelina
-	-	kIx~	-
kimberlitech	kimberlit	k1gInPc6	kimberlit
<g/>
,	,	kIx,	,
lamproitech	lamproit	k1gInPc6	lamproit
a	a	k8xC	a
komatiitech	komatiit	k1gInPc6	komatiit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
izotopového	izotopový	k2eAgNnSc2d1	izotopové
složení	složení	k1gNnSc2	složení
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
diamantech	diamant	k1gInPc6	diamant
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
harzburgitických	harzburgitický	k2eAgInPc6d1	harzburgitický
diamantech	diamant	k1gInPc6	diamant
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
a	a	k8xC	a
eklogitických	eklogitický	k2eAgInPc6d1	eklogitický
diamantech	diamant	k1gInPc6	diamant
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
podíl	podíl	k1gInSc4	podíl
uhlíku	uhlík	k1gInSc2	uhlík
z	z	k7c2	z
organických	organický	k2eAgInPc2d1	organický
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
hluboké	hluboký	k2eAgFnSc6d1	hluboká
subdukci	subdukce	k1gFnSc6	subdukce
sedimentů	sediment	k1gInPc2	sediment
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
geneze	geneze	k1gFnSc1	geneze
diamantů	diamant	k1gInPc2	diamant
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
diamanty	diamant	k1gInPc1	diamant
v	v	k7c6	v
granulitech	granulit	k1gInPc6	granulit
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
za	za	k7c2	za
podstatně	podstatně	k6eAd1	podstatně
nižších	nízký	k2eAgInPc2d2	nižší
tlaků	tlak	k1gInPc2	tlak
než	než	k8xS	než
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
u	u	k7c2	u
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
nedořešeným	dořešený	k2eNgInSc7d1	nedořešený
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
tmavých	tmavý	k2eAgInPc2d1	tmavý
diamantů	diamant	k1gInPc2	diamant
carbonado	carbonada	k1gFnSc5	carbonada
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
vznikají	vznikat	k5eAaImIp3nP	vznikat
diamanty	diamant	k1gInPc1	diamant
ve	v	k7c6	v
vychladlých	vychladlý	k2eAgFnPc6d1	vychladlá
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc1d3	veliký
takový	takový	k3xDgInSc1	takový
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
bílém	bílý	k1gMnSc6	bílý
trpaslíku	trpaslík	k1gMnSc6	trpaslík
v	v	k7c6	v
systému	systém	k1gInSc6	systém
PSR	PSR	kA	PSR
J	J	kA	J
<g/>
2222	[number]	k4	2222
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
137	[number]	k4	137
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
diamantu	diamant	k1gInSc6	diamant
bývá	bývat	k5eAaImIp3nS	bývat
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
rýhování	rýhování	k1gNnSc1	rýhování
<g/>
,	,	kIx,	,
plochy	plocha	k1gFnPc1	plocha
někdy	někdy	k6eAd1	někdy
naleptány	naleptán	k2eAgFnPc1d1	naleptán
a	a	k8xC	a
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
matné	matný	k2eAgFnPc4d1	matná
<g/>
.	.	kIx.	.
</s>
<s>
Diamant	diamant	k1gInSc1	diamant
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
drahý	drahý	k2eAgInSc1d1	drahý
kámen	kámen	k1gInSc1	kámen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
barevných	barevný	k2eAgFnPc6d1	barevná
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
,	,	kIx,	,
neprůhledný	průhledný	k2eNgInSc4d1	neprůhledný
bort	bort	k1gInSc4	bort
<g/>
,	,	kIx,	,
karbonádo	karbonádo	k1gNnSc4	karbonádo
<g/>
,	,	kIx,	,
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
štěpný	štěpný	k2eAgInSc1d1	štěpný
podle	podle	k7c2	podle
osmistěnu	osmistěn	k1gInSc2	osmistěn
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
tvrdost	tvrdost	k1gFnSc1	tvrdost
10	[number]	k4	10
v	v	k7c6	v
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
140	[number]	k4	140
<g/>
×	×	k?	×
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
než	než	k8xS	než
korund	korund	k1gInSc1	korund
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
tvrdost	tvrdost	k1gFnSc1	tvrdost
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
plochách	plocha	k1gFnPc6	plocha
štěpného	štěpný	k2eAgInSc2d1	štěpný
osmistěnu	osmistěn	k1gInSc2	osmistěn
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
broušení	broušení	k1gNnSc4	broušení
diamantu	diamant	k1gInSc2	diamant
diamantovým	diamantový	k2eAgInSc7d1	diamantový
práškem	prášek	k1gInSc7	prášek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
3,52	[number]	k4	3,52
<g/>
-	-	kIx~	-
<g/>
3,6	[number]	k4	3,6
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc1d1	vysoký
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
-	-	kIx~	-
přes	přes	k7c4	přes
2,4	[number]	k4	2,4
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
moissanit	moissanit	k1gInSc1	moissanit
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgMnSc1d2	vyšší
-	-	kIx~	-
2,65	[number]	k4	2,65
<g/>
-	-	kIx~	-
<g/>
2,69	[number]	k4	2,69
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
vodivost	vodivost	k1gFnSc4	vodivost
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
látek	látka	k1gFnPc2	látka
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
je	být	k5eAaImIp3nS	být
5,5	[number]	k4	5,5
<g/>
.	.	kIx.	.
</s>
<s>
Nevede	vést	k5eNaImIp3nS	vést
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
určitých	určitý	k2eAgFnPc2d1	určitá
nečistot	nečistota	k1gFnPc2	nečistota
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
diamantů	diamant	k1gInPc2	diamant
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
<g/>
.	.	kIx.	.
</s>
<s>
Metrický	metrický	k2eAgInSc1d1	metrický
karát	karát	k1gInSc1	karát
(	(	kIx(	(
<g/>
Carat	Carat	k1gInSc1	Carat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
0,2	[number]	k4	0,2
g	g	kA	g
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
ct	ct	k?	ct
<g/>
.	.	kIx.	.
</s>
<s>
Diamant	diamant	k1gInSc1	diamant
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
g	g	kA	g
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
5	[number]	k4	5
karátů	karát	k1gInPc2	karát
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
v	v	k7c6	v
běžně	běžně	k6eAd1	běžně
prodávaných	prodávaný	k2eAgInPc6d1	prodávaný
špercích	šperk	k1gInPc6	šperk
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
hmotnost	hmotnost	k1gFnSc4	hmotnost
v	v	k7c6	v
setinách	setina	k1gFnPc6	setina
<g/>
,	,	kIx,	,
desetinách	desetina	k1gFnPc6	desetina
až	až	k8xS	až
jednotkách	jednotka	k1gFnPc6	jednotka
karátů	karát	k1gInPc2	karát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
od	od	k7c2	od
hmotnosti	hmotnost	k1gFnSc2	hmotnost
semene	semeno	k1gNnSc2	semeno
svatojánského	svatojánský	k2eAgInSc2d1	svatojánský
chleba	chléb	k1gInSc2	chléb
(	(	kIx(	(
<g/>
Rohovník	rohovník	k1gInSc1	rohovník
obecný	obecný	k2eAgInSc1d1	obecný
-	-	kIx~	-
Ceratonia	Ceratonium	k1gNnSc2	Ceratonium
siliqua	siliqu	k1gInSc2	siliqu
-	-	kIx~	-
Carob	Caroba	k1gFnPc2	Caroba
Tree	Tree	k1gInSc1	Tree
-	-	kIx~	-
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bread	Bread	k1gInSc1	Bread
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
byla	být	k5eAaImAgNnP	být
tradičně	tradičně	k6eAd1	tradičně
používána	používat	k5eAaImNgNnP	používat
v	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
a	a	k8xC	a
Persii	Persie	k1gFnSc6	Persie
jako	jako	k8xC	jako
závaží	závaží	k1gNnSc6	závaží
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
hmotnosti	hmotnost	k1gFnSc2	hmotnost
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
těchto	tento	k3xDgNnPc2	tento
semen	semeno	k1gNnPc2	semeno
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
uniformní	uniformní	k2eAgMnSc1d1	uniformní
a	a	k8xC	a
v	v	k7c6	v
uvedené	uvedený	k2eAgFnSc6d1	uvedená
oblasti	oblast	k1gFnSc6	oblast
světa	svět	k1gInSc2	svět
jsou	být	k5eAaImIp3nP	být
semena	semeno	k1gNnPc4	semeno
běžně	běžně	k6eAd1	běžně
dostupná	dostupný	k2eAgNnPc4d1	dostupné
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
jsou	být	k5eAaImIp3nP	být
oceňovány	oceňovat	k5eAaImNgInP	oceňovat
podle	podle	k7c2	podle
4C	[number]	k4	4C
tříd	třída	k1gFnPc2	třída
<g/>
:	:	kIx,	:
Carat	Carat	k1gInSc1	Carat
-	-	kIx~	-
Váha	váha	k1gFnSc1	váha
<g/>
,	,	kIx,	,
Clarity	Clarit	k1gInPc1	Clarit
-	-	kIx~	-
Čistota	čistota	k1gFnSc1	čistota
<g/>
,	,	kIx,	,
Colour	Colour	k1gMnSc1	Colour
-	-	kIx~	-
Barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
Cut	Cut	k1gFnSc1	Cut
-	-	kIx~	-
Brus	brus	k1gInSc1	brus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barva	barva	k1gFnSc1	barva
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
čistý	čistý	k2eAgInSc1d1	čistý
a	a	k8xC	a
strukturálně	strukturálně	k6eAd1	strukturálně
perfektní	perfektní	k2eAgInSc1d1	perfektní
diamant	diamant	k1gInSc1	diamant
je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
čirý	čirý	k2eAgMnSc1d1	čirý
bez	bez	k7c2	bez
odstínů	odstín	k1gInPc2	odstín
nebo	nebo	k8xC	nebo
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
diamantu	diamant	k1gInSc2	diamant
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
chemickou	chemický	k2eAgFnSc7d1	chemická
nečistotou	nečistota	k1gFnSc7	nečistota
nebo	nebo	k8xC	nebo
strukturálními	strukturální	k2eAgInPc7d1	strukturální
kazy	kaz	k1gInPc7	kaz
v	v	k7c6	v
krystalové	krystalový	k2eAgFnSc6d1	krystalová
mřížce	mřížka	k1gFnSc6	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Sytost	sytost	k1gFnSc1	sytost
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
odstínu	odstín	k1gInSc2	odstín
může	moct	k5eAaImIp3nS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
i	i	k8xC	i
snížit	snížit	k5eAaPmF	snížit
cenu	cena	k1gFnSc4	cena
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
čiré	čirý	k2eAgInPc1d1	čirý
diamanty	diamant	k1gInPc1	diamant
s	s	k7c7	s
odstínem	odstín	k1gInSc7	odstín
žluté	žlutý	k2eAgFnSc2d1	žlutá
nebo	nebo	k8xC	nebo
hnědé	hnědý	k2eAgFnSc2d1	hnědá
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
levnější	levný	k2eAgFnPc1d2	levnější
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
sytě	sytě	k6eAd1	sytě
žluté	žlutý	k2eAgInPc1d1	žlutý
nebo	nebo	k8xC	nebo
modré	modrý	k2eAgInPc1d1	modrý
diamanty	diamant	k1gInPc1	diamant
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
diamant	diamant	k1gInSc1	diamant
Hope	Hop	k1gFnSc2	Hop
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
ceněny	cenit	k5eAaImNgFnP	cenit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
provádí	provádět	k5eAaImIp3nS	provádět
umělá	umělý	k2eAgFnSc1d1	umělá
změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
diamantů	diamant	k1gInPc2	diamant
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
ozařování	ozařování	k1gNnSc1	ozařování
nebo	nebo	k8xC	nebo
žíhání	žíhání	k1gNnSc1	žíhání
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
úpravou	úprava	k1gFnSc7	úprava
(	(	kIx(	(
<g/>
zlepšením	zlepšení	k1gNnSc7	zlepšení
<g/>
)	)	kIx)	)
barvy	barva	k1gFnSc2	barva
je	být	k5eAaImIp3nS	být
bělení	bělení	k1gNnSc1	bělení
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
úpravy	úprava	k1gFnPc1	úprava
barev	barva	k1gFnPc2	barva
diamantů	diamant	k1gInPc2	diamant
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
účel	účel	k1gInSc4	účel
zvýšit	zvýšit	k5eAaPmF	zvýšit
cenu	cena	k1gFnSc4	cena
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Diamanty	diamant	k1gInPc1	diamant
s	s	k7c7	s
upravenou	upravený	k2eAgFnSc7d1	upravená
barvou	barva	k1gFnSc7	barva
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
označeny	označit	k5eAaPmNgInP	označit
jako	jako	k9	jako
upravené	upravený	k2eAgInPc1d1	upravený
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Čistota	čistota	k1gFnSc1	čistota
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Čistotou	čistota	k1gFnSc7	čistota
se	se	k3xPyFc4	se
míní	mínit	k5eAaImIp3nS	mínit
míra	míra	k1gFnSc1	míra
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
defektů	defekt	k1gInPc2	defekt
<g/>
.	.	kIx.	.
</s>
<s>
Můžou	Můžou	k?	Můžou
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
třeba	třeba	k6eAd1	třeba
krystaly	krystal	k1gInPc4	krystal
cizího	cizí	k2eAgInSc2d1	cizí
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
nezkrystalizovaný	zkrystalizovaný	k2eNgInSc4d1	zkrystalizovaný
uhlík	uhlík	k1gInSc4	uhlík
nebo	nebo	k8xC	nebo
strukturální	strukturální	k2eAgInPc4d1	strukturální
nedostatky	nedostatek	k1gInPc4	nedostatek
jako	jako	k8xC	jako
malé	malý	k2eAgFnPc4d1	malá
praskliny	prasklina	k1gFnPc4	prasklina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
bělavý	bělavý	k2eAgInSc4d1	bělavý
nádech	nádech	k1gInSc4	nádech
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
vytěžených	vytěžený	k2eAgInPc2d1	vytěžený
diamantů	diamant	k1gInPc2	diamant
má	mít	k5eAaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
čistotu	čistota	k1gFnSc4	čistota
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
užity	užít	k5eAaPmNgFnP	užít
v	v	k7c4	v
klenotnictví	klenotnictví	k1gNnSc4	klenotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Ostatních	ostatní	k2eAgNnPc2d1	ostatní
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
průmyslově	průmyslově	k6eAd1	průmyslově
jako	jako	k8xS	jako
řezné	řezný	k2eAgInPc4d1	řezný
nástroje	nástroj	k1gInPc4	nástroj
nebo	nebo	k8xC	nebo
brusivo	brusivo	k1gNnSc4	brusivo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Brus	brus	k1gInSc1	brus
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Broušení	broušení	k1gNnSc1	broušení
diamantů	diamant	k1gInPc2	diamant
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc1	umění
i	i	k8xC	i
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
jak	jak	k6eAd1	jak
vytvořit	vytvořit	k5eAaPmF	vytvořit
drahokam	drahokam	k1gInSc4	drahokam
z	z	k7c2	z
hrubého	hrubý	k2eAgInSc2d1	hrubý
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Broušení	broušení	k1gNnSc1	broušení
diamantů	diamant	k1gInPc2	diamant
popisuje	popisovat	k5eAaImIp3nS	popisovat
způsob	způsob	k1gInSc1	způsob
jakým	jaký	k3yQgNnSc7	jaký
byl	být	k5eAaImAgInS	být
hrubý	hrubý	k2eAgInSc4d1	hrubý
kámen	kámen	k1gInSc4	kámen
vybroušen	vybroušen	k2eAgInSc1d1	vybroušen
a	a	k8xC	a
vyleštěn	vyleštěn	k2eAgMnSc1d1	vyleštěn
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
finální	finální	k2eAgFnSc2d1	finální
drahokamové	drahokamový	k2eAgFnSc2d1	drahokamová
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
brus	brus	k1gInSc1	brus
s	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
briliantů	briliant	k1gInPc2	briliant
<g/>
.	.	kIx.	.
</s>
<s>
Techniky	technika	k1gFnPc1	technika
broušení	broušení	k1gNnSc2	broušení
diamantu	diamant	k1gInSc2	diamant
jsou	být	k5eAaImIp3nP	být
zlepšovány	zlepšován	k2eAgInPc1d1	zlepšován
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
zásluh	zásluha	k1gFnPc2	zásluha
na	na	k7c6	na
moderním	moderní	k2eAgInSc6d1	moderní
brusu	brus	k1gInSc6	brus
má	mít	k5eAaImIp3nS	mít
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
drahokamový	drahokamový	k2eAgMnSc1d1	drahokamový
nadšenec	nadšenec	k1gMnSc1	nadšenec
Marcel	Marcel	k1gMnSc1	Marcel
Tolkowski	Tolkowsk	k1gMnSc3	Tolkowsk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
kruhový	kruhový	k2eAgInSc4d1	kruhový
briliantový	briliantový	k2eAgInSc4d1	briliantový
brus	brus	k1gInSc4	brus
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spočítal	spočítat	k5eAaPmAgMnS	spočítat
ideální	ideální	k2eAgInSc4d1	ideální
tvar	tvar	k1gInSc4	tvar
pro	pro	k7c4	pro
maximální	maximální	k2eAgInSc4d1	maximální
zpětný	zpětný	k2eAgInSc4d1	zpětný
odraz	odraz	k1gInSc4	odraz
a	a	k8xC	a
disperzi	disperze	k1gFnSc4	disperze
barev	barva	k1gFnPc2	barva
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
kruhový	kruhový	k2eAgInSc1d1	kruhový
briliant	briliant	k1gInSc1	briliant
má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
57	[number]	k4	57
facet	faceta	k1gFnPc2	faceta
(	(	kIx(	(
<g/>
vybroušených	vybroušený	k2eAgFnPc2d1	vybroušená
plošek	ploška	k1gFnPc2	ploška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
menších	malý	k2eAgInPc2d2	menší
diamantů	diamant	k1gInPc2	diamant
brousí	brousit	k5eAaImIp3nP	brousit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
levná	levný	k2eAgFnSc1d1	levná
lidská	lidský	k2eAgFnSc1d1	lidská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
operací	operace	k1gFnPc2	operace
při	při	k7c6	při
broušení	broušení	k1gNnSc6	broušení
menších	malý	k2eAgInPc2d2	menší
diamantů	diamant	k1gInPc2	diamant
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
zautomatizována	zautomatizován	k2eAgFnSc1d1	zautomatizována
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
diamanty	diamant	k1gInPc1	diamant
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
těží	těžet	k5eAaImIp3nS	těžet
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
povrchových	povrchový	k2eAgInPc6d1	povrchový
lomech	lom	k1gInPc6	lom
<g/>
,	,	kIx,	,
hlubinnou	hlubinný	k2eAgFnSc7d1	hlubinná
důlní	důlní	k2eAgFnSc7d1	důlní
těžbou	těžba	k1gFnSc7	těžba
kimberlitových	kimberlitový	k2eAgInPc2d1	kimberlitový
nebo	nebo	k8xC	nebo
lamproitových	lamproitový	k2eAgInPc2d1	lamproitový
komínů	komín	k1gInPc2	komín
(	(	kIx(	(
<g/>
důl	důl	k1gInSc1	důl
Argyle	Argyl	k1gInSc5	Argyl
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podmořskou	podmořský	k2eAgFnSc7d1	podmořská
těžbou	těžba	k1gFnSc7	těžba
ze	z	k7c2	z
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
vyrobit	vyrobit	k5eAaPmF	vyrobit
umělé	umělý	k2eAgInPc4d1	umělý
diamanty	diamant	k1gInPc4	diamant
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krystalizací	krystalizace	k1gFnSc7	krystalizace
uhlíku	uhlík	k1gInSc2	uhlík
z	z	k7c2	z
kovových	kovový	k2eAgFnPc2d1	kovová
slitin	slitina	k1gFnPc2	slitina
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgInPc2d1	vysoký
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
levná	levný	k2eAgFnSc1d1	levná
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
diamanty	diamant	k1gInPc1	diamant
pro	pro	k7c4	pro
řezné	řezný	k2eAgInPc4d1	řezný
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
brusivo	brusivo	k1gNnSc4	brusivo
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
velkých	velký	k2eAgInPc2d1	velký
syntetických	syntetický	k2eAgInPc2d1	syntetický
diamantů	diamant	k1gInPc2	diamant
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
potenciálně	potenciálně	k6eAd1	potenciálně
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
průmysl	průmysl	k1gInSc1	průmysl
přírodních	přírodní	k2eAgInPc2d1	přírodní
šperkářských	šperkářský	k2eAgInPc2d1	šperkářský
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
efekt	efekt	k1gInSc1	efekt
obecné	obecný	k2eAgFnSc2d1	obecná
dostupnosti	dostupnost	k1gFnSc2	dostupnost
syntetických	syntetický	k2eAgInPc2d1	syntetický
diamantů	diamant	k1gInPc2	diamant
šperkařské	šperkařský	k2eAgFnSc2d1	šperkařská
kvality	kvalita	k1gFnSc2	kvalita
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
přírodních	přírodní	k2eAgInPc2d1	přírodní
diamantů	diamant	k1gInPc2	diamant
lze	lze	k6eAd1	lze
těžko	těžko	k6eAd1	těžko
předvídat	předvídat	k5eAaImF	předvídat
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
a	a	k8xC	a
umělé	umělý	k2eAgFnPc1d1	umělá
(	(	kIx(	(
<g/>
syntetické	syntetický	k2eAgFnPc1d1	syntetická
<g/>
)	)	kIx)	)
diamanty	diamant	k1gInPc4	diamant
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
většinou	většinou	k6eAd1	většinou
spektroskopickými	spektroskopický	k2eAgFnPc7d1	spektroskopická
metodami	metoda	k1gFnPc7	metoda
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
vybavené	vybavený	k2eAgFnSc6d1	vybavená
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
epitaxe	epitaxe	k1gFnSc2	epitaxe
z	z	k7c2	z
molekulárních	molekulární	k2eAgInPc2d1	molekulární
svazků	svazek	k1gInPc2	svazek
se	se	k3xPyFc4	se
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
získávají	získávat	k5eAaImIp3nP	získávat
diamanty	diamant	k1gInPc1	diamant
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
tenkých	tenký	k2eAgFnPc2d1	tenká
destiček	destička	k1gFnPc2	destička
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
umělé	umělý	k2eAgInPc1d1	umělý
diamanty	diamant	k1gInPc1	diamant
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgInPc1d1	drahý
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
použití	použití	k1gNnSc4	použití
např.	např.	kA	např.
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
<g/>
,	,	kIx,	,
elektronice	elektronika	k1gFnSc6	elektronika
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
speciálních	speciální	k2eAgNnPc6d1	speciální
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
diamantů	diamant	k1gInPc2	diamant
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
vynikly	vyniknout	k5eAaPmAgInP	vyniknout
jejich	jejich	k3xOp3gFnPc4	jejich
optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vybrušovány	vybrušovat	k5eAaImNgInP	vybrušovat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
speciálního	speciální	k2eAgInSc2d1	speciální
mnohostěnu	mnohostěn	k1gInSc2	mnohostěn
-	-	kIx~	-
briliantu	briliant	k1gInSc2	briliant
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
diamantu	diamant	k1gInSc2	diamant
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
(	(	kIx(	(
<g/>
ct	ct	k?	ct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
diamantů	diamant	k1gInPc2	diamant
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
spotřeba	spotřeba	k1gFnSc1	spotřeba
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
řezné	řezný	k2eAgInPc4d1	řezný
<g/>
,	,	kIx,	,
vrtné	vrtný	k2eAgInPc4d1	vrtný
a	a	k8xC	a
brusné	brusný	k2eAgInPc4d1	brusný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
prášky	prášek	k1gInPc4	prášek
a	a	k8xC	a
pasty	pasta	k1gFnPc4	pasta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
diamanty	diamant	k1gInPc1	diamant
pro	pro	k7c4	pro
šperkařství	šperkařství	k1gNnSc4	šperkařství
bezcenné	bezcenný	k2eAgNnSc4d1	bezcenné
(	(	kIx(	(
<g/>
špatná	špatný	k2eAgFnSc1d1	špatná
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
špatná	špatný	k2eAgFnSc1d1	špatná
čistota	čistota	k1gFnSc1	čistota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diamantový	diamantový	k2eAgInSc1d1	diamantový
prach	prach	k1gInSc1	prach
a	a	k8xC	a
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
diamanty	diamant	k1gInPc4	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
extrémním	extrémní	k2eAgFnPc3d1	extrémní
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
vlastnostem	vlastnost	k1gFnPc3	vlastnost
se	se	k3xPyFc4	se
diamanty	diamant	k1gInPc1	diamant
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
speciálních	speciální	k2eAgFnPc2d1	speciální
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
a	a	k8xC	a
v	v	k7c6	v
laboratorním	laboratorní	k2eAgInSc6d1	laboratorní
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
naleziště	naleziště	k1gNnPc1	naleziště
diamantů	diamant	k1gInPc2	diamant
jsou	být	k5eAaImIp3nP	být
známa	známo	k1gNnPc1	známo
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Golgonda	Golgonda	k1gFnSc1	Golgonda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
nacházen	nacházet	k5eAaImNgInS	nacházet
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
(	(	kIx(	(
<g/>
stát	stát	k5eAaPmF	stát
Minas	Minas	k1gInSc4	Minas
Gerais	Gerais	k1gFnSc2	Gerais
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc3d1	jižní
Africe	Afrika	k1gFnSc3	Afrika
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
Dlažkovice	Dlažkovice	k1gFnSc1	Dlažkovice
<g/>
,	,	kIx,	,
Chrášťany	Chrášťan	k1gMnPc4	Chrášťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
nález	nález	k1gInSc1	nález
u	u	k7c2	u
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
natavený	natavený	k2eAgInSc1d1	natavený
kousek	kousek	k1gInSc1	kousek
umělého	umělý	k2eAgInSc2d1	umělý
sklo-keramického	skloeramický	k2eAgInSc2d1	sklo-keramický
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
těchto	tento	k3xDgInPc2	tento
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
Českého	český	k2eAgInSc2d1	český
středohoří	středohoří	k1gNnPc4	středohoří
objeveny	objeven	k2eAgFnPc4d1	objevena
mikrodiamanty	mikrodiamanta	k1gFnPc4	mikrodiamanta
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
5	[number]	k4	5
až	až	k9	až
30	[number]	k4	30
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
významná	významný	k2eAgFnSc1d1	významná
světová	světový	k2eAgFnSc1d1	světová
naleziště	naleziště	k1gNnSc1	naleziště
diamantů	diamant	k1gInPc2	diamant
(	(	kIx(	(
<g/>
seřazená	seřazený	k2eAgFnSc1d1	seřazená
podle	podle	k7c2	podle
hodnoty	hodnota	k1gFnSc2	hodnota
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
Botswana	Botswana	k1gFnSc1	Botswana
Rusko	Rusko	k1gNnSc4	Rusko
(	(	kIx(	(
<g/>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
)	)	kIx)	)
Kanada	Kanada	k1gFnSc1	Kanada
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
Angola	Angola	k1gFnSc1	Angola
Namibie	Namibie	k1gFnSc1	Namibie
Austrálie	Austrálie	k1gFnSc2	Austrálie
Napodobeniny	napodobenina	k1gFnSc2	napodobenina
diamantů	diamant	k1gInPc2	diamant
jsou	být	k5eAaImIp3nP	být
minerály	minerál	k1gInPc1	minerál
jako	jako	k8xS	jako
karbid	karbid	k1gInSc1	karbid
křemíku	křemík	k1gInSc2	křemík
(	(	kIx(	(
<g/>
moissanit	moissanit	k1gInSc1	moissanit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kubická	kubický	k2eAgFnSc1d1	kubická
zirkónie	zirkónie	k1gFnSc1	zirkónie
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgInPc1d1	umělý
diamanty	diamant	k1gInPc1	diamant
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
šperkařské	šperkařský	k2eAgInPc4d1	šperkařský
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
účely	účel	k1gInPc4	účel
podle	podle	k7c2	podle
metodiky	metodika	k1gFnSc2	metodika
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klenotnické	klenotnický	k2eAgFnSc2d1	klenotnická
organizace	organizace	k1gFnSc2	organizace
CIBJO	CIBJO	kA	CIBJO
(	(	kIx(	(
<g/>
např.	např.	kA	např.
The	The	k1gMnSc1	The
Diamond	Diamond	k1gMnSc1	Diamond
Book	Book	k1gMnSc1	Book
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
napodobeniny	napodobenina	k1gFnPc4	napodobenina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
fyzikálně	fyzikálně	k6eAd1	fyzikálně
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
diamanty	diamant	k1gInPc4	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Diamant	diamant	k1gInSc1	diamant
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
diamant	diamant	k1gInSc1	diamant
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
DIAMANTY	diamant	k1gInPc1	diamant
-	-	kIx~	-
Příručka	příručka	k1gFnSc1	příručka
hodnocení	hodnocení	k1gNnSc2	hodnocení
diamantů	diamant	k1gInPc2	diamant
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIBJO	CIBJO	kA	CIBJO
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Diamond	Diamond	k1gMnSc1	Diamond
Book	Book	k1gMnSc1	Book
2006-1	[number]	k4	2006-1
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Gemesis	Gemesis	k1gFnSc1	Gemesis
-	-	kIx~	-
Syntetické	syntetický	k2eAgInPc1d1	syntetický
diamanty	diamant	k1gInPc1	diamant
šperkářské	šperkářský	k2eAgFnSc2d1	šperkářský
kvality	kvalita	k1gFnSc2	kvalita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Diamant	diamant	k1gInSc1	diamant
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
World	World	k1gInSc1	World
of	of	k?	of
diamonds	diamonds	k1gInSc1	diamonds
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
5000	[number]	k4	5000
pages	pages	k1gInSc1	pages
in	in	k?	in
50	[number]	k4	50
e-text	eexta	k1gFnPc2	e-texta
historical	historicat	k5eAaPmAgMnS	historicat
references	references	k1gMnSc1	references
Robert	Robert	k1gMnSc1	Robert
Boyle	Boyle	k1gFnSc1	Boyle
<g/>
,	,	kIx,	,
Theophrastus	Theophrastus	k1gMnSc1	Theophrastus
<g/>
,	,	kIx,	,
Albertus	Albertus	k1gMnSc1	Albertus
Magnus	Magnus	k1gMnSc1	Magnus
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Frederick	Frederick	k1gMnSc1	Frederick
Kunz	Kunz	k1gMnSc1	Kunz
<g/>
,	,	kIx,	,
US	US	kA	US
Geological	Geological	k1gMnSc2	Geological
Survey	Survea	k1gMnSc2	Survea
etc	etc	k?	etc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Diamond	Diamond	k1gInSc1	Diamond
-	-	kIx~	-
the	the	k?	the
extreme	extrat	k5eAaImIp1nP	extrat
Kings	Kings	k1gInSc4	Kings
of	of	k?	of
Gems	Gems	k1gInSc1	Gems
</s>
