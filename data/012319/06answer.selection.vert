<s>
Mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
mobil	mobil	k1gInSc1	mobil
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektronické	elektronický	k2eAgNnSc4d1	elektronické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
telefonní	telefonní	k2eAgInPc4d1	telefonní
hovory	hovor	k1gInPc4	hovor
jako	jako	k8xS	jako
normální	normální	k2eAgInSc4d1	normální
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
uživatel	uživatel	k1gMnSc1	uživatel
však	však	k9	však
není	být	k5eNaImIp3nS	být
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
končí	končit	k5eAaImIp3nS	končit
telefonní	telefonní	k2eAgFnSc1d1	telefonní
přípojka	přípojka	k1gFnSc1	přípojka
<g/>
.	.	kIx.	.
</s>
