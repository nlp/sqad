<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1986	[number]	k4	1986
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
konat	konat	k5eAaImF	konat
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
nesplnila	splnit	k5eNaPmAgFnS	splnit
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
<g/>
,	,	kIx,	,
FIFA	FIFA	kA	FIFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
místě	místo	k1gNnSc6	místo
konání	konání	k1gNnSc2	konání
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
24	[number]	k4	24
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
padlo	padnout	k5eAaPmAgNnS	padnout
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
132	[number]	k4	132
branek	branka	k1gFnPc2	branka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
2,5	[number]	k4	2,5
branky	branka	k1gFnSc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
turnaje	turnaj	k1gInSc2	turnaj
s	s	k7c7	s
6	[number]	k4	6
brankami	branka	k1gFnPc7	branka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Gary	Gara	k1gFnSc2	Gara
Lineker	Lineker	k1gInSc1	Lineker
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
:	:	kIx,	:
Diego	Diego	k6eAd1	Diego
Maradona	Maradon	k1gMnSc2	Maradon
(	(	kIx(	(
<g/>
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Harald	Harald	k1gMnSc1	Harald
Schumacher	Schumachra	k1gFnPc2	Schumachra
(	(	kIx(	(
<g/>
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Preben	Preben	k2eAgMnSc1d1	Preben
Elkjæ	Elkjæ	k1gMnSc1	Elkjæ
Larsen	larsena	k1gFnPc2	larsena
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítězným	vítězný	k2eAgInSc7d1	vítězný
týmem	tým	k1gInSc7	tým
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
mužstvo	mužstvo	k1gNnSc1	mužstvo
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
utkání	utkání	k1gNnSc6	utkání
na	na	k7c6	na
Aztéckém	aztécký	k2eAgInSc6d1	aztécký
stadionu	stadion	k1gInSc6	stadion
v	v	k7c4	v
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
porazilo	porazit	k5eAaPmAgNnS	porazit
Německo	Německo	k1gNnSc1	Německo
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stupně	stupeň	k1gInSc2	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
121	[number]	k4	121
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
reprezentací	reprezentace	k1gFnPc2	reprezentace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bojovaly	bojovat	k5eAaImAgFnP	bojovat
o	o	k7c4	o
22	[number]	k4	22
místenek	místenka	k1gFnPc2	místenka
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelské	pořadatelský	k2eAgNnSc1d1	pořadatelské
Mexiko	Mexiko	k1gNnSc1	Mexiko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
-	-	kIx~	-
Itálií	Itálie	k1gFnSc7	Itálie
měli	mít	k5eAaImAgMnP	mít
účast	účast	k1gFnSc4	účast
jistou	jistý	k2eAgFnSc4d1	jistá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kvalifikované	kvalifikovaný	k2eAgInPc1d1	kvalifikovaný
týmy	tým	k1gInPc1	tým
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Skupinová	skupinový	k2eAgFnSc1d1	skupinová
fáze	fáze	k1gFnSc1	fáze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
C	C	kA	C
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
D	D	kA	D
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
E	E	kA	E
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
F	F	kA	F
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Žebříček	žebříček	k1gInSc1	žebříček
týmů	tým	k1gInPc2	tým
na	na	k7c6	na
třetích	třetí	k4xOgNnPc6	třetí
místech	místo	k1gNnPc6	místo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Vyřazovací	vyřazovací	k2eAgFnSc1d1	vyřazovací
fáze	fáze	k1gFnSc1	fáze
==	==	k?	==
</s>
</p>
<p>
<s>
Vyřazovací	vyřazovací	k2eAgFnSc1d1	vyřazovací
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
podle	podle	k7c2	podle
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
pavouka	pavouk	k1gMnSc2	pavouk
play-off	playff	k1gMnSc1	play-off
se	s	k7c7	s
16	[number]	k4	16
týmy	tým	k1gInPc7	tým
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
postoupily	postoupit	k5eAaPmAgFnP	postoupit
z	z	k7c2	z
6	[number]	k4	6
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
fotbalových	fotbalový	k2eAgNnPc6d1	fotbalové
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osmifinále	osmifinále	k1gNnPc3	osmifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1986	[number]	k4	1986
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
webu	web	k1gInSc6	web
FIFA	FIFA	kA	FIFA
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
–	–	k?	–
Maradonova	Maradonův	k2eAgInSc2d1	Maradonův
"	"	kIx"	"
<g/>
boží	boží	k2eAgFnSc1d1	boží
ruka	ruka	k1gFnSc1	ruka
<g/>
"	"	kIx"	"
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Argentiny	Argentina	k1gFnSc2	Argentina
proti	proti	k7c3	proti
Anglii	Anglie	k1gFnSc3	Anglie
na	na	k7c4	na
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
