<p>
<s>
Dřemlík	dřemlík	k1gMnSc1	dřemlík
tundrový	tundrový	k2eAgMnSc1d1	tundrový
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
columbarius	columbarius	k1gMnSc1	columbarius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
sokolovitých	sokolovitý	k2eAgMnPc2d1	sokolovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
24	[number]	k4	24
–	–	k?	–
33	[number]	k4	33
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
53	[number]	k4	53
–	–	k?	–
69	[number]	k4	69
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
samců	samec	k1gMnPc2	samec
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
165	[number]	k4	165
g	g	kA	g
a	a	k8xC	a
u	u	k7c2	u
dospělých	dospělý	k2eAgFnPc2d1	dospělá
samic	samice	k1gFnPc2	samice
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
230	[number]	k4	230
g.	g.	k?	g.
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
malí	malý	k2eAgMnPc1d1	malý
sokoli	sokol	k1gMnPc1	sokol
je	on	k3xPp3gNnSc4	on
i	i	k9	i
dřemlík	dřemlík	k1gMnSc1	dřemlík
tundrový	tundrový	k2eAgMnSc1d1	tundrový
silně	silně	k6eAd1	silně
stavěný	stavěný	k2eAgMnSc1d1	stavěný
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
modrošedý	modrošedý	k2eAgInSc4d1	modrošedý
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
naoranžovělou	naoranžovělý	k2eAgFnSc4d1	naoranžovělá
spodinu	spodina	k1gFnSc4	spodina
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
svrchu	svrchu	k6eAd1	svrchu
tmavě	tmavě	k6eAd1	tmavě
hnědí	hnědit	k5eAaImIp3nS	hnědit
a	a	k8xC	a
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
strany	strana	k1gFnSc2	strana
světlí	světlet	k5eAaImIp3nP	světlet
s	s	k7c7	s
hnědým	hnědý	k2eAgNnSc7d1	hnědé
skvrněním	skvrnění	k1gNnSc7	skvrnění
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
tmavý	tmavý	k2eAgInSc4d1	tmavý
konec	konec	k1gInSc4	konec
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
zejména	zejména	k9	zejména
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
slabých	slabý	k2eAgInPc2d1	slabý
tmavých	tmavý	k2eAgInPc2d1	tmavý
"	"	kIx"	"
<g/>
vousků	vousek	k1gInPc2	vousek
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
sokolů	sokol	k1gMnPc2	sokol
méně	málo	k6eAd2	málo
kontrastní	kontrastní	k2eAgInSc4d1	kontrastní
vzhled	vzhled	k1gInSc4	vzhled
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Dřemlík	dřemlík	k1gMnSc1	dřemlík
tundrový	tundrový	k2eAgMnSc1d1	tundrový
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
severoevropští	severoevropský	k2eAgMnPc1d1	severoevropský
ptáci	pták	k1gMnPc1	pták
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
američtí	americký	k2eAgMnPc1d1	americký
zase	zase	k9	zase
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
krajinách	krajina	k1gFnPc6	krajina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
slatinách	slatina	k1gFnPc6	slatina
<g/>
,	,	kIx,	,
stepích	step	k1gFnPc6	step
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
rozvolněným	rozvolněný	k2eAgInSc7d1	rozvolněný
stromovým	stromový	k2eAgInSc7d1	stromový
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
zalétá	zalétat	k5eAaImIp3nS	zalétat
i	i	k9	i
na	na	k7c4	na
louky	louka	k1gFnPc4	louka
a	a	k8xC	a
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ohroženým	ohrožený	k2eAgMnSc7d1	ohrožený
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
přísně	přísně	k6eAd1	přísně
chráněným	chráněný	k2eAgInSc7d1	chráněný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
početní	početní	k2eAgInPc1d1	početní
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
na	na	k7c6	na
našem	náš	k3xOp1gInSc6	náš
území	území	k1gNnSc6	území
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
90	[number]	k4	90
-	-	kIx~	-
180	[number]	k4	180
jedinci	jedinec	k1gMnPc7	jedinec
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Dřemlík	dřemlík	k1gMnSc1	dřemlík
tundrový	tundrový	k2eAgMnSc1d1	tundrový
loví	lovit	k5eAaImIp3nS	lovit
obvykle	obvykle	k6eAd1	obvykle
malé	malý	k2eAgMnPc4d1	malý
ptáky	pták	k1gMnPc4	pták
do	do	k7c2	do
hmotnosti	hmotnost	k1gFnSc2	hmotnost
40	[number]	k4	40
g.	g.	k?	g.
Ty	ty	k3xPp2nSc1	ty
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
v	v	k7c6	v
nízkém	nízký	k2eAgInSc6d1	nízký
letu	let	k1gInSc6	let
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
drží	držet	k5eAaImIp3nS	držet
méně	málo	k6eAd2	málo
jak	jak	k8xC	jak
metr	metr	k1gInSc4	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejčastější	častý	k2eAgFnSc7d3	nejčastější
kořistí	kořist	k1gFnSc7	kořist
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
skřivani	skřivan	k1gMnPc1	skřivan
<g/>
,	,	kIx,	,
lindušky	linduška	k1gFnPc1	linduška
<g/>
,	,	kIx,	,
jespáci	jespák	k1gMnPc1	jespák
a	a	k8xC	a
vrabci	vrabec	k1gMnPc1	vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
však	však	k9	však
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
ptáka	pták	k1gMnSc4	pták
až	až	k6eAd1	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
holuba	holub	k1gMnSc2	holub
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
v	v	k7c6	v
páru	pár	k1gInSc6	pár
spolu	spolu	k6eAd1	spolu
často	často	k6eAd1	často
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeden	jeden	k4xCgMnSc1	jeden
kořist	kořist	k1gFnSc4	kořist
nažene	nahnat	k5eAaPmIp3nS	nahnat
tomu	ten	k3xDgInSc3	ten
druhému	druhý	k4xOgInSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
především	především	k9	především
vážky	vážka	k1gFnSc2	vážka
a	a	k8xC	a
můry	můra	k1gFnSc2	můra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malé	malý	k2eAgMnPc4d1	malý
savce	savec	k1gMnPc4	savec
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
netopýry	netopýr	k1gMnPc4	netopýr
a	a	k8xC	a
hraboše	hraboš	k1gMnPc4	hraboš
<g/>
)	)	kIx)	)
a	a	k8xC	a
plazy	plaz	k1gInPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
využívá	využívat	k5eAaImIp3nS	využívat
opuštěná	opuštěný	k2eAgNnPc4d1	opuštěné
hnízda	hnízdo	k1gNnPc4	hnízdo
krkavcovitých	krkavcovitý	k2eAgMnPc2d1	krkavcovitý
ptáků	pták	k1gMnPc2	pták
nebo	nebo	k8xC	nebo
jestřábů	jestřáb	k1gMnPc2	jestřáb
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
nebo	nebo	k8xC	nebo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
3	[number]	k4	3
-	-	kIx~	-
5	[number]	k4	5
vajec	vejce	k1gNnPc2	vejce
s	s	k7c7	s
tmavým	tmavý	k2eAgNnSc7d1	tmavé
skvrněním	skvrnění	k1gNnSc7	skvrnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Merlin	Merlin	k2eAgInSc4d1	Merlin
(	(	kIx(	(
<g/>
bird	bird	k1gInSc4	bird
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dungel	Dungel	k1gInSc1	Dungel
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
K.	K.	kA	K.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
str	str	kA	str
<g/>
.	.	kIx.	.
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-0927-2	[number]	k4	978-80-200-0927-2
</s>
</p>
<p>
<s>
Dierschke	Dierschke	k1gFnSc1	Dierschke
<g/>
,	,	kIx,	,
V.	V.	kA	V.
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Robovský	Robovský	k1gMnSc1	Robovský
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
<g/>
;	;	kIx,	;
str	str	kA	str
<g/>
.	.	kIx.	.
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
<s>
Euromedia	Euromedium	k1gNnPc1	Euromedium
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-242-2193-9	[number]	k4	978-80-242-2193-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dřemlík	dřemlík	k1gMnSc1	dřemlík
tundrový	tundrový	k2eAgMnSc1d1	tundrový
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dřemlík	dřemlík	k1gMnSc1	dřemlík
tundrový	tundrový	k2eAgMnSc1d1	tundrový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gMnSc1	BioLib
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Falco	Falco	k1gMnSc1	Falco
columbarius	columbarius	k1gMnSc1	columbarius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
