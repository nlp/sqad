<p>
<s>
Estadio	Estadio	k6eAd1	Estadio
Santiago	Santiago	k1gNnSc1	Santiago
Bernabéu	Bernabéus	k1gInSc2	Bernabéus
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Otevřen	otevřen	k2eAgMnSc1d1	otevřen
byl	být	k5eAaImAgMnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
domácí	domácí	k2eAgInSc4d1	domácí
stadion	stadion	k1gInSc4	stadion
týmu	tým	k1gInSc2	tým
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
předsednictví	předsednictví	k1gNnSc2	předsednictví
ho	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
hráč	hráč	k1gMnSc1	hráč
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
Santiago	Santiago	k1gNnSc1	Santiago
Bernabéu	Bernabéum	k1gNnSc3	Bernabéum
<g/>
.	.	kIx.	.
</s>
<s>
Výstavbou	výstavba	k1gFnSc7	výstavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
chtěl	chtít	k5eAaImAgMnS	chtít
zvýšit	zvýšit	k5eAaPmF	zvýšit
počet	počet	k1gInSc4	počet
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
zisky	zisk	k1gInPc1	zisk
ze	z	k7c2	z
vstupného	vstupné	k1gNnSc2	vstupné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k6eAd1	především
chtěl	chtít	k5eAaImAgMnS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
světový	světový	k2eAgInSc1d1	světový
velkoklub	velkoklub	k1gInSc1	velkoklub
<g/>
.	.	kIx.	.
</s>
<s>
Zadlužil	zadlužit	k5eAaPmAgMnS	zadlužit
proto	proto	k8xC	proto
klub	klub	k1gInSc4	klub
u	u	k7c2	u
Banco	Banco	k6eAd1	Banco
Mercantil	Mercantil	k1gMnPc2	Mercantil
<g/>
,	,	kIx,	,
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
pozemku	pozemek	k1gInSc2	pozemek
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dluhy	dluh	k1gInPc1	dluh
byly	být	k5eAaImAgInP	být
úplně	úplně	k6eAd1	úplně
doplaceny	doplatit	k5eAaPmNgInP	doplatit
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
tohoto	tento	k3xDgNnSc2	tento
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
prezidenta	prezident	k1gMnSc4	prezident
Florentina	Florentin	k1gMnSc4	Florentin
Peréze	Peréza	k1gFnSc6	Peréza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prodal	prodat	k5eAaPmAgInS	prodat
zastaralý	zastaralý	k2eAgInSc4d1	zastaralý
tréninkový	tréninkový	k2eAgInSc4d1	tréninkový
areál	areál	k1gInSc4	areál
Realu	Real	k1gInSc2	Real
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kapacitou	kapacita	k1gFnSc7	kapacita
přesahující	přesahující	k2eAgFnSc7d1	přesahující
81	[number]	k4	81
tisíc	tisíc	k4xCgInSc4	tisíc
diváků	divák	k1gMnPc2	divák
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
šestý	šestý	k4xOgInSc4	šestý
největší	veliký	k2eAgInSc4d3	veliký
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
stadion	stadion	k1gInSc1	stadion
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
podle	podle	k7c2	podle
předchozího	předchozí	k2eAgInSc2d1	předchozí
stadionu	stadion	k1gInSc2	stadion
Realu	Real	k1gInSc2	Real
Estadio	Estadio	k1gMnSc1	Estadio
Chamartín	Chamartín	k1gMnSc1	Chamartín
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
podle	podle	k7c2	podle
Bernabeua	Bernabeu	k1gInSc2	Bernabeu
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
utkání	utkání	k1gNnSc1	utkání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc1	Madrid
a	a	k8xC	a
portugalským	portugalský	k2eAgMnSc7d1	portugalský
CF	CF	kA	CF
Os	osa	k1gFnPc2	osa
Belenenses	Belenensesa	k1gFnPc2	Belenensesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stadion	stadion	k1gInSc1	stadion
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
významně	významně	k6eAd1	významně
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
byly	být	k5eAaImAgInP	být
dostavěny	dostavěn	k2eAgInPc1d1	dostavěn
druhé	druhý	k4xOgInPc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInPc1	třetí
balkóny	balkón	k1gInPc1	balkón
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
tribuně	tribuna	k1gFnSc6	tribuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
před	před	k7c7	před
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
fasáda	fasáda	k1gFnSc1	fasáda
stadionu	stadion	k1gInSc2	stadion
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
tribuny	tribuna	k1gFnPc1	tribuna
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
zastřešení	zastřešení	k1gNnSc2	zastřešení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
stadion	stadion	k1gInSc1	stadion
znova	znova	k6eAd1	znova
rozšiřoval	rozšiřovat	k5eAaImAgInS	rozšiřovat
<g/>
,	,	kIx,	,
přibyla	přibýt	k5eAaPmAgFnS	přibýt
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sekce	sekce	k1gFnSc1	sekce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
stavaři	stavař	k1gMnPc1	stavař
začali	začít	k5eAaPmAgMnP	začít
se	s	k7c7	s
zastřešováním	zastřešování	k1gNnSc7	zastřešování
západní	západní	k2eAgFnSc2d1	západní
tribuny	tribuna	k1gFnSc2	tribuna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gFnPc3	on
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc4d1	významný
zápasy	zápas	k1gInPc4	zápas
na	na	k7c4	na
Santiago	Santiago	k1gNnSc4	Santiago
Bernabéu	Bernabéus	k1gInSc2	Bernabéus
==	==	k?	==
</s>
</p>
<p>
<s>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
–	–	k?	–
finále	finále	k1gNnSc4	finále
Ligy	liga	k1gFnSc2	liga
Mistrů	mistr	k1gMnPc2	mistr
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
–	–	k?	–
finále	finále	k1gNnSc1	finále
ME	ME	kA	ME
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
finále	finále	k1gNnSc1	finále
MS	MS	kA	MS
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
největších	veliký	k2eAgInPc2d3	veliký
evropských	evropský	k2eAgInPc2d1	evropský
stadionů	stadion	k1gInPc2	stadion
podle	podle	k7c2	podle
kapacity	kapacita	k1gFnSc2	kapacita
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Estadio	Estadio	k6eAd1	Estadio
Santiago	Santiago	k1gNnSc1	Santiago
Bernabéu	Bernabéus	k1gInSc2	Bernabéus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.realmadridcf.cz/santiago-bernabeu	[url]	k6eAd1	http://www.realmadridcf.cz/santiago-bernabeu
</s>
</p>
