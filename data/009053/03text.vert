<p>
<s>
Vosa	vosa	k1gFnSc1	vosa
útočná	útočný	k2eAgFnSc1d1	útočná
(	(	kIx(	(
<g/>
Vespula	Vespula	k1gFnSc1	Vespula
germanica	germanica	k1gFnSc1	germanica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zástupce	zástupce	k1gMnSc1	zástupce
společenského	společenský	k2eAgInSc2d1	společenský
blanokřídlého	blanokřídlý	k2eAgInSc2d1	blanokřídlý
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sršňovitých	sršňovití	k1gMnPc2	sršňovití
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
žihadlo	žihadlo	k1gNnSc1	žihadlo
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
opatřeno	opatřit	k5eAaPmNgNnS	opatřit
zpětnými	zpětný	k2eAgInPc7d1	zpětný
háčky	háček	k1gInPc7	háček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
buduje	budovat	k5eAaImIp3nS	budovat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
též	též	k9	též
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
či	či	k8xC	či
trámech	trám	k1gInPc6	trám
různých	různý	k2eAgFnPc2d1	různá
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
podobných	podobný	k2eAgFnPc2d1	podobná
vos	vosa	k1gFnPc2	vosa
obecných	obecný	k2eAgFnPc2d1	obecná
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
odlišíme	odlišit	k5eAaPmIp1nP	odlišit
podle	podle	k7c2	podle
tří	tři	k4xCgFnPc2	tři
oddělených	oddělený	k2eAgFnPc2d1	oddělená
teček	tečka	k1gFnPc2	tečka
na	na	k7c6	na
čelním	čelní	k2eAgInSc6d1	čelní
štítku	štítek	k1gInSc6	štítek
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
dalšímu	další	k2eAgInSc3d1	další
častému	častý	k2eAgInSc3d1	častý
druhu	druh	k1gInSc3	druh
vos	vosa	k1gFnPc2	vosa
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sršňovitých	sršňovití	k1gMnPc2	sršňovití
<g/>
,	,	kIx,	,
vosíkům	vosík	k1gMnPc3	vosík
francouzským	francouzský	k2eAgMnSc7d1	francouzský
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
černá	černá	k1gFnSc1	černá
tykadla	tykadlo	k1gNnSc2	tykadlo
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
baze	baz	k1gFnSc2	baz
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
najdeme	najít	k5eAaPmIp1nP	najít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
vosy	vosa	k1gFnSc2	vosa
obecné	obecný	k2eAgFnSc2d1	obecná
v	v	k7c6	v
Eurasii	Eurasie	k1gFnSc6	Eurasie
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
končin	končina	k1gFnPc2	končina
nových	nový	k2eAgFnPc2d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vosou	vosa	k1gFnSc7	vosa
obecnou	obecný	k2eAgFnSc7d1	obecná
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgNnP	setkat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
však	však	k9	však
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
(	(	kIx(	(
<g/>
tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
vosou	vosa	k1gFnSc7	vosa
obecnou	obecný	k2eAgFnSc7d1	obecná
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
pak	pak	k6eAd1	pak
kolonizovala	kolonizovat	k5eAaBmAgFnS	kolonizovat
Jihoafrickou	jihoafrický	k2eAgFnSc4d1	Jihoafrická
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrační	ilustrační	k2eAgFnSc1d1	ilustrační
mapka	mapka	k1gFnSc1	mapka
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
i	i	k9	i
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chybná	chybný	k2eAgFnSc1d1	chybná
-	-	kIx~	-
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
vosy	vosa	k1gFnSc2	vosa
útočné	útočný	k2eAgFnSc2d1	útočná
nalazneme	nalaznout	k5eAaImIp1nP	nalaznout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Kapsku	Kapsko	k1gNnSc6	Kapsko
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
areál	areál	k1gInSc1	areál
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
rovněž	rovněž	k9	rovněž
občas	občas	k6eAd1	občas
hnízda	hnízdo	k1gNnSc2	hnízdo
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
nezanikají	zanikat	k5eNaImIp3nP	zanikat
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
tedy	tedy	k9	tedy
dorůst	dorůst	k5eAaPmF	dorůst
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc6d2	veliký
velikosti	velikost	k1gFnSc6	velikost
než	než	k8xS	než
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
areálu	areál	k1gInSc6	areál
-	-	kIx~	-
rekordmanem	rekordman	k1gMnSc7	rekordman
bylo	být	k5eAaImAgNnS	být
hnízdo	hnízdo	k1gNnSc1	hnízdo
vysoké	vysoký	k2eAgNnSc1d1	vysoké
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
m	m	kA	m
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
asi	asi	k9	asi
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
komůrek	komůrka	k1gFnPc2	komůrka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŽĎÁREK	ŽĎÁREK	kA	ŽĎÁREK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Hmyzí	hmyzí	k2eAgFnPc1d1	hmyzí
rodiny	rodina	k1gFnPc1	rodina
a	a	k8xC	a
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
582	[number]	k4	582
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2225	[number]	k4	2225
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
OCLC	OCLC	kA	OCLC
857951938	[number]	k4	857951938
LESTER	LESTER	kA	LESTER
<g/>
,	,	kIx,	,
Phil	Phil	k1gMnSc1	Phil
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Vulgar	Vulgar	k1gMnSc1	Vulgar
Wasp	Wasp	k1gMnSc1	Wasp
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
a	a	k8xC	a
Ruthless	Ruthless	k1gInSc1	Ruthless
Invader	Invader	k1gMnSc1	Invader
and	and	k?	and
Ingenious	Ingenious	k1gMnSc1	Ingenious
Predator	Predator	k1gMnSc1	Predator
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9781776561858	[number]	k4	9781776561858
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vosa	vosa	k1gFnSc1	vosa
útočná	útočný	k2eAgFnSc1d1	útočná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
