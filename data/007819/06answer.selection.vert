<s>
Tyto	tento	k3xDgInPc1	tento
receptory	receptor	k1gInPc1	receptor
jsou	být	k5eAaImIp3nP	být
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
v	v	k7c6	v
chuťových	chuťový	k2eAgInPc6d1	chuťový
pohárcích	pohárek	k1gInPc6	pohárek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
v	v	k7c6	v
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
