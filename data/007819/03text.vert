<s>
Chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
vnímat	vnímat	k5eAaImF	vnímat
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
ve	v	k7c6	v
slinách	slina	k1gFnPc6	slina
nebo	nebo	k8xC	nebo
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
existují	existovat	k5eAaImIp3nP	existovat
chuťové	chuťový	k2eAgInPc4d1	chuťový
receptory	receptor	k1gInPc4	receptor
vnímající	vnímající	k2eAgFnSc2d1	vnímající
hořké	hořká	k1gFnSc2	hořká
<g/>
,	,	kIx,	,
sladké	sladký	k2eAgFnSc2d1	sladká
<g/>
,	,	kIx,	,
slané	slaný	k2eAgFnSc2d1	slaná
<g/>
,	,	kIx,	,
kyselé	kyselý	k2eAgFnSc3d1	kyselá
<g/>
,	,	kIx,	,
umami	uma	k1gFnPc7	uma
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
nových	nový	k2eAgInPc2d1	nový
výzkumů	výzkum	k1gInPc2	výzkum
i	i	k8xC	i
"	"	kIx"	"
<g/>
vápníkové	vápníkový	k2eAgInPc1d1	vápníkový
<g/>
"	"	kIx"	"
a	a	k8xC	a
tučné	tučný	k2eAgNnSc4d1	tučné
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zvířata	zvíře	k1gNnPc1	zvíře
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
chuťové	chuťový	k2eAgInPc1d1	chuťový
receptory	receptor	k1gInPc1	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
receptory	receptor	k1gInPc1	receptor
jsou	být	k5eAaImIp3nP	být
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
v	v	k7c6	v
chuťových	chuťový	k2eAgInPc6d1	chuťový
pohárcích	pohárek	k1gInPc6	pohárek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
v	v	k7c6	v
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Chuťových	chuťový	k2eAgInPc2d1	chuťový
pohárků	pohárek	k1gInPc2	pohárek
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
10000	[number]	k4	10000
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgInSc2	tento
poměrně	poměrně	k6eAd1	poměrně
velkého	velký	k2eAgInSc2d1	velký
rozptylu	rozptyl	k1gInSc2	rozptyl
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
citlivost	citlivost	k1gFnSc1	citlivost
chuti	chuť	k1gFnSc2	chuť
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
lidí	člověk	k1gMnPc2	člověk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
značně	značně	k6eAd1	značně
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
chuťových	chuťový	k2eAgInPc2d1	chuťový
pohárků	pohárek	k1gInPc2	pohárek
průměrně	průměrně	k6eAd1	průměrně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
ROZLOŽENÍ	rozložení	k1gNnSc1	rozložení
CHUTI	chuť	k1gFnSc2	chuť
NA	na	k7c6	na
JAZYKU	jazyk	k1gInSc6	jazyk
<g/>
:	:	kIx,	:
Chuťové	chuťový	k2eAgInPc1d1	chuťový
chemoreceptory	chemoreceptor	k1gInPc1	chemoreceptor
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
kuželovitých	kuželovitý	k2eAgFnPc6d1	kuželovitá
papilách	papila	k1gFnPc6	papila
s	s	k7c7	s
tupou	tupý	k2eAgFnSc7d1	tupá
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
papila	papila	k1gFnSc1	papila
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
receptorové	receptorové	k?	receptorové
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
brvy	brva	k1gFnPc4	brva
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
chuť	chuť	k1gFnSc4	chuť
vstupující	vstupující	k2eAgFnSc4d1	vstupující
do	do	k7c2	do
póru	pór	k1gInSc2	pór
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
receptorové	receptorové	k?	receptorové
buňky	buňka	k1gFnPc1	buňka
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
novými	nový	k2eAgMnPc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Papily	papila	k1gFnPc1	papila
vzadu	vzadu	k6eAd1	vzadu
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
chuť	chuť	k1gFnSc4	chuť
hořkou	hořký	k2eAgFnSc4d1	hořká
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
více	hodně	k6eAd2	hodně
vzadu	vzadu	k6eAd1	vzadu
je	být	k5eAaImIp3nS	být
chuť	chuť	k1gFnSc1	chuť
kyselá	kyselý	k2eAgFnSc1d1	kyselá
<g/>
,	,	kIx,	,
kousek	kousek	k1gInSc1	kousek
více	hodně	k6eAd2	hodně
vepředu	vepříst	k5eAaPmIp1nS	vepříst
chuť	chuť	k1gFnSc1	chuť
slaná	slaný	k2eAgFnSc1d1	slaná
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
chuť	chuť	k1gFnSc1	chuť
sladká	sladký	k2eAgFnSc1d1	sladká
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
vnímaní	vnímaný	k2eAgMnPc1d1	vnímaný
chuti	chuť	k1gFnSc2	chuť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
temenním	temenní	k2eAgInSc6d1	temenní
laloku	lalok	k1gInSc6	lalok
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kombinací	kombinace	k1gFnSc7	kombinace
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
tvoří	tvořit	k5eAaImIp3nS	tvořit
výsledná	výsledný	k2eAgFnSc1d1	výsledná
chuť	chuť	k1gFnSc1	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
nejen	nejen	k6eAd1	nejen
složením	složení	k1gNnSc7	složení
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jeho	jeho	k3xOp3gFnSc7	jeho
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
teplá	teplý	k2eAgFnSc1d1	teplá
limonáda	limonáda	k1gFnSc1	limonáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konzistencí	konzistence	k1gFnSc7	konzistence
(	(	kIx(	(
<g/>
mokrý	mokrý	k2eAgInSc1d1	mokrý
chléb	chléb	k1gInSc1	chléb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
předchozím	předchozí	k2eAgInSc7d1	předchozí
vjemem	vjem	k1gInSc7	vjem
(	(	kIx(	(
<g/>
aperitiv	aperitiv	k1gInSc1	aperitiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
vůní	vůně	k1gFnPc2	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
velmi	velmi	k6eAd1	velmi
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
čichem	čich	k1gInSc7	čich
<g/>
.	.	kIx.	.
</s>
<s>
Chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
jsou	být	k5eAaImIp3nP	být
rozloženy	rozložit	k5eAaPmNgInP	rozložit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgFnPc1d1	různá
chutě	chuť	k1gFnPc1	chuť
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
konkrétní	konkrétní	k2eAgFnSc7d1	konkrétní
částí	část	k1gFnSc7	část
jazyka	jazyk	k1gInSc2	jazyk
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chuťový	chuťový	k2eAgInSc4d1	chuťový
vjem	vjem	k1gInSc4	vjem
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
určité	určitý	k2eAgFnPc4d1	určitá
prahové	prahový	k2eAgFnPc4d1	prahová
koncentrace	koncentrace	k1gFnPc4	koncentrace
chuťově	chuťově	k6eAd1	chuťově
aktivní	aktivní	k2eAgFnPc4d1	aktivní
látky	látka	k1gFnPc4	látka
v	v	k7c6	v
pohárcích	pohárek	k1gInPc6	pohárek
<g/>
.	.	kIx.	.
</s>
<s>
Chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
jsou	být	k5eAaImIp3nP	být
omývány	omývat	k5eAaImNgInP	omývat
sekretem	sekret	k1gInSc7	sekret
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
slinných	slinný	k2eAgFnPc2d1	slinná
žláz	žláza	k1gFnPc2	žláza
(	(	kIx(	(
<g/>
Ebnerovy	Ebnerův	k2eAgFnPc1d1	Ebnerův
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
gll	gll	k?	gll
<g/>
.	.	kIx.	.
<g/>
gustatoriae	gustatoriae	k1gFnSc1	gustatoriae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
rozpouštěné	rozpouštěný	k2eAgFnPc1d1	rozpouštěná
molekuly	molekula	k1gFnPc1	molekula
látek	látka	k1gFnPc2	látka
vnímaných	vnímaný	k2eAgFnPc2d1	vnímaná
jako	jako	k8xS	jako
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Kyselost	kyselost	k1gFnSc1	kyselost
je	být	k5eAaImIp3nS	být
zprostředkována	zprostředkovat	k5eAaPmNgFnS	zprostředkovat
protony	proton	k1gInPc7	proton
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slanost	slanost	k1gFnSc1	slanost
ionty	ion	k1gInPc4	ion
anorganických	anorganický	k2eAgFnPc2d1	anorganická
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
sladkost	sladkost	k1gFnSc1	sladkost
převážně	převážně	k6eAd1	převážně
organickými	organický	k2eAgFnPc7d1	organická
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
z	z	k7c2	z
anorganických	anorganický	k2eAgNnPc2d1	anorganické
např.	např.	kA	např.
solemi	sůl	k1gFnPc7	sůl
olova	olovo	k1gNnSc2	olovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
hořkost	hořkost	k1gFnSc1	hořkost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
solemi	sůl	k1gFnPc7	sůl
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
chutě	chuť	k1gFnPc4	chuť
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
práh	práh	k1gInSc1	práh
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
koncentraci	koncentrace	k1gFnSc6	koncentrace
a	a	k8xC	a
době	doba	k1gFnSc6	doba
expozice	expozice	k1gFnSc2	expozice
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
aktivovaných	aktivovaný	k2eAgNnPc2d1	aktivované
čidel	čidlo	k1gNnPc2	čidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
0,08	[number]	k4	0,08
mol	molo	k1gNnPc2	molo
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
na	na	k7c4	na
sladké	sladký	k2eAgNnSc4d1	sladké
(	(	kIx(	(
<g/>
glukóza	glukóza	k1gFnSc1	glukóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slané	slaný	k2eAgFnSc2d1	slaná
=	=	kIx~	=
0,02	[number]	k4	0,02
mol	molo	k1gNnPc2	molo
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gMnSc1	NaCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyselé	kyselé	k1gNnSc1	kyselé
=	=	kIx~	=
0,0001	[number]	k4	0,0001
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc2	HCl
<g/>
,	,	kIx,	,
pH	ph	kA	ph
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
hořké	hořký	k2eAgInPc1d1	hořký
0,000008	[number]	k4	0,000008
mol	molo	k1gNnPc2	molo
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
chininhydrochlorid	chininhydrochlorid	k1gInSc1	chininhydrochlorid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrost	ostrost	k1gFnSc1	ostrost
či	či	k8xC	či
pálivost	pálivost	k1gFnSc1	pálivost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
paprik	paprika	k1gFnPc2	paprika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
pociťována	pociťován	k2eAgFnSc1d1	pociťována
jako	jako	k9	jako
chuťový	chuťový	k2eAgInSc4d1	chuťový
vjem	vjem	k1gInSc4	vjem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nervový	nervový	k2eAgInSc4d1	nervový
signál	signál	k1gInSc4	signál
bolesti	bolest	k1gFnSc2	bolest
<g/>
,	,	kIx,	,
vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
alkaloidem	alkaloid	k1gInSc7	alkaloid
kapsanthinem	kapsanthin	k1gMnSc7	kapsanthin
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
chuťových	chuťový	k2eAgInPc2d1	chuťový
vjemů	vjem	k1gInPc2	vjem
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
chuťový	chuťový	k2eAgInSc4d1	chuťový
kontrast	kontrast	k1gInSc4	kontrast
a	a	k8xC	a
chuťovou	chuťový	k2eAgFnSc4d1	chuťová
kompenzaci	kompenzace	k1gFnSc4	kompenzace
<g/>
.	.	kIx.	.
</s>
<s>
Kompenzace	kompenzace	k1gFnSc1	kompenzace
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zprostředkována	zprostředkovat	k5eAaPmNgFnS	zprostředkovat
cukrem	cukr	k1gInSc7	cukr
u	u	k7c2	u
hořké	hořký	k2eAgFnSc2d1	hořká
chuti	chuť	k1gFnSc2	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
sladkého	sladký	k2eAgNnSc2d1	sladké
jídla	jídlo	k1gNnSc2	jídlo
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
účinek	účinek	k1gInSc4	účinek
kyselosti	kyselost	k1gFnSc2	kyselost
citrónové	citrónový	k2eAgFnSc2d1	citrónová
šťávy	šťáva	k1gFnSc2	šťáva
výraznější	výrazný	k2eAgFnPc1d2	výraznější
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
kontrast	kontrast	k1gInSc4	kontrast
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
smyslů	smysl	k1gInPc2	smysl
je	být	k5eAaImIp3nS	být
rozlišen	rozlišen	k2eAgInSc4d1	rozlišen
vjem	vjem	k1gInSc4	vjem
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
vnímá	vnímat	k5eAaImIp3nS	vnímat
(	(	kIx(	(
<g/>
zvuk	zvuk	k1gInSc1	zvuk
-	-	kIx~	-
sluch	sluch	k1gInSc1	sluch
<g/>
,	,	kIx,	,
pach	pach	k1gInSc1	pach
-	-	kIx~	-
čich	čich	k1gInSc1	čich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
chuti	chuť	k1gFnSc2	chuť
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
tento	tento	k3xDgInSc4	tento
rozdíl	rozdíl	k1gInSc4	rozdíl
nenastává	nastávat	k5eNaImIp3nS	nastávat
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
chuť	chuť	k1gFnSc4	chuť
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
označuje	označovat	k5eAaImIp3nS	označovat
nejen	nejen	k6eAd1	nejen
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vnímaný	vnímaný	k2eAgInSc4d1	vnímaný
podnět	podnět	k1gInSc4	podnět
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
chuti	chuť	k1gFnSc6	chuť
jídla	jídlo	k1gNnSc2	jídlo
či	či	k8xC	či
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
chuť	chuť	k1gFnSc1	chuť
jídla	jídlo	k1gNnSc2	jídlo
vnímána	vnímat	k5eAaImNgFnS	vnímat
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
pachuť	pachuť	k1gFnSc1	pachuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jídle	jídlo	k1gNnSc6	jídlo
obsažena	obsažen	k2eAgFnSc1d1	obsažena
nepatrná	nepatrný	k2eAgFnSc1d1	nepatrná
příměs	příměs	k1gFnSc1	příměs
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
příchuti	příchuť	k1gFnSc6	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
smyslu	smysl	k1gInSc6	smysl
může	moct	k5eAaImIp3nS	moct
chuť	chuť	k1gFnSc4	chuť
označovat	označovat	k5eAaImF	označovat
i	i	k9	i
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
čemkoliv	cokoliv	k3yInSc6	cokoliv
-	-	kIx~	-
např.	např.	kA	např.
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc4	chuť
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc4	chuť
poslouchat	poslouchat	k5eAaImF	poslouchat
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc4	chuť
hrát	hrát	k5eAaImF	hrát
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
chuti	chuť	k1gFnSc6	chuť
na	na	k7c4	na
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
míněna	míněn	k2eAgFnSc1d1	míněna
fyzická	fyzický	k2eAgFnSc1d1	fyzická
žádostivost	žádostivost	k1gFnSc1	žádostivost
-	-	kIx~	-
sexuální	sexuální	k2eAgFnSc1d1	sexuální
touha	touha	k1gFnSc1	touha
<g/>
.	.	kIx.	.
</s>
