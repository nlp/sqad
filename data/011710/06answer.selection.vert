<s>
Vikingové	Viking	k1gMnPc1	Viking
(	(	kIx(	(
<g/>
vikings-muži	vikingsuzat	k5eAaPmIp1nS	vikings-muzat
Fjordů	fjord	k1gInPc2	fjord
<g/>
,	,	kIx,	,
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
vikinský	vikinský	k2eAgMnSc1d1	vikinský
spíše	spíše	k9	spíše
než	než	k8xS	než
vikingský	vikingský	k2eAgInSc1d1	vikingský
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
skandinávští	skandinávský	k2eAgMnPc1d1	skandinávský
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vydávali	vydávat	k5eAaImAgMnP	vydávat
na	na	k7c4	na
"	"	kIx"	"
<g/>
viking	viking	k1gMnSc1	viking
<g/>
"	"	kIx"	"
–	–	k?	–
loupeživé	loupeživý	k2eAgFnSc2d1	loupeživá
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
