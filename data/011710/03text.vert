<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
(	(	kIx(	(
<g/>
vikings-muži	vikingsuzat	k5eAaPmIp1nS	vikings-muzat
Fjordů	fjord	k1gInPc2	fjord
<g/>
,	,	kIx,	,
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
vikinský	vikinský	k2eAgMnSc1d1	vikinský
spíše	spíše	k9	spíše
než	než	k8xS	než
vikingský	vikingský	k2eAgInSc1d1	vikingský
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
skandinávští	skandinávský	k2eAgMnPc1d1	skandinávský
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vydávali	vydávat	k5eAaPmAgMnP	vydávat
na	na	k7c4	na
"	"	kIx"	"
<g/>
viking	viking	k1gMnSc1	viking
<g/>
"	"	kIx"	"
–	–	k?	–
loupeživé	loupeživý	k2eAgFnSc2d1	loupeživá
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konsolidující	konsolidující	k2eAgMnPc4d1	konsolidující
se	se	k3xPyFc4	se
evropské	evropský	k2eAgMnPc4d1	evropský
státy	stát	k1gInPc4	stát
tehdy	tehdy	k6eAd1	tehdy
představovali	představovat	k5eAaImAgMnP	představovat
hrozivé	hrozivý	k2eAgNnSc4d1	hrozivé
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
etnického	etnický	k2eAgInSc2d1	etnický
původu	původ	k1gInSc2	původ
představovali	představovat	k5eAaImAgMnP	představovat
severní	severní	k2eAgFnSc4d1	severní
větev	větev	k1gFnSc4	větev
Germánů	Germán	k1gMnPc2	Germán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
Normany	Norman	k1gMnPc4	Norman
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dány	Dán	k1gMnPc4	Dán
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ascomany	Ascoman	k1gMnPc7	Ascoman
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Varjagy	Varjaga	k1gFnSc2	Varjaga
(	(	kIx(	(
<g/>
Rus	Rus	k1gMnSc1	Rus
<g/>
,	,	kIx,	,
Byzanc	Byzanc	k1gFnSc1	Byzanc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vikingský	vikingský	k2eAgMnSc1d1	vikingský
vůdce	vůdce	k1gMnSc1	vůdce
Erik	Erik	k1gMnSc1	Erik
Rudý	rudý	k1gMnSc1	rudý
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
980	[number]	k4	980
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zřejmě	zřejmě	k6eAd1	zřejmě
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
Vikingů	Viking	k1gMnPc2	Viking
doplul	doplout	k5eAaPmAgMnS	doplout
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
zde	zde	k6eAd1	zde
první	první	k4xOgFnPc1	první
osady	osada	k1gFnPc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Erikův	Erikův	k2eAgMnSc1d1	Erikův
syn	syn	k1gMnSc1	syn
Leif	Leif	k1gMnSc1	Leif
též	též	k9	též
doplul	doplout	k5eAaPmAgMnS	doplout
na	na	k7c4	na
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zažité	zažitý	k2eAgFnPc1d1	zažitá
stereotypní	stereotypní	k2eAgFnPc1d1	stereotypní
představy	představa	k1gFnPc1	představa
o	o	k7c6	o
vikinzích	viking	k1gMnPc6	viking
jako	jako	k8xS	jako
urostlých	urostlý	k2eAgFnPc6d1	urostlá
polonahých	polonahý	k2eAgFnPc6d1	polonahá
válečnících	válečník	k1gMnPc6	válečník
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
vousy	vous	k1gInPc7	vous
a	a	k8xC	a
rohatými	rohatý	k2eAgFnPc7d1	rohatá
přilbami	přilba	k1gFnPc7	přilba
<g/>
,	,	kIx,	,
oděných	oděný	k2eAgFnPc2d1	oděná
jen	jen	k9	jen
v	v	k7c6	v
nezpracovaných	zpracovaný	k2eNgFnPc6d1	nezpracovaná
zvířecích	zvířecí	k2eAgFnPc6d1	zvířecí
kůžích	kůže	k1gFnPc6	kůže
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
hony	hon	k1gInPc4	hon
vzdáleny	vzdálen	k2eAgFnPc1d1	vzdálena
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mylný	mylný	k2eAgInSc4d1	mylný
obraz	obraz	k1gInSc4	obraz
z	z	k7c2	z
éry	éra	k1gFnSc2	éra
romantismu	romantismus	k1gInSc2	romantismus
období	období	k1gNnSc1	období
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
šířený	šířený	k2eAgInSc1d1	šířený
zpočátku	zpočátku	k6eAd1	zpočátku
zejména	zejména	k9	zejména
německými	německý	k2eAgMnPc7d1	německý
romantiky	romantik	k1gMnPc7	romantik
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mylné	mylný	k2eAgFnPc1d1	mylná
záměny	záměna	k1gFnPc1	záměna
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
válečníků	válečník	k1gMnPc2	válečník
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
keltskými	keltský	k2eAgMnPc7d1	keltský
Galy	Gal	k1gMnPc7	Gal
<g/>
,	,	kIx,	,
jistou	jistý	k2eAgFnSc4d1	jistá
roli	role	k1gFnSc4	role
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
ale	ale	k9	ale
mohlo	moct	k5eAaImAgNnS	moct
sehrát	sehrát	k5eAaPmF	sehrát
i	i	k9	i
mylné	mylný	k2eAgNnSc4d1	mylné
pochopení	pochopení	k1gNnSc4	pochopení
germánských	germánský	k2eAgInPc2d1	germánský
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgFnSc3d1	zobrazující
neurčité	určitý	k2eNgFnPc4d1	neurčitá
rohaté	rohatý	k2eAgFnPc4d1	rohatá
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
válečníky	válečník	k1gMnPc4	válečník
nebo	nebo	k8xC	nebo
berserky	berserk	k1gMnPc4	berserk
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgFnP	být
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
rohaté	rohatý	k2eAgFnPc1d1	rohatá
ani	ani	k8xC	ani
okřídlené	okřídlený	k2eAgFnPc1d1	okřídlená
přilby	přilba	k1gFnPc1	přilba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
vikingů	viking	k1gMnPc2	viking
<g/>
.	.	kIx.	.
</s>
<s>
Podstatně	podstatně	k6eAd1	podstatně
staršího	starý	k2eAgNnSc2d2	starší
data	datum	k1gNnSc2	datum
jsou	být	k5eAaImIp3nP	být
přilby	přilba	k1gFnPc4	přilba
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rohy	roh	k1gInPc7	roh
vyhotovené	vyhotovený	k2eAgFnSc2d1	vyhotovená
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Mýty	mýtus	k1gInPc1	mýtus
o	o	k7c6	o
vikinských	vikinský	k2eAgFnPc6d1	vikinská
rohatých	rohatý	k2eAgFnPc6d1	rohatá
helmách	helma	k1gFnPc6	helma
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
zde	zde	k6eAd1	zde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgFnPc1d1	životní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
se	se	k3xPyFc4	se
živilo	živit	k5eAaImAgNnS	živit
převážně	převážně	k6eAd1	převážně
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
a	a	k8xC	a
rybolovem	rybolov	k1gInSc7	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
obživu	obživa	k1gFnSc4	obživa
tvořily	tvořit	k5eAaImAgInP	tvořit
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pojídali	pojídat	k5eAaImAgMnP	pojídat
také	také	k9	také
stravu	strava	k1gFnSc4	strava
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc2	zemědělství
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaImAgMnP	věnovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byly	být	k5eAaImAgFnP	být
přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
(	(	kIx(	(
<g/>
švédské	švédský	k2eAgFnPc1d1	švédská
Skå	Skå	k1gFnPc1	Skå
a	a	k8xC	a
Gotland	Gotland	k1gInSc1	Gotland
Jekaterinsburg	Jekaterinsburg	k1gInSc1	Jekaterinsburg
<g/>
,	,	kIx,	,
norský	norský	k2eAgInSc1d1	norský
Viken	Viken	k1gInSc1	Viken
<g/>
,	,	kIx,	,
Jutský	jutský	k2eAgInSc1d1	jutský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
pak	pak	k6eAd1	pak
muži	muž	k1gMnPc1	muž
vyráželi	vyrážet	k5eAaImAgMnP	vyrážet
za	za	k7c7	za
obchodem	obchod	k1gInSc7	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
starci	stařec	k1gMnPc1	stařec
a	a	k8xC	a
otroci	otrok	k1gMnPc1	otrok
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
doma	doma	k6eAd1	doma
a	a	k8xC	a
starali	starat	k5eAaImAgMnP	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
Vikingů	Viking	k1gMnPc2	Viking
neměly	mít	k5eNaImAgFnP	mít
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
zrovna	zrovna	k6eAd1	zrovna
nízké	nízký	k2eAgNnSc1d1	nízké
postavení	postavení	k1gNnSc1	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
samy	sám	k3xTgInPc1	sám
musely	muset	k5eAaImAgInP	muset
o	o	k7c4	o
všechno	všechen	k3xTgNnSc4	všechen
postarat	postarat	k5eAaPmF	postarat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
byly	být	k5eAaImAgFnP	být
i	i	k9	i
válečnice	válečnice	k1gFnPc1	válečnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Expanze	expanze	k1gFnSc2	expanze
na	na	k7c4	na
jih	jih	k1gInSc4	jih
==	==	k?	==
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vypravili	vypravit	k5eAaPmAgMnP	vypravit
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
plenili	plenit	k5eAaImAgMnP	plenit
pobřeží	pobřeží	k1gNnSc2	pobřeží
Británie	Británie	k1gFnSc2	Británie
(	(	kIx(	(
<g/>
kláštery	klášter	k1gInPc1	klášter
Iona	Ionum	k1gNnSc2	Ionum
a	a	k8xC	a
Lindisfarne	Lindisfarn	k1gInSc5	Lindisfarn
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
pobřeží	pobřeží	k1gNnSc2	pobřeží
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pirátské	pirátský	k2eAgInPc4d1	pirátský
nájezdy	nájezd	k1gInPc4	nájezd
spojovali	spojovat	k5eAaImAgMnP	spojovat
s	s	k7c7	s
obchodem	obchod	k1gInSc7	obchod
–	–	k?	–
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
s	s	k7c7	s
kořistí	kořist	k1gFnSc7	kořist
i	i	k8xC	i
válečnými	válečný	k2eAgMnPc7d1	válečný
zajatci	zajatec	k1gMnPc7	zajatec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důvody	důvod	k1gInPc1	důvod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nedostatek	nedostatek	k1gInSc1	nedostatek
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
</s>
</p>
<p>
<s>
populační	populační	k2eAgInSc1d1	populační
boom	boom	k1gInSc1	boom
Seveřanů	Seveřan	k1gMnPc2	Seveřan
<g/>
,	,	kIx,	,
u	u	k7c2	u
potomků	potomek	k1gMnPc2	potomek
špatné	špatný	k2eAgFnSc2d1	špatná
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
</s>
</p>
<p>
<s>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
velké	velký	k2eAgNnSc4d1	velké
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
Franky	Frank	k1gMnPc7	Frank
a	a	k8xC	a
Dány	Dán	k1gMnPc7	Dán
</s>
</p>
<p>
<s>
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
územní	územní	k2eAgFnSc1d1	územní
expanze	expanze	k1gFnSc1	expanze
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
násilná	násilný	k2eAgFnSc1d1	násilná
christianizace	christianizace	k1gFnSc1	christianizace
pohanů	pohan	k1gMnPc2	pohan
</s>
</p>
<p>
<s>
obchody	obchod	k1gInPc1	obchod
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
bohatá	bohatý	k2eAgFnSc1d1	bohatá
kořist	kořist	k1gFnSc1	kořist
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sociální	sociální	k2eAgMnPc1d1	sociální
změnyVyvinuli	změnyVyvinout	k5eAaPmAgMnP	změnyVyvinout
si	se	k3xPyFc3	se
i	i	k8xC	i
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
lodě	loď	k1gFnSc2	loď
–	–	k?	–
veslice	veslice	k1gFnSc2	veslice
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
přezdívané	přezdívaný	k2eAgInPc4d1	přezdívaný
drakkary	drakkar	k1gInPc4	drakkar
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lodě	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
malý	malý	k2eAgInSc4d1	malý
ponor	ponor	k1gInSc4	ponor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vikingům	viking	k1gMnPc3	viking
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
pronikat	pronikat	k5eAaImF	pronikat
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vikingové	Viking	k1gMnPc1	Viking
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
vikingové	viking	k1gMnPc1	viking
Tunna	Tunna	k1gFnSc1	Tunna
a	a	k8xC	a
Gommon	Gommon	k1gMnSc1	Gommon
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
svatou	svatý	k2eAgFnSc4d1	svatá
Ludmilu	Ludmila	k1gFnSc4	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
testů	test	k1gInPc2	test
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
severský	severský	k2eAgMnSc1d1	severský
válečník	válečník	k1gMnSc1	válečník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vpády	vpád	k1gInPc7	vpád
vikingů	viking	k1gMnPc2	viking
==	==	k?	==
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
lze	lze	k6eAd1	lze
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
tři	tři	k4xCgFnPc4	tři
větve	větev	k1gFnPc4	větev
ještě	ještě	k6eAd1	ještě
jednotného	jednotný	k2eAgInSc2d1	jednotný
národa	národ	k1gInSc2	národ
severských	severský	k2eAgMnPc2d1	severský
Germánů	Germán	k1gMnPc2	Germán
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
směry	směr	k1gInPc4	směr
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
oblasti	oblast	k1gFnPc4	oblast
byly	být	k5eAaImAgFnP	být
geograficky	geograficky	k6eAd1	geograficky
nejbližší	blízký	k2eAgFnPc1d3	nejbližší
a	a	k8xC	a
které	který	k3yRgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
moderním	moderní	k2eAgInSc7d1	moderní
třem	tři	k4xCgInPc3	tři
severským	severský	k2eAgInPc3d1	severský
národům	národ	k1gInPc3	národ
-	-	kIx~	-
Norům	Nor	k1gMnPc3	Nor
<g/>
,	,	kIx,	,
Švédům	Švéd	k1gMnPc3	Švéd
a	a	k8xC	a
Dánům	Dán	k1gMnPc3	Dán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Norští	norský	k2eAgMnPc1d1	norský
vikingové	viking	k1gMnPc1	viking
===	===	k?	===
</s>
</p>
<p>
<s>
Norští	norský	k2eAgMnPc1d1	norský
vikingové	viking	k1gMnPc1	viking
se	se	k3xPyFc4	se
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
také	také	k9	také
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Osídlili	osídlit	k5eAaPmAgMnP	osídlit
severoskotské	severoskotský	k2eAgInPc4d1	severoskotský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
přivlastnili	přivlastnit	k5eAaPmAgMnP	přivlastnit
si	se	k3xPyFc3	se
ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zachytili	zachytit	k5eAaPmAgMnP	zachytit
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
pobřežích	pobřeží	k1gNnPc6	pobřeží
okolo	okolo	k7c2	okolo
Skotského	skotský	k1gInSc2	skotský
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
postupovali	postupovat	k5eAaImAgMnP	postupovat
jak	jak	k8xC	jak
do	do	k7c2	do
anglického	anglický	k2eAgNnSc2d1	anglické
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
zájmy	zájem	k1gInPc1	zájem
střetly	střetnout	k5eAaPmAgInP	střetnout
se	s	k7c7	s
zájmy	zájem	k1gInPc7	zájem
Dánů	Dán	k1gMnPc2	Dán
<g/>
,	,	kIx,	,
postupujících	postupující	k2eAgFnPc2d1	postupující
od	od	k7c2	od
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
mladší	mladý	k2eAgFnSc6d2	mladší
době	doba	k1gFnSc6	doba
vikingů	viking	k1gMnPc2	viking
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
potom	potom	k6eAd1	potom
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
vedeni	vést	k5eAaImNgMnP	vést
náčelníkem	náčelník	k1gInSc7	náčelník
Leifem	Leif	k1gMnSc7	Leif
Erikssonem	Eriksson	k1gMnSc7	Eriksson
byli	být	k5eAaImAgMnP	být
norští	norský	k2eAgMnPc1d1	norský
vikingové	viking	k1gMnPc1	viking
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
985	[number]	k4	985
prvními	první	k4xOgInPc7	první
Evropany	Evropan	k1gMnPc7	Evropan
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
zde	zde	k6eAd1	zde
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
malou	malý	k2eAgFnSc4d1	malá
osadu	osada	k1gFnSc4	osada
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Newfoundlandu	Newfoundland	k1gInSc2	Newfoundland
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
legendární	legendární	k2eAgInSc4d1	legendární
Vinland	Vinland	k1gInSc4	Vinland
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
existovala	existovat	k5eAaImAgFnS	existovat
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vikingové	viking	k1gMnPc1	viking
vydávali	vydávat	k5eAaPmAgMnP	vydávat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k8xC	i
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kam	kam	k6eAd1	kam
až	až	k9	až
dopluli	doplout	k5eAaPmAgMnP	doplout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
odhalit	odhalit	k5eAaPmF	odhalit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
lokalita	lokalita	k1gFnSc1	lokalita
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vikinský	vikinský	k2eAgInSc1d1	vikinský
skanzen	skanzen	k1gInSc1	skanzen
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Anse	Anse	k1gInSc1	Anse
aux	aux	k?	aux
Meadows	Meadows	k1gInSc1	Meadows
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osady	osada	k1gFnPc1	osada
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dánští	dánský	k2eAgMnPc1d1	dánský
vikingové	viking	k1gMnPc1	viking
===	===	k?	===
</s>
</p>
<p>
<s>
Dánští	dánský	k2eAgMnPc1d1	dánský
vikingové	viking	k1gMnPc1	viking
se	se	k3xPyFc4	se
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Pronikli	proniknout	k5eAaPmAgMnP	proniknout
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
norskými	norský	k2eAgMnPc7d1	norský
vikingy	viking	k1gMnPc7	viking
dopluli	doplout	k5eAaPmAgMnP	doplout
až	až	k9	až
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
911	[number]	k4	911
udělil	udělit	k5eAaPmAgMnS	udělit
západofranský	západofranský	k2eAgMnSc1d1	západofranský
král	král	k1gMnSc1	král
normanskému	normanský	k2eAgMnSc3d1	normanský
náčelníkovi	náčelník	k1gMnSc3	náčelník
Rollovi	Roll	k1gMnSc3	Roll
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
hrabství	hrabství	k1gNnSc2	hrabství
Rouen	Rouna	k1gFnPc2	Rouna
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Normandské	normandský	k2eAgNnSc1d1	normandské
vévodství	vévodství	k1gNnSc1	vévodství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
také	také	k9	také
část	část	k1gFnSc4	část
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
britských	britský	k2eAgMnPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
království	království	k1gNnPc4	království
–	–	k?	–
na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
vikinské	vikinský	k2eAgFnSc3d1	vikinská
Danelaw	Danelaw	k1gFnSc3	Danelaw
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
anglosaský	anglosaský	k2eAgInSc4d1	anglosaský
Wessex	Wessex	k1gInSc4	Wessex
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
též	též	k6eAd1	též
zaměřeni	zaměřit	k5eAaPmNgMnP	zaměřit
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1002	[number]	k4	1002
nařídil	nařídit	k5eAaPmAgMnS	nařídit
anglosaský	anglosaský	k2eAgMnSc1d1	anglosaský
král	král	k1gMnSc1	král
Ethelred	Ethelred	k1gMnSc1	Ethelred
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
povražděni	povražděn	k2eAgMnPc1d1	povražděn
všichni	všechen	k3xTgMnPc1	všechen
Dánové	Dán	k1gMnPc1	Dán
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
oblasti	oblast	k1gFnPc4	oblast
Danelawu	Danelawus	k1gInSc2	Danelawus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
dánské	dánský	k2eAgNnSc1d1	dánské
osídlení	osídlení	k1gNnSc1	osídlení
příliš	příliš	k6eAd1	příliš
silné	silný	k2eAgNnSc1d1	silné
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
odrazil	odrazit	k5eAaPmAgMnS	odrazit
anglosaský	anglosaský	k2eAgMnSc1d1	anglosaský
král	král	k1gMnSc1	král
Harold	Harold	k1gMnSc1	Harold
Godwinson	Godwinson	k1gMnSc1	Godwinson
invazi	invaze	k1gFnSc4	invaze
norského	norský	k2eAgMnSc2d1	norský
krále	král	k1gMnSc2	král
Haralda	Harald	k1gMnSc2	Harald
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
poražen	porazit	k5eAaPmNgMnS	porazit
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
normandským	normandský	k2eAgMnSc7d1	normandský
vévodou	vévoda	k1gMnSc7	vévoda
Vilémem	Vilém	k1gMnSc7	Vilém
Dobyvatelem	dobyvatel	k1gMnSc7	dobyvatel
<g/>
,	,	kIx,	,
potomkem	potomek	k1gMnSc7	potomek
vikingského	vikingský	k2eAgMnSc2d1	vikingský
náčelníka	náčelník	k1gMnSc2	náčelník
Rolla	Roll	k1gMnSc2	Roll
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
vlády	vláda	k1gFnPc4	vláda
nad	nad	k7c7	nad
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Švédští	švédský	k2eAgMnPc1d1	švédský
vikingové	viking	k1gMnPc1	viking
(	(	kIx(	(
<g/>
varjagové	varjag	k1gMnPc1	varjag
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Švédští	švédský	k2eAgMnPc1d1	švédský
vikingové	viking	k1gMnPc1	viking
(	(	kIx(	(
<g/>
varjagové	varjag	k1gMnPc1	varjag
<g/>
)	)	kIx)	)
tvořili	tvořit	k5eAaImAgMnP	tvořit
elitní	elitní	k2eAgFnPc4d1	elitní
družiny	družina	k1gFnPc4	družina
ozbrojených	ozbrojený	k2eAgMnPc2d1	ozbrojený
kupců	kupec	k1gMnPc2	kupec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vodních	vodní	k2eAgInPc6d1	vodní
tocích	tok	k1gInPc6	tok
a	a	k8xC	a
plochách	plocha	k1gFnPc6	plocha
pronikali	pronikat	k5eAaImAgMnP	pronikat
z	z	k7c2	z
Finského	finský	k2eAgInSc2d1	finský
nebo	nebo	k8xC	nebo
Rižského	rižský	k2eAgInSc2d1	rižský
zálivu	záliv	k1gInSc2	záliv
k	k	k7c3	k
Volze	Volha	k1gFnSc3	Volha
nebo	nebo	k8xC	nebo
Dněpru	Dněpr	k1gInSc3	Dněpr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
vodních	vodní	k2eAgFnPc6d1	vodní
cestách	cesta	k1gFnPc6	cesta
postupovali	postupovat	k5eAaImAgMnP	postupovat
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
,	,	kIx,	,
po	po	k7c6	po
Volze	Volha	k1gFnSc6	Volha
do	do	k7c2	do
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
po	po	k7c6	po
Dněpru	Dněpr	k1gInSc6	Dněpr
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začali	začít	k5eAaPmAgMnP	začít
varjagové	varjag	k1gMnPc1	varjag
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
osobní	osobní	k2eAgFnSc1d1	osobní
garda	garda	k1gFnSc1	garda
byzantských	byzantský	k2eAgMnPc2d1	byzantský
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
normanské	normanský	k2eAgFnSc2d1	normanská
teorie	teorie	k1gFnSc2	teorie
vzniku	vznik	k1gInSc2	vznik
ruského	ruský	k2eAgInSc2d1	ruský
státu	stát	k1gInSc2	stát
byli	být	k5eAaImAgMnP	být
Varjagové	Varjagový	k2eAgFnPc4d1	Varjagový
zakladateli	zakladatel	k1gMnPc7	zakladatel
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
původní	původní	k2eAgFnSc2d1	původní
panovnické	panovnický	k2eAgFnSc2d1	panovnická
dynastie	dynastie	k1gFnSc2	dynastie
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
carském	carský	k2eAgNnSc6d1	carské
Rusku	Rusko	k1gNnSc6	Rusko
vládla	vládnout	k5eAaImAgFnS	vládnout
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
Rurik	Rurik	k1gMnSc1	Rurik
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
švédský	švédský	k2eAgMnSc1d1	švédský
Varjag	Varjag	k1gMnSc1	Varjag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jómští	Jómský	k2eAgMnPc1d1	Jómský
vikingové	viking	k1gMnPc1	viking
===	===	k?	===
</s>
</p>
<p>
<s>
Jómští	Jómský	k2eAgMnPc1d1	Jómský
vikingové	viking	k1gMnPc1	viking
(	(	kIx(	(
<g/>
také	také	k9	také
jómsvikingové	jómsvikingový	k2eAgInPc1d1	jómsvikingový
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
družina	družina	k1gFnSc1	družina
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
a	a	k8xC	a
banditů	bandita	k1gMnPc2	bandita
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
norských	norský	k2eAgFnPc2d1	norská
ság	sága	k1gFnPc2	sága
<g/>
,	,	kIx,	,
usídlili	usídlit	k5eAaPmAgMnP	usídlit
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zřídili	zřídit	k5eAaPmAgMnP	zřídit
svou	svůj	k3xOyFgFnSc4	svůj
pevnost	pevnost	k1gFnSc4	pevnost
Jomsborg	Jomsborg	k1gInSc1	Jomsborg
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechávali	nechávat	k5eAaImAgMnP	nechávat
se	se	k3xPyFc4	se
najímat	najímat	k5eAaImF	najímat
do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
armád	armáda	k1gFnPc2	armáda
kohokoli	kdokoli	k3yInSc2	kdokoli
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
ochotný	ochotný	k2eAgMnSc1d1	ochotný
jim	on	k3xPp3gMnPc3	on
za	za	k7c4	za
jejich	jejich	k3xOp3gFnPc4	jejich
služby	služba	k1gFnPc4	služba
patřičně	patřičně	k6eAd1	patřičně
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
věrnými	věrný	k2eAgMnPc7d1	věrný
pohany	pohan	k1gMnPc7	pohan
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
bojovali	bojovat	k5eAaImAgMnP	bojovat
i	i	k9	i
po	po	k7c6	po
boku	bok	k1gInSc6	bok
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejednou	jednou	k6eNd1	jednou
však	však	k9	však
museli	muset	k5eAaImAgMnP	muset
čelit	čelit	k5eAaImF	čelit
jejich	jejich	k3xOp3gFnSc3	jejich
expanzivní	expanzivní	k2eAgFnSc3d1	expanzivní
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
se	se	k3xPyFc4	se
střetávali	střetávat	k5eAaImAgMnP	střetávat
s	s	k7c7	s
vojsky	vojsky	k6eAd1	vojsky
skandinávských	skandinávský	k2eAgMnPc2d1	skandinávský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
Magnus	Magnus	k1gMnSc1	Magnus
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1024	[number]	k4	1024
<g/>
-	-	kIx~	-
<g/>
1047	[number]	k4	1047
<g/>
)	)	kIx)	)
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
jómskými	jómský	k2eAgMnPc7d1	jómský
vikingy	viking	k1gMnPc7	viking
definitivně	definitivně	k6eAd1	definitivně
skoncuje	skoncovat	k5eAaPmIp3nS	skoncovat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1043	[number]	k4	1043
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
jejich	jejich	k3xOp3gFnSc4	jejich
pevnost	pevnost	k1gFnSc4	pevnost
Jomsborg	Jomsborg	k1gMnSc1	Jomsborg
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
konec	konec	k1gInSc1	konec
jómských	jómský	k2eAgMnPc2d1	jómský
vikingů	viking	k1gMnPc2	viking
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
poloha	poloha	k1gFnSc1	poloha
pevnosti	pevnost	k1gFnSc2	pevnost
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
bývá	bývat	k5eAaImIp3nS	bývat
umisťována	umisťován	k2eAgFnSc1d1	umisťován
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Wolin	Wolin	k2eAgInSc4d1	Wolin
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
cípu	cíp	k1gInSc6	cíp
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vikinská	vikinský	k2eAgFnSc1d1	vikinská
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
se	se	k3xPyFc4	se
dělili	dělit	k5eAaImAgMnP	dělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
stavů	stav	k1gInPc2	stav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Otroci	otrok	k1gMnPc1	otrok
–	–	k?	–
do	do	k7c2	do
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
skupiny	skupina	k1gFnSc2	skupina
patřili	patřit	k5eAaImAgMnP	patřit
otroci	otrok	k1gMnPc1	otrok
(	(	kIx(	(
<g/>
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
otroci	otrok	k1gMnPc1	otrok
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedláci	Sedlák	k1gMnPc1	Sedlák
–	–	k?	–
selský	selský	k2eAgInSc1d1	selský
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
nehospodařili	hospodařit	k5eNaImAgMnP	hospodařit
na	na	k7c6	na
velkých	velký	k2eAgInPc6d1	velký
pozemcích	pozemek	k1gInPc6	pozemek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
patřili	patřit	k5eAaImAgMnP	patřit
také	také	k9	také
řemeslníci	řemeslník	k1gMnPc1	řemeslník
a	a	k8xC	a
válečníci	válečník	k1gMnPc1	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jarla	jarl	k1gMnSc2	jarl
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Podnikali	podnikat	k5eAaImAgMnP	podnikat
takto	takto	k6eAd1	takto
velké	velký	k2eAgFnPc4d1	velká
zámořské	zámořský	k2eAgFnPc4d1	zámořská
výpravy	výprava	k1gFnPc4	výprava
(	(	kIx(	(
<g/>
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
výše	výše	k1gFnPc4	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jarlové	jarl	k1gMnPc1	jarl
–	–	k?	–
jakási	jakýsi	k3yIgFnSc1	jakýsi
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
velkostatkáři	velkostatkář	k1gMnPc1	velkostatkář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
docela	docela	k6eAd1	docela
slušný	slušný	k2eAgInSc4d1	slušný
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
si	se	k3xPyFc3	se
také	také	k9	také
mohli	moct	k5eAaImAgMnP	moct
udržet	udržet	k5eAaPmF	udržet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
družinu	družina	k1gFnSc4	družina
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
i	i	k9	i
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
uplatňovali	uplatňovat	k5eAaImAgMnP	uplatňovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Králové	Král	k1gMnPc1	Král
–	–	k?	–
Krále	Král	k1gMnSc2	Král
si	se	k3xPyFc3	se
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
volili	volit	k5eAaImAgMnP	volit
náčelníci	náčelník	k1gMnPc1	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
mocí	moc	k1gFnSc7	moc
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Tzn.	tzn.	kA	tzn.
shromáždění	shromáždění	k1gNnSc1	shromáždění
svobodných	svobodný	k2eAgMnPc2d1	svobodný
mužů	muž	k1gMnPc2	muž
thingu	thing	k1gInSc2	thing
<g/>
.	.	kIx.	.
</s>
<s>
Učinit	učinit	k5eAaPmF	učinit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
vůli	vůle	k1gFnSc3	vůle
bylo	být	k5eAaImAgNnS	být
velice	velice	k6eAd1	velice
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
až	až	k6eAd1	až
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
<g/>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
role	role	k1gFnSc1	role
Thingu	Thing	k1gInSc2	Thing
<g/>
,	,	kIx,	,
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodovaly	rozhodovat	k5eAaImAgFnP	rozhodovat
důležité	důležitý	k2eAgFnPc1d1	důležitá
záležitosti	záležitost	k1gFnPc1	záležitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hrály	hrát	k5eAaImAgFnP	hrát
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čest	čest	k1gFnSc1	čest
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
vikingy	viking	k1gMnPc4	viking
vše	všechen	k3xTgNnSc1	všechen
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
uchylovali	uchylovat	k5eAaImAgMnP	uchylovat
k	k	k7c3	k
soubojům	souboj	k1gInPc3	souboj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgFnP	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gInPc2	jejich
životů	život	k1gInPc2	život
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženy	žena	k1gFnPc1	žena
měly	mít	k5eAaImAgFnP	mít
taktéž	taktéž	k?	taktéž
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Mohly	moct	k5eAaImAgFnP	moct
muže	muž	k1gMnPc4	muž
žalovat	žalovat	k5eAaImF	žalovat
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
sáhl	sáhnout	k5eAaPmAgMnS	sáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
kam	kam	k6eAd1	kam
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
žena	žena	k1gFnSc1	žena
žalovala	žalovat	k5eAaImAgFnS	žalovat
<g/>
,	,	kIx,	,
určilo	určit	k5eAaPmAgNnS	určit
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
musí	muset	k5eAaImIp3nP	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vikinské	vikinský	k2eAgInPc1d1	vikinský
mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
legendy	legenda	k1gFnPc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
měli	mít	k5eAaImAgMnP	mít
velmi	velmi	k6eAd1	velmi
bohatou	bohatý	k2eAgFnSc4d1	bohatá
a	a	k8xC	a
propracovanou	propracovaný	k2eAgFnSc4d1	propracovaná
mytologii	mytologie	k1gFnSc4	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
existovaly	existovat	k5eAaImAgInP	existovat
jen	jen	k6eAd1	jen
dva	dva	k4xCgInPc1	dva
světy	svět	k1gInPc1	svět
–	–	k?	–
svět	svět	k1gInSc4	svět
zimy	zima	k1gFnSc2	zima
Niflheim	Niflheim	k1gInSc1	Niflheim
a	a	k8xC	a
svět	svět	k1gInSc1	svět
ohně	oheň	k1gInSc2	oheň
Múspellheim	Múspellheima	k1gFnPc2	Múspellheima
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
světy	svět	k1gInPc1	svět
byly	být	k5eAaImAgInP	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
propastí	propast	k1gFnSc7	propast
Ginnungagap	Ginnungagap	k1gInSc1	Ginnungagap
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
však	však	k9	však
oheň	oheň	k1gInSc1	oheň
spojil	spojit	k5eAaPmAgInS	spojit
s	s	k7c7	s
ledem	led	k1gInSc7	led
a	a	k8xC	a
ze	z	k7c2	z
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgMnS	zrodit
obr	obr	k1gMnSc1	obr
Ymli	Yml	k1gFnSc2	Yml
a	a	k8xC	a
kráva	kráva	k1gFnSc1	kráva
Audhumla	Audhumlo	k1gNnSc2	Audhumlo
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
mlékem	mléko	k1gNnSc7	mléko
se	se	k3xPyFc4	se
Ymli	Ymle	k1gFnSc4	Ymle
živil	živit	k5eAaImAgMnS	živit
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
časech	čas	k1gInPc6	čas
pustý	pustý	k2eAgInSc4d1	pustý
a	a	k8xC	a
tak	tak	k6eAd1	tak
Audhumla	Audhumla	k1gFnSc1	Audhumla
olizovala	olizovat	k5eAaImAgFnS	olizovat
alespoň	alespoň	k9	alespoň
kameny	kámen	k1gInPc1	kámen
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pravé	pravá	k1gFnPc1	pravá
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
takového	takový	k3xDgInSc2	takový
kamene	kámen	k1gInSc2	kámen
vzešel	vzejít	k5eAaPmAgInS	vzejít
"	"	kIx"	"
<g/>
olizováním	olizování	k1gNnPc3	olizování
<g/>
"	"	kIx"	"
první	první	k4xOgMnSc1	první
bůh	bůh	k1gMnSc1	bůh
Búri	Búr	k1gFnSc2	Búr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc2	syn
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
jak	jak	k6eAd1	jak
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
)	)	kIx)	)
Boriho	Bori	k1gMnSc2	Bori
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
obryní	obryně	k1gFnSc7	obryně
Bestlou	Bestlý	k2eAgFnSc7d1	Bestlý
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Ymliho	Ymli	k1gMnSc2	Ymli
–	–	k?	–
nové	nový	k2eAgFnPc1d1	nová
bytosti	bytost	k1gFnPc1	bytost
vznikaly	vznikat	k5eAaImAgFnP	vznikat
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
potu	pot	k1gInSc2	pot
když	když	k8xS	když
spal	spát	k5eAaImAgMnS	spát
<g/>
)	)	kIx)	)
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
–	–	k?	–
Viliho	Vili	k1gMnSc4	Vili
<g/>
,	,	kIx,	,
Véa	Véa	k1gMnSc4	Véa
a	a	k8xC	a
Odina	Odin	k1gMnSc4	Odin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
znepokojeni	znepokojen	k2eAgMnPc1d1	znepokojen
množstvím	množství	k1gNnSc7	množství
trollů	troll	k1gMnPc2	troll
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nekontrolovatelně	kontrolovatelně	k6eNd1	kontrolovatelně
tvořili	tvořit	k5eAaImAgMnP	tvořit
z	z	k7c2	z
Ymliho	Ymli	k1gMnSc2	Ymli
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
rasu	rasa	k1gFnSc4	rasa
včetně	včetně	k7c2	včetně
Ymliho	Ymli	k1gMnSc2	Ymli
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k1gInSc4	pár
trollů	troll	k1gMnPc2	troll
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
základ	základ	k1gInSc4	základ
nové	nový	k2eAgFnSc2d1	nová
generaci	generace	k1gFnSc3	generace
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úhlavními	úhlavní	k2eAgMnPc7d1	úhlavní
nepřáteli	nepřítel	k1gMnPc7	nepřítel
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Ymliho	Ymli	k1gMnSc2	Ymli
těla	tělo	k1gNnSc2	tělo
poté	poté	k6eAd1	poté
bratři	bratr	k1gMnPc1	bratr
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
devět	devět	k4xCc4	devět
světů	svět	k1gInPc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
podpírá	podpírat	k5eAaImIp3nS	podpírat
světový	světový	k2eAgInSc1d1	světový
strom	strom	k1gInSc1	strom
Yggdrasil	Yggdrasil	k1gFnSc2	Yggdrasil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
jsou	být	k5eAaImIp3nP	být
Ásgard	Ásgard	k1gInSc4	Ásgard
a	a	k8xC	a
Vanaheim	Vanaheim	k1gInSc4	Vanaheim
<g/>
,	,	kIx,	,
světy	svět	k1gInPc4	svět
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
i	i	k9	i
Valhalla	Valhalla	k1gFnSc1	Valhalla
–	–	k?	–
obří	obří	k2eAgFnSc1d1	obří
hodovní	hodovní	k2eAgFnSc1d1	hodovní
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Odin	Odin	k1gMnSc1	Odin
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
duše	duše	k1gFnPc4	duše
lidských	lidský	k2eAgMnPc2d1	lidský
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
buduje	budovat	k5eAaImIp3nS	budovat
armádu	armáda	k1gFnSc4	armáda
pro	pro	k7c4	pro
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
bitvu	bitva	k1gFnSc4	bitva
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nastane	nastat	k5eAaPmIp3nS	nastat
konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
–	–	k?	–
Ragnarök	Ragnarök	k1gMnSc1	Ragnarök
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
do	do	k7c2	do
Valhally	Valhalla	k1gFnSc2	Valhalla
přinášely	přinášet	k5eAaImAgFnP	přinášet
Valkýry	valkýra	k1gFnPc1	valkýra
–	–	k?	–
ženské	ženský	k2eAgFnPc1d1	ženská
služebnice	služebnice	k1gFnPc1	služebnice
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Asgardu	Asgard	k1gInSc2	Asgard
se	se	k3xPyFc4	se
po	po	k7c6	po
duhovém	duhový	k2eAgInSc6d1	duhový
mostě	most	k1gInSc6	most
Bifröstu	Bifröst	k1gInSc2	Bifröst
dostaneme	dostat	k5eAaPmIp1nP	dostat
do	do	k7c2	do
Midgardu	Midgard	k1gInSc2	Midgard
–	–	k?	–
světa	svět	k1gInSc2	svět
lidí	člověk	k1gMnPc2	člověk
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
prolíná	prolínat	k5eAaImIp3nS	prolínat
i	i	k9	i
s	s	k7c7	s
Jotunheimem	Jotunheim	k1gInSc7	Jotunheim
<g/>
,	,	kIx,	,
zemí	zem	k1gFnSc7	zem
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
těmito	tento	k3xDgInPc7	tento
světy	svět	k1gInPc7	svět
je	být	k5eAaImIp3nS	být
Niflheim	Niflheim	k1gInSc1	Niflheim
<g/>
,	,	kIx,	,
země	zem	k1gFnPc1	zem
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vládne	vládnout	k5eAaImIp3nS	vládnout
bohyně	bohyně	k1gFnSc1	bohyně
Hel	Hela	k1gFnPc2	Hela
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
Yggdrasil	Yggdrasil	k1gFnSc2	Yggdrasil
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
tři	tři	k4xCgInPc4	tři
kořeny	kořen	k1gInPc4	kořen
–	–	k?	–
jeden	jeden	k4xCgInSc1	jeden
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
studni	studna	k1gFnSc3	studna
osudu	osud	k1gInSc2	osud
Urdi	Urd	k1gFnSc2	Urd
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
3	[number]	k4	3
bohyně	bohyně	k1gFnPc4	bohyně
osudu	osud	k1gInSc2	osud
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Norny	norna	k1gFnSc2	norna
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
kořen	kořen	k1gInSc1	kořen
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
studni	studna	k1gFnSc3	studna
moudrosti	moudrost	k1gFnSc2	moudrost
kterou	který	k3yRgFnSc4	který
hlídá	hlídat	k5eAaImIp3nS	hlídat
obr	obr	k1gMnSc1	obr
Mími	Mím	k1gFnSc2	Mím
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
kořen	kořen	k1gInSc1	kořen
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
prameni	pramen	k1gInSc3	pramen
řeky	řeka	k1gFnSc2	řeka
Gjöll	Gjöll	k1gInSc1	Gjöll
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
drak	drak	k1gMnSc1	drak
Nidhögg	Nidhögg	k1gMnSc1	Nidhögg
který	který	k3yRgMnSc1	který
kořeny	kořen	k1gInPc7	kořen
okusuje	okusovat	k5eAaImIp3nS	okusovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kmeni	kmen	k1gInSc6	kmen
neustále	neustále	k6eAd1	neustále
běhá	běhat	k5eAaImIp3nS	běhat
mezi	mezi	k7c7	mezi
světy	svět	k1gInPc7	svět
veverka	veverek	k1gMnSc2	veverek
Ratatosk	Ratatosk	k1gInSc1	Ratatosk
a	a	k8xC	a
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
pomluvy	pomluva	k1gFnPc4	pomluva
od	od	k7c2	od
draka	drak	k1gMnSc2	drak
nebeskému	nebeský	k2eAgMnSc3d1	nebeský
orlu	orel	k1gMnSc3	orel
Vidofniru	Vidofnir	k1gMnSc3	Vidofnir
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
zase	zase	k9	zase
naopak	naopak	k6eAd1	naopak
od	od	k7c2	od
orla	orel	k1gMnSc2	orel
drakovi	drak	k1gMnSc6	drak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Odin	Odin	k1gInSc1	Odin
<g/>
,	,	kIx,	,
Vili	vít	k5eAaImAgMnP	vít
a	a	k8xC	a
Vé	Vé	k1gMnPc1	Vé
vdechli	vdechnout	k5eAaPmAgMnP	vdechnout
život	život	k1gInSc4	život
dvěma	dva	k4xCgInPc3	dva
kmenům	kmen	k1gInPc3	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
našli	najít	k5eAaPmAgMnP	najít
vyplavené	vyplavený	k2eAgInPc1d1	vyplavený
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
jasanu	jasan	k1gInSc2	jasan
–	–	k?	–
Ask	Ask	k1gMnSc1	Ask
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
z	z	k7c2	z
jívy	jíva	k1gFnSc2	jíva
–	–	k?	–
Embla	Embla	k1gMnSc1	Embla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trojice	trojice	k1gFnSc1	trojice
hlavních	hlavní	k2eAgMnPc2d1	hlavní
bohů	bůh	k1gMnPc2	bůh
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
na	na	k7c4	na
početnou	početný	k2eAgFnSc4d1	početná
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Odin	Odin	k1gMnSc1	Odin
stanul	stanout	k5eAaPmAgMnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
všech	všecek	k3xTgMnPc2	všecek
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obětoval	obětovat	k5eAaBmAgMnS	obětovat
své	svůj	k3xOyFgNnSc4	svůj
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
moudrost	moudrost	k1gFnSc4	moudrost
ze	z	k7c2	z
studně	studně	k1gFnSc2	studně
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
bohyní	bohyně	k1gFnPc2	bohyně
Frigg	Frigg	k1gMnSc1	Frigg
zplodil	zplodit	k5eAaPmAgMnS	zplodit
několik	několik	k4yIc4	několik
synů	syn	k1gMnPc2	syn
–	–	k?	–
například	například	k6eAd1	například
boha	bůh	k1gMnSc2	bůh
války	válka	k1gFnSc2	válka
Tyra	Tyrus	k1gMnSc2	Tyrus
<g/>
,	,	kIx,	,
boha	bůh	k1gMnSc2	bůh
světla	světlo	k1gNnSc2	světlo
Baldra	Baldr	k1gMnSc2	Baldr
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
Odinovým	Odinův	k2eAgMnSc7d1	Odinův
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vzešlým	vzešlý	k2eAgMnPc3d1	vzešlý
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Thor	Thor	k1gMnSc1	Thor
–	–	k?	–
bůh	bůh	k1gMnSc1	bůh
hromu	hrom	k1gInSc2	hrom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc7d1	populární
postavou	postava	k1gFnSc7	postava
byl	být	k5eAaImAgInS	být
také	také	k9	také
Loki	Loke	k1gFnSc4	Loke
<g/>
,	,	kIx,	,
Odinův	Odinův	k2eAgMnSc1d1	Odinův
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
míšenec	míšenec	k1gMnSc1	míšenec
obra	obr	k1gMnSc2	obr
a	a	k8xC	a
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
lstivý	lstivý	k2eAgInSc1d1	lstivý
<g/>
,	,	kIx,	,
škodolibý	škodolibý	k2eAgInSc1d1	škodolibý
a	a	k8xC	a
prospěchářský	prospěchářský	k2eAgInSc1d1	prospěchářský
a	a	k8xC	a
bohům	bůh	k1gMnPc3	bůh
způsobil	způsobit	k5eAaPmAgInS	způsobit
nejednu	nejeden	k4xCyIgFnSc4	nejeden
nepříjemnost	nepříjemnost	k1gFnSc4	nepříjemnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vikingové	Viking	k1gMnPc1	Viking
a	a	k8xC	a
oděvy	oděv	k1gInPc7	oděv
==	==	k?	==
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
oblékání	oblékání	k1gNnSc2	oblékání
vikingů	viking	k1gMnPc2	viking
závisel	záviset	k5eAaImAgMnS	záviset
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
věcech	věc	k1gFnPc6	věc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
v	v	k7c6	v
jaké	jaký	k3yRgFnSc6	jaký
části	část	k1gFnSc6	část
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
žili	žít	k5eAaImAgMnP	žít
</s>
</p>
<p>
<s>
jaké	jaký	k3yRgNnSc1	jaký
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc1	jejich
postavení	postavení	k1gNnSc1	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
</s>
</p>
<p>
<s>
jak	jak	k6eAd1	jak
byli	být	k5eAaImAgMnP	být
bohatíOblečení	bohatíOblečení	k1gNnSc2	bohatíOblečení
vikingů	viking	k1gMnPc2	viking
bylo	být	k5eAaImAgNnS	být
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS	přizpůsobit
jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
a	a	k8xC	a
pohodlí	pohodlí	k1gNnSc1	pohodlí
a	a	k8xC	a
daleko	daleko	k6eAd1	daleko
méně	málo	k6eAd2	málo
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
módě	móda	k1gFnSc3	móda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
skandinávské	skandinávský	k2eAgNnSc1d1	skandinávské
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oblečení	oblečení	k1gNnSc1	oblečení
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vlněné	vlněný	k2eAgFnPc1d1	vlněná
nebo	nebo	k8xC	nebo
lněné	lněný	k2eAgFnPc1d1	lněná
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
měli	mít	k5eAaImAgMnP	mít
rádi	rád	k2eAgMnPc1d1	rád
pestré	pestrý	k2eAgInPc1d1	pestrý
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
často	často	k6eAd1	často
jimi	on	k3xPp3gFnPc7	on
své	svůj	k3xOyFgNnSc4	svůj
oblečení	oblečení	k1gNnSc4	oblečení
zdobili	zdobit	k5eAaImAgMnP	zdobit
<g/>
.	.	kIx.	.
</s>
<s>
Barviva	barvivo	k1gNnPc4	barvivo
na	na	k7c4	na
látky	látka	k1gFnPc4	látka
byla	být	k5eAaImAgFnS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
a	a	k8xC	a
získávala	získávat	k5eAaImAgFnS	získávat
se	se	k3xPyFc4	se
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
vikinská	vikinský	k2eAgFnSc1d1	vikinská
žena	žena	k1gFnSc1	žena
měla	mít	k5eAaImAgFnS	mít
doma	doma	k6eAd1	doma
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
výroba	výroba	k1gFnSc1	výroba
každého	každý	k3xTgInSc2	každý
kusu	kus	k1gInSc2	kus
oblečení	oblečení	k1gNnSc2	oblečení
byla	být	k5eAaImAgFnS	být
časově	časově	k6eAd1	časově
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
kus	kus	k1gInSc1	kus
velmi	velmi	k6eAd1	velmi
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vraceli	vracet	k5eAaImAgMnP	vracet
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Hedvábí	hedvábí	k1gNnSc1	hedvábí
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgNnSc1d1	drahé
a	a	k8xC	a
tak	tak	k9	tak
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
pouze	pouze	k6eAd1	pouze
bohatí	bohatý	k2eAgMnPc1d1	bohatý
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ženy	žena	k1gFnPc1	žena
nosily	nosit	k5eAaImAgFnP	nosit
lněné	lněný	k2eAgInPc4d1	lněný
šaty	šat	k1gInPc4	šat
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
měly	mít	k5eAaImAgFnP	mít
zástěru	zástěra	k1gFnSc4	zástěra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
chránila	chránit	k5eAaImAgFnS	chránit
šaty	šat	k1gInPc4	šat
před	před	k7c7	před
ušpiněním	ušpinění	k1gNnSc7	ušpinění
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
daly	dát	k5eAaPmAgFnP	dát
často	často	k6eAd1	často
používané	používaný	k2eAgInPc4d1	používaný
předměty	předmět	k1gInPc4	předmět
např.	např.	kA	např.
nůž	nůž	k1gInSc4	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgInPc1d1	drobný
předměty	předmět	k1gInPc1	předmět
se	se	k3xPyFc4	se
také	také	k9	také
uvazovaly	uvazovat	k5eAaImAgFnP	uvazovat
kolem	kolem	k7c2	kolem
zápěstí	zápěstí	k1gNnPc2	zápěstí
protože	protože	k8xS	protože
šaty	šat	k1gInPc1	šat
neměly	mít	k5eNaImAgInP	mít
žádné	žádný	k3yNgFnPc4	žádný
kapsy	kapsa	k1gFnPc4	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
nosily	nosit	k5eAaImAgFnP	nosit
šátek	šátek	k1gInSc4	šátek
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
krku	krk	k1gInSc2	krk
uvázán	uvázat	k5eAaPmNgInS	uvázat
a	a	k8xC	a
opatřen	opatřit	k5eAaPmNgInS	opatřit
broží	brož	k1gFnSc7	brož
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mýty	mýtus	k1gInPc1	mýtus
o	o	k7c6	o
vikinských	vikinský	k2eAgFnPc6d1	vikinská
rohatých	rohatý	k2eAgFnPc6d1	rohatá
helmách	helma	k1gFnPc6	helma
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vikingové	Viking	k1gMnPc1	Viking
nosili	nosit	k5eAaImAgMnP	nosit
rohaté	rohatý	k2eAgFnPc4d1	rohatá
přilby	přilba	k1gFnPc4	přilba
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
-	-	kIx~	-
vikinští	vikinský	k2eAgMnPc1d1	vikinský
válečníci	válečník	k1gMnPc1	válečník
totiž	totiž	k8xC	totiž
přilby	přilba	k1gFnPc1	přilba
s	s	k7c7	s
rohy	roh	k1gInPc7	roh
zřejmě	zřejmě	k6eAd1	zřejmě
nikdy	nikdy	k6eAd1	nikdy
nenosili	nosit	k5eNaImAgMnP	nosit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
vědců	vědec	k1gMnPc2	vědec
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
žádné	žádný	k3yNgFnSc6	žádný
nalezené	nalezený	k2eAgFnSc6d1	nalezená
helmě	helma	k1gFnSc6	helma
vikinga	viking	k1gMnSc2	viking
se	se	k3xPyFc4	se
rohy	roh	k1gInPc1	roh
zvířete	zvíře	k1gNnSc2	zvíře
nenašly	najít	k5eNaPmAgInP	najít
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
helem	helma	k1gFnPc2	helma
některých	některý	k3yIgMnPc2	některý
severských	severský	k2eAgMnPc2d1	severský
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
,	,	kIx,	,
nijak	nijak	k6eAd1	nijak
nepodložené	podložený	k2eNgFnPc1d1	nepodložená
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc4	tento
mýty	mýtus	k1gInPc4	mýtus
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c6	o
Vikinzích	Viking	k1gMnPc6	Viking
mluvili	mluvit	k5eAaImAgMnP	mluvit
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
ďáblech	ďábel	k1gMnPc6	ďábel
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kronikáři	kronikář	k1gMnPc1	kronikář
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
skutečné	skutečný	k2eAgMnPc4d1	skutečný
ďábly	ďábel	k1gMnPc4	ďábel
a	a	k8xC	a
tak	tak	k9	tak
je	on	k3xPp3gNnSc4	on
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
s	s	k7c7	s
rohy	roh	k1gInPc7	roh
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
naše	náš	k3xOp1gMnPc4	náš
známé	známý	k2eAgMnPc4d1	známý
vikingy	viking	k1gMnPc4	viking
s	s	k7c7	s
rohy	roh	k1gInPc7	roh
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
–	–	k?	–
herci	herec	k1gMnPc1	herec
neměli	mít	k5eNaImAgMnP	mít
jiné	jiný	k2eAgFnPc4d1	jiná
helmy	helma	k1gFnPc4	helma
<g/>
,	,	kIx,	,
než	než	k8xS	než
ty	ty	k3xPp2nSc1	ty
s	s	k7c7	s
rohy	roh	k1gInPc7	roh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rohatí	rohatý	k2eAgMnPc1d1	rohatý
Vikingové	Viking	k1gMnPc1	Viking
dostali	dostat	k5eAaPmAgMnP	dostat
mezi	mezi	k7c4	mezi
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Vikinské	vikinský	k2eAgFnPc1d1	vikinská
lodě	loď	k1gFnPc1	loď
==	==	k?	==
</s>
</p>
<p>
<s>
Drakkar	Drakkar	k1gInSc1	Drakkar
–	–	k?	–
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
válečná	válečný	k2eAgFnSc1d1	válečná
loď	loď	k1gFnSc1	loď
</s>
</p>
<p>
<s>
Snekkar	Snekkar	k1gInSc1	Snekkar
–	–	k?	–
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
válečná	válečný	k2eAgFnSc1d1	válečná
loď	loď	k1gFnSc1	loď
</s>
</p>
<p>
<s>
Knarr	Knarr	k1gInSc1	Knarr
–	–	k?	–
v	v	k7c6	v
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
kolonizátorská	kolonizátorský	k2eAgFnSc1d1	kolonizátorský
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
loď	loď	k1gFnSc1	loď
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ARBMAN	ARBMAN	kA	ARBMAN
<g/>
,	,	kIx,	,
Holger	Holger	k1gInSc1	Holger
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BEDNAŘÍKOVÁ	Bednaříková	k1gFnSc1	Bednaříková
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
.	.	kIx.	.
</s>
<s>
Stěhování	stěhování	k1gNnSc1	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
sever	sever	k1gInSc4	sever
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
na	na	k7c6	na
mořích	moře	k1gNnPc6	moře
i	i	k8xC	i
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9788074297441	[number]	k4	9788074297441
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BRONSTED	BRONSTED	kA	BRONSTED
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Sága	ságo	k1gNnSc2	ságo
tří	tři	k4xCgNnPc2	tři
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Graham-Campbell	Graham-Campbell	k1gMnSc1	Graham-Campbell
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Batey	Batea	k1gMnSc2	Batea
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
;	;	kIx,	;
Clarke	Clark	k1gMnSc2	Clark
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
;	;	kIx,	;
Page	Pag	k1gMnSc2	Pag
<g/>
,	,	kIx,	,
R.	R.	kA	R.
I.	I.	kA	I.
<g/>
;	;	kIx,	;
Price	Price	k1gMnSc1	Price
<g/>
,	,	kIx,	,
N.	N.	kA	N.
S.	S.	kA	S.
Svět	svět	k1gInSc1	svět
Vikingů	Viking	k1gMnPc2	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7176-669-0	[number]	k4	80-7176-669-0
</s>
</p>
<p>
<s>
RAGACHE	RAGACHE	kA	RAGACHE
<g/>
,	,	kIx,	,
Gilles	Gilles	k1gMnSc1	Gilles
<g/>
;	;	kIx,	;
LAVERDET	LAVERDET	kA	LAVERDET
<g/>
,	,	kIx,	,
Marcel	Marcel	k1gMnSc1	Marcel
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
47	[number]	k4	47
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
8085265451	[number]	k4	8085265451
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RANGI	RANGI	kA	RANGI
<g/>
,	,	kIx,	,
Hiroa	Hiroa	k1gFnSc1	Hiroa
te	te	k?	te
<g/>
;	;	kIx,	;
CINCIBUS	CINCIBUS	kA	CINCIBUS
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
jižních	jižní	k2eAgNnPc2d1	jižní
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
203	[number]	k4	203
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vikingové	Viking	k1gMnPc1	Viking
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Vikingové	Viking	k1gMnPc1	Viking
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Viking	Viking	k1gMnSc1	Viking
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
muzeum	muzeum	k1gNnSc1	muzeum
Vikinské	vikinský	k2eAgFnSc2d1	vikinská
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
v	v	k7c6	v
Rosklide	Rosklid	k1gMnSc5	Rosklid
</s>
</p>
