<s>
Švýcarsko	Švýcarsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1960	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
1960	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
výpravyIOC	výpravyIOC	k?
kód	kód	k1gInSc4
</s>
<s>
SUI	SUI	kA
</s>
<s>
ZlatáStříbrnáBronzováCelkem	ZlatáStříbrnáBronzováCelek	k1gInSc7
</s>
<s>
0	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
NOV	nov	k1gInSc1
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Švýcarska	Švýcarsko	k1gNnSc2
Vlajkonoš	vlajkonoš	k1gMnSc1
</s>
<s>
August	August	k1gMnSc1
Hollenstein	Hollenstein	k1gMnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
Letní	letní	k2eAgNnSc1d1
olympiády	olympiáda	k1gFnSc2
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastupovalo	zastupovat	k5eAaImAgNnS
ho	on	k3xPp3gNnSc4
149	#num#	k4
sportovců	sportovec	k1gMnPc2
(	(	kIx(
<g/>
147	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
2	#num#	k4
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
v	v	k7c6
16	#num#	k4
sportech	sport	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Medailisté	medailista	k1gMnPc1
</s>
<s>
Medaile	medaile	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
StříbroGustav	StříbroGustavit	k5eAaPmRp2nS
FischerJezdectvíDrezura	FischerJezdectvíDrezura	k1gFnSc1
-	-	kIx~
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
StříbroAnton	StříbroAnton	k1gInSc1
Bühler	Bühler	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Günthardt	Günthardta	k1gFnPc2
Hans	hansa	k1gFnPc2
SchwarzenbachJezdectvíJezdecká	SchwarzenbachJezdectvíJezdecký	k2eAgFnSc1d1
všestrannost	všestrannost	k1gFnSc1
-	-	kIx~
družstva	družstvo	k1gNnPc1
</s>
<s>
StíbroHans-Rudolf	StíbroHans-Rudolf	k1gMnSc1
Spillmann	Spillmann	k1gInSc4
Sportovní	sportovní	k2eAgInSc4d1
střelbaMuži	střelbaMuzat	k5eAaPmIp1nS
300	#num#	k4
m	m	kA
pistole	pistol	k1gFnSc2
tři	tři	k4xCgFnPc4
pozice	pozice	k1gFnPc4
</s>
<s>
BronzAnton	BronzAnton	k1gInSc1
BühlerJezdectvíJezdecká	BühlerJezdectvíJezdecký	k2eAgFnSc1d1
všestrannost	všestrannost	k1gFnSc1
-	-	kIx~
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
BronzErnst	BronzErnst	k1gMnSc1
HürlimannRolf	HürlimannRolf	k1gMnSc1
Larcher	Larchra	k1gFnPc2
VeslováníMuži	VeslováníMuž	k1gFnSc3
dvojskif	dvojskif	k1gInSc1
</s>
<s>
BronzHenri	BronzHenri	k6eAd1
Copponex	Copponex	k1gInSc1
Pierre	Pierr	k1gInSc5
Girard	Girarda	k1gFnPc2
Manfred	Manfred	k1gMnSc1
Metzger	Metzger	k1gMnSc1
Jachting	jachting	k1gInSc4
<g/>
5,5	5,5	k4
m	m	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Švýcarsko	Švýcarsko	k1gNnSc1
LOH	LOH	kA
1960	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Švýcarsko	Švýcarsko	k1gNnSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Švýcarsko	Švýcarsko	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
1896	#num#	k4
•	•	k?
1900	#num#	k4
•	•	k?
1904	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
Švýcarsko	Švýcarsko	k1gNnSc4
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
