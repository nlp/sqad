<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
(	(	kIx(	(
<g/>
albánsky	albánsky	k6eAd1	albánsky
<g/>
:	:	kIx,	:
Shqipëri	Shqipëri	k1gNnSc7	Shqipëri
<g/>
/	/	kIx~	/
<g/>
Shqipëria	Shqipërium	k1gNnPc1	Shqipërium
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
Arbëria	Arbërium	k1gNnSc2	Arbërium
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Albánská	albánský	k2eAgFnSc1d1	albánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
albánsky	albánsky	k6eAd1	albánsky
Republika	republika	k1gFnSc1	republika
e	e	k0	e
Shqipërisë	Shqipërisë	k1gMnSc5	Shqipërisë
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středomořský	středomořský	k2eAgInSc1d1	středomořský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Makedonií	Makedonie	k1gFnSc7	Makedonie
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Albánie	Albánie	k1gFnSc2	Albánie
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
její	její	k3xOp3gFnSc1	její
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jónského	jónský	k2eAgNnSc2d1	Jónské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
od	od	k7c2	od
Albánie	Albánie	k1gFnSc2	Albánie
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Otrantský	Otrantský	k2eAgInSc1d1	Otrantský
průliv	průliv	k1gInSc1	průliv
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
72	[number]	k4	72
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Tirana	Tirana	k1gFnSc1	Tirana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
895	[number]	k4	895
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgMnSc7d1	finanční
centrem	centr	k1gMnSc7	centr
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
2	[number]	k4	2
821	[number]	k4	821
997	[number]	k4	997
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Etnicky	etnicky	k6eAd1	etnicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
homogenní	homogenní	k2eAgFnSc6d1	homogenní
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
95	[number]	k4	95
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
,	,	kIx,	,
z	z	k7c2	z
náboženství	náboženství	k1gNnSc2	náboženství
zde	zde	k6eAd1	zde
převažuje	převažovat	k5eAaImIp3nS	převažovat
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Demograficky	demograficky	k6eAd1	demograficky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
mladou	mladý	k2eAgFnSc4d1	mladá
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
střední	střední	k2eAgInSc4d1	střední
věk	věk	k1gInSc4	věk
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
29,9	[number]	k4	29,9
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
40,1	[number]	k4	40,1
let	léto	k1gNnPc2	léto
u	u	k7c2	u
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Albánie	Albánie	k1gFnSc1	Albánie
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
OBSE	OBSE	kA	OBSE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
CEFTA	CEFTA	kA	CEFTA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
islámské	islámský	k2eAgFnSc2d1	islámská
spolupráce	spolupráce	k1gFnSc2	spolupráce
(	(	kIx(	(
<g/>
OIC	OIC	kA	OIC
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
Středomoří	středomoří	k1gNnSc4	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
potenciálním	potenciální	k2eAgMnSc7d1	potenciální
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
formálně	formálně	k6eAd1	formálně
požádala	požádat	k5eAaPmAgFnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
tržní	tržní	k2eAgFnSc7d1	tržní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
a	a	k8xC	a
transformace	transformace	k1gFnPc1	transformace
trhu	trh	k1gInSc2	trh
otevřely	otevřít	k5eAaPmAgFnP	otevřít
zemi	zem	k1gFnSc4	zem
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
investicím	investice	k1gFnPc3	investice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
co	co	k9	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
rozvoje	rozvoj	k1gInSc2	rozvoj
energetického	energetický	k2eAgInSc2d1	energetický
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc2d1	dopravní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
několika	několik	k4yIc2	několik
antických	antický	k2eAgMnPc2d1	antický
spisovatelů	spisovatel	k1gMnPc2	spisovatel
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dovědět	dovědět	k5eAaPmF	dovědět
o	o	k7c6	o
jistém	jistý	k2eAgNnSc6d1	jisté
předřeckém	předřecký	k2eAgNnSc6d1	předřecký
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
Pelasgové	Pelasg	k1gMnPc1	Pelasg
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
byla	být	k5eAaImAgFnS	být
Dodóna	Dodóna	k1gFnSc1	Dodóna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
centrem	centr	k1gInSc7	centr
říše	říš	k1gFnSc2	říš
Epirus	Epirus	k1gInSc1	Epirus
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
království	království	k1gNnPc2	království
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dodoně	Dodona	k1gFnSc6	Dodona
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
nejstarší	starý	k2eAgFnSc1d3	nejstarší
věštírna	věštírna	k1gFnSc1	věštírna
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
asi	asi	k9	asi
o	o	k7c4	o
2500	[number]	k4	2500
let	léto	k1gNnPc2	léto
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
věštírna	věštírna	k1gFnSc1	věštírna
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Antika	antika	k1gFnSc1	antika
===	===	k?	===
</s>
</p>
<p>
<s>
Antické	antický	k2eAgNnSc1d1	antické
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
originální	originální	k2eAgFnSc1d1	originální
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Ilyrové	Ilyr	k1gMnPc1	Ilyr
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
objevovat	objevovat	k5eAaImF	objevovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jako	jako	k8xS	jako
lidé	člověk	k1gMnPc1	člověk
mluvící	mluvící	k2eAgInSc1d1	mluvící
indoevropským	indoevropský	k2eAgInSc7d1	indoevropský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kultura	kultura	k1gFnSc1	kultura
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
řeckou	řecký	k2eAgFnSc7d1	řecká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
byly	být	k5eAaImAgFnP	být
ale	ale	k8xC	ale
také	také	k6eAd1	také
řecké	řecký	k2eAgFnSc2d1	řecká
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
antiky	antika	k1gFnSc2	antika
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
název	název	k1gInSc1	název
Albania	Albanium	k1gNnSc2	Albanium
jako	jako	k8xS	jako
země	zem	k1gFnPc1	zem
bílá	bílý	k2eAgFnSc1d1	bílá
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
sousední	sousední	k2eAgFnSc3d1	sousední
Černé	Černá	k1gFnSc3	Černá
Hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
dobyta	dobýt	k5eAaPmNgFnS	dobýt
východořímskou	východořímský	k2eAgFnSc7d1	Východořímská
Byzantskou	byzantský	k2eAgFnSc7d1	byzantská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Osmanů	Osman	k1gMnPc2	Osman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
a	a	k8xC	a
novověk	novověk	k1gInSc1	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
Byzantinský	byzantinský	k2eAgInSc4d1	byzantinský
vliv	vliv	k1gInSc4	vliv
ohrožovalo	ohrožovat	k5eAaImAgNnS	ohrožovat
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Raška	raška	k1gFnSc1	raška
<g/>
,	,	kIx,	,
Zeta	Zet	k1gMnSc4	Zet
a	a	k8xC	a
Benátská	benátský	k2eAgFnSc1d1	Benátská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
na	na	k7c6	na
území	území	k1gNnSc6	území
současné	současný	k2eAgFnSc2d1	současná
Albánie	Albánie	k1gFnSc2	Albánie
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
též	též	k9	též
barbaři	barbar	k1gMnPc1	barbar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc4	první
albánský	albánský	k2eAgInSc4d1	albánský
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jím	on	k3xPp3gNnSc7	on
Arbešské	Arbešský	k2eAgNnSc1d1	Arbešský
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgNnSc1d1	neustálé
válčení	válčení	k1gNnSc1	válčení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
migrace	migrace	k1gFnPc4	migrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
utíkalo	utíkat	k5eAaImAgNnS	utíkat
buď	buď	k8xC	buď
na	na	k7c4	na
jih	jih	k1gInSc4	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Snad	snad	k9	snad
podle	podle	k7c2	podle
jihoalbánských	jihoalbánský	k2eAgInPc2d1	jihoalbánský
Tosků	Tosk	k1gInPc2	Tosk
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
regionální	regionální	k2eAgInSc1d1	regionální
název	název	k1gInSc1	název
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
až	až	k9	až
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
postupně	postupně	k6eAd1	postupně
zemi	zem	k1gFnSc4	zem
ovládat	ovládat	k5eAaImF	ovládat
Osmané	Osman	k1gMnPc1	Osman
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
však	však	k9	však
postavil	postavit	k5eAaPmAgMnS	postavit
Gjergj	Gjergj	k1gMnSc1	Gjergj
Kastrioti	Kastriot	k1gMnPc1	Kastriot
Skanderbeg	Skanderbeg	k1gMnSc1	Skanderbeg
z	z	k7c2	z
Krujë	Krujë	k1gFnSc2	Krujë
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
úspěšně	úspěšně	k6eAd1	úspěšně
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
dočasně	dočasně	k6eAd1	dočasně
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osmané	Osman	k1gMnPc1	Osman
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
nakonec	nakonec	k6eAd1	nakonec
vládli	vládnout	k5eAaImAgMnP	vládnout
další	další	k2eAgFnPc4d1	další
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
křesťanství	křesťanství	k1gNnPc4	křesťanství
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
zavedli	zavést	k5eAaPmAgMnP	zavést
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
i	i	k9	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zaostalou	zaostalý	k2eAgFnSc7d1	zaostalá
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
negramotnosti	negramotnost	k1gFnSc2	negramotnost
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
ženy	žena	k1gFnPc1	žena
byly	být	k5eAaImAgFnP	být
nevzdělané	vzdělaný	k2eNgFnPc1d1	nevzdělaná
<g/>
)	)	kIx)	)
a	a	k8xC	a
skoro	skoro	k6eAd1	skoro
žádným	žádný	k3yNgInSc7	žádný
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
nadvládě	nadvláda	k1gFnSc3	nadvláda
propukala	propukat	k5eAaImAgNnP	propukat
různá	různý	k2eAgNnPc1d1	různé
povstání	povstání	k1gNnPc1	povstání
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1756	[number]	k4	1756
a	a	k8xC	a
1831	[number]	k4	1831
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
moc	moc	k6eAd1	moc
rod	rod	k1gInSc4	rod
Bušatliů	Bušatli	k1gMnPc2	Bušatli
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1787	[number]	k4	1787
a	a	k8xC	a
1822	[number]	k4	1822
pak	pak	k6eAd1	pak
jih	jih	k1gInSc4	jih
země	zem	k1gFnSc2	zem
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Ali	Ali	k1gMnSc1	Ali
paša	paša	k1gMnSc1	paša
janinský	janinský	k2eAgMnSc1d1	janinský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
albánské	albánský	k2eAgNnSc1d1	albánské
národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
téhož	týž	k3xTgNnSc2	týž
století	století	k1gNnSc2	století
také	také	k9	také
působila	působit	k5eAaImAgFnS	působit
Prizrenská	Prizrenský	k2eAgFnSc1d1	Prizrenská
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
národních	národní	k2eAgFnPc2d1	národní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požadovala	požadovat	k5eAaImAgFnS	požadovat
na	na	k7c6	na
sultánovi	sultán	k1gMnSc6	sultán
autonomii	autonomie	k1gFnSc4	autonomie
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
===	===	k?	===
</s>
</p>
<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1912	[number]	k4	1912
nezávislost	nezávislost	k1gFnSc1	nezávislost
po	po	k7c6	po
první	první	k4xOgFnSc6	první
balkánské	balkánský	k2eAgFnSc6d1	balkánská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
byla	být	k5eAaImAgFnS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1913	[number]	k4	1913
podepsána	podepsán	k2eAgFnSc1d1	podepsána
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Albánii	Albánie	k1gFnSc4	Albánie
již	již	k6eAd1	již
stanovila	stanovit	k5eAaPmAgFnS	stanovit
definitivně	definitivně	k6eAd1	definitivně
jako	jako	k8xS	jako
autonomní	autonomní	k2eAgNnSc4d1	autonomní
<g/>
,	,	kIx,	,
suverénní	suverénní	k2eAgNnSc4d1	suverénní
a	a	k8xC	a
dědičné	dědičný	k2eAgNnSc4d1	dědičné
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
existence	existence	k1gFnSc1	existence
a	a	k8xC	a
neutralita	neutralita	k1gFnSc1	neutralita
byla	být	k5eAaImAgFnS	být
garantována	garantovat	k5eAaBmNgFnS	garantovat
šesti	šest	k4xCc7	šest
velmocemi	velmoc	k1gFnPc7	velmoc
(	(	kIx(	(
<g/>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
zvolení	zvolení	k1gNnSc2	zvolení
knížete	kníže	k1gNnSc2wR	kníže
měla	mít	k5eAaImAgFnS	mít
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
moc	moc	k1gFnSc1	moc
vykonávat	vykonávat	k5eAaImF	vykonávat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
MKK	MKK	kA	MKK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c4	o
albánský	albánský	k2eAgInSc4d1	albánský
knížecí	knížecí	k2eAgInSc4d1	knížecí
trůn	trůn	k1gInSc4	trůn
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
např.	např.	kA	např.
italský	italský	k2eAgMnSc1d1	italský
markýz	markýz	k1gMnSc1	markýz
Giovanni	Giovanň	k1gMnSc6	Giovanň
Kastriota	Kastriota	k1gFnSc1	Kastriota
Skanderbeg	Skanderbega	k1gFnPc2	Skanderbega
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Auletta	Auletta	k1gMnSc1	Auletta
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
aristokrat	aristokrat	k1gMnSc1	aristokrat
baron	baron	k1gMnSc1	baron
Franz	Franz	k1gMnSc1	Franz
Nopcsa	Nopcsa	k1gFnSc1	Nopcsa
nebo	nebo	k8xC	nebo
španělský	španělský	k2eAgMnSc1d1	španělský
šlechtic	šlechtic	k1gMnSc1	šlechtic
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Alandro	Alandra	k1gFnSc5	Alandra
Kastrioti	Kastriot	k1gMnPc1	Kastriot
y	y	k?	y
Perez	Perez	k1gMnSc1	Perez
de	de	k?	de
Velasco	Velasco	k1gMnSc1	Velasco
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odvozovali	odvozovat	k5eAaImAgMnP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
hrdinného	hrdinný	k2eAgMnSc2d1	hrdinný
Skanderbega	Skanderbeg	k1gMnSc2	Skanderbeg
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
však	však	k9	však
podporu	podpora	k1gFnSc4	podpora
velmocí	velmoc	k1gFnPc2	velmoc
a	a	k8xC	a
tak	tak	k6eAd1	tak
nepřicházeli	přicházet	k5eNaImAgMnP	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
dopadli	dopadnout	k5eAaPmAgMnP	dopadnout
i	i	k9	i
kandidáti	kandidát	k1gMnPc1	kandidát
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
nebo	nebo	k8xC	nebo
černohorský	černohorský	k2eAgMnSc1d1	černohorský
král	král	k1gMnSc1	král
Nikola	Nikola	k1gMnSc1	Nikola
<g/>
,	,	kIx,	,
neuspěli	uspět	k5eNaPmAgMnP	uspět
však	však	k9	však
ani	ani	k8xC	ani
kandidáti	kandidát	k1gMnPc1	kandidát
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
dopadl	dopadnout	k5eAaPmAgMnS	dopadnout
až	až	k9	až
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Wied	Wied	k1gMnSc1	Wied
z	z	k7c2	z
Neuwiedu	Neuwied	k1gMnSc6	Neuwied
v	v	k7c6	v
Porýní	Porýní	k1gNnSc6	Porýní
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
příbuzným	příbuzný	k1gMnSc7	příbuzný
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
protestant	protestant	k1gMnSc1	protestant
a	a	k8xC	a
tedy	tedy	k9	tedy
přijatelný	přijatelný	k2eAgInSc1d1	přijatelný
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
autonomním	autonomní	k2eAgNnSc7d1	autonomní
<g/>
,	,	kIx,	,
suverénním	suverénní	k2eAgNnSc7d1	suverénní
a	a	k8xC	a
dědičným	dědičný	k2eAgNnSc7d1	dědičné
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
vládcem	vládce	k1gMnSc7	vládce
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Wied	Wied	k1gMnSc1	Wied
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Albánie	Albánie	k1gFnSc2	Albánie
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
a	a	k8xC	a
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
sídelní	sídelní	k2eAgNnSc4d1	sídelní
město	město	k1gNnSc4	město
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgInS	vybrat
přístav	přístav	k1gInSc1	přístav
Durrës	Durrës	k1gInSc1	Durrës
(	(	kIx(	(
<g/>
Drač	Drač	k1gInSc1	Drač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Albánie	Albánie	k1gFnSc2	Albánie
však	však	k9	však
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
MKK	MKK	kA	MKK
jeden	jeden	k4xCgInSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
začali	začít	k5eAaPmAgMnP	začít
i	i	k9	i
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
jednotkami	jednotka	k1gFnPc7	jednotka
opouštět	opouštět	k5eAaImF	opouštět
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
definitivním	definitivní	k2eAgInSc6d1	definitivní
odchodu	odchod	k1gInSc6	odchod
členů	člen	k1gInPc2	člen
MKK	MKK	kA	MKK
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
jednotek	jednotka	k1gFnPc2	jednotka
nenacházeje	nacházet	k5eNaImSgInS	nacházet
východisko	východisko	k1gNnSc4	východisko
opustil	opustit	k5eAaPmAgMnS	opustit
kníže	kníže	k1gMnSc1	kníže
Vilém	Vilém	k1gMnSc1	Vilém
Albánii	Albánie	k1gFnSc3	Albánie
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neabdikuje	abdikovat	k5eNaBmIp3nS	abdikovat
a	a	k8xC	a
že	že	k8xS	že
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
dočasné	dočasný	k2eAgFnSc2d1	dočasná
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
"	"	kIx"	"
<g/>
si	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
lid	lid	k1gInSc4	lid
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
rozmyslet	rozmyslet	k5eAaPmF	rozmyslet
a	a	k8xC	a
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
správnou	správný	k2eAgFnSc4d1	správná
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
Albánie	Albánie	k1gFnSc1	Albánie
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
bojišť	bojiště	k1gNnPc2	bojiště
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ukončení	ukončení	k1gNnSc6	ukončení
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
změně	změna	k1gFnSc3	změna
<g/>
;	;	kIx,	;
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
z	z	k7c2	z
Drače	Drač	k1gInSc2	Drač
do	do	k7c2	do
Tirany	Tirana	k1gFnSc2	Tirana
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
malého	malý	k2eAgMnSc2d1	malý
20	[number]	k4	20
<g/>
tisícového	tisícový	k2eAgNnSc2d1	tisícové
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Republika	republika	k1gFnSc1	republika
a	a	k8xC	a
monarchie	monarchie	k1gFnSc1	monarchie
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
zůstával	zůstávat	k5eAaImAgInS	zůstávat
osud	osud	k1gInSc1	osud
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Albánie	Albánie	k1gFnSc2	Albánie
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c6	v
zrušení	zrušení	k1gNnSc6	zrušení
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
albánskou	albánský	k2eAgFnSc7d1	albánská
postavou	postava	k1gFnSc7	postava
Ahmet	Ahmeta	k1gFnPc2	Ahmeta
Zogu	Zogus	k1gInSc2	Zogus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
premiérem	premiér	k1gMnSc7	premiér
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Albánie	Albánie	k1gFnSc2	Albánie
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
letech	let	k1gInPc6	let
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
král	král	k1gMnSc1	král
Albánců	Albánec	k1gMnPc2	Albánec
Zog	Zog	k1gMnSc1	Zog
I.	I.	kA	I.
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dosazení	dosazení	k1gNnPc1	dosazení
napomohly	napomoct	k5eAaPmAgFnP	napomoct
jugoslávské	jugoslávský	k2eAgFnPc4d1	jugoslávská
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zog	Zog	k?	Zog
se	se	k3xPyFc4	se
však	však	k9	však
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
obrátil	obrátit	k5eAaPmAgInS	obrátit
k	k	k7c3	k
Bělehradu	Bělehrad	k1gInSc3	Bělehrad
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Mussolinim	Mussolini	k1gNnSc7	Mussolini
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
spojenectví	spojenectví	k1gNnSc1	spojenectví
(	(	kIx(	(
<g/>
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
patřilo	patřit	k5eAaImAgNnS	patřit
též	též	k9	též
i	i	k9	i
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
právě	právě	k6eAd1	právě
proti	proti	k7c3	proti
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
překazil	překazit	k5eAaPmAgInS	překazit
vliv	vliv	k1gInSc1	vliv
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc1	pět
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
Albánii	Albánie	k1gFnSc6	Albánie
obsadila	obsadit	k5eAaPmAgFnS	obsadit
fašistická	fašistický	k2eAgFnSc1d1	fašistická
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
titul	titul	k1gInSc1	titul
albánského	albánský	k2eAgMnSc2d1	albánský
krále	král	k1gMnSc2	král
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
italské	italský	k2eAgFnSc2d1	italská
vlády	vláda	k1gFnSc2	vláda
král	král	k1gMnSc1	král
Viktor	Viktor	k1gMnSc1	Viktor
Emanuel	Emanuel	k1gMnSc1	Emanuel
III	III	kA	III
<g/>
..	..	k?	..
Regiony	region	k1gInPc1	region
Kosovo	Kosův	k2eAgNnSc4d1	Kosovo
a	a	k8xC	a
Kamerie	Kamerie	k1gFnSc2	Kamerie
byly	být	k5eAaImAgFnP	být
připojené	připojený	k2eAgFnPc1d1	připojená
k	k	k7c3	k
Albánii	Albánie	k1gFnSc3	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Albánci	Albánec	k1gMnPc1	Albánec
podporovali	podporovat	k5eAaImAgMnP	podporovat
Mussoliniho	Mussolini	k1gMnSc4	Mussolini
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
sousedů	soused	k1gMnPc2	soused
byli	být	k5eAaImAgMnP	být
spojeni	spojen	k2eAgMnPc1d1	spojen
do	do	k7c2	do
loutkového	loutkový	k2eAgNnSc2d1	loutkové
království	království	k1gNnSc2	království
Viktora	Viktor	k1gMnSc2	Viktor
Emanuela	Emanuel	k1gMnSc2	Emanuel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
spoluúčasti	spoluúčast	k1gFnSc2	spoluúčast
albánské	albánský	k2eAgFnSc2d1	albánská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
protifašistické	protifašistický	k2eAgNnSc1d1	protifašistické
partyzánské	partyzánský	k2eAgNnSc1d1	partyzánské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
zemi	zem	k1gFnSc6	zem
osvobodilo	osvobodit	k5eAaPmAgNnS	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Enver	Enver	k1gMnSc1	Enver
Hodža	Hodža	k1gMnSc1	Hodža
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
získal	získat	k5eAaPmAgMnS	získat
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
oblibu	obliba	k1gFnSc4	obliba
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
autoritou	autorita	k1gFnSc7	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
bojů	boj	k1gInPc2	boj
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gFnSc1	jeho
Albánská	albánský	k2eAgFnSc1d1	albánská
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
PPSH	PPSH	kA	PPSH
<g/>
)	)	kIx)	)
moc	moc	k6eAd1	moc
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
provádět	provádět	k5eAaImF	provádět
komunistické	komunistický	k2eAgFnSc2d1	komunistická
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1945	[number]	k4	1945
a	a	k8xC	a
1990	[number]	k4	1990
měla	mít	k5eAaImAgFnS	mít
Albánie	Albánie	k1gFnSc1	Albánie
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgInPc2d3	nejtvrdší
režimů	režim	k1gInPc2	režim
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
se	se	k3xPyFc4	se
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
izolaci	izolace	k1gFnSc6	izolace
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
přerušila	přerušit	k5eAaPmAgFnS	přerušit
kontakty	kontakt	k1gInPc4	kontakt
se	s	k7c7	s
západem	západ	k1gInSc7	západ
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
s	s	k7c7	s
východními	východní	k2eAgFnPc7d1	východní
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
ČSSR	ČSSR	kA	ČSSR
z	z	k7c2	z
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
zničena	zničen	k2eAgFnSc1d1	zničena
spousta	spousta	k1gFnSc1	spousta
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Paranoidní	paranoidní	k2eAgMnSc1d1	paranoidní
diktátor	diktátor	k1gMnSc1	diktátor
Enver	Enver	k1gMnSc1	Enver
Hodža	Hodža	k1gMnSc1	Hodža
rozesel	rozesít	k5eAaPmAgMnS	rozesít
po	po	k7c6	po
krajině	krajina	k1gFnSc6	krajina
betonové	betonový	k2eAgInPc1d1	betonový
bunkry	bunkr	k1gInPc1	bunkr
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
půl	půl	k1xP	půl
až	až	k9	až
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
ateistickou	ateistický	k2eAgFnSc4d1	ateistická
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
praktikování	praktikování	k1gNnSc1	praktikování
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
náboženství	náboženství	k1gNnSc2	náboženství
bylo	být	k5eAaImAgNnS	být
přísně	přísně	k6eAd1	přísně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
;	;	kIx,	;
Albánie	Albánie	k1gFnSc2	Albánie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc4	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
)	)	kIx)	)
oficiálně	oficiálně	k6eAd1	oficiálně
ateistickou	ateistický	k2eAgFnSc7d1	ateistická
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postkomunistická	postkomunistický	k2eAgFnSc1d1	postkomunistická
Albánie	Albánie	k1gFnSc1	Albánie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
zřízena	zřízen	k2eAgFnSc1d1	zřízena
pluralitní	pluralitní	k2eAgFnSc1d1	pluralitní
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Transformace	transformace	k1gFnSc1	transformace
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
obtížnou	obtížný	k2eAgFnSc7d1	obtížná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
musí	muset	k5eAaImIp3nP	muset
vypořádávat	vypořádávat	k5eAaImF	vypořádávat
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
korupcí	korupce	k1gFnSc7	korupce
<g/>
,	,	kIx,	,
zničenou	zničený	k2eAgFnSc7d1	zničená
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc7d1	velká
sítí	síť	k1gFnSc7	síť
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
s	s	k7c7	s
napojením	napojení	k1gNnSc7	napojení
na	na	k7c4	na
vládní	vládní	k2eAgMnPc4d1	vládní
úředníky	úředník	k1gMnPc4	úředník
a	a	k8xC	a
roztříštěnou	roztříštěný	k2eAgFnSc7d1	roztříštěná
politickou	politický	k2eAgFnSc7d1	politická
opozicí	opozice	k1gFnSc7	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
také	také	k9	také
přímo	přímo	k6eAd1	přímo
podporovala	podporovat	k5eAaImAgFnS	podporovat
organizaci	organizace	k1gFnSc4	organizace
UÇK	UÇK	kA	UÇK
působící	působící	k2eAgInSc1d1	působící
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nepokojům	nepokoj	k1gInPc3	nepokoj
i	i	k8xC	i
silným	silný	k2eAgInPc3d1	silný
ekonomickým	ekonomický	k2eAgInPc3d1	ekonomický
propadům	propad	k1gInPc3	propad
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
krutě	krutě	k6eAd1	krutě
potlačované	potlačovaný	k2eAgNnSc1d1	potlačované
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Albánie	Albánie	k1gFnSc1	Albánie
stala	stát	k5eAaPmAgFnS	stát
plnoprávným	plnoprávný	k2eAgInSc7d1	plnoprávný
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
integraci	integrace	k1gFnSc4	integrace
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
a	a	k8xC	a
Jónského	jónský	k2eAgNnSc2d1	Jónské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Albánské	albánský	k2eAgNnSc1d1	albánské
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
362	[number]	k4	362
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
italského	italský	k2eAgInSc2d1	italský
poloostrova	poloostrov	k1gInSc2	poloostrov
Salento	Salento	k1gNnSc1	Salento
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
západě	západ	k1gInSc6	západ
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
73	[number]	k4	73
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Otrantský	Otrantský	k2eAgInSc1d1	Otrantský
průliv	průliv	k1gInSc1	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
albánská	albánský	k2eAgFnSc1d1	albánská
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
720	[number]	k4	720
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
173	[number]	k4	173
km	km	kA	km
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Černou	černý	k2eAgFnSc7d1	černá
Horou	hora	k1gFnSc7	hora
<g/>
,	,	kIx,	,
114	[number]	k4	114
km	km	kA	km
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
<g/>
,	,	kIx,	,
151	[number]	k4	151
km	km	kA	km
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Makedonií	Makedonie	k1gFnSc7	Makedonie
a	a	k8xC	a
282	[number]	k4	282
km	km	kA	km
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
28	[number]	k4	28
748	[number]	k4	748
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
nejmenším	malý	k2eAgInSc7d3	nejmenší
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
2,7	[number]	k4	2,7
<g/>
×	×	k?	×
menším	menšit	k5eAaImIp1nS	menšit
než	než	k8xS	než
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
horstva	horstvo	k1gNnPc4	horstvo
albánsko-řecké	albánsko-řecký	k2eAgFnSc2d1	albánsko-řecký
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
leží	ležet	k5eAaImIp3nS	ležet
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Korab	Koraba	k1gFnPc2	Koraba
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
horstva	horstvo	k1gNnPc1	horstvo
Dinárských	dinárský	k2eAgInPc2d1	dinárský
hor.	hor.	k?	hor.
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Drin	Drin	k1gInSc1	Drin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
leží	ležet	k5eAaImIp3nP	ležet
hraniční	hraniční	k2eAgInSc4d1	hraniční
jezera	jezero	k1gNnSc2	jezero
Skadarské	skadarský	k2eAgInPc4d1	skadarský
<g/>
,	,	kIx,	,
Ohridské	Ohridský	k2eAgInPc4d1	Ohridský
a	a	k8xC	a
Prespanské	Prespanský	k2eAgInPc4d1	Prespanský
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
ryze	ryze	k6eAd1	ryze
středomořské	středomořský	k2eAgNnSc1d1	středomořské
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
panují	panovat	k5eAaImIp3nP	panovat
suchá	suchý	k2eAgNnPc4d1	suché
horká	horký	k2eAgNnPc4d1	horké
léta	léto	k1gNnPc4	léto
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
okolo	okolo	k7c2	okolo
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
mírné	mírný	k2eAgFnSc2d1	mírná
vlhké	vlhký	k2eAgFnSc2d1	vlhká
zimy	zima	k1gFnSc2	zima
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
okolo	okolo	k7c2	okolo
7	[number]	k4	7
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
hornatém	hornatý	k2eAgNnSc6d1	hornaté
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
až	až	k9	až
2000	[number]	k4	2000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
určuje	určovat	k5eAaImIp3nS	určovat
ústava	ústava	k1gFnSc1	ústava
přijatá	přijatý	k2eAgFnSc1d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Exekutivní	exekutivní	k2eAgFnSc1d1	exekutivní
moc	moc	k1gFnSc1	moc
má	mít	k5eAaImIp3nS	mít
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
předseda	předseda	k1gMnSc1	předseda
(	(	kIx(	(
<g/>
Kryeministri	Kryeministri	k1gNnSc1	Kryeministri
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
osobou	osoba	k1gFnSc7	osoba
v	v	k7c6	v
albánské	albánský	k2eAgFnSc6d1	albánská
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgFnSc7d1	formální
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volený	volený	k2eAgInSc1d1	volený
parlamentem	parlament	k1gInSc7	parlament
(	(	kIx(	(
<g/>
Kuvendi	Kuvend	k1gMnPc1	Kuvend
neboli	neboli	k8xC	neboli
Shromážděním	shromáždění	k1gNnSc7	shromáždění
Albánské	albánský	k2eAgFnSc2d1	albánská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
a	a	k8xC	a
bez	bez	k7c2	bez
rozpravy	rozprava	k1gFnSc2	rozprava
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
tří	tři	k4xCgFnPc2	tři
pětin	pětina	k1gFnPc2	pětina
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
kandidát	kandidát	k1gMnSc1	kandidát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgNnSc4d1	jdoucí
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
též	též	k6eAd1	též
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Legislativní	legislativní	k2eAgFnSc4d1	legislativní
pravomoc	pravomoc	k1gFnSc4	pravomoc
má	mít	k5eAaImIp3nS	mít
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
140	[number]	k4	140
poslanců	poslanec	k1gMnPc2	poslanec
je	být	k5eAaImIp3nS	být
voleno	volen	k2eAgNnSc1d1	voleno
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentarismus	parlamentarismus	k1gInSc1	parlamentarismus
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
silnou	silný	k2eAgFnSc4d1	silná
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
parlament	parlament	k1gInSc1	parlament
byl	být	k5eAaImAgInS	být
svolán	svolat	k5eAaPmNgInS	svolat
již	již	k9	již
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1444	[number]	k4	1444
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Lezhë	Lezhë	k1gFnSc6	Lezhë
<g/>
,	,	kIx,	,
národním	národní	k2eAgMnSc7d1	národní
hrdinou	hrdina	k1gMnSc7	hrdina
Skanderbegem	Skanderbeg	k1gMnSc7	Skanderbeg
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
politický	politický	k2eAgInSc4d1	politický
systém	systém	k1gInSc4	systém
demokratický	demokratický	k2eAgInSc4d1	demokratický
<g/>
,	,	kIx,	,
pluralitní	pluralitní	k2eAgInSc4d1	pluralitní
a	a	k8xC	a
vícestranický	vícestranický	k2eAgInSc4d1	vícestranický
<g/>
.	.	kIx.	.
</s>
<s>
Limit	limit	k1gInSc1	limit
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
procenta	procento	k1gNnSc2	procento
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
koalici	koalice	k1gFnSc4	koalice
5	[number]	k4	5
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
volit	volit	k5eAaImF	volit
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c4	na
výkonné	výkonný	k2eAgFnPc4d1	výkonná
a	a	k8xC	a
zákonodárné	zákonodárný	k2eAgFnPc4d1	zákonodárná
moci	moc	k1gFnPc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
Ilir	Ilir	k1gMnSc1	Ilir
Meta	meta	k1gFnSc1	meta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
premiérem	premiér	k1gMnSc7	premiér
Edi	Edi	k1gFnSc2	Edi
Rama	Ram	k1gInSc2	Ram
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
vedly	vést	k5eAaImAgFnP	vést
všechny	všechen	k3xTgFnPc1	všechen
vlády	vláda	k1gFnSc2	vláda
dvě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
středolevá	středolevý	k2eAgFnSc1d1	středolevá
<g/>
,	,	kIx,	,
proevropská	proevropský	k2eAgFnSc1d1	proevropská
<g/>
,	,	kIx,	,
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
strana	strana	k1gFnSc1	strana
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
strana	strana	k1gFnSc1	strana
Albánie	Albánie	k1gFnSc1	Albánie
(	(	kIx(	(
<g/>
Partia	Partia	k1gFnSc1	Partia
Socialiste	socialist	k1gMnSc5	socialist
e	e	k0	e
Shqipërisë	Shqipërisë	k1gMnSc5	Shqipërisë
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
středopravá	středopravý	k2eAgFnSc1d1	středopravá
<g/>
,	,	kIx,	,
liberálně-konzervativní	liberálněonzervativní	k2eAgFnSc1d1	liberálně-konzervativní
a	a	k8xC	a
proevropská	proevropský	k2eAgFnSc1d1	proevropská
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Albánie	Albánie	k1gFnSc1	Albánie
(	(	kIx(	(
<g/>
Partia	Partia	k1gFnSc1	Partia
Demokratike	Demokratik	k1gInSc2	Demokratik
e	e	k0	e
Shqipërisë	Shqipërisë	k1gFnSc3	Shqipërisë
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
12	[number]	k4	12
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
albánsky	albánsky	k6eAd1	albánsky
<g/>
:	:	kIx,	:
qark	qark	k6eAd1	qark
<g/>
/	/	kIx~	/
<g/>
qarku	qarku	k6eAd1	qarku
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
prefekturë	prefekturë	k?	prefekturë
<g/>
/	/	kIx~	/
<g/>
prefektura	prefektura	k1gFnSc1	prefektura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
36	[number]	k4	36
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
rrethe	rrethat	k5eAaPmIp3nS	rrethat
<g/>
)	)	kIx)	)
a	a	k8xC	a
351	[number]	k4	351
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
polovinou	polovina	k1gFnSc7	polovina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zabývající	zabývající	k2eAgNnSc1d1	zabývající
se	s	k7c7	s
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
korupcí	korupce	k1gFnSc7	korupce
dosahující	dosahující	k2eAgMnSc1d1	dosahující
až	až	k6eAd1	až
na	na	k7c4	na
nejvyšší	vysoký	k2eAgNnPc4d3	nejvyšší
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
organizovaným	organizovaný	k2eAgInSc7d1	organizovaný
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
dováží	dovážet	k5eAaImIp3nP	dovážet
hodně	hodně	k6eAd1	hodně
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
finanční	finanční	k2eAgFnSc2d1	finanční
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
přivezených	přivezený	k2eAgInPc2d1	přivezený
uprchlíky	uprchlík	k1gMnPc4	uprchlík
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nerostným	nerostný	k2eAgNnSc7d1	nerostné
bohatstvím	bohatství	k1gNnSc7	bohatství
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc4	měď
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
barevné	barevný	k2eAgInPc4d1	barevný
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
albánský	albánský	k2eAgInSc1d1	albánský
lek	lek	k1gInSc1	lek
(	(	kIx(	(
<g/>
100	[number]	k4	100
leků	lek	k1gInPc2	lek
=	=	kIx~	=
0,71	[number]	k4	0,71
EUR	euro	k1gNnPc2	euro
=	=	kIx~	=
19,20	[number]	k4	19,20
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Turistický	turistický	k2eAgInSc4d1	turistický
potenciál	potenciál	k1gInSc4	potenciál
Albánie	Albánie	k1gFnSc2	Albánie
je	být	k5eAaImIp3nS	být
perspektivní	perspektivní	k2eAgNnSc1d1	perspektivní
a	a	k8xC	a
bezesporu	bezesporu	k9	bezesporu
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
příjemné	příjemný	k2eAgNnSc1d1	příjemné
<g/>
,	,	kIx,	,
albánské	albánský	k2eAgNnSc1d1	albánské
pobřeží	pobřeží	k1gNnSc1	pobřeží
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
a	a	k8xC	a
Jónského	jónský	k2eAgNnSc2d1	Jónské
moře	moře	k1gNnSc2	moře
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
420	[number]	k4	420
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
turistická	turistický	k2eAgNnPc4d1	turistické
střediska	středisko	k1gNnPc4	středisko
patří	patřit	k5eAaImIp3nP	patřit
okolí	okolí	k1gNnPc1	okolí
Drače	Drač	k1gFnSc2	Drač
(	(	kIx(	(
<g/>
Jadran	Jadran	k1gInSc1	Jadran
<g/>
)	)	kIx)	)
a	a	k8xC	a
riviéra	riviéra	k1gFnSc1	riviéra
mezi	mezi	k7c7	mezi
Vlorou	Vlora	k1gFnSc7	Vlora
a	a	k8xC	a
Sarandou	Saranda	k1gFnSc7	Saranda
(	(	kIx(	(
<g/>
Jónské	jónský	k2eAgNnSc1d1	Jónské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
potenciál	potenciál	k1gInSc1	potenciál
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
také	také	k9	také
horské	horský	k2eAgFnPc1d1	horská
oblasti	oblast	k1gFnPc1	oblast
Albánie	Albánie	k1gFnPc1	Albánie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
ohromné	ohromný	k2eAgNnSc4d1	ohromné
množství	množství	k1gNnSc4	množství
přírodních	přírodní	k2eAgFnPc2d1	přírodní
i	i	k8xC	i
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
velkolepé	velkolepý	k2eAgFnPc1d1	velkolepá
scenérie	scenérie	k1gFnPc1	scenérie
horských	horský	k2eAgInPc2d1	horský
masivů	masiv	k1gInPc2	masiv
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výška	výška	k1gFnSc1	výška
běžně	běžně	k6eAd1	běžně
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
země	zem	k1gFnSc2	zem
zde	zde	k6eAd1	zde
však	však	k9	však
je	být	k5eAaImIp3nS	být
turistická	turistický	k2eAgFnSc1d1	turistická
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
stále	stále	k6eAd1	stále
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
je	být	k5eAaImIp3nS	být
rozvíjena	rozvíjet	k5eAaImNgFnS	rozvíjet
a	a	k8xC	a
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Albánských	albánský	k2eAgFnPc6d1	albánská
Alpách	Alpy	k1gFnPc6	Alpy
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
českým	český	k2eAgInSc7d1	český
dobrovolnickým	dobrovolnický	k2eAgInSc7d1	dobrovolnický
projektem	projekt	k1gInSc7	projekt
Albanian	Albaniana	k1gFnPc2	Albaniana
Challenge	Challenge	k1gFnPc2	Challenge
podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
modelu	model	k1gInSc2	model
značení	značení	k1gNnSc2	značení
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3	nejrozsáhlejší
síť	síť	k1gFnSc1	síť
turistických	turistický	k2eAgFnPc2d1	turistická
stezek	stezka	k1gFnPc2	stezka
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čítá	čítat	k5eAaImIp3nS	čítat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
km	km	kA	km
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Propojuje	propojovat	k5eAaImIp3nS	propojovat
tak	tak	k9	tak
známé	známý	k2eAgFnSc2d1	známá
turistické	turistický	k2eAgFnSc2d1	turistická
lokality	lokalita	k1gFnSc2	lokalita
Theth	Thetha	k1gFnPc2	Thetha
a	a	k8xC	a
Valbonu	Valbona	k1gFnSc4	Valbona
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Curraj	Curraj	k1gFnSc1	Curraj
i	i	k8xC	i
Epërm	Epërm	k1gInSc1	Epërm
a	a	k8xC	a
přehradou	přehrada	k1gFnSc7	přehrada
Koman	Komana	k1gFnPc2	Komana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozvoje	rozvoj	k1gInSc2	rozvoj
turistické	turistický	k2eAgFnSc2d1	turistická
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nP	stavit
nové	nový	k2eAgFnPc1d1	nová
ubytovací	ubytovací	k2eAgFnPc1d1	ubytovací
kapacity	kapacita	k1gFnPc1	kapacita
s	s	k7c7	s
požadovanou	požadovaný	k2eAgFnSc7d1	požadovaná
vybaveností	vybavenost	k1gFnSc7	vybavenost
(	(	kIx(	(
<g/>
hotely	hotel	k1gInPc4	hotel
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
pizzerie	pizzerie	k1gFnPc4	pizzerie
<g/>
,	,	kIx,	,
kavárny	kavárna	k1gFnPc4	kavárna
a	a	k8xC	a
samoobsluhy	samoobsluha	k1gFnPc4	samoobsluha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnPc1	Albánie
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
usiluje	usilovat	k5eAaImIp3nS	usilovat
i	i	k9	i
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
kvality	kvalita	k1gFnSc2	kvalita
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
stále	stále	k6eAd1	stále
v	v	k7c6	v
neudržovaném	udržovaný	k2eNgInSc6d1	neudržovaný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
využívání	využívání	k1gNnSc4	využívání
volného	volný	k2eAgInSc2d1	volný
časů	čas	k1gInPc2	čas
turistů	turist	k1gMnPc2	turist
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
albánského	albánský	k2eAgNnSc2d1	albánské
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
turistiky	turistika	k1gFnSc2	turistika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
zemi	zem	k1gFnSc4	zem
375	[number]	k4	375
000	[number]	k4	000
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
98,5	[number]	k4	98,5
<g/>
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
albánská	albánský	k2eAgFnSc1d1	albánská
politika	politika	k1gFnSc1	politika
podle	podle	k7c2	podle
různých	různý	k2eAgFnPc2d1	různá
organizací	organizace	k1gFnPc2	organizace
nepřiznává	přiznávat	k5eNaImIp3nS	přiznávat
pravdivá	pravdivý	k2eAgNnPc4d1	pravdivé
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přikrášluje	přikrášlovat	k5eAaImIp3nS	přikrášlovat
je	on	k3xPp3gMnPc4	on
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Albánců	Albánec	k1gMnPc2	Albánec
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
menšina	menšina	k1gFnSc1	menšina
však	však	k9	však
proti	proti	k7c3	proti
výsledkům	výsledek	k1gInPc3	výsledek
neprotestovala	protestovat	k5eNaBmAgFnS	protestovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
menšinu	menšina	k1gFnSc4	menšina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Řekové	Řek	k1gMnPc1	Řek
s	s	k7c7	s
asi	asi	k9	asi
14	[number]	k4	14
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
Arumuni	Arumuen	k2eAgMnPc1d1	Arumuen
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
Černohorci	Černohorec	k1gMnPc1	Černohorec
<g/>
,	,	kIx,	,
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
Balkánští	balkánský	k2eAgMnPc1d1	balkánský
Romové	Rom	k1gMnPc1	Rom
a	a	k8xC	a
slovanští	slovanský	k2eAgMnPc1d1	slovanský
Makedonci	Makedonec	k1gMnPc1	Makedonec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
etnických	etnický	k2eAgMnPc2d1	etnický
Albánců	Albánec	k1gMnPc2	Albánec
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
v	v	k7c6	v
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc4d1	severní
Makedonii	Makedonie	k1gFnSc4	Makedonie
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
800	[number]	k4	800
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgFnSc3d1	Černé
Hoře	hora	k1gFnSc3	hora
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
62	[number]	k4	62
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Albánci	Albánec	k1gMnPc1	Albánec
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
menšin	menšina	k1gFnPc2	menšina
(	(	kIx(	(
<g/>
až	až	k9	až
600	[number]	k4	600
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řekové	Řek	k1gMnPc1	Řek
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Albánie	Albánie	k1gFnSc2	Albánie
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
se	se	k3xPyFc4	se
v	v	k7c6	v
městech	město	k1gNnPc6	město
Gjirokastër	Gjirokastëra	k1gFnPc2	Gjirokastëra
<g/>
,	,	kIx,	,
Himara	Himara	k1gFnSc1	Himara
<g/>
,	,	kIx,	,
Saranda	Saranda	k1gFnSc1	Saranda
<g/>
,	,	kIx,	,
Vlora	Vlora	k1gFnSc1	Vlora
<g/>
,	,	kIx,	,
Delvina	Delvina	k1gFnSc1	Delvina
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
obývají	obývat	k5eAaImIp3nP	obývat
i	i	k9	i
albánský	albánský	k2eAgInSc4d1	albánský
ostrov	ostrov	k1gInSc4	ostrov
Ksamil	Ksamil	k1gMnSc2	Ksamil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
řeckého	řecký	k2eAgInSc2d1	řecký
ostrova	ostrov	k1gInSc2	ostrov
Korfu	Korfu	k1gNnSc1	Korfu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arumuni	Arumun	k1gMnPc1	Arumun
obývají	obývat	k5eAaImIp3nP	obývat
jižní	jižní	k2eAgFnSc4d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
střediskem	středisko	k1gNnSc7	středisko
arumunů	arumun	k1gInPc2	arumun
je	být	k5eAaImIp3nS	být
však	však	k9	však
město	město	k1gNnSc1	město
Korçë	Korçë	k1gFnPc2	Korçë
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
bylo	být	k5eAaImAgNnS	být
známé	známý	k2eAgFnPc4d1	známá
svými	svůj	k3xOyFgFnPc7	svůj
podnikatelskými	podnikatelský	k2eAgFnPc7d1	podnikatelská
aktivitami	aktivita	k1gFnPc7	aktivita
již	již	k6eAd1	již
v	v	k7c6	v
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
Arumunským	Arumunský	k2eAgMnSc7d1	Arumunský
centrem	centr	k1gMnSc7	centr
je	být	k5eAaImIp3nS	být
i	i	k9	i
město	město	k1gNnSc1	město
Voskopojë	Voskopojë	k1gFnSc2	Voskopojë
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žili	žít	k5eAaImAgMnP	žít
i	i	k9	i
řečtí	řecký	k2eAgMnPc1d1	řecký
podnikatelé	podnikatel	k1gMnPc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
žijí	žít	k5eAaImIp3nP	žít
Arumuni	Arumun	k1gMnPc1	Arumun
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Divjaka	Divjak	k1gMnSc2	Divjak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Makedonií	Makedonie	k1gFnSc7	Makedonie
u	u	k7c2	u
Prespanského	Prespanský	k2eAgNnSc2d1	Prespanské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
etnických	etnický	k2eAgMnPc2d1	etnický
Řeků	Řek	k1gMnPc2	Řek
se	se	k3xPyFc4	se
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
vystěhovalo	vystěhovat	k5eAaPmAgNnS	vystěhovat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
emigrace	emigrace	k1gFnSc2	emigrace
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
ekonomickým	ekonomický	k2eAgInPc3d1	ekonomický
důvodům	důvod	k1gInPc3	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Řeků	Řek	k1gMnPc2	Řek
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
emigrují	emigrovat	k5eAaBmIp3nP	emigrovat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
i	i	k8xC	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
skupiny	skupina	k1gFnPc4	skupina
žijící	žijící	k2eAgFnPc4d1	žijící
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
patří	patřit	k5eAaImIp3nP	patřit
Ghegové	Gheg	k1gMnPc1	Gheg
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
přetrvávalo	přetrvávat	k5eAaImAgNnS	přetrvávat
rodové	rodový	k2eAgNnSc1d1	rodové
zřízení	zřízení	k1gNnSc1	zřízení
a	a	k8xC	a
zvykové	zvykový	k2eAgNnSc1d1	zvykové
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Ghegské	Ghegský	k2eAgNnSc1d1	Ghegský
nářečí	nářečí	k1gNnSc1	nářečí
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
literárním	literární	k2eAgInSc7d1	literární
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Toskové	Tosková	k1gFnPc1	Tosková
–	–	k?	–
žijí	žít	k5eAaImIp3nP	žít
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Skhumbin	Skhumbin	k2eAgInSc1d1	Skhumbin
<g/>
,	,	kIx,	,
toskický	toskický	k2eAgInSc1d1	toskický
dialekt	dialekt	k1gInSc1	dialekt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
převzat	převzít	k5eAaPmNgInS	převzít
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Albánie	Albánie	k1gFnSc2	Albánie
je	být	k5eAaImIp3nS	být
albánština	albánština	k1gFnSc1	albánština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
domluvit	domluvit	k5eAaPmF	domluvit
i	i	k9	i
řecky	řecky	k6eAd1	řecky
<g/>
.	.	kIx.	.
</s>
<s>
Albánci	Albánec	k1gMnPc1	Albánec
rozumí	rozumět	k5eAaImIp3nP	rozumět
zpravidla	zpravidla	k6eAd1	zpravidla
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
nejenom	nejenom	k6eAd1	nejenom
díky	díky	k7c3	díky
rozvětveným	rozvětvený	k2eAgFnPc3d1	rozvětvená
rodinným	rodinný	k2eAgFnPc3d1	rodinná
vazbám	vazba	k1gFnPc3	vazba
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
díky	díky	k7c3	díky
činnosti	činnost	k1gFnSc3	činnost
místních	místní	k2eAgNnPc2d1	místní
médií	médium	k1gNnPc2	médium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
rádio	rádio	k1gNnSc1	rádio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
albánskou	albánský	k2eAgFnSc7d1	albánská
komunitou	komunita	k1gFnSc7	komunita
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
)	)	kIx)	)
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
městě	město	k1gNnSc6	město
Piana	piano	k1gNnSc2	piano
degli	degnout	k5eAaPmAgMnP	degnout
Albanesi	Albanese	k1gFnSc4	Albanese
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
této	tento	k3xDgFnSc2	tento
komunity	komunita	k1gFnSc2	komunita
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
archaické	archaický	k2eAgFnSc2d1	archaická
formy	forma	k1gFnSc2	forma
albánského	albánský	k2eAgInSc2d1	albánský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
nadvlády	nadvláda	k1gFnSc2	nadvláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
náboženstvím	náboženství	k1gNnSc7	náboženství
Albánie	Albánie	k1gFnSc2	Albánie
sunnitská	sunnitský	k2eAgFnSc1d1	sunnitská
odnož	odnož	k1gFnSc1	odnož
islámu	islám	k1gInSc2	islám
(	(	kIx(	(
<g/>
56,70	[number]	k4	56,70
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
islámských	islámský	k2eAgFnPc2d1	islámská
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zapsal	zapsat	k5eAaPmAgMnS	zapsat
islámský	islámský	k2eAgInSc4d1	islámský
mystický	mystický	k2eAgInSc4d1	mystický
řád	řád	k1gInSc4	řád
tzv.	tzv.	kA	tzv.
Bektashizmi	Bektashiz	k1gFnPc7	Bektashiz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Bektašíja	Bektašíja	k1gMnSc1	Bektašíja
<g/>
,	,	kIx,	,
Bektaš	Bektaš	k1gMnSc1	Bektaš
<g/>
(	(	kIx(	(
<g/>
i	i	k8xC	i
<g/>
)	)	kIx)	)
<g/>
ové	ové	k0wR	ové
<g/>
,	,	kIx,	,
Bektašismus	Bektašismus	k1gInSc1	Bektašismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dnes	dnes	k6eAd1	dnes
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
2,09	[number]	k4	2,09
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
hlavními	hlavní	k2eAgMnPc7d1	hlavní
náboženstvími	náboženství	k1gNnPc7	náboženství
jsou	být	k5eAaImIp3nP	být
albánské	albánský	k2eAgNnSc4d1	albánské
ortodoxní	ortodoxní	k2eAgNnSc4d1	ortodoxní
křesťanství	křesťanství	k1gNnSc4	křesťanství
(	(	kIx(	(
<g/>
6,75	[number]	k4	6,75
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římskokatolické	římskokatolický	k2eAgNnSc4d1	římskokatolické
křesťanství	křesťanství	k1gNnSc4	křesťanství
(	(	kIx(	(
<g/>
10,03	[number]	k4	10,03
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protestanté	protestant	k1gMnPc1	protestant
(	(	kIx(	(
<g/>
0,14	[number]	k4	0,14
%	%	kIx~	%
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiné	k1gNnSc4	jiné
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ateisté	ateista	k1gMnPc1	ateista
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ortodoxní	ortodoxní	k2eAgMnPc1d1	ortodoxní
Albánci	Albánec	k1gMnPc1	Albánec
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Albánci	Albánec	k1gMnPc1	Albánec
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
pak	pak	k6eAd1	pak
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
byly	být	k5eAaImAgInP	být
náboženské	náboženský	k2eAgInPc1d1	náboženský
projevy	projev	k1gInPc1	projev
pronásledovány	pronásledován	k2eAgInPc1d1	pronásledován
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
mešit	mešita	k1gFnPc2	mešita
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
náboženský	náboženský	k2eAgInSc1d1	náboženský
projev	projev	k1gInSc1	projev
byl	být	k5eAaImAgInS	být
trestán	trestat	k5eAaImNgInS	trestat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
některých	některý	k3yIgFnPc2	některý
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
všude	všude	k6eAd1	všude
<g/>
.	.	kIx.	.
</s>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kosovem	Kosov	k1gInSc7	Kosov
a	a	k8xC	a
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
většinou	většina	k1gFnSc7	většina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
nepanují	panovat	k5eNaImIp3nP	panovat
žádné	žádný	k3yNgInPc4	žádný
výrazné	výrazný	k2eAgInPc4d1	výrazný
náboženské	náboženský	k2eAgInPc4d1	náboženský
střety	střet	k1gInPc4	střet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Albánci	Albánec	k1gMnPc1	Albánec
jsou	být	k5eAaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
svou	svůj	k3xOyFgFnSc7	svůj
bohatou	bohatý	k2eAgFnSc7d1	bohatá
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k6eAd1	ještě
pohanské	pohanský	k2eAgInPc4d1	pohanský
<g/>
,	,	kIx,	,
předkřesťanské	předkřesťanský	k2eAgInPc4d1	předkřesťanský
kořeny	kořen	k1gInPc4	kořen
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
jistou	jistý	k2eAgFnSc4d1	jistá
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInPc1d1	kulturní
rozdíly	rozdíl	k1gInPc1	rozdíl
existují	existovat	k5eAaImIp3nP	existovat
mezi	mezi	k7c7	mezi
severními	severní	k2eAgMnPc7d1	severní
a	a	k8xC	a
jižními	jižní	k2eAgMnPc7d1	jižní
Albánci	Albánec	k1gMnPc7	Albánec
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgMnPc1d1	severní
Albánci	Albánec	k1gMnPc1	Albánec
mají	mít	k5eAaImIp3nP	mít
kulturu	kultura	k1gFnSc4	kultura
podobnou	podobný	k2eAgFnSc4d1	podobná
ostatnímu	ostatní	k2eAgInSc3d1	ostatní
Balkánu	Balkán	k1gInSc3	Balkán
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ti	ten	k3xDgMnPc1	ten
jižní	jižní	k2eAgMnPc1d1	jižní
byli	být	k5eAaImAgMnP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
silnému	silný	k2eAgInSc3d1	silný
řeckému	řecký	k2eAgInSc3d1	řecký
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
vlivu	vliv	k1gInSc3	vliv
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
na	na	k7c6	na
tradiční	tradiční	k2eAgFnSc6d1	tradiční
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
však	však	k9	však
odrážejí	odrážet	k5eAaImIp3nP	odrážet
i	i	k9	i
silný	silný	k2eAgInSc4d1	silný
osmanský	osmanský	k2eAgInSc4d1	osmanský
kulturní	kulturní	k2eAgInSc4d1	kulturní
odkaz	odkaz	k1gInSc4	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
albánské	albánský	k2eAgFnPc1d1	albánská
legendy	legenda	k1gFnPc1	legenda
jsou	být	k5eAaImIp3nP	být
Gjergj	Gjergj	k1gInSc4	Gjergj
Elez	Eleza	k1gFnPc2	Eleza
Alia	Ali	k1gInSc2	Ali
či	či	k8xC	či
Rozafa	Rozaf	k1gMnSc2	Rozaf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgFnSc4d1	tradiční
lidovou	lidový	k2eAgFnSc4d1	lidová
hudbu	hudba	k1gFnSc4	hudba
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
severoalbánský	severoalbánský	k2eAgInSc4d1	severoalbánský
a	a	k8xC	a
jihooalbánský	jihooalbánský	k2eAgInSc4d1	jihooalbánský
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Albánie	Albánie	k1gFnSc2	Albánie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
balkánském	balkánský	k2eAgInSc6d1	balkánský
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
srbské	srbský	k2eAgFnSc3d1	Srbská
národní	národní	k2eAgFnSc3d1	národní
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Albánie	Albánie	k1gFnSc2	Albánie
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
podobá	podobat	k5eAaImIp3nS	podobat
řecké	řecký	k2eAgFnSc3d1	řecká
lidové	lidový	k2eAgFnSc3d1	lidová
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
té	ten	k3xDgFnSc2	ten
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Epirus	Epirus	k1gInSc1	Epirus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
nástroji	nástroj	k1gInPc7	nástroj
jsou	být	k5eAaImIp3nP	být
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Zpívají	zpívat	k5eAaImIp3nP	zpívat
se	se	k3xPyFc4	se
polyfonní	polyfonní	k2eAgFnPc1d1	polyfonní
melodie	melodie	k1gFnPc1	melodie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
albánské	albánský	k2eAgFnSc6d1	albánská
hudbě	hudba	k1gFnSc6	hudba
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
i	i	k8xC	i
italský	italský	k2eAgInSc1d1	italský
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradičních	tradiční	k2eAgInPc6d1	tradiční
tancích	tanec	k1gInPc6	tanec
je	být	k5eAaImIp3nS	být
také	také	k9	také
patrný	patrný	k2eAgInSc1d1	patrný
silný	silný	k2eAgInSc1d1	silný
řecký	řecký	k2eAgInSc1d1	řecký
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
albánskému	albánský	k2eAgInSc3d1	albánský
mužskému	mužský	k2eAgInSc3d1	mužský
kroji	kroj	k1gInSc3	kroj
patří	patřit	k5eAaImIp3nS	patřit
suknice	suknice	k1gFnSc1	suknice
fustanella	fustanella	k1gFnSc1	fustanella
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
nosí	nosit	k5eAaImIp3nP	nosit
i	i	k9	i
pevninští	pevninský	k2eAgMnPc1d1	pevninský
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
jižní	jižní	k2eAgMnPc1d1	jižní
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
albánský	albánský	k2eAgInSc1d1	albánský
kroj	kroj	k1gInSc1	kroj
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
řeckému	řecký	k2eAgNnSc3d1	řecké
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
také	také	k9	také
řecký	řecký	k2eAgMnSc1d1	řecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
albánským	albánský	k2eAgMnSc7d1	albánský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
je	být	k5eAaImIp3nS	být
Ismail	Ismail	k1gMnSc1	Ismail
Kadare	Kadar	k1gMnSc5	Kadar
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
Gjon	Gjon	k1gMnSc1	Gjon
Buzuku	Buzuk	k1gInSc2	Buzuk
napsal	napsat	k5eAaPmAgMnS	napsat
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
v	v	k7c6	v
albánštině	albánština	k1gFnSc6	albánština
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
prvního	první	k4xOgMnSc2	první
albánského	albánský	k2eAgMnSc2d1	albánský
spisovatele	spisovatel	k1gMnSc2	spisovatel
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
Pjetër	Pjetër	k1gInSc1	Pjetër
Bogdani	Bogdan	k1gMnPc1	Bogdan
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
středověké	středověký	k2eAgFnSc2d1	středověká
albánštiny	albánština	k1gFnSc2	albánština
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Frang	Frang	k1gInSc1	Frang
Bardhi	Bardh	k1gFnSc2	Bardh
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
albánského	albánský	k2eAgNnSc2d1	albánské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
byl	být	k5eAaImAgInS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
Naim	Naima	k1gFnPc2	Naima
Frashëri	Frashër	k1gFnSc2	Frashër
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Sami	sám	k3xTgMnPc1	sám
Frašeri	Frašer	k1gMnPc1	Frašer
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
epos	epos	k1gInSc1	epos
Horská	horský	k2eAgFnSc1d1	horská
loutna	loutna	k1gFnSc1	loutna
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Gjergj	Gjergj	k1gMnSc1	Gjergj
Fishta	Fishta	k1gMnSc1	Fishta
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
Aleksandëra	Aleksandër	k1gInSc2	Aleksandër
Stavre	Stavr	k1gInSc5	Stavr
Drenovy	Drenov	k1gInPc7	Drenov
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
textem	text	k1gInSc7	text
albánské	albánský	k2eAgFnSc2d1	albánská
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
socialistické	socialistický	k2eAgFnSc2d1	socialistická
poezie	poezie	k1gFnSc2	poezie
byl	být	k5eAaImAgInS	být
Dritëro	Dritëro	k1gNnSc4	Dritëro
Agolli	Agoll	k1gMnSc3	Agoll
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
albánské	albánský	k2eAgFnPc4d1	albánská
osobnosti	osobnost	k1gFnPc4	osobnost
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
malíř	malíř	k1gMnSc1	malíř
Ibrahim	Ibrahim	k1gMnSc1	Ibrahim
Kodra	Kodra	k1gMnSc1	Kodra
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Simon	Simon	k1gMnSc1	Simon
Gjoni	Gjoň	k1gMnSc3	Gjoň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
interprety	interpret	k1gMnPc7	interpret
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
sopranistka	sopranistka	k1gFnSc1	sopranistka
Inva	Inva	k1gFnSc1	Inva
Mula	mula	k1gFnSc1	mula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
produkcích	produkce	k1gFnPc6	produkce
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
herečka	herečka	k1gFnSc1	herečka
Masiela	Masiel	k1gMnSc4	Masiel
Lusha	Lush	k1gMnSc4	Lush
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
opačná	opačný	k2eAgFnSc1d1	opačná
gestikulace	gestikulace	k1gFnSc1	gestikulace
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
:	:	kIx,	:
vertikálními	vertikální	k2eAgInPc7d1	vertikální
pohyby	pohyb	k1gInPc7	pohyb
hlavy	hlava	k1gFnSc2	hlava
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
nesouhlas	nesouhlas	k1gInSc1	nesouhlas
<g/>
,	,	kIx,	,
horizontálními	horizontální	k2eAgFnPc7d1	horizontální
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ústně	ústně	k6eAd1	ústně
předávané	předávaný	k2eAgNnSc1d1	předávané
zvykové	zvykový	k2eAgNnSc1d1	zvykové
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kanun	Kanuno	k1gNnPc2	Kanuno
di	di	k?	di
Lekë	Lekë	k1gFnSc2	Lekë
Dukagjini	Dukagjin	k1gMnPc1	Dukagjin
příznačný	příznačný	k2eAgInSc4d1	příznačný
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
tribalismus	tribalismus	k1gInSc4	tribalismus
<g/>
.	.	kIx.	.
</s>
<s>
Kanun	Kanun	k1gInSc1	Kanun
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
základní	základní	k2eAgInPc4d1	základní
pilíře	pilíř	k1gInPc4	pilíř
<g/>
:	:	kIx,	:
loajalitu	loajalita	k1gFnSc4	loajalita
k	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc4	čest
osobní	osobní	k2eAgFnSc4d1	osobní
i	i	k9	i
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
<g/>
,	,	kIx,	,
pohostinnost	pohostinnost	k1gFnSc4	pohostinnost
a	a	k8xC	a
morálně	morálně	k6eAd1	morálně
správné	správný	k2eAgNnSc4d1	správné
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnPc4d3	nejznámější
pravidla	pravidlo	k1gNnPc4	pravidlo
kodexu	kodex	k1gInSc2	kodex
patří	patřit	k5eAaImIp3nS	patřit
nechvalně	chvalně	k6eNd1	chvalně
známá	známý	k2eAgFnSc1d1	známá
krevní	krevní	k2eAgFnSc1d1	krevní
msta	msta	k1gFnSc1	msta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
institut	institut	k1gInSc4	institut
burneshi	burnesh	k1gFnSc2	burnesh
nebo	nebo	k8xC	nebo
také	také	k9	také
virgjineshi	virgjineshi	k6eAd1	virgjineshi
<g/>
.	.	kIx.	.
</s>
<s>
Burnesha	Burnesha	k1gFnSc1	Burnesha
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
přibližně	přibližně	k6eAd1	přibližně
"	"	kIx"	"
<g/>
panna	panna	k1gFnSc1	panna
z	z	k7c2	z
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
své	svůj	k3xOyFgFnSc2	svůj
či	či	k8xC	či
častěji	často	k6eAd2	často
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
rodiny	rodina	k1gFnSc2	rodina
přijímá	přijímat	k5eAaImIp3nS	přijímat
doživotně	doživotně	k6eAd1	doživotně
mužskou	mužský	k2eAgFnSc4d1	mužská
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Složí	složit	k5eAaPmIp3nS	složit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
prožije	prožít	k5eAaPmIp3nS	prožít
život	život	k1gInSc4	život
v	v	k7c6	v
celibátu	celibát	k1gInSc6	celibát
a	a	k8xC	a
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
povinnosti	povinnost	k1gFnPc1	povinnost
<g/>
,	,	kIx,	,
práva	právo	k1gNnPc1	právo
<g/>
)	)	kIx)	)
–	–	k?	–
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
všeho	všecek	k3xTgInSc2	všecek
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
msta	msta	k1gFnSc1	msta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
gjakmarrja	gjakmarrja	k1gFnSc1	gjakmarrja
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Albánie	Albánie	k1gFnSc2	Albánie
stále	stále	k6eAd1	stále
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
gjakmarrja	gjakmarrja	k1gFnSc1	gjakmarrja
týkala	týkat	k5eAaImAgFnS	týkat
přibližně	přibližně	k6eAd1	přibližně
3000	[number]	k4	3000
albánských	albánský	k2eAgFnPc2d1	albánská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
sportů	sport	k1gInPc2	sport
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
je	být	k5eAaImIp3nS	být
vzpírání	vzpírání	k1gNnSc4	vzpírání
<g/>
.	.	kIx.	.
</s>
<s>
Albánští	albánský	k2eAgMnPc1d1	albánský
vzpěrači	vzpěrač	k1gMnPc1	vzpěrač
získali	získat	k5eAaPmAgMnP	získat
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dvě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
byla	být	k5eAaImAgFnS	být
zlaté	zlatá	k1gFnSc2	zlatá
<g/>
,	,	kIx,	,
7	[number]	k4	7
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
a	a	k8xC	a
9	[number]	k4	9
bronzových	bronzový	k2eAgFnPc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
Erkand	Erkando	k1gNnPc2	Erkando
Qerimaj	Qerimaj	k1gInSc4	Qerimaj
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
Briken	Brikno	k1gNnPc2	Brikno
Calja	Calja	k1gFnSc1	Calja
<g/>
.	.	kIx.	.
<g/>
Izmir	Izmir	k1gMnSc1	Izmir
Smajlaj	Smajlaj	k1gMnSc1	Smajlaj
je	být	k5eAaImIp3nS	být
halovým	halový	k2eAgMnSc7d1	halový
mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
dalekém	daleký	k2eAgInSc6d1	daleký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Běžkyně	běžkyně	k1gFnPc1	běžkyně
na	na	k7c4	na
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
Luiza	Luiz	k1gMnSc2	Luiz
Gega	Gegus	k1gMnSc2	Gegus
má	mít	k5eAaImIp3nS	mít
stříbro	stříbro	k1gNnSc4	stříbro
z	z	k7c2	z
atletického	atletický	k2eAgNnSc2d1	atletické
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Albánská	albánský	k2eAgFnSc1d1	albánská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
probojovala	probojovat	k5eAaPmAgFnS	probojovat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
zde	zde	k6eAd1	zde
i	i	k9	i
svého	své	k1gNnSc2	své
prvního	první	k4xOgNnSc2	první
vítězství	vítězství	k1gNnSc2	vítězství
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
albánské	albánský	k2eAgInPc4d1	albánský
vztahy	vztah	k1gInPc4	vztah
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
březen	březen	k1gInSc1	březen
1925	[number]	k4	1925
–	–	k?	–
V	v	k7c6	v
Tiraně	Tiran	k1gInSc6	Tiran
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
Československý	československý	k2eAgInSc1d1	československý
konzulát	konzulát	k1gInSc1	konzulát
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
červen	červen	k1gInSc1	červen
1927	[number]	k4	1927
–	–	k?	–
Československý	československý	k2eAgInSc1d1	československý
konzulát	konzulát	k1gInSc1	konzulát
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
generální	generální	k2eAgInSc4d1	generální
konzulát	konzulát	k1gInSc4	konzulát
a	a	k8xC	a
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
agencii	agencie	k1gFnSc4	agencie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
červen	červen	k1gInSc1	červen
1929	[number]	k4	1929
–	–	k?	–
Vzniká	vznikat	k5eAaImIp3nS	vznikat
Československé	československý	k2eAgNnSc1d1	Československé
vyslanectví	vyslanectví	k1gNnSc1	vyslanectví
v	v	k7c6	v
Tiraně	Tirana	k1gFnSc6	Tirana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
–	–	k?	–
Obrat	obrat	k1gInSc1	obrat
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
dodávek	dodávka	k1gFnPc2	dodávka
činil	činit	k5eAaImAgInS	činit
13	[number]	k4	13
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
z	z	k7c2	z
toho	ten	k3xDgMnSc4	ten
vývoz	vývoz	k1gInSc4	vývoz
do	do	k7c2	do
Albánie	Albánie	k1gFnSc2	Albánie
9,5	[number]	k4	9,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
dovoz	dovoz	k1gInSc4	dovoz
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
3,5	[number]	k4	3,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
–	–	k?	–
Byla	být	k5eAaImAgFnS	být
zastavena	zastaven	k2eAgFnSc1d1	zastavena
činnost	činnost	k1gFnSc1	činnost
Československého	československý	k2eAgNnSc2d1	Československé
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
listopad	listopad	k1gInSc1	listopad
1947	[number]	k4	1947
–	–	k?	–
Výměna	výměna	k1gFnSc1	výměna
vyslanců	vyslanec	k1gMnPc2	vyslanec
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
červen	červen	k1gInSc1	červen
1949	[number]	k4	1949
–	–	k?	–
Bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
Československé	československý	k2eAgNnSc1d1	Československé
vyslanectví	vyslanectví	k1gNnSc1	vyslanectví
v	v	k7c6	v
Tiraně	Tirana	k1gFnSc6	Tirana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
květen	květen	k1gInSc1	květen
1949	[number]	k4	1949
–	–	k?	–
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
vyslanectví	vyslanectví	k1gNnSc1	vyslanectví
bylo	být	k5eAaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c6	na
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1956-1961	[number]	k4	1956-1961
–	–	k?	–
Československo	Československo	k1gNnSc1	Československo
dodalo	dodat	k5eAaPmAgNnS	dodat
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
technickou	technický	k2eAgFnSc4d1	technická
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
kapacit	kapacita	k1gFnPc2	kapacita
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
železnonikelnatých	železnonikelnatý	k2eAgFnPc2d1	železnonikelnatý
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
listopad	listopad	k1gInSc1	listopad
1961	[number]	k4	1961
–	–	k?	–
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odvolání	odvolání	k1gNnSc3	odvolání
československého	československý	k2eAgMnSc2d1	československý
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
–	–	k?	–
Albánie	Albánie	k1gFnSc1	Albánie
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
ČSSR	ČSSR	kA	ČSSR
z	z	k7c2	z
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
12	[number]	k4	12
%	%	kIx~	%
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
obratu	obrat	k1gInSc6	obrat
albánského	albánský	k2eAgInSc2d1	albánský
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
jako	jako	k8xS	jako
její	její	k3xOp3gInSc4	její
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgMnSc1d3	veliký
obchodní	obchodní	k2eAgMnSc1d1	obchodní
partner	partner	k1gMnSc1	partner
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
obrat	obrat	k1gInSc1	obrat
činil	činit	k5eAaImAgInS	činit
262,1	[number]	k4	262,1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
z	z	k7c2	z
toho	ten	k3xDgMnSc4	ten
vývoz	vývoz	k1gInSc4	vývoz
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
121,1	[number]	k4	121,1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
dovoz	dovoz	k1gInSc4	dovoz
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
pak	pak	k6eAd1	pak
141	[number]	k4	141
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Albania	Albanium	k1gNnSc2	Albanium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HLADKÝ	Hladký	k1gMnSc1	Hladký
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
HRADEČNÝ	HRADEČNÝ	kA	HRADEČNÝ
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
939	[number]	k4	939
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Albánská	albánský	k2eAgFnSc1d1	albánská
armáda	armáda	k1gFnSc1	armáda
</s>
</p>
<p>
<s>
Skanderbeg	Skanderbeg	k1gMnSc1	Skanderbeg
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Albánie	Albánie	k1gFnSc2	Albánie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Albánie	Albánie	k1gFnSc2	Albánie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Albanie	Albanie	k1gFnSc2	Albanie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Albánie	Albánie	k1gFnSc2	Albánie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Albánie	Albánie	k1gFnSc2	Albánie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Albánie	Albánie	k1gFnSc1	Albánie
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Cestopis	cestopis	k1gInSc1	cestopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Albania	Albanium	k1gNnSc2	Albanium
–	–	k?	–
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Albania	Albanium	k1gNnPc1	Albanium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
of	of	k?	of
Origin	Origin	k2eAgInSc1d1	Origin
Information	Information	k1gInSc1	Information
Report	report	k1gInSc1	report
–	–	k?	–
Albania	Albanium	k1gNnSc2	Albanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
Border	Border	k1gMnSc1	Border
Agency	Agenca	k1gMnSc2	Agenca
<g/>
,	,	kIx,	,
2011-03-25	[number]	k4	2011-03-25
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Albania	Albanium	k1gNnSc2	Albanium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftun	Stiftun	k1gMnSc1	Stiftun
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Albania	Albanium	k1gNnSc2	Albanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-01-04	[number]	k4	2011-01-04
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
–	–	k?	–
Albania	Albanium	k1gNnSc2	Albanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Tiraně	Tirana	k1gFnSc6	Tirana
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Albánie	Albánie	k1gFnSc1	Albánie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-04-01	[number]	k4	2011-04-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BIBERAJ	BIBERAJ	kA	BIBERAJ
<g/>
,	,	kIx,	,
Elez	Elez	k1gMnSc1	Elez
<g/>
;	;	kIx,	;
PRIFTI	PRIFTI	kA	PRIFTI
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
R	R	kA	R
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Albania	Albanium	k1gNnPc1	Albanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
