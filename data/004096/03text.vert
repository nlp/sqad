<s>
Remoska	remoska	k1gFnSc1	remoska
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
elektrická	elektrický	k2eAgFnSc1d1	elektrická
přenosná	přenosný	k2eAgFnSc1d1	přenosná
trouba	trouba	k1gFnSc1	trouba
s	s	k7c7	s
ohřevem	ohřev	k1gInSc7	ohřev
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
víko	víko	k1gNnSc1	víko
se	s	k7c7	s
zabudovaným	zabudovaný	k2eAgInSc7d1	zabudovaný
elektrickým	elektrický	k2eAgInSc7d1	elektrický
odporovým	odporový	k2eAgInSc7d1	odporový
topným	topný	k2eAgInSc7d1	topný
drátem	drát	k1gInSc7	drát
<g/>
,	,	kIx,	,
se	s	k7c7	s
zaskleným	zasklený	k2eAgNnSc7d1	zasklené
kruhovým	kruhový	k2eAgNnSc7d1	kruhové
okénkem	okénko	k1gNnSc7	okénko
a	a	k8xC	a
otvory	otvor	k1gInPc7	otvor
pro	pro	k7c4	pro
únik	únik	k1gInSc4	únik
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
víko	víko	k1gNnSc1	víko
bylo	být	k5eAaImAgNnS	být
dodáváno	dodávat	k5eAaImNgNnS	dodávat
s	s	k7c7	s
aluminiovými	aluminiový	k2eAgInPc7d1	aluminiový
hrnci	hrnec	k1gInPc7	hrnec
různé	různý	k2eAgFnSc2d1	různá
hloubky	hloubka	k1gFnSc2	hloubka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
rozměrů	rozměr	k1gInPc2	rozměr
pečené	pečený	k2eAgFnSc2d1	pečená
potraviny	potravina	k1gFnSc2	potravina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
z	z	k7c2	z
víka	víko	k1gNnSc2	víko
sálá	sálat	k5eAaImIp3nS	sálat
shora	shora	k6eAd1	shora
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
též	též	k9	též
rozváděno	rozváděn	k2eAgNnSc1d1	rozváděno
po	po	k7c6	po
stěnách	stěna	k1gFnPc6	stěna
hrnce	hrnec	k1gInSc2	hrnec
(	(	kIx(	(
<g/>
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
připalování	připalování	k1gNnSc3	připalování
odspodu	odspodu	k6eAd1	odspodu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
Remoska	remoska	k1gFnSc1	remoska
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
regulace	regulace	k1gFnSc2	regulace
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
bez	bez	k7c2	bez
vnější	vnější	k2eAgFnSc2d1	vnější
tepelné	tepelný	k2eAgFnSc2d1	tepelná
izolace	izolace	k1gFnSc2	izolace
hrnce	hrnec	k1gInSc2	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Vaření-pečení	Vařeníečení	k1gNnSc1	Vaření-pečení
v	v	k7c6	v
Remosce	remoska	k1gFnSc6	remoska
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadné	snadný	k2eAgNnSc1d1	snadné
a	a	k8xC	a
jídla	jídlo	k1gNnPc1	jídlo
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
připravená	připravený	k2eAgFnSc1d1	připravená
neztrácejí	ztrácet	k5eNaImIp3nP	ztrácet
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc4	maso
nevysychá	vysychat	k5eNaImIp3nS	vysychat
a	a	k8xC	a
např.	např.	kA	např.
kuře	kuře	k1gNnSc1	kuře
pečené	pečený	k2eAgNnSc1d1	pečené
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
troubě	trouba	k1gFnSc6	trouba
nesnese	snést	k5eNaPmIp3nS	snést
srovnání	srovnání	k1gNnSc1	srovnání
s	s	k7c7	s
kuřetem	kuře	k1gNnSc7	kuře
připraveným	připravený	k2eAgFnPc3d1	připravená
v	v	k7c6	v
Remosce	remoska	k1gFnSc6	remoska
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dnešní	dnešní	k2eAgNnSc4d1	dnešní
nebo	nebo	k8xC	nebo
tehdejší	tehdejší	k2eAgNnSc4d1	tehdejší
provedení	provedení	k1gNnSc4	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
Remoska	remoska	k1gFnSc1	remoska
bez	bez	k7c2	bez
skleněného	skleněný	k2eAgNnSc2d1	skleněné
okénka	okénko	k1gNnSc2	okénko
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
<g/>
,	,	kIx,	,
okénkem	okénko	k1gNnSc7	okénko
totiž	totiž	k8xC	totiž
unikala	unikat	k5eAaImAgFnS	unikat
část	část	k1gFnSc4	část
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
střed	střed	k1gInSc1	střed
pečené	pečený	k2eAgFnSc2d1	pečená
potraviny	potravina	k1gFnSc2	potravina
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
snáze	snadno	k6eAd2	snadno
nedopekl	dopéct	k5eNaPmAgMnS	dopéct
<g/>
.	.	kIx.	.
</s>
<s>
Remosku	remoska	k1gFnSc4	remoska
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
počátkem	počátkem	k7c2	počátkem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
Oldřich	Oldřich	k1gMnSc1	Oldřich
Homuta	Homut	k1gMnSc2	Homut
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prototyp	prototyp	k1gInSc1	prototyp
dále	daleko	k6eAd2	daleko
vylepšoval	vylepšovat	k5eAaImAgInS	vylepšovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
remoska	remoska	k1gFnSc1	remoska
tak	tak	k6eAd1	tak
nesla	nést	k5eAaImAgFnS	nést
označení	označení	k1gNnSc4	označení
HUT	HUT	kA	HUT
(	(	kIx(	(
<g/>
Homuta	Homut	k1gMnSc2	Homut
<g/>
,	,	kIx,	,
Uher	uher	k1gInSc1	uher
<g/>
,	,	kIx,	,
Tyburec	Tyburec	k1gInSc1	Tyburec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Homutova	Homutův	k2eAgFnSc1d1	Homutův
dílna	dílna	k1gFnSc1	dílna
byla	být	k5eAaImAgFnS	být
znárodněna	znárodnit	k5eAaPmNgFnS	znárodnit
a	a	k8xC	a
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
podniku	podnik	k1gInSc3	podnik
místního	místní	k2eAgNnSc2d1	místní
hospodářství	hospodářství	k1gNnSc2	hospodářství
REMOS	REMOS	kA	REMOS
(	(	kIx(	(
<g/>
=	=	kIx~	=
revize	revize	k1gFnSc1	revize
<g/>
,	,	kIx,	,
elektro	elektro	k1gNnSc1	elektro
<g/>
,	,	kIx,	,
montáže	montáž	k1gFnPc1	montáž
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc1	oprava
<g/>
,	,	kIx,	,
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podnik	podnik	k1gInSc1	podnik
REMOS	REMOS	kA	REMOS
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
například	například	k6eAd1	například
i	i	k9	i
držáky	držák	k1gInPc1	držák
přístrojových	přístrojový	k2eAgFnPc2d1	přístrojová
pojistek	pojistka	k1gFnPc2	pojistka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
Remosky	remoska	k1gFnPc1	remoska
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
sériově	sériově	k6eAd1	sériově
vyrábět	vyrábět	k5eAaImF	vyrábět
ve	v	k7c6	v
Zdicích	Zdice	k1gFnPc6	Zdice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zdejší	zdejší	k2eAgInSc1d1	zdejší
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgInS	specializovat
na	na	k7c4	na
pečicí	pečicí	k2eAgFnPc4d1	pečicí
trouby	trouba	k1gFnPc4	trouba
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
výrobky	výrobek	k1gInPc1	výrobek
nesly	nést	k5eAaImAgInP	nést
značku	značka	k1gFnSc4	značka
ESČ	ESČ	kA	ESČ
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
Lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
všech	všecek	k3xTgFnPc2	všecek
velikostí	velikost	k1gFnPc2	velikost
Remosky	remoska	k1gFnSc2	remoska
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
2,7	[number]	k4	2,7
miliónu	milión	k4xCgInSc2	milión
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
Remosky	remoska	k1gFnSc2	remoska
dvojice	dvojice	k1gFnSc1	dvojice
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
Uličník	uličník	k1gMnSc1	uličník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
také	také	k9	také
nový	nový	k2eAgInSc4d1	nový
výrobní	výrobní	k2eAgInSc4d1	výrobní
závod	závod	k1gInSc4	závod
Remoska	remoska	k1gFnSc1	remoska
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
ve	v	k7c6	v
Frenštátě	Frenštát	k1gInSc6	Frenštát
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
zakoupena	zakoupen	k2eAgFnSc1d1	zakoupena
licence	licence	k1gFnSc1	licence
DuPont	DuPonta	k1gFnPc2	DuPonta
na	na	k7c4	na
postřik	postřik	k1gInSc4	postřik
teflonovou	teflonový	k2eAgFnSc7d1	teflonová
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
začal	začít	k5eAaPmAgInS	začít
export	export	k1gInSc1	export
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
desítky	desítka	k1gFnSc2	desítka
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
50	[number]	k4	50
<g/>
%	%	kIx~	%
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
exportuje	exportovat	k5eAaBmIp3nS	exportovat
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
znovuzrozené	znovuzrozený	k2eAgFnSc2d1	znovuzrozená
Remosky	remoska	k1gFnSc2	remoska
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
cca	cca	kA	cca
Kč	Kč	kA	Kč
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
Kčs	Kčs	kA	Kčs
180	[number]	k4	180
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
u	u	k7c2	u
menšího	malý	k2eAgNnSc2d2	menší
a	a	k8xC	a
210	[number]	k4	210
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
u	u	k7c2	u
většího	veliký	k2eAgNnSc2d2	veliký
provedení	provedení	k1gNnSc2	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
remoska	remoska	k1gFnSc1	remoska
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Remoska	remoska	k1gFnSc1	remoska
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Web	web	k1gInSc4	web
Remoska	remoska	k1gFnSc1	remoska
Web	web	k1gInSc1	web
Kde	kde	k6eAd1	kde
bydlet	bydlet	k5eAaImF	bydlet
</s>
