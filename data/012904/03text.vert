<p>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Medveď	Medveď	k1gMnSc1	Medveď
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Bacúch	Bacúch	k1gInSc1	Bacúch
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
SMER	SMER	kA	SMER
-	-	kIx~	-
sociálna	sociálno	k1gNnSc2	sociálno
demokracia	demokracium	k1gNnSc2	demokracium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
odboru	odbor	k1gInSc3	odbor
finance	finance	k1gFnSc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
studijní	studijní	k2eAgInSc1d1	studijní
a	a	k8xC	a
pracovní	pracovní	k2eAgInSc1d1	pracovní
pobyt	pobyt	k1gInSc1	pobyt
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
proděkana	proděkan	k1gMnSc2	proděkan
Fakulty	fakulta	k1gFnSc2	fakulta
ekonomiky	ekonomika	k1gFnSc2	ekonomika
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
Vysoké	vysoký	k2eAgFnSc3d1	vysoká
škole	škola	k1gFnSc3	škola
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
děkanem	děkan	k1gMnSc7	děkan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
"	"	kIx"	"
<g/>
Carpatia	Carpatia	k1gFnSc1	Carpatia
Consulta	Consult	k1gInSc2	Consult
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prorektorem	prorektor	k1gMnSc7	prorektor
na	na	k7c6	na
banskobystrické	banskobystrický	k2eAgFnSc6d1	Banskobystrická
pobočce	pobočka	k1gFnSc6	pobočka
české	český	k2eAgFnSc2d1	Česká
soukromé	soukromý	k2eAgFnSc2d1	soukromá
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
Bankovní	bankovní	k2eAgInSc1d1	bankovní
institut	institut	k1gInSc1	institut
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
působil	působit	k5eAaImAgMnS	působit
též	též	k9	též
jako	jako	k9	jako
děkan	děkan	k1gMnSc1	děkan
Fakulty	fakulta	k1gFnSc2	fakulta
financí	finance	k1gFnPc2	finance
Univerzity	univerzita	k1gFnSc2	univerzita
Mateje	Matej	k1gInSc2	Matej
Bela	Bela	k1gFnSc1	Bela
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
resortu	resort	k1gInSc2	resort
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
odvolal	odvolat	k5eAaPmAgMnS	odvolat
nominanta	nominant	k1gMnSc4	nominant
za	za	k7c4	za
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
národní	národní	k2eAgFnSc4d1	národní
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
toto	tento	k3xDgNnSc1	tento
křesla	křeslo	k1gNnPc4	křeslo
náleželo	náležet	k5eAaImAgNnS	náležet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
koaliční	koaliční	k2eAgFnSc2d1	koaliční
smlouvy	smlouva	k1gFnSc2	smlouva
právě	právě	k9	právě
SNS	SNS	kA	SNS
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Medvedě	Medveda	k1gFnSc3	Medveda
jakožto	jakožto	k8xS	jakožto
nominanta	nominanta	k1gFnSc1	nominanta
strany	strana	k1gFnSc2	strana
SMER	SMER	kA	SMER
-	-	kIx~	-
sociálna	sociálno	k1gNnSc2	sociálno
demokracia	demokracium	k1gNnSc2	demokracium
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
celého	celý	k2eAgNnSc2d1	celé
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
agendu	agenda	k1gFnSc4	agenda
tak	tak	k9	tak
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
převzalo	převzít	k5eAaPmAgNnS	převzít
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
regionálního	regionální	k2eAgInSc2d1	regionální
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
aktuality	aktualita	k1gFnSc2	aktualita
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
osobní	osobní	k2eAgFnPc4d1	osobní
stránky	stránka	k1gFnPc4	stránka
Jozefa	Jozef	k1gMnSc2	Jozef
Medvedě	Medved	k1gInSc6	Medved
</s>
</p>
