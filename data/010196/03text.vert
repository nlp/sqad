<p>
<s>
John	John	k1gMnSc1	John
George	Georg	k1gMnSc2	Georg
Kemeny	Kemena	k1gFnSc2	Kemena
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
jménem	jméno	k1gNnSc7	jméno
Kemény	Keména	k1gFnSc2	Keména
János	János	k1gInSc1	János
György	Györg	k1gInPc1	Györg
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
maďarsko-americký	maďarskomerický	k2eAgMnSc1d1	maďarsko-americký
matematik	matematik	k1gMnSc1	matematik
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
spoluautor	spoluautor	k1gMnSc1	spoluautor
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
BASIC	Basic	kA	Basic
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
americké	americký	k2eAgFnSc2d1	americká
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
János	János	k1gMnSc1	János
Kemény	Keména	k1gFnSc2	Keména
utekl	utéct	k5eAaPmAgMnS	utéct
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
kvůli	kvůli	k7c3	kvůli
sílícímu	sílící	k2eAgNnSc3d1	sílící
postavení	postavení	k1gNnSc3	postavení
fašistického	fašistický	k2eAgNnSc2d1	fašistické
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
evropě	evropa	k1gFnSc6	evropa
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
ukončil	ukončit	k5eAaPmAgInS	ukončit
už	už	k6eAd1	už
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
George	Georg	k1gInSc2	Georg
Washington	Washington	k1gInSc1	Washington
High	High	k1gInSc1	High
School	School	k1gInSc1	School
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Princetonské	Princetonský	k2eAgFnSc6d1	Princetonská
univerzitě	univerzita	k1gFnSc6	univerzita
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc4	studium
musel	muset	k5eAaImAgInS	muset
kvůli	kvůli	k7c3	kvůli
druhé	druhý	k4xOgFnSc3	druhý
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
na	na	k7c4	na
rok	rok	k1gInSc4	rok
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vývoji	vývoj	k1gInSc3	vývoj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
poznal	poznat	k5eAaPmAgMnS	poznat
Richarda	Richard	k1gMnSc4	Richard
Feynmana	Feynman	k1gMnSc4	Feynman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnPc3	jeho
nadřízeným	nadřízený	k1gMnPc3	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
dojem	dojem	k1gInSc1	dojem
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
udělal	udělat	k5eAaPmAgMnS	udělat
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Princeton	Princeton	k1gInSc4	Princeton
ukončil	ukončit	k5eAaPmAgMnS	ukončit
studium	studium	k1gNnSc1	studium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
získal	získat	k5eAaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Doktorandské	doktorandský	k2eAgNnSc4d1	doktorandské
studium	studium	k1gNnSc4	studium
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Alonzo	Alonza	k1gFnSc5	Alonza
Churche	Churche	k1gNnSc3	Churche
<g/>
,	,	kIx,	,
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k6eAd1	také
matematickým	matematický	k2eAgMnSc7d1	matematický
asistentem	asistent	k1gMnSc7	asistent
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
27	[number]	k4	27
<g/>
letý	letý	k2eAgInSc1d1	letý
přijal	přijmout	k5eAaPmAgInS	přijmout
úkol	úkol	k1gInSc1	úkol
vytvořit	vytvořit	k5eAaPmF	vytvořit
katedru	katedra	k1gFnSc4	katedra
matematiky	matematika	k1gFnSc2	matematika
na	na	k7c6	na
Darthmouthské	Darthmouthský	k2eAgFnSc6d1	Darthmouthský
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
až	až	k9	až
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
jí	on	k3xPp3gFnSc3	on
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
ho	on	k3xPp3gNnSc4	on
prezident	prezident	k1gMnSc1	prezident
Jimmy	Jimma	k1gFnSc2	Jimma
Carter	Carter	k1gMnSc1	Carter
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
havárie	havárie	k1gFnSc2	havárie
atomové	atomový	k2eAgFnSc2d1	atomová
elektrárny	elektrárna	k1gFnSc2	elektrárna
na	na	k7c6	na
Three	Three	k1gFnSc6	Three
Miles	Milesa	k1gFnPc2	Milesa
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
roztavil	roztavit	k5eAaPmAgInS	roztavit
atomový	atomový	k2eAgInSc1d1	atomový
reaktor	reaktor	k1gInSc1	reaktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Programovací	programovací	k2eAgInSc1d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
BASIC	Basic	kA	Basic
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
Eugenem	Eugen	k1gMnSc7	Eugen
Kurtzem	Kurtz	k1gMnSc7	Kurtz
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
i	i	k9	i
Dartmouth	Dartmouth	k1gMnSc1	Dartmouth
Time-Sharing	Time-Sharing	k1gInSc1	Time-Sharing
System	Syst	k1gMnSc7	Syst
(	(	kIx(	(
<g/>
DTSS	DTSS	kA	DTSS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
více	hodně	k6eAd2	hodně
lidem	člověk	k1gMnPc3	člověk
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
John	John	k1gMnSc1	John
George	Georg	k1gMnSc2	Georg
Kemeny	Kemena	k1gFnSc2	Kemena
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Britannica	Britannic	k1gInSc2	Britannic
</s>
</p>
<p>
<s>
Portrét	portrét	k1gInSc1	portrét
v	v	k7c6	v
The	The	k1gFnSc6	The
MacTutor	MacTutor	k1gInSc1	MacTutor
History	Histor	k1gInPc1	Histor
of	of	k?	of
Mathematics	Mathematics	k1gInSc1	Mathematics
archive	archiv	k1gInSc5	archiv
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgInPc1d1	encyklopedický
zdroje	zdroj	k1gInPc1	zdroj
na	na	k7c4	na
Answers	Answers	k1gInSc4	Answers
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Nekrolog	nekrolog	k1gInSc1	nekrolog
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
</s>
</p>
