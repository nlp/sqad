<s>
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
NAFTA	nafta	k1gFnSc1	nafta
z	z	k7c2	z
angl.	angl.	k?	angl.
North	Northa	k1gFnPc2	Northa
American	Americany	k1gInPc2	Americany
Free	Fre	k1gFnSc2	Fre
Trade	Trad	k1gInSc5	Trad
Agreement	Agreement	k1gInSc1	Agreement
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgFnSc1d1	obchodní
dohoda	dohoda	k1gFnSc1	dohoda
spojující	spojující	k2eAgFnSc4d1	spojující
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
omezit	omezit	k5eAaPmF	omezit
obchodní	obchodní	k2eAgFnPc4d1	obchodní
a	a	k8xC	a
celní	celní	k2eAgFnPc4d1	celní
bariéry	bariéra	k1gFnPc4	bariéra
a	a	k8xC	a
liberalizovat	liberalizovat	k5eAaImF	liberalizovat
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
