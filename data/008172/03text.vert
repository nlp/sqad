<p>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
Gould	Gould	k1gMnSc1	Gould
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
jako	jako	k8xC	jako
Gold	Gold	k1gInSc1	Gold
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
světových	světový	k2eAgInPc2d1	světový
klavírních	klavírní	k2eAgInPc2d1	klavírní
virtuózů	virtuóz	k1gInPc2	virtuóz
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
Herbert	Herbert	k1gMnSc1	Herbert
Gould	Gould	k1gMnSc1	Gould
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
otci	otec	k1gMnSc3	otec
Herberu	Herber	k1gInSc2	Herber
Russelovi	Russel	k1gMnSc3	Russel
Goldovi	Golda	k1gMnSc3	Golda
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Florence	Florenc	k1gFnSc2	Florenc
Emmě	Emma	k1gFnSc3	Emma
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgNnSc1d1	rozené
Griegové	Griegový	k2eAgNnSc1d1	Griegový
<g/>
,	,	kIx,	,
presbyteriánům	presbyterián	k1gMnPc3	presbyterián
se	se	k3xPyFc4	se
skotskými	skotský	k1gInPc7	skotský
a	a	k8xC	a
britskými	britský	k2eAgInPc7d1	britský
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Matčin	matčin	k2eAgMnSc1d1	matčin
dědeček	dědeček	k1gMnSc1	dědeček
byl	být	k5eAaImAgMnS	být
bratrancem	bratranec	k1gMnSc7	bratranec
norského	norský	k2eAgMnSc2d1	norský
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Edvarda	Edvard	k1gMnSc2	Edvard
Griega	Grieg	k1gMnSc2	Grieg
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
i	i	k8xC	i
matka	matka	k1gFnSc1	matka
byli	být	k5eAaImAgMnP	být
muzikální	muzikální	k2eAgMnPc1d1	muzikální
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
mít	mít	k5eAaImF	mít
ze	z	k7c2	z
syna	syn	k1gMnSc2	syn
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ho	on	k3xPp3gInSc4	on
učila	učít	k5eAaPmAgFnS	učít
zpívat	zpívat	k5eAaImF	zpívat
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
od	od	k7c2	od
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pečovala	pečovat	k5eAaImAgFnS	pečovat
o	o	k7c4	o
obratnost	obratnost	k1gFnSc4	obratnost
jeho	jeho	k3xOp3gInPc2	jeho
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
mohl	moct	k5eAaImAgInS	moct
stát	stát	k1gInSc1	stát
buď	buď	k8xC	buď
chirurg	chirurg	k1gMnSc1	chirurg
nebo	nebo	k8xC	nebo
pianista	pianista	k1gMnSc1	pianista
<g/>
.	.	kIx.	.
</s>
<s>
Gould	Gould	k1gMnSc1	Gould
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
první	první	k4xOgInSc4	první
veřejný	veřejný	k2eAgInSc4d1	veřejný
koncert	koncert	k1gInSc4	koncert
a	a	k8xC	a
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
přijat	přijat	k2eAgMnSc1d1	přijat
na	na	k7c4	na
Kanadskou	kanadský	k2eAgFnSc4d1	kanadská
královskou	královský	k2eAgFnSc4d1	královská
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
sílícího	sílící	k2eAgInSc2d1	sílící
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
vsunutím	vsunutí	k1gNnSc7	vsunutí
samohlásky	samohláska	k1gFnSc2	samohláska
"	"	kIx"	"
<g/>
u	u	k7c2	u
<g/>
"	"	kIx"	"
na	na	k7c4	na
Gould	Gould	k1gInSc4	Gould
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
Gould	Gould	k1gMnSc1	Gould
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgMnS	složit
zkoušky	zkouška	k1gFnPc4	zkouška
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgMnS	ukončit
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
Gould	Goulda	k1gFnPc2	Goulda
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
navázal	navázat	k5eAaPmAgInS	navázat
milostný	milostný	k2eAgInSc1d1	milostný
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
Cornelií	Cornelie	k1gFnSc7	Cornelie
Foss	Fossa	k1gFnPc2	Fossa
<g/>
,	,	kIx,	,
výtvarnicí	výtvarnice	k1gFnSc7	výtvarnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kvůli	kvůli	k7c3	kvůli
Gouldovi	Gould	k1gMnSc3	Gould
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
opustila	opustit	k5eAaPmAgFnS	opustit
manžela	manžel	k1gMnSc4	manžel
a	a	k8xC	a
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
syny	syn	k1gMnPc7	syn
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Toronta	Toronto	k1gNnSc2	Toronto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
koupila	koupit	k5eAaPmAgFnS	koupit
dům	dům	k1gInSc4	dům
blízko	blízko	k7c2	blízko
jeho	jeho	k3xOp3gInPc2	jeho
<g/>
;	;	kIx,	;
vztah	vztah	k1gInSc1	vztah
skončil	skončit	k5eAaPmAgInS	skončit
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
veřejně	veřejně	k6eAd1	veřejně
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
13	[number]	k4	13
do	do	k7c2	do
31	[number]	k4	31
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
skladeb	skladba	k1gFnPc2	skladba
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc6	psaní
a	a	k8xC	a
na	na	k7c4	na
rozhlasové	rozhlasový	k2eAgInPc4d1	rozhlasový
pořady	pořad	k1gInPc4	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hudebně	hudebně	k6eAd1	hudebně
kritických	kritický	k2eAgFnPc2d1	kritická
esejí	esej	k1gFnPc2	esej
psal	psát	k5eAaImAgInS	psát
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
žánry	žánr	k1gInPc4	žánr
<g/>
,	,	kIx,	,
psaním	psaní	k1gNnSc7	psaní
byl	být	k5eAaImAgInS	být
posedlý	posedlý	k2eAgInSc1d1	posedlý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
nepostrádají	postrádat	k5eNaImIp3nP	postrádat
filozofický	filozofický	k2eAgInSc4d1	filozofický
nadhled	nadhled	k1gInSc4	nadhled
<g/>
,	,	kIx,	,
humor	humor	k1gInSc4	humor
a	a	k8xC	a
břitkou	břitký	k2eAgFnSc4d1	břitká
ironii	ironie	k1gFnSc4	ironie
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritika	k1gFnPc4	kritika
psal	psát	k5eAaImAgMnS	psát
pod	pod	k7c7	pod
sedmi	sedm	k4xCc7	sedm
různými	různý	k2eAgInPc7d1	různý
pseudonymy	pseudonym	k1gInPc7	pseudonym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trpěl	trpět	k5eAaImAgMnS	trpět
mnoha	mnoho	k4c7	mnoho
bolestmi	bolest	k1gFnPc7	bolest
a	a	k8xC	a
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
považován	považován	k2eAgInSc1d1	považován
okolím	okolí	k1gNnSc7	okolí
za	za	k7c4	za
neurotika	neurotik	k1gMnSc4	neurotik
a	a	k8xC	a
hypochondra	hypochondr	k1gMnSc4	hypochondr
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
prášky	prášek	k1gInPc4	prášek
a	a	k8xC	a
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
si	se	k3xPyFc3	se
zejména	zejména	k9	zejména
na	na	k7c4	na
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
páteře	páteř	k1gFnSc2	páteř
(	(	kIx(	(
<g/>
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
utrpěl	utrpět	k5eAaPmAgInS	utrpět
úraz	úraz	k1gInSc1	úraz
páteře	páteř	k1gFnSc2	páteř
pádem	pád	k1gInSc7	pád
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
lodi	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
padesátinách	padesátina	k1gFnPc6	padesátina
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
polovinu	polovina	k1gFnSc4	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
další	další	k2eAgFnSc2d1	další
mozkové	mozkový	k2eAgFnSc2d1	mozková
příhody	příhoda	k1gFnSc2	příhoda
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
kariéry	kariéra	k1gFnSc2	kariéra
měl	mít	k5eAaImAgInS	mít
Gould	Gould	k1gInSc1	Gould
velmi	velmi	k6eAd1	velmi
široký	široký	k2eAgInSc4d1	široký
repertoár	repertoár	k1gInSc4	repertoár
od	od	k7c2	od
předbarokních	předbarokní	k2eAgMnPc2d1	předbarokní
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Jana	Jana	k1gFnSc1	Jana
Sweelincka	Sweelincka	k1gFnSc1	Sweelincka
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
barokní	barokní	k2eAgFnPc4d1	barokní
klasiky	klasika	k1gFnPc4	klasika
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Bachů	Bach	k1gInPc2	Bach
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Beethovena	Beethoven	k1gMnSc4	Beethoven
<g/>
,	,	kIx,	,
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
,	,	kIx,	,
Haydna	Haydna	k1gFnSc1	Haydna
<g/>
,	,	kIx,	,
Brahmse	Brahms	k1gMnPc4	Brahms
<g/>
,	,	kIx,	,
romantiky	romantik	k1gMnPc4	romantik
Schumanna	Schumann	k1gMnSc2	Schumann
<g/>
,	,	kIx,	,
Liszta	Liszt	k1gMnSc2	Liszt
a	a	k8xC	a
Chopina	Chopin	k1gMnSc2	Chopin
až	až	k9	až
po	po	k7c4	po
avantgardu	avantgarda	k1gFnSc4	avantgarda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
Schoenberg	Schoenberg	k1gMnSc1	Schoenberg
nebo	nebo	k8xC	nebo
Alban	Alban	k1gMnSc1	Alban
Berg	Berg	k1gMnSc1	Berg
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
jako	jako	k9	jako
interpret	interpret	k1gMnSc1	interpret
náročných	náročný	k2eAgFnPc2d1	náročná
klávesových	klávesový	k2eAgFnPc2d1	klávesová
skladeb	skladba	k1gFnPc2	skladba
Johanna	Johanno	k1gNnSc2	Johanno
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bacha	Bacha	k?	Bacha
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
nahrávky	nahrávka	k1gFnPc4	nahrávka
Goldbergových	Goldbergův	k2eAgFnPc2d1	Goldbergova
variací	variace	k1gFnPc2	variace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
osobitá	osobitý	k2eAgFnSc1d1	osobitá
<g/>
,	,	kIx,	,
vyniká	vynikat	k5eAaImIp3nS	vynikat
technikou	technika	k1gFnSc7	technika
i	i	k8xC	i
mnoha	mnoho	k4c7	mnoho
zvláštnostmi	zvláštnost	k1gFnPc7	zvláštnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
netypického	typický	k2eNgNnSc2d1	netypické
držení	držení	k1gNnSc2	držení
těla	tělo	k1gNnSc2	tělo
u	u	k7c2	u
klavíru	klavír	k1gInSc2	klavír
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
stejná	stejný	k2eAgFnSc1d1	stejná
nízká	nízký	k2eAgFnSc1d1	nízká
židle	židle	k1gFnSc1	židle
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývající	vyplývající	k2eAgFnSc4d1	vyplývající
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
držení	držení	k1gNnSc4	držení
rukou	ruka	k1gFnPc2	ruka
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
střídmé	střídmý	k2eAgNnSc1d1	střídmé
používání	používání	k1gNnSc1	používání
pedálu	pedál	k1gInSc2	pedál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Bachových	Bachův	k2eAgFnPc2d1	Bachova
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
tak	tak	k6eAd1	tak
zvýrazňoval	zvýrazňovat	k5eAaImAgMnS	zvýrazňovat
jejich	jejich	k3xOp3gFnSc4	jejich
polyfonii	polyfonie	k1gFnSc4	polyfonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
veřejných	veřejný	k2eAgInPc6d1	veřejný
koncertech	koncert	k1gInPc6	koncert
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgInS	uctívat
a	a	k8xC	a
oslavován	oslavovat	k5eAaImNgInS	oslavovat
nejen	nejen	k6eAd1	nejen
diváky	divák	k1gMnPc4	divák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k9	taky
dirigenty	dirigent	k1gMnPc4	dirigent
<g/>
,	,	kIx,	,
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
Herbert	Herbert	k1gInSc1	Herbert
von	von	k1gInSc1	von
Karajan	Karajan	k1gInSc1	Karajan
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
nebo	nebo	k8xC	nebo
Yehudi	Yehud	k1gMnPc1	Yehud
Menuhin	Menuhina	k1gFnPc2	Menuhina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Severoameričan	Severoameričan	k1gMnSc1	Severoameričan
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
podnikl	podniknout	k5eAaPmAgMnS	podniknout
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Několika	několik	k4yIc7	několik
skladbami	skladba	k1gFnPc7	skladba
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
také	také	k9	také
mezi	mezi	k7c4	mezi
hudební	hudební	k2eAgMnPc4d1	hudební
skladatele	skladatel	k1gMnPc4	skladatel
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
rukopis	rukopis	k1gInSc1	rukopis
nezapře	zapřít	k5eNaPmIp3nS	zapřít
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
barokní	barokní	k2eAgFnSc3d1	barokní
polyfonii	polyfonie	k1gFnSc3	polyfonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
skladbě	skladba	k1gFnSc6	skladba
<g/>
,	,	kIx,	,
zachycené	zachycený	k2eAgFnSc6d1	zachycená
na	na	k7c6	na
videosnímku	videosnímek	k1gInSc6	videosnímek
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
zpívá	zpívat	k5eAaImIp3nS	zpívat
v	v	k7c6	v
duetu	duet	k1gInSc6	duet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
negativní	negativní	k2eAgInSc4d1	negativní
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
moderním	moderní	k2eAgInPc3d1	moderní
hudebním	hudební	k2eAgInPc3d1	hudební
žánrům	žánr	k1gInPc3	žánr
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgMnS	nazývat
Poslední	poslední	k2eAgMnSc1d1	poslední
puritán	puritán	k1gMnSc1	puritán
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
klasický	klasický	k2eAgInSc4d1	klasický
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
s	s	k7c7	s
jazzmany	jazzman	k1gMnPc7	jazzman
také	také	k6eAd1	také
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
<g/>
,	,	kIx,	,
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
kantilénový	kantilénový	k2eAgInSc4d1	kantilénový
zpěv	zpěv	k1gInSc4	zpěv
Petuly	Petula	k1gFnSc2	Petula
Clarkové	Clarková	k1gFnSc2	Clarková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
například	například	k6eAd1	například
Beatles	Beatles	k1gFnPc2	Beatles
za	za	k7c4	za
špatné	špatný	k2eAgFnPc4d1	špatná
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidské	lidský	k2eAgFnPc4d1	lidská
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
==	==	k?	==
</s>
</p>
<p>
<s>
Gould	Gould	k1gInSc1	Gould
byl	být	k5eAaImAgInS	být
lidsky	lidsky	k6eAd1	lidsky
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
osamělý	osamělý	k2eAgMnSc1d1	osamělý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
soustředěný	soustředěný	k2eAgMnSc1d1	soustředěný
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
zájmy	zájem	k1gInPc4	zájem
hudební	hudební	k2eAgInPc4d1	hudební
<g/>
,	,	kIx,	,
hudebně	hudebně	k6eAd1	hudebně
interpretační	interpretační	k2eAgFnPc1d1	interpretační
a	a	k8xC	a
publicistické	publicistický	k2eAgFnPc1d1	publicistická
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hře	hra	k1gFnSc6	hra
si	se	k3xPyFc3	se
často	často	k6eAd1	často
pobrukoval	pobrukovat	k5eAaImAgMnS	pobrukovat
či	či	k8xC	či
prozpěvoval	prozpěvovat	k5eAaImAgMnS	prozpěvovat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
nahrávkách	nahrávka	k1gFnPc6	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vozil	vozit	k5eAaImAgMnS	vozit
výhradně	výhradně	k6eAd1	výhradně
starou	starý	k2eAgFnSc4d1	stará
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
židli	židle	k1gFnSc4	židle
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
otec	otec	k1gMnSc1	otec
jako	jako	k8xC	jako
dorůstajícímu	dorůstající	k2eAgMnSc3d1	dorůstající
chlapci	chlapec	k1gMnSc3	chlapec
pečlivě	pečlivě	k6eAd1	pečlivě
přizpůsoboval	přizpůsobovat	k5eAaImAgMnS	přizpůsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
ji	on	k3xPp3gFnSc4	on
i	i	k8xC	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
opotřebeném	opotřebený	k2eAgNnSc6d1	opotřebené
sedadle	sedadlo	k1gNnSc6	sedadlo
chyběla	chybět	k5eAaImAgFnS	chybět
dvě	dva	k4xCgNnPc4	dva
prkénka	prkénko	k1gNnPc4	prkénko
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
torontském	torontský	k2eAgNnSc6d1	Torontské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgMnS	trpět
zimomřivostí	zimomřivost	k1gFnSc7	zimomřivost
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
chodil	chodit	k5eAaImAgMnS	chodit
teple	teple	k6eAd1	teple
oblečen	obléct	k5eAaPmNgMnS	obléct
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
kabátě	kabát	k1gInSc6	kabát
<g/>
,	,	kIx,	,
s	s	k7c7	s
čepicí	čepice	k1gFnSc7	čepice
a	a	k8xC	a
v	v	k7c6	v
rukavicích	rukavice	k1gFnPc6	rukavice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
parku	park	k1gInSc6	park
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
policií	policie	k1gFnPc2	policie
omylem	omylem	k6eAd1	omylem
zatčen	zatknout	k5eAaPmNgMnS	zatknout
v	v	k7c4	v
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bezdomovec	bezdomovec	k1gMnSc1	bezdomovec
<g/>
.	.	kIx.	.
</s>
<s>
Stravoval	stravovat	k5eAaImAgInS	stravovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
restauraci	restaurace	k1gFnSc6	restaurace
s	s	k7c7	s
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
provozem	provoz	k1gInSc7	provoz
Fran	Frana	k1gFnPc2	Frana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přicházel	přicházet	k5eAaImAgInS	přicházet
mezi	mezi	k7c7	mezi
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
hodinou	hodina	k1gFnSc7	hodina
ranní	ranní	k2eAgFnSc7d1	ranní
<g/>
,	,	kIx,	,
sedal	sedat	k5eAaImAgMnS	sedat
stále	stále	k6eAd1	stále
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
jedl	jíst	k5eAaImAgMnS	jíst
stejné	stejný	k2eAgNnSc4d1	stejné
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
míchaná	míchaný	k2eAgNnPc4d1	míchané
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
domácí	domácí	k2eAgFnSc4d1	domácí
stravu	strava	k1gFnSc4	strava
omezil	omezit	k5eAaPmAgInS	omezit
na	na	k7c4	na
sklenici	sklenice	k1gFnSc4	sklenice
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
vejce	vejce	k1gNnSc4	vejce
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žil	žít	k5eAaImAgMnS	žít
osaměle	osaměle	k6eAd1	osaměle
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mlčenlivý	mlčenlivý	k2eAgMnSc1d1	mlčenlivý
a	a	k8xC	a
málokdy	málokdy	k6eAd1	málokdy
se	se	k3xPyFc4	se
s	s	k7c7	s
někým	někdo	k3yInSc7	někdo
sblížil	sblížit	k5eAaPmAgInS	sblížit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setkáních	setkání	k1gNnPc6	setkání
nepodával	podávat	k5eNaImAgMnS	podávat
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
choulostivý	choulostivý	k2eAgInSc1d1	choulostivý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nedotýkal	dotýkat	k5eNaImAgMnS	dotýkat
a	a	k8xC	a
neobjímal	objímat	k5eNaImAgMnS	objímat
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
psychiatr	psychiatr	k1gMnSc1	psychiatr
Peter	Peter	k1gMnSc1	Peter
Ostwald	Ostwald	k1gMnSc1	Ostwald
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
chování	chování	k1gNnSc4	chování
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
autismu	autismus	k1gInSc2	autismus
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Aspergerův	Aspergerův	k2eAgInSc4d1	Aspergerův
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
–	–	k?	–
chronologický	chronologický	k2eAgInSc1d1	chronologický
seznam	seznam	k1gInSc1	seznam
nahrávek	nahrávka	k1gFnPc2	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
<g/>
/	/	kIx~	/
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
<g/>
30	[number]	k4	30
E	E	kA	E
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
109	[number]	k4	109
<g/>
;	;	kIx,	;
č.	č.	k?	č.
31	[number]	k4	31
A-Flat	A-Fle	k1gNnPc2	A-Fle
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
110	[number]	k4	110
<g/>
;	;	kIx,	;
č.	č.	k?	č.
32	[number]	k4	32
C	C	kA	C
minor	minor	k2eAgFnSc1d1	minor
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
111	[number]	k4	111
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
<g/>
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
1	[number]	k4	1
D	D	kA	D
Minor	minor	k2eAgMnSc1d1	minor
<g/>
;	;	kIx,	;
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
2	[number]	k4	2
B-Flat	B-Fle	k1gNnPc2	B-Fle
Major	major	k1gMnSc1	major
<g/>
;	;	kIx,	;
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
/	/	kIx~	/
orchestr	orchestr	k1gInSc1	orchestr
Colombia	Colombium	k1gNnSc2	Colombium
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
/	/	kIx~	/
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
<g/>
/	/	kIx~	/
Partita	partita	k1gFnSc1	partita
č.	č.	k?	č.
5	[number]	k4	5
in	in	k?	in
G	G	kA	G
major	major	k1gMnSc1	major
<g/>
;	;	kIx,	;
Partita	partita	k1gFnSc1	partita
č.	č.	k?	č.
6	[number]	k4	6
in	in	k?	in
E	E	kA	E
Minor	minor	k2eAgFnSc1d1	minor
<g/>
;	;	kIx,	;
Fugy	fuga	k1gFnSc2	fuga
č.	č.	k?	č.
14	[number]	k4	14
F-Sharp	F-Sharp	k1gInSc1	F-Sharp
Minor	minor	k2eAgInSc1d1	minor
a	a	k8xC	a
č.	č.	k?	č.
9	[number]	k4	9
E	E	kA	E
Major	major	k1gMnSc1	major
z	z	k7c2	z
druhého	druhý	k4xOgInSc2	druhý
cyklu	cyklus	k1gInSc2	cyklus
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
57	[number]	k4	57
<g/>
/	/	kIx~	/
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
/	/	kIx~	/
<g/>
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
59	[number]	k4	59
E-Flat	E-Fle	k1gNnPc2	E-Fle
Major	major	k1gMnSc1	major
<g/>
;	;	kIx,	;
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
/	/	kIx~	/
<g/>
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
10	[number]	k4	10
C	C	kA	C
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
K.	K.	kA	K.
330	[number]	k4	330
<g/>
;	;	kIx,	;
Fantazie	fantazie	k1gFnSc1	fantazie
a	a	k8xC	a
Fuga	fuga	k1gFnSc1	fuga
C	C	kA	C
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
K.	K.	kA	K.
394	[number]	k4	394
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
/	/	kIx~	/
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
<g/>
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
5	[number]	k4	5
F	F	kA	F
Major	major	k1gMnSc1	major
<g/>
;	;	kIx,	;
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
1	[number]	k4	1
C	C	kA	C
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
15	[number]	k4	15
<g/>
;	;	kIx,	;
Vladimir	Vladimir	k1gMnSc1	Vladimir
Golschmann	Golschmann	k1gMnSc1	Golschmann
<g/>
/	/	kIx~	/
Colombia	Colombia	k1gFnSc1	Colombia
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
/	/	kIx~	/
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alban	Alban	k1gMnSc1	Alban
Berg	Berg	k1gMnSc1	Berg
<g/>
/	/	kIx~	/
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
1	[number]	k4	1
<g/>
;	;	kIx,	;
A.	A.	kA	A.
Schoenberg	Schoenberg	k1gInSc1	Schoenberg
<g/>
/	/	kIx~	/
Tři	tři	k4xCgFnPc1	tři
klavírní	klavírní	k2eAgFnPc1d1	klavírní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
11	[number]	k4	11
<g/>
;	;	kIx,	;
Ernst	Ernst	k1gMnSc1	Ernst
Křenek	Křenek	k1gMnSc1	Křenek
<g/>
/	/	kIx~	/
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
92	[number]	k4	92
<g/>
,	,	kIx,	,
č.	č.	k?	č.
4	[number]	k4	4
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
/	/	kIx~	/
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
3	[number]	k4	3
C	C	kA	C
Minor	minor	k2eAgFnSc1d1	minor
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
39	[number]	k4	39
<g/>
;	;	kIx,	;
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
/	/	kIx~	/
Colombia	Colombia	k1gFnSc1	Colombia
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Partita	partita	k1gFnSc1	partita
č.	č.	k?	č.
1	[number]	k4	1
B-Flat	B-Fle	k1gNnPc2	B-Fle
Major	major	k1gMnSc1	major
<g/>
;	;	kIx,	;
č.	č.	k?	č.
2	[number]	k4	2
C	C	kA	C
Minor	minor	k2eAgMnSc1d1	minor
<g/>
;	;	kIx,	;
Italský	italský	k2eAgInSc1d1	italský
koncert	koncert	k1gInSc1	koncert
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
Gould	Gould	k1gMnSc1	Gould
/	/	kIx~	/
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
1	[number]	k4	1
<g/>
;	;	kIx,	;
Symphonia	Symphonium	k1gNnPc1	Symphonium
Quartet	Quarteta	k1gFnPc2	Quarteta
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Brahms	Brahms	k1gMnSc1	Brahms
/	/	kIx~	/
Deset	deset	k4xCc4	deset
intermezz	intermezzo	k1gNnPc2	intermezzo
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
4	[number]	k4	4
G	G	kA	G
Major	major	k1gMnSc1	major
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
58	[number]	k4	58
<g/>
:	:	kIx,	:
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
/	/	kIx~	/
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
/	/	kIx~	/
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Umění	umění	k1gNnSc1	umění
fugy	fuga	k1gFnSc2	fuga
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Kontrapunkty	kontrapunkt	k1gInPc7	kontrapunkt
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
na	na	k7c4	na
varhany	varhany	k1gFnPc4	varhany
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
/	/	kIx~	/
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
24	[number]	k4	24
C	C	kA	C
Minor	minor	k2eAgMnSc1d1	minor
<g/>
,	,	kIx,	,
K.	K.	kA	K.
491	[number]	k4	491
/	/	kIx~	/
Walter	Walter	k1gMnSc1	Walter
Busskind	Busskind	k1gMnSc1	Busskind
/	/	kIx~	/
CBC	CBC	kA	CBC
Symphony	Symphona	k1gFnSc2	Symphona
<g/>
;	;	kIx,	;
A.	A.	kA	A.
Schoenberg	Schoenberg	k1gInSc4	Schoenberg
/	/	kIx~	/
Klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
42	[number]	k4	42
/	/	kIx~	/
Robert	Robert	k1gMnSc1	Robert
Croft	Croft	k1gMnSc1	Croft
/	/	kIx~	/
CBC	CBC	kA	CBC
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
/	/	kIx~	/
<g/>
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Strauss	Strauss	k1gInSc1	Strauss
/	/	kIx~	/
Enoch	Enoch	k1gInSc1	Enoch
Arden	Ardeny	k1gFnPc2	Ardeny
<g/>
;	;	kIx,	;
Claude	Claud	k1gInSc5	Claud
Rains	Rains	k1gInSc1	Rains
<g/>
,	,	kIx,	,
přednes	přednes	k1gInSc1	přednes
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
/	/	kIx~	/
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
desky	deska	k1gFnPc4	deska
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
/	/	kIx~	/
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Invence	invence	k1gFnPc1	invence
a	a	k8xC	a
sinfonie	sinfonia	k1gFnPc1	sinfonia
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
/	/	kIx~	/
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1-3	[number]	k4	1-3
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
/	/	kIx~	/
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Schoenberg	Schoenberg	k1gMnSc1	Schoenberg
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnPc1d1	klavírní
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
Opusy	opus	k1gInPc1	opus
11	[number]	k4	11
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
,	,	kIx,	,
a	a	k8xC	a
33	[number]	k4	33
<g/>
;	;	kIx,	;
Suita	suita	k1gFnSc1	suita
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
25	[number]	k4	25
<g/>
;	;	kIx,	;
Písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
1	[number]	k4	1
(	(	kIx(	(
<g/>
s	s	k7c7	s
Danoe	Danoe	k1gNnSc7	Danoe
Grammem	Grammo	k1gNnSc7	Grammo
<g/>
,	,	kIx,	,
basbaryton	basbaryton	k1gInSc1	basbaryton
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ellen	Ellen	k1gInSc1	Ellen
Faull	Faull	k1gInSc1	Faull
<g/>
,	,	kIx,	,
soprán	soprán	k1gInSc1	soprán
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
visutých	visutý	k2eAgFnPc6d1	visutá
zahradách	zahrada	k1gFnPc6	zahrada
Semiramidiných	Semiramidin	k2eAgMnPc2d1	Semiramidin
(	(	kIx(	(
<g/>
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Vanni	Vann	k1gMnPc1	Vann
<g/>
,	,	kIx,	,
soprán	soprán	k1gInSc1	soprán
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
/	/	kIx~	/
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
5	[number]	k4	5
"	"	kIx"	"
<g/>
Císařský	císařský	k2eAgMnSc1d1	císařský
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Leopold	Leopold	k1gMnSc1	Leopold
Stokowski	Stokowsk	k1gFnSc2	Stokowsk
/	/	kIx~	/
American	American	k1gInSc1	American
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnPc1d1	klavírní
sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
13	[number]	k4	13
"	"	kIx"	"
<g/>
Patetická	patetický	k2eAgNnPc4d1	patetické
<g/>
"	"	kIx"	"
a	a	k8xC	a
14	[number]	k4	14
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bacha	k1gFnPc2	Bacha
/	/	kIx~	/
Klavírní	klavírní	k2eAgInPc1d1	klavírní
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
a	a	k8xC	a
7	[number]	k4	7
<g/>
;	;	kIx,	;
Vladimir	Vladimir	k1gMnSc1	Vladimir
Golschmann	Golschmann	k1gMnSc1	Golschmann
/	/	kIx~	/
Colombia	Colombia	k1gFnSc1	Colombia
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Schoenberg	Schoenberg	k1gMnSc1	Schoenberg
/	/	kIx~	/
Óda	óda	k1gFnSc1	óda
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
<g/>
,	,	kIx,	,
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Hortonem	Horton	k1gInSc7	Horton
(	(	kIx(	(
<g/>
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
)	)	kIx)	)
a	a	k8xC	a
Juilliard	Juilliard	k1gMnSc1	Juilliard
Quartet	Quartet	k1gMnSc1	Quartet
<g/>
;	;	kIx,	;
Fantazie	fantazie	k1gFnSc1	fantazie
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
s	s	k7c7	s
Israelem	Israel	k1gMnSc7	Israel
Bakerem	Baker	k1gMnSc7	Baker
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
/	/	kIx~	/
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
hudba	hudba	k1gFnSc1	hudba
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
Oskar	Oskar	k1gMnSc1	Oskar
Morawetz	Morawetz	k1gMnSc1	Morawetz
/	/	kIx~	/
Fantazie	fantazie	k1gFnSc1	fantazie
in	in	k?	in
D	D	kA	D
<g/>
;	;	kIx,	;
Anhalt	Anhalt	k1gMnSc1	Anhalt
/	/	kIx~	/
Fantasia	Fantasia	k1gFnSc1	Fantasia
<g/>
;	;	kIx,	;
Hétu	Hétus	k1gInSc2	Hétus
/	/	kIx~	/
Variace	variace	k1gFnPc1	variace
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	van	k1gInSc1	van
Beethoven-Férencz	Beethoven-Férencz	k1gInSc1	Beethoven-Férencz
Liszt	Liszt	k1gInSc1	Liszt
/	/	kIx~	/
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
5	[number]	k4	5
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Nos	nos	k1gInSc1	nos
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
K.	K.	kA	K.
279-283	[number]	k4	279-283
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
2	[number]	k4	2
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
desky	deska	k1gFnPc1	deska
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
S.	S.	kA	S.
Prokofjev	Prokofjev	k1gFnSc1	Prokofjev
/	/	kIx~	/
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
7	[number]	k4	7
<g/>
;	;	kIx,	;
Skrjabin	Skrjabina	k1gFnPc2	Skrjabina
/	/	kIx~	/
Sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
3	[number]	k4	3
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
a	a	k8xC	a
9	[number]	k4	9
<g/>
,	,	kIx,	,
K.	K.	kA	K.
284	[number]	k4	284
<g/>
,	,	kIx,	,
309	[number]	k4	309
<g/>
,	,	kIx,	,
311	[number]	k4	311
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bacha	k1gFnPc2	Bacha
/	/	kIx~	/
Klavírní	klavírní	k2eAgInPc1d1	klavírní
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
č.	č.	k?	č.
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
<g/>
;	;	kIx,	;
Vladimir	Vladimir	k1gMnSc1	Vladimir
Golschmann	Golschmann	k1gMnSc1	Golschmann
/	/	kIx~	/
Colombia	Colombia	k1gFnSc1	Colombia
Symphony	Symphona	k1gFnSc2	Symphona
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Schumann	Schumann	k1gMnSc1	Schumann
/	/	kIx~	/
Klavírní	klavírní	k2eAgInSc1d1	klavírní
kvartet	kvartet	k1gInSc1	kvartet
E-Flat	E-Fle	k1gNnPc2	E-Fle
Major	major	k1gMnSc1	major
<g/>
;	;	kIx,	;
Juilliard	Juilliard	k1gMnSc1	Juilliard
Quartet	Quartet	k1gMnSc1	Quartet
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnPc1d1	klavírní
sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
13	[number]	k4	13
"	"	kIx"	"
<g/>
Patetická	patetický	k2eAgFnSc1d1	patetická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
27	[number]	k4	27
č.	č.	k?	č.
2	[number]	k4	2
"	"	kIx"	"
<g/>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
57	[number]	k4	57
"	"	kIx"	"
<g/>
Appassionata	appassionato	k1gNnSc2	appassionato
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Variace	variace	k1gFnSc1	variace
<g/>
,	,	kIx,	,
WoO	WoO	k1gFnSc1	WoO
80	[number]	k4	80
a	a	k8xC	a
Opusy	opus	k1gInPc1	opus
34	[number]	k4	34
a	a	k8xC	a
35	[number]	k4	35
"	"	kIx"	"
<g/>
Eroica	Eroic	k1gInSc2	Eroic
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Consort	Consort	k1gInSc1	Consort
of	of	k?	of
Musicke	Musicke	k1gInSc1	Musicke
Bye	Bye	k1gMnSc1	Bye
William	William	k1gInSc1	William
Byrde	Byrd	k1gInSc5	Byrd
a	a	k8xC	a
Orlando	Orlanda	k1gFnSc5	Orlanda
Gibbons	Gibbonsa	k1gFnPc2	Gibbonsa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
/	/	kIx~	/
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
:	:	kIx,	:
č.	č.	k?	č.
8	[number]	k4	8
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
a	a	k8xC	a
13	[number]	k4	13
<g/>
,	,	kIx,	,
K.	K.	kA	K.
310	[number]	k4	310
<g/>
,	,	kIx,	,
330	[number]	k4	330
<g/>
,	,	kIx,	,
332	[number]	k4	332
<g/>
,	,	kIx,	,
a	a	k8xC	a
333	[number]	k4	333
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
56	[number]	k4	56
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
/	/	kIx~	/
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Schoenberg	Schoenberg	k1gInSc1	Schoenberg
/	/	kIx~	/
Písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
a	a	k8xC	a
48	[number]	k4	48
<g/>
,	,	kIx,	,
a	a	k8xC	a
Opus	opus	k1gInSc1	opus
posthumus	posthumus	k1gInSc1	posthumus
<g/>
,	,	kIx,	,
s	s	k7c7	s
Donaldem	Donald	k1gMnSc7	Donald
Grammem	Gramm	k1gMnSc7	Gramm
<g/>
,	,	kIx,	,
Helenou	Helena	k1gFnSc7	Helena
Vanni	Vanen	k2eAgMnPc1d1	Vanen
a	a	k8xC	a
Cornelisem	Cornelis	k1gInSc7	Cornelis
Opthofem	Opthof	k1gMnSc7	Opthof
<g/>
,	,	kIx,	,
baryton	baryton	k1gMnSc1	baryton
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
/	/	kIx~	/
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
G.	G.	kA	G.
F.	F.	kA	F.
Händel	Händlo	k1gNnPc2	Händlo
/	/	kIx~	/
Suity	suita	k1gFnSc2	suita
1-4	[number]	k4	1-4
pro	pro	k7c4	pro
harpsichord	harpsichord	k1gInSc4	harpsichord
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
/	/	kIx~	/
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Grieg	Grieg	k1gInSc1	Grieg
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
sonáta	sonáta	k1gFnSc1	sonáta
<g/>
;	;	kIx,	;
G.	G.	kA	G.
Bizet	Bizet	k1gInSc1	Bizet
/	/	kIx~	/
První	první	k4xOgNnSc1	první
nokturno	nokturno	k1gNnSc1	nokturno
a	a	k8xC	a
Chromatické	chromatický	k2eAgFnPc1d1	chromatická
variace	variace	k1gFnPc1	variace
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
suity	suita	k1gFnSc2	suita
č.	č.	k?	č.
1-4	[number]	k4	1-4
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
:	:	kIx,	:
č.	č.	k?	č.
11	[number]	k4	11
a	a	k8xC	a
15	[number]	k4	15
<g/>
,	,	kIx,	,
K.	K.	kA	K.
331	[number]	k4	331
a	a	k8xC	a
545	[number]	k4	545
<g/>
;	;	kIx,	;
K.	K.	kA	K.
533	[number]	k4	533
<g/>
;	;	kIx,	;
Rondo	rondo	k1gNnSc1	rondo
<g/>
,	,	kIx,	,
K.	K.	kA	K.
494	[number]	k4	494
<g/>
;	;	kIx,	;
Fantasia	Fantasia	k1gFnSc1	Fantasia
in	in	k?	in
d	d	k?	d
minor	minor	k2eAgFnPc7d1	minor
<g/>
,	,	kIx,	,
K.	K.	kA	K.
397	[number]	k4	397
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnPc1d1	klavírní
sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
31	[number]	k4	31
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
"	"	kIx"	"
<g/>
Tempest	Tempest	k1gInSc1	Tempest
<g/>
"	"	kIx"	"
a	a	k8xC	a
3	[number]	k4	3
(	(	kIx(	(
<g/>
196	[number]	k4	196
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Hindemith	Hindemith	k1gMnSc1	Hindemith
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1-3	[number]	k4	1-3
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Wagner	Wagner	k1gMnSc1	Wagner
–	–	k?	–
G.	G.	kA	G.
Gould	Gould	k1gInSc1	Gould
/	/	kIx~	/
Předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
Mistrům	mistr	k1gMnPc3	mistr
pěvcům	pěvec	k1gMnPc3	pěvec
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Úsvit	úsvit	k1gInSc1	úsvit
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Siegfriedovo	Siegfriedův	k2eAgNnSc1d1	Siegfriedův
putování	putování	k1gNnSc1	putování
po	po	k7c6	po
Rýně	Rýn	k1gInSc6	Rýn
<g/>
"	"	kIx"	"
ze	z	k7c2	z
Soumraku	soumrak	k1gInSc2	soumrak
bohů	bůh	k1gMnPc2	bůh
<g/>
;	;	kIx,	;
Siegfriedova	Siegfriedův	k2eAgFnSc1d1	Siegfriedova
idyla	idyla	k1gFnSc1	idyla
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
/	/	kIx~	/
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bacha	k1gFnPc2	Bacha
<g/>
,	,	kIx,	,
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
suity	suita	k1gFnSc2	suita
<g/>
,	,	kIx,	,
č.	č.	k?	č.
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
;	;	kIx,	;
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
overtura	overtura	k1gFnSc1	overtura
B	B	kA	B
Minor	minor	k2eAgFnSc1d1	minor
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
/	/	kIx~	/
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Sonáty	sonáta	k1gFnPc1	sonáta
pro	pro	k7c4	pro
violu	viola	k1gFnSc4	viola
da	da	k?	da
gamba	gamba	k1gFnSc1	gamba
a	a	k8xC	a
harpsichord	harpsichord	k1gInSc1	harpsichord
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
s	s	k7c7	s
Leonardem	Leonardo	k1gMnSc7	Leonardo
Rosem	Ros	k1gMnSc7	Ros
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
/	/	kIx~	/
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Walton	Walton	k1gInSc1	Walton
[	[	kIx(	[
William	William	k1gInSc1	William
Turner	turner	k1gMnSc1	turner
Walton	Walton	k1gInSc1	Walton
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
–	–	k?	–
1983	[number]	k4	1983
]	]	kIx)	]
<g/>
,	,	kIx,	,
From	From	k1gMnSc1	From
Facades	Facades	k1gMnSc1	Facades
Suites	Suites	k1gMnSc1	Suites
No	no	k9	no
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Scotch	Scotch	k1gInSc1	Scotch
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
From	Froma	k1gFnPc2	Froma
The	The	k1gFnSc2	The
Album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Glenn	Glenn	k1gInSc1	Glenn
Gould	Gould	k1gMnSc1	Gould
Collection	Collection	k1gInSc1	Collection
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.01	.01	k4	.01
<g/>
,	,	kIx,	,
prolog	prolog	k1gInSc1	prolog
zpívá	zpívat	k5eAaImIp3nS	zpívat
Patricia	Patricius	k1gMnSc4	Patricius
Rideout	Rideout	k1gMnSc1	Rideout
<g/>
,	,	kIx,	,
alt	alt	k1gInSc1	alt
<g/>
,	,	kIx,	,
a	a	k8xC	a
Glenn	Glenn	k1gMnSc1	Glenn
Gould	Gould	k1gMnSc1	Gould
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Bagatelly	Bagatella	k1gFnPc1	Bagatella
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
33	[number]	k4	33
a	a	k8xC	a
126	[number]	k4	126
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
/	/	kIx~	/
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
/	/	kIx~	/
Klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
:	:	kIx,	:
č.	č.	k?	č.
14	[number]	k4	14
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
a	a	k8xC	a
17	[number]	k4	17
<g/>
,	,	kIx,	,
K.	K.	kA	K.
457	[number]	k4	457
<g/>
,	,	kIx,	,
570	[number]	k4	570
<g/>
,	,	kIx,	,
a	a	k8xC	a
576	[number]	k4	576
<g/>
;	;	kIx,	;
Fantasia	Fantasia	k1gFnSc1	Fantasia
C	C	kA	C
Minor	minor	k2eAgFnSc1d1	minor
<g/>
,	,	kIx,	,
K.	K.	kA	K.
475	[number]	k4	475
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
/	/	kIx~	/
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Hindemith	Hindemith	k1gInSc1	Hindemith
/	/	kIx~	/
Sonáty	sonáta	k1gFnPc1	sonáta
pro	pro	k7c4	pro
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
tubu	tuba	k1gFnSc4	tuba
<g/>
,	,	kIx,	,
trubku	trubka	k1gFnSc4	trubka
<g/>
,	,	kIx,	,
altový	altový	k2eAgInSc4d1	altový
roh	roh	k1gInSc4	roh
a	a	k8xC	a
trombón	trombón	k1gInSc4	trombón
<g/>
,	,	kIx,	,
Filadelfský	filadelfský	k2eAgInSc4d1	filadelfský
dechový	dechový	k2eAgInSc4d1	dechový
soubor	soubor	k1gInSc4	soubor
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
76	[number]	k4	76
<g/>
/	/	kIx~	/
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bacha	k1gFnPc2	Bacha
/	/	kIx~	/
Šest	šest	k4xCc1	šest
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
harpsichord	harpsichord	k1gInSc4	harpsichord
<g/>
,	,	kIx,	,
s	s	k7c7	s
Jaime	Jaim	k1gMnSc5	Jaim
Laredo	Lareda	k1gMnSc5	Lareda
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
76	[number]	k4	76
<g/>
/	/	kIx~	/
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Šest	šest	k4xCc4	šest
anglických	anglický	k2eAgFnPc2d1	anglická
suit	suita	k1gFnPc2	suita
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
76	[number]	k4	76
<g/>
/	/	kIx~	/
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Sibelius	Sibelius	k1gInSc1	Sibelius
/	/	kIx~	/
Tři	tři	k4xCgFnPc1	tři
sonatiny	sonatina	k1gFnPc1	sonatina
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
67	[number]	k4	67
<g/>
;	;	kIx,	;
Kyllikki	Kyllikki	k1gNnSc4	Kyllikki
/	/	kIx~	/
Tři	tři	k4xCgFnPc1	tři
lyrické	lyrický	k2eAgFnPc1d1	lyrická
skladby	skladba	k1gFnPc1	skladba
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Hindemith	Hindemith	k1gMnSc1	Hindemith
/	/	kIx~	/
Život	život	k1gInSc1	život
Mariin	Mariin	k2eAgInSc1d1	Mariin
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
verze	verze	k1gFnSc1	verze
z	z	k7c2	z
r.	r.	kA	r.
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
s	s	k7c7	s
Roxolanou	Roxolaný	k2eAgFnSc7d1	Roxolaný
Roslakovou	Roslakový	k2eAgFnSc7d1	Roslakový
<g/>
,	,	kIx,	,
soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
/	/	kIx~	/
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bacha	k1gFnPc2	Bacha
<g/>
/	/	kIx~	/
Sedm	sedm	k4xCc1	sedm
toccat	toccata	k1gFnPc2	toccata
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
albech	album	k1gNnPc6	album
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
/	/	kIx~	/
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bacha	k1gFnPc2	Bacha
/	/	kIx~	/
Preludia	preludium	k1gNnSc2	preludium
<g/>
,	,	kIx,	,
Fugetty	Fugetta	k1gFnSc2	Fugetta
a	a	k8xC	a
Fugy	fuga	k1gFnSc2	fuga
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
jubilejní	jubilejní	k2eAgNnSc1d1	jubilejní
album	album	k1gNnSc1	album
Glenna	Glenn	k1gMnSc2	Glenn
Goulda	Gould	k1gMnSc2	Gould
<g/>
:	:	kIx,	:
D.	D.	kA	D.
Scarlatti	Scarlatť	k1gFnSc2	Scarlatť
<g/>
/	/	kIx~	/
Sonáty	sonáta	k1gFnSc2	sonáta
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
/	/	kIx~	/
463	[number]	k4	463
<g/>
,	,	kIx,	,
413	[number]	k4	413
<g/>
,	,	kIx,	,
a	a	k8xC	a
486	[number]	k4	486
(	(	kIx(	(
<g/>
rec	rec	k?	rec
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
C.	C.	kA	C.
P.	P.	kA	P.
E.	E.	kA	E.
Bach	Bach	k1gMnSc1	Bach
<g/>
/	/	kIx~	/
Würtemberská	Würtemberský	k2eAgFnSc1d1	Würtemberský
sonáta	sonáta	k1gFnSc1	sonáta
č.	č.	k?	č.
1	[number]	k4	1
(	(	kIx(	(
<g/>
rec	rec	k?	rec
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Glenn	Glenn	k1gMnSc1	Glenn
Gould	Gould	k1gMnSc1	Gould
/	/	kIx~	/
Tak	tak	k6eAd1	tak
ty	ten	k3xDgMnPc4	ten
chceš	chtít	k5eAaImIp2nS	chtít
psát	psát	k5eAaImF	psát
fugu	fuga	k1gFnSc4	fuga
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
rec	rec	k?	rec
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
A.	A.	kA	A.
N.	N.	kA	N.
Skrjabin	Skrjabina	k1gFnPc2	Skrjabina
/	/	kIx~	/
Dvě	dva	k4xCgFnPc1	dva
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
57	[number]	k4	57
(	(	kIx(	(
<g/>
rec	rec	k?	rec
<g/>
.	.	kIx.	.
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
J.	J.	kA	J.
Strauss	Strauss	k1gInSc1	Strauss
/	/	kIx~	/
Písně	píseň	k1gFnPc1	píseň
Ofélie	Ofélie	k1gFnPc1	Ofélie
<g/>
,	,	kIx,	,
s	s	k7c7	s
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Schwarzkopfovou	Schwarzkopfový	k2eAgFnSc4d1	Schwarzkopfová
<g/>
,	,	kIx,	,
soprán	soprán	k1gInSc1	soprán
(	(	kIx(	(
<g/>
rec	rec	k?	rec
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
–	–	k?	–
F.	F.	kA	F.
Liszt	Liszt	k1gInSc1	Liszt
/	/	kIx~	/
Symfonie	symfonie	k1gFnSc1	symfonie
pastorální	pastorální	k2eAgFnSc1d1	pastorální
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
(	(	kIx(	(
<g/>
rec	rec	k?	rec
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Fantazie	fantazie	k1gFnSc2	fantazie
Glenna	Glenn	k1gMnSc4	Glenn
Goulda	Gould	k1gMnSc4	Gould
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Haydn	Haydn	k1gInSc1	Haydn
/	/	kIx~	/
Sonáty	sonáta	k1gFnSc2	sonáta
č.	č.	k?	č.
56	[number]	k4	56
<g/>
,	,	kIx,	,
58-62	[number]	k4	58-62
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
/	/	kIx~	/
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Bach	Bach	k1gMnSc1	Bach
/	/	kIx~	/
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
/	/	kIx~	/
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Brahms	Brahms	k1gMnSc1	Brahms
/	/	kIx~	/
Balady	balada	k1gFnPc1	balada
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
10	[number]	k4	10
<g/>
;	;	kIx,	;
Rapsodie	rapsodie	k1gFnSc1	rapsodie
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
79	[number]	k4	79
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
L.	L.	kA	L.
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
/	/	kIx~	/
Sonáty	sonáta	k1gFnPc1	sonáta
<g/>
,	,	kIx,	,
Opusy	opus	k1gInPc1	opus
26	[number]	k4	26
a	a	k8xC	a
27	[number]	k4	27
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
/	/	kIx~	/
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
J.	J.	kA	J.
<g/>
Strauss	Strauss	k1gInSc1	Strauss
/	/	kIx~	/
Sonáta	sonáta	k1gFnSc1	sonáta
in	in	k?	in
B	B	kA	B
minor	minor	k2eAgFnSc1d1	minor
<g/>
,	,	kIx,	,
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
;	;	kIx,	;
Pět	pět	k4xCc4	pět
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
Opus	opus	k1gInSc1	opus
3	[number]	k4	3
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Glenn	Glenna	k1gFnPc2	Glenna
Gould	Gouldo	k1gNnPc2	Gouldo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Glenn	Glenno	k1gNnPc2	Glenno
Gould	Gouldo	k1gNnPc2	Gouldo
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
