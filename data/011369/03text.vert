<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1934	[number]	k4	1934
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
dialektolog	dialektolog	k1gMnSc1	dialektolog
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnSc1d1	přední
představitel	představitel	k1gMnSc1	představitel
onomastiky	onomastika	k1gFnSc2	onomastika
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žák	Žák	k1gMnSc1	Žák
zejména	zejména	k9	zejména
Františka	František	k1gMnSc4	František
Trávníčka	Trávníček	k1gMnSc4	Trávníček
<g/>
,	,	kIx,	,
Arnošta	Arnošt	k1gMnSc4	Arnošt
Lamprechta	Lamprecht	k1gMnSc4	Lamprecht
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
Machka	Machek	k1gMnSc4	Machek
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
významném	významný	k2eAgInSc6d1	významný
Etymologickém	etymologický	k2eAgInSc6d1	etymologický
slovníku	slovník	k1gInSc6	slovník
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
se	se	k3xPyFc4	se
spolupodílel	spolupodílet	k5eAaImAgMnS	spolupodílet
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
přínosem	přínos	k1gInSc7	přínos
prof.	prof.	kA	prof.
Šrámka	Šrámek	k1gMnSc4	Šrámek
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
onomastiku	onomastika	k1gFnSc4	onomastika
již	již	k6eAd1	již
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
lingvistickou	lingvistický	k2eAgFnSc4d1	lingvistická
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
pomocnou	pomocný	k2eAgFnSc4d1	pomocná
vědu	věda	k1gFnSc4	věda
nebo	nebo	k8xC	nebo
okrajový	okrajový	k2eAgInSc4d1	okrajový
obor	obor	k1gInSc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Prof.	prof.	kA	prof.
Šrámek	Šrámek	k1gMnSc1	Šrámek
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Občanské	občanský	k2eAgFnSc2d1	občanská
iniciativy	iniciativa	k1gFnSc2	iniciativa
Česko	Česko	k1gNnSc1	Česko
/	/	kIx~	/
Czechia	Czechia	k1gFnSc1	Czechia
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnSc4d1	snažící
se	se	k3xPyFc4	se
o	o	k7c4	o
funkční	funkční	k2eAgNnSc4d1	funkční
rozlišování	rozlišování	k1gNnSc4	rozlišování
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
názvu	název	k1gInSc2	název
ČR	ČR	kA	ČR
Česko	Česko	k1gNnSc1	Česko
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Czechia	Czechia	k1gFnSc1	Czechia
[	[	kIx(	[
<g/>
čekia	čekia	k1gFnSc1	čekia
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
Tchéquie	Tchéquie	k1gFnSc1	Tchéquie
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
názvu	název	k1gInSc2	název
ČR	ČR	kA	ČR
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
République	République	k1gInSc1	République
tchè	tchè	k?	tchè
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
onomastických	onomastický	k2eAgNnPc6d1	onomastické
sdruženích	sdružení	k1gNnPc6	sdružení
==	==	k?	==
</s>
</p>
<p>
<s>
Předseda	předseda	k1gMnSc1	předseda
Onomastické	onomastický	k2eAgFnSc2d1	onomastická
komise	komise	k1gFnSc2	komise
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
-	-	kIx~	-
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Místopředseda	místopředseda	k1gMnSc1	místopředseda
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
onomastiku	onomastika	k1gFnSc4	onomastika
</s>
</p>
<p>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
International	International	k1gMnSc1	International
Council	Councila	k1gFnPc2	Councila
of	of	k?	of
Onomastic	Onomastice	k1gFnPc2	Onomastice
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
-	-	kIx~	-
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Zeměpisná	zeměpisný	k2eAgNnPc1d1	zeměpisné
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
I.	I.	kA	I.
Luttererem	Lutterer	k1gInSc7	Lutterer
</s>
</p>
<p>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
obecné	obecný	k2eAgFnSc2d1	obecná
onomastiky	onomastika	k1gFnSc2	onomastika
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
Šrámek	Šrámek	k1gMnSc1	Šrámek
</s>
</p>
<p>
<s>
ŠRÁMEK	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
Prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
CSc.	CSc.	kA	CSc.
na	na	k7c4	na
slaviste	slavist	k1gMnSc5	slavist
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
