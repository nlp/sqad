<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
přínosem	přínos	k1gInSc7	přínos
prof.	prof.	kA	prof.
Šrámka	Šrámek	k1gMnSc4	Šrámek
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
onomastiku	onomastika	k1gFnSc4	onomastika
již	již	k6eAd1	již
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
lingvistickou	lingvistický	k2eAgFnSc4d1	lingvistická
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
pomocnou	pomocný	k2eAgFnSc4d1	pomocná
vědu	věda	k1gFnSc4	věda
nebo	nebo	k8xC	nebo
okrajový	okrajový	k2eAgInSc4d1	okrajový
obor	obor	k1gInSc4	obor
<g/>
.	.	kIx.	.
</s>
