<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
italská	italský	k2eAgFnSc1d1	italská
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc1	tři
stejně	stejně	k6eAd1	stejně
široké	široký	k2eAgInPc1d1	široký
<g/>
,	,	kIx,	,
svislé	svislý	k2eAgInPc1d1	svislý
pruhy	pruh	k1gInPc1	pruh
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
však	však	k9	však
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zeleným	zelený	k2eAgInSc7d1	zelený
pruhem	pruh	k1gInSc7	pruh
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
barva	barva	k1gFnSc1	barva
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
rovnosti	rovnost	k1gFnSc2	rovnost
a	a	k8xC	a
nového	nový	k2eAgInSc2d1	nový
politického	politický	k2eAgInSc2d1	politický
pořádku	pořádek	k1gInSc2	pořádek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1946	[number]	k4	1946
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijata	přijmout	k5eAaPmNgFnS	přijmout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výklad	výklad	k1gInSc1	výklad
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	Zelená	k1gFnSc1	Zelená
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
zemi	zem	k1gFnSc4	zem
plání	pláň	k1gFnPc2	pláň
a	a	k8xC	a
kopců	kopec	k1gInPc2	kopec
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
sníh	sníh	k1gInSc4	sníh
z	z	k7c2	z
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc4	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
za	za	k7c4	za
italskou	italský	k2eAgFnSc4d1	italská
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
náboženský	náboženský	k2eAgInSc1d1	náboženský
výklad	výklad	k1gInSc1	výklad
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zelená	zelená	k1gFnSc1	zelená
znamená	znamenat	k5eAaImIp3nS	znamenat
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
představuje	představovat	k5eAaImIp3nS	představovat
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
červená	červená	k1gFnSc1	červená
představuje	představovat	k5eAaImIp3nS	představovat
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
teologické	teologický	k2eAgFnPc4d1	teologická
ctnosti	ctnost	k1gFnPc4	ctnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
odstíny	odstín	k1gInPc1	odstín
jsou	být	k5eAaImIp3nP	být
tyto	tyt	k2eAgNnSc4d1	tyto
<g/>
:	:	kIx,	:
zeleň	zeleň	k1gFnSc4	zeleň
luční	luční	k2eAgFnPc1d1	luční
<g/>
,	,	kIx,	,
běloba	běloba	k1gFnSc1	běloba
mléčná	mléčný	k2eAgFnSc1d1	mléčná
a	a	k8xC	a
červeň	červeň	k1gFnSc1	červeň
rajčatová	rajčatový	k2eAgFnSc1d1	rajčatová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
italské	italský	k2eAgFnPc1d1	italská
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
lombardská	lombardský	k2eAgFnSc1d1	Lombardská
národní	národní	k2eAgFnSc1d1	národní
garda	garda	k1gFnSc1	garda
a	a	k8xC	a
následně	následně	k6eAd1	následně
italská	italský	k2eAgFnSc1d1	italská
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
"	"	kIx"	"
<g/>
lombardská	lombardský	k2eAgFnSc1d1	Lombardská
<g/>
"	"	kIx"	"
legie	legie	k1gFnSc1	legie
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc4	barva
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Napoleon	napoleon	k1gInSc1	napoleon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
právě	právě	k9	právě
zelenou	zelená	k1gFnSc4	zelená
(	(	kIx(	(
<g/>
podobná	podobný	k2eAgFnSc1d1	podobná
trikolóra	trikolóra	k1gFnSc1	trikolóra
se	se	k3xPyFc4	se
však	však	k9	však
objevila	objevit	k5eAaPmAgFnS	objevit
již	již	k6eAd1	již
při	při	k7c6	při
studentských	studentský	k2eAgFnPc6d1	studentská
demonstracích	demonstrace	k1gFnPc6	demonstrace
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajkou	vlajka	k1gFnSc7	vlajka
republiky	republika	k1gFnSc2	republika
Cisalpinské	Cisalpinský	k2eAgFnSc2d1	Cisalpinská
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
republiky	republika	k1gFnSc2	republika
Italské	italský	k2eAgFnSc2d1	italská
(	(	kIx(	(
<g/>
přeměněné	přeměněný	k2eAgFnSc2d1	přeměněná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1805	[number]	k4	1805
-	-	kIx~	-
1814	[number]	k4	1814
na	na	k7c4	na
Italské	italský	k2eAgNnSc4d1	italské
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Napoleona	Napoleon	k1gMnSc4	Napoleon
zradilo	zradit	k5eAaPmAgNnS	zradit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
učinil	učinit	k5eAaImAgMnS	učinit
sardinský	sardinský	k2eAgMnSc1d1	sardinský
král	král	k1gMnSc1	král
Viktor	Viktor	k1gMnSc1	Viktor
Emanuel	Emanuel	k1gMnSc1	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
svou	svůj	k3xOyFgFnSc7	svůj
vlajkou	vlajka	k1gFnSc7	vlajka
a	a	k8xC	a
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
středu	střed	k1gInSc2	střed
přidal	přidat	k5eAaPmAgInS	přidat
znak	znak	k1gInSc1	znak
savojského	savojský	k2eAgInSc2d1	savojský
domu	dům	k1gInSc2	dům
-	-	kIx~	-
bílý	bílý	k2eAgInSc1d1	bílý
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
lemovaném	lemovaný	k2eAgInSc6d1	lemovaný
štítě	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
trikolóra	trikolóra	k1gFnSc1	trikolóra
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
vlajkou	vlajka	k1gFnSc7	vlajka
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
královský	královský	k2eAgInSc1d1	královský
znak	znak	k1gInSc1	znak
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1946	[number]	k4	1946
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přijata	přijmout	k5eAaPmNgFnS	přijmout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Talianska	Taliansko	k1gNnPc4	Taliansko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Italská	italský	k2eAgFnSc1d1	italská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
