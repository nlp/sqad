<p>
<s>
Jaderné	jaderný	k2eAgInPc1d1	jaderný
pohony	pohon	k1gInPc1	pohon
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
široký	široký	k2eAgInSc4d1	široký
výběr	výběr	k1gInSc4	výběr
typů	typ	k1gInPc2	typ
pohonů	pohon	k1gInPc2	pohon
využívajících	využívající	k2eAgFnPc2d1	využívající
jako	jako	k9	jako
primární	primární	k2eAgInSc4d1	primární
zdroj	zdroj	k1gInSc4	zdroj
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
využít	využít	k5eAaPmF	využít
jaderné	jaderný	k2eAgInPc4d1	jaderný
materiály	materiál	k1gInPc4	materiál
pro	pro	k7c4	pro
pohony	pohon	k1gInPc4	pohon
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
radium	radium	k1gNnSc1	radium
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vhodným	vhodný	k2eAgNnSc7d1	vhodné
palivem	palivo	k1gNnSc7	palivo
pro	pro	k7c4	pro
motory	motor	k1gInPc4	motor
pohánějící	pohánějící	k2eAgNnSc4d1	pohánějící
auta	auto	k1gNnPc4	auto
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc4	loď
či	či	k8xC	či
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
hypotézy	hypotéza	k1gFnSc2	hypotéza
ve	v	k7c4	v
skutečnost	skutečnost	k1gFnSc4	skutečnost
se	se	k3xPyFc4	se
realizovala	realizovat	k5eAaBmAgNnP	realizovat
o	o	k7c4	o
půlstoletí	půlstoletí	k1gNnSc4	půlstoletí
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
první	první	k4xOgFnSc1	první
jaderná	jaderný	k2eAgFnSc1d1	jaderná
ponorka	ponorka	k1gFnSc1	ponorka
s	s	k7c7	s
nápadným	nápadný	k2eAgInSc7d1	nápadný
názvem	název	k1gInSc7	název
Nautilus	nautilus	k1gInSc1	nautilus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgInPc1d1	jaderný
pohony	pohon	k1gInPc1	pohon
vděčí	vděčit	k5eAaImIp3nP	vděčit
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
využití	využití	k1gNnSc4	využití
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
studené	studený	k2eAgFnSc2d1	studená
válce	válka	k1gFnSc3	válka
z	z	k7c2	z
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhaly	probíhat	k5eAaImAgInP	probíhat
závody	závod	k1gInPc1	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
mezi	mezi	k7c7	mezi
největšími	veliký	k2eAgFnPc7d3	veliký
mocnostmi	mocnost	k1gFnPc7	mocnost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
bude	být	k5eAaImBp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
využít	využít	k5eAaPmF	využít
energii	energie	k1gFnSc4	energie
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
vývoj	vývoj	k1gInSc1	vývoj
jaderných	jaderný	k2eAgInPc2d1	jaderný
pohonů	pohon	k1gInPc2	pohon
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
ponorky	ponorka	k1gFnPc1	ponorka
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
tanky	tank	k1gInPc1	tank
a	a	k8xC	a
kosmické	kosmický	k2eAgFnPc1d1	kosmická
sondy	sonda	k1gFnPc1	sonda
byly	být	k5eAaImAgFnP	být
předmětem	předmět	k1gInSc7	předmět
vývoje	vývoj	k1gInSc2	vývoj
zejména	zejména	k9	zejména
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Reaktory	reaktor	k1gInPc1	reaktor
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
ponorek	ponorka	k1gFnPc2	ponorka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
vývoje	vývoj	k1gInSc2	vývoj
reaktorů	reaktor	k1gInPc2	reaktor
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgFnPc4d1	jaderná
elektrárny	elektrárna	k1gFnPc4	elektrárna
v	v	k7c6	v
USA	USA	kA	USA
i	i	k9	i
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
vlastní	vlastní	k2eAgInPc1d1	vlastní
jaderné	jaderný	k2eAgInPc1d1	jaderný
pohony	pohon	k1gInPc1	pohon
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
nebo	nebo	k8xC	nebo
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
pohon	pohon	k1gInSc1	pohon
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
zejména	zejména	k9	zejména
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
ponorkách	ponorka	k1gFnPc6	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
jaderného	jaderný	k2eAgInSc2d1	jaderný
pohonu	pohon	k1gInSc2	pohon
také	také	k9	také
na	na	k7c6	na
letadlových	letadlový	k2eAgFnPc6d1	letadlová
lodích	loď	k1gFnPc6	loď
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
raketových	raketový	k2eAgInPc6d1	raketový
křižnících	křižník	k1gInPc6	křižník
(	(	kIx(	(
<g/>
RUS	Rus	k1gMnSc1	Rus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediné	jediný	k2eAgInPc1d1	jediný
jaderné	jaderný	k2eAgInPc1d1	jaderný
pohony	pohon	k1gInPc1	pohon
pro	pro	k7c4	pro
mírové	mírový	k2eAgInPc4d1	mírový
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgInP	prosadit
jaderné	jaderný	k2eAgInPc1d1	jaderný
ledoborce	ledoborec	k1gInPc1	ledoborec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
také	také	k9	také
tři	tři	k4xCgFnPc1	tři
nákladní	nákladní	k2eAgFnPc1d1	nákladní
lodě	loď	k1gFnPc1	loď
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
odstaveny	odstaven	k2eAgFnPc1d1	odstavena
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přestavěny	přestavět	k5eAaPmNgFnP	přestavět
na	na	k7c4	na
konvenční	konvenční	k2eAgInSc4d1	konvenční
spalovací	spalovací	k2eAgInSc4d1	spalovací
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Jaderná	jaderný	k2eAgNnPc1d1	jaderné
letadla	letadlo	k1gNnPc1	letadlo
ani	ani	k8xC	ani
tanky	tank	k1gInPc1	tank
se	se	k3xPyFc4	se
neprosadily	prosadit	k5eNaPmAgInP	prosadit
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
problému	problém	k1gInSc2	problém
odstínění	odstínění	k1gNnSc2	odstínění
posádky	posádka	k1gFnSc2	posádka
od	od	k7c2	od
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Jaderná	jaderný	k2eAgNnPc1d1	jaderné
letadla	letadlo	k1gNnPc1	letadlo
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
jaderného	jaderný	k2eAgInSc2d1	jaderný
tanku	tank	k1gInSc2	tank
skočil	skočit	k5eAaPmAgMnS	skočit
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
makety	maketa	k1gFnSc2	maketa
<g/>
.	.	kIx.	.
</s>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
pohon	pohon	k1gInSc1	pohon
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
využilo	využít	k5eAaPmAgNnS	využít
v	v	k7c6	v
necelých	celý	k2eNgNnPc6d1	necelé
30	[number]	k4	30
kosmických	kosmický	k2eAgFnPc6d1	kosmická
sondách	sonda	k1gFnPc6	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zdroj	zdroj	k1gInSc1	zdroj
vždy	vždy	k6eAd1	vždy
pracoval	pracovat	k5eAaImAgInS	pracovat
naprosto	naprosto	k6eAd1	naprosto
spolehlivě	spolehlivě	k6eAd1	spolehlivě
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
radioizotopového	radioizotopový	k2eAgInSc2d1	radioizotopový
pohonu	pohon	k1gInSc2	pohon
hojně	hojně	k6eAd1	hojně
využívat	využívat	k5eAaImF	využívat
k	k	k7c3	k
cestám	cesta	k1gFnPc3	cesta
do	do	k7c2	do
meziplanetárního	meziplanetární	k2eAgInSc2d1	meziplanetární
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgFnPc4	tento
sondy	sonda	k1gFnPc4	sonda
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pohonů	pohon	k1gInPc2	pohon
využívajících	využívající	k2eAgInPc2d1	využívající
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
pracují	pracovat	k5eAaImIp3nP	pracovat
spolehlivě	spolehlivě	k6eAd1	spolehlivě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technická	technický	k2eAgNnPc1d1	technické
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
pohonu	pohon	k1gInSc2	pohon
lze	lze	k6eAd1	lze
teoreticky	teoreticky	k6eAd1	teoreticky
využít	využít	k5eAaPmF	využít
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
zdrojů	zdroj	k1gInPc2	zdroj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
výrazně	výrazně	k6eAd1	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
využití	využití	k1gNnSc4	využití
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
ze	z	k7c2	z
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
probíhající	probíhající	k2eAgFnSc2d1	probíhající
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
přeměny	přeměna	k1gFnSc2	přeměna
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
přeměně	přeměna	k1gFnSc6	přeměna
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
pomocí	pomoc	k1gFnPc2	pomoc
radioizotopový	radioizotopový	k2eAgInSc1d1	radioizotopový
termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
generátor	generátor	k1gInSc1	generátor
(	(	kIx(	(
<g/>
RTG	RTG	kA	RTG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
využívající	využívající	k2eAgInSc4d1	využívající
štěpnou	štěpný	k2eAgFnSc4d1	štěpná
reakci	reakce	k1gFnSc4	reakce
z	z	k7c2	z
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
efektivní	efektivní	k2eAgInSc1d1	efektivní
pohon	pohon	k1gInSc1	pohon
<g/>
,	,	kIx,	,
dovolující	dovolující	k2eAgInSc1d1	dovolující
plavidlům	plavidlo	k1gNnPc3	plavidlo
plout	plout	k5eAaImF	plout
téměř	téměř	k6eAd1	téměř
kamkoliv	kamkoliv	k6eAd1	kamkoliv
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
doplňování	doplňování	k1gNnSc1	doplňování
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
rychlost	rychlost	k1gFnSc1	rychlost
většiny	většina	k1gFnSc2	většina
konvenčních	konvenční	k2eAgInPc2d1	konvenční
pohonů	pohon	k1gInPc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgInPc1d1	jaderný
pohony	pohon	k1gInPc1	pohon
využívají	využívat	k5eAaImIp3nP	využívat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
díky	díky	k7c3	díky
limitovanému	limitovaný	k2eAgInSc3d1	limitovaný
prostoru	prostor	k1gInSc3	prostor
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
tlakovodní	tlakovodní	k2eAgInPc1d1	tlakovodní
reaktory	reaktor	k1gInPc1	reaktor
(	(	kIx(	(
<g/>
PWR	PWR	kA	PWR
<g/>
/	/	kIx~	/
<g/>
VVER	VVER	kA	VVER
<g/>
)	)	kIx)	)
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hustotou	hustota	k1gFnSc7	hustota
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgFnP	přidat
také	také	k9	také
reaktory	reaktor	k1gInPc4	reaktor
chlazené	chlazený	k2eAgInPc4d1	chlazený
tekutým	tekutý	k2eAgInSc7d1	tekutý
kovem	kov	k1gInSc7	kov
Pb-Bi	Pb-B	k1gFnSc2	Pb-B
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
energie	energie	k1gFnSc1	energie
ze	z	k7c2	z
štěpení	štěpení	k1gNnSc2	štěpení
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tepla	teplo	k1gNnSc2	teplo
pak	pak	k6eAd1	pak
předává	předávat	k5eAaImIp3nS	předávat
energii	energie	k1gFnSc4	energie
do	do	k7c2	do
chladiva	chladivo	k1gNnSc2	chladivo
-	-	kIx~	-
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parogenerátoru	parogenerátor	k1gInSc6	parogenerátor
je	být	k5eAaImIp3nS	být
teplo	teplo	k1gNnSc1	teplo
dále	daleko	k6eAd2	daleko
předáváno	předávat	k5eAaImNgNnS	předávat
do	do	k7c2	do
sekundárního	sekundární	k2eAgInSc2d1	sekundární
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzniká	vznikat	k5eAaImIp3nS	vznikat
pára	pára	k1gFnSc1	pára
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
ta	ten	k3xDgNnPc1	ten
pohání	pohánět	k5eAaImIp3nP	pohánět
turbíny	turbína	k1gFnPc4	turbína
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
pohon	pohon	k1gInSc4	pohon
plavidel	plavidlo	k1gNnPc2	plavidlo
a	a	k8xC	a
také	také	k9	také
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
reaktory	reaktor	k1gInPc4	reaktor
plavidel	plavidlo	k1gNnPc2	plavidlo
s	s	k7c7	s
jaderným	jaderný	k2eAgInSc7d1	jaderný
pohonem	pohon	k1gInSc7	pohon
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
izotop	izotop	k1gInSc1	izotop
uranu	uran	k1gInSc2	uran
U	u	k7c2	u
<g/>
235	[number]	k4	235
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
u	u	k7c2	u
komerčních	komerční	k2eAgInPc2d1	komerční
reaktorů	reaktor	k1gInPc2	reaktor
vyrábějících	vyrábějící	k2eAgFnPc2d1	vyrábějící
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jaderných	jaderný	k2eAgNnPc6d1	jaderné
plavidlech	plavidlo	k1gNnPc6	plavidlo
má	mít	k5eAaImIp3nS	mít
uran	uran	k1gInSc4	uran
vyšší	vysoký	k2eAgNnSc1d2	vyšší
obohacení	obohacení	k1gNnSc1	obohacení
U	u	k7c2	u
<g/>
235	[number]	k4	235
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
Jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
využívající	využívající	k2eAgInSc4d1	využívající
radioaktivní	radioaktivní	k2eAgFnSc4d1	radioaktivní
přeměnu	přeměna	k1gFnSc4	přeměna
je	být	k5eAaImIp3nS	být
využit	využit	k2eAgInSc1d1	využit
zejména	zejména	k9	zejména
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nelze	lze	k6eNd1	lze
využít	využít	k5eAaPmF	využít
energie	energie	k1gFnPc4	energie
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
sondy	sonda	k1gFnPc4	sonda
velmi	velmi	k6eAd1	velmi
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
odstíněné	odstíněný	k2eAgFnSc6d1	odstíněná
straně	strana	k1gFnSc6	strana
planety	planeta	k1gFnSc2	planeta
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
radioizotopový	radioizotopový	k2eAgInSc1d1	radioizotopový
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
získaný	získaný	k2eAgInSc4d1	získaný
pomocí	pomocí	k7c2	pomocí
termočlánku	termočlánek	k1gInSc2	termočlánek
(	(	kIx(	(
<g/>
využitím	využití	k1gNnSc7	využití
Seebeckova	Seebeckov	k1gInSc2	Seebeckov
jevu	jev	k1gInSc2	jev
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
pilotované	pilotovaný	k2eAgFnPc4d1	pilotovaná
i	i	k8xC	i
bezpilotní	bezpilotní	k2eAgFnPc4d1	bezpilotní
vesmírné	vesmírný	k2eAgFnPc4d1	vesmírná
sondy	sonda	k1gFnPc4	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Radioizotopový	radioizotopový	k2eAgInSc1d1	radioizotopový
zdroj	zdroj	k1gInSc1	zdroj
využívá	využívat	k5eAaPmIp3nS	využívat
</s>
</p>
<p>
<s>
rozpadu	rozpad	k1gInSc3	rozpad
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
radioizotopů	radioizotop	k1gInPc2	radioizotop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vhodný	vhodný	k2eAgInSc4d1	vhodný
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
</s>
</p>
<p>
<s>
výkonu	výkon	k1gInSc3	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
průmysl	průmysl	k1gInSc4	průmysl
využívá	využívat	k5eAaImIp3nS	využívat
izotop	izotop	k1gInSc1	izotop
Pu	Pu	k1gFnSc2	Pu
<g/>
238	[number]	k4	238
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
výhodnému	výhodný	k2eAgInSc3d1	výhodný
poločasu	poločas	k1gInSc3	poločas
přeměny	přeměna	k1gFnSc2	přeměna
87,7	[number]	k4	87,7
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plavidla	plavidlo	k1gNnPc4	plavidlo
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
vybavit	vybavit	k5eAaPmF	vybavit
plavidla	plavidlo	k1gNnSc2	plavidlo
jaderným	jaderný	k2eAgInSc7d1	jaderný
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
její	její	k3xOp3gFnSc1	její
praktická	praktický	k2eAgFnSc1d1	praktická
realizace	realizace	k1gFnSc1	realizace
byla	být	k5eAaImAgFnS	být
možná	možná	k9	možná
až	až	k9	až
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgInPc1d1	jaderný
reaktory	reaktor	k1gInPc1	reaktor
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
vojenských	vojenský	k2eAgFnPc2d1	vojenská
ponorek	ponorka	k1gFnPc2	ponorka
a	a	k8xC	a
hladinových	hladinový	k2eAgFnPc2d1	hladinová
lodí	loď	k1gFnPc2	loď
byly	být	k5eAaImAgFnP	být
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
technickým	technický	k2eAgInSc7d1	technický
úspěchem	úspěch	k1gInSc7	úspěch
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnPc1	první
dostaly	dostat	k5eAaPmAgFnP	dostat
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
ponorky	ponorka	k1gFnSc2	ponorka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
kategorii	kategorie	k1gFnSc4	kategorie
plavidel	plavidlo	k1gNnPc2	plavidlo
obrovská	obrovský	k2eAgFnSc1d1	obrovská
síla	síla	k1gFnSc1	síla
získávaná	získávaný	k2eAgFnSc1d1	získávaná
štěpením	štěpení	k1gNnSc7	štěpení
atomů	atom	k1gInPc2	atom
znamená	znamenat	k5eAaImIp3nS	znamenat
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgInSc4d1	zásadní
zvrat	zvrat	k1gInSc4	zvrat
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
,	,	kIx,	,
výkonech	výkon	k1gInPc6	výkon
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnSc3d1	vojenská
taktice	taktika	k1gFnSc3	taktika
i	i	k8xC	i
strategii	strategie	k1gFnSc3	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
všech	všecek	k3xTgNnPc2	všecek
plavidel	plavidlo	k1gNnPc2	plavidlo
byly	být	k5eAaImAgFnP	být
instalovány	instalovat	k5eAaBmNgFnP	instalovat
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
malé	malý	k2eAgInPc4d1	malý
tlakovodní	tlakovodní	k2eAgInPc4d1	tlakovodní
reaktory	reaktor	k1gInPc4	reaktor
<g/>
,	,	kIx,	,
v	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
reaktory	reaktor	k1gInPc1	reaktor
chlazené	chlazený	k2eAgInPc1d1	chlazený
tekutými	tekutý	k2eAgInPc7d1	tekutý
kovy	kov	k1gInPc7	kov
Pb-Bi	Pb-B	k1gFnSc2	Pb-B
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
ponorky	ponorka	k1gFnPc1	ponorka
===	===	k?	===
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
ponorky	ponorka	k1gFnPc1	ponorka
získaly	získat	k5eAaPmAgFnP	získat
díky	díky	k7c3	díky
obrovské	obrovský	k2eAgFnSc3d1	obrovská
energii	energie	k1gFnSc3	energie
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
možnost	možnost	k1gFnSc4	možnost
vydržet	vydržet	k5eAaPmF	vydržet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
až	až	k9	až
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
vynořovat	vynořovat	k5eAaImF	vynořovat
kvůli	kvůli	k7c3	kvůli
dobití	dobití	k1gNnSc3	dobití
baterií	baterie	k1gFnPc2	baterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
dodává	dodávat	k5eAaImIp3nS	dodávat
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgFnPc1d1	klasická
diesel-elektrické	diesellektrický	k2eAgFnPc1d1	diesel-elektrická
ponorky	ponorka	k1gFnPc1	ponorka
musí	muset	k5eAaImIp3nP	muset
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
palubě	paluba	k1gFnSc6	paluba
nést	nést	k5eAaImF	nést
těžké	těžký	k2eAgInPc4d1	těžký
akumulátory	akumulátor	k1gInPc4	akumulátor
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgInSc4d2	menší
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
akční	akční	k2eAgInSc4d1	akční
rádius	rádius	k1gInSc4	rádius
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInSc4	tisíc
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
vysoké	vysoký	k2eAgFnSc2d1	vysoká
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
umožnění	umožnění	k1gNnSc1	umožnění
velmi	velmi	k6eAd1	velmi
rychlé	rychlý	k2eAgFnSc2d1	rychlá
plavby	plavba	k1gFnSc2	plavba
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
45	[number]	k4	45
uzlů	uzel	k1gInPc2	uzel
<g/>
,	,	kIx,	,
cca	cca	kA	cca
85	[number]	k4	85
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
ponorky	ponorka	k1gFnPc1	ponorka
hrály	hrát	k5eAaImAgFnP	hrát
největší	veliký	k2eAgInSc4d3	veliký
prim	prim	k1gInSc4	prim
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
USA	USA	kA	USA
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
sloužily	sloužit	k5eAaImAgInP	sloužit
k	k	k7c3	k
odstrašení	odstrašení	k1gNnSc3	odstrašení
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
jadernému	jaderný	k2eAgInSc3d1	jaderný
arsenálu	arsenál	k1gInSc3	arsenál
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
začali	začít	k5eAaPmAgMnP	začít
realizovat	realizovat	k5eAaBmF	realizovat
projekt	projekt	k1gInSc4	projekt
jaderné	jaderný	k2eAgFnSc2d1	jaderná
ponorky	ponorka	k1gFnSc2	ponorka
Američané	Američan	k1gMnPc1	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
projektu	projekt	k1gInSc2	projekt
admirál	admirál	k1gMnSc1	admirál
Hyman	Hyman	k1gMnSc1	Hyman
G.	G.	kA	G.
Rickover	Rickover	k1gMnSc1	Rickover
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
plavit	plavit	k5eAaImF	plavit
první	první	k4xOgFnSc1	první
ponorka	ponorka	k1gFnSc1	ponorka
USS	USS	kA	USS
Nautilus	nautilus	k1gInSc4	nautilus
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
ponorka	ponorka	k1gFnSc1	ponorka
byla	být	k5eAaImAgFnS	být
vzor	vzor	k1gInSc4	vzor
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
pozdější	pozdní	k2eAgFnPc4d2	pozdější
jaderné	jaderný	k2eAgFnPc4d1	jaderná
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
plavidlo	plavidlo	k1gNnSc1	plavidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
geografického	geografický	k2eAgInSc2d1	geografický
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
pod	pod	k7c7	pod
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
první	první	k4xOgFnSc4	první
jadernou	jaderný	k2eAgFnSc4d1	jaderná
ponorku	ponorka	k1gFnSc4	ponorka
s	s	k7c7	s
názvem	název	k1gInSc7	název
K-3	K-3	k1gFnSc2	K-3
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
ponorky	ponorka	k1gFnPc1	ponorka
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgNnSc1d1	drahé
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
mohou	moct	k5eAaImIp3nP	moct
dovolit	dovolit	k5eAaPmF	dovolit
jen	jen	k9	jen
největší	veliký	k2eAgFnPc1d3	veliký
mocnosti	mocnost	k1gFnPc1	mocnost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
SSSR	SSSR	kA	SSSR
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
na	na	k7c4	na
450	[number]	k4	450
jaderných	jaderný	k2eAgFnPc2d1	jaderná
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
dvakrát	dvakrát	k6eAd1	dvakrát
tolik	tolik	k4xDc1	tolik
jako	jako	k8xS	jako
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hladinová	hladinový	k2eAgNnPc1d1	hladinové
plavidla	plavidlo	k1gNnPc1	plavidlo
===	===	k?	===
</s>
</p>
<p>
<s>
Hladinová	hladinový	k2eAgNnPc1d1	hladinové
plavidla	plavidlo	k1gNnPc1	plavidlo
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
-	-	kIx~	-
letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jaderné	jaderný	k2eAgInPc1d1	jaderný
ledoborce	ledoborec	k1gInPc1	ledoborec
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
/	/	kIx~	/
<g/>
bývalé	bývalý	k2eAgFnSc2d1	bývalá
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
raketové	raketový	k2eAgInPc1d1	raketový
křižníky	křižník	k1gInPc1	křižník
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
/	/	kIx~	/
<g/>
bývalé	bývalý	k2eAgFnSc2d1	bývalá
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
nákladní	nákladní	k2eAgFnSc2d1	nákladní
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
====	====	k?	====
</s>
</p>
<p>
<s>
Používané	používaný	k2eAgInPc1d1	používaný
jaderné	jaderný	k2eAgInPc1d1	jaderný
reaktory	reaktor	k1gInPc1	reaktor
jsou	být	k5eAaImIp3nP	být
konstrukčně	konstrukčně	k6eAd1	konstrukčně
takřka	takřka	k6eAd1	takřka
shodné	shodný	k2eAgInPc4d1	shodný
s	s	k7c7	s
reaktory	reaktor	k1gInPc7	reaktor
na	na	k7c6	na
jaderných	jaderný	k2eAgFnPc6d1	jaderná
ponorkách	ponorka	k1gFnPc6	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vyšších	vysoký	k2eAgInPc2d2	vyšší
výkonů	výkon	k1gInPc2	výkon
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
reaktory	reaktor	k1gInPc1	reaktor
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
oproti	oproti	k7c3	oproti
ponorkám	ponorka	k1gFnPc3	ponorka
větší	veliký	k2eAgFnSc1d2	veliký
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
zhoršená	zhoršený	k2eAgFnSc1d1	zhoršená
manévrovatelnost	manévrovatelnost	k1gFnSc1	manévrovatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
vyplula	vyplout	k5eAaPmAgFnS	vyplout
na	na	k7c6	na
moře	mora	k1gFnSc6	mora
první	první	k4xOgFnSc1	první
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
názvem	název	k1gInSc7	název
USS	USS	kA	USS
Enterprise	Enterpris	k1gInSc6	Enterpris
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
pohon	pohon	k1gInSc4	pohon
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
osm	osm	k4xCc1	osm
reaktorů	reaktor	k1gInPc2	reaktor
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
tepelném	tepelný	k2eAgInSc6d1	tepelný
výkonu	výkon	k1gInSc6	výkon
cca	cca	kA	cca
210	[number]	k4	210
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Raketové	raketový	k2eAgInPc1d1	raketový
křižníky	křižník	k1gInPc1	křižník
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velké	velký	k2eAgFnPc4d1	velká
válečné	válečný	k2eAgFnPc4d1	válečná
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
monitorování	monitorování	k1gNnSc4	monitorování
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
jak	jak	k8xC	jak
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
úkoly	úkol	k1gInPc1	úkol
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
námořních	námořní	k2eAgMnPc2d1	námořní
konvojů	konvoj	k1gInPc2	konvoj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pro	pro	k7c4	pro
útočné	útočný	k2eAgFnPc4d1	útočná
operace	operace	k1gFnPc4	operace
a	a	k8xC	a
ostřelování	ostřelování	k1gNnPc4	ostřelování
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
raketovému	raketový	k2eAgInSc3d1	raketový
arsenálu	arsenál	k1gInSc3	arsenál
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
jako	jako	k9	jako
průzkumná	průzkumný	k2eAgNnPc1d1	průzkumné
plavidla	plavidlo	k1gNnPc1	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
válečné	válečný	k2eAgNnSc4d1	válečné
hladinové	hladinový	k2eAgNnSc4d1	hladinové
palvidlo	palvidlo	k1gNnSc4	palvidlo
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
byl	být	k5eAaImAgInS	být
právě	právě	k9	právě
raketový	raketový	k2eAgInSc1d1	raketový
křižník	křižník	k1gInSc1	křižník
USS	USS	kA	USS
Long	Long	k1gInSc1	Long
Beach	Beach	k1gInSc1	Beach
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
loděnici	loděnice	k1gFnSc6	loděnice
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
Leningradu	Leningrad	k1gInSc2	Leningrad
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
1144.2	[number]	k4	1144.2
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
postaveny	postavit	k5eAaPmNgInP	postavit
celkem	celek	k1gInSc7	celek
4	[number]	k4	4
křižníky	křižník	k1gInPc4	křižník
s	s	k7c7	s
jaderným	jaderný	k2eAgInSc7d1	jaderný
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
už	už	k9	už
jen	jen	k9	jen
křížník	křížník	k1gMnSc1	křížník
Pjotr	Pjotr	k1gMnSc1	Pjotr
Velikij	Velikij	k1gMnSc1	Velikij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Jaderný	jaderný	k2eAgInSc1d1	jaderný
ledoborec	ledoborec	k1gInSc1	ledoborec
====	====	k?	====
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgInPc1d1	jaderný
ledoborce	ledoborec	k1gInPc1	ledoborec
jsou	být	k5eAaImIp3nP	být
jedinou	jediný	k2eAgFnSc4d1	jediná
komerčně	komerčně	k6eAd1	komerčně
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
aplikací	aplikace	k1gFnSc7	aplikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
mírové	mírový	k2eAgInPc4d1	mírový
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Využívají	využívat	k5eAaImIp3nP	využívat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
plavbu	plavba	k1gFnSc4	plavba
po	po	k7c6	po
zamrzlých	zamrzlý	k2eAgFnPc6d1	zamrzlá
vodních	vodní	k2eAgFnPc6d1	vodní
plochách	plocha	k1gFnPc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
velké	velký	k2eAgFnSc2d1	velká
nákladní	nákladní	k2eAgFnSc2d1	nákladní
lodě	loď	k1gFnSc2	loď
určené	určený	k2eAgFnSc2d1	určená
k	k	k7c3	k
prorážení	prorážení	k1gNnSc3	prorážení
cest	cesta	k1gFnPc2	cesta
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
lodě	loď	k1gFnPc4	loď
nebo	nebo	k8xC	nebo
k	k	k7c3	k
vyprošťování	vyprošťování	k1gNnSc3	vyprošťování
lodí	loď	k1gFnPc2	loď
uvízlých	uvízlý	k2eAgFnPc2d1	uvízlá
v	v	k7c6	v
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
jaderná	jaderný	k2eAgNnPc1d1	jaderné
plavidla	plavidlo	k1gNnPc1	plavidlo
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
obrovské	obrovský	k2eAgFnSc3d1	obrovská
síle	síla	k1gFnSc3	síla
získané	získaný	k2eAgFnSc3d1	získaná
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
prorazit	prorazit	k5eAaPmF	prorazit
led	led	k1gInSc4	led
až	až	k9	až
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
výhodou	výhoda	k1gFnSc7	výhoda
oproti	oproti	k7c3	oproti
konvenčním	konvenční	k2eAgFnPc3d1	konvenční
(	(	kIx(	(
<g/>
dieselovým	dieselový	k2eAgMnPc3d1	dieselový
<g/>
)	)	kIx)	)
ledoborcům	ledoborec	k1gMnPc3	ledoborec
opět	opět	k6eAd1	opět
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
plavit	plavit	k5eAaImF	plavit
bez	bez	k7c2	bez
doplňování	doplňování	k1gNnSc2	doplňování
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
také	také	k9	také
nevznikají	vznikat	k5eNaImIp3nP	vznikat
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
podmínkách	podmínka	k1gFnPc6	podmínka
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zamrzáním	zamrzání	k1gNnSc7	zamrzání
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgInPc1d1	existující
ledoborce	ledoborec	k1gInPc1	ledoborec
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
v	v	k7c4	v
Rusku	Ruska	k1gFnSc4	Ruska
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bývalém	bývalý	k2eAgInSc6d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
prvním	první	k4xOgInSc7	první
ledoborcem	ledoborec	k1gInSc7	ledoborec
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k8xC	i
první	první	k4xOgFnSc7	první
hladinovou	hladinový	k2eAgFnSc7d1	hladinová
lodí	loď	k1gFnSc7	loď
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ledoborec	ledoborec	k1gMnSc1	ledoborec
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nákladní	nákladní	k2eAgFnPc1d1	nákladní
lodě	loď	k1gFnPc1	loď
====	====	k?	====
</s>
</p>
<p>
<s>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
nákladní	nákladní	k2eAgFnPc1d1	nákladní
lodě	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
demonstrace	demonstrace	k1gFnSc1	demonstrace
mírového	mírový	k2eAgNnSc2d1	Mírové
užití	užití	k1gNnSc2	užití
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Atoms	Atomsa	k1gFnPc2	Atomsa
for	forum	k1gNnPc2	forum
Peace	Peace	k1gMnSc1	Peace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
obchodní	obchodní	k2eAgFnSc1d1	obchodní
loď	loď	k1gFnSc1	loď
NS	NS	kA	NS
Savannah	Savannah	k1gInSc1	Savannah
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc4	loď
poháněl	pohánět	k5eAaImAgInS	pohánět
tlakovodní	tlakovodní	k2eAgInSc1d1	tlakovodní
reaktor	reaktor	k1gInSc1	reaktor
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
pojmout	pojmout	k5eAaPmF	pojmout
8500	[number]	k4	8500
t	t	k?	t
nákladu	náklad	k1gInSc2	náklad
nebo	nebo	k8xC	nebo
60	[number]	k4	60
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
luxusním	luxusní	k2eAgInSc6d1	luxusní
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
spíše	spíše	k9	spíše
luxusní	luxusní	k2eAgFnSc4d1	luxusní
jachtu	jachta	k1gFnSc4	jachta
než	než	k8xS	než
nákladní	nákladní	k2eAgFnSc4d1	nákladní
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
provozu	provoz	k1gInSc2	provoz
urazila	urazit	k5eAaPmAgFnS	urazit
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
skončil	skončit	k5eAaPmAgInS	skončit
její	její	k3xOp3gInSc4	její
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gInPc4	její
roční	roční	k2eAgInPc4d1	roční
provozní	provozní	k2eAgInPc4d1	provozní
náklady	náklad	k1gInPc4	náklad
vycházely	vycházet	k5eAaImAgFnP	vycházet
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
srovnatelné	srovnatelný	k2eAgFnSc2d1	srovnatelná
lodi	loď	k1gFnSc2	loď
s	s	k7c7	s
klasickým	klasický	k2eAgInSc7d1	klasický
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
chýlil	chýlit	k5eAaImAgInS	chýlit
konec	konec	k1gInSc1	konec
provozu	provoz	k1gInSc2	provoz
NS	NS	kA	NS
Savannah	Savannah	k1gMnSc1	Savannah
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
civilní	civilní	k2eAgFnSc4d1	civilní
loď	loď	k1gFnSc4	loď
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
s	s	k7c7	s
názvem	název	k1gInSc7	název
NS	NS	kA	NS
Otto	Otto	k1gMnSc1	Otto
Hahn	Hahn	k1gMnSc1	Hahn
a	a	k8xC	a
uvezla	uvézt	k5eAaPmAgFnS	uvézt
až	až	k9	až
14	[number]	k4	14
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
loď	loď	k1gFnSc1	loď
sloužila	sloužit	k5eAaImAgFnS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
plavidel	plavidlo	k1gNnPc2	plavidlo
právě	právě	k9	právě
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
devíti	devět	k4xCc6	devět
letech	let	k1gInPc6	let
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
urazila	urazit	k5eAaPmAgFnS	urazit
650	[number]	k4	650
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
taktéž	taktéž	k?	taktéž
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
odstavena	odstaven	k2eAgFnSc1d1	odstavena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
její	její	k3xOp3gInSc1	její
osud	osud	k1gInSc1	osud
byl	být	k5eAaImAgInS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
než	než	k8xS	než
u	u	k7c2	u
americké	americký	k2eAgFnSc2d1	americká
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
NS	NS	kA	NS
Otto	Otto	k1gMnSc1	Otto
Hahn	Hahn	k1gMnSc1	Hahn
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
konvenční	konvenční	k2eAgInSc4d1	konvenční
spalovací	spalovací	k2eAgInSc4d1	spalovací
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
lodi	loď	k1gFnSc2	loď
původně	původně	k6eAd1	původně
počítáno	počítat	k5eAaImNgNnS	počítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
civilní	civilní	k2eAgFnSc7d1	civilní
jadernou	jaderný	k2eAgFnSc7d1	jaderná
lodí	loď	k1gFnSc7	loď
je	být	k5eAaImIp3nS	být
nákladní	nákladní	k2eAgFnSc1d1	nákladní
loď	loď	k1gFnSc1	loď
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
NS	NS	kA	NS
Mutsu	Mutsa	k1gFnSc4	Mutsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
komerční	komerční	k2eAgInSc4d1	komerční
náklad	náklad	k1gInSc4	náklad
nevezla	vézt	k5eNaImAgFnS	vézt
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
experimentální	experimentální	k2eAgFnSc1d1	experimentální
<g/>
.	.	kIx.	.
</s>
<s>
Fungovala	fungovat	k5eAaImAgFnS	fungovat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1972	[number]	k4	1972
a	a	k8xC	a
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
provoz	provoz	k1gInSc4	provoz
komplikovaly	komplikovat	k5eAaBmAgFnP	komplikovat
zpočátku	zpočátku	k6eAd1	zpočátku
obavy	obava	k1gFnPc1	obava
rybářů	rybář	k1gMnPc2	rybář
v	v	k7c6	v
domovském	domovský	k2eAgInSc6d1	domovský
přístavu	přístav	k1gInSc6	přístav
Ohminato	Ohminat	k2eAgNnSc1d1	Ohminat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gInSc4	on
musela	muset	k5eAaImAgFnS	muset
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
najít	najít	k5eAaPmF	najít
si	se	k3xPyFc3	se
přístav	přístav	k1gInSc4	přístav
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
lodi	loď	k1gFnSc2	loď
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
jaderný	jaderný	k2eAgInSc1d1	jaderný
pohon	pohon	k1gInSc1	pohon
nahrazen	nahradit	k5eAaPmNgInS	nahradit
konvenčním	konvenční	k2eAgInSc7d1	konvenční
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jaderná	jaderný	k2eAgNnPc1d1	jaderné
letadla	letadlo	k1gNnPc1	letadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
byly	být	k5eAaImAgFnP	být
vyvíjeny	vyvíjet	k5eAaImNgFnP	vyvíjet
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
jaderné	jaderný	k2eAgFnSc2d1	jaderná
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
ruští	ruský	k2eAgMnPc1d1	ruský
a	a	k8xC	a
američtí	americký	k2eAgMnPc1d1	americký
inženýři	inženýr	k1gMnPc1	inženýr
dostali	dostat	k5eAaPmAgMnP	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyprojektovat	vyprojektovat	k5eAaPmF	vyprojektovat
letadlo	letadlo	k1gNnSc4	letadlo
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
s	s	k7c7	s
takřka	takřka	k6eAd1	takřka
neomezeným	omezený	k2eNgInSc7d1	neomezený
doletem	dolet	k1gInSc7	dolet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
operovat	operovat	k5eAaImF	operovat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
celé	celý	k2eAgInPc4d1	celý
týdny	týden	k1gInPc4	týden
bez	bez	k7c2	bez
doplňování	doplňování	k1gNnSc2	doplňování
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americký	americký	k2eAgInSc1d1	americký
projekt	projekt	k1gInSc1	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955-1957	[number]	k4	1955-1957
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
testován	testován	k2eAgMnSc1d1	testován
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
bombardér	bombardér	k1gMnSc1	bombardér
Convair	Convair	k1gMnSc1	Convair
B-36	B-36	k1gMnSc1	B-36
Peacekeeper	Peacekeeper	k1gMnSc1	Peacekeeper
bomber	bomber	k1gMnSc1	bomber
(	(	kIx(	(
<g/>
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
)	)	kIx)	)
využívající	využívající	k2eAgInSc4d1	využívající
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Jaderný	jaderný	k2eAgInSc1d1	jaderný
reaktor	reaktor	k1gInSc1	reaktor
byl	být	k5eAaImAgInS	být
chlazen	chladit	k5eAaImNgInS	chladit
tekutým	tekutý	k2eAgInSc7d1	tekutý
sodíkem	sodík	k1gInSc7	sodík
a	a	k8xC	a
disponoval	disponovat	k5eAaBmAgInS	disponovat
tepelným	tepelný	k2eAgInSc7d1	tepelný
výkonem	výkon	k1gInSc7	výkon
1	[number]	k4	1
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
start	start	k1gInSc4	start
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
používal	používat	k5eAaImAgMnS	používat
letoun	letoun	k1gInSc4	letoun
klasické	klasický	k2eAgInPc4d1	klasický
motory	motor	k1gInPc4	motor
na	na	k7c4	na
chemické	chemický	k2eAgNnSc4d1	chemické
palivo	palivo	k1gNnSc4	palivo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
případné	případný	k2eAgFnSc6d1	případná
kolizi	kolize	k1gFnSc6	kolize
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
</s>
</p>
<p>
<s>
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pětičlenná	pětičlenný	k2eAgFnSc1d1	pětičlenná
posádka	posádka	k1gFnSc1	posádka
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
reaktoru	reaktor	k1gInSc2	reaktor
odstíněna	odstíněn	k2eAgNnPc4d1	odstíněn
dvanáctitunovým	dvanáctitunový	k2eAgInSc7d1	dvanáctitunový
olovo-pryžovým	olovoryžový	k2eAgInSc7d1	olovo-pryžový
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
letoun	letoun	k1gInSc1	letoun
provedl	provést	k5eAaPmAgInS	provést
47	[number]	k4	47
zkušebních	zkušební	k2eAgInPc2d1	zkušební
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
nebyl	být	k5eNaImAgInS	být
k	k	k7c3	k
pohonu	pohon	k1gInSc3	pohon
letadla	letadlo	k1gNnSc2	letadlo
využit	využít	k5eAaPmNgInS	využít
pouze	pouze	k6eAd1	pouze
přímý	přímý	k2eAgInSc1d1	přímý
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
reaktor	reaktor	k1gInSc1	reaktor
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgFnP	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
a	a	k8xC	a
sběru	sběr	k1gInSc3	sběr
dat	datum	k1gNnPc2	datum
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
zastavení	zastavení	k1gNnSc4	zastavení
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
investovalo	investovat	k5eAaBmAgNnS	investovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
problém	problém	k1gInSc1	problém
s	s	k7c7	s
odstíněním	odstínění	k1gNnSc7	odstínění
posádky	posádka	k1gFnSc2	posádka
od	od	k7c2	od
reaktoru	reaktor	k1gInSc2	reaktor
nebo	nebo	k8xC	nebo
strach	strach	k1gInSc4	strach
z	z	k7c2	z
pádu	pád	k1gInSc2	pád
letadla	letadlo	k1gNnSc2	letadlo
na	na	k7c4	na
obydlené	obydlený	k2eAgNnSc4d1	obydlené
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
převážily	převážit	k5eAaPmAgFnP	převážit
nad	nad	k7c7	nad
výhodou	výhoda	k1gFnSc7	výhoda
neomezené	omezený	k2eNgFnSc2d1	neomezená
délky	délka	k1gFnSc2	délka
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
projekt	projekt	k1gInSc1	projekt
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
světa	svět	k1gInSc2	svět
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
pokusy	pokus	k1gInPc1	pokus
postavit	postavit	k5eAaPmF	postavit
letadlo	letadlo	k1gNnSc4	letadlo
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
týmy	tým	k1gInPc7	tým
jaderných	jaderný	k2eAgMnPc2d1	jaderný
inženýrů	inženýr	k1gMnPc2	inženýr
od	od	k7c2	od
</s>
</p>
<p>
<s>
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
snažily	snažit	k5eAaImAgInP	snažit
přestavět	přestavět	k5eAaPmF	přestavět
letoun	letoun	k1gInSc4	letoun
Tu-	Tu-	k1gFnSc2	Tu-
<g/>
95	[number]	k4	95
<g/>
M.	M.	kA	M.
Nový	nový	k2eAgInSc1d1	nový
letoun	letoun	k1gInSc1	letoun
pak	pak	k6eAd1	pak
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Tu-	Tu-	k1gFnSc4	Tu-
<g/>
119	[number]	k4	119
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
svým	svůj	k3xOyFgInSc7	svůj
malým	malý	k2eAgInSc7d1	malý
hrbem	hrb	k1gInSc7	hrb
na	na	k7c6	na
trupu	trup	k1gInSc6	trup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyčníval	vyčnívat	k5eAaImAgInS	vyčnívat
reaktor	reaktor	k1gInSc1	reaktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nevešel	vejít	k5eNaPmAgInS	vejít
celý	celý	k2eAgInSc1d1	celý
do	do	k7c2	do
trupu	trup	k1gInSc2	trup
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
zkušebních	zkušební	k2eAgMnPc2d1	zkušební
letů	let	k1gInPc2	let
na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zápisů	zápis	k1gInPc2	zápis
zkušebního	zkušební	k2eAgInSc2d1	zkušební
pilota	pilota	k1gFnSc1	pilota
tohoto	tento	k3xDgNnSc2	tento
letadla	letadlo	k1gNnSc2	letadlo
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
dbáno	dbán	k2eAgNnSc1d1	dbáno
na	na	k7c4	na
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
odstínění	odstínění	k1gNnSc4	odstínění
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
vypouštěn	vypouštět	k5eAaImNgInS	vypouštět
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
kontaminovaný	kontaminovaný	k2eAgInSc4d1	kontaminovaný
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
zkušebních	zkušební	k2eAgFnPc2d1	zkušební
posádek	posádka	k1gFnPc2	posádka
přežili	přežít	k5eAaPmAgMnP	přežít
jen	jen	k9	jen
3	[number]	k4	3
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Neřešitelným	řešitelný	k2eNgInSc7d1	neřešitelný
problémem	problém	k1gInSc7	problém
inženýrů	inženýr	k1gMnPc2	inženýr
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jako	jako	k9	jako
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
problém	problém	k1gInSc1	problém
s	s	k7c7	s
odstíněním	odstínění	k1gNnSc7	odstínění
posádky	posádka	k1gFnSc2	posádka
před	před	k7c7	před
radiací	radiace	k1gFnSc7	radiace
a	a	k8xC	a
přetížený	přetížený	k2eAgInSc1d1	přetížený
letoun	letoun	k1gInSc1	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
i	i	k9	i
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc4	vývoj
jaderného	jaderný	k2eAgNnSc2d1	jaderné
letadla	letadlo	k1gNnSc2	letadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
zastaven	zastavit	k5eAaPmNgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jaderné	jaderný	k2eAgInPc1d1	jaderný
tanky	tank	k1gInPc1	tank
==	==	k?	==
</s>
</p>
<p>
<s>
Zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
tank	tank	k1gInSc4	tank
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
cílem	cíl	k1gInSc7	cíl
projektů	projekt	k1gInPc2	projekt
opět	opět	k6eAd1	opět
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
největší	veliký	k2eAgFnPc4d3	veliký
mocnosti	mocnost	k1gFnPc4	mocnost
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
tak	tak	k6eAd1	tak
logicky	logicky	k6eAd1	logicky
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
vývoj	vývoj	k1gInSc4	vývoj
jaderného	jaderný	k2eAgInSc2d1	jaderný
pohonu	pohon	k1gInSc2	pohon
v	v	k7c6	v
ponorkách	ponorka	k1gFnPc6	ponorka
<g/>
,	,	kIx,	,
lodích	loď	k1gFnPc6	loď
a	a	k8xC	a
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
chtěly	chtít	k5eAaImAgInP	chtít
sestrojit	sestrojit	k5eAaPmF	sestrojit
hlavně	hlavně	k9	hlavně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
i	i	k9	i
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
díky	díky	k7c3	díky
jadernému	jaderný	k2eAgInSc3d1	jaderný
pohonu	pohon	k1gInSc3	pohon
takřka	takřka	k6eAd1	takřka
neomezeného	omezený	k2eNgInSc2d1	neomezený
akčního	akční	k2eAgInSc2d1	akční
rádiusu	rádius	k1gInSc2	rádius
<g/>
,	,	kIx,	,
vysokého	vysoký	k2eAgInSc2d1	vysoký
výkonu	výkon	k1gInSc2	výkon
a	a	k8xC	a
nepřetržité	přetržitý	k2eNgFnSc2d1	nepřetržitá
provozní	provozní	k2eAgFnSc2d1	provozní
pohotovosti	pohotovost	k1gFnSc2	pohotovost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
armáda	armáda	k1gFnSc1	armáda
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
reaktory	reaktor	k1gInPc4	reaktor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přesouvat	přesouvat	k5eAaImF	přesouvat
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k9	tak
mobilní	mobilní	k2eAgInPc1d1	mobilní
zdroje	zdroj	k1gInPc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
například	například	k6eAd1	například
mohly	moct	k5eAaImAgFnP	moct
dodávat	dodávat	k5eAaImF	dodávat
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgFnPc4d1	vojenská
základny	základna	k1gFnPc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
reaktor	reaktor	k1gInSc4	reaktor
APPR	APPR	kA	APPR
(	(	kIx(	(
<g/>
Army	Arma	k1gMnSc2	Arma
Package	Packag	k1gMnSc2	Packag
Power	Power	k1gMnSc1	Power
Reactor	Reactor	k1gMnSc1	Reactor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dodával	dodávat	k5eAaImAgMnS	dodávat
elektrický	elektrický	k2eAgInSc4d1	elektrický
výkon	výkon	k1gInSc4	výkon
2	[number]	k4	2
MW	MW	kA	MW
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gNnSc4	on
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
několik	několik	k4yIc1	několik
pojízdných	pojízdný	k2eAgFnPc2d1	pojízdná
vojenských	vojenský	k2eAgFnPc2d1	vojenská
elektráren	elektrárna	k1gFnPc2	elektrárna
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
TES-	TES-	k1gFnPc2	TES-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
přemisťovány	přemisťovat	k5eAaImNgFnP	přemisťovat
na	na	k7c6	na
prodloužených	prodloužený	k2eAgInPc6d1	prodloužený
podvozcích	podvozek	k1gInPc6	podvozek
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgMnPc4	ten
byly	být	k5eAaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
vyrábět	vyrábět	k5eAaImF	vyrábět
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
tank	tank	k1gInSc4	tank
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
postupně	postupně	k6eAd1	postupně
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
návrhy	návrh	k1gInPc4	návrh
tanků	tank	k1gInPc2	tank
poháněných	poháněný	k2eAgInPc2d1	poháněný
přímo	přímo	k6eAd1	přímo
malým	malý	k2eAgInSc7d1	malý
reaktorem	reaktor	k1gInSc7	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
studie	studie	k1gFnPc4	studie
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
TV-	TV-	k1gFnPc2	TV-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Rex	Rex	k1gFnSc1	Rex
R-	R-	k1gFnSc1	R-
<g/>
32	[number]	k4	32
<g/>
,	,	kIx,	,
TV-	TV-	k1gFnSc1	TV-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Nejdále	daleko	k6eAd3	daleko
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
maketě	maketa	k1gFnSc3	maketa
ve	v	k7c6	v
skutečné	skutečný	k2eAgFnSc6d1	skutečná
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
model	model	k1gInSc1	model
TV-	TV-	k1gFnSc2	TV-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
jaderného	jaderný	k2eAgNnSc2d1	jaderné
letadla	letadlo	k1gNnSc2	letadlo
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
takřka	takřka	k6eAd1	takřka
nemožné	možný	k2eNgNnSc1d1	nemožné
dostatečně	dostatečně	k6eAd1	dostatečně
odstínit	odstínit	k5eAaPmF	odstínit
posádku	posádka	k1gFnSc4	posádka
vozidla	vozidlo	k1gNnSc2	vozidlo
od	od	k7c2	od
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
od	od	k7c2	od
dalšího	další	k2eAgInSc2d1	další
vývoje	vývoj	k1gInSc2	vývoj
jaderného	jaderný	k2eAgInSc2d1	jaderný
tanku	tank	k1gInSc2	tank
upuštěno	upuštěn	k2eAgNnSc1d1	upuštěno
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
pohon	pohon	k1gInSc4	pohon
tak	tak	k6eAd1	tak
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jaderné	jaderný	k2eAgInPc1d1	jaderný
pohony	pohon	k1gInPc1	pohon
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kosmické	kosmický	k2eAgFnPc4d1	kosmická
sondy	sonda	k1gFnPc4	sonda
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
jaderné	jaderný	k2eAgFnSc2d1	jaderná
přeměny	přeměna	k1gFnPc1	přeměna
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
vojenských	vojenský	k2eAgFnPc6d1	vojenská
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
sondách	sonda	k1gFnPc6	sonda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
sondy	sonda	k1gFnSc2	sonda
Transit	transit	k1gInSc1	transit
4	[number]	k4	4
<g/>
a	a	k8xC	a
a	a	k8xC	a
4	[number]	k4	4
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
sondy	sonda	k1gFnSc2	sonda
Kosmos	kosmos	k1gInSc4	kosmos
84	[number]	k4	84
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1	ruská
sondy	sonda	k1gFnPc1	sonda
využívaly	využívat	k5eAaPmAgFnP	využívat
izotop	izotop	k1gInSc4	izotop
Po	po	k7c4	po
<g/>
210	[number]	k4	210
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
poločas	poločas	k1gInSc4	poločas
přeměny	přeměna	k1gFnSc2	přeměna
pouze	pouze	k6eAd1	pouze
138	[number]	k4	138
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
radioizotopů	radioizotop	k1gInPc2	radioizotop
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kosmickém	kosmický	k2eAgInSc6d1	kosmický
programu	program	k1gInSc6	program
velmi	velmi	k6eAd1	velmi
výhodné	výhodný	k2eAgNnSc1d1	výhodné
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
výhody	výhoda	k1gFnPc4	výhoda
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
životnost	životnost	k1gFnSc1	životnost
<g/>
,	,	kIx,	,
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
a	a	k8xC	a
funkční	funkční	k2eAgFnSc4d1	funkční
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
magnetickému	magnetický	k2eAgNnSc3d1	magnetické
poli	pole	k1gNnSc3	pole
<g/>
,	,	kIx,	,
radiaci	radiace	k1gFnSc3	radiace
<g/>
,	,	kIx,	,
okolní	okolní	k2eAgFnSc3d1	okolní
teplotě	teplota	k1gFnSc3	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
využito	využít	k5eAaPmNgNnS	využít
radioizotopového	radioizotopový	k2eAgInSc2d1	radioizotopový
termoelektrického	termoelektrický	k2eAgInSc2d1	termoelektrický
generátoru	generátor	k1gInSc2	generátor
v	v	k7c6	v
necelých	celý	k2eNgInPc6d1	necelý
30	[number]	k4	30
kosmických	kosmický	k2eAgFnPc6d1	kosmická
sondách	sonda	k1gFnPc6	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
radioizotopu	radioizotop	k1gInSc2	radioizotop
bychom	by	kYmCp1nP	by
našli	najít	k5eAaPmAgMnP	najít
i	i	k9	i
na	na	k7c6	na
vědeckých	vědecký	k2eAgInPc6d1	vědecký
přístrojích	přístroj	k1gInPc6	přístroj
<g/>
,	,	kIx,	,
zanechaných	zanechaný	k2eAgFnPc6d1	zanechaná
programem	program	k1gInSc7	program
Apollo	Apollo	k1gNnSc1	Apollo
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
nebo	nebo	k8xC	nebo
i	i	k9	i
na	na	k7c6	na
sondách	sonda	k1gFnPc6	sonda
pohybujících	pohybující	k2eAgMnPc2d1	pohybující
se	se	k3xPyFc4	se
na	na	k7c6	na
únikové	únikový	k2eAgFnSc6d1	úniková
dráze	dráha	k1gFnSc6	dráha
ze	z	k7c2	z
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
Voyager	Voyager	k1gMnSc1	Voyager
1	[number]	k4	1
<g/>
,	,	kIx,	,
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
radioizotopového	radioizotopový	k2eAgInSc2d1	radioizotopový
pohonu	pohon	k1gInSc2	pohon
hojně	hojně	k6eAd1	hojně
využívat	využívat	k5eAaImF	využívat
k	k	k7c3	k
cestám	cesta	k1gFnPc3	cesta
do	do	k7c2	do
meziplanetárního	meziplanetární	k2eAgInSc2d1	meziplanetární
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgFnPc4	tento
sondy	sonda	k1gFnPc4	sonda
spolehlivě	spolehlivě	k6eAd1	spolehlivě
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pohonů	pohon	k1gInPc2	pohon
využívající	využívající	k2eAgNnPc4d1	využívající
sluneční	sluneční	k2eAgNnPc4d1	sluneční
záření	záření	k1gNnPc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
již	již	k6eAd1	již
projektuje	projektovat	k5eAaBmIp3nS	projektovat
nový	nový	k2eAgInSc1d1	nový
radioizotopový	radioizotopový	k2eAgInSc1d1	radioizotopový
termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
generátor	generátor	k1gInSc1	generátor
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
účinností	účinnost	k1gFnSc7	účinnost
konverze	konverze	k1gFnSc2	konverze
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
materiály	materiál	k1gInPc1	materiál
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
účinnost	účinnost	k1gFnSc4	účinnost
konverze	konverze	k1gFnSc2	konverze
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Projekty	projekt	k1gInPc1	projekt
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
pohonů	pohon	k1gInPc2	pohon
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
spuštěny	spuštěn	k2eAgInPc1d1	spuštěn
programy	program	k1gInPc1	program
využívající	využívající	k2eAgFnSc4d1	využívající
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energii	energie	k1gFnSc4	energie
jako	jako	k8xS	jako
impulsní	impulsní	k2eAgInSc4d1	impulsní
pohon	pohon	k1gInSc4	pohon
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
reaktory	reaktor	k1gInPc4	reaktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
paliva	palivo	k1gNnSc2	palivo
ohřívaly	ohřívat	k5eAaImAgInP	ohřívat
plyn	plyn	k1gInSc4	plyn
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
by	by	kYmCp3nS	by
plyn	plyn	k1gInSc1	plyn
expandoval	expandovat	k5eAaImAgInS	expandovat
v	v	k7c6	v
raketových	raketový	k2eAgFnPc6d1	raketová
tryskách	tryska	k1gFnPc6	tryska
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
tak	tak	k9	tak
pohyb	pohyb	k1gInSc1	pohyb
kosmického	kosmický	k2eAgNnSc2d1	kosmické
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Projekty	projekt	k1gInPc4	projekt
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
pohon	pohon	k1gInSc4	pohon
Rover	rover	k1gMnSc1	rover
<g/>
/	/	kIx~	/
<g/>
NERVA	rvát	k5eNaImSgInS	rvát
měly	mít	k5eAaImAgInP	mít
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
cíle	cíl	k1gInPc1	cíl
<g/>
:	:	kIx,	:
vytvořit	vytvořit	k5eAaPmF	vytvořit
impuls	impuls	k1gInSc4	impuls
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nP	by
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
rozpohyboval	rozpohybovat	k5eAaPmAgMnS	rozpohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
pohony	pohon	k1gInPc4	pohon
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc1d1	použit
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
NERVA	rvát	k5eNaImSgInS	rvát
<g/>
/	/	kIx~	/
<g/>
Rover	rover	k1gMnSc1	rover
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mise	mise	k1gFnSc1	mise
na	na	k7c4	na
Mars	Mars	k1gInSc4	Mars
a	a	k8xC	a
Lunární	lunární	k2eAgFnSc2d1	lunární
mise	mise	k1gFnSc2	mise
byly	být	k5eAaImAgFnP	být
odloženy	odložit	k5eAaPmNgFnP	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
podobným	podobný	k2eAgInSc7d1	podobný
projektem	projekt	k1gInSc7	projekt
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
projekt	projekt	k1gInSc1	projekt
VASIMR	VASIMR	kA	VASIMR
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
pohon	pohon	k1gInSc4	pohon
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
palivo	palivo	k1gNnSc1	palivo
se	se	k3xPyFc4	se
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
pohonu	pohon	k1gInSc2	pohon
využívá	využívat	k5eAaPmIp3nS	využívat
neutrálního	neutrální	k2eAgInSc2d1	neutrální
plynu	plyn	k1gInSc2	plyn
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
helium	helium	k1gNnSc1	helium
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
nebo	nebo	k8xC	nebo
xenon	xenon	k1gInSc1	xenon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohon	pohon	k1gInSc1	pohon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c4	na
nízký	nízký	k2eAgInSc4d1	nízký
tah	tah	k1gInSc4	tah
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
malým	malý	k2eAgInSc7d1	malý
proudem	proud	k1gInSc7	proud
plazmatu	plazma	k1gNnSc2	plazma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
tah	tah	k1gInSc4	tah
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dohledné	dohledný	k2eAgFnSc6d1	dohledná
době	doba	k1gFnSc6	doba
nerealizovatelný	realizovatelný	k2eNgInSc1d1	nerealizovatelný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ani	ani	k8xC	ani
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
zkonstruován	zkonstruován	k2eAgInSc1d1	zkonstruován
fúzní	fúzní	k2eAgInSc1d1	fúzní
reaktor	reaktor	k1gInSc1	reaktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
dodával	dodávat	k5eAaImAgInS	dodávat
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
než	než	k8xS	než
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
