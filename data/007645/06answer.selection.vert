<s>
Hypernova	Hypernův	k2eAgFnSc1d1	Hypernova
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
typ	typ	k1gInSc1	typ
supernovy	supernova	k1gFnSc2	supernova
vznikající	vznikající	k2eAgFnSc2d1	vznikající
kolapsem	kolaps	k1gInSc7	kolaps
na	na	k7c6	na
konci	konec	k1gInSc6	konec
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
výjimečně	výjimečně	k6eAd1	výjimečně
velké	velký	k2eAgFnPc1d1	velká
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
