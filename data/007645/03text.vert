<s>
Hypernova	Hypernův	k2eAgFnSc1d1	Hypernova
je	být	k5eAaImIp3nS	být
teoreticky	teoreticky	k6eAd1	teoreticky
předpokládaný	předpokládaný	k2eAgInSc1d1	předpokládaný
typ	typ	k1gInSc1	typ
supernovy	supernova	k1gFnSc2	supernova
vznikající	vznikající	k2eAgFnSc2d1	vznikající
kolapsem	kolaps	k1gInSc7	kolaps
na	na	k7c6	na
konci	konec	k1gInSc6	konec
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
výjimečně	výjimečně	k6eAd1	výjimečně
velké	velký	k2eAgFnPc1d1	velká
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hypernově	hypernov	k1gInSc6	hypernov
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
hroutí	hroutit	k5eAaImIp3nS	hroutit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
a	a	k8xC	a
z	z	k7c2	z
pólů	pól	k1gInPc2	pól
její	její	k3xOp3gFnSc2	její
rotace	rotace	k1gFnSc2	rotace
vytrysknou	vytrysknout	k5eAaPmIp3nP	vytrysknout
dva	dva	k4xCgInPc1	dva
extrémně	extrémně	k6eAd1	extrémně
energetické	energetický	k2eAgInPc1d1	energetický
proudy	proud	k1gInPc1	proud
plazmatu	plazma	k1gNnSc2	plazma
dosahující	dosahující	k2eAgFnPc1d1	dosahující
takřka	takřka	k6eAd1	takřka
rychlosti	rychlost	k1gFnPc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výtrysky	výtrysk	k1gInPc1	výtrysk
emitují	emitovat	k5eAaBmIp3nP	emitovat
intenzívní	intenzívní	k2eAgNnSc4d1	intenzívní
gama	gama	k1gNnSc4	gama
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ony	onen	k3xDgFnPc1	onen
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tzv.	tzv.	kA	tzv.
gama	gama	k1gNnSc6	gama
záblesky	záblesk	k1gInPc1	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
data	datum	k1gNnPc1	datum
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
gama	gama	k1gNnSc2	gama
záblesků	záblesk	k1gInPc2	záblesk
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
našemu	náš	k3xOp1gNnSc3	náš
chápání	chápání	k1gNnSc3	chápání
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
relativně	relativně	k6eAd1	relativně
blízkou	blízký	k2eAgFnSc4d1	blízká
hypernovou	hypernová	k1gFnSc4	hypernová
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
schopna	schopen	k2eAgFnSc1d1	schopna
vyhladit	vyhladit	k5eAaPmF	vyhladit
život	život	k1gInSc4	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
NASA	NASA	kA	NASA
a	a	k8xC	a
z	z	k7c2	z
Kansaské	kansaský	k2eAgFnSc2d1	Kansaská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
publikovali	publikovat	k5eAaBmAgMnP	publikovat
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
masové	masový	k2eAgFnSc2d1	masová
vymírání	vymírání	k1gNnSc4	vymírání
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ordoviku	ordovik	k1gInSc2	ordovik
před	před	k7c7	před
450	[number]	k4	450
milióny	milión	k4xCgInPc7	milión
lety	léto	k1gNnPc7	léto
mohl	moct	k5eAaImAgInS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
gama	gama	k1gNnSc4	gama
záblesk	záblesk	k1gInSc4	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Proud	proud	k1gInSc1	proud
paprsků	paprsek	k1gInPc2	paprsek
gama	gama	k1gNnSc2	gama
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
hypernovou	hypernová	k1gFnSc7	hypernová
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
Zemi	zem	k1gFnSc4	zem
na	na	k7c4	na
pouhých	pouhý	k2eAgFnPc2d1	pouhá
deset	deset	k4xCc1	deset
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
zničit	zničit	k5eAaPmF	zničit
polovinu	polovina	k1gFnSc4	polovina
ochranné	ochranný	k2eAgFnSc2d1	ochranná
ozónové	ozónový	k2eAgFnSc2d1	ozónová
vrstvy	vrstva	k1gFnSc2	vrstva
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poškození	poškození	k1gNnSc6	poškození
ozónové	ozónový	k2eAgFnSc2d1	ozónová
vrstvy	vrstva	k1gFnSc2	vrstva
je	být	k5eAaImIp3nS	být
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
schopno	schopen	k2eAgNnSc1d1	schopno
vyhubit	vyhubit	k5eAaPmF	vyhubit
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
a	a	k8xC	a
blízko	blízko	k7c2	blízko
hladiny	hladina	k1gFnSc2	hladina
oceánů	oceán	k1gInPc2	oceán
i	i	k8xC	i
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
přerušit	přerušit	k5eAaPmF	přerušit
potravní	potravní	k2eAgInSc4d1	potravní
řetězec	řetězec	k1gInSc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
hvězdy	hvězda	k1gFnPc1	hvězda
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgFnPc1d1	velká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
přímo	přímo	k6eAd1	přímo
zhroutit	zhroutit	k5eAaPmF	zhroutit
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
hypernovy	hypernov	k1gInPc1	hypernov
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
také	také	k9	také
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
hypernovy	hypernův	k2eAgFnSc2d1	hypernova
dochází	docházet	k5eAaImIp3nS	docházet
každých	každý	k3xTgInPc2	každý
200	[number]	k4	200
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
teorií	teorie	k1gFnPc2	teorie
a	a	k8xC	a
simulací	simulace	k1gFnPc2	simulace
vznikají	vznikat	k5eAaImIp3nP	vznikat
hypernovy	hypernov	k1gInPc4	hypernov
z	z	k7c2	z
masívních	masívní	k2eAgFnPc2d1	masívní
hvězd	hvězda	k1gFnPc2	hvězda
přinejmenším	přinejmenším	k6eAd1	přinejmenším
40	[number]	k4	40
<g/>
×	×	k?	×
těžších	těžký	k2eAgInPc2d2	těžší
než	než	k8xS	než
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
hypernovu	hypernův	k2eAgFnSc4d1	hypernova
<g/>
,	,	kIx,	,
Eta	Eta	k1gFnSc4	Eta
Carinae	Carina	k1gFnSc2	Carina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vzplanout	vzplanout	k5eAaPmF	vzplanout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
příštího	příští	k2eAgInSc2d1	příští
miliónu	milión	k4xCgInSc2	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
7500	[number]	k4	7500
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
