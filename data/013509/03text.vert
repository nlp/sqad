<s>
Andimáchia	Andimáchia	k1gFnSc1
</s>
<s>
Andimáchia	Andimáchia	k1gFnSc1
Α	Α	k?
Větrný	větrný	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
ve	v	k7c6
městě	město	k1gNnSc6
AndimáchiaPoloha	AndimáchiaPoloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
36	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
27	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
38	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
kraj	kraj	k7c2
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Egeis	Egeis	k1gFnSc1
regionální	regionální	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Kós	Kós	k1gInSc1
obec	obec	k1gFnSc1
</s>
<s>
Kós	Kós	k1gInSc1
obecní	obecní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Irakleides	Irakleides	k1gInSc1
komunita	komunita	k1gFnSc1
</s>
<s>
Andimáchia	Andimáchia	k1gFnSc1
</s>
<s>
Andimáchia	Andimáchia	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
2	#num#	k4
068	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Náboženské	náboženský	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Pravoslaví	pravoslaví	k1gNnSc1
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
město	město	k1gNnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Andimáchia	Andimáchia	k1gFnSc1
<g/>
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
<g/>
:	:	kIx,
Α	Α	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
řeckém	řecký	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
Kós	Kós	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
23	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Kósu	Kós	k1gInSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
na	na	k7c6
náhorní	náhorní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
uprostřed	uprostřed	k7c2
ostrova	ostrov	k1gInSc2
Kósu	Kós	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
města	město	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
větrný	větrný	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
tam	tam	k6eAd1
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jediný	jediný	k2eAgInSc1d1
funkční	funkční	k2eAgInSc1d1
takový	takový	k3xDgInSc1
mlýn	mlýn	k1gInSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
silného	silný	k2eAgInSc2d1
větru	vítr	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
ještě	ještě	k9
dnes	dnes	k6eAd1
umlít	umlít	k5eAaPmF
až	až	k9
800	#num#	k4
kilogramů	kilogram	k1gInPc2
mouky	mouka	k1gFnSc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členění	členění	k1gNnSc1
komunity	komunita	k1gFnSc2
</s>
<s>
Komunita	komunita	k1gFnSc1
Andimáchia	Andimáchium	k1gNnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
vlastního	vlastní	k2eAgNnSc2d1
města	město	k1gNnSc2
Andimáchia	Andimáchium	k1gNnSc2
(	(	kIx(
<g/>
2538	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vesnice	vesnice	k1gFnSc1
Mastichari	Mastichar	k1gFnSc2
(	(	kIx(
<g/>
470	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hrad	hrad	k1gInSc1
</s>
<s>
Hrad	hrad	k1gInSc1
v	v	k7c6
Andimáchii	Andimáchie	k1gFnSc6
</s>
<s>
Maltézský	maltézský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
</s>
<s>
Na	na	k7c6
malém	malý	k2eAgInSc6d1
kopci	kopec	k1gInSc6
nad	nad	k7c7
Andimáchií	Andimáchie	k1gFnSc7
leží	ležet	k5eAaImIp3nS
benátský	benátský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
,	,	kIx,
známý	známý	k2eAgInSc4d1
jako	jako	k8xS,k8xC
andimáchský	andimáchský	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
na	na	k7c6
začátku	začátek	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
řádem	řád	k1gInSc7
rytířů	rytíř	k1gMnPc2
svatého	svatý	k1gMnSc2
Jana	Jan	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
historie	historie	k1gFnSc2
jako	jako	k8xC,k8xS
johanité	johanita	k1gMnPc1
nebo	nebo	k8xC
později	pozdě	k6eAd2
maltézští	maltézský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
byly	být	k5eAaImAgFnP
ještě	ještě	k6eAd1
intenzivnější	intenzivní	k2eAgFnPc1d2
na	na	k7c6
konci	konec	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
roce	rok	k1gInSc6
1494	#num#	k4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
kvůli	kvůli	k7c3
turecké	turecký	k2eAgFnSc3d1
hrozbě	hrozba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
letopočet	letopočet	k1gInSc4
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
nad	nad	k7c7
vstupní	vstupní	k2eAgFnSc7d1
bránou	brána	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachovalo	zachovat	k5eAaPmAgNnS
se	s	k7c7
vnější	vnější	k2eAgFnSc7d1
opevnění	opevnění	k1gNnSc4
a	a	k8xC
uvnitř	uvnitř	k7c2
hradu	hrad	k1gInSc2
dva	dva	k4xCgInPc1
chrámy	chrám	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrám	chrám	k1gInSc1
sv.	sv.	kA
Paraskevi	Paraskeev	k1gFnSc6
(	(	kIx(
<g/>
Agia	Agia	k1gFnSc1
Paraskevi	Paraskeev	k1gFnSc3
<g/>
)	)	kIx)
z	z	k7c2
počátku	počátek	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
sv.	sv.	kA
Mikuláše	mikuláš	k1gInPc1
(	(	kIx(
<g/>
Agios	Agios	k1gMnSc1
Nikolaos	Nikolaos	k1gMnSc1
<g/>
)	)	kIx)
ze	z	k7c2
století	století	k1gNnSc2
šestnáctého	šestnáctý	k4xOgNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Antimachia	Antimachium	k1gNnSc2
na	na	k7c6
nizozemské	nizozemský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
2068	#num#	k4
<g/>
,	,	kIx,
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
komunity	komunita	k1gFnSc2
je	být	k5eAaImIp3nS
2538	#num#	k4
dle	dle	k7c2
↑	↑	k?
GREEKA	GREEKA	kA
<g/>
.	.	kIx.
<g/>
COM	COM	kA
<g/>
.	.	kIx.
greeka	greeka	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WWW.GREEKA.COM	WWW.GREEKA.COM	k1gFnSc1
<g/>
,	,	kIx,
Antimachia	Antimachia	k1gFnSc1
Venetian	Venetian	k1gInSc1
Castle	Castle	k1gFnSc1
<g/>
.	.	kIx.
greeka	greeka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Andimáchia	Andimáchium	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.kosinfo.gr/villages/antimachia	http://www.kosinfo.gr/villages/antimachia	k1gFnSc1
</s>
