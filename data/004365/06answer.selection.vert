<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
opery	opera	k1gFnPc1	opera
Rienzi	Rienze	k1gFnSc3	Rienze
<g/>
,	,	kIx,	,
Bludný	bludný	k2eAgMnSc1d1	bludný
Holanďan	Holanďan	k1gMnSc1	Holanďan
<g/>
,	,	kIx,	,
Tannhäuser	Tannhäuser	k1gMnSc1	Tannhäuser
<g/>
,	,	kIx,	,
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
<g/>
,	,	kIx,	,
Mistři	mistr	k1gMnPc1	mistr
pěvci	pěvec	k1gMnPc1	pěvec
norimberští	norimberský	k2eAgMnPc1d1	norimberský
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
tetralogie	tetralogie	k1gFnSc1	tetralogie
Prsten	prsten	k1gInSc4	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
opera	opera	k1gFnSc1	opera
Parsifal	Parsifal	k1gFnSc2	Parsifal
<g/>
.	.	kIx.	.
</s>
