<s>
Kokosová	kokosový	k2eAgFnSc1d1	kokosová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
vitamínu	vitamín	k1gInSc2	vitamín
B	B	kA	B
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
B6	B6	k1gFnSc2	B6
a	a	k8xC	a
B	B	kA	B
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitaminu	vitamin	k1gInSc2	vitamin
C.	C.	kA	C.
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
kokosového	kokosový	k2eAgInSc2d1	kokosový
ořechu	ořech	k1gInSc2	ořech
prochází	procházet	k5eAaImIp3nS	procházet
náročnou	náročný	k2eAgFnSc7d1	náročná
filtrací	filtrace	k1gFnSc7	filtrace
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
od	od	k7c2	od
kořenového	kořenový	k2eAgInSc2d1	kořenový
systému	systém	k1gInSc2	systém
do	do	k7c2	do
semene	semeno	k1gNnSc2	semeno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
sterilní	sterilní	k2eAgNnSc1d1	sterilní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
přirozené	přirozený	k2eAgInPc1d1	přirozený
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
infikované	infikovaný	k2eAgInPc1d1	infikovaný
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
znečištěné	znečištěný	k2eAgNnSc1d1	znečištěné
<g/>
.	.	kIx.	.
</s>
