<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
standard	standard	k1gInSc4	standard
MIME	mim	k1gMnSc5	mim
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
kódování	kódování	k1gNnSc1	kódování
vkládaných	vkládaný	k2eAgInPc2d1	vkládaný
HTML	HTML	kA	HTML
a	a	k8xC	a
binárních	binární	k2eAgFnPc2d1	binární
příloh	příloha	k1gFnPc2	příloha
<g/>
,	,	kIx,	,
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
videí	video	k1gNnPc2	video
<g/>
.	.	kIx.	.
</s>
