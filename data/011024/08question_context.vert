<s>
Fjord	fjord	k1gInSc1	fjord
je	být	k5eAaImIp3nS	být
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
po	po	k7c6	po
ledovcové	ledovcový	k2eAgFnSc6d1	ledovcová
činnosti	činnost	k1gFnSc6	činnost
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
ledových	ledový	k2eAgFnPc6d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
(	(	kIx(	(
<g/>
občas	občas	k6eAd1	občas
desítky	desítka	k1gFnPc4	desítka
až	až	k8xS	až
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
úzký	úzký	k2eAgInSc1d1	úzký
mořský	mořský	k2eAgInSc1d1	mořský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
prohloubením	prohloubení	k1gNnSc7	prohloubení
říčních	říční	k2eAgNnPc2d1	říční
údolí	údolí	k1gNnPc2	údolí
vlivem	vlivem	k7c2	vlivem
tlaků	tlak	k1gInPc2	tlak
postupujících	postupující	k2eAgInPc2d1	postupující
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
