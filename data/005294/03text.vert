<s>
Rigel	Rigel	k1gInSc1	Rigel
(	(	kIx(	(
<g/>
β	β	k?	β
Ori	Ori	k1gFnSc1	Ori
/	/	kIx~	/
β	β	k?	β
Orionis	Orionis	k1gInSc1	Orionis
/	/	kIx~	/
Beta	beta	k1gNnSc1	beta
Orionis	Orionis	k1gFnSc2	Orionis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Orion	orion	k1gInSc1	orion
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
Bayerova	Bayerův	k2eAgNnSc2d1	Bayerovo
značení	značení	k1gNnSc2	značení
písmeno	písmeno	k1gNnSc1	písmeno
β	β	k?	β
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
převážnou	převážný	k2eAgFnSc4d1	převážná
dobu	doba	k1gFnSc4	doba
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Betelgeuze	Betelgeuze	k1gFnSc1	Betelgeuze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
hvězdy	hvězda	k1gFnSc2	hvězda
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
arabského	arabský	k2eAgNnSc2d1	arabské
slova	slovo	k1gNnSc2	slovo
ridžl	ridžl	k1gInSc1	ridžl
(	(	kIx(	(
<g/>
noha	noha	k1gFnSc1	noha
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
Oriona	Orion	k1gMnSc2	Orion
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Poseidóna	Poseidón	k1gMnSc2	Poseidón
<g/>
,	,	kIx,	,
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
levé	levý	k2eAgFnSc2d1	levá
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
území	území	k1gNnSc2	území
Česka	Česko	k1gNnSc2	Česko
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Orionem	orion	k1gInSc7	orion
<g/>
)	)	kIx)	)
vidět	vidět	k5eAaImF	vidět
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Rigel	Rigel	k1gInSc1	Rigel
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vůbec	vůbec	k9	vůbec
nejjasnější	jasný	k2eAgFnPc4d3	nejjasnější
hvězdy	hvězda	k1gFnPc4	hvězda
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgMnSc2	tento
veleobra	veleobr	k1gMnSc2	veleobr
činí	činit	k5eAaImIp3nS	činit
0,12	[number]	k4	0,12
M.	M.	kA	M.
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
–	–	k?	–
<g/>
6,7	[number]	k4	6,7
<g/>
.	.	kIx.	.
</s>
<s>
Spektrální	spektrální	k2eAgFnSc1d1	spektrální
třída	třída	k1gFnSc1	třída
je	být	k5eAaImIp3nS	být
B8	B8	k1gFnSc4	B8
Ia	ia	k0	ia
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
modrobílý	modrobílý	k2eAgInSc4d1	modrobílý
typ	typ	k1gInSc4	typ
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
teplotou	teplota	k1gFnSc7	teplota
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
K.	K.	kA	K.
Kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
velké	velký	k2eAgFnSc3d1	velká
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
(	(	kIx(	(
<g/>
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
900	[number]	k4	900
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
změřit	změřit	k5eAaPmF	změřit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
asi	asi	k9	asi
78	[number]	k4	78
poloměrů	poloměr	k1gInPc2	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
modrobílé	modrobílý	k2eAgMnPc4d1	modrobílý
veleobry	veleobr	k1gMnPc4	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Rigel	Rigel	k1gInSc1	Rigel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
66	[number]	k4	66
000	[number]	k4	000
L	L	kA	L
<g/>
☉	☉	k?	☉
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
nadobry	nadobr	k1gInPc7	nadobr
v	v	k7c6	v
asociaci	asociace	k1gFnSc6	asociace
Orion	orion	k1gInSc1	orion
má	mít	k5eAaImIp3nS	mít
nejpozdnější	pozdný	k2eAgNnSc4d3	nejpozdnější
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
i	i	k9	i
ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
asociaci	asociace	k1gFnSc6	asociace
OB	Ob	k1gInSc1	Ob
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsvítivější	svítivý	k2eAgMnSc1d3	svítivý
člen	člen	k1gMnSc1	člen
(	(	kIx(	(
<g/>
mag	mag	k?	mag
<g/>
.	.	kIx.	.
-8,5	-8,5	k4	-8,5
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nejpozdnější	pozdný	k2eAgNnSc1d3	nejpozdnější
spektrum	spektrum	k1gNnSc1	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Rigel	Rigel	k1gInSc1	Rigel
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
9,5	[number]	k4	9,5
<g/>
"	"	kIx"	"
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
,	,	kIx,	,
sp	sp	k?	sp
<g/>
.	.	kIx.	.
</s>
<s>
B9V	B9V	k4	B9V
mv	mv	k?	mv
<g/>
:	:	kIx,	:
6,7	[number]	k4	6,7
(	(	kIx(	(
<g/>
mag	mag	k?	mag
<g/>
.	.	kIx.	.
-0,9	-0,9	k4	-0,9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
průvodce	průvodce	k1gMnSc1	průvodce
je	být	k5eAaImIp3nS	být
těsnou	těsný	k2eAgFnSc7d1	těsná
vizuální	vizuální	k2eAgFnSc7d1	vizuální
dvojhvězdou	dvojhvězda	k1gFnSc7	dvojhvězda
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
jasnosti	jasnost	k1gFnPc1	jasnost
mv	mv	k?	mv
<g/>
:	:	kIx,	:
7,4	[number]	k4	7,4
<g/>
,	,	kIx,	,
mag	mag	k?	mag
<g/>
.	.	kIx.	.
-0,9	-0,9	k4	-0,9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
jako	jako	k8xS	jako
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
spektroskopická	spektroskopický	k2eAgFnSc1d1	spektroskopická
o	o	k7c6	o
periodě	perioda	k1gFnSc6	perioda
9,9	[number]	k4	9,9
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
těsných	těsný	k2eAgFnPc2d1	těsná
vizuálních	vizuální	k2eAgFnPc2d1	vizuální
složek	složka	k1gFnPc2	složka
je	být	k5eAaImIp3nS	být
touto	tento	k3xDgFnSc7	tento
spektroskopickou	spektroskopický	k2eAgFnSc7d1	spektroskopická
dvojhvězdou	dvojhvězda	k1gFnSc7	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
velkou	velký	k2eAgFnSc4d1	velká
spotřebu	spotřeba	k1gFnSc4	spotřeba
vodíkového	vodíkový	k2eAgNnSc2d1	vodíkové
paliva	palivo	k1gNnSc2	palivo
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
tuto	tento	k3xDgFnSc4	tento
žhavou	žhavý	k2eAgFnSc4d1	žhavá
hvězdu	hvězda	k1gFnSc4	hvězda
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
brzkému	brzký	k2eAgInSc3d1	brzký
konci	konec	k1gInSc3	konec
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
výbuchu	výbuch	k1gInSc2	výbuch
supernovy	supernova	k1gFnSc2	supernova
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
proměně	proměna	k1gFnSc6	proměna
v	v	k7c4	v
neutronovou	neutronový	k2eAgFnSc4d1	neutronová
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Životnost	životnost	k1gFnSc1	životnost
těchto	tento	k3xDgMnPc2	tento
žhavých	žhavý	k2eAgMnPc2d1	žhavý
veleobrů	veleobr	k1gMnPc2	veleobr
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
Rigel	Rigela	k1gFnPc2	Rigela
je	být	k5eAaImIp3nS	být
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
ve	v	k7c6	v
vědeckofantastickém	vědeckofantastický	k2eAgInSc6d1	vědeckofantastický
románu	román	k1gInSc6	román
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
labyrintu	labyrint	k1gInSc6	labyrint
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
in	in	k?	in
the	the	k?	the
Maze	Maga	k1gFnSc6	Maga
<g/>
)	)	kIx)	)
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Roberta	Robert	k1gMnSc2	Robert
Silverberga	Silverberg	k1gMnSc2	Silverberg
<g/>
.	.	kIx.	.
</s>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Orionu	orion	k1gInSc2	orion
Seznam	seznam	k1gInSc1	seznam
tradičních	tradiční	k2eAgInPc2d1	tradiční
názvů	název	k1gInPc2	název
hvězd	hvězda	k1gFnPc2	hvězda
Seznam	seznam	k1gInSc4	seznam
hvězd	hvězda	k1gFnPc2	hvězda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rigel	Rigela	k1gFnPc2	Rigela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
