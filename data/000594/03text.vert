<s>
Kennedyho	Kennedyze	k6eAd1	Kennedyze
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
Space	Space	k1gFnSc2	Space
Center	centrum	k1gNnPc2	centrum
<g/>
;	;	kIx,	;
zkratka	zkratka	k1gFnSc1	zkratka
KSC	KSC	kA	KSC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
kosmodrom	kosmodrom	k1gInSc1	kosmodrom
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
ostrova	ostrov	k1gInSc2	ostrov
Merritt	Merrittum	k1gNnPc2	Merrittum
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Středisko	středisko	k1gNnSc1	středisko
je	být	k5eAaImIp3nS	být
spravováno	spravovat	k5eAaImNgNnS	spravovat
americkým	americký	k2eAgInSc7d1	americký
Národním	národní	k2eAgInSc7d1	národní
úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Floridy	Florida	k1gFnSc2	Florida
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
největší	veliký	k2eAgFnSc1d3	veliký
americká	americký	k2eAgFnSc1d1	americká
kosmická	kosmický	k2eAgFnSc1d1	kosmická
základna	základna	k1gFnSc1	základna
Eastern	Eastern	k1gInSc4	Eastern
Test	test	k1gInSc1	test
Range	Rang	k1gInSc2	Rang
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polovina	polovina	k1gFnSc1	polovina
patřila	patřit	k5eAaImAgFnS	patřit
vojenskému	vojenský	k2eAgInSc3d1	vojenský
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zde	zde	k6eAd1	zde
vybudovalo	vybudovat	k5eAaPmAgNnS	vybudovat
komplex	komplex	k1gInSc4	komplex
Cape	capat	k5eAaImIp3nS	capat
Canaveral	Canaveral	k1gFnSc1	Canaveral
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
startovalo	startovat	k5eAaBmAgNnS	startovat
mnoho	mnoho	k4c1	mnoho
raket	raketa	k1gFnPc2	raketa
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
startovaly	startovat	k5eAaBmAgFnP	startovat
i	i	k9	i
první	první	k4xOgFnPc1	první
kosmické	kosmický	k2eAgFnPc1d1	kosmická
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
postavila	postavit	k5eAaPmAgFnS	postavit
Kennedyho	Kennedy	k1gMnSc4	Kennedy
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
půl	půl	k1xP	půl
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
Miami	Miami	k1gNnSc7	Miami
a	a	k8xC	a
Jacksonville	Jacksonville	k1gNnSc7	Jacksonville
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
55	[number]	k4	55
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
10	[number]	k4	10
km	km	kA	km
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
567	[number]	k4	567
km2	km2	k4	km2
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
tam	tam	k6eAd1	tam
okolo	okolo	k7c2	okolo
17	[number]	k4	17
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
vlastní	vlastní	k2eAgNnSc1d1	vlastní
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
a	a	k8xC	a
veřejné	veřejný	k2eAgFnPc4d1	veřejná
turistické	turistický	k2eAgFnPc4d1	turistická
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
omezený	omezený	k2eAgInSc4d1	omezený
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
i	i	k9	i
důležitá	důležitý	k2eAgFnSc1d1	důležitá
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
desítky	desítka	k1gFnPc1	desítka
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
lze	lze	k6eAd1	lze
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc4d1	žijící
aligátory	aligátor	k1gMnPc4	aligátor
<g/>
.	.	kIx.	.
</s>
<s>
Kosmodrom	kosmodrom	k1gInSc1	kosmodrom
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
americkém	americký	k2eAgInSc6d1	americký
prezidentu	prezident	k1gMnSc3	prezident
Johnu	John	k1gMnSc3	John
Fitzgeraldu	Fitzgerald	k1gMnSc3	Fitzgerald
Kennedym	Kennedym	k1gInSc4	Kennedym
Starty	start	k1gInPc4	start
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
raketoplánů	raketoplán	k1gInPc2	raketoplán
byly	být	k5eAaImAgFnP	být
ze	z	k7c2	z
Startovacího	startovací	k2eAgInSc2d1	startovací
komplexu	komplex	k1gInSc2	komplex
39	[number]	k4	39
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
hangár	hangár	k1gInSc1	hangár
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
startovací	startovací	k2eAgFnPc4d1	startovací
rampy	rampa	k1gFnPc4	rampa
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
kilometrů	kilometr	k1gInPc2	kilometr
jižně	jižně	k6eAd1	jižně
je	být	k5eAaImIp3nS	být
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
administrativní	administrativní	k2eAgFnPc1d1	administrativní
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
VAB	VAB	k?	VAB
(	(	kIx(	(
<g/>
Vehicle	Vehicle	k1gFnSc1	Vehicle
Assembly	Assembly	k1gMnSc1	Assembly
Building	Building	k1gInSc1	Building
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byly	být	k5eAaImAgFnP	být
prováděny	prováděn	k2eAgFnPc1d1	prováděna
kontroly	kontrola	k1gFnPc1	kontrola
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc1	oprava
a	a	k8xC	a
úpravy	úprava	k1gFnPc4	úprava
raketoplánů	raketoplán	k1gInPc2	raketoplán
před	před	k7c7	před
každým	každý	k3xTgInSc7	každý
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
na	na	k7c4	na
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
odpalovací	odpalovací	k2eAgFnSc4d1	odpalovací
rampu	rampa	k1gFnSc4	rampa
raketoplán	raketoplán	k1gInSc1	raketoplán
přepraví	přepravit	k5eAaPmIp3nP	přepravit
pomocí	pomocí	k7c2	pomocí
pásového	pásový	k2eAgInSc2d1	pásový
transportéru	transportér	k1gInSc2	transportér
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Crawler	Crawler	k1gMnSc1	Crawler
Transporter	Transporter	k1gMnSc1	Transporter
<g/>
)	)	kIx)	)
-	-	kIx~	-
zdolání	zdolání	k1gNnSc1	zdolání
5,5	[number]	k4	5,5
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
trasy	trasa	k1gFnSc2	trasa
trvá	trvat	k5eAaImIp3nS	trvat
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
pro	pro	k7c4	pro
raketoplány	raketoplán	k1gInPc4	raketoplán
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
120	[number]	k4	120
m	m	kA	m
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přes	přes	k7c4	přes
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnické	návštěvnický	k2eAgNnSc1d1	návštěvnické
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Kennedy	Kenneda	k1gMnSc2	Kenneda
Space	Space	k1gFnSc2	Space
Center	centrum	k1gNnPc2	centrum
Visitor	Visitor	k1gMnSc1	Visitor
Complex	Complex	k1gInSc1	Complex
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
expozic	expozice	k1gFnPc2	expozice
<g/>
,	,	kIx,	,
např	např	kA	např
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
dobývání	dobývání	k1gNnSc4	dobývání
kosmu	kosmos	k1gInSc2	kosmos
Památník	památník	k1gInSc4	památník
astronautům	astronaut	k1gMnPc3	astronaut
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
vesmíru	vesmír	k1gInSc2	vesmír
Budoucnost	budoucnost	k1gFnSc1	budoucnost
vesmírných	vesmírný	k2eAgMnPc2d1	vesmírný
letů	let	k1gInPc2	let
IMAX	IMAX	kA	IMAX
kino	kino	k1gNnSc4	kino
Maketa	maketa	k1gFnSc1	maketa
raketoplánu	raketoplán	k1gInSc2	raketoplán
Explorer	Explorra	k1gFnPc2	Explorra
v	v	k7c4	v
životní	životní	k2eAgFnPc4d1	životní
velikosti	velikost	k1gFnPc4	velikost
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
vstupenky	vstupenka	k1gFnSc2	vstupenka
<g />
.	.	kIx.	.
</s>
<s>
podniknout	podniknout	k5eAaPmF	podniknout
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
výpravu	výprava	k1gFnSc4	výprava
k	k	k7c3	k
několika	několik	k4yIc3	několik
vzdálenějším	vzdálený	k2eAgInPc3d2	vzdálenější
objektům	objekt	k1gInPc3	objekt
<g/>
:	:	kIx,	:
LC	LC	kA	LC
39	[number]	k4	39
Observation	Observation	k1gInSc4	Observation
Gantry	Gantrum	k1gNnPc7	Gantrum
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
odpalovací	odpalovací	k2eAgFnPc4d1	odpalovací
rampy	rampa	k1gFnPc4	rampa
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
/	/	kIx~	/
<g/>
Saturn	Saturn	k1gMnSc1	Saturn
V	v	k7c6	v
Center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
raket	raketa	k1gFnPc2	raketa
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
,	,	kIx,	,
raket	raketa	k1gFnPc2	raketa
Saturn	Saturn	k1gInSc1	Saturn
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
vybavení	vybavení	k1gNnSc1	vybavení
International	International	k1gFnPc2	International
Space	Spaec	k1gInSc2	Spaec
Station	station	k1gInSc4	station
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
projít	projít	k5eAaPmF	projít
několika	několik	k4yIc7	několik
moduly	modul	k1gInPc7	modul
ISS	ISS	kA	ISS
Johnsonovo	Johnsonův	k2eAgNnSc1d1	Johnsonovo
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
Space	Spaec	k1gInSc2	Spaec
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
</s>
