<s>
Kennedyho	Kennedyze	k6eAd1	Kennedyze
vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
Space	Space	k1gFnSc2	Space
Center	centrum	k1gNnPc2	centrum
<g/>
;	;	kIx,	;
zkratka	zkratka	k1gFnSc1	zkratka
KSC	KSC	kA	KSC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
kosmodrom	kosmodrom	k1gInSc1	kosmodrom
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
ostrova	ostrov	k1gInSc2	ostrov
Merritt	Merrittum	k1gNnPc2	Merrittum
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
