<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
Freising	Freising	k1gInSc1	Freising
je	být	k5eAaImIp3nS	být
někdejší	někdejší	k2eAgInSc4d1	někdejší
klášter	klášter	k1gInSc4	klášter
františkánských	františkánský	k2eAgMnPc2d1	františkánský
reformátorů	reformátor	k1gMnPc2	reformátor
ve	v	k7c6	v
Freisingu	Freising	k1gInSc6	Freising
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
v	v	k7c6	v
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
mnichovském	mnichovský	k2eAgNnSc6d1	mnichovské
<g/>
,	,	kIx,	,
diecéze	diecéze	k1gFnSc1	diecéze
Freising	Freising	k1gInSc1	Freising
<g/>
.	.	kIx.	.
</s>
