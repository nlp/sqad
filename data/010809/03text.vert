<p>
<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
Freising	Freising	k1gInSc1	Freising
je	být	k5eAaImIp3nS	být
někdejší	někdejší	k2eAgInSc4d1	někdejší
klášter	klášter	k1gInSc4	klášter
františkánských	františkánský	k2eAgMnPc2d1	františkánský
reformátorů	reformátor	k1gMnPc2	reformátor
ve	v	k7c6	v
Freisingu	Freising	k1gInSc6	Freising
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
v	v	k7c6	v
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
mnichovském	mnichovský	k2eAgNnSc6d1	mnichovské
<g/>
,	,	kIx,	,
diecéze	diecéze	k1gFnSc1	diecéze
Freising	Freising	k1gInSc1	Freising
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Klášter	klášter	k1gInSc1	klášter
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc6	Assis
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Bavor	Bavory	k1gInPc2	Bavory
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
z	z	k7c2	z
Freisingu	Freising	k1gInSc2	Freising
a	a	k8xC	a
kolínský	kolínský	k2eAgMnSc1d1	kolínský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
klášterem	klášter	k1gInSc7	klášter
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
hospic	hospic	k1gInSc1	hospic
<g/>
,	,	kIx,	,
od	od	k7c2	od
1621	[number]	k4	1621
konvent	konvent	k1gInSc1	konvent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zesvětštění	zesvětštění	k1gNnSc2	zesvětštění
byl	být	k5eAaImAgInS	být
klášterní	klášterní	k2eAgInSc1d1	klášterní
konvent	konvent	k1gInSc1	konvent
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
přestavěn	přestavět	k5eAaPmNgInS	přestavět
a	a	k8xC	a
zmenšen	zmenšit	k5eAaPmNgInS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Přízemí	přízemí	k1gNnSc1	přízemí
kláštera	klášter	k1gInSc2	klášter
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
hasičská	hasičský	k2eAgFnSc1d1	hasičská
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
poschodí	poschodí	k1gNnSc1	poschodí
bylo	být	k5eAaImAgNnS	být
chudými	chudý	k2eAgFnPc7d1	chudá
školními	školní	k2eAgFnPc7d1	školní
sestrami	sestra	k1gFnPc7	sestra
zařízeno	zařídit	k5eAaPmNgNnS	zařídit
jako	jako	k8xS	jako
domácí	domácí	k2eAgFnSc2d1	domácí
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
dívčí	dívčí	k2eAgFnSc4d1	dívčí
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Franziskanerkloster	Franziskanerkloster	k1gInSc1	Franziskanerkloster
Freising	Freising	k1gInSc1	Freising
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kláštery	klášter	k1gInPc1	klášter
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
:	:	kIx,	:
Františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
Freising	Freising	k1gInSc1	Freising
(	(	kIx(	(
<g/>
Dům	dům	k1gInSc1	dům
bavorských	bavorský	k2eAgFnPc2d1	bavorská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
)	)	kIx)	)
</s>
</p>
