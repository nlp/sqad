<s>
Romaric	Romaric	k1gMnSc1
</s>
<s>
Romaric	Romaric	k1gMnSc1
Osobní	osobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Christian	Christian	k1gMnSc1
N	N	kA
<g/>
'	'	kIx"
<g/>
dri	dri	k?
Koffi	Koff	k1gFnSc2
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1983	#num#	k4
(	(	kIx(
<g/>
37	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Abidžan	Abidžan	k1gInSc1
Výška	výška	k1gFnSc1
</s>
<s>
187	#num#	k4
cm	cm	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
NorthEast	NorthEast	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
Pozice	pozice	k1gFnSc1
</s>
<s>
záložník	záložník	k1gMnSc1
Reprezentace	reprezentace	k1gFnSc2
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
<g/>
0	#num#	k4
<g/>
460	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Africký	africký	k2eAgInSc4d1
pohár	pohár	k1gInSc4
národů	národ	k1gInPc2
</s>
<s>
2006	#num#	k4
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
<g/>
*	*	kIx~
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
za	za	k7c4
klub	klub	k1gInSc4
aktuální	aktuální	k2eAgInSc4d1
k	k	k7c3
červenec	červenec	k1gInSc1
2017	#num#	k4
<g/>
**	**	k?
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
aktuální	aktuální	k2eAgFnSc4d1
k	k	k7c3
březen	březen	k1gInSc4
2014	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Christian	Christian	k1gMnSc1
N	N	kA
<g/>
'	'	kIx"
<g/>
dri	dri	k?
Koffi	Koffi	k1gNnPc2
<g/>
,	,	kIx,
fotbalovou	fotbalový	k2eAgFnSc7d1
přezdívkou	přezdívka	k1gFnSc7
Romaric	Romaric	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1983	#num#	k4
Abidžan	Abidžan	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fotbalový	fotbalový	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
z	z	k7c2
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
hráč	hráč	k1gMnSc1
indického	indický	k2eAgInSc2d1
klubu	klub	k1gInSc2
NorthEast	NorthEast	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
absolventem	absolvent	k1gMnSc7
mládežnické	mládežnický	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
klubu	klub	k1gInSc2
ASEC	ASEC	kA
Mimosas	Mimosas	k1gMnSc1
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
belgického	belgický	k2eAgInSc2d1
KSK	KSK	kA
Beveren	Beverna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
hrál	hrát	k5eAaImAgInS
za	za	k7c4
Le	Le	k1gMnSc4
Mans	Mansa	k1gFnPc2
FC	FC	kA
<g/>
,	,	kIx,
Sevilla	Sevilla	k1gFnSc1
FC	FC	kA
<g/>
,	,	kIx,
RCD	RCD	kA
Espanyol	Espanyol	k1gInSc1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
<g/>
,	,	kIx,
SC	SC	kA
Bastia	Bastia	k1gFnSc1
a	a	k8xC
AC	AC	kA
Omonia	Omonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
Sevillou	Sevilla	k1gFnSc7
vyhrál	vyhrát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
a	a	k8xC
zahrál	zahrát	k5eAaPmAgInS
si	se	k3xPyFc3
také	také	k6eAd1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
fotbalovou	fotbalový	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
odehrál	odehrát	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
až	až	k9
2013	#num#	k4
celkem	celkem	k6eAd1
46	#num#	k4
zápasů	zápas	k1gInPc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
branky	branka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Startoval	startovat	k5eAaBmAgMnS
na	na	k7c6
Africkém	africký	k2eAgInSc6d1
poháru	pohár	k1gInSc6
národů	národ	k1gInPc2
2006	#num#	k4
(	(	kIx(
<g/>
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Africkém	africký	k2eAgInSc6d1
poháru	pohár	k1gInSc6
národů	národ	k1gInPc2
2008	#num#	k4
(	(	kIx(
<g/>
čtvrté	čtvrtá	k1gFnSc6
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
Africkém	africký	k2eAgInSc6d1
poháru	pohár	k1gInSc6
národů	národ	k1gInPc2
2013	#num#	k4
(	(	kIx(
<g/>
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2006	#num#	k4
a	a	k8xC
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
v	v	k7c4
utkání	utkání	k1gNnSc4
proti	proti	k7c3
KLDR	KLDR	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
tým	tým	k1gInSc4
vyhrál	vyhrát	k5eAaPmAgMnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Romaric	Romarice	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
Équipe	Équip	k1gInSc5
</s>
<s>
Abidjan	Abidjan	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
UEFA	UEFA	kA
</s>
<s>
FIFA	FIFA	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
