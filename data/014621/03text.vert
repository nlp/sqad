<s>
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc1
Framework	Framework	k1gInSc1
</s>
<s>
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc4
Framework	Framework	k1gInSc1
(	(	kIx(
<g/>
RDF	RDF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
systém	systém	k1gInSc1
popisu	popis	k1gInSc2
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rodina	rodina	k1gFnSc1
specifikací	specifikace	k1gFnPc2
vypracovaných	vypracovaný	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
World	World	k1gMnSc1
Wide	Wid	k1gFnSc2
Web	web	k1gInSc1
Consortium	Consortium	k1gNnSc1
(	(	kIx(
<g/>
W	W	kA
<g/>
3	#num#	k4
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
původně	původně	k6eAd1
navržených	navržený	k2eAgNnPc2d1
jako	jako	k8xC,k8xS
model	model	k1gInSc4
metadat	metadat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
obecná	obecný	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
pro	pro	k7c4
modelování	modelování	k1gNnSc4
informací	informace	k1gFnPc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
syntaxích	syntax	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
obecný	obecný	k2eAgInSc4d1
rámec	rámec	k1gInSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
popisují	popisovat	k5eAaImIp3nP
zdrojový	zdrojový	k2eAgInSc4d1
dokument	dokument	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
popis	popis	k1gInSc4
</s>
<s>
čitelný	čitelný	k2eAgInSc1d1
jak	jak	k8xS,k8xC
lidsky	lidsky	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
strojově	strojově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RDF	RDF	kA
je	být	k5eAaImIp3nS
standardizovaný	standardizovaný	k2eAgInSc1d1
formát	formát	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
vyjadřovat	vyjadřovat	k5eAaImF
popisné	popisný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
WWW	WWW	kA
zdrojích	zdroj	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
formát	formát	k1gInSc1
grafový	grafový	k2eAgInSc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
veškerá	veškerý	k3xTgNnPc1
data	datum	k1gNnPc1
v	v	k7c6
RDF	RDF	kA
lze	lze	k6eAd1
zapsat	zapsat	k5eAaPmF
pomocí	pomocí	k7c2
grafu	graf	k1gInSc2
s	s	k7c7
orientovanými	orientovaný	k2eAgFnPc7d1
hranami	hrana	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zapsat	zapsat	k5eAaPmF
jako	jako	k9
množinu	množina	k1gFnSc4
trojic	trojice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdrojem	zdroj	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
popsán	popsat	k5eAaPmNgInS
pomocí	pomocí	k7c2
RDF	RDF	kA
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
jakýkoliv	jakýkoliv	k3yIgInSc4
zdroj	zdroj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
lze	lze	k6eAd1
</s>
<s>
jednoznačně	jednoznačně	k6eAd1
identifikovat	identifikovat	k5eAaBmF
pomocí	pomocí	k7c2
URI	URI	kA
(	(	kIx(
<g/>
Uniform	Uniform	k1gInSc4
Resource	Resourka	k1gFnSc3
Identificator	Identificator	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
identifikátor	identifikátor	k1gInSc1
jednoznačně	jednoznačně	k6eAd1
určuje	určovat	k5eAaImIp3nS
<g/>
,	,	kIx,
o	o	k7c4
jaký	jaký	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
se	se	k3xPyFc4
ve	v	k7c6
webovém	webový	k2eAgInSc6d1
obsahu	obsah	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
různými	různý	k2eAgInPc7d1
typy	typ	k1gInPc7
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Formát	formát	k1gInSc1
nemá	mít	k5eNaImIp3nS
konkrétního	konkrétní	k2eAgMnSc4d1
autora	autor	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
reakce	reakce	k1gFnPc4
na	na	k7c4
potřebu	potřeba	k1gFnSc4
flexibilní	flexibilní	k2eAgFnSc2d1
</s>
<s>
architektury	architektura	k1gFnPc1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
vytváření	vytváření	k1gNnSc2
a	a	k8xC
výměny	výměna	k1gFnSc2
webových	webový	k2eAgFnPc2d1
metadat	metadat	k5eAaPmF,k5eAaImF
různých	různý	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
společné	společný	k2eAgFnSc6d1
kooperaci	kooperace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
výslednou	výsledný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
měly	mít	k5eAaImAgFnP
<g/>
,	,	kIx,
kromě	kromě	k7c2
jiných	jiný	k1gMnPc2
<g/>
,	,	kIx,
vliv	vliv	k1gInSc1
platforma	platforma	k1gFnSc1
PICS	PICS	kA
a	a	k8xC
další	další	k2eAgInPc4d1
metadatové	metadatový	k2eAgInPc4d1
formáty	formát	k1gInPc4
jako	jako	k8xC,k8xS
například	například	k6eAd1
Dublin	Dublin	k1gInSc4
Core	Cor	k1gFnSc2
<g/>
,	,	kIx,
Meta	meta	k1gFnSc1
Content	Content	k1gMnSc1
Framework	Framework	k1gInSc1
nebo	nebo	k8xC
Warwick	Warwick	k1gInSc1
Framework	Framework	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RDF	RDF	kA
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
komponentou	komponenta	k1gFnSc7
sémantického	sémantický	k2eAgInSc2d1
webu	web	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
návrh	návrh	k1gInSc1
konsorcia	konsorcium	k1gNnSc2
W	W	kA
<g/>
3	#num#	k4
<g/>
C	C	kA
<g/>
:	:	kIx,
evoluční	evoluční	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
World	Worlda	k1gFnPc2
Wide	Wid	k1gInSc2
Webu	web	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
mohou	moct	k5eAaImIp3nP
aplikace	aplikace	k1gFnPc1
ukládat	ukládat	k5eAaImF
<g/>
,	,	kIx,
vyměňovat	vyměňovat	k5eAaImF
si	se	k3xPyFc3
a	a	k8xC
používat	používat	k5eAaImF
strojově	strojově	k6eAd1
čitelné	čitelný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
distribuovaně	distribuovaně	k6eAd1
přes	přes	k7c4
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
uživatelům	uživatel	k1gMnPc3
pracovat	pracovat	k5eAaImF
s	s	k7c7
informacemi	informace	k1gFnPc7
efektivněji	efektivně	k6eAd2
a	a	k8xC
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
jistotou	jistota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Datový	datový	k2eAgInSc1d1
model	model	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
RDF	RDF	kA
je	být	k5eAaImIp3nS
k	k	k7c3
popisovanému	popisovaný	k2eAgInSc3d1
zdroji	zdroj	k1gInSc3
přiřadit	přiřadit	k5eAaPmF
výraz	výraz	k1gInSc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
podmět	podmět	k1gInSc1
–	–	k?
</s>
<s>
vlastnost	vlastnost	k1gFnSc1
–	–	k?
předmět	předmět	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
subjekt	subjekt	k1gInSc1
–	–	k?
predikát	predikát	k1gInSc1
–	–	k?
objekt	objekt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
výraz	výraz	k1gInSc4
se	se	k3xPyFc4
také	také	k9
používá	používat	k5eAaImIp3nS
termín	termín	k1gInSc4
trojice	trojice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jinak	jinak	k6eAd1
řečeno	říct	k5eAaPmNgNnS
RDF	RDF	kA
popisuje	popisovat	k5eAaImIp3nS
zdroj	zdroj	k1gInSc1
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
má	mít	k5eAaImIp3nS
nějaké	nějaký	k3yIgFnPc4
vlastnosti	vlastnost	k1gFnPc4
a	a	k8xC
tyto	tento	k3xDgFnPc4
vlastnosti	vlastnost	k1gFnPc4
mají	mít	k5eAaImIp3nP
</s>
<s>
odpovídající	odpovídající	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přičemž	přičemž	k6eAd1
podmět	podmět	k1gInSc1
definuje	definovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
o	o	k7c4
jaký	jaký	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
zdroj	zdroj	k1gInSc4
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
<g/>
,	,	kIx,
vlastnost	vlastnost	k1gFnSc1
určuje	určovat	k5eAaImIp3nS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
charakter	charakter	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
vzájemný	vzájemný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
podmětem	podmět	k1gInSc7
a	a	k8xC
předmětem	předmět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1
příklad	příklad	k1gInSc1
lze	lze	k6eAd1
ukázat	ukázat	k5eAaPmF
například	například	k6eAd1
na	na	k7c6
výroku	výrok	k1gInSc6
<g/>
:	:	kIx,
Obloha	obloha	k1gFnSc1
má	mít	k5eAaImIp3nS
modrou	modrý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
</s>
<s>
rozložení	rozložení	k1gNnSc1
do	do	k7c2
trojice	trojice	k1gFnSc2
pak	pak	k6eAd1
obloha	obloha	k1gFnSc1
je	být	k5eAaImIp3nS
podmětem	podmět	k1gInSc7
<g/>
,	,	kIx,
má	můj	k3xOp1gFnSc1
barvu	barva	k1gFnSc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
vlastnost	vlastnost	k1gFnSc4
a	a	k8xC
modrou	modrý	k2eAgFnSc4d1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
předmět	předmět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
grafické	grafický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
RDF	RDF	kA
grafu	graf	k1gInSc2
by	by	kYmCp3nP
pak	pak	k6eAd1
vlastnosti	vlastnost	k1gFnPc1
byly	být	k5eAaImAgFnP
jeho	jeho	k3xOp3gFnPc1
hrany	hrana	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
orientovány	orientovat	k5eAaBmNgFnP
od	od	k7c2
podmětu	podmět	k1gInSc2
k	k	k7c3
předmětu	předmět	k1gInSc3
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgInPc1
by	by	kYmCp3nP
tvořily	tvořit	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
vrcholy	vrchol	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lépe	dobře	k6eAd2
pochopit	pochopit	k5eAaPmF
řetězec	řetězec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vznikne	vzniknout	k5eAaPmIp3nS
při	při	k7c6
vytvoření	vytvoření	k1gNnSc6
trojice	trojice	k1gFnSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
i	i	k9
pomocí	pomocí	k7c2
konkrétního	konkrétní	k2eAgInSc2d1
příkladu	příklad	k1gInSc2
Erica	Ericus	k1gMnSc2
J.	J.	kA
Millera	Miller	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
použil	použít	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Níže	nízce	k6eAd2
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
trochu	trochu	k6eAd1
pozměněné	pozměněný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Máme	mít	k5eAaImIp1nP
dvě	dva	k4xCgNnPc4
prohlášení	prohlášení	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
)	)	kIx)
Babiččin	babiččin	k2eAgMnSc1d1
autor	autor	k1gMnSc1
je	být	k5eAaImIp3nS
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
)	)	kIx)
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
Babičky	babička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
odvodit	odvodit	k5eAaPmF
si	se	k3xPyFc3
obsah	obsah	k1gInSc4
sdělení	sdělení	k1gNnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
větných	větný	k2eAgInPc2d1
konstruktů	konstrukt	k1gInPc2
<g/>
,	,	kIx,
nesou	nést	k5eAaImIp3nP
obě	dva	k4xCgFnPc1
tato	tento	k3xDgNnPc1
prohlášení	prohlášení	k1gNnSc6
stejnou	stejný	k2eAgFnSc4d1
informaci	informace	k1gFnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
knihy	kniha	k1gFnSc2
Babička	babička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
stroj	stroj	k1gInSc4
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
obě	dva	k4xCgFnPc1
tato	tento	k3xDgNnPc1
sdělení	sdělení	k1gNnPc1
vnímána	vnímat	k5eAaImNgNnP
jako	jako	k9
dva	dva	k4xCgInPc4
rozdílné	rozdílný	k2eAgInPc4d1
řetězce	řetězec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
tedy	tedy	k9
obě	dva	k4xCgNnPc1
prohlášení	prohlášení	k1gNnPc1
rozložíme	rozložit	k5eAaPmIp1nP
do	do	k7c2
trojice	trojice	k1gFnSc2
subjekt	subjekt	k1gInSc1
–	–	k?
predikát	predikát	k1gInSc1
–	–	k?
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
dělá	dělat	k5eAaImIp3nS
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
vypadat	vypadat	k5eAaImF,k5eAaPmF
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
)	)	kIx)
"	"	kIx"
<g/>
Babička	babička	k1gFnSc1
<g/>
"	"	kIx"
=	=	kIx~
podmět	podmět	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
má	mít	k5eAaImIp3nS
autora	autor	k1gMnSc4
<g/>
"	"	kIx"
=	=	kIx~
vlastnost	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Boženu	Božena	k1gFnSc4
Němcovou	Němcová	k1gFnSc4
<g/>
"	"	kIx"
=	=	kIx~
předmět	předmět	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
)	)	kIx)
"	"	kIx"
<g/>
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
<g/>
"	"	kIx"
=	=	kIx~
podmět	podmět	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
<g/>
"	"	kIx"
=	=	kIx~
vlastnost	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Babičky	babička	k1gFnPc4
<g/>
"	"	kIx"
=	=	kIx~
předmět	předmět	k1gInSc1
</s>
<s>
V	v	k7c6
prvním	první	k4xOgInSc6
prohlášení	prohlášení	k1gNnSc6
je	být	k5eAaImIp3nS
podmětem	podmět	k1gInSc7
kniha	kniha	k1gFnSc1
Babička	babička	k1gFnSc1
v	v	k7c6
druhém	druhý	k4xOgInSc6
pak	pak	k6eAd1
autorka	autorka	k1gFnSc1
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
každému	každý	k3xTgInSc3
podmětu	podmět	k1gInSc3
lze	lze	k6eAd1
také	také	k9
přiřazovat	přiřazovat	k5eAaImF
různé	různý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
související	související	k2eAgNnSc1d1
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
povahou	povaha	k1gFnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
k	k	k7c3
autorovi	autor	k1gMnSc3
můžeme	moct	k5eAaImIp1nP
dodat	dodat	k5eAaPmF
kontaktní	kontaktní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nebo	nebo	k8xC
seznam	seznam	k1gInSc4
jeho	jeho	k3xOp3gNnPc2
dalších	další	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
knize	kniha	k1gFnSc3
pak	pak	k6eAd1
například	například	k6eAd1
počet	počet	k1gInSc1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
rok	rok	k1gInSc4
vydání	vydání	k1gNnSc2
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RDF	RDF	kA
syntaxe	syntaxe	k1gFnSc1
</s>
<s>
RDF	RDF	kA
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
pouze	pouze	k6eAd1
abstraktním	abstraktní	k2eAgInSc7d1
formátem	formát	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
pouze	pouze	k6eAd1
udává	udávat	k5eAaImIp3nS
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
způsobem	způsob	k1gInSc7
</s>
<s>
informace	informace	k1gFnPc1
o	o	k7c6
zdroji	zdroj	k1gInSc6
zapsat	zapsat	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
nemá	mít	k5eNaImIp3nS
definovanou	definovaný	k2eAgFnSc4d1
syntaxi	syntaxe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
není	být	k5eNaImIp3nS
samotné	samotný	k2eAgFnPc4d1
RDF	RDF	kA
v	v	k7c6
dokumentu	dokument	k1gInSc6
viditelné	viditelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
internetovém	internetový	k2eAgInSc6d1
prohlížeči	prohlížeč	k1gInSc6
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
není	být	k5eNaImIp3nS
poznat	poznat	k5eAaPmF
<g/>
,	,	kIx,
zdali	zdali	k8xS
dokument	dokument	k1gInSc1
RDF	RDF	kA
obsahuje	obsahovat	k5eAaImIp3nS
nebo	nebo	k8xC
ne	ne	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
zapsání	zapsání	k1gNnSc3
RDF	RDF	kA
do	do	k7c2
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
lidsky	lidsky	k6eAd1
a	a	k8xC
strojově	strojově	k6eAd1
čitelné	čitelný	k2eAgFnPc4d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
použít	použít	k5eAaPmF
různé	různý	k2eAgInPc4d1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojně	hojně	k6eAd1
používaným	používaný	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
díky	díky	k7c3
jeho	jeho	k3xOp3gFnSc3
oblíbenosti	oblíbenost	k1gFnSc3
a	a	k8xC
univerzálnosti	univerzálnost	k1gFnSc6
<g/>
,	,	kIx,
značkovací	značkovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
XML	XML	kA
(	(	kIx(
<g/>
Extensible	Extensible	k1gMnSc1
Markup	Markup	k1gMnSc1
Language	language	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
W3C	W3C	k1gFnPc2
pak	pak	k6eAd1
definovalo	definovat	k5eAaBmAgNnS
konkrétní	konkrétní	k2eAgFnSc4d1
syntaxi	syntaxe	k1gFnSc4
RDF	RDF	kA
<g/>
/	/	kIx~
<g/>
XML	XML	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vychází	vycházet	k5eAaImIp3nS
právě	právě	k9
ze	z	k7c2
zmíněného	zmíněný	k2eAgInSc2d1
XML	XML	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RDF	RDF	kA
schéma	schéma	k1gNnSc1
</s>
<s>
Pro	pro	k7c4
konkrétnější	konkrétní	k2eAgFnSc4d2
specifikaci	specifikace	k1gFnSc4
abstraktního	abstraktní	k2eAgMnSc2d1
RDF	RDF	kA
vznikl	vzniknout	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
rozšiřovací	rozšiřovací	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
RDF	RDF	kA
Schema	schema	k1gNnSc1
(	(	kIx(
<g/>
RDF	RDF	kA
<g/>
(	(	kIx(
<g/>
S	s	k7c7
<g/>
))	))	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
RDF	RDF	kA
<g/>
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednoduchý	jednoduchý	k2eAgInSc1d1
<g/>
,	,	kIx,
na	na	k7c6
XML	XML	kA
založený	založený	k2eAgInSc1d1
<g/>
,	,	kIx,
ontologický	ontologický	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
konkrétněji	konkrétně	k6eAd2
specifikovat	specifikovat	k5eAaBmF
vlastnosti	vlastnost	k1gFnPc4
přidružené	přidružený	k2eAgFnSc2d1
k	k	k7c3
jednotlivým	jednotlivý	k2eAgInPc3d1
RDF	RDF	kA
objektům	objekt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
definovat	definovat	k5eAaBmF
jejich	jejich	k3xOp3gFnPc4
možné	možný	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
<g/>
,	,	kIx,
popisovat	popisovat	k5eAaImF
vzájemné	vzájemný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
a	a	k8xC
tím	ten	k3xDgNnSc7
nám	my	k3xPp1nPc3
umožňuje	umožňovat	k5eAaImIp3nS
vkládat	vkládat	k5eAaImF
do	do	k7c2
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
sémantiku	sémantika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
vyjadřování	vyjadřování	k1gNnSc3
hierarchie	hierarchie	k1gFnSc2
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
systému	systém	k1gInSc2
tříd	třída	k1gFnPc2
a	a	k8xC
vlastností	vlastnost	k1gFnPc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
slovníky	slovník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
užitečné	užitečný	k2eAgFnPc1d1
<g/>
,	,	kIx,
musejí	muset	k5eAaImIp3nP
být	být	k5eAaImF
chápány	chápat	k5eAaImNgInP
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
jak	jak	k8xS,k8xC
autorem	autor	k1gMnSc7
<g/>
,	,	kIx,
tak	tak	k9
čtenářem	čtenář	k1gMnSc7
informace	informace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
RDF	RDF	kA
<g/>
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
umožňuje	umožňovat	k5eAaImIp3nS
k	k	k7c3
popisu	popis	k1gInSc3
použít	použít	k5eAaPmF
již	již	k6eAd1
existující	existující	k2eAgInPc4d1
slovníky	slovník	k1gInPc4
nebo	nebo	k8xC
definovat	definovat	k5eAaBmF
slovníky	slovník	k1gInPc1
nové	nový	k2eAgInPc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
rozšiřovat	rozšiřovat	k5eAaImF
možnosti	možnost	k1gFnPc4
popisování	popisování	k1gNnSc2
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Formát	formát	k1gInSc1
RDF	RDF	kA
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
pro	pro	k7c4
publikaci	publikace	k1gFnSc4
otevřených	otevřený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
Česká	český	k2eAgFnSc1d1
obchodní	obchodní	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
a	a	k8xC
Ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
pro	pro	k7c4
Centrální	centrální	k2eAgInSc4d1
registr	registr	k1gInSc4
dotací	dotace	k1gFnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Použitá	použitý	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
1	#num#	k4
2	#num#	k4
NEEDLEMAN	NEEDLEMAN	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
H.	H.	kA
RDF	RDF	kA
<g/>
:	:	kIx,
THE	THE	kA
RESOURCE	RESOURCE	kA
DESCRIPTION	DESCRIPTION	kA
FRAMEWORK	FRAMEWORK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serials	Serials	k1gInSc1
Review	Review	k1gFnSc2
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
27	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
98	#num#	k4
<g/>
-	-	kIx~
<g/>
7913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
<g/>
:	:	kIx,
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
1	#num#	k4
2	#num#	k4
3	#num#	k4
MYNARZ	MYNARZ	kA
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RDFa	RDF	k1gInSc2
Intro	Intro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GitHub	GitHub	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://github.com/OPLZZ/datamodelling/	https://github.com/OPLZZ/datamodelling/	k?
</s>
<s>
wiki	wiki	k1gNnSc1
<g/>
/	/	kIx~
<g/>
RDFa-intro	RDFa-intro	k1gNnSc4
<g/>
.1	.1	k4
2	#num#	k4
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
J.	J.	kA
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc4
Framework	Framework	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Library	Librar	k1gInPc1
Administration	Administration	k1gInSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
34	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
245	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
193	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
826	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
↑	↑	k?
RDF	RDF	kA
<g/>
/	/	kIx~
<g/>
XML	XML	kA
Syntax	syntax	k1gFnSc1
Specification	Specification	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
W3C	W3C	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.w3.org/TR/REC-rdfsyntax/%5B%5D.	http://www.w3.org/TR/REC-rdfsyntax/%5B%5D.	k4
<g/>
↑	↑	k?
Principy	princip	k1gInPc1
sémantického	sémantický	k2eAgInSc2d1
webu	web	k1gInSc2
<g/>
:	:	kIx,
Ontologie	ontologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VŠE	všechen	k3xTgNnSc1
<g/>
:	:	kIx,
studijní	studijní	k2eAgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://vse.stencek.com/semanticky-web/ch03s05.html.	http://vse.stencek.com/semanticky-web/ch03s05.html.	k4
<g/>
↑	↑	k?
VØ	VØ	k1gMnSc1
<g/>
,	,	kIx,
Torunn	Torunn	k1gMnSc1
<g/>
,	,	kIx,
Fei	Fei	k1gMnSc1
LIU	LIU	kA
a	a	k8xC
Sheng-Uei	Sheng-Uei	k1gNnSc7
GUAN	GUAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Layered	Layered	k1gInSc1
RDF	RDF	kA
<g/>
:	:	kIx,
An	An	k1gMnSc1
object-oriented	object-oriented	k1gMnSc1
approach	approach	k1gMnSc1
to	ten	k3xDgNnSc4
web	web	k1gInSc4
information	information	k1gInSc4
</s>
<s>
representation	representation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
Intelligence	Intelligence	k1gFnSc2
&	&	k?
Agent	agent	k1gMnSc1
Systems	Systems	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
281	#num#	k4
<g/>
-	-	kIx~
<g/>
301	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1570	#num#	k4
<g/>
-	-	kIx~
<g/>
1263	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
</s>
<s>
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
↑	↑	k?
IS	IS	kA
CEDR	cedr	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
cedropendata	cedropendata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mfcr	mfcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BOUDA	Bouda	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sémantický	sémantický	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
,	,	kIx,
standardy	standard	k1gInPc1
RDF	RDF	kA
a	a	k8xC
Topic	Topic	k1gMnSc1
Maps	Maps	k1gInSc4
v	v	k7c6
Kurzu	kurz	k1gInSc6
Digitální	digitální	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
(	(	kIx(
<g/>
OUC	OUC	kA
<g/>
,	,	kIx,
Oslo	Oslo	k1gNnSc1
<g/>
/	/	kIx~
<g/>
NO	no	k9
<g/>
,	,	kIx,
ERASMUS	ERASMUS	kA
<g/>
,	,	kIx,
podzim	podzim	k1gInSc4
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inflow	Inflow	k1gFnSc1
<g/>
:	:	kIx,
information	information	k1gInSc1
journal	journat	k5eAaImAgInS,k5eAaPmAgInS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
2	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.inflow.cz/semanticky-web-standardy-rdf-topic-maps-v-kurzu-digitalni-dokumenty-ouc-oslono-erasmus-podzim-2009.	http://www.inflow.cz/semanticky-web-standardy-rdf-topic-maps-v-kurzu-digitalni-dokumenty-ouc-oslono-erasmus-podzim-2009.	k4
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
9736	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HORSÁK	HORSÁK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Struktura	struktura	k1gFnSc1
RDF	RDF	kA
pro	pro	k7c4
metadata	metade	k1gNnPc4
<g/>
,	,	kIx,
její	její	k3xOp3gInSc1
vývoj	vývoj	k1gInSc1
a	a	k8xC
perspektivy	perspektiva	k1gFnPc1
aplikace	aplikace	k1gFnSc2
v	v	k7c6
digitálních	digitální	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
(	(	kIx(
<g/>
Mgr.	Mgr.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
informačních	informační	k2eAgFnPc2d1
studií	studie	k1gFnPc2
a	a	k8xC
knihovnictví	knihovnictví	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Check	Check	k1gInSc1
and	and	k?
Visualize	Visualize	k1gFnSc2
your	your	k1gMnSc1
RDF	RDF	kA
documents	documents	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
W3C	W3C	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.w3.org/TR/REC-rdf-syntax/.	http://www.w3.org/TR/REC-rdf-syntax/.	k4
</s>
<s>
KITCHAROENSAKKUL	KITCHAROENSAKKUL	kA
<g/>
,	,	kIx,
Supanat	Supanat	k1gMnSc1
a	a	k8xC
Vilas	Vilas	k1gMnSc1
WUWONGSE	WUWONGSE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Towards	Towards	k1gInSc4
a	a	k8xC
Unified	Unified	k1gInSc4
Version	Version	k1gInSc1
Model	model	k1gInSc1
Using	Using	k1gInSc1
The	The	k1gFnSc2
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc4
Framework	Framework	k1gInSc1
(	(	kIx(
<g/>
RDF	RDF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Software	software	k1gInSc1
Engineering	Engineering	k1gInSc1
&	&	k?
Knowledge	Knowledge	k1gInSc1
Engineering	Engineering	k1gInSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
675	#num#	k4
<g/>
-	-	kIx~
<g/>
701	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
218	#num#	k4
<g/>
-	-	kIx~
<g/>
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
<g/>
:	:	kIx,
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
J.	J.	kA
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc4
Framework	Framework	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Library	Librar	k1gInPc1
Administration	Administration	k1gInSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
34	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
245	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
193	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
826	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
<g/>
:	:	kIx,
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
</s>
<s>
MYNARZ	MYNARZ	kA
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RDFa	RDF	k1gInSc2
Intro	Intro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GitHub	GitHub	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://github.com/OPLZZ/data-modelling/wiki/RDFa-intro.	https://github.com/OPLZZ/data-modelling/wiki/RDFa-intro.	k?
</s>
<s>
NEEDLEMAN	NEEDLEMAN	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
H.	H.	kA
RDF	RDF	kA
<g/>
:	:	kIx,
THE	THE	kA
RESOURCE	RESOURCE	kA
DESCRIPTION	DESCRIPTION	kA
FRAMEWORK	FRAMEWORK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serials	Serials	k1gInSc1
Review	Review	k1gFnSc2
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
27	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
-	-	kIx~
<g/>
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
98	#num#	k4
<g/>
-	-	kIx~
<g/>
7913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
<g/>
:	:	kIx,
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
</s>
<s>
Principy	princip	k1gInPc1
sémantického	sémantický	k2eAgInSc2d1
webu	web	k1gInSc2
<g/>
:	:	kIx,
RDF	RDF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
VŠE	všechen	k3xTgNnSc1
<g/>
:	:	kIx,
studijní	studijní	k2eAgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://vse.stencek.com/semanticky-web/ch03s05.html.	http://vse.stencek.com/semanticky-web/ch03s05.html.	k4
</s>
<s>
RDF	RDF	kA
Tutorial	Tutorial	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
W	W	kA
<g/>
3	#num#	k4
<g/>
schools	schools	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
1999	#num#	k4
-	-	kIx~
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://web.archive.org/web/20130605164237/http://www.w3schools.com/rdf/rdf_schema.asp.	https://web.archive.org/web/20130605164237/http://www.w3schools.com/rdf/rdf_schema.asp.	k4
</s>
<s>
RDF	RDF	kA
<g/>
/	/	kIx~
<g/>
XML	XML	kA
Syntax	syntax	k1gFnSc1
Specification	Specification	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
W3C	W3C	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.w3.org/TR/REC-rdf-syntax/.	http://www.w3.org/TR/REC-rdf-syntax/.	k4
</s>
<s>
Reprezentácia	Reprezentácia	k1gFnSc1
dát	dát	k5eAaPmF
pomocou	pomocá	k1gFnSc4
RDF	RDF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ĎURECH	ĎURECH	kA
<g/>
,	,	kIx,
Juraj	Juraj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapustik	Kapustika	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
-	-	kIx~
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www2.fiit.stuba.sk/~kapustik/ZS/Clanky0708/durech/index.html.	http://www2.fiit.stuba.sk/~kapustik/ZS/Clanky0708/durech/index.html.	k4
</s>
<s>
Resource	Resourka	k1gFnSc3
Definiction	Definiction	k1gInSc1
Framework	Framework	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GlobalSemantic	GlobalSemantice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
Net	Net	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://sites.google.com/a/globalsemantic.net/gsn/swp/resource-definiction-framework.	https://sites.google.com/a/globalsemantic.net/gsn/swp/resource-definiction-framework.	k?
</s>
<s>
Resource	Resourka	k1gFnSc3
Descripiton	Descripiton	k1gInSc1
Framework	Framework	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
W3C	W3C	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
http://www.w3.org/RDF/.	http://www.w3.org/RDF/.	k4
</s>
<s>
Visual	Visual	k1gMnSc1
RDF	RDF	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://web.archive.org/web/20160824193704/https://graves.cl/visualRDF/?url=http%3A%2F%2Fgraves.cl%2FvisualRDF%2F.	https://web.archive.org/web/20160824193704/https://graves.cl/visualRDF/?url=http%3A%2F%2Fgraves.cl%2FvisualRDF%2F.	k4
</s>
<s>
VØ	VØ	k?
<g/>
,	,	kIx,
Torunn	Torunn	k1gMnSc1
<g/>
,	,	kIx,
Fei	Fei	k1gMnSc1
LIU	LIU	kA
a	a	k8xC
Sheng-Uei	Sheng-Uei	k1gNnSc7
GUAN	GUAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Layered	Layered	k1gInSc1
RDF	RDF	kA
<g/>
:	:	kIx,
An	An	k1gMnSc1
object-oriented	object-oriented	k1gMnSc1
approach	approach	k1gMnSc1
to	ten	k3xDgNnSc4
web	web	k1gInSc4
information	information	k1gInSc1
representation	representation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
Intelligence	Intelligence	k1gFnSc2
&	&	k?
Agent	agent	k1gMnSc1
Systems	Systems	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
281	#num#	k4
<g/>
-	-	kIx~
<g/>
301	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1570	#num#	k4
<g/>
-	-	kIx~
<g/>
1263	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
komerčně	komerčně	k6eAd1
z	z	k7c2
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc4
Framework	Framework	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Výukový	výukový	k2eAgInSc1d1
kurs	kurs	k1gInSc1
RDF	RDF	kA
ve	v	k7c6
Wikiverzitě	Wikiverzita	k1gFnSc6
</s>
<s>
Resource	Resourka	k1gFnSc3
Description	Description	k1gInSc1
Framework	Framework	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
konsorcia	konsorcium	k1gNnSc2
W3	W3	k1gMnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Informační	informační	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
knihovnictví	knihovnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4737512-7	4737512-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2003010124	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2003010124	#num#	k4
</s>
