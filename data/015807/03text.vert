<s>
Bulbul	bulbul	k1gMnSc1wB
rudooký	rudooký	k2eAgMnSc1d1
</s>
<s>
Bulbul	bulbul	k1gMnSc1wB
rudooký	rudooký	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
letci	letec	k1gMnSc3
(	(	kIx(
<g/>
Neognathae	Neognathae	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pěvci	pěvec	k1gMnPc1
(	(	kIx(
<g/>
Passeriformes	Passeriformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
bulbulovití	bulbulovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Pycnonotidae	Pycnonotidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
bulbul	bulbul	k1gMnSc1wB
(	(	kIx(
<g/>
Pycnonotus	Pycnonotus	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Pycnonotus	Pycnonotus	k1gInSc1
nigricans	nigricans	k1gInSc1
<g/>
(	(	kIx(
<g/>
Vieillot	Vieillot	k1gInSc1
<g/>
,	,	kIx,
1818	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bulbul	bulbul	k1gMnSc1wB
rudooký	rudooký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pycnonotus	Pycnonotus	k1gInSc1
nigricans	nigricans	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
asi	asi	k9
20	#num#	k4
cm	cm	kA
velký	velký	k2eAgInSc4d1
druh	druh	k1gInSc4
zpěvného	zpěvný	k2eAgMnSc2d1
ptáka	pták	k1gMnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
bulbulovitých	bulbulovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Pycnonotidae	Pycnonotidae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Má	mít	k5eAaImIp3nS
černou	černý	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
a	a	k8xC
zobák	zobák	k1gInSc4
<g/>
,	,	kIx,
tmavě	tmavě	k6eAd1
hnědý	hnědý	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
<g/>
,	,	kIx,
světle	světle	k6eAd1
hnědou	hnědý	k2eAgFnSc4d1
hruď	hruď	k1gFnSc4
a	a	k8xC
boky	boka	k1gFnPc4
a	a	k8xC
světlé	světlý	k2eAgNnSc4d1
břicho	břicho	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazným	výrazný	k2eAgInSc7d1
znakem	znak	k1gInSc7
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
rudě	rudě	k6eAd1
červený	červený	k2eAgInSc1d1
kroužek	kroužek	k1gInSc1
kolem	kolem	k7c2
očí	oko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Areál	areál	k1gInSc4
rozšíření	rozšíření	k1gNnSc2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
Angolu	Angola	k1gFnSc4
<g/>
,	,	kIx,
Botswanu	Botswana	k1gFnSc4
<g/>
,	,	kIx,
Lesotho	Lesot	k1gMnSc4
<g/>
,	,	kIx,
Namibii	Namibie	k1gFnSc4
<g/>
,	,	kIx,
Jihoafrickou	jihoafrický	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
,	,	kIx,
Svazijsko	Svazijsko	k1gNnSc4
<g/>
,	,	kIx,
Zambii	Zambie	k1gFnSc4
a	a	k8xC
Zimbabwe	Zimbabwe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
biotopem	biotop	k1gInSc7
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
suché	suchý	k2eAgFnPc1d1
savany	savana	k1gFnPc1
a	a	k8xC
subtropické	subtropický	k2eAgFnPc1d1
nebo	nebo	k8xC
tropické	tropický	k2eAgFnPc1d1
suché	suchý	k2eAgFnPc1d1
křovinaté	křovinatý	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Black-fronted	Black-fronted	k1gMnSc1
Bulbul	bulbul	k1gMnSc1wB
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Pycnonotus	Pycnonotus	k1gInSc1
nigricans	nigricans	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zvíře	zvíře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
David	David	k1gMnSc1
Burnie	Burnie	k1gFnSc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
;	;	kIx,
překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Šmaha	Šmaha	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8024208628	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
</s>
