<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
klifové	klifový	k2eAgNnSc4d1	klifový
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
plážové	plážový	k2eAgNnSc4d1	plážové
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
stavbou	stavba	k1gFnSc7	stavba
a	a	k8xC	a
vertikální	vertikální	k2eAgFnSc7d1	vertikální
členitostí	členitost	k1gFnSc7	členitost
<g/>
.	.	kIx.	.
</s>
