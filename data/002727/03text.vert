<s>
Boubín	Boubín	k1gInSc1	Boubín
(	(	kIx(	(
<g/>
1362	[number]	k4	1362
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Kubany	Kubana	k1gFnPc4	Kubana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Boubínské	boubínský	k2eAgFnSc6d1	Boubínská
hornatině	hornatina	k1gFnSc6	hornatina
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgInSc1	pátý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Šumavy	Šumava	k1gFnSc2	Šumava
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
ležící	ležící	k2eAgFnSc7d1	ležící
uvnitř	uvnitř	k7c2	uvnitř
území	území	k1gNnSc2	území
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
mimo	mimo	k7c4	mimo
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
takovou	takový	k3xDgFnSc7	takový
horou	hora	k1gFnSc7	hora
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
3,5	[number]	k4	3,5
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Kubovy	Kubův	k2eAgFnSc2d1	Kubova
Hutě	huť	k1gFnSc2	huť
<g/>
,	,	kIx,	,
vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
část	část	k1gFnSc1	část
náleží	náležet	k5eAaImIp3nS	náležet
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Včelná	včelný	k2eAgNnPc4d1	Včelné
pod	pod	k7c7	pod
Boubínem	Boubín	k1gInSc7	Boubín
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
Buk	buk	k1gInSc1	buk
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblý	oblý	k2eAgInSc1d1	oblý
masív	masív	k1gInSc1	masív
Boubína	Boubín	k1gInSc2	Boubín
tvoří	tvořit	k5eAaImIp3nS	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostře	ostro	k6eAd1	ostro
tvarovaným	tvarovaný	k2eAgInSc7d1	tvarovaný
Bobíkem	Bobík	k1gInSc7	Bobík
(	(	kIx(	(
<g/>
1264	[number]	k4	1264
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
siluetu	silueta	k1gFnSc4	silueta
Šumavy	Šumava	k1gFnSc2	Šumava
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
i	i	k9	i
ze	z	k7c2	z
značně	značně	k6eAd1	značně
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Masiv	masiv	k1gInSc1	masiv
Boubína	Boubín	k1gInSc2	Boubín
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
především	především	k6eAd1	především
horninami	hornina	k1gFnPc7	hornina
moldanubika	moldanubikum	k1gNnPc1	moldanubikum
-	-	kIx~	-
migmatity	migmatit	k1gInPc1	migmatit
a	a	k8xC	a
pararulami	pararula	k1gFnPc7	pararula
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
jméno	jméno	k1gNnSc1	jméno
hory	hora	k1gFnSc2	hora
(	(	kIx(	(
<g/>
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Bubyn	Bubyna	k1gFnPc2	Bubyna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
latinské	latinský	k2eAgFnSc6d1	Latinská
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1398	[number]	k4	1398
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
Búbův	Búbův	k2eAgMnSc1d1	Búbův
(	(	kIx(	(
<g/>
Boubův	Boubův	k2eAgInSc1d1	Boubův
<g/>
)	)	kIx)	)
vrch	vrch	k1gInSc1	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgNnSc1d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nazývalo	nazývat	k5eAaImAgNnS	nazývat
horu	hora	k1gFnSc4	hora
Kuboberg	Kuboberg	k1gInSc1	Kuboberg
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
osady	osada	k1gFnSc2	osada
Kubohütten	Kubohütten	k2eAgMnSc1d1	Kubohütten
(	(	kIx(	(
<g/>
Kubova	Kubův	k2eAgFnSc1d1	Kubova
Huť	huť	k1gFnSc1	huť
<g/>
)	)	kIx)	)
založené	založený	k2eAgFnPc4d1	založená
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
uměle	uměle	k6eAd1	uměle
pozměněn	pozměnit	k5eAaPmNgInS	pozměnit
na	na	k7c4	na
Kubany	Kuban	k1gMnPc4	Kuban
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
hora	hora	k1gFnSc1	hora
Kubanova	Kubanův	k2eAgFnSc1d1	Kubanův
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
Boubínského	boubínský	k2eAgInSc2d1	boubínský
pralesa	prales	k1gInSc2	prales
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
000	[number]	k4	000
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
měl	mít	k5eAaImAgInS	mít
prales	prales	k1gInSc1	prales
i	i	k8xC	i
hora	hora	k1gFnSc1	hora
mnohá	mnohé	k1gNnPc1	mnohé
využití	využití	k1gNnSc1	využití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Boubín	Boubín	k1gInSc1	Boubín
používal	používat	k5eAaImAgInS	používat
především	především	k9	především
ke	k	k7c3	k
splavování	splavování	k1gNnSc3	splavování
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
tudy	tudy	k6eAd1	tudy
pašerácké	pašerácký	k2eAgFnPc1d1	Pašerácká
stezky	stezka	k1gFnPc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
umožnění	umožnění	k1gNnSc4	umožnění
plavby	plavba	k1gFnSc2	plavba
dřeva	dřevo	k1gNnSc2	dřevo
z	z	k7c2	z
boubínských	boubínský	k2eAgInPc2d1	boubínský
lesů	les	k1gInPc2	les
do	do	k7c2	do
sklárny	sklárna	k1gFnSc2	sklárna
v	v	k7c6	v
Lenoře	Lenora	k1gFnSc6	Lenora
vybudována	vybudován	k2eAgFnSc1d1	vybudována
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
Boubína	Boubín	k1gInSc2	Boubín
(	(	kIx(	(
<g/>
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
925	[number]	k4	925
m	m	kA	m
<g/>
)	)	kIx)	)
splavovací	splavovací	k2eAgFnSc1d1	splavovací
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
Kaplickém	Kaplický	k2eAgInSc6d1	Kaplický
potoce	potok	k1gInSc6	potok
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Boubínské	boubínský	k2eAgNnSc4d1	Boubínské
jezírko	jezírko	k1gNnSc4	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
sloužilo	sloužit	k5eAaImAgNnS	sloužit
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Boubínského	boubínský	k2eAgInSc2d1	boubínský
pralesa	prales	k1gInSc2	prales
rezervací	rezervace	k1gFnPc2	rezervace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
měl	mít	k5eAaImAgMnS	mít
schwarzenberský	schwarzenberský	k2eAgMnSc1d1	schwarzenberský
lesmistr	lesmistr	k1gMnSc1	lesmistr
Josef	Josef	k1gMnSc1	Josef
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gFnSc4	jehož
počest	počest	k1gFnSc4	počest
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Srním	srní	k2eAgInSc6d1	srní
vrchu	vrch	k1gInSc6	vrch
(	(	kIx(	(
<g/>
1	[number]	k4	1
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
Boubína	Boubín	k1gInSc2	Boubín
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1296	[number]	k4	1296
m	m	kA	m
<g/>
)	)	kIx)	)
postaven	postavit	k5eAaPmNgInS	postavit
pomník	pomník	k1gInSc1	pomník
Johnův	Johnův	k2eAgInSc1d1	Johnův
kámen	kámen	k1gInSc4	kámen
označující	označující	k2eAgInSc4d1	označující
trojmezí	trojmezí	k1gNnSc2	trojmezí
tří	tři	k4xCgNnPc2	tři
bývalých	bývalý	k2eAgNnPc2d1	bývalé
polesí	polesí	k1gNnPc2	polesí
-	-	kIx~	-
Zátoň	Zátoň	k1gFnSc1	Zátoň
<g/>
,	,	kIx,	,
Kubova	Kubův	k2eAgFnSc1d1	Kubova
Huť	huť	k1gFnSc1	huť
a	a	k8xC	a
Včelná	včelný	k2eAgFnSc1d1	Včelná
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Boubín	Boubín	k1gInSc1	Boubín
(	(	kIx(	(
<g/>
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
znovu	znovu	k6eAd1	znovu
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
,	,	kIx,	,
21	[number]	k4	21
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
nabízející	nabízející	k2eAgInSc1d1	nabízející
znamenitý	znamenitý	k2eAgInSc4d1	znamenitý
rozhled	rozhled	k1gInSc4	rozhled
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
panorama	panorama	k1gNnSc4	panorama
Šumavy	Šumava	k1gFnSc2	Šumava
(	(	kIx(	(
<g/>
Libín	Libín	k1gInSc1	Libín
<g/>
,	,	kIx,	,
Kleť	Kleť	k1gFnSc1	Kleť
<g/>
,	,	kIx,	,
Chlum	chlum	k1gInSc1	chlum
<g/>
,	,	kIx,	,
Bobík	Bobík	k1gInSc1	Bobík
<g/>
,	,	kIx,	,
Smrčina	smrčina	k1gFnSc1	smrčina
<g/>
,	,	kIx,	,
Plechý	plechý	k2eAgInSc1d1	plechý
<g/>
,	,	kIx,	,
Stožec	Stožec	k1gInSc1	Stožec
<g/>
,	,	kIx,	,
Třístoličník	Třístoličník	k1gInSc1	Třístoličník
<g/>
,	,	kIx,	,
Luzný	luzný	k2eAgInSc1d1	luzný
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Poledník	poledník	k1gInSc1	poledník
<g/>
)	)	kIx)	)
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
Vimperk	Vimperk	k1gInSc1	Vimperk
<g/>
,	,	kIx,	,
Brdy	Brdy	k1gInPc1	Brdy
<g/>
,	,	kIx,	,
JE	být	k5eAaImIp3nS	být
Temelín	Temelín	k1gInSc1	Temelín
<g/>
,	,	kIx,	,
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
)	)	kIx)	)
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobré	dobrý	k2eAgFnPc4d1	dobrá
viditelnosti	viditelnost	k1gFnPc4	viditelnost
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
Alpy	Alpy	k1gFnPc4	Alpy
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dachstein	Dachstein	k1gInSc1	Dachstein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejvýše	nejvýše	k6eAd1	nejvýše
položenou	položený	k2eAgFnSc4d1	položená
rozhlednu	rozhledna	k1gFnSc4	rozhledna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
nejvýše	nejvýše	k6eAd1	nejvýše
položenou	položený	k2eAgFnSc4d1	položená
rozhlednu	rozhledna	k1gFnSc4	rozhledna
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dřevěném	dřevěný	k2eAgNnSc6d1	dřevěné
schodišti	schodiště	k1gNnSc6	schodiště
(	(	kIx(	(
<g/>
109	[number]	k4	109
schodů	schod	k1gInPc2	schod
<g/>
)	)	kIx)	)
umístěném	umístěný	k2eAgInSc6d1	umístěný
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
turista	turista	k1gMnSc1	turista
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
na	na	k7c4	na
ochoz	ochoz	k1gInSc4	ochoz
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
nadzemním	nadzemní	k2eAgNnSc6d1	nadzemní
podlaží	podlaží	k1gNnSc6	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
základně	základna	k1gFnSc6	základna
široká	široký	k2eAgFnSc1d1	široká
7	[number]	k4	7
×	×	k?	×
7	[number]	k4	7
m	m	kA	m
a	a	k8xC	a
ve	v	k7c6	v
vrcholové	vrcholový	k2eAgFnSc6d1	vrcholová
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
4	[number]	k4	4
×	×	k?	×
4	[number]	k4	4
m.	m.	k?	m.
Rozhledna	rozhledna	k1gFnSc1	rozhledna
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
rozhledny	rozhledna	k1gFnSc2	rozhledna
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
celkově	celkově	k6eAd1	celkově
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
firmou	firma	k1gFnSc7	firma
SMP	SMP	kA	SMP
Construction	Construction	k1gInSc1	Construction
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
stavebního	stavební	k2eAgNnSc2d1	stavební
povolení	povolení	k1gNnSc2	povolení
nesměla	smět	k5eNaImAgFnS	smět
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
žádná	žádný	k3yNgFnSc1	žádný
těžká	těžký	k2eAgFnSc1d1	těžká
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
60	[number]	k4	60
tun	tuna	k1gFnPc2	tuna
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
dopraveno	dopravit	k5eAaPmNgNnS	dopravit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Boubína	Boubín	k1gInSc2	Boubín
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
lanovkou	lanovka	k1gFnSc7	lanovka
nebo	nebo	k8xC	nebo
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Boubín	Boubín	k1gInSc1	Boubín
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgMnSc1d1	přístupný
z	z	k7c2	z
několika	několik	k4yIc2	několik
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgFnSc1d3	nejkratší
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Kubovy	Kubův	k2eAgFnSc2d1	Kubova
Huti	huť	k1gFnSc2	huť
po	po	k7c6	po
modré	modrý	k2eAgFnSc6d1	modrá
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Boubín	Boubín	k1gInSc4	Boubín
vede	vést	k5eAaImIp3nS	vést
též	též	k9	též
červená	červený	k2eAgFnSc1d1	červená
značka	značka	k1gFnSc1	značka
z	z	k7c2	z
Vimperka	Vimperk	k1gInSc2	Vimperk
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
značka	značka	k1gFnSc1	značka
ze	z	k7c2	z
Šumavských	šumavský	k2eAgFnPc2d1	Šumavská
Hoštic	Hoštice	k1gFnPc2	Hoštice
přes	přes	k7c4	přes
Včelnou	včelný	k2eAgFnSc4d1	Včelná
pod	pod	k7c7	pod
Boubínem	Boubín	k1gInSc7	Boubín
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
značka	značka	k1gFnSc1	značka
z	z	k7c2	z
Volar	Volara	k1gFnPc2	Volara
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
značka	značka	k1gFnSc1	značka
z	z	k7c2	z
Lenory	Lenora	k1gFnSc2	Lenora
přes	přes	k7c4	přes
Zátoň	Zátoň	k1gFnSc4	Zátoň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
otevření	otevření	k1gNnSc6	otevření
rozhledny	rozhledna	k1gFnSc2	rozhledna
Správa	správa	k1gFnSc1	správa
NP	NP	kA	NP
a	a	k8xC	a
CHKO	CHKO	kA	CHKO
Šumava	Šumava	k1gFnSc1	Šumava
dočasně	dočasně	k6eAd1	dočasně
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
přístupové	přístupový	k2eAgFnSc2d1	přístupová
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Boubína	Boubín	k1gInSc2	Boubín
až	až	k9	až
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
provizorní	provizorní	k2eAgFnSc4d1	provizorní
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
znovuotevřena	znovuotevřen	k2eAgFnSc1d1	znovuotevřen
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
rozcestí	rozcestí	k1gNnSc2	rozcestí
Na	na	k7c6	na
křížkách	křížkách	k?	křížkách
-	-	kIx~	-
vrchol	vrchol	k1gInSc1	vrchol
Boubína	Boubín	k1gInSc2	Boubín
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
vedou	vést	k5eAaImIp3nP	vést
společně	společně	k6eAd1	společně
červená	červený	k2eAgFnSc1d1	červená
značka	značka	k1gFnSc1	značka
z	z	k7c2	z
Volar	Volara	k1gFnPc2	Volara
a	a	k8xC	a
modrá	modrat	k5eAaImIp3nS	modrat
z	z	k7c2	z
Lenory	Lenora	k1gFnSc2	Lenora
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
zůstaly	zůstat	k5eAaPmAgInP	zůstat
uzavřeny	uzavřít	k5eAaPmNgInP	uzavřít
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
úseky	úsek	k1gInPc1	úsek
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
tří	tři	k4xCgFnPc2	tři
turistických	turistický	k2eAgFnPc2d1	turistická
stezek	stezka	k1gFnPc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
značka	značka	k1gFnSc1	značka
z	z	k7c2	z
Kubovy	Kubův	k2eAgFnSc2d1	Kubova
Hutě	huť	k1gFnSc2	huť
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
pod	pod	k7c7	pod
Johnovým	Johnův	k2eAgInSc7d1	Johnův
kamenem	kámen	k1gInSc7	kámen
na	na	k7c4	na
náhradní	náhradní	k2eAgFnPc4d1	náhradní
zeleně	zeleň	k1gFnPc4	zeleň
značenou	značený	k2eAgFnSc4d1	značená
trasu	trasa	k1gFnSc4	trasa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
lesní	lesní	k2eAgFnSc6d1	lesní
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
napojují	napojovat	k5eAaImIp3nP	napojovat
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
i	i	k8xC	i
stezky	stezka	k1gFnPc4	stezka
z	z	k7c2	z
Vimperka	Vimperk	k1gInSc2	Vimperk
a	a	k8xC	a
Šumavských	šumavský	k2eAgFnPc2d1	Šumavská
Hoštic	Hoštice	k1gFnPc2	Hoštice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
závěr	závěr	k1gInSc1	závěr
výstupové	výstupový	k2eAgFnSc2d1	výstupová
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
Boubín	Boubín	k1gInSc4	Boubín
přeměněn	přeměněn	k2eAgInSc4d1	přeměněn
na	na	k7c4	na
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
chodník	chodník	k1gInSc4	chodník
se	s	k7c7	s
zábradlím	zábradlí	k1gNnSc7	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholové	vrcholový	k2eAgFnSc6d1	vrcholová
plošině	plošina	k1gFnSc6	plošina
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
chráněný	chráněný	k2eAgInSc1d1	chráněný
trigonometrický	trigonometrický	k2eAgInSc1d1	trigonometrický
bod	bod	k1gInSc1	bod
I.	I.	kA	I.
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
pamětní	pamětní	k2eAgInSc4d1	pamětní
žulový	žulový	k2eAgInSc4d1	žulový
obelisk	obelisk	k1gInSc4	obelisk
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Kardinal	Kardinal	k1gMnSc1	Kardinal
Friedrich	Friedrich	k1gMnSc1	Friedrich
Fürst	Fürst	k1gMnSc1	Fürst
von	von	k1gInSc4	von
Schwarzenberg	Schwarzenberg	k1gInSc1	Schwarzenberg
3	[number]	k4	3
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1867	[number]	k4	1867
<g/>
"	"	kIx"	"
připomínající	připomínající	k2eAgFnSc4d1	připomínající
návštěvu	návštěva	k1gFnSc4	návštěva
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
1,2	[number]	k4	1,2
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1135	[number]	k4	1135
m	m	kA	m
se	se	k3xPyFc4	se
na	na	k7c6	na
Lukenské	Lukenský	k2eAgFnSc6d1	Lukenská
cestě	cesta	k1gFnSc6	cesta
nalézá	nalézat	k5eAaImIp3nS	nalézat
místo	místo	k1gNnSc4	místo
zvané	zvaný	k2eAgNnSc4d1	zvané
Křížová	Křížová	k1gFnSc1	Křížová
smrč	smrč	k1gFnSc1	smrč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kamenný	kamenný	k2eAgInSc1d1	kamenný
pomníček	pomníček	k1gInSc1	pomníček
připomínající	připomínající	k2eAgInSc4d1	připomínající
strom	strom	k1gInSc4	strom
"	"	kIx"	"
<g/>
Křížovou	křížový	k2eAgFnSc4d1	křížová
smrč	smrč	k1gFnSc4	smrč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
stával	stávat	k5eAaImAgInS	stávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
395	[number]	k4	395
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
57,72	[number]	k4	57,72
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trojhranný	trojhranný	k2eAgInSc1d1	trojhranný
kamenný	kamenný	k2eAgInSc1d1	kamenný
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
rovněž	rovněž	k9	rovněž
připomíná	připomínat	k5eAaImIp3nS	připomínat
geografický	geografický	k2eAgInSc4d1	geografický
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
bývalé	bývalý	k2eAgNnSc1d1	bývalé
polesí	polesí	k1gNnSc4	polesí
včelenské	včelenský	k2eAgNnSc4d1	včelenský
<g/>
,	,	kIx,	,
zátoňské	zátoňský	k2eAgNnSc4d1	zátoňský
a	a	k8xC	a
mlynařovické	mlynařovický	k2eAgNnSc4d1	mlynařovický
<g/>
.	.	kIx.	.
</s>
