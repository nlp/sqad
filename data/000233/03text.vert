<s>
Antiperle	Antiperle	k6eAd1	Antiperle
jsou	být	k5eAaImIp3nP	být
mentolové	mentolový	k2eAgInPc4d1	mentolový
bonbony	bonbon	k1gInPc4	bonbon
přibližného	přibližný	k2eAgInSc2d1	přibližný
tvaru	tvar	k1gInSc2	tvar
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
vyráběné	vyráběný	k2eAgFnSc2d1	vyráběná
Sfinxem	Sfinx	k1gInSc7	Sfinx
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
čárového	čárový	k2eAgInSc2d1	čárový
kódu	kód	k1gInSc2	kód
výrobku	výrobek	k1gInSc2	výrobek
je	být	k5eAaImIp3nS	být
85920965	[number]	k4	85920965
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
sto	sto	k4xCgNnSc4	sto
gramech	gram	k1gInPc6	gram
Antiperlí	Antiperlí	k1gNnPc2	Antiperlí
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
průměrná	průměrný	k2eAgFnSc1d1	průměrná
energetická	energetický	k2eAgFnSc1d1	energetická
hodnota	hodnota	k1gFnSc1	hodnota
1540	[number]	k4	1540
kilojoulů	kilojoule	k1gInPc2	kilojoule
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
368	[number]	k4	368
kilokalorií	kilokalorie	k1gFnPc2	kilokalorie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
0	[number]	k4	0
gramů	gram	k1gInPc2	gram
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
0	[number]	k4	0
gramů	gram	k1gInPc2	gram
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
97,4	[number]	k4	97,4
gramu	gram	k1gInSc2	gram
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Antiperle	Antiperle	k1gFnPc1	Antiperle
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c6	v
Chotyni	Chotyně	k1gFnSc6	Chotyně
v	v	k7c6	v
pobočce	pobočka	k1gFnSc6	pobočka
libereckého	liberecký	k2eAgInSc2d1	liberecký
podniku	podnik	k1gInSc2	podnik
Lipo	Lipo	k1gMnSc1	Lipo
<g/>
.	.	kIx.	.
</s>
<s>
Antiperle	Antiperle	k1gFnPc1	Antiperle
se	se	k3xPyFc4	se
prodávaly	prodávat	k5eAaImAgFnP	prodávat
v	v	k7c6	v
plechových	plechový	k2eAgFnPc6d1	plechová
krabičkách	krabička	k1gFnPc6	krabička
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
toho	ten	k3xDgMnSc2	ten
začaly	začít	k5eAaPmAgFnP	začít
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
zelených	zelený	k2eAgFnPc6d1	zelená
krabičkách	krabička	k1gFnPc6	krabička
navrchu	navrchu	k7c2	navrchu
průhledných	průhledný	k2eAgInPc2d1	průhledný
tvaru	tvar	k1gInSc2	tvar
malého	malý	k2eAgInSc2d1	malý
válce	válec	k1gInSc2	válec
z	z	k7c2	z
polyethylenu	polyethylen	k1gInSc2	polyethylen
a	a	k8xC	a
polypropylenu	polypropylen	k1gInSc2	polypropylen
<g/>
,	,	kIx,	,
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
krabiček	krabička	k1gFnPc2	krabička
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Antiperle	Antiperle	k6eAd1	Antiperle
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
krystalu	krystal	k1gInSc2	krystal
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
asi	asi	k9	asi
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
na	na	k7c4	na
průměr	průměr	k1gInSc4	průměr
zhruba	zhruba	k6eAd1	zhruba
čtyři	čtyři	k4xCgInPc4	čtyři
milimetry	milimetr	k1gInPc4	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
snahy	snaha	k1gFnPc1	snaha
zmodernizovat	zmodernizovat	k5eAaPmF	zmodernizovat
a	a	k8xC	a
zrychlit	zrychlit	k5eAaPmF	zrychlit
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skončilo	skončit	k5eAaPmAgNnS	skončit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výsledné	výsledný	k2eAgFnPc1d1	výsledná
Antiperle	Antiperle	k1gFnPc1	Antiperle
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
měkké	měkký	k2eAgFnPc1d1	měkká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krystal	krystal	k1gInSc4	krystal
cukru	cukr	k1gInSc2	cukr
se	se	k3xPyFc4	se
nanáší	nanášet	k5eAaImIp3nP	nanášet
postupně	postupně	k6eAd1	postupně
vrstvičky	vrstvička	k1gFnPc1	vrstvička
cukerného	cukerný	k2eAgInSc2d1	cukerný
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
týdnů	týden	k1gInPc2	týden
se	se	k3xPyFc4	se
krystal	krystal	k1gInSc1	krystal
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
na	na	k7c4	na
průměr	průměr	k1gInSc4	průměr
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
milimetry	milimetr	k1gInPc4	milimetr
<g/>
.	.	kIx.	.
cukr	cukr	k1gInSc4	cukr
glukózový	glukózový	k2eAgInSc4d1	glukózový
sirup	sirup	k1gInSc4	sirup
aromata	aroma	k1gNnPc1	aroma
<g/>
:	:	kIx,	:
menthol	menthol	k1gInSc1	menthol
<g/>
,	,	kIx,	,
eukalyptová	eukalyptový	k2eAgFnSc1d1	eukalyptová
silice	silice	k1gFnSc1	silice
<g/>
,	,	kIx,	,
skořicové	skořicový	k2eAgNnSc1d1	skořicové
aroma	aroma	k1gNnSc1	aroma
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgNnSc1d1	růžové
aroma	aroma	k1gNnSc1	aroma
<g/>
,	,	kIx,	,
ethylvanilin	ethylvanilin	k1gInSc1	ethylvanilin
lešticí	lešticí	k2eAgFnSc2d1	lešticí
látky	látka	k1gFnSc2	látka
<g/>
:	:	kIx,	:
včelí	včelí	k2eAgInSc4d1	včelí
vosk	vosk	k1gInSc4	vosk
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
901	[number]	k4	901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karnaubský	karnaubský	k2eAgInSc1d1	karnaubský
vosk	vosk	k1gInSc1	vosk
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
903	[number]	k4	903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šelak	šelak	k1gInSc1	šelak
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
904	[number]	k4	904
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
stopy	stopa	k1gFnPc4	stopa
lepku	lepek	k1gInSc2	lepek
</s>
