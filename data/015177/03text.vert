<s>
Má	můj	k3xOp1gFnSc1
vlast	vlast	k1gFnSc1
(	(	kIx(
<g/>
Čína	Čína	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Má	mít	k5eAaImIp3nS
vlast	vlast	k1gFnSc4
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
<g/>
:	:	kIx,
我	我	k?
<g/>
;	;	kIx,
pinyin	pinyin	k1gInSc4
<g/>
:	:	kIx,
Wǒ	Wǒ	k1gMnSc5
zǔ	zǔ	k?
<g/>
;	;	kIx,
český	český	k2eAgInSc4d1
přepis	přepis	k1gInSc4
<g/>
:	:	kIx,
Wo-te	Wo-t	k1gMnSc5
cu-kuo	cu-kua	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
zazněla	zaznět	k5eAaImAgNnP,k5eAaPmAgNnP
v	v	k7c6
černobílém	černobílý	k2eAgInSc6d1
válečném	válečný	k2eAgInSc6d1
filmu	film	k1gInSc6
Šang-kan-ling	Šang-kan-ling	k1gInSc1
(	(	kIx(
<g/>
上	上	k?
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zachycuje	zachycovat	k5eAaImIp3nS
24	#num#	k4
dní	den	k1gInPc2
dlouhé	dlouhý	k2eAgNnSc4d1
obklíčení	obklíčení	k1gNnSc4
čínských	čínský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c4
horu	hora	k1gFnSc4
Šang-kan-ling	Šang-kan-ling	k1gInSc4
roku	rok	k1gInSc2
1952	#num#	k4
během	během	k7c2
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
hudby	hudba	k1gFnSc2
je	být	k5eAaImIp3nS
Liou	Lio	k2eAgFnSc4d1
Čch	Čch	k1gFnSc4
<g/>
’	’	k?
(	(	kIx(
<g/>
刘	刘	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slova	slovo	k1gNnSc2
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
Čchiao	Čchiao	k1gMnSc1
Jü	Jü	k1gMnSc1
(	(	kIx(
<g/>
喬	喬	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc7d1
interpretkou	interpretka	k1gFnSc7
byla	být	k5eAaImAgFnS
Kuo	Kuo	k1gFnSc1
Lan-jing	Lan-jing	k1gInSc1
(	(	kIx(
<g/>
郭	郭	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
novější	nový	k2eAgFnSc4d2
verzi	verze	k1gFnSc4
pak	pak	k6eAd1
mj.	mj.	kA
nazpívala	nazpívat	k5eAaBmAgFnS,k5eAaPmAgFnS
Chan	Chan	k1gNnSc4
Chung	Chunga	k1gFnPc2
(	(	kIx(
<g/>
韩	韩	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Skladba	skladba	k1gFnSc1
Má	mít	k5eAaImIp3nS
vlast	vlast	k1gFnSc4
se	se	k3xPyFc4
záhy	záhy	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vzniku	vznik	k1gInSc6
stala	stát	k5eAaPmAgFnS
klasickou	klasický	k2eAgFnSc7d1
revoluční	revoluční	k2eAgFnSc7d1
písní	píseň	k1gFnSc7
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
text	text	k1gInSc1
písně	píseň	k1gFnSc2
</s>
<s>
Čínština	čínština	k1gFnSc1
</s>
<s>
Hanyu	Hanyu	k6eAd1
pinyin	pinyin	k2eAgInSc1d1
</s>
<s>
Český	český	k2eAgInSc1d1
přepis	přepis	k1gInSc1
</s>
<s>
一	一	k?
</s>
<s>
风	风	k?
</s>
<s>
我	我	k?
</s>
<s>
听	听	k?
</s>
<s>
看	看	k?
</s>
<s>
这	这	k?
</s>
<s>
是	是	k?
</s>
<s>
在	在	k?
</s>
<s>
到	到	k?
</s>
<s>
姑	姑	k?
</s>
<s>
小	小	k?
</s>
<s>
为	为	k?
</s>
<s>
唤	唤	k?
</s>
<s>
让	让	k?
</s>
<s>
这	这	k?
</s>
<s>
是	是	k?
</s>
<s>
在	在	k?
</s>
<s>
到	到	k?
</s>
<s>
好	好	k?
</s>
<s>
条	条	k?
</s>
<s>
朋	朋	k?
</s>
<s>
若	若	k?
</s>
<s>
迎	迎	k?
</s>
<s>
这	这	k?
</s>
<s>
是	是	k?
</s>
<s>
在	在	k?
</s>
<s>
到	到	k?
</s>
<s>
Yī	Yī	k?
tiáo	tiáo	k6eAd1
Dà	Dà	k2eAgFnPc1d1
bō	bō	k1gInSc4
kuā	kuā	k1gFnPc2
</s>
<s>
fē	fē	k1gMnSc1
chuī	chuī	k?
dà	dà	k1gMnSc1
huā	huā	k1gMnSc1
liǎ	liǎ	k1gMnSc1
<g/>
'	'	kIx"
<g/>
à	à	k1gMnSc1
</s>
<s>
wǒ	wǒ	k?
jiù	jiù	k?
zà	zà	k1gNnPc2
à	à	k1gInSc1
zhù	zhù	k?
</s>
<s>
tī	tī	k1gMnSc1
liǎ	liǎ	k1gMnSc1
shā	shā	k1gMnSc1
de	de	k?
hà	hà	k1gFnSc4
</s>
<s>
kà	kà	k1gMnSc1
liǎ	liǎ	k1gMnSc1
chuánshà	chuánshà	k1gMnSc1
de	de	k?
báifā	báifā	k1gMnSc1
</s>
<s>
Zhè	Zhè	k?
shì	shì	k?
měilì	měilì	k?
de	de	k?
zǔ	zǔ	k?
</s>
<s>
shì	shì	k?
wǒ	wǒ	k?
shē	shē	k1gMnSc1
de	de	k?
dì	dì	k1gMnSc1
</s>
<s>
zà	zà	k6eAd1
zhè	zhè	k?
pià	pià	k1gInSc1
liáokuò	liáokuò	k?
de	de	k?
tǔ	tǔ	k1gInSc1
</s>
<s>
dà	dà	k?
dō	dō	k5eAaPmIp1nS
yǒ	yǒ	k6eAd1
míngmè	míngmè	k6eAd1
de	de	k?
fē	fē	k1gInSc1
</s>
<s>
Gū	Gū	k1gMnSc1
hǎ	hǎ	k1gMnSc1
huā	huā	k1gMnSc1
yī	yī	k1gMnSc1
</s>
<s>
xiǎ	xiǎ	k1gMnSc1
xī	xī	k1gMnSc1
duō	duō	k?
kuā	kuā	k1gMnSc1
</s>
<s>
wè	wè	k1gMnSc1
kā	kā	k?
xī	xī	k1gMnSc1
tiā	tiā	k?
</s>
<s>
huà	huà	k1gMnSc1
liǎ	liǎ	k1gMnSc1
chénshuì	chénshuì	k?
de	de	k?
gā	gā	k1gInSc1
</s>
<s>
rà	rà	k1gInSc1
nà	nà	k?
héliú	héliú	k?
gǎ	gǎ	k1gMnSc1
liǎ	liǎ	k1gMnSc1
móyà	móyà	k1gMnSc1
</s>
<s>
Zhè	Zhè	k?
shì	shì	k?
yī	yī	k1gInSc1
de	de	k?
zǔ	zǔ	k?
</s>
<s>
shì	shì	k?
wǒ	wǒ	k?
shē	shē	k1gMnSc1
de	de	k?
dì	dì	k1gMnSc1
</s>
<s>
zà	zà	k6eAd1
zhè	zhè	k?
pià	pià	k1gMnSc1
gǔ	gǔ	k1gMnSc1
de	de	k?
tǔ	tǔ	k1gMnSc1
</s>
<s>
dà	dà	k?
dō	dō	k5eAaPmIp1nS
yǒ	yǒ	k6eAd1
qī	qī	k1gInSc1
de	de	k?
lì	lì	k1gInSc1
</s>
<s>
Hǎ	Hǎ	k1gMnSc1
shā	shā	k1gMnSc1
hǎ	hǎ	k1gMnSc1
shuǐ	shuǐ	k?
hǎ	hǎ	k1gMnSc1
dì	dì	k1gMnSc1
</s>
<s>
tiáotiáo	tiáotiáo	k6eAd1
dà	dà	k?
dō	dō	k5eAaPmIp1nS
kuā	kuā	k1gInSc1
</s>
<s>
péngyou	péngyou	k6eAd1
lái	lái	k?
liǎ	liǎ	k1gMnSc1
yǒ	yǒ	k1gInSc2
hǎ	hǎ	k1gMnSc1
jiǔ	jiǔ	k?
</s>
<s>
ruò	ruò	k?
nà	nà	k?
cháiláng	cháiláng	k1gMnSc1
lái	lái	k?
liǎ	liǎ	k1gMnSc1
</s>
<s>
yíngjiē	yíngjiē	k?
tā	tā	k?
de	de	k?
yǒ	yǒ	k5eAaPmIp1nS
liè	liè	k1gInSc1
</s>
<s>
Zhè	Zhè	k?
shì	shì	k?
qiángdà	qiángdà	k?
de	de	k?
zǔ	zǔ	k?
</s>
<s>
shì	shì	k?
wǒ	wǒ	k?
shē	shē	k1gMnSc1
de	de	k?
dì	dì	k1gMnSc1
</s>
<s>
zà	zà	k6eAd1
zhè	zhè	k?
pià	pià	k1gMnSc1
wē	wē	k1gMnSc1
de	de	k?
tǔ	tǔ	k1gMnSc1
</s>
<s>
dà	dà	k?
dō	dō	k5eAaPmIp1nS
yǒ	yǒ	k6eAd1
hépíng	hépíng	k1gInSc1
de	de	k?
yángguā	yángguā	k1gInSc1
</s>
<s>
I	i	k9
tchiao	tchiao	k1gMnSc1
Ta-che	Ta-ch	k1gFnSc2
po-lang	po-lang	k1gMnSc1
kchuan	kchuan	k1gMnSc1
</s>
<s>
feng	feng	k1gInSc1
čchuej	čchuej	k1gInSc1
tao	tao	k?
chua-siang	chua-siang	k1gMnSc1
liang-an	liang-an	k1gMnSc1
</s>
<s>
wo-ťia	wo-ťia	k1gFnSc1
ťiou	ťiou	k5eAaPmIp1nS
caj	caj	k?
an-šang	an-šang	k1gInSc1
ču	ču	k?
</s>
<s>
tching-kuan	tching-kuan	k1gMnSc1
liao	liao	k1gMnSc1
šao-kung	šao-kung	k1gMnSc1
te	te	k?
chao-c	chao-c	k1gInSc1
<g/>
'	'	kIx"
</s>
<s>
kchan-kuan	kchan-kuan	k1gMnSc1
liao	liao	k1gMnSc1
čchuan-šang	čchuan-šang	k1gMnSc1
te	te	k?
paj-fan	paj-fan	k1gMnSc1
</s>
<s>
Če	Če	k?
š	š	k?
<g/>
'	'	kIx"
mej	mej	k?
<g/>
-li	-li	k?
te	te	k?
cu-kuo	cu-kuo	k6eAd1
</s>
<s>
š	š	k?
<g/>
'	'	kIx"
wo	wo	k?
šeng-čang	šeng-čang	k1gMnSc1
te	te	k?
ti-fang	ti-fang	k1gMnSc1
</s>
<s>
caj	caj	k?
če	če	k?
pchien	pchien	k2eAgMnSc1d1
liao-kchuo	liao-kchuo	k1gMnSc1
te	te	k?
tchu-ti-šang	tchu-ti-šang	k1gMnSc1
</s>
<s>
tao-čchu	tao-čcha	k1gFnSc4
tou	ten	k3xDgFnSc7
jou	jou	k?
ming-mej	ming-mej	k1gInSc1
te	te	k?
feng-kuang	feng-kuang	k1gInSc1
</s>
<s>
Ku-niang	Ku-niang	k1gMnSc1
chao-siang	chao-siang	k1gMnSc1
chua-er	chua-er	k1gMnSc1
i-jang	i-jang	k1gMnSc1
</s>
<s>
siao-chuo-er	siao-chuo-er	k1gMnSc1
sin-siung	sin-siung	k1gMnSc1
tuo	tuo	k?
kchuan-kuang	kchuan-kuang	k1gMnSc1
</s>
<s>
wej-liao	wej-liao	k1gNnSc1
kchaj-pchi	kchaj-pchi	k1gNnPc2
sin	sin	kA
tchien-ti	tchien-ť	k1gFnSc2
</s>
<s>
chuan-sing	chuan-sing	k1gInSc1
liao	liao	k6eAd1
čchen-šuej	čchen-šuej	k1gInSc1
te	te	k?
kao-šan	kao-šan	k1gInSc1
</s>
<s>
žang	žang	k1gInSc1
na	na	k7c4
che-liou	che-liá	k1gFnSc4
kaj-pien	kaj-pien	k2eAgMnSc1d1
liao	liao	k1gMnSc1
mo-jang	mo-jang	k1gMnSc1
</s>
<s>
Če	Če	k?
š	š	k?
<g/>
'	'	kIx"
jing-siung	jing-siung	k1gMnSc1
te	te	k?
cu-kuo	cu-kuo	k1gMnSc1
</s>
<s>
š	š	k?
<g/>
'	'	kIx"
wo	wo	k?
šeng-čang	šeng-čang	k1gMnSc1
te	te	k?
ti-fang	ti-fang	k1gMnSc1
</s>
<s>
caj	caj	k?
če	če	k?
pchien	pchien	k2eAgMnSc1d1
ku-lao	ku-lao	k1gMnSc1
te	te	k?
tchu-ti-šang	tchu-ti-šang	k1gMnSc1
</s>
<s>
tao-čchu	tao-čcha	k1gFnSc4
tou	ten	k3xDgFnSc7
jou	jou	k?
čching-čchun	čching-čchun	k1gMnSc1
te	te	k?
li-liang	li-liang	k1gMnSc1
</s>
<s>
chao	chao	k1gMnSc1
šan	šan	k?
chao	chao	k1gMnSc1
šuej	šuej	k1gMnSc1
chao	chao	k1gMnSc1
ti-fang	ti-fang	k1gMnSc1
</s>
<s>
tchiao-tchiao	tchiao-tchiao	k1gNnSc1
ta-lu	ta-lat	k5eAaPmIp1nS
tou	ten	k3xDgFnSc7
kchuan-čchang	kchuan-čchang	k1gInSc1
</s>
<s>
pcheng-jou	pcheng-jou	k6eAd1
laj	lát	k5eAaImRp2nS
liao	liao	k1gNnSc4
jou	jou	k?
chao	chao	k6eAd1
ťiou	ťiou	k5eAaPmIp1nS
</s>
<s>
žuo-š	žuo-š	k5eAaPmIp2nS
<g/>
'	'	kIx"
na	na	k7c4
čchaj-lang	čchaj-lang	k1gInSc4
laj	lát	k5eAaImRp2nS
liao	liaa	k1gFnSc5
</s>
<s>
jing-ťie	jing-ťie	k1gFnSc1
tcha	tcha	k0
te	te	k?
jou	jou	k?
lie-čchiang	lie-čchiang	k1gInSc1
</s>
<s>
če	če	k?
ši	ši	k?
čchiang-ta	čchiang-ta	k1gMnSc1
te	te	k?
cu-kuo	cu-kuo	k1gMnSc1
</s>
<s>
š	š	k?
<g/>
'	'	kIx"
wo	wo	k?
šeng-čang	šeng-čang	k1gMnSc1
te	te	k?
ti-fang	ti-fang	k1gMnSc1
</s>
<s>
caj	caj	k?
če	če	k?
pchien	pchien	k2eAgMnSc1d1
wen-nuan	wen-nuan	k1gMnSc1
te	te	k?
tchu-ti-šang	tchu-ti-šang	k1gMnSc1
</s>
<s>
tao-čchu	tao-čcha	k1gFnSc4
tou	ten	k3xDgFnSc7
jou	jou	k?
che-pching	che-pching	k1gInSc1
te	te	k?
jang-kuang	jang-kuang	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Scéna	scéna	k1gFnSc1
z	z	k7c2
filmu	film	k1gInSc2
Šang-kan-ling	Šang-kan-ling	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
píseň	píseň	k1gFnSc1
zazněla	zaznět	k5eAaImAgFnS,k5eAaPmAgFnS
poprvé	poprvé	k6eAd1
</s>
<s>
Píseň	píseň	k1gFnSc1
v	v	k7c6
podání	podání	k1gNnSc6
Chan	Chan	k1gMnSc1
Chung	Chung	k1gMnSc1
s	s	k7c7
obrazovým	obrazový	k2eAgInSc7d1
doprovodem	doprovod	k1gInSc7
</s>
