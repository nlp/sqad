<s desamb="1">
Poprvé	poprvé	k6eAd1
zazněla	zaznět	k5eAaImAgNnP,k5eAaPmAgNnP
v	v	k7c6
černobílém	černobílý	k2eAgInSc6d1
válečném	válečný	k2eAgInSc6d1
filmu	film	k1gInSc6
Šang-kan-ling	Šang-kan-ling	k1gInSc1
(	(	kIx(
<g/>
上	上	k?
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zachycuje	zachycovat	k5eAaImIp3nS
24	#num#	k4
dní	den	k1gInPc2
dlouhé	dlouhý	k2eAgNnSc4d1
obklíčení	obklíčení	k1gNnSc4
čínských	čínský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c4
horu	hora	k1gFnSc4
Šang-kan-ling	Šang-kan-ling	k1gInSc4
roku	rok	k1gInSc2
1952	#num#	k4
během	během	k7c2
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>