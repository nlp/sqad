<s>
Terminátor	terminátor	k1gMnSc1	terminátor
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
robota	robot	k1gMnSc4	robot
<g/>
,	,	kIx,	,
či	či	k8xC	či
kyborga	kyborga	k1gFnSc1	kyborga
<g/>
,	,	kIx,	,
objevujících	objevující	k2eAgMnPc2d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
vykreslené	vykreslený	k2eAgNnSc1d1	vykreslené
filmovou	filmový	k2eAgFnSc7d1	filmová
sérií	série	k1gFnSc7	série
počínající	počínající	k2eAgFnSc7d1	počínající
filmem	film	k1gInSc7	film
Terminátor	terminátor	k1gInSc1	terminátor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
