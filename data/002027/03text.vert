<s>
Terminátor	terminátor	k1gMnSc1	terminátor
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
robota	robot	k1gMnSc4	robot
<g/>
,	,	kIx,	,
či	či	k8xC	či
kyborga	kyborga	k1gFnSc1	kyborga
<g/>
,	,	kIx,	,
objevujících	objevující	k2eAgMnPc2d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
vykreslené	vykreslený	k2eAgNnSc1d1	vykreslené
filmovou	filmový	k2eAgFnSc7d1	filmová
sérií	série	k1gFnSc7	série
počínající	počínající	k2eAgFnSc7d1	počínající
filmem	film	k1gInSc7	film
Terminátor	terminátor	k1gInSc1	terminátor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Terminátory	terminátor	k1gInPc4	terminátor
zde	zde	k6eAd1	zde
stvořil	stvořit	k5eAaPmAgInS	stvořit
Skynet	Skynet	k1gInSc1	Skynet
-	-	kIx~	-
armádní	armádní	k2eAgInSc1d1	armádní
superpočítač	superpočítač	k1gInSc1	superpočítač
disponující	disponující	k2eAgInSc1d1	disponující
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
vzbouří	vzbouřit	k5eAaPmIp3nS	vzbouřit
proti	proti	k7c3	proti
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
robotů	robot	k1gMnPc2	robot
vede	vést	k5eAaImIp3nS	vést
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgFnSc1d2	dokonalejší
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
a	a	k8xC	a
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
představitelem	představitel	k1gMnSc7	představitel
terminátora	terminátor	k1gMnSc2	terminátor
je	být	k5eAaImIp3nS	být
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
postupně	postupně	k6eAd1	postupně
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
několik	několik	k4yIc4	několik
různých	různý	k2eAgFnPc2d1	různá
jednotek	jednotka	k1gFnPc2	jednotka
modelu	model	k1gInSc2	model
označeného	označený	k2eAgInSc2d1	označený
jako	jako	k8xC	jako
T-800	T-800	k1gMnSc2	T-800
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
odlišné	odlišný	k2eAgFnPc4d1	odlišná
tváře	tvář	k1gFnPc4	tvář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
celokovového	celokovový	k2eAgMnSc4d1	celokovový
kyborga	kyborg	k1gMnSc4	kyborg
potaženého	potažený	k2eAgMnSc4d1	potažený
lidskou	lidský	k2eAgFnSc7d1	lidská
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
k	k	k7c3	k
infiltraci	infiltrace	k1gFnSc3	infiltrace
a	a	k8xC	a
provedení	provedení	k1gNnSc3	provedení
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilejšího	pokročilý	k2eAgMnSc4d2	pokročilejší
terminátora	terminátor	k1gMnSc4	terminátor
T-1000	T-1000	k1gFnSc2	T-1000
z	z	k7c2	z
tekutého	tekutý	k2eAgInSc2d1	tekutý
kovu	kov	k1gInSc2	kov
hraje	hrát	k5eAaImIp3nS	hrát
Robert	Robert	k1gMnSc1	Robert
Patrick	Patrick	k1gMnSc1	Patrick
<g/>
.	.	kIx.	.
</s>
<s>
T-800	T-800	k4	T-800
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zlikvidován	zlikvidovat	k5eAaPmNgMnS	zlikvidovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnSc1	jeho
kovová	kovový	k2eAgFnSc1d1	kovová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
T-1000	T-1000	k1gFnSc1	T-1000
se	se	k3xPyFc4	se
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
poddajné	poddajný	k2eAgFnSc3d1	poddajná
struktuře	struktura	k1gFnSc3	struktura
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
však	však	k9	však
objevilo	objevit	k5eAaPmAgNnS	objevit
rovněž	rovněž	k9	rovněž
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
T-	T-	k1gFnSc1	T-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
400	[number]	k4	400
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
500	[number]	k4	500
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
600	[number]	k4	600
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
T-	T-	k?	T-
<g/>
700	[number]	k4	700
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
800	[number]	k4	800
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
850	[number]	k4	850
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
888	[number]	k4	888
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
900	[number]	k4	900
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
950	[number]	k4	950
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
1001	[number]	k4	1001
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
1002	[number]	k4	1002
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
T-	T-	k1gFnSc1	T-
<g/>
5000	[number]	k4	5000
<g/>
,	,	kIx,	,
T-XA	T-XA	k1gFnSc1	T-XA
<g/>
,	,	kIx,	,
TOK-	TOK-	k1gFnSc1	TOK-
<g/>
715	[number]	k4	715
<g/>
,	,	kIx,	,
T-X	T-X	k1gMnSc1	T-X
<g/>
,	,	kIx,	,
Mototerminator	Mototerminator	k1gMnSc1	Mototerminator
<g/>
,	,	kIx,	,
FK	FK	kA	FK
<g/>
.	.	kIx.	.
</s>
<s>
T-800	T-800	k4	T-800
je	být	k5eAaImIp3nS	být
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
typ	typ	k1gInSc1	typ
bojové	bojový	k2eAgFnSc2d1	bojová
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
parametrům	parametr	k1gInPc3	parametr
často	často	k6eAd1	často
dokáže	dokázat	k5eAaPmIp3nS	dokázat
překonat	překonat	k5eAaPmF	překonat
své	svůj	k3xOyFgMnPc4	svůj
modernější	moderní	k2eAgMnPc4d2	modernější
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
T-800	T-800	k4	T-800
je	být	k5eAaImIp3nS	být
také	také	k9	také
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
schopný	schopný	k2eAgInSc1d1	schopný
sebeuvědomění	sebeuvědomění	k1gNnPc1	sebeuvědomění
-	-	kIx~	-
čím	co	k3yInSc7	co
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
tvůrce	tvůrce	k1gMnSc4	tvůrce
(	(	kIx(	(
<g/>
Skynet	Skynet	k1gMnSc1	Skynet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
T-800	T-800	k4	T-800
vědomý	vědomý	k2eAgInSc1d1	vědomý
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
neuposlechnout	uposlechnout	k5eNaPmF	uposlechnout
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
tulák	tulák	k1gMnSc1	tulák
a	a	k8xC	a
nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
na	na	k7c4	na
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
jí	on	k3xPp3gFnSc3	on
aspoň	aspoň	k9	aspoň
nepřímo	přímo	k6eNd1	přímo
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
tuláků	tulák	k1gMnPc2	tulák
byl	být	k5eAaImAgMnS	být
vyvinut	vyvinut	k2eAgMnSc1d1	vyvinut
T-	T-	k1gMnSc1	T-
<g/>
X.	X.	kA	X.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Terminátor	terminátor	k1gInSc1	terminátor
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
jako	jako	k8xC	jako
T-800	T-800	k1gMnSc1	T-800
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
budoucnosti	budoucnost	k1gFnSc2	budoucnost
zabít	zabít	k5eAaPmF	zabít
Sáru	Sára	k1gFnSc4	Sára
Connorovou	Connorová	k1gFnSc7	Connorová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohla	moct	k5eNaImAgFnS	moct
porodit	porodit	k5eAaPmF	porodit
Johna	John	k1gMnSc4	John
Connora	Connor	k1gMnSc4	Connor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
stane	stanout	k5eAaPmIp3nS	stanout
vůdcem	vůdce	k1gMnSc7	vůdce
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
strojům	stroj	k1gInPc3	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Terminátor	terminátor	k1gInSc1	terminátor
2	[number]	k4	2
<g/>
:	:	kIx,	:
Den	den	k1gInSc1	den
zúčtování	zúčtování	k1gNnSc1	zúčtování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	dílo	k1gNnSc6	dílo
Terminátora	terminátor	k1gMnSc2	terminátor
má	mít	k5eAaImIp3nS	mít
T-800	T-800	k1gFnSc4	T-800
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chránit	chránit	k5eAaImF	chránit
Johna	John	k1gMnSc2	John
Connora	Connor	k1gMnSc2	Connor
před	před	k7c7	před
terminátorem	terminátor	k1gMnSc7	terminátor
T-	T-	k1gMnSc7	T-
<g/>
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
budoucnosti	budoucnost	k1gFnSc2	budoucnost
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
má	mít	k5eAaImIp3nS	mít
TOK-715	TOK-715	k1gFnSc1	TOK-715
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
podobě	podoba	k1gFnSc6	podoba
Cameron	Cameron	k1gInSc4	Cameron
chránit	chránit	k5eAaImF	chránit
mladého	mladý	k2eAgMnSc4d1	mladý
Johna	John	k1gMnSc4	John
a	a	k8xC	a
Sáru	Sára	k1gFnSc4	Sára
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Terminátor	terminátor	k1gInSc1	terminátor
3	[number]	k4	3
<g/>
:	:	kIx,	:
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
díle	díl	k1gInSc6	díl
má	mít	k5eAaImIp3nS	mít
T-850	T-850	k1gFnSc1	T-850
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
chránit	chránit	k5eAaImF	chránit
Johna	John	k1gMnSc2	John
Connora	Connor	k1gMnSc2	Connor
a	a	k8xC	a
Kate	kat	k1gInSc5	kat
Brewsterovou	Brewsterův	k2eAgFnSc7d1	Brewsterův
před	před	k7c7	před
T-X	T-X	k1gFnPc7	T-X
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
oba	dva	k4xCgMnPc1	dva
přežili	přežít	k5eAaPmAgMnP	přežít
soudný	soudný	k2eAgInSc4d1	soudný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Terminátor	terminátor	k1gMnSc1	terminátor
Salvation	Salvation	k1gInSc1	Salvation
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
T-800	T-800	k1gFnSc1	T-800
jako	jako	k8xC	jako
nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
Terminátora	terminátor	k1gMnSc2	terminátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
plánuje	plánovat	k5eAaImIp3nS	plánovat
nasadit	nasadit	k5eAaPmF	nasadit
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Connor	Connor	k1gMnSc1	Connor
"	"	kIx"	"
<g/>
nakažen	nakažen	k2eAgMnSc1d1	nakažen
<g/>
"	"	kIx"	"
terminátorem	terminátor	k1gInSc7	terminátor
T-5000	T-5000	k1gFnSc2	T-5000
a	a	k8xC	a
prěměněn	prěměnit	k5eAaPmNgMnS	prěměnit
v	v	k7c4	v
terminátora	terminátor	k1gMnSc4	terminátor
T-	T-	k1gFnSc2	T-
<g/>
3000	[number]	k4	3000
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Connor	Connor	k1gMnSc1	Connor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zabít	zabít	k5eAaPmF	zabít
Sarah	Sarah	k1gFnSc1	Sarah
<g/>
,	,	kIx,	,
Kyla	Kylus	k1gMnSc2	Kylus
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc2	jejich
strážce	strážce	k1gMnSc2	strážce
terminátora	terminátor	k1gMnSc2	terminátor
T-	T-	k1gFnSc2	T-
<g/>
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
