<s>
Některé	některý	k3yIgFnPc1
báje	báj	k1gFnPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Pelasgos	Pelasgos	k1gMnSc1
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
stvořen	stvořit	k5eAaPmNgInS
bohyní	bohyně	k1gFnSc7
Eurynomé	Eurynomý	k2eAgFnSc2d1
ze	z	k7c2
země	zem	k1gFnSc2
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
ze	z	k7c2
země	zem	k1gFnSc2
povstali	povstat	k5eAaPmAgMnP
další	další	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kmen	kmen	k1gInSc1
Pelasgů	Pelasg	k1gMnPc2
<g/>
.	.	kIx.
</s>