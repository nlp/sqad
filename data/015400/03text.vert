<s>
Lykáón	Lykáón	k1gMnSc1
</s>
<s>
Lykáón	Lykáón	k1gInSc1
Zeus	Zeusa	k1gFnPc2
proměňuje	proměňovat	k5eAaImIp3nS
Lykaóna	Lykaóna	k1gFnSc1
ve	v	k7c4
vlka	vlk	k1gMnSc4
<g/>
,	,	kIx,
rytinu	rytina	k1gFnSc4
zhotovil	zhotovit	k5eAaPmAgMnS
Hendrik	Hendrik	k1gMnSc1
Goltzius	Goltzius	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
HaemonMantineusParrhasiusThesprotusMaenalusAkakosAcontesArkasNyctimusMakedonTegeatesCia	HaemonMantineusParrhasiusThesprotusMaenalusAkakosAcontesArkasNyctimusMakedonTegeatesCia	k1gFnSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Rodiče	rodič	k1gMnPc1
</s>
<s>
Pelasgus	Pelasgus	k1gInSc1
a	a	k8xC
Cyllene	Cyllen	k1gInSc5
<g/>
,	,	kIx,
Meliboea	Meliboea	k1gFnSc1
a	a	k8xC
Deianira	Deianira	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
mythological	mythologicat	k5eAaPmAgInS
king	king	k1gInSc1
of	of	k?
Arcadia	Arcadium	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lykáón	Lykáón	k1gInSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Λ	Λ	k?
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Lycaon	Lycaon	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
řecké	řecký	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
potomek	potomek	k1gMnSc1
Pelasgův	Pelasgův	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
báje	báj	k1gFnPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Pelasgos	Pelasgos	k1gMnSc1
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
stvořen	stvořit	k5eAaPmNgInS
bohyní	bohyně	k1gFnSc7
Eurynomé	Eurynomý	k2eAgFnSc2d1
ze	z	k7c2
země	zem	k1gFnSc2
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
ze	z	k7c2
země	zem	k1gFnSc2
povstali	povstat	k5eAaPmAgMnP
další	další	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kmen	kmen	k1gInSc1
Pelasgů	Pelasg	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Lykáón	Lykáón	k1gMnSc1
byl	být	k5eAaImAgMnS
králem	král	k1gMnSc7
arkadského	arkadský	k2eAgNnSc2d1
města	město	k1gNnSc2
Lykosúry	Lykosúra	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
otcem	otec	k1gMnSc7
Kallisty	Kallista	k1gMnSc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
milenkou	milenka	k1gFnSc7
Diovou	Diův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
bylo	být	k5eAaImAgNnS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
čeká	čekat	k5eAaImIp3nS
dítě	dítě	k1gNnSc4
<g/>
,	,	kIx,
vyhnala	vyhnat	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
bohyně	bohyně	k1gFnSc1
Artemis	Artemis	k1gFnSc1
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
lovecké	lovecký	k2eAgFnSc2d1
družiny	družina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kallistó	Kallistó	k1gFnSc1
porodila	porodit	k5eAaPmAgFnS
v	v	k7c6
lese	les	k1gInSc6
syna	syn	k1gMnSc2
Arkada	Arkada	k1gFnSc1
a	a	k8xC
hned	hned	k6eAd1
nato	nato	k6eAd1
ji	on	k3xPp3gFnSc4
bohyně	bohyně	k1gFnSc1
Héra	Héra	k1gFnSc1
ze	z	k7c2
žárlivosti	žárlivost	k1gFnSc2
proměnila	proměnit	k5eAaPmAgFnS
v	v	k7c6
medvědici	medvědice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Arkas	Arkas	k1gMnSc1
dospěl	dochvít	k5eAaPmAgMnS
<g/>
,	,	kIx,
chtěl	chtít	k5eAaImAgMnS
tuto	tento	k3xDgFnSc4
medvědici	medvědice	k1gFnSc4
zabít	zabít	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c4
poslední	poslední	k2eAgFnSc4d1
chvíli	chvíle	k1gFnSc4
tomu	ten	k3xDgMnSc3
zabránil	zabránit	k5eAaPmAgMnS
Zeus	Zeus	k1gInSc4
<g/>
,	,	kIx,
proměnil	proměnit	k5eAaPmAgMnS
i	i	k9
jeho	jeho	k3xOp3gMnSc1
v	v	k7c4
medvěda	medvěd	k1gMnSc4
a	a	k8xC
vyzvedl	vyzvednout	k5eAaPmAgMnS
oba	dva	k4xCgInPc4
na	na	k7c4
oblohu	obloha	k1gFnSc4
jako	jako	k9
Velkého	velký	k2eAgInSc2d1
a	a	k8xC
Malého	Malý	k1gMnSc2
medvěda	medvěd	k1gMnSc2
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
a	a	k8xC
Malá	malý	k2eAgFnSc1d1
medvědice	medvědice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Král	Král	k1gMnSc1
Lykáón	Lykáón	k1gMnSc1
byl	být	k5eAaImAgMnS
krutý	krutý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
asi	asi	k9
padesát	padesát	k4xCc4
synů	syn	k1gMnPc2
a	a	k8xC
ti	ten	k3xDgMnPc1
prý	prý	k9
byli	být	k5eAaImAgMnP
ještě	ještě	k9
horší	zlý	k2eAgMnSc1d2
než	než	k8xS
on	on	k3xPp3gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
Zeus	Zeusa	k1gFnPc2
byl	být	k5eAaImAgMnS
velmi	velmi	k6eAd1
nespokojen	spokojen	k2eNgMnSc1d1
s	s	k7c7
lidským	lidský	k2eAgNnSc7d1
pokolením	pokolení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
neuctívalo	uctívat	k5eNaImAgNnS
bohy	bůh	k1gMnPc4
<g/>
,	,	kIx,
vyvyšovalo	vyvyšovat	k5eAaImAgNnS
se	se	k3xPyFc4
nad	nad	k7c4
ně	on	k3xPp3gInPc4
a	a	k8xC
páchalo	páchat	k5eAaImAgNnS
násilí	násilí	k1gNnSc4
a	a	k8xC
zločiny	zločin	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověst	pověst	k1gFnSc1
o	o	k7c6
Lykáónově	Lykáónův	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
se	se	k3xPyFc4
donesla	donést	k5eAaPmAgFnS
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
sluchu	sluch	k1gInSc3
a	a	k8xC
on	on	k3xPp3gMnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
obyčejného	obyčejný	k2eAgMnSc2d1
pocestného	pocestný	k1gMnSc2
navštívil	navštívit	k5eAaPmAgInS
králův	králův	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lykáón	Lykáón	k1gMnSc1
však	však	k9
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
božský	božský	k2eAgInSc4d1
původ	původ	k1gInSc4
poznal	poznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
žádnou	žádný	k3yNgFnSc4
úctu	úcta	k1gFnSc4
natož	natož	k6eAd1
bázeň	bázeň	k1gFnSc4
nepocítil	pocítit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
prověřit	prověřit	k5eAaPmF
božskou	božský	k2eAgFnSc4d1
vševědoucnost	vševědoucnost	k1gFnSc4
a	a	k8xC
tak	tak	k9
—	—	k?
zřejmě	zřejmě	k6eAd1
i	i	k9
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
syny	syn	k1gMnPc7
—	—	k?
zabili	zabít	k5eAaPmAgMnP
nejmladšího	mladý	k2eAgMnSc4d3
Nyktima	Nyktim	k1gMnSc4
a	a	k8xC
připravili	připravit	k5eAaPmAgMnP
z	z	k7c2
něj	on	k3xPp3gNnSc2
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
předložili	předložit	k5eAaPmAgMnP
Diovi	Diův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
strašně	strašně	k6eAd1
rozzuřil	rozzuřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
bleskem	blesk	k1gInSc7
spálil	spálit	k5eAaPmAgMnS
Lykáónovo	Lykáónův	k2eAgNnSc4d1
</s>
<s>
sídlo	sídlo	k1gNnSc1
a	a	k8xC
krále	král	k1gMnSc2
i	i	k9
jeho	jeho	k3xOp3gMnPc2
zbylých	zbylý	k2eAgMnPc2d1
čtyřicet	čtyřicet	k4xCc1
devět	devět	k4xCc1
synů	syn	k1gMnPc2
proměnil	proměnit	k5eAaPmAgInS
ve	v	k7c4
vlky	vlk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Lykáón	Lykáón	k1gInSc1
byl	být	k5eAaImAgInS
pravděpodobně	pravděpodobně	k6eAd1
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
připravil	připravit	k5eAaPmAgInS
takto	takto	k6eAd1
hrůznou	hrůzný	k2eAgFnSc4d1
hostinu	hostina	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
měl	mít	k5eAaImAgInS
následovníky	následovník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
stejně	stejně	k6eAd1
krutí	krutý	k2eAgMnPc1d1
a	a	k8xC
zvrhlí	zvrhlý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
Tantalos	Tantalos	k1gMnSc1
<g/>
,	,	kIx,
Átreus	Átreus	k1gMnSc1
a	a	k8xC
možná	možná	k9
by	by	kYmCp3nP
se	se	k3xPyFc4
našli	najít	k5eAaPmAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
nejvyššího	vysoký	k2eAgMnSc4d3
boha	bůh	k1gMnSc4
Dia	Dia	k1gMnPc4
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
kapka	kapka	k1gFnSc1
do	do	k7c2
hodnocení	hodnocení	k1gNnSc2
lidí	člověk	k1gMnPc2
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
vyhubit	vyhubit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prométheus-	Prométheus-	k1gMnSc1
otec	otec	k1gMnSc1
Deukalióna	Deukalión	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
manželku	manželka	k1gFnSc4
Pyrrhu	Pyrrhos	k1gMnSc5
včas	včas	k6eAd1
varoval	varovat	k5eAaImAgMnS
aby	aby	kYmCp3nP
si	se	k3xPyFc3
postavili	postavit	k5eAaPmAgMnP
lodku	lodku	k6eAd1
ve	v	k7c4
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
přežít	přežít	k5eAaPmF
a	a	k8xC
znovu	znovu	k6eAd1
zalidnit	zalidnit	k5eAaPmF
svět	svět	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
lepší	dobrý	k2eAgMnPc4d2
obyvatele	obyvatel	k1gMnPc4
než	než	k8xS
dosud	dosud	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Slovník	slovník	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
nakl	naknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1974	#num#	k4
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Bohové	bůh	k1gMnPc1
a	a	k8xC
hrdinové	hrdina	k1gMnPc1
antických	antický	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
</s>
<s>
Graves	Graves	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
,	,	kIx,
Řecké	řecký	k2eAgInPc4d1
mýty	mýtus	k1gInPc4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7309-153-4	80-7309-153-4	k4
</s>
<s>
Houtzager	Houtzager	k1gInSc1
<g/>
,	,	kIx,
Guus	Guus	k1gInSc1
<g/>
,	,	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
řecké	řecký	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7234-287-8	80-7234-287-8	k4
</s>
<s>
Gerhard	Gerhard	k1gMnSc1
Löwe	Löw	k1gInSc2
<g/>
,	,	kIx,
Heindrich	Heindrich	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Stoll	Stoll	k1gMnSc1
<g/>
,	,	kIx,
ABC	ABC	kA
Antiky	antika	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lykáón	Lykáón	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
132124874	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
67618836	#num#	k4
</s>
