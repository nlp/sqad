<p>
<s>
Lisabon	Lisabon	k1gInSc1	Lisabon
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
Lisboa	Lisbo	k2eAgFnSc1d1	Lisbo
/	/	kIx~	/
<g/>
ližboa	ližboa	k1gFnSc1	ližboa
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
507	[number]	k4	507
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
aglomerace	aglomerace	k1gFnSc1	aglomerace
čítá	čítat	k5eAaImIp3nS	čítat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
000	[number]	k4	000
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejzápadnější	západní	k2eAgNnSc4d3	nejzápadnější
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
pevnině	pevnina	k1gFnSc6	pevnina
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnSc6d1	malá
ploše	plocha	k1gFnSc6	plocha
84,92	[number]	k4	84,92
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgFnPc1d1	současná
hranice	hranice	k1gFnPc1	hranice
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
původní	původní	k2eAgNnSc4d1	původní
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
jeho	jeho	k3xOp3gNnPc4	jeho
předměstí	předměstí	k1gNnPc4	předměstí
jako	jako	k8xS	jako
Loures	Loures	k1gInSc4	Loures
<g/>
,	,	kIx,	,
Odivelas	Odivelas	k1gInSc1	Odivelas
<g/>
,	,	kIx,	,
Amadora	Amadora	k1gFnSc1	Amadora
a	a	k8xC	a
Oeiras	Oeiras	k1gInSc1	Oeiras
formálně	formálně	k6eAd1	formálně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
samostatné	samostatný	k2eAgFnSc2d1	samostatná
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jeho	jeho	k3xOp3gFnSc2	jeho
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
Lisabonu	Lisabon	k1gInSc2	Lisabon
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
fénické	fénický	k2eAgNnSc1d1	fénické
obchodní	obchodní	k2eAgNnSc1d1	obchodní
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Fénický	fénický	k2eAgInSc1d1	fénický
termín	termín	k1gInSc1	termín
Allis	Allis	k1gFnSc1	Allis
Ubbo	Ubbo	k1gMnSc1	Ubbo
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
přístav	přístav	k1gInSc1	přístav
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ostatně	ostatně	k6eAd1	ostatně
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
výkladů	výklad	k1gInPc2	výklad
původu	původ	k1gInSc2	původ
názvu	název	k1gInSc2	název
Lisabon	Lisabon	k1gInSc1	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Řekům	Řek	k1gMnPc3	Řek
bylo	být	k5eAaImAgNnS	být
osídlení	osídlení	k1gNnSc1	osídlení
známo	znám	k2eAgNnSc1d1	známo
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Olissipo	Olissipa	k1gFnSc5	Olissipa
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
lidovou	lidový	k2eAgFnSc7d1	lidová
latinou	latina	k1gFnSc7	latina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
Olissipona	Olissipon	k1gMnSc2	Olissipon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
stálo	stát	k5eAaImAgNnS	stát
Olissipo	Olissipa	k1gFnSc5	Olissipa
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Římanů	Říman	k1gMnPc2	Říman
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
za	za	k7c4	za
odměnu	odměna	k1gFnSc4	odměna
součástí	součást	k1gFnPc2	součást
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgNnPc7d1	rozsáhlé
privilegii	privilegium	k1gNnPc7	privilegium
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Felicitas	Felicitas	k1gFnSc1	Felicitas
Julia	Julius	k1gMnSc2	Julius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lisabon	Lisabon	k1gInSc1	Lisabon
pod	pod	k7c7	pod
římskou	římský	k2eAgFnSc7d1	římská
vládou	vláda	k1gFnSc7	vláda
prosperoval	prosperovat	k5eAaImAgMnS	prosperovat
a	a	k8xC	a
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
ohniskům	ohnisko	k1gNnPc3	ohnisko
křesťanství	křesťanství	k1gNnSc2	křesťanství
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
trpělo	trpět	k5eAaImAgNnS	trpět
vpády	vpád	k1gInPc4	vpád
Alanů	Alan	k1gMnPc2	Alan
a	a	k8xC	a
Vandalů	Vandal	k1gMnPc2	Vandal
a	a	k8xC	a
následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
vizigótské	vizigótský	k2eAgFnSc2d1	Vizigótská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
711	[number]	k4	711
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
dobyto	dobýt	k5eAaPmNgNnS	dobýt
Maury	Maur	k1gMnPc4	Maur
(	(	kIx(	(
<g/>
za	za	k7c2	za
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
al-ʾ	al-ʾ	k?	al-ʾ
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
ovládali	ovládat	k5eAaImAgMnP	ovládat
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1147	[number]	k4	1147
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reconquisty	reconquist	k1gInPc4	reconquist
dobyl	dobýt	k5eAaPmAgInS	dobýt
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
anglických	anglický	k2eAgMnPc2d1	anglický
<g/>
,	,	kIx,	,
francouzských	francouzský	k2eAgMnPc2d1	francouzský
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
první	první	k4xOgMnSc1	první
portugalský	portugalský	k2eAgMnSc1d1	portugalský
král	král	k1gMnSc1	král
Afonso	Afonsa	k1gFnSc5	Afonsa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Portugalského	portugalský	k2eAgNnSc2d1	portugalské
království	království	k1gNnSc2	království
se	se	k3xPyFc4	se
Lisabon	Lisabon	k1gInSc1	Lisabon
stal	stát	k5eAaPmAgInS	stát
až	až	k9	až
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1255	[number]	k4	1255
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Manuela	Manuel	k1gMnSc2	Manuel
I.	I.	kA	I.
<g/>
,	,	kIx,	,
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
důležitost	důležitost	k1gFnSc4	důležitost
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
stalo	stát	k5eAaPmAgNnS	stát
koloniální	koloniální	k2eAgFnSc7d1	koloniální
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
připlouvalo	připlouvat	k5eAaImAgNnS	připlouvat
do	do	k7c2	do
města	město	k1gNnSc2	město
několik	několik	k4yIc1	několik
lodí	loď	k1gFnPc2	loď
naplněných	naplněný	k2eAgInPc2d1	naplněný
zbožím	zboží	k1gNnSc7	zboží
a	a	k8xC	a
Lisabon	Lisabon	k1gInSc1	Lisabon
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgNnPc2d3	nejbohatší
měst	město	k1gNnPc2	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1755	[number]	k4	1755
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
město	město	k1gNnSc1	město
katastrofální	katastrofální	k2eAgNnSc1d1	katastrofální
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
následnými	následný	k2eAgInPc7d1	následný
požáry	požár	k1gInPc7	požár
a	a	k8xC	a
vlnou	vlna	k1gFnSc7	vlna
tsunami	tsunami	k1gNnSc1	tsunami
zničilo	zničit	k5eAaPmAgNnS	zničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zahynulo	zahynout	k5eAaPmAgNnS	zahynout
tehdy	tehdy	k6eAd1	tehdy
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc4d1	významná
škody	škoda	k1gFnPc4	škoda
způsobila	způsobit	k5eAaPmAgFnS	způsobit
městu	město	k1gNnSc3	město
také	také	k9	také
napoleonská	napoleonský	k2eAgNnPc1d1	napoleonské
vojska	vojsko	k1gNnPc1	vojsko
na	na	k7c4	na
poč	poč	k?	poč
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
a	a	k8xC	a
následně	následně	k6eAd1	následně
modernizační	modernizační	k2eAgInPc4d1	modernizační
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobily	způsobit	k5eAaPmAgFnP	způsobit
vyloupení	vyloupení	k1gNnSc4	vyloupení
řady	řada	k1gFnSc2	řada
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
církevních	církevní	k2eAgFnPc2d1	církevní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
dařit	dařit	k5eAaImF	dařit
městu	město	k1gNnSc3	město
v	v	k7c6	v
pol.	pol.	k?	pol.
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
moderní	moderní	k2eAgFnSc1d1	moderní
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
několik	několik	k4yIc1	několik
parků	park	k1gInPc2	park
a	a	k8xC	a
významné	významný	k2eAgFnPc1d1	významná
kulturní	kulturní	k2eAgFnPc1d1	kulturní
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
Castelo	Castela	k1gFnSc5	Castela
de	de	k?	de
Sã	Sã	k1gMnSc5	Sã
Jorge	Jorgus	k1gMnSc5	Jorgus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
městského	městský	k2eAgInSc2d1	městský
vrchu	vrch	k1gInSc2	vrch
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
lisabonské	lisabonský	k2eAgFnSc6d1	Lisabonská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Alfama	Alfama	k?	Alfama
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
stávalo	stávat	k5eAaImAgNnS	stávat
hradiště	hradiště	k1gNnSc1	hradiště
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
maurská	maurský	k2eAgFnSc1d1	maurská
královská	královský	k2eAgFnSc1d1	královská
rezidence	rezidence	k1gFnSc1	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
byla	být	k5eAaImAgFnS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
romantické	romantický	k2eAgFnSc2d1	romantická
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
pevnosti	pevnost	k1gFnSc2	pevnost
je	být	k5eAaImIp3nS	být
palác	palác	k1gInSc1	palác
Paco	Paco	k1gMnSc1	Paco
de	de	k?	de
Alcácova	Alcácův	k2eAgInSc2d1	Alcácův
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
okolo	okolo	k7c2	okolo
r.	r.	kA	r.
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
Sé	Sé	k1gFnSc1	Sé
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1147	[number]	k4	1147
<g/>
,	,	kIx,	,
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
románskými	románský	k2eAgInPc7d1	románský
<g/>
,	,	kIx,	,
gotickými	gotický	k2eAgInPc7d1	gotický
a	a	k8xC	a
barokními	barokní	k2eAgInPc7d1	barokní
<g/>
;	;	kIx,	;
v	v	k7c6	v
křížové	křížový	k2eAgFnSc6d1	křížová
chodbě	chodba	k1gFnSc6	chodba
jsou	být	k5eAaImIp3nP	být
odkryty	odkryt	k2eAgInPc1d1	odkryt
základy	základ	k1gInPc1	základ
maurských	maurský	k2eAgFnPc2d1	maurská
a	a	k8xC	a
římských	římský	k2eAgFnPc2d1	římská
hradeb	hradba	k1gFnPc2	hradba
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
starého	starý	k2eAgNnSc2d1	staré
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Calousta	Caloust	k1gMnSc2	Caloust
Gulbenkiana	Gulbenkian	k1gMnSc2	Gulbenkian
</s>
</p>
<p>
<s>
Klášter	klášter	k1gInSc1	klášter
Sã	Sã	k1gFnSc2	Sã
Vicente	Vicent	k1gInSc5	Vicent
de	de	k?	de
Fora	forum	k1gNnSc2	forum
<g/>
,	,	kIx,	,
pozdně	pozdně	k6eAd1	pozdně
renesanční	renesanční	k2eAgMnPc4d1	renesanční
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1582	[number]	k4	1582
až	až	k6eAd1	až
1627	[number]	k4	1627
</s>
</p>
<p>
<s>
Diamantový	diamantový	k2eAgInSc1d1	diamantový
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Casa	Casa	k1gFnSc1	Casa
dos	dos	k?	dos
Bicos	Bicos	k1gInSc1	Bicos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Albuquerqueů	Albuquerque	k1gMnPc2	Albuquerque
z	z	k7c2	z
pol.	pol.	k?	pol.
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
dostavěn	dostavět	k5eAaPmNgInS	dostavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
(	(	kIx(	(
<g/>
Igreja	Igrej	k2eAgFnSc1d1	Igreja
de	de	k?	de
Sã	Sã	k1gFnSc1	Sã
Roque	Roque	k1gFnSc1	Roque
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInSc1d1	renesanční
kostel	kostel	k1gInSc1	kostel
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nezasáhlo	zasáhnout	k5eNaPmAgNnS	zasáhnout
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
;	;	kIx,	;
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
je	být	k5eAaImIp3nS	být
kaple	kaple	k1gFnSc1	kaple
Sao	Sao	k1gFnSc2	Sao
Joao	Joao	k6eAd1	Joao
s	s	k7c7	s
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
zlatem	zlato	k1gNnSc7	zlato
a	a	k8xC	a
vzácnými	vzácný	k2eAgFnPc7d1	vzácná
dřevinami	dřevina	k1gFnPc7	dřevina
</s>
</p>
<p>
<s>
Convento	Convento	k1gNnSc1	Convento
do	do	k7c2	do
Carmo	Carma	k1gFnSc5	Carma
<g/>
,	,	kIx,	,
s	s	k7c7	s
troskami	troska	k1gFnPc7	troska
kostela	kostel	k1gInSc2	kostel
Igreja	Igrejus	k1gMnSc2	Igrejus
do	do	k7c2	do
Carmo	Carma	k1gFnSc5	Carma
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
Matky	matka	k1gFnSc2	matka
boží	božit	k5eAaImIp3nS	božit
(	(	kIx(	(
<g/>
Madre	Madr	k1gInSc5	Madr
de	de	k?	de
Deus	Deus	k1gInSc1	Deus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
zrestaurovaný	zrestaurovaný	k2eAgInSc1d1	zrestaurovaný
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
klášterní	klášterní	k2eAgInSc1d1	klášterní
kapitulní	kapitulní	k2eAgInSc1d1	kapitulní
sál	sál	k1gInSc1	sál
a	a	k8xC	a
reprezentační	reprezentační	k2eAgInSc1d1	reprezentační
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
azulejos	azulejosa	k1gFnPc2	azulejosa
</s>
</p>
<p>
<s>
Basílica	Basílic	k2eAgFnSc1d1	Basílic
da	da	k?	da
Estrela	Estrela	k1gFnSc1	Estrela
<g/>
,	,	kIx,	,
barokně-novoklasicistní	barokněovoklasicistní	k2eAgInSc1d1	barokně-novoklasicistní
chrám	chrám	k1gInSc4	chrám
vystavěný	vystavěný	k2eAgInSc4d1	vystavěný
v	v	k7c6	v
letech	let	k1gInPc6	let
1779	[number]	k4	1779
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
</s>
</p>
<p>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
Aqueduto	Aquedut	k2eAgNnSc1d1	Aquedut
das	das	k?	das
Águas	Águas	k1gMnSc1	Águas
Livres	Livres	k1gMnSc1	Livres
<g/>
,	,	kIx,	,
lisabonský	lisabonský	k2eAgInSc1d1	lisabonský
akvadukt	akvadukt	k1gInSc1	akvadukt
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
st.	st.	kA	st.
<g/>
;	;	kIx,	;
stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1731	[number]	k4	1731
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
italského	italský	k2eAgMnSc2d1	italský
architekta	architekt	k1gMnSc2	architekt
A.	A.	kA	A.
Canevariho	Canevari	k1gMnSc2	Canevari
</s>
</p>
<p>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Praça	Praçum	k1gNnSc2	Praçum
do	do	k7c2	do
Comércio	Comércio	k6eAd1	Comércio
<g/>
,	,	kIx,	,
s	s	k7c7	s
Vítězným	vítězný	k2eAgInSc7d1	vítězný
obloukem	oblouk	k1gInSc7	oblouk
(	(	kIx(	(
<g/>
Arco	Arco	k6eAd1	Arco
da	da	k?	da
Rua	Rua	k1gMnSc1	Rua
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Praça	Praç	k1gInSc2	Praç
Dom	Dom	k?	Dom
Pedro	Pedro	k1gNnSc1	Pedro
IV	IV	kA	IV
<g/>
.	.	kIx.	.
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
D.	D.	kA	D.
Maria	Mario	k1gMnSc4	Mario
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
</s>
</p>
<p>
<s>
Pěší	pěší	k2eAgFnSc1d1	pěší
zóna	zóna	k1gFnSc1	zóna
Rua	Rua	k1gMnSc2	Rua
Augusta	August	k1gMnSc2	August
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
domů	dům	k1gInPc2	dům
s	s	k7c7	s
cennými	cenný	k2eAgFnPc7d1	cenná
fasádami	fasáda	k1gFnPc7	fasáda
<g/>
,	,	kIx,	,
s	s	k7c7	s
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
kavárnami	kavárna	k1gFnPc7	kavárna
</s>
</p>
<p>
<s>
Elevador	Elevador	k1gMnSc1	Elevador
da	da	k?	da
Santa	Santa	k1gMnSc1	Santa
Justa	Justa	k1gMnSc1	Justa
historický	historický	k2eAgInSc4d1	historický
výtah	výtah	k1gInSc4	výtah
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
R.	R.	kA	R.
Mesniera	Mesnier	k1gMnSc2	Mesnier
di	di	k?	di
Ponard	Ponard	k1gInSc1	Ponard
<g/>
,	,	kIx,	,
Eiffelova	Eiffelův	k2eAgMnSc4d1	Eiffelův
žáka	žák	k1gMnSc4	žák
</s>
</p>
<p>
<s>
Avenida	Avenida	k1gFnSc1	Avenida
da	da	k?	da
Liberdade	Liberdad	k1gInSc5	Liberdad
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
</s>
</p>
<p>
<s>
===	===	k?	===
Belém	Belý	k2eAgMnSc6d1	Belý
===	===	k?	===
</s>
</p>
<p>
<s>
Klášter	klášter	k1gInSc1	klášter
řádu	řád	k1gInSc2	řád
sv.	sv.	kA	sv.
Jeronýma	Jeroným	k1gMnSc2	Jeroným
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Belém	Belé	k1gNnSc6	Belé
(	(	kIx(	(
<g/>
Mosteiro	Mosteiro	k1gNnSc1	Mosteiro
dos	dos	k?	dos
Jerónimos	Jerónimosa	k1gFnPc2	Jerónimosa
<g/>
)	)	kIx)	)
–	–	k?	–
zapsán	zapsán	k2eAgMnSc1d1	zapsán
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
</s>
</p>
<p>
<s>
Belémská	Belémský	k2eAgFnSc1d1	Belémská
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
Torre	torr	k1gInSc5	torr
de	de	k?	de
Belém	Belé	k1gNnSc6	Belé
<g/>
)	)	kIx)	)
–	–	k?	–
zapsána	zapsat	k5eAaPmNgFnS	zapsat
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
objevitelů	objevitel	k1gMnPc2	objevitel
(	(	kIx(	(
<g/>
Padrã	Padrã	k1gMnSc1	Padrã
dos	dos	k?	dos
Descobrimentos	Descobrimentos	k1gMnSc1	Descobrimentos
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
</s>
</p>
<p>
<s>
==	==	k?	==
Městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Lisabon	Lisabon	k1gInSc1	Lisabon
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
24	[number]	k4	24
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
freguesia	freguesia	k1gFnSc1	freguesia
<g/>
,	,	kIx,	,
civilní	civilní	k2eAgFnSc1d1	civilní
farnost	farnost	k1gFnSc1	farnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
samosprávu	samospráva	k1gFnSc4	samospráva
volenou	volený	k2eAgFnSc4d1	volená
místními	místní	k2eAgMnPc7d1	místní
občany	občan	k1gMnPc7	občan
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
53	[number]	k4	53
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
zákonem	zákon	k1gInSc7	zákon
"	"	kIx"	"
<g/>
Lei	lei	k1gInSc1	lei
n.	n.	k?	n.
<g/>
o	o	k7c4	o
56	[number]	k4	56
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
"	"	kIx"	"
snížen	snížit	k5eAaPmNgInS	snížit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
významnější	významný	k2eAgInPc4d2	významnější
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
původní	původní	k2eAgInPc4d1	původní
historické	historický	k2eAgInPc4d1	historický
názvy	název	k1gInPc4	název
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Alfama	Alfama	k?	Alfama
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgFnSc1d3	nejstarší
lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
arabského	arabský	k2eAgNnSc2d1	arabské
slova	slovo	k1gNnSc2	slovo
Al-hamma	Alammum	k1gNnSc2	Al-hammum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baixa	Baixa	k1gFnSc1	Baixa
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
čtvrť	čtvrť	k1gFnSc1	čtvrť
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bairro	Bairro	k1gNnSc1	Bairro
Alto	Alto	k1gNnSc1	Alto
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
horní	horní	k2eAgFnSc1d1	horní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
,	,	kIx,	,
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
knihkupectví	knihkupectví	k1gNnPc2	knihkupectví
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
náměstí	náměstí	k1gNnSc2	náměstí
Praça	Praçus	k1gMnSc2	Praçus
de	de	k?	de
Camõ	Camõ	k1gMnSc2	Camõ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belém	Belé	k1gNnSc6	Belé
(	(	kIx(	(
<g/>
čtvrť	čtvrť	k1gFnSc1	čtvrť
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
předměstí	předměstí	k1gNnSc4	předměstí
Lisabonu	Lisabon	k1gInSc2	Lisabon
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
památkami	památka	k1gFnPc7	památka
zapsanými	zapsaný	k2eAgFnPc7d1	zapsaná
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Estrela	Estrel	k1gMnSc4	Estrel
<g/>
,	,	kIx,	,
Alcântara	Alcântar	k1gMnSc4	Alcântar
<g/>
,	,	kIx,	,
Graça	Graçus	k1gMnSc4	Graçus
<g/>
,	,	kIx,	,
Anjos	Anjos	k1gMnSc1	Anjos
<g/>
,	,	kIx,	,
Xabregas	Xabregas	k1gMnSc1	Xabregas
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
lisabonských	lisabonský	k2eAgFnPc2d1	Lisabonská
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
</s>
</p>
<p>
<s>
==	==	k?	==
Mosty	most	k1gInPc4	most
==	==	k?	==
</s>
</p>
<p>
<s>
Lisabon	Lisabon	k1gInSc1	Lisabon
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
především	především	k9	především
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Tejo	Tejo	k6eAd1	Tejo
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
estuárovitém	estuárovitý	k2eAgNnSc6d1	estuárovitý
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
lisabonský	lisabonský	k2eAgInSc4d1	lisabonský
most	most	k1gInSc4	most
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
(	(	kIx(	(
<g/>
Ponte	Pont	k1gInSc5	Pont
25	[number]	k4	25
de	de	k?	de
Abril	Abrila	k1gFnPc2	Abrila
<g/>
,	,	kIx,	,
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
Karafiátové	Karafiátové	k2eAgFnSc2d1	Karafiátové
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
až	až	k9	až
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřen	k2eAgMnSc1d1	otevřen
byl	být	k5eAaImAgMnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1966	[number]	k4	1966
a	a	k8xC	a
nejprve	nejprve	k6eAd1	nejprve
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Ponte	Pont	k1gInSc5	Pont
Salazar	Salazar	k1gInSc1	Salazar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
2	[number]	k4	2
277,64	[number]	k4	277,64
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
maximálním	maximální	k2eAgInSc6d1	maximální
bodě	bod	k1gInSc6	bod
vysoký	vysoký	k2eAgMnSc1d1	vysoký
70	[number]	k4	70
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
Portugalci	Portugalec	k1gMnPc1	Portugalec
slavili	slavit	k5eAaImAgMnP	slavit
500	[number]	k4	500
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
přelomové	přelomový	k2eAgFnSc2d1	přelomová
plavby	plavba	k1gFnSc2	plavba
Vasca	Vasca	k1gMnSc1	Vasca
da	da	k?	da
Gamy	game	k1gInPc1	game
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
Expa	Expo	k1gNnSc2	Expo
98	[number]	k4	98
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
i	i	k9	i
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
mostu	most	k1gInSc2	most
Vasca	Vasca	k1gFnSc1	Vasca
da	da	k?	da
Gamy	game	k1gInPc4	game
(	(	kIx(	(
<g/>
Ponte	Pont	k1gInSc5	Pont
Vasco	Vasco	k1gNnSc4	Vasco
da	da	k?	da
Gama	gama	k1gNnSc2	gama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
17	[number]	k4	17
345	[number]	k4	345
m	m	kA	m
druhý	druhý	k4xOgInSc4	druhý
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
most	most	k1gInSc4	most
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
Krymském	krymský	k2eAgInSc6d1	krymský
mostu	most	k1gInSc2	most
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výškou	výška	k1gFnSc7	výška
155	[number]	k4	155
m	m	kA	m
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvyšším	vysoký	k2eAgFnPc3d3	nejvyšší
konstrukcím	konstrukce	k1gFnPc3	konstrukce
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
konstrukce	konstrukce	k1gFnSc1	konstrukce
vydržela	vydržet	k5eAaPmAgFnS	vydržet
4,5	[number]	k4	4,5
<g/>
×	×	k?	×
větší	veliký	k2eAgInPc4d2	veliký
seismické	seismický	k2eAgInPc4d1	seismický
otřesy	otřes	k1gInPc4	otřes
než	než	k8xS	než
jakých	jaký	k3yIgMnPc2	jaký
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
sídlí	sídlet	k5eAaImIp3nP	sídlet
tři	tři	k4xCgInPc1	tři
přední	přední	k2eAgInPc1d1	přední
portugalské	portugalský	k2eAgInPc1d1	portugalský
kluby	klub	k1gInPc1	klub
<g/>
:	:	kIx,	:
Benfica	Benfica	k1gFnSc1	Benfica
<g/>
,	,	kIx,	,
Sporting	Sporting	k1gInSc1	Sporting
a	a	k8xC	a
Os	osa	k1gFnPc2	osa
Belenenses	Belenensesa	k1gFnPc2	Belenensesa
<g/>
.	.	kIx.	.
</s>
<s>
Benfica	Benfica	k6eAd1	Benfica
hraje	hrát	k5eAaImIp3nS	hrát
své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Estádio	Estádio	k1gMnSc1	Estádio
da	da	k?	da
Luz	luza	k1gFnPc2	luza
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
pěti	pět	k4xCc3	pět
hvězdičkami	hvězdička	k1gFnPc7	hvězdička
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
stadion	stadion	k1gInSc1	stadion
Sportingu	Sporting	k1gInSc2	Sporting
Estádio	Estádio	k6eAd1	Estádio
José	Josý	k2eAgFnPc1d1	Josý
Alvalade	Alvalad	k1gInSc5	Alvalad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
sehráno	sehrát	k5eAaPmNgNnS	sehrát
několik	několik	k4yIc1	několik
zápasů	zápas	k1gInPc2	zápas
Eura	euro	k1gNnSc2	euro
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
populární	populární	k2eAgInPc1d1	populární
sporty	sport	k1gInPc1	sport
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
golf	golf	k1gInSc4	golf
<g/>
,	,	kIx,	,
futsal	futsat	k5eAaPmAgMnS	futsat
<g/>
,	,	kIx,	,
hokejbal	hokejbal	k1gInSc4	hokejbal
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Calouste	Caloousit	k5eAaPmRp2nP	Caloousit
Gulbenkian	Gulbenkian	k1gInSc4	Gulbenkian
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
–	–	k?	–
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
a	a	k8xC	a
mecenáš	mecenáš	k1gMnSc1	mecenáš
umění	umění	k1gNnSc2	umění
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Lisabon	Lisabon	k1gInSc1	Lisabon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lisabon	Lisabon	k1gInSc1	Lisabon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Câmara	Câmara	k1gFnSc1	Câmara
Municipal	Municipal	k1gFnSc1	Municipal
de	de	k?	de
Lisboa	Lisboum	k1gNnSc2	Lisboum
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Lisabon	Lisabon	k1gInSc1	Lisabon
</s>
</p>
