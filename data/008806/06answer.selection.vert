<s>
Niob	niob	k1gInSc1	niob
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Nb	Nb	k1gFnSc2	Nb
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
niobium	niobium	k1gNnSc1	niobium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kovovým	kovový	k2eAgInSc7d1	kovový
<g/>
,	,	kIx,	,
přechodným	přechodný	k2eAgInSc7d1	přechodný
prvkem	prvek	k1gInSc7	prvek
5	[number]	k4	5
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
