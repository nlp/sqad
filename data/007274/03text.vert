<s>
Infrazvuk	infrazvuk	k1gInSc1	infrazvuk
je	být	k5eAaImIp3nS	být
akustické	akustický	k2eAgNnSc4d1	akustické
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
lidské	lidský	k2eAgNnSc4d1	lidské
ucho	ucho	k1gNnSc4	ucho
není	být	k5eNaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
slyšitelným	slyšitelný	k2eAgInSc7d1	slyšitelný
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
infrazvukem	infrazvuk	k1gInSc7	infrazvuk
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
16	[number]	k4	16
až	až	k9	až
20	[number]	k4	20
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
mezi	mezi	k7c7	mezi
0,001	[number]	k4	0,001
a	a	k8xC	a
0,2	[number]	k4	0,2
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
hroši	hroch	k1gMnPc1	hroch
<g/>
,	,	kIx,	,
nosorožci	nosorožec	k1gMnPc1	nosorožec
<g/>
,	,	kIx,	,
okapi	okapi	k1gFnPc1	okapi
a	a	k8xC	a
aligátoři	aligátor	k1gMnPc1	aligátor
používají	používat	k5eAaImIp3nP	používat
infrazvuk	infrazvuk	k1gInSc4	infrazvuk
k	k	k7c3	k
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
infrazvuk	infrazvuk	k1gInSc4	infrazvuk
neslyšíme	slyšet	k5eNaImIp1nP	slyšet
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
vážné	vážný	k2eAgFnPc4d1	vážná
závratě	závrať	k1gFnPc4	závrať
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
intenzitě	intenzita	k1gFnSc6	intenzita
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
perforaci	perforace	k1gFnSc4	perforace
kochleární	kochleární	k2eAgFnSc2d1	kochleární
membrány	membrána	k1gFnSc2	membrána
nebo	nebo	k8xC	nebo
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
jedinci	jedinec	k1gMnPc1	jedinec
pociťují	pociťovat	k5eAaImIp3nP	pociťovat
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
(	(	kIx(	(
<g/>
pocit	pocit	k1gInSc1	pocit
"	"	kIx"	"
<g/>
zalehnutí	zalehnutí	k1gNnSc1	zalehnutí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
citlivé	citlivý	k2eAgFnSc6d1	citlivá
oblasti	oblast	k1gFnSc6	oblast
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např	např	kA	např
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
hřbet	hřbet	k1gInSc4	hřbet
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Vlny	vlna	k1gFnPc4	vlna
akustického	akustický	k2eAgInSc2d1	akustický
"	"	kIx"	"
<g/>
signálu	signál	k1gInSc2	signál
<g/>
"	"	kIx"	"
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
ve	v	k7c6	v
frekvenční	frekvenční	k2eAgFnSc6d1	frekvenční
oblasti	oblast	k1gFnSc6	oblast
0,05	[number]	k4	0,05
-	-	kIx~	-
20	[number]	k4	20
Hz	Hz	kA	Hz
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
80-90	[number]	k4	80-90
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bouřích	bouř	k1gFnPc6	bouř
<g/>
,	,	kIx,	,
přechodech	přechod	k1gInPc6	přechod
front	fronta	k1gFnPc2	fronta
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
120	[number]	k4	120
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1	aerodynamický
infrazvuk	infrazvuk	k1gInSc1	infrazvuk
v	v	k7c6	v
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
(	(	kIx(	(
<g/>
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
ČD	ČD	kA	ČD
řad	řad	k1gInSc4	řad
163	[number]	k4	163
<g/>
,	,	kIx,	,
263	[number]	k4	263
<g/>
,	,	kIx,	,
363	[number]	k4	363
a	a	k8xC	a
obdobné	obdobný	k2eAgNnSc4d1	obdobné
<g/>
)	)	kIx)	)
při	při	k7c6	při
rychlostech	rychlost	k1gFnPc6	rychlost
jízdy	jízda	k1gFnSc2	jízda
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc4	hod
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
hodnotu	hodnota	k1gFnSc4	hodnota
130	[number]	k4	130
dB	db	kA	db
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
i	i	k9	i
140	[number]	k4	140
dB	db	kA	db
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
prahem	práh	k1gInSc7	práh
bolestivosti	bolestivost	k1gFnSc2	bolestivost
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
to	ten	k3xDgNnSc1	ten
tlakům	tlak	k1gInPc3	tlak
řádově	řádově	k6eAd1	řádově
jednoty	jednota	k1gFnSc2	jednota
až	až	k9	až
desítky	desítka	k1gFnPc1	desítka
pascal	pascal	k1gInSc1	pascal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgNnSc1d1	uvedené
tvrzení	tvrzení	k1gNnSc1	tvrzení
platí	platit	k5eAaImIp3nS	platit
při	při	k7c6	při
porušení	porušení	k1gNnSc6	porušení
hermetizace	hermetizace	k1gFnSc2	hermetizace
kabiny	kabina	k1gFnSc2	kabina
(	(	kIx(	(
<g/>
otevření	otevření	k1gNnSc1	otevření
okének	okénko	k1gNnPc2	okénko
<g/>
)	)	kIx)	)
a	a	k8xC	a
současné	současný	k2eAgFnSc6d1	současná
změně	změna	k1gFnSc6	změna
laminárního	laminární	k2eAgNnSc2d1	laminární
obtékání	obtékání	k1gNnSc2	obtékání
kabiny	kabina	k1gFnSc2	kabina
okolním	okolní	k2eAgInSc7d1	okolní
vzduchem	vzduch	k1gInSc7	vzduch
na	na	k7c4	na
turbulentní	turbulentní	k2eAgNnSc4d1	turbulentní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
nejen	nejen	k6eAd1	nejen
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
účinky	účinek	k1gInPc1	účinek
fyziologické	fyziologický	k2eAgInPc1d1	fyziologický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mechanické	mechanický	k2eAgInPc1d1	mechanický
<g/>
.	.	kIx.	.
</s>
<s>
Zvukové	zvukový	k2eAgNnSc1d1	zvukové
dělo	dělo	k1gNnSc1	dělo
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
sestřelit	sestřelit	k5eAaPmF	sestřelit
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
intenzita	intenzita	k1gFnSc1	intenzita
zvuku	zvuk	k1gInSc2	zvuk
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
mocninou	mocnina	k1gFnSc7	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosah	dosah	k1gInSc4	dosah
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc4d1	malý
a	a	k8xC	a
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
dosti	dosti	k6eAd1	dosti
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
činnost	činnost	k1gFnSc1	činnost
Živočichové	živočich	k1gMnPc1	živočich
Větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
Feraliminální	Feraliminální	k2eAgInSc1d1	Feraliminální
Lykantropizér	Lykantropizér	k1gInSc1	Lykantropizér
</s>
