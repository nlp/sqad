<s>
Vojta	Vojta	k1gMnSc1	Vojta
Náprstek	náprstek	k1gInSc4	náprstek
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
německým	německý	k2eAgNnSc7d1	německé
jménem	jméno	k1gNnSc7	jméno
Adalbert	Adalbert	k1gMnSc1	Adalbert
Fingerhut	Fingerhut	k1gMnSc1	Fingerhut
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1826	[number]	k4	1826
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1894	[number]	k4	1894
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
národopisec	národopisec	k1gMnSc1	národopisec
<g/>
,	,	kIx,	,
mecenáš	mecenáš	k1gMnSc1	mecenáš
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
za	za	k7c4	za
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
majitelů	majitel	k1gMnPc2	majitel
lihovaru	lihovar	k1gInSc2	lihovar
manželů	manžel	k1gMnPc2	manžel
Fingerhutových	Fingerhutův	k2eAgMnPc2d1	Fingerhutův
<g/>
;	;	kIx,	;
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
Náprstek	náprstek	k1gInSc1	náprstek
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Adalbert	Adalbert	k1gMnSc1	Adalbert
Fingerhut	Fingerhut	k1gMnSc1	Fingerhut
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
klasické	klasický	k2eAgNnSc4d1	klasické
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
projevoval	projevovat	k5eAaImAgInS	projevovat
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
cizí	cizí	k2eAgFnPc4d1	cizí
kultury	kultura	k1gFnPc4	kultura
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
studovat	studovat	k5eAaImF	studovat
orientalistiku	orientalistika	k1gFnSc4	orientalistika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
matky	matka	k1gFnSc2	matka
ale	ale	k8xC	ale
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
práv	právo	k1gNnPc2	právo
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
povstání	povstání	k1gNnSc3	povstání
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k8xC	i
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byla	být	k5eAaImAgFnS	být
revoluce	revoluce	k1gFnSc1	revoluce
potlačena	potlačit	k5eAaPmNgFnS	potlačit
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
prožil	prožít	k5eAaPmAgMnS	prožít
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
průběhu	průběh	k1gInSc6	průběh
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
různými	různý	k2eAgFnPc7d1	různá
činnostmi	činnost	k1gFnPc7	činnost
-	-	kIx~	-
jako	jako	k8xC	jako
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
nádeník	nádeník	k1gMnSc1	nádeník
<g/>
,	,	kIx,	,
knihkupec	knihkupec	k1gMnSc1	knihkupec
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
významným	významný	k2eAgNnSc7d1	významné
krajanským	krajanský	k2eAgNnSc7d1	krajanské
střediskem	středisko	k1gNnSc7	středisko
pro	pro	k7c4	pro
vystěhovalce	vystěhovalec	k1gMnPc4	vystěhovalec
přicházející	přicházející	k2eAgFnPc1d1	přicházející
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
vládní	vládní	k2eAgFnSc2d1	vládní
expedice	expedice	k1gFnSc2	expedice
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
indiánský	indiánský	k2eAgInSc4d1	indiánský
kmen	kmen	k1gInSc4	kmen
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgInSc4	první
impuls	impuls	k1gInSc4	impuls
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
nasměroval	nasměrovat	k5eAaPmAgInS	nasměrovat
k	k	k7c3	k
národopisu	národopis	k1gInSc3	národopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
-	-	kIx~	-
za	za	k7c4	za
vydatné	vydatný	k2eAgFnPc4d1	vydatná
spoluúčasti	spoluúčast	k1gFnPc4	spoluúčast
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
Anny	Anna	k1gFnSc2	Anna
Náprstkové	náprstkový	k2eAgInPc1d1	náprstkový
-	-	kIx~	-
rodinný	rodinný	k2eAgInSc1d1	rodinný
pivovar	pivovar	k1gInSc1	pivovar
"	"	kIx"	"
<g/>
U	u	k7c2	u
Halánků	Halánek	k1gInPc2	Halánek
<g/>
"	"	kIx"	"
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Betlémském	betlémský	k2eAgNnSc6d1	Betlémské
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c4	na
centrum	centrum	k1gNnSc4	centrum
české	český	k2eAgFnSc2d1	Česká
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
osvětové	osvětový	k2eAgFnSc2d1	osvětová
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
propagoval	propagovat	k5eAaImAgMnS	propagovat
emancipaci	emancipace	k1gFnSc4	emancipace
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
šíření	šíření	k1gNnSc4	šíření
pokroku	pokrok	k1gInSc2	pokrok
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
(	(	kIx(	(
<g/>
Americký	americký	k2eAgInSc1d1	americký
klub	klub	k1gInSc1	klub
dam	dáma	k1gFnPc2	dáma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
"	"	kIx"	"
<g/>
Českého	český	k2eAgMnSc4d1	český
konzula	konzul	k1gMnSc4	konzul
<g/>
"	"	kIx"	"
všech	všecek	k3xTgMnPc2	všecek
Čechů	Čech	k1gMnPc2	Čech
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
rostly	růst	k5eAaImAgFnP	růst
jeho	jeho	k3xOp3gFnPc1	jeho
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
změnil	změnit	k5eAaPmAgInS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
rodný	rodný	k2eAgInSc4d1	rodný
dům	dům	k1gInSc4	dům
na	na	k7c4	na
České	český	k2eAgNnSc4d1	české
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
muzeum	muzeum	k1gNnSc4	muzeum
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Náprstkovo	Náprstkův	k2eAgNnSc4d1	Náprstkovo
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
významným	významný	k2eAgMnSc7d1	významný
sponzorem	sponzor	k1gMnSc7	sponzor
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Přispěvateli	přispěvatel	k1gMnSc3	přispěvatel
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
sbírek	sbírka	k1gFnPc2	sbírka
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
cestovatelé	cestovatel	k1gMnPc1	cestovatel
Emil	Emil	k1gMnSc1	Emil
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kořenský	Kořenský	k2eAgMnSc1d1	Kořenský
<g/>
,	,	kIx,	,
Enrique	Enrique	k1gFnSc1	Enrique
Stanko	Stanko	k1gMnSc1	Stanko
Vráz	vráz	k6eAd1	vráz
<g/>
,	,	kIx,	,
zoolog	zoolog	k1gMnSc1	zoolog
Antonín	Antonín	k1gMnSc1	Antonín
Frič	Frič	k1gMnSc1	Frič
a	a	k8xC	a
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Bedřich	Bedřich	k1gMnSc1	Bedřich
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
původní	původní	k2eAgInSc1d1	původní
účel	účel	k1gInSc1	účel
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
přinášet	přinášet	k5eAaImF	přinášet
lidem	lid	k1gInSc7	lid
praktické	praktický	k2eAgFnPc4d1	praktická
ukázky	ukázka	k1gFnPc4	ukázka
pokroku	pokrok	k1gInSc2	pokrok
<g/>
,	,	kIx,	,
a	a	k8xC	a
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
rostoucí	rostoucí	k2eAgFnPc4d1	rostoucí
národopisné	národopisný	k2eAgFnPc4d1	národopisná
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
bývalý	bývalý	k2eAgInSc1d1	bývalý
rodinný	rodinný	k2eAgInSc1d1	rodinný
pivovar	pivovar	k1gInSc1	pivovar
nestačil	stačit	k5eNaBmAgInS	stačit
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
Náprstek	náprstek	k1gInSc1	náprstek
postavit	postavit	k5eAaPmF	postavit
budovu	budova	k1gFnSc4	budova
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
-	-	kIx~	-
Náprstkovo	Náprstkův	k2eAgNnSc1d1	Náprstkovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1868	[number]	k4	1868
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
81	[number]	k4	81
signatářů	signatář	k1gMnPc2	signatář
státoprávní	státoprávní	k2eAgFnSc2d1	státoprávní
deklarace	deklarace	k1gFnSc2	deklarace
českých	český	k2eAgMnPc2d1	český
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
česká	český	k2eAgFnSc1d1	Česká
politická	politický	k2eAgFnSc1d1	politická
reprezentace	reprezentace	k1gFnSc1	reprezentace
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
centralistické	centralistický	k2eAgNnSc4d1	centralistické
směřování	směřování	k1gNnSc4	směřování
státu	stát	k1gInSc2	stát
a	a	k8xC	a
hájila	hájit	k5eAaImAgFnS	hájit
české	český	k2eAgNnSc4d1	české
státní	státní	k2eAgNnSc4d1	státní
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
společně	společně	k6eAd1	společně
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
Kurzem	kurz	k1gInSc7	kurz
založil	založit	k5eAaPmAgInS	založit
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
pokrok	pokrok	k1gInSc4	pokrok
byl	být	k5eAaImAgInS	být
patrný	patrný	k2eAgInSc1d1	patrný
i	i	k9	i
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
Čechů	Čech	k1gMnPc2	Čech
přál	přát	k5eAaImAgMnS	přát
být	být	k5eAaImF	být
zpopelněn	zpopelněn	k2eAgMnSc1d1	zpopelněn
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
však	však	k9	však
tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
rakousko-uherské	rakouskoherský	k2eAgInPc1d1	rakousko-uherský
zákony	zákon	k1gInPc1	zákon
neumožňovaly	umožňovat	k5eNaImAgInP	umožňovat
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
převezeny	převezen	k2eAgInPc1d1	převezen
ke	k	k7c3	k
zpopelnění	zpopelnění	k1gNnSc3	zpopelnění
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
svůj	svůj	k3xOyFgInSc4	svůj
majetek	majetek	k1gInSc4	majetek
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
svému	svůj	k3xOyFgMnSc3	svůj
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
;	;	kIx,	;
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gFnPc2	jeho
osobních	osobní	k2eAgFnPc2d1	osobní
sbírek	sbírka	k1gFnPc2	sbírka
bylo	být	k5eAaImAgNnS	být
46	[number]	k4	46
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
18	[number]	k4	18
000	[number]	k4	000
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
