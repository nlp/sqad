<s>
Vojta	Vojta	k1gMnSc1	Vojta
Náprstek	náprstek	k1gInSc4	náprstek
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
německým	německý	k2eAgNnSc7d1	německé
jménem	jméno	k1gNnSc7	jméno
Adalbert	Adalbert	k1gMnSc1	Adalbert
Fingerhut	Fingerhut	k1gMnSc1	Fingerhut
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1826	[number]	k4	1826
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1894	[number]	k4	1894
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
národopisec	národopisec	k1gMnSc1	národopisec
<g/>
,	,	kIx,	,
mecenáš	mecenáš	k1gMnSc1	mecenáš
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
za	za	k7c4	za
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
