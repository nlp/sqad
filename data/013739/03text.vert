<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
Velké	velký	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
s	s	k7c7
dominantou	dominanta	k1gFnSc7
gotické	gotický	k2eAgFnSc2d1
katedrály	katedrála	k1gFnSc2
sv.	sv.	kA
Ducha	duch	k1gMnSc2
<g/>
,	,	kIx,
renesanční	renesanční	k2eAgNnSc1d1
Bílé	bílé	k1gNnSc1
věže	věž	k1gFnSc2
a	a	k8xC
barokní	barokní	k2eAgFnSc2d1
Staré	Staré	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0521	CZ0521	k4
569810	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
521	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Královéhradecký	královéhradecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
92	#num#	k4
939	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
105,69	105,69	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
235	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
500	#num#	k4
xx	xx	k?
a	a	k8xC
503	#num#	k4
xx	xx	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
11	#num#	k4
815	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
21	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
21	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
84	#num#	k4
Rozpočt	Rozpočt	k1gInSc1
<g/>
.	.	kIx.
výdaje	výdaj	k1gInPc1
</s>
<s>
2043	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
magistrátu	magistrát	k1gInSc3
</s>
<s>
Magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
KrálovéČeskoslovenské	KrálovéČeskoslovenský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
408	#num#	k4
<g/>
/	/	kIx~
<g/>
51502	#num#	k4
00	#num#	k4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
posta@mmhk.cz	posta@mmhk.cz	k1gMnSc1
Primátor	primátor	k1gMnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Hrabálek	Hrabálek	k1gMnSc1
(	(	kIx(
<g/>
nestr	nestr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.hradeckralove.org	www.hradeckralove.org	k1gInSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Obec	obec	k1gFnSc1
přátelská	přátelský	k2eAgFnSc1d1
rodině	rodina	k1gFnSc3
Kód	kód	k1gInSc4
obce	obec	k1gFnSc2
</s>
<s>
569810	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Königgrätz	Königgrätz	k1gInSc1
<g/>
;	;	kIx,
latinsky	latinsky	k6eAd1
Hradecz	Hradecz	k1gMnSc1
Reginae	Regina	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
statutární	statutární	k2eAgMnSc1d1
město	město	k1gNnSc4
na	na	k7c6
východě	východ	k1gInSc6
Čech	Čechy	k1gFnPc2
a	a	k8xC
metropole	metropol	k1gFnSc2
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
soutoku	soutok	k1gInSc6
Labe	Labe	k1gNnSc2
s	s	k7c7
Orlicí	Orlice	k1gFnSc7
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
hradecko-pardubické	hradecko-pardubický	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
93	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
příhodné	příhodný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Hradce	Hradec	k1gInSc2
osídleno	osídlit	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
dobách	doba	k1gFnPc6
prehistorických	prehistorický	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
šlo	jít	k5eAaImAgNnS
o	o	k7c4
věnné	věnný	k2eAgNnSc4d1
město	město	k1gNnSc4
českých	český	k2eAgFnPc2d1
královen	královna	k1gFnPc2
a	a	k8xC
této	tento	k3xDgFnSc3
době	doba	k1gFnSc3
vděčí	vděčit	k5eAaImIp3nS
za	za	k7c4
gotickou	gotický	k2eAgFnSc4d1
katedrálu	katedrála	k1gFnSc4
sv.	sv.	kA
Ducha	duch	k1gMnSc2
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
Velkém	velký	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dnes	dnes	k6eAd1
vedle	vedle	k6eAd1
Bílé	bílý	k2eAgFnPc1d1
věže	věž	k1gFnPc1
a	a	k8xC
Staré	Staré	k2eAgFnPc1d1
radnice	radnice	k1gFnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
městské	městský	k2eAgFnPc4d1
dominanty	dominanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1766	#num#	k4
až	až	k9
1857	#num#	k4
sloužilo	sloužit	k5eAaImAgNnS
město	město	k1gNnSc1
jako	jako	k8xS,k8xC
vojenská	vojenský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
nezájem	nezájem	k1gInSc4
města	město	k1gNnSc2
obnovená	obnovený	k2eAgFnSc1d1
a	a	k8xC
zrušená	zrušený	k2eAgFnSc1d1
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1884	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
města	město	k1gNnSc2
plně	plně	k6eAd1
využila	využít	k5eAaPmAgFnS
možnosti	možnost	k1gFnPc4
úplného	úplný	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
urbanistického	urbanistický	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
doslova	doslova	k6eAd1
uvolnil	uvolnit	k5eAaPmAgInS
prostor	prostor	k1gInSc1
pro	pro	k7c4
zlatou	zlatý	k2eAgFnSc4d1
éru	éra	k1gFnSc4
královéhradecké	královéhradecký	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
si	se	k3xPyFc3
především	především	k9
díky	díky	k7c3
stavbám	stavba	k1gFnPc3
Gočára	Gočár	k1gMnSc2
a	a	k8xC
Kotěry	Kotěra	k1gFnSc2
město	město	k1gNnSc1
odneslo	odnést	k5eAaPmAgNnS
označení	označení	k1gNnSc1
Salon	salon	k1gInSc4
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
je	být	k5eAaImIp3nS
univerzitní	univerzitní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
,	,	kIx,
vyučuje	vyučovat	k5eAaImIp3nS
zde	zde	k6eAd1
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
některé	některý	k3yIgFnSc2
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
a	a	k8xC
Univerzity	univerzita	k1gFnSc2
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
zde	zde	k6eAd1
např.	např.	kA
krajský	krajský	k2eAgInSc1d1
soud	soud	k1gInSc1
nebo	nebo	k8xC
biskupové	biskup	k1gMnPc1
královéhradecké	královéhradecký	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
církve	církev	k1gFnSc2
katolické	katolický	k2eAgFnSc2d1
a	a	k8xC
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klicperovo	Klicperův	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
je	být	k5eAaImIp3nS
uznávanou	uznávaný	k2eAgFnSc7d1
scénou	scéna	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
čtyřikrát	čtyřikrát	k6eAd1
získala	získat	k5eAaPmAgFnS
ocenění	ocenění	k1gNnSc4
„	„	k?
<g/>
Divadlo	divadlo	k1gNnSc1
roku	rok	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
artkino	artkino	k1gNnSc1
Centrál	centrála	k1gFnPc2
se	se	k3xPyFc4
pyšní	pyšnit	k5eAaImIp3nS
oceněním	ocenění	k1gNnSc7
„	„	k?
<g/>
Nejlepší	dobrý	k2eAgNnSc1d3
evropské	evropský	k2eAgNnSc1d1
kino	kino	k1gNnSc1
pro	pro	k7c4
mladé	mladý	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
<g/>
“	“	k?
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královéhradecký	královéhradecký	k2eAgInSc1d1
Festival	festival	k1gInSc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
bývalého	bývalý	k2eAgNnSc2d1
vojenského	vojenský	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
<g/>
,	,	kIx,
poskytuje	poskytovat	k5eAaImIp3nS
prostor	prostor	k1gInSc4
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
známým	známý	k2eAgMnPc3d1
letním	letní	k2eAgMnPc3d1
festivalům	festival	k1gInPc3
Rock	rock	k1gInSc1
for	forum	k1gNnPc2
People	People	k1gFnSc2
a	a	k8xC
Hip	hip	k0
Hop	hop	k0
Kemp	kemp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Krajina	Krajina	k1gFnSc1
na	na	k7c6
soutoku	soutok	k1gInSc6
Labe	Labe	k1gNnSc2
s	s	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
dominuje	dominovat	k5eAaImIp3nS
město	město	k1gNnSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
obydlena	obydlet	k5eAaPmNgFnS
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
prehistorické	prehistorický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archeologické	archeologický	k2eAgInPc1d1
naleziště	naleziště	k1gNnSc4
na	na	k7c6
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
v	v	k7c6
Plotištích	Plotiště	k1gNnPc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
vykazuje	vykazovat	k5eAaImIp3nS
několikanásobné	několikanásobný	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
jak	jak	k8xS,k8xC
z	z	k7c2
období	období	k1gNnSc2
pravěku	pravěk	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
z	z	k7c2
doby	doba	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kumulace	kumulace	k1gFnSc1
pravěkých	pravěký	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
na	na	k7c6
jednom	jeden	k4xCgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
brodu	brod	k1gInSc2
přes	přes	k7c4
Labe	Labe	k1gNnSc4
a	a	k8xC
charakter	charakter	k1gInSc1
nálezů	nález	k1gInPc2
dokazují	dokazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c4
celé	celý	k2eAgInPc4d1
dlouhé	dlouhý	k2eAgInPc4d1
úseky	úsek	k1gInPc4
svého	svůj	k3xOyFgNnSc2
osídlení	osídlení	k1gNnSc2
měla	mít	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
významná	významný	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
ráz	ráz	k1gInSc1
obchodního	obchodní	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
zde	zde	k6eAd1
slovanské	slovanský	k2eAgNnSc1d1
hradiště	hradiště	k1gNnSc1
s	s	k7c7
rušným	rušný	k2eAgNnSc7d1
tržištěm	tržiště	k1gNnSc7
<g/>
,	,	kIx,
ovládajícím	ovládající	k2eAgNnSc7d1
starou	starý	k2eAgFnSc4d1
obchodní	obchodní	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
od	od	k7c2
Krakova	Krakov	k1gInSc2
přes	přes	k7c4
Náchod	Náchod	k1gInSc4
k	k	k7c3
Praze	Praha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
desátého	desátý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
Hradec	Hradec	k1gInSc1
stal	stát	k5eAaPmAgInS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
center	centrum	k1gNnPc2
hradské	hradský	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
bylo	být	k5eAaImAgNnS
spravováno	spravovat	k5eAaImNgNnS
území	území	k1gNnSc4
severovýchodních	severovýchodní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
Labe	Labe	k1gNnSc4
podle	podle	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
toku	tok	k1gInSc2
od	od	k7c2
Dvora	Dvůr	k1gInSc2
Králové	Králová	k1gFnSc2
až	až	k9
k	k	k7c3
Pardubicím	Pardubice	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zaujímá	zaujímat	k5eAaImIp3nS
Hradecko	Hradecko	k1gNnSc4
již	již	k9
několik	několik	k4yIc4
správních	správní	k2eAgInPc2d1
okrsků	okrsek	k1gInPc2
<g/>
,	,	kIx,
spojených	spojený	k2eAgInPc2d1
v	v	k7c4
celou	celý	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
hrady	hrad	k1gInPc7
<g/>
,	,	kIx,
purkrabským	purkrabský	k2eAgInSc7d1
soudem	soud	k1gInSc7
a	a	k8xC
arcijáhenstvím	arcijáhenství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesné	přesný	k2eAgNnSc4d1
založení	založení	k1gNnSc4
města	město	k1gNnSc2
není	být	k5eNaImIp3nS
známé	známý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivem	vlivem	k7c2
značného	značný	k2eAgInSc2d1
stavebního	stavební	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
,	,	kIx,
terénních	terénní	k2eAgFnPc2d1
úprav	úprava	k1gFnPc2
<g/>
,	,	kIx,
požárů	požár	k1gInPc2
a	a	k8xC
válečných	válečný	k2eAgFnPc2d1
škod	škoda	k1gFnPc2
po	po	k7c4
staletí	staletí	k1gNnPc4
na	na	k7c6
městském	městský	k2eAgInSc6d1
pohorku	pohorek	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
nejstarších	starý	k2eAgFnPc2d3
archeologických	archeologický	k2eAgFnPc2d1
stop	stopa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Osudový	osudový	k2eAgInSc4d1
význam	význam	k1gInSc4
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
městského	městský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
mělo	mít	k5eAaImAgNnS
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Hradec	Hradec	k1gInSc1
s	s	k7c7
tržním	tržní	k2eAgNnSc7d1
předhradím	předhradí	k1gNnSc7
královským	královský	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
1225	#num#	k4
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
v	v	k7c6
písemné	písemný	k2eAgFnSc6d1
listině	listina	k1gFnSc6
(	(	kIx(
<g/>
smlouvě	smlouva	k1gFnSc6
<g/>
)	)	kIx)
připomíná	připomínat	k5eAaImIp3nS
město	město	k1gNnSc1
(	(	kIx(
<g/>
civitas	civitas	k1gInSc1
<g/>
)	)	kIx)
Hradec	Hradec	k1gInSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
s	s	k7c7
přívlastkem	přívlastek	k1gInSc7
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
až	až	k9
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakládající	zakládající	k2eAgFnSc1d1
listina	listina	k1gFnSc1
Hradce	Hradec	k1gInSc2
jako	jako	k8xC,k8xS
města	město	k1gNnSc2
se	se	k3xPyFc4
nedochovala	dochovat	k5eNaPmAgFnS
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
přesně	přesně	k6eAd1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
založeno	založit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
písemná	písemný	k2eAgFnSc1d1
listina	listina	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1225	#num#	k4
je	být	k5eAaImIp3nS
prvním	první	k4xOgInSc7
nezpochybnitelným	zpochybnitelný	k2eNgInSc7d1
pramenným	pramenný	k2eAgInSc7d1
dokladem	doklad	k1gInSc7
o	o	k7c4
existenci	existence	k1gFnSc4
města	město	k1gNnSc2
<g/>
...	...	k?
Ještě	ještě	k6eAd1
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
gotický	gotický	k2eAgInSc1d1
královský	královský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
často	často	k6eAd1
přebývali	přebývat	k5eAaImAgMnP
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
i	i	k8xC
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
ustanovil	ustanovit	k5eAaPmAgInS
Václav	Václav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
za	za	k7c4
část	část	k1gFnSc4
věna	věno	k1gNnSc2
českým	český	k2eAgFnPc3d1
královnám	královna	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
na	na	k7c6
hradě	hrad	k1gInSc6
žily	žít	k5eAaImAgFnP
jako	jako	k9
vdovy	vdova	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založení	založení	k1gNnSc1
města	město	k1gNnSc2
bylo	být	k5eAaImAgNnS
zprvu	zprvu	k6eAd1
hospodářským	hospodářský	k2eAgInSc7d1
a	a	k8xC
právním	právní	k2eAgInSc7d1
aktem	akt	k1gInSc7
<g/>
,	,	kIx,
urbanistický	urbanistický	k2eAgMnSc1d1
dosah	dosah	k1gInSc4
městského	městský	k2eAgNnSc2d1
založení	založení	k1gNnSc2
se	se	k3xPyFc4
dostavil	dostavit	k5eAaPmAgMnS
později	pozdě	k6eAd2
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
13	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
rozvoji	rozvoj	k1gInSc6
města	město	k1gNnSc2
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
němečtí	německý	k2eAgMnPc1d1
kolonisté	kolonista	k1gMnPc1
a	a	k8xC
početná	početný	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
kupců	kupec	k1gMnPc2
a	a	k8xC
řemeslníků	řemeslník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
osídlili	osídlit	k5eAaPmAgMnP
ostroh	ostroh	k1gInSc4
v	v	k7c6
celém	celý	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
vytvořila	vytvořit	k5eAaPmAgFnS
i	i	k9
půdorysná	půdorysný	k2eAgFnSc1d1
osnova	osnova	k1gFnSc1
původní	původní	k2eAgFnSc1d1
městské	městský	k2eAgNnSc4d1
opevnění	opevnění	k1gNnSc4
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
při	při	k7c6
úpatí	úpatí	k1gNnSc6
návrší	návrš	k1gFnPc2
kolem	kolem	k7c2
celého	celý	k2eAgInSc2d1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podoba	podoba	k1gFnSc1
středověkého	středověký	k2eAgNnSc2d1
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vzrostla	vzrůst	k5eAaPmAgFnS
prestiž	prestiž	k1gFnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
hlavou	hlava	k1gFnSc7
byl	být	k5eAaImAgInS
královský	královský	k2eAgMnSc1d1
rychtář	rychtář	k1gMnSc1
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
a	a	k8xC
obecní	obecní	k2eAgNnSc1d1
starší	starý	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spravovali	spravovat	k5eAaImAgMnP
městskou	městský	k2eAgFnSc4d1
pečeť	pečeť	k1gFnSc4
s	s	k7c7
českým	český	k2eAgMnSc7d1
lvem	lev	k1gMnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
známe	znát	k5eAaImIp1nP
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1362	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářskou	hospodářský	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
města	město	k1gNnSc2
posílily	posílit	k5eAaPmAgFnP
výhody	výhoda	k1gFnPc1
a	a	k8xC
dary	dar	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
městu	město	k1gNnSc3
potvrdili	potvrdit	k5eAaPmAgMnP
čeští	český	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
i	i	k8xC
Václav	Václav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
kodifikaci	kodifikace	k1gFnSc3
městského	městský	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
<g/>
;	;	kIx,
monopolní	monopolní	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
městských	městský	k2eAgNnPc2d1
řemesel	řemeslo	k1gNnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
výsadní	výsadní	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
hradeckých	hradecký	k2eAgInPc2d1
cechů	cech	k1gInPc2
bylo	být	k5eAaImAgNnS
zaručeno	zaručit	k5eAaPmNgNnS
právem	právo	k1gNnSc7
mílovým	mílový	k2eAgNnSc7d1
<g/>
,	,	kIx,
příjmy	příjem	k1gInPc1
měšťanů	měšťan	k1gMnPc2
zaručovalo	zaručovat	k5eAaImAgNnS
zase	zase	k9
právo	právo	k1gNnSc1
várečné	várečný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
svým	svůj	k3xOyFgInSc7
významem	význam	k1gInSc7
<g/>
,	,	kIx,
rozlohou	rozloha	k1gFnSc7
a	a	k8xC
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
nejvýznačnějším	význačný	k2eAgNnSc7d3
českým	český	k2eAgNnSc7d1
městem	město	k1gNnSc7
po	po	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotným	hmotný	k2eAgInSc7d1
dokladem	doklad	k1gInSc7
bohatství	bohatství	k1gNnSc2
města	město	k1gNnSc2
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
Hradci	Hradec	k1gInSc6
sídlil	sídlit	k5eAaImAgInS
skvělý	skvělý	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
královny	královna	k1gFnSc2
Elišky	Eliška	k1gFnSc2
Rejčky	Rejčka	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chrám	chrám	k1gInSc1
sv.	sv.	kA
Ducha	duch	k1gMnSc2
z	z	k7c2
počátku	počátek	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
Velkém	velký	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
stavební	stavební	k2eAgInSc1d1
ruch	ruch	k1gInSc1
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
také	také	k9
v	v	k7c6
obou	dva	k4xCgNnPc6
podměstích	podměstí	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
západním	západní	k2eAgNnSc6d1
Pražském	pražský	k2eAgNnSc6d1
u	u	k7c2
Labe	Labe	k1gNnSc2
a	a	k8xC
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Mýtském	mýtský	k2eAgNnSc6d1
u	u	k7c2
Orlice	Orlice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc1
území	území	k1gNnSc3
tvořila	tvořit	k5eAaImAgFnS
bohatá	bohatý	k2eAgFnSc1d1
spleť	spleť	k1gFnSc1
patnácti	patnáct	k4xCc2
ostrovů	ostrov	k1gInPc2
mezi	mezi	k7c7
rameny	rameno	k1gNnPc7
obou	dva	k4xCgFnPc2
řek	řeka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
spojovalo	spojovat	k5eAaImAgNnS
šestnáct	šestnáct	k4xCc4
mostů	most	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
předměstích	předměstí	k1gNnPc6
bylo	být	k5eAaImAgNnS
sedm	sedm	k4xCc1
farních	farní	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
kláštery	klášter	k1gInPc4
a	a	k8xC
tři	tři	k4xCgInPc4
špitály	špitál	k1gInPc4
pro	pro	k7c4
chudé	chudý	k2eAgMnPc4d1
a	a	k8xC
nemocné	nemocný	k1gMnPc4
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgFnPc6
byly	být	k5eAaImAgInP
také	také	k9
kostely	kostel	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Výbuch	výbuch	k1gInSc1
husitské	husitský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
město	město	k1gNnSc1
postavilo	postavit	k5eAaPmAgNnS
na	na	k7c4
stranu	strana	k1gFnSc4
Jana	Jan	k1gMnSc2
Žižky	Žižka	k1gMnSc2
<g/>
,	,	kIx,
sice	sice	k8xC
počeštil	počeštit	k5eAaPmAgMnS
město	město	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
ochudil	ochudit	k5eAaPmAgMnS
o	o	k7c4
četné	četný	k2eAgInPc4d1
umělecké	umělecký	k2eAgInPc4d1
a	a	k8xC
stavitelské	stavitelský	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žižka	Žižka	k1gMnSc1
byl	být	k5eAaImAgMnS
roku	rok	k1gInSc2
1424	#num#	k4
pochován	pochovat	k5eAaPmNgMnS
v	v	k7c6
chrámu	chrám	k1gInSc6
sv.	sv.	kA
Ducha	duch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konsolidaci	konsolidace	k1gFnSc3
hospodářského	hospodářský	k2eAgInSc2d1
a	a	k8xC
kulturního	kulturní	k2eAgInSc2d1
života	život	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
až	až	k9
za	za	k7c2
vlády	vláda	k1gFnSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
a	a	k8xC
Vladislava	Vladislav	k1gMnSc2
Jagellonského	jagellonský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
městu	město	k1gNnSc3
přál	přát	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
byl	být	k5eAaImAgInS
opraven	opraven	k2eAgInSc1d1
chrám	chrám	k1gInSc1
sv.	sv.	kA
Ducha	duch	k1gMnSc2
<g/>
,	,	kIx,
pořízena	pořízen	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
kruchta	kruchta	k1gFnSc1
a	a	k8xC
postavena	postaven	k2eAgFnSc1d1
honosná	honosný	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
na	na	k7c6
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ovšem	ovšem	k9
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1782	#num#	k4
stržena	stržen	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
G	G	kA
<g/>
“	“	k?
v	v	k7c6
městském	městský	k2eAgInSc6d1
znaku	znak	k1gInSc6
vysvětluje	vysvětlovat	k5eAaImIp3nS
tradice	tradice	k1gFnSc1
jako	jako	k8xC,k8xS
monogram	monogram	k1gInSc1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
potvrdili	potvrdit	k5eAaPmAgMnP
městu	město	k1gNnSc3
stará	starý	k2eAgNnPc4d1
privilegia	privilegium	k1gNnPc4
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
mohl	moct	k5eAaImAgInS
znovu	znovu	k6eAd1
zařadit	zařadit	k5eAaPmF
mezi	mezi	k7c4
nejbohatší	bohatý	k2eAgNnPc4d3
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Demokratizace	demokratizace	k1gFnSc1
<g/>
,	,	kIx,
projevující	projevující	k2eAgFnSc1d1
se	s	k7c7
zvýšeným	zvýšený	k2eAgNnSc7d1
uplatňováním	uplatňování	k1gNnSc7
městského	městský	k2eAgInSc2d1
stavu	stav	k1gInSc2
byla	být	k5eAaImAgFnS
živnou	živný	k2eAgFnSc7d1
půdou	půda	k1gFnSc7
pro	pro	k7c4
odboj	odboj	k1gInSc4
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
a	a	k8xC
královských	královský	k2eAgNnPc2d1
měst	město	k1gNnPc2
proti	proti	k7c3
císaři	císař	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
mocenskou	mocenský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
svého	svůj	k3xOyFgInSc2
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovědí	odpověď	k1gFnPc2
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1547	#num#	k4
konfiskace	konfiskace	k1gFnSc1
majetku	majetek	k1gInSc2
města	město	k1gNnSc2
a	a	k8xC
ztráta	ztráta	k1gFnSc1
politických	politický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
dosazením	dosazení	k1gNnSc7
královského	královský	k2eAgMnSc2d1
rychtáře	rychtář	k1gMnSc2
<g/>
,	,	kIx,
ochuzení	ochuzený	k2eAgMnPc1d1
půjčkami	půjčka	k1gFnPc7
<g/>
,	,	kIx,
daněmi	daň	k1gFnPc7
a	a	k8xC
pokutami	pokuta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
majetku	majetek	k1gInSc2
byla	být	k5eAaImAgFnS
sice	sice	k8xC
roku	rok	k1gInSc2
1562	#num#	k4
městu	město	k1gNnSc3
vrácena	vrácen	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
vůdčí	vůdčí	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
v	v	k7c6
kraji	kraj	k1gInSc6
město	město	k1gNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
těžkých	těžký	k2eAgInPc2d1
hospodářských	hospodářský	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
vyvedl	vyvést	k5eAaPmAgMnS
město	město	k1gNnSc4
primas	primas	k1gMnSc1
Martin	Martin	k1gMnSc1
Cejp	Cejp	k1gMnSc1
z	z	k7c2
Peclinovce	Peclinovec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
více	hodně	k6eAd2
než	než	k8xS
třicetiletého	třicetiletý	k2eAgNnSc2d1
působení	působení	k1gNnSc2
na	na	k7c6
radnici	radnice	k1gFnSc6
byla	být	k5eAaImAgFnS
velkorysá	velkorysý	k2eAgFnSc1d1
renesanční	renesanční	k2eAgFnSc1d1
přestavba	přestavba	k1gFnSc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
nové	nový	k2eAgNnSc4d1
dláždění	dláždění	k1gNnSc4
<g/>
,	,	kIx,
úpravy	úprava	k1gFnPc4
radnice	radnice	k1gFnSc2
<g/>
,	,	kIx,
opevnění	opevnění	k1gNnSc1
<g/>
,	,	kIx,
stavba	stavba	k1gFnSc1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
předbraní	předbraní	k1gNnSc2
Pražské	pražský	k2eAgFnSc2d1
brány	brána	k1gFnSc2
a	a	k8xC
Bílé	bílý	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nejtypičtější	typický	k2eAgFnSc7d3
a	a	k8xC
nepředstiženou	předstižený	k2eNgFnSc7d1
hradeckou	hradecký	k2eAgFnSc7d1
dominantou	dominanta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cejp	Cejp	k1gInSc1
pečoval	pečovat	k5eAaImAgInS
rovněž	rovněž	k9
o	o	k7c4
úroveň	úroveň	k1gFnSc4
hradeckého	hradecký	k2eAgNnSc2d1
latinského	latinský	k2eAgNnSc2d1
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
době	doba	k1gFnSc6
byli	být	k5eAaImAgMnP
v	v	k7c6
čele	čelo	k1gNnSc6
hradecké	hradecký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
vynikající	vynikající	k2eAgMnSc1d1
rektoři	rektor	k1gMnPc1
Valentin	Valentin	k1gMnSc1
Kochan	kochan	k1gMnSc1
z	z	k7c2
Prachové	prachový	k2eAgFnSc2d1
a	a	k8xC
Jan	Jan	k1gMnSc1
Kampanus	Kampanus	k1gMnSc1
Vodňanský	vodňanský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnými	významný	k2eAgMnPc7d1
rodáky	rodák	k1gMnPc7
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byli	být	k5eAaImAgMnP
Cyprian	Cyprian	k1gMnSc1
Lvovický	Lvovický	k2eAgMnSc1d1
ze	z	k7c2
Lvovic	Lvovice	k1gFnPc2
a	a	k8xC
Václav	Václav	k1gMnSc1
Plácel	Plácel	k1gMnSc1
z	z	k7c2
Elbinku	Elbink	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
pohromou	pohroma	k1gFnSc7
pro	pro	k7c4
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umístění	umístění	k1gNnSc1
císařské	císařský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
švédská	švédský	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
<g/>
,	,	kIx,
příchod	příchod	k1gInSc1
jezuitů	jezuita	k1gMnPc2
<g/>
,	,	kIx,
násilné	násilný	k2eAgNnSc4d1
nucení	nucení	k1gNnSc4
ke	k	k7c3
katolické	katolický	k2eAgFnSc3d1
víře	víra	k1gFnSc3
–	–	k?
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
sužovalo	sužovat	k5eAaImAgNnS
město	město	k1gNnSc4
hmotně	hmotně	k6eAd1
i	i	k9
duchovně	duchovně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédské	švédský	k2eAgInPc1d1
vpády	vpád	k1gInPc1
postihly	postihnout	k5eAaPmAgFnP
Hradec	Hradec	k1gInSc4
nejen	nejen	k6eAd1
vysokým	vysoký	k2eAgNnPc3d1
výpalným	výpalné	k1gNnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
těžkými	těžký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
na	na	k7c6
obytných	obytný	k2eAgInPc6d1
domech	dům	k1gInPc6
na	na	k7c6
předměstích	předměstí	k1gNnPc6
a	a	k8xC
na	na	k7c6
uměleckých	umělecký	k2eAgFnPc6d1
a	a	k8xC
stavebních	stavební	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
a	a	k8xC
lidnatá	lidnatý	k2eAgNnPc1d1
předměstí	předměstí	k1gNnPc1
byla	být	k5eAaImAgNnP
fortifikacemi	fortifikace	k1gFnPc7
<g/>
,	,	kIx,
vojenskými	vojenský	k2eAgFnPc7d1
akcemi	akce	k1gFnPc7
a	a	k8xC
požáry	požár	k1gInPc7
proměněna	proměnit	k5eAaPmNgFnS
v	v	k7c4
poušť	poušť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zmenšená	zmenšený	k2eAgFnSc1d1
veduta	veduta	k1gFnSc1
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1602	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1567	#num#	k4
byl	být	k5eAaImAgMnS
prováděn	provádět	k5eAaImNgMnS
v	v	k7c6
královských	královský	k2eAgNnPc6d1
městech	město	k1gNnPc6
soupis	soupis	k1gInSc4
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
jich	on	k3xPp3gNnPc2
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
780	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1664	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
zřízeno	zřízen	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Ducha	duch	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
katedrálním	katedrální	k2eAgInSc7d1
chrámem	chrám	k1gInSc7
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
šestičlenná	šestičlenný	k2eAgFnSc1d1
kanovnická	kanovnický	k2eAgFnSc1d1
kapitula	kapitula	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
nabylo	nabýt	k5eAaPmAgNnS
barokní	barokní	k2eAgFnPc4d1
tvářnosti	tvářnost	k1gFnPc4
stavební	stavební	k2eAgFnPc4d1
aktivitou	aktivita	k1gFnSc7
biskupa	biskup	k1gInSc2
a	a	k8xC
jezuitů	jezuita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přizvali	přizvat	k5eAaPmAgMnP
si	se	k3xPyFc3
na	na	k7c4
stavbu	stavba	k1gFnSc4
tehdejší	tehdejší	k2eAgMnPc4d1
vynikající	vynikající	k2eAgMnPc4d1
architekty	architekt	k1gMnPc4
(	(	kIx(
<g/>
Carlo	Carla	k1gMnSc5
Lurago	Luraga	k1gMnSc5
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Blažej	Blažej	k1gMnSc1
Santini-Aichel	Santini-Aichel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
na	na	k7c6
náměstí	náměstí	k1gNnSc6
vyrostla	vyrůst	k5eAaPmAgFnS
monumentální	monumentální	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
chrámu	chrám	k1gInSc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
s	s	k7c7
přilehlou	přilehlý	k2eAgFnSc7d1
jezuitskou	jezuitský	k2eAgFnSc7d1
kolejí	kolej	k1gFnSc7
<g/>
,	,	kIx,
biskupská	biskupský	k2eAgFnSc1d1
rezidence	rezidence	k1gFnSc1
<g/>
,	,	kIx,
vznosná	vznosný	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Klimenta	Kliment	k1gMnSc2
<g/>
,	,	kIx,
nový	nový	k2eAgInSc1d1
morový	morový	k2eAgInSc1d1
sloup	sloup	k1gInSc1
a	a	k8xC
v	v	k7c6
místech	místo	k1gNnPc6
bývalého	bývalý	k2eAgInSc2d1
hradu	hrad	k1gInSc2
seminární	seminární	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Podoba	podoba	k1gFnSc1
pevnosti	pevnost	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
ze	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
K	k	k7c3
dalšímu	další	k2eAgInSc3d1
stavebnímu	stavební	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
nedošlo	dojít	k5eNaPmAgNnS
pro	pro	k7c4
válku	válka	k1gFnSc4
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
a	a	k8xC
válku	válka	k1gFnSc4
sedmiletou	sedmiletý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žalostný	žalostný	k2eAgInSc1d1
osud	osud	k1gInSc1
města	město	k1gNnSc2
dovršil	dovršit	k5eAaPmAgInS
požár	požár	k1gInSc1
roku	rok	k1gInSc2
1762	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
téměř	téměř	k6eAd1
polovina	polovina	k1gFnSc1
města	město	k1gNnSc2
podlehla	podlehnout	k5eAaPmAgFnS
ohni	oheň	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
založili	založit	k5eAaPmAgMnP
současně	současně	k6eAd1
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
pruští	pruský	k2eAgMnPc1d1
vetřelci	vetřelec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pruské	pruský	k2eAgInPc1d1
vpády	vpád	k1gInPc1
do	do	k7c2
země	zem	k1gFnSc2
přinutily	přinutit	k5eAaPmAgInP
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
spoluvladaře	spoluvladař	k1gMnSc4
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
vybudovat	vybudovat	k5eAaPmF
z	z	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
pevnost	pevnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbě	stavba	k1gFnSc3
pevnosti	pevnost	k1gFnSc2
<g/>
,	,	kIx,
prováděné	prováděný	k2eAgInPc1d1
ve	v	k7c6
dvou	dva	k4xCgFnPc6
etapách	etapa	k1gFnPc6
v	v	k7c6
letech	léto	k1gNnPc6
1766	#num#	k4
až	až	k9
1789	#num#	k4
<g/>
,	,	kIx,
musela	muset	k5eAaImAgFnS
ustoupit	ustoupit	k5eAaPmF
předměstí	předměstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vystěhováno	vystěhovat	k5eAaPmNgNnS
daleko	daleko	k6eAd1
za	za	k7c4
vnější	vnější	k2eAgInSc4d1
okraj	okraj	k1gInSc4
inundačního	inundační	k2eAgNnSc2d1
a	a	k8xC
demoličního	demoliční	k2eAgNnSc2d1
území	území	k1gNnSc2
pevnosti	pevnost	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
nově	nově	k6eAd1
založených	založený	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
Nového	Nového	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
<g/>
,	,	kIx,
Kuklen	kuklen	k2eAgInSc1d1
a	a	k8xC
Pouchova	Pouchův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustavu	soustava	k1gFnSc4
hradeb	hradba	k1gFnPc2
doplňovaly	doplňovat	k5eAaImAgFnP
účelové	účelový	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgFnPc1
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgFnP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Germanizace	germanizace	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
přinesla	přinést	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
dala	dát	k5eAaPmAgFnS
městu	město	k1gNnSc3
zevní	zevní	k2eAgFnSc2d1
německý	německý	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastenecká	vlastenecký	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
však	však	k9
žila	žít	k5eAaImAgFnS
ve	v	k7c6
městě	město	k1gNnSc6
od	od	k7c2
dob	doba	k1gFnPc2
jezuitů	jezuita	k1gMnPc2
<g/>
,	,	kIx,
Bohuslava	Bohuslav	k1gMnSc2
Balbína	Balbín	k1gMnSc2
a	a	k8xC
matematika	matematik	k1gMnSc2
Stanislava	Stanislav	k1gMnSc2
Vydry	Vydra	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
narodili	narodit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
českého	český	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
obrození	obrození	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
město	město	k1gNnSc1
slavně	slavně	k6eAd1
zapsalo	zapsat	k5eAaPmAgNnS
do	do	k7c2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulturní	kulturní	k2eAgInSc4d1
a	a	k8xC
společenský	společenský	k2eAgInSc4d1
život	život	k1gInSc4
obrozeneckého	obrozenecký	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
soustřeďoval	soustřeďovat	k5eAaImAgMnS
kolem	kolem	k7c2
čtyř	čtyři	k4xCgFnPc2
institucí	instituce	k1gFnPc2
<g/>
:	:	kIx,
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
gymnázia	gymnázium	k1gNnPc1
<g/>
,	,	kIx,
semináře	seminář	k1gInPc1
a	a	k8xC
nakladatelství	nakladatelství	k1gNnSc1
s	s	k7c7
knihkupectvím	knihkupectví	k1gNnSc7
Jana	Jan	k1gMnSc2
Hostivíta	Hostivít	k1gMnSc2
Pospíšila	Pospíšil	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
významný	významný	k2eAgMnSc1d1
vlastenecký	vlastenecký	k2eAgMnSc1d1
nakladatel	nakladatel	k1gMnSc1
stál	stát	k5eAaImAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
nadšené	nadšený	k2eAgFnSc2d1
družiny	družina	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
tvořili	tvořit	k5eAaImAgMnP
dramatik	dramatik	k1gMnSc1
Václav	Václav	k1gMnSc1
Kliment	Kliment	k1gMnSc1
Klicpera	Klicpera	k1gFnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Josef	Josef	k1gMnSc1
Chmela	Chmel	k1gMnSc2
<g/>
,	,	kIx,
kněz	kněz	k1gMnSc1
Josef	Josef	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Ziegler	Ziegler	k1gMnSc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
přátelé	přítel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
žili	žít	k5eAaImAgMnP
v	v	k7c6
kraji	kraj	k1gInSc6
a	a	k8xC
s	s	k7c7
nimiž	jenž	k3xRgInPc7
byli	být	k5eAaImAgMnP
hradečtí	hradecký	k2eAgMnPc1d1
buditelé	buditel	k1gMnPc1
ve	v	k7c6
spojení	spojení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Však	však	k8xC
také	také	k9
Pospíšilův	Pospíšilův	k2eAgInSc1d1
dům	dům	k1gInSc1
hostil	hostit	k5eAaImAgInS
vlastence	vlastenec	k1gMnSc4
z	z	k7c2
celé	celý	k2eAgFnSc2d1
země	zem	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
Hanku	Hanka	k1gFnSc4
<g/>
,	,	kIx,
Rettigovou	Rettigový	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgInPc7
nechyběli	chybět	k5eNaImAgMnP
ani	ani	k9
Ľudovít	Ľudovít	k1gMnPc1
Štúr	Štúr	k1gMnSc1
a	a	k8xC
Jozef	Jozef	k1gMnSc1
Miloslav	Miloslav	k1gMnSc1
Hurban	Hurban	k1gMnSc1
ze	z	k7c2
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hradeckém	hradecký	k2eAgNnSc6d1
gymnasiu	gymnasion	k1gNnSc6
tehdy	tehdy	k6eAd1
studovali	studovat	k5eAaImAgMnP
Václav	Václav	k1gMnSc1
Hanka	Hanka	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Langer	Langer	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Kajetán	Kajetán	k1gMnSc1
Tyl	Tyl	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Erben	Erben	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Škroup	Škroup	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
divadle	divadlo	k1gNnSc6
„	„	k?
<g/>
U	u	k7c2
zlatého	zlatý	k2eAgMnSc2d1
orla	orel	k1gMnSc2
<g/>
“	“	k?
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgNnP
česká	český	k2eAgNnPc1d1
představení	představení	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
organizoval	organizovat	k5eAaBmAgMnS
Václav	Václav	k1gMnSc1
Kliment	Kliment	k1gMnSc1
Klicpera	Klicpera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1848	#num#	k4
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
utvořila	utvořit	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc4
prapor	prapor	k1gInSc4
namaloval	namalovat	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Mánes	Mánes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bachův	Bachův	k2eAgInSc4d1
absolutismus	absolutismus	k1gInSc4
sice	sice	k8xC
na	na	k7c4
čas	čas	k1gInSc4
zlomil	zlomit	k5eAaPmAgInS
národní	národní	k2eAgInSc1d1
rozkvět	rozkvět	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
šedesátá	šedesátý	k4xOgNnPc1
léta	léto	k1gNnPc1
jsou	být	k5eAaImIp3nP
opět	opět	k6eAd1
obdobím	období	k1gNnSc7
rozvoje	rozvoj	k1gInSc2
českého	český	k2eAgInSc2d1
živlu	živel	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásluhu	zásluha	k1gFnSc4
o	o	k7c4
to	ten	k3xDgNnSc4
měli	mít	k5eAaImAgMnP
František	František	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Kampelík	Kampelík	k1gMnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
v	v	k7c6
Kuklenách	Kuklena	k1gFnPc6
<g/>
,	,	kIx,
Kristian	Kristian	k1gMnSc1
Stefan	Stefan	k1gMnSc1
<g/>
,	,	kIx,
přítel	přítel	k1gMnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
a	a	k8xC
dopisovatel	dopisovatel	k1gMnSc1
České	český	k2eAgFnSc2d1
včely	včela	k1gFnSc2
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
Václav	Václav	k1gMnSc1
František	František	k1gMnSc1
Červený	Červený	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
slavné	slavný	k2eAgFnSc2d1
hradecké	hradecký	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
na	na	k7c4
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
kresba	kresba	k1gFnSc1
Johanna	Johann	k1gMnSc2
Venuta	Venut	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
</s>
<s>
Roku	rok	k1gInSc2
1851	#num#	k4
byl	být	k5eAaImAgInS
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
prohlášen	prohlášen	k2eAgInSc1d1
samostatným	samostatný	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
zvolen	zvolen	k2eAgMnSc1d1
jeho	jeho	k3xOp3gMnSc1
první	první	k4xOgMnSc1
starosta	starosta	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
hospodářství	hospodářství	k1gNnSc2
Ignác	Ignác	k1gMnSc1
Lhotský	Lhotský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
dosavadní	dosavadní	k2eAgFnSc2d1
součásti	součást	k1gFnSc2
vně	vně	k7c2
hradeb	hradba	k1gFnPc2
získaly	získat	k5eAaPmAgInP
samostatnost	samostatnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územně	územně	k6eAd1
a	a	k8xC
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
okleštěné	okleštěný	k2eAgNnSc1d1
město	město	k1gNnSc1
usilovalo	usilovat	k5eAaImAgNnS
o	o	k7c4
dosažení	dosažení	k1gNnSc4
dalších	další	k2eAgFnPc2d1
hospodářských	hospodářský	k2eAgFnPc2d1
výhod	výhoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1857	#num#	k4
bylo	být	k5eAaImAgNnS
spojeno	spojit	k5eAaPmNgNnS
se	s	k7c7
světem	svět	k1gInSc7
železnicí	železnice	k1gFnSc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
založilo	založit	k5eAaPmAgNnS
cukrovar	cukrovar	k1gInSc4
<g/>
,	,	kIx,
strojírnu	strojírna	k1gFnSc4
<g/>
,	,	kIx,
plynárnu	plynárna	k1gFnSc4
<g/>
,	,	kIx,
záložnu	záložna	k1gFnSc4
a	a	k8xC
spořitelnu	spořitelna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušen	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
se	se	k3xPyFc4
však	však	k9
zaleklo	zaleknout	k5eAaPmAgNnS
nákladů	náklad	k1gInPc2
na	na	k7c4
likvidaci	likvidace	k1gFnSc4
a	a	k8xC
pevnostní	pevnostní	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
neodkoupilo	odkoupit	k5eNaPmAgNnS
ani	ani	k8xC
při	při	k7c6
druhé	druhý	k4xOgFnSc6
nabídce	nabídka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1873	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erár	erár	k1gInSc4
proto	proto	k8xC
pevnost	pevnost	k1gFnSc4
obnovil	obnovit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
demoličního	demoliční	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1864	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
světoznámá	světoznámý	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
na	na	k7c4
piana	piano	k1gNnPc4
firmy	firma	k1gFnSc2
Antonín	Antonín	k1gMnSc1
Petrof	Petrof	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
<g/>
,	,	kIx,
malba	malba	k1gFnSc1
Eduarda	Eduard	k1gMnSc2
Gurka	Gurek	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1836	#num#	k4
</s>
<s>
Roku	rok	k1gInSc2
1866	#num#	k4
se	se	k3xPyFc4
nedaleko	daleko	k6eNd1
od	od	k7c2
města	město	k1gNnSc2
rozhodla	rozhodnout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Rakouskem	Rakousko	k1gNnSc7
a	a	k8xC
Pruskem	Prusko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vina	vina	k1gFnSc1
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
hledána	hledat	k5eAaImNgFnS
i	i	k9
v	v	k7c6
nečinnosti	nečinnost	k1gFnSc6
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
zrušení	zrušení	k1gNnSc6
pevnosti	pevnost	k1gFnSc2
<g/>
,	,	kIx,
odprodej	odprodej	k1gInSc4
vojenských	vojenský	k2eAgInPc2d1
pevnostních	pevnostní	k2eAgInPc2d1
objektů	objekt	k1gInPc2
a	a	k8xC
pozemků	pozemek	k1gInPc2
městu	město	k1gNnSc3
a	a	k8xC
zbourání	zbourání	k1gNnSc4
hradeb	hradba	k1gFnPc2
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
úřadující	úřadující	k2eAgMnSc1d1
náměstek	náměstek	k1gMnSc1
purkmistra	purkmistr	k1gMnSc2
Ladislav	Ladislav	k1gMnSc1
Jan	Jan	k1gMnSc1
Pospíšil	Pospíšil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vleklé	vleklý	k2eAgFnPc4d1
jednání	jednání	k1gNnPc4
skončilo	skončit	k5eAaPmAgNnS
až	až	k9
roku	rok	k1gInSc2
1893	#num#	k4
osnovou	osnova	k1gFnSc7
zákona	zákon	k1gInSc2
o	o	k7c6
prodeji	prodej	k1gInSc6
fortifikačních	fortifikační	k2eAgInPc2d1
objektů	objekt	k1gInPc2
a	a	k8xC
pozemků	pozemek	k1gInPc2
městu	město	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
tuto	tento	k3xDgFnSc4
zprávu	zpráva	k1gFnSc4
šťastný	šťastný	k2eAgInSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
vyčerpaný	vyčerpaný	k2eAgMnSc1d1
Pospíšil	Pospíšil	k1gMnSc1
oznamoval	oznamovat	k5eAaImAgMnS
obecnímu	obecní	k2eAgNnSc3d1
zastupitelstvu	zastupitelstvo	k1gNnSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
stižen	stihnout	k5eAaPmNgInS
mozkovou	mozkový	k2eAgFnSc7d1
mrtvicí	mrtvice	k1gFnSc7
a	a	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
skonal	skonat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradby	hradba	k1gFnPc1
byly	být	k5eAaImAgFnP
na	na	k7c6
základě	základ	k1gInSc6
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
erárem	erár	k1gInSc7
odstraněny	odstranit	k5eAaPmNgFnP
v	v	k7c6
průběhu	průběh	k1gInSc6
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
zachování	zachování	k1gNnSc4
nejvýznamnějších	významný	k2eAgFnPc2d3
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulturní	kulturní	k2eAgInSc1d1
život	život	k1gInSc1
se	se	k3xPyFc4
koncem	koncem	k7c2
století	století	k1gNnSc2
projevil	projevit	k5eAaPmAgInS
v	v	k7c6
bohatém	bohatý	k2eAgInSc6d1
spolkovém	spolkový	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
Klicperovo	Klicperův	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
založeno	založit	k5eAaPmNgNnS
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
živý	živý	k2eAgInSc1d1
kulturní	kulturní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
panoval	panovat	k5eAaImAgInS
i	i	k9
na	na	k7c6
hradeckém	hradecký	k2eAgNnSc6d1
gymnasiu	gymnasion	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
žákem	žák	k1gMnSc7
byl	být	k5eAaImAgInS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
Alois	Alois	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
i	i	k9
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
a	a	k8xC
Emil	Emil	k1gMnSc1
Vachek	Vachek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Starostou	Starosta	k1gMnSc7
města	město	k1gNnSc2
byl	být	k5eAaImAgMnS
roku	rok	k1gInSc2
1895	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
JUDr.	JUDr.	kA
František	František	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
po	po	k7c6
30	#num#	k4
let	léto	k1gNnPc2
svého	svůj	k3xOyFgInSc2
funkčního	funkční	k2eAgInSc2d1
období	období	k1gNnSc3
organizoval	organizovat	k5eAaBmAgInS
budování	budování	k1gNnSc4
moderní	moderní	k2eAgFnSc2d1
metropole	metropol	k1gFnSc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gInSc7
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
místostarostou	místostarosta	k1gMnSc7
všemi	všecek	k3xTgInPc7
hlasy	hlas	k1gInPc7
Wilhelm	Wilhelm	k1gMnSc1
Waldek	Waldek	k1gMnSc1
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1
velkoprůmyslníka	velkoprůmyslník	k1gMnSc2
Františka	František	k1gMnSc2
von	von	k1gInSc1
Waldek	Waldek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátečním	počáteční	k2eAgNnSc6d1
velmi	velmi	k6eAd1
náročném	náročný	k2eAgNnSc6d1
období	období	k1gNnSc6
rozvoje	rozvoj	k1gInSc2
se	se	k3xPyFc4
bourala	bourat	k5eAaImAgFnS
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
upravovaly	upravovat	k5eAaImAgInP
uvolněné	uvolněný	k2eAgInPc1d1
pozemky	pozemek	k1gInPc1
<g/>
,	,	kIx,
stavěly	stavět	k5eAaImAgFnP
nové	nový	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
<g/>
,	,	kIx,
komunikace	komunikace	k1gFnPc4
<g/>
,	,	kIx,
vznikaly	vznikat	k5eAaImAgInP
první	první	k4xOgInPc1
regulační	regulační	k2eAgInPc1d1
plány	plán	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
jsou	být	k5eAaImIp3nP
do	do	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králové	k2eAgMnPc1d1
zváni	zván	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
z	z	k7c2
Vídně	Vídeň	k1gFnSc2
a	a	k8xC
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
vyznávající	vyznávající	k2eAgFnSc2d1
zásady	zásada	k1gFnSc2
moderní	moderní	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokladem	doklad	k1gInSc7
jejich	jejich	k3xOp3gNnSc2
působení	působení	k1gNnSc2
je	být	k5eAaImIp3nS
např.	např.	kA
budova	budova	k1gFnSc1
Obchodní	obchodní	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
na	na	k7c4
nám.	nám.	k?
Svobody	svoboda	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Okresní	okresní	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Palackého	Palackého	k2eAgNnSc6d1
ul	ul	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
přístavba	přístavba	k1gFnSc1
Grandhotelu	grandhotel	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
mnohé	mnohý	k2eAgFnPc4d1
další	další	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
1909	#num#	k4
je	být	k5eAaImIp3nS
dalším	další	k2eAgInSc7d1
milníkem	milník	k1gInSc7
pro	pro	k7c4
formování	formování	k1gNnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vypsána	vypsán	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
na	na	k7c4
nový	nový	k2eAgInSc4d1
regulační	regulační	k2eAgInSc4d1
plán	plán	k1gInSc4
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
díla	dílo	k1gNnPc1
jako	jako	k9
Městské	městský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
(	(	kIx(
<g/>
J.	J.	kA
Kotěra	Kotěra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
schodiště	schodiště	k1gNnSc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
J.	J.	kA
Gočár	Gočár	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Labská	labský	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
(	(	kIx(
<g/>
F.	F.	kA
Sander	Sandra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Evangelický	evangelický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c6
Nezvalově	Nezvalův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
(	(	kIx(
<g/>
O.	O.	kA
Liska	Liska	k1gMnSc1
<g/>
)	)	kIx)
atd.	atd.	kA
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
historický	historický	k2eAgInSc4d1
střed	střed	k1gInSc4
města	město	k1gNnSc2
s	s	k7c7
Velkým	velký	k2eAgNnSc7d1
náměstím	náměstí	k1gNnSc7
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Rozvíjející	rozvíjející	k2eAgFnSc1d1
se	se	k3xPyFc4
poválečná	poválečný	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
si	se	k3xPyFc3
vyžádala	vyžádat	k5eAaPmAgFnS
nové	nový	k2eAgInPc4d1
tvůrčí	tvůrčí	k2eAgInPc4d1
impulsy	impuls	k1gInPc4
v	v	k7c6
územním	územní	k2eAgNnSc6d1
plánování	plánování	k1gNnSc6
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
rychlým	rychlý	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
urbanismu	urbanismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regulační	regulační	k2eAgInSc1d1
plán	plán	k1gInSc1
města	město	k1gNnSc2
arch	archa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josefa	Josef	k1gMnSc2
Gočára	Gočár	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1926	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
radiálně	radiálně	k6eAd1
okružním	okružní	k2eAgInSc7d1
principem	princip	k1gInSc7
výstavby	výstavba	k1gFnSc2
dodnes	dodnes	k6eAd1
inspirující	inspirující	k2eAgFnSc1d1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
dále	daleko	k6eAd2
naplňoval	naplňovat	k5eAaImAgMnS
svými	svůj	k3xOyFgNnPc7
díly	díl	k1gInPc4
–	–	k?
úprava	úprava	k1gFnSc1
Masarykova	Masarykův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
,	,	kIx,
školní	školní	k2eAgInSc4d1
areál	areál	k1gInSc4
na	na	k7c6
Tylově	Tylův	k2eAgNnSc6d1
nábřeží	nábřeží	k1gNnSc6
<g/>
,	,	kIx,
Sbor	sbor	k1gInSc4
kněze	kněz	k1gMnSc2
Ambrože	Ambrož	k1gMnSc2
<g/>
,	,	kIx,
úprava	úprava	k1gFnSc1
Ulrichova	Ulrichův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
,	,	kIx,
Okresní	okresní	k2eAgInPc1d1
a	a	k8xC
finanční	finanční	k2eAgInPc1d1
úřady	úřad	k1gInPc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvíjející	rozvíjející	k2eAgMnSc1d1
se	se	k3xPyFc4
krajské	krajský	k2eAgNnSc1d1
město	město	k1gNnSc1
navštívil	navštívit	k5eAaPmAgInS
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1929	#num#	k4
prezident	prezident	k1gMnSc1
republiky	republika	k1gFnSc2
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
etapa	etapa	k1gFnSc1
rozvoje	rozvoj	k1gInSc2
města	město	k1gNnSc2
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
nazývána	nazývat	k5eAaImNgFnS
„	„	k?
<g/>
Gočárův	Gočárův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
úspěších	úspěch	k1gInPc6
meziválečné	meziválečný	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
také	také	k9
další	další	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
–	–	k?
Oldřich	Oldřich	k1gMnSc1
Liska	Liska	k1gMnSc1
(	(	kIx(
<g/>
např.	např.	kA
Městské	městský	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Fňouk	Fňouk	k1gMnSc1
(	(	kIx(
<g/>
např.	např.	kA
Novákovy	Novákův	k2eAgFnPc4d1
garáže	garáž	k1gFnPc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Palace	Palace	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
Novotný	Novotný	k1gMnSc1
(	(	kIx(
<g/>
Palác	palác	k1gInSc1
Steinský-Sehnoutka	Steinský-Sehnoutka	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
ČSOB	ČSOB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Rejchl	Rejchl	k1gMnSc1
(	(	kIx(
<g/>
např.	např.	kA
Sborové	sborový	k2eAgNnSc1d1
velitelství	velitelství	k1gNnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UK	UK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Rejchl	Rejchl	k1gFnSc2
ml.	ml.	kA
(	(	kIx(
<g/>
např.	např.	kA
výpravní	výpravní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
hlavního	hlavní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
nebo	nebo	k8xC
krajský	krajský	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
Sláma	Sláma	k1gMnSc1
(	(	kIx(
<g/>
monumentální	monumentální	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Božského	božský	k2eAgNnSc2d1
srdce	srdce	k1gNnSc2
Páně	páně	k2eAgNnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
výstavbě	výstavba	k1gFnSc6
města	město	k1gNnSc2
kromě	kromě	k7c2
významných	významný	k2eAgMnPc2d1
architektů	architekt	k1gMnPc2
<g/>
,	,	kIx,
stavitelů	stavitel	k1gMnPc2
a	a	k8xC
osvícených	osvícený	k2eAgMnPc2d1
starostů	starosta	k1gMnPc2
měla	mít	k5eAaImAgFnS
rovněž	rovněž	k9
městská	městský	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
stavební	stavební	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
bezprostředně	bezprostředně	k6eAd1
řídila	řídit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
Bílé	bílý	k2eAgFnSc2d1
věže	věž	k1gFnSc2
na	na	k7c4
Pražské	pražský	k2eAgNnSc4d1
předměstí	předměstí	k1gNnSc4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Průmyslový	průmyslový	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
města	město	k1gNnSc2
pokračoval	pokračovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stávající	stávající	k2eAgInPc1d1
závody	závod	k1gInPc1
se	se	k3xPyFc4
rozšiřovaly	rozšiřovat	k5eAaImAgInP
a	a	k8xC
vznikaly	vznikat	k5eAaImAgInP
nové	nový	k2eAgInPc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
to	ten	k3xDgNnSc1
vyplývalo	vyplývat	k5eAaImAgNnS
ze	z	k7c2
značného	značný	k2eAgInSc2d1
finančního	finanční	k2eAgInSc2d1
obratu	obrat	k1gInSc2
zdejších	zdejší	k2eAgInPc2d1
peněžních	peněžní	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibývaly	přibývat	k5eAaImAgFnP
nové	nový	k2eAgFnPc1d1
školy	škola	k1gFnPc1
<g/>
,	,	kIx,
ústavy	ústav	k1gInPc1
a	a	k8xC
úřady	úřad	k1gInPc1
<g/>
,	,	kIx,
komunikace	komunikace	k1gFnPc1
<g/>
,	,	kIx,
nové	nový	k2eAgFnPc1d1
čtvrti	čtvrt	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
přiváděly	přivádět	k5eAaImAgFnP
do	do	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
pravidelně	pravidelně	k6eAd1
význačné	význačný	k2eAgMnPc4d1
hosty	host	k1gMnPc4
<g/>
,	,	kIx,
takže	takže	k8xS
dosud	dosud	k6eAd1
poměrně	poměrně	k6eAd1
malé	malý	k2eAgNnSc1d1
město	město	k1gNnSc1
získávalo	získávat	k5eAaImAgNnS
věhlas	věhlas	k1gInSc4
a	a	k8xC
oprávněnou	oprávněný	k2eAgFnSc4d1
pověst	pověst	k1gFnSc4
„	„	k?
<g/>
salonu	salon	k1gInSc2
republiky	republika	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
města	město	k1gNnSc2
byl	být	k5eAaImAgInS
násilně	násilně	k6eAd1
přerušen	přerušit	k5eAaPmNgInS
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
jejím	její	k3xOp3gInSc6
průběhu	průběh	k1gInSc6
však	však	k9
vznikl	vzniknout	k5eAaPmAgInS
Velký	velký	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
německou	německý	k2eAgFnSc7d1
správou	správa	k1gFnSc7
spojený	spojený	k2eAgInSc1d1
z	z	k7c2
obcí	obec	k1gFnPc2
vzniklých	vzniklý	k2eAgFnPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejím	její	k3xOp3gNnSc6
skončení	skončení	k1gNnSc6
zůstal	zůstat	k5eAaPmAgInS
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
stále	stále	k6eAd1
hospodářským	hospodářský	k2eAgNnSc7d1
a	a	k8xC
kulturním	kulturní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
východních	východní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
naprosto	naprosto	k6eAd1
odlišných	odlišný	k2eAgFnPc6d1
politických	politický	k2eAgFnPc6d1
a	a	k8xC
společenských	společenský	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poválečné	poválečný	k2eAgInPc1d1
období	období	k1gNnSc4
socialismu	socialismus	k1gInSc2
ušlechtilé	ušlechtilý	k2eAgNnSc4d1
městské	městský	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
nepoznamenalo	poznamenat	k5eNaPmAgNnS
tak	tak	k6eAd1
negativně	negativně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
především	především	k9
na	na	k7c4
místní	místní	k2eAgMnPc4d1
architekty	architekt	k1gMnPc4
působil	působit	k5eAaImAgMnS
genius	genius	k1gMnSc1
loci	loc	k1gFnSc2
s	s	k7c7
předválečnou	předválečný	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
a	a	k8xC
urbanismem	urbanismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
však	však	k9
výstavba	výstavba	k1gFnSc1
Velkého	velký	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
začala	začít	k5eAaPmAgFnS
v	v	k7c6
nových	nový	k2eAgFnPc6d1
politických	politický	k2eAgFnPc6d1
a	a	k8xC
společenských	společenský	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
upadat	upadat	k5eAaImF,k5eAaPmF
do	do	k7c2
průměru	průměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Urbanismus	urbanismus	k1gInSc4
a	a	k8xC
architektura	architektura	k1gFnSc1
sídliště	sídliště	k1gNnSc2
Slezské	Slezská	k1gFnSc2
předměstí	předměstí	k1gNnSc2
však	však	k9
náleží	náležet	k5eAaImIp3nS
k	k	k7c3
nejlepším	dobrý	k2eAgFnPc3d3
dobovým	dobový	k2eAgFnPc3d1
realizacím	realizace	k1gFnPc3
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
monotónní	monotónní	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
bytová	bytový	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
Moravského	moravský	k2eAgNnSc2d1
předměstí	předměstí	k1gNnSc2
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
měřítkem	měřítko	k1gNnSc7
a	a	k8xC
panelovou	panelový	k2eAgFnSc7d1
technologií	technologie	k1gFnSc7
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
vyčleňovat	vyčleňovat	k5eAaImF
z	z	k7c2
tradičního	tradiční	k2eAgInSc2d1
obrazu	obraz	k1gInSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
pak	pak	k6eAd1
přinesla	přinést	k5eAaPmAgFnS
možnost	možnost	k1gFnSc4
obnovy	obnova	k1gFnSc2
tradičních	tradiční	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
demokratické	demokratický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
individualismus	individualismus	k1gInSc4
v	v	k7c6
projektování	projektování	k1gNnSc6
a	a	k8xC
inspiraci	inspirace	k1gFnSc3
světovou	světový	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3
doložený	doložený	k2eAgInSc1d1
název	název	k1gInSc1
původně	původně	k6eAd1
(	(	kIx(
<g/>
od	od	k7c2
konce	konec	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
hradiště	hradiště	k1gNnSc2
a	a	k8xC
sídla	sídlo	k1gNnPc4
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
,	,	kIx,
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sídelního	sídelní	k2eAgInSc2d1
hradu	hrad	k1gInSc2
Přemyslovců	Přemyslovec	k1gMnPc2
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1073	#num#	k4
(	(	kIx(
<g/>
castrum	castrum	k1gNnSc1
Gradec	Gradec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
během	běh	k1gInSc7
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vzniklo	vzniknout	k5eAaPmAgNnS
německé	německý	k2eAgInPc4d1
Grätz	Grätz	k1gInSc4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
1259	#num#	k4
doloženo	doložit	k5eAaPmNgNnS
jako	jako	k9
Gretz	Gretza	k1gFnPc2
<g/>
,	,	kIx,
1352	#num#	k4
Grecz	Grecza	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
díky	díky	k7c3
hláskové	hláskový	k2eAgFnSc3d1
změně	změna	k1gFnSc3
g	g	kA
v	v	k7c6
h	h	k?
v	v	k7c4
první	první	k4xOgInPc4
<g />
.	.	kIx.
</s>
<s hack="1">
polovině	polovina	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
vyvinulo	vyvinout	k5eAaPmAgNnS
v	v	k7c4
Hradec	Hradec	k1gInSc4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
staročeské	staročeský	k2eAgNnSc4d1
hradec	hradec	k1gInSc1
znamenalo	znamenat	k5eAaImAgNnS
„	„	k?
<g/>
menší	malý	k2eAgInSc1d2
nebo	nebo	k8xC
vedlejší	vedlejší	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
již	již	k9
hrazené	hrazený	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
město	město	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
roku	rok	k1gInSc2
1373	#num#	k4
věnným	věnný	k2eAgNnSc7d1
městem	město	k1gNnSc7
českých	český	k2eAgFnPc2d1
královen	královna	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ke	k	k7c3
jménu	jméno	k1gNnSc3
připojován	připojován	k2eAgInSc4d1
neshodný	shodný	k2eNgInSc4d1
přívlastek	přívlastek	k1gInSc4
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
„	„	k?
<g/>
manželka	manželka	k1gFnSc1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
královna	královna	k1gFnSc1
vdova	vdova	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
němčině	němčina	k1gFnSc6
se	se	k3xPyFc4
původní	původní	k2eAgInSc1d1
der	drát	k5eAaImRp2nS
Stat	Stat	k1gMnSc1
Khunigin	Khunigin	k1gMnSc1
Gract	Gract	k1gMnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1557	#num#	k4
<g/>
)	)	kIx)
nakonec	nakonec	k6eAd1
zkrátilo	zkrátit	k5eAaPmAgNnS
na	na	k7c4
Königgrätz	Königgrätz	k1gInSc4
(	(	kIx(
<g/>
Khuniggräcz	Khuniggräcz	k1gMnSc1
<g/>
,	,	kIx,
1568	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
mylně	mylně	k6eAd1
chápáno	chápat	k5eAaImNgNnS
jako	jako	k9
„	„	k?
<g/>
králův	králův	k2eAgInSc4d1
hradec	hradec	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
německé	německý	k2eAgInPc1d1
König	König	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
král	král	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
posessivní	posessivní	k2eAgInSc4d1
genitiv	genitiv	k1gInSc4
královny	královna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
zní	znět	k5eAaImIp3nS
Hradecz	Hradecz	k1gInSc1
regine	reginout	k5eAaPmIp3nS
(	(	kIx(
<g/>
1373	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
Hradecz	Hradecz	k1gInSc4
Reginae	Regina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romsky	romsky	k6eAd1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
nazýváno	nazývat	k5eAaImNgNnS
Hradecis	Hradecis	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pověsti	pověst	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Královéhradecké	královéhradecký	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Soutok	soutok	k1gInSc1
Labe	Labe	k1gNnSc2
s	s	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
V	v	k7c6
Hradci	Hradec	k1gInSc6
se	se	k3xPyFc4
chystala	chystat	k5eAaImAgFnS
exekuce	exekuce	k1gFnSc1
rakovnického	rakovnický	k2eAgMnSc2d1
zločince	zločinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zločinec	zločinec	k1gMnSc1
před	před	k7c7
popravou	poprava	k1gFnSc7
uviděl	uvidět	k5eAaPmAgMnS
v	v	k7c6
davu	dav	k1gInSc6
lidí	člověk	k1gMnPc2
svého	svůj	k1gMnSc4
známého	známý	k1gMnSc4
<g/>
,	,	kIx,
žebráka	žebrák	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
řekl	říct	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Rakovníka	Rakovník	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pod	pod	k7c7
stromem	strom	k1gInSc7
nalezne	naleznout	k5eAaPmIp3nS,k5eAaBmIp3nS
poklad	poklad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
části	část	k1gFnSc2
peněz	peníze	k1gInPc2
nechť	nechť	k9
koupí	koupit	k5eAaPmIp3nS
Hradci	Hradec	k1gInSc6
zvon	zvon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
také	také	k9
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
se	se	k3xPyFc4
vypravuje	vypravovat	k5eAaImIp3nS
také	také	k9
o	o	k7c6
zvonu	zvon	k1gInSc6
v	v	k7c6
Rakovníku	Rakovník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
soutoku	soutok	k1gInSc6
řek	řeka	k1gFnPc2
Labe	Labe	k1gNnSc1
a	a	k8xC
Orlice	Orlice	k1gFnSc1
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
východních	východní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
mezi	mezi	k7c7
15	#num#	k4
<g/>
°	°	k?
východní	východní	k2eAgFnSc2d1
zeměpisné	zeměpisný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
a	a	k8xC
50	#num#	k4
<g/>
°	°	k?
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
umístění	umístění	k1gNnSc3
v	v	k7c6
Polabské	polabský	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
patří	patřit	k5eAaImIp3nS
Hradec	Hradec	k1gInSc1
do	do	k7c2
teplé	teplý	k2eAgFnSc2d1
klimatické	klimatický	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
nepřesahující	přesahující	k2eNgFnSc4d1
240	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
okolí	okolí	k1gNnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
i	i	k9
díky	díky	k7c3
kvalitní	kvalitní	k2eAgFnSc3d1
půdě	půda	k1gFnSc3
<g/>
,	,	kIx,
zemědělsky	zemědělsky	k6eAd1
hojně	hojně	k6eAd1
využívané	využívaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reliéf	reliéf	k1gInSc1
je	být	k5eAaImIp3nS
rovinatý	rovinatý	k2eAgInSc1d1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
zde	zde	k6eAd1
žádný	žádný	k3yNgInSc1
výrazný	výrazný	k2eAgInSc1d1
kopec	kopec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
katastru	katastr	k1gInSc6
města	město	k1gNnSc2
se	se	k3xPyFc4
rovněž	rovněž	k9
nacházejí	nacházet	k5eAaImIp3nP
rozlehlé	rozlehlý	k2eAgInPc1d1
Hradecké	Hradecké	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Na	na	k7c6
Plachtě	plachta	k1gFnSc6
i	i	k9
několik	několik	k4yIc4
rybníků	rybník	k1gInPc2
a	a	k8xC
vodních	vodní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
rybníky	rybník	k1gInPc1
Biřička	biřička	k1gFnSc1
<g/>
,	,	kIx,
Cikán	cikán	k1gMnSc1
<g/>
,	,	kIx,
Datlík	datlík	k1gMnSc1
<g/>
,	,	kIx,
Roudnička	Roudnička	k1gFnSc1
<g/>
,	,	kIx,
Stříbrný	stříbrný	k2eAgInSc1d1
rybník	rybník	k1gInSc1
<g/>
,	,	kIx,
Jáma	jáma	k1gFnSc1
či	či	k8xC
ramena	rameno	k1gNnPc4
Starého	Starého	k2eAgNnSc2d1
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léta	léto	k1gNnPc1
bývají	bývat	k5eAaImIp3nP
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
horká	horký	k2eAgNnPc1d1
a	a	k8xC
vlhká	vlhký	k2eAgNnPc1d1
<g/>
,	,	kIx,
zimy	zima	k1gFnPc1
mírné	mírný	k2eAgFnPc1d1
a	a	k8xC
suché	suchý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
10	#num#	k4
°	°	kIx~
<g/>
C	C	kA
s	s	k7c7
ročním	roční	k2eAgInSc7d1
průměrem	průměr	k1gInSc7
srážek	srážka	k1gFnPc2
cca	cca	kA
600	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
podnebi	podnebi	k1gNnSc1
th	th	k?
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
90	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
podnebi	podnebi	k1gNnSc1
td	td	k?
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
podnebí	podnebí	k1gNnSc1
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
leden	leden	k1gInSc1
</s>
<s>
únor	únor	k1gInSc1
</s>
<s>
březen	březen	k1gInSc1
</s>
<s>
duben	duben	k1gInSc1
</s>
<s>
květen	květen	k1gInSc4
</s>
<s>
červen	červen	k1gInSc1
</s>
<s>
červenec	červenec	k1gInSc1
</s>
<s>
srpen	srpen	k1gInSc1
</s>
<s>
září	zářit	k5eAaImIp3nS
</s>
<s>
říjen	říjen	k1gInSc1
</s>
<s>
listopad	listopad	k1gInSc1
</s>
<s>
prosinec	prosinec	k1gInSc1
</s>
<s>
rok	rok	k1gInSc4
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1
denní	denní	k2eAgNnSc1d1
maximum	maximum	k1gNnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
0,2	0,2	k4
</s>
<s>
3,1	3,1	k4
</s>
<s>
8,5	8,5	k4
</s>
<s>
14,4	14,4	k4
</s>
<s>
19,5	19,5	k4
</s>
<s>
22,6	22,6	k4
</s>
<s>
24,5	24,5	k4
</s>
<s>
24,2	24,2	k4
</s>
<s>
20,2	20,2	k4
</s>
<s>
14,1	14,1	k4
</s>
<s>
6,6	6,6	k4
</s>
<s>
1,9	1,9	k4
</s>
<s>
13,3	13,3	k4
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1
denní	denní	k2eAgNnSc1d1
minimum	minimum	k1gNnSc1
[	[	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
]	]	kIx)
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
−	−	k?
</s>
<s>
3,9	3,9	k4
</s>
<s>
8,3	8,3	k4
</s>
<s>
11,4	11,4	k4
</s>
<s>
12,7	12,7	k4
</s>
<s>
12,6	12,6	k4
</s>
<s>
9,5	9,5	k4
</s>
<s>
5,0	5,0	k4
</s>
<s>
0,9	0,9	k4
</s>
<s>
−	−	k?
</s>
<s>
4,4	4,4	k4
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
[	[	kIx(
<g/>
mm	mm	kA
<g/>
]	]	kIx)
</s>
<s>
24,6	24,6	k4
</s>
<s>
23,8	23,8	k4
</s>
<s>
24,4	24,4	k4
</s>
<s>
31,5	31,5	k4
</s>
<s>
61,0	61,0	k4
</s>
<s>
72,2	72,2	k4
</s>
<s>
63,7	63,7	k4
</s>
<s>
56,3	56,3	k4
</s>
<s>
37,6	37,6	k4
</s>
<s>
30,7	30,7	k4
</s>
<s>
37,4	37,4	k4
</s>
<s>
27,1	27,1	k4
</s>
<s>
490,3	490,3	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
Světová	světový	k2eAgFnSc1d1
meteorologická	meteorologický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
UN	UN	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Listopad	listopad	k1gInSc1
2011	#num#	k4
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc4
k	k	k7c3
vybraným	vybraný	k2eAgNnPc3d1
městům	město	k1gNnPc3
<g/>
:	:	kIx,
</s>
<s>
Jičín	Jičín	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
43	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
83	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
~	~	kIx~
<g/>
137	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Trutnov	Trutnov	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
39	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jelenia	Jelenium	k1gNnPc1
Góra	Gór	k2eAgNnPc1d1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
78	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Legnica	Legnica	k1gMnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
112	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Náchod	Náchod	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
33	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Wałbrzych	Wałbrzych	k1gMnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
69	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
131	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
105	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
210	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cheb	Cheb	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
247	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Aš	Aš	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
259	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Jeseník	Jeseník	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
97	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Opole	Opole	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
157	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
180	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
169	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
183	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Domažlice	Domažlice	k1gFnPc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
226	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
19	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
91	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
127	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Břeclav	Břeclav	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
172	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
127	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
~	~	kIx~
<g/>
171	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
patřil	patřit	k5eAaImAgInS
k	k	k7c3
českým	český	k2eAgFnPc3d1
„	„	k?
<g/>
stotisícovým	stotisícový	k2eAgFnPc3d1
městům	město	k1gNnPc3
<g/>
“	“	k?
<g/>
:	:	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jím	jíst	k5eAaImIp1nS
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
a	a	k8xC
svého	svůj	k3xOyFgNnSc2
maxima	maximum	k1gNnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
101	#num#	k4
272	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Pak	pak	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
zejména	zejména	k9
kvůli	kvůli	k7c3
stěhování	stěhování	k1gNnSc3
za	za	k7c4
město	město	k1gNnSc4
(	(	kIx(
<g/>
suburbanizace	suburbanizace	k1gFnPc1
<g/>
)	)	kIx)
začal	začít	k5eAaPmAgInS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
snižovat	snižovat	k5eAaImF
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1997	#num#	k4
klesl	klesnout	k5eAaPmAgInS
pod	pod	k7c4
statisícovou	statisícový	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
a	a	k8xC
např.	např.	kA
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
měl	mít	k5eAaImAgInS
už	už	k6eAd1
jen	jen	k9
přes	přes	k7c4
94	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
hradecko-pardubická	hradecko-pardubický	k2eAgFnSc1d1
aglomerace	aglomerace	k1gFnSc1
ale	ale	k9
měla	mít	k5eAaImAgFnS
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2013	#num#	k4
celkem	celkem	k6eAd1
335	#num#	k4
118	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
vlastního	vlastní	k2eAgNnSc2d1
města	město	k1gNnSc2
dojíždí	dojíždět	k5eAaImIp3nS
přes	přes	k7c4
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
za	za	k7c2
prací	práce	k1gFnPc2
i	i	k9
za	za	k7c7
studiem	studio	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnPc2
1921	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
v	v	k7c6
547	#num#	k4
domech	dům	k1gInPc6
13	#num#	k4
115	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
bylo	být	k5eAaImAgNnS
6	#num#	k4
159	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
12	#num#	k4
472	#num#	k4
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
československé	československý	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
,	,	kIx,
342	#num#	k4
k	k	k7c3
německé	německý	k2eAgFnSc3d1
a	a	k8xC
7	#num#	k4
k	k	k7c3
židovské	židovská	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žilo	žít	k5eAaImAgNnS
zde	zde	k6eAd1
9	#num#	k4
149	#num#	k4
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
686	#num#	k4
evangelíků	evangelík	k1gMnPc2
<g/>
,	,	kIx,
527	#num#	k4
příslušníků	příslušník	k1gMnPc2
Církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
a	a	k8xC
314	#num#	k4
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
sčítání	sčítání	k1gNnPc2
1930	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
v	v	k7c6
861	#num#	k4
domech	dům	k1gInPc6
17	#num#	k4
819	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
16	#num#	k4
854	#num#	k4
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
československé	československý	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
a	a	k8xC
359	#num#	k4
k	k	k7c3
německé	německý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žilo	žít	k5eAaImAgNnS
zde	zde	k6eAd1
10	#num#	k4
991	#num#	k4
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
1	#num#	k4
219	#num#	k4
evangelíků	evangelík	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
082	#num#	k4
příslušníků	příslušník	k1gMnPc2
Církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
a	a	k8xC
425	#num#	k4
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ale	ale	k9
tvořilo	tvořit	k5eAaImAgNnS
jen	jen	k9
dnešní	dnešní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
také	také	k6eAd1
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
Věkoše	Věkoš	k1gMnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
spolu	spolu	k6eAd1
s	s	k7c7
tehdy	tehdy	k6eAd1
samostatnými	samostatný	k2eAgNnPc7d1
předměstími	předměstí	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
městem	město	k1gNnSc7
úzce	úzko	k6eAd1
provázána	provázán	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pražské	pražský	k2eAgNnSc1d1
předměstí	předměstí	k1gNnSc1
<g/>
,	,	kIx,
Slezské	slezský	k2eAgNnSc1d1
předměstí	předměstí	k1gNnSc1
<g/>
,	,	kIx,
Kukleny	kuklen	k2eAgFnPc1d1
<g/>
,	,	kIx,
Malšovice	Malšovice	k1gFnSc1
a	a	k8xC
Pouchov	Pouchov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žilo	žít	k5eAaImAgNnS
k	k	k7c3
roku	rok	k1gInSc3
1930	#num#	k4
v	v	k7c4
takto	takto	k6eAd1
vymezené	vymezený	k2eAgFnSc6d1
aglomeraci	aglomerace	k1gFnSc6
celkem	celkem	k6eAd1
34	#num#	k4
517	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
po	po	k7c6
započtení	započtení	k1gNnSc6
Plotiště	Plotiště	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
a	a	k8xC
Svobodných	svobodný	k2eAgMnPc2d1
Dvorů	Dvůr	k1gInPc2
už	už	k9
40	#num#	k4
928	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
5	#num#	k4
4938	#num#	k4
1667	#num#	k4
8169	#num#	k4
76711	#num#	k4
06513	#num#	k4
11517	#num#	k4
81944	#num#	k4
80955	#num#	k4
13677	#num#	k4
54195	#num#	k4
58899	#num#	k4
91797	#num#	k4
15594	#num#	k4
314	#num#	k4
</s>
<s>
Struktura	struktura	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
</s>
<s>
Věková	věkový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
roku	rok	k1gInSc2
2011	#num#	k4
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1
stav	stav	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
roku	rok	k1gInSc2
2011	#num#	k4
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
roku	rok	k1gInSc2
2011	#num#	k4
</s>
<s>
Královéhradecká	královéhradecký	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
</s>
<s>
95	#num#	k4
%	%	kIx~
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
</s>
<s>
0,19	0,19	k4
%	%	kIx~
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
</s>
<s>
0	#num#	k4
%	%	kIx~
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
</s>
<s>
1,42	1,42	k4
%	%	kIx~
</s>
<s>
Německá	německý	k2eAgFnSc1d1
</s>
<s>
0,12	0,12	k4
%	%	kIx~
</s>
<s>
Polská	polský	k2eAgFnSc1d1
</s>
<s>
0,18	0,18	k4
%	%	kIx~
</s>
<s>
Romská	romský	k2eAgFnSc1d1
</s>
<s>
0,07	0,07	k4
%	%	kIx~
</s>
<s>
Nezjištěna	zjištěn	k2eNgNnPc1d1
a	a	k8xC
další	další	k2eAgNnPc1d1
</s>
<s>
3,0	3,0	k4
%	%	kIx~
</s>
<s>
Náboženské	náboženský	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
</s>
<s>
Bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
</s>
<s>
66,89	66,89	k4
%	%	kIx~
</s>
<s>
Věřící	věřící	k1gMnPc1
celkem	celkem	k6eAd1
</s>
<s>
21,21	21,21	k4
%	%	kIx~
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
15,86	15,86	k4
%	%	kIx~
</s>
<s>
Českobratrská	českobratrský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
evangelická	evangelický	k2eAgFnSc1d1
</s>
<s>
1,33	1,33	k4
%	%	kIx~
</s>
<s>
Církev	církev	k1gFnSc1
čsl	čsl	kA
<g/>
.	.	kIx.
Husitská	husitský	k2eAgFnSc1d1
</s>
<s>
1,29	1,29	k4
%	%	kIx~
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
2,73	2,73	k4
%	%	kIx~
</s>
<s>
Nezjištěno	zjištěn	k2eNgNnSc1d1
</s>
<s>
11,90	11,90	k4
%	%	kIx~
</s>
<s>
Sestaveno	sestavit	k5eAaPmNgNnS
dle	dle	k7c2
statistik	statistika	k1gFnPc2
Sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měl	mít	k5eAaImAgInS
Hradec	Hradec	k1gInSc1
97	#num#	k4
155	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
a	a	k8xC
členění	členění	k1gNnSc1
města	město	k1gNnSc2
</s>
<s>
Územní	územní	k2eAgInSc1d1
plán	plán	k1gInSc1
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc4
částí	část	k1gFnSc7
města	město	k1gNnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vzniku	vznik	k1gInSc6
Velkého	velký	k2eAgInSc2d1
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
v	v	k7c6
protektorátním	protektorátní	k2eAgNnSc6d1
období	období	k1gNnSc6
(	(	kIx(
<g/>
katastrální	katastrální	k2eAgFnSc1d1
území	území	k1gNnSc4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Pražské	pražský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
<g/>
,	,	kIx,
Kukleny	kuklen	k2eAgInPc1d1
<g/>
,	,	kIx,
Svobodné	svobodný	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
<g/>
,	,	kIx,
Plotiště	Plotiště	k1gNnPc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Plácky	plácek	k1gInPc7
<g/>
,	,	kIx,
Věkoše	Věkoš	k1gMnPc4
<g/>
,	,	kIx,
Pouchov	Pouchov	k1gInSc1
<g/>
,	,	kIx,
Slezské	slezský	k2eAgNnSc1d1
předměstí	předměstí	k1gNnSc1
<g/>
,	,	kIx,
Malšovice	Malšovice	k1gFnSc1
<g/>
,	,	kIx,
Třebeš	Třebeš	k1gFnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
a	a	k8xC
Kluky	kluk	k1gMnPc7
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
obce	obec	k1gFnPc1
později	pozdě	k6eAd2
opět	opět	k6eAd1
osamostatnily	osamostatnit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
se	se	k3xPyFc4
ale	ale	k8xC
k	k	k7c3
Hradci	Hradec	k1gInSc3
Králové	Králová	k1gFnSc2
znovu	znovu	k6eAd1
připojily	připojit	k5eAaPmAgFnP
Plačice	Plačice	k1gFnPc1
<g/>
,	,	kIx,
Plácky	plácek	k1gInPc1
<g/>
,	,	kIx,
Plotiště	Plotiště	k1gNnPc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Pouchov	Pouchov	k1gInSc1
<g/>
,	,	kIx,
Svobodné	svobodný	k2eAgInPc4d1
Dvory	Dvůr	k1gInPc4
a	a	k8xC
Věkoše	Věkoš	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
Březhrad	Březhrada	k1gFnPc2
<g/>
,	,	kIx,
Malšova	Malšův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
Roudnička	Roudnička	k1gFnSc1
<g/>
,	,	kIx,
Rusek	Rusko	k1gNnPc2
včetně	včetně	k7c2
Piletic	Piletice	k1gFnPc2
a	a	k8xC
Slatina	slatina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
poslední	poslední	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
obce	obec	k1gFnSc2
k	k	k7c3
městu	město	k1gNnSc3
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
připojením	připojení	k1gNnSc7
Svinar	Svinara	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
21	#num#	k4
místních	místní	k2eAgFnPc2d1
částí	část	k1gFnPc2
a	a	k8xC
21	#num#	k4
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
však	však	k9
nejsou	být	k5eNaImIp3nP
zcela	zcela	k6eAd1
totožná	totožný	k2eAgNnPc1d1
<g/>
:	:	kIx,
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
Kluky	kluk	k1gMnPc4
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
a	a	k8xC
místní	místní	k2eAgFnSc4d1
část	část	k1gFnSc4
Moravské	moravský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Třebeš	Třebeš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
patří	patřit	k5eAaImIp3nS
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
Olomoucí	Olomouc	k1gFnSc7
<g/>
,	,	kIx,
Českými	český	k2eAgInPc7d1
Budějovicemi	Budějovice	k1gInPc7
a	a	k8xC
zčásti	zčásti	k6eAd1
také	také	k9
Libercem	Liberec	k1gInSc7
<g/>
)	)	kIx)
k	k	k7c3
největším	veliký	k2eAgNnPc3d3
statutárním	statutární	k2eAgNnPc3d1
městům	město	k1gNnPc3
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nejsou	být	k5eNaImIp3nP
rozčleněna	rozčlenit	k5eAaPmNgFnS
na	na	k7c4
samosprávné	samosprávný	k2eAgFnPc4d1
městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
ani	ani	k8xC
obvody	obvod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
iniciativní	iniciativní	k2eAgInPc1d1
a	a	k8xC
poradní	poradní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
zde	zde	k6eAd1
působí	působit	k5eAaImIp3nP
celkem	celkem	k6eAd1
25	#num#	k4
komisí	komise	k1gFnSc7
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
územní	územní	k2eAgFnSc1d1
působnost	působnost	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
vymezení	vymezení	k1gNnSc2
místních	místní	k2eAgFnPc2d1
částí	část	k1gFnPc2
odlišuje	odlišovat	k5eAaImIp3nS
(	(	kIx(
<g/>
nejvýraznější	výrazný	k2eAgFnPc1d3
odlišnosti	odlišnost	k1gFnPc1
viz	vidět	k5eAaImRp2nS
seznam	seznam	k1gInSc4
níže	níže	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
místní	místní	k2eAgFnPc1d1
části	část	k1gFnPc1
</s>
<s>
Komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
3,45	3,45	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Střed	středa	k1gFnPc2
města	město	k1gNnSc2
a	a	k8xC
Střed	střed	k1gInSc1
města	město	k1gNnSc2
–	–	k?
historické	historický	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
</s>
<s>
Březhrad	Březhrad	k1gInSc1
(	(	kIx(
<g/>
2,80	2,80	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Kluky	kluk	k1gMnPc7
(	(	kIx(
<g/>
1,31	1,31	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
spadající	spadající	k2eAgNnSc4d1
do	do	k7c2
části	část	k1gFnSc2
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Kukleny	kuklen	k2eAgInPc4d1
(	(	kIx(
<g/>
4,03	4,03	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalé	bývalý	k2eAgFnSc2d1
osady	osada	k1gFnSc2
Temešvár	Temešvár	k1gInSc1
I	i	k8xC
</s>
<s>
Malšova	Malšův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
(	(	kIx(
<g/>
1,94	1,94	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Malšovice	Malšovice	k1gFnSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Malšovice	Malšovice	k1gFnSc2
u	u	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2,37	2,37	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalých	bývalý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
Náhon	náhon	k1gInSc1
a	a	k8xC
Zámostí	Zámost	k1gFnSc7
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
<g/>
,	,	kIx,
místní	místní	k2eAgFnSc1d1
část	část	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Třebeš	Třebeš	k1gFnSc1
<g/>
;	;	kIx,
komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Moravské	moravský	k2eAgNnSc4d1
Předměstí	předměstí	k1gNnSc4
–	–	k?
jih	jih	k1gInSc1
(	(	kIx(
<g/>
komise	komise	k1gFnSc1
Moravské	moravský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
–	–	k?
sever	sever	k1gInSc4
a	a	k8xC
Moravské	moravský	k2eAgNnSc4d1
Předměstí	předměstí	k1gNnSc4
–	–	k?
východ	východ	k1gInSc1
působí	působit	k5eAaImIp3nS
v	v	k7c6
částech	část	k1gFnPc6
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
a	a	k8xC
Třebeš	Třebeš	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
24,74	24,74	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
místní	místní	k2eAgFnSc1d1
část	část	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
a	a	k8xC
Kluky	kluk	k1gMnPc7
</s>
<s>
Piletice	Piletika	k1gFnSc3
(	(	kIx(
<g/>
3,03	3,03	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Plácky	plácek	k1gInPc1
(	(	kIx(
<g/>
1,68	1,68	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Plačice	Plačice	k1gFnSc1
(	(	kIx(
<g/>
7,31	7,31	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Plotiště	Plotiště	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
6,44	6,44	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalé	bývalý	k2eAgFnSc2d1
osady	osada	k1gFnSc2
Kobylí	kobylí	k2eAgInPc4d1
Doly	dol	k1gInPc4
</s>
<s>
Pouchov	Pouchov	k1gInSc1
(	(	kIx(
<g/>
2,48	2,48	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Pouchov	Pouchov	k1gInSc1
–	–	k?
Věkoše	Věkoš	k1gMnSc2
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
(	(	kIx(
<g/>
4,94	4,94	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalých	bývalý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
Farářství	farářství	k1gNnSc2
<g/>
,	,	kIx,
Šosteny	Šosten	k1gInPc1
a	a	k8xC
Temešvár	Temešvár	k1gInSc1
II	II	kA
<g/>
;	;	kIx,
komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Pražské	pražský	k2eAgNnSc4d1
Předměstí	předměstí	k1gNnSc4
–	–	k?
jih	jih	k1gInSc1
a	a	k8xC
Pražské	pražský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
–	–	k?
sever	sever	k1gInSc4
</s>
<s>
Roudnička	Roudnička	k1gFnSc1
(	(	kIx(
<g/>
2,12	2,12	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Rusek	Ruska	k1gFnPc2
(	(	kIx(
<g/>
4,78	4,78	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Slatina	slatina	k1gFnSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Slatina	slatina	k1gFnSc1
u	u	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
4,11	4,11	k4
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Slezské	slezský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
(	(	kIx(
<g/>
7,62	7,62	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Slezské	Slezská	k1gFnSc2
Předměstí	předměstí	k1gNnSc2
–	–	k?
jih	jih	k1gInSc4
a	a	k8xC
Slezské	slezský	k2eAgNnSc4d1
Předměstí	předměstí	k1gNnSc4
–	–	k?
sever	sever	k1gInSc1
</s>
<s>
Svinary	Svinara	k1gFnPc1
(	(	kIx(
<g/>
3,42	3,42	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalé	bývalý	k2eAgFnSc2d1
osady	osada	k1gFnSc2
Svinárky	Svinárka	k1gFnSc2
</s>
<s>
Svobodné	svobodný	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
(	(	kIx(
<g/>
6,94	6,94	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalých	bývalý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
Bohdanečské	bohdanečský	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
<g/>
,	,	kIx,
Cihelna	cihelna	k1gFnSc1
a	a	k8xC
Klacov	Klacov	k1gInSc1
</s>
<s>
Třebeš	Třebat	k5eAaPmIp2nS
(	(	kIx(
<g/>
4,63	4,63	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalých	bývalý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
Kopec	kopec	k1gInSc1
Svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
a	a	k8xC
Lhota	Lhota	k1gFnSc1
pod	pod	k7c7
Strání	stráň	k1gFnSc7
<g/>
;	;	kIx,
katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
Třebeš	Třebeš	k1gFnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
kromě	kromě	k7c2
místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
Třebeš	Třebeš	k1gFnSc2
i	i	k9
místní	místní	k2eAgFnSc4d1
část	část	k1gFnSc4
Moravské	moravský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
</s>
<s>
Věkoše	Věkoš	k1gMnSc4
(	(	kIx(
<g/>
5,54	5,54	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
bývalé	bývalý	k2eAgFnSc2d1
osady	osada	k1gFnSc2
Správčice	správčice	k1gFnSc2
<g/>
;	;	kIx,
komise	komise	k1gFnSc1
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
Pouchov	Pouchov	k1gInSc1
–	–	k?
Věkoše	Věkoš	k1gMnSc2
</s>
<s>
Správní	správní	k2eAgNnSc1d1
území	území	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Okres	okres	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
a	a	k8xC
Obvod	obvod	k1gInSc1
obce	obec	k1gFnSc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
je	být	k5eAaImIp3nS
krajským	krajský	k2eAgNnSc7d1
městem	město	k1gNnSc7
a	a	k8xC
také	také	k9
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
a	a	k8xC
pověřeným	pověřený	k2eAgInSc7d1
obecním	obecní	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
4759	#num#	k4
km²	km²	k?
a	a	k8xC
žije	žít	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
zhruba	zhruba	k6eAd1
550	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okres	okres	k1gInSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
104	#num#	k4
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
ORP	ORP	kA
z	z	k7c2
81	#num#	k4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Instituce	instituce	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
představitelů	představitel	k1gMnPc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
je	být	k5eAaImIp3nS
řízeno	řídit	k5eAaImNgNnS
magistrátem	magistrát	k1gInSc7
<g/>
,	,	kIx,
primátorem	primátor	k1gMnSc7
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
už	už	k6eAd1
druhé	druhý	k4xOgNnSc4
volební	volební	k2eAgNnSc4d1
období	období	k1gNnSc4
Zdeněk	Zdeněk	k1gMnSc1
Fink	Fink	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
toto	tento	k3xDgNnSc4
místo	místo	k1gNnSc4
převzal	převzít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
od	od	k7c2
Otakara	Otakar	k1gMnSc2
Divíška	Divíšek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
radě	rada	k1gFnSc6
města	město	k1gNnSc2
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupen	k2eAgFnPc4d1
strany	strana	k1gFnPc4
Hradecký	hradecký	k2eAgInSc4d1
demokratický	demokratický	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
,	,	kIx,
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
Koalice	koalice	k1gFnSc1
pro	pro	k7c4
Hradec	Hradec	k1gInSc4
(	(	kIx(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnPc1
a	a	k8xC
nestraníci	nestraník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
města	město	k1gNnSc2
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
voleno	volen	k2eAgNnSc1d1
občany	občan	k1gMnPc4
města	město	k1gNnSc2
každé	každý	k3xTgInPc4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
oblasti	oblast	k1gFnSc2
justice	justice	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
působí	působit	k5eAaImIp3nS
jak	jak	k6eAd1
okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
krajský	krajský	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
s	s	k7c7
příslušnými	příslušný	k2eAgInPc7d1
státními	státní	k2eAgInPc7d1
zastupitelstvími	zastupitelství	k1gNnPc7
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
s	s	k7c7
vazební	vazební	k2eAgFnSc7d1
věznicí	věznice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
dále	daleko	k6eAd2
krajské	krajský	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
tří	tři	k4xCgNnPc2
obvodních	obvodní	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
<g/>
,	,	kIx,
i	i	k8xC
vlastní	vlastní	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
celý	celý	k2eAgInSc4d1
obvod	obvod	k1gInSc4
krajského	krajský	k2eAgInSc2d1
soudu	soud	k1gInSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
zřízena	zřízen	k2eAgFnSc1d1
notářská	notářský	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
je	být	k5eAaImIp3nS
sídlem	sídlo	k1gNnSc7
biskupů	biskup	k1gMnPc2
královéhradecké	královéhradecký	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
církve	církev	k1gFnSc2
katolické	katolický	k2eAgFnSc2d1
i	i	k8xC
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
odtud	odtud	k6eAd1
studio	studio	k1gNnSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
významných	významný	k2eAgFnPc2d1
celostátních	celostátní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
sídlí	sídlet	k5eAaImIp3nP
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
také	také	k6eAd1
např.	např.	kA
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
Lesy	les	k1gInPc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
nebo	nebo	k8xC
Povodí	povodí	k1gNnSc2
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
</s>
<s>
Průmysl	průmysl	k1gInSc1
</s>
<s>
S	s	k7c7
městem	město	k1gNnSc7
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
je	být	k5eAaImIp3nS
neodmyslitelně	odmyslitelně	k6eNd1
spjat	spjat	k2eAgInSc1d1
průmyslový	průmyslový	k2eAgInSc1d1
areál	areál	k1gInSc1
ZVU	zvát	k5eAaImIp1nS
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Kukleny	kuklen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
ze	z	k7c2
společností	společnost	k1gFnPc2
nacházejících	nacházející	k2eAgFnPc2d1
se	se	k3xPyFc4
v	v	k7c6
prostorách	prostora	k1gFnPc6
areálu	areál	k1gInSc2
je	být	k5eAaImIp3nS
i	i	k9
strojírna	strojírna	k1gFnSc1
ZVU	zvát	k5eAaImIp1nS
Strojírny	strojírna	k1gFnPc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInPc1
kořeny	kořen	k1gInPc1
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1869	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
„	„	k?
<g/>
První	první	k4xOgFnSc1
česká	český	k2eAgFnSc1d1
akciová	akciový	k2eAgFnSc1d1
strojírna	strojírna	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
ZVU	zvát	k5eAaImIp1nS
Strojírny	strojírna	k1gFnPc4
a.s.	a.s.	k?
zaměstnává	zaměstnávat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobní	výrobní	k2eAgInSc4d1
program	program	k1gInSc4
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
na	na	k7c4
dodávky	dodávka	k1gFnPc4
aparátů	aparát	k1gInPc2
pro	pro	k7c4
chemický	chemický	k2eAgInSc4d1
<g/>
,	,	kIx,
petrochemický	petrochemický	k2eAgInSc4d1
<g/>
,	,	kIx,
potravinářský	potravinářský	k2eAgInSc4d1
<g/>
,	,	kIx,
energetický	energetický	k2eAgInSc4d1
a	a	k8xC
farmaceutický	farmaceutický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
poslední	poslední	k2eAgInPc4d1
velké	velký	k2eAgInPc4d1
projekty	projekt	k1gInPc4
společnosti	společnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
výstavba	výstavba	k1gFnSc1
průmyslového	průmyslový	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
„	„	k?
<g/>
na	na	k7c4
klíč	klíč	k1gInSc4
<g/>
“	“	k?
o	o	k7c6
kapacitě	kapacita	k1gFnSc6
500	#num#	k4
000	#num#	k4
hl	hl	k?
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
v	v	k7c6
Gruzii	Gruzie	k1gFnSc6
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc1
Saguramo	Sagurama	k1gFnSc5
<g/>
,	,	kIx,
nebo	nebo	k8xC
pivovar	pivovar	k1gInSc1
za	za	k7c4
700	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
v	v	k7c6
Etiopii	Etiopie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tradiční	tradiční	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
pián	pián	k?
Petrof	Petrof	k1gMnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
ulici	ulice	k1gFnSc6
Na	na	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ulici	ulice	k1gFnSc6
Bratří	bratřit	k5eAaImIp3nS
Štefanů	Štefan	k1gMnPc2
sídlí	sídlet	k5eAaImIp3nS
firma	firma	k1gFnSc1
Glatt	Glatt	k1gInSc1
Pharma	Pharma	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
výrobu	výroba	k1gFnSc4
z	z	k7c2
nerezové	rezový	k2eNgFnSc2d1
oceli	ocel	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pro	pro	k7c4
farmaceutický	farmaceutický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Glatt	Glatt	k2eAgMnSc1d1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
významného	významný	k2eAgMnSc2d1
dodavatele	dodavatel	k1gMnSc2
strojů	stroj	k1gInPc2
a	a	k8xC
technologií	technologie	k1gFnPc2
pro	pro	k7c4
farmaceutický	farmaceutický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
letišti	letiště	k1gNnSc6
v	v	k7c6
Pouchově	pouchově	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
firma	firma	k1gFnSc1
TL-Ultralight	TL-Ultralight	k1gMnSc1
<g/>
,	,	kIx,
výrobce	výrobce	k1gMnSc1
a	a	k8xC
exportér	exportér	k1gMnSc1
ultra	ultra	k2eAgFnPc2d1
lehkých	lehký	k2eAgFnPc2d1
sportovních	sportovní	k2eAgFnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
turistických	turistický	k2eAgNnPc2d1
center	centrum	k1gNnPc2
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
nejčastějšími	častý	k2eAgMnPc7d3
návštěvníky	návštěvník	k1gMnPc7
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
Britové	Brit	k1gMnPc1
<g/>
,	,	kIx,
Američané	Američan	k1gMnPc1
a	a	k8xC
Poláci	Polák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
díky	díky	k7c3
velkému	velký	k2eAgNnSc3d1
množství	množství	k1gNnSc3
turistů	turist	k1gMnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
několik	několik	k4yIc4
hotelů	hotel	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
stojí	stát	k5eAaImIp3nS
za	za	k7c4
připomenutí	připomenutí	k1gNnSc4
Amber	ambra	k1gFnPc2
Hotel	hotel	k1gInSc1
Černigov	Černigovo	k1gNnPc2
naproti	naproti	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
nádraží	nádraží	k1gNnSc3
v	v	k7c6
centru	centrum	k1gNnSc6
dopravního	dopravní	k2eAgInSc2d1
terminálu	terminál	k1gInSc2
<g/>
,	,	kIx,
hotel	hotel	k1gInSc1
EA	EA	kA
Tereziánský	tereziánský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
historického	historický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
hotel	hotel	k1gInSc1
Alessandria	Alessandrium	k1gNnSc2
na	na	k7c6
Slezském	slezský	k2eAgNnSc6d1
Předměstí	předměstí	k1gNnSc1
<g/>
,	,	kIx,
hotel	hotel	k1gInSc1
Stadion	stadion	k1gInSc1
spojený	spojený	k2eAgInSc1d1
se	s	k7c7
zimním	zimní	k2eAgInSc7d1
stadionem	stadion	k1gInSc7
hokejového	hokejový	k2eAgInSc2d1
klubu	klub	k1gInSc2
Mountfield	Mountfielda	k1gFnPc2
HK	HK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
Boromeum	Boromeum	k1gInSc1
residence	residence	k1gFnSc2
v	v	k7c6
historické	historický	k2eAgFnSc6d1
části	část	k1gFnSc6
starého	starý	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
o	o	k7c4
několik	několik	k4yIc4
metrů	metr	k1gInPc2
vedle	vedle	k6eAd1
hotel	hotel	k1gInSc1
U	u	k7c2
královny	královna	k1gFnSc2
Elišky	Eliška	k1gFnSc2
nebo	nebo	k8xC
Nové	Nové	k2eAgNnSc1d1
Adalbertinum	Adalbertinum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
jako	jako	k9
bývalá	bývalý	k2eAgFnSc1d1
jezuitská	jezuitský	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
samo	sám	k3xTgNnSc1
tvoří	tvořit	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
dominant	dominanta	k1gFnPc2
Velkého	velký	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Samo	sám	k3xTgNnSc1
město	město	k1gNnSc1
kromě	kromě	k7c2
jedinečného	jedinečný	k2eAgNnSc2d1
panoramatu	panorama	k1gNnSc2
na	na	k7c6
starém	starý	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
několik	několik	k4yIc1
stavebních	stavební	k2eAgInPc2d1
slohů	sloh	k1gInPc2
<g/>
,	,	kIx,
sem	sem	k6eAd1
turisty	turist	k1gMnPc4
láká	lákat	k5eAaImIp3nS
i	i	k9
díky	díky	k7c3
botanické	botanický	k2eAgFnSc3d1
zahradě	zahrada	k1gFnSc3
léčivých	léčivý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
obřímu	obří	k2eAgInSc3d1
akvárium	akvárium	k1gNnSc1
či	či	k8xC
za	za	k7c7
zábavou	zábava	k1gFnSc7
prostředkované	prostředkovaný	k2eAgFnSc2d1
aquacentrem	aquacentr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
také	také	k9
navštívit	navštívit	k5eAaPmF
chráněné	chráněný	k2eAgNnSc4d1
území	území	k1gNnSc4
Orlice	Orlice	k1gFnSc2
či	či	k8xC
Hradecké	Hradecké	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
též	též	k9
důležitým	důležitý	k2eAgInSc7d1
uzlem	uzel	k1gInSc7
turistů	turist	k1gMnPc2
pokračujícím	pokračující	k2eAgFnPc3d1
za	za	k7c7
zimními	zimní	k2eAgInPc7d1
sporty	sport	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	daleko	k6eNd1
od	od	k7c2
něj	on	k3xPp3gNnSc2
se	se	k3xPyFc4
totiž	totiž	k9
nachází	nacházet	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnPc4d3
pohoří	pohořet	k5eAaPmIp3nP
Krkonoše	Krkonoše	k1gFnPc1
či	či	k8xC
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
</s>
<s>
Školství	školství	k1gNnSc1
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Filosofická	filosofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
Univerzity	univerzita	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Školství	školství	k1gNnSc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
je	být	k5eAaImIp3nS
univerzitní	univerzitní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
působí	působit	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gInSc6
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
se	s	k7c7
čtyřmi	čtyři	k4xCgFnPc7
fakultami	fakulta	k1gFnPc7
a	a	k8xC
jedním	jeden	k4xCgInSc7
ústavem	ústav	k1gInSc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
a	a	k8xC
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
a	a	k8xC
Fakulta	fakulta	k1gFnSc1
vojenského	vojenský	k2eAgNnSc2d1
zdravotnictví	zdravotnictví	k1gNnSc2
Univerzity	univerzita	k1gFnSc2
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zde	zde	k6eAd1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
pobočku	pobočka	k1gFnSc4
Metropolitní	metropolitní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Praha	Praha	k1gFnSc1
známá	známý	k2eAgFnSc1d1
také	také	k9
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
MUP	MUP	kA
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
soukromou	soukromý	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Působí	působit	k5eAaImIp3nS
zde	zde	k6eAd1
tři	tři	k4xCgNnPc4
významná	významný	k2eAgNnPc4d1
gymnázia	gymnázium	k1gNnPc4
s	s	k7c7
dlouhou	dlouhý	k2eAgFnSc7d1
vzdělávací	vzdělávací	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
:	:	kIx,
Gymnázium	gymnázium	k1gNnSc1
J.	J.	kA
K.	K.	kA
Tyla	Tyl	k1gMnSc2
<g/>
,	,	kIx,
Gymnázium	gymnázium	k1gNnSc4
Boženy	Božena	k1gFnSc2
Němcové	Němcové	k2eAgNnSc4d1
a	a	k8xC
Biskupské	biskupský	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Bohuslava	Bohuslava	k1gFnSc1
Balbína	Balbína	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
historickou	historický	k2eAgFnSc7d1
návazností	návaznost	k1gFnSc7
na	na	k7c4
dávné	dávný	k2eAgFnPc4d1
zdejší	zdejší	k2eAgFnPc4d1
jezuitské	jezuitský	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtým	čtvrtý	k4xOgNnSc7
gymnáziem	gymnázium	k1gNnSc7
je	být	k5eAaImIp3nS
První	první	k4xOgNnSc4
soukromé	soukromý	k2eAgNnSc4d1
jazykové	jazykový	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
okolo	okolo	k7c2
deseti	deset	k4xCc2
odborných	odborný	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgMnPc4
patří	patřit	k5eAaImIp3nS
Obchodní	obchodní	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
,	,	kIx,
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
veterinární	veterinární	k2eAgFnSc1d1
či	či	k8xC
soukromá	soukromý	k2eAgFnSc1d1
Střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
aplikované	aplikovaný	k2eAgFnSc2d1
kybernetiky	kybernetika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Klicperovo	Klicperův	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Loutkové	loutkový	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Drak	drak	k1gInSc1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Muzeum	muzeum	k1gNnSc1
východních	východní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Z	z	k7c2
umění	umění	k1gNnSc2
nabízí	nabízet	k5eAaImIp3nS
metropole	metropole	k1gFnSc1
několik	několik	k4yIc4
divadel	divadlo	k1gNnPc2
(	(	kIx(
<g/>
Klicperovo	Klicperův	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Adalbertinum	Adalbertinum	k1gNnSc1
<g/>
,	,	kIx,
loutkové	loutkový	k2eAgNnSc1d1
Divadlo	divadlo	k1gNnSc1
Drak	drak	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kin	kino	k1gNnPc2
(	(	kIx(
<g/>
Centrál	centrála	k1gFnPc2
<g/>
,	,	kIx,
letní	letní	k2eAgNnSc4d1
kino	kino	k1gNnSc4
Širák	širák	k1gInSc1
<g/>
,	,	kIx,
multikino	multikino	k1gNnSc1
Cinestar	Cinestara	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
filharmonii	filharmonie	k1gFnSc4
<g/>
,	,	kIx,
Galerii	galerie	k1gFnSc4
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
východních	východní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
či	či	k8xC
Zahrádkářské	zahrádkářský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštívit	navštívit	k5eAaPmF
lze	lze	k6eAd1
i	i	k9
vědeckou	vědecký	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
<g/>
,	,	kIx,
městskou	městský	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
nebo	nebo	k8xC
místní	místní	k2eAgFnSc4d1
hvězdárnu	hvězdárna	k1gFnSc4
a	a	k8xC
planetárium	planetárium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Festivaly	festival	k1gInPc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
evropských	evropský	k2eAgInPc2d1
regionů	region	k1gInPc2
–	–	k?
vyvrcholení	vyvrcholení	k1gNnSc4
divadelní	divadelní	k2eAgFnSc2d1
sezony	sezona	k1gFnSc2
</s>
<s>
Čekání	čekání	k1gNnSc1
na	na	k7c4
Václava	Václav	k1gMnSc4
–	–	k?
„	„	k?
<g/>
festival	festival	k1gInSc4
velkých	velký	k2eAgNnPc2d1
malých	malý	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
<g/>
“	“	k?
</s>
<s>
Jazz	jazz	k1gInSc1
goes	goes	k6eAd1
to	ten	k3xDgNnSc4
town	town	k1gMnSc1
–	–	k?
nejprestižnější	prestižní	k2eAgMnSc1d3
z	z	k7c2
tuzemských	tuzemský	k2eAgMnPc2d1
i	i	k8xC
evropských	evropský	k2eAgMnPc2d1
jazzových	jazzový	k2eAgMnPc2d1
festivalů	festival	k1gInPc2
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
–	–	k?
festival	festival	k1gInSc4
moderní	moderní	k2eAgFnSc2d1
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
</s>
<s>
Majáles	majáles	k1gInSc1
–	–	k?
studentský	studentský	k2eAgInSc1d1
festival	festival	k1gInSc1
</s>
<s>
Rock	rock	k1gInSc1
for	forum	k1gNnPc2
People	People	k1gFnSc2
–	–	k?
jeden	jeden	k4xCgInSc4
z	z	k7c2
největších	veliký	k2eAgInPc2d3
hudebních	hudební	k2eAgInPc2d1
festivalů	festival	k1gInPc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Hip	hip	k0
Hop	hop	k0
Kemp	kemp	k1gInSc1
–	–	k?
jediný	jediný	k2eAgInSc1d1
třídenní	třídenní	k2eAgInSc1d1
hip-hopový	hip-hopový	k2eAgInSc1d1
festival	festival	k1gInSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Truckfest	Truckfest	k1gMnSc1
–	–	k?
největší	veliký	k2eAgNnSc1d3
mezinárodní	mezinárodní	k2eAgNnSc1d1
setkání	setkání	k1gNnSc1
autodopravců	autodopravce	k1gMnPc2
se	s	k7c7
spanilou	spanilý	k2eAgFnSc7d1
projížďkou	projížďka	k1gFnSc7
městem	město	k1gNnSc7
</s>
<s>
Gastro	Gastro	k6eAd1
Hradec	Hradec	k1gInSc1
–	–	k?
mistrovství	mistrovství	k1gNnSc2
republiky	republika	k1gFnSc2
kuchařů	kuchař	k1gMnPc2
a	a	k8xC
cukrářů	cukrář	k1gMnPc2
i	i	k9
přehlídka	přehlídka	k1gFnSc1
kuchařského	kuchařský	k2eAgNnSc2d1
a	a	k8xC
cukrářského	cukrářský	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
Febiofest	Febiofest	k1gInSc1
–	–	k?
filmový	filmový	k2eAgInSc1d1
festival	festival	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
Hradci	Hradec	k1gInSc6
se	se	k3xPyFc4
v	v	k7c6
Cinestaru	Cinestar	k1gInSc6
konají	konat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnPc1
Ozvěny	ozvěna	k1gFnPc1
</s>
<s>
Entrée	entrée	k1gNnSc4
k	k	k7c3
tanci	tanec	k1gInSc3
–	–	k?
festival	festival	k1gInSc4
taneční	taneční	k2eAgInSc4d1
a	a	k8xC
pohybové	pohybový	k2eAgFnPc4d1
tvorby	tvorba	k1gFnSc2
</s>
<s>
Setkání	setkání	k1gNnSc1
s	s	k7c7
folklórem	folklór	k1gInSc7
–	–	k?
festival	festival	k1gInSc1
pořádaný	pořádaný	k2eAgInSc1d1
domácím	domácí	k2eAgInSc7d1
folklórním	folklórní	k2eAgInSc7d1
souborem	soubor	k1gInSc7
Kvítek	kvítek	k1gInSc1
</s>
<s>
Hradecká	Hradecká	k1gFnSc1
Odette	Odett	k1gInSc5
–	–	k?
festival	festival	k1gInSc4
tance	tanec	k1gInSc2
a	a	k8xC
mezinárodní	mezinárodní	k2eAgFnSc4d1
taneční	taneční	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
</s>
<s>
Slavnosti	slavnost	k1gFnPc1
</s>
<s>
Slavnosti	slavnost	k1gFnPc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
1866	#num#	k4
</s>
<s>
Slavnosti	slavnost	k1gFnPc1
královny	královna	k1gFnSc2
Elišky	Eliška	k1gFnSc2
</s>
<s>
Oblast	oblast	k1gFnSc1
sociálně-zdravotní	sociálně-zdravotní	k2eAgFnSc2d1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
mezi	mezi	k7c4
města	město	k1gNnPc4
s	s	k7c7
vynikající	vynikající	k2eAgFnSc7d1
kvalitou	kvalita	k1gFnSc7
života	život	k1gInSc2
a	a	k8xC
vynikající	vynikající	k2eAgFnSc7d1
sociální	sociální	k2eAgFnSc7d1
situací	situace	k1gFnSc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
ocenění	ocenění	k1gNnPc2
MPSV	MPSV	kA
ČR	ČR	kA
Obec	obec	k1gFnSc1
přátelská	přátelský	k2eAgFnSc1d1
rodině	rodina	k1gFnSc6
2018	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
úspěch	úspěch	k1gInSc4
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Mastercard	Mastercard	k1gInSc1
Česká	český	k2eAgFnSc1d1
karta	karta	k1gFnSc1
rozvoje	rozvoj	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
nebo	nebo	k8xC
Cena	cena	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
působí	působit	k5eAaImIp3nS
Fakultní	fakultní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
jedno	jeden	k4xCgNnSc1
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
zdravotnických	zdravotnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
nejen	nejen	k6eAd1
ve	v	k7c6
východních	východní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
celé	celý	k2eAgFnSc6d1
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilíře	pilíř	k1gInSc2
sociální	sociální	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
krajská	krajský	k2eAgFnSc1d1
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Domov	domov	k1gInSc4
u	u	k7c2
Biřičky	biřička	k1gFnSc2
(	(	kIx(
<g/>
zajišťující	zajišťující	k2eAgFnSc2d1
služby	služba	k1gFnSc2
domova	domov	k1gInSc2
pro	pro	k7c4
seniory	senior	k1gMnPc4
a	a	k8xC
domova	domov	k1gInSc2
se	s	k7c7
zvláštním	zvláštní	k2eAgInSc7d1
režimem	režim	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
bohatá	bohatý	k2eAgFnSc1d1
síť	síť	k1gFnSc1
privátních	privátní	k2eAgMnPc2d1
poskytovatelů	poskytovatel	k1gMnPc2
sociálně-zdravotních	sociálně-zdravotní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřebnost	potřebnost	k1gFnSc1
a	a	k8xC
dostupnost	dostupnost	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
a	a	k8xC
souvisejících	související	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
oficiálním	oficiální	k2eAgInSc7d1
rozvojovým	rozvojový	k2eAgInSc7d1
dokumentem	dokument	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vzniká	vznikat	k5eAaImIp3nS
na	na	k7c6
principu	princip	k1gInSc6
komunitního	komunitní	k2eAgNnSc2d1
plánování	plánování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statutární	statutární	k2eAgInSc4d1
město	město	k1gNnSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
zřizuje	zřizovat	k5eAaImIp3nS
příspěvkovou	příspěvkový	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
Dětský	dětský	k2eAgInSc4d1
denní	denní	k2eAgInSc4d1
rehabilitační	rehabilitační	k2eAgInSc4d1
stacionář	stacionář	k1gInSc4
zabývající	zabývající	k2eAgInSc4d1
se	se	k3xPyFc4
komplexní	komplexní	k2eAgFnSc7d1
poradenskou	poradenský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
a	a	k8xC
rehabilitační	rehabilitační	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
o	o	k7c4
děti	dítě	k1gFnPc4
s	s	k7c7
poruchami	porucha	k1gFnPc7
hybnosti	hybnost	k1gFnSc2
a	a	k8xC
opožděným	opožděný	k2eAgInSc7d1
psychomotorickým	psychomotorický	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
organizační	organizační	k2eAgFnSc4d1
složky	složka	k1gFnPc4
odboru	odbor	k1gInSc2
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
a	a	k8xC
zdravotnictví	zdravotnictví	k1gNnSc1
patří	patřit	k5eAaImIp3nS
městské	městský	k2eAgFnPc4d1
jesle	jesle	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
je	být	k5eAaImIp3nS
věnována	věnován	k2eAgFnSc1d1
prevenci	prevence	k1gFnSc4
kriminality	kriminalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
informací	informace	k1gFnPc2
<g/>
:	:	kIx,
https://www.hradeckralove.org/socialni-portal/	https://www.hradeckralove.org/socialni-portal/	k?
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Model	model	k1gInSc1
Všesportovního	všesportovní	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
v	v	k7c6
Malšovicích	Malšovice	k1gFnPc6
</s>
<s>
Sportoviště	sportoviště	k1gNnSc1
</s>
<s>
Všesportovní	všesportovní	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Malšovice	Malšovice	k1gFnSc2
–	–	k?
domovské	domovský	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
„	„	k?
<g/>
Pod	pod	k7c7
lízátky	lízátko	k1gNnPc7
<g/>
“	“	k?
klubu	klub	k1gInSc2
FC	FC	kA
Hradec	Hradec	k1gInSc1
Králové	Králové	k2eAgInSc2d1
</s>
<s>
ČPP	ČPP	kA
Aréna	aréna	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
ledová	ledový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
–	–	k?
stadion	stadion	k1gInSc4
klubu	klub	k1gInSc2
Mountfield	Mountfielda	k1gFnPc2
HK	HK	kA
</s>
<s>
Aquacentrum	Aquacentrum	k1gNnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
propojené	propojený	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
plaveckého	plavecký	k2eAgInSc2d1
a	a	k8xC
zábavního	zábavní	k2eAgInSc2d1
bazénu	bazén	k1gInSc2
</s>
<s>
Víceúčelová	víceúčelový	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
Třebeš	Třebeš	k1gFnSc2
–	–	k?
nový	nový	k2eAgInSc4d1
projekt	projekt	k1gInSc4
magistrátu	magistrát	k1gInSc2
o	o	k7c4
vybudování	vybudování	k1gNnSc4
sportovního	sportovní	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
</s>
<s>
Celoroční	celoroční	k2eAgNnSc1d1
koupaliště	koupaliště	k1gNnSc1
Flošna	Flošn	k1gInSc2
–	–	k?
nový	nový	k2eAgInSc1d1
multiplex	multiplex	k1gInSc1
bazénů	bazén	k1gInPc2
</s>
<s>
Sokol	Sokol	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
atletický	atletický	k2eAgInSc1d1
klub	klub	k1gInSc1
Sokol	Sokol	k1gMnSc1
na	na	k7c6
Eliščině	Eliščin	k2eAgNnSc6d1
nábřeží	nábřeží	k1gNnSc6
</s>
<s>
Armádní	armádní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
střelnice	střelnice	k1gFnSc1
v	v	k7c6
Malšovicích	Malšovice	k1gFnPc6
–	–	k?
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nemnoha	nemnoha	k1gFnSc1
vrcholných	vrcholný	k2eAgFnPc2d1
střelnic	střelnice	k1gFnPc2
pro	pro	k7c4
brokové	brokový	k2eAgFnPc4d1
disciplíny	disciplína	k1gFnPc4
v	v	k7c6
ČR	ČR	kA
</s>
<s>
CzechRing	CzechRing	k1gInSc1
–	–	k?
Autodrom	autodrom	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Sportovní	sportovní	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
DFC	DFC	kA
Slavia	Slavia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
ženský	ženský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
DTJ	DTJ	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
oddíl	oddíl	k1gInSc1
stolního	stolní	k2eAgInSc2d1
tenisu	tenis	k1gInSc2
</s>
<s>
FbC	FbC	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
florbalový	florbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
FBC	FBC	kA
Sion	Sion	k1gInSc4
Hradečtí	hradecký	k2eAgMnPc1d1
lvi	lev	k1gMnPc1
–	–	k?
florbalový	florbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
FC	FC	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
FC	FC	kA
Olympia	Olympia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
HBC	HBC	kA
Autosklo	Autosklo	k1gMnSc1
H.A.K.	H.A.K.	k1gMnSc1
HK	HK	kA
–	–	k?
hokejbalový	hokejbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
</s>
<s>
HK	HK	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
házenkářský	házenkářský	k2eAgInSc1d1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Dragons	Dragons	k1gInSc1
–	–	k?
klub	klub	k1gInSc4
amerického	americký	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
</s>
<s>
HSK	HSK	kA
cycling	cycling	k1gInSc1
team	team	k1gInSc1
–	–	k?
cyklistický	cyklistický	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
IBK	IBK	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
florbalový	florbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Mountfield	Mountfield	k1gMnSc1
HK	HK	kA
–	–	k?
hokejový	hokejový	k2eAgInSc4d1
klub	klub	k1gInSc4
</s>
<s>
PK	PK	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
plavecký	plavecký	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
RLC	RLC	kA
Slavia	Slavia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
klub	klub	k1gInSc1
rugby	rugby	k1gNnSc2
league	leaguat	k5eAaPmIp3nS
</s>
<s>
ŠK	ŠK	kA
ORTEX-RETA	ORTEX-RETA	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
šachový	šachový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Slavia	Slavia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
volejbal	volejbal	k1gInSc1
</s>
<s>
Slavia	Slavia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
pozemní	pozemní	k2eAgInSc1d1
hokejový	hokejový	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Slavia	Slavia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
vodní	vodní	k2eAgNnSc1d1
pólo	pólo	k1gNnSc1
</s>
<s>
SK	Sk	kA
Karate	karate	k1gNnSc1
Spartak	Spartak	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
karate	karate	k1gNnSc1
</s>
<s>
Sokol	Sokol	k1gMnSc1
PP	PP	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
2	#num#	k4
–	–	k?
basketbalový	basketbalový	k2eAgInSc4d1
klub	klub	k1gInSc4
mužů	muž	k1gMnPc2
</s>
<s>
SSK	SSK	kA
Třebeš	Třebeš	k1gFnSc1
–	–	k?
sportovní	sportovní	k2eAgInSc1d1
střelecký	střelecký	k2eAgInSc1d1
klub	klub	k1gInSc1
Třebeš	Třebeš	k1gFnSc2
</s>
<s>
TJ	tj	kA
Slavia	Slavia	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
oddíl	oddíl	k1gInSc1
volejbalu	volejbal	k1gInSc2
chlapců	chlapec	k1gMnPc2
a	a	k8xC
mužů	muž	k1gMnPc2
</s>
<s>
TJ	tj	kA
Sokol	Sokol	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
kanoistický	kanoistický	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
</s>
<s>
TJ	tj	kA
Sokol	Sokol	k1gMnSc1
VČP	VČP	kA
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
basketbal	basketbal	k1gInSc1
</s>
<s>
TK	TK	kA
TŠ	TŠ	kA
KROK	krok	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
taneční	taneční	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Masarykovo	Masarykův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
proslulé	proslulý	k2eAgNnSc1d1
moderní	moderní	k2eAgFnSc7d1
výstavbou	výstavba	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
dokonce	dokonce	k9
mu	on	k3xPp3gNnSc3
bylo	být	k5eAaImAgNnS
přisouzeno	přisouzen	k2eAgNnSc1d1
označení	označení	k1gNnSc1
Salon	salon	k1gInSc4
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
této	tento	k3xDgFnSc2
intenzivní	intenzivní	k2eAgFnSc2d1
práce	práce	k1gFnSc2
významných	významný	k2eAgMnPc2d1
architektů	architekt	k1gMnPc2
patří	patřit	k5eAaImIp3nS
Janu	Jan	k1gMnSc3
Kotěrovi	Kotěra	k1gMnSc3
<g/>
,	,	kIx,
autorovi	autor	k1gMnSc3
secesní	secesní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
stává	stávat	k5eAaImIp3nS
vůdčím	vůdčí	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
města	město	k1gNnSc2
Josef	Josef	k1gMnSc1
Gočár	Gočár	k1gMnSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
vděčí	vděčit	k5eAaImIp3nS
za	za	k7c4
vznik	vznik	k1gInSc4
mnoha	mnoho	k4c2
staveb	stavba	k1gFnPc2
nové	nový	k2eAgFnSc2d1
části	část	k1gFnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
jinými	jiný	k2eAgInPc7d1
budova	budova	k1gFnSc1
Ředitelství	ředitelství	k1gNnSc2
drah	draha	k1gFnPc2
na	na	k7c6
Ulrichově	Ulrichův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Krajské	krajský	k2eAgNnSc1d1
ředitelství	ředitelství	k1gNnSc1
Policie	policie	k1gFnSc2
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úprava	úprava	k1gFnSc1
Masarykova	Masarykův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
s	s	k7c7
pomníkem	pomník	k1gInSc7
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
komplex	komplex	k1gInSc4
škol	škola	k1gFnPc2
na	na	k7c6
Tylově	Tylův	k2eAgNnSc6d1
nábřeží	nábřeží	k1gNnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgNnSc1d1
Gymnázium	gymnázium	k1gNnSc1
J.	J.	kA
K.	K.	kA
Tyla	tyla	k1gFnSc1
a	a	k8xC
přilehlá	přilehlý	k2eAgFnSc1d1
bývalá	bývalý	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
areál	areál	k1gInSc1
budov	budova	k1gFnPc2
Sboru	sbor	k1gInSc2
kněze	kněz	k1gMnSc2
Ambrože	Ambrož	k1gMnSc2
Církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
Magistrátu	magistrát	k1gInSc2
na	na	k7c6
třídě	třída	k1gFnSc6
ČSA	ČSA	kA
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Dálnice	dálnice	k1gFnSc2
D	D	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
Dálnice	dálnice	k1gFnSc1
D35	D35	k1gFnSc1
a	a	k8xC
Silnice	silnice	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
důležité	důležitý	k2eAgFnSc6d1
křižovatce	křižovatka	k1gFnSc6
cest	cesta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
později	pozdě	k6eAd2
nahradily	nahradit	k5eAaPmAgFnP
silnice	silnice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
městem	město	k1gNnSc7
procházejí	procházet	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
I	I	kA
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
I	I	kA
<g/>
/	/	kIx~
<g/>
35	#num#	k4
a	a	k8xC
I	I	kA
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
31	#num#	k4
tvoří	tvořit	k5eAaImIp3nS
městský	městský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
končí	končit	k5eAaImIp3nS
dálnice	dálnice	k1gFnSc1
D11	D11	k1gFnSc1
a	a	k8xC
D	D	kA
<g/>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrajovými	okrajový	k2eAgFnPc7d1
částmi	část	k1gFnPc7
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
procházejí	procházet	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
druhé	druhý	k4xOgFnSc2
třídy	třída	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
308	#num#	k4
a	a	k8xC
II	II	kA
<g/>
/	/	kIx~
<g/>
324	#num#	k4
a	a	k8xC
evropské	evropský	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
silnice	silnice	k1gFnPc1
E67	E67	k1gFnSc1
a	a	k8xC
E	E	kA
<g/>
442	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budoucnu	budoucno	k1gNnSc6
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
všechny	všechen	k3xTgFnPc1
silnice	silnice	k1gFnPc1
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
převedeny	převeden	k2eAgFnPc1d1
na	na	k7c4
dálnice	dálnice	k1gFnPc4
nebo	nebo	k8xC
obchvaty	obchvat	k1gInPc4
a	a	k8xC
tranzitní	tranzitní	k2eAgInSc4d1
provoz	provoz	k1gInSc4
úplně	úplně	k6eAd1
vyloučen	vyloučen	k2eAgMnSc1d1
z	z	k7c2
městských	městský	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dálková	dálkový	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Terminál	terminál	k1gInSc1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Terminál	terminála	k1gFnPc2
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
sousedních	sousední	k2eAgInPc2d1
Pardubic	Pardubice	k1gInPc2
(	(	kIx(
<g/>
železniční	železniční	k2eAgInSc1d1
koridor	koridor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
důležitým	důležitý	k2eAgInSc7d1
bodem	bod	k1gInSc7
v	v	k7c6
autobusové	autobusový	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
mezi	mezi	k7c7
Polskem	Polsko	k1gNnSc7
a	a	k8xC
zbytkem	zbytek	k1gInSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
jiná	jiný	k2eAgNnPc4d1
krajská	krajský	k2eAgNnPc4d1
města	město	k1gNnPc4
je	být	k5eAaImIp3nS
důležitým	důležitý	k2eAgInSc7d1
dopravním	dopravní	k2eAgInSc7d1
uzlem	uzel	k1gInSc7
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
důležitá	důležitý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
kraj	kraj	k1gInSc4
hlavně	hlavně	k9
doprava	doprava	k1gFnSc1
mezi	mezi	k7c7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Prahou	Praha	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
po	po	k7c6
dálnici	dálnice	k1gFnSc6
D	D	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
sousedním	sousední	k2eAgNnSc7d1
městem	město	k1gNnSc7
Pardubice	Pardubice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
doprava	doprava	k1gFnSc1
vedena	vést	k5eAaImNgFnS
především	především	k6eAd1
železniční	železniční	k2eAgFnSc1d1
tratí	trať	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
magistrát	magistrát	k1gInSc1
přikládá	přikládat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc4d2
důležitost	důležitost	k1gFnSc4
právě	právě	k6eAd1
autobusové	autobusový	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
s	s	k7c7
novým	nový	k2eAgInSc7d1
terminálem	terminál	k1gInSc7
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
bude	být	k5eAaImBp3nS
mezi	mezi	k7c7
městy	město	k1gNnPc7
fungovat	fungovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
po	po	k7c6
Rašínově	Rašínův	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
dále	daleko	k6eAd2
silnicí	silnice	k1gFnSc7
I	I	kA
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Trolejbus	trolejbus	k1gInSc1
Škoda	škoda	k1gFnSc1
31	#num#	k4
<g/>
Tr	Tr	k1gFnSc1
SOR	SOR	kA
na	na	k7c6
Riegrově	Riegrův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
před	před	k7c7
budovou	budova	k1gFnSc7
hlavního	hlavní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Trolejbusová	trolejbusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
a	a	k8xC
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
denně	denně	k6eAd1
přepraví	přepravit	k5eAaPmIp3nS
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
po	po	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
města	město	k1gNnSc2
i	i	k9
do	do	k7c2
okolních	okolní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
jezdí	jezdit	k5eAaImIp3nS
44	#num#	k4
linek	linka	k1gFnPc2
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
jsou	být	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc1
noční	noční	k2eAgFnPc1d1
autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc1
rychlíkové	rychlíkový	k2eAgInPc1d1
<g/>
,	,	kIx,
osm	osm	k4xCc1
školních	školní	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
<g/>
,	,	kIx,
20	#num#	k4
linek	linka	k1gFnPc2
městské	městský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
a	a	k8xC
8	#num#	k4
linek	linka	k1gFnPc2
trolejbusové	trolejbusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
jezdí	jezdit	k5eAaImIp3nP
obyčejné	obyčejný	k2eAgFnPc4d1
pravidelné	pravidelný	k2eAgFnPc4d1
jízdy	jízda	k1gFnPc4
podle	podle	k7c2
jízdních	jízdní	k2eAgInPc2d1
řádů	řád	k1gInPc2
dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
města	město	k1gNnSc2
po	po	k7c4
celý	celý	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
čísla	číslo	k1gNnPc4
linek	linka	k1gFnPc2
MHD	MHD	kA
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
jak	jak	k6eAd1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
trolejbusy	trolejbus	k1gInPc1
(	(	kIx(
<g/>
v	v	k7c6
minulosti	minulost	k1gFnSc6
plánované	plánovaný	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
nebyly	být	k5eNaImAgInP
zavedeny	zaveden	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síť	síť	k1gFnSc1
MHD	MHD	kA
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgNnP
do	do	k7c2
dvou	dva	k4xCgNnPc2
tarifních	tarifní	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
druhé	druhý	k4xOgNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
však	však	k9
pouze	pouze	k6eAd1
okolní	okolní	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Charbuzice	Charbuzice	k1gFnSc1
<g/>
,	,	kIx,
Předměřice	Předměřice	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Lochenice	Lochenice	k1gFnSc2
<g/>
,	,	kIx,
Stěžery	Stěžera	k1gFnSc2
<g/>
,	,	kIx,
Stěžírky	Stěžírka	k1gFnSc2
<g/>
,	,	kIx,
Vysokou	vysoká	k1gFnSc4
nad	nad	k7c7
Labem	Labe	k1gNnSc7
a	a	k8xC
Běleč	Běleč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
sítě	síť	k1gFnSc2
MHD	MHD	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
prvním	první	k4xOgNnSc6
tarifním	tarifní	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
zaveden	zaveden	k2eAgInSc4d1
čipový	čipový	k2eAgInSc4d1
odbavovací	odbavovací	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
druhem	druh	k1gMnSc7
karty	karta	k1gFnSc2
je	být	k5eAaImIp3nS
časová	časový	k2eAgFnSc1d1
<g/>
:	:	kIx,
na	na	k7c4
jeden	jeden	k4xCgInSc4
týden	týden	k1gInSc4
<g/>
,	,	kIx,
měsíc	měsíc	k1gInSc4
<g/>
,	,	kIx,
čtvrt	čtvrt	k1gFnSc4
roku	rok	k1gInSc2
<g/>
,	,	kIx,
půl	půl	k6eAd1
roku	rok	k1gInSc2
nebo	nebo	k8xC
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc4
je	být	k5eAaImIp3nS
kuponová	kuponový	k2eAgFnSc1d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
do	do	k7c2
karty	karta	k1gFnSc2
dobít	dobít	k5eAaPmF
kredit	kredit	k1gInSc4
u	u	k7c2
automatů	automat	k1gInPc2
či	či	k8xC
v	v	k7c6
kancelářích	kancelář	k1gFnPc6
dopravního	dopravní	k2eAgInSc2d1
podniku	podnik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
variantě	varianta	k1gFnSc6
však	však	k9
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
označit	označit	k5eAaPmF
každý	každý	k3xTgInSc4
nástup	nástup	k1gInSc4
do	do	k7c2
dopravního	dopravní	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
na	na	k7c6
čtečce	čtečka	k1gFnSc6
karet	kareta	k1gFnPc2
u	u	k7c2
dveří	dveře	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zvýhodněnou	zvýhodněný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
při	při	k7c6
přestupu	přestup	k1gInSc6
nebo	nebo	k8xC
jízdě	jízda	k1gFnSc6
jen	jen	k9
na	na	k7c4
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
zastávky	zastávka	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
označit	označit	k5eAaPmF
i	i	k9
výstup	výstup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
uhradit	uhradit	k5eAaPmF
jízdné	jízdné	k1gNnSc1
bezkontaktními	bezkontaktní	k2eAgFnPc7d1
platebními	platební	k2eAgFnPc7d1
kartami	karta	k1gFnPc7
VISA	viso	k1gNnSc2
a	a	k8xC
MasterCard	MasterCarda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtečka	čtečka	k1gFnSc1
platebních	platební	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
označovač	označovač	k1gInSc4
papírových	papírový	k2eAgFnPc2d1
jízdenek	jízdenka	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
řidiče	řidič	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
začal	začít	k5eAaPmAgInS
od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
fungovat	fungovat	k5eAaImF
nový	nový	k2eAgInSc4d1
terminál	terminál	k1gInSc4
MHD	MHD	kA
i	i	k8xC
vnitrostátních	vnitrostátní	k2eAgFnPc2d1
a	a	k8xC
mezinárodních	mezinárodní	k2eAgFnPc2d1
autobusových	autobusový	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
město	město	k1gNnSc1
čekalo	čekat	k5eAaImAgNnS
přes	přes	k7c4
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejmodernějších	moderní	k2eAgInPc2d3
terminálů	terminál	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terminál	terminál	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
nástupišti	nástupiště	k1gNnPc7
A	a	k9
<g/>
–	–	k?
<g/>
J	J	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nástupiště	nástupiště	k1gNnSc1
A	a	k8xC
<g/>
–	–	k?
<g/>
D	D	kA
patří	patřit	k5eAaImIp3nS
MHD	MHD	kA
a	a	k8xC
nástupiště	nástupiště	k1gNnSc2
E	E	kA
<g/>
–	–	k?
<g/>
J	J	kA
spojům	spoj	k1gInPc3
vnitrostátní	vnitrostátní	k2eAgInPc4d1
i	i	k9
mezinárodní	mezinárodní	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
vzniklý	vzniklý	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
komplex	komplex	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
asi	asi	k9
300	#num#	k4
metrů	metr	k1gInPc2
severně	severně	k6eAd1
od	od	k7c2
hlavního	hlavní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeprava	přeprava	k1gFnSc1
cestujících	cestující	k1gFnPc2
od	od	k7c2
terminálu	terminál	k1gInSc2
před	před	k7c4
nádražní	nádražní	k2eAgFnSc4d1
halu	hala	k1gFnSc4
na	na	k7c6
Riegrově	Riegrův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
je	být	k5eAaImIp3nS
ve	v	k7c6
vozech	vůz	k1gInPc6
MHD	MHD	kA
zajištěna	zajistit	k5eAaPmNgFnS
zdarma	zdarma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
komplex	komplex	k1gInSc1
disponuje	disponovat	k5eAaBmIp3nS
moderní	moderní	k2eAgFnSc7d1
čekárnou	čekárna	k1gFnSc7
a	a	k8xC
pizzerií	pizzerie	k1gFnSc7
v	v	k7c6
jedné	jeden	k4xCgFnSc6
budově	budova	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
budově	budova	k1gFnSc6
nového	nový	k2eAgInSc2d1
terminálu	terminál	k1gInSc2
je	být	k5eAaImIp3nS
též	též	k9
moderní	moderní	k2eAgFnSc1d1
čekárna	čekárna	k1gFnSc1
s	s	k7c7
restaurací	restaurace	k1gFnSc7
pro	pro	k7c4
80	#num#	k4
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
praktické	praktický	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
cestující	cestující	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
cestují	cestovat	k5eAaImIp3nP
přes	přes	k7c4
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
novém	nový	k2eAgInSc6d1
terminálu	terminál	k1gInSc6
vidí	vidět	k5eAaImIp3nS
na	na	k7c6
digitálním	digitální	k2eAgInSc6d1
panelu	panel	k1gInSc6
odjezdy	odjezd	k1gInPc4
spojů	spoj	k1gInPc2
Českých	český	k2eAgFnPc2d1
drah	draha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Riegrovo	Riegrův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
a	a	k8xC
Hlavní	hlavní	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
má	mít	k5eAaImIp3nS
přímé	přímý	k2eAgNnSc1d1
železniční	železniční	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
do	do	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
Chocně	Choceň	k1gFnSc2
(	(	kIx(
<g/>
trať	trať	k1gFnSc1
0	#num#	k4
<g/>
20	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
Pardubic	Pardubice	k1gInPc2
a	a	k8xC
Liberce	Liberec	k1gInSc2
(	(	kIx(
<g/>
trať	trať	k1gFnSc1
0	#num#	k4
<g/>
31	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
Turnova	Turnov	k1gInSc2
(	(	kIx(
<g/>
trať	trať	k1gFnSc1
0	#num#	k4
<g/>
41	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
pět	pět	k4xCc4
stanic	stanice	k1gFnPc2
–	–	k?
největší	veliký	k2eAgInSc1d3
a	a	k8xC
nejznámější	známý	k2eAgInSc1d3
je	být	k5eAaImIp3nS
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
hlavní	hlavní	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
moderní	moderní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
již	již	k6eAd1
z	z	k7c2
období	období	k1gNnSc2
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
nádražím	nádraží	k1gNnSc7
maloměstského	maloměstský	k2eAgInSc2d1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
podobné	podobný	k2eAgNnSc1d1
má	mít	k5eAaImIp3nS
například	například	k6eAd1
Kroměříž	Kroměříž	k1gFnSc1
<g/>
,	,	kIx,
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
či	či	k8xC
Třebechovice	Třebechovice	k1gFnPc4
pod	pod	k7c7
Orebem	Oreb	k1gInSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Hradec	Hradec	k1gInSc4
Králové-Slezské	Králové-Slezský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
zastávkami	zastávka	k1gFnPc7
jsou	být	k5eAaImIp3nP
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
zastávka	zastávka	k1gFnSc1
na	na	k7c6
kraji	kraj	k1gInSc6
místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
Pouchov	Pouchov	k1gInSc1
a	a	k8xC
Hradec	Hradec	k1gInSc1
Králové-Kukleny	Králové-Kuklena	k1gFnSc2
ve	v	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
místní	místní	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
Plotiště	Plotiště	k1gNnSc4
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Na	na	k7c6
královéhradeckém	královéhradecký	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
se	se	k3xPyFc4
pořádá	pořádat	k5eAaImIp3nS
řada	řada	k1gFnSc1
akcí	akce	k1gFnPc2
jako	jako	k8xS,k8xC
Hip	hip	k0
Hop	hop	k0
Kemp	kemp	k1gInSc1
<g/>
,	,	kIx,
Rock	rock	k1gInSc1
for	forum	k1gNnPc2
People	People	k1gFnSc2
<g/>
,	,	kIx,
Air	Air	k1gFnSc2
Ambulance	ambulance	k1gFnSc2
<g/>
,	,	kIx,
Helicoptershow	Helicoptershow	k1gFnSc2
nebo	nebo	k8xC
z	z	k7c2
Brna	Brno	k1gNnSc2
zpět	zpět	k6eAd1
přesunutý	přesunutý	k2eAgMnSc1d1
CIAF	CIAF	kA
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
J.	J.	kA
K.	K.	kA
Tyla	Tyl	k1gMnSc2
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
osobností	osobnost	k1gFnPc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
starostům	starosta	k1gMnPc3
města	město	k1gNnSc2
patřil	patřit	k5eAaImAgMnS
František	František	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
zásluhou	zásluha	k1gFnSc7
byl	být	k5eAaImAgInS
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nazýván	nazýván	k2eAgInSc4d1
„	„	k?
<g/>
salonem	salon	k1gInSc7
republiky	republika	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
toto	tento	k3xDgNnSc4
označení	označení	k1gNnSc4
se	se	k3xPyFc4
zasloužili	zasloužit	k5eAaPmAgMnP
i	i	k9
architekti	architekt	k1gMnPc1
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Gočár	Gočár	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
historií	historie	k1gFnSc7
města	město	k1gNnSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
Bohuslav	Bohuslav	k1gMnSc1
Balbín	Balbín	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Kliment	Kliment	k1gMnSc1
Klicpera	Klicpera	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Chmela	Chmel	k1gMnSc2
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Durych	Durych	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Preclík	preclík	k1gInSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Kampelík	Kampelík	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
František	František	k1gMnSc1
Červený	Červený	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
slavné	slavný	k2eAgFnSc2d1
hradecké	hradecký	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
na	na	k7c4
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Petrof	Petrof	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
továrny	továrna	k1gFnSc2
na	na	k7c4
piana	piano	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgNnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
královéhradeckém	královéhradecký	k2eAgInSc6d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Tylově	Tylův	k2eAgNnSc6d1
<g/>
)	)	kIx)
gymnáziu	gymnázium	k1gNnSc6
studovali	studovat	k5eAaImAgMnP
například	například	k6eAd1
Václav	Václav	k1gMnSc1
Hanka	Hanka	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Langer	Langer	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Kajetán	Kajetán	k1gMnSc1
Tyl	Tyl	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Erben	Erben	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Škroup	Škroup	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
i	i	k9
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
nebo	nebo	k8xC
Emil	Emil	k1gMnSc1
Vachek	Vachek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Rodáci	rodák	k1gMnPc1
</s>
<s>
Dagmar	Dagmar	k1gFnSc1
Andrtová-Voňková	Andrtová-Voňková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
kytaristka	kytaristka	k1gFnSc1
<g/>
,	,	kIx,
skladatelka	skladatelka	k1gFnSc1
a	a	k8xC
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1
Balbín	Balbín	k1gMnSc1
(	(	kIx(
<g/>
1621	#num#	k4
<g/>
–	–	k?
<g/>
1688	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
literát	literát	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
zeměpisec	zeměpisec	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
</s>
<s>
Bohumil	Bohumil	k1gMnSc1
Bauše	Bauše	k1gFnSc2
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
středoškolský	středoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
přírodovědec	přírodovědec	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
<g/>
,	,	kIx,
propagátor	propagátor	k1gMnSc1
a	a	k8xC
popularizátor	popularizátor	k1gMnSc1
přírodních	přírodní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
</s>
<s>
Josef	Josef	k1gMnSc1
Bek	bek	k1gMnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Bubeníček	Bubeníček	k1gMnSc1
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
brutalismu	brutalismus	k1gInSc2
</s>
<s>
Adolf	Adolf	k1gMnSc1
Černý	Černý	k1gMnSc1
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
etnograf	etnograf	k1gMnSc1
<g/>
,	,	kIx,
filolog	filolog	k1gMnSc1
<g/>
,	,	kIx,
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
a	a	k8xC
univerzitní	univerzitní	k2eAgMnSc1d1
lektor	lektor	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Dolanský	Dolanský	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
herec	herec	k1gMnSc1
a	a	k8xC
dabér	dabér	k1gMnSc1
</s>
<s>
Dominik	Dominik	k1gMnSc1
Duka	Duka	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dominikán	dominikán	k1gMnSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
biskup	biskup	k1gMnSc1
královéhradecký	královéhradecký	k2eAgMnSc1d1
<g/>
,	,	kIx,
36	#num#	k4
<g/>
.	.	kIx.
arcibiskup	arcibiskup	k1gMnSc1
pražský	pražský	k2eAgMnSc1d1
a	a	k8xC
primas	primas	k1gMnSc1
český	český	k2eAgMnSc1d1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Durych	Durych	k1gMnSc1
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
vojenský	vojenský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
a	a	k8xC
katolický	katolický	k2eAgMnSc1d1
prozaik	prozaik	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
</s>
<s>
Silvie	Silvie	k1gFnSc1
Dymáková	Dymáková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režisérka	režisérka	k1gFnSc1
<g/>
,	,	kIx,
kameramanka	kameramanka	k1gFnSc1
<g/>
,	,	kIx,
scenáristka	scenáristka	k1gFnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Fischl	Fischl	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
židovský	židovský	k2eAgMnSc1d1
a	a	k8xC
izraelský	izraelský	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
prozaik	prozaik	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Frýdek	Frýdek	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
a	a	k8xC
čs	čs	kA
<g/>
.	.	kIx.
reprezentant	reprezentant	k1gMnSc1
</s>
<s>
Emil	Emil	k1gMnSc1
Hájek	Hájek	k1gMnSc1
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česko-srbský	česko-srbský	k2eAgMnSc1d1
pianista	pianista	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Hála	Hála	k1gMnSc1
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přední	přední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
swingový	swingový	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
</s>
<s>
Leoš	Leoš	k1gMnSc1
Heger	Heger	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
<g/>
;	;	kIx,
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zdravotnictví	zdravotnictví	k1gNnSc2
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Hejda	Hejda	k1gMnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
básník	básník	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
</s>
<s>
Aleš	Aleš	k1gMnSc1
Hodina	hodina	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
občanský	občanský	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Horák	Horák	k1gMnSc1
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
politolog	politolog	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
exilový	exilový	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
ČSSD	ČSSD	kA
</s>
<s>
Martin	Martin	k1gMnSc1
Hosták	Hosták	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
hokejový	hokejový	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
<g/>
,	,	kIx,
televizní	televizní	k2eAgMnSc1d1
komentátor	komentátor	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Hrdina	Hrdina	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lední	lední	k2eAgMnSc1d1
hokejista	hokejista	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kikinčuk	Kikinčuk	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Kulič	kulič	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Loevenstein	Loevenstein	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průmyslník	průmyslník	k1gMnSc1
a	a	k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Škodových	Škodových	k2eAgInPc2d1
závodů	závod	k1gInPc2
</s>
<s>
Cyprián	Cyprián	k1gMnSc1
Karásek	Karásek	k1gMnSc1
Lvovický	Lvovický	k2eAgMnSc1d1
(	(	kIx(
<g/>
1514	#num#	k4
<g/>
–	–	k?
<g/>
1574	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
astronom	astronom	k1gMnSc1
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
a	a	k8xC
astrolog	astrolog	k1gMnSc1
</s>
<s>
Ljuba	Ljuba	k1gFnSc1
Krbová	krbový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Medek	Medek	k1gMnSc1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
legionář	legionář	k1gMnSc1
a	a	k8xC
vlastenec	vlastenec	k1gMnSc1
</s>
<s>
Zet	Zet	k1gMnSc1
Molas	Molas	k1gMnSc1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
–	–	k?
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
režisérka	režisérka	k1gFnSc1
<g/>
,	,	kIx,
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Zdenka	Zdenka	k1gFnSc1
Holubová	holubový	k2eAgFnSc1d1
<g/>
,	,	kIx,
provdaná	provdaný	k2eAgFnSc1d1
Smolová	Smolová	k1gFnSc1
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Navarová	Navarová	k1gFnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpěvačka	zpěvačka	k1gFnSc1
<g/>
,	,	kIx,
skladatelka	skladatelka	k1gFnSc1
a	a	k8xC
textařka	textařka	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Piták	Piták	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Plesl	Plesl	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Preclík	preclík	k1gInSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
a	a	k8xC
první	první	k4xOgMnSc1
děkan	děkan	k1gMnSc1
Fakulty	fakulta	k1gFnSc2
prům	prům	k1gMnSc1
<g/>
.	.	kIx.
designu	design	k1gInSc2
VUT	VUT	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Karel	Karel	k1gMnSc1
Rokytanský	Rokytanský	k2eAgMnSc1d1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Carl	Carl	k1gMnSc1
Freiherr	Freiherra	k1gFnPc2
von	von	k1gInSc4
Rokitansky	Rokitansky	k1gFnSc4
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rakouský	rakouský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
patolog	patolog	k1gMnSc1
<g/>
,	,	kIx,
filosof	filosof	k1gMnSc1
a	a	k8xC
liberální	liberální	k2eAgMnSc1d1
politik	politik	k1gMnSc1
českého	český	k2eAgInSc2d1
původu	původ	k1gInSc2
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Siniaková	Siniakový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
tenistka	tenistka	k1gFnSc1
</s>
<s>
Ota	Ota	k1gMnSc1
Sklenčka	Sklenčka	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Sládek	Sládek	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
několikanásobný	několikanásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
footbagu	footbag	k1gInSc6
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Sládek	Sládek	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
</s>
<s>
Tereza	Tereza	k1gFnSc1
Smitková	Smitková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
tenistka	tenistka	k1gFnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Socha	Socha	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
popularizátor	popularizátor	k1gMnSc1
paleontologie	paleontologie	k1gFnSc2
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Svěcený	svěcený	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
houslový	houslový	k2eAgInSc1d1
virtuóz	virtuóza	k1gFnPc2
</s>
<s>
Karel	Karel	k1gMnSc1
Sudimír	Sudimír	k1gMnSc1
Šnajdr	Šnajdr	k1gMnSc1
(	(	kIx(
<g/>
1766	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
obrozenecký	obrozenecký	k2eAgMnSc1d1
básník	básník	k1gMnSc1
píšící	píšící	k2eAgMnSc1d1
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
balady	balada	k1gFnSc2
Jan	Jan	k1gMnSc1
za	za	k7c2
chrta	chrt	k1gMnSc2
dán	dán	k2eAgMnSc1d1
<g/>
,	,	kIx,
přítel	přítel	k1gMnSc1
Václava	Václav	k1gMnSc2
Klimenta	Kliment	k1gMnSc2
Klicpery	Klicpera	k1gFnSc2
</s>
<s>
František	František	k1gMnSc1
Štolba	Štolba	k1gMnSc1
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
chemické	chemický	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
a	a	k8xC
enzymologie	enzymologie	k1gFnSc2
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
technice	technika	k1gFnSc6
a	a	k8xC
rektor	rektor	k1gMnSc1
ČVUT	ČVUT	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Štolba	Štolba	k1gMnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
vychovatel	vychovatel	k1gMnSc1
<g/>
,	,	kIx,
právník	právník	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Švorcová	švorcový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
a	a	k8xC
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
a	a	k8xC
komunistická	komunistický	k2eAgFnSc1d1
politička	politička	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Tuček	Tuček	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
několikanásobný	několikanásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
footbagu	footbag	k1gInSc6
</s>
<s>
Lenka	Lenka	k1gFnSc1
Termerová	Termerová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Vladivoj	Vladivoj	k1gInSc1
Tomek	Tomek	k1gMnSc1
(	(	kIx(
<g/>
1818	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
archivář	archivář	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Ulrich	Ulrich	k1gMnSc1
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
advokát	advokát	k1gMnSc1
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
básní	báseň	k1gFnPc2
a	a	k8xC
prózy	próza	k1gFnSc2
<g/>
,	,	kIx,
dlouholetý	dlouholetý	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
města	město	k1gNnSc2
</s>
<s>
Petr	Petr	k1gMnSc1
Ulrych	Ulrych	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
textař	textař	k1gMnSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
zpěvačky	zpěvačka	k1gFnSc2
Hany	Hana	k1gFnSc2
Ulrychové	Ulrychová	k1gFnSc2
</s>
<s>
Emil	Emil	k1gMnSc1
Vachek	Vachek	k1gMnSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
prozaik	prozaik	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
a	a	k8xC
novinář	novinář	k1gMnSc1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Eduardem	Eduard	k1gMnSc7
Fikerem	Fiker	k1gMnSc7
zakladatel	zakladatel	k1gMnSc1
české	český	k2eAgFnPc4d1
detektivky	detektivka	k1gFnPc4
<g/>
,	,	kIx,
tvůrce	tvůrce	k1gMnSc1
prvního	první	k4xOgInSc2
ryze	ryze	k6eAd1
českého	český	k2eAgMnSc2d1
detektiva	detektiv	k1gMnSc2
Klubíčka	klubíčko	k1gNnSc2
</s>
<s>
Otakar	Otakar	k1gMnSc1
Vávra	Vávra	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
,	,	kIx,
scenárista	scenárista	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Hradce	Hradec	k1gInSc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
europoslanec	europoslanec	k1gMnSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Výborná	výborný	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
televizní	televizní	k2eAgFnSc1d1
a	a	k8xC
rozhlasová	rozhlasový	k2eAgFnSc1d1
moderátorka	moderátorka	k1gFnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Vydra	Vydra	k1gMnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1804	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
a	a	k8xC
národní	národní	k2eAgMnSc1d1
buditel	buditel	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Ziegler	Ziegler	k1gMnSc1
(	(	kIx(
<g/>
1782	#num#	k4
<g/>
–	–	k?
<g/>
1846	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
a	a	k8xC
vlastenecký	vlastenecký	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Zounar	Zounar	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
moderátor	moderátor	k1gMnSc1
a	a	k8xC
herec	herec	k1gMnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Alessandria	Alessandrium	k1gNnPc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Arnhem	Arnh	k1gInSc7
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc4
</s>
<s>
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Černihiv	Černihiv	k6eAd1
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Gießen	Gießen	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Kaštela	Kaštela	k1gFnSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Metz	Metz	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Vratislav	Vratislav	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Wałbrzych	Wałbrzych	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Kronštadt	Kronštadt	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spolupracující	spolupracující	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Carovigno	Carovigno	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Montana	Montana	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Székesfehérvár	Székesfehérvár	k1gInSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Pošty	pošta	k1gFnSc2
a	a	k8xC
PSČ	PSČ	kA
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pošty	pošta	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rozpočet	rozpočet	k1gInSc1
města	město	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2015-01-07	2015-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
E.	E.	kA
Poche	Poch	k1gFnSc2
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Čech	Čechy	k1gFnPc2
I.	I.	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
460.1	460.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
LUTTERER	LUTTERER	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
MAJTÁN	MAJTÁN	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
ŠRÁMEK	Šrámek	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisná	zeměpisný	k2eAgFnSc1d1
jména	jméno	k1gNnPc4
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
vybraných	vybraný	k2eAgNnPc2d1
zeměpisných	zeměpisný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
s	s	k7c7
výkladem	výklad	k1gInSc7
jejich	jejich	k3xOp3gInSc2
původu	původ	k1gInSc2
a	a	k8xC
historického	historický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
376	#num#	k4
s.	s.	k?
S.	S.	kA
115	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠEBKOVÁ	Šebková	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
ŽLNAYOVÁ	ŽLNAYOVÁ	kA
<g/>
,	,	kIx,
Edita	Edita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Romaňi	Romaňi	k1gNnSc1
čhib	čhiba	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubice	Pardubice	k1gInPc4
<g/>
:	:	kIx,
Fortuna	Fortuna	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7168	#num#	k4
<g/>
-	-	kIx~
<g/>
684	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
194	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gInSc1
Weather	Weathra	k1gFnPc2
Information	Information	k1gInSc1
Service	Service	k1gFnSc1
–	–	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Nations	Nations	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
1949	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Strategie	strategie	k1gFnSc2
integrované	integrovaný	k2eAgFnSc2d1
územní	územní	k2eAgFnSc2d1
investice	investice	k1gFnSc2
Hradecko-pardubické	hradecko-pardubický	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
investic	investice	k1gFnPc2
<g/>
,	,	kIx,
rozvoje	rozvoj	k1gInSc2
a	a	k8xC
inovací	inovace	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Malý	malý	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013-12-16	2013-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
Republice	republika	k1gFnSc6
československé	československý	k2eAgFnSc6d1
1921	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
I.	I.	kA
Země	země	k1gFnSc1
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
.	.	kIx.
598	#num#	k4
s.	s.	k?
S.	S.	kA
117	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
Republice	republika	k1gFnSc6
československé	československý	k2eAgFnSc6d1
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
I.	I.	kA
Země	země	k1gFnSc1
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
613	#num#	k4
s.	s.	k?
S.	S.	kA
83	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KÁRNÍK	Kárník	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
v	v	k7c6
éře	éra	k1gFnSc6
První	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
třetí	třetí	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
přežití	přežití	k1gNnSc4
a	a	k8xC
o	o	k7c4
život	život	k1gInSc4
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
119	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
120	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
I.	I.	kA
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
25	#num#	k4
<g/>
/	/	kIx~
<g/>
1943	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
změnách	změna	k1gFnPc6
úředních	úřední	k2eAgInPc2d1
názvů	název	k1gInPc2
obcí	obec	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
částí	část	k1gFnPc2
<g/>
,	,	kIx,
povolených	povolený	k2eAgFnPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Komise	komise	k1gFnSc2
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2015-10-06	2015-10-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zastávkové	zastávkový	k2eAgInPc4d1
jízdní	jízdní	k2eAgInPc4d1
řády	řád	k1gInPc4
|	|	kIx~
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
www.dpmhk.cz	www.dpmhk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠMÍDOVÁ	Šmídová	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
:	:	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
má	mít	k5eAaImIp3nS
nové	nový	k2eAgNnSc1d1
partnerské	partnerský	k2eAgNnSc1d1
město	město	k1gNnSc1
Kronštadt	Kronštadt	k1gInSc1
-	-	kIx~
článek	článek	k1gInSc1
na	na	k7c6
oficiálním	oficiální	k2eAgInSc6d1
webu	web	k1gInSc6
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
860	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
504	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEK	Tomek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Vladivoj	Vladivoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místopisné	místopisný	k2eAgFnPc4d1
paměti	paměť	k1gFnPc4
města	město	k1gNnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1885	#num#	k4
<g/>
.	.	kIx.
60	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
E.	E.	kA
Poche	Poche	k1gFnSc1
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Čech	Čechy	k1gFnPc2
I.	I.	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
450	#num#	k4
<g/>
–	–	k?
<g/>
462	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Funerální	Funerální	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
hlavní	hlavní	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
</s>
<s>
Hradubice	Hradubice	k1gFnSc1
</s>
<s>
Votrok	Votrok	k1gInSc1
</s>
<s>
Mosty	most	k1gInPc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
</s>
<s>
Koupaliště	koupaliště	k1gNnSc1
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
obchodních	obchodní	k2eAgNnPc2d1
center	centrum	k1gNnPc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
současných	současný	k2eAgInPc2d1
názvů	název	k1gInPc2
ulic	ulice	k1gFnPc2
a	a	k8xC
veřejných	veřejný	k2eAgNnPc2d1
prostranství	prostranství	k1gNnPc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
http://www.hradeckralove.org	http://www.hradeckralove.org	k1gInSc1
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Magistrátu	magistrát	k1gInSc2
města	město	k1gNnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Edice	edice	k1gFnSc1
Radnice	radnice	k1gFnSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
průvodce	průvodce	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
–	–	k?
turistické	turistický	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
město	město	k1gNnSc1
na	na	k7c6
dlani	dlaň	k1gFnSc6
–	–	k?
regionální	regionální	k2eAgInSc4d1
portál	portál	k1gInSc4
pro	pro	k7c4
občany	občan	k1gMnPc4
i	i	k8xC
turisty	turist	k1gMnPc4
</s>
<s>
Knihovna	knihovna	k1gFnSc1
města	město	k1gNnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
městské	městský	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Statutární	statutární	k2eAgFnSc1d1
město	město	k1gNnSc4
Hradec	Hradec	k1gInSc1
Králové	Králové	k2eAgFnSc2d1
Místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
a	a	k8xC
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Březhrad	Březhrad	k1gInSc1
•	•	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Nový	nový	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Kluky	kluk	k1gMnPc4
<g/>
)	)	kIx)
•	•	k?
Kukleny	kuklen	k2eAgFnPc4d1
•	•	k?
Malšova	Malšův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Malšovice	Malšovice	k1gFnSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Malšovice	Malšovice	k1gFnSc2
u	u	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Moravské	moravský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Třebeš	Třebeš	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Piletice	Piletika	k1gFnSc6
•	•	k?
Plácky	plácek	k1gInPc4
•	•	k?
Plačice	Plačice	k1gFnSc2
•	•	k?
Plotiště	Plotiště	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Pouchov	Pouchov	k1gInSc1
•	•	k?
Pražské	pražský	k2eAgNnSc1d1
Předměstí	předměstí	k1gNnSc1
•	•	k?
Roudnička	Roudnička	k1gFnSc1
•	•	k?
Rusek	Ruska	k1gFnPc2
•	•	k?
Slatina	slatina	k1gFnSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Slatina	slatina	k1gFnSc1
u	u	k7c2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Slezské	Slezská	k1gFnSc2
Předměstí	předměstí	k1gNnSc2
•	•	k?
Svinary	Svinar	k1gInPc1
•	•	k?
Svobodné	svobodný	k2eAgInPc1d1
Dvory	Dvůr	k1gInPc1
•	•	k?
Třebeš	Třebeš	k1gFnSc2
•	•	k?
Věkoše	Věkoš	k1gMnSc2
Seznam	seznam	k1gInSc4
částí	část	k1gFnSc7
města	město	k1gNnSc2
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Babice	babice	k1gFnSc1
•	•	k?
Barchov	Barchov	k1gInSc1
•	•	k?
Běleč	Běleč	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Benátky	Benátky	k1gFnPc4
•	•	k?
Blešno	Blešno	k1gNnSc1
•	•	k?
Boharyně	Boharyně	k1gFnSc2
•	•	k?
Černilov	Černilov	k1gInSc1
•	•	k?
Černožice	Černožice	k1gFnSc2
•	•	k?
Čistěves	Čistěves	k1gMnSc1
•	•	k?
Divec	Divec	k1gMnSc1
•	•	k?
Dobřenice	Dobřenice	k1gFnSc2
•	•	k?
Dohalice	Dohalice	k1gFnSc2
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Přím	přít	k5eAaImIp1nS
•	•	k?
Habřina	habřina	k1gFnSc1
•	•	k?
Hlušice	Hlušice	k1gFnSc2
•	•	k?
Hněvčeves	Hněvčeves	k1gMnSc1
•	•	k?
Holohlavy	Holohlavy	k?
•	•	k?
Hořiněves	Hořiněves	k1gInSc1
•	•	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Humburky	Humburka	k1gFnSc2
•	•	k?
Hvozdnice	Hvozdnice	k1gFnSc2
•	•	k?
Chlumec	Chlumec	k1gInSc1
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Chudeřice	Chudeřice	k1gFnSc2
•	•	k?
Jeníkovice	Jeníkovice	k1gFnSc2
•	•	k?
Jílovice	Jílovice	k1gFnSc2
•	•	k?
Káranice	Káranice	k1gFnSc2
•	•	k?
Klamoš	Klamoš	k1gMnSc1
•	•	k?
Kobylice	kobylice	k1gFnSc2
•	•	k?
Kosice	kosice	k1gFnSc2
•	•	k?
Kosičky	kosička	k1gFnSc2
•	•	k?
Králíky	Králík	k1gMnPc4
•	•	k?
Kratonohy	Kratonoh	k1gInPc1
•	•	k?
Kunčice	Kunčice	k1gFnPc4
•	•	k?
Ledce	Ledce	k1gMnSc1
•	•	k?
Lejšovka	Lejšovka	k1gFnSc1
•	•	k?
Lhota	Lhota	k1gFnSc1
pod	pod	k7c7
Libčany	Libčan	k1gMnPc7
•	•	k?
Libčany	Libčan	k1gMnPc4
•	•	k?
Libníkovice	Libníkovice	k1gFnSc1
•	•	k?
Librantice	Librantice	k1gFnSc2
•	•	k?
Libřice	Libřice	k1gFnSc2
•	•	k?
Lišice	Lišice	k1gFnSc2
•	•	k?
Lodín	Lodín	k1gMnSc1
•	•	k?
Lochenice	Lochenice	k1gFnSc2
•	•	k?
Lovčice	Lovčice	k1gFnSc2
•	•	k?
Lužany	Lužana	k1gFnSc2
•	•	k?
Lužec	Lužec	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
nad	nad	k7c7
Cidlinou	Cidlina	k1gFnSc7
•	•	k?
Máslojedy	Máslojeda	k1gMnSc2
•	•	k?
Měník	Měník	k1gMnSc1
•	•	k?
Mlékosrby	Mlékosrba	k1gFnSc2
•	•	k?
Mokrovousy	Mokrovous	k1gInPc1
•	•	k?
Myštěves	Myštěves	k1gInSc1
•	•	k?
Mžany	Mžana	k1gFnSc2
•	•	k?
Neděliště	Neděliště	k1gNnSc1
•	•	k?
Nechanice	Nechanice	k1gFnPc4
•	•	k?
Nepolisy	Nepolis	k1gInPc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Bydžov	Bydžov	k1gInSc1
•	•	k?
Obědovice	Obědovice	k1gFnSc2
•	•	k?
Ohnišťany	Ohnišťan	k1gMnPc4
•	•	k?
Olešnice	Olešnice	k1gFnSc1
•	•	k?
Osice	osika	k1gFnSc6
•	•	k?
Osičky	osička	k1gFnSc2
•	•	k?
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Písek	Písek	k1gInSc1
•	•	k?
Prasek	Prasek	k1gInSc1
•	•	k?
Praskačka	Praskačka	k1gFnSc1
•	•	k?
Předměřice	Předměřice	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Převýšov	Převýšovo	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Pšánky	Pšánek	k1gInPc1
•	•	k?
Puchlovice	Puchlovice	k1gFnSc2
•	•	k?
Račice	račice	k1gFnSc1
nad	nad	k7c7
Trotinou	Trotina	k1gFnSc7
•	•	k?
Radíkovice	Radíkovice	k1gFnSc2
•	•	k?
Radostov	Radostov	k1gInSc1
•	•	k?
Roudnice	Roudnice	k1gFnSc2
•	•	k?
Sadová	sadový	k2eAgFnSc1d1
•	•	k?
Sendražice	Sendražice	k1gFnSc1
•	•	k?
Skalice	Skalice	k1gFnSc2
•	•	k?
Skřivany	Skřivan	k1gMnPc7
•	•	k?
Sloupno	Sloupno	k6eAd1
•	•	k?
Smidary	Smidara	k1gFnPc4
•	•	k?
Smiřice	Smiřice	k1gFnPc4
•	•	k?
Smržov	Smržov	k1gInSc1
•	•	k?
Sovětice	Sovětice	k1gFnSc2
•	•	k?
Stará	starý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
•	•	k?
Starý	starý	k2eAgInSc1d1
Bydžov	Bydžov	k1gInSc1
•	•	k?
Stěžery	Stěžera	k1gFnSc2
•	•	k?
Stračov	Stračov	k1gInSc1
•	•	k?
Střezetice	Střezetika	k1gFnSc6
•	•	k?
Světí	světit	k5eAaImIp3nS
•	•	k?
Syrovátka	syrovátka	k1gFnSc1
•	•	k?
Šaplava	Šaplava	k1gFnSc1
•	•	k?
Těchlovice	Těchlovice	k1gFnPc4
•	•	k?
Třebechovice	Třebechovice	k1gFnPc4
pod	pod	k7c7
Orebem	Oreb	k1gInSc7
•	•	k?
Třesovice	Třesovice	k1gFnSc2
•	•	k?
Urbanice	Urbanice	k1gFnSc2
•	•	k?
Vinary	Vinara	k1gFnSc2
•	•	k?
Vrchovnice	Vrchovnice	k1gFnSc2
•	•	k?
Všestary	Všestara	k1gFnSc2
•	•	k?
Výrava	Výrava	k1gFnSc1
•	•	k?
Vysoká	vysoká	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Zachrašťany	Zachrašťan	k1gMnPc4
•	•	k?
Zdechovice	Zdechovice	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Česka	Česko	k1gNnSc2
Samosprávné	samosprávný	k2eAgInPc1d1
kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
od	od	k7c2
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Praha	Praha	k1gFnSc1
•	•	k?
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jihočeský	jihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
(	(	kIx(
<g/>
Jihlava	Jihlava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Zlín	Zlín	k1gInSc1
<g/>
)	)	kIx)
Regiony	region	k1gInPc1
soudržnosti	soudržnost	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
•	•	k?
Střední	střední	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
•	•	k?
Jihozápad	jihozápad	k1gInSc1
•	•	k?
Severozápad	severozápad	k1gInSc1
•	•	k?
Severovýchod	severovýchod	k1gInSc1
•	•	k?
Jihovýchod	jihovýchod	k1gInSc1
•	•	k?
Střední	střední	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
•	•	k?
Moravskoslezsko	Moravskoslezsko	k1gNnSc1
Územní	územní	k2eAgInPc1d1
kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
samostatná	samostatný	k2eAgFnSc1d1
územní	územní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jihočeský	jihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Západočeský	západočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Severočeský	severočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Východočeský	východočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Severomoravský	severomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Statutární	statutární	k2eAgNnPc1d1
města	město	k1gNnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
Liberec	Liberec	k1gInSc1
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
70	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Zlín	Zlín	k1gInSc1
30	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Děčín	Děčín	k1gInSc1
</s>
<s>
Frýdek-Místek	Frýdek-Místek	k1gInSc1
</s>
<s>
Chomutov	Chomutov	k1gInSc1
</s>
<s>
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
Karviná	Karviná	k1gFnSc1
</s>
<s>
Kladno	Kladno	k1gNnSc1
</s>
<s>
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
Most	most	k1gInSc1
</s>
<s>
Opava	Opava	k1gFnSc1
</s>
<s>
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Přerov	Přerov	k1gInSc1
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Primátoři	primátor	k1gMnPc1
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
</s>
<s>
1990	#num#	k4
Josef	Josef	k1gMnSc1
Potoček	Potočka	k1gFnPc2
•	•	k?
1990	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
Martin	Martin	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
•	•	k?
1998	#num#	k4
Jan	Jan	k1gMnSc1
Doskočil	Doskočil	k1gMnSc1
•	•	k?
1998	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
Oldřich	Oldřich	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
•	•	k?
2004	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
Otakar	Otakara	k1gFnPc2
Divíšek	Divíšek	k1gInSc4
•	•	k?
2010	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
Zdeněk	Zdeňka	k1gFnPc2
Fink	Finka	k1gFnPc2
•	•	k?
2018	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Alexandr	Alexandr	k1gMnSc1
Hrabálek	Hrabálek	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
129332	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1106410947	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81022922	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131355332	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81022922	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
