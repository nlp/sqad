<s>
Vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
VPD	VPD	kA
<g/>
;	;	kIx,
anglicky	anglicky	k6eAd1
a	a	k8xC
často	často	k6eAd1
také	také	k9
runway	runway	k1gFnSc1
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
ranvej	ranvej	k1gInSc1
<g/>
,	,	kIx,
zkracováno	zkracován	k2eAgNnSc1d1
RWY	RWY	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
plocha	plocha	k1gFnSc1
sloužící	sloužící	k2eAgFnSc1d1
ke	k	k7c3
vzletům	vzlet	k1gInPc3
a	a	k8xC
přistáním	přistání	k1gNnPc3
letadel	letadlo	k1gNnPc2
na	na	k7c6
letištích	letiště	k1gNnPc6
<g/>
.	.	kIx.
</s>