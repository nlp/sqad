<s>
Vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
30	#num#	k4
letiště	letiště	k1gNnSc4
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
VPD	VPD	kA
<g/>
;	;	kIx,
anglicky	anglicky	k6eAd1
a	a	k8xC
často	často	k6eAd1
také	také	k9
runway	runway	k1gFnSc1
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
ranvej	ranvej	k1gInSc1
<g/>
,	,	kIx,
zkracováno	zkracován	k2eAgNnSc1d1
RWY	RWY	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
plocha	plocha	k1gFnSc1
sloužící	sloužící	k2eAgFnSc1d1
ke	k	k7c3
vzletům	vzlet	k1gInPc3
a	a	k8xC
přistáním	přistání	k1gNnPc3
letadel	letadlo	k1gNnPc2
na	na	k7c6
letištích	letiště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dráha	dráha	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zpevněná	zpevněný	k2eAgFnSc1d1
(	(	kIx(
<g/>
beton	beton	k1gInSc1
<g/>
,	,	kIx,
asfalt	asfalt	k1gInSc1
<g/>
,	,	kIx,
asfaltobeton	asfaltobeton	k1gInSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
nezpevněná	zpevněný	k2eNgFnSc1d1
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
travnatá	travnatý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1
drah	draha	k1gFnPc2
</s>
<s>
Dráhy	dráha	k1gFnPc1
se	se	k3xPyFc4
pojmenovávají	pojmenovávat	k5eAaImIp3nP
číselným	číselný	k2eAgNnSc7d1
označením	označení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
udává	udávat	k5eAaImIp3nS
směr	směr	k1gInSc4
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
rovno	roven	k2eAgNnSc4d1
magnetickému	magnetický	k2eAgInSc3d1
směru	směr	k1gInSc6
dráhy	dráha	k1gFnPc1
zaokrouhlenému	zaokrouhlený	k2eAgInSc3d1
na	na	k7c4
nejbližších	blízký	k2eAgInPc2d3
deset	deset	k4xCc4
stupňů	stupeň	k1gInPc2
a	a	k8xC
poté	poté	k6eAd1
vydělenému	vydělený	k2eAgNnSc3d1
deseti	deset	k4xCc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
dráha	dráha	k1gFnSc1
s	s	k7c7
označením	označení	k1gNnSc7
„	„	k?
<g/>
24	#num#	k4
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
směr	směr	k1gInSc1
přibližně	přibližně	k6eAd1
240	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
tzn.	tzn.	kA
zhruba	zhruba	k6eAd1
jihozápadně	jihozápadně	k6eAd1
<g/>
,	,	kIx,
dráha	dráha	k1gFnSc1
„	„	k?
<g/>
36	#num#	k4
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
směr	směr	k1gInSc4
360	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
tzn.	tzn.	kA
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
dráhu	dráha	k1gFnSc4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
využít	využít	k5eAaPmF
dvěma	dva	k4xCgInPc7
směry	směr	k1gInPc7
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
také	také	k9
dvojí	dvojí	k4xRgNnSc4
označení	označení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
číselně	číselně	k6eAd1
liší	lišit	k5eAaImIp3nS
o	o	k7c4
18	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
180	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dráha	dráha	k1gFnSc1
„	„	k?
<g/>
24	#num#	k4
<g/>
“	“	k?
tak	tak	k6eAd1
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
směru	směr	k1gInSc6
nese	nést	k5eAaImIp3nS
označení	označení	k1gNnSc1
„	„	k?
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
“	“	k?
(	(	kIx(
<g/>
úvodní	úvodní	k2eAgFnSc1d1
nula	nula	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
prevenci	prevence	k1gFnSc4
přeslechů	přeslech	k1gInPc2
v	v	k7c6
radiokomunikaci	radiokomunikace	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dráha	dráha	k1gFnSc1
jako	jako	k8xC,k8xS
celek	celek	k1gInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
označovat	označovat	k5eAaImF
např.	např.	kA
„	„	k?
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
na	na	k7c6
letišti	letiště	k1gNnSc6
více	hodně	k6eAd2
rovnoběžných	rovnoběžný	k2eAgInPc2d1
(	(	kIx(
<g/>
nebo	nebo	k8xC
téměř	téměř	k6eAd1
rovnoběžných	rovnoběžný	k2eAgFnPc2d1
<g/>
)	)	kIx)
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
rozlišují	rozlišovat	k5eAaImIp3nP
se	s	k7c7
přidaným	přidaný	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
L	L	kA
(	(	kIx(
<g/>
levá	levý	k2eAgFnSc1d1
<g/>
,	,	kIx,
left	left	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
R	R	kA
(	(	kIx(
<g/>
pravá	pravý	k2eAgFnSc1d1
<g/>
,	,	kIx,
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
C	C	kA
(	(	kIx(
<g/>
prostřední	prostřední	k2eAgFnSc4d1
<g/>
,	,	kIx,
center	centrum	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
dráha	dráha	k1gFnSc1
24L	24L	k4
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
směru	směr	k1gInSc6
označení	označení	k1gNnSc1
0	#num#	k4
<g/>
6	#num#	k4
<g/>
R.	R.	kA
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
letiště	letiště	k1gNnSc4
ještě	ještě	k9
více	hodně	k6eAd2
než	než	k8xS
tři	tři	k4xCgFnPc1
takové	takový	k3xDgFnPc1
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
použije	použít	k5eAaPmIp3nS
se	se	k3xPyFc4
další	další	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
a	a	k8xC
číslo	číslo	k1gNnSc1
pak	pak	k6eAd1
přesně	přesně	k6eAd1
neodpovídá	odpovídat	k5eNaImIp3nS
magnetickému	magnetický	k2eAgInSc3d1
směru	směr	k1gInSc3
dráhy	dráha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
na	na	k7c6
Losangeleském	losangeleský	k2eAgNnSc6d1
mezinárodním	mezinárodní	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
existují	existovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc1
rovnoběžné	rovnoběžný	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
značené	značený	k2eAgFnPc1d1
6	#num#	k4
<g/>
L	L	kA
<g/>
,	,	kIx,
6	#num#	k4
<g/>
R	R	kA
<g/>
,	,	kIx,
7	#num#	k4
<g/>
L	L	kA
<g/>
,	,	kIx,
7R	7R	k4
(	(	kIx(
<g/>
a	a	k8xC
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
směru	směr	k1gInSc6
24	#num#	k4
<g/>
R	R	kA
<g/>
,	,	kIx,
24	#num#	k4
<g/>
L	L	kA
<g/>
,	,	kIx,
25	#num#	k4
<g/>
R	R	kA
<g/>
,	,	kIx,
25	#num#	k4
<g/>
L	L	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přestože	přestože	k8xS
mají	mít	k5eAaImIp3nP
naprosto	naprosto	k6eAd1
shodný	shodný	k2eAgInSc4d1
směr	směr	k1gInSc4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
69,1	69,1	k4
<g/>
°	°	k?
<g/>
/	/	kIx~
<g/>
249,1	249,1	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
na	na	k7c6
tuřanském	tuřanský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
betonová	betonový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
označena	označit	k5eAaPmNgFnS
RWY	RWY	kA
0	#num#	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
sousední	sousední	k2eAgInSc1d1
rovnoběžný	rovnoběžný	k2eAgInSc1d1
travnatý	travnatý	k2eAgInSc1d1
pás	pás	k1gInSc1
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
RWY	RWY	kA
0	#num#	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dráha	dráha	k1gFnSc1
22	#num#	k4
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
fluktuaci	fluktuace	k1gFnSc3
zemského	zemský	k2eAgInSc2d1
severního	severní	k2eAgInSc2d1
magnetického	magnetický	k2eAgInSc2d1
pólu	pól	k1gInSc2
dochází	docházet	k5eAaImIp3nS
občas	občas	k6eAd1
k	k	k7c3
přeznačování	přeznačování	k1gNnSc3
drah	draha	k1gFnPc2
<g/>
;	;	kIx,
Například	například	k6eAd1
dnešní	dnešní	k2eAgFnSc1d1
ruzyňská	ruzyňský	k2eAgFnSc1d1
RWY	RWY	kA
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
nesla	nést	k5eAaImAgFnS
dříve	dříve	k6eAd2
označení	označení	k1gNnSc4
0	#num#	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volba	volba	k1gFnSc1
používané	používaný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
letiště	letiště	k1gNnSc2
Johna	John	k1gMnSc2
F.	F.	kA
Kennedyho	Kennedy	k1gMnSc2
se	se	k3xPyFc4
čtyřmi	čtyři	k4xCgNnPc7
VPD	VPD	kA
</s>
<s>
Jako	jako	k8xC,k8xS
dráha	dráha	k1gFnSc1
v	v	k7c6
používání	používání	k1gNnSc6
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
jsou	být	k5eAaImIp3nP
v	v	k7c4
danou	daný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
prováděny	prováděn	k2eAgInPc4d1
vzlety	vzlet	k1gInPc4
a	a	k8xC
přistání	přistání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
často	často	k6eAd1
některé	některý	k3yIgFnPc1
dráhy	dráha	k1gFnPc1
křižují	křižovat	k5eAaImIp3nP
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
současně	současně	k6eAd1
používat	používat	k5eAaImF
všechny	všechen	k3xTgFnPc4
dráhy	dráha	k1gFnPc4
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
je	být	k5eAaImIp3nS
letiště	letiště	k1gNnSc1
vybaveno	vybaven	k2eAgNnSc1d1
(	(	kIx(
<g/>
a	a	k8xC
i	i	k9
pokud	pokud	k8xS
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
dráhy	dráha	k1gFnPc1
nekřižují	křižovat	k5eNaImIp3nP
<g/>
,	,	kIx,
mohla	moct	k5eAaImAgFnS
by	by	kYmCp3nP
si	se	k3xPyFc3
navzájem	navzájem	k6eAd1
překážet	překážet	k5eAaImF
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
právě	právě	k6eAd1
přibližují	přibližovat	k5eAaImIp3nP
na	na	k7c4
přistání	přistání	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
odlétají	odlétat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgNnSc7d1
kritériem	kritérion	k1gNnSc7
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
se	se	k3xPyFc4
vybírá	vybírat	k5eAaImIp3nS
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
používat	používat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vítr	vítr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzlety	vzlet	k1gInPc4
i	i	k8xC
přistání	přistání	k1gNnSc4
je	být	k5eAaImIp3nS
totiž	totiž	k9
nejvhodnější	vhodný	k2eAgNnSc1d3
provádět	provádět	k5eAaImF
proti	proti	k7c3
větru	vítr	k1gInSc3
<g/>
;	;	kIx,
vítr	vítr	k1gInSc1
z	z	k7c2
boku	bok	k1gInSc2
či	či	k8xC
do	do	k7c2
zad	záda	k1gNnPc2
je	být	k5eAaImIp3nS
nežádoucí	žádoucí	k2eNgFnSc1d1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
udržení	udržení	k1gNnSc4
se	se	k3xPyFc4
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
se	se	k3xPyFc4
letoun	letoun	k1gInSc1
musí	muset	k5eAaImIp3nS
pohybovat	pohybovat	k5eAaImF
určitou	určitý	k2eAgFnSc7d1
minimální	minimální	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
vůči	vůči	k7c3
okolnímu	okolní	k2eAgInSc3d1
vzduchu	vzduch	k1gInSc3
<g/>
,	,	kIx,
tzn.	tzn.	kA
pokud	pokud	k8xS
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
vzduch	vzduch	k1gInSc1
pohybuje	pohybovat	k5eAaImIp3nS
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
směru	směr	k1gInSc6
jako	jako	k8xS,k8xC
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výsledná	výsledný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
vůči	vůči	k7c3
zemi	zem	k1gFnSc3
o	o	k7c4
to	ten	k3xDgNnSc4
vyšší	vysoký	k2eAgMnSc1d2
<g/>
,	,	kIx,
takže	takže	k8xS
při	při	k7c6
vzletu	vzlet	k1gInSc6
musí	muset	k5eAaImIp3nS
letoun	letoun	k1gInSc1
před	před	k7c7
odlepením	odlepení	k1gNnSc7
od	od	k7c2
země	zem	k1gFnSc2
tuto	tento	k3xDgFnSc4
vyšší	vysoký	k2eAgFnSc4d2
rychlost	rychlost	k1gFnSc4
vyvinout	vyvinout	k5eAaPmF
<g/>
,	,	kIx,
případně	případně	k6eAd1
z	z	k7c2
ní	on	k3xPp3gFnSc2
při	při	k7c6
přistávání	přistávání	k1gNnSc6
zabrzdit	zabrzdit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
při	při	k7c6
protivětru	protivítr	k1gInSc6
je	být	k5eAaImIp3nS
potřebná	potřebný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
vůči	vůči	k7c3
zemi	zem	k1gFnSc3
o	o	k7c4
rychlost	rychlost	k1gFnSc4
větru	vítr	k1gInSc2
nižší	nízký	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Kromě	kromě	k7c2
tohoto	tento	k3xDgNnSc2
kritéria	kritérion	k1gNnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc1d1
(	(	kIx(
<g/>
používaná	používaný	k2eAgNnPc1d1
zvláště	zvláště	k6eAd1
za	za	k7c2
bezvětří	bezvětří	k1gNnSc2
či	či	k8xC
jen	jen	k9
mírného	mírný	k2eAgInSc2d1
větru	vítr	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
kvůli	kvůli	k7c3
omezení	omezení	k1gNnSc3
hluku	hluk	k1gInSc2
se	se	k3xPyFc4
volí	volit	k5eAaImIp3nS
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
u	u	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
nemusí	muset	k5eNaImIp3nS
odlétající	odlétající	k2eAgFnSc1d1
a	a	k8xC
přistávající	přistávající	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
přelétat	přelétat	k5eAaPmF,k5eAaImF
nad	nad	k7c7
obydlenými	obydlený	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
(	(	kIx(
<g/>
to	ten	k3xDgNnSc4
je	být	k5eAaImIp3nS
časté	častý	k2eAgNnSc1d1
kritérium	kritérium	k1gNnSc1
zvláště	zvláště	k6eAd1
v	v	k7c6
nočních	noční	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
zvažovat	zvažovat	k5eAaImF
argumenty	argument	k1gInPc4
pro	pro	k7c4
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
nejjednodušší	jednoduchý	k2eAgNnSc1d3
a	a	k8xC
nejrychlejší	rychlý	k2eAgNnSc1d3
odbavení	odbavení	k1gNnSc1
co	co	k9
nejvyššího	vysoký	k2eAgInSc2d3
počtu	počet	k1gInSc2
letadel	letadlo	k1gNnPc2
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
převažující	převažující	k2eAgInSc4d1
směr	směr	k1gInSc4
dopravy	doprava	k1gFnSc2
či	či	k8xC
navazující	navazující	k2eAgInPc1d1
letové	letový	k2eAgInPc1d1
trasy	tras	k1gInPc1
(	(	kIx(
<g/>
tento	tento	k3xDgInSc1
argument	argument	k1gInSc1
se	se	k3xPyFc4
zase	zase	k9
nejčastěji	často	k6eAd3
aplikuje	aplikovat	k5eAaBmIp3nS
v	v	k7c6
době	doba	k1gFnSc6
dopravní	dopravní	k2eAgFnSc2d1
špičky	špička	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
Na	na	k7c6
malých	malý	k2eAgNnPc6d1
letištích	letiště	k1gNnPc6
pro	pro	k7c4
všeobecné	všeobecný	k2eAgNnSc4d1
letectví	letectví	k1gNnSc4
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
malé	malý	k2eAgFnPc1d1
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
nezpevněné	zpevněný	k2eNgFnPc1d1
<g/>
)	)	kIx)
dráhy	dráha	k1gFnPc1
s	s	k7c7
délkou	délka	k1gFnSc7
několika	několik	k4yIc2
set	set	k1gInSc4
m	m	kA
<g/>
,	,	kIx,
nejmenší	malý	k2eAgMnSc1d3
zhruba	zhruba	k6eAd1
250	#num#	k4
<g/>
×	×	k?
<g/>
8	#num#	k4
m.	m.	k?
Velká	velká	k1gFnSc1
letiště	letiště	k1gNnSc2
pak	pak	k6eAd1
používají	používat	k5eAaImIp3nP
zpevněné	zpevněný	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
o	o	k7c6
šířce	šířka	k1gFnSc6
typicky	typicky	k6eAd1
45	#num#	k4
<g/>
,	,	kIx,
60	#num#	k4
nebo	nebo	k8xC
80	#num#	k4
m	m	kA
a	a	k8xC
délce	délka	k1gFnSc6
v	v	k7c6
rozsahu	rozsah	k1gInSc6
zhruba	zhruba	k6eAd1
2	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgInPc1d1
letouny	letoun	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Boeing	boeing	k1gInSc4
747	#num#	k4
<g/>
,	,	kIx,
767	#num#	k4
či	či	k8xC
777	#num#	k4
<g/>
,	,	kIx,
McDonnell	McDonnell	k1gMnSc1
Douglas	Douglas	k1gMnSc1
DC-10	DC-10	k1gMnSc1
či	či	k8xC
MD-	MD-	k1gMnSc1
<g/>
11	#num#	k4
<g/>
,	,	kIx,
Airbus	airbus	k1gInSc1
A340	A340	k1gMnPc2
či	či	k8xC
A380	A380	k1gMnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
vyžadují	vyžadovat	k5eAaImIp3nP
dráhu	dráha	k1gFnSc4
zhruba	zhruba	k6eAd1
2400	#num#	k4
m	m	kA
(	(	kIx(
<g/>
minimum	minimum	k1gNnSc4
za	za	k7c2
příznivých	příznivý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
při	při	k7c6
hladině	hladina	k1gFnSc6
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
vyšší	vysoký	k2eAgFnSc6d2
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
či	či	k8xC
za	za	k7c2
zhoršených	zhoršený	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
více	hodně	k6eAd2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc4d3
dráhu	dráha	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
délkou	délka	k1gFnSc7
5	#num#	k4
000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
letiště	letiště	k1gNnSc2
v	v	k7c6
Žikace	Žikace	k1gFnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
89	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
v	v	k7c6
Tibetské	tibetský	k2eAgFnSc6d1
AO	AO	kA
ČLR	ČLR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
výrazně	výrazně	k6eAd1
delší	dlouhý	k2eAgInPc1d2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
specializované	specializovaný	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
mimo	mimo	k7c4
běžná	běžný	k2eAgNnPc4d1
letiště	letiště	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
nezpevněná	zpevněný	k2eNgFnSc1d1
dráha	dráha	k1gFnSc1
18	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
vyznačená	vyznačený	k2eAgFnSc1d1
na	na	k7c6
dně	dno	k1gNnSc6
solné	solný	k2eAgFnSc2d1
pláně	pláň	k1gFnSc2
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
v	v	k7c6
USA	USA	kA
má	mít	k5eAaImIp3nS
délku	délka	k1gFnSc4
cca	cca	kA
12	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdelší	dlouhý	k2eAgFnSc7d3
dráhou	dráha	k1gFnSc7
pražského	pražský	k2eAgNnSc2d1
ruzyňského	ruzyňský	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
je	být	k5eAaImIp3nS
dráha	dráha	k1gFnSc1
0	#num#	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
s	s	k7c7
rozměry	rozměr	k1gInPc7
3715	#num#	k4
<g/>
×	×	k?
<g/>
45	#num#	k4
m.	m.	k?
</s>
<s>
Pro	pro	k7c4
případ	případ	k1gInSc4
nehody	nehoda	k1gFnSc2
jsou	být	k5eAaImIp3nP
u	u	k7c2
velkých	velký	k2eAgNnPc2d1
letišť	letiště	k1gNnPc2
zpravidla	zpravidla	k6eAd1
vyžadovány	vyžadován	k2eAgFnPc4d1
bezpečnostní	bezpečnostní	k2eAgFnPc4d1
zóny	zóna	k1gFnPc4
za	za	k7c7
okraji	okraj	k1gInPc7
vzletových	vzletový	k2eAgFnPc2d1
a	a	k8xC
přistávacích	přistávací	k2eAgFnPc2d1
drah	draha	k1gFnPc2
<g/>
,	,	kIx,
někde	někde	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
také	také	k9
speciální	speciální	k2eAgInSc4d1
povrch	povrch	k1gInSc4
pomáhající	pomáhající	k2eAgInSc4d1
v	v	k7c6
nouzi	nouze	k1gFnSc6
zastavit	zastavit	k5eAaPmF
letadlo	letadlo	k1gNnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
EMAS	EMAS	kA
<g/>
,	,	kIx,
Engineered	Engineered	k1gMnSc1
Materials	Materialsa	k1gFnPc2
Arrestor	Arrestor	k1gMnSc1
System	Syst	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vybavení	vybavení	k1gNnSc1
<g/>
,	,	kIx,
osvětlení	osvětlení	k1gNnSc1
a	a	k8xC
značení	značení	k1gNnSc1
</s>
<s>
Jelikož	jelikož	k8xS
vzlet	vzlet	k1gInSc1
a	a	k8xC
přistání	přistání	k1gNnSc2
jsou	být	k5eAaImIp3nP
nejrizikovějšími	rizikový	k2eAgFnPc7d3
částmi	část	k1gFnPc7
letu	let	k1gInSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
vybavení	vybavení	k1gNnSc1
vzletových	vzletový	k2eAgFnPc2d1
a	a	k8xC
přistávacích	přistávací	k2eAgFnPc2d1
drah	draha	k1gFnPc2
zásadní	zásadní	k2eAgFnSc4d1
důležitost	důležitost	k1gFnSc4
pro	pro	k7c4
bezpečnost	bezpečnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Runwaye	runway	k1gInSc2
jsou	být	k5eAaImIp3nP
jednak	jednak	k8xC
vybaveny	vybavit	k5eAaPmNgInP
světelnými	světelný	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
jednak	jednak	k8xC
různými	různý	k2eAgFnPc7d1
radionavigačními	radionavigační	k2eAgFnPc7d1
pomůckami	pomůcka	k1gFnPc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
obojí	obojí	k4xRgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
létání	létání	k1gNnSc4
v	v	k7c6
noci	noc	k1gFnSc6
a	a	k8xC
za	za	k7c2
zhoršené	zhoršený	k2eAgFnSc2d1
viditelnosti	viditelnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šíře	šíře	k1gFnSc1
vybavení	vybavení	k1gNnSc2
dráhy	dráha	k1gFnSc2
samozřejmě	samozřejmě	k6eAd1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
velikosti	velikost	k1gFnSc6
letiště	letiště	k1gNnSc2
<g/>
,	,	kIx,
nejmenší	malý	k2eAgNnPc4d3
letiště	letiště	k1gNnPc4
pro	pro	k7c4
amatérské	amatérský	k2eAgNnSc4d1
létání	létání	k1gNnSc4
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
vybavena	vybavit	k5eAaPmNgFnS
jen	jen	k9
větrným	větrný	k2eAgInSc7d1
rukávem	rukáv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Radionavigační	radionavigační	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
Vysílač	vysílač	k1gInSc1
směrového	směrový	k2eAgInSc2d1
paprsku	paprsek	k1gInSc2
ILS	ILS	kA
a	a	k8xC
světelný	světelný	k2eAgInSc4d1
systém	systém	k1gInSc4
ALS	ALS	kA
na	na	k7c6
americké	americký	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
Whiteman	Whiteman	k1gMnSc1
</s>
<s>
Různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
přístrojových	přístrojový	k2eAgNnPc2d1
a	a	k8xC
vizuálních	vizuální	k2eAgNnPc2d1
přiblížení	přiblížení	k1gNnPc2
vyžadují	vyžadovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
radionavigační	radionavigační	k2eAgFnPc4d1
pomůcky	pomůcka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
alespoň	alespoň	k9
hlavní	hlavní	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
na	na	k7c6
velkých	velký	k2eAgNnPc6d1
letištích	letiště	k1gNnPc6
zpravidla	zpravidla	k6eAd1
vybaveny	vybavit	k5eAaPmNgInP
zařízením	zařízení	k1gNnSc7
ILS	ILS	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
letadlu	letadlo	k1gNnSc3
přesně	přesně	k6eAd1
sledovat	sledovat	k5eAaImF
ideální	ideální	k2eAgFnSc4d1
sestupovou	sestupový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
i	i	k9
při	při	k7c6
minimální	minimální	k2eAgFnSc6d1
viditelnosti	viditelnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílače	vysílač	k1gInSc2
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
jednak	jednak	k8xC
poblíž	poblíž	k7c2
dotykového	dotykový	k2eAgInSc2d1
bodu	bod	k1gInSc2
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
vysílač	vysílač	k1gInSc1
sestupové	sestupový	k2eAgFnSc2d1
roviny	rovina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednak	jednak	k8xC
na	na	k7c6
opačném	opačný	k2eAgInSc6d1
konci	konec	k1gInSc6
dráhy	dráha	k1gFnSc2
v	v	k7c6
její	její	k3xOp3gFnSc6
ose	osa	k1gFnSc6
(	(	kIx(
<g/>
vysílač	vysílač	k1gInSc1
směrového	směrový	k2eAgInSc2d1
paprsku	paprsek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezbytnou	zbytný	k2eNgFnSc7d1,k2eAgFnSc7d1
součástí	součást	k1gFnSc7
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
také	také	k9
zařízení	zařízení	k1gNnSc1
DME	dmout	k5eAaImIp3nS
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
měří	měřit	k5eAaImIp3nP
vzdálenost	vzdálenost	k1gFnSc4
letadla	letadlo	k1gNnSc2
od	od	k7c2
prahu	práh	k1gInSc2
dráhy	dráha	k1gFnSc2
a	a	k8xC
vysílač	vysílač	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
umístěn	umístit	k5eAaPmNgInS
u	u	k7c2
tohoto	tento	k3xDgInSc2
prahu	práh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
k	k	k7c3
systému	systém	k1gInSc3
patří	patřit	k5eAaImIp3nS
2	#num#	k4
-	-	kIx~
3	#num#	k4
radiomajáky	radiomaják	k1gInPc1
umístěné	umístěný	k2eAgInPc1d1
v	v	k7c6
ose	osa	k1gFnSc6
dráhy	dráha	k1gFnSc2
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
vzdálenostech	vzdálenost	k1gFnPc6
před	před	k7c7
prahem	práh	k1gInSc7
(	(	kIx(
<g/>
Outer	Outer	k1gInSc1
marker	marker	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k9
dálná	dálný	k2eAgFnSc1d1
<g/>
)	)	kIx)
typicky	typicky	k6eAd1
4	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
nm	nm	k?
<g/>
,	,	kIx,
Middle	Middle	k1gMnSc1
marker	marker	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
nazývaná	nazývaný	k2eAgFnSc1d1
též	též	k9
bližná	bližný	k2eAgFnSc1d1
<g/>
)	)	kIx)
0,5	0,5	k4
<g/>
–	–	k?
<g/>
0,8	0,8	k4
nm	nm	k?
a	a	k8xC
Inner	Innra	k1gFnPc2
marker	marker	k1gInSc1
těsně	těsně	k6eAd1
před	před	k7c7
dráhou	dráha	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osvětlení	osvětlení	k1gNnSc1
</s>
<s>
ALS	ALS	kA
</s>
<s>
Součástí	součást	k1gFnSc7
vybavení	vybavení	k1gNnSc2
vzletových	vzletový	k2eAgFnPc2d1
a	a	k8xC
přistávacích	přistávací	k2eAgFnPc2d1
drah	draha	k1gFnPc2
je	být	k5eAaImIp3nS
také	také	k9
několik	několik	k4yIc1
soustav	soustava	k1gFnPc2
osvětlení	osvětlení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
před	před	k7c7
přistávací	přistávací	k2eAgFnSc7d1
dráhou	dráha	k1gFnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
její	její	k3xOp3gFnSc6
ose	osa	k1gFnSc6
umístěna	umístěn	k2eAgFnSc1d1
řada	řada	k1gFnSc1
světel	světlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
přistávající	přistávající	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
směrují	směrovat	k5eAaImIp3nP
k	k	k7c3
okraji	okraj	k1gInSc3
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
soustava	soustava	k1gFnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
ALS	ALS	kA
(	(	kIx(
<g/>
Approach	Approach	k1gInSc1
Light	Light	k2eAgInSc1d1
System	Syst	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
řady	řada	k1gFnSc2
trvale	trvale	k6eAd1
svítících	svítící	k2eAgNnPc2d1
a	a	k8xC
blikajících	blikající	k2eAgNnPc2d1
světel	světlo	k1gNnPc2
(	(	kIx(
<g/>
často	často	k6eAd1
načasovaných	načasovaný	k2eAgInPc2d1
tak	tak	k8xS,k8xC
<g/>
,	,	kIx,
že	že	k8xS
blikající	blikající	k2eAgNnPc1d1
světla	světlo	k1gNnPc1
jakoby	jakoby	k8xS
vedou	vést	k5eAaImIp3nP
letadlo	letadlo	k1gNnSc4
k	k	k7c3
dráze	dráha	k1gFnSc3
<g/>
)	)	kIx)
a	a	k8xC
vede	vést	k5eAaImIp3nS
od	od	k7c2
konce	konec	k1gInSc2
dráhy	dráha	k1gFnSc2
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
cca	cca	kA
900	#num#	k4
m.	m.	k?
</s>
<s>
Přímo	přímo	k6eAd1
na	na	k7c6
dráze	dráha	k1gFnSc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
umístěno	umístit	k5eAaPmNgNnS
mnoho	mnoho	k4c1
světel	světlo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
ji	on	k3xPp3gFnSc4
zviditelňují	zviditelňovat	k5eAaImIp3nP
–	–	k?
na	na	k7c6
stranách	strana	k1gFnPc6
dráhy	dráha	k1gFnSc2
jsou	být	k5eAaImIp3nP
pravidelně	pravidelně	k6eAd1
rozmístěna	rozmístěn	k2eAgNnPc1d1
bílá	bílý	k2eAgNnPc1d1
světla	světlo	k1gNnPc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
cca	cca	kA
600	#num#	k4
m	m	kA
dráhy	dráha	k1gFnPc1
střídají	střídat	k5eAaImIp3nP
se	s	k7c7
žlutými	žlutý	k2eAgInPc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
na	na	k7c6
runwayi	runway	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
světelně	světelně	k6eAd1
vyznačena	vyznačen	k2eAgFnSc1d1
středová	středový	k2eAgFnSc1d1
čára	čára	k1gFnSc1
–	–	k?
po	po	k7c4
většinu	většina	k1gFnSc4
dráhy	dráha	k1gFnSc2
bílými	bílý	k2eAgNnPc7d1
světly	světlo	k1gNnPc7
<g/>
,	,	kIx,
k	k	k7c3
okraji	okraj	k1gInSc3
se	se	k3xPyFc4
postupně	postupně	k6eAd1
začnou	začít	k5eAaPmIp3nP
střídat	střídat	k5eAaImF
s	s	k7c7
červenými	červená	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c4
posledních	poslední	k2eAgInPc2d1
300	#num#	k4
m	m	kA
pak	pak	k6eAd1
jen	jen	k9
červenými	červený	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odbočky	odbočka	k1gFnPc4
na	na	k7c4
navazující	navazující	k2eAgFnPc4d1
pojezdové	pojezdový	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
jsou	být	k5eAaImIp3nP
značeny	značit	k5eAaImNgFnP
zeleno-žlutými	zeleno-žlutý	k2eAgNnPc7d1
světly	světlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
druhů	druh	k1gInPc2
osvětlení	osvětlení	k1gNnSc2
dráhy	dráha	k1gFnSc2
lišících	lišící	k2eAgFnPc2d1
se	se	k3xPyFc4
intenzitou	intenzita	k1gFnSc7
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
zkratky	zkratka	k1gFnPc1
HIRL	HIRL	kA
<g/>
,	,	kIx,
MIRL	MIRL	kA
<g/>
,	,	kIx,
LIRL	LIRL	kA
pro	pro	k7c4
high	high	k1gInSc4
<g/>
,	,	kIx,
medium	medium	k1gNnSc4
<g/>
,	,	kIx,
low	low	k?
intensity	intensita	k1gFnSc2
runway	runway	k1gFnSc1
lighting	lighting	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
osvětlení	osvětlení	k1gNnSc4
dráhy	dráha	k1gFnSc2
s	s	k7c7
vysokou	vysoká	k1gFnSc7
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc2d1
<g/>
,	,	kIx,
resp.	resp.	kA
nízkou	nízký	k2eAgFnSc7d1
intenzitou	intenzita	k1gFnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
některých	některý	k3yIgNnPc6
letištích	letiště	k1gNnPc6
s	s	k7c7
menším	malý	k2eAgInSc7d2
provozem	provoz	k1gInSc7
je	být	k5eAaImIp3nS
osvětlení	osvětlení	k1gNnSc1
po	po	k7c4
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
vypnuto	vypnut	k2eAgNnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
si	se	k3xPyFc3
ho	on	k3xPp3gMnSc4
může	moct	k5eAaImIp3nS
pilot	pilot	k1gMnSc1
zapnout	zapnout	k5eAaPmF
na	na	k7c4
dálku	dálka	k1gFnSc4
rádiem	rádio	k1gNnSc7
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
příslušné	příslušný	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
několikrát	několikrát	k6eAd1
zaklíčuje	zaklíčovat	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
stiskne	stisknout	k5eAaPmIp3nS
tlačítko	tlačítko	k1gNnSc4
vysílačky	vysílačka	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
možno	možno	k6eAd1
ovlivnit	ovlivnit	k5eAaPmF
intenzitu	intenzita	k1gFnSc4
osvětlení	osvětlení	k1gNnSc2
(	(	kIx(
<g/>
troje	troje	k4xRgNnPc4
zaklíčování	zaklíčování	k1gNnPc4
během	během	k7c2
pěti	pět	k4xCc2
sekund	sekunda	k1gFnPc2
zapne	zapnout	k5eAaPmIp3nS
osvětlení	osvětlení	k1gNnSc1
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
intenzitou	intenzita	k1gFnSc7
<g/>
,	,	kIx,
patero	patero	k1gNnSc1
<g/>
,	,	kIx,
resp.	resp.	kA
sedmero	sedmero	k1gNnSc1
zapne	zapnout	k5eAaPmIp3nS
střední	střední	k2eAgNnSc1d1
<g/>
,	,	kIx,
resp.	resp.	kA
vysokou	vysoký	k2eAgFnSc4d1
intenzitu	intenzita	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schopnost	schopnost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
osvětlení	osvětlení	k1gNnSc2
řízeného	řízený	k2eAgNnSc2d1
pilotem	pilot	k1gMnSc7
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
PCL	PCL	kA
(	(	kIx(
<g/>
pilot	pilot	k1gMnSc1
controlled	controlled	k1gMnSc1
lighting	lighting	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
osvětlení	osvětlení	k1gNnSc2
dráhy	dráha	k1gFnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
také	také	k9
světelná	světelný	k2eAgFnSc1d1
signalizace	signalizace	k1gFnSc1
pomáhající	pomáhající	k2eAgFnSc1d1
pilotovi	pilot	k1gMnSc3
udržet	udržet	k5eAaPmF
správnou	správný	k2eAgFnSc4d1
sestupovou	sestupový	k2eAgFnSc4d1
rovinu	rovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
signalizační	signalizační	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
světel	světlo	k1gNnPc2
vedle	vedle	k7c2
dráhy	dráha	k1gFnSc2
poblíž	poblíž	k7c2
jejího	její	k3xOp3gInSc2
začátku	začátek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
natočena	natočen	k2eAgNnPc1d1
k	k	k7c3
přistávajícím	přistávající	k2eAgNnPc3d1
letadlům	letadlo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k8xS
je	být	k5eAaImIp3nS
letadlo	letadlo	k1gNnSc4
na	na	k7c6
ideální	ideální	k2eAgFnSc6d1
sestupové	sestupový	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
klesání	klesání	k1gNnSc1
pod	pod	k7c7
úhlem	úhel	k1gInSc7
3	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vidí	vidět	k5eAaImIp3nS
pilot	pilot	k1gMnSc1
např.	např.	kA
dvě	dva	k4xCgNnPc1
bílá	bílý	k2eAgNnPc1d1
a	a	k8xC
dvě	dva	k4xCgNnPc4
červená	červený	k2eAgNnPc4d1
světla	světlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
letadlo	letadlo	k1gNnSc1
blíží	blížit	k5eAaImIp3nS
příliš	příliš	k6eAd1
nízko	nízko	k6eAd1
(	(	kIx(
<g/>
pod	pod	k7c7
sestupovou	sestupový	k2eAgFnSc7d1
rovinou	rovina	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
z	z	k7c2
bílých	bílý	k2eAgNnPc2d1
světel	světlo	k1gNnPc2
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
na	na	k7c4
červené	červený	k2eAgNnSc4d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
ještě	ještě	k9
níž	jenž	k3xRgFnSc3
<g/>
,	,	kIx,
vidí	vidět	k5eAaImIp3nS
červená	červenat	k5eAaImIp3nS
všechna	všechen	k3xTgNnPc4
světla	světlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
letadlo	letadlo	k1gNnSc4
nad	nad	k7c7
sestupovou	sestupový	k2eAgFnSc7d1
rovinou	rovina	k1gFnSc7
<g/>
,	,	kIx,
vidí	vidět	k5eAaImIp3nS
pilot	pilot	k1gMnSc1
více	hodně	k6eAd2
bílých	bílý	k2eAgNnPc2d1
než	než	k8xS
červených	červený	k2eAgNnPc2d1
světel	světlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
různých	různý	k2eAgInPc2d1
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
společný	společný	k2eAgInSc4d1
tento	tento	k3xDgInSc4
základní	základní	k2eAgInSc4d1
princip	princip	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
liší	lišit	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
detailech	detail	k1gInPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
počet	počet	k1gInSc4
a	a	k8xC
uspořádání	uspořádání	k1gNnSc4
světel	světlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mírně	mírně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
systém	systém	k1gInSc1
používá	používat	k5eAaImIp3nS
jediné	jediný	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
mění	měnit	k5eAaImIp3nS
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
nad	nad	k7c7
rovinou	rovina	k1gFnSc7
jantarová	jantarový	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
rovině	rovina	k1gFnSc6
zelená	zelenat	k5eAaImIp3nS
<g/>
,	,	kIx,
pod	pod	k7c7
rovinou	rovina	k1gFnSc7
červená	červená	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
bliká	blikat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tyto	tento	k3xDgFnPc1
pomůcky	pomůcka	k1gFnPc1
(	(	kIx(
<g/>
označované	označovaný	k2eAgFnPc1d1
zkratkami	zkratka	k1gFnPc7
VASI	VASI	kA
či	či	k8xC
PAPI	papi	k1gMnPc1
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
jednoduchý	jednoduchý	k2eAgInSc4d1
základní	základní	k2eAgInSc4d1
princip	princip	k1gInSc4
<g/>
:	:	kIx,
světla	světlo	k1gNnSc2
jsou	být	k5eAaImIp3nP
vybavena	vybavit	k5eAaPmNgFnS
optickou	optický	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
světlo	světlo	k1gNnSc4
rozděluje	rozdělovat	k5eAaImIp3nS
do	do	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
horní	horní	k2eAgFnSc1d1
část	část	k1gFnSc1
svítí	svítit	k5eAaImIp3nS
bíle	bíle	k6eAd1
<g/>
,	,	kIx,
dolní	dolní	k2eAgFnSc2d1
červeně	červeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
barevná	barevný	k2eAgFnSc1d1
kombinace	kombinace	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
pilot	pilot	k1gMnSc1
vidí	vidět	k5eAaImIp3nS
<g/>
,	,	kIx,
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
relativní	relativní	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
vůči	vůči	k7c3
jednotlivým	jednotlivý	k2eAgNnPc3d1
světlům	světlo	k1gNnPc3
a	a	k8xC
při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
vhodném	vhodný	k2eAgNnSc6d1
nastavení	nastavení	k1gNnSc6
indikuje	indikovat	k5eAaBmIp3nS
polohu	poloha	k1gFnSc4
vůči	vůči	k7c3
sestupové	sestupový	k2eAgFnSc3d1
rovině	rovina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Schématický	schématický	k2eAgInSc1d1
diagram	diagram	k1gInSc1
PAPI	papi	k1gMnPc2
světel	světlo	k1gNnPc2
</s>
<s>
1	#num#	k4
=	=	kIx~
Rozdělení	rozdělení	k1gNnSc1
2	#num#	k4
=	=	kIx~
Zdroj	zdroj	k1gInSc4
světla	světlo	k1gNnSc2
<g/>
3	#num#	k4
=	=	kIx~
Červený	červený	k2eAgInSc1d1
filtr	filtr	k1gInSc1
<g/>
4	#num#	k4
=	=	kIx~
Čočky	čočka	k1gFnSc2
<g/>
5	#num#	k4
/	/	kIx~
6	#num#	k4
=	=	kIx~
Světelný	světelný	k2eAgInSc1d1
paprsek	paprsek	k1gInSc1
bílý	bílý	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
červený	červený	k2eAgInSc1d1
</s>
<s>
Značení	značení	k1gNnSc1
</s>
<s>
Na	na	k7c6
vzletových	vzletový	k2eAgFnPc6d1
a	a	k8xC
přistávacích	přistávací	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
značení	značení	k1gNnSc1
podobné	podobný	k2eAgNnSc1d1
vodorovným	vodorovný	k2eAgNnSc7d1
dopravním	dopravní	k2eAgNnSc7d1
značkám	značka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Samotná	samotný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
a	a	k8xC
na	na	k7c6
ní	on	k3xPp3gFnSc6
umístěné	umístěný	k2eAgNnSc1d1
značení	značení	k1gNnSc1
je	být	k5eAaImIp3nS
prováděno	provádět	k5eAaImNgNnS
v	v	k7c6
bílé	bílý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
(	(	kIx(
<g/>
na	na	k7c6
světlých	světlý	k2eAgInPc6d1
<g/>
,	,	kIx,
např.	např.	kA
betonových	betonový	k2eAgInPc6d1
<g/>
,	,	kIx,
drahách	draha	k1gFnPc6
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
použít	použít	k5eAaPmF
černé	černý	k2eAgInPc4d1
obrysy	obrys	k1gInPc4
pro	pro	k7c4
zvýraznění	zvýraznění	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navazující	navazující	k2eAgNnSc4d1
značení	značení	k1gNnSc4
mimo	mimo	k7c4
vzletové	vzletový	k2eAgFnPc4d1
a	a	k8xC
přistávací	přistávací	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
je	být	k5eAaImIp3nS
vyvedeno	vyveden	k2eAgNnSc1d1
žlutě	žlutě	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Značení	značení	k1gNnSc1
vzletových	vzletový	k2eAgFnPc2d1
a	a	k8xC
přistávacích	přistávací	k2eAgFnPc2d1
drah	draha	k1gFnPc2
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
v	v	k7c6
měřítku	měřítko	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Dráha	dráha	k1gFnSc1
je	být	k5eAaImIp3nS
orámována	orámovat	k5eAaPmNgFnS
plnou	plný	k2eAgFnSc7d1
bílou	bílý	k2eAgFnSc7d1
čárou	čára	k1gFnSc7
a	a	k8xC
její	její	k3xOp3gInSc1
střed	střed	k1gInSc1
je	být	k5eAaImIp3nS
vyznačen	vyznačit	k5eAaPmNgInS
přerušovanou	přerušovaný	k2eAgFnSc7d1
čárou	čára	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práh	práh	k1gInSc1
dráhy	dráha	k1gFnSc2
je	být	k5eAaImIp3nS
vyznačen	vyznačit	k5eAaPmNgInS
podélnými	podélný	k2eAgInPc7d1
pásy	pás	k1gInPc7
po	po	k7c6
celé	celá	k1gFnSc6
její	její	k3xOp3gFnSc6
šířce	šířka	k1gFnSc6
(	(	kIx(
<g/>
obdobné	obdobný	k2eAgNnSc1d1
značení	značení	k1gNnSc1
přechodu	přechod	k1gInSc2
pro	pro	k7c4
chodce	chodec	k1gMnPc4
na	na	k7c6
silnici	silnice	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
počet	počet	k1gInSc1
pruhů	pruh	k1gInPc2
obvykle	obvykle	k6eAd1
označuje	označovat	k5eAaImIp3nS
šířku	šířka	k1gFnSc4
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
4	#num#	k4
pruhy	pruh	k1gInPc7
pro	pro	k7c4
18	#num#	k4
m	m	kA
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
6	#num#	k4
pro	pro	k7c4
23	#num#	k4
m	m	kA
<g/>
,	,	kIx,
8	#num#	k4
pro	pro	k7c4
30	#num#	k4
m	m	kA
<g/>
,	,	kIx,
12	#num#	k4
pro	pro	k7c4
45	#num#	k4
m	m	kA
<g/>
,	,	kIx,
16	#num#	k4
pruhů	pruh	k1gInPc2
pro	pro	k7c4
60	#num#	k4
metrovou	metrový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
na	na	k7c6
začátku	začátek	k1gInSc6
dráhy	dráha	k1gFnSc2
její	její	k3xOp3gNnSc1
číselné	číselný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
(	(	kIx(
<g/>
s	s	k7c7
případným	případný	k2eAgNnSc7d1
rozlišujícím	rozlišující	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dotykové	dotykový	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
jsou	být	k5eAaImIp3nP
pravidelně	pravidelně	k6eAd1
po	po	k7c4
150	#num#	k4
m	m	kA
rozmístěné	rozmístěný	k2eAgFnSc2d1
značky	značka	k1gFnSc2
–	–	k?
nejprve	nejprve	k6eAd1
tři	tři	k4xCgInPc4
pruhy	pruh	k1gInPc4
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
dva	dva	k4xCgInPc4
a	a	k8xC
nakonec	nakonec	k6eAd1
jeden	jeden	k4xCgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
přistávající	přistávající	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
mají	mít	k5eAaImIp3nP
při	při	k7c6
ideálním	ideální	k2eAgNnSc6d1
přistání	přistání	k1gNnSc6
dotknout	dotknout	k5eAaPmF
země	zem	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vyznačeno	vyznačen	k2eAgNnSc1d1
jedním	jeden	k4xCgInSc7
širokým	široký	k2eAgInSc7d1
bílým	bílý	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
před	před	k7c7
prahem	práh	k1gInSc7
dráhy	dráha	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
část	část	k1gFnSc1
dráhy	dráha	k1gFnSc2
použitelná	použitelný	k2eAgFnSc1d1
jen	jen	k9
pro	pro	k7c4
vzlety	vzlet	k1gInPc4
<g/>
,	,	kIx,
nikoli	nikoli	k9
pro	pro	k7c4
přistání	přistání	k1gNnSc4
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
pro	pro	k7c4
přistání	přistání	k1gNnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
přistání	přistání	k1gNnSc6
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
směru	směr	k1gInSc6
ji	on	k3xPp3gFnSc4
využít	využít	k5eAaPmF
lze	lze	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
vyznačena	vyznačit	k5eAaPmNgFnS
bílými	bílý	k2eAgFnPc7d1
šipkami	šipka	k1gFnPc7
v	v	k7c6
ose	osa	k1gFnSc6
dráhy	dráha	k1gFnSc2
a	a	k8xC
práh	práh	k1gInSc1
(	(	kIx(
<g/>
tzn.	tzn.	kA
začátek	začátek	k1gInSc4
dráhy	dráha	k1gFnSc2
použitelné	použitelný	k2eAgFnSc2d1
pro	pro	k7c4
přistání	přistání	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
zdůrazněn	zdůraznit	k5eAaPmNgInS
širokým	široký	k2eAgInSc7d1
bílým	bílý	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
a	a	k8xC
šipičkami	šipička	k1gFnPc7
po	po	k7c6
celé	celý	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
před	před	k7c7
dráhou	dráha	k1gFnSc7
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zpevněná	zpevněný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
dojezdová	dojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
navazující	navazující	k2eAgFnSc1d1
pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nelze	lze	k6eNd1
využívat	využívat	k5eAaPmF,k5eAaImF
pro	pro	k7c4
vzlety	vzlet	k1gInPc4
ani	ani	k8xC
přistání	přistání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
dráha	dráha	k1gFnSc1
je	být	k5eAaImIp3nS
vyznačena	vyznačit	k5eAaPmNgFnS
žlutou	žlutý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
<g/>
;	;	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
plocha	plocha	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
letadla	letadlo	k1gNnPc1
nemají	mít	k5eNaImIp3nP
vůbec	vůbec	k9
pohybovat	pohybovat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vyznačena	vyznačit	k5eAaPmNgFnS
širokými	široký	k2eAgInPc7d1
žlutými	žlutý	k2eAgInPc7d1
symboly	symbol	k1gInPc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
V.	V.	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.aeroweb.cz/clanky/3064-ruzyska-draha-13-31-ponese-od-jara-2012-oznaceni-12-30	https://www.aeroweb.cz/clanky/3064-ruzyska-draha-13-31-ponese-od-jara-2012-oznaceni-12-30	k4
<g/>
↑	↑	k?
Boeing	boeing	k1gInSc4
777	#num#	k4
Flight	Flight	k1gMnSc1
Manual	Manual	k1gMnSc1
<g/>
,	,	kIx,
Sec	sec	kA
<g/>
.	.	kIx.
1	#num#	k4
Page	Pag	k1gInSc2
6	#num#	k4
<g/>
↑	↑	k?
AIM	AIM	kA
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Aeronautical	Aeronautical	k1gFnSc1
Lighting	Lighting	k1gInSc1
and	and	k?
Other	Other	k1gInSc1
Airport	Airport	k1gInSc1
Visual	Visual	k1gInSc1
Aids	aids	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
AIM	AIM	kA
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Airport	Airport	k1gInSc1
Marking	Marking	k1gInSc1
Aids	aids	k1gInSc1
and	and	k?
Signs	Signs	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Letiště	letiště	k1gNnSc1
</s>
<s>
Heliport	heliport	k1gInSc1
</s>
<s>
Pojezdová	pojezdový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
českých	český	k2eAgNnPc2d1
letišť	letiště	k1gNnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vzletová	vzletový	k2eAgFnSc1d1
a	a	k8xC
přistávací	přistávací	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ranvej	ranvej	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
1169	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
