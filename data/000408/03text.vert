<p>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Hg	Hg	k1gFnSc2	Hg
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Hydrargyrum	Hydrargyrum	k1gInSc1	Hydrargyrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgMnSc1d1	těžký
<g/>
,	,	kIx,	,
toxický	toxický	k2eAgInSc1d1	toxický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
slitin	slitina	k1gFnPc2	slitina
(	(	kIx(	(
<g/>
amalgámů	amalgám	k1gInPc2	amalgám
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
různých	různý	k2eAgInPc2d1	různý
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
teploměry	teploměr	k1gInPc1	teploměr
<g/>
,	,	kIx,	,
barometry	barometr	k1gInPc1	barometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediné	k1gNnSc7	jediné
z	z	k7c2	z
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
kapalný	kapalný	k2eAgMnSc1d1	kapalný
<g/>
.	.	kIx.	.
</s>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
velice	velice	k6eAd1	velice
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
je	být	k5eAaImIp3nS	být
kapalný	kapalný	k2eAgInSc1d1	kapalný
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
těžká	těžký	k2eAgFnSc1d1	těžká
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vede	vést	k5eAaImIp3nS	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
1.	[number]	k4	1.
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
4,154	[number]	k4	4,154
K	k	k7c3	k
(	(	kIx(	(
<g/>
Hg	Hg	k1gMnSc1	Hg
-	-	kIx~	-
α	α	k?	α
<g/>
)	)	kIx)	)
a	a	k8xC	a
3,949	[number]	k4	3,949
K	k	k7c3	k
(	(	kIx(	(
<g/>
Hg	Hg	k1gMnSc1	Hg
-	-	kIx~	-
β	β	k?	β
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Hg	Hg	k1gFnSc2	Hg
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
kovalentní	kovalentní	k2eAgFnSc1d1	kovalentní
vazba	vazba	k1gFnSc1	vazba
rtuť-rtuť	rtuťtuť	k1gFnSc1	rtuť-rtuť
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hg2	Hg2	k1gMnSc1	Hg2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vlastnosti	vlastnost	k1gFnPc1	vlastnost
sloučenin	sloučenina	k1gFnPc2	sloučenina
rtuťných	rtuťný	k2eAgFnPc2d1	rtuťný
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
sloučeninám	sloučenina	k1gFnPc3	sloučenina
stříbrným	stříbrný	k2eAgFnPc3d1	stříbrná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
rtuťnaté	rtuťnatý	k2eAgFnPc1d1	rtuťnatý
soli	sůl	k1gFnPc1	sůl
připomínají	připomínat	k5eAaImIp3nP	připomínat
spíše	spíše	k9	spíše
sloučeniny	sloučenina	k1gFnPc1	sloučenina
měďnaté	měďnatý	k2eAgFnPc1d1	měďnatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
rtuť	rtuť	k1gFnSc1	rtuť
neomezeně	omezeně	k6eNd1	omezeně
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
však	však	k9	však
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
elementární	elementární	k2eAgFnSc7d1	elementární
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
halogeny	halogen	k1gInPc7	halogen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
některými	některý	k3yIgInPc7	některý
kovy	kov	k1gInPc7	kov
tvoří	tvořit	k5eAaImIp3nS	tvořit
kapalné	kapalný	k2eAgNnSc1d1	kapalné
i	i	k9	i
pevné	pevný	k2eAgFnPc4d1	pevná
slitiny	slitina	k1gFnPc4	slitina
–	–	k?	–
amalgámy	amalgáma	k1gFnSc2	amalgáma
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
snadno	snadno	k6eAd1	snadno
vzniká	vznikat	k5eAaImIp3nS	vznikat
amalgám	amalgám	k1gInSc1	amalgám
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
rtuť	rtuť	k1gFnSc1	rtuť
proto	proto	k8xC	proto
vzbuzovala	vzbuzovat	k5eAaImAgFnS	vzbuzovat
již	již	k6eAd1	již
odedávna	odedávna	k6eAd1	odedávna
zájem	zájem	k1gInSc4	zájem
alchymistů	alchymista	k1gMnPc2	alchymista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
zlato	zlato	k1gNnSc4	zlato
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
prvků	prvek	k1gInPc2	prvek
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
transmutace	transmutace	k1gFnSc2	transmutace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
rtuť	rtuť	k1gFnSc1	rtuť
velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
3	[number]	k4	3
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
koncentrace	koncentrace	k1gFnSc1	koncentrace
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
měřitelnosti	měřitelnost	k1gFnSc2	měřitelnost
–	–	k?	–
0,03	[number]	k4	0,03
mikrogramu	mikrogram	k1gInSc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
rtuti	rtuť	k1gFnSc2	rtuť
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
rtuť	rtuť	k1gFnSc1	rtuť
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
jako	jako	k9	jako
elementární	elementární	k2eAgInSc4d1	elementární
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
minerálem	minerál	k1gInSc7	minerál
a	a	k8xC	a
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
však	však	k9	však
sulfid	sulfid	k1gInSc1	sulfid
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
<g/>
,	,	kIx,	,
HgS	HgS	k1gFnPc7	HgS
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
rumělka	rumělka	k1gFnSc1	rumělka
neboli	neboli	k8xC	neboli
cinabarit	cinabarit	k1gInSc1	cinabarit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
světová	světový	k2eAgFnSc1d1	světová
ložiska	ložisko	k1gNnSc2	ložisko
tohoto	tento	k3xDgInSc2	tento
nerostu	nerost	k1gInSc2	nerost
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
rtuti	rtuť	k1gFnSc2	rtuť
z	z	k7c2	z
rumělky	rumělka	k1gFnSc2	rumělka
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
pražení	pražení	k1gNnSc6	pražení
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
HgS	HgS	k?	HgS
+	+	kIx~	+
O2	O2	k1gMnSc1	O2
→	→	k?	→
Hg	Hg	k1gFnSc1	Hg
+	+	kIx~	+
SO2Další	SO2Další	k1gNnSc1	SO2Další
možností	možnost	k1gFnPc2	možnost
získání	získání	k1gNnSc2	získání
elementární	elementární	k2eAgFnSc2d1	elementární
rtuti	rtuť	k1gFnSc2	rtuť
ze	z	k7c2	z
sulfidických	sulfidický	k2eAgFnPc2d1	sulfidická
rud	ruda	k1gFnPc2	ruda
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
redukce	redukce	k1gFnSc1	redukce
kovovým	kovový	k2eAgNnSc7d1	kovové
železem	železo	k1gNnSc7	železo
nebo	nebo	k8xC	nebo
pražení	pražení	k1gNnSc1	pražení
rudy	ruda	k1gFnSc2	ruda
s	s	k7c7	s
přídavky	přídavek	k1gInPc7	přídavek
oxidu	oxid	k1gInSc2	oxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
následující	následující	k2eAgFnPc4d1	následující
reakce	reakce	k1gFnPc4	reakce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
HgS	HgS	k1gFnSc1	HgS
+	+	kIx~	+
4	[number]	k4	4
CaO	CaO	k1gFnSc2	CaO
→	→	k?	→
4	[number]	k4	4
Hg	Hg	k1gFnPc2	Hg
+	+	kIx~	+
3	[number]	k4	3
CaS	CaS	k1gFnPc2	CaS
+	+	kIx~	+
CaSO4Vzniklé	CaSO4Vzniklý	k2eAgInPc1d1	CaSO4Vzniklý
rtuťové	rtuťový	k2eAgInPc1d1	rtuťový
páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
ochlazovány	ochlazovat	k5eAaImNgInP	ochlazovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
kondenzaci	kondenzace	k1gFnSc3	kondenzace
a	a	k8xC	a
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velmi	velmi	k6eAd1	velmi
čistá	čistý	k2eAgFnSc1d1	čistá
kovová	kovový	k2eAgFnSc1d1	kovová
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
destilace	destilace	k1gFnSc2	destilace
rtuti	rtuť	k1gFnSc2	rtuť
je	být	k5eAaImIp3nS	být
i	i	k9	i
spolehlivým	spolehlivý	k2eAgInSc7d1	spolehlivý
způsobem	způsob	k1gInSc7	způsob
jejího	její	k3xOp3gNnSc2	její
čištění	čištění	k1gNnSc2	čištění
a	a	k8xC	a
rafinace	rafinace	k1gFnSc2	rafinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
využití	využití	k1gNnSc1	využití
rtuti	rtuť	k1gFnSc2	rtuť
přináší	přinášet	k5eAaImIp3nS	přinášet
vážné	vážný	k2eAgInPc4d1	vážný
ekologické	ekologický	k2eAgInPc4d1	ekologický
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
a	a	k8xC	a
společenské	společenský	k2eAgInPc4d1	společenský
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
proto	proto	k8xC	proto
přijala	přijmout	k5eAaPmAgFnS	přijmout
strategii	strategie	k1gFnSc4	strategie
eliminace	eliminace	k1gFnSc2	eliminace
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
snížení	snížení	k1gNnSc3	snížení
emisí	emise	k1gFnPc2	emise
rtuti	rtuť	k1gFnSc2	rtuť
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc1	řešení
problému	problém	k1gInSc2	problém
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
přebytků	přebytek	k1gInPc2	přebytek
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
akcí	akce	k1gFnPc2	akce
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
strategie	strategie	k1gFnSc1	strategie
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
bezprostředně	bezprostředně	k6eAd1	bezprostředně
dotýkat	dotýkat	k5eAaImF	dotýkat
také	také	k9	také
sektoru	sektor	k1gInSc6	sektor
nakládání	nakládání	k1gNnSc1	nakládání
s	s	k7c7	s
odpady	odpad	k1gInPc7	odpad
<g/>
.	.	kIx.	.
<g/>
Ceny	cena	k1gFnPc1	cena
rtuti	rtuť	k1gFnSc2	rtuť
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
v	v	k7c6	v
EU	EU	kA	EU
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
uplatnění	uplatnění	k1gNnSc1	uplatnění
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
má	mít	k5eAaImIp3nS	mít
rtuť	rtuť	k1gFnSc1	rtuť
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
svých	svůj	k3xOyFgFnPc2	svůj
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
kovy	kov	k1gInPc7	kov
–	–	k?	–
amalgámy	amalgám	k1gInPc4	amalgám
<g/>
.	.	kIx.	.
</s>
<s>
Ochotně	ochotně	k6eAd1	ochotně
je	být	k5eAaImIp3nS	být
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
s	s	k7c7	s
Au	au	k0	au
<g/>
,	,	kIx,	,
Ag	Ag	k1gMnSc1	Ag
<g/>
,	,	kIx,	,
Cu	Cu	k1gMnSc1	Cu
<g/>
,	,	kIx,	,
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Cd	cd	kA	cd
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
železnými	železný	k2eAgInPc7d1	železný
kovy	kov	k1gInPc7	kov
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Fe	Fe	k1gFnSc4	Fe
<g/>
,	,	kIx,	,
Ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
Co	co	k3yQnSc4	co
nevznikají	vznikat	k5eNaImIp3nP	vznikat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dentální	dentální	k2eAgInPc1d1	dentální
amalgámy	amalgám	k1gInPc1	amalgám
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
amalgámy	amalgám	k1gInPc7	amalgám
dentálními	dentální	k2eAgInPc7d1	dentální
<g/>
,	,	kIx,	,
používanými	používaný	k2eAgInPc7d1	používaný
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lékařství	lékařství	k1gNnSc6	lékařství
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
odolná	odolný	k2eAgNnPc4d1	odolné
výplň	výplň	k1gFnSc4	výplň
zubu	zub	k1gInSc2	zub
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
zubního	zubní	k2eAgInSc2d1	zubní
kazu	kaz	k1gInSc2	kaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
amalgámy	amalgáma	k1gFnPc1	amalgáma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
smísením	smísení	k1gNnSc7	smísení
rtuti	rtuť	k1gFnSc2	rtuť
se	s	k7c7	s
slitinou	slitina	k1gFnSc7	slitina
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
posledních	poslední	k2eAgInPc2d1	poslední
tří	tři	k4xCgInPc2	tři
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
výsledný	výsledný	k2eAgInSc1d1	výsledný
amalgám	amalgám	k1gInSc1	amalgám
tvořen	tvořit	k5eAaImNgInS	tvořit
přibližně	přibližně	k6eAd1	přibližně
stejným	stejný	k2eAgNnSc7d1	stejné
váhovým	váhový	k2eAgNnSc7d1	váhové
množstvím	množství	k1gNnSc7	množství
rtuti	rtuť	k1gFnSc2	rtuť
jako	jako	k8xC	jako
sumy	suma	k1gFnSc2	suma
zbývajících	zbývající	k2eAgInPc2d1	zbývající
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dentální	dentální	k2eAgInSc1d1	dentální
amalgám	amalgám	k1gInSc1	amalgám
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
řadu	řada	k1gFnSc4	řada
přísných	přísný	k2eAgNnPc2d1	přísné
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lékař	lékař	k1gMnSc1	lékař
měl	mít	k5eAaImAgMnS	mít
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
plombu	plomb	k1gInSc2	plomb
do	do	k7c2	do
zubu	zub	k1gInSc2	zub
správně	správně	k6eAd1	správně
zasadit	zasadit	k5eAaPmF	zasadit
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
upravit	upravit	k5eAaPmF	upravit
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
by	by	kYmCp3nS	by
však	však	k9	však
již	již	k6eAd1	již
po	po	k7c6	po
hodině	hodina	k1gFnSc6	hodina
až	až	k9	až
dvou	dva	k4xCgMnPc6	dva
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
natolik	natolik	k6eAd1	natolik
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
pacient	pacient	k1gMnSc1	pacient
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
(	(	kIx(	(
<g/>
kousat	kousat	k5eAaImF	kousat
na	na	k7c4	na
ošetřený	ošetřený	k2eAgInSc4d1	ošetřený
zub	zub	k1gInSc4	zub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
amalgám	amalgám	k1gInSc1	amalgám
tvrdne	tvrdnout	k5eAaImIp3nS	tvrdnout
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tvrdnutí	tvrdnutí	k1gNnSc2	tvrdnutí
nesmí	smět	k5eNaImIp3nS	smět
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
rozměrovým	rozměrový	k2eAgFnPc3d1	rozměrová
změnám	změna	k1gFnPc3	změna
amalgámu	amalgám	k1gInSc2	amalgám
–	–	k?	–
při	při	k7c6	při
expanzi	expanze	k1gFnSc6	expanze
by	by	kYmCp3nS	by
hrozilo	hrozit	k5eAaImAgNnS	hrozit
roztržení	roztržení	k1gNnSc1	roztržení
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
při	při	k7c6	při
zmenšení	zmenšení	k1gNnSc6	zmenšení
objemu	objem	k1gInSc2	objem
by	by	kYmCp3nS	by
plomba	plomba	k1gFnSc1	plomba
vypadávala	vypadávat	k5eAaImAgFnS	vypadávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amalgám	amalgám	k1gInSc1	amalgám
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
co	co	k3yRnSc4	co
nejvíce	hodně	k6eAd3	hodně
chemicky	chemicky	k6eAd1	chemicky
odolný	odolný	k2eAgMnSc1d1	odolný
vůči	vůči	k7c3	vůči
prostředí	prostředí	k1gNnSc3	prostředí
v	v	k7c6	v
lidských	lidský	k2eAgNnPc6d1	lidské
ústech	ústa	k1gNnPc6	ústa
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
zbylých	zbylý	k2eAgInPc2d1	zbylý
kovů	kov	k1gInPc2	kov
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
amalgám	amalgám	k1gInSc1	amalgám
v	v	k7c6	v
dentální	dentální	k2eAgFnSc6d1	dentální
medicíně	medicína	k1gFnSc6	medicína
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
různými	různý	k2eAgInPc7d1	různý
plastickými	plastický	k2eAgInPc7d1	plastický
polymery	polymer	k1gInPc7	polymer
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc4	jeho
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
stále	stále	k6eAd1	stále
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zubních	zubní	k2eAgFnPc2d1	zubní
výplní	výplň	k1gFnPc2	výplň
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
většina	většina	k1gFnSc1	většina
zubních	zubní	k2eAgMnPc2d1	zubní
lékařů	lékař	k1gMnPc2	lékař
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
k	k	k7c3	k
výplním	výplň	k1gFnPc3	výplň
stoliček	stolička	k1gFnPc2	stolička
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nevadí	vadit	k5eNaImIp3nS	vadit
jeho	jeho	k3xOp3gFnSc1	jeho
estetická	estetický	k2eAgFnSc1d1	estetická
nevzhlednost	nevzhlednost	k1gFnSc1	nevzhlednost
(	(	kIx(	(
<g/>
tmavá	tmavý	k2eAgFnSc1d1	tmavá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
jeho	jeho	k3xOp3gFnSc1	jeho
tvrdost	tvrdost	k1gFnSc1	tvrdost
a	a	k8xC	a
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
mechanická	mechanický	k2eAgFnSc1d1	mechanická
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
amalgámy	amalgáma	k1gFnPc1	amalgáma
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
amalgám	amalgám	k1gInSc1	amalgám
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
sporadicky	sporadicky	k6eAd1	sporadicky
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
amalgamaci	amalgamace	k1gFnSc6	amalgamace
zlata	zlato	k1gNnSc2	zlato
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
těžbě	těžba	k1gFnSc6	těžba
z	z	k7c2	z
rud	ruda	k1gFnPc2	ruda
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
kovnatosti	kovnatost	k1gFnSc6	kovnatost
<g/>
.	.	kIx.	.
</s>
<s>
Jemně	jemně	k6eAd1	jemně
rozdrcená	rozdrcený	k2eAgFnSc1d1	rozdrcená
hornina	hornina	k1gFnSc1	hornina
se	se	k3xPyFc4	se
kontaktuje	kontaktovat	k5eAaImIp3nS	kontaktovat
s	s	k7c7	s
kovovou	kovový	k2eAgFnSc7d1	kovová
rtutí	rtuť	k1gFnSc7	rtuť
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
prakticky	prakticky	k6eAd1	prakticky
kompletně	kompletně	k6eAd1	kompletně
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
kapalného	kapalný	k2eAgInSc2d1	kapalný
amalgámu	amalgám	k1gInSc2	amalgám
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
od	od	k7c2	od
horniny	hornina	k1gFnSc2	hornina
se	se	k3xPyFc4	se
rtuť	rtuť	k1gFnSc1	rtuť
oddestiluje	oddestilovat	k5eAaPmIp3nS	oddestilovat
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
získané	získaný	k2eAgNnSc1d1	získané
zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
rafinuje	rafinovat	k5eAaImIp3nS	rafinovat
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
těžby	těžba	k1gFnSc2	těžba
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
kompletní	kompletní	k2eAgNnSc1d1	kompletní
oddělení	oddělení	k1gNnSc1	oddělení
rtuti	rtuť	k1gFnSc2	rtuť
od	od	k7c2	od
zbytkové	zbytkový	k2eAgFnSc2d1	zbytková
hlušiny	hlušina	k1gFnSc2	hlušina
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
ke	k	k7c3	k
kontaminaci	kontaminace	k1gFnSc3	kontaminace
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
vysoce	vysoce	k6eAd1	vysoce
toxickou	toxický	k2eAgFnSc7d1	toxická
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sodíkový	sodíkový	k2eAgInSc1d1	sodíkový
amalgám	amalgám	k1gInSc1	amalgám
vznikající	vznikající	k2eAgInSc1d1	vznikající
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
rtuťové	rtuťový	k2eAgFnSc2d1	rtuťová
katody	katoda	k1gFnSc2	katoda
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
ekologické	ekologický	k2eAgFnSc2d1	ekologická
havárie	havárie	k1gFnSc2	havárie
pří	přít	k5eAaImIp3nS	přít
záplavách	záplava	k1gFnPc6	záplava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
ve	v	k7c6	v
Spolaně	Spolana	k1gFnSc6	Spolana
Neratovice	Neratovice	k1gFnSc2	Neratovice
byla	být	k5eAaImAgFnS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
zatopením	zatopení	k1gNnSc7	zatopení
provozu	provoz	k1gInSc2	provoz
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
kontaminací	kontaminace	k1gFnSc7	kontaminace
labské	labský	k2eAgFnSc2d1	Labská
vody	voda	k1gFnSc2	voda
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
přístroje	přístroj	k1gInPc1	přístroj
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
rtuť	rtuť	k1gFnSc1	rtuť
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
různých	různý	k2eAgInPc2d1	různý
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
přístrojů	přístroj	k1gInPc2	přístroj
–	–	k?	–
teploměrů	teploměr	k1gInPc2	teploměr
a	a	k8xC	a
tlakoměrů	tlakoměr	k1gInPc2	tlakoměr
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
udávat	udávat	k5eAaImF	udávat
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
mm	mm	kA	mm
rtuťového	rtuťový	k2eAgInSc2d1	rtuťový
sloupce	sloupec	k1gInSc2	sloupec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
normální	normální	k2eAgInSc1d1	normální
tlak	tlak	k1gInSc1	tlak
měl	mít	k5eAaImAgInS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
760	[number]	k4	760
mm	mm	kA	mm
Hg	Hg	k1gFnPc2	Hg
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgFnPc1d1	dobrá
elektrické	elektrický	k2eAgFnPc1d1	elektrická
vodivosti	vodivost	k1gFnPc1	vodivost
a	a	k8xC	a
tekutosti	tekutost	k1gFnPc1	tekutost
rtuti	rtuť	k1gFnSc2	rtuť
i	i	k9	i
za	za	k7c2	za
pokojových	pokojový	k2eAgFnPc2d1	pokojová
teplot	teplota	k1gFnPc2	teplota
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
využívá	využívat	k5eAaImIp3nS	využívat
ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
polohových	polohový	k2eAgInPc2d1	polohový
spínačů	spínač	k1gInPc2	spínač
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
v	v	k7c6	v
žargonu	žargon	k1gInSc2	žargon
prasátek	prasátko	k1gNnPc2	prasátko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
výrobu	výroba	k1gFnSc4	výroba
rtuťových	rtuťový	k2eAgInPc2d1	rtuťový
teploměrů	teploměr	k1gInPc2	teploměr
zakázala	zakázat	k5eAaPmAgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
přestávají	přestávat	k5eAaImIp3nP	přestávat
kalibrovat	kalibrovat	k5eAaImF	kalibrovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výbojky	výbojka	k1gFnPc4	výbojka
a	a	k8xC	a
zářivky	zářivka	k1gFnPc4	zářivka
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
výboj	výboj	k1gInSc1	výboj
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
rtuťových	rtuťový	k2eAgFnPc2d1	rtuťová
par	para	k1gFnPc2	para
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
tlakem	tlak	k1gInSc7	tlak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
inertními	inertní	k2eAgInPc7d1	inertní
plyny	plyn	k1gInPc7	plyn
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
silné	silný	k2eAgNnSc1d1	silné
světelné	světelný	k2eAgNnSc1d1	světelné
vyzařování	vyzařování	k1gNnSc1	vyzařování
v	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
luminoforu	luminofor	k1gInSc6	luminofor
naneseném	nanesený	k2eAgInSc6d1	nanesený
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
povrchu	povrch	k1gInSc6	povrch
mění	měnit	k5eAaImIp3nS	měnit
ve	v	k7c4	v
viditelné	viditelný	k2eAgNnSc4d1	viditelné
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
tak	tak	k9	tak
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
osvětlovacích	osvětlovací	k2eAgNnPc2d1	osvětlovací
těles	těleso	k1gNnPc2	těleso
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
světelnou	světelný	k2eAgFnSc7d1	světelná
účinností	účinnost	k1gFnSc7	účinnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
klasické	klasický	k2eAgFnPc1d1	klasická
žárovky	žárovka	k1gFnPc1	žárovka
s	s	k7c7	s
wolframovým	wolframový	k2eAgNnSc7d1	wolframové
vláknem	vlákno	k1gNnSc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Zářivkové	zářivkový	k2eAgFnPc1d1	zářivková
trubice	trubice	k1gFnPc1	trubice
tak	tak	k6eAd1	tak
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dbát	dbát	k5eAaImF	dbát
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
opatrnosti	opatrnost	k1gFnSc3	opatrnost
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
likvidaci	likvidace	k1gFnSc6	likvidace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Polarografie	polarografie	k1gFnSc2	polarografie
===	===	k?	===
</s>
</p>
<p>
<s>
Elektrochemická	elektrochemický	k2eAgFnSc1d1	elektrochemická
analytická	analytický	k2eAgFnSc1d1	analytická
technika	technika	k1gFnSc1	technika
–	–	k?	–
polarografie	polarografie	k1gFnSc1	polarografie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
intenzity	intenzita	k1gFnSc2	intenzita
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
mezi	mezi	k7c7	mezi
rtuťovou	rtuťový	k2eAgFnSc7d1	rtuťová
kapkovou	kapkový	k2eAgFnSc7d1	kapková
a	a	k8xC	a
referenční	referenční	k2eAgFnSc7d1	referenční
elektrodou	elektroda	k1gFnSc7	elektroda
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
elektrickém	elektrický	k2eAgInSc6d1	elektrický
potenciálu	potenciál	k1gInSc6	potenciál
<g/>
,	,	kIx,	,
vloženém	vložený	k2eAgMnSc6d1	vložený
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
elektrody	elektroda	k1gFnPc4	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
měření	měření	k1gNnSc6	měření
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
elektrody	elektroda	k1gFnPc1	elektroda
ponoří	ponořit	k5eAaPmIp3nP	ponořit
do	do	k7c2	do
analyzovaného	analyzovaný	k2eAgInSc2d1	analyzovaný
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
se	se	k3xPyFc4	se
intenzita	intenzita	k1gFnSc1	intenzita
proudu	proud	k1gInSc2	proud
procházejícího	procházející	k2eAgInSc2d1	procházející
mezi	mezi	k7c7	mezi
elektrodami	elektroda	k1gFnPc7	elektroda
při	při	k7c6	při
plynulé	plynulý	k2eAgFnSc6d1	plynulá
změně	změna	k1gFnSc6	změna
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Analyzované	analyzovaný	k2eAgInPc1d1	analyzovaný
ionty	ion	k1gInPc1	ion
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
redukují	redukovat	k5eAaBmIp3nP	redukovat
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
redoxního	redoxní	k2eAgInSc2d1	redoxní
potenciálu	potenciál	k1gInSc2	potenciál
a	a	k8xC	a
intenzita	intenzita	k1gFnSc1	intenzita
dosaženého	dosažený	k2eAgInSc2d1	dosažený
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
limitní	limitní	k2eAgInSc1d1	limitní
difuzní	difuzní	k2eAgInSc1d1	difuzní
proud	proud	k1gInSc1	proud
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
koncentrace	koncentrace	k1gFnSc2	koncentrace
měřené	měřený	k2eAgFnSc2d1	měřená
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
využití	využití	k1gNnSc2	využití
polarografické	polarografický	k2eAgFnSc2d1	polarografická
metody	metoda	k1gFnSc2	metoda
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
získal	získat	k5eAaPmAgMnS	získat
akademik	akademik	k1gMnSc1	akademik
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
elektrochemii	elektrochemie	k1gFnSc6	elektrochemie
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
polarografického	polarografický	k2eAgInSc2d1	polarografický
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
však	však	k9	však
rtuťovou	rtuťový	k2eAgFnSc4d1	rtuťová
kapkovou	kapkový	k2eAgFnSc4d1	kapková
elektrodu	elektroda	k1gFnSc4	elektroda
jinými	jiný	k2eAgInPc7d1	jiný
typy	typ	k1gInPc7	typ
elektrod	elektroda	k1gFnPc2	elektroda
(	(	kIx(	(
<g/>
rotující	rotující	k2eAgFnSc1d1	rotující
disková	diskový	k2eAgFnSc1d1	disková
elektroda	elektroda	k1gFnSc1	elektroda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
modifikují	modifikovat	k5eAaBmIp3nP	modifikovat
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
elektrický	elektrický	k2eAgInSc4d1	elektrický
potenciál	potenciál	k1gInSc4	potenciál
vložený	vložený	k2eAgInSc4d1	vložený
na	na	k7c4	na
měrné	měrný	k2eAgFnPc4d1	měrná
elektrody	elektroda	k1gFnPc4	elektroda
(	(	kIx(	(
<g/>
diferenční	diferenční	k2eAgFnPc1d1	diferenční
pulsní	pulsní	k2eAgFnPc1d1	pulsní
voltametrie	voltametrie	k1gFnPc1	voltametrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vakcíny	vakcína	k1gFnSc2	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
vakcíny	vakcína	k1gFnPc1	vakcína
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
stopové	stopová	k1gFnPc4	stopová
množství	množství	k1gNnSc2	množství
rtuti	rtuť	k1gFnSc2	rtuť
ve	v	k7c6	v
sloučenině	sloučenina	k1gFnSc6	sloučenina
zvané	zvaný	k2eAgFnSc2d1	zvaná
thiomersal	thiomersat	k5eAaPmAgMnS	thiomersat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
přítomnost	přítomnost	k1gFnSc1	přítomnost
zabraňovala	zabraňovat	k5eAaImAgFnS	zabraňovat
jednak	jednak	k8xC	jednak
množení	množení	k1gNnPc1	množení
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k6eAd1	hlavně
likvidovala	likvidovat	k5eAaBmAgFnS	likvidovat
případné	případný	k2eAgInPc4d1	případný
aktivní	aktivní	k2eAgInPc4d1	aktivní
zbytky	zbytek	k1gInPc4	zbytek
virů	vir	k1gInPc2	vir
(	(	kIx(	(
<g/>
účinně	účinně	k6eAd1	účinně
likvidovala	likvidovat	k5eAaBmAgFnS	likvidovat
hepatitidu	hepatitida	k1gFnSc4	hepatitida
typu	typ	k1gInSc2	typ
B	B	kA	B
<g/>
,	,	kIx,	,
meningitidu	meningitida	k1gFnSc4	meningitida
<g/>
,	,	kIx,	,
tetanus	tetanus	k1gInSc1	tetanus
<g/>
,	,	kIx,	,
viry	vir	k1gInPc1	vir
dětské	dětský	k2eAgFnSc2d1	dětská
obrny	obrna	k1gFnSc2	obrna
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
ukazovaly	ukazovat	k5eAaImAgFnP	ukazovat
na	na	k7c4	na
možnou	možný	k2eAgFnSc4d1	možná
toxicitu	toxicita	k1gFnSc4	toxicita
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
její	její	k3xOp3gFnSc2	její
použití	použití	k1gNnSc2	použití
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
<g/>
Historie	historie	k1gFnSc1	historie
používání	používání	k1gNnSc2	používání
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
velké	velký	k2eAgFnSc2d1	velká
paniky	panika	k1gFnSc2	panika
okolo	okolo	k7c2	okolo
očkování	očkování	k1gNnSc2	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
knih	kniha	k1gFnPc2	kniha
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
autoři	autor	k1gMnPc1	autor
často	často	k6eAd1	často
citují	citovat	k5eAaBmIp3nP	citovat
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podávání	podávání	k1gNnSc1	podávání
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
autismu	autismus	k1gInSc2	autismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
Autoři	autor	k1gMnPc1	autor
však	však	k9	však
mnohdy	mnohdy	k6eAd1	mnohdy
vůbec	vůbec	k9	vůbec
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
přidávání	přidávání	k1gNnSc1	přidávání
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
do	do	k7c2	do
vakcín	vakcína	k1gFnPc2	vakcína
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k9	již
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výroba	výroba	k1gFnSc1	výroba
chlóru	chlór	k1gInSc2	chlór
===	===	k?	===
</s>
</p>
<p>
<s>
Velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
pro	pro	k7c4	pro
elektrolytickou	elektrolytický	k2eAgFnSc4d1	elektrolytická
výrobu	výroba	k1gFnSc4	výroba
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
energeticky	energeticky	k6eAd1	energeticky
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
znečištění	znečištění	k1gNnSc4	znečištění
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
rtutí	rtuť	k1gFnPc2	rtuť
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
provozuje	provozovat	k5eAaImIp3nS	provozovat
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
například	například	k6eAd1	například
chemička	chemička	k1gFnSc1	chemička
Spolana	Spolana	k1gFnSc1	Spolana
Neratovice	Neratovice	k1gFnSc1	Neratovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
areálu	areál	k1gInSc6	areál
přes	přes	k7c4	přes
250	[number]	k4	250
tun	tuna	k1gFnPc2	tuna
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
kontaminovalo	kontaminovat	k5eAaBmAgNnS	kontaminovat
několik	několik	k4yIc1	několik
výrobních	výrobní	k2eAgInPc2d1	výrobní
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
desítky	desítka	k1gFnSc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
krychlových	krychlový	k2eAgInPc2d1	krychlový
metrů	metr	k1gInPc2	metr
zeminy	zemina	k1gFnSc2	zemina
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
řadami	řada	k1gFnPc7	řada
sloučenin	sloučenina	k1gFnPc2	sloučenina
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
:	:	kIx,	:
Hg	Hg	k1gFnSc1	Hg
<g/>
+	+	kIx~	+
a	a	k8xC	a
Hg2	Hg2	k1gMnSc1	Hg2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
stejně	stejně	k6eAd1	stejně
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
,	,	kIx,	,
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
však	však	k9	však
podstatně	podstatně	k6eAd1	podstatně
jinými	jiný	k2eAgFnPc7d1	jiná
chemickými	chemický	k2eAgFnPc7d1	chemická
a	a	k8xC	a
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
Hg	Hg	k1gMnPc2	Hg
<g/>
+	+	kIx~	+
===	===	k?	===
</s>
</p>
<p>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
chemickým	chemický	k2eAgNnSc7d1	chemické
chováním	chování	k1gNnSc7	chování
připomínají	připomínat	k5eAaImIp3nP	připomínat
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
sloučeninách	sloučenina	k1gFnPc6	sloučenina
rtuť	rtuť	k1gFnSc1	rtuť
dvojvazná	dvojvazný	k2eAgFnSc1d1	dvojvazný
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
Hg-Hg	Hg-Hg	k1gInSc1	Hg-Hg
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typický	typický	k2eAgInSc1d1	typický
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc1	příklad
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
rtuťné	rtuťný	k2eAgFnSc2d1	rtuťný
sloučeniny	sloučenina	k1gFnSc2	sloučenina
–	–	k?	–
chloridu	chlorid	k1gInSc2	chlorid
rtuťného	rtuťný	k2eAgInSc2d1	rtuťný
<g/>
,	,	kIx,	,
kalomelu	kalomel	k1gInSc2	kalomel
Hg2Cl2	Hg2Cl2	k1gFnSc2	Hg2Cl2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
AgCl	AgCl	k1gInSc4	AgCl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
toxický	toxický	k2eAgInSc1d1	toxický
jako	jako	k8xC	jako
všechny	všechen	k3xTgFnPc4	všechen
soli	sůl	k1gFnPc4	sůl
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízké	nízký	k2eAgFnSc3d1	nízká
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
se	se	k3xPyFc4	se
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
do	do	k7c2	do
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
medicínsky	medicínsky	k6eAd1	medicínsky
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xC	jako
projímadlo	projímadlo	k1gNnSc1	projímadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značný	značný	k2eAgInSc4d1	značný
význam	význam	k1gInSc4	význam
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
kalomel	kalomel	k1gInSc1	kalomel
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektrochemii	elektrochemie	k1gFnSc6	elektrochemie
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nejvíce	nejvíce	k6eAd1	nejvíce
používanou	používaný	k2eAgFnSc7d1	používaná
referenční	referenční	k2eAgFnSc7d1	referenční
elektrodou	elektroda	k1gFnSc7	elektroda
kalomelová	kalomelový	k2eAgFnSc1d1	kalomelová
elektroda	elektroda	k1gFnSc1	elektroda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
potenciál	potenciál	k1gInSc4	potenciál
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
neměnný	měnný	k2eNgInSc1d1	neměnný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc7d1	nízká
ale	ale	k8xC	ale
stálou	stálý	k2eAgFnSc7d1	stálá
koncentrací	koncentrace	k1gFnSc7	koncentrace
iontů	ion	k1gInPc2	ion
Hg	Hg	k1gFnSc4	Hg
2+2	[number]	k4	2+2
uvolněných	uvolněný	k2eAgMnPc2d1	uvolněný
z	z	k7c2	z
kalomelu	kalomel	k1gInSc2	kalomel
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
(	(	kIx(	(
<g/>
KCl	KCl	k1gFnSc1	KCl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
roztoku	roztok	k1gInSc2	roztok
rtuťné	rtuťný	k2eAgFnSc2d1	rtuťný
soli	sůl	k1gFnSc2	sůl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dusičnan	dusičnan	k1gInSc1	dusičnan
rtuťný	rtuťný	k2eAgInSc1d1	rtuťný
(	(	kIx(	(
<g/>
Hg2	Hg2	k1gMnSc1	Hg2
<g/>
(	(	kIx(	(
<g/>
NO3	NO3	k1gMnSc1	NO3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
kapka	kapka	k1gFnSc1	kapka
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
nežádoucím	žádoucí	k2eNgInPc3d1	nežádoucí
redoxním	redoxní	k2eAgInPc3d1	redoxní
dějům	děj	k1gInPc3	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
kalomel	kalomel	k1gInSc4	kalomel
v	v	k7c6	v
gravimetrické	gravimetrický	k2eAgFnSc6d1	gravimetrická
analýze	analýza	k1gFnSc6	analýza
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
selektivní	selektivní	k2eAgNnSc1d1	selektivní
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
kyselost	kyselost	k1gFnSc1	kyselost
<g/>
)	)	kIx)	)
redukuje	redukovat	k5eAaBmIp3nS	redukovat
přídavek	přídavek	k1gInSc1	přídavek
kalomelu	kalomel	k1gInSc2	kalomel
různé	různý	k2eAgFnSc2d1	různá
skupiny	skupina	k1gFnSc2	skupina
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
jako	jako	k8xC	jako
platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
rhodium	rhodium	k1gNnSc1	rhodium
a	a	k8xC	a
iridium	iridium	k1gNnSc1	iridium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
Hg2	Hg2	k1gMnPc2	Hg2
<g/>
+	+	kIx~	+
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
chemickým	chemický	k2eAgNnSc7d1	chemické
chováním	chování	k1gNnSc7	chování
připomínají	připomínat	k5eAaImIp3nP	připomínat
měďnaté	měďnatý	k2eAgFnPc4d1	měďnatá
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
chlorid	chlorid	k1gInSc1	chlorid
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
(	(	kIx(	(
<g/>
HgCl2	HgCl2	k1gFnSc1	HgCl2
<g/>
,	,	kIx,	,
sublimát	sublimát	k1gInSc1	sublimát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
a	a	k8xC	a
současně	současně	k6eAd1	současně
mimořádně	mimořádně	k6eAd1	mimořádně
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
HgCl2	HgCl2	k1gFnSc1	HgCl2
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
nedisociuje	disociovat	k5eNaBmIp3nS	disociovat
jako	jako	k9	jako
běžné	běžný	k2eAgFnPc4d1	běžná
iontové	iontový	k2eAgFnPc4d1	iontová
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pouze	pouze	k6eAd1	pouze
solvatované	solvatovaný	k2eAgFnPc1d1	solvatovaný
molekuly	molekula	k1gFnPc1	molekula
HgCl2	HgCl2	k1gFnSc2	HgCl2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sublimát	sublimát	k1gInSc1	sublimát
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
jedů	jed	k1gInPc2	jed
na	na	k7c4	na
hlodavce	hlodavec	k1gMnSc4	hlodavec
a	a	k8xC	a
k	k	k7c3	k
moření	moření	k1gNnSc3	moření
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
setí	setí	k1gNnSc4	setí
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
napuštěna	napuštěn	k2eAgFnSc1d1	napuštěna
roztokem	roztok	k1gInSc7	roztok
sublimátu	sublimát	k1gInSc2	sublimát
a	a	k8xC	a
tak	tak	k6eAd1	tak
chráněna	chránit	k5eAaImNgFnS	chránit
před	před	k7c7	před
hlodavci	hlodavec	k1gMnPc7	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
však	však	k9	však
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
tragickým	tragický	k2eAgInPc3d1	tragický
omylům	omyl	k1gInPc3	omyl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
ošetřené	ošetřený	k2eAgNnSc1d1	ošetřené
obilí	obilí	k1gNnSc1	obilí
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
mlýna	mlýn	k1gInSc2	mlýn
a	a	k8xC	a
pak	pak	k6eAd1	pak
sloužilo	sloužit	k5eAaImAgNnS	sloužit
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
v	v	k7c6	v
pečivu	pečivo	k1gNnSc6	pečivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
(	(	kIx(	(
<g/>
HgS	HgS	k1gFnSc1	HgS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
rumělka	rumělka	k1gFnSc1	rumělka
nejen	nejen	k6eAd1	nejen
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
přírodním	přírodní	k2eAgInSc7d1	přírodní
zdrojem	zdroj	k1gInSc7	zdroj
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
používaným	používaný	k2eAgInSc7d1	používaný
barvířským	barvířský	k2eAgInSc7d1	barvířský
pigmentem	pigment	k1gInSc7	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
přidávám	přidávat	k5eAaImIp1nS	přidávat
i	i	k9	i
do	do	k7c2	do
líčidel	líčidlo	k1gNnPc2	líčidlo
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fulminát	fulminát	k1gInSc1	fulminát
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
(	(	kIx(	(
<g/>
Hg	Hg	k1gFnSc1	Hg
<g/>
(	(	kIx(	(
<g/>
ONC	ONC	kA	ONC
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
třaskavá	třaskavý	k2eAgFnSc1d1	třaskavá
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používaných	používaný	k2eAgFnPc2d1	používaná
pyrotechnických	pyrotechnický	k2eAgFnPc2d1	pyrotechnická
rozbušek	rozbuška	k1gFnPc2	rozbuška
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
třením	tření	k1gNnSc7	tření
<g/>
,	,	kIx,	,
úderem	úder	k1gInSc7	úder
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dimethylrtuť	Dimethylrtuť	k1gFnSc1	Dimethylrtuť
(	(	kIx(	(
<g/>
Hg	Hg	k1gMnSc1	Hg
<g/>
(	(	kIx(	(
<g/>
CH3	CH3	k1gMnSc1	CH3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapalná	kapalný	k2eAgFnSc1d1	kapalná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
rtuti	rtuť	k1gFnSc2	rtuť
za	za	k7c2	za
anaerobních	anaerobní	k2eAgFnPc2d1	anaerobní
podmínek	podmínka	k1gFnPc2	podmínka
působením	působení	k1gNnSc7	působení
mikroorganizmů	mikroorganizmus	k1gInPc2	mikroorganizmus
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobný	podobný	k2eAgInSc4d1	podobný
bod	bod	k1gInSc4	bod
varu	var	k1gInSc2	var
jako	jako	k8xS	jako
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
je	být	k5eAaImIp3nS	být
lipofilní	lipofilní	k2eAgNnSc1d1	lipofilní
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc1d3	nejznámější
otrava	otrava	k1gFnSc1	otrava
dimethylrtutí	dimethylrtutí	k1gNnSc2	dimethylrtutí
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
zátoce	zátoka	k1gFnSc6	zátoka
Minamata	Minama	k1gNnPc4	Minama
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
tisíce	tisíc	k4xCgInPc1	tisíc
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
==	==	k?	==
</s>
</p>
<p>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kumulativním	kumulativní	k2eAgInSc7d1	kumulativní
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
chovající	chovající	k2eAgNnSc1d1	chovající
kadmium	kadmium	k1gNnSc1	kadmium
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pozvolna	pozvolna	k6eAd1	pozvolna
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
především	především	k9	především
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
slezině	slezina	k1gFnSc6	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rtuť	rtuť	k1gFnSc1	rtuť
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
setrvat	setrvat	k5eAaPmF	setrvat
až	až	k6eAd1	až
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
chronické	chronický	k2eAgFnSc6d1	chronická
otravě	otrava	k1gFnSc6	otrava
rtutí	rtuť	k1gFnPc2	rtuť
nejvíce	nejvíce	k6eAd1	nejvíce
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Projevy	projev	k1gInPc1	projev
chronické	chronický	k2eAgFnSc2d1	chronická
otravy	otrava	k1gFnSc2	otrava
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
nespecifické	specifický	k2eNgFnSc2d1	nespecifická
–	–	k?	–
od	od	k7c2	od
studených	studený	k2eAgFnPc2d1	studená
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
vypadávání	vypadávání	k1gNnSc1	vypadávání
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
zažívací	zažívací	k2eAgFnPc4d1	zažívací
poruchy	porucha	k1gFnPc4	porucha
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
neurologické	neurologický	k2eAgFnPc4d1	neurologická
a	a	k8xC	a
psychické	psychický	k2eAgFnPc4d1	psychická
potíže	potíž	k1gFnPc4	potíž
až	až	k9	až
po	po	k7c4	po
závažné	závažný	k2eAgInPc4d1	závažný
stavy	stav	k1gInPc4	stav
jako	jako	k8xS	jako
např.	např.	kA	např.
chudokrevnost	chudokrevnost	k1gFnSc1	chudokrevnost
<g/>
,	,	kIx,	,
léčbě	léčba	k1gFnSc6	léčba
odporující	odporující	k2eAgFnSc1d1	odporující
chronická	chronický	k2eAgFnSc1d1	chronická
candidóza	candidóza	k1gFnSc1	candidóza
<g/>
,	,	kIx,	,
revmatické	revmatický	k2eAgFnPc4d1	revmatická
choroby	choroba	k1gFnPc4	choroba
či	či	k8xC	či
onemocnění	onemocnění	k1gNnPc4	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednorázové	jednorázový	k2eAgFnSc6d1	jednorázová
vysoké	vysoký	k2eAgFnSc6d1	vysoká
dávce	dávka	k1gFnSc6	dávka
rtuti	rtuť	k1gFnSc2	rtuť
se	se	k3xPyFc4	se
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
bolesti	bolest	k1gFnPc1	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
průjmy	průjem	k1gInPc7	průjem
a	a	k8xC	a
zvracení	zvracení	k1gNnPc2	zvracení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
se	se	k3xPyFc4	se
rtuť	rtuť	k1gFnSc1	rtuť
dostává	dostávat	k5eAaImIp3nS	dostávat
především	především	k9	především
dvěma	dva	k4xCgNnPc7	dva
cestami	cesta	k1gFnPc7	cesta
–	–	k?	–
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
a	a	k8xC	a
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
potravin	potravina	k1gFnPc2	potravina
jsou	být	k5eAaImIp3nP	být
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
především	především	k9	především
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
(	(	kIx(	(
<g/>
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc1	ledvina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
kontaminovány	kontaminovat	k5eAaBmNgInP	kontaminovat
rtutí	rtuť	k1gFnSc7	rtuť
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
růstu	růst	k1gInSc6	růst
<g/>
.	.	kIx.	.
</s>
<s>
Rizikové	rizikový	k2eAgInPc1d1	rizikový
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
plodiny	plodina	k1gFnPc1	plodina
<g/>
,	,	kIx,	,
pěstované	pěstovaný	k2eAgFnPc1d1	pěstovaná
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
zamořené	zamořený	k2eAgInPc1d1	zamořený
rtuťnatými	rtuťnatý	k2eAgFnPc7d1	rtuťnatý
sloučeninami	sloučenina	k1gFnPc7	sloučenina
ať	ať	k8xS	ať
již	již	k9	již
z	z	k7c2	z
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
zdrojů	zdroj	k1gInPc2	zdroj
nebo	nebo	k8xC	nebo
nevhodně	vhodně	k6eNd1	vhodně
použitými	použitý	k2eAgInPc7d1	použitý
přípravky	přípravek	k1gInPc7	přípravek
k	k	k7c3	k
hubení	hubení	k1gNnSc3	hubení
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
rtuť	rtuť	k1gFnSc1	rtuť
je	být	k5eAaImIp3nS	být
zdraví	zdraví	k1gNnSc4	zdraví
člověka	člověk	k1gMnSc2	člověk
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vdechování	vdechování	k1gNnSc2	vdechování
jejích	její	k3xOp3gFnPc2	její
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
přechovávat	přechovávat	k5eAaImF	přechovávat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nelze	lze	k6eNd1	lze
uzavřít	uzavřít	k5eAaPmF	uzavřít
do	do	k7c2	do
utěsněné	utěsněný	k2eAgFnSc2d1	utěsněná
nádoby	nádoba	k1gFnSc2	nádoba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
polarografické	polarografický	k2eAgInPc1d1	polarografický
rezervoáry	rezervoár	k1gInPc1	rezervoár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překrytou	překrytý	k2eAgFnSc7d1	překrytá
vrstvou	vrstva	k1gFnSc7	vrstva
destilované	destilovaný	k2eAgFnSc2d1	destilovaná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kritickým	kritický	k2eAgInSc7d1	kritický
orgánem	orgán	k1gInSc7	orgán
při	při	k7c6	při
akutním	akutní	k2eAgNnSc6d1	akutní
vystavení	vystavení	k1gNnSc6	vystavení
parám	para	k1gFnPc3	para
rtuti	rtuť	k1gFnSc2	rtuť
jsou	být	k5eAaImIp3nP	být
plíce	plíce	k1gFnPc1	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
erozivní	erozivní	k2eAgFnSc1d1	erozivní
bronchitida	bronchitida	k1gFnSc1	bronchitida
a	a	k8xC	a
postižený	postižený	k2eAgMnSc1d1	postižený
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
zemřít	zemřít	k5eAaPmF	zemřít
na	na	k7c4	na
respirační	respirační	k2eAgNnSc4d1	respirační
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
provázeny	provázet	k5eAaImNgInP	provázet
také	také	k6eAd1	také
příznaky	příznak	k1gInPc1	příznak
poškození	poškození	k1gNnSc1	poškození
centrálního	centrální	k2eAgInSc2d1	centrální
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páry	pár	k1gInPc1	pár
elementární	elementární	k2eAgFnSc2d1	elementární
rtuti	rtuť	k1gFnSc2	rtuť
totiž	totiž	k9	totiž
snadno	snadno	k6eAd1	snadno
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
za	za	k7c4	za
hematoencefalickou	hematoencefalický	k2eAgFnSc4d1	hematoencefalická
bariéru	bariéra	k1gFnSc4	bariéra
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
vyškolení	vyškolený	k2eAgMnPc1d1	vyškolený
odborníci	odborník	k1gMnPc1	odborník
větší	veliký	k2eAgNnPc4d2	veliký
množství	množství	k1gNnPc4	množství
rozlité	rozlitý	k2eAgFnSc2d1	rozlitá
rtuti	rtuť	k1gFnSc2	rtuť
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
v	v	k7c6	v
protichemických	protichemický	k2eAgInPc6d1	protichemický
oblecích	oblek	k1gInPc6	oblek
vybavených	vybavený	k2eAgFnPc2d1	vybavená
dýchacími	dýchací	k2eAgInPc7d1	dýchací
přístroji	přístroj	k1gInPc7	přístroj
<g/>
.	.	kIx.	.
<g/>
Zvláště	zvláště	k6eAd1	zvláště
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
jsou	být	k5eAaImIp3nP	být
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
snadno	snadno	k6eAd1	snadno
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
živých	živý	k2eAgFnPc2d1	živá
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
i	i	k9	i
pouhým	pouhý	k2eAgInSc7d1	pouhý
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
např.	např.	kA	např.
rozkladem	rozklad	k1gInSc7	rozklad
různých	různý	k2eAgFnPc2d1	různá
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
rtuti	rtuť	k1gFnSc2	rtuť
nebo	nebo	k8xC	nebo
i	i	k9	i
metabolickými	metabolický	k2eAgInPc7d1	metabolický
pochody	pochod	k1gInPc7	pochod
mikroorganizmů	mikroorganizmus	k1gInPc2	mikroorganizmus
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
uváděným	uváděný	k2eAgInSc7d1	uváděný
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
dimethylrtuť	dimethylrtuť	k1gFnSc1	dimethylrtuť
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
CH3	CH3	k1gFnSc1	CH3
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
Hg	Hg	k1gFnSc1	Hg
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
CH3	CH3	k1gFnSc1	CH3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
pro	pro	k7c4	pro
dospělého	dospělý	k2eAgMnSc4d1	dospělý
člověka	člověk	k1gMnSc4	člověk
uváděno	uvádět	k5eAaImNgNnS	uvádět
již	již	k9	již
0,1	[number]	k4	0,1
ml	ml	kA	ml
této	tento	k3xDgFnSc2	tento
kapalné	kapalný	k2eAgFnSc2d1	kapalná
substance	substance	k1gFnSc2	substance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sporná	sporný	k2eAgFnSc1d1	sporná
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
působení	působení	k1gNnSc2	působení
amalgámových	amalgámový	k2eAgFnPc2d1	amalgámová
zubních	zubní	k2eAgFnPc2d1	zubní
plomb	plomba	k1gFnPc2	plomba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
někteří	některý	k3yIgMnPc1	některý
lékaři	lékař	k1gMnPc1	lékař
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
neškodné	škodný	k2eNgMnPc4d1	neškodný
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
glomerulopatie	glomerulopatie	k1gFnPc4	glomerulopatie
a	a	k8xC	a
autoimunitní	autoimunitní	k2eAgNnPc1d1	autoimunitní
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
expozici	expozice	k1gFnSc3	expozice
rtuti	rtuť	k1gFnSc2	rtuť
popsány	popsat	k5eAaPmNgInP	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Popisované	popisovaný	k2eAgFnPc1d1	popisovaná
hypersenzitivní	hypersenzitivní	k2eAgFnPc1d1	hypersenzitivní
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
rtuť	rtuť	k1gFnSc4	rtuť
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
celkovými	celkový	k2eAgInPc7d1	celkový
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
vyrážkou	vyrážka	k1gFnSc7	vyrážka
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
<g/>
,	,	kIx,	,
na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
ohybu	ohyb	k1gInSc2	ohyb
končetin	končetina	k1gFnPc2	končetina
(	(	kIx(	(
<g/>
flexní	flexní	k2eAgFnSc2d1	flexní
rýhy	rýha	k1gFnSc2	rýha
<g/>
)	)	kIx)	)
končetin	končetina	k1gFnPc2	končetina
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
bylo	být	k5eAaImAgNnS	být
</s>
</p>
<p>
<s>
i	i	k9	i
nespecifické	specifický	k2eNgNnSc1d1	nespecifické
poškození	poškození	k1gNnSc1	poškození
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
zvané	zvaný	k2eAgFnSc2d1	zvaná
lichen	lichna	k1gFnPc2	lichna
ruber	rubrum	k1gNnPc2	rubrum
planus	planus	k1gMnSc1	planus
<g/>
.	.	kIx.	.
</s>
<s>
Problematický	problematický	k2eAgInSc1d1	problematický
je	být	k5eAaImIp3nS	být
však	však	k9	však
především	především	k9	především
osud	osud	k1gInSc4	osud
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
při	při	k7c6	při
zpopelňování	zpopelňování	k1gNnSc6	zpopelňování
těchto	tento	k3xDgFnPc2	tento
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
krematoriích	krematorium	k1gNnPc6	krematorium
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
častější	častý	k2eAgInSc1d2	častější
způsob	způsob	k1gInSc1	způsob
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1 100	[number]	k4	1 100
tun	tuna	k1gFnPc2	tuna
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
končí	končit	k5eAaImIp3nS	končit
jen	jen	k9	jen
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
asi	asi	k9	asi
30	[number]	k4	30
tun	tuna	k1gFnPc2	tuna
rtuti	rtuť	k1gFnSc2	rtuť
ze	z	k7c2	z
zubních	zubní	k2eAgInPc2d1	zubní
amalgámů	amalgám	k1gInPc2	amalgám
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
24	[number]	k4	24
tun	tuna	k1gFnPc2	tuna
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
23	[number]	k4	23
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
.	.	kIx.	.
<g/>
Toxicita	toxicita	k1gFnSc1	toxicita
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
především	především	k9	především
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
rozpustnosti	rozpustnost	k1gFnSc6	rozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
jsou	být	k5eAaImIp3nP	být
nejvíce	nejvíce	k6eAd1	nejvíce
rizikové	rizikový	k2eAgFnPc1d1	riziková
sloučeniny	sloučenina	k1gFnPc1	sloučenina
dvojmocné	dvojmocný	k2eAgFnSc2d1	dvojmocná
rtuti	rtuť	k1gFnSc2	rtuť
Hg2	Hg2	k1gFnSc2	Hg2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
bývaly	bývat	k5eAaImAgInP	bývat
<g/>
,	,	kIx,	,
užívány	užíván	k2eAgInPc1d1	užíván
jako	jako	k8xS	jako
jedy	jed	k1gInPc1	jed
na	na	k7c6	na
hubení	hubení	k1gNnSc6	hubení
hlodavců	hlodavec	k1gMnPc2	hlodavec
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebezpečnost	nebezpečnost	k1gFnSc1	nebezpečnost
elementární	elementární	k2eAgFnSc2d1	elementární
rtuti	rtuť	k1gFnSc2	rtuť
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
se	se	k3xPyFc4	se
cca	cca	kA	cca
0,01	[number]	k4	0,01
%	%	kIx~	%
požité	požitý	k2eAgFnSc2d1	požitá
rtuti	rtuť	k1gFnSc2	rtuť
a	a	k8xC	a
pokud	pokud	k8xS	pokud
rtuť	rtuť	k1gFnSc1	rtuť
nesetrvá	setrvat	k5eNaPmIp3nS	setrvat
v	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
požívána	požívat	k5eAaImNgFnS	požívat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
žádné	žádný	k3yNgInPc4	žádný
toxické	toxický	k2eAgInPc4d1	toxický
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
<g/>
Minimální	minimální	k2eAgFnSc1d1	minimální
škodlivost	škodlivost	k1gFnSc1	škodlivost
elementární	elementární	k2eAgFnSc2d1	elementární
rtuti	rtuť	k1gFnSc2	rtuť
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
kuriózní	kuriózní	k2eAgInSc1d1	kuriózní
příklad	příklad	k1gInSc1	příklad
nepovedené	povedený	k2eNgFnSc2d1	nepovedená
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
potenciální	potenciální	k2eAgMnSc1d1	potenciální
sebevrah	sebevrah	k1gMnSc1	sebevrah
vstříkl	vstříknout	k5eAaPmAgMnS	vstříknout
injekčně	injekčně	k6eAd1	injekčně
několik	několik	k4yIc4	několik
mililitrů	mililitr	k1gInPc2	mililitr
rtuti	rtuť	k1gFnSc2	rtuť
do	do	k7c2	do
žíly	žíla	k1gFnSc2	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pH	ph	kA	ph
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
naprosto	naprosto	k6eAd1	naprosto
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
"	"	kIx"	"
<g/>
sebevraha	sebevrah	k1gMnSc4	sebevrah
<g/>
"	"	kIx"	"
a	a	k8xC	a
on	on	k3xPp3gInSc1	on
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
žil	žít	k5eAaImAgMnS	žít
ještě	ještě	k6eAd1	ještě
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Nejtoxičtější	toxický	k2eAgFnSc1d3	nejtoxičtější
je	být	k5eAaImIp3nS	být
rtuť	rtuť	k1gFnSc1	rtuť
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
organosloučenin	organosloučenina	k1gFnPc2	organosloučenina
(	(	kIx(	(
<g/>
methylrtuť	methylrtuť	k1gFnSc1	methylrtuť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
rybách	ryba	k1gFnPc6	ryba
a	a	k8xC	a
organismus	organismus	k1gInSc4	organismus
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ji	on	k3xPp3gFnSc4	on
přijmout	přijmout	k5eAaPmF	přijmout
téměř	téměř	k6eAd1	téměř
ze	z	k7c2	z
sta	sto	k4xCgNnSc2	sto
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
neurologické	neurologický	k2eAgFnPc4d1	neurologická
poruchy	porucha	k1gFnPc4	porucha
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc4	porucha
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
svalovou	svalový	k2eAgFnSc7d1	svalová
slabost	slabost	k1gFnSc4	slabost
<g/>
,	,	kIx,	,
únavu	únava	k1gFnSc4	únava
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
reprodukční	reprodukční	k2eAgFnPc4d1	reprodukční
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
placentou	placenta	k1gFnSc7	placenta
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
psychomotorické	psychomotorický	k2eAgNnSc1d1	psychomotorické
poškození	poškození	k1gNnSc1	poškození
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekologická	ekologický	k2eAgNnPc4d1	ekologické
rizika	riziko	k1gNnPc4	riziko
==	==	k?	==
</s>
</p>
<p>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
vypuštěná	vypuštěný	k2eAgFnSc1d1	vypuštěná
do	do	k7c2	do
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
představuje	představovat	k5eAaImIp3nS	představovat
vážné	vážný	k2eAgNnSc4d1	vážné
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
putovat	putovat	k5eAaImF	putovat
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
kontaminovat	kontaminovat	k5eAaBmF	kontaminovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
i	i	k9	i
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
snižovat	snižovat	k5eAaImF	snižovat
znečištění	znečištění	k1gNnSc4	znečištění
rtutí	rtuť	k1gFnPc2	rtuť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gNnSc1	její
vypouštěné	vypouštěný	k2eAgNnSc1d1	vypouštěné
množství	množství	k1gNnSc1	množství
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Vážná	vážná	k1gFnSc1	vážná
ohrožení	ohrožení	k1gNnSc2	ohrožení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
představuje	představovat	k5eAaImIp3nS	představovat
zejména	zejména	k9	zejména
používání	používání	k1gNnSc1	používání
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
RoHS	RoHS	k1gMnPc5	RoHS
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
nebezpečnosti	nebezpečnost	k1gFnSc3	nebezpečnost
je	být	k5eAaImIp3nS	být
omezeno	omezen	k2eAgNnSc1d1	omezeno
používání	používání	k1gNnSc1	používání
rtuti	rtuť	k1gFnSc2	rtuť
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
elektronických	elektronický	k2eAgInPc6d1	elektronický
a	a	k8xC	a
elektrických	elektrický	k2eAgInPc6d1	elektrický
zařízeních	zařízení	k1gNnPc6	zařízení
směrnicí	směrnice	k1gFnSc7	směrnice
RoHS	RoHS	k1gFnSc3	RoHS
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
olovem	olovo	k1gNnSc7	olovo
<g/>
,	,	kIx,	,
kadmiem	kadmium	k1gNnSc7	kadmium
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jursík	Jursík	k1gMnSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Greenwood	Greenwood	k1gInSc1	Greenwood
N.N.	N.N.	k1gMnSc2	N.N.
<g/>
,	,	kIx,	,
Earnshaw	Earnshaw	k1gMnSc2	Earnshaw
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
II	II	kA	II
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Cikrt	Cikrt	k1gInSc1	Cikrt
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lener	Lenra	k1gFnPc2	Lenra
<g/>
:	:	kIx,	:
Toxické	toxický	k2eAgInPc1d1	toxický
kovy	kov	k1gInPc1	kov
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
a	a	k8xC	a
pracovním	pracovní	k2eAgNnSc6d1	pracovní
prostředí	prostředí	k1gNnSc6	prostředí
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7169-150-X	[number]	k4	80-7169-150-X
</s>
</p>
<p>
<s>
Handbook	handbook	k1gInSc1	handbook
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Toxicology	Toxicolog	k1gMnPc4	Toxicolog
of	of	k?	of
Metals	Metalsa	k1gFnPc2	Metalsa
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ročenka	ročenka	k1gFnSc1	ročenka
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
a	a	k8xC	a
ČEú	ČEú	k1gFnSc1	ČEú
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Píša	Píša	k1gMnSc1	Píša
<g/>
:	:	kIx,	:
Narušení	narušení	k1gNnSc1	narušení
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
procesů	proces	k1gInPc2	proces
působením	působení	k1gNnSc7	působení
kadmia	kadmium	k1gNnSc2	kadmium
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
in	in	k?	in
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Cibulka	Cibulka	k1gMnSc1	Cibulka
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
v	v	k7c6	v
biosféře	biosféra	k1gFnSc6	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Akademia	Akademia	k1gFnSc1	Akademia
Praha	Praha	k1gFnSc1	Praha
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
H.	H.	kA	H.
Pohunková	Pohunková	k1gFnSc1	Pohunková
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Reisnerová	Reisnerová	k1gFnSc1	Reisnerová
<g/>
:	:	kIx,	:
Vliv	vliv	k1gInSc1	vliv
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
orgánech	orgán	k1gInPc6	orgán
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
<g/>
in	in	k?	in
<g/>
:	:	kIx,	:
Pohyb	pohyb	k1gInSc1	pohyb
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
rtuti	rtuť	k1gFnSc2	rtuť
v	v	k7c6	v
biosféře	biosféra	k1gFnSc6	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1991.	[number]	k4	1991.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rtuť	rtuť	k1gFnSc1	rtuť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rtuť	rtuť	k1gFnSc1	rtuť
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Pavel	Pavel	k1gMnSc1	Pavel
Urban	Urban	k1gMnSc1	Urban
<g/>
:	:	kIx,	:
Aktuální	aktuální	k2eAgInPc4d1	aktuální
problémy	problém	k1gInPc4	problém
neurotoxicity	neurotoxicita	k1gFnSc2	neurotoxicita
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
Neurológia	Neurológia	k1gFnSc1	Neurológia
pre	pre	k?	pre
prax	prax	k1gInSc1	prax
<g/>
,	,	kIx,	,
5/2006	[number]	k4	5/2006
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
–	–	k?	–
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
:	:	kIx,	:
Kauza	kauza	k1gFnSc1	kauza
amalgám	amalgám	k1gInSc1	amalgám
–	–	k?	–
amalgámová	amalgámový	k2eAgFnSc1d1	amalgámová
výplň	výplň	k1gFnSc1	výplň
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
6.	[number]	k4	6.
<g/>
9.2005	[number]	k4	9.2005
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Mlčoch	Mlčoch	k1gMnSc1	Mlčoch
<g/>
:	:	kIx,	:
Otrava	otrava	k1gMnSc1	otrava
(	(	kIx(	(
<g/>
intoxikace	intoxikace	k1gFnSc1	intoxikace
<g/>
)	)	kIx)	)
rtutí	rtuť	k1gFnPc2	rtuť
–	–	k?	–
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
<g/>
,	,	kIx,	,
prevence	prevence	k1gFnSc1	prevence
<g/>
,	,	kIx,	,
amalgám	amalgám	k1gInSc1	amalgám
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Environmental	Environmental	k1gMnSc1	Environmental
Protection	Protection	k1gInSc4	Protection
Agency	Agenca	k1gFnSc2	Agenca
<g/>
:	:	kIx,	:
Mercury	Mercura	k1gFnSc2	Mercura
–	–	k?	–
Spills	Spills	k1gInSc1	Spills
<g/>
,	,	kIx,	,
Disposal	Disposal	k1gFnSc1	Disposal
and	and	k?	and
Site	Site	k1gInSc1	Site
Cleanup	Cleanup	k1gInSc1	Cleanup
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Environmental	Environmental	k1gMnSc1	Environmental
Protection	Protection	k1gInSc4	Protection
Agency	Agenca	k1gFnSc2	Agenca
<g/>
:	:	kIx,	:
Initial	Initial	k1gInSc1	Initial
Risk-Based	Risk-Based	k1gInSc1	Risk-Based
Prioritization	Prioritization	k1gInSc1	Prioritization
of	of	k?	of
Mercury	Mercura	k1gFnSc2	Mercura
in	in	k?	in
Certain	Certain	k2eAgInSc1d1	Certain
Products	Products	k1gInSc1	Products
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
UNEP	UNEP	kA	UNEP
<g/>
:	:	kIx,	:
UNEP	UNEP	kA	UNEP
Global	globat	k5eAaImAgInS	globat
Mercury	Mercura	k1gFnSc2	Mercura
Partnership	Partnership	k1gInSc1	Partnership
</s>
</p>
