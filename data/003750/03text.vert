<s>
Data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
informace	informace	k1gFnSc2	informace
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
(	(	kIx(	(
<g/>
číselné	číselný	k2eAgFnSc6d1	číselná
<g/>
)	)	kIx)	)
podobě	podoba	k1gFnSc6	podoba
určené	určený	k2eAgFnSc6d1	určená
k	k	k7c3	k
počítačovému	počítačový	k2eAgNnSc3d1	počítačové
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
obrázek	obrázek	k1gInSc1	obrázek
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
programu	program	k1gInSc2	program
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zapsána	zapsat	k5eAaPmNgNnP	zapsat
(	(	kIx(	(
<g/>
kódována	kódovat	k5eAaBmNgFnS	kódovat
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
posloupností	posloupnost	k1gFnPc2	posloupnost
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
a	a	k8xC	a
uloženy	uložit	k5eAaPmNgInP	uložit
např.	např.	kA	např.
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
počítače	počítač	k1gInSc2	počítač
nebo	nebo	k8xC	nebo
na	na	k7c6	na
záznamovém	záznamový	k2eAgNnSc6d1	záznamové
médiu	médium	k1gNnSc6	médium
(	(	kIx(	(
<g/>
pevný	pevný	k2eAgInSc1d1	pevný
disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
paměťová	paměťový	k2eAgFnSc1d1	paměťová
karta	karta	k1gFnSc1	karta
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
vedle	vedle	k7c2	vedle
dat	datum	k1gNnPc2	datum
uložen	uložen	k2eAgMnSc1d1	uložen
i	i	k8xC	i
sled	sled	k1gInSc1	sled
instrukcí	instrukce	k1gFnPc2	instrukce
tvořící	tvořící	k2eAgInSc1d1	tvořící
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
počítač	počítač	k1gInSc4	počítač
data	datum	k1gNnSc2	datum
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
definice	definice	k1gFnPc1	definice
<g/>
:	:	kIx,	:
Data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
vyjádření	vyjádření	k1gNnPc4	vyjádření
(	(	kIx(	(
<g/>
reprezentace	reprezentace	k1gFnSc1	reprezentace
<g/>
)	)	kIx)	)
informace	informace	k1gFnSc2	informace
formálním	formální	k2eAgInSc7d1	formální
způsobem	způsob	k1gInSc7	způsob
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
přenášet	přenášet	k5eAaImF	přenášet
nebo	nebo	k8xC	nebo
zpracovat	zpracovat	k5eAaPmF	zpracovat
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
posloupnost	posloupnost	k1gFnSc4	posloupnost
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
je	být	k5eAaImIp3nS	být
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
určitá	určitý	k2eAgFnSc1d1	určitá
interpretace	interpretace	k1gFnSc1	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
digitálních	digitální	k2eAgNnPc2d1	digitální
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
odhadnut	odhadnout	k5eAaPmNgInS	odhadnout
na	na	k7c4	na
281	[number]	k4	281
bilionů	bilion	k4xCgInPc2	bilion
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1	reprezentace
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
digitálních	digitální	k2eAgInPc6d1	digitální
počítačích	počítač	k1gInPc6	počítač
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
binární	binární	k2eAgMnSc1d1	binární
<g/>
:	:	kIx,	:
data	datum	k1gNnSc2	datum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvukové	zvukový	k2eAgInPc4d1	zvukový
záznamy	záznam	k1gInPc4	záznam
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
sérií	série	k1gFnSc7	série
binárních	binární	k2eAgFnPc2d1	binární
číslic	číslice	k1gFnPc2	číslice
(	(	kIx(	(
<g/>
bitů	bit	k1gInPc2	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
osm	osm	k4xCc1	osm
bitů	bit	k1gInPc2	bit
seskupuje	seskupovat	k5eAaImIp3nS	seskupovat
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bajtu	bajt	k1gInSc2	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
a	a	k8xC	a
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
jako	jako	k9	jako
posloupnosti	posloupnost	k1gFnPc1	posloupnost
bajtů	bajt	k1gInPc2	bajt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidsky	lidsky	k6eAd1	lidsky
přívětivější	přívětivý	k2eAgNnSc1d2	přívětivější
je	být	k5eAaImIp3nS	být
uvažovat	uvažovat	k5eAaImF	uvažovat
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
úrovni	úroveň	k1gFnSc6	úroveň
abstrakce	abstrakce	k1gFnSc2	abstrakce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
tento	tento	k3xDgInSc1	tento
soubor	soubor	k1gInSc1	soubor
je	být	k5eAaImIp3nS	být
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
formátu	formát	k1gInSc6	formát
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
být	být	k5eAaImF	být
korektně	korektně	k6eAd1	korektně
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
kódu	kód	k1gInSc6	kód
a	a	k8xC	a
dodržovat	dodržovat	k5eAaImF	dodržovat
určitý	určitý	k2eAgInSc4d1	určitý
formát	formát	k1gInSc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc4	kód
a	a	k8xC	a
formát	formát	k1gInSc4	formát
dat	datum	k1gNnPc2	datum
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgInPc1d1	blízký
pojmy	pojem	k1gInPc1	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
elementární	elementární	k2eAgFnSc6d1	elementární
úrovni	úroveň	k1gFnSc6	úroveň
obvykle	obvykle	k6eAd1	obvykle
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
kódu	kód	k1gInSc6	kód
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
o	o	k7c4	o
kódování	kódování	k1gNnSc4	kódování
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
obecněji	obecně	k6eAd2	obecně
o	o	k7c6	o
znakové	znakový	k2eAgFnSc6d1	znaková
sadě	sada	k1gFnSc6	sada
(	(	kIx(	(
<g/>
reprezentaci	reprezentace	k1gFnSc6	reprezentace
znaků	znak	k1gInPc2	znak
nějaké	nějaký	k3yIgFnSc2	nějaký
abecedy	abeceda	k1gFnSc2	abeceda
čísly	čísnout	k5eAaPmAgFnP	čísnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
endianita	endianita	k1gFnSc1	endianita
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
uložení	uložení	k1gNnSc2	uložení
čísel	číslo	k1gNnPc2	číslo
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgNnSc6	jaký
pořadí	pořadí	k1gNnSc6	pořadí
se	se	k3xPyFc4	se
uloží	uložit	k5eAaPmIp3nP	uložit
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
bajty	bajt	k1gInPc1	bajt
číselného	číselný	k2eAgInSc2d1	číselný
datového	datový	k2eAgInSc2d1	datový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Strukturu	struktura	k1gFnSc4	struktura
vyšší	vysoký	k2eAgFnSc2d2	vyšší
úrovně	úroveň	k1gFnSc2	úroveň
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k8xC	jako
formát	formát	k1gInSc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
značkovací	značkovací	k2eAgInSc1d1	značkovací
jazyk	jazyk	k1gInSc1	jazyk
HTML	HTML	kA	HTML
nebo	nebo	k8xC	nebo
formáty	formát	k1gInPc1	formát
dokumentů	dokument	k1gInPc2	dokument
(	(	kIx(	(
<g/>
formát	formát	k1gInSc1	formát
textového	textový	k2eAgInSc2d1	textový
procesoru	procesor	k1gInSc2	procesor
Microsoft	Microsoft	kA	Microsoft
Word	Word	kA	Word
<g/>
,	,	kIx,	,
OpenPocument	OpenPocument	k1gMnSc1	OpenPocument
<g/>
,	,	kIx,	,
Office	Office	kA	Office
Open	Openo	k1gNnPc2	Openo
XML	XML	kA	XML
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgInPc4d1	grafický
formáty	formát	k1gInPc4	formát
<g/>
.	.	kIx.	.
</s>
<s>
Formát	formát	k1gInSc1	formát
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
standardizován	standardizován	k2eAgMnSc1d1	standardizován
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
počítače	počítač	k1gInPc1	počítač
a	a	k8xC	a
programy	program	k1gInPc1	program
různých	různý	k2eAgMnPc2d1	různý
výrobců	výrobce	k1gMnPc2	výrobce
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
formátu	formát	k1gInSc2	formát
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
k	k	k7c3	k
většině	většina	k1gFnSc3	většina
manipulací	manipulace	k1gFnPc2	manipulace
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Neveřejné	veřejný	k2eNgNnSc1d1	neveřejné
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
binární	binární	k2eAgInPc1d1	binární
<g/>
)	)	kIx)	)
formáty	formát	k1gInPc1	formát
nutí	nutit	k5eAaImIp3nP	nutit
uživatele	uživatel	k1gMnPc4	uživatel
používat	používat	k5eAaImF	používat
programy	program	k1gInPc1	program
určitého	určitý	k2eAgMnSc2d1	určitý
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgInPc1d1	otevřený
formáty	formát	k1gInPc1	formát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
textové	textový	k2eAgInPc1d1	textový
<g/>
,	,	kIx,	,
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
tuto	tento	k3xDgFnSc4	tento
nepříjemnost	nepříjemnost	k1gFnSc4	nepříjemnost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
formátem	formát	k1gInSc7	formát
dat	datum	k1gNnPc2	datum
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
rozumí	rozumět	k5eAaImIp3nS	rozumět
formát	formát	k1gInSc4	formát
souboru	soubor	k1gInSc2	soubor
<g/>
;	;	kIx,	;
formát	formát	k1gInSc1	formát
ovšem	ovšem	k9	ovšem
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
ukládána	ukládán	k2eAgNnPc1d1	ukládáno
nebo	nebo	k8xC	nebo
předávána	předáván	k2eAgNnPc1d1	předáváno
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
například	například	k6eAd1	například
u	u	k7c2	u
komunikačních	komunikační	k2eAgInPc2d1	komunikační
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
archivu	archiv	k1gInSc2	archiv
na	na	k7c6	na
magnetické	magnetický	k2eAgFnSc6d1	magnetická
pásce	páska	k1gFnSc6	páska
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
uložení	uložení	k1gNnSc4	uložení
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
datům	datum	k1gNnPc3	datum
(	(	kIx(	(
<g/>
sekvencím	sekvence	k1gFnPc3	sekvence
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
taktéž	taktéž	k?	taktéž
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
<g/>
;	;	kIx,	;
prostředkem	prostředek	k1gInSc7	prostředek
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
metadata	metadat	k2eAgNnPc1d1	metadat
<g/>
.	.	kIx.	.
</s>
<s>
Metadata	Metade	k1gNnPc1	Metade
jsou	být	k5eAaImIp3nP	být
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
jiných	jiný	k2eAgNnPc6d1	jiné
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Metadata	Metadata	k1gFnSc1	Metadata
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyhledání	vyhledání	k1gNnSc3	vyhledání
nebo	nebo	k8xC	nebo
zpracování	zpracování	k1gNnSc4	zpracování
popisovaných	popisovaný	k2eAgNnPc2d1	popisované
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Dávají	dávat	k5eAaImIp3nP	dávat
návod	návod	k1gInSc4	návod
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
interpretaci	interpretace	k1gFnSc3	interpretace
<g/>
,	,	kIx,	,
při	při	k7c6	při
počítačovém	počítačový	k2eAgNnSc6d1	počítačové
zpracování	zpracování	k1gNnSc6	zpracování
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
metadat	metadat	k5eAaPmF	metadat
automaticky	automaticky	k6eAd1	automaticky
přiřadit	přiřadit	k5eAaPmF	přiřadit
k	k	k7c3	k
popisovaným	popisovaný	k2eAgNnPc3d1	popisované
datům	datum	k1gNnPc3	datum
algoritmus	algoritmus	k1gInSc4	algoritmus
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
data	datum	k1gNnPc4	datum
správně	správně	k6eAd1	správně
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
a	a	k8xC	a
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
metadat	metadat	k5eAaImF	metadat
je	být	k5eAaImIp3nS	být
přípona	přípona	k1gFnSc1	přípona
souboru	soubor	k1gInSc2	soubor
nebo	nebo	k8xC	nebo
hlavička	hlavička	k1gFnSc1	hlavička
souboru	soubor	k1gInSc2	soubor
nebo	nebo	k8xC	nebo
MIME	mim	k1gMnSc5	mim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
papírový	papírový	k2eAgInSc4d1	papírový
katalogizační	katalogizační	k2eAgInSc4d1	katalogizační
lístek	lístek	k1gInSc4	lístek
v	v	k7c6	v
neautomatizované	automatizovaný	k2eNgFnSc6d1	neautomatizovaná
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
data	datum	k1gNnPc4	datum
výrazně	výrazně	k6eAd1	výrazně
redundantní	redundantní	k2eAgMnSc1d1	redundantní
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
expert	expert	k1gMnSc1	expert
pohledem	pohled	k1gInSc7	pohled
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
jsou	být	k5eAaImIp3nP	být
formátu	formát	k1gInSc3	formát
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
např.	např.	kA	např.
o	o	k7c4	o
komprimovaný	komprimovaný	k2eAgInSc4d1	komprimovaný
soubor	soubor	k1gInSc4	soubor
nebo	nebo	k8xC	nebo
o	o	k7c4	o
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
binární	binární	k2eAgInSc4d1	binární
formát	formát	k1gInSc4	formát
nebo	nebo	k8xC	nebo
o	o	k7c4	o
málo	málo	k6eAd1	málo
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
formát	formát	k1gInSc4	formát
<g/>
,	,	kIx,	,
bez	bez	k1gInSc4	bez
metadat	metadat	k5eAaPmF	metadat
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
stojíme	stát	k5eAaImIp1nP	stát
nad	nad	k7c7	nad
sekvencí	sekvence	k1gFnSc7	sekvence
bajtů	bajt	k1gInPc2	bajt
a	a	k8xC	a
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
<s>
Konverze	konverze	k1gFnSc1	konverze
je	být	k5eAaImIp3nS	být
převod	převod	k1gInSc4	převod
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
kódy	kód	k1gInPc7	kód
a	a	k8xC	a
formáty	formát	k1gInPc7	formát
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
kódy	kód	k1gInPc1	kód
i	i	k8xC	i
formáty	formát	k1gInPc1	formát
standardizují	standardizovat	k5eAaBmIp3nP	standardizovat
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
konverze	konverze	k1gFnSc1	konverze
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgFnPc4d1	častá
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konverzi	konverze	k1gFnSc6	konverze
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
informace	informace	k1gFnSc2	informace
<g/>
;	;	kIx,	;
buď	buď	k8xC	buď
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
výstupní	výstupní	k2eAgInSc1d1	výstupní
formát	formát	k1gInSc1	formát
určité	určitý	k2eAgFnSc2d1	určitá
typy	typa	k1gFnSc2	typa
záznamů	záznam	k1gInPc2	záznam
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
konverzní	konverzní	k2eAgInSc1d1	konverzní
algoritmus	algoritmus	k1gInSc1	algoritmus
neumí	umět	k5eNaImIp3nS	umět
převést	převést	k5eAaPmF	převést
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
operuje	operovat	k5eAaImIp3nS	operovat
v	v	k7c6	v
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
přiřazený	přiřazený	k2eAgInSc4d1	přiřazený
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc1	typ
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
kódují	kódovat	k5eAaBmIp3nP	kódovat
data	datum	k1gNnPc1	datum
do	do	k7c2	do
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
a	a	k8xC	a
tedy	tedy	k9	tedy
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
chápat	chápat	k5eAaImF	chápat
bajty	bajt	k1gInPc4	bajt
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
uložená	uložený	k2eAgNnPc1d1	uložené
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
uschovávání	uschovávání	k1gNnSc4	uschovávání
nebo	nebo	k8xC	nebo
přenášení	přenášení	k1gNnSc4	přenášení
serializují	serializovat	k5eAaBmIp3nP	serializovat
do	do	k7c2	do
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Komprese	komprese	k1gFnSc1	komprese
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
konverze	konverze	k1gFnSc2	konverze
počítačových	počítačový	k2eAgNnPc2d1	počítačové
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
zmenšit	zmenšit	k5eAaPmF	zmenšit
jejich	jejich	k3xOp3gInSc1	jejich
objem	objem	k1gInSc1	objem
(	(	kIx(	(
<g/>
v	v	k7c6	v
bitech	bit	k1gInPc6	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ztrátové	ztrátový	k2eAgFnSc6d1	ztrátová
kompresi	komprese	k1gFnSc6	komprese
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
informace	informace	k1gFnPc1	informace
nenávratně	návratně	k6eNd1	návratně
ztraceny	ztracen	k2eAgFnPc1d1	ztracena
a	a	k8xC	a
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
zpět	zpět	k6eAd1	zpět
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ztrátu	ztráta	k1gFnSc4	ztráta
části	část	k1gFnSc3	část
informací	informace	k1gFnPc2	informace
tolerovat	tolerovat	k5eAaImF	tolerovat
<g/>
.	.	kIx.	.
</s>
<s>
Bezeztrátová	bezeztrátový	k2eAgFnSc1d1	bezeztrátová
komprese	komprese	k1gFnSc1	komprese
obvykle	obvykle	k6eAd1	obvykle
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
takové	takový	k3xDgFnPc4	takový
úrovně	úroveň	k1gFnPc4	úroveň
komprese	komprese	k1gFnSc1	komprese
jako	jako	k8xC	jako
ztrátová	ztrátový	k2eAgFnSc1d1	ztrátová
komprese	komprese	k1gFnSc1	komprese
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komprimovaný	komprimovaný	k2eAgInSc1d1	komprimovaný
soubor	soubor	k1gInSc1	soubor
lze	lze	k6eAd1	lze
opačným	opačný	k2eAgInSc7d1	opačný
postupem	postup	k1gInSc7	postup
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
přesně	přesně	k6eAd1	přesně
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
data	datum	k1gNnPc1	datum
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
textová	textový	k2eAgNnPc1d1	textové
(	(	kIx(	(
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
bajty	bajt	k1gInPc4	bajt
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
127	[number]	k4	127
<g/>
)	)	kIx)	)
a	a	k8xC	a
binární	binární	k2eAgMnSc1d1	binární
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
255	[number]	k4	255
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
platné	platný	k2eAgNnSc1d1	platné
(	(	kIx(	(
<g/>
nástup	nástup	k1gInSc1	nástup
UTF-	UTF-	k1gFnSc2	UTF-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
za	za	k7c2	za
data	datum	k1gNnSc2	datum
někdy	někdy	k6eAd1	někdy
považují	považovat	k5eAaImIp3nP	považovat
jen	jen	k9	jen
binární	binární	k2eAgInPc1d1	binární
soubory	soubor	k1gInPc1	soubor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
snadno	snadno	k6eAd1	snadno
lidmi	člověk	k1gMnPc7	člověk
čitelné	čitelný	k2eAgInPc1d1	čitelný
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lidmi	člověk	k1gMnPc7	člověk
čitelných	čitelný	k2eAgInPc2d1	čitelný
textových	textový	k2eAgInPc2d1	textový
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Standardizované	standardizovaný	k2eAgInPc4d1	standardizovaný
značkovací	značkovací	k2eAgInPc4d1	značkovací
jazyky	jazyk	k1gInPc4	jazyk
jako	jako	k8xS	jako
SGML	SGML	kA	SGML
<g/>
,	,	kIx,	,
XML	XML	kA	XML
a	a	k8xC	a
JSON	JSON	kA	JSON
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
zachycovat	zachycovat	k5eAaImF	zachycovat
data	datum	k1gNnPc1	datum
ve	v	k7c4	v
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
<g/>
)	)	kIx)	)
lidsky	lidsky	k6eAd1	lidsky
čitelné	čitelný	k2eAgFnSc6d1	čitelná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
i	i	k8xC	i
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
paměti	paměť	k1gFnSc6	paměť
uloženy	uložit	k5eAaPmNgFnP	uložit
jako	jako	k9	jako
sekvence	sekvence	k1gFnPc1	sekvence
bajtů	bajt	k1gInPc2	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
von	von	k1gInSc4	von
Neumannově	Neumannův	k2eAgFnSc3d1	Neumannova
architektuře	architektura	k1gFnSc3	architektura
není	být	k5eNaImIp3nS	být
zásadní	zásadní	k2eAgInSc1d1	zásadní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
způsobem	způsob	k1gInSc7	způsob
uložení	uložení	k1gNnSc2	uložení
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Kompilátor	kompilátor	k1gInSc1	kompilátor
generuje	generovat	k5eAaImIp3nS	generovat
budoucí	budoucí	k2eAgInSc4d1	budoucí
spustitelný	spustitelný	k2eAgInSc4d1	spustitelný
kód	kód	k1gInSc4	kód
jako	jako	k8xS	jako
data	datum	k1gNnPc4	datum
a	a	k8xC	a
počítač	počítač	k1gInSc4	počítač
jej	on	k3xPp3gInSc2	on
pak	pak	k6eAd1	pak
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
jako	jako	k8xC	jako
posloupnost	posloupnost	k1gFnSc4	posloupnost
instrukcí	instrukce	k1gFnPc2	instrukce
určenou	určený	k2eAgFnSc4d1	určená
ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
data	datum	k1gNnPc1	datum
měnit	měnit	k5eAaImF	měnit
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
programu	program	k1gInSc2	program
a	a	k8xC	a
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pragmatický	pragmatický	k2eAgInSc4d1	pragmatický
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
chybám	chyba	k1gFnPc3	chyba
softwaru	software	k1gInSc2	software
a	a	k8xC	a
před	před	k7c7	před
úmyslnou	úmyslný	k2eAgFnSc7d1	úmyslná
snahou	snaha	k1gFnSc7	snaha
program	program	k1gInSc1	program
narušit	narušit	k5eAaPmF	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
harvardská	harvardský	k2eAgFnSc1d1	Harvardská
architektura	architektura	k1gFnSc1	architektura
počítače	počítač	k1gInSc2	počítač
fyzicky	fyzicky	k6eAd1	fyzicky
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
paměť	paměť	k1gFnSc4	paměť
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
mít	mít	k5eAaImF	mít
paměť	paměť	k1gFnSc4	paměť
stejných	stejný	k2eAgInPc2d1	stejný
parametrů	parametr	k1gInPc2	parametr
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
pro	pro	k7c4	pro
data	datum	k1gNnPc4	datum
a	a	k8xC	a
pro	pro	k7c4	pro
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
uložena	uložit	k5eAaPmNgNnP	uložit
např.	např.	kA	např.
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
počítače	počítač	k1gInSc2	počítač
nebo	nebo	k8xC	nebo
na	na	k7c6	na
datovém	datový	k2eAgNnSc6d1	datové
médiu	médium	k1gNnSc6	médium
(	(	kIx(	(
<g/>
pevný	pevný	k2eAgInSc1d1	pevný
disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
paměťová	paměťový	k2eAgFnSc1d1	paměťová
karta	karta	k1gFnSc1	karta
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
počítače	počítač	k1gInSc2	počítač
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
soubory	soubor	k1gInPc7	soubor
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Databáze	databáze	k1gFnSc1	databáze
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
a	a	k8xC	a
zpracovávání	zpracovávání	k1gNnSc4	zpracovávání
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Databáze	databáze	k1gFnPc1	databáze
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
množina	množina	k1gFnSc1	množina
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
uložená	uložený	k2eAgFnSc1d1	uložená
na	na	k7c6	na
paměťovém	paměťový	k2eAgNnSc6d1	paměťové
médiu	médium	k1gNnSc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
báze	báze	k1gFnSc2	báze
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
program	program	k1gInSc4	program
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
a	a	k8xC	a
zpracovávání	zpracovávání	k1gNnSc2	zpracovávání
těchto	tento	k3xDgNnPc2	tento
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgFnSc1d1	operační
paměť	paměť	k1gFnSc1	paměť
počítače	počítač	k1gInSc2	počítač
je	být	k5eAaImIp3nS	být
elektronická	elektronický	k2eAgFnSc1d1	elektronická
paměť	paměť	k1gFnSc1	paměť
umožňující	umožňující	k2eAgNnSc4d1	umožňující
čtení	čtení	k1gNnSc4	čtení
i	i	k9	i
zápis	zápis	k1gInSc1	zápis
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
dočasné	dočasný	k2eAgNnSc4d1	dočasné
ukládání	ukládání	k1gNnSc4	ukládání
zpracovávaných	zpracovávaný	k2eAgNnPc2d1	zpracovávané
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
spouštěných	spouštěný	k2eAgInPc2d1	spouštěný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
operační	operační	k2eAgFnSc3d1	operační
paměti	paměť	k1gFnSc3	paměť
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
k	k	k7c3	k
vnější	vnější	k2eAgFnSc3d1	vnější
paměti	paměť	k1gFnSc3	paměť
<g/>
;	;	kIx,	;
procesor	procesor	k1gInSc1	procesor
pomocí	pomocí	k7c2	pomocí
adresy	adresa	k1gFnSc2	adresa
přímo	přímo	k6eAd1	přímo
vybírá	vybírat	k5eAaImIp3nS	vybírat
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
buňku	buňka	k1gFnSc4	buňka
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc4	některý
výhodné	výhodný	k2eAgFnPc4d1	výhodná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
seřazena	seřazen	k2eAgNnPc4d1	seřazeno
podle	podle	k7c2	podle
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
podmnožin	podmnožina	k1gFnPc2	podmnožina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
sekvenčním	sekvenční	k2eAgNnSc6d1	sekvenční
zpracování	zpracování	k1gNnSc6	zpracování
následují	následovat	k5eAaImIp3nP	následovat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
agregaci	agregace	k1gFnSc4	agregace
dat	datum	k1gNnPc2	datum
na	na	k7c6	na
podmnožinách	podmnožina	k1gFnPc6	podmnožina
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Vybrání	vybrání	k1gNnSc1	vybrání
podmnožiny	podmnožina	k1gFnSc2	podmnožina
z	z	k7c2	z
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
dat	datum	k1gNnPc2	datum
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
prohledávání	prohledávání	k1gNnSc1	prohledávání
celého	celý	k2eAgInSc2d1	celý
objemu	objem	k1gInSc2	objem
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
časově	časově	k6eAd1	časově
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Indexy	index	k1gInPc1	index
jsou	být	k5eAaImIp3nP	být
odvozené	odvozený	k2eAgFnPc1d1	odvozená
<g/>
,	,	kIx,	,
dodatečné	dodatečný	k2eAgFnPc1d1	dodatečná
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hodnoty	hodnota	k1gFnPc4	hodnota
klíčů	klíč	k1gInPc2	klíč
a	a	k8xC	a
adresy	adresa	k1gFnSc2	adresa
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
přímo	přímo	k6eAd1	přímo
hodnoty	hodnota	k1gFnPc4	hodnota
dat	datum	k1gNnPc2	datum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
podle	podle	k7c2	podle
klíče	klíč	k1gInSc2	klíč
zvoleného	zvolený	k2eAgInSc2d1	zvolený
při	při	k7c6	při
indexaci	indexace	k1gFnSc6	indexace
pak	pak	k6eAd1	pak
indexy	index	k1gInPc1	index
výrazně	výrazně	k6eAd1	výrazně
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Indexy	index	k1gInPc1	index
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ukládány	ukládat	k5eAaImNgInP	ukládat
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
B-stromů	Btrom	k1gInPc2	B-strom
nebo	nebo	k8xC	nebo
hašovacích	hašovací	k2eAgFnPc2d1	hašovací
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Speciálními	speciální	k2eAgInPc7d1	speciální
typy	typ	k1gInPc7	typ
indexů	index	k1gInPc2	index
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
trie	trie	k6eAd1	trie
a	a	k8xC	a
sufixový	sufixový	k2eAgInSc1d1	sufixový
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
dodatečná	dodatečný	k2eAgFnSc1d1	dodatečná
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
předává	předávat	k5eAaImIp3nS	předávat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
původními	původní	k2eAgNnPc7d1	původní
daty	datum	k1gNnPc7	datum
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
dat	datum	k1gNnPc2	datum
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
chybě	chyba	k1gFnSc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
přesně	přesně	k6eAd1	přesně
určené	určený	k2eAgFnPc4d1	určená
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgInPc4d1	provedený
s	s	k7c7	s
původními	původní	k2eAgNnPc7d1	původní
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Příjemce	příjemce	k1gMnSc1	příjemce
informace	informace	k1gFnSc2	informace
má	můj	k3xOp1gFnSc1	můj
možnost	možnost	k1gFnSc1	možnost
sám	sám	k3xTgInSc4	sám
znovu	znovu	k6eAd1	znovu
spočítat	spočítat	k5eAaPmF	spočítat
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
součet	součet	k1gInSc4	součet
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vypočtený	vypočtený	k2eAgInSc4d1	vypočtený
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
součet	součet	k1gInSc4	součet
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
předaným	předaný	k2eAgInSc7d1	předaný
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
součtem	součet	k1gInSc7	součet
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
přenosu	přenos	k1gInSc2	přenos
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
zprávy	zpráva	k1gFnSc2	zpráva
nebo	nebo	k8xC	nebo
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
součtu	součet	k1gInSc2	součet
<g/>
.	.	kIx.	.
</s>
<s>
Šifrování	šifrování	k1gNnSc1	šifrování
dat	datum	k1gNnPc2	datum
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gNnSc3	jejich
přečtení	přečtení	k1gNnSc4	přečtení
neautorizovanou	autorizovaný	k2eNgFnSc7d1	neautorizovaná
osobou	osoba	k1gFnSc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
steganografii	steganografie	k1gFnSc6	steganografie
je	být	k5eAaImIp3nS	být
zpráva	zpráva	k1gFnSc1	zpráva
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
ukryta	ukryt	k2eAgNnPc4d1	ukryto
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
neuvědomil	uvědomit	k5eNaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunikace	komunikace	k1gFnSc1	komunikace
vůbec	vůbec	k9	vůbec
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
zálohování	zálohování	k1gNnSc2	zálohování
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
uložení	uložení	k1gNnSc1	uložení
záložní	záložní	k2eAgFnSc2d1	záložní
kopie	kopie	k1gFnSc2	kopie
dat	datum	k1gNnPc2	datum
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
datovém	datový	k2eAgInSc6d1	datový
nosiči	nosič	k1gInSc6	nosič
(	(	kIx(	(
<g/>
a	a	k8xC	a
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
i	i	k9	i
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
budově	budova	k1gFnSc6	budova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záložní	záložní	k2eAgNnPc1d1	záložní
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
využívána	využívat	k5eAaPmNgNnP	využívat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc2	poškození
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
potřeby	potřeba	k1gFnSc2	potřeba
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
uloženými	uložený	k2eAgInPc7d1	uložený
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Systematickým	systematický	k2eAgNnSc7d1	systematické
zajištěním	zajištění	k1gNnSc7	zajištění
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
použitelnosti	použitelnost	k1gFnSc2	použitelnost
dat	datum	k1gNnPc2	datum
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obor	obor	k1gInSc1	obor
ochrana	ochrana	k1gFnSc1	ochrana
digitálních	digitální	k2eAgInPc2d1	digitální
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
