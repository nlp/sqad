<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
zvaný	zvaný	k2eAgInSc1d1	zvaný
Spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
(	(	kIx(	(
<g/>
1138	[number]	k4	1138
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1194	[number]	k4	1194
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
polský	polský	k2eAgInSc4d1	polský
kníže-senior	knížeenior	k1gInSc4	kníže-senior
vládnoucí	vládnoucí	k2eAgMnPc4d1	vládnoucí
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1177	[number]	k4	1177
až	až	k9	až
1194	[number]	k4	1194
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
sandoměřský	sandoměřský	k2eAgMnSc1d1	sandoměřský
<g/>
,	,	kIx,	,
mazovský	mazovský	k2eAgInSc1d1	mazovský
<g/>
,	,	kIx,	,
kališský	kališský	k2eAgInSc1d1	kališský
a	a	k8xC	a
hnězdenský	hnězdenský	k2eAgInSc1d1	hnězdenský
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
synem	syn	k1gMnSc7	syn
polského	polský	k2eAgNnSc2d1	polské
knížete	kníže	k1gNnSc2wR	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
Křivoústého	křivoústý	k2eAgNnSc2d1	křivoústý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
Salomeny	Salomena	k1gFnSc2	Salomena
z	z	k7c2	z
Bergu	Berg	k1gInSc2	Berg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
roku	rok	k1gInSc2	rok
1138	[number]	k4	1138
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
Polské	polský	k2eAgNnSc4d1	polské
knížectví	knížectví	k1gNnSc4	knížectví
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Kazimír	Kazimír	k1gMnSc1	Kazimír
narodil	narodit	k5eAaPmAgMnS	narodit
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
otcovou	otcův	k2eAgFnSc7d1	otcova
smrtí	smrt	k1gFnSc7	smrt
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
jako	jako	k9	jako
pohrobek	pohrobek	k1gMnSc1	pohrobek
<g/>
,	,	kIx,	,
nezískal	získat	k5eNaPmAgMnS	získat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dělbě	dělba	k1gFnSc6	dělba
žádný	žádný	k3yNgInSc4	žádný
úděl	úděl	k1gInSc4	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
získal	získat	k5eAaPmAgMnS	získat
až	až	k6eAd1	až
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Jindřicha	Jindřich	k1gMnSc2	Jindřich
roku	rok	k1gInSc2	rok
1166	[number]	k4	1166
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
zdědil	zdědit	k5eAaPmAgInS	zdědit
jeho	jeho	k3xOp3gNnSc4	jeho
Sandoměřsko	Sandoměřsko	k1gNnSc4	Sandoměřsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Jindřichovy	Jindřichův	k2eAgFnSc2d1	Jindřichova
poslední	poslední	k2eAgFnSc2d1	poslední
vůle	vůle	k1gFnSc2	vůle
však	však	k9	však
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
další	další	k2eAgMnSc1d1	další
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
kníže-senior	knížeenior	k1gMnSc1	kníže-senior
Boleslav	Boleslav	k1gMnSc1	Boleslav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Kadeřavý	kadeřavý	k2eAgMnSc1d1	kadeřavý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Sandoměřsko	Sandoměřsko	k1gNnSc4	Sandoměřsko
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
;	;	kIx,	;
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc7d2	menší
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
Měškovi	Měšek	k1gMnSc3	Měšek
Starému	Starý	k1gMnSc3	Starý
a	a	k8xC	a
na	na	k7c4	na
Kazimíra	Kazimír	k1gMnSc4	Kazimír
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k6eAd1	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
Wiślica	Wiślic	k1gInSc2	Wiślic
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc4d1	celé
Sandoměřsko	Sandoměřsko	k1gNnSc4	Sandoměřsko
obdržel	obdržet	k5eAaPmAgMnS	obdržet
až	až	k9	až
po	po	k7c6	po
Boleslavově	Boleslavův	k2eAgFnSc6d1	Boleslavova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1173	[number]	k4	1173
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1177	[number]	k4	1177
se	se	k3xPyFc4	se
vzbouřil	vzbouřit	k5eAaPmAgMnS	vzbouřit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
bratru	bratr	k1gMnSc3	bratr
<g/>
,	,	kIx,	,
novému	nový	k2eAgMnSc3d1	nový
knížeti-senioru	knížetienior	k1gMnSc3	knížeti-senior
Měškovi	Měšek	k1gMnSc3	Měšek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
odboji	odboj	k1gInSc6	odboj
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
např.	např.	kA	např.
slezským	slezský	k2eAgMnSc7d1	slezský
knížetem	kníže	k1gMnSc7	kníže
Boleslavem	Boleslav	k1gMnSc7	Boleslav
Vysokým	vysoký	k2eAgMnSc7d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Měšek	Měšek	k1gMnSc1	Měšek
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
Poznaně	Poznaň	k1gFnSc2	Poznaň
a	a	k8xC	a
Kazimír	Kazimír	k1gMnSc1	Kazimír
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Malopolsko	Malopolsko	k1gNnSc4	Malopolsko
se	s	k7c7	s
sídelním	sídelní	k2eAgNnSc7d1	sídelní
místem	místo	k1gNnSc7	místo
polských	polský	k2eAgMnPc2d1	polský
knížat	kníže	k1gMnPc2wR	kníže
Krakovem	Krakov	k1gInSc7	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1191	[number]	k4	1191
se	se	k3xPyFc4	se
Měšekznovu	Měšekznův	k2eAgFnSc4d1	Měšekznův
krátce	krátce	k6eAd1	krátce
stal	stát	k5eAaPmAgMnS	stát
polským	polský	k2eAgMnSc7d1	polský
seniorem	senior	k1gMnSc7	senior
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
malopolská	malopolský	k2eAgFnSc1d1	Malopolská
šlechta	šlechta	k1gFnSc1	šlechta
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
se	s	k7c7	s
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
politikou	politika	k1gFnSc7	politika
Kazimíra	Kazimír	k1gMnSc2	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Měšek	Měšek	k1gInSc1	Měšek
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgInS	získat
Krakov	Krakov	k1gInSc1	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
polských	polský	k2eAgMnPc2d1	polský
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
však	však	k9	však
Kazimír	Kazimír	k1gMnSc1	Kazimír
brzy	brzy	k6eAd1	brzy
získal	získat	k5eAaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1194	[number]	k4	1194
<g/>
.	.	kIx.	.
</s>
<s>
Měšek	Měšek	k6eAd1	Měšek
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
knížete	kníže	k1gMnSc2	kníže
seniora	senior	k1gMnSc2	senior
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
však	však	k9	však
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1195	[number]	k4	1195
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Mozgawy	Mozgawa	k1gFnSc2	Mozgawa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
Měšek	Měšek	k1gMnSc1	Měšek
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kazimír	Kazimír	k1gMnSc1	Kazimír
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
