<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnPc2	si
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
značka	značka	k1gFnSc1	značka
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
9	[number]	k4	9
192	[number]	k4	192
631	[number]	k4	631
770	[number]	k4	770
period	perioda	k1gFnPc2	perioda
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hladinami	hladina	k1gFnPc7	hladina
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgFnPc4d1	jemná
struktury	struktura	k1gFnPc4	struktura
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
atomu	atom	k1gInSc2	atom
cesia	cesium	k1gNnSc2	cesium
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
