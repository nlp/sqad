<s>
Akrylová	akrylový	k2eAgFnSc1d1	akrylová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
barvy	barva	k1gFnSc2	barva
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
malbě	malba	k1gFnSc6	malba
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
