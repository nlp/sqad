<p>
<s>
Akrylová	akrylový	k2eAgFnSc1d1	akrylová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
barvy	barva	k1gFnSc2	barva
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
malbě	malba	k1gFnSc6	malba
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
výhod	výhoda	k1gFnPc2	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
disperzní	disperzní	k2eAgFnSc4d1	disperzní
barvu	barva	k1gFnSc4	barva
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
polyakrylové	polyakrylový	k2eAgFnSc2d1	polyakrylová
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
ředitelná	ředitelný	k2eAgFnSc1d1	ředitelná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
zaschnutém	zaschnutý	k2eAgInSc6d1	zaschnutý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
voděodolná	voděodolný	k2eAgFnSc1d1	voděodolná
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
zaschnutý	zaschnutý	k2eAgInSc4d1	zaschnutý
akryl	akryl	k1gInSc4	akryl
nalakovat	nalakovat	k5eAaPmF	nalakovat
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
karton	karton	k1gInSc4	karton
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc4	dřevo
nebo	nebo	k8xC	nebo
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
cca	cca	kA	cca
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
uplatnění	uplatnění	k1gNnSc1	uplatnění
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
omítek	omítka	k1gFnPc2	omítka
<g/>
,	,	kIx,	,
betonu	beton	k1gInSc2	beton
nebo	nebo	k8xC	nebo
sádrokartonu	sádrokarton	k1gInSc2	sádrokarton
<g/>
.	.	kIx.	.
</s>
<s>
Akrylové	akrylový	k2eAgFnPc4d1	akrylová
barvy	barva	k1gFnPc4	barva
lze	lze	k6eAd1	lze
tónovat	tónovat	k5eAaBmF	tónovat
různými	různý	k2eAgNnPc7d1	různé
barvivy	barvivo	k1gNnPc7	barvivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akrylové	akrylový	k2eAgFnPc1d1	akrylová
barvy	barva	k1gFnPc1	barva
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
olejových	olejový	k2eAgFnPc2d1	olejová
barev	barva	k1gFnPc2	barva
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
usychají	usychat	k5eAaImIp3nP	usychat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
zdraví	zdraví	k1gNnSc2	zdraví
škodlivého	škodlivý	k2eAgInSc2d1	škodlivý
terpentýnu	terpentýn	k1gInSc2	terpentýn
stačí	stačit	k5eAaBmIp3nS	stačit
pro	pro	k7c4	pro
naředění	naředění	k1gNnSc4	naředění
pouze	pouze	k6eAd1	pouze
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Klasické	klasický	k2eAgFnPc1d1	klasická
malířské	malířský	k2eAgFnPc1d1	malířská
akrylové	akrylový	k2eAgFnPc1d1	akrylová
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
polyakrylátů	polyakrylát	k1gInPc2	polyakrylát
a	a	k8xC	a
polymetakrylátů	polymetakrylát	k1gInPc2	polymetakrylát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
smíchá	smíchat	k5eAaPmIp3nS	smíchat
pigment	pigment	k1gInSc1	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čerstva	čerstva	k?	čerstva
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
ředitelná	ředitelný	k2eAgFnSc1d1	ředitelná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
zaschnutá	zaschnutý	k2eAgFnSc1d1	zaschnutá
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
voděodolná	voděodolný	k2eAgFnSc1d1	voděodolná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pigmenty	pigment	k1gInPc4	pigment
==	==	k?	==
</s>
</p>
<p>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
výběru	výběr	k1gInSc2	výběr
pigmentů	pigment	k1gInPc2	pigment
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
široký	široký	k2eAgInSc1d1	široký
jako	jako	k8xS	jako
u	u	k7c2	u
klasických	klasický	k2eAgFnPc2d1	klasická
malířských	malířský	k2eAgFnPc2d1	malířská
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
olejových	olejový	k2eAgFnPc2d1	olejová
a	a	k8xC	a
akvarelových	akvarelový	k2eAgFnPc2d1	akvarelová
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
nových	nový	k2eAgNnPc2d1	nové
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neužívaných	užívaný	k2eNgInPc2d1	neužívaný
pigmentů	pigment	k1gInPc2	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
získávají	získávat	k5eAaImIp3nP	získávat
nové	nový	k2eAgFnPc1d1	nová
barvy	barva	k1gFnPc1	barva
vyšší	vysoký	k2eAgFnSc4d2	vyšší
světlostálost	světlostálost	k1gFnSc4	světlostálost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc4	užití
akrylových	akrylový	k2eAgFnPc2d1	akrylová
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
schnutí	schnutí	k1gNnSc2	schnutí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výhodou	výhoda	k1gFnSc7	výhoda
i	i	k8xC	i
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
potřebám	potřeba	k1gFnPc3	potřeba
umělce	umělec	k1gMnSc2	umělec
a	a	k8xC	a
k	k	k7c3	k
stylu	styl	k1gInSc3	styl
jeho	jeho	k3xOp3gMnPc2	jeho
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
zaschnutá	zaschnutý	k2eAgFnSc1d1	zaschnutá
vrstva	vrstva	k1gFnSc1	vrstva
akrylu	akryl	k1gInSc2	akryl
je	být	k5eAaImIp3nS	být
voděodolná	voděodolný	k2eAgFnSc1d1	voděodolná
<g/>
,	,	kIx,	,
vrstvení	vrstvení	k1gNnSc1	vrstvení
malby	malba	k1gFnSc2	malba
a	a	k8xC	a
opravy	oprava	k1gFnPc4	oprava
tedy	tedy	k9	tedy
nebývají	bývat	k5eNaImIp3nP	bývat
problematické	problematický	k2eAgFnPc1d1	problematická
<g/>
.	.	kIx.	.
</s>
<s>
Akrylové	akrylový	k2eAgFnPc1d1	akrylová
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
všestranné	všestranný	k2eAgFnPc1d1	všestranná
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
nanášet	nanášet	k5eAaImF	nanášet
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
tenkých	tenký	k2eAgFnPc6d1	tenká
lazurách	lazura	k1gFnPc6	lazura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
pastózních	pastózní	k2eAgInPc6d1	pastózní
nátěrech	nátěr	k1gInPc6	nátěr
s	s	k7c7	s
texturními	texturní	k2eAgInPc7d1	texturní
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akrylová	akrylový	k2eAgNnPc1d1	akrylové
média	médium	k1gNnPc1	médium
==	==	k?	==
</s>
</p>
<p>
<s>
Akrylová	akrylový	k2eAgNnPc1d1	akrylové
média	médium	k1gNnPc1	médium
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
vlastností	vlastnost	k1gFnPc2	vlastnost
akrylových	akrylový	k2eAgFnPc2d1	akrylová
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lesklé	lesklý	k2eAgNnSc1d1	lesklé
médium	médium	k1gNnSc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
po	po	k7c6	po
zaschnutí	zaschnutí	k1gNnSc6	zaschnutí
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pružnost	pružnost	k1gFnSc1	pružnost
</s>
</p>
<p>
<s>
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
zachycování	zachycování	k1gNnSc1	zachycování
nečistot	nečistota	k1gFnPc2	nečistota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
filmu	film	k1gInSc2	film
kvůli	kvůli	k7c3	kvůli
možné	možný	k2eAgFnSc3d1	možná
lepkavosti	lepkavost	k1gFnSc3	lepkavost
</s>
</p>
<p>
<s>
===	===	k?	===
Matové	matový	k2eAgNnSc1d1	matové
médium	médium	k1gNnSc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
hustší	hustý	k2eAgNnSc1d2	hustší
než	než	k8xS	než
lesklé	lesklý	k2eAgNnSc1d1	lesklé
médium	médium	k1gNnSc1	médium
kvůli	kvůli	k7c3	kvůli
obsahu	obsah	k1gInSc3	obsah
matových	matový	k2eAgInPc2d1	matový
činidel	činidlo	k1gNnPc2	činidlo
</s>
</p>
<p>
<s>
===	===	k?	===
Gelové	gelový	k2eAgNnSc1d1	gelové
médium	médium	k1gNnSc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
zprůhledňuje	zprůhledňovat	k5eAaImIp3nS	zprůhledňovat
vrstvy	vrstva	k1gFnPc4	vrstva
malby	malba	k1gFnSc2	malba
bez	bez	k7c2	bez
ničení	ničení	k1gNnSc2	ničení
textury	textura	k1gFnSc2	textura
</s>
</p>
<p>
<s>
===	===	k?	===
Retardér	retardér	k1gInSc4	retardér
===	===	k?	===
</s>
</p>
<p>
<s>
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
schnutí	schnutí	k1gNnSc1	schnutí
barvy	barva	k1gFnSc2	barva
</s>
</p>
<p>
<s>
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
gelu	gel	k1gInSc2	gel
nebo	nebo	k8xC	nebo
tekutiny	tekutina	k1gFnSc2	tekutina
</s>
</p>
<p>
<s>
užíváme	užívat	k5eAaImIp1nP	užívat
jej	on	k3xPp3gInSc4	on
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přiměřeném	přiměřený	k2eAgNnSc6d1	přiměřené
množství	množství	k1gNnSc6	množství
</s>
</p>
<p>
<s>
==	==	k?	==
Podklady	podklad	k1gInPc1	podklad
pro	pro	k7c4	pro
akrylovou	akrylový	k2eAgFnSc4d1	akrylová
malbu	malba	k1gFnSc4	malba
==	==	k?	==
</s>
</p>
<p>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
povrchů	povrch	k1gInPc2	povrch
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
drsné	drsný	k2eAgInPc1d1	drsný
a	a	k8xC	a
nemastné	mastný	k2eNgInPc1d1	nemastný
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
podklad	podklad	k1gInSc4	podklad
nejprve	nejprve	k6eAd1	nejprve
natřít	natřít	k5eAaPmF	natřít
šepsem	šeps	k1gInSc7	šeps
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
vhodných	vhodný	k2eAgInPc2d1	vhodný
podkladů	podklad	k1gInPc2	podklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
plátno	plátno	k1gNnSc1	plátno
</s>
</p>
<p>
<s>
dřevo	dřevo	k1gNnSc1	dřevo
</s>
</p>
<p>
<s>
papír	papír	k1gInSc1	papír
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
gramáží	gramáž	k1gFnSc7	gramáž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karton	karton	k1gInSc4	karton
</s>
</p>
<p>
<s>
sololit	sololit	k1gInSc1	sololit
</s>
</p>
<p>
<s>
===	===	k?	===
Plátno	plátno	k1gNnSc1	plátno
===	===	k?	===
</s>
</p>
<p>
<s>
Nemáme	mít	k5eNaImIp1nP	mít
<g/>
-li	i	k?	-li
zakoupené	zakoupený	k2eAgInPc1d1	zakoupený
již	již	k6eAd1	již
předem	předem	k6eAd1	předem
našepsované	našepsovaný	k2eAgNnSc4d1	našepsované
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
jej	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
připravit	připravit	k5eAaPmF	připravit
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednou	jednou	k6eAd1	jednou
či	či	k8xC	či
více	hodně	k6eAd2	hodně
vrstvami	vrstva	k1gFnPc7	vrstva
šepsu	šeps	k1gInSc2	šeps
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
technikách	technika	k1gFnPc6	technika
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
při	při	k7c6	při
technice	technika	k1gFnSc6	technika
netransparentní	transparentní	k2eNgFnSc6d1	netransparentní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
podklad	podklad	k1gInSc4	podklad
přímo	přímo	k6eAd1	přímo
natřít	natřít	k5eAaPmF	natřít
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
tmavou	tmavý	k2eAgFnSc7d1	tmavá
akrylovou	akrylový	k2eAgFnSc7d1	akrylová
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pevné	pevný	k2eAgFnPc4d1	pevná
podložky	podložka	k1gFnPc4	podložka
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
pevné	pevný	k2eAgFnPc4d1	pevná
podložky	podložka	k1gFnPc4	podložka
řadíme	řadit	k5eAaImIp1nP	řadit
například	například	k6eAd1	například
sololit	sololit	k1gInSc4	sololit
<g/>
.	.	kIx.	.
</s>
<s>
Šeps	šeps	k1gInSc4	šeps
nanášíme	nanášet	k5eAaImIp1nP	nanášet
podle	podle	k7c2	podle
savosti	savost	k1gFnSc2	savost
podkladu	podklad	k1gInSc2	podklad
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
nátěr	nátěr	k1gInSc1	nátěr
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
naředit	naředit	k5eAaPmF	naředit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
plochy	plocha	k1gFnPc4	plocha
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
užití	užití	k1gNnSc1	užití
stříkací	stříkací	k2eAgFnSc2d1	stříkací
pistole	pistol	k1gFnSc2	pistol
<g/>
,	,	kIx,	,
docílíme	docílit	k5eAaPmIp1nP	docílit
tím	ten	k3xDgNnSc7	ten
hladkého	hladký	k2eAgInSc2d1	hladký
povrchu	povrch	k1gInSc2	povrch
pro	pro	k7c4	pro
malbu	malba	k1gFnSc4	malba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velmi	velmi	k6eAd1	velmi
savé	savý	k2eAgInPc4d1	savý
povrchy	povrch	k1gInPc4	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
savé	savý	k2eAgInPc4d1	savý
povrchy	povrch	k1gInPc4	povrch
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nejprve	nejprve	k6eAd1	nejprve
natřít	natřít	k5eAaPmF	natřít
vodou	voda	k1gFnSc7	voda
ředěným	ředěný	k2eAgNnSc7d1	ředěné
lesklým	lesklý	k2eAgNnSc7d1	lesklé
akrylovým	akrylový	k2eAgNnSc7d1	akrylové
médiem	médium	k1gNnSc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
podklad	podklad	k1gInSc1	podklad
natřeli	natřít	k5eAaPmAgMnP	natřít
šepsem	šeps	k1gInSc7	šeps
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
šeps	šeps	k1gInSc4	šeps
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vsál	vsát	k5eAaPmAgMnS	vsát
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
vrstva	vrstva	k1gFnSc1	vrstva
nátěrů	nátěr	k1gInPc2	nátěr
by	by	kYmCp3nP	by
již	již	k9	již
k	k	k7c3	k
právě	právě	k6eAd1	právě
"	"	kIx"	"
<g/>
práškovému	práškový	k2eAgMnSc3d1	práškový
<g/>
"	"	kIx"	"
podkladu	podklad	k1gInSc3	podklad
nepřilnula	přilnout	k5eNaPmAgFnS	přilnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
akrylovými	akrylový	k2eAgFnPc7d1	akrylová
barvami	barva	k1gFnPc7	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
schnou	schnout	k5eAaImIp3nP	schnout
akrylové	akrylový	k2eAgFnPc1d1	akrylová
barvy	barva	k1gFnPc1	barva
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zvláště	zvláště	k6eAd1	zvláště
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
štětce	štětec	k1gInPc4	štětec
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
malířské	malířský	k2eAgNnSc4d1	malířské
náčiní	náčiní	k1gNnSc4	náčiní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mít	mít	k5eAaImF	mít
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
ponořené	ponořený	k2eAgNnSc1d1	ponořené
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
pečlivě	pečlivě	k6eAd1	pečlivě
umýt	umýt	k5eAaPmF	umýt
a	a	k8xC	a
vysušit	vysušit	k5eAaPmF	vysušit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
akrylová	akrylový	k2eAgFnSc1d1	akrylová
barva	barva	k1gFnSc1	barva
po	po	k7c6	po
zaschnutí	zaschnutí	k1gNnSc6	zaschnutí
voděodolná	voděodolný	k2eAgFnSc1d1	voděodolná
<g/>
,	,	kIx,	,
zatvrdlé	zatvrdlý	k2eAgInPc1d1	zatvrdlý
štětce	štětec	k1gInPc1	štětec
již	již	k6eAd1	již
nadále	nadále	k6eAd1	nadále
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SMITH	SMITH	kA	SMITH
<g/>
,	,	kIx,	,
Ray	Ray	k1gFnSc1	Ray
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
technik	technika	k1gFnPc2	technika
a	a	k8xC	a
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
384	[number]	k4	384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7391	[number]	k4	7391
<g/>
-	-	kIx~	-
<g/>
482	[number]	k4	482
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
202	[number]	k4	202
<g/>
-	-	kIx~	-
<g/>
221	[number]	k4	221
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Akrylová	akrylový	k2eAgFnSc1d1	akrylová
farba	farba	k1gFnSc1	farba
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
