<s>
Synonyma	synonymum	k1gNnPc1	synonymum
<g/>
:	:	kIx,	:
distorze	distorze	k1gFnSc1	distorze
<g/>
,	,	kIx,	,
vymknutí	vymknutí	k1gNnSc1	vymknutí
<g/>
,	,	kIx,	,
výron	výron	k1gInSc1	výron
<g/>
,	,	kIx,	,
vyvrtnutí	vyvrtnutí	k1gNnSc1	vyvrtnutí
kotníku	kotník	k1gInSc2	kotník
Podvrtnutí	podvrtnutí	k1gNnSc2	podvrtnutí
kotníku	kotník	k1gInSc2	kotník
(	(	kIx(	(
<g/>
správně	správně	k6eAd1	správně
hlezna	hlezno	k1gNnSc2	hlezno
;	;	kIx,	;
kotník	kotník	k1gInSc1	kotník
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
část	část	k1gFnSc1	část
hlezna	hlezno	k1gNnSc2	hlezno
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejčastějším	častý	k2eAgNnPc3d3	nejčastější
zraněním	zranění	k1gNnPc3	zranění
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
sportovci	sportovec	k1gMnPc7	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
však	však	k9	však
postihnout	postihnout	k5eAaPmF	postihnout
každého	každý	k3xTgMnSc4	každý
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
recidivy	recidiva	k1gFnPc1	recidiva
(	(	kIx(	(
<g/>
opakované	opakovaný	k2eAgNnSc1d1	opakované
podvrtnutí	podvrtnutí	k1gNnSc1	podvrtnutí
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nekvalitního	kvalitní	k2eNgNnSc2d1	nekvalitní
zhojení	zhojení	k1gNnSc2	zhojení
původního	původní	k2eAgNnSc2d1	původní
poranění	poranění	k1gNnSc2	poranění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podvrtnutí	podvrtnutí	k1gNnSc6	podvrtnutí
kotníku	kotník	k1gInSc2	kotník
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
vazů	vaz	k1gInPc2	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Vazy	vaz	k1gInPc1	vaz
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nataženy	natažen	k2eAgInPc1d1	natažen
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
natrženy	natržen	k2eAgFnPc1d1	natržena
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
přetrženy	přetržen	k2eAgFnPc1d1	přetržen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
3	[number]	k4	3
stupně	stupeň	k1gInSc2	stupeň
závažnosti	závažnost	k1gFnSc2	závažnost
podvrtnutí	podvrtnutí	k1gNnSc2	podvrtnutí
kotníku	kotník	k1gInSc2	kotník
<g/>
:	:	kIx,	:
stupeň	stupeň	k1gInSc1	stupeň
<g/>
:	:	kIx,	:
vazy	vaz	k1gInPc1	vaz
jsou	být	k5eAaImIp3nP	být
natažené	natažený	k2eAgFnPc1d1	natažená
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
kotníku	kotník	k1gInSc2	kotník
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
zhoršená	zhoršený	k2eAgFnSc1d1	zhoršená
stupeň	stupeň	k1gInSc1	stupeň
<g/>
:	:	kIx,	:
vazy	vaz	k1gInPc1	vaz
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
natržené	natržený	k2eAgInPc4d1	natržený
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
kotníku	kotník	k1gInSc2	kotník
je	být	k5eAaImIp3nS	být
narušená	narušený	k2eAgFnSc1d1	narušená
stupeň	stupeň	k1gInSc1	stupeň
<g/>
:	:	kIx,	:
vazy	vaz	k1gInPc1	vaz
jsou	být	k5eAaImIp3nP	být
úplně	úplně	k6eAd1	úplně
přetržené	přetržený	k2eAgFnPc1d1	přetržená
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
kotníku	kotník	k1gInSc2	kotník
je	být	k5eAaImIp3nS	být
vážně	vážně	k6eAd1	vážně
narušená	narušený	k2eAgNnPc4d1	narušené
Podvrtnutí	podvrtnutí	k1gNnPc4	podvrtnutí
kotníku	kotník	k1gInSc2	kotník
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
náhlou	náhlý	k2eAgFnSc7d1	náhlá
bolestí	bolest	k1gFnSc7	bolest
iniciovanou	iniciovaný	k2eAgFnSc4d1	iniciovaná
úrazem	úraz	k1gInSc7	úraz
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
otokem	otok	k1gInSc7	otok
<g/>
,	,	kIx,	,
v	v	k7c6	v
závažnějších	závažný	k2eAgInPc6d2	závažnější
případech	případ	k1gInPc6	případ
i	i	k9	i
krevním	krevní	k2eAgInSc7d1	krevní
výronem	výron	k1gInSc7	výron
<g/>
.	.	kIx.	.
</s>
<s>
Postižený	postižený	k1gMnSc1	postižený
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
jen	jen	k9	jen
omezené	omezený	k2eAgFnSc2d1	omezená
zátěže	zátěž	k1gFnSc2	zátěž
poraněného	poraněný	k2eAgInSc2d1	poraněný
kloubu	kloub	k1gInSc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
léčby	léčba	k1gFnSc2	léčba
podvrtnutí	podvrtnutí	k1gNnSc2	podvrtnutí
kotníku	kotník	k1gInSc2	kotník
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
zásady	zásada	k1gFnSc2	zásada
RICE	RICE	kA	RICE
(	(	kIx(	(
<g/>
Rest	rest	k6eAd1	rest
<g/>
,	,	kIx,	,
Ice	Ice	k1gFnSc1	Ice
<g/>
,	,	kIx,	,
Compression	Compression	k1gInSc1	Compression
<g/>
,	,	kIx,	,
Elevation	Elevation	k1gInSc1	Elevation
<g/>
)	)	kIx)	)
Klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
Rest	rest	k6eAd1	rest
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zraněná	zraněný	k2eAgFnSc1d1	zraněná
noha	noha	k1gFnSc1	noha
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
zatěžovat	zatěžovat	k5eAaImF	zatěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Ledování	ledování	k1gNnPc4	ledování
(	(	kIx(	(
<g/>
Ice	Ice	k1gFnSc4	Ice
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
denně	denně	k6eAd1	denně
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
15-20	[number]	k4	15-20
minut	minuta	k1gFnPc2	minuta
připevňujeme	připevňovat	k5eAaImIp1nP	připevňovat
ke	k	k7c3	k
kotníku	kotník	k1gInSc2	kotník
chladicí	chladicí	k2eAgNnSc4d1	chladicí
médium	médium	k1gNnSc4	médium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ledovou	ledový	k2eAgFnSc4d1	ledová
tříšť	tříšť	k1gFnSc4	tříšť
v	v	k7c6	v
plastovém	plastový	k2eAgInSc6d1	plastový
sáčku	sáček	k1gInSc6	sáček
či	či	k8xC	či
zmraženou	zmražený	k2eAgFnSc4d1	zmražená
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komprese	komprese	k1gFnSc1	komprese
(	(	kIx(	(
<g/>
Compression	Compression	k1gInSc1	Compression
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
kotníku	kotník	k1gInSc2	kotník
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
elastický	elastický	k2eAgInSc1d1	elastický
obvaz	obvaz	k1gInSc1	obvaz
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
používání	používání	k1gNnSc2	používání
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
závažnosti	závažnost	k1gFnSc2	závažnost
zranění	zranění	k1gNnSc1	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Zvednutí	zvednutí	k1gNnSc1	zvednutí
(	(	kIx(	(
<g/>
Elevation	Elevation	k1gInSc1	Elevation
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jako	jako	k9	jako
u	u	k7c2	u
předchozích	předchozí	k2eAgInPc2d1	předchozí
dvou	dva	k4xCgInPc2	dva
bodů	bod	k1gInPc2	bod
má	mít	k5eAaImIp3nS	mít
vyvýšená	vyvýšený	k2eAgFnSc1d1	vyvýšená
poloha	poloha	k1gFnSc1	poloha
kotníku	kotník	k1gInSc2	kotník
za	za	k7c4	za
následek	následek	k1gInSc4	následek
omezení	omezení	k1gNnSc4	omezení
tvorby	tvorba	k1gFnSc2	tvorba
otoku	otok	k1gInSc2	otok
<g/>
.	.	kIx.	.
</s>
<s>
Kotník	kotník	k1gInSc1	kotník
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nacházet	nacházet	k5eAaImF	nacházet
ve	v	k7c4	v
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pozici	pozice	k1gFnSc4	pozice
než	než	k8xS	než
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
variantou	varianta	k1gFnSc7	varianta
léčby	léčba	k1gFnSc2	léčba
podvrtnutí	podvrtnutí	k1gNnSc4	podvrtnutí
kotníku	kotník	k1gInSc2	kotník
je	být	k5eAaImIp3nS	být
užití	užití	k1gNnSc1	užití
injekcí	injekce	k1gFnPc2	injekce
kyseliny	kyselina	k1gFnSc2	kyselina
hyaluronové	hyaluronový	k2eAgFnSc2d1	hyaluronová
(	(	kIx(	(
<g/>
komerčně	komerčně	k6eAd1	komerčně
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
1	[number]	k4	1
<g/>
%	%	kIx~	%
roztoku	roztok	k1gInSc3	roztok
hyaluronátu	hyaluronát	k1gInSc2	hyaluronát
sodného	sodný	k2eAgInSc2d1	sodný
ve	v	k7c6	v
fosfátem	fosfát	k1gInSc7	fosfát
pufrovaném	pufrovaný	k2eAgInSc6d1	pufrovaný
fyziologickém	fyziologický	k2eAgInSc6d1	fyziologický
roztoku	roztok	k1gInSc6	roztok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
k	k	k7c3	k
poškozeným	poškozený	k2eAgInPc3d1	poškozený
vazům	vaz	k1gInPc3	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
terapie	terapie	k1gFnSc1	terapie
zřejmě	zřejmě	k6eAd1	zřejmě
snižuje	snižovat	k5eAaImIp3nS	snižovat
bolestivost	bolestivost	k1gFnSc1	bolestivost
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
plné	plný	k2eAgFnSc3d1	plná
zátěži	zátěž	k1gFnSc3	zátěž
<g/>
.	.	kIx.	.
</s>
