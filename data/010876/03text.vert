<p>
<s>
Minsk	Minsk	k1gInSc1	Minsk
(	(	kIx(	(
<g/>
bělorusky	bělorusky	k6eAd1	bělorusky
<g/>
:	:	kIx,	:
původně	původně	k6eAd1	původně
<g/>
:	:	kIx,	:
М	М	k?	М
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
:	:	kIx,	:
М	М	k?	М
<g/>
;	;	kIx,	;
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
М	М	k?	М
<g/>
;	;	kIx,	;
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
М	М	k?	М
<g/>
;	;	kIx,	;
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Mińsk	Mińsk	k1gInSc1	Mińsk
(	(	kIx(	(
<g/>
Litewski	Litewski	k1gNnSc1	Litewski
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
409	[number]	k4	409
km	km	kA	km
<g/>
2	[number]	k4	2
1	[number]	k4	1
982	[number]	k4	982
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
9	[number]	k4	9
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
rajónů	rajón	k1gInPc2	rajón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
7	[number]	k4	7
030	[number]	k4	030
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
1	[number]	k4	1
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
i	i	k8xC	i
kuturním	kuturní	k2eAgNnSc7d1	kuturní
centrem	centrum	k1gNnSc7	centrum
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Minsk	Minsk	k1gInSc1	Minsk
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
zvlněné	zvlněný	k2eAgFnSc6d1	zvlněná
a	a	k8xC	a
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
krajině	krajina	k1gFnSc6	krajina
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
;	;	kIx,	;
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
280	[number]	k4	280
m	m	kA	m
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
646	[number]	k4	646
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Minsk	Minsk	k1gInSc1	Minsk
je	být	k5eAaImIp3nS	být
křižovatkou	křižovatka	k1gFnSc7	křižovatka
hlavních	hlavní	k2eAgFnPc2d1	hlavní
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tepen	tepna	k1gFnPc2	tepna
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
územního	územní	k2eAgNnSc2d1	územní
dělení	dělení	k1gNnSc2	dělení
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
status	status	k1gInSc4	status
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
Minské	minský	k2eAgFnSc2d1	Minská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
М	М	k?	М
в	в	k?	в
<g/>
)	)	kIx)	)
a	a	k8xC	a
Minského	minský	k2eAgInSc2d1	minský
rajonu	rajon	k1gInSc2	rajon
(	(	kIx(	(
<g/>
М	М	k?	М
р	р	k?	р
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Minsk	Minsk	k1gInSc1	Minsk
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Svislač	Svislač	k1gInSc1	Svislač
a	a	k8xC	a
Njamiha	Njamiha	k1gFnSc1	Njamiha
(	(	kIx(	(
<g/>
Н	Н	k?	Н
<g/>
)	)	kIx)	)
na	na	k7c4	na
53	[number]	k4	53
<g/>
°	°	k?	°
<g/>
54	[number]	k4	54
<g/>
'	'	kIx"	'
<g/>
8	[number]	k4	8
<g/>
"	"	kIx"	"
s.	s.	k?	s.
š.	š.	k?	š.
a	a	k8xC	a
27	[number]	k4	27
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
<g/>
41	[number]	k4	41
<g/>
"	"	kIx"	"
v.	v.	k?	v.
d.	d.	k?	d.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Minsku	Minsk	k1gInSc6	Minsk
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1067	[number]	k4	1067
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1326	[number]	k4	1326
se	se	k3xPyFc4	se
Minsk	Minsk	k1gInSc1	Minsk
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Velkovévodství	velkovévodství	k1gNnPc4	velkovévodství
litevského	litevský	k2eAgInSc2d1	litevský
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1499	[number]	k4	1499
získal	získat	k5eAaPmAgInS	získat
městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Minského	minský	k2eAgNnSc2d1	minské
vojvodství	vojvodství	k1gNnSc2	vojvodství
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jej	on	k3xPp3gMnSc4	on
zničili	zničit	k5eAaPmAgMnP	zničit
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
důsledek	důsledek	k1gInSc1	důsledek
tzv.	tzv.	kA	tzv.
druhého	druhý	k4xOgNnSc2	druhý
dělení	dělení	k1gNnSc2	dělení
Polska	Polsko	k1gNnSc2	Polsko
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
anektován	anektovat	k5eAaBmNgInS	anektovat
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
éra	éra	k1gFnSc1	éra
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
jej	on	k3xPp3gMnSc4	on
okupovali	okupovat	k5eAaBmAgMnP	okupovat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
Minsk	Minsk	k1gInSc1	Minsk
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
ale	ale	k9	ale
rekonstruován	rekonstruován	k2eAgMnSc1d1	rekonstruován
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jiná	jiný	k2eAgNnPc1d1	jiné
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Ruiny	ruina	k1gFnPc1	ruina
staletých	staletý	k2eAgInPc2d1	staletý
starých	starý	k2eAgInPc2d1	starý
domů	dům	k1gInPc2	dům
ustoupily	ustoupit	k5eAaPmAgFnP	ustoupit
stalinistické	stalinistický	k2eAgFnPc1d1	stalinistická
architektuře	architektura	k1gFnSc6	architektura
–	–	k?	–
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
tak	tak	k9	tak
velké	velký	k2eAgInPc1d1	velký
bulváry	bulvár	k1gInPc1	bulvár
a	a	k8xC	a
velkolepé	velkolepý	k2eAgFnPc1d1	velkolepá
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
město	město	k1gNnSc1	město
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
překročilo	překročit	k5eAaPmAgNnS	překročit
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Minska	Minsk	k1gInSc2	Minsk
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
venkované	venkovan	k1gMnPc1	venkovan
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
velká	velký	k2eAgFnSc1d1	velká
panelová	panelový	k2eAgFnSc1d1	panelová
sídliště	sídliště	k1gNnSc1	sídliště
<g/>
;	;	kIx,	;
město	město	k1gNnSc1	město
několikrát	několikrát	k6eAd1	několikrát
muselo	muset	k5eAaImAgNnS	muset
posouvat	posouvat	k5eAaImF	posouvat
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
zde	zde	k6eAd1	zde
probíhaly	probíhat	k5eAaImAgFnP	probíhat
masové	masový	k2eAgFnPc1d1	masová
demonstrace	demonstrace	k1gFnPc1	demonstrace
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
požadavků	požadavek	k1gInPc2	požadavek
při	při	k7c6	při
přestavbě	přestavba	k1gFnSc6	přestavba
skomírajícího	skomírající	k2eAgInSc2d1	skomírající
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
koordinační	koordinační	k2eAgInPc4d1	koordinační
orgány	orgán	k1gInPc4	orgán
SNS	SNS	kA	SNS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nová	nový	k2eAgFnSc1d1	nová
doba	doba	k1gFnSc1	doba
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
Minsk	Minsk	k1gInSc1	Minsk
začal	začít	k5eAaPmAgInS	začít
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
nová	nový	k2eAgNnPc1d1	nové
velvyslanectví	velvyslanectví	k1gNnPc1	velvyslanectví
<g/>
,	,	kIx,	,
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
bývalých	bývalý	k2eAgInPc2d1	bývalý
úřadů	úřad	k1gInPc2	úřad
SSR	SSR	kA	SSR
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
úřady	úřad	k1gInPc1	úřad
vládní	vládní	k2eAgInPc1d1	vládní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
k	k	k7c3	k
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
město	město	k1gNnSc4	město
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
mnoho	mnoho	k4c1	mnoho
velkých	velký	k2eAgInPc2d1	velký
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
investic	investice	k1gFnPc2	investice
opět	opět	k6eAd1	opět
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
;	;	kIx,	;
staví	stavit	k5eAaImIp3nS	stavit
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc1	silnice
i	i	k8xC	i
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgFnPc2	tento
aktivit	aktivita	k1gFnPc2	aktivita
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
soukromý	soukromý	k2eAgInSc1d1	soukromý
sektor	sektor	k1gInSc1	sektor
není	být	k5eNaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
dostatečně	dostatečně	k6eAd1	dostatečně
rozvinut	rozvinut	k2eAgInSc1d1	rozvinut
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
omezován	omezovat	k5eAaImNgInS	omezovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Minsk	Minsk	k1gInSc1	Minsk
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
250	[number]	k4	250
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
podpořen	podpořen	k2eAgInSc1d1	podpořen
výstavbou	výstavba	k1gFnSc7	výstavba
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vybavení	vybavení	k1gNnSc2	vybavení
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
první	první	k4xOgFnSc4	první
<g/>
.	.	kIx.	.
a	a	k8xC	a
zejména	zejména	k9	zejména
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
odvětví	odvětví	k1gNnSc1	odvětví
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
vědou	věda	k1gFnSc7	věda
a	a	k8xC	a
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Minsk	Minsk	k1gInSc1	Minsk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
centrem	centr	k1gMnSc7	centr
strojírenství	strojírenství	k1gNnSc2	strojírenství
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
výroby	výroba	k1gFnPc4	výroba
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemického	chemický	k2eAgInSc2d1	chemický
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
gumárenství	gumárenství	k1gNnSc2	gumárenství
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
televizí	televize	k1gFnPc2	televize
<g/>
,	,	kIx,	,
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
přijímačů	přijímač	k1gMnPc2	přijímač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
klíčových	klíčový	k2eAgInPc2d1	klíčový
oborů	obor	k1gInPc2	obor
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
zpracovatelského	zpracovatelský	k2eAgInSc2d1	zpracovatelský
<g/>
,	,	kIx,	,
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
a	a	k8xC	a
textilního	textilní	k2eAgInSc2d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
podnikům	podnik	k1gInPc3	podnik
patří	patřit	k5eAaImIp3nS	patřit
MAZ	maz	k1gInSc1	maz
(	(	kIx(	(
<g/>
Minský	minský	k2eAgInSc1d1	minský
automobilový	automobilový	k2eAgInSc1d1	automobilový
závod	závod	k1gInSc1	závod
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
MTZ	MTZ	kA	MTZ
(	(	kIx(	(
<g/>
Minský	minský	k2eAgInSc1d1	minský
traktorový	traktorový	k2eAgInSc1d1	traktorový
závod	závod	k1gInSc1	závod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
závody	závod	k1gInPc1	závod
jsou	být	k5eAaImIp3nP	být
věhlasné	věhlasný	k2eAgInPc1d1	věhlasný
výrobou	výroba	k1gFnSc7	výroba
osobních	osobní	k2eAgInPc2d1	osobní
i	i	k8xC	i
nákladních	nákladní	k2eAgInPc2d1	nákladní
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
techniky	technika	k1gFnSc2	technika
atd.	atd.	kA	atd.
Jejich	jejich	k3xOp3gFnSc1	jejich
filiálky	filiálka	k1gFnPc1	filiálka
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
sovětských	sovětský	k2eAgFnPc2d1	sovětská
dob	doba	k1gFnPc2	doba
byl	být	k5eAaImAgInS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
úzce	úzko	k6eAd1	úzko
svázán	svázat	k5eAaPmNgInS	svázat
s	s	k7c7	s
dodavateli	dodavatel	k1gMnPc7	dodavatel
a	a	k8xC	a
odběrateli	odběratel	k1gMnPc7	odběratel
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
k	k	k7c3	k
vážným	vážný	k2eAgMnPc3d1	vážný
ekonomickým	ekonomický	k2eAgMnPc3d1	ekonomický
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
neokeynesiánských	okeynesiánský	k2eNgNnPc2d1	okeynesiánský
opatření	opatření	k1gNnPc2	opatření
Alexandra	Alexandra	k1gFnSc1	Alexandra
Lukašenka	Lukašenka	k1gFnSc1	Lukašenka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
většina	většina	k1gFnSc1	většina
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
produkce	produkce	k1gFnSc2	produkce
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
neprošel	projít	k5eNaPmAgInS	projít
Minsk	Minsk	k1gInSc1	Minsk
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
masivní	masivní	k2eAgFnSc7d1	masivní
vlnou	vlna	k1gFnSc7	vlna
deindustrializace	deindustrializace	k1gFnSc2	deindustrializace
jako	jako	k8xC	jako
ostatní	ostatní	k1gNnSc4	ostatní
města	město	k1gNnSc2	město
SNS	SNS	kA	SNS
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
40	[number]	k4	40
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgInPc2d1	aktivní
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
ve	v	k7c6	v
strojírenském	strojírenský	k2eAgInSc6d1	strojírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
70	[number]	k4	70
<g/>
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
je	být	k5eAaImIp3nS	být
exportováno	exportován	k2eAgNnSc1d1	exportováno
<g/>
,	,	kIx,	,
především	především	k9	především
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
SNS	SNS	kA	SNS
<g/>
.	.	kIx.	.
</s>
<s>
Oživení	oživení	k1gNnSc1	oživení
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
nevedlo	vést	k5eNaImAgNnS	vést
ale	ale	k9	ale
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
vybavení	vybavení	k1gNnSc2	vybavení
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
odrazování	odrazování	k1gNnSc2	odrazování
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
průmysl	průmysl	k1gInSc1	průmysl
příliš	příliš	k6eAd1	příliš
konkurenceschopný	konkurenceschopný	k2eAgInSc1d1	konkurenceschopný
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Současné	současný	k2eAgNnSc1d1	současné
administrativní	administrativní	k2eAgNnSc1d1	administrativní
členění	členění	k1gNnSc1	členění
Minska	Minsk	k1gInSc2	Minsk
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
nárůstem	nárůst	k1gInSc7	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
218	[number]	k4	218
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Usnesení	usnesení	k1gNnSc1	usnesení
Výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
BSSR	BSSR	kA	BSSR
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Stalinský	stalinský	k2eAgInSc1d1	stalinský
rajón	rajón	k1gInSc1	rajón
(	(	kIx(	(
<g/>
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1961	[number]	k4	1961
Zavodzký	Zavodzký	k2eAgInSc1d1	Zavodzký
rajón	rajón	k1gInSc1	rajón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Varašylaŭ	Varašylaŭ	k1gMnSc1	Varašylaŭ
rajón	rajón	k1gInSc1	rajón
(	(	kIx(	(
<g/>
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
Sovětský	sovětský	k2eAgInSc1d1	sovětský
rajón	rajón	k1gInSc1	rajón
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kahanovický	Kahanovický	k2eAgInSc1d1	Kahanovický
rajón	rajón	k1gInSc1	rajón
(	(	kIx(	(
<g/>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1957	[number]	k4	1957
Kastryčnický	Kastryčnický	k2eAgInSc1d1	Kastryčnický
rajón	rajón	k1gInSc1	rajón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
9	[number]	k4	9
administrativních	administrativní	k2eAgInPc2d1	administrativní
rajónů	rajón	k1gInPc2	rajón
(	(	kIx(	(
<g/>
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Mírné	mírný	k2eAgNnSc4d1	mírné
přechodné	přechodný	k2eAgNnSc4d1	přechodné
mezi	mezi	k7c7	mezi
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
a	a	k8xC	a
oceánským	oceánský	k2eAgInSc7d1	oceánský
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
-39,1	-39,1	k4	-39,1
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
35,8	[number]	k4	35,8
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
školství	školství	k1gNnSc1	školství
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
mají	mít	k5eAaImIp3nP	mít
asi	asi	k9	asi
32	[number]	k4	32
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
12	[number]	k4	12
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
státní	státní	k2eAgFnSc1d1	státní
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
nebo	nebo	k8xC	nebo
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uspořádaly	uspořádat	k5eAaPmAgFnP	uspořádat
Evropské	evropský	k2eAgFnPc1d1	Evropská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
domácí	domácí	k2eAgMnPc1d1	domácí
sportovci	sportovec	k1gMnPc1	sportovec
zazářili	zazářit	k5eAaPmAgMnP	zazářit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Minsk	Minsk	k1gInSc1	Minsk
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
dopravních	dopravní	k2eAgInPc2d1	dopravní
uzlů	uzel	k1gInPc2	uzel
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
odtud	odtud	k6eAd1	odtud
vycházejí	vycházet	k5eAaImIp3nP	vycházet
sice	sice	k8xC	sice
jen	jen	k9	jen
do	do	k7c2	do
4	[number]	k4	4
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
hlavní	hlavní	k2eAgFnPc4d1	hlavní
evropské	evropský	k2eAgFnPc4d1	Evropská
magistrály	magistrála	k1gFnPc4	magistrála
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jihozápad	jihozápad	k1gInSc1	jihozápad
-	-	kIx~	-
severovýchod	severovýchod	k1gInSc1	severovýchod
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
-	-	kIx~	-
Varšava	Varšava	k1gFnSc1	Varšava
-	-	kIx~	-
Brest	Brest	k1gInSc1	Brest
-	-	kIx~	-
Minsk	Minsk	k1gInSc1	Minsk
-	-	kIx~	-
Orša	Orša	k1gFnSc1	Orša
-	-	kIx~	-
Smolensk	Smolensk	k1gInSc1	Smolensk
-	-	kIx~	-
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
a	a	k8xC	a
severozápad	severozápad	k1gInSc1	severozápad
-	-	kIx~	-
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
(	(	kIx(	(
<g/>
Riga	Riga	k1gFnSc1	Riga
-	-	kIx~	-
Vilnius	Vilnius	k1gInSc1	Vilnius
-	-	kIx~	-
Maladzečna	Maladzečno	k1gNnSc2	Maladzečno
-	-	kIx~	-
Minsk	Minsk	k1gInSc1	Minsk
-	-	kIx~	-
Homel	Homel	k1gInSc1	Homel
-	-	kIx~	-
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejných	stejný	k2eAgInPc6d1	stejný
směrech	směr	k1gInPc6	směr
vedou	vést	k5eAaImIp3nP	vést
i	i	k9	i
hlavní	hlavní	k2eAgInPc1d1	hlavní
silniční	silniční	k2eAgInPc1d1	silniční
tahy	tah	k1gInPc1	tah
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
silnice	silnice	k1gFnPc1	silnice
vedou	vést	k5eAaImIp3nP	vést
na	na	k7c4	na
Sluck	Sluck	k1gInSc4	Sluck
a	a	k8xC	a
Polock	Polock	k1gInSc4	Polock
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Vitebsk	Vitebsk	k1gInSc1	Vitebsk
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Minsk-	Minsk-	k1gFnSc2	Minsk-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
pak	pak	k8xC	pak
leží	ležet	k5eAaImIp3nS	ležet
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
letiště	letiště	k1gNnSc4	letiště
Minsk-	Minsk-	k1gFnPc2	Minsk-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgNnSc1d1	otevřené
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
osobním	osobní	k2eAgNnSc7d1	osobní
nádražím	nádraží	k1gNnSc7	nádraží
je	být	k5eAaImIp3nS	být
Minsk	Minsk	k1gInSc1	Minsk
Passažyrskij	Passažyrskij	k1gFnSc2	Passažyrskij
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
tvoří	tvořit	k5eAaImIp3nS	tvořit
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Minské	minský	k2eAgNnSc1d1	minské
metro	metro	k1gNnSc1	metro
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
20	[number]	k4	20
stanic	stanice	k1gFnPc2	stanice
<g/>
;	;	kIx,	;
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
by	by	kYmCp3nP	by
jich	on	k3xPp3gMnPc2	on
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
již	již	k6eAd1	již
25	[number]	k4	25
<g/>
,	,	kIx,	,
konečný	konečný	k2eAgInSc1d1	konečný
počet	počet	k1gInSc1	počet
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgInS	mít
činit	činit	k5eAaImF	činit
48	[number]	k4	48
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
disponuje	disponovat	k5eAaBmIp3nS	disponovat
8	[number]	k4	8
tramvajovými	tramvajový	k2eAgFnPc7d1	tramvajová
linkami	linka	k1gFnPc7	linka
s	s	k7c7	s
50	[number]	k4	50
zastávkami	zastávka	k1gFnPc7	zastávka
a	a	k8xC	a
70	[number]	k4	70
trolejbusovými	trolejbusový	k2eAgFnPc7d1	trolejbusová
linkami	linka	k1gFnPc7	linka
s	s	k7c7	s
330	[number]	k4	330
zastávkami	zastávka	k1gFnPc7	zastávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Minsk	Minsk	k1gInSc1	Minsk
navázal	navázat	k5eAaPmAgInS	navázat
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
partnerskými	partnerský	k2eAgNnPc7d1	partnerské
městy	město	k1gNnPc7	město
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Káthmándú	Káthmándú	k?	Káthmándú
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
</s>
</p>
<p>
<s>
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
Bengalúru	Bengalúra	k1gFnSc4	Bengalúra
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
</s>
</p>
<p>
<s>
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Detroit	Detroit	k1gInSc1	Detroit
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
</s>
</p>
<p>
<s>
Eindhoven	Eindhoven	k2eAgMnSc1d1	Eindhoven
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
</s>
</p>
<p>
<s>
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Nottingham	Nottingham	k1gInSc1	Nottingham
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Sendai	Sendai	k1gNnSc1	Sendai
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
Postupim	Postupim	k1gFnSc1	Postupim
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Bakersfield	Bakersfield	k1gInSc1	Bakersfield
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
</s>
</p>
<p>
<s>
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
</s>
</p>
<p>
<s>
Teherán	Teherán	k1gInSc1	Teherán
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
</s>
</p>
<p>
<s>
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Minsk	Minsk	k1gInSc1	Minsk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Minsk	Minsk	k1gInSc1	Minsk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Weather	Weathra	k1gFnPc2	Weathra
of	of	k?	of
Minsk	Minsk	k1gInSc1	Minsk
-	-	kIx~	-
minsk	minsk	k1gInSc1	minsk
<g/>
.	.	kIx.	.
<g/>
the	the	k?	the
<g/>
.	.	kIx.	.
<g/>
by	by	k9	by
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ruština	ruština	k1gFnSc1	ruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
Ш	Ш	k?	Ш
п	п	k?	п
с	с	k?	с
М	М	k?	М
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
М	М	k?	М
с	с	k?	с
б	б	k?	б
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
М	М	k?	М
<g/>
,	,	kIx,	,
ш	ш	k?	ш
м	м	k?	м
г	г	k?	г
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
М	М	k?	М
р	р	k?	р
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
П	П	k?	П
м	м	k?	м
э	э	k?	э
с	с	k?	с
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
П	П	k?	П
м	м	k?	м
т	т	k?	т
д	д	k?	д
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
Л	Л	k?	Л
г	г	k?	г
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
В	В	k?	В
ш	ш	k?	ш
у	у	k?	у
М	М	k?	М
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
Т	Т	k?	Т
к	к	k?	к
б	б	k?	б
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jurkau	Jurkau	k6eAd1	Jurkau
kutoczak	kutoczak	k1gInSc1	kutoczak
–	–	k?	–
Ю	Ю	k?	Ю
к	к	k?	к
–	–	k?	–
Yury	Yura	k1gFnSc2	Yura
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Corner	Cornra	k1gFnPc2	Cornra
<g/>
.	.	kIx.	.
М	М	k?	М
п	п	k?	п
ф	ф	k?	ф
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
běloruština	běloruština	k1gFnSc1	běloruština
<g/>
)	)	kIx)	)
</s>
</p>
