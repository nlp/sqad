<s>
Adenin	adenin	k1gInSc1	adenin
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
vitamín	vitamín	k1gInSc1	vitamín
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
heterocyklická	heterocyklický	k2eAgFnSc1d1	heterocyklická
sloučenina	sloučenina	k1gFnSc1	sloučenina
o	o	k7c6	o
sumárním	sumární	k2eAgInSc6d1	sumární
vzorci	vzorec	k1gInSc6	vzorec
C5N5H5	C5N5H5	k1gFnSc2	C5N5H5
a	a	k8xC	a
relativní	relativní	k2eAgFnSc3d1	relativní
molekulové	molekulový	k2eAgFnSc3d1	molekulová
hmotnosti	hmotnost	k1gFnSc3	hmotnost
135,125	[number]	k4	135,125
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
guaninem	guanin	k1gInSc7	guanin
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
dusíkatým	dusíkatý	k2eAgFnPc3d1	dusíkatá
bázím	báze	k1gFnPc3	báze
purinového	purinový	k2eAgInSc2d1	purinový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Adenin	adenin	k1gInSc1	adenin
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
ADP	ADP	kA	ADP
a	a	k8xC	a
AMP	AMP	kA	AMP
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nukleová	nukleový	k2eAgFnSc1d1	nukleová
báze	báze	k1gFnSc1	báze
komplementární	komplementární	k2eAgFnSc1d1	komplementární
s	s	k7c7	s
thyminem	thymin	k1gInSc7	thymin
v	v	k7c6	v
DNA	DNA	kA	DNA
nebo	nebo	k8xC	nebo
uracilem	uracil	k1gMnSc7	uracil
v	v	k7c6	v
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Adenin	adenin	k1gInSc1	adenin
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
molekuly	molekula	k1gFnSc2	molekula
monofosforylovaného	monofosforylovaný	k2eAgInSc2d1	monofosforylovaný
nukleotidu	nukleotid	k1gInSc2	nukleotid
(	(	kIx(	(
<g/>
adenosinmonofosfát	adenosinmonofosfát	k1gInSc1	adenosinmonofosfát
<g/>
,	,	kIx,	,
AMP	AMP	kA	AMP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
enzymatickou	enzymatický	k2eAgFnSc7d1	enzymatická
přeměnou	přeměna	k1gFnSc7	přeměna
z	z	k7c2	z
inositolmonofosfátu	inositolmonofosfát	k1gInSc2	inositolmonofosfát
(	(	kIx(	(
<g/>
IMP	IMP	kA	IMP
<g/>
)	)	kIx)	)
činností	činnost	k1gFnPc2	činnost
enzymů	enzym	k1gInPc2	enzym
adenylosukcinátsyntetázy	adenylosukcinátsyntetáza	k1gFnSc2	adenylosukcinátsyntetáza
a	a	k8xC	a
adenylosukcinátlyázy	adenylosukcinátlyáza	k1gFnSc2	adenylosukcinátlyáza
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
reakcí	reakce	k1gFnPc2	reakce
biosyntézy	biosyntéza	k1gFnSc2	biosyntéza
purinů	purin	k1gInPc2	purin
<g/>
.	.	kIx.	.
</s>
<s>
Degradace	degradace	k1gFnSc1	degradace
adeninu	adenin	k1gInSc2	adenin
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
přes	přes	k7c4	přes
nukleotidové	nukleotidový	k2eAgInPc4d1	nukleotidový
deriváty	derivát	k1gInPc4	derivát
-	-	kIx~	-
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
nejprve	nejprve	k6eAd1	nejprve
převedeny	převést	k5eAaPmNgInP	převést
na	na	k7c4	na
adenosin	adenosin	k1gInSc4	adenosin
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
konvertovány	konvertován	k2eAgInPc1d1	konvertován
adenosindeaminázou	adenosindeamináza	k1gFnSc7	adenosindeamináza
na	na	k7c4	na
inosin	inosin	k1gInSc4	inosin
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
dalšími	další	k2eAgInPc7d1	další
enzymy	enzym	k1gInPc7	enzym
na	na	k7c4	na
hypoxantin	hypoxantin	k1gInSc4	hypoxantin
<g/>
,	,	kIx,	,
xantin	xantin	k1gInSc4	xantin
a	a	k8xC	a
kyselinu	kyselina	k1gFnSc4	kyselina
močovou	močový	k2eAgFnSc4d1	močová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vylučována	vylučovat	k5eAaImNgFnS	vylučovat
močí	moč	k1gFnSc7	moč
<g/>
.	.	kIx.	.
</s>
