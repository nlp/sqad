<s>
Adenin	adenin	k1gInSc1	adenin
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
vitamín	vitamín	k1gInSc1	vitamín
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
heterocyklická	heterocyklický	k2eAgFnSc1d1	heterocyklická
sloučenina	sloučenina	k1gFnSc1	sloučenina
o	o	k7c6	o
sumárním	sumární	k2eAgInSc6d1	sumární
vzorci	vzorec	k1gInSc6	vzorec
C5N5H5	C5N5H5	k1gFnSc2	C5N5H5
a	a	k8xC	a
relativní	relativní	k2eAgFnSc3d1	relativní
molekulové	molekulový	k2eAgFnSc3d1	molekulová
hmotnosti	hmotnost	k1gFnSc3	hmotnost
135,125	[number]	k4	135,125
<g/>
.	.	kIx.	.
</s>
