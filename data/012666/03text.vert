<p>
<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
(	(	kIx(	(
<g/>
Parabuteo	Parabuteo	k1gMnSc1	Parabuteo
unicinctus	unicinctus	k1gMnSc1	unicinctus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
dravec	dravec	k1gMnSc1	dravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jestřábovitých	jestřábovitý	k2eAgMnPc2d1	jestřábovitý
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
jižně	jižně	k6eAd1	jižně
až	až	k9	až
po	po	k7c6	po
Chile	Chile	k1gNnSc6	Chile
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
část	část	k1gFnSc4	část
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
hlášen	hlásit	k5eAaImNgInS	hlásit
i	i	k9	i
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sokolnickým	sokolnický	k2eAgInSc7d1	sokolnický
druhem	druh	k1gInSc7	druh
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
právě	právě	k9	právě
v	v	k7c6	v
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
unikli	uniknout	k5eAaPmAgMnP	uniknout
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
rodu	rod	k1gInSc2	rod
Parabuteo	Parabuteo	k1gNnSc1	Parabuteo
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
para	para	k1gInSc2	para
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
jako	jako	k8xC	jako
nebo	nebo	k8xC	nebo
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
a	a	k8xC	a
latinského	latinský	k2eAgInSc2d1	latinský
buteo	buteo	k6eAd1	buteo
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnSc4d1	označující
káni	káně	k1gFnSc4	káně
<g/>
,	,	kIx,	,
uni	uni	k?	uni
značí	značit	k5eAaImIp3nP	značit
jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
cinctus	cinctus	k1gMnSc1	cinctus
znamená	znamenat	k5eAaImIp3nS	znamenat
obklopený	obklopený	k2eAgMnSc1d1	obklopený
nebo	nebo	k8xC	nebo
ohraničený	ohraničený	k2eAgMnSc1d1	ohraničený
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
bílý	bílý	k2eAgInSc4d1	bílý
pás	pás	k1gInSc4	pás
na	na	k7c6	na
základu	základ	k1gInSc6	základ
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
jméno	jméno	k1gNnSc1	jméno
pták	pták	k1gMnSc1	pták
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Jamese	Jamese	k1gFnSc2	Jamese
Audubona	Audubon	k1gMnSc2	Audubon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svého	svůj	k3xOyFgMnSc2	svůj
ornitologického	ornitologický	k2eAgMnSc2d1	ornitologický
druha	druh	k1gMnSc2	druh
<g/>
,	,	kIx,	,
finančního	finanční	k2eAgMnSc2d1	finanční
sponzora	sponzor	k1gMnSc2	sponzor
a	a	k8xC	a
přítele	přítel	k1gMnSc2	přítel
Edwarda	Edward	k1gMnSc2	Edward
Harrise	Harrise	k1gFnSc2	Harrise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnSc7	svůj
význačným	význačný	k2eAgNnSc7d1	význačné
chováním	chování	k1gNnSc7	chování
-	-	kIx~	-
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
členů	člen	k1gInPc2	člen
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
dravců	dravec	k1gMnPc2	dravec
loví	lovit	k5eAaImIp3nS	lovit
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Obecně	obecně	k6eAd1	obecně
===	===	k?	===
</s>
</p>
<p>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
délkou	délka	k1gFnSc7	délka
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
46	[number]	k4	46
do	do	k7c2	do
76	[number]	k4	76
cm	cm	kA	cm
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
mají	mít	k5eAaImIp3nP	mít
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
okolo	okolo	k7c2	okolo
1,1	[number]	k4	1,1
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
samců	samec	k1gMnPc2	samec
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
710	[number]	k4	710
g	g	kA	g
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
průměr	průměr	k1gInSc1	průměr
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
je	být	k5eAaImIp3nS	být
1020	[number]	k4	1020
g.	g.	k?	g.
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sexuálního	sexuální	k2eAgInSc2d1	sexuální
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
samice	samice	k1gFnPc1	samice
až	až	k6eAd1	až
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgNnSc4d1	hnědé
opeření	opeření	k1gNnSc4	opeření
s	s	k7c7	s
kaštanovými	kaštanový	k2eAgNnPc7d1	kaštanové
rameny	rameno	k1gNnPc7	rameno
<g/>
,	,	kIx,	,
lemem	lem	k1gInSc7	lem
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
stehny	stehno	k1gNnPc7	stehno
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
základ	základ	k1gInSc1	základ
a	a	k8xC	a
konec	konec	k1gInSc1	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
žluté	žlutý	k2eAgFnPc4d1	žlutá
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
žluté	žlutý	k2eAgNnSc4d1	žluté
ozobí	ozobí	k1gNnSc4	ozobí
<g/>
.	.	kIx.	.
</s>
<s>
Hlasové	hlasový	k2eAgInPc1d1	hlasový
projevy	projev	k1gInPc1	projev
kání	káně	k1gFnPc2	káně
Harrisových	Harrisový	k2eAgFnPc2d1	Harrisová
tvoří	tvořit	k5eAaImIp3nS	tvořit
velmi	velmi	k6eAd1	velmi
ostré	ostrý	k2eAgInPc4d1	ostrý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
ptáci	pták	k1gMnPc1	pták
dožívají	dožívat	k5eAaImIp3nP	dožívat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
žijící	žijící	k2eAgFnSc1d1	žijící
zdokumentovaná	zdokumentovaný	k2eAgFnSc1d1	zdokumentovaná
samice	samice	k1gFnSc1	samice
měla	mít	k5eAaImAgFnS	mít
14	[number]	k4	14
let	léto	k1gNnPc2	léto
a	a	k8xC	a
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
ptáci	pták	k1gMnPc1	pták
dožívají	dožívat	k5eAaImIp3nP	dožívat
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dravci	dravec	k1gMnSc6	dravec
v	v	k7c6	v
šatě	šata	k1gFnSc6	šata
mladých	mladý	k2eAgMnPc2d1	mladý
===	===	k?	===
</s>
</p>
<p>
<s>
Dravci	dravec	k1gMnPc1	dravec
v	v	k7c6	v
šatě	šata	k1gFnSc6	šata
mladých	mladý	k1gMnPc2	mladý
jsou	být	k5eAaImIp3nP	být
podobní	podobný	k2eAgMnPc1d1	podobný
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
žíhaní	žíhaný	k2eAgMnPc1d1	žíhaný
a	a	k8xC	a
během	během	k7c2	během
letu	let	k1gInSc2	let
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
naspodu	naspodu	k7c2	naspodu
křídel	křídlo	k1gNnPc2	křídlo
pozorovat	pozorovat	k5eAaImF	pozorovat
bledě	bledě	k6eAd1	bledě
žluté	žlutý	k2eAgNnSc4d1	žluté
zbarvení	zbarvení	k1gNnSc4	zbarvení
s	s	k7c7	s
hnědým	hnědý	k2eAgNnSc7d1	hnědé
žíháním	žíhání	k1gNnSc7	žíhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poddruhy	poddruh	k1gInPc4	poddruh
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
tři	tři	k4xCgInPc1	tři
poddruhy	poddruh	k1gInPc1	poddruh
káně	káně	k1gFnSc2	káně
Harrisovy	Harrisův	k2eAgInPc1d1	Harrisův
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
P.	P.	kA	P.
u.	u.	k?	u.
superior	superior	k1gMnSc1	superior
<g/>
:	:	kIx,	:
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c4	v
Baja	Baj	k2eAgNnPc4d1	Baj
California	Californium	k1gNnPc4	Californium
<g/>
,	,	kIx,	,
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
,	,	kIx,	,
Sonoře	sonora	k1gFnSc6	sonora
<g/>
,	,	kIx,	,
Sinaloe	Sinaloe	k1gFnSc1	Sinaloe
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
P.	P.	kA	P.
u.	u.	k?	u.
superior	superior	k1gMnSc1	superior
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
delší	dlouhý	k2eAgNnPc4d2	delší
křídla	křídlo	k1gNnPc4	křídlo
i	i	k8xC	i
tmavší	tmavý	k2eAgNnSc4d2	tmavší
zbarvení	zbarvení	k1gNnSc4	zbarvení
než	než	k8xS	než
P.	P.	kA	P.
u.	u.	k?	u.
harrisi	harrise	k1gFnSc6	harrise
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
množství	množství	k1gNnSc1	množství
pozorovaných	pozorovaný	k2eAgMnPc2d1	pozorovaný
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc1	pět
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
šest	šest	k4xCc4	šest
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
doplnil	doplnit	k5eAaPmAgMnS	doplnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzické	fyzický	k2eAgInPc1d1	fyzický
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
odůvodněnosti	odůvodněnost	k1gFnSc3	odůvodněnost
oddělení	oddělení	k1gNnSc2	oddělení
poddruhu	poddruh	k1gInSc2	poddruh
hovoří	hovořit	k5eAaImIp3nP	hovořit
též	též	k9	též
další	další	k2eAgInPc1d1	další
ekologické	ekologický	k2eAgInPc1d1	ekologický
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
klima	klima	k1gNnSc4	klima
daného	daný	k2eAgNnSc2d1	dané
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
P.	P.	kA	P.
u.	u.	k?	u.
harrisi	harrise	k1gFnSc6	harrise
<g/>
:	:	kIx,	:
nalezen	nalezen	k2eAgMnSc1d1	nalezen
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
východním	východní	k2eAgNnSc6d1	východní
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
většině	většina	k1gFnSc6	většina
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
P.	P.	kA	P.
u.	u.	k?	u.
unicinctus	unicinctus	k1gInSc1	unicinctus
<g/>
:	:	kIx,	:
nalezen	nalézt	k5eAaBmNgInS	nalézt
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
severoamerický	severoamerický	k2eAgInSc1d1	severoamerický
poddruh	poddruh	k1gInSc1	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Biotop	biotop	k1gInSc4	biotop
===	===	k?	===
</s>
</p>
<p>
<s>
Ptáci	pták	k1gMnPc1	pták
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
řídkých	řídký	k2eAgInPc6d1	řídký
lesích	les	k1gInPc6	les
a	a	k8xC	a
polopouštích	polopoušť	k1gFnPc6	polopoušť
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
mokřinách	mokřina	k1gFnPc6	mokřina
(	(	kIx(	(
<g/>
s	s	k7c7	s
občasnými	občasný	k2eAgInPc7d1	občasný
stromy	strom	k1gInPc7	strom
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
jejich	jejich	k3xOp3gNnSc2	jejich
rozšíření	rozšíření	k1gNnSc2	rozšíření
(	(	kIx(	(
<g/>
Howell	Howell	k1gInSc1	Howell
a	a	k8xC	a
Webb	Webb	k1gInSc1	Webb
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
také	také	k9	také
v	v	k7c6	v
mangrovových	mangrovový	k2eAgFnPc6d1	mangrovová
bažinách	bažina	k1gFnPc6	bažina
<g/>
,	,	kIx,	,
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
oblasti	oblast	k1gFnSc6	oblast
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
nemigruje	migrovat	k5eNaImIp3nS	migrovat
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
trvalé	trvalý	k2eAgNnSc4d1	trvalé
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
0,2	[number]	k4	0,2
-	-	kIx~	-
5,5	[number]	k4	5,5
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Potrava	potrava	k1gFnSc1	potrava
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
malých	malý	k2eAgMnPc2d1	malý
živočichů	živočich	k1gMnPc2	živočich
včetně	včetně	k7c2	včetně
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
ještěrek	ještěrka	k1gFnPc2	ještěrka
<g/>
,	,	kIx,	,
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
velkého	velký	k2eAgInSc2d1	velký
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
loví	lovit	k5eAaImIp3nS	lovit
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
ulovit	ulovit	k5eAaPmF	ulovit
též	též	k9	též
větší	veliký	k2eAgFnSc4d2	veliký
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
králíka	králík	k1gMnSc2	králík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lov	lov	k1gInSc1	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
dravců	dravec	k1gMnPc2	dravec
samotářských	samotářský	k2eAgInPc2d1	samotářský
a	a	k8xC	a
setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
a	a	k8xC	a
tahu	tah	k1gInSc6	tah
<g/>
,	,	kIx,	,
káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
loví	lovit	k5eAaImIp3nS	lovit
ve	v	k7c6	v
spolupracujících	spolupracující	k2eAgFnPc6d1	spolupracující
skupinách	skupina	k1gFnPc6	skupina
od	od	k7c2	od
dvou	dva	k4xCgNnPc2	dva
do	do	k7c2	do
čtyř	čtyři	k4xCgMnPc2	čtyři
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kooperace	kooperace	k1gFnSc1	kooperace
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
adaptace	adaptace	k1gFnSc2	adaptace
na	na	k7c4	na
pouštní	pouštní	k2eAgNnSc4d1	pouštní
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
lovecká	lovecký	k2eAgFnSc1d1	lovecká
technika	technika	k1gFnSc1	technika
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
létá	létat	k5eAaImIp3nS	létat
vepředu	vepředu	k6eAd1	vepředu
a	a	k8xC	a
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
kořisti	kořist	k1gFnSc6	kořist
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiný	jiný	k2eAgMnSc1d1	jiný
člen	člen	k1gMnSc1	člen
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
letí	letět	k5eAaImIp3nS	letět
zezadu	zezadu	k6eAd1	zezadu
a	a	k8xC	a
kořist	kořist	k1gFnSc1	kořist
nahání	nahánět	k5eAaImIp3nS	nahánět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
kořist	kořist	k1gFnSc1	kořist
zahnána	zahnán	k2eAgFnSc1d1	zahnána
do	do	k7c2	do
pasti	past	k1gFnSc2	past
a	a	k8xC	a
následně	následně	k6eAd1	následně
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
technika	technika	k1gFnSc1	technika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozmístí	rozmístit	k5eAaPmIp3nS	rozmístit
okolo	okolo	k7c2	okolo
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
člen	člen	k1gInSc1	člen
ji	on	k3xPp3gFnSc4	on
usmrtí	usmrtit	k5eAaPmIp3nP	usmrtit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc5	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
predátorem	predátor	k1gMnSc7	predátor
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kání	káně	k1gFnPc2	káně
je	být	k5eAaImIp3nS	být
výr	výr	k1gMnSc1	výr
virginský	virginský	k2eAgMnSc1d1	virginský
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
ptáky	pták	k1gMnPc4	pták
představují	představovat	k5eAaImIp3nP	představovat
největší	veliký	k2eAgNnSc4d3	veliký
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
zejména	zejména	k9	zejména
kojoti	kojot	k1gMnPc1	kojot
a	a	k8xC	a
velcí	velký	k2eAgMnPc1d1	velký
krkavci	krkavec	k1gMnPc1	krkavec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vybírají	vybírat	k5eAaImIp3nP	vybírat
i	i	k9	i
jejich	jejich	k3xOp3gNnSc2	jejich
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Káně	káně	k1gFnSc1	káně
však	však	k9	však
svá	svůj	k3xOyFgNnPc4	svůj
hnízda	hnízdo	k1gNnPc4	hnízdo
aktivně	aktivně	k6eAd1	aktivně
brání	bránit	k5eAaImIp3nS	bránit
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pokusu	pokus	k1gInSc2	pokus
o	o	k7c6	o
napadení	napadení	k1gNnSc6	napadení
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
varovným	varovný	k2eAgInSc7d1	varovný
voláním	volání	k1gNnSc7	volání
a	a	k8xC	a
na	na	k7c4	na
predátora	predátor	k1gMnSc4	predátor
útočí	útočit	k5eAaImIp3nS	útočit
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gNnSc4	on
od	od	k7c2	od
vyplenění	vyplenění	k1gNnSc2	vyplenění
hnízda	hnízdo	k1gNnSc2	hnízdo
odradila	odradit	k5eAaPmAgFnS	odradit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnízdění	hnízdění	k1gNnPc4	hnízdění
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Káně	káně	k1gFnPc1	káně
Harrisovy	Harrisův	k2eAgFnPc1d1	Harrisova
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
malých	malý	k2eAgInPc6d1	malý
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
křovinách	křovina	k1gFnPc6	křovina
nebo	nebo	k8xC	nebo
kaktusech	kaktus	k1gInPc6	kaktus
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
švédské	švédský	k2eAgNnSc4d1	švédské
pojmenování	pojmenování	k1gNnSc4	pojmenování
-	-	kIx~	-
kaktusvrå	kaktusvrå	k?	kaktusvrå
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
kompaktní	kompaktní	k2eAgNnSc1d1	kompaktní
<g/>
,	,	kIx,	,
vystavěné	vystavěný	k2eAgNnSc1d1	vystavěné
z	z	k7c2	z
větviček	větvička	k1gFnPc2	větvička
<g/>
,	,	kIx,	,
kořenů	kořen	k1gInPc2	kořen
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
mechu	mech	k1gInSc2	mech
a	a	k8xC	a
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
většinou	většinou	k6eAd1	většinou
staví	stavit	k5eAaBmIp3nS	stavit
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
většinou	většina	k1gFnSc7	většina
dvě	dva	k4xCgNnPc4	dva
až	až	k9	až
čtyři	čtyři	k4xCgNnPc4	čtyři
vejce	vejce	k1gNnPc4	vejce
barvy	barva	k1gFnSc2	barva
bílé	bílý	k2eAgFnPc4d1	bílá
až	až	k6eAd1	až
namodralé	namodralý	k2eAgFnPc1d1	namodralá
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	s	k7c7	s
světle	světle	k6eAd1	světle
hnědými	hnědý	k2eAgFnPc7d1	hnědá
nebo	nebo	k8xC	nebo
šedými	šedý	k2eAgFnPc7d1	šedá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdoši	hnízdoš	k1gMnPc1	hnízdoš
jsou	být	k5eAaImIp3nP	být
zprvu	zprvu	k6eAd1	zprvu
světle	světle	k6eAd1	světle
žlutohnědí	žlutohnědý	k2eAgMnPc1d1	žlutohnědý
<g/>
,	,	kIx,	,
v	v	k7c6	v
pěti	pět	k4xCc6	pět
nebo	nebo	k8xC	nebo
šesti	šest	k4xCc6	šest
dnech	den	k1gInPc6	den
změní	změnit	k5eAaPmIp3nS	změnit
barvu	barva	k1gFnSc4	barva
na	na	k7c4	na
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
<g/>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
obývají	obývat	k5eAaImIp3nP	obývat
hnízdo	hnízdo	k1gNnSc4	hnízdo
tři	tři	k4xCgFnPc4	tři
káně	káně	k1gFnPc4	káně
<g/>
:	:	kIx,	:
dva	dva	k4xCgMnPc1	dva
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
polyandrii	polyandrie	k1gFnSc6	polyandrie
<g/>
,	,	kIx,	,
matoucí	matoucí	k2eAgMnSc1d1	matoucí
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
backstanding	backstanding	k1gInSc1	backstanding
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
pták	pták	k1gMnSc1	pták
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
prvního	první	k4xOgNnSc2	první
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k9	ještě
třetí	třetí	k4xOgFnSc7	třetí
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
druhého	druhý	k4xOgMnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
31	[number]	k4	31
do	do	k7c2	do
36	[number]	k4	36
dnů	den	k1gInPc2	den
a	a	k8xC	a
na	na	k7c6	na
snůšce	snůška	k1gFnSc6	snůška
sedí	sedit	k5eAaImIp3nS	sedit
většinou	většina	k1gFnSc7	většina
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
38	[number]	k4	38
dnech	den	k1gInPc6	den
začínají	začínat	k5eAaImIp3nP	začínat
mláďata	mládě	k1gNnPc4	mládě
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
okolí	okolí	k1gNnSc3	okolí
hnízda	hnízdo	k1gNnSc2	hnízdo
a	a	k8xC	a
ve	v	k7c6	v
45	[number]	k4	45
až	až	k9	až
50	[number]	k4	50
dnech	den	k1gInPc6	den
začínají	začínat	k5eAaImIp3nP	začínat
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
dvakrát	dvakrát	k6eAd1	dvakrát
nebo	nebo	k8xC	nebo
třikrát	třikrát	k6eAd1	třikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
mohou	moct	k5eAaImIp3nP	moct
zůstat	zůstat	k5eAaPmF	zůstat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
s	s	k7c7	s
vyváděním	vyvádění	k1gNnSc7	vyvádění
dalšího	další	k2eAgNnSc2d1	další
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
káně	káně	k1gFnPc1	káně
Harrisovy	Harrisův	k2eAgFnPc1d1	Harrisova
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnSc2	změna
biotopu	biotop	k1gInSc2	biotop
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
není	být	k5eNaImIp3nS	být
káně	káně	k1gNnSc1	káně
Harrisova	Harrisův	k2eAgInSc2d1	Harrisův
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
divoká	divoký	k2eAgFnSc1d1	divoká
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
kvůli	kvůli	k7c3	kvůli
ztrátě	ztráta	k1gFnSc3	ztráta
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
nicméně	nicméně	k8xC	nicméně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozvinutých	rozvinutý	k2eAgFnPc2d1	rozvinutá
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jako	jako	k9	jako
původní	původní	k2eAgInSc1d1	původní
druh	druh	k1gInSc1	druh
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
sedmi	sedm	k4xCc2	sedm
států	stát	k1gInPc2	stát
Střední	střední	k2eAgFnSc2d1	střední
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
<g/>
,	,	kIx,	,
Panamy	Panama	k1gFnSc2	Panama
a	a	k8xC	a
Surinamu	Surinam	k1gInSc2	Surinam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
původ	původ	k1gInSc1	původ
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
málo	málo	k4c1	málo
dotčených	dotčený	k2eAgInPc2d1	dotčený
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
v	v	k7c6	v
CITES	CITES	kA	CITES
jej	on	k3xPp3gMnSc4	on
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
populace	populace	k1gFnSc2	populace
káně	káně	k1gFnSc2	káně
Harrisovy	Harrisův	k2eAgFnSc2d1	Harrisova
čítala	čítat	k5eAaImAgFnS	čítat
přibližně	přibližně	k6eAd1	přibližně
390	[number]	k4	390
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
velké	velká	k1gFnSc2	velká
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
000	[number]	k4	000
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sokolnictví	sokolnictví	k1gNnSc2	sokolnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zvedat	zvedat	k5eAaImF	zvedat
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
káně	káně	k1gFnSc2	káně
Harrisovy	Harrisův	k2eAgFnSc2d1	Harrisova
v	v	k7c6	v
sokolnictví	sokolnictví	k1gNnSc6	sokolnictví
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
dravcem	dravec	k1gMnSc7	dravec
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
chovaným	chovaný	k2eAgFnPc3d1	chovaná
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
jednoduchému	jednoduchý	k2eAgInSc3d1	jednoduchý
výcviku	výcvik	k1gInSc3	výcvik
a	a	k8xC	a
snadné	snadný	k2eAgFnSc3d1	snadná
socializaci	socializace	k1gFnSc3	socializace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
umění	umění	k1gNnSc6	umění
===	===	k?	===
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
James	James	k1gMnSc1	James
Audubon	Audubon	k1gMnSc1	Audubon
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ilustraci	ilustrace	k1gFnSc4	ilustrace
káně	káně	k1gFnSc2	káně
Harrisovy	Harrisův	k2eAgFnSc2d1	Harrisova
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
Birds	Birdsa	k1gFnPc2	Birdsa
of	of	k?	of
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
č.	č.	k?	č.
392	[number]	k4	392
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
Louisiana	Louisiana	k1gFnSc1	Louisiana
Hawk	Hawk	k1gMnSc1	Hawk
-	-	kIx~	-
Buteo	Buteo	k1gMnSc1	Buteo
harrisi	harrise	k1gFnSc4	harrise
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
vyryt	vyrýt	k5eAaPmNgInS	vyrýt
a	a	k8xC	a
kolorován	kolorován	k2eAgInSc1d1	kolorován
Robertem	Robert	k1gMnSc7	Robert
Havellem	Havell	k1gMnSc7	Havell
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
dílně	dílna	k1gFnSc6	dílna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgNnSc1d1	originální
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
Newyorskou	newyorský	k2eAgFnSc7d1	newyorská
historickou	historický	k2eAgFnSc7d1	historická
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gMnSc4	on
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
až	až	k9	až
do	do	k7c2	do
nynějška	nynějšek	k1gInSc2	nynějšek
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Harris	Harris	k1gFnSc2	Harris
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hawk	Hawka	k1gFnPc2	Hawka
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HOWELL	HOWELL	kA	HOWELL
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
<g/>
,	,	kIx,	,
N.	N.	kA	N.
G	G	kA	G
<g/>
;	;	kIx,	;
WEBB	WEBB	kA	WEBB
<g/>
,	,	kIx,	,
Sophie	Sophie	k1gFnSc1	Sophie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
guide	guide	k6eAd1	guide
to	ten	k3xDgNnSc1	ten
the	the	k?	the
birds	birds	k1gInSc1	birds
of	of	k?	of
Mexico	Mexico	k6eAd1	Mexico
and	and	k?	and
northern	northern	k1gMnSc1	northern
Central	Central	k1gMnSc1	Central
America	America	k1gMnSc1	America
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
854012	[number]	k4	854012
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
káně	káně	k1gFnSc2	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Parabuteo	Parabuteo	k1gMnSc1	Parabuteo
unicinctus	unicinctus	k1gMnSc1	unicinctus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
káni	káně	k1gFnSc6	káně
Harrisově	Harrisův	k2eAgFnSc6d1	Harrisova
na	na	k7c6	na
Peregrine	Peregrin	k1gInSc5	Peregrin
Fund	fund	k1gInSc4	fund
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
káni	káně	k1gFnSc6	káně
Harrisově	Harrisův	k2eAgFnSc6d1	Harrisova
na	na	k7c6	na
Borch	Borch	k1gMnSc1	Borch
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Falconry	Falconr	k1gMnPc7	Falconr
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Krátké	Krátké	k2eAgNnSc1d1	Krátké
popisné	popisný	k2eAgNnSc1d1	popisné
video	video	k1gNnSc1	video
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
v	v	k7c6	v
sokolnictví	sokolnictví	k1gNnSc6	sokolnictví
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
fotoalbum	fotoalbum	k1gNnSc1	fotoalbum
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
)	)	kIx)	)
</s>
</p>
