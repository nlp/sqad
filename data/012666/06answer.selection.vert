<s>
Káně	káně	k1gFnSc1	káně
Harrisova	Harrisův	k2eAgFnSc1d1	Harrisova
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnSc7	svůj
význačným	význačný	k2eAgNnSc7d1	význačné
chováním	chování	k1gNnSc7	chování
-	-	kIx~	-
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
členů	člen	k1gInPc2	člen
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
dravců	dravec	k1gMnPc2	dravec
loví	lovit	k5eAaImIp3nS	lovit
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
