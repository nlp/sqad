<s>
Novo	nova	k1gFnSc5
Selo	selo	k1gNnSc1
(	(	kIx(
<g/>
Mavrovo	Mavrův	k2eAgNnSc1d1
a	a	k8xC
Rostuša	Rostuša	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Novo	nova	k1gFnSc5
Selo	selo	k1gNnSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
600	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
</s>
<s>
Novo	nova	k1gFnSc5
Selo	selo	k1gNnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
33	#num#	k4
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
042	#num#	k4
PSČ	PSČ	kA
</s>
<s>
1230	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Novo	nova	k1gFnSc5
Selo	selo	k1gNnSc1
(	(	kIx(
<g/>
makedonsky	makedonsky	k6eAd1
<g/>
:	:	kIx,
Н	Н	k?
С	С	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
v	v	k7c6
opštině	opština	k1gFnSc6
Mavrovo	Mavrův	k2eAgNnSc1d1
a	a	k8xC
Rostuša	Rostuša	k1gFnSc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Položském	Položský	k2eAgInSc6d1
regionu	region	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
žije	žít	k5eAaImIp3nS
v	v	k7c6
opštině	opština	k1gFnSc6
33	#num#	k4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
všichni	všechen	k3xTgMnPc1
jsou	být	k5eAaImIp3nP
Makedonci	Makedonec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Novo	nova	k1gFnSc5
Selo	selo	k1gNnSc1
<g/>
,	,	kIx,
Mavrovo	Mavrův	k2eAgNnSc1d1
and	and	k?
Rostuša	Rostušum	k1gNnPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnSc1d1
</s>
<s>
Opština	opština	k1gFnSc1
Mavrovo	Mavrův	k2eAgNnSc1d1
a	a	k8xC
Rostuša	Rostuša	k1gFnSc1
</s>
<s>
Položský	Položský	k2eAgInSc1d1
region	region	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Novo	nova	k1gFnSc5
Selo	sít	k5eAaImAgNnS
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
