<s>
Absces	absces	k1gInSc1	absces
(	(	kIx(	(
<g/>
abscessus	abscessus	k1gInSc1	abscessus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
<g/>
,	,	kIx,	,
chorobná	chorobný	k2eAgFnSc1d1	chorobná
dutina	dutina	k1gFnSc1	dutina
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
zánětem	zánět	k1gInSc7	zánět
a	a	k8xC	a
vyplněná	vyplněná	k1gFnSc1	vyplněná
hnisem	hnis	k1gInSc7	hnis
<g/>
.	.	kIx.	.
</s>
<s>
Absces	absces	k1gInSc1	absces
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
orgánu	orgán	k1gInSc6	orgán
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jeho	jeho	k3xOp3gNnSc3	jeho
poškození	poškození	k1gNnSc3	poškození
a	a	k8xC	a
místní	místní	k2eAgInPc4d1	místní
příznaky	příznak	k1gInPc4	příznak
(	(	kIx(	(
<g/>
okolní	okolní	k2eAgInSc4d1	okolní
otok	otok	k1gInSc4	otok
<g/>
,	,	kIx,	,
útlak	útlak	k1gInSc4	útlak
<g/>
,	,	kIx,	,
palčivou	palčivý	k2eAgFnSc4d1	palčivá
bolest	bolest	k1gFnSc4	bolest
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
i	i	k9	i
celkové	celkový	k2eAgInPc1d1	celkový
příznaky	příznak	k1gInPc1	příznak
chronického	chronický	k2eAgInSc2d1	chronický
zánětu	zánět	k1gInSc2	zánět
(	(	kIx(	(
<g/>
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
hubnutí	hubnutí	k1gNnSc4	hubnutí
<g/>
,	,	kIx,	,
nechutenství	nechutenství	k1gNnSc4	nechutenství
<g/>
,	,	kIx,	,
anemii	anemie	k1gFnSc4	anemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absces	absces	k1gInSc1	absces
vzniká	vznikat	k5eAaImIp3nS	vznikat
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zánět	zánět	k1gInSc1	zánět
mazových	mazový	k2eAgFnPc2d1	mazová
žlázek	žlázka	k1gFnPc2	žlázka
či	či	k8xC	či
vlasových	vlasový	k2eAgInPc2d1	vlasový
folikulů	folikul	k1gInPc2	folikul
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
bez	bez	k7c2	bez
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
a	a	k8xC	a
vážnější	vážní	k2eAgInPc1d2	vážnější
jsou	být	k5eAaImIp3nP	být
abscesy	absces	k1gInPc1	absces
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
kořenové	kořenový	k2eAgFnSc2d1	kořenová
hrotu	hrot	k1gInSc2	hrot
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
propagovat	propagovat	k5eAaImF	propagovat
do	do	k7c2	do
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
mozku	mozek	k1gInSc2	mozek
či	či	k8xC	či
mediastina	mediastinum	k1gNnSc2	mediastinum
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
abscesu	absces	k1gInSc2	absces
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Absces	absces	k1gInSc1	absces
vzniká	vznikat	k5eAaImIp3nS	vznikat
většinou	většina	k1gFnSc7	většina
po	po	k7c6	po
hlubším	hluboký	k2eAgNnSc6d2	hlubší
poranění	poranění	k1gNnSc6	poranění
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
bulku	bulka	k1gFnSc4	bulka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
značné	značný	k2eAgFnPc4d1	značná
bolesti	bolest	k1gFnPc4	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Absces	absces	k1gInSc1	absces
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vstřebat	vstřebat	k5eAaPmF	vstřebat
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
praskne	prasknout	k5eAaPmIp3nS	prasknout
a	a	k8xC	a
hnis	hnis	k1gInSc1	hnis
se	se	k3xPyFc4	se
vyvalí	vyvalit	k5eAaPmIp3nS	vyvalit
ven	ven	k6eAd1	ven
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horším	zlý	k2eAgInSc6d2	horší
případě	případ	k1gInSc6	případ
praskne	prasknout	k5eAaPmIp3nS	prasknout
dutina	dutina	k1gFnSc1	dutina
dovnitř	dovnitř	k6eAd1	dovnitř
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
nastartovat	nastartovat	k5eAaPmF	nastartovat
flegmónu	flegmóna	k1gFnSc4	flegmóna
(	(	kIx(	(
<g/>
neohraničené	ohraničený	k2eNgNnSc4d1	neohraničené
hnisání	hnisání	k1gNnSc4	hnisání
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
otravu	otrava	k1gFnSc4	otrava
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Absces	absces	k1gInSc1	absces
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
častý	častý	k2eAgInSc1d1	častý
následek	následek	k1gInSc1	následek
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
mezi	mezi	k7c7	mezi
narkomany	narkoman	k1gMnPc7	narkoman
užívajícími	užívající	k2eAgMnPc7d1	užívající
drogy	droga	k1gFnPc4	droga
nitrožilně	nitrožilně	k6eAd1	nitrožilně
a	a	k8xC	a
bez	bez	k7c2	bez
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Bolestivý	bolestivý	k2eAgInSc1d1	bolestivý
absces	absces	k1gInSc1	absces
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
mj.	mj.	kA	mj.
např.	např.	kA	např.
hordeola	hordeola	k1gFnSc1	hordeola
(	(	kIx(	(
<g/>
ječné	ječný	k2eAgNnSc1d1	ječné
zrno	zrno	k1gNnSc1	zrno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
většího	veliký	k2eAgInSc2d2	veliký
abscesu	absces	k1gInSc2	absces
je	být	k5eAaImIp3nS	být
chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
incizi	incize	k1gFnSc4	incize
(	(	kIx(	(
<g/>
rozříznutí	rozříznutí	k1gNnSc2	rozříznutí
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyčištění	vyčištění	k1gNnSc4	vyčištění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
výplach	výplach	k1gInSc4	výplach
dezinfekčním	dezinfekční	k2eAgInSc7d1	dezinfekční
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
sterilním	sterilní	k2eAgInSc7d1	sterilní
roztokem	roztok	k1gInSc7	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Punkce	punkce	k1gFnPc4	punkce
jehlou	jehla	k1gFnSc7	jehla
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
<g/>
,	,	kIx,	,
abscesová	abscesový	k2eAgFnSc1d1	abscesový
dutina	dutina	k1gFnSc1	dutina
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ošetřena	ošetřit	k5eAaPmNgFnS	ošetřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
volný	volný	k2eAgInSc1d1	volný
odtok	odtok	k1gInSc1	odtok
sekretů	sekret	k1gInPc2	sekret
<g/>
,	,	kIx,	,
např.	např.	kA	např.
umístěním	umístění	k1gNnSc7	umístění
drénu	drén	k1gInSc2	drén
nebo	nebo	k8xC	nebo
širokým	široký	k2eAgNnSc7d1	široké
vyšitím	vyšití	k1gNnSc7	vyšití
abscesové	abscesový	k2eAgFnSc2d1	abscesový
dutiny	dutina	k1gFnSc2	dutina
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
dle	dle	k7c2	dle
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Angína	angína	k1gFnSc1	angína
Cysta	cysta	k1gFnSc1	cysta
Flegmóna	flegmóna	k1gFnSc1	flegmóna
Infekce	infekce	k1gFnSc2	infekce
Panaricium	Panaricium	k1gNnSc1	Panaricium
(	(	kIx(	(
<g/>
vidlák	vidlák	k1gInSc1	vidlák
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Absces	absces	k1gInSc4	absces
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Absces	absces	k1gInSc4	absces
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Absces	absces	k1gInSc1	absces
-	-	kIx~	-
příčiny	příčina	k1gFnPc1	příčina
<g/>
,	,	kIx,	,
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
absces	absces	k1gInSc1	absces
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
