<s>
Deflace	deflace	k1gFnSc1	deflace
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgInSc4d1	absolutní
meziroční	meziroční	k2eAgInSc4d1	meziroční
pokles	pokles	k1gInSc4	pokles
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
indexem	index	k1gInSc7	index
spotřebitelských	spotřebitelský	k2eAgFnPc2d1	spotřebitelská
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
deflace	deflace	k1gFnSc2	deflace
je	být	k5eAaImIp3nS	být
inflace	inflace	k1gFnSc1	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Deflace	deflace	k1gFnSc1	deflace
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
rysů	rys	k1gInPc2	rys
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Irving	Irving	k1gInSc1	Irving
Fisher	Fishra	k1gFnPc2	Fishra
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
negativní	negativní	k2eAgFnSc2d1	negativní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
popsal	popsat	k5eAaPmAgInS	popsat
fenomén	fenomén	k1gInSc1	fenomén
dluhové	dluhový	k2eAgFnSc2d1	dluhová
deflace	deflace	k1gFnSc2	deflace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pokles	pokles	k1gInSc1	pokles
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
reálnou	reálný	k2eAgFnSc4d1	reálná
hodnotu	hodnota	k1gFnSc4	hodnota
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dále	daleko	k6eAd2	daleko
podrývá	podrývat	k5eAaImIp3nS	podrývat
poptávku	poptávka	k1gFnSc4	poptávka
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
snižování	snižování	k1gNnSc3	snižování
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc1	zvýšení
reálné	reálný	k2eAgFnSc2d1	reálná
dluhové	dluhový	k2eAgFnSc2d1	dluhová
zátěže	zátěž	k1gFnSc2	zátěž
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tohoto	tento	k3xDgInSc2	tento
mechanismu	mechanismus	k1gInSc2	mechanismus
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
deflace	deflace	k1gFnSc1	deflace
velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
nejen	nejen	k6eAd1	nejen
makroekonomický	makroekonomický	k2eAgInSc4d1	makroekonomický
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
finanční	finanční	k2eAgFnSc4d1	finanční
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
negativní	negativní	k2eAgFnSc4d1	negativní
zkušenost	zkušenost	k1gFnSc4	zkušenost
s	s	k7c7	s
deflací	deflace	k1gFnSc7	deflace
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
i	i	k9	i
tzv.	tzv.	kA	tzv.
dvě	dva	k4xCgFnPc4	dva
ztracené	ztracený	k2eAgFnPc4d1	ztracená
dekády	dekáda	k1gFnPc4	dekáda
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
deflaci	deflace	k1gFnSc4	deflace
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
jako	jako	k9	jako
důsledek	důsledek	k1gInSc4	důsledek
<g/>
,	,	kIx,	,
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgNnP	být
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
záměrem	záměr	k1gInSc7	záměr
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
úspěšně	úspěšně	k6eAd1	úspěšně
oddělil	oddělit	k5eAaPmAgMnS	oddělit
československou	československý	k2eAgFnSc4d1	Československá
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
uchránil	uchránit	k5eAaPmAgMnS	uchránit
před	před	k7c7	před
poválečnou	poválečný	k2eAgFnSc7d1	poválečná
inflací	inflace	k1gFnSc7	inflace
<g/>
,	,	kIx,	,
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
oběživa	oběživo	k1gNnSc2	oběživo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
žák	žák	k1gMnSc1	žák
A.	A.	kA	A.
Bráfa	Bráf	k1gMnSc4	Bráf
a	a	k8xC	a
přívrženec	přívrženec	k1gMnSc1	přívrženec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
školy	škola	k1gFnSc2	škola
byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
prosperity	prosperita	k1gFnSc2	prosperita
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
měna	měna	k1gFnSc1	měna
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
důslednou	důsledný	k2eAgFnSc4d1	důsledná
deflační	deflační	k2eAgFnSc4d1	deflační
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
standardu	standard	k1gInSc6	standard
a	a	k8xC	a
přes	přes	k7c4	přes
varování	varování	k1gNnSc4	varování
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zpevnit	zpevnit	k5eAaPmF	zpevnit
kurs	kurs	k1gInSc4	kurs
koruny	koruna	k1gFnSc2	koruna
k	k	k7c3	k
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
měnám	měna	k1gFnPc3	měna
právě	právě	k9	právě
deflací	deflace	k1gFnSc7	deflace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1921	[number]	k4	1921
a	a	k8xC	a
1923	[number]	k4	1923
se	se	k3xPyFc4	se
cenová	cenový	k2eAgFnSc1d1	cenová
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
snížila	snížit	k5eAaPmAgFnS	snížit
o	o	k7c4	o
43	[number]	k4	43
%	%	kIx~	%
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
export	export	k1gInSc1	export
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
53	[number]	k4	53
%	%	kIx~	%
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
ze	z	k7c2	z
72	[number]	k4	72
na	na	k7c4	na
207	[number]	k4	207
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Deflační	deflační	k2eAgFnSc4d1	deflační
politiku	politika	k1gFnSc4	politika
sice	sice	k8xC	sice
parlament	parlament	k1gInSc4	parlament
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
Národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
však	však	k9	však
dále	daleko	k6eAd2	daleko
trvalo	trvat	k5eAaImAgNnS	trvat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
za	za	k7c2	za
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
cena	cena	k1gFnSc1	cena
zlata	zlato	k1gNnSc2	zlato
prudce	prudko	k6eAd1	prudko
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
kurz	kurz	k1gInSc1	kurz
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1930	[number]	k4	1930
a	a	k8xC	a
1933	[number]	k4	1933
se	se	k3xPyFc4	se
domácí	domácí	k2eAgFnSc1d1	domácí
cenová	cenový	k2eAgFnSc1d1	cenová
hladina	hladina	k1gFnSc1	hladina
sice	sice	k8xC	sice
snížila	snížit	k5eAaPmAgFnS	snížit
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
19	[number]	k4	19
%	%	kIx~	%
<g/>
,	,	kIx,	,
export	export	k1gInSc1	export
však	však	k9	však
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
64	[number]	k4	64
%	%	kIx~	%
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
ze	z	k7c2	z
105	[number]	k4	105
na	na	k7c4	na
736	[number]	k4	736
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
mohl	moct	k5eAaImAgInS	moct
Engliš	Engliš	k1gInSc1	Engliš
prosadit	prosadit	k5eAaPmF	prosadit
devalvaci	devalvace	k1gFnSc4	devalvace
koruny	koruna	k1gFnSc2	koruna
o	o	k7c4	o
16	[number]	k4	16
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
málo	málo	k1gNnSc1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
inflace	inflace	k1gFnSc2	inflace
k	k	k7c3	k
deflaci	deflace	k1gFnSc3	deflace
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
jejímu	její	k3xOp3gInSc3	její
vzniku	vznik	k1gInSc3	vznik
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
obvykle	obvykle	k6eAd1	obvykle
snaží	snažit	k5eAaImIp3nS	snažit
preventivně	preventivně	k6eAd1	preventivně
bojovat	bojovat	k5eAaImF	bojovat
<g/>
;	;	kIx,	;
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
krátkých	krátký	k2eAgNnPc6d1	krátké
časových	časový	k2eAgNnPc6d1	časové
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
deflace	deflace	k1gFnSc1	deflace
nemusí	muset	k5eNaImIp3nS	muset
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
působit	působit	k5eAaImF	působit
nepříznivě	příznivě	k6eNd1	příznivě
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
deflace	deflace	k1gFnSc2	deflace
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
za	za	k7c2	za
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
služby	služba	k1gFnSc2	služba
platí	platit	k5eAaImIp3nS	platit
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
výrobce	výrobce	k1gMnPc4	výrobce
a	a	k8xC	a
prodejce	prodejce	k1gMnSc1	prodejce
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nevýhodnou	výhodný	k2eNgFnSc4d1	nevýhodná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
inkasují	inkasovat	k5eAaBmIp3nP	inkasovat
za	za	k7c4	za
poskytované	poskytovaný	k2eAgInPc4d1	poskytovaný
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zdraží	zdražit	k5eAaPmIp3nS	zdražit
se	se	k3xPyFc4	se
dovážené	dovážený	k2eAgNnSc1d1	dovážené
zboží	zboží	k1gNnSc1	zboží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvážené	vyvážený	k2eAgInPc4d1	vyvážený
výrobky	výrobek	k1gInPc4	výrobek
lze	lze	k6eAd1	lze
nabízet	nabízet	k5eAaImF	nabízet
za	za	k7c4	za
nižší	nízký	k2eAgFnPc4d2	nižší
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
ekonomice	ekonomika	k1gFnSc6	ekonomika
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
oživení	oživení	k1gNnSc4	oživení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
deflace	deflace	k1gFnSc1	deflace
trvá	trvat	k5eAaImIp3nS	trvat
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
rozvinout	rozvinout	k5eAaPmF	rozvinout
až	až	k9	až
v	v	k7c4	v
deflační	deflační	k2eAgFnSc4d1	deflační
spirálu	spirála	k1gFnSc4	spirála
či	či	k8xC	či
deflační	deflační	k2eAgFnSc4d1	deflační
past	past	k1gFnSc4	past
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
pokles	pokles	k1gInSc1	pokles
cen	cena	k1gFnPc2	cena
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
tržeb	tržba	k1gFnPc2	tržba
a	a	k8xC	a
případně	případně	k6eAd1	případně
zisků	zisk	k1gInPc2	zisk
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
sféry	sféra	k1gFnSc2	sféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
musí	muset	k5eAaImIp3nS	muset
snižovat	snižovat	k5eAaImF	snižovat
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
snížení	snížení	k1gNnSc1	snížení
nákladů	náklad	k1gInPc2	náklad
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
propouštění	propouštění	k1gNnSc1	propouštění
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
mzdy	mzda	k1gFnSc2	mzda
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
faktory	faktor	k1gInPc1	faktor
–	–	k?	–
růst	růst	k1gInSc4	růst
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
i	i	k8xC	i
pokles	pokles	k1gInSc1	pokles
mzdy	mzda	k1gFnSc2	mzda
pak	pak	k6eAd1	pak
sníží	snížit	k5eAaPmIp3nS	snížit
poptávku	poptávka	k1gFnSc4	poptávka
domácnosti	domácnost	k1gFnSc2	domácnost
po	po	k7c6	po
zboží	zboží	k1gNnSc6	zboží
a	a	k8xC	a
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
tlačí	tlačit	k5eAaImIp3nS	tlačit
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
opět	opět	k6eAd1	opět
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
cen	cena	k1gFnPc2	cena
atd.	atd.	kA	atd.
ve	v	k7c6	v
spirále	spirála	k1gFnSc6	spirála
ve	v	k7c6	v
stále	stále	k6eAd1	stále
rychlejším	rychlý	k2eAgNnSc6d2	rychlejší
tempu	tempo	k1gNnSc6	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Deflační	deflační	k2eAgFnSc1d1	deflační
past	past	k1gFnSc1	past
v	v	k7c6	v
reálné	reálný	k2eAgFnSc6d1	reálná
ekonomice	ekonomika	k1gFnSc6	ekonomika
navyšuje	navyšovat	k5eAaImIp3nS	navyšovat
nejistotu	nejistota	k1gFnSc4	nejistota
při	při	k7c6	při
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
rozhodování	rozhodování	k1gNnSc6	rozhodování
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
alokaci	alokace	k1gFnSc4	alokace
dalšího	další	k2eAgInSc2d1	další
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
investice	investice	k1gFnSc1	investice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
distribučních	distribuční	k2eAgInPc2d1	distribuční
a	a	k8xC	a
logistických	logistický	k2eAgInPc2d1	logistický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
potenciál	potenciál	k1gInSc1	potenciál
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
využívaná	využívaný	k2eAgFnSc1d1	využívaná
část	část	k1gFnSc1	část
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
výrobních	výrobní	k2eAgFnPc2d1	výrobní
kapacit	kapacita	k1gFnPc2	kapacita
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nadbytečnými	nadbytečný	k2eAgInPc7d1	nadbytečný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
peněžní	peněžní	k2eAgFnSc6d1	peněžní
ekonomice	ekonomika	k1gFnSc6	ekonomika
pak	pak	k6eAd1	pak
klesají	klesat	k5eAaImIp3nP	klesat
ceny	cena	k1gFnPc1	cena
aktiv	aktivum	k1gNnPc2	aktivum
s	s	k7c7	s
prudkým	prudký	k2eAgInSc7d1	prudký
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
akciové	akciový	k2eAgInPc4d1	akciový
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
peněžní	peněžní	k2eAgFnSc1d1	peněžní
zásoba	zásoba	k1gFnSc1	zásoba
a	a	k8xC	a
úvěrová	úvěrový	k2eAgFnSc1d1	úvěrová
emise	emise	k1gFnSc1	emise
bank	banka	k1gFnPc2	banka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
škola	škola	k1gFnSc1	škola
na	na	k7c6	na
deflaci	deflace	k1gFnSc6	deflace
hledí	hledět	k5eAaImIp3nS	hledět
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Deflace	deflace	k1gFnSc1	deflace
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
nemůže	moct	k5eNaImIp3nS	moct
mít	mít	k5eAaImF	mít
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
negativní	negativní	k2eAgInPc4d1	negativní
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ještě	ještě	k9	ještě
před	před	k7c7	před
počátkem	počátkem	k7c2	počátkem
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přesto	přesto	k8xC	přesto
tehdy	tehdy	k6eAd1	tehdy
západní	západní	k2eAgFnPc1d1	západní
země	zem	k1gFnPc1	zem
zažívaly	zažívat	k5eAaImAgFnP	zažívat
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
odlišných	odlišný	k2eAgInPc6d1	odlišný
měnových	měnový	k2eAgInPc6d1	měnový
standardech	standard	k1gInPc6	standard
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
komoditní	komoditní	k2eAgInPc1d1	komoditní
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zlatý	zlatý	k2eAgInSc4d1	zlatý
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
prakticky	prakticky	k6eAd1	prakticky
inflační	inflační	k2eAgFnSc4d1	inflační
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jediným	jediný	k2eAgInSc7d1	jediný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
množství	množství	k1gNnSc4	množství
peněz	peníze	k1gInPc2	peníze
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
náročná	náročný	k2eAgFnSc1d1	náročná
těžba	těžba	k1gFnSc1	těžba
měnového	měnový	k2eAgInSc2d1	měnový
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
však	však	k9	však
byla	být	k5eAaImAgFnS	být
přetržena	přetržen	k2eAgFnSc1d1	přetržen
vazba	vazba	k1gFnSc1	vazba
mezi	mezi	k7c7	mezi
komoditou	komodita	k1gFnSc7	komodita
a	a	k8xC	a
penězi	peníze	k1gInPc7	peníze
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pouhý	pouhý	k2eAgInSc1d1	pouhý
papír	papír	k1gInSc1	papír
stal	stát	k5eAaPmAgInS	stát
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
už	už	k6eAd1	už
pro	pro	k7c4	pro
monetární	monetární	k2eAgFnSc4d1	monetární
expanzi	expanze	k1gFnSc4	expanze
žádná	žádný	k3yNgNnPc1	žádný
fyzická	fyzický	k2eAgNnPc1d1	fyzické
omezení	omezení	k1gNnPc1	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyrobit	vyrobit	k5eAaPmF	vyrobit
přesně	přesně	k6eAd1	přesně
tolik	tolik	k4xDc4	tolik
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
chce	chtít	k5eAaImIp3nS	chtít
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
keynesiánci	keynesiánec	k1gMnPc1	keynesiánec
monetární	monetární	k2eAgFnSc2d1	monetární
expanzi	expanze	k1gFnSc3	expanze
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Phillipsova	Phillipsův	k2eAgFnSc1d1	Phillipsova
křivka	křivka	k1gFnSc1	křivka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
monetaristé	monetarista	k1gMnPc1	monetarista
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
mnohem	mnohem	k6eAd1	mnohem
opatrnější	opatrný	k2eAgMnPc1d2	opatrnější
<g/>
,	,	kIx,	,
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c6	o
nízké	nízký	k2eAgFnSc6d1	nízká
a	a	k8xC	a
stabilní	stabilní	k2eAgFnSc6d1	stabilní
inflaci	inflace	k1gFnSc6	inflace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
inflace	inflace	k1gFnSc1	inflace
pouhým	pouhý	k2eAgNnSc7d1	pouhé
přerozdělováním	přerozdělování	k1gNnSc7	přerozdělování
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vždy	vždy	k6eAd1	vždy
negativní	negativní	k2eAgInSc4d1	negativní
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Kritickou	kritický	k2eAgFnSc7d1	kritická
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
škol	škola	k1gFnPc2	škola
řeší	řešit	k5eAaImIp3nS	řešit
odlišně	odlišně	k6eAd1	odlišně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
připouštěl	připouštět	k5eAaImAgMnS	připouštět
monetární	monetární	k2eAgFnSc3d1	monetární
expanzi	expanze	k1gFnSc3	expanze
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
agregátní	agregátní	k2eAgFnSc2d1	agregátní
poptávky	poptávka	k1gFnSc2	poptávka
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
preferoval	preferovat	k5eAaImAgInS	preferovat
expanzi	expanze	k1gFnSc3	expanze
fiskální	fiskální	k2eAgFnSc2d1	fiskální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zastánce	zastánce	k1gMnSc1	zastánce
pobídek	pobídka	k1gFnPc2	pobídka
<g/>
)	)	kIx)	)
Milton	Milton	k1gInSc1	Milton
Friedman	Friedman	k1gMnSc1	Friedman
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
klonil	klonit	k5eAaImAgInS	klonit
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
měnovou	měnový	k2eAgFnSc4d1	měnová
zásobu	zásoba	k1gFnSc4	zásoba
lze	lze	k6eAd1	lze
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
nanejvýše	nanejvýše	k6eAd1	nanejvýše
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
roste	růst	k5eAaImIp3nS	růst
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
<g/>
:	:	kIx,	:
Tím	ten	k3xDgNnSc7	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zaručena	zaručit	k5eAaPmNgFnS	zaručit
stabilita	stabilita	k1gFnSc1	stabilita
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
udržování	udržování	k1gNnSc1	udržování
konstantních	konstantní	k2eAgFnPc2d1	konstantní
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
intervencí	intervence	k1gFnPc2	intervence
<g/>
)	)	kIx)	)
A	a	k9	a
konečně	konečně	k6eAd1	konečně
Ludwig	Ludwiga	k1gFnPc2	Ludwiga
von	von	k1gInSc1	von
Mises	Mises	k1gMnSc1	Mises
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
dobré	dobrý	k2eAgFnPc4d1	dobrá
jako	jako	k8xS	jako
libovolné	libovolný	k2eAgFnPc4d1	libovolná
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nP	měnit
(	(	kIx(	(
<g/>
tržní	tržní	k2eAgFnPc1d1	tržní
ceny	cena	k1gFnPc1	cena
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
s	s	k7c7	s
efektivitou	efektivita	k1gFnSc7	efektivita
výroby	výroba	k1gFnSc2	výroba
i	i	k8xC	i
klesat	klesat	k5eAaImF	klesat
<g/>
)	)	kIx)	)
Různé	různý	k2eAgFnPc1d1	různá
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
školy	škola	k1gFnPc1	škola
tudíž	tudíž	k8xC	tudíž
dávají	dávat	k5eAaImIp3nP	dávat
na	na	k7c4	na
tutéž	týž	k3xTgFnSc4	týž
otázku	otázka	k1gFnSc4	otázka
odlišné	odlišný	k2eAgFnSc2d1	odlišná
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jediné	jediné	k1gNnSc4	jediné
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
dosud	dosud	k6eAd1	dosud
celá	celý	k2eAgFnSc1d1	celá
profese	profese	k1gFnSc1	profese
nemůže	moct	k5eNaImIp3nS	moct
shodnout	shodnout	k5eAaBmF	shodnout
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
vyspělého	vyspělý	k2eAgInSc2d1	vyspělý
světa	svět	k1gInSc2	svět
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
udržovat	udržovat	k5eAaImF	udržovat
nízkou	nízký	k2eAgFnSc4d1	nízká
stabilní	stabilní	k2eAgFnSc4d1	stabilní
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
provádí	provádět	k5eAaImIp3nS	provádět
inflační	inflační	k2eAgNnSc4d1	inflační
cílování	cílování	k1gNnSc4	cílování
<g/>
.	.	kIx.	.
</s>
