<p>
<s>
Furiant	furiant	k1gInSc1	furiant
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
furians	furians	k1gInSc1	furians
–	–	k?	–
rozzuřený	rozzuřený	k2eAgInSc1d1	rozzuřený
<g/>
,	,	kIx,	,
rozrušený	rozrušený	k2eAgInSc1d1	rozrušený
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
český	český	k2eAgInSc1d1	český
tanec	tanec	k1gInSc1	tanec
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
dvoučtvrťovém	dvoučtvrťový	k2eAgInSc6d1	dvoučtvrťový
a	a	k8xC	a
tříčtvrťovém	tříčtvrťový	k2eAgInSc6d1	tříčtvrťový
taktu	takt	k1gInSc6	takt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
Daniel	Daniel	k1gMnSc1	Daniel
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Türk	Türk	k1gMnSc1	Türk
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Klavírní	klavírní	k2eAgFnSc6d1	klavírní
škole	škola	k1gFnSc6	škola
(	(	kIx(	(
<g/>
Clavierschule	Clavierschula	k1gFnSc6	Clavierschula
oder	odra	k1gFnPc2	odra
Anweisung	Anweisung	k1gInSc1	Anweisung
zum	zum	k?	zum
Clavierspielen	Clavierspielen	k2eAgInSc1d1	Clavierspielen
für	für	k?	für
Lehrer	Lehrer	k1gInSc1	Lehrer
und	und	k?	und
Lernende	Lernend	k1gInSc5	Lernend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stylizované	stylizovaný	k2eAgFnSc6d1	stylizovaná
podobě	podoba	k1gFnSc6	podoba
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
českými	český	k2eAgMnPc7d1	český
skladateli	skladatel	k1gMnPc7	skladatel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
skladeb	skladba	k1gFnPc2	skladba
Slovanské	slovanský	k2eAgInPc1d1	slovanský
tance	tanec	k1gInPc1	tanec
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
46	[number]	k4	46
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Symfonii	symfonie	k1gFnSc6	symfonie
č.	č.	k?	č.
6	[number]	k4	6
D	D	kA	D
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
České	český	k2eAgMnPc4d1	český
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
Enjott	Enjott	k1gMnSc1	Enjott
Schneider	Schneider	k1gMnSc1	Schneider
opus	opus	k1gInSc1	opus
"	"	kIx"	"
<g/>
Furiant	furiant	k1gInSc1	furiant
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
orchestr	orchestr	k1gInSc4	orchestr
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Výrazu	výraz	k1gInSc3	výraz
furiant	furiant	k1gInSc1	furiant
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
dávajícího	dávající	k2eAgNnSc2d1	dávající
přemrštěně	přemrštěně	k6eAd1	přemrštěně
najevo	najevo	k6eAd1	najevo
své	svůj	k3xOyFgNnSc4	svůj
sebevědomí	sebevědomí	k1gNnSc4	sebevědomí
a	a	k8xC	a
svoje	svůj	k3xOyFgFnPc4	svůj
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
komunitě	komunita	k1gFnSc6	komunita
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
první	první	k4xOgFnSc2	první
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
všem	všecek	k3xTgInSc6	všecek
pravdu	pravda	k1gFnSc4	pravda
(	(	kIx(	(
<g/>
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
ji	on	k3xPp3gFnSc4	on
neměl	mít	k5eNaImAgMnS	mít
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
drama	drama	k1gNnSc1	drama
Naši	náš	k3xOp1gMnPc1	náš
furianti	furiant	k1gMnPc1	furiant
Ladislava	Ladislav	k1gMnSc4	Ladislav
Stroupežnického	Stroupežnický	k2eAgMnSc4d1	Stroupežnický
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
života	život	k1gInSc2	život
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
vesnici	vesnice	k1gFnSc6	vesnice
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dějstvích	dějství	k1gNnPc6	dějství
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vejtaha	vejtaha	k?	vejtaha
<g/>
,	,	kIx,	,
náfuka	náfuka	k1gMnSc1	náfuka
<g/>
,	,	kIx,	,
haur	haur	k1gMnSc1	haur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Furiant	furiant	k1gInSc4	furiant
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
veselá	veselý	k2eAgFnSc1d1	veselá
<g/>
,	,	kIx,	,
žertovná	žertovný	k2eAgFnSc1d1	žertovná
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
a	a	k8xC	a
štědrá	štědrý	k2eAgFnSc1d1	štědrá
<g/>
.	.	kIx.	.
</s>
<s>
Furiant	furiant	k1gMnSc1	furiant
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
předhání	předhánět	k5eAaImIp3nS	předhánět
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
získá	získat	k5eAaPmIp3nS	získat
více	hodně	k6eAd2	hodně
společenského	společenský	k2eAgInSc2d1	společenský
obdivu	obdiv	k1gInSc2	obdiv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Daneš	Daneš	k1gMnSc1	Daneš
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Machač	Machač	k1gMnSc1	Machač
<g/>
,	,	kIx,	,
J.	J.	kA	J.
.	.	kIx.	.
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
:	:	kIx,	:
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Furiant	furiant	k1gInSc4	furiant
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Furiant	furiant	k1gInSc4	furiant
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
furiant	furiant	k1gInSc1	furiant
ve	v	k7c6	v
WikislovníkuVRKOČOVÁ	WikislovníkuVRKOČOVÁ	k1gFnSc6	WikislovníkuVRKOČOVÁ
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Slovníček	slovníček	k1gInSc4	slovníček
základních	základní	k2eAgInPc2d1	základní
hudebních	hudební	k2eAgInPc2d1	hudební
pojmů	pojem	k1gInPc2	pojem
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
nákl	nákl	k1gMnSc1	nákl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901611	[number]	k4	901611
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
