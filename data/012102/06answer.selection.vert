<s>
Výrazu	výraz	k1gInSc3	výraz
furiant	furiant	k1gInSc1	furiant
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
dávajícího	dávající	k2eAgNnSc2d1	dávající
přemrštěně	přemrštěně	k6eAd1	přemrštěně
najevo	najevo	k6eAd1	najevo
své	svůj	k3xOyFgNnSc4	svůj
sebevědomí	sebevědomí	k1gNnSc4	sebevědomí
a	a	k8xC	a
svoje	svůj	k3xOyFgFnPc4	svůj
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
komunitě	komunita	k1gFnSc6	komunita
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
první	první	k4xOgFnSc2	první
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
všem	všecek	k3xTgInSc6	všecek
pravdu	pravda	k1gFnSc4	pravda
(	(	kIx(	(
<g/>
i	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
ji	on	k3xPp3gFnSc4	on
neměl	mít	k5eNaImAgMnS	mít
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
drama	drama	k1gNnSc1	drama
Naši	náš	k3xOp1gMnPc1	náš
furianti	furiant	k1gMnPc1	furiant
Ladislava	Ladislav	k1gMnSc4	Ladislav
Stroupežnického	Stroupežnický	k2eAgMnSc4d1	Stroupežnický
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
života	život	k1gInSc2	život
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
vesnici	vesnice	k1gFnSc6	vesnice
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
dějstvích	dějství	k1gNnPc6	dějství
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vejtaha	vejtaha	k?	vejtaha
<g/>
,	,	kIx,	,
náfuka	náfuka	k1gMnSc1	náfuka
<g/>
,	,	kIx,	,
haur	haur	k1gMnSc1	haur
<g/>
.	.	kIx.	.
</s>
