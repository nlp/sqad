<p>
<s>
Freska	freska	k1gFnSc1	freska
je	být	k5eAaImIp3nS	být
nástěnná	nástěnný	k2eAgFnSc1d1	nástěnná
malba	malba	k1gFnSc1	malba
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
na	na	k7c6	na
vlhké	vlhký	k2eAgFnSc6d1	vlhká
omítce	omítka	k1gFnSc6	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
freska	fresko	k1gNnSc2	fresko
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
al	ala	k1gFnPc2	ala
fresco	fresco	k1gMnSc1	fresco
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
na	na	k7c4	na
čerstvo	čerstvo	k1gNnSc4	čerstvo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
al	ala	k1gFnPc2	ala
secco	secco	k6eAd1	secco
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
na	na	k7c4	na
sucho	sucho	k1gNnSc4	sucho
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Technika	technika	k1gFnSc1	technika
malování	malování	k1gNnSc2	malování
fresek	freska	k1gFnPc2	freska
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Raně	raně	k6eAd1	raně
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
v	v	k7c6	v
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
dalo	dát	k5eAaPmAgNnS	dát
přednost	přednost	k1gFnSc4	přednost
mozaikám	mozaika	k1gFnPc3	mozaika
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
umění	umění	k1gNnSc1	umění
fresky	freska	k1gFnSc2	freska
upadlo	upadnout	k5eAaPmAgNnS	upadnout
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc6	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
koncem	koncem	k7c2	koncem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malování	malování	k1gNnSc1	malování
fresek	freska	k1gFnPc2	freska
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
náročná	náročný	k2eAgFnSc1d1	náročná
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nános	nános	k1gInSc1	nános
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
rozmělněné	rozmělněný	k2eAgInPc1d1	rozmělněný
minerální	minerální	k2eAgInPc1d1	minerální
pigmenty	pigment	k1gInPc1	pigment
smíchané	smíchaný	k2eAgInPc1d1	smíchaný
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nanášejí	nanášet	k5eAaImIp3nP	nanášet
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
vlhké	vlhký	k2eAgFnSc2d1	vlhká
omítky	omítka	k1gFnSc2	omítka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
barvy	barva	k1gFnPc1	barva
vsákly	vsáknout	k5eAaPmAgFnP	vsáknout
do	do	k7c2	do
podkladu	podklad	k1gInSc2	podklad
a	a	k8xC	a
usychaly	usychat	k5eAaImAgInP	usychat
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
malíř	malíř	k1gMnSc1	malíř
musí	muset	k5eAaImIp3nS	muset
pracovat	pracovat	k5eAaImF	pracovat
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
omítka	omítka	k1gFnSc1	omítka
nezaschla	zaschnout	k5eNaPmAgFnS	zaschnout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
poté	poté	k6eAd1	poté
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnSc2d1	malá
opravy	oprava	k1gFnSc2	oprava
temperou	tempera	k1gFnSc7	tempera
<g/>
.	.	kIx.	.
</s>
<s>
Fresky	freska	k1gFnPc1	freska
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nP	hodit
do	do	k7c2	do
vlhkého	vlhký	k2eAgNnSc2d1	vlhké
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jediná	jediný	k2eAgFnSc1d1	jediná
freska	freska	k1gFnSc1	freska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postup	postup	k1gInSc4	postup
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
fresce	freska	k1gFnSc6	freska
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
hrubou	hrubý	k2eAgFnSc4d1	hrubá
omítku	omítka	k1gFnSc4	omítka
(	(	kIx(	(
<g/>
arriccio	arriccio	k6eAd1	arriccio
–	–	k?	–
vápno	vápno	k1gNnSc4	vápno
s	s	k7c7	s
pískem	písek	k1gInSc7	písek
<g/>
)	)	kIx)	)
se	s	k7c7	s
vlhkým	vlhký	k2eAgInSc7d1	vlhký
křídovým	křídový	k2eAgInSc7d1	křídový
prachem	prach	k1gInSc7	prach
nanesly	nanést	k5eAaPmAgFnP	nanést
pomocné	pomocný	k2eAgFnPc1d1	pomocná
linky	linka	k1gFnPc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
umělec	umělec	k1gMnSc1	umělec
provedl	provést	k5eAaPmAgMnS	provést
hnědočervenou	hnědočervený	k2eAgFnSc7d1	hnědočervená
barvou	barva	k1gFnSc7	barva
přípravnou	přípravný	k2eAgFnSc4d1	přípravná
kresbu	kresba	k1gFnSc4	kresba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
sinopie	sinopie	k1gFnSc1	sinopie
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
barviva	barvivo	k1gNnSc2	barvivo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pocházelo	pocházet	k5eAaImAgNnS	pocházet
ze	z	k7c2	z
Sinope	Sinop	k1gInSc5	Sinop
v	v	k7c6	v
Černomoří	Černomoří	k1gNnSc6	Černomoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
díky	díky	k7c3	díky
novým	nový	k2eAgFnPc3d1	nová
technikám	technika	k1gFnPc3	technika
pro	pro	k7c4	pro
přemístění	přemístění	k1gNnSc4	přemístění
fresek	freska	k1gFnPc2	freska
některé	některý	k3yIgFnSc2	některý
sinopie	sinopie	k1gFnSc2	sinopie
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
výjev	výjev	k1gInSc1	výjev
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
úseky	úsek	k1gInPc4	úsek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zpracovat	zpracovat	k5eAaPmF	zpracovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
giornata	giornata	k1gFnSc1	giornata
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
rozloha	rozloha	k1gFnSc1	rozloha
záležela	záležet	k5eAaImAgFnS	záležet
na	na	k7c6	na
povaze	povaha	k1gFnSc6	povaha
výjevu	výjev	k1gInSc2	výjev
–	–	k?	–
šlo	jít	k5eAaImAgNnS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
giornata	giornata	k1gFnSc1	giornata
pochopitelně	pochopitelně	k6eAd1	pochopitelně
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
denní	denní	k2eAgInPc1d1	denní
úseky	úsek	k1gInPc1	úsek
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
nahozeny	nahodit	k5eAaPmNgInP	nahodit
druhou	druhý	k4xOgFnSc7	druhý
vrstvou	vrstva	k1gFnSc7	vrstva
jemné	jemný	k2eAgFnPc1d1	jemná
omítky	omítka	k1gFnPc1	omítka
(	(	kIx(	(
<g/>
intonaco	intonaco	k6eAd1	intonaco
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgNnSc4d1	každodenní
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
fresky	freska	k1gFnPc4	freska
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
díla	dílo	k1gNnPc4	dílo
Giottova	Giottův	k2eAgNnPc4d1	Giottovo
<g/>
,	,	kIx,	,
Michelangellova	Michelangellův	k2eAgMnSc2d1	Michelangellův
nebo	nebo	k8xC	nebo
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Secco	secco	k6eAd1	secco
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Corrainová	Corrainový	k2eAgFnSc1d1	Corrainový
Lucia	Lucia	k1gFnSc1	Lucia
<g/>
,	,	kIx,	,
Giotto	Giott	k2eAgNnSc4d1	Giotto
a	a	k8xC	a
středověké	středověký	k2eAgNnSc4d1	středověké
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Životy	život	k1gInPc1	život
a	a	k8xC	a
díla	dílo	k1gNnPc1	dílo
středověkých	středověký	k2eAgMnPc2d1	středověký
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
Svojtka	Svojtka	k1gMnSc1	Svojtka
a	a	k8xC	a
Vašut	Vašut	k1gMnSc1	Vašut
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
freska	freska	k1gFnSc1	freska
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
freska	fresko	k1gNnSc2	fresko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Art	Art	k1gFnSc1	Art
and	and	k?	and
Nature	Natur	k1gMnSc5	Natur
of	of	k?	of
Fresco	Fresco	k6eAd1	Fresco
by	by	kYmCp3nS	by
Lucia	Lucia	k1gFnSc1	Lucia
Wiley	Wilea	k1gFnSc2	Wilea
</s>
</p>
