<s>
Létající	létající	k2eAgMnSc1d1	létající
Čestmír	Čestmír	k1gMnSc1	Čestmír
je	být	k5eAaImIp3nS	být
šestidílný	šestidílný	k2eAgInSc4d1	šestidílný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
i	i	k9	i
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
fliegende	fliegend	k1gMnSc5	fliegend
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
Latający	Latający	k1gInPc4	Latający
Czestmir	Czestmir	k1gInSc1	Czestmir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
žák	žák	k1gMnSc1	žák
Čestmír	Čestmír	k1gMnSc1	Čestmír
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
prapodivnému	prapodivný	k2eAgMnSc3d1	prapodivný
a	a	k8xC	a
zázračnému	zázračný	k2eAgInSc3d1	zázračný
meteoritu	meteorit	k1gInSc3	meteorit
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
si	se	k3xPyFc3	se
přinese	přinést	k5eAaPmIp3nS	přinést
semínka	semínko	k1gNnPc4	semínko
dvou	dva	k4xCgFnPc6	dva
kouzelných	kouzelný	k2eAgFnPc2d1	kouzelná
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jedné	jeden	k4xCgFnSc3	jeden
může	moct	k5eAaImIp3nS	moct
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
druhé	druhý	k4xOgNnSc1	druhý
vypadá	vypadat	k5eAaImIp3nS	vypadat
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
jako	jako	k9	jako
dospělý	dospělý	k1gMnSc1	dospělý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Exteriéry	exteriér	k1gInPc1	exteriér
seriálu	seriál	k1gInSc2	seriál
byly	být	k5eAaImAgInP	být
natočeny	natočit	k5eAaBmNgInP	natočit
v	v	k7c6	v
Mělníku	Mělník	k1gInSc6	Mělník
a	a	k8xC	a
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Štiřín	Štiřín	k1gInSc1	Štiřín
<g/>
.	.	kIx.	.
</s>
<s>
Modrý	modrý	k2eAgInSc4d1	modrý
kámen	kámen	k1gInSc4	kámen
Šest	šest	k4xCc4	šest
květináčů	květináč	k1gInPc2	květináč
Geniální	geniální	k2eAgFnSc1d1	geniální
rodina	rodina	k1gFnSc1	rodina
Velký	velký	k2eAgInSc1d1	velký
vezír	vezír	k1gMnSc1	vezír
Rodina	rodina	k1gFnSc1	rodina
na	na	k7c6	na
větvi	větev	k1gFnSc6	větev
Poslední	poslední	k2eAgFnSc1d1	poslední
květina	květina	k1gFnSc1	květina
</s>
