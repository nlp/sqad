<s>
Létající	létající	k2eAgMnSc1d1	létající
Čestmír	Čestmír	k1gMnSc1	Čestmír
je	být	k5eAaImIp3nS	být
šestidílný	šestidílný	k2eAgInSc4d1	šestidílný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
.	.	kIx.	.
</s>
