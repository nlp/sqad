<s>
Muammar	Muammar	k1gInSc1	Muammar
Kaddáfí	Kaddáfí	k1gFnSc2	Kaddáfí
či	či	k8xC	či
al-Kaddáfi	al-Kaddáf	k1gFnSc2	al-Kaddáf
/	/	kIx~	/
<g/>
arabsky	arabsky	k6eAd1	arabsky
audio	audio	k2eAgFnSc4d1	audio
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Syrta	Syrt	k1gInSc2	Syrt
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
Syrta	Syrto	k1gNnSc2	Syrto
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
voják	voják	k1gMnSc1	voják
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
plukovníka	plukovník	k1gMnSc2	plukovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Vůdce	vůdce	k1gMnSc1	vůdce
Velké	velký	k2eAgFnSc2d1	velká
revoluce	revoluce	k1gFnSc2	revoluce
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
arabské	arabský	k2eAgFnSc2d1	arabská
lidové	lidový	k2eAgFnSc2d1	lidová
socialistické	socialistický	k2eAgFnSc2d1	socialistická
džamáhíríje	džamáhíríj	k1gFnSc2	džamáhíríj
<g/>
"	"	kIx"	"
vládcem	vládce	k1gMnSc7	vládce
Libye	Libye	k1gFnSc2	Libye
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
také	také	k9	také
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejdéle	dlouho	k6eAd3	dlouho
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
vůdců	vůdce	k1gMnPc2	vůdce
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
