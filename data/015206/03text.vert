<s>
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Turnov	Turnov	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
trenér	trenér	k1gMnSc1
a	a	k8xC
atlet	atlet	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
ČR	ČR	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MČR	MČR	kA
1997	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
1998	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
1999	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2000	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2001	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2002	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2003	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2004	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2005	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2007	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MČR	MČR	kA
2008	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
Halové	halový	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
ČR	ČR	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
HMČR	HMČR	kA
1998	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
HMČR	HMČR	kA
1999	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
HMČR	HMČR	kA
2001	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
HMČR	HMČR	kA
2002	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
HMČR	HMČR	kA
2005	#num#	k4
</s>
<s>
vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
</s>
<s>
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1977	#num#	k4
Turnov	Turnov	k1gInSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
atlet	atlet	k1gMnSc1
specializující	specializující	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c4
vrh	vrh	k1gInSc4
koulí	koule	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
závodník	závodník	k1gMnSc1
AC	AC	kA
Turnov	Turnov	k1gInSc1
však	však	k9
nastupoval	nastupovat	k5eAaImAgInS
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
vrhačských	vrhačský	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
<g/>
:	:	kIx,
hodu	hod	k1gInSc2
diskem	disk	k1gInSc7
<g/>
,	,	kIx,
oštěpem	oštěp	k1gInSc7
i	i	k8xC
kladivem	kladivo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aktivní	aktivní	k2eAgMnSc1d1
atlet	atlet	k1gMnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
reprezentoval	reprezentovat	k5eAaImAgInS
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Athénách	Athéna	k1gFnPc6
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
ve	v	k7c6
vrhu	vrh	k1gInSc6
koulí	koule	k1gFnPc2
a	a	k8xC
obsadil	obsadit	k5eAaPmAgMnS
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
atletickém	atletický	k2eAgInSc6d1
mítinku	mítink	k1gInSc6
kategorie	kategorie	k1gFnSc2
Super	super	k1gInSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
v	v	k7c6
Dauhá	Dauhý	k2eAgFnSc1d1
vybojoval	vybojovat	k5eAaPmAgMnS
výkonem	výkon	k1gInSc7
20,05	20,05	k4
metru	metr	k1gInSc2
šesté	šestý	k4xOgNnSc1
místo	místo	k1gNnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
tak	tak	k6eAd1
splnil	splnit	k5eAaPmAgInS
B	B	kA
limit	limit	k1gInSc1
pro	pro	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trenérská	trenérský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
začal	začít	k5eAaPmAgMnS
trénovat	trénovat	k5eAaImF
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
skupinu	skupina	k1gFnSc4
českých	český	k2eAgMnPc2d1
reprezentantů	reprezentant	k1gMnPc2
vrhačů	vrhač	k1gMnPc2
s	s	k7c7
dobrými	dobrý	k2eAgInPc7d1
výsledky	výsledek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skupině	skupina	k1gFnSc6
má	mít	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
Ladislava	Ladislav	k1gMnSc4
Prášila	Prášil	k1gMnSc4
<g/>
,	,	kIx,
Martina	Martin	k1gMnSc4
Staška	Stašek	k1gMnSc4
<g/>
,	,	kIx,
Tomáše	Tomáš	k1gMnSc4
Staňka	Staněk	k1gMnSc4
<g/>
,	,	kIx,
Martina	Martin	k1gMnSc4
Nováka	Novák	k1gMnSc4
a	a	k8xC
další	další	k2eAgMnPc4d1
atlety	atlet	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
na	na	k7c4
www.acturnov.cz	www.acturnov.cz	k1gMnSc1
<g/>
↑	↑	k?
Koulař	koulař	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
si	se	k3xPyFc3
přiveze	přivézt	k5eAaPmIp3nS
z	z	k7c2
Dauhá	Dauhý	k2eAgFnSc1d1
olympijský	olympijský	k2eAgInSc1d1
limit	limit	k1gInSc1
<g/>
↑	↑	k?
Michal	Michal	k1gMnSc1
Osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
Právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tiché	Tiché	k2eAgMnPc7d1
siláky	silák	k1gMnPc7
nakopnou	nakopnout	k5eAaPmIp3nP
i	i	k9
sázky	sázka	k1gFnPc4
s	s	k7c7
trenérem	trenér	k1gMnSc7
<g/>
.	.	kIx.
sport	sport	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-08-07	2013-08-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
IAAF	IAAF	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistři	mistr	k1gMnPc1
Československa	Československo	k1gNnSc2
a	a	k8xC
Česka	Česko	k1gNnSc2
ve	v	k7c6
vrhu	vrh	k1gInSc6
koulí	koule	k1gFnPc2
mužů	muž	k1gMnPc2
Československo	Československo	k1gNnSc1
</s>
<s>
1945	#num#	k4
Čestmír	Čestmír	k1gMnSc1
Kalina	Kalina	k1gMnSc1
•	•	k?
1946	#num#	k4
Čestmír	Čestmír	k1gMnSc1
Kalina	Kalina	k1gMnSc1
•	•	k?
1947	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Knotek	Knotek	k1gMnSc1
•	•	k?
1948	#num#	k4
Čestmír	Čestmír	k1gMnSc1
Kalina	Kalina	k1gMnSc1
•	•	k?
1949	#num#	k4
Čestmír	Čestmír	k1gMnSc1
Kalina	Kalina	k1gMnSc1
•	•	k?
1950	#num#	k4
Čestmír	Čestmír	k1gMnSc1
Kalina	Kalina	k1gMnSc1
•	•	k?
1951	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1952	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1953	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1954	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1955	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
1956	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1957	#num#	k4
Jiří	Jiří	k1gMnSc2
Skobla	Skobla	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1958	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1959	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Plíhal	Plíhal	k1gMnSc1
•	•	k?
1960	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1961	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1962	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1963	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
1964	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1965	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
1966	#num#	k4
Jiří	Jiří	k1gMnSc1
Skobla	Skobla	k1gMnSc1
•	•	k?
1967	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
1968	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
•	•	k?
1969	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
•	•	k?
1970	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jaroslav	Jaroslav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
1971	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1972	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1973	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1974	#num#	k4
Jaromír	Jaromír	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
1975	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1976	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1977	#num#	k4
Jaromír	Jaromír	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
1978	#num#	k4
Jaromír	Jaromír	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
1979	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1980	#num#	k4
Jaromír	Jaromír	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
1981	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1982	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1983	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1984	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1985	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1986	#num#	k4
Jozef	Jozef	k1gMnSc1
Kubeš	Kubeš	k1gMnSc1
•	•	k?
1987	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1988	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1989	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1990	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1991	#num#	k4
Karel	Karel	k1gMnSc1
Šula	Šula	k1gMnSc1
•	•	k?
1992	#num#	k4
Karel	Karel	k1gMnSc1
Šula	Šula	k1gMnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
1993	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Menc	Menc	k1gInSc1
•	•	k?
1994	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
1995	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
1996	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
1997	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
1998	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
1999	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2000	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
2001	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2002	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2003	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2004	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2005	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2006	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
2007	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2008	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2009	#num#	k4
Antonín	Antonín	k1gMnSc1
Žalský	Žalský	k1gMnSc1
•	•	k?
2010	#num#	k4
Antonín	Antonín	k1gMnSc1
Žalský	Žalský	k1gMnSc1
•	•	k?
2011	#num#	k4
Jan	Jan	k1gMnSc1
Marcell	Marcell	k1gMnSc1
•	•	k?
2012	#num#	k4
Ladislav	Ladislav	k1gMnSc1
Prášil	Prášil	k1gMnSc1
•	•	k?
2013	#num#	k4
Ladislav	Ladislav	k1gMnSc1
Prášil	Prášil	k1gMnSc1
•	•	k?
2014	#num#	k4
Jan	Jan	k1gMnSc1
Marcell	Marcell	k1gMnSc1
•	•	k?
2015	#num#	k4
Martin	Martin	k1gMnSc1
Novák	Novák	k1gMnSc1
•	•	k?
2016	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2017	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2018	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2019	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2020	#num#	k4
Martin	Martin	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
Haloví	halový	k2eAgMnPc1d1
mistři	mistr	k1gMnPc1
Československa	Československo	k1gNnSc2
a	a	k8xC
Česka	Česko	k1gNnSc2
ve	v	k7c6
vrhu	vrh	k1gInSc6
koulí	koule	k1gFnPc2
mužů	muž	k1gMnPc2
Československo	Československo	k1gNnSc1
</s>
<s>
1969	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
•	•	k?
1970	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
•	•	k?
1971	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1972	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
•	•	k?
1973	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1974	#num#	k4
Dušan	Dušan	k1gMnSc1
Hamar	Hamar	k1gMnSc1
•	•	k?
1975	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1976	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
•	•	k?
1977	#num#	k4
Dušan	Dušan	k1gMnSc1
Hamar	Hamar	k1gMnSc1
•	•	k?
1978	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1979	#num#	k4
Jaroslav	Jaroslav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
•	•	k?
1980	#num#	k4
Jaromír	Jaromír	k1gMnSc1
Vlk	Vlk	k1gMnSc1
•	•	k?
1981	#num#	k4
Dalibor	Dalibor	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
•	•	k?
1982	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1983	#num#	k4
Jakub	Jakub	k1gMnSc1
Lang	Lang	k1gMnSc1
•	•	k?
1984	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1985	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1986	#num#	k4
Richard	Richarda	k1gFnPc2
Navara	Navara	k1gFnSc1
•	•	k?
1987	#num#	k4
Richard	Richarda	k1gFnPc2
Navara	Navara	k1gFnSc1
•	•	k?
1988	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1989	#num#	k4
Karel	Karel	k1gMnSc1
Šula	Šula	k1gMnSc1
•	•	k?
1990	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
1991	#num#	k4
Jaroslav	Jaroslava	k1gFnPc2
Žitnanský	Žitnanský	k2eAgInSc1d1
•	•	k?
1992	#num#	k4
Karel	Karel	k1gMnSc1
Šula	Šula	k1gMnSc1
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1993	#num#	k4
Jan	Jan	k1gMnSc1
Bártl	Bártl	k1gMnSc1
•	•	k?
1994	#num#	k4
Jan	Jan	k1gMnSc1
Bártl	Bártl	k1gMnSc1
•	•	k?
1995	#num#	k4
Martin	Martin	k1gInSc1
Bílek	bílek	k1gInSc4
•	•	k?
1996	#num#	k4
Josef	Josef	k1gMnSc1
Rosůlek	Rosůlek	k1gMnSc1
•	•	k?
1997	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
1998	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
1999	#num#	k4
Josef	Josef	k1gMnSc1
Rosůlek	Rosůlek	k1gMnSc1
•	•	k?
2000	#num#	k4
Richard	Richarda	k1gFnPc2
Navara	Navara	k1gFnSc1
•	•	k?
2001	#num#	k4
Miroslav	Miroslava	k1gFnPc2
Menc	Menc	k1gInSc4
•	•	k?
2002	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
•	•	k?
2003	#num#	k4
Antonín	Antonín	k1gMnSc1
Žalský	Žalský	k1gMnSc1
•	•	k?
2004	#num#	k4
Antonín	Antonín	k1gMnSc1
Žalský	Žalský	k1gMnSc1
•	•	k?
2005	#num#	k4
Petr	Petr	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2006	#num#	k4
Antonín	Antonín	k1gMnSc1
Žalský	Žalský	k1gMnSc1
•	•	k?
2007	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
2008	#num#	k4
Jan	Jan	k1gMnSc1
Tylče	Tylč	k1gInSc2
•	•	k?
2009	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
2010	#num#	k4
Remigius	Remigius	k1gInSc1
Machura	Machura	k1gFnSc1
•	•	k?
2011	#num#	k4
Jan	Jan	k1gMnSc1
Marcell	Marcell	k1gMnSc1
•	•	k?
2012	#num#	k4
Jan	Jan	k1gMnSc1
Marcell	Marcell	k1gMnSc1
•	•	k?
2013	#num#	k4
Martin	Martin	k1gMnSc1
Stašek	Stašek	k1gMnSc1
•	•	k?
2014	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2015	#num#	k4
Jan	Jan	k1gMnSc1
Marcell	Marcell	k1gMnSc1
•	•	k?
2016	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2017	#num#	k4
Ladislav	Ladislav	k1gMnSc1
Prášil	Prášil	k1gMnSc1
•	•	k?
2018	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
•	•	k?
2019	#num#	k4
Ladislav	Ladislav	k1gMnSc1
Prášil	Prášil	k1gMnSc1
•	•	k?
2020	#num#	k4
Tomáš	Tomáš	k1gMnSc1
Staněk	Staněk	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
