<s>
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
formální	formální	k2eAgInSc1d1	formální
systém	systém	k1gInSc1	systém
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
filozofii	filozofie	k1gFnSc6	filozofie
<g/>
,	,	kIx,	,
lingvistice	lingvistika	k1gFnSc6	lingvistika
a	a	k8xC	a
informatice	informatika	k1gFnSc6	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
označení	označení	k1gNnSc4	označení
používá	používat	k5eAaImIp3nS	používat
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
méně	málo	k6eAd2	málo
přesný	přesný	k2eAgInSc4d1	přesný
termín	termín	k1gInSc4	termín
predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
<g/>
.	.	kIx.	.
</s>
<s>
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
výrokové	výrokový	k2eAgFnSc2d1	výroková
logiky	logika	k1gFnSc2	logika
zavedením	zavedení	k1gNnSc7	zavedení
kvantifikovaných	kvantifikovaný	k2eAgFnPc2d1	kvantifikovaná
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
určitém	určitý	k2eAgNnSc6d1	určité
tématu	téma	k1gNnSc6	téma
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
právě	právě	k6eAd1	právě
predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
společně	společně	k6eAd1	společně
se	s	k7c7	s
<g/>
:	:	kIx,	:
specifickou	specifický	k2eAgFnSc7d1	specifická
univerzální	univerzální	k2eAgFnSc7d1	univerzální
množinou	množina	k1gFnSc7	množina
(	(	kIx(	(
<g/>
též	též	k9	též
<g/>
.	.	kIx.	.
univerzem	univerzum	k1gNnSc7	univerzum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
jsou	být	k5eAaImIp3nP	být
brány	brána	k1gFnPc1	brána
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
konečně	konečně	k6eAd1	konečně
mnoha	mnoho	k4c7	mnoho
funkcemi	funkce	k1gFnPc7	funkce
a	a	k8xC	a
predikáty	predikát	k1gInPc4	predikát
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
množinou	množina	k1gFnSc7	množina
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
množinou	množina	k1gFnSc7	množina
rekurzivních	rekurzivní	k2eAgInPc2d1	rekurzivní
axiomů	axiom	k1gInPc2	axiom
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
teorie	teorie	k1gFnSc2	teorie
pokládány	pokládán	k2eAgFnPc4d1	pokládána
za	za	k7c4	za
platné	platný	k2eAgInPc4d1	platný
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
pojmem	pojem	k1gInSc7	pojem
teorie	teorie	k1gFnSc2	teorie
formálně	formálně	k6eAd1	formálně
rozumíme	rozumět	k5eAaImIp1nP	rozumět
množinu	množina	k1gFnSc4	množina
vět	věta	k1gFnPc2	věta
(	(	kIx(	(
<g/>
sentencí	sentence	k1gFnPc2	sentence
<g/>
)	)	kIx)	)
zapsaných	zapsaný	k2eAgInPc2d1	zapsaný
v	v	k7c6	v
predikátové	predikátový	k2eAgFnSc6d1	predikátová
logice	logika	k1gFnSc6	logika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
existují	existovat	k5eAaImIp3nP	existovat
logiky	logika	k1gFnPc1	logika
vyšších	vysoký	k2eAgInPc2d2	vyšší
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
logiky	logika	k1gFnPc1	logika
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
povolují	povolovat	k5eAaImIp3nP	povolovat
predikáty	predikát	k1gInPc1	predikát
uvnitř	uvnitř	k7c2	uvnitř
predikátů	predikát	k1gInPc2	predikát
<g/>
,	,	kIx,	,
kvantifikování	kvantifikování	k1gNnSc1	kvantifikování
predikátu	predikát	k1gInSc2	predikát
i	i	k8xC	i
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
predikátů	predikát	k1gInPc2	predikát
a	a	k8xC	a
funkcí	funkce	k1gFnPc2	funkce
zároveň	zároveň	k6eAd1	zároveň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
teorií	teorie	k1gFnPc2	teorie
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
jsou	být	k5eAaImIp3nP	být
predikáty	predikát	k1gInPc1	predikát
svázány	svázán	k2eAgInPc1d1	svázán
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
případě	případ	k1gInSc6	případ
logik	logika	k1gFnPc2	logika
vyšších	vysoký	k2eAgInPc2d2	vyšší
řádů	řád	k1gInPc2	řád
bývají	bývat	k5eAaImIp3nP	bývat
predikáty	predikát	k1gInPc1	predikát
interpretovány	interpretovat	k5eAaBmNgInP	interpretovat
jako	jako	k8xC	jako
množiny	množina	k1gFnPc1	množina
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
deduktivních	deduktivní	k2eAgInPc2d1	deduktivní
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
predikátovou	predikátový	k2eAgFnSc4d1	predikátová
logiku	logika	k1gFnSc4	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
korektní	korektní	k2eAgFnPc1d1	korektní
(	(	kIx(	(
<g/>
všechna	všechen	k3xTgNnPc1	všechen
dokazatelná	dokazatelný	k2eAgNnPc1d1	dokazatelné
tvrzení	tvrzení	k1gNnPc1	tvrzení
jsou	být	k5eAaImIp3nP	být
pravdivá	pravdivý	k2eAgNnPc1d1	pravdivé
<g/>
)	)	kIx)	)
a	a	k8xC	a
úplné	úplný	k2eAgNnSc1d1	úplné
(	(	kIx(	(
<g/>
všechna	všechen	k3xTgNnPc1	všechen
pravdivá	pravdivý	k2eAgNnPc1d1	pravdivé
tvrzení	tvrzení	k1gNnPc1	tvrzení
jsou	být	k5eAaImIp3nP	být
dokazatelná	dokazatelný	k2eAgNnPc1d1	dokazatelné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokrok	pokrok	k1gInSc1	pokrok
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
automatických	automatický	k2eAgInPc2d1	automatický
dokazovačů	dokazovač	k1gInPc2	dokazovač
postavených	postavený	k2eAgInPc2d1	postavený
právě	právě	k6eAd1	právě
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
logice	logika	k1gFnSc6	logika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
její	její	k3xOp3gFnSc4	její
semi-rozhodnutelnost	semiozhodnutelnost	k1gFnSc4	semi-rozhodnutelnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dokazovačů	dokazovač	k1gInPc2	dokazovač
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
splňuje	splňovat	k5eAaImIp3nS	splňovat
několik	několik	k4yIc1	několik
metalogických	metalogický	k2eAgFnPc2d1	metalogický
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Löwenheim-Skolemovu	Löwenheim-Skolemův	k2eAgFnSc4d1	Löwenheim-Skolemův
větu	věta	k1gFnSc4	věta
nebo	nebo	k8xC	nebo
větu	věta	k1gFnSc4	věta
o	o	k7c6	o
kompaktnosti	kompaktnost	k1gFnSc6	kompaktnost
<g/>
.	.	kIx.	.
</s>
<s>
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
nesmírně	smírně	k6eNd1	smírně
důležitá	důležitý	k2eAgFnSc1d1	důležitá
již	již	k9	již
pro	pro	k7c4	pro
samotné	samotný	k2eAgInPc4d1	samotný
základy	základ	k1gInPc4	základ
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc7d1	standardní
logikou	logika	k1gFnSc7	logika
pro	pro	k7c4	pro
axiomatické	axiomatický	k2eAgInPc4d1	axiomatický
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
běžných	běžný	k2eAgInPc2d1	běžný
axiomatických	axiomatický	k2eAgInPc2d1	axiomatický
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Peanova	Peanův	k2eAgFnSc1d1	Peanova
aritmetika	aritmetika	k1gFnSc1	aritmetika
a	a	k8xC	a
axiomatická	axiomatický	k2eAgFnSc1d1	axiomatická
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Zermel-Fraenkelovy	Zermel-Fraenkelův	k2eAgFnSc2d1	Zermel-Fraenkelův
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
formalizovat	formalizovat	k5eAaBmF	formalizovat
pomocí	pomocí	k7c2	pomocí
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
žádná	žádný	k3yNgFnSc1	žádný
teorie	teorie	k1gFnSc1	teorie
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
nemá	mít	k5eNaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
plně	plně	k6eAd1	plně
a	a	k8xC	a
kategoricky	kategoricky	k6eAd1	kategoricky
popsat	popsat	k5eAaPmF	popsat
struktury	struktura	k1gFnPc4	struktura
s	s	k7c7	s
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
doménou	doména	k1gFnSc7	doména
<g/>
,	,	kIx,	,
např.	např.	kA	např.
celá	celý	k2eAgNnPc4d1	celé
čísla	číslo	k1gNnPc4	číslo
nebo	nebo	k8xC	nebo
reálná	reálný	k2eAgNnPc4d1	reálné
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
logiky	logicus	k1gMnPc7	logicus
vyšších	vysoký	k2eAgInPc2d2	vyšší
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
výroková	výrokový	k2eAgFnSc1d1	výroková
logika	logika	k1gFnSc1	logika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
a	a	k8xC	a
deklarativními	deklarativní	k2eAgInPc7d1	deklarativní
výroky	výrok	k1gInPc7	výrok
<g/>
,	,	kIx,	,
predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
zavádí	zavádět	k5eAaImIp3nS	zavádět
jako	jako	k9	jako
nadstavbu	nadstavba	k1gFnSc4	nadstavba
predikáty	predikát	k1gInPc1	predikát
a	a	k8xC	a
kvantifikátory	kvantifikátor	k1gInPc1	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Predikát	predikát	k1gInSc1	predikát
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
funkci	funkce	k1gFnSc4	funkce
nabývající	nabývající	k2eAgFnSc2d1	nabývající
booleovské	booleovský	k2eAgFnSc2d1	booleovská
hodnoty	hodnota	k1gFnSc2	hodnota
(	(	kIx(	(
<g/>
pravda	pravda	k1gFnSc1	pravda
-	-	kIx~	-
nepravda	nepravda	k1gFnSc1	nepravda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
následující	následující	k2eAgMnSc1d1	následující
věty	věta	k1gFnPc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sókratés	Sókratés	k1gInSc4	Sókratés
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Platón	Platón	k1gMnSc1	Platón
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
výrokové	výrokový	k2eAgFnSc6d1	výroková
logice	logika	k1gFnSc6	logika
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
čistě	čistě	k6eAd1	čistě
nezávislé	závislý	k2eNgInPc4d1	nezávislý
výroky	výrok	k1gInPc4	výrok
<g/>
,	,	kIx,	,
označeny	označit	k5eAaPmNgInP	označit
např.	např.	kA	např.
p	p	k?	p
a	a	k8xC	a
q.	q.	k?	q.
V	v	k7c6	v
predikátové	predikátový	k2eAgFnSc6d1	predikátová
logice	logika	k1gFnSc6	logika
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
bude	být	k5eAaImBp3nS	být
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
daleko	daleko	k6eAd1	daleko
provázaněji	provázaně	k6eAd2	provázaně
za	za	k7c2	za
použití	použití	k1gNnSc2	použití
predikátu	predikát	k1gInSc2	predikát
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Predikát	predikát	k1gInSc1	predikát
P	P	kA	P
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
reprezentovaný	reprezentovaný	k2eAgInSc1d1	reprezentovaný
proměnnou	proměnná	k1gFnSc7	proměnná
x	x	k?	x
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
x	x	k?	x
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
Sókrata	Sókrat	k1gMnSc4	Sókrat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
P	P	kA	P
říkat	říkat	k5eAaImF	říkat
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
co	co	k9	co
výrok	výrok	k1gInSc1	výrok
p.	p.	k?	p.
Obdobně	obdobně	k6eAd1	obdobně
tomu	ten	k3xDgNnSc3	ten
bude	být	k5eAaImBp3nS	být
u	u	k7c2	u
Platóna	Platón	k1gMnSc2	Platón
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
výroku	výrok	k1gInSc2	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
příkladu	příklad	k1gInSc6	příklad
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
klíčový	klíčový	k2eAgInSc1d1	klíčový
aspekt	aspekt	k1gInSc1	aspekt
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
:	:	kIx,	:
P	P	kA	P
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
syntaktickou	syntaktický	k2eAgFnSc7d1	syntaktická
entitou	entita	k1gFnSc7	entita
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
byl	být	k5eAaImAgInS	být
dán	dán	k2eAgInSc1d1	dán
sémantický	sémantický	k2eAgInSc1d1	sémantický
význam	význam	k1gInSc1	význam
deklarováním	deklarování	k1gNnSc7	deklarování
<g/>
,	,	kIx,	,
že	že	k8xS	že
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozofem	filozof	k1gMnSc7	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Takovémuto	takovýto	k3xDgNnSc3	takovýto
přiřazení	přiřazení	k1gNnSc3	přiřazení
sémantického	sémantický	k2eAgInSc2d1	sémantický
významu	význam	k1gInSc2	význam
říkáme	říkat	k5eAaImIp1nP	říkat
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uvažování	uvažování	k1gNnSc4	uvažování
nad	nad	k7c7	nad
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
sdíleny	sdílen	k2eAgFnPc1d1	sdílena
mnoha	mnoho	k4c7	mnoho
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Uveďme	uvést	k5eAaPmRp1nP	uvést
si	se	k3xPyFc3	se
příklad	příklad	k1gInSc4	příklad
<g/>
,	,	kIx,	,
nechť	nechť	k9	nechť
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
nechť	nechť	k9	nechť
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
formule	formule	k1gFnSc1	formule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
→	→	k?	→
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
pak	pak	k6eAd1	pak
x	x	k?	x
je	být	k5eAaImIp3nS	být
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
}	}	kIx)	}
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
implikace	implikace	k1gFnSc2	implikace
(	(	kIx(	(
<g/>
podmínkové	podmínkový	k2eAgFnSc2d1	podmínková
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
leží	ležet	k5eAaImIp3nS	ležet
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
šipky	šipka	k1gFnSc2	šipka
<g/>
,	,	kIx,	,
závěr	závěr	k1gInSc4	závěr
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivost	pravdivost	k1gFnSc1	pravdivost
této	tento	k3xDgFnSc2	tento
formule	formule	k1gFnSc2	formule
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
reprezentován	reprezentovat	k5eAaImNgInS	reprezentovat
proměnnou	proměnná	k1gFnSc7	proměnná
x	x	k?	x
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
interpretaci	interpretace	k1gFnSc6	interpretace
predikátů	predikát	k1gInPc2	predikát
P	P	kA	P
a	a	k8xC	a
S.	S.	kA	S.
Pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
výrazů	výraz	k1gInPc2	výraz
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
x	x	k?	x
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozofem	filozof	k1gMnSc7	filozof
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
x	x	k?	x
je	být	k5eAaImIp3nS	být
učencem	učenec	k1gMnSc7	učenec
<g/>
"	"	kIx"	"
již	již	k6eAd1	již
nestačí	stačit	k5eNaBmIp3nS	stačit
použití	použití	k1gNnSc1	použití
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musíme	muset	k5eAaImIp1nP	muset
použít	použít	k5eAaPmF	použít
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
nechť	nechť	k9	nechť
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
nechť	nechť	k9	nechť
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
učencem	učenec	k1gMnSc7	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
sentence	sentence	k1gFnSc1	sentence
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
→	→	k?	→
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
}	}	kIx)	}
:	:	kIx,	:
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
x	x	k?	x
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jestliže	jestliže	k8xS	jestliže
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozofem	filozof	k1gMnSc7	filozof
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
x	x	k?	x
je	být	k5eAaImIp3nS	být
učencem	učenec	k1gMnSc7	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
,	,	kIx,	,
neboli	neboli	k8xC	neboli
univerzální	univerzální	k2eAgInSc4d1	univerzální
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
výraz	výraz	k1gInSc1	výraz
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
volby	volba	k1gFnPc4	volba
proměnné	proměnná	k1gFnSc2	proměnná
x.	x.	k?	x.
Pro	pro	k7c4	pro
dokázání	dokázání	k1gNnSc4	dokázání
nepravdivosti	nepravdivost	k1gFnSc2	nepravdivost
tvrzení	tvrzení	k1gNnSc2	tvrzení
"	"	kIx"	"
<g/>
jestliže	jestliže	k8xS	jestliže
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
x	x	k?	x
je	být	k5eAaImIp3nS	být
učencem	učenec	k1gMnSc7	učenec
<g/>
"	"	kIx"	"
musíme	muset	k5eAaImIp1nP	muset
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
není	být	k5eNaImIp3nS	být
učencem	učenec	k1gMnSc7	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
protitvrzení	protitvrzení	k1gNnSc4	protitvrzení
můžeme	moct	k5eAaImIp1nP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
existenčního	existenční	k2eAgInSc2d1	existenční
kvantifikátoru	kvantifikátor	k1gInSc2	kvantifikátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	exists	k1gInSc4	exists
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
∧	∧	k?	∧
¬	¬	k?	¬
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
land	land	k1gMnSc1	land
\	\	kIx~	\
<g/>
lnot	lnot	k1gMnSc1	lnot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lnot	lnot	k5eAaPmF	lnot
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
operátor	operátor	k1gInSc4	operátor
negace	negace	k1gFnSc2	negace
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lnot	lnotum	k1gNnPc2	lnotum
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
nepravda	nepravda	k1gFnSc1	nepravda
-	-	kIx~	-
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
není	být	k5eNaImIp3nS	být
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
land	land	k1gInSc4	land
}	}	kIx)	}
operátor	operátor	k1gInSc4	operátor
konjunkce	konjunkce	k1gFnSc2	konjunkce
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
∧	∧	k?	∧
¬	¬	k?	¬
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
land	land	k1gMnSc1	land
\	\	kIx~	\
<g/>
lnot	lnot	k1gMnSc1	lnot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
není	být	k5eNaImIp3nS	být
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Predikáty	predikát	k1gInPc1	predikát
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
a	a	k8xC	a
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
příkladu	příklad	k1gInSc2	příklad
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
parametr	parametr	k1gInSc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
schopná	schopný	k2eAgFnSc1d1	schopná
zvládnout	zvládnout	k5eAaPmF	zvládnout
i	i	k8xC	i
predikáty	predikát	k1gInPc1	predikát
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgInSc7	jeden
parametrem	parametr	k1gInSc7	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
pokaždé	pokaždé	k6eAd1	pokaždé
napálit	napálit	k5eAaPmF	napálit
<g/>
"	"	kIx"	"
můžeme	moct	k5eAaImIp1nP	moct
popsat	popsat	k5eAaPmF	popsat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
x	x	k?	x
(	(	kIx(	(
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
∧	∧	k?	∧
∀	∀	k?	∀
y	y	k?	y
(	(	kIx(	(
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
(	(	kIx(	(
y	y	k?	y
)	)	kIx)	)
→	→	k?	→
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
)	)	kIx)	)
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
land	land	k1gInSc1	land
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
y	y	k?	y
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)))	)))	k?	)))
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tentokrát	tentokrát	k6eAd1	tentokrát
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
Time	Time	k1gFnSc1	Time
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
interpretován	interpretovat	k5eAaBmNgInS	interpretovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
y	y	k?	y
je	být	k5eAaImIp3nS	být
moment	moment	k1gInSc4	moment
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
osoba	osoba	k1gFnSc1	osoba
<g/>
)	)	kIx)	)
x	x	k?	x
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
napálena	napálit	k5eAaPmNgFnS	napálit
v	v	k7c6	v
(	(	kIx(	(
<g/>
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
y.	y.	k?	y.
Pro	pro	k7c4	pro
jasnost	jasnost	k1gFnSc4	jasnost
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
napálena	napálit	k5eAaPmNgFnS	napálit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
časových	časový	k2eAgInPc6d1	časový
momentech	moment	k1gInPc6	moment
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgNnSc1d2	silnější
tvrzení	tvrzení	k1gNnSc1	tvrzení
než	než	k8xS	než
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
časové	časový	k2eAgInPc4d1	časový
momenty	moment	k1gInPc4	moment
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
napálena	napálit	k5eAaPmNgFnS	napálit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
tvrzení	tvrzení	k1gNnSc1	tvrzení
totiž	totiž	k9	totiž
nespecifikuje	specifikovat	k5eNaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
ona	onen	k3xDgFnSc1	onen
napálená	napálený	k2eAgFnSc1d1	napálená
osoba	osoba	k1gFnSc1	osoba
jedna	jeden	k4xCgFnSc1	jeden
a	a	k8xC	a
tatáž	týž	k3xTgFnSc1	týž
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
okamžiky	okamžik	k1gInPc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Rozsahem	rozsah	k1gInSc7	rozsah
kvantifikátorů	kvantifikátor	k1gInPc2	kvantifikátor
rozumíme	rozumět	k5eAaImIp1nP	rozumět
množinu	množina	k1gFnSc4	množina
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	on	k3xPp3gInPc4	on
mohou	moct	k5eAaImIp3nP	moct
uspokojit	uspokojit	k5eAaPmF	uspokojit
(	(	kIx(	(
<g/>
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
neformálních	formální	k2eNgInPc6d1	neformální
příkladech	příklad	k1gInPc6	příklad
nebyl	být	k5eNaImAgInS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
specifikace	specifikace	k1gFnSc2	specifikace
významu	význam	k1gInSc2	význam
predikátových	predikátový	k2eAgInPc2d1	predikátový
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
interpretace	interpretace	k1gFnSc1	interpretace
určit	určit	k5eAaPmF	určit
neprázdnou	prázdný	k2eNgFnSc4d1	neprázdná
množinu	množina	k1gFnSc4	množina
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
univerzální	univerzální	k2eAgFnSc1d1	univerzální
množina	množina	k1gFnSc1	množina
nebo	nebo	k8xC	nebo
univerzum	univerzum	k1gNnSc1	univerzum
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
rozsah	rozsah	k1gInSc1	rozsah
pro	pro	k7c4	pro
kvantifikátory	kvantifikátor	k1gInPc4	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
o	o	k7c6	o
prohlášení	prohlášení	k1gNnSc6	prohlášení
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
a	a	k8xC	a
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	exists	k1gInSc4	exists
a	a	k8xC	a
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
interpretaci	interpretace	k1gFnSc3	interpretace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
nějaký	nějaký	k3yIgInSc4	nějaký
objekt	objekt	k1gInSc4	objekt
z	z	k7c2	z
univerza	univerzum	k1gNnSc2	univerzum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zaručí	zaručit	k5eAaPmIp3nS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
interpretace	interpretace	k1gFnSc1	interpretace
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
správný	správný	k2eAgInSc4d1	správný
význam	význam	k1gInSc4	význam
predikátu	predikát	k1gInSc2	predikát
P.	P.	kA	P.
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
klíčových	klíčový	k2eAgFnPc2d1	klíčová
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Syntax	syntax	k1gFnSc1	syntax
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
kolekce	kolekce	k1gFnSc2	kolekce
(	(	kIx(	(
<g/>
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
)	)	kIx)	)
symbolů	symbol	k1gInPc2	symbol
jsou	být	k5eAaImIp3nP	být
legální	legální	k2eAgInPc1d1	legální
výrazy	výraz	k1gInPc1	výraz
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sémantika	sémantika	k1gFnSc1	sémantika
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
významu	význam	k1gInSc6	význam
za	za	k7c7	za
těmito	tento	k3xDgInPc7	tento
výrazy	výraz	k1gInPc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
přirozených	přirozený	k2eAgInPc2d1	přirozený
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
zcela	zcela	k6eAd1	zcela
formální	formální	k2eAgInSc1d1	formální
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
můžeme	moct	k5eAaImIp1nP	moct
zcela	zcela	k6eAd1	zcela
mechanicky	mechanicky	k6eAd1	mechanicky
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
výraz	výraz	k1gInSc1	výraz
legálně	legálně	k6eAd1	legálně
utvořen	utvořit	k5eAaPmNgInS	utvořit
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
výrazů	výraz	k1gInPc2	výraz
<g/>
:	:	kIx,	:
termy	term	k1gInPc1	term
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
intuitivně	intuitivně	k6eAd1	intuitivně
představují	představovat	k5eAaImIp3nP	představovat
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
predikáty	predikát	k1gInPc4	predikát
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
nebo	nebo	k8xC	nebo
nepravdivé	pravdivý	k2eNgFnPc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Termy	termy	k1gFnPc1	termy
a	a	k8xC	a
formule	formule	k1gFnSc1	formule
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
jsou	být	k5eAaImIp3nP	být
řetězce	řetězec	k1gInPc4	řetězec
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nP	tvořit
abecedu	abeceda	k1gFnSc4	abeceda
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
formálních	formální	k2eAgInPc2d1	formální
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povaha	povaha	k1gFnSc1	povaha
těchto	tento	k3xDgInPc2	tento
symbolů	symbol	k1gInPc2	symbol
mimo	mimo	k7c4	mimo
záběr	záběr	k1gInSc4	záběr
formální	formální	k2eAgFnSc2d1	formální
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
brány	brána	k1gFnPc1	brána
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k8xC	jako
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
interpunkční	interpunkční	k2eAgNnPc4d1	interpunkční
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
symboly	symbol	k1gInPc1	symbol
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
logické	logický	k2eAgInPc4d1	logický
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
tentýž	týž	k3xTgInSc4	týž
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
mimo-logické	mimoogický	k2eAgInPc4d1	mimo-logický
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
význam	význam	k1gInSc1	význam
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
dle	dle	k7c2	dle
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
,	,	kIx,	,
logický	logický	k2eAgInSc1d1	logický
symbol	symbol	k1gInSc1	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
land	land	k1gInSc4	land
}	}	kIx)	}
vždy	vždy	k6eAd1	vždy
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
interpretován	interpretován	k2eAgInSc1d1	interpretován
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nebo	nebo	k8xC	nebo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
jako	jako	k9	jako
jiná	jiný	k2eAgFnSc1d1	jiná
spojka	spojka	k1gFnSc1	spojka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
mimo-logický	mimoogický	k2eAgInSc4d1	mimo-logický
predikátový	predikátový	k2eAgInSc4d1	predikátový
symbol	symbol	k1gInSc4	symbol
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
interpretovat	interpretovat	k5eAaBmF	interpretovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
x	x	k?	x
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
jiný	jiný	k2eAgInSc1d1	jiný
unární	unární	k2eAgInSc1d1	unární
predikát	predikát	k1gInSc1	predikát
závisející	závisející	k2eAgInSc1d1	závisející
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
interpretaci	interpretace	k1gFnSc4	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
několik	několik	k4yIc4	několik
logických	logický	k2eAgInPc2d1	logický
symbolů	symbol	k1gInPc2	symbol
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
autor	autor	k1gMnSc1	autor
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Kvantifikační	Kvantifikační	k2eAgInPc1d1	Kvantifikační
symboly	symbol	k1gInPc1	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	exists	k1gInSc4	exists
}	}	kIx)	}
:	:	kIx,	:
Logické	logický	k2eAgFnPc1d1	logická
spojky	spojka	k1gFnPc1	spojka
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
land	land	k1gInSc4	land
}	}	kIx)	}
pro	pro	k7c4	pro
konjunkci	konjunkce	k1gFnSc4	konjunkce
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∨	∨	k?	∨
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lor	lor	k?	lor
}	}	kIx)	}
pro	pro	k7c4	pro
disjunkci	disjunkce	k1gFnSc4	disjunkce
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
pro	pro	k7c4	pro
implikaci	implikace	k1gFnSc4	implikace
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
↔	↔	k?	↔
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
leftrightarrow	leftrightarrow	k?	leftrightarrow
}	}	kIx)	}
pro	pro	k7c4	pro
ekvivalenci	ekvivalence	k1gFnSc4	ekvivalence
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lnot	lnot	k5eAaPmF	lnot
}	}	kIx)	}
pro	pro	k7c4	pro
negaci	negace	k1gFnSc4	negace
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
další	další	k2eAgFnPc1d1	další
logické	logický	k2eAgFnPc1d1	logická
spojky	spojka	k1gFnPc1	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
autoři	autor	k1gMnPc1	autor
používají	používat	k5eAaImIp3nP	používat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⇒	⇒	k?	⇒
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gMnSc6	Rightarrow
}	}	kIx)	}
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Cpq	Cpq	k1gFnSc1	Cpq
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
,	,	kIx,	,
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⇔	⇔	k?	⇔
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gMnSc6	Leftrightarrow
}	}	kIx)	}
,	,	kIx,	,
či	či	k8xC	či
Epq	Epq	k1gFnSc1	Epq
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
↔	↔	k?	↔
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
leftrightarrow	leftrightarrow	k?	leftrightarrow
}	}	kIx)	}
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊃	⊃	k?	⊃
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
supset	supset	k5eAaImF	supset
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
nahradit	nahradit	k5eAaPmF	nahradit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
;	;	kIx,	;
místo	místo	k1gNnSc1	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
↔	↔	k?	↔
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
leftrightarrow	leftrightarrow	k?	leftrightarrow
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
equiv	equivo	k1gNnPc2	equivo
}	}	kIx)	}
,	,	kIx,	,
a	a	k8xC	a
tilda	tilda	k1gFnSc1	tilda
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Np	Np	k1gFnSc1	Np
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Fpq	Fpq	k1gFnSc1	Fpq
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
nahradit	nahradit	k5eAaPmF	nahradit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
lnot	lnot	k1gInSc1	lnot
}	}	kIx)	}
;	;	kIx,	;
||	||	k?	||
<g/>
,	,	kIx,	,
či	či	k8xC	či
Apq	Apq	k1gFnSc1	Apq
zase	zase	k9	zase
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∨	∨	k?	∨
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lor	lor	k?	lor
}	}	kIx)	}
;	;	kIx,	;
a	a	k8xC	a
&	&	k?	&
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Kpq	Kpq	k1gFnSc1	Kpq
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
místo	místo	k7c2	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
land	lando	k1gNnPc2	lando
}	}	kIx)	}
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
tyto	tento	k3xDgInPc4	tento
symboly	symbol	k1gInPc4	symbol
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
závorek	závorka	k1gFnPc2	závorka
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
interpunkční	interpunkční	k2eAgNnPc4d1	interpunkční
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
těchto	tento	k3xDgInPc2	tento
symbolů	symbol	k1gInPc2	symbol
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
dle	dle	k7c2	dle
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečná	konečný	k2eNgFnSc1d1	nekonečná
množina	množina	k1gFnSc1	množina
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
označovaných	označovaný	k2eAgMnPc2d1	označovaný
malými	malý	k2eAgFnPc7d1	malá
písmeny	písmeno	k1gNnPc7	písmeno
z	z	k7c2	z
konce	konec	k1gInSc2	konec
abecedy	abeceda	k1gFnSc2	abeceda
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
etc	etc	k?	etc
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
použít	použít	k5eAaPmF	použít
dolní	dolní	k2eAgInSc4d1	dolní
index	index	k1gInSc4	index
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
:	:	kIx,	:
x	x	k?	x
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
Symbol	symbol	k1gInSc1	symbol
rovnosti	rovnost	k1gFnSc2	rovnost
=	=	kIx~	=
Stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
poznamenání	poznamenání	k1gNnSc4	poznamenání
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všechny	všechen	k3xTgInPc4	všechen
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
symboly	symbol	k1gInPc4	symbol
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgInPc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Postačují	postačovat	k5eAaImIp3nP	postačovat
<g/>
:	:	kIx,	:
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
kvantifikátorů	kvantifikátor	k1gInPc2	kvantifikátor
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
univerzální	univerzální	k2eAgMnSc1d1	univerzální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
negace	negace	k1gFnSc1	negace
a	a	k8xC	a
konjunkce	konjunkce	k1gFnPc1	konjunkce
<g/>
,	,	kIx,	,
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
,	,	kIx,	,
závorky	závorka	k1gFnPc1	závorka
a	a	k8xC	a
symbol	symbol	k1gInSc1	symbol
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
ještě	ještě	k9	ještě
další	další	k2eAgFnPc4d1	další
drobné	drobný	k2eAgFnPc4d1	drobná
variace	variace	k1gFnPc4	variace
<g/>
:	:	kIx,	:
Mnohdy	mnohdy	k6eAd1	mnohdy
zavádíme	zavádět	k5eAaImIp1nP	zavádět
pravdivostní	pravdivostní	k2eAgFnPc1d1	pravdivostní
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
:	:	kIx,	:
T	T	kA	T
<g/>
,	,	kIx,	,
Vpq	Vpq	k1gFnSc1	Vpq
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊤	⊤	k?	⊤
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
top	topit	k5eAaImRp2nS	topit
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
pravdu	pravda	k1gFnSc4	pravda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tautologii	tautologie	k1gFnSc4	tautologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
F	F	kA	F
<g/>
,	,	kIx,	,
Opq	Opq	k1gFnSc1	Opq
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊥	⊥	k?	⊥
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
bot	bota	k1gFnPc2	bota
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
nepravdu	nepravda	k1gFnSc4	nepravda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kontradikci	kontradikce	k1gFnSc4	kontradikce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
takovýchto	takovýto	k3xDgInPc2	takovýto
logických	logický	k2eAgInPc2d1	logický
operátorů	operátor	k1gInPc2	operátor
nulové	nulový	k2eAgInPc4d1	nulový
arity	arit	k1gInPc4	arit
můžeme	moct	k5eAaImIp1nP	moct
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
konstanty	konstanta	k1gFnPc1	konstanta
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kvantifikátorů	kvantifikátor	k1gInPc2	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
přidáváme	přidávat	k5eAaImIp1nP	přidávat
další	další	k2eAgFnPc4d1	další
logické	logický	k2eAgFnPc4d1	logická
spojky	spojka	k1gFnPc4	spojka
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
Shefferova	Shefferův	k2eAgNnPc4d1	Shefferův
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
Dpq	Dpq	k1gFnSc2	Dpq
(	(	kIx(	(
<g/>
NAND	Nanda	k1gFnPc2	Nanda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
exkluzivní	exkluzivní	k2eAgFnPc4d1	exkluzivní
disjunkce	disjunkce	k1gFnPc4	disjunkce
<g/>
,	,	kIx,	,
Jpq	Jpq	k1gFnSc4	Jpq
<g/>
.	.	kIx.	.
</s>
<s>
Mimo-logické	Mimoogický	k2eAgInPc1d1	Mimo-logický
symboly	symbol	k1gInPc1	symbol
představují	představovat	k5eAaImIp3nP	představovat
predikáty	predikát	k1gInPc1	predikát
(	(	kIx(	(
<g/>
relace	relace	k1gFnPc1	relace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
konstanty	konstanta	k1gFnPc1	konstanta
představené	představený	k2eAgFnPc1d1	představená
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
univerza	univerzum	k1gNnSc2	univerzum
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc7d1	standardní
praxí	praxe	k1gFnSc7	praxe
bylo	být	k5eAaImAgNnS	být
používat	používat	k5eAaImF	používat
fixní	fixní	k2eAgFnSc4d1	fixní
<g/>
,	,	kIx,	,
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
množinu	množina	k1gFnSc4	množina
mimologických	mimologický	k2eAgInPc2d1	mimologický
symbolů	symbol	k1gInPc2	symbol
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používáme	používat	k5eAaImIp1nP	používat
různé	různý	k2eAgInPc4d1	různý
mimo-logické	mimoogický	k2eAgInPc4d1	mimo-logický
symboly	symbol	k1gInPc4	symbol
podle	podle	k7c2	podle
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
nutné	nutný	k2eAgNnSc1d1	nutné
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgInPc2	všecek
mimo-logických	mimoogický	k2eAgInPc2d1	mimo-logický
symbolů	symbol	k1gInPc2	symbol
použitou	použitý	k2eAgFnSc4d1	použitá
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
volbu	volba	k1gFnSc4	volba
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
tzv.	tzv.	kA	tzv.
signatura	signatura	k1gFnSc1	signatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
přístupu	přístup	k1gInSc6	přístup
máme	mít	k5eAaImIp1nP	mít
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
množinu	množina	k1gFnSc4	množina
mimo-logických	mimoogický	k2eAgInPc2d1	mimo-logický
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
jednu	jeden	k4xCgFnSc4	jeden
signaturu	signatura	k1gFnSc4	signatura
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
jazyk	jazyk	k1gInSc1	jazyk
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
filosoficky	filosoficky	k6eAd1	filosoficky
orientovaných	orientovaný	k2eAgFnPc6d1	orientovaná
knihách	kniha	k1gFnPc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
n	n	k0	n
≥	≥	k?	≥
0	[number]	k4	0
existuje	existovat	k5eAaImIp3nS	existovat
posloupnost	posloupnost	k1gFnSc4	posloupnost
n-árních	n-ární	k2eAgInPc2d1	n-ární
predikátových	predikátový	k2eAgInPc2d1	predikátový
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
predikáty	predikát	k1gInPc1	predikát
představují	představovat	k5eAaImIp3nP	představovat
relace	relace	k1gFnPc4	relace
mezi	mezi	k7c7	mezi
n	n	k0	n
elementy	element	k1gInPc4	element
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
relační	relační	k2eAgInPc4d1	relační
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
aritu	arit	k1gInSc2	arit
n	n	k0	n
jich	on	k3xPp3gMnPc2	on
máme	mít	k5eAaImIp1nP	mít
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
zásobu	zásoba	k1gFnSc4	zásoba
<g/>
:	:	kIx,	:
Pn	Pn	k1gFnSc1	Pn
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Pn	Pn	k1gFnSc1	Pn
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Pn	Pn	k1gFnSc1	Pn
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Pn	Pn	k1gFnSc1	Pn
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
...	...	k?	...
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
n	n	k0	n
≥	≥	k?	≥
0	[number]	k4	0
existuje	existovat	k5eAaImIp3nS	existovat
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
n-árních	n-ární	k2eAgInPc2d1	n-ární
funkčních	funkční	k2eAgInPc2d1	funkční
symbolů	symbol	k1gInPc2	symbol
<g/>
:	:	kIx,	:
f	f	k?	f
n	n	k0	n
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
f	f	k?	f
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
f	f	k?	f
n	n	k0	n
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
f	f	k?	f
n	n	k0	n
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
...	...	k?	...
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
matematické	matematický	k2eAgFnSc6d1	matematická
logice	logika	k1gFnSc6	logika
se	se	k3xPyFc4	se
signatura	signatura	k1gFnSc1	signatura
liší	lišit	k5eAaImIp3nS	lišit
dle	dle	k7c2	dle
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
signatury	signatura	k1gFnPc1	signatura
jsou	být	k5eAaImIp3nP	být
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
×	×	k?	×
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
{	{	kIx(	{
<g/>
×	×	k?	×
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
grupy	grupa	k1gFnPc4	grupa
<g/>
,	,	kIx,	,
či	či	k8xC	či
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
,	,	kIx,	,
×	×	k?	×
<g/>
,	,	kIx,	,
<	<	kIx(	<
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
uspořádaná	uspořádaný	k2eAgNnPc4d1	uspořádané
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počet	počet	k1gInSc4	počet
mimo-logických	mimoogický	k2eAgInPc2d1	mimo-logický
symbolů	symbol	k1gInPc2	symbol
nejsou	být	k5eNaImIp3nP	být
kladena	kladen	k2eAgNnPc1d1	kladeno
žádná	žádný	k3yNgNnPc4	žádný
omezení	omezení	k1gNnSc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Signaturou	signatura	k1gFnSc7	signatura
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
,	,	kIx,	,
konečná	konečný	k2eAgFnSc1d1	konečná
nebo	nebo	k8xC	nebo
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
nespočetná	spočetný	k2eNgFnSc1d1	nespočetná
množina	množina	k1gFnSc1	množina
<g/>
.	.	kIx.	.
</s>
<s>
Nespočetné	spočetný	k2eNgFnPc1d1	nespočetná
signatury	signatura	k1gFnPc1	signatura
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
např.	např.	kA	např.
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
moderních	moderní	k2eAgInPc2d1	moderní
důkazů	důkaz	k1gInPc2	důkaz
Löwenheim-Skolemovy	Löwenheim-Skolemův	k2eAgFnSc2d1	Löwenheim-Skolemův
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
přístupu	přístup	k1gInSc6	přístup
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
mimo-logický	mimoogický	k2eAgInSc1d1	mimo-logický
symbol	symbol	k1gInSc1	symbol
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Predikátový	predikátový	k2eAgInSc1d1	predikátový
symbol	symbol	k1gInSc1	symbol
(	(	kIx(	(
<g/>
relační	relační	k2eAgInSc1d1	relační
symbol	symbol	k1gInSc1	symbol
<g/>
)	)	kIx)	)
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
valencí	valence	k1gFnSc7	valence
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
aritou	aritá	k1gFnSc4	aritá
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
argumentů	argument	k1gInPc2	argument
<g/>
)	)	kIx)	)
větší	veliký	k2eAgFnPc4d2	veliký
nebo	nebo	k8xC	nebo
rovnou	rovnou	k6eAd1	rovnou
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	on	k3xPp3gInPc4	on
značíme	značit	k5eAaImIp1nP	značit
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
P	P	kA	P
<g/>
,	,	kIx,	,
Q	Q	kA	Q
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
Relace	relace	k1gFnSc1	relace
s	s	k7c7	s
valencí	valence	k1gFnSc7	valence
0	[number]	k4	0
mohou	moct	k5eAaImIp3nP	moct
značit	značit	k5eAaImF	značit
výrokové	výrokový	k2eAgFnPc4d1	výroková
proměnné	proměnná	k1gFnPc4	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
P	P	kA	P
může	moct	k5eAaImIp3nS	moct
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
výrok	výrok	k1gInSc4	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
predikát	predikát	k1gInSc1	predikát
s	s	k7c7	s
valencí	valence	k1gFnSc7	valence
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
možnou	možný	k2eAgFnSc7d1	možná
interpretací	interpretace	k1gFnSc7	interpretace
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
x	x	k?	x
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Q	Q	kA	Q
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
predikát	predikát	k1gInSc1	predikát
s	s	k7c7	s
valencí	valence	k1gFnSc7	valence
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Možné	možný	k2eAgFnPc1d1	možná
interpretace	interpretace	k1gFnPc1	interpretace
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
x	x	k?	x
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
y	y	k?	y
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
x	x	k?	x
je	být	k5eAaImIp3nS	být
otec	otec	k1gMnSc1	otec
y	y	k?	y
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgInSc1d1	funkční
symbol	symbol	k1gInSc1	symbol
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
valencí	valence	k1gFnSc7	valence
větší	veliký	k2eAgFnSc7d2	veliký
nebo	nebo	k8xC	nebo
rovnou	rovnou	k6eAd1	rovnou
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgInPc4d1	funkční
symboly	symbol	k1gInPc4	symbol
často	často	k6eAd1	často
značíme	značit	k5eAaImIp1nP	značit
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
interpretováno	interpretovat	k5eAaBmNgNnS	interpretovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
x	x	k?	x
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
"	"	kIx"	"
<g/>
-x	-x	k?	-x
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
může	moct	k5eAaImIp3nS	moct
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
"	"	kIx"	"
<g/>
potenční	potenční	k2eAgFnSc4d1	potenční
množinu	množina	k1gFnSc4	množina
množiny	množina	k1gFnSc2	množina
x	x	k?	x
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
korespondovat	korespondovat	k5eAaImF	korespondovat
s	s	k7c7	s
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
zase	zase	k9	zase
"	"	kIx"	"
<g/>
sjednocení	sjednocení	k1gNnSc1	sjednocení
x	x	k?	x
a	a	k8xC	a
y	y	k?	y
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgInPc4d1	funkční
symboly	symbol	k1gInPc4	symbol
valence	valence	k1gFnSc2	valence
0	[number]	k4	0
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
konstantní	konstantní	k2eAgInPc1d1	konstantní
symboly	symbol	k1gInPc1	symbol
a	a	k8xC	a
často	často	k6eAd1	často
je	on	k3xPp3gInPc4	on
píšeme	psát	k5eAaImIp1nP	psát
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
představovat	představovat	k5eAaImF	představovat
Sókrata	Sókrata	k1gFnSc1	Sókrata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
představit	představit	k5eAaPmF	představit
jako	jako	k9	jako
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
zase	zase	k9	zase
jako	jako	k9	jako
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
množinu	množina	k1gFnSc4	množina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tradičního	tradiční	k2eAgInSc2d1	tradiční
přístupu	přístup	k1gInSc2	přístup
se	se	k3xPyFc4	se
jde	jít	k5eAaImIp3nS	jít
jednoduše	jednoduše	k6eAd1	jednoduše
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
moderního	moderní	k2eAgMnSc2d1	moderní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
specifikujeme	specifikovat	k5eAaBmIp1nP	specifikovat
signaturu	signatura	k1gFnSc4	signatura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
tradičních	tradiční	k2eAgFnPc2d1	tradiční
sekvencí	sekvence	k1gFnPc2	sekvence
mimologických	mimologický	k2eAgInPc2d1	mimologický
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
tvorby	tvorba	k1gFnSc2	tvorba
definují	definovat	k5eAaBmIp3nP	definovat
termy	termy	k1gFnPc1	termy
a	a	k8xC	a
formule	formule	k1gFnSc1	formule
pro	pro	k7c4	pro
predikátovou	predikátový	k2eAgFnSc4d1	predikátová
logiku	logika	k1gFnSc4	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
termy	termy	k1gFnPc1	termy
a	a	k8xC	a
formule	formule	k1gFnPc1	formule
reprezentovány	reprezentován	k2eAgFnPc1d1	reprezentována
řetězci	řetězec	k1gInSc3	řetězec
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
použita	použit	k2eAgNnPc1d1	použito
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
formální	formální	k2eAgFnSc2d1	formální
gramatiky	gramatika	k1gFnSc2	gramatika
pro	pro	k7c4	pro
termy	termy	k1gFnPc4	termy
a	a	k8xC	a
formule	formule	k1gFnSc2	formule
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
bezkontextová	bezkontextový	k2eAgFnSc1d1	bezkontextová
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
symbolů	symbol	k1gInPc2	symbol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
a	a	k8xC	a
startovacích	startovací	k2eAgInPc2d1	startovací
symbolů	symbol	k1gInPc2	symbol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
termů	term	k1gInPc2	term
je	být	k5eAaImIp3nS	být
induktivně	induktivně	k6eAd1	induktivně
definována	definovat	k5eAaBmNgFnS	definovat
pomocí	pomocí	k7c2	pomocí
následujících	následující	k2eAgNnPc2d1	následující
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
:	:	kIx,	:
Proměnné	proměnná	k1gFnSc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
proměnná	proměnná	k1gFnSc1	proměnná
je	být	k5eAaImIp3nS	být
term	term	k1gInSc4	term
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
výraz	výraz	k1gInSc1	výraz
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
tn	tn	k?	tn
<g/>
)	)	kIx)	)
s	s	k7c7	s
n	n	k0	n
argumenty	argument	k1gInPc7	argument
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
argument	argument	k1gInSc1	argument
ti	ty	k3xPp2nSc3	ty
je	být	k5eAaImIp3nS	být
term	term	k1gInSc1	term
a	a	k8xC	a
f	f	k?	f
je	být	k5eAaImIp3nS	být
funkční	funkční	k2eAgInSc1d1	funkční
symbol	symbol	k1gInSc1	symbol
arity	arit	k1gInPc1	arit
n	n	k0	n
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
term	term	k1gInSc4	term
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
<g/>
,	,	kIx,	,
symboly	symbol	k1gInPc7	symbol
reprezentující	reprezentující	k2eAgFnSc2d1	reprezentující
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
konstanty	konstanta	k1gFnSc2	konstanta
jsou	být	k5eAaImIp3nP	být
0	[number]	k4	0
<g/>
-ární	-ární	k2eAgInPc4d1	-ární
funkční	funkční	k2eAgInPc4d1	funkční
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
termy	termy	k1gFnPc1	termy
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
výrazy	výraz	k1gInPc1	výraz
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
konečně	konečně	k6eAd1	konečně
mnoha	mnoho	k4c7	mnoho
aplikacemi	aplikace	k1gFnPc7	aplikace
pravidel	pravidlo	k1gNnPc2	pravidlo
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
termy	termy	k1gFnPc1	termy
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
žádný	žádný	k3yNgInSc4	žádný
výraz	výraz	k1gInSc4	výraz
obsahující	obsahující	k2eAgInSc1d1	obsahující
predikátový	predikátový	k2eAgInSc1d1	predikátový
symbol	symbol	k1gInSc1	symbol
není	být	k5eNaImIp3nS	být
term	term	k1gInSc4	term
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
formulí	formule	k1gFnPc2	formule
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k9	jako
dobře	dobře	k6eAd1	dobře
utvořené	utvořený	k2eAgFnSc2d1	utvořená
formule	formule	k1gFnSc2	formule
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rekursivně	rekursivně	k6eAd1	rekursivně
definována	definovat	k5eAaBmNgFnS	definovat
následujícími	následující	k2eAgFnPc7d1	následující
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
:	:	kIx,	:
Predikátové	predikátový	k2eAgInPc1d1	predikátový
symboly	symbol	k1gInPc1	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
P	P	kA	P
je	být	k5eAaImIp3nS	být
n-ární	n-ární	k2eAgInSc1d1	n-ární
predikátový	predikátový	k2eAgInSc1d1	predikátový
symbol	symbol	k1gInSc1	symbol
t	t	k?	t
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
tn	tn	k?	tn
jsou	být	k5eAaImIp3nP	být
termy	termy	k1gFnPc4	termy
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
tn	tn	k?	tn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
formule	formule	k1gFnSc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc1	symbol
rovnosti	rovnost	k1gFnSc2	rovnost
součástí	součást	k1gFnPc2	součást
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
t	t	k?	t
<g/>
1	[number]	k4	1
a	a	k8xC	a
t	t	k?	t
<g/>
2	[number]	k4	2
jsou	být	k5eAaImIp3nP	být
termy	term	k1gInPc1	term
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
t	t	k?	t
<g/>
1	[number]	k4	1
=	=	kIx~	=
t	t	k?	t
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
formule	formule	k1gFnSc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Negace	negace	k1gFnSc1	negace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
neg	neg	k?	neg
}	}	kIx)	}
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
také	také	k9	také
formule	formule	k1gFnSc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Binární	binární	k2eAgFnPc1d1	binární
spojky	spojka	k1gFnPc1	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
φ	φ	k?	φ
a	a	k8xC	a
ψ	ψ	k?	ψ
jsou	být	k5eAaImIp3nP	být
formule	formule	k1gFnPc4	formule
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
ψ	ψ	k?	ψ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
formule	formule	k1gFnSc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
definujeme	definovat	k5eAaBmIp1nP	definovat
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
binární	binární	k2eAgFnPc4d1	binární
spojky	spojka	k1gFnPc4	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Kvantifikátory	kvantifikátor	k1gInPc1	kvantifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
formule	formule	k1gFnSc1	formule
a	a	k8xC	a
x	x	k?	x
je	být	k5eAaImIp3nS	být
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
varphi	varph	k1gFnSc2	varph
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
x	x	k?	x
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
varphi	varph	k1gFnSc2	varph
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
formule	formule	k1gFnPc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
výrazy	výraz	k1gInPc1	výraz
získané	získaný	k2eAgInPc1d1	získaný
konečně	konečně	k6eAd1	konečně
mnoha	mnoho	k4c7	mnoho
aplikacemi	aplikace	k1gFnPc7	aplikace
pravidel	pravidlo	k1gNnPc2	pravidlo
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
jsou	být	k5eAaImIp3nP	být
formule	formule	k1gFnPc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Formulím	formule	k1gFnPc3	formule
sestavených	sestavený	k2eAgInPc2d1	sestavený
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgNnPc2	dva
pravidel	pravidlo	k1gNnPc2	pravidlo
říkáme	říkat	k5eAaImIp1nP	říkat
atomické	atomický	k2eAgFnPc4d1	atomická
formule	formule	k1gFnPc4	formule
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
∀	∀	k?	∀
y	y	k?	y
(	(	kIx(	(
P	P	kA	P
(	(	kIx(	(
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
→	→	k?	→
¬	¬	k?	¬
(	(	kIx(	(
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
→	→	k?	→
Q	Q	kA	Q
(	(	kIx(	(
f	f	k?	f
(	(	kIx(	(
y	y	k?	y
)	)	kIx)	)
,	,	kIx,	,
x	x	k?	x
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
forall	foralnout	k5eAaPmAgMnS	foralnout
y	y	k?	y
<g/>
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
neg	neg	k?	neg
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Q	Q	kA	Q
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)))	)))	k?	)))
<g/>
}	}	kIx)	}
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
f	f	k?	f
je	být	k5eAaImIp3nS	být
unární	unární	k2eAgInSc1d1	unární
funkční	funkční	k2eAgInSc1d1	funkční
symbol	symbol	k1gInSc1	symbol
<g/>
,	,	kIx,	,
P	P	kA	P
je	být	k5eAaImIp3nS	být
unární	unární	k2eAgInSc1d1	unární
predikátový	predikátový	k2eAgInSc1d1	predikátový
symbol	symbol	k1gInSc1	symbol
a	a	k8xC	a
Q	Q	kA	Q
je	být	k5eAaImIp3nS	být
ternální	ternální	k2eAgInSc1d1	ternální
predikátový	predikátový	k2eAgInSc1d1	predikátový
symbol	symbol	k1gInSc1	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
není	být	k5eNaImIp3nS	být
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
řetězec	řetězec	k1gInSc4	řetězec
znaků	znak	k1gInPc2	znak
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Rolí	role	k1gFnSc7	role
závorek	závorka	k1gFnPc2	závorka
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
je	být	k5eAaImIp3nS	být
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
formule	formule	k1gFnSc1	formule
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
utvořena	utvořen	k2eAgFnSc1d1	utvořena
jedním	jeden	k4xCgInSc7	jeden
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dle	dle	k7c2	dle
rekurzivní	rekurzivní	k2eAgFnSc2d1	rekurzivní
definice	definice	k1gFnSc2	definice
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
derivační	derivační	k2eAgInSc1d1	derivační
strom	strom	k1gInSc1	strom
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
formuli	formule	k1gFnSc4	formule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
čitelnost	čitelnost	k1gFnSc1	čitelnost
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
unique	unique	k1gFnSc1	unique
readability	readabilita	k1gFnSc2	readabilita
<g/>
)	)	kIx)	)
formulí	formule	k1gFnPc2	formule
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
mnoho	mnoho	k4c4	mnoho
konvencí	konvence	k1gFnPc2	konvence
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
jak	jak	k6eAd1	jak
používat	používat	k5eAaImF	používat
závorky	závorka	k1gFnPc4	závorka
ve	v	k7c6	v
formulích	formule	k1gFnPc6	formule
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
používají	používat	k5eAaImIp3nP	používat
dvojtečky	dvojtečka	k1gFnPc4	dvojtečka
nebo	nebo	k8xC	nebo
tečky	tečka	k1gFnPc4	tečka
namísto	namísto	k7c2	namísto
závorek	závorka	k1gFnPc2	závorka
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
umístění	umístění	k1gNnSc1	umístění
závorek	závorka	k1gFnPc2	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
autor	autor	k1gMnSc1	autor
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
konvence	konvence	k1gFnSc2	konvence
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
řešení	řešení	k1gNnSc1	řešení
dostává	dostávat	k5eAaImIp3nS	dostávat
čitelnosti	čitelnost	k1gFnSc3	čitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
formule	formule	k1gFnSc2	formule
představená	představený	k2eAgFnSc1d1	představená
výše	výše	k1gFnSc1	výše
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
definování	definování	k1gNnSc4	definování
jestliže-pak-jinak	jestližeakinak	k1gInSc1	jestliže-pak-jinak
(	(	kIx(	(
<g/>
if-then-else	ifhenlse	k1gFnSc1	if-then-else
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnSc1	funkce
ite	ite	k?	ite
<g/>
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
c	c	k0	c
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
podmínka	podmínka	k1gFnSc1	podmínka
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
jako	jako	k8xS	jako
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
navracela	navracet	k5eAaImAgFnS	navracet
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
c	c	k0	c
bylo	být	k5eAaImAgNnS	být
pravdivé	pravdivý	k2eAgInPc1d1	pravdivý
a	a	k8xC	a
"	"	kIx"	"
<g/>
b	b	k?	b
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc1	nedostatek
je	být	k5eAaImIp3nS	být
důsledek	důsledek	k1gInSc4	důsledek
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
predikáty	predikát	k1gInPc1	predikát
a	a	k8xC	a
funkce	funkce	k1gFnPc1	funkce
mohou	moct	k5eAaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
pouze	pouze	k6eAd1	pouze
termy	termy	k1gFnPc4	termy
jako	jako	k8xC	jako
parametry	parametr	k1gInPc4	parametr
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
ite	ite	k?	ite
<g/>
'	'	kIx"	'
funkce	funkce	k1gFnSc1	funkce
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
první	první	k4xOgInSc1	první
parametr	parametr	k1gInSc1	parametr
formuli	formule	k1gFnSc4	formule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
stavějící	stavějící	k2eAgInPc1d1	stavějící
na	na	k7c6	na
predikátové	predikátový	k2eAgFnSc6d1	predikátová
logice	logika	k1gFnSc6	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
např.	např.	kA	např.
SMT-LIB	SMT-LIB	k1gFnSc1	SMT-LIB
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
pohodlnosti	pohodlnost	k1gFnSc3	pohodlnost
se	se	k3xPyFc4	se
zavedlo	zavést	k5eAaPmAgNnS	zavést
mnoho	mnoho	k4c4	mnoho
konvencí	konvence	k1gFnPc2	konvence
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	s	k7c7	s
prioritou	priorita	k1gFnSc7	priorita
logických	logický	k2eAgInPc2d1	logický
operátorů	operátor	k1gInPc2	operátor
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vyhnutí	vyhnutí	k1gNnSc2	vyhnutí
se	se	k3xPyFc4	se
psaní	psaní	k1gNnSc1	psaní
závorek	závorka	k1gFnPc2	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
prioritě	priorita	k1gFnSc3	priorita
početních	početní	k2eAgFnPc2d1	početní
operací	operace	k1gFnPc2	operace
v	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
<g/>
.	.	kIx.	.
</s>
<s>
Běžnou	běžný	k2eAgFnSc7d1	běžná
konvencí	konvence	k1gFnSc7	konvence
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lnot	lnot	k2eAgMnSc1d1	lnot
}	}	kIx)	}
se	se	k3xPyFc4	se
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
land	lando	k1gNnPc2	lando
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∨	∨	k?	∨
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lor	lor	k?	lor
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
vyhodnoceny	vyhodnotit	k5eAaPmNgInP	vyhodnotit
následně	následně	k6eAd1	následně
Kvantifikátory	kvantifikátor	k1gInPc1	kvantifikátor
následují	následovat	k5eAaImIp3nP	následovat
ve	v	k7c6	v
vyhodnocení	vyhodnocení	k1gNnSc6	vyhodnocení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vyhodnocena	vyhodnotit	k5eAaPmNgFnS	vyhodnotit
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
přidat	přidat	k5eAaPmF	přidat
extra	extra	k2eAgFnSc4d1	extra
interpunkci	interpunkce	k1gFnSc4	interpunkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
definicí	definice	k1gFnSc7	definice
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
formule	formule	k1gFnSc1	formule
čitelnější	čitelný	k2eAgFnSc1d2	čitelnější
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
formule	formule	k1gFnSc1	formule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
¬	¬	k?	¬
∀	∀	k?	∀
x	x	k?	x
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
→	→	k?	→
∃	∃	k?	∃
x	x	k?	x
¬	¬	k?	¬
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
lnot	lnot	k1gInSc1	lnot
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
xP	xP	k?	xP
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
exists	exists	k6eAd1	exists
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
lnot	lnot	k1gMnSc1	lnot
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
}	}	kIx)	}
:	:	kIx,	:
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přepsána	přepsat	k5eAaPmNgFnS	přepsat
na	na	k7c6	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
¬	¬	k?	¬
[	[	kIx(	[
∀	∀	k?	∀
x	x	k?	x
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
]	]	kIx)	]
)	)	kIx)	)
→	→	k?	→
∃	∃	k?	∃
x	x	k?	x
[	[	kIx(	[
¬	¬	k?	¬
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
]	]	kIx)	]
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
lnot	lnot	k5eAaPmF	lnot
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
xP	xP	k?	xP
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
x	x	k?	x
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
lnot	lnot	k1gInSc1	lnot
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
oborech	obor	k1gInPc6	obor
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
používat	používat	k5eAaImF	používat
infixovou	infixový	k2eAgFnSc4d1	Infixová
notaci	notace	k1gFnSc4	notace
pro	pro	k7c4	pro
binární	binární	k2eAgFnPc4d1	binární
relace	relace	k1gFnPc4	relace
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
namísto	namísto	k7c2	namísto
prefixové	prefixový	k2eAgFnSc2d1	prefixová
notace	notace	k1gFnSc2	notace
(	(	kIx(	(
<g/>
použité	použitý	k2eAgFnSc2d1	použitá
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
typicky	typicky	k6eAd1	typicky
píšeme	psát	k5eAaImIp1nP	psát
"	"	kIx"	"
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
=	=	kIx~	=
4	[number]	k4	4
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
2,2	[number]	k4	2,2
<g/>
)	)	kIx)	)
<g/>
,4	,4	k4	,4
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
považovat	považovat	k5eAaImF	považovat
formule	formule	k1gFnSc2	formule
zapsané	zapsaný	k2eAgFnSc2d1	zapsaná
v	v	k7c6	v
infixové	infixový	k2eAgFnSc6d1	Infixová
notaci	notace	k1gFnSc6	notace
za	za	k7c2	za
zkratky	zkratka	k1gFnSc2	zkratka
pro	pro	k7c4	pro
odpovídající	odpovídající	k2eAgFnPc4d1	odpovídající
formule	formule	k1gFnPc4	formule
v	v	k7c6	v
prefixové	prefixový	k2eAgFnSc6d1	prefixová
notaci	notace	k1gFnSc6	notace
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
představené	představený	k2eAgFnSc2d1	představená
definice	definice	k1gFnSc2	definice
používají	používat	k5eAaImIp3nP	používat
infixovou	infixový	k2eAgFnSc4d1	Infixová
notaci	notace	k1gFnSc4	notace
pro	pro	k7c4	pro
binární	binární	k2eAgFnPc4d1	binární
spojky	spojka	k1gFnPc4	spojka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
používanou	používaný	k2eAgFnSc7d1	používaná
konvencí	konvence	k1gFnSc7	konvence
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Polská	polský	k2eAgFnSc1d1	polská
notace	notace	k1gFnSc1	notace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc2	který
píšeme	psát	k5eAaImIp1nP	psát
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∧	∧	k?	∧
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
wedge	wedge	k1gNnPc3	wedge
}	}	kIx)	}
,	,	kIx,	,
atd.	atd.	kA	atd.
před	před	k7c4	před
jejich	jejich	k3xOp3gInPc4	jejich
argumenty	argument	k1gInPc4	argument
namísto	namísto	k7c2	namísto
mezi	mezi	k7c7	mezi
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
notace	notace	k1gFnSc1	notace
nám	my	k3xPp1nPc3	my
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
odprostit	odprostit	k5eAaPmF	odprostit
se	se	k3xPyFc4	se
od	od	k7c2	od
veškeré	veškerý	k3xTgFnSc2	veškerý
interpunkce	interpunkce	k1gFnSc2	interpunkce
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
notace	notace	k1gFnSc1	notace
je	být	k5eAaImIp3nS	být
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
a	a	k8xC	a
elegantní	elegantní	k2eAgInPc4d1	elegantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřídka	zřídka	k6eAd1	zřídka
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
ke	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polské	polský	k2eAgFnSc6d1	polská
notaci	notace	k1gFnSc6	notace
přepíšeme	přepsat	k5eAaPmIp1nP	přepsat
formuli	formule	k1gFnSc4	formule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
∀	∀	k?	∀
y	y	k?	y
(	(	kIx(	(
P	P	kA	P
(	(	kIx(	(
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
→	→	k?	→
¬	¬	k?	¬
(	(	kIx(	(
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
→	→	k?	→
Q	Q	kA	Q
(	(	kIx(	(
f	f	k?	f
(	(	kIx(	(
y	y	k?	y
)	)	kIx)	)
,	,	kIx,	,
x	x	k?	x
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	foralnout	k5eAaPmAgMnS	foralnout
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
y	y	k?	y
<g/>
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
neg	neg	k?	neg
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Q	Q	kA	Q
<g/>
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)))	)))	k?	)))
<g/>
}	}	kIx)	}
:	:	kIx,	:
na	na	k7c4	na
"	"	kIx"	"
<g/>
∀	∀	k?	∀
<g/>
x	x	k?	x
<g/>
∀	∀	k?	∀
<g/>
y	y	k?	y
<g/>
→	→	k?	→
<g/>
Pfx	Pfx	k1gFnSc2	Pfx
<g/>
¬	¬	k?	¬
<g/>
→	→	k?	→
PxQfyxz	PxQfyxz	k1gInSc1	PxQfyxz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Formule	formule	k1gFnPc1	formule
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
buď	buď	k8xC	buď
volné	volný	k2eAgFnPc4d1	volná
nebo	nebo	k8xC	nebo
vázané	vázaný	k2eAgFnPc4d1	vázaná
proměnné	proměnná	k1gFnPc4	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Intuitivně	intuitivně	k6eAd1	intuitivně
<g/>
,	,	kIx,	,
proměnnou	proměnná	k1gFnSc4	proměnná
nazveme	nazvat	k5eAaPmIp1nP	nazvat
volnou	volný	k2eAgFnSc4d1	volná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
kvantifikována	kvantifikován	k2eAgFnSc1d1	kvantifikována
<g/>
:	:	kIx,	:
ve	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
y	y	k?	y
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proměnná	proměnná	k1gFnSc1	proměnná
x	x	k?	x
volnou	volný	k2eAgFnSc4d1	volná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
y	y	k?	y
je	být	k5eAaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgFnPc1d1	volná
a	a	k8xC	a
vázané	vázaný	k2eAgFnPc1d1	vázaná
proměnné	proměnná	k1gFnPc1	proměnná
formule	formule	k1gFnSc2	formule
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Atomické	atomický	k2eAgFnSc2d1	atomická
formule	formule	k1gFnSc2	formule
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
atomická	atomický	k2eAgFnSc1d1	atomická
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
x	x	k?	x
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
φ	φ	k?	φ
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
x	x	k?	x
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
φ	φ	k?	φ
Dále	daleko	k6eAd2	daleko
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgFnPc4	žádný
vázané	vázaný	k2eAgFnPc4d1	vázaná
proměnné	proměnná	k1gFnPc4	proměnná
v	v	k7c6	v
atomických	atomický	k2eAgFnPc6d1	atomická
formulích	formule	k1gFnPc6	formule
<g/>
.	.	kIx.	.
</s>
<s>
Negace	negace	k1gFnSc1	negace
<g/>
:	:	kIx,	:
x	x	k?	x
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
ve	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
neg	neg	k?	neg
}	}	kIx)	}
φ	φ	k?	φ
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
ve	v	k7c6	v
φ	φ	k?	φ
x	x	k?	x
je	být	k5eAaImIp3nS	být
vázaná	vázané	k1gNnPc4	vázané
ve	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
¬	¬	k?	¬
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
neg	neg	k?	neg
}	}	kIx)	}
φ	φ	k?	φ
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
ve	v	k7c6	v
φ	φ	k?	φ
Binární	binární	k2eAgFnSc2d1	binární
spojky	spojka	k1gFnSc2	spojka
<g/>
:	:	kIx,	:
x	x	k?	x
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
ve	v	k7c6	v
(	(	kIx(	(
<g/>
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
ψ	ψ	k?	ψ
<g/>
)	)	kIx)	)
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s hack="1">
volná	volný	k2eAgFnSc1d1	volná
buď	buď	k8xC	buď
ve	v	k7c6	v
φ	φ	k?	φ
nebo	nebo	k8xC	nebo
ψ	ψ	k?	ψ
x	x	k?	x
je	být	k5eAaImIp3nS	být
vázaná	vázané	k1gNnPc4	vázané
ve	v	k7c4	v
(	(	kIx(	(
<g/>
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
ψ	ψ	k?	ψ
<g/>
)	)	kIx)	)
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
buď	buď	k8xC	buď
ve	v	k7c6	v
φ	φ	k?	φ
nebo	nebo	k8xC	nebo
ψ	ψ	k?	ψ
Stejné	stejný	k2eAgNnSc4d1	stejné
pravidlo	pravidlo	k1gNnSc4	pravidlo
uplatníme	uplatnit	k5eAaPmIp1nP	uplatnit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jiných	jiný	k2eAgFnPc2d1	jiná
binárních	binární	k2eAgFnPc2d1	binární
spojek	spojka	k1gFnPc2	spojka
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Kvantifikátory	kvantifikátor	k1gInPc1	kvantifikátor
<g/>
:	:	kIx,	:
x	x	k?	x
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
ve	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
y	y	k?	y
φ	φ	k?	φ
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
ve	v	k7c6	v
φ	φ	k?	φ
a	a	k8xC	a
x	x	k?	x
je	být	k5eAaImIp3nS	být
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
symbol	symbol	k1gInSc1	symbol
od	od	k7c2	od
y.	y.	k?	y.
Dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
x	x	k?	x
je	být	k5eAaImIp3nS	být
vázaná	vázané	k1gNnPc4	vázané
ve	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
y	y	k?	y
φ	φ	k?	φ
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
x	x	k?	x
je	být	k5eAaImIp3nS	být
y	y	k?	y
nebo	nebo	k8xC	nebo
x	x	k?	x
je	být	k5eAaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
ve	v	k7c4	v
φ	φ	k?	φ
Stejné	stejný	k2eAgNnSc4d1	stejné
pravidlo	pravidlo	k1gNnSc4	pravidlo
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	existsa	k1gFnPc2	existsa
}	}	kIx)	}
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallum	k1gNnPc2	forallum
}	}	kIx)	}
y	y	k?	y
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
}	}	kIx)	}
Q	Q	kA	Q
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
x	x	k?	x
a	a	k8xC	a
y	y	k?	y
jsou	být	k5eAaImIp3nP	být
vázané	vázaný	k2eAgFnPc1d1	vázaná
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
,	,	kIx,	,
z	z	k7c2	z
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
a	a	k8xC	a
w	w	k?	w
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
volná	volný	k2eAgFnSc1d1	volná
ani	ani	k8xC	ani
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Volnost	volnost	k1gFnSc1	volnost
a	a	k8xC	a
vázanost	vázanost	k1gFnSc1	vázanost
lze	lze	k6eAd1	lze
také	také	k9	také
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
specifický	specifický	k2eAgInSc4d1	specifický
výskyt	výskyt	k1gInSc4	výskyt
proměnných	proměnná	k1gFnPc2	proměnná
ve	v	k7c6	v
formuli	formule	k1gFnSc6	formule
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
→	→	k?	→
∀	∀	k?	∀
x	x	k?	x
:	:	kIx,	:
Q	Q	kA	Q
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Q	Q	kA	Q
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
výskyt	výskyt	k1gInSc1	výskyt
x	x	k?	x
volný	volný	k2eAgInSc1d1	volný
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
druhý	druhý	k4xOgInSc1	druhý
vázaný	vázaný	k2eAgMnSc1d1	vázaný
<g/>
.	.	kIx.	.
</s>
<s>
Řečeno	řečen	k2eAgNnSc1d1	řečeno
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
x	x	k?	x
v	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
:	:	kIx,	:
Q	Q	kA	Q
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Q	Q	kA	Q
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
.	.	kIx.	.
</s>
<s>
Formule	formule	k1gFnSc1	formule
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
bez	bez	k7c2	bez
volných	volný	k2eAgFnPc2d1	volná
proměnných	proměnná	k1gFnPc2	proměnná
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sentence	sentence	k1gFnSc1	sentence
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Sentence	sentence	k1gFnPc1	sentence
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
formule	formule	k1gFnPc1	formule
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
dobře	dobře	k6eAd1	dobře
definované	definovaný	k2eAgFnPc1d1	definovaná
pravdivostní	pravdivostní	k2eAgFnPc1d1	pravdivostní
hodnoty	hodnota	k1gFnPc1	hodnota
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
určité	určitý	k2eAgFnSc2d1	určitá
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
např.	např.	kA	např.
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
objekt	objekt	k1gInSc4	objekt
x	x	k?	x
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
sentence	sentence	k1gFnSc1	sentence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
bude	být	k5eAaImBp3nS	být
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
nebo	nebo	k8xC	nebo
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
interpretaci	interpretace	k1gFnSc6	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jazyk	jazyk	k1gInSc1	jazyk
uspořádané	uspořádaný	k2eAgFnSc2d1	uspořádaná
abelovy	abelův	k2eAgFnSc2d1	abelův
grupy	grupa	k1gFnSc2	grupa
jeden	jeden	k4xCgMnSc1	jeden
konstantní	konstantní	k2eAgInSc1d1	konstantní
symbol	symbol	k1gInSc1	symbol
0	[number]	k4	0
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
unární	unární	k2eAgInSc1d1	unární
funkční	funkční	k2eAgInSc1d1	funkční
symbol	symbol	k1gInSc1	symbol
-	-	kIx~	-
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
binární	binární	k2eAgInSc1d1	binární
funkční	funkční	k2eAgInSc1d1	funkční
symbol	symbol	k1gInSc1	symbol
+	+	kIx~	+
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
binární	binární	k2eAgInSc1d1	binární
relační	relační	k2eAgInSc1d1	relační
symbol	symbol	k1gInSc1	symbol
≤	≤	k?	≤
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
Výrazy	výraz	k1gInPc4	výraz
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
)	)	kIx)	)
a	a	k8xC	a
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)))	)))	k?	)))
jsou	být	k5eAaImIp3nP	být
termy	termy	k1gFnPc1	termy
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
jako	jako	k9	jako
'	'	kIx"	'
<g/>
'	'	kIx"	'
<g/>
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
<g/>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
<g/>
'	'	kIx"	'
<g/>
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
-	-	kIx~	-
z	z	k7c2	z
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Výrazy	výraz	k1gInPc1	výraz
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
a	a	k8xC	a
≤	≤	k?	≤
<g/>
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)))	)))	k?	)))
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
))	))	k?	))
jsou	být	k5eAaImIp3nP	být
atomické	atomický	k2eAgFnPc1d1	atomická
formule	formule	k1gFnPc1	formule
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
zápis	zápis	k1gInSc1	zápis
je	být	k5eAaImIp3nS	být
'	'	kIx"	'
<g/>
'	'	kIx"	'
<g/>
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
=	=	kIx~	=
0	[number]	k4	0
<g/>
'	'	kIx"	'
a	a	k8xC	a
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
-	-	kIx~	-
z	z	k7c2	z
≤	≤	k?	≤
x	x	k?	x
+	+	kIx~	+
y.	y.	k?	y.
Výraz	výraz	k1gInSc1	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
x	x	k?	x
∀	∀	k?	∀
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
z	z	k7c2	z
)	)	kIx)	)
→	→	k?	→
∀	∀	k?	∀
x	x	k?	x
:	:	kIx,	:
∀	∀	k?	∀
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathop	mathop	k1gInSc1	mathop
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathop	mathop	k1gInSc1	mathop
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
forall	foralnout	k5eAaPmAgMnS	foralnout
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathop	mathop	k1gInSc1	mathop
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
formule	formule	k1gFnSc1	formule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
∀	∀	k?	∀
y	y	k?	y
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
≤	≤	k?	≤
z	z	k7c2	z
)	)	kIx)	)
→	→	k?	→
∀	∀	k?	∀
x	x	k?	x
∀	∀	k?	∀
y	y	k?	y
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
=	=	kIx~	=
0	[number]	k4	0
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
y	y	k?	y
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
y	y	k?	y
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
vhodně	vhodně	k6eAd1	vhodně
stanovíme	stanovit	k5eAaPmIp1nP	stanovit
axiomy	axiom	k1gInPc1	axiom
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
nám	my	k3xPp1nPc3	my
zavést	zavést	k5eAaPmF	zavést
následující	následující	k2eAgInPc4d1	následující
4	[number]	k4	4
axiomy	axiom	k1gInPc4	axiom
<g/>
:	:	kIx,	:
PRED-	PRED-	k1gFnSc1	PRED-
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
⇒	⇒	k?	⇒
Z	z	k7c2	z
(	(	kIx(	(
y	y	k?	y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
xZ	xZ	k?	xZ
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gFnSc1	Rightarrow
Z	z	k7c2	z
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
PRED-	PRED-	k1gFnSc1	PRED-
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
(	(	kIx(	(
y	y	k?	y
)	)	kIx)	)
⇒	⇒	k?	⇒
∃	∃	k?	∃
x	x	k?	x
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gFnSc1	Rightarrow
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
xZ	xZ	k?	xZ
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
PRED-	PRED-	k1gFnSc1	PRED-
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∀	∀	k?	∀
x	x	k?	x
(	(	kIx(	(
W	W	kA	W
⇒	⇒	k?	⇒
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
→	→	k?	→
(	(	kIx(	(
W	W	kA	W
⇒	⇒	k?	⇒
∀	∀	k?	∀
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
forall	forallit	k5eAaPmRp2nS	forallit
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gFnSc1	Rightarrow
Z	z	k7c2	z
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gMnSc1	Rightarrow
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
xZ	xZ	k?	xZ
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
))	))	k?	))
<g/>
}	}	kIx)	}
:	:	kIx,	:
PRED-	PRED-	k1gFnSc1	PRED-
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∃	∃	k?	∃
x	x	k?	x
(	(	kIx(	(
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
⇒	⇒	k?	⇒
W	W	kA	W
)	)	kIx)	)
⇒	⇒	k?	⇒
(	(	kIx(	(
∃	∃	k?	∃
x	x	k?	x
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
⇒	⇒	k?	⇒
W	W	kA	W
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gMnSc3	Rightarrow
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gFnSc1	Rightarrow
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
xZ	xZ	k?	xZ
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gMnSc3	Rightarrow
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Odvozovací	odvozovací	k2eAgNnSc1d1	odvozovací
pravidlo	pravidlo	k1gNnSc1	pravidlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
formulováno	formulovat	k5eAaImNgNnS	formulovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
o	o	k7c6	o
k	k	k7c3	k
u	u	k7c2	u
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
⊢	⊢	k?	⊢
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
a	a	k8xC	a
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
⊢	⊢	k?	⊢
∀	∀	k?	∀
x	x	k?	x
Z	z	k7c2	z
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathit	mathit	k1gInSc1	mathit
{	{	kIx(	{
<g/>
Pokud	pokud	k8xS	pokud
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
vdash	vdash	k1gInSc1	vdash
<g />
.	.	kIx.	.
</s>
<s hack="1">
Z	z	k7c2	z
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathit	mathit	k5eAaPmF	mathit
{	{	kIx(	{
<g/>
pak	pak	k6eAd1	pak
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
vdash	vdash	k1gInSc1	vdash
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
xZ	xZ	k?	xZ
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
Z	z	k7c2	z
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
značí	značit	k5eAaImIp3nS	značit
pravdivou	pravdivý	k2eAgFnSc4d1	pravdivá
formuli	formule	k1gFnSc4	formule
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
∀	∀	k?	∀
<g/>
xZ	xZ	k?	xZ
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
rozšířením	rozšíření	k1gNnSc7	rozšíření
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
proměnné	proměnná	k1gFnSc3	proměnná
x.	x.	k?	x.
</s>
