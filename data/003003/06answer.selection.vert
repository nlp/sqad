<s>
Syntax	syntax	k1gFnSc1	syntax
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnSc2	jaký
kolekce	kolekce	k1gFnSc2	kolekce
(	(	kIx(	(
<g/>
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
)	)	kIx)	)
symbolů	symbol	k1gInPc2	symbol
jsou	být	k5eAaImIp3nP	být
legální	legální	k2eAgInPc1d1	legální
výrazy	výraz	k1gInPc1	výraz
predikátové	predikátový	k2eAgFnSc2d1	predikátová
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sémantika	sémantika	k1gFnSc1	sémantika
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
významu	význam	k1gInSc6	význam
za	za	k7c7	za
těmito	tento	k3xDgInPc7	tento
výrazy	výraz	k1gInPc7	výraz
<g/>
.	.	kIx.	.
</s>
