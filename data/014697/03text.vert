<s>
1580	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
</s>
<s>
◄	◄	k?
15	#num#	k4
<g/>
.	.	kIx.
st.	st.	kA
•	•	k?
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
•	•	k?
17	#num#	k4
<g/>
.	.	kIx.
st.	st.	kA
►	►	k?
<g/>
◄	◄	k?
◄	◄	k?
1576	#num#	k4
•	•	k?
1577	#num#	k4
•	•	k?
1578	#num#	k4
•	•	k?
1579	#num#	k4
•	•	k?
1580	#num#	k4
•	•	k?
1581	#num#	k4
•	•	k?
1582	#num#	k4
•	•	k?
1583	#num#	k4
•	•	k?
1584	#num#	k4
►	►	k?
►	►	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
.	.	kIx.
<g/>
toc	toc	k?
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
.	.	kIx.
<g/>
toc	toc	k?
.	.	kIx.
<g/>
toctitle	toctitle	k1gFnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
shorttoc	shorttoc	k1gFnSc1
h	h	k?
<g/>
2	#num#	k4
span	spana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
ul	ul	kA
span	span	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
li	li	k8xS
span	span	k1gInSc1
<g/>
{	{	kIx(
<g/>
float	float	k1gInSc1
<g/>
:	:	kIx,
<g/>
left	left	k5eAaPmF
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
.	.	kIx.
<g/>
toc	toc	k?
span	span	k1gInSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
tocnumber	tocnumber	k1gInSc1
<g/>
{	{	kIx(
<g/>
display	display	k1gInPc1
<g/>
:	:	kIx,
<g/>
none	none	k1gFnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gFnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
toctogglespan	toctogglespan	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
h	h	k?
<g/>
2	#num#	k4
span	spana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
ul	ul	kA
span	span	k1gInSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
margin	margin	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.5	.5	k4
<g/>
em	em	k?
0	#num#	k4
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnSc3
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gNnSc3
<g/>
:	:	kIx,
<g/>
none	none	k1gNnSc3
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
li	li	k8xS
<g/>
{	{	kIx(
<g/>
margin-left	margin-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
.5	.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
#	#	kIx~
<g/>
shorttoc	shorttoc	k1gInSc1
td	td	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
.5	.5	k4
<g/>
em	em	k?
0	#num#	k4
.5	.5	k4
<g/>
em	em	k?
.5	.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
</s>
<s>
1580	#num#	k4
(	(	kIx(
<g/>
MDLXXX	MDLXXX	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rok	rok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
dle	dle	k7c2
juliánského	juliánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
započal	započnout	k5eAaPmAgInS
pátkem	pátek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Události	událost	k1gFnPc1
</s>
<s>
V	v	k7c6
Kralicích	Kralice	k1gFnPc6
vyšel	vyjít	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
díl	díl	k1gInSc1
šestisvazkové	šestisvazkový	k2eAgFnSc2d1
Bible	bible	k1gFnSc2
kralické	kralický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Probíhající	probíhající	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
1558	#num#	k4
<g/>
–	–	k?
<g/>
1583	#num#	k4
–	–	k?
Livonská	Livonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
1562	#num#	k4
<g/>
–	–	k?
<g/>
1598	#num#	k4
–	–	k?
Hugenotské	hugenotský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
1568	#num#	k4
<g/>
–	–	k?
<g/>
1648	#num#	k4
–	–	k?
Osmdesátiletá	osmdesátiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
1568	#num#	k4
<g/>
–	–	k?
<g/>
1648	#num#	k4
–	–	k?
Nizozemská	nizozemský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
–	–	k?
Jan	Jan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Harant	Harant	k?
z	z	k7c2
Polžic	Polžice	k1gFnPc2
a	a	k8xC
Bezdružic	Bezdružice	k1gFnPc2
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1648	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
Francisco	Francisco	k1gMnSc1
de	de	k?
Quevedo	Quevedo	k1gNnSc4
<g/>
,	,	kIx,
španělský	španělský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1645	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
–	–	k?
Willebrord	Willebrord	k1gMnSc1
Snellius	Snellius	k1gMnSc1
<g/>
,	,	kIx,
nizozemský	nizozemský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
a	a	k8xC
fyzik	fyzik	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1626	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
Dirck	Dirck	k1gMnSc1
Hartog	Hartog	k1gMnSc1
<g/>
,	,	kIx,
holandský	holandský	k2eAgMnSc1d1
mořeplavec	mořeplavec	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1621	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
–	–	k?
Nicolas-Claude	Nicolas-Claud	k1gMnSc5
Fabri	Fabr	k1gMnSc5
de	de	k?
Peiresc	Peiresc	k1gInSc1
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
astronom	astronom	k1gMnSc1
<g/>
,	,	kIx,
přírodovědec	přírodovědec	k1gMnSc1
a	a	k8xC
sběratel	sběratel	k1gMnSc1
starožitností	starožitnost	k1gFnPc2
(	(	kIx(
<g/>
†	†	k?
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1637	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
John	John	k1gMnSc1
Webster	Webster	k1gMnSc1
<g/>
,	,	kIx,
anglický	anglický	k2eAgMnSc1d1
dramatik	dramatik	k1gMnSc1
<g/>
(	(	kIx(
<g/>
†	†	k?
po	po	k7c6
1625	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Svatý	svatý	k2eAgMnSc1d1
Petr	Petr	k1gMnSc1
Claver	Claver	k1gMnSc1
<g/>
,	,	kIx,
jezuitský	jezuitský	k2eAgMnSc1d1
misionář	misionář	k1gMnSc1
v	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
(	(	kIx(
<g/>
+	+	kIx~
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1654	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Lžidimitrij	Lžidimitrij	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1606	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Andrea	Andrea	k1gFnSc1
Spezza	Spezza	k1gFnSc1
<g/>
,	,	kIx,
italský	italský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1628	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
António	António	k1gMnSc1
de	de	k?
Andrade	Andrad	k1gInSc5
<g/>
,	,	kIx,
portugalský	portugalský	k2eAgMnSc1d1
jezuitský	jezuitský	k2eAgMnSc1d1
misionář	misionář	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1634	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Gabriel	Gabriel	k1gMnSc1
Betlen	Betlen	k2eAgMnSc1d1
<g/>
,	,	kIx,
sedmihradský	sedmihradský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1629	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Jindřich	Jindřich	k1gMnSc1
Duval	Duval	k1gMnSc1
Dampierre	Dampierr	k1gInSc5
<g/>
,	,	kIx,
generál	generál	k1gMnSc1
rakouské	rakouský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
†	†	k?
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1620	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Melchior	Melchior	k1gMnSc1
Franck	Franck	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1639	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Svatý	svatý	k2eAgMnSc1d1
Josafat	Josafat	k1gMnSc1
Kuncevič	Kuncevič	k1gMnSc1
<g/>
,	,	kIx,
arcibiskup	arcibiskup	k1gMnSc1
polocký	polocký	k1gMnSc1
<g/>
,	,	kIx,
světec	světec	k1gMnSc1
Řeckokatolické	řeckokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
(	(	kIx(
<g/>
†	†	k?
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1623	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Petr	Petr	k1gMnSc1
Arnošt	Arnošt	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mansfeld	Mansfeld	k1gMnSc1
<g/>
,	,	kIx,
vojevůdce	vojevůdce	k1gMnSc1
na	na	k7c6
straně	strana	k1gFnSc6
protihabsburského	protihabsburský	k2eAgInSc2d1
odboje	odboj	k1gInSc2
(	(	kIx(
<g/>
†	†	k?
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1626	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Peter	Peter	k1gMnSc1
Minnewitt	Minnewitt	k1gMnSc1
<g/>
,	,	kIx,
guvernér	guvernér	k1gMnSc1
Nového	Nového	k2eAgNnSc2d1
Holandska	Holandsko	k1gNnSc2
(	(	kIx(
<g/>
†	†	k?
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1638	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Martin	Martin	k1gMnSc1
Pansa	Pans	k1gMnSc2
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1626	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Gerhard	Gerhard	k1gMnSc1
z	z	k7c2
Questenberka	Questenberka	k1gFnSc1
<g/>
,	,	kIx,
šlechtic	šlechtic	k1gMnSc1
a	a	k8xC
vysoký	vysoký	k2eAgMnSc1d1
císařský	císařský	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1646	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
<g/>
)	)	kIx)
–	–	k?
Antonín	Antonín	k1gMnSc1
Brus	brus	k1gInSc1
z	z	k7c2
Mohelnice	Mohelnice	k1gFnSc2
<g/>
,	,	kIx,
velmistr	velmistr	k1gMnSc1
Křižovníků	křižovník	k1gMnPc2
s	s	k7c7
červenou	červený	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
biskup	biskup	k1gMnSc1
vídeňský	vídeňský	k2eAgMnSc1d1
a	a	k8xC
arcibiskup	arcibiskup	k1gMnSc1
pražský	pražský	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1518	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
Jiří	Jiří	k1gMnSc1
Melantrich	Melantrich	k1gMnSc1
z	z	k7c2
Aventina	Aventin	k1gMnSc2
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
renesanční	renesanční	k2eAgMnSc1d1
tiskař	tiskař	k1gMnSc1
a	a	k8xC
nakladatel	nakladatel	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1511	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
–	–	k?
Jindřich	Jindřich	k1gMnSc1
I.	I.	kA
Portugalský	portugalský	k2eAgMnSc1d1
<g/>
,	,	kIx,
portugalský	portugalský	k2eAgMnSc1d1
král	král	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1512	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
–	–	k?
Filipína	Filipína	k1gFnSc1
Welserová	Welserový	k2eAgFnSc1d1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
manželka	manželka	k1gFnSc1
arcivévody	arcivévoda	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrolského	tyrolský	k2eAgMnSc4d1
(	(	kIx(
<g/>
*	*	kIx~
1527	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
–	–	k?
Dorotea	Dorotea	k1gFnSc1
Dánská	dánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
dánská	dánský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
<g/>
,	,	kIx,
falcká	falcký	k2eAgFnSc1d1
kurfiřtka	kurfiřtka	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
*	*	kIx~
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1520	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
–	–	k?
Luís	Luís	k1gInSc1
Vaz	vaz	k1gInSc1
de	de	k?
Camõ	Camõ	k1gInSc1
<g/>
,	,	kIx,
portugalský	portugalský	k2eAgMnSc1d1
básník	básník	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1524	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
–	–	k?
Andrea	Andrea	k1gFnSc1
Palladio	Palladio	k1gMnSc1
<g/>
,	,	kIx,
italský	italský	k2eAgMnSc1d1
renesanční	renesanční	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1508	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
–	–	k?
Jerónimo	Jerónima	k1gFnSc5
Osório	Osório	k1gMnSc1
<g/>
,	,	kIx,
portugalský	portugalský	k2eAgMnSc1d1
teolog	teolog	k1gMnSc1
<g/>
,	,	kIx,
humanista	humanista	k1gMnSc1
a	a	k8xC
historik	historik	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1506	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
Jacopo	Jacopa	k1gFnSc5
Strada	strada	k1gFnSc1
<g/>
,	,	kIx,
italský	italský	k2eAgMnSc1d1
učenec	učenec	k1gMnSc1
<g/>
,	,	kIx,
zlatník	zlatník	k1gMnSc1
<g/>
,	,	kIx,
sběratel	sběratel	k1gMnSc1
starožitností	starožitnost	k1gFnPc2
a	a	k8xC
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
numismatiky	numismatika	k1gFnSc2
a	a	k8xC
správce	správce	k1gMnSc2
uměleckých	umělecký	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
Maxmiliána	Maxmilián	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Rudolfa	Rudolf	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
*	*	kIx~
1507	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
Hieronymus	Hieronymus	k1gMnSc1
Wolf	Wolf	k1gMnSc1
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
historik	historik	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1516	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
–	–	k?
Anna	Anna	k1gFnSc1
Habsburská	habsburský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1549	#num#	k4
<g/>
–	–	k?
<g/>
1580	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
španělská	španělský	k2eAgFnSc1d1
<g/>
,	,	kIx,
sicilská	sicilský	k2eAgFnSc1d1
a	a	k8xC
neapolská	neapolský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1570	#num#	k4
do	do	k7c2
1580	#num#	k4
(	(	kIx(
<g/>
*	*	kIx~
1549	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
Jerónimo	Jerónima	k1gFnSc5
Zurita	Zurita	k1gMnSc1
<g/>
,	,	kIx,
španělský	španělský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1512	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
Jakobea	Jakobea	k1gMnSc1
z	z	k7c2
Badenu	Baden	k1gInSc2
<g/>
,	,	kIx,
bavorská	bavorský	k2eAgFnSc1d1
vévodkyně	vévodkyně	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1507	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Juan	Juan	k1gMnSc1
Baptista	baptista	k1gMnSc1
Pastene	Pasten	k1gInSc5
<g/>
,	,	kIx,
janovský	janovský	k2eAgMnSc1d1
mořeplavec	mořeplavec	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1507	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
–	–	k?
Ruy	Ruy	k1gMnSc1
López	López	k1gMnSc1
de	de	k?
Segura	Segura	k1gFnSc1
<g/>
,	,	kIx,
španělský	španělský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
a	a	k8xC
šachový	šachový	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1540	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hlavy	hlava	k1gFnPc1
států	stát	k1gInPc2
</s>
<s>
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
–	–	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Papež	Papež	k1gMnSc1
–	–	k?
Řehoř	Řehoř	k1gMnSc1
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
Alžběta	Alžběta	k1gFnSc1
I.	I.	kA
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
Jindřich	Jindřich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Polské	polský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
Štěpán	Štěpán	k1gMnSc1
Báthory	Báthora	k1gFnSc2
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
–	–	k?
Murad	Murad	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Perská	perský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
–	–	k?
Muhammad	Muhammad	k1gInSc1
Chodábende	Chodábend	k1gInSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
1580	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
