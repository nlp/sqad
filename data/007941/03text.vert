<s>
Bezděz	Bezděz	k1gInSc1	Bezděz
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Bösig	Bösig	k1gInSc1	Bösig
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Velký	velký	k2eAgInSc1d1	velký
Bezděz	Bezděz	k1gInSc1	Bezděz
(	(	kIx(	(
<g/>
603,5	[number]	k4	603,5
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
Dokeské	dokeský	k2eAgFnSc6d1	Dokeská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
vsí	ves	k1gFnSc7	ves
Bezděz	Bezděz	k1gInSc1	Bezděz
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Doksy	Doksy	k1gInPc1	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hrad	hrad	k1gInSc4	hrad
s	s	k7c7	s
obvodovou	obvodový	k2eAgFnSc7d1	obvodová
zástavbou	zástavba	k1gFnSc7	zástavba
(	(	kIx(	(
<g/>
aplikace	aplikace	k1gFnPc1	aplikace
na	na	k7c4	na
úzké	úzký	k2eAgNnSc4d1	úzké
staveniště	staveniště	k1gNnSc4	staveniště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
roku	rok	k1gInSc2	rok
1264	[number]	k4	1264
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
majetkem	majetek	k1gInSc7	majetek
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
správou	správa	k1gFnSc7	správa
je	být	k5eAaImIp3nS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
hradu	hrad	k1gInSc2	hrad
bezprostředně	bezprostředně	k6eAd1	bezprostředně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
kolonizačním	kolonizační	k2eAgNnSc7d1	kolonizační
úsilím	úsilí	k1gNnSc7	úsilí
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgInSc2	jenž
podnětu	podnět	k1gInSc2	podnět
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
nových	nový	k2eAgFnPc2d1	nová
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
také	také	k9	také
tři	tři	k4xCgNnPc4	tři
města	město	k1gNnPc4	město
–	–	k?	–
Bezděz	Bezděz	k1gInSc1	Bezděz
<g/>
,	,	kIx,	,
Doksy	Doksy	k1gInPc1	Doksy
a	a	k8xC	a
Kuřívody	Kuřívod	k1gInPc1	Kuřívod
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
samotný	samotný	k2eAgInSc1d1	samotný
byl	být	k5eAaImAgInS	být
panovníkem	panovník	k1gMnSc7	panovník
určen	určit	k5eAaPmNgInS	určit
jako	jako	k8xC	jako
správní	správní	k2eAgNnSc1d1	správní
centrum	centrum	k1gNnSc1	centrum
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
královského	královský	k2eAgNnSc2d1	královské
panství	panství	k1gNnSc2	panství
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
významné	významný	k2eAgFnPc4d1	významná
obchodní	obchodní	k2eAgFnPc4d1	obchodní
cesty	cesta	k1gFnPc4	cesta
vedoucí	vedoucí	k1gMnPc1	vedoucí
od	od	k7c2	od
Mělníka	Mělník	k1gInSc2	Mělník
okolo	okolo	k7c2	okolo
Bezdězu	Bezděz	k1gInSc2	Bezděz
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
hradu	hrad	k1gInSc2	hrad
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1265	[number]	k4	1265
<g/>
–	–	k?	–
<g/>
1278	[number]	k4	1278
a	a	k8xC	a
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
realizaci	realizace	k1gFnSc6	realizace
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
významná	významný	k2eAgFnSc1d1	významná
královská	královský	k2eAgFnSc1d1	královská
huť	huť	k1gFnSc1	huť
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
stavebním	stavební	k2eAgNnSc7d1	stavební
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
přivezli	přivézt	k5eAaPmAgMnP	přivézt
mniši	mnich	k1gMnPc1	mnich
řádu	řád	k1gInSc2	řád
cisterciáků	cisterciák	k1gMnPc2	cisterciák
z	z	k7c2	z
Burgundska	Burgundsko	k1gNnSc2	Burgundsko
a	a	k8xC	a
severního	severní	k2eAgNnSc2d1	severní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
s	s	k7c7	s
nesmírnou	smírný	k2eNgFnSc7d1	nesmírná
precizností	preciznost	k1gFnSc7	preciznost
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zde	zde	k6eAd1	zde
velice	velice	k6eAd1	velice
působivé	působivý	k2eAgNnSc1d1	působivé
dílo	dílo	k1gNnSc1	dílo
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
rané	raný	k2eAgFnSc2d1	raná
gotiky	gotika	k1gFnSc2	gotika
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
osobu	osoba	k1gFnSc4	osoba
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
i	i	k9	i
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
i	i	k8xC	i
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
Kunhutou	Kunhuta	k1gFnSc7	Kunhuta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
sem	sem	k6eAd1	sem
byli	být	k5eAaImAgMnP	být
přivezeni	přivezen	k2eAgMnPc1d1	přivezen
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1279	[number]	k4	1279
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vpádu	vpád	k1gInSc2	vpád
Braniborů	Branibor	k1gMnPc2	Branibor
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
jimi	on	k3xPp3gInPc7	on
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
střeženi	stříct	k5eAaPmNgMnP	stříct
<g/>
.	.	kIx.	.
</s>
<s>
Kunhutě	Kunhuta	k1gFnSc3	Kunhuta
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
Žitavy	Žitava	k1gFnSc2	Žitava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
strávil	strávit	k5eAaPmAgMnS	strávit
další	další	k2eAgMnSc1d1	další
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c4	na
Bezděz	Bezděz	k1gInSc4	Bezděz
několikrát	několikrát	k6eAd1	několikrát
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
zastavil	zastavit	k5eAaPmAgMnS	zastavit
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
se	se	k3xPyFc4	se
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
hradu	hrad	k1gInSc2	hrad
střídala	střídat	k5eAaImAgFnS	střídat
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
královskou	královský	k2eAgFnSc7d1	královská
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
období	období	k1gNnPc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
zástavě	zástava	k1gFnSc6	zástava
mocných	mocný	k2eAgInPc2d1	mocný
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
jej	on	k3xPp3gMnSc4	on
vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
až	až	k9	až
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1351	[number]	k4	1351
<g/>
,	,	kIx,	,
1352	[number]	k4	1352
<g/>
,	,	kIx,	,
1357	[number]	k4	1357
a	a	k8xC	a
1367	[number]	k4	1367
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgMnSc1d1	Karlův
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
hrad	hrad	k1gInSc1	hrad
dal	dát	k5eAaPmAgInS	dát
opět	opět	k6eAd1	opět
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
vlastníky	vlastník	k1gMnPc7	vlastník
byli	být	k5eAaImAgMnP	být
markrabě	markrabě	k1gMnSc1	markrabě
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
,	,	kIx,	,
Havel	Havel	k1gMnSc1	Havel
ze	z	k7c2	z
Zvířetic	Zvířetice	k1gFnPc2	Zvířetice
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Michalovic	Michalovice	k1gFnPc2	Michalovice
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
dobyt	dobýt	k5eAaPmNgInS	dobýt
a	a	k8xC	a
vypálen	vypálit	k5eAaPmNgInS	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
ho	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
povolal	povolat	k5eAaPmAgInS	povolat
mnichy	mnich	k1gInPc4	mnich
řádu	řád	k1gInSc2	řád
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pozvolného	pozvolný	k2eAgNnSc2d1	pozvolné
budování	budování	k1gNnSc2	budování
kláštera	klášter	k1gInSc2	klášter
hrad	hrad	k1gInSc1	hrad
dobyli	dobýt	k5eAaPmAgMnP	dobýt
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
Švédové	Švédová	k1gFnSc2	Švédová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
řádu	řád	k1gInSc2	řád
montserratských	montserratský	k2eAgMnPc2d1	montserratský
benediktinů	benediktin	k1gMnPc2	benediktin
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInSc1d1	fungující
také	také	k9	také
jako	jako	k9	jako
významné	významný	k2eAgNnSc4d1	významné
mariánské	mariánský	k2eAgNnSc4d1	Mariánské
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1666	[number]	k4	1666
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
hradní	hradní	k2eAgFnSc2d1	hradní
kaple	kaple	k1gFnSc2	kaple
přenesena	přenést	k5eAaPmNgFnS	přenést
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
kopií	kopie	k1gFnPc2	kopie
černé	černý	k2eAgFnSc2d1	černá
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Montserratské	Montserratský	k2eAgFnSc2d1	Montserratský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
dobyt	dobyt	k2eAgInSc1d1	dobyt
pruským	pruský	k2eAgNnSc7d1	pruské
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
konec	konec	k1gInSc1	konec
kláštera	klášter	k1gInSc2	klášter
jeho	jeho	k3xOp3gInSc4	jeho
zánik	zánik	k1gInSc4	zánik
způsobil	způsobit	k5eAaPmAgMnS	způsobit
svým	svůj	k3xOyFgNnSc7	svůj
nařízením	nařízení	k1gNnSc7	nařízení
Josef	Josefa	k1gFnPc2	Josefa
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
kláštera	klášter	k1gInSc2	klášter
byl	být	k5eAaImAgMnS	být
hrad	hrad	k1gInSc4	hrad
opuštěn	opuštěn	k2eAgMnSc1d1	opuštěn
<g/>
,	,	kIx,	,
mniši	mnich	k1gMnPc1	mnich
vybavení	vybavení	k1gNnSc2	vybavení
rozdali	rozdat	k5eAaPmAgMnP	rozdat
po	po	k7c6	po
okolních	okolní	k2eAgInPc6d1	okolní
kostelích	kostel	k1gInPc6	kostel
nebo	nebo	k8xC	nebo
je	on	k3xPp3gMnPc4	on
odvezli	odvézt	k5eAaPmAgMnP	odvézt
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
opatství	opatství	k1gNnSc4	opatství
do	do	k7c2	do
Emauz	Emauzy	k1gInPc2	Emauzy
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Valdštejnů	Valdštejn	k1gInPc2	Valdštejn
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
za	za	k7c4	za
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
částku	částka	k1gFnSc4	částka
do	do	k7c2	do
Klubu	klub	k1gInSc2	klub
československých	československý	k2eAgMnPc2d1	československý
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
předán	předán	k2eAgInSc1d1	předán
státní	státní	k2eAgFnSc3d1	státní
památkové	památkový	k2eAgFnSc3d1	památková
péči	péče	k1gFnSc3	péče
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Okresního	okresní	k2eAgInSc2d1	okresní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Lípě	lípa	k1gFnSc6	lípa
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jenž	k3xRgNnSc4	jenž
náklady	náklad	k1gInPc1	náklad
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
prováděny	prováděn	k2eAgInPc4d1	prováděn
záchranné	záchranný	k2eAgInPc4d1	záchranný
a	a	k8xC	a
konzervační	konzervační	k2eAgFnPc4d1	konzervační
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
provedena	proveden	k2eAgFnSc1d1	provedena
oprava	oprava	k1gFnSc1	oprava
hradní	hradní	k2eAgFnSc2d1	hradní
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
střešní	střešní	k2eAgFnSc2d1	střešní
krytiny	krytina	k1gFnSc2	krytina
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
konzervace	konzervace	k1gFnSc2	konzervace
a	a	k8xC	a
opětovné	opětovný	k2eAgNnSc1d1	opětovné
zpřístupnění	zpřístupnění	k1gNnSc1	zpřístupnění
velké	velký	k2eAgFnSc2d1	velká
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
obnova	obnova	k1gFnSc1	obnova
purkrabského	purkrabský	k2eAgInSc2d1	purkrabský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
v	v	k7c6	v
dílčích	dílčí	k2eAgFnPc6d1	dílčí
etapách	etapa	k1gFnPc6	etapa
probíhají	probíhat	k5eAaImIp3nP	probíhat
konzervační	konzervační	k2eAgFnPc1d1	konzervační
a	a	k8xC	a
restaurátorské	restaurátorský	k2eAgFnPc1d1	restaurátorská
práce	práce	k1gFnPc1	práce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
oprava	oprava	k1gFnSc1	oprava
části	část	k1gFnSc2	část
exteriéru	exteriér	k1gInSc2	exteriér
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hrad	hrad	k1gInSc1	hrad
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
,	,	kIx,	,
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
sklep	sklep	k1gInSc1	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
kolem	kolem	k7c2	kolem
vrchu	vrch	k1gInSc2	vrch
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
v	v	k7c6	v
dostřelu	dostřel	k1gInSc6	dostřel
z	z	k7c2	z
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgInPc1d1	vybudován
dvě	dva	k4xCgFnPc1	dva
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
areálu	areál	k1gInSc2	areál
se	se	k3xPyFc4	se
vcházelo	vcházet	k5eAaImAgNnS	vcházet
třetí	třetí	k4xOgNnSc1	třetí
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k8xC	i
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
branou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
zbraněmi	zbraň	k1gFnPc7	zbraň
nedobytný	dobytný	k2eNgInSc4d1	nedobytný
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
bylo	být	k5eAaImAgNnS	být
zesíleno	zesílit	k5eAaPmNgNnS	zesílit
několika	několik	k4yIc7	několik
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
baštami	bašta	k1gFnPc7	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
obranná	obranný	k2eAgFnSc1d1	obranná
věž	věž	k1gFnSc1	věž
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Čertova	čertův	k2eAgFnSc1d1	Čertova
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přes	přes	k7c4	přes
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
zdí	zdít	k5eAaPmIp3nS	zdít
4	[number]	k4	4
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dominující	dominující	k2eAgFnSc1d1	dominující
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
40	[number]	k4	40
m	m	kA	m
<g/>
,	,	kIx,	,
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
zdí	zdít	k5eAaPmIp3nS	zdít
pět	pět	k4xCc1	pět
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
(	(	kIx(	(
<g/>
Bezděz	Bezděz	k1gInSc1	Bezděz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
staveb	stavba	k1gFnPc2	stavba
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
raně	raně	k6eAd1	raně
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgInSc1d1	vstupní
portál	portál	k1gInSc1	portál
s	s	k7c7	s
tympanonem	tympanon	k1gInSc7	tympanon
je	být	k5eAaImIp3nS	být
ozdoben	ozdobit	k5eAaPmNgInS	ozdobit
rostlinnými	rostlinný	k2eAgInPc7d1	rostlinný
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
zdobených	zdobený	k2eAgInPc2d1	zdobený
sloupů	sloup	k1gInPc2	sloup
a	a	k8xC	a
krytým	krytý	k2eAgInSc7d1	krytý
ochozem	ochoz	k1gInSc7	ochoz
<g/>
.	.	kIx.	.
</s>
<s>
Tribuna	tribuna	k1gFnSc1	tribuna
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
sloužila	sloužit	k5eAaImAgFnS	sloužit
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Stropní	stropní	k2eAgFnSc1d1	stropní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zaklenuta	zaklenout	k5eAaPmNgFnS	zaklenout
křížovými	křížový	k2eAgFnPc7d1	křížová
klenbami	klenba	k1gFnPc7	klenba
bez	bez	k7c2	bez
žeber	žebro	k1gNnPc2	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Kapli	kaple	k1gFnSc3	kaple
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
sedm	sedm	k4xCc1	sedm
mohutných	mohutný	k2eAgNnPc2d1	mohutné
oken	okno	k1gNnPc2	okno
s	s	k7c7	s
lomeným	lomený	k2eAgInSc7d1	lomený
obloukem	oblouk	k1gInSc7	oblouk
a	a	k8xC	a
kružbami	kružba	k1gFnPc7	kružba
umístěných	umístěný	k2eAgInPc2d1	umístěný
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
prvního	první	k4xOgNnSc2	první
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
upravována	upravovat	k5eAaImNgFnS	upravovat
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
neměl	mít	k5eNaImAgInS	mít
svou	svůj	k3xOyFgFnSc4	svůj
studnu	studna	k1gFnSc4	studna
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jímala	jímat	k5eAaImAgFnS	jímat
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
cisterny	cisterna	k1gFnSc2	cisterna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhloubena	vyhloubit	k5eAaPmNgFnS	vyhloubit
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
nádvoří	nádvoří	k1gNnSc6	nádvoří
dolního	dolní	k2eAgInSc2d1	dolní
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dobrou	dobrý	k2eAgFnSc4d1	dobrá
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
a	a	k8xC	a
víno	víno	k1gNnSc1	víno
<g/>
)	)	kIx)	)
v	v	k7c6	v
mírových	mírový	k2eAgFnPc6d1	mírová
dobách	doba	k1gFnPc6	doba
si	se	k3xPyFc3	se
nechávala	nechávat	k5eAaImAgFnS	nechávat
vrchnost	vrchnost	k1gFnSc4	vrchnost
dovážet	dovážet	k5eAaImF	dovážet
z	z	k7c2	z
podhradí	podhradí	k1gNnSc2	podhradí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sloužil	sloužit	k5eAaImAgInS	sloužit
hrad	hrad	k1gInSc1	hrad
jako	jako	k8xS	jako
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
budov	budova	k1gFnPc2	budova
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
pobyt	pobyt	k1gInSc4	pobyt
mnichů	mnich	k1gInPc2	mnich
upravena	upraven	k2eAgFnSc1d1	upravena
a	a	k8xC	a
hraběnka	hraběnka	k1gFnSc1	hraběnka
Anna	Anna	k1gFnSc1	Anna
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
nechala	nechat	k5eAaPmAgFnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1686	[number]	k4	1686
postavit	postavit	k5eAaPmF	postavit
podél	podél	k7c2	podél
přístupové	přístupový	k2eAgFnSc2d1	přístupová
cesty	cesta	k1gFnSc2	cesta
15	[number]	k4	15
kapliček	kaplička	k1gFnPc2	kaplička
Křížové	Křížové	k2eAgFnSc2d1	Křížové
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
poč	poč	k?	poč
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
opevněn	opevněn	k2eAgMnSc1d1	opevněn
i	i	k8xC	i
Malý	malý	k2eAgInSc1d1	malý
Bezděz	Bezděz	k1gInSc1	Bezděz
a	a	k8xC	a
sedlo	sedlo	k1gNnSc1	sedlo
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
vrcholy	vrchol	k1gInPc7	vrchol
valem	valem	k6eAd1	valem
a	a	k8xC	a
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
brána	brána	k1gFnSc1	brána
na	na	k7c6	na
Malém	malý	k2eAgInSc6d1	malý
Bezdězu	Bezděz	k1gInSc6	Bezděz
stála	stát	k5eAaImAgFnS	stát
za	za	k7c7	za
malým	malý	k2eAgInSc7d1	malý
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
torzo	torzo	k1gNnSc1	torzo
kruhové	kruhový	k2eAgFnSc2d1	kruhová
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
obdélné	obdélný	k2eAgFnSc2d1	obdélná
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
terasy	terasa	k1gFnSc2	terasa
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
zdiva	zdivo	k1gNnSc2	zdivo
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Bezděz	Bezděz	k1gInSc1	Bezděz
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
památky	památka	k1gFnPc4	památka
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejlépe	dobře	k6eAd3	dobře
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
hrad	hrad	k1gInSc4	hrad
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
–	–	k?	–
územní	územní	k2eAgNnSc1d1	územní
odborné	odborný	k2eAgNnSc1d1	odborné
pracoviště	pracoviště	k1gNnSc1	pracoviště
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Přístupný	přístupný	k2eAgInSc1d1	přístupný
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc1	areál
původního	původní	k2eAgInSc2d1	původní
horního	horní	k2eAgInSc2d1	horní
hradu	hrad	k1gInSc2	hrad
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
významná	významný	k2eAgFnSc1d1	významná
raně	raně	k6eAd1	raně
gotická	gotický	k2eAgFnSc1d1	gotická
kaple	kaple	k1gFnSc1	kaple
s	s	k7c7	s
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc1d1	starý
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
purkrabství	purkrabství	k1gNnSc1	purkrabství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
hradebních	hradební	k2eAgFnPc2d1	hradební
zdí	zeď	k1gFnPc2	zeď
<g/>
,	,	kIx,	,
Čertova	čertův	k2eAgFnSc1d1	Čertova
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
s	s	k7c7	s
barokními	barokní	k2eAgFnPc7d1	barokní
kapličkami	kaplička	k1gFnPc7	kaplička
podél	podél	k7c2	podél
přístupové	přístupový	k2eAgFnSc2d1	přístupová
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
zpřístupněná	zpřístupněný	k2eAgFnSc1d1	zpřístupněná
jako	jako	k8xS	jako
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Prohlídku	prohlídka	k1gFnSc4	prohlídka
lze	lze	k6eAd1	lze
absolvovat	absolvovat	k5eAaPmF	absolvovat
s	s	k7c7	s
průvodcem	průvodce	k1gMnSc7	průvodce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
samostatně	samostatně	k6eAd1	samostatně
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
konal	konat	k5eAaImAgInS	konat
18	[number]	k4	18
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
akce	akce	k1gFnSc2	akce
Májová	májový	k2eAgFnSc1d1	Májová
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
stal	stát	k5eAaPmAgInS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
romantických	romantický	k2eAgMnPc2d1	romantický
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
hrad	hrad	k1gInSc4	hrad
podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
deníku	deník	k1gInSc2	deník
v	v	k7c6	v
letech	let	k1gInPc6	let
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
šestkrát	šestkrát	k6eAd1	šestkrát
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
lyrizovanou	lyrizovaný	k2eAgFnSc4d1	lyrizovaná
prózu	próza	k1gFnSc4	próza
Večer	večer	k6eAd1	večer
na	na	k7c6	na
Bezdězu	Bezděz	k1gInSc6	Bezděz
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
vyobrazil	vyobrazit	k5eAaPmAgInS	vyobrazit
také	také	k9	také
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
kresbách	kresba	k1gFnPc6	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Elišky	Eliška	k1gFnSc2	Eliška
Krásnohorské	krásnohorský	k2eAgFnSc2d1	Krásnohorská
operu	opera	k1gFnSc4	opera
Tajemství	tajemství	k1gNnSc2	tajemství
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
děj	děj	k1gInSc1	děj
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
řeší	řešit	k5eAaImIp3nS	řešit
údajný	údajný	k2eAgInSc1d1	údajný
poklad	poklad	k1gInSc1	poklad
pod	pod	k7c7	pod
bezdězským	bezdězský	k2eAgInSc7d1	bezdězský
benediktinským	benediktinský	k2eAgInSc7d1	benediktinský
klášterem	klášter	k1gInSc7	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Josef	Josef	k1gMnSc1	Josef
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Bubák	bubák	k1gMnSc1	bubák
<g/>
,	,	kIx,	,
Cína	Cína	k1gMnSc1	Cína
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Ullich	Ullich	k1gMnSc1	Ullich
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
Bezdězy	Bezděz	k1gInPc1	Bezděz
vč.	vč.	k?	vč.
hradu	hrad	k1gInSc2	hrad
byly	být	k5eAaImAgFnP	být
vyhláškou	vyhláška	k1gFnSc7	vyhláška
OkÚ	OkÚ	k1gMnPc2	OkÚ
Česká	český	k2eAgFnSc1d1	Česká
Lípa	lípa	k1gFnSc1	lípa
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1993	[number]	k4	1993
zařazeny	zařazen	k2eAgMnPc4d1	zařazen
mezi	mezi	k7c4	mezi
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
PR	pr	k0	pr
Velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Bezděz	Bezděz	k1gInSc1	Bezděz
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
..	..	k?	..
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
přeřazeny	přeřadit	k5eAaPmNgInP	přeřadit
pod	pod	k7c4	pod
kategorii	kategorie	k1gFnSc4	kategorie
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
