<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
(	(	kIx(	(
<g/>
Perdix	Perdix	k1gInSc1	Perdix
perdix	perdix	k1gInSc1	perdix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
druh	druh	k1gInSc1	druh
hrabavého	hrabavý	k2eAgMnSc2d1	hrabavý
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bažantovitých	bažantovitý	k2eAgFnPc2d1	bažantovitý
(	(	kIx(	(
<g/>
Phasianidae	Phasianidae	k1gFnPc2	Phasianidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
celkem	celek	k1gInSc7	celek
8	[number]	k4	8
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
holandská	holandský	k2eAgFnSc1d1	holandská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
sphagnetorum	sphagnetorum	k1gInSc1	sphagnetorum
<g/>
)	)	kIx)	)
–	–	k?	–
severovýchodní	severovýchodní	k2eAgNnSc1d1	severovýchodní
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
a	a	k8xC	a
severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
francouzská	francouzský	k2eAgFnSc1d1	francouzská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
armoricana	armoricana	k1gFnSc1	armoricana
<g/>
)	)	kIx)	)
–	–	k?	–
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
pyrenejská	pyrenejský	k2eAgFnSc1d1	pyrenejská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
hispaniensis	hispaniensis	k1gInSc1	hispaniensis
<g/>
)	)	kIx)	)
–	–	k?	–
sever	sever	k1gInSc4	sever
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
italská	italský	k2eAgFnSc1d1	italská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
italica	italica	k1gFnSc1	italica
<g/>
)	)	kIx)	)
–	–	k?	–
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
ruská	ruský	k2eAgFnSc1d1	ruská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
lucida	lucida	k1gFnSc1	lucida
<g/>
)	)	kIx)	)
–	–	k?	–
východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
a	a	k8xC	a
severní	severní	k2eAgInSc4d1	severní
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
perdix	perdix	k1gInSc1	perdix
<g/>
)	)	kIx)	)
–	–	k?	–
zbytek	zbytek	k1gInSc1	zbytek
areálu	areál	k1gInSc2	areál
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
včetně	včetně	k7c2	včetně
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
západosibiřská	západosibiřský	k2eAgFnSc1d1	Západosibiřská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
robusta	robusta	k1gFnSc1	robusta
<g/>
)	)	kIx)	)
–	–	k?	–
Ural	Ural	k1gInSc1	Ural
východně	východně	k6eAd1	východně
přes	přes	k7c4	přes
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
po	po	k7c4	po
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
maloasijská	maloasijský	k2eAgFnSc1d1	maloasijská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
p.	p.	k?	p.
canescens	canescens	k1gInSc1	canescens
<g/>
)	)	kIx)	)
–	–	k?	–
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
Írán	Írán	k1gInSc1	Írán
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
350	[number]	k4	350
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
g	g	kA	g
</s>
<s>
O	o	k7c6	o
něco	něco	k6eAd1	něco
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
holub	holub	k1gMnSc1	holub
<g/>
,	,	kIx,	,
zavalitá	zavalitý	k2eAgFnSc1d1	zavalitá
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Tváře	tvář	k1gFnPc1	tvář
a	a	k8xC	a
hrdlo	hrdlo	k1gNnSc1	hrdlo
jsou	být	k5eAaImIp3nP	být
oranžovohnědé	oranžovohnědý	k2eAgInPc1d1	oranžovohnědý
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc4	hruď
jemně	jemně	k6eAd1	jemně
vlnkovaně	vlnkovaně	k6eAd1	vlnkovaně
popelavě	popelavě	k6eAd1	popelavě
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
bělavé	bělavý	k2eAgNnSc1d1	bělavé
s	s	k7c7	s
nápadnou	nápadný	k2eAgFnSc7d1	nápadná
podkovovitou	podkovovitý	k2eAgFnSc7d1	podkovovitá
černavě	černavě	k6eAd1	černavě
hnědavou	hnědavý	k2eAgFnSc7d1	hnědavá
skvrnou	skvrna	k1gFnSc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
sytějším	sytý	k2eAgNnSc7d2	sytější
zbarvením	zbarvení	k1gNnSc7	zbarvení
s	s	k7c7	s
výraznější	výrazný	k2eAgFnSc7d2	výraznější
kresbou	kresba	k1gFnSc7	kresba
a	a	k8xC	a
větší	veliký	k2eAgFnSc7d2	veliký
skvrnou	skvrna	k1gFnSc7	skvrna
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
pták	pták	k1gMnSc1	pták
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc4d1	celý
žlutohnědý	žlutohnědý	k2eAgInSc4d1	žlutohnědý
a	a	k8xC	a
šedohnědý	šedohnědý	k2eAgInSc4d1	šedohnědý
bez	bez	k1gInSc4	bez
skvrny	skvrna	k1gFnSc2	skvrna
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlas	hlas	k1gInSc1	hlas
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vzlétnutí	vzlétnutí	k1gNnSc6	vzlétnutí
vydává	vydávat	k5eAaImIp3nS	vydávat
ostré	ostrý	k2eAgNnSc1d1	ostré
"	"	kIx"	"
<g/>
prri	prri	k1gNnSc1	prri
prri	prr	k1gFnSc2	prr
prri	prr	k1gFnSc2	prr
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
rik	rik	k?	rik
rik	rik	k?	rik
rik	rik	k?	rik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	se	k3xPyFc4	se
často	často	k6eAd1	často
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ozývají	ozývat	k5eAaImIp3nP	ozývat
také	také	k6eAd1	také
drsným	drsný	k2eAgFnPc3d1	drsná
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
opakovaným	opakovaný	k2eAgMnPc3d1	opakovaný
"	"	kIx"	"
<g/>
kierrik	kierrika	k1gFnPc2	kierrika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
evropsko-turkestánský	evropskourkestánský	k2eAgInSc4d1	evropsko-turkestánský
typ	typ	k1gInSc4	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
s	s	k7c7	s
izolovanými	izolovaný	k2eAgFnPc7d1	izolovaná
populacemi	populace	k1gFnPc7	populace
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
introdukována	introdukován	k2eAgFnSc1d1	introdukován
byla	být	k5eAaImAgFnS	být
také	také	k9	také
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
nejpočetněji	početně	k6eAd3	početně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
teplejších	teplý	k2eAgFnPc6d2	teplejší
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místy	místy	k6eAd1	místy
i	i	k9	i
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
k	k	k7c3	k
rapidnímu	rapidní	k2eAgNnSc3d1	rapidní
snížení	snížení	k1gNnSc3	snížení
početnosti	početnost	k1gFnSc2	početnost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
se	se	k3xPyFc4	se
kmenové	kmenový	k2eAgInPc1d1	kmenový
stavy	stav	k1gInPc1	stav
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
odhadovaly	odhadovat	k5eAaImAgFnP	odhadovat
na	na	k7c4	na
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
byly	být	k5eAaImAgInP	být
jarní	jarní	k2eAgInPc1d1	jarní
kmenové	kmenový	k2eAgInPc1d1	kmenový
stavy	stav	k1gInPc1	stav
odhadnuty	odhadnut	k2eAgInPc1d1	odhadnut
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
772	[number]	k4	772
824	[number]	k4	824
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Příčinami	příčina	k1gFnPc7	příčina
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
zemědělském	zemědělský	k2eAgNnSc6d1	zemědělské
hospodaření	hospodaření	k1gNnSc1	hospodaření
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
scelování	scelování	k1gNnSc1	scelování
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
mechanizaci	mechanizace	k1gFnSc6	mechanizace
a	a	k8xC	a
chemizaci	chemizace	k1gFnSc6	chemizace
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
používání	používání	k1gNnSc1	používání
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
stavy	stav	k1gInPc7	stav
opět	opět	k6eAd1	opět
pomalu	pomalu	k6eAd1	pomalu
navyšují	navyšovat	k5eAaImIp3nP	navyšovat
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
tisíc	tisíc	k4xCgInPc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
na	na	k7c4	na
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
tisíc	tisíc	k4xCgInPc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
krajinu	krajina	k1gFnSc4	krajina
s	s	k7c7	s
remízky	remízek	k1gInPc7	remízek
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
úkryty	úkryt	k1gInPc7	úkryt
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
nebo	nebo	k8xC	nebo
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
obdělávaných	obdělávaný	k2eAgFnPc6d1	obdělávaná
polích	pole	k1gFnPc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
monogamně	monogamně	k6eAd1	monogamně
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
za	za	k7c2	za
příznivého	příznivý	k2eAgNnSc2d1	příznivé
počasí	počasí	k1gNnSc2	počasí
začínají	začínat	k5eAaImIp3nP	začínat
tvořit	tvořit	k5eAaImF	tvořit
již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
mělká	mělký	k2eAgFnSc1d1	mělká
jamka	jamka	k1gFnSc1	jamka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
porostu	porost	k1gInSc2	porost
<g/>
,	,	kIx,	,
vystlaná	vystlaný	k2eAgFnSc1d1	vystlaná
suchými	suchý	k2eAgInPc7d1	suchý
stébly	stéblo	k1gNnPc7	stéblo
trav	tráva	k1gFnPc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
snáší	snášet	k5eAaImIp3nP	snášet
v	v	k7c6	v
jednodenních	jednodenní	k2eAgInPc6d1	jednodenní
intervalech	interval	k1gInPc6	interval
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
žlutohnědých	žlutohnědý	k2eAgInPc2d1	žlutohnědý
<g/>
,	,	kIx,	,
žlutoolivových	žlutoolivový	k2eAgInPc2d1	žlutoolivový
<g/>
,	,	kIx,	,
žlutozelených	žlutozelený	k2eAgNnPc2d1	žlutozelené
nebo	nebo	k8xC	nebo
šedozelených	šedozelený	k2eAgNnPc2d1	šedozelené
neposkvrněných	poskvrněný	k2eNgNnPc2d1	neposkvrněné
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
34,99	[number]	k4	34,99
x	x	k?	x
26,46	[number]	k4	26,46
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
až	až	k9	až
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
dnů	den	k1gInPc2	den
po	po	k7c6	po
snesení	snesení	k1gNnSc6	snesení
posledního	poslední	k2eAgNnSc2d1	poslední
vejce	vejce	k1gNnSc2	vejce
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
hlídá	hlídat	k5eAaImIp3nS	hlídat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
během	během	k7c2	během
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
již	již	k6eAd1	již
po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
hodinách	hodina	k1gFnPc6	hodina
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
choulostivá	choulostivý	k2eAgNnPc4d1	choulostivé
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chladného	chladný	k2eAgNnSc2d1	chladné
nebo	nebo	k8xC	nebo
deštivého	deštivý	k2eAgNnSc2d1	deštivé
počasí	počasí	k1gNnSc2	počasí
často	často	k6eAd1	často
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Vodí	vodit	k5eAaImIp3nS	vodit
je	on	k3xPp3gMnPc4	on
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Vzletnosti	vzletnost	k1gFnPc1	vzletnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
16	[number]	k4	16
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
během	během	k7c2	během
hnízdění	hnízdění	k1gNnSc2	hnízdění
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
;	;	kIx,	;
ze	z	k7c2	z
76	[number]	k4	76
hnízd	hnízdo	k1gNnPc2	hnízdo
bylo	být	k5eAaImAgNnS	být
neúspěšných	úspěšný	k2eNgNnPc2d1	neúspěšné
67	[number]	k4	67
(	(	kIx(	(
<g/>
88,15	[number]	k4	88,15
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
predace	predace	k1gFnSc2	predace
(	(	kIx(	(
<g/>
35	[number]	k4	35
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
46,06	[number]	k4	46,06
%	%	kIx~	%
<g/>
;	;	kIx,	;
hlavně	hlavně	k9	hlavně
vránou	vrána	k1gFnSc7	vrána
<g/>
,	,	kIx,	,
ježkem	ježek	k1gMnSc7	ježek
<g/>
,	,	kIx,	,
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
kočkou	kočka	k1gFnSc7	kočka
a	a	k8xC	a
lasicovitými	lasicovitý	k2eAgMnPc7d1	lasicovitý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zničení	zničení	k1gNnSc2	zničení
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
7	[number]	k4	7
vysečením	vysečení	k1gNnPc3	vysečení
<g/>
,	,	kIx,	,
4	[number]	k4	4
úmyslně	úmyslně	k6eAd1	úmyslně
a	a	k8xC	a
7	[number]	k4	7
neúmyslně	úmyslně	k6eNd1	úmyslně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
14	[number]	k4	14
hnízd	hnízdo	k1gNnPc2	hnízdo
(	(	kIx(	(
<g/>
18,42	[number]	k4	18,42
%	%	kIx~	%
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
opouštěno	opouštěn	k2eAgNnSc1d1	opouštěno
z	z	k7c2	z
neznámých	známý	k2eNgFnPc2d1	neznámá
příčin	příčina	k1gFnPc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
známý	známý	k2eAgInSc1d1	známý
věk	věk	k1gInSc1	věk
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Převážně	převážně	k6eAd1	převážně
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
s	s	k7c7	s
doplňkem	doplněk	k1gInSc7	doplněk
živočišné	živočišný	k2eAgFnSc2d1	živočišná
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
zrna	zrno	k1gNnPc1	zrno
obilovin	obilovina	k1gFnPc2	obilovina
a	a	k8xC	a
vegetační	vegetační	k2eAgFnSc2d1	vegetační
části	část	k1gFnSc2	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
mravenci	mravenec	k1gMnPc1	mravenec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
kukly	kukla	k1gFnPc1	kukla
<g/>
,	,	kIx,	,
nosatci	nosatec	k1gMnPc1	nosatec
a	a	k8xC	a
sarančata	saranče	k1gNnPc1	saranče
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
3	[number]	k4	3
týdny	týden	k1gInPc7	týden
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
tvořena	tvořit	k5eAaImNgFnS	tvořit
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
její	její	k3xOp3gInSc1	její
podíl	podíl	k1gInSc1	podíl
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
po	po	k7c6	po
osmi	osm	k4xCc6	osm
týdnech	týden	k1gInPc6	týden
života	život	k1gInSc2	život
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
byla	být	k5eAaImAgFnS	být
koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
až	až	k9	až
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
hlavním	hlavní	k2eAgMnSc7d1	hlavní
lovným	lovný	k2eAgMnSc7d1	lovný
ptákem	pták	k1gMnSc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
bylo	být	k5eAaImAgNnS	být
např.	např.	kA	např.
jen	jen	k9	jen
v	v	k7c6	v
samotných	samotný	k2eAgFnPc6d1	samotná
Čechách	Čechy	k1gFnPc6	Čechy
uloveno	ulovit	k5eAaPmNgNnS	ulovit
1	[number]	k4	1
500	[number]	k4	500
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Československu	Československo	k1gNnSc6	Československo
ročně	ročně	k6eAd1	ročně
průměrně	průměrně	k6eAd1	průměrně
674	[number]	k4	674
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
celkové	celkový	k2eAgInPc1d1	celkový
stavy	stav	k1gInPc1	stav
koroptví	koroptev	k1gFnPc2	koroptev
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
asi	asi	k9	asi
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začalo	začít	k5eAaPmAgNnS	začít
koroptví	koroptví	k2eAgNnSc1d1	koroptví
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
prudce	prudko	k6eAd1	prudko
ubývat	ubývat	k5eAaImF	ubývat
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kvůli	kvůli	k7c3	kvůli
chemizaci	chemizace	k1gFnSc3	chemizace
a	a	k8xC	a
rozorávání	rozorávání	k1gNnSc3	rozorávání
mezí	mez	k1gFnPc2	mez
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poklesu	pokles	k1gInSc6	pokles
stavů	stav	k1gInPc2	stav
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nestřílela	střílet	k5eNaImAgFnS	střílet
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
mezi	mezi	k7c7	mezi
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
provést	provést	k5eAaPmF	provést
odstřel	odstřel	k1gInSc4	odstřel
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bezzel	Bezzet	k5eAaPmAgMnS	Bezzet
<g/>
,	,	kIx,	,
E.	E.	kA	E.
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Rebo	Rebo	k1gNnSc1	Rebo
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7234-292-1	[number]	k4	978-80-7234-292-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Biolib	Biolib	k1gMnSc1	Biolib
</s>
</p>
<p>
<s>
Koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ČSO	ČSO	kA	ČSO
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
ČIŘIKÁNÍ	čiřikání	k1gNnSc2	čiřikání
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
komplexně	komplexně	k6eAd1	komplexně
chránit	chránit	k5eAaImF	chránit
polní	polní	k2eAgNnSc4d1	polní
ptactvo	ptactvo	k1gNnSc4	ptactvo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
koroptve	koroptev	k1gFnSc2	koroptev
polní	polní	k2eAgFnSc2d1	polní
</s>
</p>
<p>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
