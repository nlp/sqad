<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
je	být	k5eAaImIp3nS	být
George	George	k1gFnSc1	George
Lucas	Lucas	k1gInSc1	Lucas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
snímek	snímek	k1gInSc4	snímek
ze	z	k7c2	z
série	série	k1gFnSc2	série
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
tuto	tento	k3xDgFnSc4	tento
společnost	společnost	k1gFnSc4	společnost
založil	založit	k5eAaPmAgMnS	založit
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
speciálních	speciální	k2eAgInPc2d1	speciální
efektů	efekt	k1gInPc2	efekt
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
