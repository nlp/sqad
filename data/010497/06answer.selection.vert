<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Magyarország	Magyarország	k1gInSc1	Magyarország
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
oficiálně	oficiálně	k6eAd1	oficiálně
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Magyar	Magyar	k1gMnSc1	Magyar
Köztársaság	Köztársaság	k1gMnSc1	Köztársaság
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
