<s>
Přerov	Přerov	k1gInSc1	Přerov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Prerau	Preraus	k1gInSc3	Preraus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
21	[number]	k4	21
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Olomouce	Olomouc	k1gFnSc2	Olomouc
v	v	k7c6	v
Hornomoravském	hornomoravský	k2eAgInSc6d1	hornomoravský
úvalu	úval	k1gInSc6	úval
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Bečvě	Bečva	k1gFnSc6	Bečva
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
58,48	[number]	k4	58,48
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
