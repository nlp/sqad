<s>
I	i	k9	i
přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1745	[number]	k4	1745
stal	stát	k5eAaPmAgMnS	stát
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
římskoněmeckým	římskoněmecký	k2eAgMnSc7d1	římskoněmecký
císařem	císař	k1gMnSc7	císař
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
tak	tak	k9	tak
jméno	jméno	k1gNnSc4	jméno
František	František	k1gMnSc1	František
I.	I.	kA	I.
Z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
stává	stávat	k5eAaImIp3nS	stávat
říšské	říšský	k2eAgNnSc1d1	říšské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
