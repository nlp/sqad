<p>
<s>
CD	CD	kA	CD
Projekt	projekt	k1gInSc1	projekt
RED	RED	kA	RED
je	být	k5eAaImIp3nS	být
herní	herní	k2eAgNnSc1d1	herní
vývojářské	vývojářský	k2eAgNnSc1d1	vývojářské
studio	studio	k1gNnSc1	studio
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Založené	založený	k2eAgNnSc4d1	založené
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jako	jako	k8xC	jako
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
CD	CD	kA	CD
Projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
nejznámější	známý	k2eAgFnPc4d3	nejznámější
hry	hra	k1gFnPc4	hra
patří	patřit	k5eAaImIp3nS	patřit
herní	herní	k2eAgFnPc4d1	herní
série	série	k1gFnPc4	série
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Studio	studio	k1gNnSc1	studio
bylo	být	k5eAaImAgNnS	být
založené	založený	k2eAgNnSc1d1	založené
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
Michałem	Michał	k1gInSc7	Michał
Kicińskim	Kicińskima	k1gFnPc2	Kicińskima
a	a	k8xC	a
Marcinem	Marcin	k1gInSc7	Marcin
Iwińskim	Iwińskima	k1gFnPc2	Iwińskima
jako	jako	k8xC	jako
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
CD	CD	kA	CD
Projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vydalo	vydat	k5eAaPmAgNnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hru	hra	k1gFnSc4	hra
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
pevných	pevný	k2eAgInPc6d1	pevný
základech	základ	k1gInPc6	základ
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
Andrzeje	Andrzeje	k1gMnSc2	Andrzeje
Sapkowského	Sapkowský	k1gMnSc2	Sapkowský
se	s	k7c7	s
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
hra	hra	k1gFnSc1	hra
sklidila	sklidit	k5eAaPmAgFnS	sklidit
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
se	se	k3xPyFc4	se
studio	studio	k1gNnSc4	studio
vytvořit	vytvořit	k5eAaPmF	vytvořit
další	další	k2eAgInSc4d1	další
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zaklínač	zaklínač	k1gMnSc1	zaklínač
2	[number]	k4	2
<g/>
:	:	kIx,	:
Vrahové	vrah	k1gMnPc1	vrah
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Wiedźmin	Wiedźmin	k1gInSc1	Wiedźmin
2	[number]	k4	2
<g/>
:	:	kIx,	:
Zabójcy	Zabójca	k1gFnSc2	Zabójca
królów	królów	k?	królów
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c6	na
PC	PC	kA	PC
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
na	na	k7c4	na
Xbox	Xbox	k1gInSc4	Xbox
360	[number]	k4	360
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
CD	CD	kA	CD
Projekt	projekt	k1gInSc1	projekt
RED	RED	kA	RED
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
díky	díky	k7c3	díky
hře	hra	k1gFnSc3	hra
Zaklínač	zaklínač	k1gMnSc1	zaklínač
3	[number]	k4	3
<g/>
:	:	kIx,	:
Divoký	divoký	k2eAgInSc1d1	divoký
hon	hon	k1gInSc1	hon
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Wiedźmin	Wiedźmin	k1gInSc1	Wiedźmin
3	[number]	k4	3
<g/>
:	:	kIx,	:
Dziki	Dzik	k1gFnSc2	Dzik
Gon	Gon	k1gFnSc2	Gon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
na	na	k7c6	na
PC	PC	kA	PC
<g/>
,	,	kIx,	,
PS4	PS4	k1gFnSc4	PS4
a	a	k8xC	a
Xbox	Xbox	k1gInSc4	Xbox
One	One	k1gFnSc2	One
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Hrou	hra	k1gFnSc7	hra
roku	rok	k1gInSc2	rok
na	na	k7c6	na
prestižních	prestižní	k2eAgFnPc6d1	prestižní
herních	herní	k2eAgFnPc6d1	herní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
další	další	k2eAgFnSc1d1	další
videohra	videohra	k1gFnSc1	videohra
s	s	k7c7	s
názvem	název	k1gInSc7	název
Cyberpunk	Cyberpunk	k1gInSc4	Cyberpunk
2077	[number]	k4	2077
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
RPG	RPG	kA	RPG
hře	hra	k1gFnSc3	hra
Cyberpunk	Cyberpunk	k1gInSc1	Cyberpunk
2020	[number]	k4	2020
od	od	k7c2	od
Mikea	Mikeus	k1gMnSc2	Mikeus
Pondsmitha	Pondsmith	k1gMnSc2	Pondsmith
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hry	hra	k1gFnPc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Projekt	projekt	k1gInSc1	projekt
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
CD	CD	kA	CD
Projekt	projekt	k1gInSc4	projekt
RED	RED	kA	RED
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
studia	studio	k1gNnSc2	studio
</s>
</p>
