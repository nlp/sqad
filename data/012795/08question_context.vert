<s>
CD	CD	kA	CD
Projekt	projekt	k1gInSc1	projekt
RED	RED	kA	RED
je	být	k5eAaImIp3nS	být
herní	herní	k2eAgNnSc1d1	herní
vývojářské	vývojářský	k2eAgNnSc1d1	vývojářské
studio	studio	k1gNnSc1	studio
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Založené	založený	k2eAgNnSc4d1	založené
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jako	jako	k8xC	jako
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
CD	CD	kA	CD
Projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
nejznámější	známý	k2eAgFnPc4d3	nejznámější
hry	hra	k1gFnPc4	hra
patří	patřit	k5eAaImIp3nS	patřit
herní	herní	k2eAgFnPc4d1	herní
série	série	k1gFnPc4	série
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Studio	studio	k1gNnSc1	studio
bylo	být	k5eAaImAgNnS	být
založené	založený	k2eAgNnSc1d1	založené
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
Michałem	Michał	k1gInSc7	Michał
Kicińskim	Kicińskima	k1gFnPc2	Kicińskima
a	a	k8xC	a
Marcinem	Marcin	k1gInSc7	Marcin
Iwińskim	Iwińskima	k1gFnPc2	Iwińskima
jako	jako	k8xC	jako
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
CD	CD	kA	CD
Projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>

