<s>
Nevada	Nevada	k1gFnSc1	Nevada
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
nɪ	nɪ	k?	nɪ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
nɪ	nɪ	k?	nɪ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
horských	horský	k2eAgInPc2d1	horský
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nevada	Nevada	k1gFnSc1	Nevada
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Oregonem	Oregon	k1gInSc7	Oregon
a	a	k8xC	a
Idahem	Idah	k1gInSc7	Idah
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Utahem	Utah	k1gInSc7	Utah
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Kalifornií	Kalifornie	k1gFnSc7	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
286	[number]	k4	286
380	[number]	k4	380
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Nevada	Nevada	k1gFnSc1	Nevada
sedmým	sedmý	k4xOgInSc7	sedmý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2,9	[number]	k4	2,9
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
10	[number]	k4	10
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
42	[number]	k4	42
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Carson	Carson	k1gNnSc1	Carson
City	City	k1gFnSc2	City
s	s	k7c7	s
60	[number]	k4	60
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
s	s	k7c7	s
610	[number]	k4	610
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Henderson	Henderson	k1gNnSc1	Henderson
(	(	kIx(	(
<g/>
290	[number]	k4	290
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Reno	Reno	k1gMnSc1	Reno
(	(	kIx(	(
<g/>
240	[number]	k4	240
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
North	North	k1gMnSc1	North
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
(	(	kIx(	(
<g/>
240	[number]	k4	240
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sparks	Sparks	k1gInSc1	Sparks
(	(	kIx(	(
<g/>
90	[number]	k4	90
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Boundary	Boundara	k1gFnSc2	Boundara
Peak	Peaka	k1gFnPc2	Peaka
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
4007	[number]	k4	4007
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
White	Whit	k1gInSc5	Whit
Mountains	Mountains	k1gInSc1	Mountains
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
<g/>
,	,	kIx,	,
a	a	k8xC	a
Humboldt	Humboldt	k1gInSc1	Humboldt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Nevady	Nevada	k1gFnSc2	Nevada
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
španělský	španělský	k2eAgMnSc1d1	španělský
františkán	františkán	k1gMnSc1	františkán
Francisco	Francisco	k1gMnSc1	Francisco
Garcés	Garcés	k1gInSc4	Garcés
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Horní	horní	k2eAgFnSc2d1	horní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
připadla	připadnout	k5eAaPmAgFnS	připadnout
samostatnému	samostatný	k2eAgNnSc3d1	samostatné
Mexiku	Mexiko	k1gNnSc3	Mexiko
<g/>
,	,	kIx,	,
od	od	k7c2	od
nějž	jenž	k3xRgInSc2	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
mexicko-americké	mexickomerický	k2eAgFnSc2d1	mexicko-americká
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgInP	získat
oblast	oblast	k1gFnSc4	oblast
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
prozkoumáno	prozkoumat	k5eAaPmNgNnS	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgInS	být
region	region	k1gInSc1	region
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
utažského	utažský	k2eAgNnSc2d1	utažský
teritoria	teritorium	k1gNnSc2	teritorium
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
první	první	k4xOgNnPc4	první
stálá	stálý	k2eAgNnPc4d1	stálé
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
vlastní	vlastní	k2eAgNnSc1d1	vlastní
nevadské	nevadský	k2eAgNnSc1d1	nevadské
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
své	svůj	k3xOyFgNnSc1	svůj
jméno	jméno	k1gNnSc1	jméno
získalo	získat	k5eAaPmAgNnS	získat
odvozením	odvození	k1gNnSc7	odvození
z	z	k7c2	z
původně	původně	k6eAd1	původně
španělského	španělský	k2eAgInSc2d1	španělský
názvu	název	k1gInSc2	název
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zasněžené	zasněžený	k2eAgFnPc1d1	zasněžená
hory	hora	k1gFnPc1	hora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevada	Nevada	k1gFnSc1	Nevada
se	s	k7c7	s
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1864	[number]	k4	1864
stala	stát	k5eAaPmAgFnS	stát
36	[number]	k4	36
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
španělského	španělský	k2eAgNnSc2d1	španělské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
nevada	nevada	k1gFnSc1	nevada
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
zasněžená	zasněžený	k2eAgFnSc1d1	zasněžená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pohoří	pohoří	k1gNnSc2	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zasněžené	zasněžený	k2eAgFnPc1d1	zasněžená
hory	hora	k1gFnPc1	hora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevada	Nevada	k1gFnSc1	Nevada
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
španělské	španělský	k2eAgFnSc2d1	španělská
provincie	provincie	k1gFnSc2	provincie
Horní	horní	k2eAgFnSc2d1	horní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mexicko-americké	mexickomerický	k2eAgFnSc6d1	mexicko-americká
válce	válka	k1gFnSc6	válka
území	území	k1gNnSc2	území
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
anektovali	anektovat	k5eAaBmAgMnP	anektovat
Američané	Američan	k1gMnPc1	Američan
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
je	on	k3xPp3gNnPc4	on
začlenili	začlenit	k5eAaPmAgMnP	začlenit
do	do	k7c2	do
teritoria	teritorium	k1gNnSc2	teritorium
Utah	Utah	k1gInSc1	Utah
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
území	území	k1gNnSc4	území
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
37	[number]	k4	37
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
začlenili	začlenit	k5eAaPmAgMnP	začlenit
do	do	k7c2	do
teritoria	teritorium	k1gNnSc2	teritorium
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
objeveno	objeven	k2eAgNnSc1d1	objeveno
velké	velký	k2eAgNnSc1d1	velké
ložisko	ložisko	k1gNnSc1	ložisko
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přilákalo	přilákat	k5eAaPmAgNnS	přilákat
mnoho	mnoho	k4c1	mnoho
zlatokopů	zlatokop	k1gMnPc2	zlatokop
toužících	toužící	k2eAgInPc2d1	toužící
po	po	k7c6	po
rychlém	rychlý	k2eAgNnSc6d1	rychlé
zbohatnutí	zbohatnutí	k1gNnSc6	zbohatnutí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
byla	být	k5eAaImAgFnS	být
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
teritoria	teritorium	k1gNnPc4	teritorium
oddělena	oddělen	k2eAgNnPc4d1	odděleno
jako	jako	k8xS	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
teritorium	teritorium	k1gNnSc4	teritorium
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1862	[number]	k4	1862
posunula	posunout	k5eAaPmAgFnS	posunout
o	o	k7c4	o
85	[number]	k4	85
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
teritoria	teritorium	k1gNnSc2	teritorium
Utah	Utah	k1gInSc1	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
se	se	k3xPyFc4	se
teritorium	teritorium	k1gNnSc1	teritorium
stalo	stát	k5eAaPmAgNnS	stát
samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
zatím	zatím	k6eAd1	zatím
neměl	mít	k5eNaImAgMnS	mít
současné	současný	k2eAgFnPc4d1	současná
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
poněkud	poněkud	k6eAd1	poněkud
menší	malý	k2eAgMnSc1d2	menší
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1866	[number]	k4	1866
se	se	k3xPyFc4	se
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
Nevady	Nevada	k1gFnSc2	Nevada
posunula	posunout	k5eAaPmAgFnS	posunout
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
85	[number]	k4	85
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
na	na	k7c4	na
moderní	moderní	k2eAgFnPc4d1	moderní
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1867	[number]	k4	1867
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
Nevady	Nevada	k1gFnSc2	Nevada
stal	stát	k5eAaPmAgInS	stát
cíp	cíp	k1gInSc1	cíp
území	území	k1gNnSc4	území
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
37	[number]	k4	37
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
dosud	dosud	k6eAd1	dosud
součástí	součást	k1gFnSc7	součást
teritoria	teritorium	k1gNnSc2	teritorium
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
ke	k	k7c3	k
114	[number]	k4	114
<g/>
°	°	k?	°
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
území	území	k1gNnSc2	území
Nevady	Nevada	k1gFnSc2	Nevada
získalo	získat	k5eAaPmAgNnS	získat
současný	současný	k2eAgInSc4d1	současný
rozsah	rozsah	k1gInSc4	rozsah
i	i	k8xC	i
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odezvě	odezva	k1gFnSc6	odezva
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
Nevada	Nevada	k1gFnSc1	Nevada
legalizovala	legalizovat	k5eAaBmAgFnS	legalizovat
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1931	[number]	k4	1931
hazard	hazard	k1gInSc1	hazard
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
město	město	k1gNnSc4	město
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Reno	Reno	k6eAd1	Reno
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
rájem	ráj	k1gInSc7	ráj
hazardu	hazard	k1gInSc2	hazard
<g/>
,	,	kIx,	,
kýče	kýč	k1gInSc2	kýč
a	a	k8xC	a
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nevada	Nevada	k1gFnSc1	Nevada
na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nP	sousedit
se	se	k3xPyFc4	se
státy	stát	k1gInPc1	stát
Oregon	Oregona	k1gFnPc2	Oregona
a	a	k8xC	a
Idaho	Ida	k1gMnSc2	Ida
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Kalifornií	Kalifornie	k1gFnSc7	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Utahem	Utah	k1gInSc7	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Colorado	Colorado	k1gNnSc4	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
mnoho	mnoho	k4c1	mnoho
krajinných	krajinný	k2eAgInPc2d1	krajinný
rázů	ráz	k1gInPc2	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
hornaté	hornatý	k2eAgFnPc4d1	hornatá
zasněžené	zasněžený	k2eAgFnPc4d1	zasněžená
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc4	pastvina
i	i	k8xC	i
vyprahlé	vyprahlý	k2eAgFnPc4d1	vyprahlá
pouště	poušť	k1gFnPc4	poušť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejsušší	suchý	k2eAgFnPc4d3	nejsušší
oblasti	oblast	k1gFnPc4	oblast
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
Nevady	Nevada	k1gFnPc1	Nevada
činí	činit	k5eAaImIp3nP	činit
69	[number]	k4	69
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
32	[number]	k4	32
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
529	[number]	k4	529
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgInPc1d1	hlavní
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
produkty	produkt	k1gInPc1	produkt
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc4	seno
<g/>
,	,	kIx,	,
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
brambory	brambor	k1gInPc4	brambor
hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
nerostů	nerost	k1gInPc2	nerost
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc3	zpracování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
příjmy	příjem	k1gInPc1	příjem
Nevady	Nevada	k1gFnSc2	Nevada
plynou	plynout	k5eAaImIp3nP	plynout
z	z	k7c2	z
hazardních	hazardní	k2eAgFnPc2d1	hazardní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
do	do	k7c2	do
Nevady	Nevada	k1gFnSc2	Nevada
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
návštěvníky	návštěvník	k1gMnPc4	návštěvník
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
700	[number]	k4	700
551	[number]	k4	551
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km	km	kA	km
čtvereční	čtvereční	k2eAgFnSc1d1	čtvereční
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Carson	Carson	k1gInSc4	Carson
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
52	[number]	k4	52
457	[number]	k4	457
ob.	ob.	k?	ob.
<g/>
)	)	kIx)	)
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
(	(	kIx(	(
<g/>
593	[number]	k4	593
922	[number]	k4	922
<g/>
)	)	kIx)	)
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
Reno	Reno	k6eAd1	Reno
(	(	kIx(	(
<g/>
180	[number]	k4	180
480	[number]	k4	480
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Henderson	Henderson	k1gMnSc1	Henderson
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
175	[number]	k4	175
381	[number]	k4	381
<g/>
)	)	kIx)	)
66,2	[number]	k4	66,2
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
8,1	[number]	k4	8,1
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,2	[number]	k4	1,2
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
7,2	[number]	k4	7,2
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,6	[number]	k4	0,6
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
12,0	[number]	k4	12,0
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,7	[number]	k4	4,7
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
26,5	[number]	k4	26,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
kvůli	kvůli	k7c3	kvůli
pouštnímu	pouštní	k2eAgInSc3d1	pouštní
charakteru	charakter	k1gInSc3	charakter
státu	stát	k1gInSc2	stát
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
významné	významný	k2eAgNnSc1d1	významné
<g/>
.	.	kIx.	.
</s>
<s>
Nevadou	Nevada	k1gFnSc7	Nevada
probíhají	probíhat	k5eAaImIp3nP	probíhat
dvě	dva	k4xCgFnPc4	dva
dálnice	dálnice	k1gFnPc4	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
na	na	k7c6	na
severu	sever	k1gInSc6	sever
území	území	k1gNnSc2	území
přes	přes	k7c4	přes
Reno	Reno	k1gNnSc4	Reno
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
do	do	k7c2	do
Utahu	Utah	k1gInSc2	Utah
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
z	z	k7c2	z
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
do	do	k7c2	do
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Arizony	Arizona	k1gFnSc2	Arizona
a	a	k8xC	a
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
prakticky	prakticky	k6eAd1	prakticky
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
to	ten	k3xDgNnSc1	ten
dálniční	dálniční	k2eAgNnSc1d1	dálniční
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc4	Vegas
a	a	k8xC	a
v	v	k7c4	v
Renu	Rena	k1gFnSc4	Rena
jsou	být	k5eAaImIp3nP	být
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
měst	město	k1gNnPc2	město
hazardu	hazard	k1gInSc2	hazard
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gMnSc1	Vegas
a	a	k8xC	a
Reno	Reno	k1gMnSc1	Reno
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
pozoruhodná	pozoruhodný	k2eAgNnPc4d1	pozoruhodné
místa	místo	k1gNnPc4	místo
Hooverova	Hooverův	k2eAgFnSc1d1	Hooverova
přehrada	přehrada	k1gFnSc1	přehrada
(	(	kIx(	(
<g/>
Hoover	Hoover	k1gInSc1	Hoover
Dam	dáma	k1gFnPc2	dáma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Colorado	Colorado	k1gNnSc4	Colorado
v	v	k7c4	v
Grand	grand	k1gMnSc1	grand
Canyonu	Canyon	k1gInSc2	Canyon
<g/>
.	.	kIx.	.
</s>
<s>
Zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
vodou	voda	k1gFnSc7	voda
celou	celý	k2eAgFnSc4d1	celá
Nevadu	Nevada	k1gFnSc4	Nevada
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
část	část	k1gFnSc4	část
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
prezidentovi	prezident	k1gMnSc6	prezident
Herbertu	Herbert	k1gMnSc6	Herbert
Hooverovi	Hoover	k1gMnSc6	Hoover
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc1d3	veliký
přehrada	přehrada	k1gFnSc1	přehrada
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
mnohé	mnohý	k2eAgFnPc4d1	mnohá
indiánské	indiánský	k2eAgFnPc4d1	indiánská
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
.	.	kIx.	.
</s>
