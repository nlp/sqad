<p>
<s>
Vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Hirundo	Hirundo	k1gNnSc1	Hirundo
rustica	rustic	k1gInSc2	rustic
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
vlaštovkovitých	vlaštovkovitý	k2eAgMnPc2d1	vlaštovkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
vypadající	vypadající	k2eAgMnPc1d1	vypadající
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
považováni	považován	k2eAgMnPc1d1	považován
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
příbuzní	příbuzný	k1gMnPc1	příbuzný
jiřička	jiřička	k1gFnSc1	jiřička
obecná	obecná	k1gFnSc1	obecná
a	a	k8xC	a
břehule	břehule	k1gFnSc1	břehule
říční	říční	k2eAgFnSc1d1	říční
a	a	k8xC	a
nepříbuzný	příbuzný	k2eNgMnSc1d1	nepříbuzný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podobný	podobný	k2eAgMnSc1d1	podobný
rorýs	rorýs	k1gMnSc1	rorýs
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
19	[number]	k4	19
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
20	[number]	k4	20
g	g	kA	g
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
cm	cm	kA	cm
</s>
<s>
Vlaštovka	Vlaštovka	k1gFnSc1	Vlaštovka
obecná	obecná	k1gFnSc1	obecná
má	mít	k5eAaImIp3nS	mít
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
vidlicovitý	vidlicovitý	k2eAgInSc4d1	vidlicovitý
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
špičatá	špičatý	k2eAgNnPc4d1	špičaté
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Čelo	čelo	k1gNnSc1	čelo
<g/>
,	,	kIx,	,
brada	brada	k1gFnSc1	brada
a	a	k8xC	a
hrdlo	hrdlo	k1gNnSc1	hrdlo
je	být	k5eAaImIp3nS	být
rezavě	rezavě	k6eAd1	rezavě
hnědé	hnědý	k2eAgNnSc1d1	hnědé
<g/>
,	,	kIx,	,
svrchní	svrchní	k2eAgFnSc1d1	svrchní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
jednolitě	jednolitě	k6eAd1	jednolitě
modročerně	modročerně	k6eAd1	modročerně
lesklá	lesklý	k2eAgFnSc1d1	lesklá
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
hruď	hruď	k1gFnSc1	hruď
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
a	a	k8xC	a
spodní	spodní	k2eAgFnPc1d1	spodní
krovky	krovka	k1gFnPc1	krovka
ocasní	ocasní	k2eAgFnPc1d1	ocasní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ocasní	ocasní	k2eAgNnPc1d1	ocasní
pera	pero	k1gNnPc1	pero
mají	mít	k5eAaImIp3nP	mít
zespodu	zespodu	k6eAd1	zespodu
řadu	řada	k1gFnSc4	řada
bílých	bílý	k2eAgFnPc2d1	bílá
skvrn	skvrna	k1gFnPc2	skvrna
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
od	od	k7c2	od
samice	samice	k1gFnSc2	samice
liší	lišit	k5eAaImIp3nS	lišit
delšími	dlouhý	k2eAgNnPc7d2	delší
ocasními	ocasní	k2eAgNnPc7d1	ocasní
pery	pero	k1gNnPc7	pero
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
mají	mít	k5eAaImIp3nP	mít
mátově	mátově	k6eAd1	mátově
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
hrdlo	hrdlo	k1gNnSc4	hrdlo
i	i	k8xC	i
svrchní	svrchní	k2eAgFnSc4d1	svrchní
stranu	strana	k1gFnSc4	strana
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
méně	málo	k6eAd2	málo
vykrojený	vykrojený	k2eAgInSc4d1	vykrojený
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlas	hlas	k1gInSc1	hlas
==	==	k?	==
</s>
</p>
<p>
<s>
Vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecná	k1gFnSc1	obecná
je	být	k5eAaImIp3nS	být
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
typické	typický	k2eAgNnSc1d1	typické
štěbetání	štěbetání	k1gNnSc1	štěbetání
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
tóninách	tónina	k1gFnPc6	tónina
přerušované	přerušovaný	k2eAgNnSc1d1	přerušované
cvrlikáním	cvrlikání	k1gNnSc7	cvrlikání
<g/>
.	.	kIx.	.
</s>
<s>
Vábení	vábení	k1gNnSc1	vábení
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
vittvitt	vittvitt	k5eAaPmF	vittvitt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
létajícím	létající	k2eAgInSc7d1	létající
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chytají	chytat	k5eAaImIp3nP	chytat
v	v	k7c6	v
letu	let	k1gInSc6	let
nebo	nebo	k8xC	nebo
sbírají	sbírat	k5eAaImIp3nP	sbírat
z	z	k7c2	z
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
za	za	k7c2	za
letu	let	k1gInSc2	let
dokonce	dokonce	k9	dokonce
i	i	k9	i
pijí	pít	k5eAaImIp3nP	pít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
dlouhotrvajících	dlouhotrvající	k2eAgInPc2d1	dlouhotrvající
dešťů	dešť	k1gInPc2	dešť
trpí	trpět	k5eAaImIp3nS	trpět
hlady	hlady	k6eAd1	hlady
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hmyz	hmyz	k1gInSc1	hmyz
nelétá	létat	k5eNaImIp3nS	létat
a	a	k8xC	a
ony	onen	k3xDgInPc1	onen
si	se	k3xPyFc3	se
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
v	v	k7c6	v
letu	let	k1gInSc6	let
potravu	potrava	k1gFnSc4	potrava
obstarat	obstarat	k5eAaPmF	obstarat
neumějí	umět	k5eNaImIp3nP	umět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odlétají	odlétat	k5eAaPmIp3nP	odlétat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
zpátky	zpátky	k6eAd1	zpátky
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
synantropní	synantropní	k2eAgNnSc1d1	synantropní
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
obydlených	obydlený	k2eAgFnPc6d1	obydlená
kulturních	kulturní	k2eAgFnPc6d1	kulturní
krajinách	krajina	k1gFnPc6	krajina
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
uvnitř	uvnitř	k7c2	uvnitř
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
objektech	objekt	k1gInPc6	objekt
s	s	k7c7	s
chovy	chov	k1gInPc1	chov
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chlévy	chlév	k1gInPc1	chlév
–	–	k?	–
kravíny	kravín	k1gInPc1	kravín
<g/>
,	,	kIx,	,
vepříny	vepřín	k1gInPc1	vepřín
<g/>
)	)	kIx)	)
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
i	i	k9	i
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
,	,	kIx,	,
stavy	stav	k1gInPc1	stav
vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
vlaštovkám	vlaštovka	k1gFnPc3	vlaštovka
trvá	trvat	k5eAaImIp3nS	trvat
2	[number]	k4	2
týdny	týden	k1gInPc7	týden
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
miskovité	miskovitý	k2eAgNnSc1d1	miskovité
z	z	k7c2	z
hliněných	hliněný	k2eAgInPc2d1	hliněný
<g/>
,	,	kIx,	,
slinami	slina	k1gFnPc7	slina
slepených	slepený	k2eAgFnPc2d1	slepená
hrudek	hrudka	k1gFnPc2	hrudka
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
,	,	kIx,	,
vystlané	vystlaný	k2eAgFnPc1d1	vystlaná
stébly	stéblo	k1gNnPc7	stéblo
a	a	k8xC	a
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
bílých	bílý	k2eAgFnPc2d1	bílá
<g/>
,	,	kIx,	,
červenohnědě	červenohnědě	k6eAd1	červenohnědě
skvrnitých	skvrnitý	k2eAgNnPc2d1	skvrnité
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
sama	sám	k3xTgMnSc4	sám
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
příletu	přílet	k1gInSc6	přílet
rodič	rodič	k1gMnSc1	rodič
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
potravu	potrava	k1gFnSc4	potrava
jednomu	jeden	k4xCgNnSc3	jeden
mláděti	mládě	k1gNnSc3	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc1	hnízdo
po	po	k7c6	po
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
dalších	další	k2eAgInPc2d1	další
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
dní	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
krmí	krmit	k5eAaImIp3nS	krmit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
vracejí	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potom	potom	k6eAd1	potom
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zahnízdění	zahnízdění	k1gNnSc3	zahnízdění
<g/>
,	,	kIx,	,
odháněna	odhánět	k5eAaImNgFnS	odhánět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
shazování	shazování	k1gNnSc2	shazování
jejich	jejich	k3xOp3gNnPc2	jejich
hnízd	hnízdo	k1gNnPc2	hnízdo
pokutováno	pokutovat	k5eAaImNgNnS	pokutovat
<g/>
.	.	kIx.	.
</s>
<s>
Nesmějí	smát	k5eNaImIp3nP	smát
se	se	k3xPyFc4	se
shazovat	shazovat	k5eAaImF	shazovat
ani	ani	k8xC	ani
použitá	použitý	k2eAgNnPc4d1	Použité
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
mohou	moct	k5eAaImIp3nP	moct
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
zase	zase	k9	zase
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
interiéru	interiér	k1gInSc2	interiér
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
provést	provést	k5eAaPmF	provést
preventivně	preventivně	k6eAd1	preventivně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sítí	sítí	k1gNnSc4	sítí
v	v	k7c6	v
okně	okno	k1gNnSc6	okno
<g/>
)	)	kIx)	)
před	před	k7c7	před
započetím	započetí	k1gNnSc7	započetí
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
hnízdění	hnízdění	k1gNnSc4	hnízdění
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
ani	ani	k8xC	ani
dalšímu	další	k2eAgNnSc3d1	další
hnízdění	hnízdění	k1gNnSc3	hnízdění
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
předchozího	předchozí	k2eAgInSc2d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Zraněnou	zraněný	k2eAgFnSc4d1	zraněná
vlaštovku	vlaštovka	k1gFnSc4	vlaštovka
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
bezodkladně	bezodkladně	k6eAd1	bezodkladně
předat	předat	k5eAaPmF	předat
k	k	k7c3	k
ošetření	ošetření	k1gNnSc3	ošetření
do	do	k7c2	do
záchranné	záchranný	k2eAgFnSc2d1	záchranná
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
je	být	k5eAaImIp3nS	být
zakázán	zakázán	k2eAgInSc1d1	zakázán
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Predátoři	predátor	k1gMnPc1	predátor
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
loví	lovit	k5eAaImIp3nP	lovit
převážně	převážně	k6eAd1	převážně
ostříž	ostříž	k1gMnSc1	ostříž
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
ostříž	ostříž	k1gMnSc1	ostříž
jižní	jižní	k2eAgMnSc1d1	jižní
a	a	k8xC	a
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
loví	lovit	k5eAaImIp3nS	lovit
zase	zase	k9	zase
marabu	marabu	k1gMnSc1	marabu
africký	africký	k2eAgMnSc1d1	africký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zimovištích	zimoviště	k1gNnPc6	zimoviště
je	on	k3xPp3gFnPc4	on
loví	lovit	k5eAaImIp3nP	lovit
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
je	on	k3xPp3gFnPc4	on
lidé	člověk	k1gMnPc1	člověk
loví	lovit	k5eAaImIp3nP	lovit
nelegálně	legálně	k6eNd1	legálně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lov	lov	k1gInSc1	lov
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
EU	EU	kA	EU
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jsou	být	k5eAaImIp3nP	být
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
špatným	špatný	k2eAgNnSc7d1	špatné
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
chladný	chladný	k2eAgInSc1d1	chladný
a	a	k8xC	a
deštivý	deštivý	k2eAgInSc1d1	deštivý
podzim	podzim	k1gInSc1	podzim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
uhynuly	uhynout	k5eAaPmAgInP	uhynout
hlady	hlady	k6eAd1	hlady
tisíce	tisíc	k4xCgInPc1	tisíc
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
mytologie	mytologie	k1gFnSc1	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slované	Slovan	k1gMnPc1	Slovan
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c7	za
posly	posel	k1gMnPc7	posel
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
posmrtné	posmrtný	k2eAgNnSc4d1	posmrtné
zosobnění	zosobnění	k1gNnSc4	zosobnění
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posvátné	posvátný	k2eAgFnSc6d1	posvátná
soše	socha	k1gFnSc6	socha
boha	bůh	k1gMnSc2	bůh
Rujevíta	Rujevít	k1gMnSc2	Rujevít
v	v	k7c6	v
městě	město	k1gNnSc6	město
Korenica	Korenic	k1gInSc2	Korenic
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgInSc6d1	dnešní
Garzu	Garz	k1gInSc6	Garz
<g/>
)	)	kIx)	)
na	na	k7c6	na
Rujáně	Rujána	k1gFnSc6	Rujána
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
hnízdo	hnízdo	k1gNnSc4	hnízdo
pod	pod	k7c7	pod
ústy	ústa	k1gNnPc7	ústa
sochy	socha	k1gFnSc2	socha
zpodobněného	zpodobněný	k2eAgNnSc2d1	zpodobněné
božstva	božstvo	k1gNnSc2	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nedotknutelné	nedotknutelné	k?	nedotknutelné
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc4	jejich
hnízda	hnízdo	k1gNnPc4	hnízdo
i	i	k8xC	i
trus	trus	k1gInSc4	trus
byly	být	k5eAaImAgFnP	být
tabu	tabu	k2eAgFnPc1d1	tabu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
-	-	kIx~	-
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
JÜRGEN	JÜRGEN	kA	JÜRGEN
<g/>
,	,	kIx,	,
Nikolai	Nikolai	k1gNnSc1	Nikolai
<g/>
;	;	kIx,	;
SINGER	SINGER	kA	SINGER
<g/>
,	,	kIx,	,
Detlef	Detlef	k1gInSc1	Detlef
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
:	:	kIx,	:
praktická	praktický	k2eAgFnSc1d1	praktická
příručka	příručka	k1gFnSc1	příručka
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
našich	náš	k3xOp1gMnPc2	náš
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7209	[number]	k4	7209
<g/>
-	-	kIx~	-
<g/>
685	[number]	k4	685
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
170	[number]	k4	170
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šťastný	Šťastný	k1gMnSc1	Šťastný
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
;	;	kIx,	;
Drchal	drchat	k5eAaImAgInS	drchat
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Naši	náš	k3xOp1gMnPc1	náš
pěvci	pěvec	k1gMnPc1	pěvec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
174	[number]	k4	174
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecný	k2eAgFnSc1d1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecná	k1gFnSc1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Hirundo	Hirundo	k6eAd1	Hirundo
rustica	rustic	k2eAgFnSc1d1	rustic
(	(	kIx(	(
<g/>
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecná	k1gFnSc1	obecná
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
obecná	obecná	k1gFnSc1	obecná
-	-	kIx~	-
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
</s>
</p>
<p>
<s>
Hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
NP	NP	kA	NP
Hortobágy	Hortobága	k1gFnPc1	Hortobága
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
29.4	[number]	k4	29.4
<g/>
.2019	.2019	k4	.2019
</s>
</p>
