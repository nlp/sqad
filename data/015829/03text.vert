<s>
Zdeněk	Zdeněk	k1gMnSc1
Jánoš	Jánoš	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
JánošOsobní	JánošOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1967	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1999	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
32	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
brankář	brankář	k1gMnSc1
Mládežnické	mládežnický	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
</s>
<s>
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
19871987	#num#	k4
<g/>
–	–	k?
<g/>
19881988	#num#	k4
<g/>
–	–	k?
<g/>
19941994	#num#	k4
<g/>
–	–	k?
<g/>
19991999	#num#	k4
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
VTJ	VTJ	kA
Tábor	Tábor	k1gInSc1
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
Jablonec	Jablonec	k1gInSc1
Dukla	Dukla	k1gFnSc1
Příbram	Příbram	k1gFnSc1
<g/>
0	#num#	k4
<g/>
1300	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
13800	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
14000	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
600	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Děti	dítě	k1gFnPc1
</s>
<s>
Adam	Adam	k1gMnSc1
Jánoš	Jánoš	k1gMnSc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Jánoš	Jánoš	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1967	#num#	k4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1999	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
při	při	k7c6
dopravní	dopravní	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Začínal	začínat	k5eAaImAgInS
v	v	k7c6
Kněžpoli	Kněžpole	k1gFnSc6
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
16	#num#	k4
letech	léto	k1gNnPc6
přešel	přejít	k5eAaPmAgMnS
do	do	k7c2
Slovácké	slovácký	k2eAgFnSc2d1
Slavie	slavie	k1gFnSc2
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
absolvoval	absolvovat	k5eAaPmAgMnS
v	v	k7c6
klubech	klub	k1gInPc6
VTJ	VTJ	kA
Tábor	Tábor	k1gInSc1
a	a	k8xC
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
jejím	její	k3xOp3gNnSc6
skončení	skončení	k1gNnSc6
působil	působit	k5eAaImAgMnS
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Slavii	slavie	k1gFnSc6
<g/>
,	,	kIx,
Jablonci	Jablonec	k1gInSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
a	a	k8xC
v	v	k7c6
příbramské	příbramský	k2eAgFnSc6d1
Dukle	Dukla	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odehrál	odehrát	k5eAaPmAgMnS
celkem	celkem	k6eAd1
284	#num#	k4
prvoligových	prvoligový	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
3	#num#	k4
góly	gól	k1gInPc7
z	z	k7c2
penalt	penalta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
vstřelil	vstřelit	k5eAaPmAgMnS
ve	v	k7c6
28	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1994	#num#	k4
<g/>
)	)	kIx)
ročníku	ročník	k1gInSc2
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
za	za	k7c4
Slavii	slavie	k1gFnSc4
Praha	Praha	k1gFnSc1
proti	proti	k7c3
Vítkovicím	Vítkovice	k1gInPc3
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zbývající	zbývající	k2eAgMnPc1d1
dvě	dva	k4xCgFnPc4
pak	pak	k6eAd1
za	za	k7c4
FK	FK	kA
Jablonec	Jablonec	k1gInSc4
rodnému	rodný	k2eAgNnSc3d1
Uherskému	uherský	k2eAgNnSc3d1
Hradišti	Hradiště	k1gNnSc3
v	v	k7c6
ročníku	ročník	k1gInSc6
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skóroval	skórovat	k5eAaBmAgMnS
jak	jak	k6eAd1
při	při	k7c6
domácí	domácí	k2eAgFnSc6d1
výhře	výhra	k1gFnSc6
Jablonce	Jablonec	k1gInSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1996	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
při	při	k7c6
totožné	totožný	k2eAgFnSc6d1
výhře	výhra	k1gFnSc6
Jablonce	Jablonec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
Klubu	klub	k1gInSc2
ligových	ligový	k2eAgMnPc2d1
brankářů	brankář	k1gMnPc2
se	s	k7c7
105	#num#	k4
odehranými	odehraný	k2eAgInPc7d1
ligovými	ligový	k2eAgInPc7d1
zápasy	zápas	k1gInPc7
bez	bez	k7c2
obdržené	obdržený	k2eAgFnSc2d1
branky	branka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Minuty	minuta	k1gFnPc1
</s>
<s>
Nuly	nula	k1gFnPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
080	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
IPS	IPS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
610	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
IPS	IPS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
980	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
IPS	IPS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
8	#num#	k4
</s>
<s>
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
14	#num#	k4
</s>
<s>
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
610	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
610	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
601	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
419	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
430	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
97	#num#	k4
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
430	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
97	#num#	k4
</s>
<s>
I.	I.	kA
liga	liga	k1gFnSc1
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
540	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
FC	FC	kA
Dukla	Dukla	k1gFnSc1
Příbram	Příbram	k1gFnSc1
</s>
<s>
CELKEM	celkem	k6eAd1
I.	I.	kA
LIGA	liga	k1gFnSc1
</s>
<s>
284	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
105	#num#	k4
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
rodiny	rodina	k1gFnSc2
se	s	k7c7
3	#num#	k4
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
tatínek	tatínek	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
byly	být	k5eAaImAgInP
3	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc3
mamince	maminka	k1gFnSc3
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
29	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
manželkou	manželka	k1gFnSc7
Evou	Eva	k1gFnSc7
měl	mít	k5eAaImAgInS
2	#num#	k4
syny	syn	k1gMnPc7
<g/>
,	,	kIx,
Dominika	Dominik	k1gMnSc4
a	a	k8xC
Adama	Adam	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Adam	Adam	k1gMnSc1
též	též	k9
hraje	hrát	k5eAaImIp3nS
fotbal	fotbal	k1gInSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
stříbrného	stříbrný	k2eAgInSc2d1
českého	český	k2eAgInSc2d1
týmu	tým	k1gInSc2
na	na	k7c6
ME	ME	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Záliba	záliba	k1gFnSc1
v	v	k7c6
automobilech	automobil	k1gInPc6
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
zálibou	záliba	k1gFnSc7
byla	být	k5eAaImAgFnS
rychlá	rychlý	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
automobilem	automobil	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
stala	stát	k5eAaPmAgFnS
osudnou	osudný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středu	střed	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1999	#num#	k4
se	se	k3xPyFc4
automobilem	automobil	k1gInSc7
Volkswagen	volkswagen	k1gInSc1
Passat	Passat	k1gFnSc2
nepřipoutaný	připoutaný	k2eNgMnSc1d1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
vracel	vracet	k5eAaImAgInS
z	z	k7c2
tréninku	trénink	k1gInSc2
klubu	klub	k1gInSc2
Dukla	Dukla	k1gFnSc1
Příbram	Příbram	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
pražskými	pražský	k2eAgFnPc7d1
částmi	část	k1gFnPc7
Jižní	jižní	k2eAgFnSc2d1
Město	město	k1gNnSc1
a	a	k8xC
Petrovice	Petrovice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
bydliště	bydliště	k1gNnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
narazil	narazit	k5eAaPmAgMnS
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
150	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
hod	hod	k1gInSc1
do	do	k7c2
protijedoucího	protijedoucí	k2eAgInSc2d1
městského	městský	k2eAgInSc2d1
autobusu	autobus	k1gInSc2
linky	linka	k1gFnSc2
číslo	číslo	k1gNnSc1
271	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byl	být	k5eAaImAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
mrtev	mrtev	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Údajně	údajně	k6eAd1
mu	on	k3xPp3gNnSc3
tehdy	tehdy	k6eAd1
při	při	k7c6
řízení	řízení	k1gNnSc6
spadl	spadnout	k5eAaPmAgInS
mobilní	mobilní	k2eAgInSc1d1
telefon	telefon	k1gInSc1
pod	pod	k7c4
volant	volant	k1gInSc4
a	a	k8xC
on	on	k3xPp3gMnSc1
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
shýbal	shýbat	k5eAaImAgInS
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Memoriál	memoriál	k1gInSc1
Zdeňka	Zdeněk	k1gMnSc2
Jánoše	Jánoš	k1gMnSc2
</s>
<s>
Na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
a	a	k8xC
památku	památka	k1gFnSc4
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
koná	konat	k5eAaImIp3nS
Memoriál	memoriál	k1gInSc1
Zdeňka	Zdeněk	k1gMnSc2
Jánoše	Jánoš	k1gMnSc2
v	v	k7c6
halové	halový	k2eAgFnSc6d1
kopané	kopaná	k1gFnSc6
internacionálů	internacionál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ŠUBRT	Šubrt	k1gMnSc1
<g/>
,	,	kIx,
Richi	Rich	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anketa	anketa	k1gFnSc1
-	-	kIx~
zvolte	zvolit	k5eAaPmRp2nP
nejlepšího	dobrý	k2eAgMnSc4d3
brankáře	brankář	k1gMnSc4
Slavie	slavie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slávistické	slávistický	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-02-15	2005-02-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EISNER	EISNER	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
fotbaloví	fotbalový	k2eAgMnPc1d1
průšviháři	průšvihář	k1gMnPc1
<g/>
:	:	kIx,
od	od	k7c2
Jana	Jan	k1gMnSc2
Bergera	Berger	k1gMnSc2
k	k	k7c3
Bednářovi	Bednář	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-05-21	2009-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jánošovo	Jánošův	k2eAgNnSc1d1
snění	snění	k1gNnPc1
o	o	k7c6
Spartě	Sparta	k1gFnSc6
a	a	k8xC
reprezentaci	reprezentace	k1gFnSc6
krutě	krutě	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-09-17	1999-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jánoš	Jánoš	k1gMnSc1
<g/>
,	,	kIx,
Kouba	Kouba	k1gMnSc1
<g/>
,	,	kIx,
Černý	Černý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
dál	daleko	k6eAd2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
fotbalzive	fotbalziev	k1gFnSc2
<g/>
.	.	kIx.
<g/>
ct	ct	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
WILKOVÁ	WILKOVÁ	kA
<g/>
,	,	kIx,
Scarlett	Scarlett	k1gMnSc1
<g/>
;	;	kIx,
PACEK	packa	k1gFnPc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehody	nehoda	k1gFnSc2
slavných	slavný	k2eAgFnPc2d1
<g/>
:	:	kIx,
Život	život	k1gInSc1
naruby	naruby	k6eAd1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-07-24	2002-07-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ŠŤASTNA	šťasten	k2eAgMnSc4d1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgMnPc1d3
čeští	český	k2eAgMnPc1d1
fotbaloví	fotbalový	k2eAgMnPc1d1
„	„	k?
<g/>
rváči	rváč	k1gMnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-09-30	2008-09-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
8991	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Hattrick	hattrick	k1gInSc1
<g/>
:	:	kIx,
Pozor	pozor	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c7
volantem	volant	k1gInSc7
fotbalista	fotbalista	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.hattrick.cz	www.hattrick.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
řízená	řízený	k2eAgFnSc1d1
střela	střela	k1gFnSc1
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
o	o	k7c6
Jánošově	Jánošův	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
řidič	řidič	k1gMnSc1
autobusu	autobus	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999-09-21	1999-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc1
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
Jenšík	Jenšík	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
fotbalu	fotbal	k1gInSc2
ve	v	k7c6
Starém	starý	k2eAgNnSc6d1
Městě	město	k1gNnSc6
a	a	k8xC
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
–	–	k?
Dáme	dát	k5eAaPmIp1nP
góla	góla	k6eAd1
<g/>
,	,	kIx,
dáme	dát	k5eAaPmIp1nP
<g/>
...	...	k?
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
FK	FK	kA
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
občanské	občanský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
<g/>
;	;	kIx,
[[	[[	k?
<g/>
International	International	k1gFnSc1
Standard	standard	k1gInSc1
Book	Book	k1gMnSc1
Number	Number	k1gMnSc1
<g/>
|	|	kIx~
<g/>
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
9259	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
]]	]]	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Jánoš	Jánoš	k1gMnSc1
<g/>
,	,	kIx,
talent	talent	k1gInSc1
od	od	k7c2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
miloval	milovat	k5eAaImAgInS
rychlou	rychlý	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
k	k	k7c3
smrti	smrt	k1gFnSc3
<g/>
…	…	k?
<g/>
,	,	kIx,
fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Záznam	záznam	k1gInSc1
prvního	první	k4xOgInSc2
Jánošova	Jánošův	k2eAgInSc2d1
gólu	gól	k1gInSc2
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1994	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
YouTube	YouTub	k1gInSc5
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Klub	klub	k1gInSc1
ligových	ligový	k2eAgInPc2d1
brankářů	brankář	k1gMnPc2
</s>
<s>
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Blažek	Blažek	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Stejskal	Stejskal	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Vaniak	Vaniak	k1gMnSc1
•	•	k?
Luděk	Luděk	k1gMnSc1
Mikloško	Mikloška	k1gFnSc5
•	•	k?
Dušan	Dušan	k1gMnSc1
Keketi	Keket	k1gMnPc1
•	•	k?
Alexander	Alexandra	k1gFnPc2
Vencel	Vencel	k1gMnSc1
•	•	k?
Anton	Anton	k1gMnSc1
Švajlen	Švajlen	k2eAgMnSc1d1
•	•	k?
Jiří	Jiří	k1gMnSc1
Sedláček	Sedláček	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Kinský	Kinský	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Laštůvka	laštůvka	k1gFnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Drobný	drobný	k2eAgMnSc1d1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Maier	Maier	k1gMnSc1
•	•	k?
Viliam	Viliam	k1gMnSc1
Schrojf	Schrojf	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Poštulka	Poštulka	k1gFnSc1
•	•	k?
Luboš	Luboš	k1gMnSc1
Přibyl	Přibyl	k1gMnSc1
•	•	k?
Ivo	Ivo	k1gMnSc1
Viktor	Viktor	k1gMnSc1
•	•	k?
Matúš	Matúš	k1gMnSc1
Kozáčik	Kozáčik	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Jánoš	Jánoš	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Grigar	Grigar	k1gMnSc1
•	•	k?
Pavol	Pavol	k1gInSc1
Michalík	Michalík	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Špit	špit	k1gInSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Vaclík	Vaclík	k1gMnSc1
*	*	kIx~
aktuální	aktuální	k2eAgInSc1d1
k	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
=	=	kIx~
23	#num#	k4
branářů	branář	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
