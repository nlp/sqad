<s>
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Academy	Academ	k1gInPc4	Academ
Award	Awardo	k1gNnPc2	Awardo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Oscar	Oscar	k1gInSc1	Oscar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
každoročně	každoročně	k6eAd1	každoročně
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
americkou	americký	k2eAgFnSc7d1	americká
Akademií	akademie	k1gFnSc7	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
