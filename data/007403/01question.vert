<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
uděluje	udělovat	k5eAaImIp3nS	udělovat
americká	americký	k2eAgFnSc1d1	americká
Akademie	akademie	k1gFnSc1	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
<g/>
?	?	kIx.	?
</s>
