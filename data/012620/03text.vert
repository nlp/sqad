<p>
<s>
Brýle	brýle	k1gFnPc4	brýle
jsou	být	k5eAaImIp3nP	být
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brýle	brýle	k1gFnPc1	brýle
obvykle	obvykle	k6eAd1	obvykle
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
pevné	pevný	k2eAgFnSc2d1	pevná
obruby	obruba	k1gFnSc2	obruba
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
jsou	být	k5eAaImIp3nP	být
upevněny	upevnit	k5eAaPmNgFnP	upevnit
buď	buď	k8xC	buď
čočky	čočka	k1gFnPc1	čočka
umístěné	umístěný	k2eAgFnPc1d1	umístěná
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
člověka	člověk	k1gMnSc2	člověk
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgFnSc4d1	vizuální
korekci	korekce	k1gFnSc4	korekce
(	(	kIx(	(
<g/>
dioptrické	dioptrický	k2eAgFnPc4d1	dioptrická
brýle	brýle	k1gFnPc4	brýle
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
průhledné	průhledný	k2eAgFnPc4d1	průhledná
zornice	zornice	k1gFnPc4	zornice
pro	pro	k7c4	pro
oční	oční	k2eAgFnSc4d1	oční
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
brýle	brýle	k1gFnPc1	brýle
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
posazeny	posazen	k2eAgFnPc1d1	posazena
plastovými	plastový	k2eAgNnPc7d1	plastové
sedýlky	sedýlko	k1gNnPc7	sedýlko
na	na	k7c6	na
nosu	nos	k1gInSc6	nos
a	a	k8xC	a
stranicemi	stranice	k1gFnPc7	stranice
s	s	k7c7	s
obvykle	obvykle	k6eAd1	obvykle
plastovými	plastový	k2eAgFnPc7d1	plastová
koncovkami	koncovka	k1gFnPc7	koncovka
za	za	k7c4	za
boltce	boltec	k1gInPc4	boltec
uší	ucho	k1gNnPc2	ucho
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
kopírovat	kopírovat	k5eAaImF	kopírovat
<g/>
.	.	kIx.	.
</s>
<s>
Dioptrické	dioptrický	k2eAgFnPc1d1	dioptrická
brýle	brýle	k1gFnPc1	brýle
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
krátkozrakosti	krátkozrakost	k1gFnSc2	krátkozrakost
nebo	nebo	k8xC	nebo
dalekozrakosti	dalekozrakost	k1gFnSc2	dalekozrakost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
korekcí	korekce	k1gFnSc7	korekce
vad	vada	k1gFnPc2	vada
oka	oko	k1gNnSc2	oko
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
astigmatismus	astigmatismus	k1gInSc4	astigmatismus
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
korekce	korekce	k1gFnSc2	korekce
vidění	vidění	k1gNnSc2	vidění
čočkami	čočka	k1gFnPc7	čočka
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
optické	optický	k2eAgFnSc2d1	optická
mohutnosti	mohutnost	k1gFnSc2	mohutnost
v	v	k7c6	v
dioptriích	dioptrie	k1gFnPc6	dioptrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dioptrické	dioptrický	k2eAgFnPc4d1	dioptrická
brýle	brýle	k1gFnPc4	brýle
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
dioptrických	dioptrický	k2eAgFnPc2d1	dioptrická
brýlí	brýle	k1gFnPc2	brýle
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
čočky	čočka	k1gFnPc1	čočka
na	na	k7c6	na
optickém	optický	k2eAgInSc6d1	optický
základě	základ	k1gInSc6	základ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
rozptylné	rozptylný	k2eAgNnSc1d1	rozptylné
-	-	kIx~	-
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
krátkozrakosti	krátkozrakost	k1gFnSc2	krátkozrakost
</s>
</p>
<p>
<s>
spojné	spojný	k2eAgNnSc1d1	spojný
-	-	kIx~	-
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
dalekozrakosti	dalekozrakost	k1gFnSc2	dalekozrakost
</s>
</p>
<p>
<s>
cylindrické	cylindrický	k2eAgNnSc1d1	cylindrické
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
torické	torický	k2eAgInPc1d1	torický
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
úroveň	úroveň	k1gFnSc4	úroveň
korekce	korekce	k1gFnSc2	korekce
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
rovinách	rovina	k1gFnPc6	rovina
<g/>
;	;	kIx,	;
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
spojné	spojný	k2eAgFnPc1d1	spojná
<g/>
,	,	kIx,	,
rozptylné	rozptylný	k2eAgFnPc1d1	rozptylná
i	i	k8xC	i
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
<g/>
)	)	kIx)	)
-	-	kIx~	-
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
astigmatismu	astigmatismus	k1gInSc2	astigmatismus
</s>
</p>
<p>
<s>
bifokální	bifokální	k2eAgMnPc1d1	bifokální
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
lámavost	lámavost	k1gFnSc4	lámavost
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
skla	sklo	k1gNnSc2	sklo
<g/>
;	;	kIx,	;
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
spojné	spojný	k2eAgFnPc1d1	spojná
<g/>
,	,	kIx,	,
rozptylné	rozptylný	k2eAgFnPc1d1	rozptylná
i	i	k8xC	i
kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgInSc1d1	dolní
segment	segment	k1gInSc1	segment
mívá	mívat	k5eAaImIp3nS	mívat
každopádně	každopádně	k6eAd1	každopádně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
plusovou	plusový	k2eAgFnSc4d1	plusová
dioptrickou	dioptrický	k2eAgFnSc4d1	dioptrická
sílu	síla	k1gFnSc4	síla
<g/>
)	)	kIx)	)
-	-	kIx~	-
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
vady	vada	k1gFnSc2	vada
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
s	s	k7c7	s
presbyopií	presbyopie	k1gFnSc7	presbyopie
nebo	nebo	k8xC	nebo
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnSc6d1	silná
vadě	vada	k1gFnSc6	vada
</s>
</p>
<p>
<s>
trifokální	trifokálnit	k5eAaPmIp3nP	trifokálnit
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
bifokální	bifokální	k2eAgNnSc1d1	bifokální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
přidán	přidat	k5eAaPmNgInS	přidat
ještě	ještě	k9	ještě
třetí	třetí	k4xOgInSc1	třetí
segment	segment	k1gInSc1	segment
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
pro	pro	k7c4	pro
kompenzaci	kompenzace	k1gFnSc4	kompenzace
afakie	afakie	k1gFnSc2	afakie
</s>
</p>
<p>
<s>
multifokální	multifokálnit	k5eAaPmIp3nS	multifokálnit
(	(	kIx(	(
<g/>
podobný	podobný	k2eAgInSc1d1	podobný
princip	princip	k1gInSc1	princip
jako	jako	k8xS	jako
bi-	bi-	k?	bi-
a	a	k8xC	a
trifokální	trifokálnit	k5eAaPmIp3nS	trifokálnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
změna	změna	k1gFnSc1	změna
lámavosti	lámavost	k1gFnSc2	lámavost
skla	sklo	k1gNnSc2	sklo
je	být	k5eAaImIp3nS	být
plynulá	plynulý	k2eAgFnSc1d1	plynulá
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
Většina	většina	k1gFnSc1	většina
brýlových	brýlový	k2eAgFnPc2d1	brýlová
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
konkávkonvexní	konkávkonvexní	k2eAgFnSc7d1	konkávkonvexní
nebo	nebo	k8xC	nebo
konvexkonkávní	konvexkonkávní	k2eAgFnSc7d1	konvexkonkávní
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
i	i	k8xC	i
zadní	zadní	k2eAgFnSc2d1	zadní
strany	strana	k1gFnSc2	strana
jsou	být	k5eAaImIp3nP	být
prohnuté	prohnutý	k2eAgMnPc4d1	prohnutý
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
ven	ven	k6eAd1	ven
od	od	k7c2	od
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgNnPc1d1	silné
skla	sklo	k1gNnPc1	sklo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
z	z	k7c2	z
některé	některý	k3yIgFnSc2	některý
strany	strana	k1gFnSc2	strana
rovná	rovnat	k5eAaImIp3nS	rovnat
(	(	kIx(	(
<g/>
plano	plano	k?	plano
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
i	i	k9	i
bikonkávní	bikonkávní	k2eAgMnSc1d1	bikonkávní
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bikonvexní	bikonvexní	k2eAgFnPc4d1	bikonvexní
<g/>
.	.	kIx.	.
</s>
<s>
Extrémně	extrémně	k6eAd1	extrémně
silné	silný	k2eAgFnPc1d1	silná
brýlové	brýlový	k2eAgFnPc1d1	brýlová
čočky	čočka	k1gFnPc1	čočka
často	často	k6eAd1	často
nevyplňují	vyplňovat	k5eNaImIp3nP	vyplňovat
celou	celý	k2eAgFnSc4d1	celá
obroučku	obroučka	k1gFnSc4	obroučka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vybroušeny	vybrousit	k5eAaPmNgFnP	vybrousit
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
okolního	okolní	k2eAgNnSc2d1	okolní
skla	sklo	k1gNnSc2	sklo
<g/>
;	;	kIx,	;
takové	takový	k3xDgNnSc1	takový
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
lentikulární	lentikulární	k2eAgInPc1d1	lentikulární
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rozptylek	rozptylka	k1gFnPc2	rozptylka
někdy	někdy	k6eAd1	někdy
myodisky	myodisky	k6eAd1	myodisky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Materiál	materiál	k1gInSc1	materiál
čoček	čočka	k1gFnPc2	čočka
===	===	k?	===
</s>
</p>
<p>
<s>
Čočky	čočka	k1gFnPc1	čočka
brýlí	brýle	k1gFnPc2	brýle
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
i	i	k9	i
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
CR-39	CR-39	k1gFnSc1	CR-39
(	(	kIx(	(
<g/>
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Trivexu	Trivex	k1gInSc2	Trivex
a	a	k8xC	a
polykarbonátu	polykarbonát	k1gInSc2	polykarbonát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
materiály	materiál	k1gInPc1	materiál
snižují	snižovat	k5eAaImIp3nP	snižovat
riziko	riziko	k1gNnSc4	riziko
rozbití	rozbití	k1gNnSc2	rozbití
<g/>
,	,	kIx,	,
čočky	čočka	k1gFnSc2	čočka
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
hmotnost	hmotnost	k1gFnSc4	hmotnost
než	než	k8xS	než
skleněné	skleněný	k2eAgFnSc2d1	skleněná
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
měkkost	měkkost	k1gFnSc1	měkkost
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
plastové	plastový	k2eAgFnSc2d1	plastová
čočky	čočka	k1gFnSc2	čočka
jsou	být	k5eAaImIp3nP	být
snáze	snadno	k6eAd2	snadno
poškrábatelné	poškrábatelný	k2eAgInPc1d1	poškrábatelný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
materiály	materiál	k1gInPc1	materiál
používané	používaný	k2eAgInPc1d1	používaný
při	při	k7c6	při
zhotovování	zhotovování	k1gNnSc6	zhotovování
korekčních	korekční	k2eAgFnPc2d1	korekční
čoček	čočka	k1gFnPc2	čočka
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInSc4d2	veliký
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
čoček	čočka	k1gFnPc2	čočka
se	s	k7c7	s
silnějšími	silný	k2eAgFnPc7d2	silnější
dioptriemi	dioptrie	k1gFnPc7	dioptrie
(	(	kIx(	(
<g/>
stejné	stejný	k2eAgFnPc1d1	stejná
optické	optický	k2eAgFnPc1d1	optická
mohutnosti	mohutnost	k1gFnPc1	mohutnost
brýlové	brýlový	k2eAgFnSc2d1	brýlová
čočky	čočka	k1gFnSc2	čočka
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
použitím	použití	k1gNnSc7	použití
menšího	malý	k2eAgNnSc2d2	menší
množství	množství	k1gNnSc2	množství
materiálu	materiál	k1gInSc2	materiál
-	-	kIx~	-
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
tenčí	tenký	k2eAgInSc1d2	tenčí
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
estetičtější	estetický	k2eAgFnSc1d2	estetičtější
korekční	korekční	k2eAgFnSc1d1	korekční
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
plastů	plast	k1gInPc2	plast
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
tzv.	tzv.	kA	tzv.
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zajistí	zajistit	k5eAaPmIp3nS	zajistit
větší	veliký	k2eAgFnSc1d2	veliký
odolnost	odolnost	k1gFnSc4	odolnost
čočky	čočka	k1gFnSc2	čočka
vůči	vůči	k7c3	vůči
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
poškození	poškození	k1gNnSc3	poškození
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
tvrdoelastický	tvrdoelastický	k2eAgInSc1d1	tvrdoelastický
lak	lak	k1gInSc1	lak
nanesený	nanesený	k2eAgInSc1d1	nanesený
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
čočky	čočka	k1gFnSc2	čočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokrytí	pokrytí	k1gNnSc1	pokrytí
hydrofobní	hydrofobní	k2eAgFnSc7d1	hydrofobní
vrstvou	vrstva	k1gFnSc7	vrstva
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
nesmáčivost	nesmáčivost	k1gFnSc1	nesmáčivost
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
hladkému	hladký	k2eAgInSc3d1	hladký
povrchu	povrch	k1gInSc3	povrch
též	též	k9	též
snazší	snadný	k2eAgNnSc4d2	snazší
čištění	čištění	k1gNnSc4	čištění
<g/>
.	.	kIx.	.
</s>
<s>
Lipofobní	Lipofobní	k2eAgFnSc1d1	Lipofobní
úprava	úprava	k1gFnSc1	úprava
zajistí	zajistit	k5eAaPmIp3nS	zajistit
větší	veliký	k2eAgFnSc4d2	veliký
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
zašpinění	zašpinění	k1gNnSc3	zašpinění
čoček	čočka	k1gFnPc2	čočka
mastnotou	mastnota	k1gFnSc7	mastnota
<g/>
,	,	kIx,	,
např.	např.	kA	např.
otisky	otisk	k1gInPc7	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Antireflexní	antireflexní	k2eAgFnSc1d1	antireflexní
vrstva	vrstva	k1gFnSc1	vrstva
snižuje	snižovat	k5eAaImIp3nS	snižovat
odrazivost	odrazivost	k1gFnSc4	odrazivost
čoček	čočka	k1gFnPc2	čočka
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
minerálního	minerální	k2eAgNnSc2d1	minerální
skla	sklo	k1gNnSc2	sklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc4d2	menší
odrazivost	odrazivost	k1gFnSc4	odrazivost
ploch	plocha	k1gFnPc2	plocha
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
její	její	k3xOp3gFnSc4	její
větší	veliký	k2eAgFnSc4d2	veliký
propustnost	propustnost	k1gFnSc4	propustnost
pro	pro	k7c4	pro
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
za	za	k7c2	za
stejných	stejný	k2eAgFnPc2d1	stejná
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
úprav	úprava	k1gFnPc2	úprava
mají	mít	k5eAaImIp3nP	mít
korekční	korekční	k2eAgFnPc4d1	korekční
čočky	čočka	k1gFnPc4	čočka
světelnou	světelný	k2eAgFnSc4d1	světelná
propustnost	propustnost	k1gFnSc4	propustnost
méně	málo	k6eAd2	málo
než	než	k8xS	než
93	[number]	k4	93
%	%	kIx~	%
<g/>
,	,	kIx,	,
s	s	k7c7	s
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
antireflexní	antireflexní	k2eAgFnSc7d1	antireflexní
úpravou	úprava	k1gFnSc7	úprava
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
propustnosti	propustnost	k1gFnPc4	propustnost
čočky	čočka	k1gFnSc2	čočka
přes	přes	k7c4	přes
99	[number]	k4	99
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
pro	pro	k7c4	pro
nositele	nositel	k1gMnPc4	nositel
brýlových	brýlový	k2eAgFnPc2d1	brýlová
čoček	čočka	k1gFnPc2	čočka
s	s	k7c7	s
antireflexní	antireflexní	k2eAgFnSc7d1	antireflexní
úpravou	úprava	k1gFnSc7	úprava
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnSc1d2	menší
únava	únava	k1gFnSc1	únava
očí	oko	k1gNnPc2	oko
při	při	k7c6	při
umělém	umělý	k2eAgNnSc6d1	umělé
osvětlení	osvětlení	k1gNnSc6	osvětlení
<g/>
,	,	kIx,	,
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
a	a	k8xC	a
sledování	sledování	k1gNnSc6	sledování
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgNnSc1d2	lepší
vidění	vidění	k1gNnSc1	vidění
při	při	k7c6	při
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
večerním	večerní	k2eAgNnSc7d1	večerní
<g/>
)	)	kIx)	)
řízení	řízení	k1gNnSc1	řízení
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jde	jít	k5eAaImIp3nS	jít
nositeli	nositel	k1gMnSc3	nositel
lépe	dobře	k6eAd2	dobře
vidět	vidět	k5eAaImF	vidět
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
efekt	efekt	k1gInSc1	efekt
na	na	k7c4	na
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
přímou	přímý	k2eAgFnSc4d1	přímá
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čočky	čočka	k1gFnPc1	čočka
CR-39	CR-39	k1gMnPc2	CR-39
mají	mít	k5eAaImIp3nP	mít
vysoké	vysoký	k2eAgNnSc4d1	vysoké
Abbeovo	Abbeův	k2eAgNnSc4d1	Abbeův
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
=	=	kIx~	=
<g/>
59,3	[number]	k4	59,3
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
činí	činit	k5eAaImIp3nS	činit
1,5	[number]	k4	1,5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polykarbonátové	Polykarbonátový	k2eAgFnPc1d1	Polykarbonátová
čočky	čočka	k1gFnPc1	čočka
jsou	být	k5eAaImIp3nP	být
lehké	lehký	k2eAgFnPc1d1	lehká
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgInPc1d1	tenký
a	a	k8xC	a
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
vlastnostem	vlastnost	k1gFnPc3	vlastnost
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
k	k	k7c3	k
osazení	osazení	k1gNnSc3	osazení
zejména	zejména	k9	zejména
vrtaných	vrtaný	k2eAgFnPc2d1	vrtaná
obrub	obruba	k1gFnPc2	obruba
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
odolnější	odolný	k2eAgFnPc1d2	odolnější
proti	proti	k7c3	proti
nárazu	náraz	k1gInSc3	náraz
než	než	k8xS	než
CR-	CR-	k1gFnSc1	CR-
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
křehčí	křehký	k2eAgFnPc1d2	křehčí
než	než	k8xS	než
trivexové	trivexový	k2eAgFnPc1d1	trivexový
čočky	čočka	k1gFnPc1	čočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
nízkému	nízký	k2eAgNnSc3d1	nízké
Abbeovu	Abbeův	k2eAgNnSc3d1	Abbeův
číslu	číslo	k1gNnSc3	číslo
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
=	=	kIx~	=
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
a	a	k8xC	a
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1,59	[number]	k4	1,59
mají	mít	k5eAaImIp3nP	mít
polykarbonátové	polykarbonátový	k2eAgFnPc1d1	polykarbonátová
čočky	čočka	k1gFnPc1	čočka
citelně	citelně	k6eAd1	citelně
horší	zlý	k2eAgFnPc1d2	horší
optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odrazivost	odrazivost	k1gFnSc4	odrazivost
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
barevnou	barevný	k2eAgFnSc4d1	barevná
vadu	vada	k1gFnSc4	vada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc1	aplikace
antireflexu	antireflex	k1gInSc2	antireflex
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nezbytností	nezbytnost	k1gFnSc7	nezbytnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trivexové	Trivexový	k2eAgFnPc4d1	Trivexový
čočky	čočka	k1gFnPc4	čočka
díky	díky	k7c3	díky
absenci	absence	k1gFnSc3	absence
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
pnutí	pnutí	k1gNnSc2	pnutí
svou	svůj	k3xOyFgFnSc7	svůj
odolností	odolnost	k1gFnSc7	odolnost
předčí	předčit	k5eAaBmIp3nS	předčit
čočky	čočka	k1gFnSc2	čočka
polykarbonátové	polykarbonátový	k2eAgNnSc1d1	polykarbonátové
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vyššímu	vysoký	k2eAgNnSc3d2	vyšší
Abbeovu	Abbeův	k2eAgNnSc3d1	Abbeův
číslu	číslo	k1gNnSc3	číslo
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
=	=	kIx~	=
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
Trivex	Trivex	k1gInSc4	Trivex
i	i	k9	i
výrazně	výrazně	k6eAd1	výrazně
lepší	dobrý	k2eAgFnPc4d2	lepší
optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
tak	tak	k6eAd1	tak
tenký	tenký	k2eAgInSc1d1	tenký
(	(	kIx(	(
<g/>
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1,53	[number]	k4	1,53
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
typy	typ	k1gInPc1	typ
čoček	čočka	k1gFnPc2	čočka
===	===	k?	===
</s>
</p>
<p>
<s>
Multifokální	Multifokálnit	k5eAaPmIp3nS	Multifokálnit
(	(	kIx(	(
<g/>
též	též	k9	též
progresivní	progresivní	k2eAgFnSc1d1	progresivní
<g/>
)	)	kIx)	)
čočka	čočka	k1gFnSc1	čočka
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
různou	různý	k2eAgFnSc4d1	různá
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
presbyopie	presbyopie	k1gFnSc2	presbyopie
(	(	kIx(	(
<g/>
stařecké	stařecký	k2eAgFnPc1d1	stařecká
vetchozrakosti	vetchozrakost	k1gFnPc1	vetchozrakost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgFnPc2	tento
brýlí	brýle	k1gFnPc2	brýle
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
jedněmi	jeden	k4xCgFnPc7	jeden
brýlemi	brýle	k1gFnPc7	brýle
vidět	vidět	k5eAaImF	vidět
jak	jak	k6eAd1	jak
do	do	k7c2	do
blízka	blízko	k1gNnSc2	blízko
tak	tak	k6eAd1	tak
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Známy	znám	k2eAgInPc1d1	znám
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
multifokální	multifokální	k2eAgFnPc4d1	multifokální
brýle	brýle	k1gFnPc4	brýle
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
jako	jako	k9	jako
brýle	brýle	k1gFnPc4	brýle
čtecí	čtecí	k2eAgFnPc4d1	čtecí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
dioptrií	dioptrie	k1gFnPc2	dioptrie
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odpadá	odpadat	k5eAaImIp3nS	odpadat
poté	poté	k6eAd1	poté
hledání	hledání	k1gNnSc1	hledání
a	a	k8xC	a
neustálé	neustálý	k2eAgNnSc1d1	neustálé
sundavání	sundavání	k1gNnSc1	sundavání
a	a	k8xC	a
nandavání	nandavání	k1gNnSc1	nandavání
čtecích	čtecí	k2eAgFnPc2d1	čtecí
brýlí	brýle	k1gFnPc2	brýle
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
brýle	brýle	k1gFnPc4	brýle
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
dioptrií	dioptrie	k1gFnPc2	dioptrie
na	na	k7c4	na
blízko	blízko	k6eAd1	blízko
<g/>
"	"	kIx"	"
-	-	kIx~	-
využitelné	využitelný	k2eAgNnSc1d1	využitelné
pro	pro	k7c4	pro
staršího	starší	k1gMnSc4	starší
myopa	myop	k1gMnSc4	myop
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
již	již	k9	již
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
přeostřováním	přeostřování	k1gNnSc7	přeostřování
na	na	k7c4	na
blízko	blízko	k1gNnSc4	blízko
v	v	k7c6	v
brýlích	brýle	k1gFnPc6	brýle
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
multifokálních	multifokální	k2eAgFnPc2d1	multifokální
čoček	čočka	k1gFnPc2	čočka
jsou	být	k5eAaImIp3nP	být
čočky	čočka	k1gFnPc1	čočka
bifokální	bifokální	k2eAgFnPc1d1	bifokální
(	(	kIx(	(
<g/>
dvouohniskové	dvouohniskový	k2eAgFnPc1d1	dvouohniskový
<g/>
,	,	kIx,	,
dělené	dělený	k2eAgFnPc1d1	dělená
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
vlisovaný	vlisovaný	k2eAgInSc4d1	vlisovaný
nebo	nebo	k8xC	nebo
vybroušený	vybroušený	k2eAgInSc4d1	vybroušený
oddíl	oddíl	k1gInSc4	oddíl
na	na	k7c6	na
blízko	blízko	k6eAd1	blízko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
vynálezcem	vynálezce	k1gMnSc7	vynálezce
byl	být	k5eAaImAgMnS	být
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gMnSc2	jejich
tvůrce	tvůrce	k1gMnSc2	tvůrce
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
název	název	k1gInSc1	název
Franklinův	Franklinův	k2eAgInSc1d1	Franklinův
bifokál	bifokál	k1gInSc1	bifokál
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
čočky	čočka	k1gFnPc4	čočka
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
na	na	k7c4	na
blízko	blízko	k1gNnSc4	blízko
<g/>
,	,	kIx,	,
tvořícím	tvořící	k2eAgInSc7d1	tvořící
celou	celý	k2eAgFnSc7d1	celá
spodní	spodní	k2eAgFnSc7d1	spodní
polovinu	polovina	k1gFnSc4	polovina
čočky	čočka	k1gFnSc2	čočka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ne	ne	k9	ne
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
srpek	srpek	k1gInSc1	srpek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asférické	Asférický	k2eAgFnPc1d1	Asférická
čočky	čočka	k1gFnPc1	čočka
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
čočky	čočka	k1gFnPc1	čočka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
nemá	mít	k5eNaImIp3nS	mít
kulovou	kulový	k2eAgFnSc4d1	kulová
(	(	kIx(	(
<g/>
sférickou	sférický	k2eAgFnSc4d1	sférická
<g/>
)	)	kIx)	)
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
silného	silný	k2eAgInSc2d1	silný
svazku	svazek	k1gInSc2	svazek
paprsků	paprsek	k1gInPc2	paprsek
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
lámavou	lámavý	k2eAgFnSc4d1	lámavá
plochu	plocha	k1gFnSc4	plocha
sférické	sférický	k2eAgFnSc2d1	sférická
čočky	čočka	k1gFnSc2	čočka
k	k	k7c3	k
nestejnoměrnému	stejnoměrný	k2eNgInSc3d1	nestejnoměrný
lomu	lom	k1gInSc3	lom
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
nikoliv	nikoliv	k9	nikoliv
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozmazané	rozmazaný	k2eAgFnPc1d1	rozmazaná
plošky	ploška	k1gFnPc1	ploška
(	(	kIx(	(
<g/>
koma	koma	k1gFnSc1	koma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sférická	sférický	k2eAgFnSc1d1	sférická
aberace	aberace	k1gFnSc1	aberace
<g/>
.	.	kIx.	.
</s>
<s>
Asférická	Asférický	k2eAgFnSc1d1	Asférická
čočka	čočka	k1gFnSc1	čočka
svou	svůj	k3xOyFgFnSc7	svůj
konstrukcí	konstrukce	k1gFnSc7	konstrukce
(	(	kIx(	(
<g/>
od	od	k7c2	od
středu	střed	k1gInSc2	střed
ke	k	k7c3	k
kraji	kraj	k1gInSc3	kraj
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
strmost	strmost	k1gFnSc1	strmost
zakřivení	zakřivení	k1gNnSc4	zakřivení
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
ploch	plocha	k1gFnPc2	plocha
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
vadu	vada	k1gFnSc4	vada
zčásti	zčásti	k6eAd1	zčásti
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
částečně	částečně	k6eAd1	částečně
snižuje	snižovat	k5eAaImIp3nS	snižovat
aberaci	aberace	k1gFnSc4	aberace
chromatickou	chromatický	k2eAgFnSc4d1	chromatická
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
není	být	k5eNaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
patrný	patrný	k2eAgInSc1d1	patrný
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
kvality	kvalita	k1gFnSc2	kvalita
visu	vis	k1gInSc2	vis
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
asférická	asférický	k2eAgFnSc1d1	asférická
čočka	čočka	k1gFnSc1	čočka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
eliminovat	eliminovat	k5eAaBmF	eliminovat
nepříjemné	příjemný	k2eNgInPc4d1	nepříjemný
průvodní	průvodní	k2eAgInPc4d1	průvodní
jevy	jev	k1gInPc4	jev
nošení	nošení	k1gNnSc2	nošení
brýlí	brýle	k1gFnPc2	brýle
<g/>
:	:	kIx,	:
velké	velký	k2eAgInPc4d1	velký
či	či	k8xC	či
malé	malý	k2eAgInPc4d1	malý
oči	oko	k1gNnPc1	oko
za	za	k7c7	za
silnějšími	silný	k2eAgNnPc7d2	silnější
skly	sklo	k1gNnPc7	sklo
a	a	k8xC	a
samotnou	samotný	k2eAgFnSc4d1	samotná
tloušťku	tloušťka	k1gFnSc4	tloušťka
skla	sklo	k1gNnSc2	sklo
(	(	kIx(	(
<g/>
asféra	asféra	k1gFnSc1	asféra
je	být	k5eAaImIp3nS	být
plošší	plochý	k2eAgFnSc1d2	plošší
než	než	k8xS	než
čočka	čočka	k1gFnSc1	čočka
sférická	sférický	k2eAgFnSc1d1	sférická
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
optické	optický	k2eAgFnSc6d1	optická
mohutnosti	mohutnost	k1gFnSc6	mohutnost
a	a	k8xC	a
indexu	index	k1gInSc6	index
lomu	lom	k1gInSc2	lom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
čoček	čočka	k1gFnPc2	čočka
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
svá	svůj	k3xOyFgNnPc4	svůj
úskalí	úskalí	k1gNnPc4	úskalí
<g/>
,	,	kIx,	,
je	on	k3xPp3gNnPc4	on
zapotřebí	zapotřebí	k6eAd1	zapotřebí
preciznějších	precizní	k2eAgNnPc2d2	preciznější
měření	měření	k1gNnPc2	měření
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
čoček	čočka	k1gFnPc2	čočka
sférické	sférický	k2eAgFnSc2d1	sférická
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
zakřivení	zakřivení	k1gNnSc4	zakřivení
a	a	k8xC	a
inklinaci	inklinace	k1gFnSc4	inklinace
očnic	očnice	k1gFnPc2	očnice
brýlí	brýle	k1gFnPc2	brýle
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
špatném	špatný	k2eAgNnSc6d1	špatné
zaměření	zaměření	k1gNnSc6	zaměření
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
pupilární	pupilární	k2eAgFnSc1d1	pupilární
distance	distance	k1gFnSc1	distance
<g/>
,	,	kIx,	,
osa	osa	k1gFnSc1	osa
otáčení	otáčení	k1gNnSc2	otáčení
<g/>
,	,	kIx,	,
inklinace	inklinace	k1gFnSc1	inklinace
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
diskomfortu	diskomfort	k1gInSc3	diskomfort
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
deformace	deformace	k1gFnSc1	deformace
obrazu	obraz	k1gInSc2	obraz
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnSc3	bolest
hlavy	hlava	k1gFnSc2	hlava
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochranné	ochranný	k2eAgFnPc4d1	ochranná
brýle	brýle	k1gFnPc4	brýle
==	==	k?	==
</s>
</p>
<p>
<s>
Ochranné	ochranný	k2eAgFnPc4d1	ochranná
brýle	brýle	k1gFnPc4	brýle
chrání	chránit	k5eAaImIp3nS	chránit
zrak	zrak	k1gInSc1	zrak
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
(	(	kIx(	(
<g/>
např.	např.	kA	např.
před	před	k7c7	před
odštěpky	odštěpek	k1gInPc7	odštěpek
odletujícími	odletující	k2eAgInPc7d1	odletující
při	při	k7c6	při
obrábění	obrábění	k1gNnSc6	obrábění
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zářením	záření	k1gNnSc7	záření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
před	před	k7c7	před
ultrafialovým	ultrafialový	k2eAgInSc7d1	ultrafialový
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
i	i	k9	i
tepelným	tepelný	k2eAgInSc7d1	tepelný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motoristické	motoristický	k2eAgFnPc4d1	motoristická
brýle	brýle	k1gFnPc4	brýle
chrání	chránit	k5eAaImIp3nS	chránit
jezdce	jezdec	k1gInPc4	jezdec
na	na	k7c6	na
motocyklu	motocykl	k1gInSc6	motocykl
před	před	k7c7	před
rychlým	rychlý	k2eAgInSc7d1	rychlý
či	či	k8xC	či
prudkým	prudký	k2eAgInSc7d1	prudký
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
slzení	slzení	k1gNnSc4	slzení
<g/>
,	,	kIx,	,
a	a	k8xC	a
před	před	k7c7	před
létajícím	létající	k2eAgInSc7d1	létající
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
helmy	helma	k1gFnSc2	helma
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
tyto	tento	k3xDgFnPc4	tento
brýle	brýle	k1gFnPc4	brýle
používali	používat	k5eAaImAgMnP	používat
i	i	k8xC	i
řidiči	řidič	k1gMnPc1	řidič
otevřených	otevřený	k2eAgInPc2d1	otevřený
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
piloti	pilot	k1gMnPc1	pilot
malých	malý	k2eAgNnPc2d1	malé
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyli	být	k5eNaImAgMnP	být
chráněni	chránit	k5eAaImNgMnP	chránit
prosklenou	prosklený	k2eAgFnSc7d1	prosklená
karosérií	karosérie	k1gFnSc7	karosérie
nebo	nebo	k8xC	nebo
kokpitem	kokpit	k1gInSc7	kokpit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potápěčské	potápěčský	k2eAgFnPc4d1	potápěčská
brýle	brýle	k1gFnPc4	brýle
a	a	k8xC	a
brýle	brýle	k1gFnPc4	brýle
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgNnSc4d1	sportovní
plavání	plavání	k1gNnSc4	plavání
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
očí	oko	k1gNnPc2	oko
od	od	k7c2	od
vodního	vodní	k2eAgNnSc2d1	vodní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	on	k3xPp3gFnPc4	on
chrání	chránit	k5eAaImIp3nP	chránit
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
ostré	ostrý	k2eAgNnSc4d1	ostré
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Lyžařské	lyžařský	k2eAgFnPc4d1	lyžařská
brýle	brýle	k1gFnPc4	brýle
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
proudem	proud	k1gInSc7	proud
studeného	studený	k2eAgInSc2d1	studený
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
před	před	k7c7	před
nárazy	náraz	k1gInPc7	náraz
poletujícího	poletující	k2eAgInSc2d1	poletující
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sluneční	sluneční	k2eAgFnPc4d1	sluneční
brýle	brýle	k1gFnPc4	brýle
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typ	typ	k1gInSc1	typ
ochranných	ochranný	k2eAgFnPc2d1	ochranná
brýlí	brýle	k1gFnPc2	brýle
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
zatmavená	zatmavený	k2eAgNnPc4d1	zatmavené
skla	sklo	k1gNnPc4	sklo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
chránily	chránit	k5eAaImAgFnP	chránit
oči	oko	k1gNnPc4	oko
před	před	k7c7	před
jasným	jasný	k2eAgNnSc7d1	jasné
slunečním	sluneční	k2eAgNnSc7d1	sluneční
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
doplňkem	doplněk	k1gInSc7	doplněk
k	k	k7c3	k
opalování	opalování	k1gNnSc3	opalování
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
při	při	k7c6	při
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
pohybu	pohyb	k1gInSc6	pohyb
venku	venku	k6eAd1	venku
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
ostrým	ostrý	k2eAgNnSc7d1	ostré
sluncem	slunce	k1gNnSc7	slunce
na	na	k7c6	na
zasněžených	zasněžený	k2eAgFnPc6d1	zasněžená
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozličným	rozličný	k2eAgFnPc3d1	rozličná
obroučkám	obroučka	k1gFnPc3	obroučka
a	a	k8xC	a
barevným	barevný	k2eAgInPc3d1	barevný
odstínům	odstín	k1gInPc3	odstín
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
jako	jako	k9	jako
výrazný	výrazný	k2eAgInSc4d1	výrazný
módní	módní	k2eAgInSc4d1	módní
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
statusový	statusový	k2eAgInSc4d1	statusový
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
profesím	profes	k1gFnPc3	profes
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnPc1d1	sluneční
brýle	brýle	k1gFnPc1	brýle
mají	mít	k5eAaImIp3nP	mít
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
smysl	smysl	k1gInSc4	smysl
zejména	zejména	k9	zejména
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
širší	široký	k2eAgFnSc4d2	širší
a	a	k8xC	a
průhlednější	průhledný	k2eAgFnSc4d2	průhlednější
čočku	čočka	k1gFnSc4	čočka
než	než	k8xS	než
dospělý	dospělý	k2eAgMnSc1d1	dospělý
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
větší	veliký	k2eAgFnSc4d2	veliký
riziko	riziko	k1gNnSc4	riziko
poškození	poškození	k1gNnSc2	poškození
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kombinace	kombinace	k1gFnPc1	kombinace
s	s	k7c7	s
dioptrickými	dioptrický	k2eAgFnPc7d1	dioptrická
brýlemi	brýle	k1gFnPc7	brýle
===	===	k?	===
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mít	mít	k5eAaImF	mít
nasazeny	nasazen	k2eAgFnPc4d1	nasazena
ochranné	ochranný	k2eAgFnPc4d1	ochranná
brýle	brýle	k1gFnPc4	brýle
přes	přes	k7c4	přes
dioptrické	dioptrický	k2eAgNnSc4d1	dioptrické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
také	také	k9	také
ochranná	ochranný	k2eAgNnPc4d1	ochranné
skla	sklo	k1gNnPc4	sklo
"	"	kIx"	"
<g/>
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
nositelovy	nositelův	k2eAgFnSc2d1	nositelův
oční	oční	k2eAgFnSc2d1	oční
vady	vada	k1gFnSc2	vada
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
dioptrické	dioptrický	k2eAgFnPc4d1	dioptrická
brýle	brýle	k1gFnPc4	brýle
sluneční	sluneční	k2eAgFnSc2d1	sluneční
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
se	s	k7c7	s
skly	sklo	k1gNnPc7	sklo
tmavými	tmavý	k2eAgNnPc7d1	tmavé
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
skly	sklo	k1gNnPc7	sklo
reagujícími	reagující	k2eAgFnPc7d1	reagující
na	na	k7c4	na
osvětlení	osvětlení	k1gNnSc4	osvětlení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
samozabarvovací	samozabarvovací	k2eAgInPc1d1	samozabarvovací
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
jsou	být	k5eAaImIp3nP	být
sluneční	sluneční	k2eAgFnPc1d1	sluneční
klapky	klapka	k1gFnPc1	klapka
-	-	kIx~	-
plastové	plastový	k2eAgInPc1d1	plastový
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
nasadit	nasadit	k5eAaPmF	nasadit
na	na	k7c4	na
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
brýle	brýle	k1gFnPc4	brýle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
3D	[number]	k4	3D
brýle	brýle	k1gFnPc1	brýle
==	==	k?	==
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc4	tento
brýle	brýle	k1gFnPc4	brýle
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
3-D	[number]	k4	3-D
filmů	film	k1gInPc2	film
či	či	k8xC	či
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
dojmu	dojem	k1gInSc2	dojem
virtuální	virtuální	k2eAgFnSc2d1	virtuální
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
brýle	brýle	k1gFnPc4	brýle
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každé	každý	k3xTgNnSc1	každý
oko	oko	k1gNnSc1	oko
mohlo	moct	k5eAaImAgNnS	moct
sledovat	sledovat	k5eAaImF	sledovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
buď	buď	k8xC	buď
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
barvami	barva	k1gFnPc7	barva
skel	sklo	k1gNnPc2	sklo
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
/	/	kIx~	/
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
–	–	k?	–
výsledný	výsledný	k2eAgInSc1d1	výsledný
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zdánlivě	zdánlivě	k6eAd1	zdánlivě
černobílý	černobílý	k2eAgInSc1d1	černobílý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
skla	sklo	k1gNnPc1	sklo
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
polarizační	polarizační	k2eAgInPc4d1	polarizační
filtry	filtr	k1gInPc4	filtr
s	s	k7c7	s
kolmými	kolmý	k2eAgFnPc7d1	kolmá
rovinami	rovina	k1gFnPc7	rovina
polarizace	polarizace	k1gFnSc2	polarizace
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
žádné	žádný	k3yNgFnPc4	žádný
omezení	omezení	k1gNnSc1	omezení
na	na	k7c4	na
barevnost	barevnost	k1gFnSc4	barevnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
3-D	[number]	k4	3-D
kina	kino	k1gNnSc2	kino
Imax	Imax	k1gInSc1	Imax
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
dioptrie	dioptrie	k1gFnSc1	dioptrie
</s>
</p>
<p>
<s>
Astigmatismus	astigmatismus	k1gInSc1	astigmatismus
(	(	kIx(	(
<g/>
medicína	medicína	k1gFnSc1	medicína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
čočka	čočka	k1gFnSc1	čočka
</s>
</p>
<p>
<s>
optometrie	optometrie	k1gFnSc1	optometrie
</s>
</p>
<p>
<s>
oční	oční	k2eAgFnSc1d1	oční
optika	optika	k1gFnSc1	optika
</s>
</p>
<p>
<s>
lorňon	lorňon	k1gInSc1	lorňon
</s>
</p>
<p>
<s>
cvikr	cvikr	k1gInSc1	cvikr
</s>
</p>
<p>
<s>
monokl	monokl	k1gInSc1	monokl
</s>
</p>
<p>
<s>
brýlový	brýlový	k2eAgInSc1d1	brýlový
fetišismus	fetišismus	k1gInSc1	fetišismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
brýle	brýle	k1gFnPc4	brýle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
brýle	brýle	k1gFnPc1	brýle
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
