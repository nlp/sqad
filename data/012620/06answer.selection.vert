<s>
Dioptrické	dioptrický	k2eAgFnPc1d1	dioptrická
brýle	brýle	k1gFnPc1	brýle
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
krátkozrakosti	krátkozrakost	k1gFnSc2	krátkozrakost
nebo	nebo	k8xC	nebo
dalekozrakosti	dalekozrakost	k1gFnSc2	dalekozrakost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
korekcí	korekce	k1gFnSc7	korekce
vad	vada	k1gFnPc2	vada
oka	oko	k1gNnSc2	oko
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
astigmatismus	astigmatismus	k1gInSc4	astigmatismus
<g/>
.	.	kIx.	.
</s>
