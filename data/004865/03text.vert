<s>
Chikungunya	Chikungunya	k6eAd1	Chikungunya
(	(	kIx(	(
<g/>
vyslov	vyslovit	k5eAaPmRp2nS	vyslovit
čikuguňa	čikuguňa	k6eAd1	čikuguňa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nemoc	nemoc	k1gFnSc1	nemoc
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
arbovirem	arbovirem	k6eAd1	arbovirem
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
alphavirů	alphavir	k1gMnPc2	alphavir
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
Togaviridae	Togavirida	k1gFnSc2	Togavirida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
viru	vir	k1gInSc2	vir
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
držení	držení	k1gNnSc2	držení
těla	tělo	k1gNnSc2	tělo
nemocných	nemocný	k2eAgFnPc2d1	nemocná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
makonde	makond	k1gInSc5	makond
to	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
kráčet	kráčet	k5eAaImF	kráčet
skloněný	skloněný	k2eAgMnSc1d1	skloněný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
onemocnění	onemocnění	k1gNnSc4	onemocnění
chikungunya	chikungunyus	k1gMnSc2	chikungunyus
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
poprvé	poprvé	k6eAd1	poprvé
izolovali	izolovat	k5eAaBmAgMnP	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
a	a	k8xC	a
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
virus	virus	k1gInSc1	virus
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
projevy	projev	k1gInPc7	projev
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
nemocní	nemocný	k2eAgMnPc1d1	nemocný
pacienti	pacient	k1gMnPc1	pacient
mají	mít	k5eAaImIp3nP	mít
další	další	k2eAgInPc4d1	další
příznaky	příznak	k1gInPc4	příznak
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyrážku	vyrážka	k1gFnSc4	vyrážka
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
únavu	únava	k1gFnSc4	únava
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc4	krvácení
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
nebo	nebo	k8xC	nebo
dásní	dáseň	k1gFnPc2	dáseň
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc4	pět
až	až	k9	až
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Přenašečem	přenašeč	k1gInSc7	přenašeč
horečky	horečka	k1gFnSc2	horečka
chikungunya	chikunguny	k1gInSc2	chikunguny
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
mušky	muška	k1gFnPc1	muška
<g/>
,	,	kIx,	,
komáři	komár	k1gMnPc1	komár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
epidemie	epidemie	k1gFnSc2	epidemie
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kauzální	kauzální	k2eAgFnSc1d1	kauzální
léčba	léčba	k1gFnSc1	léčba
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
symptomatické	symptomatický	k2eAgNnSc1d1	symptomatické
řešení	řešení	k1gNnSc1	řešení
příznaků	příznak	k1gInPc2	příznak
(	(	kIx(	(
<g/>
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
