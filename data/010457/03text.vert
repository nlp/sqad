<p>
<s>
Wiggersův	Wiggersův	k2eAgInSc1d1	Wiggersův
diagram	diagram	k1gInSc1	diagram
je	být	k5eAaImIp3nS	být
grafické	grafický	k2eAgNnSc4d1	grafické
zobrazení	zobrazení	k1gNnSc4	zobrazení
časového	časový	k2eAgInSc2d1	časový
průběhu	průběh	k1gInSc2	průběh
důležitých	důležitý	k2eAgFnPc2d1	důležitá
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
veličin	veličina	k1gFnPc2	veličina
fungování	fungování	k1gNnSc2	fungování
lidského	lidský	k2eAgNnSc2d1	lidské
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
tři	tři	k4xCgFnPc1	tři
důležité	důležitý	k2eAgFnPc1d1	důležitá
hodnoty	hodnota	k1gFnPc1	hodnota
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
červeně	červeně	k6eAd1	červeně
v	v	k7c6	v
aortě	aorta	k1gFnSc6	aorta
<g/>
,	,	kIx,	,
modře	modro	k6eAd1	modro
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
komoře	komora	k1gFnSc6	komora
a	a	k8xC	a
žlutě	žlutě	k6eAd1	žlutě
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
předsíni	předsíň	k1gFnSc6	předsíň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
průběh	průběh	k1gInSc4	průběh
elektrických	elektrický	k2eAgInPc2d1	elektrický
potenciálů	potenciál	k1gInPc2	potenciál
(	(	kIx(	(
<g/>
EKG	EKG	kA	EKG
<g/>
)	)	kIx)	)
a	a	k8xC	a
dole	dole	k6eAd1	dole
průběh	průběh	k1gInSc1	průběh
depolarizace	depolarizace	k1gFnSc2	depolarizace
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zvané	zvaný	k2eAgNnSc4d1	zvané
plató	plató	k1gNnSc4	plató
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
membrána	membrána	k1gFnSc1	membrána
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
další	další	k2eAgInPc4d1	další
impulzy	impulz	k1gInPc4	impulz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
diagramu	diagram	k1gInSc6	diagram
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyneseny	vynesen	k2eAgInPc4d1	vynesen
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
signály	signál	k1gInPc4	signál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zvukové	zvukový	k2eAgInPc4d1	zvukový
(	(	kIx(	(
<g/>
ozvy	ozev	k1gInPc4	ozev
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tepenné	tepenný	k2eAgNnSc1d1	tepenné
proudění	proudění	k1gNnSc1	proudění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
ose	osa	k1gFnSc6	osa
je	být	k5eAaImIp3nS	být
vynesen	vynesen	k2eAgInSc1d1	vynesen
čas	čas	k1gInSc1	čas
jednoho	jeden	k4xCgInSc2	jeden
srdečního	srdeční	k2eAgInSc2d1	srdeční
cyklu	cyklus	k1gInSc2	cyklus
(	(	kIx(	(
<g/>
v	v	k7c6	v
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ozvy	ozva	k1gFnPc1	ozva
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
onemocněními	onemocnění	k1gNnPc7	onemocnění
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
nejsou	být	k5eNaImIp3nP	být
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Srdce	srdce	k1gNnSc1	srdce
</s>
</p>
<p>
<s>
Oběhová	oběhový	k2eAgFnSc1d1	oběhová
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elektrokardiogram	elektrokardiogram	k1gInSc1	elektrokardiogram
</s>
</p>
<p>
<s>
Diastola	diastola	k1gFnSc1	diastola
</s>
</p>
<p>
<s>
Systola	systola	k1gFnSc1	systola
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Silbernagl	Silbernagl	k1gInSc1	Silbernagl
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Despopoulos	Despopoulos	k1gMnSc1	Despopoulos
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
fyziologie	fyziologie	k1gFnSc2	fyziologie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
Avicenum	Avicenum	k1gInSc1	Avicenum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Trojan	Trojan	k1gMnSc1	Trojan
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fyziologie	fyziologie	k1gFnSc1	fyziologie
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wiggers	Wiggers	k1gInSc1	Wiggers
diagram	diagram	k1gInSc1	diagram
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
