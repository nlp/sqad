<s>
Forsing	Forsing	k1gInSc1	Forsing
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
též	též	k9	též
anglický	anglický	k2eAgInSc1d1	anglický
termín	termín	k1gInSc1	termín
forcing	forcing	k1gInSc1	forcing
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
obecná	obecný	k2eAgFnSc1d1	obecná
důkazová	důkazový	k2eAgFnSc1d1	důkazová
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
dokazování	dokazování	k1gNnSc4	dokazování
relativních	relativní	k2eAgFnPc2d1	relativní
konzistencí	konzistence	k1gFnPc2	konzistence
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
použil	použít	k5eAaPmAgMnS	použít
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
americký	americký	k2eAgMnSc1d1	americký
matematik	matematik	k1gMnSc1	matematik
Paul	Paul	k1gMnSc1	Paul
Cohen	Cohno	k1gNnPc2	Cohno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
užitím	užití	k1gNnSc7	užití
forsingu	forsing	k1gInSc2	forsing
dokázal	dokázat	k5eAaPmAgMnS	dokázat
bezespornost	bezespornost	k1gFnSc4	bezespornost
negace	negace	k1gFnSc2	negace
hypotézy	hypotéza	k1gFnSc2	hypotéza
kontinua	kontinuum	k1gNnSc2	kontinuum
s	s	k7c7	s
axiomy	axiom	k1gInPc7	axiom
Zermelo-Fraenkelovy	Zermelo-Fraenkelův	k2eAgFnSc2d1	Zermelo-Fraenkelův
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
rozpracována	rozpracován	k2eAgFnSc1d1	rozpracována
Dana	Dana	k1gFnSc1	Dana
Scottem	Scott	k1gInSc7	Scott
<g/>
,	,	kIx,	,
Robertem	Robert	k1gMnSc7	Robert
Solovayem	Solovay	k1gMnSc7	Solovay
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
Vopěnkou	Vopěnka	k1gFnSc7	Vopěnka
do	do	k7c2	do
teorie	teorie	k1gFnSc2	teorie
booleovsky	booleovsky	k6eAd1	booleovsky
ohodnocených	ohodnocený	k2eAgInPc2d1	ohodnocený
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Forsing	Forsing	k1gInSc1	Forsing
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
univerzální	univerzální	k2eAgFnSc7d1	univerzální
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
dokazování	dokazování	k1gNnSc4	dokazování
relativních	relativní	k2eAgFnPc2d1	relativní
konzistencí	konzistence	k1gFnPc2	konzistence
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
odstavec	odstavec	k1gInSc1	odstavec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velmi	velmi	k6eAd1	velmi
zjednodušené	zjednodušený	k2eAgNnSc4d1	zjednodušené
podání	podání	k1gNnSc4	podání
základní	základní	k2eAgFnSc2d1	základní
myšlenky	myšlenka	k1gFnSc2	myšlenka
forsingu	forsing	k1gInSc2	forsing
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
motivační	motivační	k2eAgInSc4d1	motivační
smysl	smysl	k1gInSc4	smysl
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
matematicky	matematicky	k6eAd1	matematicky
zcela	zcela	k6eAd1	zcela
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc4d1	přesný
popis	popis	k1gInSc4	popis
metody	metoda	k1gFnSc2	metoda
forsingu	forsing	k1gInSc2	forsing
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
odstavcích	odstavec	k1gInPc6	odstavec
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
forsingu	forsing	k1gInSc2	forsing
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
modelů	model	k1gInPc2	model
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
do	do	k7c2	do
modelů	model	k1gInPc2	model
nových	nový	k2eAgInPc2d1	nový
přidáním	přidání	k1gNnSc7	přidání
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zajistí	zajistit	k5eAaPmIp3nP	zajistit
platnost	platnost	k1gFnSc4	platnost
požadovaného	požadovaný	k2eAgNnSc2d1	požadované
tvrzení	tvrzení	k1gNnSc2	tvrzení
v	v	k7c6	v
takto	takto	k6eAd1	takto
rozšířeném	rozšířený	k2eAgInSc6d1	rozšířený
modelu	model	k1gInSc6	model
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
výchozí	výchozí	k2eAgFnSc6d1	výchozí
situaci	situace	k1gFnSc6	situace
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
dán	dát	k5eAaPmNgInS	dát
nějaký	nějaký	k3yIgInSc4	nějaký
model	model	k1gInSc4	model
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
teorie	teorie	k1gFnSc1	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
díky	díky	k7c3	díky
Löwenheim-Skolemově	Löwenheim-Skolemův	k2eAgFnSc3d1	Löwenheim-Skolemův
větě	věta	k1gFnSc3	věta
můžeme	moct	k5eAaImIp1nP	moct
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
spočetný	spočetný	k2eAgMnSc1d1	spočetný
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
technický	technický	k2eAgInSc1d1	technický
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
obejít	obejít	k5eAaPmF	obejít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
nějaký	nějaký	k3yIgInSc1	nějaký
model	model	k1gInSc1	model
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
rozšiřující	rozšiřující	k2eAgMnSc1d1	rozšiřující
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
⊆	⊆	k?	⊆
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
N	N	kA	N
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
prvky	prvek	k1gInPc4	prvek
modelu	model	k1gInSc2	model
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
prvky	prvek	k1gInPc4	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
podmnožinami	podmnožina	k1gFnPc7	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
tj.	tj.	kA	tj.
taková	takový	k3xDgNnPc4	takový
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
N	N	kA	N
∖	∖	k?	∖
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
setminus	setminus	k1gMnSc1	setminus
<g />
.	.	kIx.	.
</s>
<s hack="1">
M	M	kA	M
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
⊆	⊆	k?	⊆
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
subseteq	subseteq	k?	subseteq
M	M	kA	M
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
taková	takový	k3xDgFnSc1	takový
x	x	k?	x
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
polomnožinami	polomnožina	k1gFnPc7	polomnožina
<g/>
"	"	kIx"	"
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
forsingu	forsing	k1gInSc2	forsing
je	být	k5eAaImIp3nS	být
sestrojit	sestrojit	k5eAaPmF	sestrojit
nějaký	nějaký	k3yIgInSc4	nějaký
model	model	k1gInSc4	model
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
ležící	ležící	k2eAgInSc1d1	ležící
mezi	mezi	k7c7	mezi
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
,	,	kIx,	,
tj.	tj.	kA	tj.
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
podmnožiny	podmnožina	k1gFnPc4	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
neleží	ležet	k5eNaImIp3nS	ležet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
konstrukce	konstrukce	k1gFnSc2	konstrukce
modelu	model	k1gInSc2	model
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
velmi	velmi	k6eAd1	velmi
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
následovně	následovně	k6eAd1	následovně
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
podmnožiny	podmnožina	k1gFnPc1	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
modelu	model	k1gInSc6	model
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ohodnotit	ohodnotit	k5eAaPmF	ohodnotit
číslem	číslo	k1gNnSc7	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
a	a	k8xC	a
zbylé	zbylý	k2eAgFnSc2d1	zbylá
množiny	množina	k1gFnSc2	množina
číslem	číslo	k1gNnSc7	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
předem	předem	k6eAd1	předem
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
množiny	množina	k1gFnPc1	množina
musí	muset	k5eAaImIp3nP	muset
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
modelem	model	k1gInSc7	model
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
nestačí	stačit	k5eNaBmIp3nS	stačit
ohodnocovat	ohodnocovat	k5eAaImF	ohodnocovat
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
nul	nula	k1gFnPc2	nula
a	a	k8xC	a
jedniček	jednička	k1gFnPc2	jednička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
strukturu	struktura	k1gFnSc4	struktura
nějaké	nějaký	k3yIgFnSc2	nějaký
Booleovy	Booleův	k2eAgFnSc2d1	Booleova
algebry	algebra	k1gFnSc2	algebra
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
∈	∈	k?	∈
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
M	M	kA	M
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
podmnožině	podmnožina	k1gFnSc6	podmnožina
M	M	kA	M
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
nějaká	nějaký	k3yIgFnSc1	nějaký
booleovská	booleovský	k2eAgFnSc1d1	booleovská
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
∈	∈	k?	∈
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
}	}	kIx)	}
,	,	kIx,	,
která	který	k3yQgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
"	"	kIx"	"
<g/>
míru	míra	k1gFnSc4	míra
<g/>
"	"	kIx"	"
jejího	její	k3xOp3gNnSc2	její
náležení	náležení	k1gNnSc2	náležení
do	do	k7c2	do
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
množiny	množina	k1gFnPc1	množina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
nakonec	nakonec	k6eAd1	nakonec
budou	být	k5eAaImBp3nP	být
skutečně	skutečně	k6eAd1	skutečně
zařazeny	zařadit	k5eAaPmNgInP	zařadit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
pomocí	pomocí	k7c2	pomocí
nějakého	nějaký	k3yIgInSc2	nějaký
filtru	filtr	k1gInSc2	filtr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
∈	∈	k?	∈
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
N	N	kA	N
<g/>
}	}	kIx)	}
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
booleovská	booleovský	k2eAgFnSc1d1	booleovská
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sestrojení	sestrojení	k1gNnSc4	sestrojení
rozšíření	rozšíření	k1gNnSc2	rozšíření
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
[	[	kIx(	[
G	G	kA	G
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
modelu	model	k1gInSc3	model
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
technika	technika	k1gFnSc1	technika
booleovských	booleovský	k2eAgNnPc2d1	booleovské
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
