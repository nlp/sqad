<s>
První	první	k4xOgInSc1	první
model	model	k1gInSc1	model
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Apple	Apple	kA	Apple
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
počítače	počítač	k1gInPc4	počítač
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
