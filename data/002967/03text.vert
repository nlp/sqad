<s>
Sirény	Siréna	k1gFnPc1	Siréna
(	(	kIx(	(
<g/>
Sirenia	Sirenium	k1gNnPc1	Sirenium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
ochechule	ochechule	k1gFnPc4	ochechule
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
býložraví	býložravý	k2eAgMnPc1d1	býložravý
savci	savec	k1gMnPc1	savec
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
adaptováni	adaptovat	k5eAaBmNgMnP	adaptovat
pro	pro	k7c4	pro
život	život	k1gInSc4	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gFnPc1	jejich
paže	paže	k1gFnPc1	paže
jsou	být	k5eAaImIp3nP	být
modifikovány	modifikovat	k5eAaBmNgFnP	modifikovat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
pádel	pádlo	k1gNnPc2	pádlo
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
nohy	noha	k1gFnPc4	noha
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgFnPc4d1	zadní
končetiny	končetina	k1gFnPc4	končetina
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
rudimenty	rudiment	k1gInPc1	rudiment
pánve	pánev	k1gFnSc2	pánev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
silnou	silný	k2eAgFnSc4d1	silná
vrstvu	vrstva	k1gFnSc4	vrstva
podkožního	podkožní	k2eAgInSc2d1	podkožní
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
srst	srst	k1gFnSc4	srst
ani	ani	k8xC	ani
boltce	boltec	k1gInPc4	boltec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
mořští	mořský	k2eAgMnPc1d1	mořský
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
osidlují	osidlovat	k5eAaImIp3nP	osidlovat
brakické	brakický	k2eAgFnPc1d1	brakická
vody	voda	k1gFnPc1	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
živící	živící	k2eAgFnSc4d1	živící
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
řasami	řasa	k1gFnPc7	řasa
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
neobyčejně	obyčejně	k6eNd1	obyčejně
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
střevo	střevo	k1gNnSc4	střevo
a	a	k8xC	a
výměnný	výměnný	k2eAgInSc4d1	výměnný
dentální	dentální	k2eAgInSc4d1	dentální
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Čelist	čelist	k1gFnSc1	čelist
a	a	k8xC	a
pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
rypec	rypec	k1gInSc1	rypec
jsou	být	k5eAaImIp3nP	být
stočeny	stočit	k5eAaPmNgInP	stočit
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
srůstu	srůst	k1gInSc3	srůst
části	část	k1gFnSc2	část
krčních	krční	k2eAgInPc2d1	krční
obratlů	obratel	k1gInPc2	obratel
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
kostra	kostra	k1gFnSc1	kostra
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
.	.	kIx.	.
</s>
<s>
Plíce	plíce	k1gFnPc1	plíce
jsou	být	k5eAaImIp3nP	být
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
uloženy	uložen	k2eAgInPc1d1	uložen
na	na	k7c6	na
dorsální	dorsální	k2eAgFnSc6d1	dorsální
straně	strana	k1gFnSc6	strana
nad	nad	k7c7	nad
útrobami	útroba	k1gFnPc7	útroba
<g/>
.	.	kIx.	.
</s>
<s>
Mléčné	mléčný	k2eAgFnPc1d1	mléčná
bradavky	bradavka	k1gFnPc1	bradavka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
blízko	blízko	k7c2	blízko
paží	paží	k1gNnSc2	paží
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
mládě	mládě	k1gNnSc4	mládě
při	při	k7c6	při
kojení	kojení	k1gNnSc6	kojení
přidržovat	přidržovat	k5eAaImF	přidržovat
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
sirény	siréna	k1gFnSc2	siréna
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
čeledi	čeleď	k1gFnPc4	čeleď
<g/>
:	:	kIx,	:
kapustňákovití	kapustňákovitý	k2eAgMnPc1d1	kapustňákovitý
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
dugongovitých	dugongovití	k1gMnPc2	dugongovití
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
lebky	lebka	k1gFnSc2	lebka
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Dugongové	dugong	k1gMnPc1	dugong
mají	mít	k5eAaImIp3nP	mít
ocas	ocas	k1gInSc4	ocas
podobný	podobný	k2eAgInSc4d1	podobný
velrybám	velryba	k1gFnPc3	velryba
-	-	kIx~	-
vidličnatě	vidličnatě	k6eAd1	vidličnatě
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
kapustňáků	kapustňák	k1gMnPc2	kapustňák
má	mít	k5eAaImIp3nS	mít
okrouhlý	okrouhlý	k2eAgInSc1d1	okrouhlý
(	(	kIx(	(
<g/>
lopatovitý	lopatovitý	k2eAgInSc1d1	lopatovitý
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapustňáci	kapustňák	k1gMnPc1	kapustňák
jsou	být	k5eAaImIp3nP	být
sladkovodní	sladkovodní	k2eAgMnPc1d1	sladkovodní
až	až	k9	až
brakičtí	brakický	k2eAgMnPc1d1	brakický
a	a	k8xC	a
dugongové	dugong	k1gMnPc1	dugong
(	(	kIx(	(
<g/>
moroňové	moroň	k1gMnPc1	moroň
<g/>
)	)	kIx)	)
mořští	mořský	k2eAgMnPc1d1	mořský
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
sirény	siréna	k1gFnSc2	siréna
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
námořnických	námořnický	k2eAgFnPc2d1	námořnická
pověstí	pověst	k1gFnPc2	pověst
o	o	k7c6	o
mořských	mořský	k2eAgFnPc6d1	mořská
pannách	panna	k1gFnPc6	panna
lákajících	lákající	k2eAgFnPc6d1	lákající
námořníky	námořník	k1gMnPc7	námořník
do	do	k7c2	do
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozbijí	rozbít	k5eAaPmIp3nP	rozbít
loď	loď	k1gFnSc4	loď
o	o	k7c4	o
skaliska	skalisko	k1gNnPc4	skalisko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
první	první	k4xOgInPc1	první
druhy	druh	k1gInPc1	druh
sirén	siréna	k1gFnPc2	siréna
<g/>
,	,	kIx,	,
přírodovědci	přírodovědec	k1gMnPc1	přírodovědec
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
dala	dát	k5eAaPmAgFnS	dát
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
pověstí	pověst	k1gFnPc2	pověst
podnět	podnět	k1gInSc1	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
ochechule	ochechule	k1gFnSc2	ochechule
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
vytváření	vytváření	k1gNnSc2	vytváření
českého	český	k2eAgNnSc2d1	české
zoologického	zoologický	k2eAgNnSc2d1	zoologické
názvosloví	názvosloví	k1gNnSc2	názvosloví
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
dugongovití	dugongovití	k1gMnPc5	dugongovití
(	(	kIx(	(
<g/>
Dugongidae	Dugongidae	k1gFnSc1	Dugongidae
<g/>
)	)	kIx)	)
Dugong	dugong	k1gMnSc1	dugong
indický	indický	k2eAgMnSc1d1	indický
-	-	kIx~	-
dugong	dugong	k1gMnSc1	dugong
(	(	kIx(	(
<g/>
Dugong	dugong	k1gMnSc1	dugong
dugong	dugong	k1gMnSc1	dugong
<g/>
)	)	kIx)	)
Koroun	koroun	k1gMnSc1	koroun
bezzubý	bezzubý	k2eAgMnSc1d1	bezzubý
-	-	kIx~	-
Stellerova	Stellerův	k2eAgFnSc1d1	Stellerův
mořská	mořský	k2eAgFnSc1d1	mořská
kráva	kráva	k1gFnSc1	kráva
(	(	kIx(	(
<g/>
Hydrodamalis	Hydrodamalis	k1gFnSc1	Hydrodamalis
gigas	gigas	k1gMnSc1	gigas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
Čeleď	čeleď	k1gFnSc4	čeleď
kapustňákovití	kapustňákovitý	k2eAgMnPc1d1	kapustňákovitý
(	(	kIx(	(
<g/>
Trichechidae	Trichechidae	k1gNnSc7	Trichechidae
<g/>
)	)	kIx)	)
Kapustňák	kapustňák	k1gMnSc1	kapustňák
širokonosý	širokonosý	k2eAgMnSc1d1	širokonosý
(	(	kIx(	(
<g/>
Trichechus	Trichechus	k1gMnSc1	Trichechus
manatus	manatus	k1gMnSc1	manatus
<g/>
)	)	kIx)	)
Kapustňák	kapustňák	k1gMnSc1	kapustňák
senegalský	senegalský	k2eAgMnSc1d1	senegalský
(	(	kIx(	(
<g/>
Trichechus	Trichechus	k1gInSc1	Trichechus
senegalensis	senegalensis	k1gFnSc2	senegalensis
<g/>
)	)	kIx)	)
Kapustňák	kapustňák	k1gMnSc1	kapustňák
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
(	(	kIx(	(
<g/>
Trichechus	Trichechus	k1gInSc1	Trichechus
inunguis	inunguis	k1gFnSc2	inunguis
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
siréna	siréna	k1gFnSc1	siréna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sirény	Siréna	k1gFnSc2	Siréna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc4	taxon
Sirenia	Sirenium	k1gNnSc2	Sirenium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
http://savci.upol.cz/sireny.htm	[url]	k6eAd1	http://savci.upol.cz/sireny.htm
Africké	africký	k2eAgFnPc1d1	africká
sirény	siréna	k1gFnPc1	siréna
-	-	kIx~	-
AFRIKAonline	AFRIKAonlin	k1gInSc5	AFRIKAonlin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
