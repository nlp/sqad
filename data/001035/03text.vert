<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Valašské	valašský	k2eAgNnSc1d1	Valašské
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
hráč	hráč	k1gMnSc1	hráč
FC	FC	kA	FC
Slovan	Slovan	k1gMnSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Česko	Česko	k1gNnSc4	Česko
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitel	držitel	k1gMnSc1	držitel
bronzové	bronzový	k2eAgFnSc2d1	bronzová
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2004	[number]	k4	2004
a	a	k8xC	a
s	s	k7c7	s
5	[number]	k4	5
góly	gól	k1gInPc7	gól
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
tohoto	tento	k3xDgInSc2	tento
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
41	[number]	k4	41
góly	gól	k1gInPc7	gól
druhý	druhý	k4xOgMnSc1	druhý
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
české	český	k2eAgFnSc2d1	Česká
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
(	(	kIx(	(
<g/>
za	za	k7c7	za
Janem	Jan	k1gMnSc7	Jan
Kollerem	Koller	k1gMnSc7	Koller
s	s	k7c7	s
55	[number]	k4	55
góly	gól	k1gInPc7	gól
<g/>
)	)	kIx)	)
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Klubu	klub	k1gInSc2	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Šmicerem	Šmicer	k1gMnSc7	Šmicer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Liverpoolu	Liverpool	k1gInSc2	Liverpool
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prvním	první	k4xOgMnSc6	první
českým	český	k2eAgMnSc7d1	český
vítězem	vítěz	k1gMnSc7	vítěz
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
znám	znát	k5eAaImIp1nS	znát
svým	svůj	k3xOyFgInSc7	svůj
přímočarým	přímočarý	k2eAgInSc7d1	přímočarý
tahem	tah	k1gInSc7	tah
na	na	k7c4	na
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc7d1	dobrá
orientací	orientace	k1gFnSc7	orientace
v	v	k7c6	v
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
kariéru	kariéra	k1gFnSc4	kariéra
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
TJ	tj	kA	tj
Vigantice	Vigantika	k1gFnSc6	Vigantika
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
mládeže	mládež	k1gFnSc2	mládež
zamířil	zamířit	k5eAaPmAgInS	zamířit
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
TJ	tj	kA	tj
Rožnov	Rožnov	k1gInSc1	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
FC	FC	kA	FC
Baník	Baník	k1gInSc1	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
propracoval	propracovat	k5eAaPmAgInS	propracovat
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
týmu	tým	k1gInSc2	tým
Baníku	Baník	k1gInSc2	Baník
<g/>
,	,	kIx,	,
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
české	český	k2eAgFnSc6d1	Česká
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hráčem	hráč	k1gMnSc7	hráč
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
anketě	anketa	k1gFnSc6	anketa
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
Talent	talent	k1gInSc4	talent
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
odehrál	odehrát	k5eAaPmAgMnS	odehrát
celkem	celek	k1gInSc7	celek
76	[number]	k4	76
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
22	[number]	k4	22
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nadějný	nadějný	k2eAgMnSc1d1	nadějný
hráč	hráč	k1gMnSc1	hráč
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
za	za	k7c4	za
cca	cca	kA	cca
3,2	[number]	k4	3,2
milionu	milion	k4xCgInSc2	milion
£	£	k?	£
do	do	k7c2	do
anglického	anglický	k2eAgInSc2d1	anglický
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
velkoklubu	velkoklub	k1gInSc6	velkoklub
dostal	dostat	k5eAaPmAgMnS	dostat
dres	dres	k1gInSc4	dres
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
remíza	remíza	k1gFnSc1	remíza
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
jediný	jediný	k2eAgInSc1d1	jediný
zápas	zápas	k1gInSc1	zápas
sezóny	sezóna	k1gFnSc2	sezóna
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgMnSc4	který
mladý	mladý	k2eAgMnSc1d1	mladý
útočník	útočník	k1gMnSc1	útočník
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
dvakrát	dvakrát	k6eAd1	dvakrát
skóroval	skórovat	k5eAaBmAgInS	skórovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
utkání	utkání	k1gNnSc6	utkání
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	League	k1gNnSc4	League
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2002	[number]	k4	2002
proti	proti	k7c3	proti
Boltonu	Bolton	k1gInSc3	Bolton
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Liverpool	Liverpool	k1gInSc1	Liverpool
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
venku	venku	k6eAd1	venku
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
pak	pak	k6eAd1	pak
skóroval	skórovat	k5eAaBmAgMnS	skórovat
ještě	ještě	k9	ještě
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
zápasech	zápas	k1gInPc6	zápas
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2002	[number]	k4	2002
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Boltonu	Bolton	k1gInSc3	Bolton
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
<g/>
,	,	kIx,	,
góly	gól	k1gInPc4	gól
v	v	k7c4	v
45	[number]	k4	45
<g/>
.	.	kIx.	.
a	a	k8xC	a
72	[number]	k4	72
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2002	[number]	k4	2002
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
West	West	k1gMnSc1	West
Bromwich	Bromwicha	k1gFnPc2	Bromwicha
<g />
.	.	kIx.	.
</s>
<s>
Albion	Albion	k1gInSc1	Albion
<g/>
,	,	kIx,	,
gól	gól	k1gInSc1	gól
v	v	k7c6	v
56	[number]	k4	56
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Fulhamu	Fulham	k1gInSc3	Fulham
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
86	[number]	k4	86
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
<g />
.	.	kIx.	.
</s>
<s>
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Sunderlandu	Sunderland	k1gInSc3	Sunderland
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
68	[number]	k4	68
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
West	West	k1gInSc1	West
Ham	ham	k0	ham
United	United	k1gInSc4	United
FC	FC	kA	FC
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
West	West	k1gInSc1	West
Bromwich	Bromwich	k1gInSc1	Bromwich
Albion	Albion	k1gInSc4	Albion
<g/>
,	,	kIx,	,
góly	gól	k1gInPc4	gól
v	v	k7c4	v
47	[number]	k4	47
<g/>
.	.	kIx.	.
a	a	k8xC	a
84	[number]	k4	84
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
59	[number]	k4	59
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Ligovou	ligový	k2eAgFnSc4d1	ligová
sezónu	sezóna	k1gFnSc4	sezóna
zakončil	zakončit	k5eAaPmAgInS	zakončit
Liverpool	Liverpool	k1gInSc1	Liverpool
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
během	během	k7c2	během
27	[number]	k4	27
zápasů	zápas	k1gInPc2	zápas
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
přidal	přidat	k5eAaPmAgMnS	přidat
v	v	k7c6	v
pohárových	pohárový	k2eAgFnPc6d1	pohárová
soutěžích	soutěž	k1gFnPc6	soutěž
-	-	kIx~	-
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
v	v	k7c6	v
Anglickém	anglický	k2eAgInSc6d1	anglický
ligovém	ligový	k2eAgInSc6d1	ligový
poháru	pohár	k1gInSc6	pohár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
Liverpool	Liverpool	k1gInSc1	Liverpool
porazil	porazit	k5eAaPmAgInS	porazit
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
Liverpool	Liverpool	k1gInSc1	Liverpool
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgInS	získat
7	[number]	k4	7
<g/>
.	.	kIx.	.
trofej	trofej	k1gFnSc1	trofej
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
dosavadním	dosavadní	k2eAgInPc3d1	dosavadní
výborným	výborný	k2eAgInPc3d1	výborný
výkonům	výkon	k1gInPc3	výkon
očekávala	očekávat	k5eAaImAgFnS	očekávat
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
útočníka	útočník	k1gMnSc2	útočník
další	další	k2eAgNnPc1d1	další
vydařená	vydařený	k2eAgNnPc1d1	vydařené
vystoupení	vystoupení	k1gNnPc1	vystoupení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
utkání	utkání	k1gNnSc6	utkání
zlomeninu	zlomenina	k1gFnSc4	zlomenina
kotníku	kotník	k1gInSc2	kotník
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
na	na	k7c4	na
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
branky	branka	k1gFnPc1	branka
<g/>
:	:	kIx,	:
v	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	League	k1gNnSc4	League
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Leedsu	Leeds	k1gInSc3	Leeds
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
gól	gól	k1gInSc1	gól
ve	v	k7c6	v
42	[number]	k4	42
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
UEFA	UEFA	kA	UEFA
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Olympique	Olympique	k1gNnSc3	Olympique
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
55	[number]	k4	55
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2004	[number]	k4	2004
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
EURO	euro	k1gNnSc1	euro
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
stal	stát	k5eAaPmAgMnS	stát
s	s	k7c7	s
5	[number]	k4	5
góly	gól	k1gInPc7	gól
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
se	se	k3xPyFc4	se
udála	udát	k5eAaPmAgFnS	udát
změna	změna	k1gFnSc1	změna
na	na	k7c6	na
trenérském	trenérský	k2eAgInSc6d1	trenérský
postu	post	k1gInSc6	post
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc2d1	francouzský
kouče	kouč	k1gMnSc2	kouč
Gérarda	Gérard	k1gMnSc2	Gérard
Houlliera	Houllier	k1gMnSc2	Houllier
(	(	kIx(	(
<g/>
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
)	)	kIx)	)
nahradil	nahradit	k5eAaPmAgMnS	nahradit
španělský	španělský	k2eAgMnSc1d1	španělský
trenér	trenér	k1gMnSc1	trenér
Rafael	Rafael	k1gMnSc1	Rafael
Benítez	Benítez	k1gMnSc1	Benítez
(	(	kIx(	(
<g/>
přišel	přijít	k5eAaPmAgInS	přijít
z	z	k7c2	z
Valencie	Valencie	k1gFnSc2	Valencie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sezóna	sezóna	k1gFnSc1	sezóna
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
hráče	hráč	k1gMnSc4	hráč
nejvydařenější	vydařený	k2eAgFnSc2d3	nejvydařenější
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
odešel	odejít	k5eAaPmAgMnS	odejít
Michael	Michael	k1gMnSc1	Michael
Owen	Owen	k1gMnSc1	Owen
do	do	k7c2	do
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
a	a	k8xC	a
Emile	Emil	k1gMnSc5	Emil
Heskey	Heskeum	k1gNnPc7	Heskeum
do	do	k7c2	do
Birminghamu	Birmingham	k1gInSc2	Birmingham
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Baroš	Baroš	k1gMnSc1	Baroš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
útočníkem	útočník	k1gMnSc7	útočník
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lize	liga	k1gFnSc6	liga
nasázel	nasázet	k5eAaPmAgInS	nasázet
9	[number]	k4	9
gólů	gól	k1gInPc2	gól
včetně	včetně	k7c2	včetně
hattricku	hattrick	k1gInSc2	hattrick
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
Crystal	Crystal	k1gFnSc2	Crystal
Palace	Palace	k1gFnSc2	Palace
FC	FC	kA	FC
<g/>
,	,	kIx,	,
v	v	k7c6	v
Anglickém	anglický	k2eAgInSc6d1	anglický
ligovém	ligový	k2eAgInSc6d1	ligový
poháru	pohár	k1gInSc6	pohár
dal	dát	k5eAaPmAgMnS	dát
dvě	dva	k4xCgFnPc4	dva
branky	branka	k1gFnPc4	branka
Millwallu	Millwall	k1gInSc2	Millwall
a	a	k8xC	a
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
rovněž	rovněž	k9	rovněž
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
AS	as	k1gNnSc3	as
Monaco	Monaco	k6eAd1	Monaco
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
a	a	k8xC	a
proti	proti	k7c3	proti
Leverkusenu	Leverkusen	k2eAgFnSc4d1	Leverkusen
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ligové	ligový	k2eAgInPc1d1	ligový
góly	gól	k1gInPc1	gól
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
ve	v	k7c6	v
48	[number]	k4	48
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2004	[number]	k4	2004
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Norwich	Norwich	k1gMnSc1	Norwich
City	City	k1gFnSc3	City
FC	FC	kA	FC
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
ve	v	k7c6	v
23	[number]	k4	23
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Fulhamu	Fulham	k1gInSc3	Fulham
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Blackburn	Blackburn	k1gInSc1	Blackburn
Rovers	Rovers	k1gInSc4	Rovers
FC	FC	kA	FC
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
53	[number]	k4	53
<g />
.	.	kIx.	.
</s>
<s>
<g/>
minutě	minuta	k1gFnSc3	minuta
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
venku	venku	k6eAd1	venku
proti	proti	k7c3	proti
Crystal	Crystal	k1gFnSc3	Crystal
Palace	Palace	k1gFnSc2	Palace
FC	FC	kA	FC
<g/>
,	,	kIx,	,
góly	gól	k1gInPc7	gól
v	v	k7c4	v
23	[number]	k4	23
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Newcastle	Newcastle	k1gFnSc3	Newcastle
United	United	k1gMnSc1	United
FC	FC	kA	FC
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
65	[number]	k4	65
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Fulhamu	Fulham	k1gInSc3	Fulham
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
77	[number]	k4	77
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
když	když	k8xS	když
Liverpool	Liverpool	k1gInSc1	Liverpool
dokráčel	dokráčet	k5eAaPmAgInS	dokráčet
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgMnSc6	který
porazil	porazit	k5eAaPmAgMnS	porazit
na	na	k7c4	na
pokutové	pokutový	k2eAgInPc4d1	pokutový
kopy	kop	k1gInPc4	kop
AC	AC	kA	AC
Milán	Milán	k1gInSc4	Milán
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgMnSc1	svůj
5	[number]	k4	5
<g/>
.	.	kIx.	.
titul	titul	k1gInSc4	titul
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
prohrával	prohrávat	k5eAaImAgInS	prohrávat
již	již	k6eAd1	již
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
po	po	k7c6	po
poločase	poločas	k1gInSc6	poločas
<g/>
.	.	kIx.	.
</s>
<s>
Baroš	Baroš	k1gMnSc1	Baroš
měl	mít	k5eAaImAgMnS	mít
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vyrovnávacím	vyrovnávací	k2eAgInSc6d1	vyrovnávací
gólu	gól	k1gInSc6	gól
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
směřovala	směřovat	k5eAaImAgFnS	směřovat
přihrávka	přihrávka	k1gFnSc1	přihrávka
do	do	k7c2	do
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
posunul	posunout	k5eAaPmAgMnS	posunout
ji	on	k3xPp3gFnSc4	on
patičkou	patička	k1gFnSc7	patička
na	na	k7c4	na
nabíhajícího	nabíhající	k2eAgMnSc4d1	nabíhající
Stevena	Steven	k2eAgMnSc4d1	Steven
Gerrarda	Gerrard	k1gMnSc4	Gerrard
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
faulován	faulovat	k5eAaBmNgMnS	faulovat
<g/>
.	.	kIx.	.
</s>
<s>
Nařízený	nařízený	k2eAgInSc4d1	nařízený
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
Xabi	Xabi	k1gNnSc4	Xabi
Alonso	Alonsa	k1gFnSc5	Alonsa
<g/>
.	.	kIx.	.
</s>
<s>
Milana	Milan	k1gMnSc4	Milan
Baroše	Baroš	k1gMnSc4	Baroš
pak	pak	k6eAd1	pak
v	v	k7c6	v
85	[number]	k4	85
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Djibril	Djibril	k1gInSc4	Djibril
Cissé	Cissý	k2eAgInPc1d1	Cissý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglickém	anglický	k2eAgInSc6d1	anglický
ligovém	ligový	k2eAgInSc6d1	ligový
poháru	pohár	k1gInSc6	pohár
se	se	k3xPyFc4	se
Liverpool	Liverpool	k1gInSc1	Liverpool
rovněž	rovněž	k9	rovněž
propracoval	propracovat	k5eAaPmAgInS	propracovat
až	až	k6eAd1	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
proti	proti	k7c3	proti
Chelsea	Chelse	k2eAgNnPc4d1	Chelse
FC	FC	kA	FC
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohrál	prohrát	k5eAaPmAgInS	prohrát
jej	on	k3xPp3gMnSc4	on
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Baroš	Baroš	k1gMnSc1	Baroš
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
utkání	utkání	k1gNnSc2	utkání
v	v	k7c6	v
74	[number]	k4	74
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
13	[number]	k4	13
vstřelenými	vstřelený	k2eAgFnPc7d1	vstřelená
brankami	branka	k1gFnPc7	branka
zakončil	zakončit	k5eAaPmAgMnS	zakončit
sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c6	na
děleném	dělený	k2eAgNnSc6d1	dělené
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
tabulky	tabulka	k1gFnSc2	tabulka
střelců	střelec	k1gMnPc2	střelec
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
Barošova	Barošův	k2eAgFnSc1d1	Barošova
budoucnost	budoucnost	k1gFnSc1	budoucnost
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
navíc	navíc	k6eAd1	navíc
získal	získat	k5eAaPmAgInS	získat
na	na	k7c4	na
hrot	hrot	k1gInSc4	hrot
konkurenta	konkurent	k1gMnSc2	konkurent
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Southamptonu	Southampton	k1gInSc2	Southampton
přišel	přijít	k5eAaPmAgMnS	přijít
vytáhlý	vytáhlý	k2eAgMnSc1d1	vytáhlý
útočník	útočník	k1gMnSc1	útočník
Peter	Peter	k1gMnSc1	Peter
Crouch	Crouch	k1gMnSc1	Crouch
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
odehrál	odehrát	k5eAaPmAgMnS	odehrát
jen	jen	k9	jen
dvě	dva	k4xCgNnPc1	dva
utkání	utkání	k1gNnPc1	utkání
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
střídající	střídající	k2eAgMnSc1d1	střídající
hráč	hráč	k1gMnSc1	hráč
(	(	kIx(	(
<g/>
gól	gól	k1gInSc1	gól
nevstřelil	vstřelit	k5eNaPmAgInS	vstřelit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Rafael	Rafael	k1gMnSc1	Rafael
Benítez	Benítez	k1gMnSc1	Benítez
začal	začít	k5eAaPmAgMnS	začít
upřednostňovat	upřednostňovat	k5eAaImF	upřednostňovat
Djibrila	Djibril	k1gMnSc4	Djibril
Cissého	Cissý	k2eAgMnSc4d1	Cissý
<g/>
,	,	kIx,	,
Fernanda	Fernando	k1gNnSc2	Fernando
Morientese	Morientese	k1gFnSc2	Morientese
a	a	k8xC	a
Petera	Peter	k1gMnSc2	Peter
Crouche	Crouch	k1gMnSc2	Crouch
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
Baroše	Baroš	k1gMnSc4	Baroš
stál	stát	k5eAaImAgMnS	stát
David	David	k1gMnSc1	David
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Leary	Lear	k1gMnPc4	Lear
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
Aston	Astona	k1gFnPc2	Astona
Villy	Villa	k1gFnSc2	Villa
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
český	český	k2eAgMnSc1d1	český
hráč	hráč	k1gMnSc1	hráč
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
přestoupit	přestoupit	k5eAaPmF	přestoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
popřel	popřít	k5eAaPmAgMnS	popřít
rozepře	rozepřít	k5eAaPmIp3nS	rozepřít
se	se	k3xPyFc4	se
španělským	španělský	k2eAgMnSc7d1	španělský
trenérem	trenér	k1gMnSc7	trenér
Liverpoolu	Liverpool	k1gInSc2	Liverpool
Benítezem	Benítez	k1gInSc7	Benítez
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
českého	český	k2eAgMnSc4d1	český
útočníka	útočník	k1gMnSc4	útočník
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
i	i	k8xC	i
francouzský	francouzský	k2eAgInSc1d1	francouzský
klub	klub	k1gInSc1	klub
Olympique	Olympique	k1gInSc1	Olympique
Lyon	Lyon	k1gInSc1	Lyon
vedený	vedený	k2eAgInSc1d1	vedený
Gérardem	Gérard	k1gMnSc7	Gérard
Houllierem	Houllier	k1gMnSc7	Houllier
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
dříve	dříve	k6eAd2	dříve
Baroš	Baroš	k1gMnSc1	Baroš
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
<g/>
,	,	kIx,	,
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
FC	FC	kA	FC
Schalke	Schalke	k1gInSc4	Schalke
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Baroš	Baroš	k1gMnSc1	Baroš
upřednostnil	upřednostnit	k5eAaPmAgMnS	upřednostnit
setrvání	setrvání	k1gNnSc4	setrvání
v	v	k7c4	v
anglické	anglický	k2eAgInPc4d1	anglický
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2005	[number]	k4	2005
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
za	za	k7c4	za
6,5	[number]	k4	6,5
milionu	milion	k4xCgInSc2	milion
£	£	k?	£
do	do	k7c2	do
birminghamského	birminghamský	k2eAgInSc2d1	birminghamský
klubu	klub	k1gInSc2	klub
Aston	Aston	k1gMnSc1	Aston
Villa	Villa	k1gMnSc1	Villa
FC	FC	kA	FC
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podepsal	podepsat	k5eAaPmAgMnS	podepsat
čtyřletý	čtyřletý	k2eAgInSc4d1	čtyřletý
kontrakt	kontrakt	k1gInSc4	kontrakt
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
dres	dres	k1gInSc4	dres
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
za	za	k7c4	za
Aston	Aston	k1gNnSc4	Aston
Villu	Vill	k1gInSc2	Vill
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
jediný	jediný	k2eAgInSc4d1	jediný
gól	gól	k1gInSc4	gól
zápasu	zápas	k1gInSc2	zápas
proti	proti	k7c3	proti
Blackburnu	Blackburno	k1gNnSc3	Blackburno
Rovers	Roversa	k1gFnPc2	Roversa
(	(	kIx(	(
<g/>
dostal	dostat	k5eAaPmAgInS	dostat
rovněž	rovněž	k9	rovněž
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
dokončil	dokončit	k5eAaPmAgInS	dokončit
s	s	k7c7	s
8	[number]	k4	8
vstřelenými	vstřelený	k2eAgInPc7d1	vstřelený
góly	gól	k1gInPc7	gól
ve	v	k7c6	v
25	[number]	k4	25
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
novým	nový	k2eAgMnSc7d1	nový
trenérem	trenér	k1gMnSc7	trenér
Martinem	Martin	k1gMnSc7	Martin
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neillem	Neill	k1gMnSc7	Neill
začal	začít	k5eAaPmAgMnS	začít
další	další	k2eAgFnSc4d1	další
sezónu	sezóna	k1gFnSc4	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
s	s	k7c7	s
kolumbijským	kolumbijský	k2eAgMnSc7d1	kolumbijský
forwardem	forward	k1gMnSc7	forward
Juanem	Juan	k1gMnSc7	Juan
Pablem	Pabl	k1gMnSc7	Pabl
Ángelem	Ángel	k1gMnSc7	Ángel
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
zranil	zranit	k5eAaPmAgMnS	zranit
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
střelecký	střelecký	k2eAgInSc1d1	střelecký
účet	účet	k1gInSc1	účet
sezóny	sezóna	k1gFnSc2	sezóna
otevřel	otevřít	k5eAaPmAgInS	otevřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Sheffield	Sheffield	k1gInSc4	Sheffield
United	United	k1gInSc1	United
FC	FC	kA	FC
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
příliš	příliš	k6eAd1	příliš
nepřesvědčilo	přesvědčit	k5eNaPmAgNnS	přesvědčit
trenéra	trenér	k1gMnSc4	trenér
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
útočník	útočník	k1gMnSc1	útočník
dostával	dostávat	k5eAaImAgMnS	dostávat
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týmu	tým	k1gInSc6	tým
Aston	Aston	k1gInSc1	Aston
Villy	Vill	k1gInPc1	Vill
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
gól	gól	k1gInSc4	gól
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Villy	Villa	k1gFnSc2	Villa
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
FA	fa	k1gNnSc4	fa
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
střídal	střídat	k5eAaImAgMnS	střídat
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
v	v	k7c6	v
53	[number]	k4	53
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
Ángela	Ángel	k1gMnSc4	Ángel
a	a	k8xC	a
v	v	k7c6	v
74	[number]	k4	74
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jediný	jediný	k2eAgInSc4d1	jediný
gól	gól	k1gInSc4	gól
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
odehrál	odehrát	k5eAaPmAgMnS	odehrát
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
v	v	k7c4	v
Aston	Aston	k1gNnSc4	Aston
Ville	Ville	k1gFnSc2	Ville
51	[number]	k4	51
utkání	utkání	k1gNnSc2	utkání
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
14	[number]	k4	14
gólů	gól	k1gInPc2	gól
(	(	kIx(	(
<g/>
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ligovou	ligový	k2eAgFnSc4d1	ligová
bilanci	bilance	k1gFnSc4	bilance
má	mít	k5eAaImIp3nS	mít
42	[number]	k4	42
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
9	[number]	k4	9
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednovém	lednový	k2eAgInSc6d1	lednový
přestupním	přestupní	k2eAgInSc6d1	přestupní
termínu	termín	k1gInSc6	termín
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
do	do	k7c2	do
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
transferu	transfer	k1gInSc2	transfer
byla	být	k5eAaImAgFnS	být
i	i	k9	i
výměna	výměna	k1gFnSc1	výměna
norského	norský	k2eAgMnSc2d1	norský
hráče	hráč	k1gMnSc2	hráč
Johna	John	k1gMnSc2	John
Carewa	Carewa	k1gMnSc1	Carewa
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
Baroš	Baroš	k1gMnSc1	Baroš
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
klubu	klub	k1gInSc2	klub
Olympique	Olympiqu	k1gFnSc2	Olympiqu
Lyon	Lyon	k1gInSc1	Lyon
<g/>
.	.	kIx.	.
</s>
<s>
Shledal	shledat	k5eAaPmAgMnS	shledat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
trenérem	trenér	k1gMnSc7	trenér
z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
Gérardem	Gérard	k1gMnSc7	Gérard
Houllierem	Houllier	k1gMnSc7	Houllier
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
debutoval	debutovat	k5eAaBmAgMnS	debutovat
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
lize	liga	k1gFnSc6	liga
proti	proti	k7c3	proti
FC	FC	kA	FC
Girondins	Girondins	k1gInSc1	Girondins
de	de	k?	de
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
58	[number]	k4	58
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Lyon	Lyon	k1gInSc1	Lyon
prohrál	prohrát	k5eAaPmAgInS	prohrát
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
francouzské	francouzský	k2eAgFnSc2d1	francouzská
ligy	liga	k1gFnSc2	liga
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
svým	svůj	k3xOyFgMnSc7	svůj
prvním	první	k4xOgInSc7	první
gólem	gól	k1gInSc7	gól
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Lyonu	Lyon	k1gInSc2	Lyon
remízu	remíz	k1gInSc2	remíz
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
OGC	OGC	kA	OGC
Nice	Nice	k1gFnSc7	Nice
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
39	[number]	k4	39
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Stade	Stad	k1gInSc5	Stad
Rennais	Rennais	k1gInSc1	Rennais
FC	FC	kA	FC
si	se	k3xPyFc3	se
před	před	k7c7	před
kamerunským	kamerunský	k2eAgMnSc7d1	kamerunský
hráčem	hráč	k1gMnSc7	hráč
soupeře	soupeř	k1gMnSc2	soupeř
Stéphanem	Stéphan	k1gMnSc7	Stéphan
Mbiou	Mbia	k1gFnSc7	Mbia
ucpával	ucpávat	k5eAaImAgInS	ucpávat
nos	nos	k1gInSc1	nos
a	a	k8xC	a
mával	mávat	k5eAaImAgInS	mávat
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
rozháněl	rozhánět	k5eAaImAgInS	rozhánět
zápach	zápach	k1gInSc1	zápach
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
gesto	gesto	k1gNnSc1	gesto
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
rozruch	rozruch	k1gInSc4	rozruch
a	a	k8xC	a
Baroš	Baroš	k1gMnSc1	Baroš
byl	být	k5eAaImAgMnS	být
nařčen	nařknout	k5eAaPmNgMnS	nařknout
z	z	k7c2	z
rasistického	rasistický	k2eAgNnSc2d1	rasistické
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
útočník	útočník	k1gMnSc1	útočník
se	se	k3xPyFc4	se
hájil	hájit	k5eAaImAgMnS	hájit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
gesto	gesto	k1gNnSc1	gesto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nemělo	mít	k5eNaImAgNnS	mít
s	s	k7c7	s
rasismem	rasismus	k1gInSc7	rasismus
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
poslal	poslat	k5eAaPmAgInS	poslat
kamerunskému	kamerunský	k2eAgMnSc3d1	kamerunský
hráči	hráč	k1gMnSc3	hráč
omluvný	omluvný	k2eAgInSc4d1	omluvný
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
nepřesvědčilo	přesvědčit	k5eNaPmAgNnS	přesvědčit
představitele	představitel	k1gMnPc4	představitel
organizací	organizace	k1gFnPc2	organizace
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
ani	ani	k8xC	ani
samotného	samotný	k2eAgMnSc4d1	samotný
Mbiu	Mbia	k1gMnSc4	Mbia
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
deníku	deník	k1gInSc6	deník
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Equipe	Equip	k1gInSc5	Equip
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Barošovi	Baroš	k1gMnSc3	Baroš
neodpustí	odpustit	k5eNaPmIp3nS	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
komise	komise	k1gFnSc1	komise
neshledala	shledat	k5eNaPmAgFnS	shledat
Baroše	Baroš	k1gMnPc4	Baroš
vinným	vinný	k1gMnSc7	vinný
z	z	k7c2	z
rasistického	rasistický	k2eAgNnSc2d1	rasistické
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udělila	udělit	k5eAaPmAgFnS	udělit
mu	on	k3xPp3gMnSc3	on
3	[number]	k4	3
<g/>
-zápasový	ápasový	k2eAgInSc4d1	-zápasový
trest	trest	k1gInSc4	trest
za	za	k7c4	za
nesportovní	sportovní	k2eNgNnSc4d1	nesportovní
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
incidentu	incident	k1gInSc3	incident
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k9	i
francouzský	francouzský	k2eAgMnSc1d1	francouzský
krajně	krajně	k6eAd1	krajně
pravicový	pravicový	k2eAgMnSc1d1	pravicový
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Jean-Marie	Jean-Marie	k1gFnSc2	Jean-Marie
Le	Le	k1gMnSc4	Le
Pen	Pen	k1gMnSc4	Pen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
býval	bývat	k5eAaImAgMnS	bývat
sám	sám	k3xTgMnSc1	sám
často	často	k6eAd1	často
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
z	z	k7c2	z
rasismu	rasismus	k1gInSc2	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jej	on	k3xPp3gNnSc4	on
takové	takový	k3xDgNnSc4	takový
chování	chování	k1gNnSc4	chování
šokuje	šokovat	k5eAaBmIp3nS	šokovat
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
moc	moc	k6eAd1	moc
elegantní	elegantní	k2eAgNnSc1d1	elegantní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
Milanu	Milan	k1gMnSc3	Milan
Barošovi	Baroš	k1gMnSc3	Baroš
zadržen	zadržet	k5eAaPmNgMnS	zadržet
policií	policie	k1gFnSc7	policie
jeho	jeho	k3xOp3gMnPc3	jeho
vůz	vůz	k1gInSc1	vůz
Ferrari	Ferrari	k1gMnSc1	Ferrari
F	F	kA	F
<g/>
430	[number]	k4	430
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
mezi	mezi	k7c7	mezi
Lyonem	Lyon	k1gInSc7	Lyon
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Ženevou	Ženeva	k1gFnSc7	Ženeva
výrazně	výrazně	k6eAd1	výrazně
překročil	překročit	k5eAaPmAgMnS	překročit
povolenou	povolený	k2eAgFnSc4d1	povolená
130	[number]	k4	130
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
radarem	radar	k1gInSc7	radar
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
271	[number]	k4	271
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Šéf	šéf	k1gMnSc1	šéf
policie	policie	k1gFnSc2	policie
v	v	k7c6	v
Ainu	Ainus	k1gInSc6	Ainus
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
rychlostní	rychlostní	k2eAgInSc4d1	rychlostní
rekord	rekord	k1gInSc4	rekord
departmentu	department	k1gInSc2	department
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
překonat	překonat	k5eAaPmF	překonat
248	[number]	k4	248
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
motocyklisty	motocyklista	k1gMnSc2	motocyklista
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Lyon	Lyon	k1gInSc4	Lyon
dal	dát	k5eAaPmAgMnS	dát
Baroš	Baroš	k1gMnSc1	Baroš
ve	v	k7c6	v
24	[number]	k4	24
ligových	ligový	k2eAgInPc6d1	ligový
zápasech	zápas	k1gInPc6	zápas
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c6	na
hostování	hostování	k1gNnSc6	hostování
do	do	k7c2	do
Portsmouthu	Portsmouth	k1gInSc2	Portsmouth
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
odehrál	odehrát	k5eAaPmAgMnS	odehrát
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
United	United	k1gMnSc1	United
<g/>
.	.	kIx.	.
</s>
<s>
Sehrál	sehrát	k5eAaPmAgMnS	sehrát
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
během	během	k7c2	během
zisku	zisk	k1gInSc2	zisk
FA	fa	k1gNnSc2	fa
Cupu	cup	k1gInSc2	cup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
FA	fa	kA	fa
Cupu	cup	k1gInSc2	cup
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
proti	proti	k7c3	proti
témuž	týž	k3xTgMnSc3	týž
soupeři	soupeř	k1gMnSc3	soupeř
(	(	kIx(	(
<g/>
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
78	[number]	k4	78
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
odpískán	odpískán	k2eAgInSc4d1	odpískán
faul	faul	k1gInSc4	faul
v	v	k7c6	v
pokutovém	pokutový	k2eAgNnSc6d1	pokutové
území	území	k1gNnSc6	území
polskému	polský	k2eAgMnSc3d1	polský
brankáři	brankář	k1gMnSc3	brankář
Tomaszovi	Tomasz	k1gMnSc3	Tomasz
Kuszczakovi	Kuszczak	k1gMnSc3	Kuszczak
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c4	za
zákrok	zákrok	k1gInSc4	zákrok
dostal	dostat	k5eAaPmAgMnS	dostat
červenou	červený	k2eAgFnSc4d1	červená
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Nařízený	nařízený	k2eAgInSc4d1	nařízený
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
proměnil	proměnit	k5eAaPmAgInS	proměnit
Sulley	Sulle	k2eAgFnPc4d1	Sulle
Ali	Ali	k1gFnPc4	Ali
Muntari	Muntari	k1gNnSc2	Muntari
a	a	k8xC	a
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
favorita	favorit	k1gMnSc2	favorit
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Old	Olda	k1gFnPc2	Olda
Trafford	Trafforda	k1gFnPc2	Trafforda
překvapivě	překvapivě	k6eAd1	překvapivě
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
výsledkem	výsledek	k1gInSc7	výsledek
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
semifinálovém	semifinálový	k2eAgNnSc6d1	semifinálové
utkání	utkání	k1gNnSc6	utkání
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
West	West	k2eAgInSc1d1	West
Bromwiche	Bromwiche	k1gInSc1	Bromwiche
Albion	Albion	k1gInSc1	Albion
přihrával	přihrávat	k5eAaImAgMnS	přihrávat
v	v	k7c6	v
54	[number]	k4	54
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
nigerijskému	nigerijský	k2eAgMnSc3d1	nigerijský
útočníkovi	útočník	k1gMnSc3	útočník
Nwankwo	Nwankwo	k1gMnSc1	Nwankwo
Kanuovi	Kanu	k1gMnSc3	Kanu
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
a	a	k8xC	a
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
gól	gól	k1gInSc4	gól
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
porazil	porazit	k5eAaPmAgMnS	porazit
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
Cardiff	Cardiff	k1gMnSc1	Cardiff
City	City	k1gFnSc2	City
FC	FC	kA	FC
opět	opět	k6eAd1	opět
stejným	stejný	k2eAgInSc7d1	stejný
výsledkem	výsledek	k1gInSc7	výsledek
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
střídal	střídat	k5eAaImAgMnS	střídat
v	v	k7c6	v
87	[number]	k4	87
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
střelce	střelec	k1gMnSc2	střelec
gólu	gól	k1gInSc2	gól
Nkwanko	Nkwanka	k1gFnSc5	Nkwanka
Kanua	Kanua	k1gFnSc1	Kanua
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Portsmouthu	Portsmouth	k1gInSc2	Portsmouth
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
do	do	k7c2	do
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
nenastoupil	nastoupit	k5eNaPmAgMnS	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Premier	Premier	k1gInSc4	Premier
League	League	k1gFnPc2	League
během	během	k7c2	během
12	[number]	k4	12
zápasů	zápas	k1gInPc2	zápas
nevstřelil	vstřelit	k5eNaPmAgMnS	vstřelit
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Portsmouthu	Portsmouth	k1gInSc2	Portsmouth
žádný	žádný	k3yNgInSc1	žádný
gól	gól	k1gInSc1	gól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
upsal	upsat	k5eAaPmAgInS	upsat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
věhlasnému	věhlasný	k2eAgInSc3d1	věhlasný
tureckému	turecký	k2eAgInSc3d1	turecký
klubu	klub	k1gInSc3	klub
z	z	k7c2	z
Istanbulu	Istanbul	k1gInSc2	Istanbul
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
SK	Sk	kA	Sk
<g/>
,	,	kIx,	,
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
za	za	k7c4	za
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
prožil	prožít	k5eAaPmAgMnS	prožít
střelecké	střelecký	k2eAgNnSc4d1	střelecké
obrození	obrození	k1gNnSc4	obrození
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgMnS	obnovit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
dřívějším	dřívější	k2eAgMnSc7d1	dřívější
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
s	s	k7c7	s
Liverpoolu	Liverpool	k1gInSc2	Liverpool
Harry	Harra	k1gFnSc2	Harra
Kewellem	Kewell	k1gMnSc7	Kewell
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
proti	proti	k7c3	proti
AC	AC	kA	AC
Bellinzoně	Bellinzoň	k1gFnSc2	Bellinzoň
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
ligovém	ligový	k2eAgInSc6d1	ligový
debutu	debut	k1gInSc6	debut
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dvakrát	dvakrát	k6eAd1	dvakrát
trefil	trefit	k5eAaPmAgMnS	trefit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
Kocaelisporu	Kocaelispor	k1gInSc2	Kocaelispor
<g/>
,	,	kIx,	,
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
u	u	k7c2	u
soupeře	soupeř	k1gMnSc2	soupeř
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
hattrickem	hattrick	k1gInSc7	hattrick
o	o	k7c6	o
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Hacettepe	Hacettep	k1gInSc5	Hacettep
SK	Sk	kA	Sk
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
hattricku	hattrick	k1gInSc3	hattrick
v	v	k7c4	v
utkání	utkání	k1gNnSc4	utkání
proti	proti	k7c3	proti
rivalovi	rival	k1gMnSc3	rival
Beşiktaş	Beşiktaş	k1gFnSc2	Beşiktaş
JK	JK	kA	JK
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
čelo	čelo	k1gNnSc4	čelo
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
14	[number]	k4	14
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Galatasaray	Galatasara	k1gMnPc4	Galatasara
porazil	porazit	k5eAaPmAgMnS	porazit
Beşiktaş	Beşiktaş	k1gMnSc1	Beşiktaş
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
dal	dát	k5eAaPmAgMnS	dát
dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
branek	branka	k1gFnPc2	branka
z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
s	s	k7c7	s
20	[number]	k4	20
góly	gól	k1gInPc7	gól
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
turecké	turecký	k2eAgFnSc2d1	turecká
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligy	liga	k1gFnSc2	liga
Süper	Süpra	k1gFnPc2	Süpra
Lig	liga	k1gFnPc2	liga
<g/>
,	,	kIx,	,
ke	k	k7c3	k
koruně	koruna	k1gFnSc3	koruna
střelců	střelec	k1gMnPc2	střelec
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
31	[number]	k4	31
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
ligových	ligový	k2eAgInPc6d1	ligový
zápasech	zápas	k1gInPc6	zápas
dvakrát	dvakrát	k6eAd1	dvakrát
skóroval	skórovat	k5eAaBmAgMnS	skórovat
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Kayserisporu	Kayserispor	k1gInSc3	Kayserispor
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
proti	proti	k7c3	proti
Beşiktaşi	Beşiktaşi	k1gNnSc3	Beşiktaşi
Istanbul	Istanbul	k1gInSc4	Istanbul
při	při	k7c6	při
výhře	výhra	k1gFnSc6	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
branka	branka	k1gFnSc1	branka
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákroku	zákrok	k1gInSc6	zákrok
Emre	Emr	k1gFnSc2	Emr
Belözoğ	Belözoğ	k1gFnSc2	Belözoğ
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
derby	derby	k1gNnSc4	derby
Fenerbahçe	Fenerbahçe	k1gFnPc2	Fenerbahçe
<g/>
-	-	kIx~	-
<g/>
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
měl	mít	k5eAaImAgInS	mít
nadvakrát	nadvakrát	k6eAd1	nadvakrát
zlomenou	zlomený	k2eAgFnSc4d1	zlomená
nohu	noha	k1gFnSc4	noha
a	a	k8xC	a
na	na	k7c4	na
4	[number]	k4	4
a	a	k8xC	a
půl	půl	k1xP	půl
měsíce	měsíc	k1gInSc2	měsíc
byl	být	k5eAaImAgMnS	být
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
MKE	MKE	kA	MKE
Ankaragücü	Ankaragücü	k1gFnSc6	Ankaragücü
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k6eAd1	také
jednou	jeden	k4xCgFnSc7	jeden
skóroval	skórovat	k5eAaBmAgInS	skórovat
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
hattrick	hattrick	k1gInSc4	hattrick
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Diyarbakirsporu	Diyarbakirspor	k1gInSc3	Diyarbakirspor
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
gól	gól	k1gInSc4	gól
proti	proti	k7c3	proti
Manisasporu	Manisaspor	k1gInSc3	Manisaspor
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
před	před	k7c7	před
startem	start	k1gInSc7	start
turecké	turecký	k2eAgFnSc2d1	turecká
ligy	liga	k1gFnSc2	liga
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Galatasaray	Galatasaray	k1gInPc7	Galatasaray
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
sužovala	sužovat	k5eAaImAgFnS	sužovat
Milana	Milana	k1gFnSc1	Milana
Baroše	Baroš	k1gMnSc2	Baroš
řada	řada	k1gFnSc1	řada
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
proti	proti	k7c3	proti
ukrajinskému	ukrajinský	k2eAgInSc3d1	ukrajinský
celku	celek	k1gInSc3	celek
FK	FK	kA	FK
Karpaty	Karpaty	k1gInPc1	Karpaty
Lvov	Lvov	k1gInSc4	Lvov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ligový	ligový	k2eAgInSc1d1	ligový
gól	gól	k1gInSc1	gól
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Eskişehirsporu	Eskişehirspor	k1gInSc3	Eskişehirspor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Galatasaray	Galatasaraa	k1gFnPc4	Galatasaraa
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
blýskl	blýsknout	k5eAaPmAgInS	blýsknout
hattrickem	hattrick	k1gInSc7	hattrick
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
İ	İ	k?	İ
Büyükşehir	Büyükşehir	k1gInSc1	Büyükşehir
Belediyesporu	Belediyespor	k1gInSc2	Belediyespor
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
zápas	zápas	k1gInSc4	zápas
nedohrál	dohrát	k5eNaPmAgMnS	dohrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
trávník	trávník	k1gInSc4	trávník
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
MKE	MKE	kA	MKE
Ankaragücü	Ankaragücü	k1gFnSc1	Ankaragücü
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
po	po	k7c6	po
7	[number]	k4	7
týdnech	týden	k1gInPc6	týden
na	na	k7c4	na
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
souboje	souboj	k1gInSc2	souboj
s	s	k7c7	s
Beşiktaşem	Beşiktaş	k1gInSc7	Beşiktaş
<g/>
.	.	kIx.	.
</s>
<s>
Smůla	smůla	k1gFnSc1	smůla
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
přišlo	přijít	k5eAaPmAgNnS	přijít
opět	opět	k6eAd1	opět
zranění	zranění	k1gNnSc1	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
ze	z	k7c2	z
zranění	zranění	k1gNnSc2	zranění
podával	podávat	k5eAaImAgMnS	podávat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
sezóny	sezóna	k1gFnSc2	sezóna
výborné	výborný	k2eAgInPc1d1	výborný
výkony	výkon	k1gInPc1	výkon
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
Baroš	Baroš	k1gMnSc1	Baroš
úvodní	úvodní	k2eAgInPc4d1	úvodní
2	[number]	k4	2
góly	gól	k1gInPc4	gól
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
svému	svůj	k3xOyFgInSc3	svůj
bývalému	bývalý	k2eAgInSc3d1	bývalý
klubu	klub	k1gInSc3	klub
Liverpoolu	Liverpool	k1gInSc2	Liverpool
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Trefil	trefit	k5eAaPmAgInS	trefit
se	se	k3xPyFc4	se
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
utkání	utkání	k1gNnSc1	utkání
skončilo	skončit	k5eAaPmAgNnS	skončit
výsledkem	výsledek	k1gInSc7	výsledek
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
Galatasaraye	Galatasaray	k1gInSc2	Galatasaray
přibyl	přibýt	k5eAaPmAgMnS	přibýt
Barošův	Barošův	k2eAgMnSc1d1	Barošův
bývalý	bývalý	k2eAgMnSc1d1	bývalý
reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
kolega	kolega	k1gMnSc1	kolega
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ujfaluši	Ujfaluše	k1gFnSc4	Ujfaluše
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
z	z	k7c2	z
Atlética	Atléticum	k1gNnSc2	Atléticum
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
útočník	útočník	k1gMnSc1	útočník
(	(	kIx(	(
<g/>
Baroš	Baroš	k1gMnSc1	Baroš
<g/>
)	)	kIx)	)
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
tak	tak	k6eAd1	tak
dravě	dravě	k6eAd1	dravě
jako	jako	k8xS	jako
v	v	k7c6	v
předešlých	předešlý	k2eAgNnPc6d1	předešlé
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slabších	slabý	k2eAgInPc6d2	slabší
výkonech	výkon	k1gInPc6	výkon
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dvou	dva	k4xCgInPc6	dva
ligových	ligový	k2eAgInPc6d1	ligový
zápasech	zápas	k1gInPc6	zápas
ztratil	ztratit	k5eAaPmAgMnS	ztratit
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gMnSc4	on
švédský	švédský	k2eAgMnSc1d1	švédský
útočník	útočník	k1gMnSc1	útočník
Johan	Johan	k1gMnSc1	Johan
Elmander	Elmander	k1gMnSc1	Elmander
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
náhradníků	náhradník	k1gMnPc2	náhradník
dostal	dostat	k5eAaPmAgMnS	dostat
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
šanci	šance	k1gFnSc4	šance
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
5	[number]	k4	5
<g/>
.	.	kIx.	.
ligového	ligový	k2eAgNnSc2d1	ligové
kola	kolo	k1gNnSc2	kolo
proti	proti	k7c3	proti
domácímu	domácí	k2eAgInSc3d1	domácí
MKE	MKE	kA	MKE
Ankaragücü	Ankaragücü	k1gFnPc6	Ankaragücü
se	se	k3xPyFc4	se
Baroš	Baroš	k1gMnSc1	Baroš
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
trávník	trávník	k1gInSc4	trávník
v	v	k7c6	v
74	[number]	k4	74
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
když	když	k8xS	když
střídal	střídat	k5eAaImAgMnS	střídat
zraněného	zraněný	k2eAgMnSc4d1	zraněný
Engina	Engin	k2eAgMnSc4d1	Engin
Baytara	Baytar	k1gMnSc4	Baytar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
necelých	celý	k2eNgInPc6d1	necelý
10	[number]	k4	10
minutách	minuta	k1gFnPc6	minuta
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
skóre	skóre	k1gNnSc4	skóre
z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
gól	gól	k1gInSc1	gól
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
zápase	zápas	k1gInSc6	zápas
6	[number]	k4	6
<g/>
.	.	kIx.	.
kola	kola	k1gFnSc1	kola
proti	proti	k7c3	proti
Bursasporu	Bursaspor	k1gInSc3	Bursaspor
šel	jít	k5eAaImAgMnS	jít
Baroš	Baroš	k1gMnSc1	Baroš
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
83	[number]	k4	83
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
již	již	k6eAd1	již
za	za	k7c4	za
čtyři	čtyři	k4xCgFnPc4	čtyři
minuty	minuta	k1gFnPc4	minuta
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
vítězný	vítězný	k2eAgInSc1d1	vítězný
gól	gól	k1gInSc1	gól
<g/>
,	,	kIx,	,
Galatasaray	Galatasaraa	k1gFnPc1	Galatasaraa
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
doma	doma	k6eAd1	doma
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
58	[number]	k4	58
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
utkání	utkání	k1gNnPc2	utkání
proti	proti	k7c3	proti
Sivassporu	Sivasspor	k1gMnSc3	Sivasspor
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
na	na	k7c4	na
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgFnSc2d1	domácí
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
udržel	udržet	k5eAaPmAgInS	udržet
vítězství	vítězství	k1gNnSc4	vítězství
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
navzdory	navzdory	k7c3	navzdory
vyloučení	vyloučení	k1gNnSc3	vyloučení
svých	svůj	k3xOyFgMnPc2	svůj
hráčů	hráč	k1gMnPc2	hráč
Engina	Engin	k1gMnSc2	Engin
Baytara	Baytar	k1gMnSc2	Baytar
a	a	k8xC	a
Johana	Johan	k1gMnSc2	Johan
Elmandera	Elmander	k1gMnSc2	Elmander
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
gólem	gól	k1gInSc7	gól
se	se	k3xPyFc4	se
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Klubu	klub	k1gInSc2	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jeho	jeho	k3xOp3gInPc1	jeho
zlepšené	zlepšený	k2eAgInPc1d1	zlepšený
výkony	výkon	k1gInPc1	výkon
vedly	vést	k5eAaImAgInP	vést
kouče	kouč	k1gMnSc2	kouč
istanbulského	istanbulský	k2eAgInSc2d1	istanbulský
klubu	klub	k1gInSc2	klub
Fatiha	Fatih	k1gMnSc4	Fatih
Terima	Terim	k1gMnSc4	Terim
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
rozestavení	rozestavení	k1gNnSc2	rozestavení
na	na	k7c4	na
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
začal	začít	k5eAaPmAgMnS	začít
nastupovat	nastupovat	k5eAaImF	nastupovat
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
vedle	vedle	k7c2	vedle
Johana	Johan	k1gMnSc2	Johan
Elmandera	Elmander	k1gMnSc2	Elmander
<g/>
.	.	kIx.	.
</s>
<s>
Změněný	změněný	k2eAgInSc1d1	změněný
systém	systém	k1gInSc1	systém
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgMnS	osvědčit
<g/>
,	,	kIx,	,
Baroš	Baroš	k1gMnSc1	Baroš
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
zápasech	zápas	k1gInPc6	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
İ	İ	k?	İ
BB	BB	kA	BB
<g/>
,	,	kIx,	,
Samsunsporu	Samsunspora	k1gFnSc4	Samsunspora
<g/>
,	,	kIx,	,
Karabüksporu	Karabükspora	k1gFnSc4	Karabükspora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
posledně	posledně	k6eAd1	posledně
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
duelu	duel	k1gInSc2	duel
s	s	k7c7	s
Karabüksporem	Karabükspor	k1gInSc7	Karabükspor
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
přišlo	přijít	k5eAaPmAgNnS	přijít
svalové	svalový	k2eAgNnSc4d1	svalové
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
zranění	zranění	k1gNnSc2	zranění
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Antalyasporu	Antalyaspor	k1gInSc3	Antalyaspor
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc4	kolo
turecké	turecký	k2eAgFnSc2d1	turecká
ligy	liga	k1gFnSc2	liga
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
57	[number]	k4	57
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
za	za	k7c4	za
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
byl	být	k5eAaImAgMnS	být
rozhodčím	rozhodčí	k1gMnPc3	rozhodčí
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
za	za	k7c4	za
protesty	protest	k1gInPc4	protest
<g/>
.	.	kIx.	.
</s>
<s>
Českému	český	k2eAgMnSc3d1	český
útočníkovi	útočník	k1gMnSc3	útočník
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
odpískán	odpískán	k2eAgInSc4d1	odpískán
pokutový	pokutový	k2eAgInSc4d1	pokutový
kop	kop	k1gInSc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
unést	unést	k5eAaPmF	unést
emocemi	emoce	k1gFnPc7	emoce
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
rozhodčímu	rozhodčí	k1gMnSc3	rozhodčí
vztyčený	vztyčený	k2eAgInSc4d1	vztyčený
prostředníček	prostředníček	k1gInSc4	prostředníček
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jej	on	k3xPp3gNnSc4	on
neminul	minout	k5eNaImAgInS	minout
třízápasový	třízápasový	k2eAgInSc1d1	třízápasový
trest	trest	k1gInSc1	trest
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
trenéra	trenér	k1gMnSc2	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
nepříjemnost	nepříjemnost	k1gFnSc1	nepříjemnost
jej	on	k3xPp3gMnSc4	on
potkala	potkat	k5eAaPmAgFnS	potkat
příští	příští	k2eAgInSc4d1	příští
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
na	na	k7c6	na
tréninku	trénink	k1gInSc6	trénink
zlomil	zlomit	k5eAaPmAgMnS	zlomit
nos	nos	k1gInSc4	nos
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončil	zakončit	k5eAaPmAgMnS	zakončit
ziskem	zisk	k1gInSc7	zisk
titulu	titul	k1gInSc2	titul
turecké	turecký	k2eAgFnSc2d1	turecká
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
přispěl	přispět	k5eAaPmAgMnS	přispět
mj.	mj.	kA	mj.
svými	svůj	k3xOyFgMnPc7	svůj
8	[number]	k4	8
vstřelenými	vstřelený	k2eAgInPc7d1	vstřelený
góly	gól	k1gInPc7	gól
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
Baroš	Baroš	k1gMnSc1	Baroš
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
,	,	kIx,	,
trenéru	trenér	k1gMnSc3	trenér
Fatihu	Fatih	k1gInSc2	Fatih
Terimovi	Terim	k1gMnSc3	Terim
nezapadal	zapadat	k5eNaImAgInS	zapadat
do	do	k7c2	do
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
snažil	snažit	k5eAaImAgMnS	snažit
prodat	prodat	k5eAaPmF	prodat
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnSc3	hráč
končila	končit	k5eAaImAgFnS	končit
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
lukrativní	lukrativní	k2eAgFnSc1d1	lukrativní
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
získal	získat	k5eAaPmAgInS	získat
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
turecký	turecký	k2eAgInSc4d1	turecký
Superpohár	superpohár	k1gInSc4	superpohár
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
nad	nad	k7c7	nad
Fenerbahçe	Fenerbahçe	k1gFnSc7	Fenerbahçe
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
do	do	k7c2	do
utkání	utkání	k1gNnSc2	utkání
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
proti	proti	k7c3	proti
portugalské	portugalský	k2eAgFnSc3d1	portugalská
Braze	Braza	k1gFnSc3	Braza
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
Bragy	Braga	k1gFnSc2	Braga
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgInS	být
ani	ani	k8xC	ani
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
náhradníků	náhradník	k1gMnPc2	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podzimní	podzimní	k2eAgFnSc6d1	podzimní
části	část	k1gFnSc6	část
sezóny	sezóna	k1gFnSc2	sezóna
nakonec	nakonec	k6eAd1	nakonec
nenastoupil	nastoupit	k5eNaPmAgMnS	nastoupit
k	k	k7c3	k
jedinému	jediný	k2eAgInSc3d1	jediný
zápasu	zápas	k1gInSc3	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Galatasaray	Galatasaray	k1gInPc1	Galatasaray
navíc	navíc	k6eAd1	navíc
posílilo	posílit	k5eAaPmAgNnS	posílit
ofenzivu	ofenziva	k1gFnSc4	ofenziva
nákupem	nákup	k1gInSc7	nákup
Didiera	Didier	k1gMnSc2	Didier
Drogby	Drogba	k1gMnSc2	Drogba
<g/>
,	,	kIx,	,
útočníka	útočník	k1gMnSc2	útočník
světové	světový	k2eAgFnSc2d1	světová
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgMnS	připravit
strávit	strávit	k5eAaPmF	strávit
jarní	jarní	k2eAgFnSc4d1	jarní
část	část	k1gFnSc4	část
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
Baníku	Baník	k1gInSc6	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
se	se	k3xPyFc4	se
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
na	na	k7c4	na
rozvázání	rozvázání	k1gNnSc4	rozvázání
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestupové	přestupový	k2eAgNnSc1d1	přestupové
okno	okno	k1gNnSc1	okno
do	do	k7c2	do
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
lig	liga	k1gFnPc2	liga
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
mohl	moct	k5eAaImAgMnS	moct
přestoupit	přestoupit	k5eAaPmF	přestoupit
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
nebo	nebo	k8xC	nebo
do	do	k7c2	do
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
asijských	asijský	k2eAgFnPc2d1	asijská
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
byl	být	k5eAaImAgMnS	být
i	i	k9	i
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Baníku	Baník	k1gInSc2	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Galatasarayi	Galatasaray	k1gInSc6	Galatasaray
Istanbul	Istanbul	k1gInSc1	Istanbul
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
během	během	k7c2	během
93	[number]	k4	93
ligových	ligový	k2eAgInPc2d1	ligový
zápasů	zápas	k1gInPc2	zápas
celkem	celkem	k6eAd1	celkem
48	[number]	k4	48
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c6	na
1	[number]	k4	1
<g/>
1⁄	1⁄	k?	1⁄
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgMnSc2	který
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dohodl	dohodnout	k5eAaPmAgInS	dohodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
platu	plat	k1gInSc6	plat
20	[number]	k4	20
000	[number]	k4	000
Kč	Kč	kA	Kč
měsíčně	měsíčně	k6eAd1	měsíčně
plus	plus	k6eAd1	plus
prémie	prémie	k1gFnPc1	prémie
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
všechny	všechen	k3xTgInPc4	všechen
vydělané	vydělaný	k2eAgInPc4d1	vydělaný
peníze	peníz	k1gInPc4	peníz
věnovat	věnovat	k5eAaImF	věnovat
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
ostravském	ostravský	k2eAgInSc6d1	ostravský
klubu	klub	k1gInSc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
Dominik	Dominik	k1gMnSc1	Dominik
Kraut	Kraut	k1gMnSc1	Kraut
přenechal	přenechat	k5eAaPmAgMnS	přenechat
Milanu	Milan	k1gMnSc3	Milan
Barošovi	Baroš	k1gMnSc3	Baroš
dres	dres	k1gInSc4	dres
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
zápasu	zápas	k1gInSc3	zápas
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c6	na
Bazalech	Bazal	k1gInPc6	Bazal
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
proti	proti	k7c3	proti
Dynamu	dynamo	k1gNnSc3	dynamo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
68	[number]	k4	68
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
na	na	k7c6	na
těžkém	těžký	k2eAgInSc6d1	těžký
terénu	terén	k1gInSc6	terén
skončilo	skončit	k5eAaPmAgNnS	skončit
remízou	remíza	k1gFnSc7	remíza
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
ligovém	ligový	k2eAgNnSc6d1	ligové
kole	kolo	k1gNnSc6	kolo
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
proti	proti	k7c3	proti
domácí	domácí	k2eAgFnSc3d1	domácí
Viktorii	Viktoria	k1gFnSc3	Viktoria
Plzeň	Plzeň	k1gFnSc4	Plzeň
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
slibných	slibný	k2eAgFnPc2d1	slibná
šancí	šance	k1gFnPc2	šance
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
trefil	trefit	k5eAaPmAgMnS	trefit
tyč	tyč	k1gFnSc4	tyč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
79	[number]	k4	79
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
střídal	střídat	k5eAaImAgMnS	střídat
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
třetím	třetí	k4xOgMnSc6	třetí
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
doma	doma	k6eAd1	doma
proti	proti	k7c3	proti
Hradci	Hradec	k1gInSc3	Hradec
Králové	Králová	k1gFnSc2	Králová
zařídil	zařídit	k5eAaPmAgInS	zařídit
hattrickem	hattrick	k1gInSc7	hattrick
vítězství	vítězství	k1gNnSc2	vítězství
Ostravy	Ostrava	k1gFnSc2	Ostrava
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Podařený	podařený	k2eAgInSc1d1	podařený
zápas	zápas	k1gInSc1	zápas
odehrál	odehrát	k5eAaPmAgInS	odehrát
i	i	k9	i
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
ve	v	k7c6	v
26	[number]	k4	26
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
proti	proti	k7c3	proti
Příbrami	Příbram	k1gFnSc3	Příbram
<g/>
,	,	kIx,	,
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
hřišti	hřiště	k1gNnSc6	hřiště
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
při	při	k7c6	při
výhře	výhra	k1gFnSc6	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
obě	dva	k4xCgFnPc4	dva
branky	branka	k1gFnPc4	branka
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
odehrál	odehrát	k5eAaPmAgInS	odehrát
za	za	k7c4	za
Ostravu	Ostrava	k1gFnSc4	Ostrava
12	[number]	k4	12
ligových	ligový	k2eAgNnPc2d1	ligové
utkání	utkání	k1gNnPc2	utkání
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
5	[number]	k4	5
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
když	když	k8xS	když
podepsal	podepsat	k5eAaPmAgMnS	podepsat
roční	roční	k2eAgFnSc4d1	roční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Antalyaspor	Antalyaspora	k1gFnPc2	Antalyaspora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
krajana	krajan	k1gMnSc2	krajan
Petra	Petr	k1gMnSc2	Petr
Jandy	Janda	k1gMnSc2	Janda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Süper	Süper	k1gInSc4	Süper
Lig	liga	k1gFnPc2	liga
debutoval	debutovat	k5eAaBmAgMnS	debutovat
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
klub	klub	k1gInSc4	klub
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Kayseri	Kayseri	k1gNnSc3	Kayseri
Erciyessporu	Erciyesspor	k1gInSc2	Erciyesspor
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
remízou	remíza	k1gFnSc7	remíza
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Baroš	Baroš	k1gMnSc1	Baroš
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
v	v	k7c6	v
54	[number]	k4	54
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
gól	gól	k1gInSc1	gól
v	v	k7c4	v
Süper	Süper	k1gInSc4	Süper
Lig	liga	k1gFnPc2	liga
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
Bursasporu	Bursaspor	k1gInSc2	Bursaspor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
otevřel	otevřít	k5eAaPmAgMnS	otevřít
skóre	skóre	k1gNnSc4	skóre
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
soupeř	soupeř	k1gMnSc1	soupeř
vývoj	vývoj	k1gInSc4	vývoj
otočil	otočit	k5eAaPmAgMnS	otočit
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
střelecky	střelecky	k6eAd1	střelecky
prosadil	prosadit	k5eAaPmAgInS	prosadit
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
proti	proti	k7c3	proti
domácímu	domácí	k2eAgInSc3d1	domácí
Çaykuru	Çaykur	k1gInSc3	Çaykur
Rizespor	Rizespora	k1gFnPc2	Rizespora
<g/>
,	,	kIx,	,
v	v	k7c6	v
88	[number]	k4	88
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
zařídil	zařídit	k5eAaPmAgInS	zařídit
vítězným	vítězný	k2eAgInSc7d1	vítězný
gólem	gól	k1gInSc7	gól
výsledek	výsledek	k1gInSc4	výsledek
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
výhru	výhra	k1gFnSc4	výhra
Antalyasporu	Antalyaspor	k1gInSc2	Antalyaspor
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
ročníku	ročník	k1gInSc6	ročník
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
si	se	k3xPyFc3	se
v	v	k7c6	v
ligovém	ligový	k2eAgNnSc6d1	ligové
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Gençlerbirliğ	Gençlerbirliğ	k1gMnSc7	Gençlerbirliğ
SK	Sk	kA	Sk
(	(	kIx(	(
<g/>
porážka	porážka	k1gFnSc1	porážka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
poranil	poranit	k5eAaPmAgInS	poranit
koleno	koleno	k1gNnSc4	koleno
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
druhého	druhý	k4xOgInSc2	druhý
poločasu	poločas	k1gInSc2	poločas
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
minutách	minuta	k1gFnPc6	minuta
musel	muset	k5eAaImAgMnS	muset
opustit	opustit	k5eAaPmF	opustit
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
přetržený	přetržený	k2eAgInSc4d1	přetržený
přední	přední	k2eAgInSc4d1	přední
křížový	křížový	k2eAgInSc4d1	křížový
vaz	vaz	k1gInSc4	vaz
a	a	k8xC	a
poškozený	poškozený	k2eAgInSc4d1	poškozený
postranní	postranní	k2eAgInSc4d1	postranní
vaz	vaz	k1gInSc4	vaz
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
podrobit	podrobit	k5eAaPmF	podrobit
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
části	část	k1gFnSc6	část
sezony	sezona	k1gFnSc2	sezona
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Baníku	Baník	k1gInSc2	Baník
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezony	sezona	k1gFnSc2	sezona
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
a	a	k8xC	a
vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
dres	dres	k1gInSc4	dres
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
zápasu	zápas	k1gInSc3	zápas
v	v	k7c4	v
Synot	Synot	k1gInSc4	Synot
lize	liga	k1gFnSc6	liga
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
v	v	k7c4	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šel	jít	k5eAaImAgMnS	jít
na	na	k7c6	na
hřište	hřišit	k5eAaImRp2nP	hřišit
v	v	k7c4	v
74	[number]	k4	74
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	on	k3xPp3gInSc4	on
první	první	k4xOgInSc4	první
soutěžní	soutěžní	k2eAgInSc4d1	soutěžní
start	start	k1gInSc4	start
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
gólem	gól	k1gInSc7	gól
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
remíze	remíza	k1gFnSc3	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
mu	on	k3xPp3gNnSc3	on
vedení	vedení	k1gNnSc3	vedení
Baníku	Baník	k1gInSc2	Baník
nenabídlo	nabídnout	k5eNaPmAgNnS	nabídnout
prodloužení	prodloužení	k1gNnSc1	prodloužení
smlouvy	smlouva	k1gFnSc2	smlouva
kvůli	kvůli	k7c3	kvůli
ostré	ostrý	k2eAgFnSc3d1	ostrá
kritice	kritika	k1gFnSc3	kritika
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
volným	volný	k2eAgMnSc7d1	volný
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
odehrál	odehrát	k5eAaPmAgMnS	odehrát
za	za	k7c4	za
Baník	Baník	k1gInSc4	Baník
11	[number]	k4	11
ligových	ligový	k2eAgInPc2d1	ligový
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
2	[number]	k4	2
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
projevil	projevit	k5eAaPmAgInS	projevit
mj.	mj.	kA	mj.
středočeský	středočeský	k2eAgInSc1d1	středočeský
klub	klub	k1gInSc1	klub
FK	FK	kA	FK
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
a	a	k8xC	a
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Baníku	Baník	k1gInSc2	Baník
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
na	na	k7c6	na
dvouleté	dvouletý	k2eAgFnSc6d1	dvouletá
smlouvě	smlouva	k1gFnSc6	smlouva
se	s	k7c7	s
středočeským	středočeský	k2eAgInSc7d1	středočeský
klubem	klub	k1gInSc7	klub
FK	FK	kA	FK
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Karel	Karel	k1gMnSc1	Karel
Jarolím	Jarolí	k1gNnSc7	Jarolí
jej	on	k3xPp3gMnSc4	on
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
nestavěl	stavět	k5eNaImAgMnS	stavět
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
<g/>
,	,	kIx,	,
Baroš	Baroš	k1gMnSc1	Baroš
měl	mít	k5eAaImAgMnS	mít
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Týmu	tým	k1gInSc3	tým
pomohl	pomoct	k5eAaPmAgInS	pomoct
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
poháru	pohár	k1gInSc6	pohár
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
angažmá	angažmá	k1gNnSc2	angažmá
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
FC	FC	kA	FC
Slovan	Slovan	k1gInSc1	Slovan
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
prošel	projít	k5eAaPmAgMnS	projít
několika	několik	k4yIc7	několik
mládežnickými	mládežnický	k2eAgFnPc7d1	mládežnická
reprezentacemi	reprezentace	k1gFnPc7	reprezentace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
<g/>
:	:	kIx,	:
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
7	[number]	k4	7
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
5	[number]	k4	5
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
1	[number]	k4	1
remíza	remíza	k1gFnSc1	remíza
<g/>
,	,	kIx,	,
1	[number]	k4	1
prohra	prohra	k1gFnSc1	prohra
<g/>
,	,	kIx,	,
3	[number]	k4	3
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
)	)	kIx)	)
reprezentaci	reprezentace	k1gFnSc3	reprezentace
do	do	k7c2	do
16	[number]	k4	16
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
4	[number]	k4	4
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
1	[number]	k4	1
remíza	remíza	k1gFnSc1	remíza
<g/>
,	,	kIx,	,
1	[number]	k4	1
vstřelený	vstřelený	k2eAgInSc4d1	vstřelený
gól	gól	k1gInSc4	gól
<g/>
)	)	kIx)	)
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
4	[number]	k4	4
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
1	[number]	k4	1
výhra	výhra	k1gFnSc1	výhra
<g/>
,	,	kIx,	,
1	[number]	k4	1
remíza	remíza	k1gFnSc1	remíza
<g/>
,	,	kIx,	,
2	[number]	k4	2
prohry	prohra	k1gFnPc4	prohra
<g/>
,	,	kIx,	,
1	[number]	k4	1
vstřelený	vstřelený	k2eAgInSc4d1	vstřelený
gól	gól	k1gInSc4	gól
<g/>
)	)	kIx)	)
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
19	[number]	k4	19
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
9	[number]	k4	9
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
5	[number]	k4	5
remíz	remíza	k1gFnPc2	remíza
<g/>
,	,	kIx,	,
5	[number]	k4	5
proher	prohra	k1gFnPc2	prohra
<g/>
,	,	kIx,	,
5	[number]	k4	5
vstřelených	vstřelený	k2eAgInPc2d1	vstřelený
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
19	[number]	k4	19
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
10	[number]	k4	10
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
4	[number]	k4	4
remíz	remíza	k1gFnPc2	remíza
<g/>
,	,	kIx,	,
5	[number]	k4	5
proher	prohra	k1gFnPc2	prohra
<g/>
,	,	kIx,	,
9	[number]	k4	9
vstřelených	vstřelený	k2eAgInPc2d1	vstřelený
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
olympijský	olympijský	k2eAgInSc1d1	olympijský
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
3	[number]	k4	3
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
0	[number]	k4	0
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
2	[number]	k4	2
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
1	[number]	k4	1
prohra	prohra	k1gFnSc1	prohra
<g/>
,	,	kIx,	,
0	[number]	k4	0
vstřelených	vstřelený	k2eAgInPc2d1	vstřelený
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
Všech	všecek	k3xTgInPc2	všecek
8	[number]	k4	8
zápasů	zápas	k1gInPc2	zápas
za	za	k7c4	za
"	"	kIx"	"
<g/>
jedenadvacítku	jedenadvacítka	k1gFnSc4	jedenadvacítka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
Baroš	Baroš	k1gMnSc1	Baroš
skóroval	skórovat	k5eAaBmAgMnS	skórovat
skončilo	skončit	k5eAaPmAgNnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Dánsku	Dánsko	k1gNnSc3	Dánsko
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
i	i	k9	i
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
olympijském	olympijský	k2eAgInSc6d1	olympijský
výběru	výběr	k1gInSc6	výběr
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2000	[number]	k4	2000
odehrál	odehrát	k5eAaPmAgMnS	odehrát
68	[number]	k4	68
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2000	[number]	k4	2000
v	v	k7c6	v
Brisbane	Brisban	k1gMnSc5	Brisban
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Baroš	Baroš	k1gMnSc1	Baroš
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
opět	opět	k6eAd1	opět
v	v	k7c6	v
základu	základ	k1gInSc6	základ
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
odehrál	odehrát	k5eAaPmAgMnS	odehrát
kompletní	kompletní	k2eAgNnSc4d1	kompletní
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
s	s	k7c7	s
Kamerunem	Kamerun	k1gInSc7	Kamerun
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
konečné	konečný	k2eAgFnSc2d1	konečná
remízy	remíza	k1gFnSc2	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
se	s	k7c7	s
2	[number]	k4	2
body	bod	k1gInPc7	bod
obsadil	obsadit	k5eAaPmAgMnS	obsadit
poslední	poslední	k2eAgFnSc4d1	poslední
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
skupiny	skupina	k1gFnSc2	skupina
C.	C.	kA	C.
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
A	a	k8xC	a
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
konaném	konaný	k2eAgNnSc6d1	konané
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
střetla	střetnout	k5eAaPmAgFnS	střetnout
postupně	postupně	k6eAd1	postupně
s	s	k7c7	s
celky	celek	k1gInPc7	celek
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
postupoval	postupovat	k5eAaImAgMnS	postupovat
vítěz	vítěz	k1gMnSc1	vítěz
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
do	do	k7c2	do
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgInSc2	první
remízového	remízový	k2eAgInSc2d1	remízový
zápasu	zápas	k1gInSc2	zápas
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
až	až	k6eAd1	až
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
v	v	k7c6	v
Trenčíně	Trenčín	k1gInSc6	Trenčín
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
ČR	ČR	kA	ČR
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odehrál	odehrát	k5eAaPmAgInS	odehrát
kompletní	kompletní	k2eAgInSc4d1	kompletní
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
v	v	k7c6	v
83	[number]	k4	83
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
dostal	dostat	k5eAaPmAgMnS	dostat
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
utkání	utkání	k1gNnSc6	utkání
skupiny	skupina	k1gFnSc2	skupina
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
proti	proti	k7c3	proti
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
hrál	hrát	k5eAaImAgInS	hrát
do	do	k7c2	do
66	[number]	k4	66
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Erich	Erich	k1gMnSc1	Erich
Brabec	Brabec	k1gMnSc1	Brabec
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
Baroš	Baroš	k1gMnSc1	Baroš
gól	gól	k1gInSc4	gól
na	na	k7c6	na
průběžných	průběžný	k2eAgNnPc6d1	průběžné
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
skončilo	skončit	k5eAaPmAgNnS	skončit
výhrou	výhra	k1gFnSc7	výhra
českého	český	k2eAgNnSc2d1	české
družstva	družstvo	k1gNnSc2	družstvo
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
jej	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
trenér	trenér	k1gMnSc1	trenér
Karel	Karel	k1gMnSc1	Karel
Brückner	Brückner	k1gMnSc1	Brückner
do	do	k7c2	do
zápasu	zápas	k1gInSc2	zápas
v	v	k7c4	v
61	[number]	k4	61
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
za	za	k7c2	za
Libora	Libor	k1gMnSc2	Libor
Sionka	Sionek	k1gMnSc2	Sionek
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
nakonec	nakonec	k6eAd1	nakonec
prohrál	prohrát	k5eAaPmAgInS	prohrát
gólem	gól	k1gInSc7	gól
Pirla	Pirlo	k1gNnSc2	Pirlo
z	z	k7c2	z
81	[number]	k4	81
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
medaile	medaile	k1gFnSc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
i	i	k9	i
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
česká	český	k2eAgFnSc1d1	Česká
reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
jedenadvacítka	jedenadvacítka	k1gFnSc1	jedenadvacítka
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
svůj	svůj	k3xOyFgInSc4	svůj
premiérový	premiérový	k2eAgInSc4d1	premiérový
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Francii	Francie	k1gFnSc4	Francie
na	na	k7c4	na
pokutové	pokutový	k2eAgInPc4d1	pokutový
kopy	kop	k1gInPc4	kop
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
celky	celek	k1gInPc4	celek
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
postupovaly	postupovat	k5eAaImAgFnP	postupovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
prohrála	prohrát	k5eAaPmAgFnS	prohrát
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
Miroslav	Miroslav	k1gMnSc1	Miroslav
Beránek	Beránek	k1gMnSc1	Beránek
nechal	nechat	k5eAaPmAgMnS	nechat
hrát	hrát	k5eAaImF	hrát
Milana	Milan	k1gMnSc2	Milan
Baroše	Baroš	k1gMnSc2	Baroš
do	do	k7c2	do
62	[number]	k4	62
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Štěpánem	Štěpán	k1gMnSc7	Štěpán
Vachouškem	Vachoušek	k1gMnSc7	Vachoušek
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
následoval	následovat	k5eAaImAgMnS	následovat
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Baroš	Baroš	k1gMnSc1	Baroš
střídal	střídat	k5eAaImAgMnS	střídat
v	v	k7c6	v
86	[number]	k4	86
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
Tomáše	Tomáš	k1gMnSc2	Tomáš
Juna	jun	k1gMnSc2	jun
<g/>
,	,	kIx,	,
utkání	utkání	k1gNnSc1	utkání
tímto	tento	k3xDgInSc7	tento
výsledkem	výsledek	k1gInSc7	výsledek
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Řecku	Řecko	k1gNnSc3	Řecko
hrál	hrát	k5eAaImAgMnS	hrát
Baroš	Baroš	k1gMnSc1	Baroš
do	do	k7c2	do
88	[number]	k4	88
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc4	minuta
(	(	kIx(	(
<g/>
v	v	k7c6	v
63	[number]	k4	63
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
obdržel	obdržet	k5eAaPmAgMnS	obdržet
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
remíze	remíza	k1gFnSc3	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
narazilo	narazit	k5eAaPmAgNnS	narazit
české	český	k2eAgNnSc4d1	české
mládežnické	mládežnický	k2eAgNnSc4d1	mládežnické
družstvo	družstvo	k1gNnSc4	družstvo
na	na	k7c4	na
tým	tým	k1gInSc4	tým
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
porazilo	porazit	k5eAaPmAgNnS	porazit
zlatým	zlatý	k2eAgInSc7d1	zlatý
gólem	gól	k1gInSc7	gól
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
Michala	Michal	k1gMnSc2	Michal
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
v	v	k7c4	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
prodloužení	prodloužení	k1gNnSc4	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
ve	v	k7c6	v
45	[number]	k4	45
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
žlutou	žlutý	k2eAgFnSc7d1	žlutá
kartou	karta	k1gFnSc7	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Libor	Libor	k1gMnSc1	Libor
Žůrek	Žůrek	k1gMnSc1	Žůrek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
ČR	ČR	kA	ČR
opět	opět	k6eAd1	opět
střetla	střetnout	k5eAaPmAgFnS	střetnout
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
soupeřem	soupeř	k1gMnSc7	soupeř
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
gól	gól	k1gInSc4	gól
v	v	k7c6	v
řádné	řádný	k2eAgFnSc6d1	řádná
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
ani	ani	k8xC	ani
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
nepadl	padnout	k5eNaPmAgMnS	padnout
a	a	k8xC	a
musely	muset	k5eAaImAgInP	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
pokutové	pokutový	k2eAgInPc1d1	pokutový
kopy	kop	k1gInPc1	kop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc2	on
exceloval	excelovat	k5eAaImAgMnS	excelovat
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
3	[number]	k4	3
ze	z	k7c2	z
4	[number]	k4	4
pokusů	pokus	k1gInPc2	pokus
soupeře	soupeř	k1gMnSc2	soupeř
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
slavil	slavit	k5eAaImAgInS	slavit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Milana	Milan	k1gMnSc4	Milan
Baroše	Baroš	k1gMnSc4	Baroš
střídal	střídat	k5eAaImAgMnS	střídat
již	již	k6eAd1	již
ve	v	k7c6	v
38	[number]	k4	38
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
Michal	Michal	k1gMnSc1	Michal
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
poslední	poslední	k2eAgNnSc1d1	poslední
vystoupení	vystoupení	k1gNnSc1	vystoupení
mladého	mladý	k2eAgMnSc2d1	mladý
útočníka	útočník	k1gMnSc2	útočník
za	za	k7c4	za
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
tým	tým	k1gInSc4	tým
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
se	se	k3xPyFc4	se
střelecky	střelecky	k6eAd1	střelecky
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
ČR	ČR	kA	ČR
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
,	,	kIx,	,
když	když	k8xS	když
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
poločasu	poločas	k1gInSc2	poločas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zápase	zápas	k1gInSc6	zápas
také	také	k9	také
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
za	za	k7c4	za
seniorskou	seniorský	k2eAgFnSc4d1	seniorská
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
,	,	kIx,	,
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
v	v	k7c6	v
82	[number]	k4	82
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
na	na	k7c6	na
konečných	konečný	k2eAgInPc6d1	konečný
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Střelecky	střelecky	k6eAd1	střelecky
nejvydařenější	vydařený	k2eAgInSc1d3	nejvydařenější
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
odehrál	odehrát	k5eAaPmAgInS	odehrát
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgNnSc6d1	kvalifikační
utkání	utkání	k1gNnSc6	utkání
na	na	k7c4	na
MS	MS	kA	MS
2010	[number]	k4	2010
proti	proti	k7c3	proti
San	San	k1gFnSc3	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
,	,	kIx,	,
vsítil	vsítit	k5eAaPmAgMnS	vsítit
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
góly	gól	k1gInPc4	gól
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
27	[number]	k4	27
<g/>
.	.	kIx.	.
a	a	k8xC	a
66	[number]	k4	66
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc1	minuta
(	(	kIx(	(
<g/>
utkání	utkání	k1gNnSc1	utkání
skončilo	skončit	k5eAaPmAgNnS	skončit
jasným	jasný	k2eAgInSc7d1	jasný
vítězstvím	vítězství	k1gNnSc7	vítězství
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
Milana	Milan	k1gMnSc2	Milan
Baroše	Baroš	k1gMnSc2	Baroš
na	na	k7c6	na
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
turnajích	turnaj	k1gInPc6	turnaj
<g/>
:	:	kIx,	:
ME	ME	kA	ME
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
ME	ME	kA	ME
2008	[number]	k4	2008
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
ME	ME	kA	ME
2012	[number]	k4	2012
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
-	-	kIx~	-
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
reprezentace	reprezentace	k1gFnSc2	reprezentace
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
prohraném	prohraný	k2eAgInSc6d1	prohraný
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
zápase	zápas	k1gInSc6	zápas
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
s	s	k7c7	s
pěti	pět	k4xCc7	pět
dalšími	další	k2eAgMnPc7d1	další
reprezentanty	reprezentant	k1gMnPc7	reprezentant
večírku	večírek	k1gInSc2	večírek
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
mladých	mladý	k2eAgFnPc2d1	mladá
dam	dáma	k1gFnPc2	dáma
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
bulvárního	bulvární	k2eAgInSc2d1	bulvární
tisku	tisk	k1gInSc2	tisk
prostitutek	prostitutka	k1gFnPc2	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
reprezentace	reprezentace	k1gFnSc2	reprezentace
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
povolán	povolat	k5eAaPmNgMnS	povolat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2009	[number]	k4	2009
novým	nový	k2eAgMnSc7d1	nový
trenérem	trenér	k1gMnSc7	trenér
Ivanem	Ivan	k1gMnSc7	Ivan
Haškem	Hašek	k1gMnSc7	Hašek
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
odehrál	odehrát	k5eAaPmAgInS	odehrát
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
ME	ME	kA	ME
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
Portugalsku	Portugalsko	k1gNnSc3	Portugalsko
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
nalosován	nalosován	k2eAgInSc1d1	nalosován
do	do	k7c2	do
poměrně	poměrně	k6eAd1	poměrně
těžké	těžký	k2eAgFnSc2d1	těžká
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
a	a	k8xC	a
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
utkání	utkání	k1gNnSc6	utkání
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
proti	proti	k7c3	proti
Lotyšsku	Lotyšsko	k1gNnSc3	Lotyšsko
prohrávalo	prohrávat	k5eAaImAgNnS	prohrávat
české	český	k2eAgNnSc1d1	české
mužstvo	mužstvo	k1gNnSc1	mužstvo
po	po	k7c6	po
gólu	gól	k1gInSc6	gól
Mā	Mā	k1gFnSc2	Mā
Verpakovskise	Verpakovskise	k1gFnSc2	Verpakovskise
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
73	[number]	k4	73
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc3	minuta
u	u	k7c2	u
postranní	postranní	k2eAgFnSc2d1	postranní
čáry	čára	k1gFnSc2	čára
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
Karel	Karel	k1gMnSc1	Karel
Poborský	Poborský	k2eAgMnSc1d1	Poborský
<g/>
,	,	kIx,	,
přelstil	přelstít	k5eAaPmAgInS	přelstít
dva	dva	k4xCgMnPc4	dva
lotyšské	lotyšský	k2eAgMnPc4d1	lotyšský
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
nasměroval	nasměrovat	k5eAaPmAgMnS	nasměrovat
do	do	k7c2	do
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
centr	centr	k1gMnSc1	centr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
brankář	brankář	k1gMnSc1	brankář
Aleksandrs	Aleksandrsa	k1gFnPc2	Aleksandrsa
Koļ	Koļ	k1gMnSc1	Koļ
vyboxoval	vyboxovat	k5eAaPmAgMnS	vyboxovat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
Milanu	Milan	k1gMnSc3	Milan
Barošovi	Baroš	k1gMnSc3	Baroš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
napálil	napálit	k5eAaPmAgMnS	napálit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Marek	Marek	k1gMnSc1	Marek
Heinz	Heinz	k1gMnSc1	Heinz
pak	pak	k6eAd1	pak
zajistil	zajistit	k5eAaPmAgMnS	zajistit
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
barvy	barva	k1gFnPc4	barva
v	v	k7c4	v
85	[number]	k4	85
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
vítězství	vítězství	k1gNnSc1	vítězství
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
zápas	zápas	k1gInSc4	zápas
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Aveiru	Aveir	k1gInSc6	Aveir
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
památném	památný	k2eAgNnSc6d1	památné
utkání	utkání	k1gNnSc6	utkání
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokázala	dokázat	k5eAaPmAgFnS	dokázat
otočit	otočit	k5eAaPmF	otočit
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
si	se	k3xPyFc3	se
zajistit	zajistit	k5eAaPmF	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
již	již	k6eAd1	již
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
napomohl	napomoct	k5eAaPmAgMnS	napomoct
tomu	ten	k3xDgNnSc3	ten
remízový	remízový	k2eAgInSc4d1	remízový
zápas	zápas	k1gInSc4	zápas
Německa	Německo	k1gNnSc2	Německo
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Baroš	Baroš	k1gMnSc1	Baroš
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
utkání	utkání	k1gNnSc6	utkání
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
23	[number]	k4	23
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
pronikl	proniknout	k5eAaPmAgMnS	proniknout
do	do	k7c2	do
pokutového	pokutový	k2eAgNnSc2d1	pokutové
území	území	k1gNnSc2	území
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgMnSc1d1	bránící
Jaap	Jaap	k1gMnSc1	Jaap
Stam	Stam	k1gMnSc1	Stam
mu	on	k3xPp3gMnSc3	on
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
odebrat	odebrat	k5eAaPmF	odebrat
míč	míč	k1gInSc4	míč
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
hráč	hráč	k1gMnSc1	hráč
jej	on	k3xPp3gNnSc2	on
s	s	k7c7	s
trochou	trocha	k1gFnSc7	trocha
štěstí	štěstí	k1gNnSc2	štěstí
posunul	posunout	k5eAaPmAgInS	posunout
na	na	k7c4	na
druhého	druhý	k4xOgMnSc4	druhý
útočníka	útočník	k1gMnSc4	útočník
Kollera	Koller	k1gMnSc4	Koller
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prostřelil	prostřelit	k5eAaPmAgMnS	prostřelit
nizozemského	nizozemský	k2eAgMnSc4d1	nizozemský
brankáře	brankář	k1gMnSc4	brankář
Edwina	Edwin	k1gMnSc4	Edwin
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Sara	Sar	k1gInSc2	Sar
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
důležitý	důležitý	k2eAgInSc1d1	důležitý
moment	moment	k1gInSc1	moment
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
71	[number]	k4	71
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
když	když	k8xS	když
Barošovi	Barošův	k2eAgMnPc1d1	Barošův
hrudí	hruď	k1gFnSc7	hruď
sklepl	sklepnout	k5eAaPmAgInS	sklepnout
míč	míč	k1gInSc1	míč
Jan	Jan	k1gMnSc1	Jan
Koller	Koller	k1gMnSc1	Koller
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
se	se	k3xPyFc4	se
z	z	k7c2	z
halfvoleje	halfvolej	k1gInSc2	halfvolej
trefil	trefit	k5eAaPmAgMnS	trefit
do	do	k7c2	do
rohu	roh	k1gInSc2	roh
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Sar	Sar	k1gFnSc7	Sar
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
na	na	k7c4	na
míč	míč	k1gInSc4	míč
sáhl	sáhnout	k5eAaPmAgMnS	sáhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
umístěnou	umístěný	k2eAgFnSc4d1	umístěná
prudkou	prudký	k2eAgFnSc4d1	prudká
ránu	rána	k1gFnSc4	rána
vytlačit	vytlačit	k5eAaPmF	vytlačit
mimo	mimo	k7c4	mimo
tyče	tyč	k1gFnPc4	tyč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmicer	Šmicer	k1gMnSc1	Šmicer
dokonal	dokonat	k5eAaPmAgMnS	dokonat
obrat	obrat	k1gInSc4	obrat
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
přihrávce	přihrávka	k1gFnSc6	přihrávka
Karla	Karel	k1gMnSc2	Karel
Poborského	Poborský	k2eAgMnSc2d1	Poborský
dával	dávat	k5eAaImAgMnS	dávat
míč	míč	k1gInSc4	míč
do	do	k7c2	do
prázdné	prázdný	k2eAgFnSc2d1	prázdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
vystoupení	vystoupení	k1gNnSc6	vystoupení
českého	český	k2eAgInSc2d1	český
celku	celek	k1gInSc2	celek
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
si	se	k3xPyFc3	se
trenér	trenér	k1gMnSc1	trenér
Karel	Karel	k1gMnSc1	Karel
Brückner	Brückner	k1gMnSc1	Brückner
mohl	moct	k5eAaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
9	[number]	k4	9
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
59	[number]	k4	59
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
o	o	k7c4	o
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
svůj	svůj	k3xOyFgInSc4	svůj
třetí	třetí	k4xOgInSc4	třetí
gól	gól	k1gInSc4	gól
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
Oliver	Oliver	k1gMnSc1	Oliver
Kahn	Kahn	k1gMnSc1	Kahn
sice	sice	k8xC	sice
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
střelu	střela	k1gFnSc4	střela
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
českému	český	k2eAgMnSc3d1	český
útočníkovi	útočník	k1gMnSc3	útočník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
neměl	mít	k5eNaImAgMnS	mít
problém	problém	k1gInSc4	problém
ji	on	k3xPp3gFnSc4	on
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
porazil	porazit	k5eAaPmAgInS	porazit
Německo	Německo	k1gNnSc4	Německo
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
plným	plný	k2eAgInSc7d1	plný
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
narazilo	narazit	k5eAaPmAgNnS	narazit
národní	národní	k2eAgNnSc1d1	národní
mužstvo	mužstvo	k1gNnSc1	mužstvo
na	na	k7c4	na
Dánsko	Dánsko	k1gNnSc4	Dánsko
<g/>
,	,	kIx,	,
celek	celek	k1gInSc1	celek
vyznávající	vyznávající	k2eAgInSc1d1	vyznávající
rovněž	rovněž	k9	rovněž
ofenzivní	ofenzivní	k2eAgFnSc4d1	ofenzivní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
63	[number]	k4	63
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
našel	najít	k5eAaPmAgMnS	najít
Poborský	Poborský	k2eAgMnSc1d1	Poborský
přihrávkou	přihrávka	k1gFnSc7	přihrávka
za	za	k7c4	za
obranu	obrana	k1gFnSc4	obrana
Milana	Milan	k1gMnSc2	Milan
Baroše	Baroš	k1gMnSc2	Baroš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obloučkem	oblouček	k1gInSc7	oblouček
překonal	překonat	k5eAaPmAgMnS	překonat
dánského	dánský	k2eAgMnSc4d1	dánský
brankáře	brankář	k1gMnSc4	brankář
Thomase	Thomas	k1gMnSc4	Thomas
Sø	Sø	k1gMnSc4	Sø
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
později	pozdě	k6eAd2	pozdě
přidal	přidat	k5eAaPmAgMnS	přidat
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
branku	branka	k1gFnSc4	branka
a	a	k8xC	a
české	český	k2eAgNnSc1d1	české
mužstvo	mužstvo	k1gNnSc1	mužstvo
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
nad	nad	k7c7	nad
skandinávským	skandinávský	k2eAgMnSc7d1	skandinávský
soupeřem	soupeř	k1gMnSc7	soupeř
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
favorizovaný	favorizovaný	k2eAgInSc1d1	favorizovaný
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
utkal	utkat	k5eAaPmAgInS	utkat
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
turnaje	turnaj	k1gInSc2	turnaj
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
mu	on	k3xPp3gMnSc3	on
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
gól	gól	k1gInSc4	gól
Traianose	Traianosa	k1gFnSc3	Traianosa
Dellase	Dellasa	k1gFnSc3	Dellasa
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
odehrál	odehrát	k5eAaPmAgMnS	odehrát
celý	celý	k2eAgInSc4d1	celý
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
ve	v	k7c6	v
102	[number]	k4	102
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
obdržel	obdržet	k5eAaPmAgMnS	obdržet
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
s	s	k7c7	s
5	[number]	k4	5
góly	gól	k1gInPc7	gól
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
turnaje	turnaj	k1gInSc2	turnaj
před	před	k7c7	před
čtyřgólovými	čtyřgólový	k2eAgFnPc7d1	čtyřgólová
Waynem	Wayno	k1gNnSc7	Wayno
Rooneym	Rooneymum	k1gNnPc2	Rooneymum
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Ruudem	Ruudo	k1gNnSc7	Ruudo
van	vana	k1gFnPc2	vana
Nistelrooyem	Nistelrooyem	k1gInSc4	Nistelrooyem
z	z	k7c2	z
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
All-Star	All-Stara	k1gFnPc2	All-Stara
sestavy	sestava	k1gFnSc2	sestava
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
MS	MS	kA	MS
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
E	E	kA	E
postupně	postupně	k6eAd1	postupně
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Ghanou	Ghana	k1gFnSc7	Ghana
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
zápasů	zápas	k1gInPc2	zápas
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
proti	proti	k7c3	proti
USA	USA	kA	USA
<g/>
,	,	kIx,	,
výhra	výhra	k1gFnSc1	výhra
ČR	ČR	kA	ČR
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
prohra	prohra	k1gFnSc1	prohra
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Ghanou	Ghana	k1gFnSc7	Ghana
<g/>
)	)	kIx)	)
Baroš	Baroš	k1gMnSc1	Baroš
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zranil	zranit	k5eAaPmAgMnS	zranit
v	v	k7c6	v
přípravném	přípravný	k2eAgInSc6d1	přípravný
zápase	zápas	k1gInSc6	zápas
před	před	k7c7	před
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
proti	proti	k7c3	proti
Trinidadu	Trinidad	k1gInSc3	Trinidad
a	a	k8xC	a
Tobagu	Tobag	k1gInSc3	Tobag
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
až	až	k9	až
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
zápase	zápas	k1gInSc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
prohry	prohra	k1gFnSc2	prohra
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
obsazení	obsazení	k1gNnSc3	obsazení
nepostupového	postupový	k2eNgInSc2d1	nepostupový
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
3	[number]	k4	3
body	bod	k1gInPc4	bod
za	za	k7c7	za
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Ghanou	Ghana	k1gFnSc7	Ghana
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
hrál	hrát	k5eAaImAgMnS	hrát
do	do	k7c2	do
64	[number]	k4	64
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc4	minuta
a	a	k8xC	a
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
pro	pro	k7c4	pro
soupeře	soupeř	k1gMnPc4	soupeř
vystřídán	vystřídán	k2eAgInSc4d1	vystřídán
Davidem	David	k1gMnSc7	David
Jarolímem	Jarolím	k1gMnSc7	Jarolím
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ME	ME	kA	ME
2008	[number]	k4	2008
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
nalosován	nalosován	k2eAgInSc1d1	nalosován
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
,	,	kIx,	,
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
zahajovací	zahajovací	k2eAgNnSc1d1	zahajovací
utkání	utkání	k1gNnSc1	utkání
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
Baroš	Baroš	k1gMnSc1	Baroš
nehrál	hrát	k5eNaImAgMnS	hrát
<g/>
,	,	kIx,	,
utkání	utkání	k1gNnSc1	utkání
skončilo	skončit	k5eAaPmAgNnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
ČR	ČR	kA	ČR
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c6	na
hrotu	hrot	k1gInSc6	hrot
útoku	útok	k1gInSc2	útok
místo	místo	k7c2	místo
Jana	Jan	k1gMnSc2	Jan
Kollera	Koller	k1gMnSc2	Koller
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
se	se	k3xPyFc4	se
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
prohrál	prohrát	k5eAaPmAgInS	prohrát
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
celky	celek	k1gInPc1	celek
utkaly	utkat	k5eAaPmAgInP	utkat
o	o	k7c4	o
přímý	přímý	k2eAgInSc4d1	přímý
postup	postup	k1gInSc4	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
sledoval	sledovat	k5eAaImAgMnS	sledovat
Baroš	Baroš	k1gMnSc1	Baroš
z	z	k7c2	z
lavičky	lavička	k1gFnSc2	lavička
kolaps	kolaps	k1gInSc4	kolaps
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
udržet	udržet	k5eAaPmF	udržet
náskok	náskok	k1gInSc4	náskok
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
prohrál	prohrát	k5eAaPmAgMnS	prohrát
výsledkem	výsledek	k1gInSc7	výsledek
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Baroš	Baroš	k1gMnSc1	Baroš
obdržel	obdržet	k5eAaPmAgMnS	obdržet
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
za	za	k7c4	za
protesty	protest	k1gInPc4	protest
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ME	ME	kA	ME
2012	[number]	k4	2012
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
octl	octnout	k5eAaPmAgInS	octnout
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
v	v	k7c6	v
papírově	papírově	k6eAd1	papírově
snadné	snadný	k2eAgFnSc6d1	snadná
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
A	a	k9	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Michal	Michal	k1gMnSc1	Michal
Bílek	Bílek	k1gMnSc1	Bílek
na	na	k7c4	na
zkušeného	zkušený	k2eAgMnSc4d1	zkušený
útočníka	útočník	k1gMnSc4	útočník
sázel	sázet	k5eAaImAgMnS	sázet
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
sestavy	sestava	k1gFnSc2	sestava
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
zde	zde	k6eAd1	zde
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
odehrál	odehrát	k5eAaPmAgInS	odehrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Baroš	Baroš	k1gMnSc1	Baroš
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
u	u	k7c2	u
prohry	prohra	k1gFnSc2	prohra
českého	český	k2eAgNnSc2d1	české
mužstva	mužstvo	k1gNnSc2	mužstvo
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
85	[number]	k4	85
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
jej	on	k3xPp3gMnSc4	on
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
David	David	k1gMnSc1	David
Limberský	Limberský	k2eAgMnSc1d1	Limberský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Řecku	Řecko	k1gNnSc3	Řecko
odehrál	odehrát	k5eAaPmAgMnS	odehrát
64	[number]	k4	64
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
střídal	střídat	k5eAaImAgMnS	střídat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pekhart	Pekharta	k1gFnPc2	Pekharta
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
uchoval	uchovat	k5eAaPmAgInS	uchovat
si	se	k3xPyFc3	se
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
postup	postup	k1gInSc4	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
zápase	zápas	k1gInSc6	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byl	být	k5eAaImAgMnS	být
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
autorem	autor	k1gMnSc7	autor
přihrávky	přihrávka	k1gFnSc2	přihrávka
na	na	k7c4	na
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
gól	gól	k1gInSc4	gól
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
posunul	posunout	k5eAaPmAgInS	posunout
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hübschman	Hübschman	k1gMnSc1	Hübschman
zachytil	zachytit	k5eAaPmAgMnS	zachytit
nepřesnou	přesný	k2eNgFnSc4d1	nepřesná
rozehrávku	rozehrávka	k1gFnSc4	rozehrávka
polských	polský	k2eAgMnPc2d1	polský
hráčů	hráč	k1gMnPc2	hráč
za	za	k7c7	za
půlící	půlící	k2eAgFnSc7d1	půlící
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
přihrál	přihrát	k5eAaPmAgMnS	přihrát
volnému	volný	k2eAgMnSc3d1	volný
Barošovi	Baroš	k1gMnSc3	Baroš
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
naběhl	naběhnout	k5eAaPmAgMnS	naběhnout
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
velkého	velký	k2eAgNnSc2d1	velké
vápna	vápno	k1gNnSc2	vápno
a	a	k8xC	a
přiťukl	přiťuknout	k5eAaPmAgMnS	přiťuknout
míč	míč	k1gInSc4	míč
Petru	Petr	k1gMnSc3	Petr
Jiráčkovi	Jiráček	k1gMnSc3	Jiráček
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
prostřelil	prostřelit	k5eAaPmAgMnS	prostřelit
polského	polský	k2eAgMnSc4d1	polský
brankáře	brankář	k1gMnSc4	brankář
Przemysława	Przemysławus	k1gMnSc4	Przemysławus
Tytońa	Tytońus	k1gMnSc4	Tytońus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
s	s	k7c7	s
celkově	celkově	k6eAd1	celkově
šesti	šest	k4xCc7	šest
body	bod	k1gInPc7	bod
postoupil	postoupit	k5eAaPmAgInS	postoupit
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
pasivní	pasivní	k2eAgNnSc4d1	pasivní
skóre	skóre	k1gNnSc4	skóre
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
zápasů	zápas	k1gInPc2	zápas
<g/>
)	)	kIx)	)
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
turnaje	turnaj	k1gInSc2	turnaj
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Ruskem	Rusko	k1gNnSc7	Rusko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
skončilo	skončit	k5eAaPmAgNnS	skončit
druhé	druhý	k4xOgFnSc2	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálový	čtvrtfinálový	k2eAgInSc1d1	čtvrtfinálový
zápas	zápas	k1gInSc1	zápas
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Portugalsku	Portugalsko	k1gNnSc3	Portugalsko
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
prohrál	prohrát	k5eAaPmAgMnS	prohrát
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
s	s	k7c7	s
turnajem	turnaj	k1gInSc7	turnaj
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
odehrál	odehrát	k5eAaPmAgMnS	odehrát
celé	celý	k2eAgNnSc4d1	celé
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
gólově	gólově	k6eAd1	gólově
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
mužstva	mužstvo	k1gNnSc2	mužstvo
z	z	k7c2	z
šampionátu	šampionát	k1gInSc2	šampionát
hráč	hráč	k1gMnSc1	hráč
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
všech	všecek	k3xTgInPc2	všecek
9	[number]	k4	9
gólů	gól	k1gInPc2	gól
Milana	Milan	k1gMnSc2	Milan
Baroše	Baroš	k1gMnSc2	Baroš
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
reprezentaci	reprezentace	k1gFnSc6	reprezentace
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
Seznam	seznam	k1gInSc4	seznam
všech	všecek	k3xTgInPc2	všecek
41	[number]	k4	41
gólů	gól	k1gInPc2	gól
Milana	Milan	k1gMnSc2	Milan
Baroše	Baroš	k1gMnSc2	Baroš
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
reklamních	reklamní	k2eAgFnPc6d1	reklamní
kampaních	kampaň	k1gFnPc6	kampaň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
firmy	firma	k1gFnPc1	firma
Nike	Nike	k1gFnSc2	Nike
nebo	nebo	k8xC	nebo
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
Victoria-Volksbanken	Victoria-Volksbankna	k1gFnPc2	Victoria-Volksbankna
a	a	k8xC	a
předvolební	předvolební	k2eAgFnSc6d1	předvolební
kampani	kampaň	k1gFnSc6	kampaň
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
poslance	poslanec	k1gMnSc2	poslanec
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Tlustého	tlusté	k1gNnSc2	tlusté
(	(	kIx(	(
<g/>
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
"	"	kIx"	"
<g/>
Natvrdo	natvrdo	k6eAd1	natvrdo
<g/>
"	"	kIx"	"
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Baroš	Baroš	k1gMnSc1	Baroš
pózoval	pózovat	k5eAaImAgMnS	pózovat
na	na	k7c6	na
volebních	volební	k2eAgInPc6d1	volební
plakátech	plakát	k1gInPc6	plakát
ODS	ODS	kA	ODS
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
za	za	k7c4	za
slib	slib	k1gInSc4	slib
dotace	dotace	k1gFnPc4	dotace
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
mládežnické	mládežnický	k2eAgFnSc2d1	mládežnická
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
vybudování	vybudování	k1gNnSc4	vybudování
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Baroš	Baroš	k1gMnSc1	Baroš
přes	přes	k7c4	přes
své	svůj	k3xOyFgFnPc4	svůj
nepopiratelné	popiratelný	k2eNgFnPc4d1	nepopiratelná
zásluhy	zásluha	k1gFnPc4	zásluha
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
postavu	postava	k1gFnSc4	postava
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
reportáže	reportáž	k1gFnPc4	reportáž
bulvárních	bulvární	k2eAgNnPc2d1	bulvární
médií	médium	k1gNnPc2	médium
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
prakticky	prakticky	k6eAd1	prakticky
nekomunikoval	komunikovat	k5eNaImAgInS	komunikovat
s	s	k7c7	s
novináři	novinář	k1gMnPc7	novinář
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zcela	zcela	k6eAd1	zcela
ztratil	ztratit	k5eAaPmAgInS	ztratit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
a	a	k8xC	a
posílil	posílit	k5eAaPmAgInS	posílit
tak	tak	k6eAd1	tak
vytvářený	vytvářený	k2eAgInSc1d1	vytvářený
negativní	negativní	k2eAgInSc1d1	negativní
obraz	obraz	k1gInSc1	obraz
"	"	kIx"	"
<g/>
nafoukaného	nafoukaný	k2eAgMnSc4d1	nafoukaný
frajera	frajer	k1gMnSc4	frajer
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
všechny	všechen	k3xTgInPc4	všechen
a	a	k8xC	a
všechno	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gInSc1	jeho
záporný	záporný	k2eAgInSc1d1	záporný
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
některým	některý	k3yIgMnPc3	některý
novinářům	novinář	k1gMnPc3	novinář
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
aférou	aféra	k1gFnSc7	aféra
po	po	k7c6	po
odjezdu	odjezd	k1gInSc6	odjezd
z	z	k7c2	z
Eura	euro	k1gNnSc2	euro
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zabitím	zabití	k1gNnSc7	zabití
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
cameo	cameo	k6eAd1	cameo
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Góóól	Góóól	k1gInSc1	Góóól
<g/>
!	!	kIx.	!
</s>
<s>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
FC	FC	kA	FC
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Anglického	anglický	k2eAgInSc2d1	anglický
ligového	ligový	k2eAgInSc2d1	ligový
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Olympique	Olympique	k1gFnSc1	Olympique
Lyon	Lyon	k1gInSc1	Lyon
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítězit	k5eAaImRp2nS	vítězit
Ligue	Ligue	k1gInSc1	Ligue
1	[number]	k4	1
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
francouzského	francouzský	k2eAgInSc2d1	francouzský
superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Portsmouth	Portsmouth	k1gInSc1	Portsmouth
FC	FC	kA	FC
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
FA	fa	k1gNnSc2	fa
Cupu	cup	k1gInSc2	cup
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
SK	Sk	kA	Sk
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Süper	Süper	k1gMnSc1	Süper
Lig	liga	k1gFnPc2	liga
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
tureckého	turecký	k2eAgInSc2d1	turecký
Superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
ME	ME	kA	ME
"	"	kIx"	"
<g/>
21	[number]	k4	21
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
MS	MS	kA	MS
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
účast	účast	k1gFnSc1	účast
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
ME	ME	kA	ME
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
-	-	kIx~	-
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
OH	OH	kA	OH
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
Talent	talent	k1gInSc1	talent
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
ME	ME	kA	ME
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
5	[number]	k4	5
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
ME	ME	kA	ME
2004	[number]	k4	2004
All-Star	All-Star	k1gInSc1	All-Star
Team	team	k1gInSc4	team
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
turecké	turecký	k2eAgFnSc2d1	turecká
Süper	Süpero	k1gNnPc2	Süpero
Lig	liga	k1gFnPc2	liga
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
20	[number]	k4	20
gólů	gól	k1gInPc2	gól
<g/>
)	)	kIx)	)
Hráč	hráč	k1gMnSc1	hráč
měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
Gambrinus	gambrinus	k1gInSc1	gambrinus
lize	liga	k1gFnSc3	liga
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
