<s>
Miříkovité	Miříkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Apiaceae	Apiacea	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
též	též	k9
mrkvovité	mrkvovitý	k2eAgFnSc2d1
nebo	nebo	k8xC
okoličnaté	okoličnatý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Umbelliferae	Umbellifera	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
čeleď	čeleď	k1gFnSc4
vyšších	vysoký	k2eAgFnPc2d2
dvouděložných	dvouděložný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
z	z	k7c2
řádu	řád	k1gInSc2
miříkotvaré	miříkotvarý	k2eAgMnPc4d1
(	(	kIx(
<g/>
Apiales	Apiales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
