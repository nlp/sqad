<s>
Miříkovité	Miříkovitý	k2eAgNnSc1d1
</s>
<s>
Miříkovité	Miříkovitý	k2eAgInPc1d1
Hladýš	hladýš	k1gInSc1
Laserpitium	Laserpitium	k1gNnSc4
halleri	halleri	k6eAd1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
miříkotvaré	miříkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Apiales	Apiales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
miříkovité	miříkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Apiaceae	Apiacea	k1gInPc4
<g/>
)	)	kIx)
<g/>
Lindl	Lindl	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1836	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Miříkovité	Miříkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Apiaceae	Apiacea	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
též	též	k9
mrkvovité	mrkvovitý	k2eAgFnSc2d1
nebo	nebo	k8xC
okoličnaté	okoličnatý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Umbelliferae	Umbellifera	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
čeleď	čeleď	k1gFnSc4
vyšších	vysoký	k2eAgFnPc2d2
dvouděložných	dvouděložný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
z	z	k7c2
řádu	řád	k1gInSc2
miříkotvaré	miříkotvarý	k2eAgMnPc4d1
(	(	kIx(
<g/>
Apiales	Apiales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
přes	přes	k7c4
420	#num#	k4
rodů	rod	k1gInPc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
3500	#num#	k4
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<s>
</s>
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
byliny	bylina	k1gFnPc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
aromatické	aromatický	k2eAgInPc1d1
<g/>
,	,	kIx,
s	s	k7c7
bohatě	bohatě	k6eAd1
členěnými	členěný	k2eAgInPc7d1
listy	list	k1gInPc7
a	a	k8xC
s	s	k7c7
květy	květ	k1gInPc7
v	v	k7c6
jednoduchém	jednoduchý	k2eAgInSc6d1
nebo	nebo	k8xC
složeném	složený	k2eAgNnSc6d1
okoličnatém	okoličnatý	k2eAgNnSc6d1
květenství	květenství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miříkovité	Miříkovitý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
bohatě	bohatě	k6eAd1
zastoupeny	zastoupit	k5eAaPmNgInP
ve	v	k7c6
flóře	flóra	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
druhů	druh	k1gInPc2
má	mít	k5eAaImIp3nS
hospodářský	hospodářský	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Miříkovité	Miříkovitý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
jednoleté	jednoletý	k2eAgFnPc1d1
nebo	nebo	k8xC
vytrvalé	vytrvalý	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
<g/>
,	,	kIx,
občas	občas	k6eAd1
na	na	k7c6
bázi	báze	k1gFnSc6
stonku	stonka	k1gFnSc4
dřevnatějící	dřevnatějící	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
výjimečně	výjimečně	k6eAd1
je	být	k5eAaImIp3nS
stonek	stonek	k1gInSc1
dřevnatý	dřevnatý	k2eAgInSc1d1
(	(	kIx(
<g/>
Myrrhidendron	Myrrhidendron	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stonek	stonek	k1gInSc1
je	být	k5eAaImIp3nS
dutý	dutý	k2eAgInSc1d1
nebo	nebo	k8xC
plný	plný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
střídavé	střídavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
vzácně	vzácně	k6eAd1
vstřícné	vstřícný	k2eAgFnPc1d1
nebo	nebo	k8xC
v	v	k7c6
přízemní	přízemní	k2eAgFnSc6d1
růžici	růžice	k1gFnSc6
<g/>
,	,	kIx,
bez	bez	k7c2
palistů	palist	k1gInPc2
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgInPc1d1
nebo	nebo	k8xC
složené	složený	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čepel	čepel	k1gFnSc1
listů	list	k1gInPc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
silně	silně	k6eAd1
členěná	členěný	k2eAgFnSc1d1
<g/>
,	,	kIx,
často	často	k6eAd1
i	i	k9
do	do	k7c2
několika	několik	k4yIc2
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řapík	řapík	k1gInSc4
lodyžních	lodyžní	k2eAgInPc2d1
listů	list	k1gInPc2
má	mít	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
na	na	k7c6
bázi	báze	k1gFnSc6
výraznou	výrazný	k2eAgFnSc4d1
pochvu	pochva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostliny	rostlina	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
ve	v	k7c6
všech	všecek	k3xTgFnPc6
částech	část	k1gFnPc6
včetně	včetně	k7c2
oplodí	oplodí	k1gNnSc2
sekreční	sekreční	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
a	a	k8xC
kanálky	kanálek	k1gInPc4
a	a	k8xC
bývají	bývat	k5eAaImIp3nP
aromatické	aromatický	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Květy	květ	k1gInPc1
jsou	být	k5eAaImIp3nP
drobné	drobný	k2eAgMnPc4d1
<g/>
,	,	kIx,
pětičetné	pětičetný	k2eAgMnPc4d1
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
oboupohlavné	oboupohlavný	k2eAgFnPc1d1
<g/>
,	,	kIx,
pravidelné	pravidelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
okrajové	okrajový	k2eAgInPc4d1
květy	květ	k1gInPc4
v	v	k7c4
květenství	květenství	k1gNnSc4
u	u	k7c2
některých	některý	k3yIgMnPc2
zástupců	zástupce	k1gMnPc2
asymetrické	asymetrický	k2eAgInPc1d1
a	a	k8xC
paprskující	paprskující	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgFnP
v	v	k7c6
jednoduchých	jednoduchý	k2eAgInPc6d1
okolících	okolík	k1gInPc6
nebo	nebo	k8xC
v	v	k7c6
okolících	okolík	k1gInPc6
složených	složený	k2eAgFnPc2d1
z	z	k7c2
okolíčků	okolíček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
květenstvím	květenství	k1gNnSc7
bývá	bývat	k5eAaImIp3nS
vytvořen	vytvořen	k2eAgInSc1d1
obal	obal	k1gInSc1
z	z	k7c2
listenů	listen	k1gInPc2
a	a	k8xC
pod	pod	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
okolíčky	okolíček	k1gInPc7
obalíček	obalíček	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgMnPc2
rodů	rod	k1gInPc2
není	být	k5eNaImIp3nS
obal	obal	k1gInSc4
či	či	k8xC
obalíček	obalíček	k1gInSc4
(	(	kIx(
<g/>
případně	případně	k6eAd1
obojí	obojí	k4xRgFnPc4
<g/>
)	)	kIx)
vyvinut	vyvinout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květenství	květenství	k1gNnSc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
volná	volný	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
dlouhými	dlouhý	k2eAgFnPc7d1
stopkami	stopka	k1gFnPc7
květů	květ	k1gInPc2
a	a	k8xC
obalíčků	obalíček	k1gInPc2
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
však	však	k9
být	být	k5eAaImF
i	i	k9
stažená	stažený	k2eAgNnPc4d1
a	a	k8xC
tvořit	tvořit	k5eAaImF
hlávku	hlávka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalich	kalich	k1gInSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc1d1
srostlý	srostlý	k2eAgInSc1d1
se	s	k7c7
semeníkem	semeník	k1gInSc7
a	a	k8xC
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
5	#num#	k4
cípů	cíp	k1gInPc2
na	na	k7c6
okraji	okraj	k1gInSc6
semeníku	semeník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunní	korunní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
volné	volný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
5	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
volné	volný	k2eAgInPc1d1
a	a	k8xC
střídají	střídat	k5eAaImIp3nP
se	se	k3xPyFc4
s	s	k7c7
korunními	korunní	k2eAgInPc7d1
lístky	lístek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prašníky	prašník	k1gInPc7
jsou	být	k5eAaImIp3nP
žluté	žlutý	k2eAgInPc1d1
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
fialové	fialový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeník	semeník	k1gInSc1
je	být	k5eAaImIp3nS
spodní	spodní	k2eAgInSc4d1
<g/>
,	,	kIx,
srostlý	srostlý	k2eAgInSc4d1
ze	z	k7c2
2	#num#	k4
plodolistů	plodolist	k1gInPc2
<g/>
,	,	kIx,
se	s	k7c7
2	#num#	k4
komůrkami	komůrka	k1gFnPc7
a	a	k8xC
2	#num#	k4
čnělkami	čnělka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
horní	horní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
na	na	k7c4
semeník	semeník	k1gInSc4
nasedá	nasedat	k5eAaImIp3nS
žlaznatý	žlaznatý	k2eAgInSc4d1
terč	terč	k1gInSc4
<g/>
,	,	kIx,
vybíhající	vybíhající	k2eAgFnSc4d1
v	v	k7c4
rozšířenou	rozšířený	k2eAgFnSc4d1
bázi	báze	k1gFnSc4
čnělek	čnělka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
plodolistu	plodolist	k1gInSc6
je	být	k5eAaImIp3nS
jediné	jediný	k2eAgNnSc1d1
vajíčko	vajíčko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
dvounažka	dvounažka	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
zralosti	zralost	k1gFnPc4
se	s	k7c7
zpravidla	zpravidla	k6eAd1
rozpadající	rozpadající	k2eAgFnSc7d1
na	na	k7c4
dva	dva	k4xCgInPc4
plůdky	plůdek	k1gInPc4
(	(	kIx(
<g/>
merikarpia	merikarpia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
spojené	spojený	k2eAgInPc1d1
plodonošem	plodonoš	k1gInSc7
(	(	kIx(
<g/>
karpoforem	karpofor	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semena	semeno	k1gNnSc2
obsahují	obsahovat	k5eAaImIp3nP
malé	malý	k2eAgNnSc4d1
embryo	embryo	k1gNnSc4
a	a	k8xC
hojný	hojný	k2eAgInSc4d1
endosperm	endosperm	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čeleď	čeleď	k1gFnSc1
miříkovité	miříkovitý	k2eAgNnSc1d1
zahrnuje	zahrnovat	k5eAaImIp3nS
asi	asi	k9
3800	#num#	k4
druhů	druh	k1gInPc2
ve	v	k7c6
434	#num#	k4
rodech	rod	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
rody	rod	k1gInPc1
jsou	být	k5eAaImIp3nP
máčka	máčka	k1gFnSc1
(	(	kIx(
<g/>
Eryngium	Eryngium	k1gNnSc1
<g/>
,	,	kIx,
přes	přes	k7c4
200	#num#	k4
druhů	druh	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prorostlík	prorostlík	k1gInSc1
(	(	kIx(
<g/>
Bupleurum	Bupleurum	k1gInSc1
<g/>
,	,	kIx,
asi	asi	k9
190	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bedrník	bedrník	k1gInSc1
(	(	kIx(
<g/>
Pimpinella	Pimpinella	k1gMnSc1
<g/>
,	,	kIx,
150	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ločidlo	ločidlo	k1gNnSc1
(	(	kIx(
<g/>
Ferula	Ferula	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
130	#num#	k4
druhů	druh	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nezralé	zralý	k2eNgInPc1d1
plody	plod	k1gInPc1
kerblíku	kerblík	k1gInSc2
lesního	lesní	k2eAgInSc2d1
(	(	kIx(
<g/>
Anthriscus	Anthriscus	k1gInSc1
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Složený	složený	k2eAgInSc1d1
okolík	okolík	k1gInSc1
smldníku	smldník	k1gInSc2
jeleního	jelení	k2eAgInSc2d1
(	(	kIx(
<g/>
Peucedanum	Peucedanum	k1gInSc1
cervaria	cervarium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Pochva	pochva	k1gFnSc1
na	na	k7c6
bázi	báze	k1gFnSc6
listu	list	k1gInSc2
anděliky	andělika	k1gFnSc2
lékařské	lékařský	k2eAgFnSc2d1
(	(	kIx(
<g/>
Angelica	Angelic	k2eAgNnPc1d1
archangelica	archangelicum	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Hvězdnatec	Hvězdnatec	k1gMnSc1
zubatý	zubatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Hacquetia	Hacquetia	k1gFnSc1
epipactis	epipactis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dřevnatý	dřevnatý	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
čeledi	čeleď	k1gFnSc2
<g/>
,	,	kIx,
Myrrhidendron	Myrrhidendron	k1gInSc1
donnellsmithii	donnellsmithie	k1gFnSc4
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Miříkovité	Miříkovitý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
rozšířeny	rozšířit	k5eAaPmNgInP
kosmopolitně	kosmopolitně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgInPc1d1
rody	rod	k1gInPc1
jsou	být	k5eAaImIp3nP
zastoupeny	zastoupit	k5eAaPmNgInP
jak	jak	k6eAd1
v	v	k7c6
Eurasii	Eurasie	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
arktických	arktický	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
druhové	druhový	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
Eurasii	Eurasie	k1gFnSc6
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
pouze	pouze	k6eAd1
50	#num#	k4
rodů	rod	k1gInPc2
a	a	k8xC
500	#num#	k4
druhů	druh	k1gInPc2
roste	růst	k5eAaImIp3nS
v	v	k7c6
neotropické	neotropický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tropech	trop	k1gInPc6
rostou	růst	k5eAaImIp3nP
převážně	převážně	k6eAd1
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
české	český	k2eAgFnSc6d1
květeně	květena	k1gFnSc6
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
čeleď	čeleď	k1gFnSc1
zastoupena	zastoupen	k2eAgFnSc1d1
asi	asi	k9
50	#num#	k4
rody	rod	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
V	v	k7c6
klasické	klasický	k2eAgFnSc6d1
taxonomii	taxonomie	k1gFnSc6
byla	být	k5eAaImAgFnS
čeleď	čeleď	k1gFnSc1
miříkovité	miříkovitý	k2eAgNnSc4d1
spolu	spolu	k6eAd1
s	s	k7c7
čeledí	čeleď	k1gFnSc7
aralkovité	aralkovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Araliaceae	Araliacea	k1gFnSc2
<g/>
)	)	kIx)
řazena	řadit	k5eAaImNgFnS
do	do	k7c2
řádu	řád	k1gInSc2
miříkotvaré	miříkotvarý	k2eAgMnPc4d1
(	(	kIx(
<g/>
Apiales	Apiales	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
nazývaného	nazývaný	k2eAgInSc2d1
aralkotvaré	aralkotvarý	k2eAgInPc1d1
(	(	kIx(
<g/>
Araliales	Araliales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
členěna	členit	k5eAaImNgFnS
na	na	k7c4
tři	tři	k4xCgFnPc4
podčeledi	podčeleď	k1gFnPc4
<g/>
:	:	kIx,
Apioideae	Apioidea	k1gMnPc4
<g/>
,	,	kIx,
Saniculoideae	Saniculoidea	k1gMnPc4
a	a	k8xC
Hydrocotyloideae	Hydrocotyloidea	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molekulární	molekulární	k2eAgInPc1d1
výzkumy	výzkum	k1gInPc1
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgFnPc1
dvě	dva	k4xCgFnPc1
podčeledi	podčeleď	k1gFnPc1
jsou	být	k5eAaImIp3nP
sesterské	sesterský	k2eAgFnPc1d1
a	a	k8xC
monofyletické	monofyletický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Hydrocotyloideae	Hydrocotyloidea	k1gFnPc1
jsou	být	k5eAaImIp3nP
parafyletická	parafyletický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dva	dva	k4xCgInPc1
rody	rod	k1gInPc1
(	(	kIx(
<g/>
Hydrocotyle	Hydrocotyl	k1gInSc5
a	a	k8xC
Trachymene	Trachymen	k1gInSc5
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
následně	následně	k6eAd1
přeřazeny	přeřadit	k5eAaPmNgInP
do	do	k7c2
čeledi	čeleď	k1gFnSc2
aralkovité	aralkovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Araliaceae	Araliacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekologické	ekologický	k2eAgFnPc1d1
interakce	interakce	k1gFnPc1
</s>
<s>
Květy	květ	k1gInPc1
miříkovitých	miříkovitý	k2eAgFnPc2d1
obsahují	obsahovat	k5eAaImIp3nP
nektar	nektar	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
navštěvovány	navštěvovat	k5eAaImNgFnP
širokou	široký	k2eAgFnSc7d1
paletou	paleta	k1gFnSc7
létajícího	létající	k2eAgInSc2d1
hmyzu	hmyz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Máčky	máčka	k1gFnSc2
(	(	kIx(
<g/>
rod	rod	k1gInSc1
Eryngia	Eryngium	k1gNnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
také	také	k9
známé	známý	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
odolné	odolný	k2eAgFnPc1d1
a	a	k8xC
snadno	snadno	k6eAd1
šiřitelné	šiřitelný	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
<g/>
,	,	kIx,
přitahující	přitahující	k2eAgInPc4d1
často	často	k6eAd1
motýly	motýl	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
andělika	andělika	k1gFnSc1
(	(	kIx(
<g/>
Angelica	Angelica	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
např.	např.	kA
andělika	andělika	k1gFnSc1
lékařská	lékařský	k2eAgFnSc1d1
</s>
<s>
arakača	arakača	k1gFnSc1
(	(	kIx(
<g/>
Arracacia	Arracacia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
azorela	azorela	k1gFnSc1
(	(	kIx(
<g/>
Azorella	Azorella	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
bedrník	bedrník	k1gInSc1
(	(	kIx(
<g/>
Pimpinella	Pimpinella	k1gFnSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
bedrník	bedrník	k1gInSc1
anýz	anýz	k1gInSc1
</s>
<s>
bezobalka	bezobalka	k1gFnSc1
(	(	kIx(
<g/>
Trinia	Trinium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
bolehlav	bolehlav	k1gInSc1
(	(	kIx(
<g/>
Conium	Conium	k1gNnSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
bolehlav	bolehlav	k1gInSc4
plamatý	plamatý	k2eAgInSc1d1
</s>
<s>
bolševník	bolševník	k1gInSc1
(	(	kIx(
<g/>
Heracleum	Heracleum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
bolševníkovec	bolševníkovec	k1gInSc1
(	(	kIx(
<g/>
Molopospermum	Molopospermum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
boulesie	boulesie	k1gFnSc1
(	(	kIx(
<g/>
Bowlesia	Bowlesia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
bršlice	bršlice	k1gFnSc1
(	(	kIx(
<g/>
Aegopodium	Aegopodium	k1gNnSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
bršlice	bršlice	k1gFnSc1
kozí	kozí	k2eAgFnSc1d1
noha	noha	k1gFnSc1
</s>
<s>
bulvuška	bulvuška	k1gFnSc1
(	(	kIx(
<g/>
Bunium	Bunium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
čechřice	čechřice	k1gFnSc1
(	(	kIx(
<g/>
Myrrhis	Myrrhis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
čechřicovice	čechřicovice	k1gFnSc1
(	(	kIx(
<g/>
Osmorhiza	Osmorhiza	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
čechřina	čechřina	k1gFnSc1
(	(	kIx(
<g/>
Myrrhoides	Myrrhoides	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
děhel	děhel	k1gInSc1
(	(	kIx(
<g/>
Angelica	Angelica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
dejvor	dejvor	k1gMnSc1
(	(	kIx(
<g/>
Astrodaucus	Astrodaucus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
dejvorec	dejvorec	k1gInSc1
(	(	kIx(
<g/>
Caucalis	Caucalis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
dorema	dorema	k1gFnSc1
(	(	kIx(
<g/>
Dorema	Dorema	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
též	též	k9
ošak	ošak	k6eAd1
</s>
<s>
draporýl	draporýl	k1gMnSc1
(	(	kIx(
<g/>
Arctopus	Arctopus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
drsnoplodík	drsnoplodík	k1gInSc1
(	(	kIx(
<g/>
Trachyspermum	Trachyspermum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
echinofora	echinofora	k1gFnSc1
(	(	kIx(
<g/>
Echinophora	Echinophora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
fenykl	fenykl	k1gInSc1
(	(	kIx(
<g/>
Foeniculum	Foeniculum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
glenie	glenie	k1gFnPc1
(	(	kIx(
<g/>
Glehnia	Glehnium	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
halucha	halucha	k1gFnSc1
(	(	kIx(
<g/>
Oenanthe	Oenanthe	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hladýš	hladýš	k1gInSc1
(	(	kIx(
<g/>
Laserpitium	Laserpitium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
hvězdnatec	hvězdnatec	k1gMnSc1
(	(	kIx(
<g/>
Hacquetia	Hacquetia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
jarmanka	jarmanka	k1gFnSc1
(	(	kIx(
<g/>
Astrantia	Astrantia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
jarva	jarva	k1gFnSc1
(	(	kIx(
<g/>
Cnidium	Cnidium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
kadidlovka	kadidlovka	k1gFnSc1
(	(	kIx(
<g/>
Cachrys	Cachrys	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kerblík	kerblík	k1gInSc1
(	(	kIx(
<g/>
Anthriscus	Anthriscus	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
kerblík	kerblík	k1gInSc4
lesní	lesní	k2eAgInSc1d1
</s>
<s>
kmín	kmín	k1gInSc1
(	(	kIx(
<g/>
Carum	Carum	k?
<g/>
)	)	kIx)
</s>
<s>
kopr	kopr	k1gInSc1
(	(	kIx(
<g/>
Anethum	Anethum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
koprníček	koprníček	k1gInSc1
(	(	kIx(
<g/>
Ligusticum	Ligusticum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
koprník	koprník	k1gInSc1
(	(	kIx(
<g/>
Meum	Meum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
koriandr	koriandr	k1gInSc1
(	(	kIx(
<g/>
Coriandrum	Coriandrum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
koromáč	koromáč	k1gInSc1
(	(	kIx(
<g/>
Silaum	Silaum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
koromáček	koromáček	k1gInSc1
(	(	kIx(
<g/>
Pachypleurum	Pachypleurum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
krabilice	krabilice	k1gFnSc1
(	(	kIx(
<g/>
Chaerophyllum	Chaerophyllum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kryptotenie	kryptotenie	k1gFnPc1
(	(	kIx(
<g/>
Cryptotaenia	Cryptotaenium	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
libeček	libeček	k1gInSc1
(	(	kIx(
<g/>
Levisticum	Levisticum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
lileopka	lileopka	k1gFnSc1
(	(	kIx(
<g/>
Lilaeopsis	Lilaeopsis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ločidlo	ločidlo	k1gNnSc1
(	(	kIx(
<g/>
Ferula	Ferula	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ločidlovka	ločidlovka	k1gFnSc1
(	(	kIx(
<g/>
Ferulago	Ferulago	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
máčka	máčka	k1gFnSc1
(	(	kIx(
<g/>
Eryngium	Eryngium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
matizna	matizna	k1gFnSc1
(	(	kIx(
<g/>
Ostericum	Ostericum	k1gInSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angelica	Angelic	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
mázdřinec	mázdřinec	k1gInSc1
(	(	kIx(
<g/>
Pleurospermum	Pleurospermum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
miřík	miřík	k1gInSc1
(	(	kIx(
<g/>
Apium	Apium	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
miřík	miřík	k1gInSc1
celer	celer	k1gInSc1
</s>
<s>
morač	morač	k1gInSc1
(	(	kIx(
<g/>
Ammi	Ammi	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
moračina	moračina	k1gFnSc1
(	(	kIx(
<g/>
Opopanax	Opopanax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
mořice	mořice	k1gFnPc1
(	(	kIx(
<g/>
Lichtensteinia	Lichtensteinium	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
motar	motar	k1gInSc1
(	(	kIx(
<g/>
Crithmum	Crithmum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
mrkev	mrkev	k1gFnSc1
(	(	kIx(
<g/>
Daucus	Daucus	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
mrkev	mrkev	k1gFnSc1
obecná	obecná	k1gFnSc1
</s>
<s>
myroda	myroda	k1gFnSc1
(	(	kIx(
<g/>
Ptychotis	Ptychotis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
nížeň	nížeň	k1gFnSc1
(	(	kIx(
<g/>
Bolax	Bolax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
obrubovec	obrubovec	k1gInSc1
(	(	kIx(
<g/>
Lomatium	Lomatium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
olešník	olešník	k1gInSc1
(	(	kIx(
<g/>
Selinum	Selinum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
paprska	paprska	k1gFnSc1
(	(	kIx(
<g/>
Orlaya	Orlaya	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
pastinák	pastinák	k1gInSc1
(	(	kIx(
<g/>
Pastinaca	Pastinaca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
pernatka	pernatka	k1gFnSc1
(	(	kIx(
<g/>
Lagoecia	Lagoecia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
petržel	petržel	k1gFnSc1
(	(	kIx(
<g/>
Petroselinum	Petroselinum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
potočník	potočník	k1gInSc1
(	(	kIx(
<g/>
Berula	Berula	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
prangos	prangos	k1gMnSc1
(	(	kIx(
<g/>
Prangos	Prangos	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
prorostlík	prorostlík	k1gInSc1
(	(	kIx(
<g/>
Bupleurum	Bupleurum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
pupečníkovec	pupečníkovec	k1gMnSc1
(	(	kIx(
<g/>
Centella	Centella	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
rozpuk	rozpuk	k1gInSc1
(	(	kIx(
<g/>
Cicuta	Cicut	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
různotvarka	různotvarka	k1gFnSc1
(	(	kIx(
<g/>
Heteromorpha	Heteromorpha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
sesel	sesel	k1gInSc1
(	(	kIx(
<g/>
Seseli	Sesel	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
sevlák	sevlák	k1gInSc1
(	(	kIx(
<g/>
Sium	Sium	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
sison	sison	k1gMnSc1
(	(	kIx(
<g/>
Sison	Sison	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
smldník	smldník	k1gInSc1
(	(	kIx(
<g/>
Peucedanum	Peucedanum	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
např.	např.	kA
smldník	smldník	k1gInSc4
bahenní	bahenní	k2eAgInSc1d1
</s>
<s>
srpek	srpek	k1gInSc1
(	(	kIx(
<g/>
Falcaria	Falcarium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
šabrej	šabrat	k5eAaImRp2nS,k5eAaPmRp2nS
(	(	kIx(
<g/>
Cuminum	Cuminum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
šabřina	šabřina	k1gFnSc1
(	(	kIx(
<g/>
Conioselinum	Conioselinum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
štěničník	štěničník	k1gInSc1
(	(	kIx(
<g/>
Bifora	Bifora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
štětináč	štětináč	k1gMnSc1
(	(	kIx(
<g/>
Turgenia	Turgenium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
šutranka	šutranka	k1gFnSc1
(	(	kIx(
<g/>
Elaeoselinum	Elaeoselinum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
tapsie	tapsie	k1gFnSc1
(	(	kIx(
<g/>
Thapsia	Thapsia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
tetlucha	tetlucha	k1gFnSc1
(	(	kIx(
<g/>
Aethusa	Aethus	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
timoj	timoj	k1gInSc1
(	(	kIx(
<g/>
Laser	laser	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
tořice	tořice	k1gFnSc1
(	(	kIx(
<g/>
Torilis	Torilis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
tromín	tromín	k1gInSc1
(	(	kIx(
<g/>
Smyrnium	Smyrnium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
vochlice	vochlice	k1gFnSc1
(	(	kIx(
<g/>
Scandix	Scandix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
všedobr	všedobr	k1gInSc1
(	(	kIx(
<g/>
Imperatoria	Imperatorium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
výkrojka	výkrojka	k1gFnSc1
(	(	kIx(
<g/>
Asteriscium	Asteriscium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
zapalička	zapalička	k1gFnSc1
(	(	kIx(
<g/>
Tordylium	Tordylium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
žebřice	žebřice	k1gFnSc1
(	(	kIx(
<g/>
Libanotis	Libanotis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
žindava	žindava	k1gFnSc1
(	(	kIx(
<g/>
Sanicula	Sanicula	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Čeleď	čeleď	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
jak	jak	k8xS,k8xC
druhy	druh	k1gInPc1
využívané	využívaný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
potrava	potrava	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
druhy	druh	k1gInPc1
vysoce	vysoce	k6eAd1
toxické	toxický	k2eAgInPc1d1
(	(	kIx(
<g/>
např.	např.	kA
rozpuk	rozpuk	k1gInSc4
jízlivý	jízlivý	k2eAgInSc1d1
<g/>
,	,	kIx,
bolehlav	bolehlav	k1gInSc1
plamatý	plamatý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
známou	známý	k2eAgFnSc7d1
kořenovou	kořenový	k2eAgFnSc7d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
listovou	listový	k2eAgFnSc4d1
zeleninu	zelenina	k1gFnSc4
patří	patřit	k5eAaImIp3nS
mrkev	mrkev	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Daucus	Daucus	k1gInSc1
carota	carot	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
petržel	petržel	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Petroselinum	Petroselinum	k1gInSc1
crispum	crispum	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
miřík	miřík	k1gInSc1
celer	celer	k1gInSc1
(	(	kIx(
<g/>
Apium	Apium	k1gInSc1
graveolens	graveolens	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
byla	být	k5eAaImAgFnS
jako	jako	k9
kořenová	kořenový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
pěstována	pěstován	k2eAgFnSc1d1
také	také	k9
krabilice	krabilice	k1gFnSc1
hlíznatá	hlíznatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Chaerophyllum	Chaerophyllum	k1gInSc1
bulbosum	bulbosum	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
kerblík	kerblík	k1gInSc1
třebule	třebule	k1gFnSc2
(	(	kIx(
<g/>
Anthriscus	Anthriscus	k1gInSc1
cerefolium	cerefolium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
koření	koření	k1gNnSc2
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
zejména	zejména	k9
kmín	kmín	k1gInSc1
kořenný	kořenný	k2eAgInSc1d1
(	(	kIx(
<g/>
Carum	Carum	k?
carvi	carev	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fenykl	fenykl	k1gInSc4
obecný	obecný	k2eAgInSc4d1
(	(	kIx(
<g/>
Foeniculum	Foeniculum	k1gInSc4
vulgare	vulgar	k1gMnSc5
<g/>
)	)	kIx)
a	a	k8xC
bedrník	bedrník	k1gInSc1
anýz	anýz	k1gInSc1
(	(	kIx(
<g/>
Pimpinella	Pimpinella	k1gFnSc1
anisum	anisum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
rostliny	rostlina	k1gFnPc1
z	z	k7c2
této	tento	k3xDgFnSc2
čeledi	čeleď	k1gFnSc2
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
v	v	k7c4
léčitelství	léčitelství	k1gNnSc4
a	a	k8xC
lékařství	lékařství	k1gNnSc4
jakožto	jakožto	k8xS
léčivé	léčivý	k2eAgFnPc4d1
byliny	bylina	k1gFnPc4
či	či	k8xC
suroviny	surovina	k1gFnPc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
léků	lék	k1gInPc2
a	a	k8xC
prostředků	prostředek	k1gInPc2
proti	proti	k7c3
početí	početí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
se	se	k3xPyFc4
pěstují	pěstovat	k5eAaImIp3nP
jako	jako	k9
okrasné	okrasný	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
máčka	máčka	k1gFnSc1
(	(	kIx(
<g/>
Eryngium	Eryngium	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
jarmanka	jarmanka	k1gFnSc1
(	(	kIx(
<g/>
Astrantia	Astrantia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
skalničky	skalnička	k1gFnPc1
se	se	k3xPyFc4
občas	občas	k6eAd1
pěstují	pěstovat	k5eAaImIp3nP
některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
z	z	k7c2
rodu	rod	k1gInSc2
Azorella	Azorello	k1gNnSc2
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgFnSc2d1
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
polokoule	polokoule	k1gFnSc2
a	a	k8xC
tvořící	tvořící	k2eAgInPc4d1
pozoruhodné	pozoruhodný	k2eAgInPc4d1
velmi	velmi	k6eAd1
kompaktní	kompaktní	k2eAgInPc4d1
trsy	trs	k1gInPc4
z	z	k7c2
drobných	drobný	k2eAgFnPc2d1
nahuštěných	nahuštěný	k2eAgFnPc2d1
rostlinek	rostlinka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mrkev	mrkev	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Daucus	Daucus	k1gInSc1
carota	carot	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kerblík	kerblík	k1gInSc1
třebule	třebule	k1gFnSc2
(	(	kIx(
<g/>
Anthriscus	Anthriscus	k1gInSc1
cerefolium	cerefolium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Hlízy	hlíza	k1gFnPc1
krabilice	krabilice	k1gFnSc2
hlíznaté	hlíznatý	k2eAgInPc1d1
(	(	kIx(
<g/>
Chaerophyllum	Chaerophyllum	k1gInSc1
bulbosum	bulbosum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Plody	plod	k1gInPc4
fenyklu	fenykl	k1gInSc2
obecného	obecný	k2eAgNnSc2d1
(	(	kIx(
<g/>
Foeniculum	Foeniculum	k1gNnSc1
vulgare	vulgar	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Azorela	Azorela	k1gFnSc1
nahloučená	nahloučený	k2eAgFnSc1d1
<g/>
,	,	kIx,
Peru	prát	k5eAaImIp1nS
</s>
<s>
Máčka	máčka	k1gFnSc1
Eryngium	Eryngium	k1gNnSc1
alpinum	alpinum	k1gNnSc5
</s>
<s>
Přehled	přehled	k1gInSc1
rodů	rod	k1gInPc2
</s>
<s>
Aciphylla	Aciphylla	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Acronema	Acronema	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Actinanthus	Actinanthus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Actinolema	Actinolema	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Actinotus	Actinotus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Adenosciadium	Adenosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Aegokeras	Aegokeras	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Aegopodium	Aegopodium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Aethusa	Aethus	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
Afroligusticum	Afroligusticum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Afrosciadium	Afrosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Agasyllis	Agasyllis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Alepidea	Alepidea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Ammi	Ammi	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Ammodaucus	Ammodaucus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Ammoides	Ammoides	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Ammoselinum	Ammoselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Andriana	Andriana	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Anethum	Anethum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Angelica	Angelica	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Anginon	Anginon	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Angoseseli	Angoseset	k5eAaPmAgMnP,k5eAaImAgMnP,k5eAaBmAgMnP
<g/>
,	,	kIx,
</s>
<s>
Anisopoda	Anisopoda	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Anisosciadium	Anisosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Anisotome	Anisotom	k1gInSc5
<g/>
,	,	kIx,
</s>
<s>
Annesorhiza	Annesorhiza	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Anthriscus	Anthriscus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Aphanopleura	Aphanopleura	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Apiastrum	Apiastrum	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Apiopetalum	Apiopetalum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Apium	Apium	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Apodicarpum	Apodicarpum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Arafoe	Arafoat	k5eAaPmIp3nS
<g/>
,	,	kIx,
</s>
<s>
Arctopus	Arctopus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Arcuatopterus	Arcuatopterus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Arracacia	Arracacia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Artedia	Artedium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Asciadium	Asciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Asteriscium	Asteriscium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Astomaea	Astomaea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Astrantia	Astrantia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Astrodaucus	Astrodaucus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Astydamia	Astydamia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Athamanta	Athamanta	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Aulacospermum	Aulacospermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Austropeucedanum	Austropeucedanum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Autumnalia	Autumnalia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Azilia	Azilia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Azorella	Azorella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Berula	Berula	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Bifora	Bifora	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Billburttia	Billburttia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Bolax	Bolax	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Bonannia	Bonannium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Bowlesia	Bowlesia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Brachyscias	Brachyscias	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Bunium	Bunium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Bupleurum	Bupleurum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cachrys	Cachrys	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Calyptrosciadium	Calyptrosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Canaria	Canarium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Cannaboides	Cannaboides	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Capnophyllum	Capnophyllum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Carlesia	Carlesia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Caropodium	Caropodium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Caropsis	Caropsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Carum	Carum	k?
<g/>
,	,	kIx,
</s>
<s>
Caucalis	Caucalis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Cenolophium	Cenolophium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Centella	Centella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Cephalopodum	Cephalopodum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Chaerophyllopsis	Chaerophyllopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Chaerophyllum	Chaerophyllum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Chaetosciadium	Chaetosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Chamaesciadium	Chamaesciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Chamaesium	Chamaesium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Chamarea	Chamarea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Changium	Changium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Chlaenosciadium	Chlaenosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Choritaenia	Choritaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Chuanminshen	Chuanminshen	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Chymsydia	Chymsydium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Cicuta	Cicut	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
Cnidiocarpa	Cnidiocarpa	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Cnidium	Cnidium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Coaxana	Coaxana	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Conioselinum	Conioselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Conium	Conium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Conopodium	Conopodium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Coriandrum	Coriandrum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cortia	Cortia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Cortiella	Cortiella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Cotopaxia	Cotopaxia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Coulterophytum	Coulterophytum	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Crithmum	Crithmum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cryptotaenia	Cryptotaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Cuminum	Cuminum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cyathoselinum	Cyathoselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cyclorhiza	Cyclorhiza	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Cyclospermum	Cyclospermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cymbocarpum	Cymbocarpum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Cymopterus	Cymopterus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Cynorhiza	Cynorhiza	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Cynosciadium	Cynosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Dactylaea	Dactylaea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Dahliaphyllum	Dahliaphyllum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Dasispermum	Dasispermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Daucosma	Daucosma	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Daucus	Daucus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Demavendia	Demavendium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Dethawia	Dethawia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Deverra	Deverra	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Dichoropetalum	Dichoropetalum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Dichosciadium	Dichosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Dickinsia	Dickinsia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Dicyclophora	Dicyclophora	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Dimorphosciadium	Dimorphosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Diplaspis	Diplaspis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Diplolophium	Diplolophium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Diplotaenia	Diplotaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Diposis	Diposis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Domeykoa	Domeykoa	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Donnellsmithia	Donnellsmithia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Dracosciadium	Dracosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Drusa	Drusa	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ducrosia	Ducrosia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Dystaenia	Dystaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Echinophora	Echinophora	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ekimia	Ekimia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Elaeosticta	Elaeosticta	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Eleutherospermum	Eleutherospermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Elwendia	Elwendium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Enantiophylla	Enantiophylla	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Endressia	Endressia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Eremocharis	Eremocharis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Eremodaucus	Eremodaucus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Ergocarpon	Ergocarpon	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Erigenia	Erigenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Eryngium	Eryngium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Eurytaenia	Eurytaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Exoacantha	Exoacantha	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ezosciadium	Ezosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Falcaria	Falcarium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Fergania	Ferganium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Ferula	Ferula	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ferulago	Ferulago	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Ferulopsis	Ferulopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Foeniculum	Foeniculum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Frommia	Frommia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Froriepia	Froriepia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Fuernrohria	Fuernrohrium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Galagania	Galaganium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Geocaryum	Geocaryum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Gingidia	Gingidium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Glaucosciadium	Glaucosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Glehnia	Glehnium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Glia	Glia	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Gongylosciadium	Gongylosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Gongylotaxis	Gongylotaxis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Grafia	Grafia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Grammosciadium	Grammosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Gymnophyton	Gymnophyton	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Halosciastrum	Halosciastrum	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Haloselinum	Haloselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Hansenia	Hansenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Haplosciadium	Haplosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Harbouria	Harbourium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Harperella	Harperella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Harrysmithia	Harrysmithia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Haussknechtia	Haussknechtia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Hellenocarum	Hellenocarum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Helosciadium	Helosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Heptaptera	Heptapter	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
Heracleum	Heracleum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Hermas	Hermas	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Heteromorpha	Heteromorpha	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Hladnikia	Hladnikia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Hohenackeria	Hohenackerium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Homalocarpus	Homalocarpus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Homalosciadium	Homalosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Horstrissea	Horstrissea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Huanaca	Huanaca	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Hyalolaena	Hyalolaena	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Hymenidium	Hymenidium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Hymenolaena	Hymenolaena	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Itasina	Itasina	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Johrenia	Johrenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Kadenia	Kadenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Kafirnigania	Kafirniganium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Kailashia	Kailashia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Kalakia	Kalakia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Kamelinia	Kamelinium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Kandaharia	Kandaharium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Karatavia	Karatavia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Karnataka	Karnatak	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
Kedarnatha	Kedarnatha	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Kelussia	Kelussia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Kenopleurum	Kenopleurum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Keraymonia	Keraymonium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Kitagawia	Kitagawia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Klotzschia	Klotzschia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Komarovia	Komarovia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Korshinskia	Korshinskia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Kozlovia	Kozlovia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Krasnovia	Krasnovia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Krubera	Krubera	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Kundmannia	Kundmannium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Kuramosciadium	Kuramosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Ladyginia	Ladyginium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Lagoecia	Lagoecia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Lalldhwojia	Lalldhwojia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Laser	laser	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Laserpitium	Laserpitium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Lecokia	Lecokia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ledebouriella	Ledebouriella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Lefebvrea	Lefebvrea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Leiotulus	Leiotulus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Lereschia	Lereschia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Leutea	Leutea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Levisticum	Levisticum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Lichtensteinia	Lichtensteinium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Lignocarpa	Lignocarpa	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ligusticopsis	Ligusticopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ligusticum	Ligusticum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Lilaeopsis	Lilaeopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Limnosciadium	Limnosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Lipskya	Lipskya	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Lisaea	Lisaea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Lithosciadium	Lithosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Lomatium	Lomatium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Lomatocarpa	Lomatocarpa	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Lomatocarum	Lomatocarum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Mackinlaya	Mackinlaya	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Magadania	Magadanium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Magydaris	Magydaris	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Marlothiella	Marlothiella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Mastigosciadium	Mastigosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Mathiasella	Mathiasella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Mediasia	Mediasia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Meeboldia	Meeboldium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Melanosciadium	Melanosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Meum	Meum	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Micropleura	Micropleura	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Microsciadium	Microsciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Modesciadium	Modesciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Mogoltavia	Mogoltavia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Molopospermum	Molopospermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Musineon	Musineon	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Mutellina	Mutellina	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Myrrhidendron	Myrrhidendron	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Myrrhis	Myrrhis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Nanobubon	Nanobubon	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Naufraga	Naufraga	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Neoconopodium	Neoconopodium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Neogaya	Neogaya	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Neogoezia	Neogoezia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Neomuretia	Neomuretia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Neonelsonia	Neonelsonium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Neoparrya	Neoparrya	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Niphogeton	Niphogeton	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Nirarathamnos	Nirarathamnos	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Normantha	Normantha	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Nothosmyrnium	Nothosmyrnium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Notiosciadium	Notiosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Notobubon	Notobubon	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Oedibasis	Oedibasis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Oenanthe	Oenanthat	k5eAaPmIp3nS
<g/>
,	,	kIx,
</s>
<s>
Oligocladus	Oligocladus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Oliveria	Oliverium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Opoidia	Opoidium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Opopanax	Opopanax	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Opsicarpium	Opsicarpium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Oreocome	Oreocom	k1gInSc5
<g/>
,	,	kIx,
</s>
<s>
Oreocomopsis	Oreocomopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Oreonana	Oreonana	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Oreoschimperella	Oreoschimperella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Orlaya	Orlaya	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Ormopterum	Ormopterum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Ormosciadium	Ormosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Oschatzia	Oschatzia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Osmorhiza	Osmorhiza	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Ostericum	Ostericum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Ottoa	Ottoa	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Oxypolis	Oxypolis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pachypleurum	Pachypleurum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Palimbia	Palimbia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Paraligusticum	Paraligusticum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Parapimpinella	Parapimpinella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Paraselinum	Paraselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Parasilaus	Parasilaus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Pastinaca	Pastinaca	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Pastinacopsis	Pastinacopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Paulita	Paulita	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pedinopetalum	Pedinopetalum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Pentapeltis	Pentapeltis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Perideridia	Perideridium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Perissocoeleum	Perissocoeleum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Petagnaea	Petagnaea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Petroedmondia	Petroedmondium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Petroselinum	Petroselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Peucedanum	Peucedanum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Phellolophium	Phellolophium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Phlojodicarpus	Phlojodicarpus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Phlyctidocarpa	Phlyctidocarpa	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Physospermopsis	Physospermopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Physospermum	Physospermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Physotrichia	Physotrichia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pilopleura	Pilopleura	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pimpinella	Pimpinella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Pinda	Pinda	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Platysace	Platysace	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pleurospermopsis	Pleurospermopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pleurospermum	Pleurospermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Podistera	Podister	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
Polemannia	Polemannium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Polemanniopsis	Polemanniopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Polytaenia	Polytaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Portenschlagiella	Portenschlagiella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Postiella	Postiella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Pozoa	Pozoa	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Prangos	Prangos	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Prionosciadium	Prionosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Psammogeton	Psammogeton	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Pseudocannaboides	Pseudocannaboides	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Pseudocarum	Pseudocarum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Pseudoridolfia	Pseudoridolfia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pseudoselinum	Pseudoselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Pseudotrachydium	Pseudotrachydium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Pternopetalum	Pternopetalum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Pterygopleurum	Pterygopleurum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Ptilimnium	Ptilimnium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Ptychotis	Ptychotis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Pycnocycla	Pycnocycla	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Pyramidoptera	Pyramidopter	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
Registaniella	Registaniella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Rhabdosciadium	Rhabdosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Rhizomatophora	Rhizomatophora	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Rhodosciadium	Rhodosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Rhopalosciadium	Rhopalosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Rhysopterus	Rhysopterus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Ridolfia	Ridolfia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Rivasmartinezia	Rivasmartinezia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Rohmooa	Rohmooa	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Rupiphila	Rupiphila	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Rutheopsis	Rutheopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Sajanella	Sajanella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Sanicula	Sanicula	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Saposhnikovia	Saposhnikovia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Scaligeria	Scaligerium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Scandia	Scandium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Scandix	Scandix	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Scaraboides	Scaraboides	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Schoenolaena	Schoenolaena	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Schoenoselinum	Schoenoselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Schrenkia	Schrenkia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Schtschurowskia	Schtschurowskia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Schulzia	Schulzia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Sclerochorton	Sclerochorton	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Sclerosciadium	Sclerosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Sclerotiaria	Sclerotiarium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Scrithacola	Scrithacola	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Selinopsis	Selinopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Selinum	Selinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Semenovia	Semenovia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Seseli	Seset	k5eAaPmAgMnP,k5eAaImAgMnP,k5eAaBmAgMnP
<g/>
,	,	kIx,
</s>
<s>
Seselopsis	Seselopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Shoshonea	Shoshonea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Siculosciadium	Siculosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Silaum	Silaum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Siler	Siler	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Sillaphyton	Sillaphyton	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Silphiodaucus	Silphiodaucus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Sinocarum	Sinocarum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Sinodielsia	Sinodielsia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Sinolimprichtia	Sinolimprichtia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Sison	Sison	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Sium	Sium	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Sivadasania	Sivadasanium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Smyrniopsis	Smyrniopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Smyrnium	Smyrnium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Spananthe	Spananthat	k5eAaPmIp3nS
<g/>
,	,	kIx,
</s>
<s>
Spermolepis	Spermolepis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Sphaenolobium	Sphaenolobium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Sphaerosciadium	Sphaerosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Sphallerocarpus	Sphallerocarpus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Spiroceratium	Spiroceratium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Spuriopimpinella	Spuriopimpinella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Stefanoffia	Stefanoffia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Steganotaenia	Steganotaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Stenocoelium	Stenocoelium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Stenosemis	Stenosemis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Stenotaenia	Stenotaenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Stewartiella	Stewartiella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Stoibrax	Stoibrax	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Symphyoloma	Symphyoloma	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Synclinostyles	Synclinostyles	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Szovitsia	Szovitsia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Taenidia	Taenidium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Taeniopetalum	Taeniopetalum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Tamamschjanella	Tamamschjanella	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Tamamschjania	Tamamschjanium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Tana	tanout	k5eAaImSgMnS
<g/>
,	,	kIx,
</s>
<s>
Tauschia	Tauschia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Tetrataenium	Tetrataenium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Thamnosciadium	Thamnosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Thapsia	Thapsia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Thaspium	Thaspium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Thecocarpus	Thecocarpus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Tiedemannia	Tiedemannium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Tilingia	Tilingia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Todaroa	Todaroa	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Tongoloa	Tongoloa	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Tordyliopsis	Tordyliopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Tordylium	Tordylium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Torilis	Torilis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Trachydium	Trachydium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Trachyspermum	Trachyspermum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Trepocarpus	Trepocarpus	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Tricholaser	Tricholaser	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Trigonosciadium	Trigonosciadium	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Trinia	Trinium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Trocdaris	Trocdaris	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Trochiscanthes	Trochiscanthes	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Tschulaktavia	Tschulaktavia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Turgenia	Turgenium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Turgeniopsis	Turgeniopsis	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Vanasushava	Vanasushava	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Vesper	Vesper	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Vicatia	Vicatia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Villarrealia	Villarrealia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Vinogradovia	Vinogradovia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Visnaga	Visnaga	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Vvedenskya	Vvedenskya	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Xanthogalum	Xanthogalum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Xanthosia	Xanthosia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Xatardia	Xatardium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Xyloselinum	Xyloselinum	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Yabea	Yabea	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Zeravschania	Zeravschanium	k1gNnPc1
<g/>
,	,	kIx,
</s>
<s>
Zizia	Zizia	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Zosima	Zosima	k1gFnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
STEVENS	STEVENS	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
F.	F.	kA
Angiosperm	Angiosperm	k1gInSc1
Phylogeny	Phylogen	k1gInPc1
Website	Websit	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Missouri	Missouri	k1gFnSc2
<g/>
,	,	kIx,
St	St	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Missouri	Missouri	k1gFnSc1
Botanical	Botanical	k1gFnSc2
Garden	Gardna	k1gFnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
20.11	20.11	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květena	květena	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
590	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Flora	Flora	k1gFnSc1
of	of	k?
China	China	k1gFnSc1
<g/>
:	:	kIx,
Apiaceae	Apiaceae	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
SMITH	SMITH	kA
<g/>
,	,	kIx,
Nantan	Nantan	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flowering	Flowering	k1gInSc1
Plants	Plants	k1gInSc4
of	of	k?
the	the	k?
Neotropics	Neotropics	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
691116946	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Apiaceae	Apiaceae	k1gInSc1
Lindl	Lindl	k1gFnSc1
<g/>
.	.	kIx.
|	|	kIx~
Plants	Plants	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
Online	Onlin	k1gInSc5
|	|	kIx~
Kew	Kew	k1gFnSc1
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plants	Plants	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
Online	Onlin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KUBÁT	Kubát	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíč	klíč	k1gInSc1
ke	k	k7c3
květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STEVENS	STEVENS	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
F.	F.	kA
Angiosperm	Angiosperm	k1gInSc1
Phylogeny	Phylogen	k1gInPc1
Website	Websit	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missouri	Missouri	k1gNnSc1
Botanical	Botanical	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
:	:	kIx,
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
ZZ	ZZ	kA
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
SKALICKÁ	Skalická	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
;	;	kIx,
VĚTVIČKA	větvička	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
;	;	kIx,
ZELENÝ	Zelený	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botanický	botanický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
rodových	rodový	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
cévnatých	cévnatý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7442	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
31	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Plants	Plants	k1gInSc1
of	of	k?
the	the	k?
world	world	k6eAd1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Botanic	Botanice	k1gFnPc2
Gardens	Gardens	k1gInSc1
<g/>
,	,	kIx,
Kew	Kew	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
miříkovité	miříkovitý	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Apiaceae	Apiaceae	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Delta-intkey	Delta-intke	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
Apiaceae	Apiacea	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4150388-0	4150388-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
88023174	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
88023174	#num#	k4
</s>
