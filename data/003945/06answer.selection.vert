<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
je	být	k5eAaImIp3nS	být
federací	federace	k1gFnSc7	federace
26	[number]	k4	26
autonomních	autonomní	k2eAgInPc2d1	autonomní
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
rovnoprávné	rovnoprávný	k2eAgMnPc4d1	rovnoprávný
navzájem	navzájem	k6eAd1	navzájem
i	i	k8xC	i
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
šesti	šest	k4xCc2	šest
kantonů	kanton	k1gInPc2	kanton
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
úředně	úředně	k6eAd1	úředně
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
hovorově	hovorově	k6eAd1	hovorově
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
polokantony	polokanton	k1gInPc4	polokanton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
zástupce	zástupce	k1gMnSc4	zástupce
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
států	stát	k1gInPc2	stát
a	a	k8xC	a
poloviční	poloviční	k2eAgFnSc4d1	poloviční
váhu	váha	k1gFnSc4	váha
při	při	k7c6	při
hlasováních	hlasování	k1gNnPc6	hlasování
podle	podle	k7c2	podle
kantonů	kanton	k1gInPc2	kanton
<g/>
.	.	kIx.	.
</s>
