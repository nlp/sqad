<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
konfederace	konfederace	k1gFnSc1	konfederace
či	či	k8xC	či
Švýcarské	švýcarský	k2eAgNnSc1d1	švýcarské
spříseženství	spříseženství	k1gNnSc1	spříseženství
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
Švýcary	Švýcary	k1gInPc1	Švýcary
<g/>
)	)	kIx)	)
–	–	k?	–
německy	německy	k6eAd1	německy
Schweizerische	Schweizerische	k1gFnSc4	Schweizerische
Eidgenossenschaft	Eidgenossenschafta	k1gFnPc2	Eidgenossenschafta
<g/>
,	,	kIx,	,
Schweiz	Schweiza	k1gFnPc2	Schweiza
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Confédération	Confédération	k1gInSc1	Confédération
suisse	suisse	k1gFnSc2	suisse
<g/>
,	,	kIx,	,
La	la	k1gNnSc2	la
Suisse	Suisse	k1gFnSc2	Suisse
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Confederazione	Confederazion	k1gInSc5	Confederazion
Svizzera	Svizzer	k1gMnSc4	Svizzer
<g/>
,	,	kIx,	,
Svizzera	Svizzer	k1gMnSc4	Svizzer
<g/>
,	,	kIx,	,
rétorománsky	rétorománsky	k6eAd1	rétorománsky
Confederaziun	Confederaziun	k1gMnSc1	Confederaziun
svizra	svizra	k1gMnSc1	svizra
<g/>
,	,	kIx,	,
Svizra	Svizra	k1gMnSc1	Svizra
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Confoederatio	Confoederatio	k1gMnSc1	Confoederatio
Helvetica	Helvetica	k1gMnSc1	Helvetica
<g/>
,	,	kIx,	,
Helvetia	Helvetia	k1gFnSc1	Helvetia
–	–	k?	–
je	být	k5eAaImIp3nS	být
vnitrozemní	vnitrozemní	k2eAgInSc1d1	vnitrozemní
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c7	mezi
Bodamským	bodamský	k2eAgNnSc7d1	Bodamské
jezerem	jezero	k1gNnSc7	jezero
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
Ženevským	ženevský	k2eAgNnSc7d1	Ženevské
jezerem	jezero	k1gNnSc7	jezero
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Horním	horní	k2eAgMnSc7d1	horní
a	a	k8xC	a
Alpským	alpský	k2eAgInSc7d1	alpský
Rýnem	Rýn	k1gInSc7	Rýn
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Rýnem	Rýn	k1gInSc7	Rýn
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Jurou	Jura	k1gMnSc7	Jura
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jižními	jižní	k2eAgFnPc7d1	jižní
Alpami	Alpy	k1gFnPc7	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgMnPc7	který
má	mít	k5eAaImIp3nS	mít
suchozemské	suchozemský	k2eAgFnSc2d1	suchozemská
hranice	hranice	k1gFnSc2	hranice
<g/>
:	:	kIx,	:
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
je	být	k5eAaImIp3nS	být
734,2	[number]	k4	734,2
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
571,8	[number]	k4	571,8
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
345,7	[number]	k4	345,7
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
165,1	[number]	k4	165,1
km	km	kA	km
(	(	kIx(	(
<g/>
přerušena	přerušit	k5eAaPmNgFnS	přerušit
Lichtenštejnskem	Lichtenštejnsko	k1gNnSc7	Lichtenštejnsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Lichtenštejnskem	Lichtenštejnsko	k1gNnSc7	Lichtenštejnsko
41,1	[number]	k4	41,1
km	km	kA	km
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
1857,9	[number]	k4	1857,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
federace	federace	k1gFnSc1	federace
má	mít	k5eAaImIp3nS	mít
23	[number]	k4	23
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
3	[number]	k4	3
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
půlkantony	půlkanton	k1gInPc4	půlkanton
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
8,039	[number]	k4	8,039
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
EFTA	EFTA	kA	EFTA
<g/>
,	,	kIx,	,
OECD	OECD	kA	OECD
a	a	k8xC	a
WTO	WTO	kA	WTO
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
EU	EU	kA	EU
občané	občan	k1gMnPc1	občan
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
a	a	k8xC	a
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
mimo	mimo	k7c4	mimo
EU	EU	kA	EU
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
elity	elita	k1gFnPc1	elita
do	do	k7c2	do
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spolu	spolu	k6eAd1	spolu
soupeřících	soupeřící	k2eAgInPc2d1	soupeřící
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kantonů	kanton	k1gInPc2	kanton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgInPc1	některý
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
členy	člen	k1gInPc1	člen
starého	starý	k2eAgInSc2d1	starý
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
posledním	poslední	k2eAgInSc6d1	poslední
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
,	,	kIx,	,
27	[number]	k4	27
denní	denní	k2eAgFnSc6d1	denní
válce	válka	k1gFnSc6	válka
Sonderbundkrieg	Sonderbundkrieg	k1gMnSc1	Sonderbundkrieg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
odhodlaly	odhodlat	k5eAaPmAgFnP	odhodlat
ukončit	ukončit	k5eAaPmF	ukončit
tuto	tento	k3xDgFnSc4	tento
staletou	staletý	k2eAgFnSc4d1	staletá
tradici	tradice	k1gFnSc4	tradice
válčení	válčení	k1gNnSc2	válčení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
několika	několik	k4yIc2	několik
předcházejících	předcházející	k2eAgInPc2d1	předcházející
menších	malý	k2eAgInPc2d2	menší
svazků	svazek	k1gInPc2	svazek
se	se	k3xPyFc4	se
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
"	"	kIx"	"
<g/>
národa	národ	k1gInSc2	národ
vzešlého	vzešlý	k2eAgInSc2d1	vzešlý
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
<g/>
"	"	kIx"	"
–	–	k?	–
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
Willensnation	Willensnation	k1gInSc1	Willensnation
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
změnou	změna	k1gFnSc7	změna
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
z	z	k7c2	z
konfederace	konfederace	k1gFnSc2	konfederace
na	na	k7c4	na
federaci	federace	k1gFnSc4	federace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
spolková	spolkový	k2eAgFnSc1d1	spolková
ústava	ústava	k1gFnSc1	ústava
(	(	kIx(	(
<g/>
revidována	revidován	k2eAgFnSc1d1	revidována
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rozšíření	rozšíření	k1gNnSc6	rozšíření
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
vedly	vést	k5eAaImAgFnP	vést
vedle	vedle	k7c2	vedle
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
i	i	k8xC	i
důvody	důvod	k1gInPc1	důvod
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
obranné	obranný	k2eAgFnSc2d1	obranná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
území	území	k1gNnSc6	území
konfederace	konfederace	k1gFnSc2	konfederace
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
válčily	válčit	k5eAaImAgInP	válčit
nejen	nejen	k6eAd1	nejen
kantony	kanton	k1gInPc1	kanton
a	a	k8xC	a
města	město	k1gNnSc2	město
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
mocnosti	mocnost	k1gFnPc1	mocnost
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
správní	správní	k2eAgInPc1d1	správní
celky	celek	k1gInPc1	celek
totiž	totiž	k9	totiž
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nemohly	moct	k5eNaImAgFnP	moct
jednotlivě	jednotlivě	k6eAd1	jednotlivě
obstát	obstát	k5eAaPmF	obstát
jak	jak	k8xS	jak
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
vojensky	vojensky	k6eAd1	vojensky
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
spojenectví	spojenectví	k1gNnSc4	spojenectví
zvolily	zvolit	k5eAaPmAgInP	zvolit
i	i	k8xC	i
směr	směr	k1gInSc1	směr
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ustanovovala	ustanovovat	k5eAaImAgFnS	ustanovovat
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
jako	jako	k8xS	jako
přísně	přísně	k6eAd1	přísně
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
neúčastnící	účastnící	k2eNgInPc1d1	účastnící
se	se	k3xPyFc4	se
ve	v	k7c6	v
vojenských	vojenský	k2eAgInPc6d1	vojenský
blocích	blok	k1gInPc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
konfederace	konfederace	k1gFnSc1	konfederace
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
silnou	silný	k2eAgFnSc4d1	silná
spolkovou	spolkový	k2eAgFnSc4d1	spolková
armádu	armáda	k1gFnSc4	armáda
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
případnou	případný	k2eAgFnSc4d1	případná
vlastní	vlastní	k2eAgFnSc4d1	vlastní
obranu	obrana	k1gFnSc4	obrana
–	–	k?	–
jak	jak	k8xS	jak
komentuje	komentovat	k5eAaBmIp3nS	komentovat
anglické	anglický	k2eAgNnSc1d1	anglické
"	"	kIx"	"
<g/>
neutral	utrat	k5eNaPmAgMnS	utrat
but	but	k?	but
heavily	heavit	k5eAaPmAgFnP	heavit
armed	armed	k1gInSc1	armed
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
41	[number]	k4	41
000	[number]	k4	000
km2	km2	k4	km2
malou	malý	k2eAgFnSc7d1	malá
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
milionu	milion	k4xCgInSc2	milion
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
188	[number]	k4	188
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
srovnatelné	srovnatelný	k2eAgNnSc4d1	srovnatelné
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
státem	stát	k1gInSc7	stát
Maryland	Marylanda	k1gFnPc2	Marylanda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Jura	Jura	k1gMnSc1	Jura
<g/>
,	,	kIx,	,
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
plošina	plošina	k1gFnSc1	plošina
a	a	k8xC	a
Švýcarské	švýcarský	k2eAgFnPc1d1	švýcarská
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Jura	jura	k1gFnSc1	jura
je	být	k5eAaImIp3nS	být
úzké	úzké	k1gInPc4	úzké
<g/>
,	,	kIx,	,
250	[number]	k4	250
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
vápencové	vápencový	k2eAgNnSc4d1	vápencové
pohoří	pohoří	k1gNnSc4	pohoří
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
až	až	k9	až
1	[number]	k4	1
700	[number]	k4	700
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Jura	jura	k1gFnSc1	jura
se	se	k3xPyFc4	se
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
obloukem	oblouk	k1gInSc7	oblouk
podél	podél	k6eAd1	podél
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
%	%	kIx~	%
území	území	k1gNnSc6	území
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarské	švýcarský	k2eAgFnPc1d1	švýcarská
Alpy	Alpy	k1gFnPc1	Alpy
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
57	[number]	k4	57
%	%	kIx~	%
území	území	k1gNnSc4	území
země	zem	k1gFnSc2	zem
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
Západním	západní	k2eAgFnPc3d1	západní
Alpám	Alpy	k1gFnPc3	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Táhnou	táhnout	k5eAaImIp3nP	táhnout
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
se	se	k3xPyFc4	se
na	na	k7c4	na
severní	severní	k2eAgNnSc4d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgNnSc4d1	jižní
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
Alp	Alpy	k1gFnPc2	Alpy
tvoří	tvořit	k5eAaImIp3nP	tvořit
Bernské	bernský	k2eAgFnPc1d1	Bernská
Alpy	Alpy	k1gFnPc1	Alpy
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Glarnské	Glarnský	k2eAgFnPc1d1	Glarnský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
Walliské	Walliský	k2eAgFnPc1d1	Walliský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Tessinské	Tessinský	k2eAgFnPc1d1	Tessinský
Alpy	Alpy	k1gFnPc1	Alpy
a	a	k8xC	a
Bündnerské	Bündnerský	k2eAgFnPc1d1	Bündnerský
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
Švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
Alp	Alpy	k1gFnPc2	Alpy
je	být	k5eAaImIp3nS	být
masiv	masiv	k1gInSc1	masiv
sv.	sv.	kA	sv.
Gotthard	Gotthard	k1gInSc1	Gotthard
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
štítem	štít	k1gInSc7	štít
Švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
Alp	Alpy	k1gFnPc2	Alpy
je	být	k5eAaImIp3nS	být
Dufourspitze	Dufourspitze	k1gFnSc1	Dufourspitze
(	(	kIx(	(
<g/>
Dufourův	Dufourův	k2eAgInSc1d1	Dufourův
štít	štít	k1gInSc1	štít
se	s	k7c7	s
4	[number]	k4	4
634	[number]	k4	634
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
evropských	evropský	k2eAgFnPc2d1	Evropská
řek	řeka	k1gFnPc2	řeka
<g/>
:	:	kIx,	:
Aare	Aare	k1gInSc1	Aare
<g/>
,	,	kIx,	,
Rýn	Rýn	k1gInSc1	Rýn
<g/>
,	,	kIx,	,
Rhôna	Rhôna	k1gFnSc1	Rhôna
či	či	k8xC	či
Inn	Inn	k1gFnSc1	Inn
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
plošina	plošina	k1gFnSc1	plošina
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
asi	asi	k9	asi
37	[number]	k4	37
%	%	kIx~	%
území	území	k1gNnSc6	území
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Jurou	jura	k1gFnSc7	jura
a	a	k8xC	a
Alpami	Alpy	k1gFnPc7	Alpy
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Ženevským	ženevský	k2eAgNnSc7d1	Ženevské
a	a	k8xC	a
Bodamským	bodamský	k2eAgNnSc7d1	Bodamské
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
centru	centrum	k1gNnSc6	centrum
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Bern	Bern	k1gInSc1	Bern
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
doliny	dolina	k1gFnPc4	dolina
a	a	k8xC	a
kopce	kopec	k1gInPc4	kopec
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
působením	působení	k1gNnSc7	působení
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
je	být	k5eAaImIp3nS	být
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Politickou	politický	k2eAgFnSc4d1	politická
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
–	–	k?	–
ale	ale	k8xC	ale
i	i	k9	i
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
–	–	k?	–
moc	moc	k6eAd1	moc
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
konfederace	konfederace	k1gFnSc2	konfederace
představují	představovat	k5eAaImIp3nP	představovat
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
jsou	být	k5eAaImIp3nP	být
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc1	jejich
parlamenty	parlament	k1gInPc1	parlament
(	(	kIx(	(
<g/>
spolkový	spolkový	k2eAgMnSc1d1	spolkový
<g/>
,	,	kIx,	,
kantonální	kantonální	k2eAgFnPc1d1	kantonální
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnPc1d1	městská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
radou	rada	k1gFnSc7	rada
(	(	kIx(	(
<g/>
kantonální	kantonální	k2eAgFnSc1d1	kantonální
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
<g/>
,	,	kIx,	,
obecní	obecní	k2eAgFnSc1d1	obecní
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
občan	občan	k1gMnSc1	občan
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
suverénem	suverén	k1gMnSc7	suverén
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Suverénem	suverén	k1gMnSc7	suverén
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
obcí	obec	k1gFnPc2	obec
<g/>
/	/	kIx~	/
<g/>
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
spolkové	spolkový	k2eAgFnPc4d1	spolková
je	být	k5eAaImIp3nS	být
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
podléhají	podléhat	k5eAaImIp3nP	podléhat
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
všechna	všechen	k3xTgNnPc4	všechen
rozhodování	rozhodování	k1gNnSc4	rozhodování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
ústavě	ústava	k1gFnSc6	ústava
a	a	k8xC	a
ústavách	ústava	k1gFnPc6	ústava
kantonů	kanton	k1gInPc2	kanton
<g/>
:	:	kIx,	:
o	o	k7c6	o
zákonech	zákon	k1gInPc6	zákon
a	a	k8xC	a
ústavách	ústava	k1gFnPc6	ústava
<g/>
,	,	kIx,	,
o	o	k7c6	o
věcných	věcný	k2eAgFnPc6d1	věcná
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
práva	právo	k1gNnPc1	právo
–	–	k?	–
podle	podle	k7c2	podle
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
zájmů	zájem	k1gInPc2	zájem
–	–	k?	–
propůjčit	propůjčit	k5eAaPmF	propůjčit
reprezentantům	reprezentant	k1gMnPc3	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
ale	ale	k9	ale
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
propůjčení	propůjčení	k1gNnSc4	propůjčení
dočasné	dočasný	k2eAgNnSc4d1	dočasné
a	a	k8xC	a
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
spokojenosti	spokojenost	k1gFnSc6	spokojenost
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
si	se	k3xPyFc3	se
tu	ten	k3xDgFnSc4	ten
či	či	k8xC	či
onu	onen	k3xDgFnSc4	onen
pravomoc	pravomoc	k1gFnSc4	pravomoc
nevrátí	vrátit	k5eNaPmIp3nS	vrátit
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
výlučných	výlučný	k2eAgFnPc2d1	výlučná
kompetencí	kompetence	k1gFnPc2	kompetence
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
politickým	politický	k2eAgNnPc3d1	politické
právům	právo	k1gNnPc3	právo
občanů	občan	k1gMnPc2	občan
–	–	k?	–
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
obcí	obec	k1gFnPc2	obec
<g/>
/	/	kIx~	/
<g/>
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
úrovni	úroveň	k1gFnSc3	úroveň
spolkové	spolkový	k2eAgFnSc2d1	spolková
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
referendum	referendum	k1gNnSc1	referendum
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
<g/>
)	)	kIx)	)
jehož	jenž	k3xRgNnSc2	jenž
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
reprezentanty	reprezentant	k1gMnPc4	reprezentant
závazná	závazný	k2eAgFnSc1d1	závazná
<g/>
:	:	kIx,	:
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
,	,	kIx,	,
při	při	k7c6	při
změnách	změna	k1gFnPc6	změna
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
fakultativní	fakultativní	k2eAgFnSc6d1	fakultativní
<g/>
,	,	kIx,	,
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s>
změnách	změna	k1gFnPc6	změna
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
referendum	referendum	k1gNnSc4	referendum
během	během	k7c2	během
100	[number]	k4	100
dní	den	k1gInPc2	den
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
nejméně	málo	k6eAd3	málo
50	[number]	k4	50
000	[number]	k4	000
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
8	[number]	k4	8
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
se	se	k3xPyFc4	se
celostátní	celostátní	k2eAgNnSc1d1	celostátní
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
zákoně	zákon	k1gInSc6	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iniciativa	iniciativa	k1gFnSc1	iniciativa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
reprezentanty	reprezentant	k1gMnPc4	reprezentant
závazná	závazný	k2eAgFnSc1d1	závazná
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
tedy	tedy	k9	tedy
pokud	pokud	k8xS	pokud
během	během	k7c2	během
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
iniciátoři	iniciátor	k1gMnPc1	iniciátor
"	"	kIx"	"
<g/>
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
"	"	kIx"	"
získají	získat	k5eAaPmIp3nP	získat
podpisy	podpis	k1gInPc1	podpis
100	[number]	k4	100
000	[number]	k4	000
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
se	se	k3xPyFc4	se
celostátní	celostátní	k2eAgNnSc1d1	celostátní
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
navrhovaném	navrhovaný	k2eAgInSc6d1	navrhovaný
zákoně	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
petice	petice	k1gFnSc1	petice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
z	z	k7c2	z
ještě	ještě	k9	ještě
nevyvinuté	vyvinutý	k2eNgFnSc2d1	nevyvinutá
demokracie	demokracie	k1gFnSc2	demokracie
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
<g/>
:	:	kIx,	:
aktivní	aktivní	k2eAgInSc4d1	aktivní
–	–	k?	–
volit	volit	k5eAaImF	volit
<g/>
,	,	kIx,	,
pasivní	pasivní	k2eAgInSc4d1	pasivní
–	–	k?	–
být	být	k5eAaImF	být
kandidován	kandidován	k2eAgInSc4d1	kandidován
a	a	k8xC	a
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
hlasování	hlasování	k1gNnSc6	hlasování
–	–	k?	–
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
se	se	k3xPyFc4	se
k	k	k7c3	k
předkládanému	předkládaný	k2eAgInSc3d1	předkládaný
návrhu	návrh	k1gInSc3	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
starší	starý	k2eAgInSc1d2	starší
většinový	většinový	k2eAgInSc1d1	většinový
systém	systém	k1gInSc1	systém
v	v	k7c6	v
reprezentativní	reprezentativní	k2eAgFnSc6d1	reprezentativní
části	část	k1gFnSc6	část
politiky	politika	k1gFnSc2	politika
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nahradil	nahradit	k5eAaPmAgInS	nahradit
systém	systém	k1gInSc1	systém
poměrný	poměrný	k2eAgInSc1d1	poměrný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
–	–	k?	–
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
demokraciích	demokracie	k1gFnPc6	demokracie
běžná	běžný	k2eAgFnSc1d1	běžná
–	–	k?	–
forma	forma	k1gFnSc1	forma
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
účast	účast	k1gFnSc4	účast
i	i	k9	i
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
mnohde	mnohde	k6eAd1	mnohde
jinde	jinde	k6eAd1	jinde
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
z	z	k7c2	z
reprezentativní	reprezentativní	k2eAgFnSc2d1	reprezentativní
politiky	politika	k1gFnSc2	politika
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
jeho	jeho	k3xOp3gNnSc4	jeho
fungování	fungování	k1gNnSc4	fungování
je	být	k5eAaImIp3nS	být
kolegialita	kolegialita	k1gFnSc1	kolegialita
a	a	k8xC	a
konkordance	konkordance	k1gFnSc1	konkordance
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nutnost	nutnost	k1gFnSc4	nutnost
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
samozřejmost	samozřejmost	k1gFnSc1	samozřejmost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
společenská	společenský	k2eAgFnSc1d1	společenská
objednávka	objednávka	k1gFnSc1	objednávka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
najít	najít	k5eAaPmF	najít
věcná	věcný	k2eAgNnPc4d1	věcné
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přes	přes	k7c4	přes
často	často	k6eAd1	často
rozdílná	rozdílný	k2eAgNnPc4d1	rozdílné
stanoviska	stanovisko	k1gNnPc4	stanovisko
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
spolkové	spolkový	k2eAgFnSc6d1	spolková
úrovni	úroveň	k1gFnSc6	úroveň
bylo	být	k5eAaImAgNnS	být
volební	volební	k2eAgNnSc1d1	volební
a	a	k8xC	a
hlasovací	hlasovací	k2eAgNnSc1d1	hlasovací
právo	právo	k1gNnSc1	právo
pro	pro	k7c4	pro
švýcarské	švýcarský	k2eAgFnPc4d1	švýcarská
ženy	žena	k1gFnPc4	žena
ustaveno	ustaven	k2eAgNnSc1d1	ustaveno
až	až	k9	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
volili	volit	k5eAaImAgMnP	volit
a	a	k8xC	a
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
starší	starý	k2eAgMnPc1d2	starší
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ale	ale	k8xC	ale
jenom	jenom	k6eAd1	jenom
pokud	pokud	k8xS	pokud
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kantonální	kantonální	k2eAgFnSc6d1	kantonální
úrovni	úroveň	k1gFnSc6	úroveň
to	ten	k3xDgNnSc1	ten
trvalo	trvat	k5eAaImAgNnS	trvat
zrovnoprávnění	zrovnoprávnění	k1gNnSc4	zrovnoprávnění
žen	žena	k1gFnPc2	žena
ještě	ještě	k6eAd1	ještě
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
hlasovací	hlasovací	k2eAgNnSc1d1	hlasovací
právo	právo	k1gNnSc1	právo
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
i	i	k9	i
v	v	k7c6	v
Appenzellu	Appenzell	k1gInSc6	Appenzell
Innerrhodenu	Innerrhoden	k1gInSc2	Innerrhoden
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
voličů	volič	k1gMnPc2	volič
(	(	kIx(	(
<g/>
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Spolkový	spolkový	k2eAgInSc1d1	spolkový
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gMnSc4	on
Rada	rada	k1gFnSc1	rada
států	stát	k1gInPc2	stát
a	a	k8xC	a
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
procentuální	procentuální	k2eAgFnSc1d1	procentuální
hranice	hranice	k1gFnSc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
komory	komora	k1gFnPc1	komora
volí	volit	k5eAaImIp3nP	volit
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
spolkový	spolkový	k2eAgInSc4d1	spolkový
výkonný	výkonný	k2eAgInSc4d1	výkonný
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
sedmičlennou	sedmičlenný	k2eAgFnSc4d1	sedmičlenná
spolkovou	spolkový	k2eAgFnSc4d1	spolková
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1	spolková
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
principu	princip	k1gInSc2	princip
Konkordance	konkordance	k1gFnSc1	konkordance
tj.	tj.	kA	tj.
začlenění	začlenění	k1gNnSc2	začlenění
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
největšího	veliký	k2eAgInSc2d3	veliký
podílu	podíl	k1gInSc2	podíl
stran	strana	k1gFnPc2	strana
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
mají	mít	k5eAaImIp3nP	mít
strany	strana	k1gFnPc1	strana
podílející	podílející	k2eAgFnPc1d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
(	(	kIx(	(
<g/>
zastoupené	zastoupený	k2eAgNnSc1d1	zastoupené
ve	v	k7c6	v
Spolkové	spolkový	k2eAgFnSc6d1	spolková
radě	rada	k1gFnSc6	rada
<g/>
)	)	kIx)	)
217	[number]	k4	217
z	z	k7c2	z
246	[number]	k4	246
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
parlamentě	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
parlamentu	parlament	k1gInSc2	parlament
i	i	k8xC	i
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Ständerat	Ständerat	k1gInSc1	Ständerat
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
46	[number]	k4	46
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
2	[number]	k4	2
zástupci	zástupce	k1gMnPc1	zástupce
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
kanton	kanton	k1gInSc4	kanton
<g/>
,	,	kIx,	,
1	[number]	k4	1
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
polokanton	polokanton	k1gInSc4	polokanton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kompetenci	kompetence	k1gFnSc6	kompetence
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Nationalrat	Nationalrat	k1gInSc1	Nationalrat
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
200	[number]	k4	200
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
proporčním	proporční	k2eAgInSc7d1	proporční
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kantonu	kanton	k1gInSc6	kanton
zvlášť	zvlášť	k6eAd1	zvlášť
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schází	scházet	k5eAaImIp3nS	scházet
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
3	[number]	k4	3
týdnů	týden	k1gInPc2	týden
4	[number]	k4	4
<g/>
x	x	k?	x
do	do	k7c2	do
roka	rok	k1gInSc2	rok
Prezident	prezident	k1gMnSc1	prezident
konfederace	konfederace	k1gFnSc2	konfederace
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
Sjednoceným	sjednocený	k2eAgInSc7d1	sjednocený
spolkovým	spolkový	k2eAgInSc7d1	spolkový
parlamentem	parlament	k1gInSc7	parlament
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
komory	komora	k1gFnPc1	komora
se	se	k3xPyFc4	se
při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
příležitostech	příležitost	k1gFnPc6	příležitost
sloučí	sloučit	k5eAaPmIp3nP	sloučit
<g/>
)	)	kIx)	)
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
prezident	prezident	k1gMnSc1	prezident
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
Švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
politickém	politický	k2eAgInSc6d1	politický
systému	systém	k1gInSc6	systém
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
definuje	definovat	k5eAaBmIp3nS	definovat
pozici	pozice	k1gFnSc4	pozice
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
systému	systém	k1gInSc6	systém
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
prvního	první	k4xOgNnSc2	první
mezi	mezi	k7c7	mezi
rovnými	rovný	k2eAgFnPc7d1	rovná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
už	už	k6eAd1	už
vůbec	vůbec	k9	vůbec
nemůže	moct	k5eNaImIp3nS	moct
určovat	určovat	k5eAaImF	určovat
směr	směr	k1gInSc4	směr
vládní	vládní	k2eAgFnSc2d1	vládní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
úřad	úřad	k1gInSc1	úřad
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
snadnější	snadný	k2eAgFnSc3d2	snazší
reprezentaci	reprezentace	k1gFnSc3	reprezentace
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
orgánů	orgán	k1gInPc2	orgán
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
je	být	k5eAaImIp3nS	být
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
názvu	název	k1gInSc6	název
státu	stát	k1gInSc2	stát
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
historické	historický	k2eAgNnSc4d1	historické
označení	označení	k1gNnSc4	označení
-	-	kIx~	-
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
výraz	výraz	k1gInSc1	výraz
Eidgenossenschaft	Eidgenossenschaft	k2eAgInSc1d1	Eidgenossenschaft
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
spříseženství	spříseženství	k1gNnSc1	spříseženství
<g/>
.	.	kIx.	.
</s>
<s>
Federací	federace	k1gFnSc7	federace
tvoří	tvořit	k5eAaImIp3nS	tvořit
26	[number]	k4	26
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
kantonů	kanton	k1gInPc2	kanton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgFnPc4d1	ústavní
listiny	listina	k1gFnPc4	listina
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
kanton	kanton	k1gInSc4	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
je	být	k5eAaImIp3nS	být
federací	federace	k1gFnSc7	federace
26	[number]	k4	26
autonomních	autonomní	k2eAgInPc2d1	autonomní
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
rovnoprávné	rovnoprávný	k2eAgMnPc4d1	rovnoprávný
navzájem	navzájem	k6eAd1	navzájem
i	i	k8xC	i
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
šesti	šest	k4xCc2	šest
kantonů	kanton	k1gInPc2	kanton
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
úředně	úředně	k6eAd1	úředně
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
hovorově	hovorově	k6eAd1	hovorově
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
polokantony	polokanton	k1gInPc4	polokanton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
zástupce	zástupce	k1gMnSc4	zástupce
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
států	stát	k1gInPc2	stát
a	a	k8xC	a
poloviční	poloviční	k2eAgFnSc4d1	poloviční
váhu	váha	k1gFnSc4	váha
při	při	k7c6	při
hlasováních	hlasování	k1gNnPc6	hlasování
podle	podle	k7c2	podle
kantonů	kanton	k1gInPc2	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
kantonů	kanton	k1gInPc2	kanton
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc1d1	vlastní
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
rozpočet	rozpočet	k1gInSc4	rozpočet
i	i	k8xC	i
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
spolkových	spolkový	k2eAgFnPc6d1	spolková
zemích	zem	k1gFnPc6	zem
Německa	Německo	k1gNnSc2	Německo
nebo	nebo	k8xC	nebo
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
hospodářsky	hospodářsky	k6eAd1	hospodářsky
vyspělým	vyspělý	k2eAgInSc7d1	vyspělý
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
vynikalo	vynikat	k5eAaImAgNnS	vynikat
však	však	k9	však
v	v	k7c6	v
řemeslech	řemeslo	k1gNnPc6	řemeslo
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
zpočátku	zpočátku	k6eAd1	zpočátku
hodinářství	hodinářství	k1gNnSc1	hodinářství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
zimní	zimní	k2eAgNnSc1d1	zimní
zaměstnání	zaměstnání	k1gNnSc1	zaměstnání
zemědělců	zemědělec	k1gMnPc2	zemědělec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
a	a	k8xC	a
jemné	jemný	k2eAgInPc1d1	jemný
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nevyžadovaly	vyžadovat	k5eNaImAgInP	vyžadovat
velkou	velký	k2eAgFnSc4d1	velká
dopravu	doprava	k1gFnSc4	doprava
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
suroviny	surovina	k1gFnSc2	surovina
<g/>
,	,	kIx,	,
daly	dát	k5eAaPmAgFnP	dát
základ	základ	k1gInSc4	základ
přesnému	přesný	k2eAgNnSc3d1	přesné
strojírenství	strojírenství	k1gNnSc3	strojírenství
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
těžkému	těžký	k2eAgNnSc3d1	těžké
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nP	rozvíjet
obory	obor	k1gInPc1	obor
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zemědělství	zemědělství	k1gNnSc2	zemědělství
se	se	k3xPyFc4	se
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
proslavené	proslavený	k2eAgInPc1d1	proslavený
sýry	sýr	k1gInPc1	sýr
<g/>
,	,	kIx,	,
čokolády	čokoláda	k1gFnPc1	čokoláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
špičkový	špičkový	k2eAgInSc4d1	špičkový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
produkční	produkční	k2eAgInSc4d1	produkční
a	a	k8xC	a
distribuční	distribuční	k2eAgInSc4d1	distribuční
systémy	systém	k1gInPc4	systém
a	a	k8xC	a
finančnictví	finančnictví	k1gNnPc4	finančnictví
na	na	k7c6	na
trzích	trh	k1gInPc6	trh
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
a	a	k8xC	a
základ	základ	k1gInSc1	základ
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
firmách	firma	k1gFnPc6	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
jejího	její	k3xOp3gNnSc2	její
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
rozvoje	rozvoj	k1gInSc2	rozvoj
změnilo	změnit	k5eAaPmAgNnS	změnit
z	z	k7c2	z
převážně	převážně	k6eAd1	převážně
zemědělsky	zemědělsky	k6eAd1	zemědělsky
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
kantonů	kanton	k1gInPc2	kanton
<g/>
)	)	kIx)	)
ve	v	k7c4	v
stát	stát	k1gInSc4	stát
s	s	k7c7	s
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
průmyslem	průmysl	k1gInSc7	průmysl
různého	různý	k2eAgInSc2d1	různý
charakteru	charakter	k1gInSc2	charakter
<g/>
:	:	kIx,	:
hodinářství	hodinářství	k1gNnSc4	hodinářství
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgNnSc4d1	přesné
a	a	k8xC	a
jemné	jemný	k2eAgNnSc4d1	jemné
strojírenství	strojírenství	k1gNnSc4	strojírenství
<g/>
,	,	kIx,	,
přístrojová	přístrojový	k2eAgFnSc1d1	přístrojová
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
farmaceutický	farmaceutický	k2eAgInSc1d1	farmaceutický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g />
.	.	kIx.	.
</s>
<s>
přístroje	přístroj	k1gInPc1	přístroj
a	a	k8xC	a
vybavení	vybavení	k1gNnSc1	vybavení
aj.	aj.	kA	aj.
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
mnoho	mnoho	k6eAd1	mnoho
renomovaných	renomovaný	k2eAgFnPc2d1	renomovaná
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
CERN	CERN	kA	CERN
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
HFSJG	HFSJG	kA	HFSJG
(	(	kIx(	(
<g/>
High	High	k1gMnSc1	High
Altitude	Altitud	k1gInSc5	Altitud
Research	Research	k1gMnSc1	Research
Stations	Stationsa	k1gFnPc2	Stationsa
Jungfraujoch	Jungfraujoch	k1gMnSc1	Jungfraujoch
and	and	k?	and
Gornergrat	Gornergrat	k1gMnSc1	Gornergrat
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
jiných	jiný	k2eAgInPc2d1	jiný
výzkumů	výzkum	k1gInPc2	výzkum
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
veřejné	veřejný	k2eAgFnSc2d1	veřejná
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
instituce	instituce	k1gFnSc2	instituce
(	(	kIx(	(
<g/>
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
polytechnika	polytechnika	k1gFnSc1	polytechnika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
instituty	institut	k1gInPc4	institut
velkých	velký	k2eAgFnPc2d1	velká
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc2	aplikace
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
atd.	atd.	kA	atd.
Patrně	patrně	k6eAd1	patrně
nejznámější	známý	k2eAgFnSc7d3	nejznámější
oblastí	oblast	k1gFnSc7	oblast
služeb	služba	k1gFnPc2	služba
ve	v	k7c4	v
Švýcarku	Švýcarka	k1gFnSc4	Švýcarka
je	být	k5eAaImIp3nS	být
bankovnictví	bankovnictví	k1gNnSc1	bankovnictví
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
bankovním	bankovní	k2eAgNnSc7d1	bankovní
tajemstvím	tajemství	k1gNnSc7	tajemství
udržované	udržovaný	k2eAgFnSc2d1	udržovaná
i	i	k9	i
přes	přes	k7c4	přes
tlaky	tlak	k1gInPc4	tlak
konkurence	konkurence	k1gFnSc2	konkurence
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
nověji	nově	k6eAd2	nově
Londýna	Londýn	k1gInSc2	Londýn
ale	ale	k8xC	ale
i	i	k9	i
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oslabit	oslabit	k5eAaPmF	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
bankou	banka	k1gFnSc7	banka
je	být	k5eAaImIp3nS	být
UBS	UBS	kA	UBS
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Credit	Credit	k1gFnSc1	Credit
Suisse	Suisse	k1gFnSc1	Suisse
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
soukromé	soukromý	k2eAgInPc1d1	soukromý
bankovní	bankovní	k2eAgInPc1d1	bankovní
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gMnPc1	jejich
majitelé	majitel	k1gMnPc1	majitel
ručí	ručit	k5eAaImIp3nP	ručit
celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgNnSc7	svůj
jměním	jmění	k1gNnSc7	jmění
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Winterthur	Winterthur	k1gMnSc1	Winterthur
<g/>
,	,	kIx,	,
Zürich	Zürich	k1gMnSc1	Zürich
<g/>
,	,	kIx,	,
Baloise	Baloise	k1gFnSc1	Baloise
aj.	aj.	kA	aj.
Ve	v	k7c6	v
finančních	finanční	k2eAgFnPc6d1	finanční
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
<g/>
,	,	kIx,	,
zakládané	zakládaný	k2eAgFnPc1d1	zakládaná
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
železnice	železnice	k1gFnSc2	železnice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
stalo	stát	k5eAaPmAgNnS	stát
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
pojmem	pojem	k1gInSc7	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Započínající	započínající	k2eAgInSc1d1	započínající
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozmach	rozmach	k1gInSc1	rozmach
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
zpomalený	zpomalený	k2eAgInSc1d1	zpomalený
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
umožnil	umožnit	k5eAaPmAgInS	umožnit
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
ukončení	ukončení	k1gNnSc6	ukončení
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
prosperitu	prosperita	k1gFnSc4	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
spojujícím	spojující	k2eAgNnSc7d1	spojující
odvětvím	odvětví	k1gNnSc7	odvětví
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
propojující	propojující	k2eAgFnSc4d1	propojující
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
hustá	hustý	k2eAgFnSc1d1	hustá
sít	síto	k1gNnPc2	síto
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc1	silnice
přes	přes	k7c4	přes
průsmyky	průsmyk	k1gInPc4	průsmyk
(	(	kIx(	(
<g/>
pass	pass	k1gInSc4	pass
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tunely	tunel	k1gInPc1	tunel
<g/>
,	,	kIx,	,
poštovní	poštovní	k2eAgInPc1d1	poštovní
autobusy	autobus	k1gInPc1	autobus
(	(	kIx(	(
<g/>
Postauto	Postaut	k2eAgNnSc1d1	Postauto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
aglomerační	aglomerační	k2eAgFnSc1d1	aglomerační
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
příměstské	příměstský	k2eAgFnPc1d1	příměstská
rychlodráhy	rychlodráha	k1gFnPc1	rychlodráha
(	(	kIx(	(
<g/>
S-Bahn	S-Bahn	k1gInSc1	S-Bahn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
systémy	systém	k1gInPc4	systém
dopravních	dopravní	k2eAgFnPc2d1	dopravní
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnPc1	železnice
<g/>
,	,	kIx,	,
lanovky	lanovka	k1gFnPc1	lanovka
<g/>
:	:	kIx,	:
SBB-CFF-FFS	SBB-CFF-FFS	k1gFnPc1	SBB-CFF-FFS
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
horské	horský	k2eAgFnPc1d1	horská
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
horské	horský	k2eAgFnPc1d1	horská
lanovky	lanovka	k1gFnPc1	lanovka
<g/>
,	,	kIx,	,
přeložení	přeložení	k1gNnPc1	přeložení
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
tranzitní	tranzitní	k2eAgFnSc2d1	tranzitní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Swiss	Swiss	k1gInSc1	Swiss
(	(	kIx(	(
<g/>
Swiss	Swiss	k1gInSc1	Swiss
International	International	k1gMnSc1	International
Air	Air	k1gMnSc1	Air
Lines	Lines	k1gMnSc1	Lines
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
nízkonákladové	nízkonákladový	k2eAgFnSc2d1	nízkonákladová
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Swissair	Swissair	k1gMnSc1	Swissair
without	without	k1gMnSc1	without
air	air	k?	air
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
přispěla	přispět	k5eAaPmAgFnS	přispět
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
těch	ten	k3xDgFnPc2	ten
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
částí	část	k1gFnPc2	část
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
záležitost	záležitost	k1gFnSc1	záležitost
patřící	patřící	k2eAgFnSc1d1	patřící
bohaté	bohatý	k2eAgFnSc3d1	bohatá
aristokracii	aristokracie	k1gFnSc3	aristokracie
se	se	k3xPyFc4	se
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stala	stát	k5eAaPmAgFnS	stát
záležitostí	záležitost	k1gFnSc7	záležitost
dostupnou	dostupný	k2eAgFnSc7d1	dostupná
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
oblast	oblast	k1gFnSc4	oblast
obyvatel	obyvatel	k1gMnPc2	obyvatel
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
rozvoji	rozvoj	k1gInSc6	rozvoj
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
dostupnosti	dostupnost	k1gFnSc2	dostupnost
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
k	k	k7c3	k
jezerům	jezero	k1gNnPc3	jezero
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
významnou	významný	k2eAgFnSc7d1	významná
oblastí	oblast	k1gFnSc7	oblast
hospodářství	hospodářství	k1gNnSc2	hospodářství
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
váže	vázat	k5eAaImIp3nS	vázat
množství	množství	k1gNnSc1	množství
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
souvisejí	souviset	k5eAaImIp3nP	souviset
<g/>
:	:	kIx,	:
hotely	hotel	k1gInPc1	hotel
<g/>
,	,	kIx,	,
penziony	penzion	k1gInPc1	penzion
<g/>
,	,	kIx,	,
privátní	privátní	k2eAgNnSc1d1	privátní
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejnavštěvovanějším	navštěvovaný	k2eAgNnPc3d3	nejnavštěvovanější
místům	místo	k1gNnPc3	místo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Alpy	Alpy	k1gFnPc1	Alpy
–	–	k?	–
Švýcarské	švýcarský	k2eAgFnPc1d1	švýcarská
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Arosa	Arosa	k1gFnSc1	Arosa
<g/>
,	,	kIx,	,
Davos	Davos	k1gInSc1	Davos
<g/>
,	,	kIx,	,
Engelberg	Engelberg	k1gInSc1	Engelberg
<g/>
,	,	kIx,	,
Bernské	bernský	k2eAgFnPc1d1	Bernská
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Jungfrauregion	Jungfrauregion	k1gInSc1	Jungfrauregion
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Moritz	moritz	k1gInSc1	moritz
<g/>
,	,	kIx,	,
Zermatt	Zermatt	k1gInSc1	Zermatt
a	a	k8xC	a
Matterhorn	Matterhorn	k1gInSc1	Matterhorn
<g/>
,	,	kIx,	,
...	...	k?	...
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
–	–	k?	–
Bodamské	bodamský	k2eAgFnPc1d1	bodamský
(	(	kIx(	(
<g/>
Bodensee	Bodense	k1gFnPc1	Bodense
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Curyšské	curyšský	k2eAgFnSc2d1	curyšská
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Zürichsee	Zürichsee	k1gNnPc3	Zürichsee
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lucernské	Lucernské	k2eAgFnSc1d1	Lucernské
(	(	kIx(	(
<g/>
Vierwaldstättersee	Vierwaldstättersee	k1gFnSc1	Vierwaldstättersee
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
Luganské	Luganský	k2eAgNnSc4d1	Luganské
(	(	kIx(	(
<g/>
Lago	Lago	k1gNnSc4	Lago
di	di	k?	di
Lugano	Lugana	k1gFnSc5	Lugana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thunské	Thunské	k2eAgFnSc1d1	Thunské
(	(	kIx(	(
<g/>
Thunersee	Thunersee	k1gFnSc1	Thunersee
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ženevské	ženevský	k2eAgFnPc1d1	Ženevská
(	(	kIx(	(
<g/>
Lac	Lac	k1gMnSc1	Lac
Léman	Léman	k1gMnSc1	Léman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g />
.	.	kIx.	.
</s>
<s>
část	část	k1gFnSc1	část
–	–	k?	–
Lausanne	Lausanne	k1gNnSc1	Lausanne
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
část	část	k1gFnSc1	část
–	–	k?	–
Locarno	Locarno	k1gNnSc1	Locarno
a	a	k8xC	a
Ascona	Ascona	k1gFnSc1	Ascona
<g/>
,	,	kIx,	,
Lugano	Lugana	k1gFnSc5	Lugana
<g/>
,	,	kIx,	,
...	...	k?	...
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
část	část	k1gFnSc1	část
–	–	k?	–
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
,	,	kIx,	,
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
Lucern	Lucern	k1gInSc1	Lucern
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
Ballenberg	Ballenberg	k1gInSc1	Ballenberg
<g/>
,	,	kIx,	,
...	...	k?	...
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
speciality	specialita	k1gFnSc2	specialita
<g/>
,	,	kIx,	,
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
:	:	kIx,	:
Glacier	Glacier	k1gInSc1	Glacier
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
SBB-CFF-FFS	SBB-CFF-FFS	k1gFnSc1	SBB-CFF-FFS
<g/>
,	,	kIx,	,
...	...	k?	...
Nejznámější	známý	k2eAgNnPc1d3	nejznámější
tradiční	tradiční	k2eAgNnPc1d1	tradiční
švýcarská	švýcarský	k2eAgNnPc1d1	švýcarské
jídla	jídlo	k1gNnPc1	jídlo
<g/>
:	:	kIx,	:
fondue	fondue	k1gInSc1	fondue
ze	z	k7c2	z
sýra	sýr	k1gInSc2	sýr
<g/>
,	,	kIx,	,
Raclette	Raclett	k1gMnSc5	Raclett
<g/>
,	,	kIx,	,
Rösti	Rösť	k1gFnSc5	Rösť
<g/>
,	,	kIx,	,
...	...	k?	...
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Švýcary	Švýcar	k1gMnPc4	Švýcar
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
války	válka	k1gFnPc1	válka
nevedou	vést	k5eNaImIp3nP	vést
k	k	k7c3	k
prosperitě	prosperita	k1gFnSc3	prosperita
a	a	k8xC	a
spokojenosti	spokojenost	k1gFnSc3	spokojenost
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Poznání	poznání	k1gNnSc1	poznání
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
revoluční	revoluční	k2eAgFnSc1d1	revoluční
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
státech	stát	k1gInPc6	stát
Evropy	Evropa	k1gFnSc2	Evropa
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
teprve	teprve	k6eAd1	teprve
až	až	k9	až
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
občan	občan	k1gMnSc1	občan
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
suverénem	suverén	k1gMnSc7	suverén
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
byl	být	k5eAaImAgMnS	být
běžný	běžný	k2eAgInSc4d1	běžný
vliv	vliv	k1gInSc4	vliv
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
státu	stát	k1gInSc2	stát
již	již	k6eAd1	již
od	od	k7c2	od
posledních	poslední	k2eAgNnPc2d1	poslední
desetiletí	desetiletí	k1gNnPc2	desetiletí
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
evropské	evropský	k2eAgInPc1d1	evropský
státy	stát	k1gInPc1	stát
dospěly	dochvít	k5eAaPmAgInP	dochvít
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
taktéž	taktéž	k?	taktéž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
ostatní	ostatní	k2eAgFnSc6d1	ostatní
Evropě	Evropa	k1gFnSc6	Evropa
začíná	začínat	k5eAaImIp3nS	začínat
diskutovat	diskutovat	k5eAaImF	diskutovat
o	o	k7c4	o
participaci	participace	k1gFnSc4	participace
občanů	občan	k1gMnPc2	občan
na	na	k7c6	na
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
již	již	k6eAd1	již
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ústavě	ústava	k1gFnSc6	ústava
přijaté	přijatý	k2eAgFnSc6d1	přijatá
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
jazyků	jazyk	k1gInPc2	jazyk
původně	původně	k6eAd1	původně
žijících	žijící	k2eAgMnPc2d1	žijící
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
zaručené	zaručený	k2eAgNnSc4d1	zaručené
je	být	k5eAaImIp3nS	být
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
se	s	k7c7	s
spolkovými	spolkový	k2eAgInPc7d1	spolkový
úřady	úřad	k1gInPc7	úřad
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
úředních	úřední	k2eAgInPc6d1	úřední
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
čtyři	čtyři	k4xCgInPc4	čtyři
úřední	úřední	k2eAgInPc4d1	úřední
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
úřady	úřad	k1gInPc1	úřad
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pokračující	pokračující	k2eAgFnSc7d1	pokračující
integrací	integrace	k1gFnSc7	integrace
cizinců	cizinec	k1gMnPc2	cizinec
a	a	k8xC	a
imigrantů	imigrant	k1gMnPc2	imigrant
úřady	úřad	k1gInPc7	úřad
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
komunikaci	komunikace	k1gFnSc3	komunikace
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
také	také	k9	také
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
omylem	omyl	k1gInSc7	omyl
je	být	k5eAaImIp3nS	být
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
Švýcaři	Švýcar	k1gMnPc1	Švýcar
hovoří	hovořit	k5eAaImIp3nP	hovořit
všemi	všecek	k3xTgMnPc7	všecek
čtyřmi	čtyři	k4xCgInPc7	čtyři
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Švýcaři	Švýcar	k1gMnPc1	Švýcar
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
mateřštinou	mateřština	k1gFnSc7	mateřština
je	být	k5eAaImIp3nS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
sice	sice	k8xC	sice
většinou	většinou	k6eAd1	většinou
učí	učit	k5eAaImIp3nS	učit
jako	jako	k9	jako
první	první	k4xOgInSc4	první
cizí	cizí	k2eAgInSc4d1	cizí
jazyk	jazyk	k1gInSc4	jazyk
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
jako	jako	k9	jako
další	další	k2eAgFnSc4d1	další
angličtinu	angličtina	k1gFnSc4	angličtina
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
kantonech	kanton	k1gInPc6	kanton
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
italsky	italsky	k6eAd1	italsky
již	již	k6eAd1	již
mluví	mluvit	k5eAaImIp3nS	mluvit
jen	jen	k9	jen
málokterý	málokterý	k3yIgMnSc1	málokterý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Němčinou	němčina	k1gFnSc7	němčina
hovoří	hovořit	k5eAaImIp3nS	hovořit
63,7	[number]	k4	63,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
13	[number]	k4	13
kantonech	kanton	k1gInPc6	kanton
a	a	k8xC	a
6	[number]	k4	6
polokantonech	polokanton	k1gInPc6	polokanton
včetně	včetně	k7c2	včetně
kantonu	kanton	k1gInSc2	kanton
Graubünden	Graubündna	k1gFnPc2	Graubündna
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
druhým	druhý	k4xOgInSc7	druhý
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
kantonech	kanton	k1gInPc6	kanton
Fribourg	Fribourg	k1gMnSc1	Fribourg
a	a	k8xC	a
Wallis	Wallis	k1gFnSc1	Wallis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
částech	část	k1gFnPc6	část
se	se	k3xPyFc4	se
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
styku	styk	k1gInSc6	styk
používá	používat	k5eAaImIp3nS	používat
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
alemánských	alemánský	k2eAgInPc2d1	alemánský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Francouzštinou	francouzština	k1gFnSc7	francouzština
hovoří	hovořit	k5eAaImIp3nS	hovořit
20,4	[number]	k4	20,4
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
6	[number]	k4	6
kantonech	kanton	k1gInPc6	kanton
(	(	kIx(	(
<g/>
Fribourg	Fribourg	k1gMnSc1	Fribourg
<g/>
,	,	kIx,	,
Jura	Jura	k1gMnSc1	Jura
<g/>
,	,	kIx,	,
Neuchâtel	Neuchâtel	k1gMnSc1	Neuchâtel
<g/>
,	,	kIx,	,
Vaud	Vaud	k1gMnSc1	Vaud
<g/>
,	,	kIx,	,
Wallis	Wallis	k1gFnSc1	Wallis
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
kantonu	kanton	k1gInSc2	kanton
Bern	Bern	k1gInSc1	Bern
<g/>
.	.	kIx.	.
</s>
<s>
Italštinou	italština	k1gFnSc7	italština
hovoří	hovořit	k5eAaImIp3nS	hovořit
6,7	[number]	k4	6,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Italština	italština	k1gFnSc1	italština
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Ticino	Ticin	k2eAgNnSc1d1	Ticino
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
kantonu	kanton	k1gInSc2	kanton
Graubünden	Graubündna	k1gFnPc2	Graubündna
<g/>
.	.	kIx.	.
</s>
<s>
Rétorománštinou	rétorománština	k1gFnSc7	rétorománština
hovoří	hovořit	k5eAaImIp3nS	hovořit
0,5	[number]	k4	0,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rétorománština	rétorománština	k1gFnSc1	rétorománština
je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
kantonu	kanton	k1gInSc2	kanton
Graubünden	Graubündna	k1gFnPc2	Graubündna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rétorománštině	rétorománština	k1gFnSc6	rétorománština
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
poloúředním	poloúřední	k2eAgMnSc6d1	poloúřední
(	(	kIx(	(
<g/>
polooficiálním	polooficiální	k2eAgInSc6d1	polooficiální
<g/>
)	)	kIx)	)
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
se	s	k7c7	s
spolkovými	spolkový	k2eAgMnPc7d1	spolkový
úřady	úřada	k1gMnPc7	úřada
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
rétorománština	rétorománština	k1gFnSc1	rétorománština
jazykem	jazyk	k1gInSc7	jazyk
mateřským	mateřský	k2eAgInSc7d1	mateřský
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
právo	právo	k1gNnSc4	právo
získali	získat	k5eAaPmAgMnP	získat
navíc	navíc	k6eAd1	navíc
rétorománsky	rétorománsky	k6eAd1	rétorománsky
hovořící	hovořící	k2eAgMnPc1d1	hovořící
Švýcaři	Švýcar	k1gMnPc1	Švýcar
až	až	k9	až
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
dostala	dostat	k5eAaPmAgFnS	dostat
rétorománština	rétorománština	k1gFnSc1	rétorománština
status	status	k1gInSc1	status
úředního	úřední	k2eAgInSc2d1	úřední
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgNnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
pouze	pouze	k6eAd1	pouze
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
mohli	moct	k5eAaImAgMnP	moct
Švýcaři	Švýcar	k1gMnPc1	Švýcar
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
úřady	úřad	k1gInPc7	úřad
rétorománsky	rétorománsky	k6eAd1	rétorománsky
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
kantonální	kantonální	k2eAgFnSc6d1	kantonální
úrovni	úroveň	k1gFnSc6	úroveň
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Graubünden	Graubündna	k1gFnPc2	Graubündna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počáteční	počáteční	k2eAgFnSc6d1	počáteční
segregační	segregační	k2eAgFnSc6d1	segregační
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc6d2	pozdější
integrační	integrační	k2eAgFnSc6d1	integrační
kontrole	kontrola	k1gFnSc6	kontrola
pod	pod	k7c7	pod
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
tlakem	tlak	k1gInSc7	tlak
protestů	protest	k1gInPc2	protest
proti	proti	k7c3	proti
cizincům	cizinec	k1gMnPc3	cizinec
se	se	k3xPyFc4	se
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
přibrzdit	přibrzdit	k5eAaPmF	přibrzdit
imigrační	imigrační	k2eAgInPc4d1	imigrační
trendy	trend	k1gInPc4	trend
a	a	k8xC	a
přesunout	přesunout	k5eAaPmF	přesunout
toto	tento	k3xDgNnSc4	tento
stěhování	stěhování	k1gNnSc4	stěhování
více	hodně	k6eAd2	hodně
do	do	k7c2	do
kolejí	kolej	k1gFnPc2	kolej
dočasné	dočasný	k2eAgFnSc2d1	dočasná
nebo	nebo	k8xC	nebo
sezónní	sezónní	k2eAgFnSc2d1	sezónní
pracovní	pracovní	k2eAgFnSc2d1	pracovní
migrace	migrace	k1gFnSc2	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
přístupem	přístup	k1gInSc7	přístup
byla	být	k5eAaImAgFnS	být
pracovní	pracovní	k2eAgFnSc1d1	pracovní
migrace	migrace	k1gFnSc1	migrace
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
pokrývána	pokrývat	k5eAaImNgFnS	pokrývat
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
najímání	najímání	k1gNnSc6	najímání
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
rozsahu	rozsah	k1gInSc6	rozsah
pak	pak	k6eAd1	pak
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zažilo	zažít	k5eAaPmAgNnS	zažít
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
nejmohutnější	mohutný	k2eAgFnSc2d3	nejmohutnější
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
téměř	téměř	k6eAd1	téměř
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgInS	způsobit
i	i	k9	i
silný	silný	k2eAgInSc1d1	silný
početní	početní	k2eAgInSc1d1	početní
vzestup	vzestup	k1gInSc1	vzestup
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodech	odchod	k1gInPc6	odchod
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
nejpočetnější	početní	k2eAgFnSc2d3	nejpočetnější
skupiny	skupina	k1gFnSc2	skupina
Italů	Ital	k1gMnPc2	Ital
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgFnP	změnit
i	i	k9	i
národnostní	národnostní	k2eAgFnPc1d1	národnostní
proporce	proporce	k1gFnPc1	proporce
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
Jugoslávců	Jugoslávec	k1gMnPc2	Jugoslávec
<g/>
,	,	kIx,	,
Turků	Turek	k1gMnPc2	Turek
a	a	k8xC	a
Portugalců	Portugalec	k1gMnPc2	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvou	smlouva	k1gFnSc7	smlouva
o	o	k7c6	o
náboru	nábor	k1gInSc6	nábor
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
začal	začít	k5eAaPmAgMnS	začít
fungovat	fungovat	k5eAaImF	fungovat
švýcarský	švýcarský	k2eAgInSc4d1	švýcarský
rotační	rotační	k2eAgInSc4d1	rotační
model	model	k1gInSc4	model
časově	časově	k6eAd1	časově
omezených	omezený	k2eAgFnPc2d1	omezená
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
obnovitelných	obnovitelný	k2eAgFnPc2d1	obnovitelná
pracovních	pracovní	k2eAgFnPc2d1	pracovní
smluv	smlouva	k1gFnPc2	smlouva
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
saisoniers	saisoniers	k1gInSc4	saisoniers
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
pobytem	pobyt	k1gInSc7	pobyt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmnácti	osmnáct	k4xCc6	osmnáct
měsících	měsíc	k1gInPc6	měsíc
pracovního	pracovní	k2eAgInSc2d1	pracovní
pobytu	pobyt	k1gInSc2	pobyt
byla	být	k5eAaImAgFnS	být
nabídnuta	nabídnout	k5eAaPmNgFnS	nabídnout
možnost	možnost	k1gFnSc1	možnost
pozvat	pozvat	k5eAaPmF	pozvat
další	další	k2eAgMnPc4d1	další
rodinné	rodinný	k2eAgMnPc4d1	rodinný
příslušníky	příslušník	k1gMnPc4	příslušník
<g/>
,	,	kIx,	,
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
volný	volný	k2eAgInSc4d1	volný
výběr	výběr	k1gInSc4	výběr
pracovního	pracovní	k2eAgNnSc2d1	pracovní
místa	místo	k1gNnSc2	místo
při	při	k7c6	při
pravidelném	pravidelný	k2eAgNnSc6d1	pravidelné
obnovování	obnovování	k1gNnSc6	obnovování
pracovního	pracovní	k2eAgMnSc2d1	pracovní
a	a	k8xC	a
pobytového	pobytový	k2eAgNnSc2d1	pobytové
povolení	povolení	k1gNnSc2	povolení
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
právo	práv	k2eAgNnSc1d1	právo
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
základě	základ	k1gInSc6	základ
plné	plný	k2eAgFnSc2d1	plná
rovnoprávnosti	rovnoprávnost	k1gFnSc2	rovnoprávnost
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
tradičně	tradičně	k6eAd1	tradičně
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
těžko	těžko	k6eAd1	těžko
dostupné	dostupný	k2eAgNnSc4d1	dostupné
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
pracovnímu	pracovní	k2eAgNnSc3d1	pracovní
a	a	k8xC	a
sociálnímu	sociální	k2eAgNnSc3d1	sociální
zrovnoprávnění	zrovnoprávnění	k1gNnSc3	zrovnoprávnění
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c4	v
druhé	druhý	k4xOgNnSc4	druhý
"	"	kIx"	"
<g/>
italské	italský	k2eAgFnSc3d1	italská
smlouvě	smlouva	k1gFnSc3	smlouva
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
zvedly	zvednout	k5eAaPmAgFnP	zvednout
mohutné	mohutný	k2eAgFnPc1d1	mohutná
protestní	protestní	k2eAgFnPc1d1	protestní
vlny	vlna	k1gFnPc1	vlna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
proticizinecká	proticizinecký	k2eAgNnPc4d1	proticizinecký
hnutí	hnutí	k1gNnPc4	hnutí
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
zahlcení	zahlcení	k1gNnSc3	zahlcení
země	zem	k1gFnSc2	zem
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
řídících	řídící	k2eAgInPc2d1	řídící
mechanismů	mechanismus	k1gInPc2	mechanismus
migrační	migrační	k2eAgFnSc2d1	migrační
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
vytvořených	vytvořený	k2eAgMnPc2d1	vytvořený
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
propuknutím	propuknutí	k1gNnSc7	propuknutí
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
počty	počet	k1gInPc1	počet
cizinců	cizinec	k1gMnPc2	cizinec
klesaly	klesat	k5eAaImAgFnP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Protestní	protestní	k2eAgNnSc4d1	protestní
hnutí	hnutí	k1gNnSc4	hnutí
umlkla	umlknout	k5eAaPmAgFnS	umlknout
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
trvalému	trvalý	k2eAgInSc3d1	trvalý
příchodu	příchod	k1gInSc3	příchod
dalších	další	k2eAgMnPc2d1	další
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
již	již	k6eAd1	již
usazených	usazený	k2eAgFnPc2d1	usazená
osob	osoba	k1gFnPc2	osoba
přidal	přidat	k5eAaPmAgInS	přidat
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
počet	počet	k1gInSc1	počet
žadatelů	žadatel	k1gMnPc2	žadatel
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
je	být	k5eAaImIp3nS	být
garantována	garantovat	k5eAaBmNgFnS	garantovat
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
ústavou	ústava	k1gFnSc7	ústava
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
15	[number]	k4	15
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Referendem	referendum	k1gNnSc7	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Ústavy	ústava	k1gFnSc2	ústava
vypuštěny	vypuštěn	k2eAgInPc1d1	vypuštěn
články	článek	k1gInPc1	článek
bránící	bránící	k2eAgInSc1d1	bránící
jezuitskému	jezuitský	k2eAgInSc3d1	jezuitský
řádu	řád	k1gInSc3	řád
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
ústavy	ústav	k1gInPc4	ústav
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neexistuje	existovat	k5eNaImIp3nS	existovat
oficiální	oficiální	k2eAgFnSc1d1	oficiální
státní	státní	k2eAgFnSc1d1	státní
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
kantonů	kanton	k1gInPc2	kanton
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Ženevy	Ženeva	k1gFnSc2	Ženeva
a	a	k8xC	a
Neuchâtelu	Neuchâtel	k1gInSc2	Neuchâtel
<g/>
)	)	kIx)	)
však	však	k9	však
finančně	finančně	k6eAd1	finančně
podporuje	podporovat	k5eAaImIp3nS	podporovat
buď	buď	k8xC	buď
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
<g/>
,	,	kIx,	,
starokatolickou	starokatolický	k2eAgFnSc4d1	Starokatolická
nebo	nebo	k8xC	nebo
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
reformovanou	reformovaný	k2eAgFnSc4d1	reformovaná
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
příslušníků	příslušník	k1gMnPc2	příslušník
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
:	:	kIx,	:
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
38,21	[number]	k4	38,21
%	%	kIx~	%
<g/>
,	,	kIx,	,
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
reformovaná	reformovaný	k2eAgFnSc1d1	reformovaná
církev	církev	k1gFnSc1	církev
26,93	[number]	k4	26,93
%	%	kIx~	%
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
protestanti	protestant	k1gMnPc1	protestant
2,89	[number]	k4	2,89
%	%	kIx~	%
<g/>
,	,	kIx,	,
muslimové	muslim	k1gMnPc1	muslim
4,95	[number]	k4	4,95
%	%	kIx~	%
<g/>
,	,	kIx,	,
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
2,14	[number]	k4	2,14
%	%	kIx~	%
<g/>
,	,	kIx,	,
hinduisté	hinduista	k1gMnPc1	hinduista
0,50	[number]	k4	0,50
%	%	kIx~	%
<g/>
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
0,25	[number]	k4	0,25
%	%	kIx~	%
<g/>
,	,	kIx,	,
buddhisté	buddhista	k1gMnPc1	buddhista
0,52	[number]	k4	0,52
%	%	kIx~	%
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
křesťané	křesťan	k1gMnPc1	křesťan
0,65	[number]	k4	0,65
%	%	kIx~	%
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
21,42	[number]	k4	21,42
%	%	kIx~	%
(	(	kIx(	(
<g/>
ateisté	ateista	k1gMnPc1	ateista
<g/>
,	,	kIx,	,
agnostici	agnostik	k1gMnPc1	agnostik
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
bez	bez	k7c2	bez
církve	církev	k1gFnSc2	církev
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgMnSc1d1	ostatní
0,28	[number]	k4	0,28
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
kultura	kultura	k1gFnSc1	kultura
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
okolních	okolní	k2eAgFnPc2d1	okolní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
však	však	k9	však
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
kultuře	kultura	k1gFnSc6	kultura
jako	jako	k8xC	jako
o	o	k7c4	o
homogenní	homogenní	k2eAgInSc4d1	homogenní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
sama	sám	k3xTgFnSc1	sám
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
jazykových	jazykový	k2eAgFnPc2d1	jazyková
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
zejména	zejména	k9	zejména
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
švýcarských	švýcarský	k2eAgMnPc2d1	švýcarský
umělců	umělec	k1gMnPc2	umělec
je	být	k5eAaImIp3nS	být
pokládano	pokládana	k1gFnSc5	pokládana
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
měla	mít	k5eAaImAgFnS	mít
zpočátku	zpočátku	k6eAd1	zpočátku
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
měšťané	měšťan	k1gMnPc1	měšťan
a	a	k8xC	a
patriciát	patriciát	k1gInSc1	patriciát
neboť	neboť	k8xC	neboť
země	země	k1gFnSc1	země
nebyla	být	k5eNaImAgFnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
feudály	feudál	k1gMnPc4	feudál
<g/>
.	.	kIx.	.
</s>
<s>
Podporováno	podporován	k2eAgNnSc1d1	podporováno
bylo	být	k5eAaImAgNnS	být
hlavně	hlavně	k6eAd1	hlavně
stavitelství	stavitelství	k1gNnSc1	stavitelství
(	(	kIx(	(
<g/>
románské	románský	k2eAgFnSc2d1	románská
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
<g/>
,	,	kIx,	,
Sionu	Siono	k1gNnSc6	Siono
Chur	Chura	k1gFnPc2	Chura
<g/>
,	,	kIx,	,
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgInPc1d1	renesanční
kostely	kostel	k1gInPc1	kostel
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Ticino	Ticin	k2eAgNnSc1d1	Ticino
či	či	k8xC	či
radnice	radnice	k1gFnSc1	radnice
v	v	k7c4	v
Lucernu	lucerna	k1gFnSc4	lucerna
<g/>
,	,	kIx,	,
Solothurnu	Solothurna	k1gFnSc4	Solothurna
a	a	k8xC	a
Brigu	briga	k1gFnSc4	briga
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInPc4d1	barokní
kláštery	klášter	k1gInPc4	klášter
v	v	k7c4	v
Einsiedelu	Einsiedela	k1gFnSc4	Einsiedela
a	a	k8xC	a
St.	st.	kA	st.
Gallenu	Gallen	k1gInSc2	Gallen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cizí	cizí	k2eAgMnPc1d1	cizí
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
,	,	kIx,	,
přitahováni	přitahován	k2eAgMnPc1d1	přitahován
i	i	k9	i
větší	veliký	k2eAgFnSc7d2	veliký
svobodou	svoboda	k1gFnSc7	svoboda
země	zem	k1gFnSc2	zem
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
krásná	krásný	k2eAgFnSc5d1	krásná
centra	centrum	k1gNnPc1	centrum
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Freiburg	Freiburg	k1gInSc1	Freiburg
<g/>
,	,	kIx,	,
Lucern	Lucern	k1gInSc1	Lucern
či	či	k8xC	či
St.	st.	kA	st.
Gallen	Gallen	k1gInSc1	Gallen
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
podle	podle	k7c2	podle
jazykových	jazykový	k2eAgFnPc2d1	jazyková
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
německou	německý	k2eAgFnSc4d1	německá
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
<g/>
,	,	kIx,	,
italskou	italský	k2eAgFnSc4d1	italská
a	a	k8xC	a
rétorománskou	rétorománský	k2eAgFnSc4d1	rétorománská
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
období	období	k1gNnSc2	období
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
moderních	moderní	k2eAgMnPc2d1	moderní
dramatiků	dramatik	k1gMnPc2	dramatik
je	být	k5eAaImIp3nS	být
Friedrich	Friedrich	k1gMnSc1	Friedrich
Dürrenmatt	Dürrenmatt	k1gMnSc1	Dürrenmatt
<g/>
.	.	kIx.	.
</s>
<s>
Hermetismem	hermetismus	k1gInSc7	hermetismus
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
dílo	dílo	k1gNnSc1	dílo
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hermana	Herman	k1gMnSc2	Herman
Hesseho	Hesse	k1gMnSc2	Hesse
<g/>
.	.	kIx.	.
</s>
<s>
Nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
je	být	k5eAaImIp3nS	být
Carl	Carl	k1gMnSc1	Carl
Spitteler	Spitteler	k1gMnSc1	Spitteler
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k9	rovněž
Max	Max	k1gMnSc1	Max
Frisch	Frisch	k1gMnSc1	Frisch
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Walser	Walser	k1gMnSc1	Walser
<g/>
,	,	kIx,	,
Gottfried	Gottfried	k1gMnSc1	Gottfried
Keller	Keller	k1gMnSc1	Keller
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
Cendrars	Cendrars	k1gInSc1	Cendrars
<g/>
,	,	kIx,	,
Charles-Ferdinand	Charles-Ferdinand	k1gInSc1	Charles-Ferdinand
Ramuz	Ramuz	k1gInSc1	Ramuz
<g/>
,	,	kIx,	,
Conrad	Conrad	k1gInSc1	Conrad
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Meyer	Meyer	k1gMnSc1	Meyer
<g/>
,	,	kIx,	,
Jeremias	Jeremias	k1gMnSc1	Jeremias
Gotthelf	Gotthelf	k1gMnSc1	Gotthelf
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Agota	Agota	k1gFnSc1	Agota
Kristofová	Kristofový	k2eAgFnSc1d1	Kristofová
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
autorkou	autorka	k1gFnSc7	autorka
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
byla	být	k5eAaImAgFnS	být
Johanna	Johanna	k1gFnSc1	Johanna
Spyri	Spyr	k1gFnSc2	Spyr
<g/>
.	.	kIx.	.
</s>
<s>
Alberto	Alberta	k1gFnSc5	Alberta
Giacometti	Giacometť	k1gFnSc5	Giacometť
je	být	k5eAaImIp3nS	být
možná	možná	k6eAd1	možná
nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
avantgardním	avantgardní	k2eAgMnSc7d1	avantgardní
sochařem	sochař	k1gMnSc7	sochař
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
avantgardy	avantgarda	k1gFnSc2	avantgarda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
Paul	Paul	k1gMnSc1	Paul
Klee	Kle	k1gFnSc2	Kle
<g/>
.	.	kIx.	.
</s>
<s>
Rodačkou	rodačka	k1gFnSc7	rodačka
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
je	být	k5eAaImIp3nS	být
i	i	k9	i
neoklasicistická	neoklasicistický	k2eAgFnSc1d1	neoklasicistický
malířka	malířka	k1gFnSc1	malířka
Angelica	Angelica	k1gFnSc1	Angelica
Kauffmanová	Kauffmanová	k1gFnSc1	Kauffmanová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
expresionisty	expresionista	k1gMnSc2	expresionista
si	se	k3xPyFc3	se
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
důležité	důležitý	k2eAgNnSc4d1	důležité
místo	místo	k1gNnSc4	místo
malíř	malíř	k1gMnSc1	malíř
Johannes	Johannes	k1gMnSc1	Johannes
Itten	Ittno	k1gNnPc2	Ittno
<g/>
.	.	kIx.	.
</s>
<s>
Prací	práce	k1gFnSc7	práce
na	na	k7c6	na
filmu	film	k1gInSc6	film
Vetřelec	vetřelec	k1gMnSc1	vetřelec
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
surrealistický	surrealistický	k2eAgMnSc1d1	surrealistický
malíř	malíř	k1gMnSc1	malíř
H.	H.	kA	H.
R.	R.	kA	R.
Giger	Giger	k1gMnSc1	Giger
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
dřevoryty	dřevoryt	k1gInPc7	dřevoryt
proslul	proslout	k5eAaPmAgInS	proslout
Félix	Félix	k1gInSc1	Félix
Vallotton	Vallotton	k1gInSc1	Vallotton
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgNnSc7	svůj
kinetickým	kinetický	k2eAgNnSc7d1	kinetické
uměním	umění	k1gNnSc7	umění
Jean	Jean	k1gMnSc1	Jean
Tinguely	Tinguela	k1gFnSc2	Tinguela
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
stylu	styl	k1gInSc3	styl
zvanému	zvaný	k2eAgInSc3d1	zvaný
paralelismus	paralelismus	k1gInSc1	paralelismus
<g/>
,	,	kIx,	,
blízkému	blízký	k2eAgInSc3d1	blízký
secesi	secese	k1gFnSc3	secese
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
propracoval	propracovat	k5eAaPmAgInS	propracovat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
malíř	malíř	k1gMnSc1	malíř
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Hodler	Hodler	k1gMnSc1	Hodler
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
výjevům	výjev	k1gInPc3	výjev
ze	z	k7c2	z
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
venkova	venkov	k1gInSc2	venkov
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nP	držet
post	post	k1gInSc4	post
národního	národní	k2eAgMnSc2d1	národní
malíře	malíř	k1gMnSc2	malíř
Albert	Albert	k1gMnSc1	Albert
Anker	Anker	k1gMnSc1	Anker
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
symbolistickým	symbolistický	k2eAgMnSc7d1	symbolistický
malířem	malíř	k1gMnSc7	malíř
byl	být	k5eAaImAgMnS	být
Arnold	Arnold	k1gMnSc1	Arnold
Böcklin	Böcklina	k1gFnPc2	Böcklina
<g/>
,	,	kIx,	,
romantickým	romantický	k2eAgInSc7d1	romantický
Henry	henry	k1gInSc1	henry
Fuseli	Fusel	k1gInSc6	Fusel
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
Švýcarů	Švýcar	k1gMnPc2	Švýcar
je	být	k5eAaImIp3nS	být
také	také	k9	také
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
slavného	slavný	k2eAgInSc2d1	slavný
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
byl	být	k5eAaImAgMnS	být
Hannes	Hannes	k1gMnSc1	Hannes
Meyer	Meyer	k1gMnSc1	Meyer
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
modernismu	modernismus	k1gInSc2	modernismus
je	být	k5eAaImIp3nS	být
i	i	k9	i
Mario	Mario	k1gMnSc1	Mario
Botta	Botta	k1gMnSc1	Botta
<g/>
.	.	kIx.	.
</s>
<s>
Prestižní	prestižní	k2eAgFnSc4d1	prestižní
Pritzkerovu	Pritzkerův	k2eAgFnSc4d1	Pritzkerova
cenu	cena	k1gFnSc4	cena
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
Peter	Peter	k1gMnSc1	Peter
Zumthor	Zumthor	k1gMnSc1	Zumthor
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Herzog	Herzoga	k1gFnPc2	Herzoga
&	&	k?	&
de	de	k?	de
Meuron	Meuron	k1gMnSc1	Meuron
architektů	architekt	k1gMnPc2	architekt
Jacquese	Jacques	k1gMnSc2	Jacques
Herzoga	Herzog	k1gMnSc2	Herzog
a	a	k8xC	a
Pierra	Pierr	k1gMnSc2	Pierr
de	de	k?	de
Meurona	Meuron	k1gMnSc2	Meuron
postavila	postavit	k5eAaPmAgFnS	postavit
například	například	k6eAd1	například
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
Allianz	Allianz	k1gInSc1	Allianz
Arenu	Aren	k1gInSc6	Aren
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
architektonického	architektonický	k2eAgInSc2d1	architektonický
dekonstruktivismu	dekonstruktivismus	k1gInSc2	dekonstruktivismus
je	být	k5eAaImIp3nS	být
Bernard	Bernard	k1gMnSc1	Bernard
Tschumi	Tschu	k1gFnPc7	Tschu
<g/>
.	.	kIx.	.
</s>
<s>
Francesco	Francesco	k6eAd1	Francesco
Borromini	Borromin	k2eAgMnPc1d1	Borromin
a	a	k8xC	a
Carlo	Carlo	k1gNnSc1	Carlo
Maderno	Maderna	k1gFnSc5	Maderna
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
narozeni	narozen	k2eAgMnPc1d1	narozen
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Ticinu	Ticin	k1gInSc6	Ticin
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
barokním	barokní	k2eAgMnPc3d1	barokní
architektům	architekt	k1gMnPc3	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
pro	pro	k7c4	pro
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
je	být	k5eAaImIp3nS	být
dechový	dechový	k2eAgInSc1d1	dechový
nástroj	nástroj	k1gInSc1	nástroj
Alphorn	Alphorna	k1gFnPc2	Alphorna
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používáný	používáný	k2eAgMnSc1d1	používáný
pastýři	pastýř	k1gMnPc1	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
hudebními	hudební	k2eAgMnPc7d1	hudební
skladateli	skladatel	k1gMnPc7	skladatel
jsou	být	k5eAaImIp3nP	být
Othmar	Othmar	k1gMnSc1	Othmar
Schoeck	Schoeck	k1gMnSc1	Schoeck
a	a	k8xC	a
Arthur	Arthur	k1gMnSc1	Arthur
Honegger	Honegger	k1gMnSc1	Honegger
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Lys	lysa	k1gFnPc2	lysa
Assia	Assia	k1gFnSc1	Assia
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
soutěže	soutěž	k1gFnSc2	soutěž
Eurovize	Eurovize	k1gFnSc2	Eurovize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
mezinárodně	mezinárodně	k6eAd1	mezinárodně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
představitel	představitel	k1gMnSc1	představitel
euro-popu	euroop	k1gInSc2	euro-pop
DJ	DJ	kA	DJ
Bobo	Bobo	k1gMnSc1	Bobo
<g/>
.	.	kIx.	.
</s>
<s>
Oceňovaným	oceňovaný	k2eAgMnSc7d1	oceňovaný
skladatelem	skladatel	k1gMnSc7	skladatel
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
Robert	Robert	k1gMnSc1	Robert
Miles	Miles	k1gMnSc1	Miles
nebo	nebo	k8xC	nebo
i	i	k9	i
Dieter	Dieter	k1gMnSc1	Dieter
Meier	Meier	k1gMnSc1	Meier
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Borisem	Boris	k1gMnSc7	Boris
Blankem	Blanek	k1gMnSc7	Blanek
tvoří	tvořit	k5eAaImIp3nP	tvořit
skupinu	skupina	k1gFnSc4	skupina
Yello	Yello	k1gNnSc1	Yello
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rocku	rock	k1gInSc6	rock
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
skupiny	skupina	k1gFnSc2	skupina
Celtic	Celtice	k1gFnPc2	Celtice
Frost	Frost	k1gFnSc1	Frost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
extrémnímu	extrémní	k2eAgInSc3d1	extrémní
metalu	metal	k1gInSc3	metal
<g/>
,	,	kIx,	,
či	či	k8xC	či
progresivní	progresivní	k2eAgMnPc1d1	progresivní
thrasheři	thrasher	k1gMnPc1	thrasher
Coroner	coroner	k1gMnSc1	coroner
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
Ursula	Ursula	k1gFnSc1	Ursula
Andressová	Andressová	k1gFnSc1	Andressová
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
populárního	populární	k2eAgInSc2d1	populární
filmu	film	k1gInSc2	film
jako	jako	k8xS	jako
bondgirl	bondgirla	k1gFnPc2	bondgirla
v	v	k7c6	v
první	první	k4xOgFnSc6	první
filmové	filmový	k2eAgFnSc6d1	filmová
bondovce	bondovce	k?	bondovce
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
No	no	k9	no
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vycházejí	vycházet	k5eAaImIp3nP	vycházet
regionální	regionální	k2eAgNnPc1d1	regionální
periodika	periodikum	k1gNnPc1	periodikum
(	(	kIx(	(
<g/>
také	také	k9	také
podle	podle	k7c2	podle
jazykových	jazykový	k2eAgFnPc2d1	jazyková
oblastí	oblast	k1gFnPc2	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
několik	několik	k4yIc4	několik
celostátních	celostátní	k2eAgFnPc2d1	celostátní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Neue	Neue	k1gInSc1	Neue
Zürcher	Zürchra	k1gFnPc2	Zürchra
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
veřejná	veřejný	k2eAgFnSc1d1	veřejná
vysílací	vysílací	k2eAgFnSc1d1	vysílací
společnost	společnost	k1gFnSc1	společnost
SRG	SRG	kA	SRG
SSR	SSR	kA	SSR
idée	idé	k1gMnSc2	idé
suisse	suiss	k1gMnSc2	suiss
vysílá	vysílat	k5eAaImIp3nS	vysílat
televizní	televizní	k2eAgInSc4d1	televizní
program	program	k1gInSc4	program
v	v	k7c6	v
jazykových	jazykový	k2eAgFnPc6d1	jazyková
mutacích	mutace	k1gFnPc6	mutace
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
skupinu	skupina	k1gFnSc4	skupina
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
rétorománské	rétorománský	k2eAgFnSc2d1	rétorománská
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgInPc4d1	rozhlasový
programy	program	k1gInPc4	program
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
rétorománských	rétorománský	k2eAgFnPc2d1	rétorománská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Daniel	Daniel	k1gMnSc1	Daniel
Bernoulli	Bernoulli	kA	Bernoulli
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
hydrodynamiky	hydrodynamika	k1gFnSc2	hydrodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
též	též	k9	též
zakladatelem	zakladatel	k1gMnSc7	zakladatel
slavného	slavný	k2eAgInSc2d1	slavný
matematického	matematický	k2eAgInSc2d1	matematický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
dalším	další	k2eAgMnSc7d1	další
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Jacob	Jacoba	k1gFnPc2	Jacoba
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
například	například	k6eAd1	například
teorii	teorie	k1gFnSc4	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
,	,	kIx,	,
či	či	k8xC	či
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Johann	Johanna	k1gFnPc2	Johanna
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
Leonharda	Leonhard	k1gMnSc2	Leonhard
Eulera	Eulero	k1gNnSc2	Eulero
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejslavnějšího	slavný	k2eAgMnSc2d3	nejslavnější
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
matematika	matematik	k1gMnSc2	matematik
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Eulerovým	Eulerův	k2eAgMnSc7d1	Eulerův
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
byl	být	k5eAaImAgMnS	být
další	další	k2eAgMnSc1d1	další
člen	člen	k1gMnSc1	člen
slavné	slavný	k2eAgFnSc2d1	slavná
rodiny	rodina	k1gFnSc2	rodina
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
matematikům	matematik	k1gMnPc3	matematik
tohoto	tento	k3xDgInSc2	tento
okruhu	okruh	k1gInSc2	okruh
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
Gabriel	Gabriel	k1gMnSc1	Gabriel
Cramer	Cramer	k1gMnSc1	Cramer
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
tzv.	tzv.	kA	tzv.
Cramerova	Cramerův	k2eAgNnSc2d1	Cramerovo
pravidla	pravidlo	k1gNnSc2	pravidlo
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
soustav	soustava	k1gFnPc2	soustava
lineárních	lineární	k2eAgFnPc2d1	lineární
rovnic	rovnice	k1gFnPc2	rovnice
metodou	metoda	k1gFnSc7	metoda
determinantů	determinant	k1gInPc2	determinant
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
původ	původ	k1gInSc1	původ
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
britský	britský	k2eAgMnSc1d1	britský
kvantový	kvantový	k2eAgMnSc1d1	kvantový
fyzik	fyzik	k1gMnSc1	fyzik
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gFnSc1	Dirac
<g/>
.	.	kIx.	.
</s>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
Fritz	Fritz	k1gMnSc1	Fritz
Zwicky	Zwicka	k1gFnSc2	Zwicka
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnSc1	jehož
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Češka	Češka	k1gFnSc1	Češka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
Švýcar	Švýcar	k1gMnSc1	Švýcar
v	v	k7c6	v
norských	norský	k2eAgFnPc6d1	norská
diplomatických	diplomatický	k2eAgFnPc6d1	diplomatická
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Fritz	Fritz	k1gMnSc1	Fritz
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
bulharské	bulharský	k2eAgFnSc6d1	bulharská
Varně	Varna	k1gFnSc6	Varna
<g/>
)	)	kIx)	)
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
průlomovým	průlomový	k2eAgInSc7d1	průlomový
konceptem	koncept	k1gInSc7	koncept
tzv.	tzv.	kA	tzv.
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Auguste	August	k1gMnSc5	August
Piccard	Piccard	k1gMnSc1	Piccard
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
výstupem	výstup	k1gInSc7	výstup
do	do	k7c2	do
stratosféry	stratosféra	k1gFnSc2	stratosféra
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
balónu	balón	k1gInSc2	balón
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jacques	Jacques	k1gMnSc1	Jacques
Piccard	Piccard	k1gMnSc1	Piccard
byl	být	k5eAaImAgMnS	být
známým	známý	k2eAgMnSc7d1	známý
oceánografem	oceánograf	k1gMnSc7	oceánograf
a	a	k8xC	a
vynálezcem	vynálezce	k1gMnSc7	vynálezce
batyskafu	batyskaf	k1gInSc2	batyskaf
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Bertrand	Bertranda	k1gFnPc2	Bertranda
Piccard	Piccard	k1gMnSc1	Piccard
zas	zas	k9	zas
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k8xC	jako
balónový	balónový	k2eAgMnSc1d1	balónový
letec	letec	k1gMnSc1	letec
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
solárního	solární	k2eAgNnSc2d1	solární
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Horace-Bénédict	Horace-Bénédict	k1gInSc1	Horace-Bénédict
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
sluneční	sluneční	k2eAgFnSc4d1	sluneční
pec	pec	k1gFnSc4	pec
<g/>
.	.	kIx.	.
</s>
<s>
Jakob	Jakob	k1gMnSc1	Jakob
Steiner	Steiner	k1gMnSc1	Steiner
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
tzv.	tzv.	kA	tzv.
Steinerovy	Steinerův	k2eAgFnSc2d1	Steinerova
věty	věta	k1gFnSc2	věta
o	o	k7c6	o
momentu	moment	k1gInSc6	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
otáčivého	otáčivý	k2eAgInSc2d1	otáčivý
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Lambert	Lambert	k1gMnSc1	Lambert
mj.	mj.	kA	mj.
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
číslo	číslo	k1gNnSc1	číslo
Pí	pí	k1gNnSc2	pí
je	být	k5eAaImIp3nS	být
iracionální	iracionální	k2eAgMnSc1d1	iracionální
<g/>
,	,	kIx,	,
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
vlhkoměr	vlhkoměr	k1gInSc4	vlhkoměr
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
komety	kometa	k1gFnPc4	kometa
<g/>
,	,	kIx,	,
mlhoviny	mlhovina	k1gFnPc4	mlhovina
či	či	k8xC	či
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
kartografii	kartografie	k1gFnSc4	kartografie
<g/>
.	.	kIx.	.
</s>
<s>
Vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
viditelných	viditelný	k2eAgFnPc2d1	viditelná
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
moderní	moderní	k2eAgNnSc1d1	moderní
pojetí	pojetí	k1gNnSc1	pojetí
atomu	atom	k1gInSc2	atom
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
Johanna	Johann	k1gMnSc2	Johann
Jakoba	Jakob	k1gMnSc2	Jakob
Balmera	Balmer	k1gMnSc2	Balmer
<g/>
.	.	kIx.	.
</s>
<s>
Hodinář	hodinář	k1gMnSc1	hodinář
Joost	Joost	k1gMnSc1	Joost
Bürgi	Bürgi	k1gNnPc2	Bürgi
pracoval	pracovat	k5eAaImAgMnS	pracovat
i	i	k9	i
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
dvoře	dvůr	k1gInSc6	dvůr
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Niklaus	Niklaus	k1gMnSc1	Niklaus
Wirth	Wirth	k1gMnSc1	Wirth
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Pascal	pascal	k1gInSc1	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
astronomem	astronom	k1gMnSc7	astronom
byl	být	k5eAaImAgMnS	být
Paul	Paul	k1gMnSc1	Paul
Guldin	Guldina	k1gFnPc2	Guldina
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
získali	získat	k5eAaPmAgMnP	získat
Charles	Charles	k1gMnSc1	Charles
Edouard	Edouard	k1gMnSc1	Edouard
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Bloch	Bloch	k1gMnSc1	Bloch
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rohrer	Rohrer	k1gMnSc1	Rohrer
a	a	k8xC	a
Karl	Karl	k1gMnSc1	Karl
Alexander	Alexandra	k1gFnPc2	Alexandra
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Paul	Paul	k1gMnSc1	Paul
Karrer	Karrer	k1gMnSc1	Karrer
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Wüthrich	Wüthrich	k1gMnSc1	Wüthrich
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
R.	R.	kA	R.
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
Emil	Emil	k1gMnSc1	Emil
Theodor	Theodora	k1gFnPc2	Theodora
Kocher	Kochra	k1gFnPc2	Kochra
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Hermann	Hermann	k1gMnSc1	Hermann
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Arber	Arber	k1gMnSc1	Arber
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hess	Hess	k1gInSc1	Hess
<g/>
,	,	kIx,	,
Rolf	Rolf	k1gMnSc1	Rolf
Martin	Martin	k1gMnSc1	Martin
Zinkernagel	Zinkernagel	k1gMnSc1	Zinkernagel
a	a	k8xC	a
Edmond	Edmond	k1gMnSc1	Edmond
Henri	Henr	k1gFnSc2	Henr
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
lékařství	lékařství	k1gNnSc2	lékařství
byl	být	k5eAaImAgInS	být
Paracelsus	Paracelsus	k1gInSc1	Paracelsus
<g/>
.	.	kIx.	.
</s>
<s>
Všestranný	všestranný	k2eAgMnSc1d1	všestranný
renesanční	renesanční	k2eAgMnSc1d1	renesanční
učenec	učenec	k1gMnSc1	učenec
Conrad	Conrada	k1gFnPc2	Conrada
Gessner	Gessner	k1gMnSc1	Gessner
založil	založit	k5eAaPmAgMnS	založit
mj.	mj.	kA	mj.
moderní	moderní	k2eAgFnSc4d1	moderní
zoologii	zoologie	k1gFnSc4	zoologie
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriolog	bakteriolog	k1gMnSc1	bakteriolog
Alexandre	Alexandr	k1gInSc5	Alexandr
Yersin	Yersin	k1gInSc1	Yersin
objevil	objevit	k5eAaPmAgMnS	objevit
původce	původce	k1gMnSc4	původce
dýmějového	dýmějový	k2eAgInSc2d1	dýmějový
moru	mor	k1gInSc2	mor
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
objevil	objevit	k5eAaPmAgMnS	objevit
DNA	dno	k1gNnPc4	dno
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
neznal	neznat	k5eAaImAgMnS	neznat
její	její	k3xOp3gFnSc4	její
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
přesnou	přesný	k2eAgFnSc4d1	přesná
roli	role	k1gFnSc4	role
v	v	k7c6	v
dědičnosti	dědičnost	k1gFnSc6	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
objevem	objev	k1gInSc7	objev
halucinogenu	halucinogen	k1gInSc2	halucinogen
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
</s>
<s>
Geolog	geolog	k1gMnSc1	geolog
Louis	Louis	k1gMnSc1	Louis
Agassiz	Agassiz	k1gMnSc1	Agassiz
zavedl	zavést	k5eAaPmAgMnS	zavést
koncept	koncept	k1gInSc4	koncept
tzv.	tzv.	kA	tzv.
doby	doba	k1gFnPc4	doba
ledové	ledový	k2eAgFnPc4d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitní	humanitní	k2eAgFnSc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
svět	svět	k1gInSc4	svět
asi	asi	k9	asi
nejvíce	hodně	k6eAd3	hodně
dílo	dílo	k1gNnSc1	dílo
filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
ženevského	ženevský	k2eAgMnSc2d1	ženevský
rodáka	rodák	k1gMnSc2	rodák
Jeana-Jacquese	Jeana-Jacquese	k1gFnSc2	Jeana-Jacquese
Rousseaua	Rousseau	k1gMnSc2	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
daří	dařit	k5eAaImIp3nS	dařit
psychologii	psychologie	k1gFnSc4	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Piaget	Piaget	k1gMnSc1	Piaget
je	být	k5eAaImIp3nS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
druhé	druhý	k4xOgFnSc2	druhý
nejvlivnější	vlivný	k2eAgFnSc2d3	nejvlivnější
psychologické	psychologický	k2eAgFnSc2d1	psychologická
školy	škola	k1gFnSc2	škola
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
došel	dojít	k5eAaPmAgMnS	dojít
věhlasu	věhlas	k1gInSc3	věhlas
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
propojení	propojení	k1gNnSc3	propojení
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
Eugen	Eugna	k1gFnPc2	Eugna
Bleuler	Bleuler	k1gMnSc1	Bleuler
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
pojmu	pojem	k1gInSc2	pojem
schizofrenie	schizofrenie	k1gFnSc2	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Psycholog	psycholog	k1gMnSc1	psycholog
Hermann	Hermann	k1gMnSc1	Hermann
Rorschach	Rorschach	k1gMnSc1	Rorschach
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
nejznámějšího	známý	k2eAgInSc2d3	nejznámější
projektivního	projektivní	k2eAgInSc2d1	projektivní
testu	test	k1gInSc2	test
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Rorschachových	Rorschachův	k2eAgFnPc2d1	Rorschachova
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
časem	čas	k1gInSc7	čas
staly	stát	k5eAaPmAgFnP	stát
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
symbolem	symbol	k1gInSc7	symbol
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
parodovaným	parodovaný	k2eAgMnPc3d1	parodovaný
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
moderní	moderní	k2eAgFnSc2d1	moderní
pedagogiky	pedagogika	k1gFnSc2	pedagogika
byl	být	k5eAaImAgMnS	být
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Pestalozzi	Pestalozze	k1gFnSc4	Pestalozze
<g/>
.	.	kIx.	.
</s>
<s>
Metodu	metoda	k1gFnSc4	metoda
výuky	výuka	k1gFnSc2	výuka
hudby	hudba	k1gFnSc2	hudba
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pohybu	pohyb	k1gInSc2	pohyb
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Émile	Émile	k1gNnSc4	Émile
Jaques-Dalcroze	Jaques-Dalcroz	k1gInSc5	Jaques-Dalcroz
<g/>
.	.	kIx.	.
</s>
<s>
Slavnou	slavný	k2eAgFnSc4d1	slavná
tezi	teze	k1gFnSc4	teze
o	o	k7c6	o
prapůvodním	prapůvodní	k2eAgInSc6d1	prapůvodní
matriarchátu	matriarchát	k1gInSc6	matriarchát
formuloval	formulovat	k5eAaImAgMnS	formulovat
antropolog	antropolog	k1gMnSc1	antropolog
Johann	Johann	k1gMnSc1	Johann
Jakob	Jakob	k1gMnSc1	Jakob
Bachofen	Bachofno	k1gNnPc2	Bachofno
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
fyziognomií	fyziognomie	k1gFnSc7	fyziognomie
a	a	k8xC	a
charakterem	charakter	k1gInSc7	charakter
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
definovat	definovat	k5eAaBmF	definovat
Johann	Johann	k1gMnSc1	Johann
Kaspar	Kaspar	k1gMnSc1	Kaspar
Lavater	Lavater	k1gMnSc1	Lavater
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
posmrtně	posmrtně	k6eAd1	posmrtně
vydaným	vydaný	k2eAgInSc7d1	vydaný
deníkem	deník	k1gInSc7	deník
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
filozof	filozof	k1gMnSc1	filozof
Henri-Frédéric	Henri-Frédéric	k1gMnSc1	Henri-Frédéric
Amiel	Amiel	k1gMnSc1	Amiel
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Constant	Constant	k1gMnSc1	Constant
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
teoretikem	teoretik	k1gMnSc7	teoretik
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
historiků	historik	k1gMnPc2	historik
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
Jacob	Jacoba	k1gFnPc2	Jacoba
Burckhardt	Burckhardt	k1gInSc1	Burckhardt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
tisícifrankové	tisícifrankový	k2eAgFnSc6d1	tisícifrankový
bankovce	bankovka	k1gFnSc6	bankovka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
byl	být	k5eAaImAgMnS	být
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wölfflin	Wölfflina	k1gFnPc2	Wölfflina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
orientalista	orientalista	k1gMnSc1	orientalista
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Johann	Johann	k1gMnSc1	Johann
Ludwig	Ludwig	k1gMnSc1	Ludwig
Burckhardt	Burckhardt	k1gMnSc1	Burckhardt
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
"	"	kIx"	"
<g/>
záhadologem	záhadolog	k1gMnSc7	záhadolog
<g/>
"	"	kIx"	"
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k9	bezpochyby
Erich	Erich	k1gMnSc1	Erich
von	von	k1gInSc4	von
Däniken	Däniken	k2eAgInSc4d1	Däniken
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dráždí	dráždit	k5eAaImIp3nP	dráždit
vědu	věda	k1gFnSc4	věda
svou	svůj	k3xOyFgFnSc7	svůj
paleoastroanautickou	paleoastroanautický	k2eAgFnSc7d1	paleoastroanautický
tezí	teze	k1gFnSc7	teze
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
vědcům	vědec	k1gMnPc3	vědec
žijícím	žijící	k2eAgNnSc6d1	žijící
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
patřil	patřit	k5eAaImAgMnS	patřit
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
a	a	k8xC	a
občanství	občanství	k1gNnSc4	občanství
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
případem	případ	k1gInSc7	případ
byl	být	k5eAaImAgMnS	být
fyzik	fyzik	k1gMnSc1	fyzik
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paule	k1gFnSc4	Paule
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
Jack	Jack	k1gMnSc1	Jack
Steinberger	Steinberger	k1gMnSc1	Steinberger
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
George	Georg	k1gMnSc2	Georg
Pólya	Pólya	k1gMnSc1	Pólya
či	či	k8xC	či
představitel	představitel	k1gMnSc1	představitel
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
Richard	Richard	k1gMnSc1	Richard
Avenarius	Avenarius	k1gMnSc1	Avenarius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Švýcaři	Švýcar	k1gMnPc1	Švýcar
výrazně	výrazně	k6eAd1	výrazně
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
tenistům	tenista	k1gMnPc3	tenista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
uspěla	uspět	k5eAaPmAgFnS	uspět
Martina	Martina	k1gFnSc1	Martina
Hingisová	Hingisový	k2eAgFnSc1d1	Hingisová
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
vozí	vozit	k5eAaImIp3nP	vozit
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
úspěchy	úspěch	k1gInPc4	úspěch
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
lyžaři	lyžař	k1gMnPc1	lyžař
-	-	kIx~	-
čtyři	čtyři	k4xCgFnPc1	čtyři
zlaté	zlatý	k2eAgFnPc1d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
má	mít	k5eAaImIp3nS	mít
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Simon	Simon	k1gMnSc1	Simon
Ammann	Ammann	k1gMnSc1	Ammann
<g/>
,	,	kIx,	,
gymnasta	gymnasta	k1gMnSc1	gymnasta
Georges	Georges	k1gMnSc1	Georges
Miez	Miez	k1gMnSc1	Miez
<g/>
,	,	kIx,	,
sjezdařka	sjezdařka	k1gFnSc1	sjezdařka
Vreni	Vreň	k1gMnSc6	Vreň
Schneiderová	Schneiderová	k1gFnSc1	Schneiderová
<g/>
,	,	kIx,	,
střelec	střelec	k1gMnSc1	střelec
Konrad	Konrad	k1gInSc4	Konrad
Stäheli	Stähel	k1gInPc7	Stähel
či	či	k8xC	či
běžec	běžec	k1gMnSc1	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Dario	Daria	k1gFnSc5	Daria
Cologna	Cologno	k1gNnSc2	Cologno
<g/>
.	.	kIx.	.
</s>
<s>
Stéphane	Stéphanout	k5eAaPmIp3nS	Stéphanout
Chapuisat	Chapuisat	k1gMnSc4	Chapuisat
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Průkopník	průkopník	k1gMnSc1	průkopník
sportu	sport	k1gInSc2	sport
Joan	Joan	k1gMnSc1	Joan
Gamper	Gamper	k1gMnSc1	Gamper
založil	založit	k5eAaPmAgMnS	založit
kromě	kromě	k7c2	kromě
řady	řada	k1gFnSc2	řada
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
také	také	k9	také
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgFnSc7d1	někdejší
hvězdou	hvězda	k1gFnSc7	hvězda
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
Clay	Clay	k1gInPc4	Clay
Regazzoni	Regazzoň	k1gFnSc3	Regazzoň
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
stáje	stáj	k1gFnSc2	stáj
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Sauber	Sauber	k1gMnSc1	Sauber
je	být	k5eAaImIp3nS	být
Peter	Peter	k1gMnSc1	Peter
Sauber	Sauber	k1gMnSc1	Sauber
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
je	být	k5eAaImIp3nS	být
také	také	k9	také
každoročně	každoročně	k6eAd1	každoročně
jede	jet	k5eAaImIp3nS	jet
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
závod	závod	k1gInSc1	závod
Kolem	kolem	k7c2	kolem
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
(	(	kIx(	(
<g/>
Tour	Tour	k1gInSc1	Tour
de	de	k?	de
Suisse	Suisse	k1gFnSc2	Suisse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
závod	závod	k1gInSc1	závod
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
nabízí	nabízet	k5eAaImIp3nS	nabízet
kopce	kopec	k1gInPc4	kopec
a	a	k8xC	a
také	také	k9	také
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
individuální	individuální	k2eAgFnSc4d1	individuální
časovku	časovka	k1gFnSc4	časovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Roman	Roman	k1gMnSc1	Roman
Kreuziger	Kreuziger	k1gMnSc1	Kreuziger
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
cyklistou	cyklista	k1gMnSc7	cyklista
je	být	k5eAaImIp3nS	být
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
vítěz	vítěz	k1gMnSc1	vítěz
Pasquale	Pasquala	k1gFnSc3	Pasquala
Fornara	Fornara	k1gFnSc1	Fornara
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Critérium	Critérium	k1gNnSc1	Critérium
du	du	k?	du
Dauphiné	Dauphiné	k1gNnSc1	Dauphiné
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc4	závod
přípravou	příprava	k1gFnSc7	příprava
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
