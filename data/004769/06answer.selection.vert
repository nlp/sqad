<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
blízkosti	blízkost	k1gFnSc6	blízkost
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgInSc2	tento
viru	vir	k1gInSc2	vir
doktorem	doktor	k1gMnSc7	doktor
Ngoy	Ngoy	k1gInPc7	Ngoy
Musholaou	Musholaá	k1gFnSc4	Musholaá
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Yambuku	Yambuk	k1gInSc2	Yambuk
(	(	kIx(	(
<g/>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
Ebola-Zair	Ebola-Zair	k1gInSc1	Ebola-Zair
<g/>
,	,	kIx,	,
Ebola-Z	Ebola-Z	k1gFnSc1	Ebola-Z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Nzara	Nzar	k1gInSc2	Nzar
<g/>
,	,	kIx,	,
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Súdánu	Súdán	k1gInSc6	Súdán
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
Ebola-Súdán	Ebola-Súdán	k2eAgInSc1d1	Ebola-Súdán
<g/>
,	,	kIx,	,
Ebola-S	Ebola-S	k1gMnSc1	Ebola-S
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
