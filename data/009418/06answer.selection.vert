<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
i	i	k8xC	i
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Rabíndranáth	Rabíndranáth	k1gMnSc1	Rabíndranáth
Thákur	Thákur	k1gMnSc1	Thákur
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
textu	text	k1gInSc2	text
indické	indický	k2eAgFnSc2d1	indická
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
