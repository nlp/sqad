<p>
<s>
Bengálskou	bengálský	k2eAgFnSc7d1	bengálská
hymnou	hymna	k1gFnSc7	hymna
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc1	píseň
Amar	Amar	k1gMnSc1	Amar
Shonar	Shonar	k1gMnSc1	Shonar
Bangla	Bangla	k1gMnSc1	Bangla
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Mé	můj	k3xOp1gNnSc4	můj
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
Bengálsko	Bengálsko	k1gNnSc4	Bengálsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
i	i	k8xC	i
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Rabíndranáth	Rabíndranáth	k1gMnSc1	Rabíndranáth
Thákur	Thákur	k1gMnSc1	Thákur
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
textu	text	k1gInSc2	text
indické	indický	k2eAgFnSc2d1	indická
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
v	v	k7c6	v
období	období	k1gNnSc6	období
Bangabhanga	Bangabhanga	k1gFnSc1	Bangabhanga
-	-	kIx~	-
rozdělení	rozdělení	k1gNnSc1	rozdělení
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
britskou	britský	k2eAgFnSc7d1	britská
vládou	vláda	k1gFnSc7	vláda
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
podle	podle	k7c2	podle
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
měla	mít	k5eAaImAgFnS	mít
podpořit	podpořit	k5eAaPmF	podpořit
jednotného	jednotný	k2eAgMnSc4d1	jednotný
ducha	duch	k1gMnSc4	duch
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc7d1	státní
hymnou	hymna	k1gFnSc7	hymna
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
je	být	k5eAaImIp3nS	být
prvních	první	k4xOgInPc2	první
deset	deset	k4xCc1	deset
řádků	řádek	k1gInPc2	řádek
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hymnu	hymna	k1gFnSc4	hymna
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlášen	k2eAgNnSc1d1	prohlášeno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
v	v	k7c6	v
bengálštině	bengálština	k1gFnSc6	bengálština
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
Instrumentální	instrumentální	k2eAgInSc4d1	instrumentální
nahrávkaV	nahrávkaV	k?	nahrávkaV
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Amar	Amar	k1gMnSc1	Amar
Shonar	Shonar	k1gMnSc1	Shonar
Bangla	Bangla	k1gMnSc1	Bangla
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
