<s>
Meč	meč	k1gInSc1	meč
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
:	:	kIx,	:
Sword	Sword	k1gInSc1	Sword
and	and	k?	and
sorcery	sorcera	k1gFnSc2	sorcera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
literárního	literární	k2eAgInSc2d1	literární
žánru	žánr	k1gInSc2	žánr
nebo	nebo	k8xC	nebo
podžánru	podžánr	k1gInSc2	podžánr
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zavedl	zavést	k5eAaPmAgMnS	zavést
Fritz	Fritz	k1gMnSc1	Fritz
Leiber	Leiber	k1gMnSc1	Leiber
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příběhy	příběh	k1gInPc4	příběh
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
děj	děj	k1gInSc4	děj
a	a	k8xC	a
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
dílem	díl	k1gInSc7	díl
Roberta	Robert	k1gMnSc2	Robert
E.	E.	kA	E.
Howarda	Howard	k1gMnSc2	Howard
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
s	s	k7c7	s
dobrodružnými	dobrodružný	k2eAgInPc7d1	dobrodružný
příběhy	příběh	k1gInPc7	příběh
barbara	barbar	k1gMnSc2	barbar
Conana	Conan	k1gMnSc2	Conan
<g/>
.	.	kIx.	.
</s>
<s>
Conan	Conan	k1gMnSc1	Conan
<g/>
,	,	kIx,	,
svalnatý	svalnatý	k2eAgMnSc1d1	svalnatý
válečník	válečník	k1gMnSc1	válečník
z	z	k7c2	z
dávné	dávný	k2eAgFnSc2d1	dávná
Cimmerie	Cimmerie	k1gFnSc2	Cimmerie
<g/>
,	,	kIx,	,
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
Conan	Conan	k1gMnSc1	Conan
–	–	k?	–
Hodina	hodina	k1gFnSc1	hodina
draka	drak	k1gMnSc2	drak
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgFnSc7d1	původní
literaturou	literatura	k1gFnSc7	literatura
typu	typ	k1gInSc2	typ
meč	meč	k1gInSc1	meč
&	&	k?	&
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc4	takový
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
následovala	následovat	k5eAaImAgFnS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
můžeme	moct	k5eAaImIp1nP	moct
uvést	uvést	k5eAaPmF	uvést
díla	dílo	k1gNnPc4	dílo
jako	jako	k8xS	jako
Meče	meč	k1gInPc4	meč
proti	proti	k7c3	proti
smrti	smrt	k1gFnSc3	smrt
nebo	nebo	k8xC	nebo
Meče	meč	k1gInPc4	meč
z	z	k7c2	z
Lankhmaru	Lankhmar	k1gInSc2	Lankhmar
od	od	k7c2	od
Fritze	Fritze	k1gFnSc2	Fritze
Leibera	Leibero	k1gNnSc2	Leibero
nebo	nebo	k8xC	nebo
také	také	k9	také
Zloděj	zloděj	k1gMnSc1	zloděj
duší	dušit	k5eAaImIp3nS	dušit
a	a	k8xC	a
Bouřenoš	Bouřenoš	k1gMnSc1	Bouřenoš
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Michael	Michael	k1gMnSc1	Michael
Moorcock	Moorcock	k1gMnSc1	Moorcock
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
žánr	žánr	k1gInSc1	žánr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
kratší	krátký	k2eAgInPc4d2	kratší
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
z	z	k7c2	z
propagace	propagace	k1gFnSc2	propagace
fašismu	fašismus	k1gInSc2	fašismus
<g/>
,	,	kIx,	,
sexismu	sexismus	k1gInSc2	sexismus
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
hříchů	hřích	k1gInPc2	hřích
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc1	kniha
skutečných	skutečný	k2eAgMnPc2d1	skutečný
mistrů	mistr	k1gMnPc2	mistr
žánru	žánr	k1gInSc2	žánr
meč	meč	k1gInSc1	meč
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
hýří	hýřit	k5eAaImIp3nS	hýřit
vtipem	vtip	k1gInSc7	vtip
i	i	k8xC	i
nevšedním	všední	k2eNgInSc7d1	nevšední
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
ponurou	ponurý	k2eAgFnSc4d1	ponurá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Fantasy	fantas	k1gInPc1	fantas
Podžánry	Podžánra	k1gFnSc2	Podžánra
fantasy	fantas	k1gInPc4	fantas
</s>
