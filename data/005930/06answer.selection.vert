<s>
Meč	meč	k1gInSc1	meč
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
:	:	kIx,	:
Sword	Sword	k1gInSc1	Sword
and	and	k?	and
sorcery	sorcera	k1gFnSc2	sorcera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
literárního	literární	k2eAgInSc2d1	literární
žánru	žánr	k1gInSc2	žánr
nebo	nebo	k8xC	nebo
podžánru	podžánr	k1gInSc2	podžánr
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zavedl	zavést	k5eAaPmAgMnS	zavést
Fritz	Fritz	k1gMnSc1	Fritz
Leiber	Leiber	k1gMnSc1	Leiber
počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
