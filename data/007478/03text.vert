<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
vrch	vrch	k1gInSc1	vrch
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
okrese	okres	k1gInSc6	okres
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
Nový	nový	k2eAgInSc1d1	nový
Lískovec	Lískovec	k1gInSc4	Lískovec
a	a	k8xC	a
Kohoutovice	Kohoutovice	k1gFnPc4	Kohoutovice
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
lokalitě	lokalita	k1gFnSc6	lokalita
Kameníky	Kameník	k1gMnPc4	Kameník
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodních	jihovýchodní	k2eAgInPc6d1	jihovýchodní
svazích	svah	k1gInPc6	svah
Kohoutovické	kohoutovický	k2eAgFnSc2d1	Kohoutovická
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
výskyt	výskyt	k1gInSc1	výskyt
koniklece	koniklec	k1gInSc2	koniklec
velkokvětého	velkokvětý	k2eAgInSc2d1	velkokvětý
(	(	kIx(	(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1	Pulsatilla
grandis	grandis	k1gFnSc2	grandis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
největší	veliký	k2eAgFnSc4d3	veliký
populaci	populace	k1gFnSc4	populace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgMnSc1d1	čítající
55	[number]	k4	55
tisíc	tisíc	k4xCgInPc2	tisíc
trsů	trs	k1gInPc2	trs
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
evropsky	evropsky	k6eAd1	evropsky
významnou	významný	k2eAgFnSc7d1	významná
lokalitou	lokalita	k1gFnSc7	lokalita
Kamenný	kamenný	k2eAgInSc1d1	kamenný
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
CZ0624067	CZ0624067	k1gFnSc2	CZ0624067
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
13,775	[number]	k4	13,775
<g/>
2	[number]	k4	2
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
květena	květena	k1gFnSc1	květena
a	a	k8xC	a
také	také	k9	také
výskyt	výskyt	k1gInSc1	výskyt
střevlíka	střevlík	k1gMnSc2	střevlík
uherského	uherský	k2eAgMnSc2d1	uherský
(	(	kIx(	(
<g/>
Carabus	Carabus	k1gInSc1	Carabus
hungaricus	hungaricus	k1gInSc1	hungaricus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
západu	západ	k1gInSc2	západ
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
ulicí	ulice	k1gFnSc7	ulice
Travní	travní	k2eAgFnSc7d1	travní
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
částí	část	k1gFnSc7	část
jsou	být	k5eAaImIp3nP	být
stepní	stepní	k2eAgInPc1d1	stepní
porosty	porost	k1gInPc1	porost
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
okraj	okraj	k1gInSc4	okraj
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
chráněna	chráněn	k2eAgFnSc1d1	chráněna
oplocením	oplocení	k1gNnSc7	oplocení
a	a	k8xC	a
vybavena	vybavit	k5eAaPmNgFnS	vybavit
naučnou	naučný	k2eAgFnSc7d1	naučná
stezkou	stezka	k1gFnSc7	stezka
z	z	k7c2	z
8	[number]	k4	8
panelů	panel	k1gInPc2	panel
<g/>
,	,	kIx,	,
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
především	především	k6eAd1	především
na	na	k7c4	na
dětské	dětský	k2eAgMnPc4d1	dětský
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Péče	péče	k1gFnSc1	péče
o	o	k7c4	o
rezervaci	rezervace	k1gFnSc4	rezervace
spočívá	spočívat	k5eAaImIp3nS	spočívat
kromě	kromě	k7c2	kromě
údržby	údržba	k1gFnSc2	údržba
vybavení	vybavení	k1gNnSc2	vybavení
také	také	k9	také
v	v	k7c6	v
úklidu	úklid	k1gInSc6	úklid
po	po	k7c6	po
návštěvnících	návštěvník	k1gMnPc6	návštěvník
<g/>
,	,	kIx,	,
pravidelném	pravidelný	k2eAgNnSc6d1	pravidelné
kosení	kosení	k1gNnSc6	kosení
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc6	odstraňování
příliš	příliš	k6eAd1	příliš
rozrostlé	rozrostlý	k2eAgFnSc2d1	rozrostlá
nepůvodní	původní	k2eNgFnSc2d1	nepůvodní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rezervaci	rezervace	k1gFnSc4	rezervace
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pečuje	pečovat	k5eAaImIp3nS	pečovat
brněnské	brněnský	k2eAgNnSc1d1	brněnské
sdružení	sdružení	k1gNnSc1	sdružení
Rezekvítek	rezekvítek	k1gInSc1	rezekvítek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
každoročně	každoročně	k6eAd1	každoročně
probíhá	probíhat	k5eAaImIp3nS	probíhat
dobrovolnická	dobrovolnický	k2eAgFnSc1d1	dobrovolnická
brigáda	brigáda	k1gFnSc1	brigáda
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Velká	velký	k2eAgFnSc1d1	velká
kameňáková	kameňákový	k2eAgFnSc1d1	kameňákový
brigáda	brigáda	k1gFnSc1	brigáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
kosení	kosení	k1gNnSc2	kosení
bělokarpatských	bělokarpatský	k2eAgFnPc2d1	bělokarpatská
a	a	k8xC	a
beskydských	beskydský	k2eAgFnPc2d1	Beskydská
luk	louka	k1gFnPc2	louka
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Vrch	vrch	k1gInSc1	vrch
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
usnesením	usnesení	k1gNnSc7	usnesení
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
vyhláška	vyhláška	k1gFnSc1	vyhláška
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1989	[number]	k4	1989
ji	on	k3xPp3gFnSc4	on
určila	určit	k5eAaPmAgFnS	určit
jako	jako	k9	jako
chráněný	chráněný	k2eAgInSc4d1	chráněný
přírodní	přírodní	k2eAgInSc4d1	přírodní
výtvor	výtvor	k1gInSc4	výtvor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
přeřazeno	přeřazen	k2eAgNnSc1d1	přeřazeno
vyhláškou	vyhláška	k1gFnSc7	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ČR	ČR	kA	ČR
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
pod	pod	k7c7	pod
(	(	kIx(	(
<g/>
špatným	špatný	k2eAgInSc7d1	špatný
<g/>
)	)	kIx)	)
názvem	název	k1gInSc7	název
Kamenný	kamenný	k2eAgInSc4d1	kamenný
vrch	vrch	k1gInSc4	vrch
nad	nad	k7c7	nad
Myslivnou	myslivna	k1gFnSc7	myslivna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
nařízením	nařízení	k1gNnSc7	nařízení
vlády	vláda	k1gFnSc2	vláda
132	[number]	k4	132
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
rovněž	rovněž	k9	rovněž
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
evropsky	evropsky	k6eAd1	evropsky
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
Kamenný	kamenný	k2eAgInSc4d1	kamenný
vrch	vrch	k1gInSc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
diority	diorit	k1gInPc1	diorit
brněnského	brněnský	k2eAgInSc2d1	brněnský
masívu	masív	k1gInSc2	masív
<g/>
,	,	kIx,	,
překryté	překrytý	k2eAgInPc1d1	překrytý
mělkými	mělký	k2eAgFnPc7d1	mělká
půdami	půda	k1gFnPc7	půda
kyselé	kyselý	k2eAgFnSc2d1	kyselá
i	i	k8xC	i
nasycené	nasycený	k2eAgFnSc2d1	nasycená
variety	varieta	k1gFnSc2	varieta
kambizemě	kambizemě	k6eAd1	kambizemě
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
koniklece	koniklec	k1gInSc2	koniklec
velkokvětého	velkokvětý	k2eAgInSc2d1	velkokvětý
(	(	kIx(	(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1	Pulsatilla
grandis	grandis	k1gFnSc2	grandis
<g/>
)	)	kIx)	)
a	a	k8xC	a
koniklece	koniklec	k1gInPc1	koniklec
lučního	luční	k2eAgInSc2d1	luční
(	(	kIx(	(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1	Pulsatilla
pratensis	pratensis	k1gInSc1	pratensis
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
bohemica	bohemica	k1gMnSc1	bohemica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
ostřice	ostřice	k1gFnSc1	ostřice
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
humilis	humilis	k1gFnSc2	humilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dominující	dominující	k2eAgInSc1d1	dominující
v	v	k7c6	v
teplomilných	teplomilný	k2eAgInPc6d1	teplomilný
trávnících	trávník	k1gInPc6	trávník
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
černohlávek	černohlávek	k1gInSc1	černohlávek
velkokvětý	velkokvětý	k2eAgInSc1d1	velkokvětý
(	(	kIx(	(
<g/>
Prunella	Prunella	k1gFnSc1	Prunella
grandiflora	grandiflora	k1gFnSc1	grandiflora
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
len	len	k1gInSc1	len
tenkolistý	tenkolistý	k2eAgInSc1d1	tenkolistý
(	(	kIx(	(
<g/>
Linum	Linum	k1gInSc1	Linum
tenuifolium	tenuifolium	k1gNnSc1	tenuifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
len	len	k1gInSc1	len
rakouský	rakouský	k2eAgInSc1d1	rakouský
(	(	kIx(	(
<g/>
Linum	Linum	k1gInSc1	Linum
austriacum	austriacum	k1gInSc1	austriacum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lněnka	lněnka	k1gFnSc1	lněnka
lnolistá	lnolistat	k5eAaImIp3nS	lnolistat
(	(	kIx(	(
<g/>
Thesium	Thesium	k1gNnSc1	Thesium
linophyllon	linophyllon	k1gInSc1	linophyllon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modřenec	modřenec	k1gInSc4	modřenec
chocholatý	chocholatý	k2eAgInSc4d1	chocholatý
(	(	kIx(	(
<g/>
Muscari	Muscar	k1gFnSc2	Muscar
comosum	comosum	k1gInSc1	comosum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sesel	sesel	k1gInSc4	sesel
roční	roční	k2eAgInSc4d1	roční
(	(	kIx(	(
<g/>
Seseli	Sesel	k1gInPc7	Sesel
annuum	annuum	k1gNnSc1	annuum
<g/>
)	)	kIx)	)
či	či	k8xC	či
<g />
.	.	kIx.	.
</s>
<s>
zahořanka	zahořanka	k1gFnSc1	zahořanka
žlutá	žlutý	k2eAgFnSc1d1	žlutá
(	(	kIx(	(
<g/>
Orthantha	Orthantha	k1gFnSc1	Orthantha
lutea	lutea	k1gMnSc1	lutea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
růže	růž	k1gFnSc2	růž
šípkové	šípkový	k2eAgInPc1d1	šípkový
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gFnSc1	Rosa
canina	canina	k1gMnSc1	canina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růž	k1gInPc1	růž
vinné	vinný	k2eAgInPc1d1	vinný
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gFnSc1	Rosa
rubiginosa	rubiginosa	k1gFnSc1	rubiginosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
je	být	k5eAaImIp3nS	být
lokalita	lokalita	k1gFnSc1	lokalita
porostlá	porostlý	k2eAgFnSc1d1	porostlá
borovicí	borovice	k1gFnSc7	borovice
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc4	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovicí	borovice	k1gFnSc7	borovice
černou	černá	k1gFnSc7	černá
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gInSc1	Pinus
nigra	nigr	k1gMnSc2	nigr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modřínem	modřín	k1gInSc7	modřín
opadavým	opadavý	k2eAgInSc7d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
deccidua	deccidu	k1gInSc2	deccidu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dubem	dub	k1gInSc7	dub
zimním	zimní	k2eAgInSc7d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
habrem	habr	k1gInSc7	habr
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Carpinus	Carpinus	k1gInSc1	Carpinus
betulus	betulus	k1gInSc1	betulus
<g/>
)	)	kIx)	)
a	a	k8xC	a
lípou	lípa	k1gFnSc7	lípa
malolistou	malolistý	k2eAgFnSc7d1	malolistá
(	(	kIx(	(
<g/>
Tilia	Tilius	k1gMnSc4	Tilius
cordata	cordat	k1gMnSc4	cordat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
borovic	borovice	k1gFnPc2	borovice
a	a	k8xC	a
modřínů	modřín	k1gInPc2	modřín
na	na	k7c6	na
stepních	stepní	k2eAgFnPc6d1	stepní
částech	část	k1gFnPc6	část
lokality	lokalita	k1gFnSc2	lokalita
mění	měnit	k5eAaImIp3nS	měnit
životní	životní	k2eAgFnSc2d1	životní
podmínky	podmínka	k1gFnSc2	podmínka
a	a	k8xC	a
mnohé	mnohý	k2eAgFnSc2d1	mnohá
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
limitujícím	limitující	k2eAgInSc7d1	limitující
faktorem	faktor	k1gInSc7	faktor
neumí	umět	k5eNaImIp3nS	umět
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
kosení	kosení	k1gNnSc4	kosení
a	a	k8xC	a
probírky	probírka	k1gFnPc4	probírka
v	v	k7c6	v
modříno-borových	modřínoorův	k2eAgInPc6d1	modříno-borův
porostech	porost	k1gInPc6	porost
<g/>
.	.	kIx.	.
</s>
<s>
Dominuje	dominovat	k5eAaImIp3nS	dominovat
teplomilný	teplomilný	k2eAgInSc1d1	teplomilný
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
můra	můra	k1gFnSc1	můra
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
(	(	kIx(	(
<g/>
Perigrapha	Perigrapha	k1gFnSc1	Perigrapha
i-cinctum	iinctum	k1gNnSc4	i-cinctum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesytka	nesytka	k1gFnSc1	nesytka
šťovíková	šťovíkový	k2eAgFnSc1d1	šťovíková
(	(	kIx(	(
<g/>
Pyropteron	Pyropteron	k1gMnSc1	Pyropteron
triannuliformis	triannuliformis	k1gFnSc2	triannuliformis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okáč	okáč	k1gMnSc1	okáč
kostřavový	kostřavový	k2eAgMnSc1d1	kostřavový
(	(	kIx(	(
<g/>
Arethusana	Arethusan	k1gMnSc4	Arethusan
arethusa	arethus	k1gMnSc4	arethus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
brouků	brouk	k1gMnPc2	brouk
krasci	krasec	k1gMnPc1	krasec
Sphenoptera	Sphenopter	k1gMnSc2	Sphenopter
antiqua	antiquus	k1gMnSc2	antiquus
a	a	k8xC	a
S.	S.	kA	S.
substriata	substriata	k1gFnSc1	substriata
<g/>
,	,	kIx,	,
roháč	roháč	k1gInSc1	roháč
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Lucanus	Lucanus	k1gInSc1	Lucanus
<g />
.	.	kIx.	.
</s>
<s>
cervus	cervus	k1gInSc1	cervus
<g/>
)	)	kIx)	)
a	a	k8xC	a
střevlík	střevlík	k1gMnSc1	střevlík
uherský	uherský	k2eAgMnSc1d1	uherský
(	(	kIx(	(
<g/>
Carabus	Carabus	k1gMnSc1	Carabus
hungaricus	hungaricus	k1gMnSc1	hungaricus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plazy	plaz	k1gInPc4	plaz
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc2	agilis
<g/>
)	)	kIx)	)
a	a	k8xC	a
slepýš	slepýš	k1gMnSc1	slepýš
křehký	křehký	k2eAgMnSc1d1	křehký
(	(	kIx(	(
<g/>
Anguis	Anguis	k1gInSc1	Anguis
fragilis	fragilis	k1gFnSc2	fragilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
tu	tu	k6eAd1	tu
krutihlav	krutihlav	k1gMnSc1	krutihlav
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Jynx	Jynx	k1gInSc1	Jynx
torquilla	torquillo	k1gNnSc2	torquillo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lejsek	lejsek	k1gMnSc1	lejsek
šedý	šedý	k2eAgMnSc1d1	šedý
(	(	kIx(	(
<g/>
Muscicapa	Muscicap	k1gMnSc4	Muscicap
striata	striat	k1gMnSc4	striat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěnice	pěnice	k1gFnSc1	pěnice
vlašská	vlašský	k2eAgFnSc1d1	vlašská
(	(	kIx(	(
<g/>
Sylvia	Sylvia	k1gFnSc1	Sylvia
nisoria	nisorium	k1gNnSc2	nisorium
<g/>
)	)	kIx)	)
a	a	k8xC	a
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gMnSc1	Lanius
collurio	collurio	k1gMnSc1	collurio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
