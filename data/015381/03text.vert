<s>
Vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
(	(	kIx(
<g/>
Michle	Michl	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Hanusova	Hanusův	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
40266	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1341	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
v	v	k7c6
Michli	Michli	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
4	#num#	k4
je	být	k5eAaImIp3nS
vodárenský	vodárenský	k2eAgInSc4d1
komplex	komplex	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vybudovalo	vybudovat	k5eAaPmAgNnS
město	město	k1gNnSc1
Vršovice	Vršovice	k1gFnPc1
v	v	k7c6
letech	léto	k1gNnPc6
1906	#num#	k4
až	až	k9
1907	#num#	k4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
svého	svůj	k3xOyFgInSc2
vodárenského	vodárenský	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
nejvýraznější	výrazný	k2eAgFnSc7d3
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
věžový	věžový	k2eAgInSc4d1
vodojem	vodojem	k1gInSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
k	k	k7c3
vodárně	vodárna	k1gFnSc3
patří	patřit	k5eAaImIp3nS
zemní	zemní	k2eAgInSc1d1
vodojem	vodojem	k1gInSc1
<g/>
,	,	kIx,
přečerpávací	přečerpávací	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
a	a	k8xC
obytný	obytný	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stavba	stavba	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
vršovický	vršovický	k2eAgMnSc1d1
či	či	k8xC
michelský	michelský	k2eAgInSc1d1
vodojem	vodojem	k1gInSc1
<g/>
,	,	kIx,
vodárenská	vodárenský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
či	či	k8xC
vodárna	vodárna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
horní	horní	k2eAgFnSc6d1
Michli	Michli	k1gFnSc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
součást	součást	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
Zelené	Zelené	k2eAgFnSc6d1
lišce	liška	k1gFnSc6
v	v	k7c6
těsném	těsný	k2eAgNnSc6d1
sousedství	sousedství	k1gNnSc6
někdejší	někdejší	k2eAgFnSc2d1
Janečkovy	Janečkův	k2eAgFnSc2d1
zbrojovky	zbrojovka	k1gFnSc2
(	(	kIx(
<g/>
JAWA	jawa	k1gFnSc1
Nusle	Nusle	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
pozdější	pozdní	k2eAgFnSc2d2
továrny	továrna	k1gFnSc2
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
motocyklů	motocykl	k1gInPc2
značky	značka	k1gFnSc2
Jawa	jawa	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
ČKD	ČKD	kA
Polovodiče	polovodič	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
ulic	ulice	k1gFnPc2
Hanusova	Hanusův	k2eAgNnSc2d1
a	a	k8xC
Pod	pod	k7c7
vršovickou	vršovický	k2eAgFnSc7d1
vodárnou	vodárna	k1gFnSc7
III	III	kA
<g/>
,	,	kIx,
nedaleko	daleko	k6eNd1
od	od	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Krčí	Krč	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodárenská	vodárenský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
po	po	k7c4
léta	léto	k1gNnPc4
tvořila	tvořit	k5eAaImAgFnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
dominant	dominanta	k1gFnPc2
pražské	pražský	k2eAgFnSc2d1
Pankráce	Pankrác	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
viditelná	viditelný	k2eAgFnSc1d1
z	z	k7c2
mnoha	mnoho	k4c2
vyvýšených	vyvýšený	k2eAgNnPc2d1
míst	místo	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
nepřehlédnutelná	přehlédnutelný	k2eNgFnSc1d1
zejména	zejména	k9
při	při	k7c6
jízdě	jízda	k1gFnSc6
po	po	k7c6
blízké	blízký	k2eAgFnSc6d1
Chodovské	chodovský	k2eAgFnSc6d1
radiále	radiála	k1gFnSc6
při	při	k7c6
jízdě	jízda	k1gFnSc6
směrem	směr	k1gInSc7
k	k	k7c3
dálnici	dálnice	k1gFnSc3
D1	D1	k1gFnSc2
(	(	kIx(
<g/>
po	po	k7c6
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
vodojemu	vodojem	k1gInSc2
na	na	k7c6
Zelené	Zelené	k2eAgFnSc6d1
lišce	liška	k1gFnSc6
tvořil	tvořit	k5eAaImAgInS
celek	celek	k1gInSc1
s	s	k7c7
tzv.	tzv.	kA
vršovickou	vršovický	k2eAgFnSc7d1
vodárnou	vodárna	k1gFnSc7
v	v	k7c6
ulici	ulice	k1gFnSc6
Vltavanů	vltavan	k1gMnPc2
č.	č.	k?
p.	p.	k?
229	#num#	k4
v	v	k7c6
Braníku	Braník	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
jímací	jímací	k2eAgFnPc1d1
studně	studně	k1gFnPc1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektonickou	architektonický	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
všech	všecek	k3xTgFnPc2
budov	budova	k1gFnPc2
vršovické	vršovický	k2eAgFnSc2d1
vodárenské	vodárenský	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
navrhl	navrhnout	k5eAaPmAgMnS
významný	významný	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fasády	fasáda	k1gFnPc1
jsou	být	k5eAaImIp3nP
provedeny	provést	k5eAaPmNgFnP
z	z	k7c2
neomítnutých	omítnutý	k2eNgFnPc2d1
cihel	cihla	k1gFnPc2
<g/>
,	,	kIx,
věžový	věžový	k2eAgInSc1d1
vodojem	vodojem	k1gInSc1
má	mít	k5eAaImIp3nS
měděnou	měděný	k2eAgFnSc4d1
střechu	střecha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
informační	informační	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
je	být	k5eAaImIp3nS
věžový	věžový	k2eAgInSc1d1
vodojem	vodojem	k1gInSc1
označen	označit	k5eAaPmNgInS
adresou	adresa	k1gFnSc7
Na	na	k7c6
schodech	schod	k1gInPc6
125	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
ulice	ulice	k1gFnPc1
Na	na	k7c6
schodech	schod	k1gInPc6
je	být	k5eAaImIp3nS
od	od	k7c2
něj	on	k3xPp3gMnSc2
asi	asi	k9
půl	půl	k1xP
kilometru	kilometr	k1gInSc2
vzdálena	vzdáleno	k1gNnSc2
<g/>
;	;	kIx,
sousedící	sousedící	k2eAgInSc1d1
obytný	obytný	k2eAgInSc1d1
domek	domek	k1gInSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
s	s	k7c7
adresou	adresa	k1gFnSc7
Hanusova	Hanusův	k2eAgNnSc2d1
365	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
domek	domek	k1gInSc1
přečerpávací	přečerpávací	k2eAgFnSc2d1
strojovny	strojovna	k1gFnSc2
vlastní	vlastní	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
realizoval	realizovat	k5eAaBmAgMnS
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pro	pro	k7c4
tehdejší	tehdejší	k2eAgInSc4d1
okres	okres	k1gInSc4
Třeboň	Třeboň	k1gFnSc4
poblíž	poblíž	k7c2
židovského	židovský	k2eAgInSc2d1
hřbitova	hřbitov	k1gInSc2
Na	na	k7c6
Kopečku	kopeček	k1gInSc6
v	v	k7c6
Třeboni	Třeboň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
pozoruhodné	pozoruhodný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
vysoké	vysoký	k2eAgFnPc1d1
60	#num#	k4
m	m	kA
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
stojí	stát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
K	k	k7c3
pražské	pražský	k2eAgFnSc3d1
vodovodní	vodovodní	k2eAgFnSc3d1
síti	síť	k1gFnSc3
byla	být	k5eAaImAgFnS
vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
napojena	napojit	k5eAaPmNgFnS
až	až	k9
roku	rok	k1gInSc2
1927	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jímací	jímací	k2eAgFnPc4d1
studně	studně	k1gFnPc4
v	v	k7c6
Braníku	Braník	k1gInSc6
ležely	ležet	k5eAaImAgFnP
totiž	totiž	k9
v	v	k7c6
záplavové	záplavový	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
a	a	k8xC
po	po	k7c6
záplavách	záplava	k1gFnPc6
se	se	k3xPyFc4
voda	voda	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
stávala	stávat	k5eAaImAgFnS
závadnou	závadný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vršovický	vršovický	k2eAgInSc1d1
vodovodní	vodovodní	k2eAgInSc1d1
systém	systém	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
pravděpodobně	pravděpodobně	k6eAd1
prvním	první	k4xOgInSc7
vodovodním	vodovodní	k2eAgInSc7d1
systémem	systém	k1gInSc7
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
voda	voda	k1gFnSc1
chlorována	chlorován	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
původní	původní	k2eAgFnSc1d1
pražská	pražský	k2eAgFnSc1d1
<g/>
,	,	kIx,
nuselská	nuselský	k2eAgFnSc1d1
(	(	kIx(
<g/>
s	s	k7c7
káranskou	káranský	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
)	)	kIx)
i	i	k9
vinohradská	vinohradský	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
v	v	k7c6
Podolí	Podolí	k1gNnSc6
byly	být	k5eAaImAgInP
zbořeny	zbořen	k2eAgInPc1d1
a	a	k8xC
nahrazeny	nahrazen	k2eAgInPc1d1
dnešní	dnešní	k2eAgFnSc7d1
podolskou	podolský	k2eAgFnSc7d1
vodárnou	vodárna	k1gFnSc7
<g/>
,	,	kIx,
branická	branický	k2eAgFnSc1d1
část	část	k1gFnSc1
původní	původní	k2eAgFnSc2d1
vršovické	vršovický	k2eAgFnSc2d1
vodárny	vodárna	k1gFnSc2
byla	být	k5eAaImAgFnS
až	až	k9
do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
udržována	udržovat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
rezervní	rezervní	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
jako	jako	k8xS,k8xC
sportovně-rekreační	sportovně-rekreační	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
jako	jako	k9
byty	byt	k1gInPc1
<g/>
,	,	kIx,
sklady	sklad	k1gInPc1
a	a	k8xC
dílny	dílna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
sloužil	sloužit	k5eAaImAgInS
vršovický	vršovický	k2eAgInSc1d1
věžový	věžový	k2eAgInSc1d1
vodojem	vodojem	k1gInSc1
v	v	k7c6
Michli	Michli	k1gFnSc6
přívodu	přívod	k1gInSc2
vody	voda	k1gFnSc2
z	z	k7c2
podolské	podolský	k2eAgFnSc2d1
vodárny	vodárna	k1gFnSc2
v	v	k7c6
Podolí	Podolí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
4	#num#	k4
do	do	k7c2
vodárenského	vodárenský	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
(	(	kIx(
<g/>
v	v	k7c6
Praze	Praha	k1gFnSc6
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
Vršovic	Vršovice	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
již	již	k6eAd1
svému	svůj	k3xOyFgInSc3
účelu	účel	k1gInSc3
slouží	sloužit	k5eAaImIp3nS
pouze	pouze	k6eAd1
zdejší	zdejší	k2eAgInSc4d1
podzemní	podzemní	k2eAgInSc4d1
vodojem	vodojem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
obnova	obnova	k1gFnSc1
vnějších	vnější	k2eAgFnPc2d1
fasád	fasáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Domek	domek	k1gInSc1
u	u	k7c2
vodojemu	vodojem	k1gInSc2
</s>
<s>
Portál	portál	k1gInSc1
věžového	věžový	k2eAgInSc2d1
vodojemu	vodojem	k1gInSc2
</s>
<s>
Přečerpávací	přečerpávací	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
v	v	k7c6
Braníku	Braník	k1gInSc6
Archivováno	archivován	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Stavbaweb	Stavbawba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
↑	↑	k?
Vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
v	v	k7c6
Braníku	Braník	k1gInSc6
vstala	vstát	k5eAaPmAgFnS
z	z	k7c2
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1
Staňková	Staňková	k1gFnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Štursa	Štursa	k1gFnSc1
<g/>
,	,	kIx,
Svatopluk	Svatopluk	k1gMnSc1
Voděra	Voděra	k1gFnSc1
<g/>
:	:	kIx,
Pražská	pražský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
významné	významný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
jedenácti	jedenáct	k4xCc2
století	století	k1gNnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-900209-6-8	80-900209-6-8	k4
</s>
<s>
JÁSEK	JÁSEK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražské	pražský	k2eAgFnSc2d1
vodní	vodní	k2eAgFnSc2d1
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
VR	vr	k0
Atelier	atelier	k1gNnPc7
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
104	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
238	#num#	k4
<g/>
-	-	kIx~
<g/>
6478	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
28	#num#	k4
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vršovická	vršovický	k2eAgFnSc1d1
vodárna	vodárna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Společenstvo	společenstvo	k1gNnSc1
vodárenských	vodárenský	k2eAgFnPc2d1
věží	věž	k1gFnPc2
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
-	-	kIx~
Michle	michl	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Robert	Robert	k1gMnSc1
Kořínek	Kořínek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
a	a	k8xC
Mgr.	Mgr.	kA
Petra	Petra	k1gFnSc1
Kořínková	Kořínková	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cestování	cestování	k1gNnSc1
s	s	k7c7
iDnes	iDnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Hrady	hrad	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Atlas	Atlas	k1gInSc1
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
</s>
