<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
littera	litter	k1gMnSc2	litter
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
písemnictví	písemnictví	k1gNnSc4	písemnictví
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
slovesnost	slovesnost	k1gFnSc1	slovesnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
souhrn	souhrn	k1gInSc4	souhrn
všech	všecek	k3xTgInPc2	všecek
písemně	písemně	k6eAd1	písemně
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
definicích	definice	k1gFnPc6	definice
někdy	někdy	k6eAd1	někdy
včetně	včetně	k7c2	včetně
ústně	ústně	k6eAd1	ústně
tradovaných	tradovaný	k2eAgInPc2d1	tradovaný
projevů	projev	k1gInPc2	projev
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ústní	ústní	k2eAgFnSc1d1	ústní
lidová	lidový	k2eAgFnSc1d1	lidová
slovesnost	slovesnost	k1gFnSc1	slovesnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
umělecké	umělecký	k2eAgFnSc2d1	umělecká
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
řadí	řadit	k5eAaImIp3nS	řadit
také	také	k9	také
odborná	odborný	k2eAgFnSc1d1	odborná
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc1d1	náboženský
spisy	spis	k1gInPc1	spis
<g/>
,	,	kIx,	,
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
korespondence	korespondence	k1gFnSc1	korespondence
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
literaturou	literatura	k1gFnSc7	literatura
obvykle	obvykle	k6eAd1	obvykle
myslí	myslet	k5eAaImIp3nS	myslet
pouze	pouze	k6eAd1	pouze
umělecká	umělecký	k2eAgFnSc1d1	umělecká
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
krásná	krásný	k2eAgFnSc1d1	krásná
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
beletrie	beletrie	k1gFnSc1	beletrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
ještě	ještě	k9	ještě
úžeji	úzko	k6eAd2	úzko
význačností	význačnost	k1gFnSc7	význačnost
nebo	nebo	k8xC	nebo
hodnotou	hodnota	k1gFnSc7	hodnota
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
imaginativností	imaginativnost	k1gFnPc2	imaginativnost
či	či	k8xC	či
fikčností	fikčnost	k1gFnPc2	fikčnost
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ozvláštňující	ozvláštňující	k2eAgFnSc7d1	ozvláštňující
funkcí	funkce	k1gFnSc7	funkce
použitého	použitý	k2eAgInSc2d1	použitý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Výpovědi	výpověď	k1gFnPc1	výpověď
předkládané	předkládaný	k2eAgFnPc1d1	Předkládaná
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
literatuře	literatura	k1gFnSc6	literatura
nejsou	být	k5eNaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
coby	coby	k?	coby
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
nebo	nebo	k8xC	nebo
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k9	jako
fikční	fikční	k2eAgFnSc1d1	fikční
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
literární	literární	k2eAgNnSc1d1	literární
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
vysoce	vysoce	k6eAd1	vysoce
komplexní	komplexní	k2eAgFnPc1d1	komplexní
a	a	k8xC	a
rozvětvené	rozvětvený	k2eAgFnPc1d1	rozvětvená
<g/>
,	,	kIx,	,
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
,	,	kIx,	,
významy	význam	k1gInPc7	význam
a	a	k8xC	a
vztahy	vztah	k1gInPc7	vztah
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gNnSc4	on
jednoduše	jednoduše	k6eAd1	jednoduše
uchopit	uchopit	k5eAaPmF	uchopit
<g/>
,	,	kIx,	,
popsat	popsat	k5eAaPmF	popsat
či	či	k8xC	či
redukovat	redukovat	k5eAaBmF	redukovat
<g/>
.	.	kIx.	.
<g/>
Literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
výraznou	výrazný	k2eAgFnSc7d1	výrazná
součástí	součást	k1gFnSc7	součást
lidské	lidský	k2eAgFnSc2d1	lidská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
značný	značný	k2eAgInSc4d1	značný
význam	význam	k1gInSc4	význam
při	při	k7c6	při
konstituci	konstituce	k1gFnSc6	konstituce
lokálních	lokální	k2eAgInPc2d1	lokální
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
národních	národní	k2eAgFnPc2d1	národní
<g/>
)	)	kIx)	)
podob	podoba	k1gFnPc2	podoba
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
šířit	šířit	k5eAaImF	šířit
standardizovaný	standardizovaný	k2eAgInSc4d1	standardizovaný
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
vědomí	vědomí	k1gNnSc4	vědomí
moderních	moderní	k2eAgFnPc2d1	moderní
jazykových	jazykový	k2eAgFnPc2d1	jazyková
a	a	k8xC	a
národních	národní	k2eAgFnPc2d1	národní
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
kosmopolitním	kosmopolitní	k2eAgInSc7d1	kosmopolitní
fenoménem	fenomén	k1gInSc7	fenomén
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
propojený	propojený	k2eAgInSc4d1	propojený
celek	celek	k1gInSc4	celek
–	–	k?	–
autoři	autor	k1gMnPc1	autor
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
bývají	bývat	k5eAaImIp3nP	bývat
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
také	také	k9	také
cizojazyčnými	cizojazyčný	k2eAgInPc7d1	cizojazyčný
texty	text	k1gInPc7	text
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
národními	národní	k2eAgFnPc7d1	národní
literaturami	literatura	k1gFnPc7	literatura
probíhá	probíhat	k5eAaImIp3nS	probíhat
výměna	výměna	k1gFnSc1	výměna
vlivů	vliv	k1gInPc2	vliv
a	a	k8xC	a
idejí	idea	k1gFnPc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
literatury	literatura	k1gFnSc2	literatura
oficiálně	oficiálně	k6eAd1	oficiálně
vychází	vycházet	k5eAaImIp3nS	vycházet
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jsou	být	k5eAaImIp3nP	být
rozšiřována	rozšiřovat	k5eAaImNgFnS	rozšiřovat
samizdatem	samizdat	k1gInSc7	samizdat
nebo	nebo	k8xC	nebo
publikována	publikovat	k5eAaBmNgFnS	publikovat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
literatura	literatura	k1gFnSc1	literatura
tradičně	tradičně	k6eAd1	tradičně
rozlišována	rozlišovat	k5eAaImNgFnS	rozlišovat
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
(	(	kIx(	(
<g/>
kanonickou	kanonický	k2eAgFnSc4d1	kanonická
<g/>
,	,	kIx,	,
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
a	a	k8xC	a
hodnou	hodný	k2eAgFnSc4d1	hodná
pozornosti	pozornost	k1gFnPc4	pozornost
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
populární	populární	k2eAgMnPc4d1	populární
(	(	kIx(	(
<g/>
brakovou	brakový	k2eAgFnSc7d1	braková
<g/>
,	,	kIx,	,
nehodnotnou	hodnotný	k2eNgFnSc7d1	nehodnotná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
postmodernismem	postmodernismus	k1gInSc7	postmodernismus
a	a	k8xC	a
novými	nový	k2eAgInPc7d1	nový
přístupy	přístup	k1gInPc7	přístup
a	a	k8xC	a
proudy	proud	k1gInPc7	proud
v	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
vědě	věda	k1gFnSc6	věda
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
rigidní	rigidní	k2eAgNnSc1d1	rigidní
rozdělení	rozdělení	k1gNnSc1	rozdělení
zpochybněno	zpochybnit	k5eAaPmNgNnS	zpochybnit
a	a	k8xC	a
upouští	upouštět	k5eAaImIp3nS	upouštět
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
také	také	k9	také
populární	populární	k2eAgFnSc1d1	populární
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Literaturou	literatura	k1gFnSc7	literatura
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
tu	ten	k3xDgFnSc4	ten
spadá	spadat	k5eAaPmIp3nS	spadat
literární	literární	k2eAgFnSc1d1	literární
teorie	teorie	k1gFnSc1	teorie
(	(	kIx(	(
<g/>
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
obecné	obecný	k2eAgFnSc3d1	obecná
zákonitosti	zákonitost	k1gFnSc3	zákonitost
literatury	literatura	k1gFnSc2	literatura
jako	jako	k8xS	jako
uměleckého	umělecký	k2eAgInSc2d1	umělecký
jevu	jev	k1gInSc2	jev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
dějiny	dějiny	k1gFnPc4	dějiny
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
a	a	k8xC	a
literární	literární	k2eAgFnSc1d1	literární
kritika	kritika	k1gFnSc1	kritika
(	(	kIx(	(
<g/>
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
a	a	k8xC	a
třídí	třídit	k5eAaImIp3nP	třídit
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
disciplínou	disciplína	k1gFnSc7	disciplína
literární	literární	k2eAgFnSc2d1	literární
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
také	také	k9	také
literární	literární	k2eAgFnSc1d1	literární
komparatistika	komparatistika	k1gFnSc1	komparatistika
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
pouze	pouze	k6eAd1	pouze
komparatistika	komparatistika	k1gFnSc1	komparatistika
či	či	k8xC	či
obecná	obecná	k1gFnSc1	obecná
a	a	k8xC	a
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
literatury	literatura	k1gFnSc2	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
literatura	literatura	k1gFnSc1	literatura
chápán	chápat	k5eAaImNgInS	chápat
pouze	pouze	k6eAd1	pouze
umělecký	umělecký	k2eAgInSc4d1	umělecký
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
čistě	čistě	k6eAd1	čistě
naratologicky	naratologicky	k6eAd1	naratologicky
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
fikce	fikce	k1gFnPc4	fikce
<g/>
,	,	kIx,	,
imaginativní	imaginativní	k2eAgNnSc4d1	imaginativní
<g/>
,	,	kIx,	,
smyšlené	smyšlený	k2eAgNnSc4d1	smyšlené
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Slabinou	slabina	k1gFnSc7	slabina
tohoto	tento	k3xDgNnSc2	tento
pojetí	pojetí	k1gNnSc2	pojetí
je	být	k5eAaImIp3nS	být
však	však	k9	však
existence	existence	k1gFnSc1	existence
nefikčních	fikční	k2eNgNnPc2d1	fikční
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
fikcí	fikce	k1gFnSc7	fikce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
obecně	obecně	k6eAd1	obecně
nepovažují	považovat	k5eNaImIp3nP	považovat
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
také	také	k9	také
děl	dít	k5eAaBmAgMnS	dít
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
fikčnosti	fikčnost	k1gFnSc6	fikčnost
či	či	k8xC	či
historičnosti	historičnost	k1gFnSc6	historičnost
nelze	lze	k6eNd1	lze
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
lingvistické	lingvistický	k2eAgNnSc1d1	lingvistické
vymezení	vymezení	k1gNnSc1	vymezení
<g/>
,	,	kIx,	,
chápání	chápání	k1gNnSc1	chápání
literatury	literatura	k1gFnSc2	literatura
jako	jako	k8xC	jako
textů	text	k1gInPc2	text
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
zacházejících	zacházející	k2eAgMnPc2d1	zacházející
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
textů	text	k1gInPc2	text
obsahující	obsahující	k2eAgFnSc4d1	obsahující
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
intenzifikaci	intenzifikace	k1gFnSc4	intenzifikace
<g/>
,	,	kIx,	,
deformaci	deformace	k1gFnSc3	deformace
a	a	k8xC	a
ozvláštnění	ozvláštnění	k1gNnSc3	ozvláštnění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
zastávali	zastávat	k5eAaImAgMnP	zastávat
formalisté	formalista	k1gMnPc1	formalista
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Šklovskij	Šklovskij	k1gMnSc1	Šklovskij
nebo	nebo	k8xC	nebo
Jakobson	Jakobson	k1gMnSc1	Jakobson
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
nabízí	nabízet	k5eAaImIp3nS	nabízet
námitka	námitka	k1gFnSc1	námitka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ozvláštnění	ozvláštnění	k1gNnSc1	ozvláštnění
a	a	k8xC	a
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
použití	použití	k1gNnSc6	použití
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literárnost	literárnost	k1gFnSc1	literárnost
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
až	až	k9	až
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
<g/>
Vhodným	vhodný	k2eAgNnSc7d1	vhodné
měřítkem	měřítko	k1gNnSc7	měřítko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hodnotový	hodnotový	k2eAgInSc1d1	hodnotový
soud	soud	k1gInSc1	soud
<g/>
:	:	kIx,	:
literární	literární	k2eAgInPc1d1	literární
texty	text	k1gInPc1	text
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádnou	žádný	k3yNgFnSc4	žádný
inherentní	inherentní	k2eAgFnSc4d1	inherentní
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
k	k	k7c3	k
literatuře	literatura	k1gFnSc3	literatura
sami	sám	k3xTgMnPc1	sám
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
texty	text	k1gInPc1	text
mohou	moct	k5eAaImIp3nP	moct
proto	proto	k8xC	proto
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
nebo	nebo	k8xC	nebo
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
společnostech	společnost	k1gFnPc6	společnost
status	status	k1gInSc4	status
literatury	literatura	k1gFnSc2	literatura
získávat	získávat	k5eAaImF	získávat
či	či	k8xC	či
ztrácet	ztrácet	k5eAaImF	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
funkčním	funkční	k2eAgInSc7d1	funkční
pojmem	pojem	k1gInSc7	pojem
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pojmem	pojem	k1gInSc7	pojem
ontologickým	ontologický	k2eAgInSc7d1	ontologický
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
věci	věc	k1gFnSc3	věc
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
rigidním	rigidní	k2eAgNnSc7d1	rigidní
chápáním	chápání	k1gNnSc7	chápání
hodnotných	hodnotný	k2eAgNnPc2d1	hodnotné
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
–	–	k?	–
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
hodnotná	hodnotný	k2eAgNnPc4d1	hodnotné
(	(	kIx(	(
<g/>
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
literární	literární	k2eAgMnSc1d1	literární
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
pouze	pouze	k6eAd1	pouze
vrcholná	vrcholný	k2eAgNnPc1d1	vrcholné
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
díla	dílo	k1gNnPc4	dílo
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
výraznými	výrazný	k2eAgFnPc7d1	výrazná
autorskými	autorský	k2eAgFnPc7d1	autorská
individualitami	individualita	k1gFnPc7	individualita
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
strukturalistického	strukturalistický	k2eAgNnSc2d1	strukturalistické
pojetí	pojetí	k1gNnSc2	pojetí
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
rozlišovacím	rozlišovací	k2eAgInSc7d1	rozlišovací
znakem	znak	k1gInSc7	znak
umělecké	umělecký	k2eAgFnSc2d1	umělecká
literatury	literatura	k1gFnSc2	literatura
její	její	k3xOp3gFnSc2	její
funkce	funkce	k1gFnSc2	funkce
<g/>
:	:	kIx,	:
plní	plnit	k5eAaImIp3nS	plnit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
funkci	funkce	k1gFnSc4	funkce
estetickou	estetický	k2eAgFnSc4d1	estetická
či	či	k8xC	či
poetickou	poetický	k2eAgFnSc4d1	poetická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
sémiotika	sémiotika	k1gFnSc1	sémiotika
a	a	k8xC	a
lingvisty	lingvista	k1gMnSc2	lingvista
Romana	Roman	k1gMnSc2	Roman
Jakobsona	Jakobson	k1gMnSc2	Jakobson
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pojem	pojem	k1gInSc1	pojem
poetické	poetický	k2eAgFnSc2d1	poetická
funkce	funkce	k1gFnSc2	funkce
zavedl	zavést	k5eAaPmAgInS	zavést
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zejména	zejména	k9	zejména
oddělení	oddělení	k1gNnSc1	oddělení
znaku	znak	k1gInSc2	znak
od	od	k7c2	od
jím	jíst	k5eAaImIp1nS	jíst
označovaného	označovaný	k2eAgInSc2d1	označovaný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
pak	pak	k6eAd1	pak
strhává	strhávat	k5eAaImIp3nS	strhávat
znak	znak	k1gInSc1	znak
sám	sám	k3xTgInSc4	sám
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
a	a	k8xC	a
autorefenční	autorefenční	k2eAgFnSc7d1	autorefenční
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poetickou	poetický	k2eAgFnSc4d1	poetická
funkci	funkce	k1gFnSc4	funkce
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
princip	princip	k1gInSc1	princip
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
:	:	kIx,	:
znak	znak	k1gInSc1	znak
není	být	k5eNaImIp3nS	být
mluvčím	mluvčí	k1gMnPc3	mluvčí
vybírán	vybírán	k2eAgInSc4d1	vybírán
bez	bez	k1gInSc4	bez
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
použitým	použitý	k2eAgInPc3d1	použitý
znakům	znak	k1gInPc3	znak
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výstavby	výstavba	k1gFnSc2	výstavba
znakové	znakový	k2eAgFnSc2d1	znaková
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
Jana	Jana	k1gFnSc1	Jana
a	a	k8xC	a
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
preferováno	preferován	k2eAgNnSc1d1	preferováno
před	před	k7c7	před
opačným	opačný	k2eAgNnSc7d1	opačné
pořadím	pořadí	k1gNnSc7	pořadí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
funguje	fungovat	k5eAaImIp3nS	fungovat
princip	princip	k1gInSc1	princip
slabičného	slabičný	k2eAgNnSc2d1	slabičné
narůstání	narůstání	k1gNnSc2	narůstání
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
vytvářeny	vytvářen	k2eAgFnPc4d1	vytvářena
aliterace	aliterace	k1gFnPc4	aliterace
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literárním	literární	k2eAgNnSc6d1	literární
díle	dílo	k1gNnSc6	dílo
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
přítomné	přítomný	k1gMnPc4	přítomný
i	i	k8xC	i
ostatní	ostatní	k2eAgFnPc4d1	ostatní
funkce	funkce	k1gFnPc4	funkce
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
emotivní	emotivní	k2eAgNnPc1d1	emotivní
<g/>
,	,	kIx,	,
poznávací	poznávací	k2eAgNnPc1d1	poznávací
<g/>
,	,	kIx,	,
fatická	fatický	k2eAgNnPc1d1	fatický
<g/>
,	,	kIx,	,
metajazyková	metajazykový	k2eAgNnPc1d1	metajazykové
a	a	k8xC	a
konotativní	konotativní	k2eAgNnPc1d1	konotativní
<g/>
)	)	kIx)	)
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
podřízená	podřízený	k2eAgFnSc1d1	podřízená
<g/>
,	,	kIx,	,
přídatná	přídatný	k2eAgFnSc1d1	přídatná
složka	složka	k1gFnSc1	složka
<g/>
.	.	kIx.	.
</s>
<s>
Epika	epika	k1gFnSc1	epika
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
referenční	referenční	k2eAgFnSc1d1	referenční
funkci	funkce	k1gFnSc4	funkce
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
lyrika	lyrika	k1gFnSc1	lyrika
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
emotivní	emotivní	k2eAgFnSc7d1	emotivní
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
básnictví	básnictví	k1gNnSc6	básnictví
oslovující	oslovující	k2eAgFnSc4d1	oslovující
druhou	druhý	k4xOgFnSc4	druhý
osobu	osoba	k1gFnSc4	osoba
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
pojí	pojíst	k5eAaPmIp3nS	pojíst
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
konotativní	konotativní	k2eAgFnSc7d1	konotativní
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
západním	západní	k2eAgInSc6d1	západní
hodnotovém	hodnotový	k2eAgInSc6d1	hodnotový
a	a	k8xC	a
uměleckém	umělecký	k2eAgInSc6d1	umělecký
diskursu	diskurs	k1gInSc6	diskurs
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
literárním	literární	k2eAgNnSc7d1	literární
dílem	dílo	k1gNnSc7	dílo
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
přítomno	přítomno	k1gNnSc1	přítomno
a	a	k8xC	a
čtenáři	čtenář	k1gMnPc1	čtenář
vnímáno	vnímán	k2eAgNnSc4d1	vnímáno
určité	určitý	k2eAgNnSc4d1	určité
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
kompozice	kompozice	k1gFnPc4	kompozice
a	a	k8xC	a
postupy	postup	k1gInPc4	postup
tradičně	tradičně	k6eAd1	tradičně
přijímané	přijímaný	k2eAgInPc4d1	přijímaný
jako	jako	k8xC	jako
literární	literární	k2eAgInPc4d1	literární
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
tradičním	tradiční	k2eAgInSc7d1	tradiční
postupem	postup	k1gInSc7	postup
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poezie	poezie	k1gFnSc2	poezie
básnickost	básnickost	k1gFnSc4	básnickost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
rytmičnost	rytmičnost	k1gFnSc1	rytmičnost
<g/>
,	,	kIx,	,
metaforičnost	metaforičnost	k1gFnSc1	metaforičnost
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc1d1	grafické
uspořádání	uspořádání	k1gNnSc1	uspořádání
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
prózy	próza	k1gFnSc2	próza
uspořádání	uspořádání	k1gNnSc2	uspořádání
pomocí	pomocí	k7c2	pomocí
syžetového	syžetový	k2eAgNnSc2d1	syžetové
vyprávění	vyprávění	k1gNnSc2	vyprávění
vytvářejícího	vytvářející	k2eAgMnSc2d1	vytvářející
fikční	fikční	k2eAgInSc4d1	fikční
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
postupem	postup	k1gInSc7	postup
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
přímé	přímý	k2eAgNnSc4d1	přímé
označení	označení	k1gNnSc4	označení
textu	text	k1gInSc2	text
za	za	k7c4	za
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
žánrová	žánrový	k2eAgFnSc1d1	žánrová
specifikace	specifikace	k1gFnSc1	specifikace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
postupy	postup	k1gInPc1	postup
ukotvují	ukotvovat	k5eAaImIp3nP	ukotvovat
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
tradici	tradice	k1gFnSc6	tradice
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
text	text	k1gInSc1	text
může	moct	k5eAaImIp3nS	moct
vymezovat	vymezovat	k5eAaImF	vymezovat
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
deformovat	deformovat	k5eAaImF	deformovat
a	a	k8xC	a
aktualizovat	aktualizovat	k5eAaBmF	aktualizovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
vytvářena	vytvářen	k2eAgFnSc1d1	vytvářena
tradice	tradice	k1gFnSc1	tradice
nová	nový	k2eAgFnSc1d1	nová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zkoumání	zkoumání	k1gNnPc4	zkoumání
literatury	literatura	k1gFnSc2	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Odborným	odborný	k2eAgNnSc7d1	odborné
zkoumáním	zkoumání	k1gNnSc7	zkoumání
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
rozdělovaná	rozdělovaný	k2eAgFnSc1d1	rozdělovaná
na	na	k7c4	na
literární	literární	k2eAgFnSc4d1	literární
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc4d1	literární
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
literární	literární	k2eAgFnSc4d1	literární
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
literární	literární	k2eAgFnSc2d1	literární
vědy	věda	k1gFnSc2	věda
existují	existovat	k5eAaImIp3nP	existovat
nebo	nebo	k8xC	nebo
kterých	který	k3yRgMnPc6	který
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
využívá	využívat	k5eAaImIp3nS	využívat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
literární	literární	k2eAgFnPc4d1	literární
interpretace	interpretace	k1gFnPc4	interpretace
<g/>
,	,	kIx,	,
textologie	textologie	k1gFnSc1	textologie
<g/>
,	,	kIx,	,
stylistika	stylistika	k1gFnSc1	stylistika
<g/>
,	,	kIx,	,
naratologie	naratologie	k1gFnSc1	naratologie
<g/>
,	,	kIx,	,
tematologie	tematologie	k1gFnSc1	tematologie
<g/>
,	,	kIx,	,
versologie	versologie	k1gFnSc1	versologie
a	a	k8xC	a
prozódie	prozódie	k1gFnSc1	prozódie
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgInPc4d1	četný
podněty	podnět	k1gInPc4	podnět
přijímá	přijímat	k5eAaImIp3nS	přijímat
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
také	také	k9	také
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc2	teorie
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
genderových	genderův	k2eAgNnPc2d1	genderův
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
queer	quera	k1gFnPc2	quera
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
sémiotiky	sémiotika	k1gFnSc2	sémiotika
<g/>
,	,	kIx,	,
kulturologie	kulturologie	k1gFnSc2	kulturologie
či	či	k8xC	či
estetiky	estetika	k1gFnSc2	estetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
byla	být	k5eAaImAgFnS	být
teoreticky	teoreticky	k6eAd1	teoreticky
reflektována	reflektován	k2eAgFnSc1d1	reflektována
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
antické	antický	k2eAgInPc4d1	antický
spisy	spis	k1gInPc4	spis
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
patří	patřit	k5eAaImIp3nS	patřit
Aristotelova	Aristotelův	k2eAgFnSc1d1	Aristotelova
Poetika	poetika	k1gFnSc1	poetika
nebo	nebo	k8xC	nebo
Longinovo	Longinův	k2eAgNnSc1d1	Longinovo
pojednání	pojednání	k1gNnSc1	pojednání
O	o	k7c6	o
vznešenu	vznešeno	k1gNnSc6	vznešeno
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
literární	literární	k2eAgFnSc2d1	literární
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
však	však	k9	však
spojena	spojit	k5eAaPmNgFnS	spojit
až	až	k9	až
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
filologie	filologie	k1gFnSc2	filologie
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Myšlení	myšlení	k1gNnSc1	myšlení
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
ruský	ruský	k2eAgInSc1d1	ruský
formalismus	formalismus	k1gInSc1	formalismus
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Šklovskij	Šklovskij	k1gMnSc1	Šklovskij
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Tyňanov	Tyňanov	k1gInSc1	Tyňanov
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
Ejchenbaum	Ejchenbaum	k1gInSc1	Ejchenbaum
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
))	))	k?	))
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
navazující	navazující	k2eAgNnSc4d1	navazující
<g />
.	.	kIx.	.
</s>
<s>
strukturalismus	strukturalismus	k1gInSc1	strukturalismus
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
poststrukturalismus	poststrukturalismus	k1gInSc1	poststrukturalismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
sémiotika	sémiotika	k1gFnSc1	sémiotika
(	(	kIx(	(
<g/>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Propp	Propp	k1gMnSc1	Propp
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Lotman	Lotman	k1gMnSc1	Lotman
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roland	Roland	k1gInSc1	Roland
Barthes	Barthes	k1gInSc1	Barthes
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tzvetan	Tzvetan	k1gInSc1	Tzvetan
Todorov	Todorov	k1gInSc1	Todorov
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Julia	Julius	k1gMnSc2	Julius
Kristeva	Kristev	k1gMnSc2	Kristev
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Umberto	Umberta	k1gFnSc5	Umberta
Eco	Eco	k1gMnSc6	Eco
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
však	však	k9	však
vlastním	vlastní	k2eAgNnSc7d1	vlastní
prizmatem	prizma	k1gNnSc7	prizma
a	a	k8xC	a
metodami	metoda	k1gFnPc7	metoda
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
také	také	k9	také
angloamerická	angloamerický	k2eAgFnSc1d1	angloamerická
nová	nový	k2eAgFnSc1d1	nová
kritika	kritika	k1gFnSc1	kritika
<g/>
,	,	kIx,	,
marxistická	marxistický	k2eAgFnSc1d1	marxistická
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
feministická	feministický	k2eAgFnSc1d1	feministická
literární	literární	k2eAgFnSc1d1	literární
teorie	teorie	k1gFnSc1	teorie
nebo	nebo	k8xC	nebo
dekonstruktivismus	dekonstruktivismus	k1gInSc1	dekonstruktivismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
zkoumání	zkoumání	k1gNnSc1	zkoumání
literatury	literatura	k1gFnSc2	literatura
spojeno	spojit	k5eAaPmNgNnS	spojit
především	především	k6eAd1	především
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
kritika	kritik	k1gMnSc2	kritik
Františka	František	k1gMnSc2	František
Xavera	Xaver	k1gMnSc2	Xaver
Šaldy	Šalda	k1gMnSc2	Šalda
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
myšlení	myšlení	k1gNnSc1	myšlení
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
později	pozdě	k6eAd2	pozdě
značně	značně	k6eAd1	značně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
strukturalista	strukturalista	k1gMnSc1	strukturalista
René	René	k1gMnSc1	René
Wellek	Wellek	k1gMnSc1	Wellek
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vlivnou	vlivný	k2eAgFnSc7d1	vlivná
teorií	teorie	k1gFnSc7	teorie
fikčních	fikční	k2eAgInPc2d1	fikční
světů	svět	k1gInPc2	svět
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgMnSc1d1	spojen
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
Lubomír	Lubomír	k1gMnSc1	Lubomír
Doležel	Doležel	k1gMnSc1	Doležel
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
současné	současný	k2eAgMnPc4d1	současný
významné	významný	k2eAgMnPc4d1	významný
literární	literární	k2eAgMnPc4d1	literární
vědce	vědec	k1gMnPc4	vědec
patří	patřit	k5eAaImIp3nS	patřit
Petr	Petr	k1gMnSc1	Petr
A.	A.	kA	A.
Bílek	Bílek	k1gMnSc1	Bílek
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Pelán	Pelán	k1gMnSc1	Pelán
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vojvodík	Vojvodík	k1gMnSc1	Vojvodík
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Vladimír	Vladimír	k1gMnSc1	Vladimír
Papoušek	Papoušek	k1gMnSc1	Papoušek
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglickou	anglický	k2eAgFnSc7d1	anglická
literaturou	literatura	k1gFnSc7	literatura
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
Martin	Martin	k1gMnSc1	Martin
Hilský	Hilský	k1gMnSc1	Hilský
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgFnPc1d1	literární
teorie	teorie	k1gFnPc1	teorie
===	===	k?	===
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnPc1d1	literární
teorie	teorie	k1gFnPc1	teorie
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
o	o	k7c4	o
popis	popis	k1gInSc4	popis
obecných	obecný	k2eAgFnPc2d1	obecná
literárních	literární	k2eAgFnPc2d1	literární
zákonitostí	zákonitost	k1gFnPc2	zákonitost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
problematika	problematika	k1gFnSc1	problematika
geneze	geneze	k1gFnSc2	geneze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
zařazování	zařazování	k1gNnSc6	zařazování
do	do	k7c2	do
literárního	literární	k2eAgInSc2d1	literární
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
čtenářské	čtenářský	k2eAgFnSc2d1	čtenářská
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
posluchačské	posluchačský	k2eAgFnSc6d1	posluchačská
či	či	k8xC	či
divácké	divácký	k2eAgFnSc6d1	divácká
<g/>
)	)	kIx)	)
konkretizaci	konkretizace	k1gFnSc6	konkretizace
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
literární	literární	k2eAgFnPc1d1	literární
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obecné	obecný	k2eAgFnSc2d1	obecná
literární	literární	k2eAgFnSc2d1	literární
teorie	teorie	k1gFnSc2	teorie
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
na	na	k7c4	na
normativní	normativní	k2eAgNnSc4d1	normativní
<g/>
,	,	kIx,	,
deskriptivní	deskriptivní	k2eAgNnSc4d1	deskriptivní
a	a	k8xC	a
empirické	empirický	k2eAgNnSc4d1	empirické
<g/>
.	.	kIx.	.
</s>
<s>
Normativní	normativní	k2eAgFnPc1d1	normativní
teorie	teorie	k1gFnPc1	teorie
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
estetická	estetický	k2eAgNnPc4d1	estetické
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
musejí	muset	k5eAaImIp3nP	muset
texty	text	k1gInPc4	text
splňovat	splňovat	k5eAaImF	splňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
akceptovány	akceptovat	k5eAaBmNgFnP	akceptovat
jako	jako	k9	jako
literární	literární	k2eAgNnPc4d1	literární
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Deskriptivní	deskriptivní	k2eAgFnPc1d1	deskriptivní
teorie	teorie	k1gFnPc1	teorie
naopak	naopak	k6eAd1	naopak
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
stanoveného	stanovený	k2eAgInSc2d1	stanovený
souboru	soubor	k1gInSc2	soubor
textů	text	k1gInPc2	text
(	(	kIx(	(
<g/>
korpusu	korpus	k1gInSc2	korpus
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
se	se	k3xPyFc4	se
vydestilovat	vydestilovat	k5eAaPmF	vydestilovat
jejich	jejich	k3xOp3gInPc4	jejich
společné	společný	k2eAgInPc4d1	společný
a	a	k8xC	a
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
rysy	rys	k1gInPc4	rys
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vytvoření	vytvoření	k1gNnSc2	vytvoření
žánrové	žánrový	k2eAgFnSc2d1	žánrová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Empirické	empirický	k2eAgFnPc1d1	empirická
teorie	teorie	k1gFnPc1	teorie
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
hypotetické	hypotetický	k2eAgFnPc1d1	hypotetická
kauzální	kauzální	k2eAgFnPc1d1	kauzální
souvislosti	souvislost	k1gFnPc1	souvislost
a	a	k8xC	a
zákonitosti	zákonitost	k1gFnPc1	zákonitost
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
následně	následně	k6eAd1	následně
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
textech	text	k1gInPc6	text
ověřují	ověřovat	k5eAaImIp3nP	ověřovat
nebo	nebo	k8xC	nebo
vyvracejí	vyvracet	k5eAaImIp3nP	vyvracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnSc1d1	literární
historie	historie	k1gFnSc1	historie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zaznamenáváním	zaznamenávání	k1gNnSc7	zaznamenávání
a	a	k8xC	a
zkoumáním	zkoumání	k1gNnSc7	zkoumání
relevantních	relevantní	k2eAgFnPc2d1	relevantní
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
publikace	publikace	k1gFnSc1	publikace
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
proměny	proměna	k1gFnSc2	proměna
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnPc4d1	životní
data	datum	k1gNnPc4	datum
autorů	autor	k1gMnPc2	autor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
jejich	jejich	k3xOp3gNnSc7	jejich
zasazováním	zasazování	k1gNnSc7	zasazování
do	do	k7c2	do
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
souvislostí	souvislost	k1gFnPc2	souvislost
a	a	k8xC	a
vytvářením	vytváření	k1gNnSc7	vytváření
systematizujících	systematizující	k2eAgInPc2d1	systematizující
literárněhistorických	literárněhistorický	k2eAgInPc2d1	literárněhistorický
konstruktů	konstrukt	k1gInPc2	konstrukt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
šest	šest	k4xCc1	šest
hlavních	hlavní	k2eAgInPc2d1	hlavní
badatelských	badatelský	k2eAgInPc2d1	badatelský
směrů	směr	k1gInPc2	směr
<g/>
:	:	kIx,	:
<g/>
sociální	sociální	k2eAgFnPc4d1	sociální
dějiny	dějiny	k1gFnPc4	dějiny
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
foucaultovská	foucaultovský	k2eAgFnSc1d1	foucaultovský
diskursivní	diskursivní	k2eAgFnSc1d1	diskursivní
analýza	analýza	k1gFnSc1	analýza
mediální	mediální	k2eAgFnSc2d1	mediální
interpretace	interpretace	k1gFnSc2	interpretace
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
materiálních	materiální	k2eAgNnPc2d1	materiální
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
moderních	moderní	k2eAgNnPc2d1	moderní
masmédií	masmédium	k1gNnPc2	masmédium
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
mocenského	mocenský	k2eAgInSc2d1	mocenský
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
systémově-teoretické	systémověeoretický	k2eAgInPc1d1	systémově-teoretický
přístupy	přístup	k1gInPc1	přístup
</s>
</p>
<p>
<s>
eklektické	eklektický	k2eAgFnPc1d1	eklektická
práce	práce	k1gFnPc1	práce
nového	nový	k2eAgInSc2d1	nový
historismu	historismus	k1gInSc2	historismus
(	(	kIx(	(
<g/>
rozvolněný	rozvolněný	k2eAgInSc1d1	rozvolněný
přístup	přístup	k1gInSc1	přístup
bez	bez	k7c2	bez
hlavní	hlavní	k2eAgFnSc2d1	hlavní
řídící	řídící	k2eAgFnSc2d1	řídící
teorie	teorie	k1gFnSc2	teorie
či	či	k8xC	či
diskursu	diskurs	k1gInSc2	diskurs
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
revizionistické	revizionistický	k2eAgFnPc1d1	revizionistická
studie	studie	k1gFnPc1	studie
například	například	k6eAd1	například
feministické	feministický	k2eAgFnPc1d1	feministická
nebo	nebo	k8xC	nebo
genderovéV	genderovéV	k?	genderovéV
české	český	k2eAgFnSc3d1	Česká
vědě	věda	k1gFnSc3	věda
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dlouho	dlouho	k6eAd1	dlouho
přetrvávalo	přetrvávat	k5eAaImAgNnS	přetrvávat
členění	členění	k1gNnSc4	členění
literární	literární	k2eAgFnSc2d1	literární
historie	historie	k1gFnSc2	historie
na	na	k7c4	na
dějiny	dějiny	k1gFnPc4	dějiny
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
národních	národní	k2eAgFnPc2d1	národní
literatur	literatura	k1gFnPc2	literatura
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
oddělené	oddělený	k2eAgNnSc4d1	oddělené
zkoumání	zkoumání	k1gNnSc4	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
snahy	snaha	k1gFnPc1	snaha
tento	tento	k3xDgInSc4	tento
přístup	přístup	k1gInSc4	přístup
nahradit	nahradit	k5eAaPmF	nahradit
komparativnějšími	komparativný	k2eAgFnPc7d2	komparativný
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
zkoumáním	zkoumání	k1gNnSc7	zkoumání
literárních	literární	k2eAgFnPc2d1	literární
dějin	dějiny	k1gFnPc2	dějiny
v	v	k7c6	v
širších	široký	k2eAgInPc6d2	širší
kulturních	kulturní	k2eAgInPc6d1	kulturní
kontextech	kontext	k1gInPc6	kontext
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
a	a	k8xC	a
žánry	žánr	k1gInPc4	žánr
umělecké	umělecký	k2eAgFnSc2d1	umělecká
literatury	literatura	k1gFnSc2	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
a	a	k8xC	a
žánry	žánr	k1gInPc1	žánr
umělecké	umělecký	k2eAgFnSc2d1	umělecká
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
genologie	genologie	k1gFnSc1	genologie
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
literárních	literární	k2eAgInPc2d1	literární
žánrů	žánr	k1gInPc2	žánr
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
poetiky	poetika	k1gFnSc2	poetika
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
úlohu	úloha	k1gFnSc4	úloha
už	už	k6eAd1	už
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Aristotelově	Aristotelův	k2eAgFnSc6d1	Aristotelova
Poetice	poetika	k1gFnSc6	poetika
už	už	k6eAd1	už
dokonce	dokonce	k9	dokonce
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
určitý	určitý	k2eAgInSc4d1	určitý
zárodek	zárodek	k1gInSc4	zárodek
moderního	moderní	k2eAgNnSc2d1	moderní
členění	členění	k1gNnSc2	členění
literárních	literární	k2eAgInPc2d1	literární
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
básnictví	básnictví	k1gNnSc2	básnictví
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
epické	epický	k2eAgNnSc1d1	epické
<g/>
,	,	kIx,	,
a	a	k8xC	a
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
tragédie	tragédie	k1gFnPc4	tragédie
a	a	k8xC	a
komedie	komedie	k1gFnPc4	komedie
<g/>
.	.	kIx.	.
<g/>
Základní	základní	k2eAgFnPc4d1	základní
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
přijímané	přijímaný	k2eAgNnSc4d1	přijímané
genologické	genologický	k2eAgNnSc4d1	genologický
dělení	dělení	k1gNnSc4	dělení
literatury	literatura	k1gFnSc2	literatura
na	na	k7c4	na
literární	literární	k2eAgInPc4d1	literární
druhy	druh	k1gInPc4	druh
či	či	k8xC	či
oblasti	oblast	k1gFnSc2	oblast
však	však	k9	však
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
v	v	k7c6	v
klasicismu	klasicismus	k1gInSc6	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgInPc1d1	literární
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
výrazu	výraz	k1gInSc2	výraz
na	na	k7c4	na
epiku	epika	k1gFnSc4	epika
(	(	kIx(	(
<g/>
vyprávěcí	vyprávěcí	k2eAgFnSc4d1	vyprávěcí
literaturu	literatura	k1gFnSc4	literatura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyriku	lyrika	k1gFnSc4	lyrika
a	a	k8xC	a
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
textové	textový	k2eAgFnSc2d1	textová
organizace	organizace	k1gFnSc2	organizace
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
prózu	próza	k1gFnSc4	próza
a	a	k8xC	a
drama	drama	k1gNnSc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
žánr	žánr	k1gInSc1	žánr
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
specifickými	specifický	k2eAgNnPc7d1	specifické
kritérii	kritérion	k1gNnPc7	kritérion
definované	definovaný	k2eAgInPc1d1	definovaný
typy	typ	k1gInPc1	typ
literárních	literární	k2eAgInPc2d1	literární
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
žánr	žánr	k1gInSc1	žánr
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnPc4d1	vlastní
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
konvence	konvence	k1gFnPc4	konvence
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
čtenář	čtenář	k1gMnSc1	čtenář
musí	muset	k5eAaImIp3nS	muset
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgInS	dokázat
text	text	k1gInSc4	text
číst	číst	k5eAaImF	číst
správným	správný	k2eAgInSc7d1	správný
způsobem	způsob	k1gInSc7	způsob
–	–	k?	–
například	například	k6eAd1	například
u	u	k7c2	u
detektivky	detektivka	k1gFnSc2	detektivka
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
čtenářova	čtenářův	k2eAgFnSc1d1	čtenářova
obeznámenost	obeznámenost	k1gFnSc1	obeznámenost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
řešení	řešení	k1gNnSc1	řešení
bude	být	k5eAaImBp3nS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jisté	jistý	k2eAgFnSc6d1	jistá
době	doba	k1gFnSc6	doba
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
epické	epický	k2eAgInPc4d1	epický
žánry	žánr	k1gInPc4	žánr
patří	patřit	k5eAaImIp3nS	patřit
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
nebo	nebo	k8xC	nebo
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
žánrové	žánrový	k2eAgFnPc4d1	žánrová
varianty	varianta	k1gFnPc4	varianta
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
románu	román	k1gInSc2	román
jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
milostný	milostný	k2eAgInSc4d1	milostný
román	román	k1gInSc4	román
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
a	a	k8xC	a
žánrové	žánrový	k2eAgFnPc1d1	žánrová
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
uvedený	uvedený	k2eAgInSc1d1	uvedený
román	román	k1gInSc1	román
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
formu	forma	k1gFnSc4	forma
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
,	,	kIx,	,
kroniky	kronika	k1gFnSc2	kronika
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc4	některý
genologické	genologický	k2eAgFnPc4d1	genologický
koncepce	koncepce	k1gFnPc4	koncepce
a	a	k8xC	a
úvahy	úvaha	k1gFnPc4	úvaha
byly	být	k5eAaImAgFnP	být
silně	silně	k6eAd1	silně
ovlivněny	ovlivněn	k2eAgFnPc1d1	ovlivněna
dobou	doba	k1gFnSc7	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Brunetiěre	Brunetiěr	k1gInSc5	Brunetiěr
například	například	k6eAd1	například
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
analogii	analogie	k1gFnSc4	analogie
žánru	žánr	k1gInSc2	žánr
s	s	k7c7	s
biologickým	biologický	k2eAgInSc7d1	biologický
druhem	druh	k1gInSc7	druh
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozkvětem	rozkvět	k1gInSc7	rozkvět
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
darwinovského	darwinovský	k2eAgNnSc2d1	darwinovské
pojetí	pojetí	k1gNnSc2	pojetí
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
genezi	geneze	k1gFnSc6	geneze
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
žánrů	žánr	k1gInPc2	žánr
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
až	až	k9	až
k	k	k7c3	k
nevyhnutelnému	vyhnutelný	k2eNgInSc3d1	nevyhnutelný
postupnému	postupný	k2eAgInSc3d1	postupný
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
podle	podle	k7c2	podle
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
či	či	k8xC	či
kulturního	kulturní	k2eAgInSc2d1	kulturní
okruhu	okruh	k1gInSc2	okruh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Albánská	albánský	k2eAgFnSc1d1	albánská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Belgická	belgický	k2eAgFnSc1d1	belgická
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Estonská	estonský	k2eAgFnSc1d1	Estonská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Finská	finský	k2eAgFnSc1d1	finská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Litevská	litevský	k2eAgFnSc1d1	Litevská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Makedonská	makedonský	k2eAgFnSc1d1	makedonská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Polská	polský	k2eAgFnSc1d1	polská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Slezská	slezský	k2eAgFnSc1d1	Slezská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Srbská	srbský	k2eAgFnSc1d1	Srbská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Amerika	Amerika	k1gFnSc1	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Asie	Asie	k1gFnSc2	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Afrika	Afrika	k1gFnSc1	Afrika
===	===	k?	===
</s>
</p>
<p>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Oceánie	Oceánie	k1gFnSc2	Oceánie
===	===	k?	===
</s>
</p>
<p>
<s>
Australská	australský	k2eAgFnSc1d1	australská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
literaturaMimo	literaturaMimo	k6eAd1	literaturaMimo
geografické	geografický	k2eAgNnSc4d1	geografické
rozčlenění	rozčlenění	k1gNnSc4	rozčlenění
stojí	stát	k5eAaImIp3nS	stát
esperantská	esperantský	k2eAgFnSc1d1	esperantská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
literatura	literatura	k1gFnSc1	literatura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Literatura	literatura	k1gFnSc1	literatura
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
literatura	literatura	k1gFnSc1	literatura
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Literatura	literatura	k1gFnSc1	literatura
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
