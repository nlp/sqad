<p>
<s>
Chlorovodík	chlorovodík	k1gInSc1	chlorovodík
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc1	HCl
<g/>
,	,	kIx,	,
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
chloran	chloran	k1gInSc1	chloran
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sloučenina	sloučenina	k1gFnSc1	sloučenina
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
tvoří	tvořit	k5eAaImIp3nS	tvořit
roztok	roztok	k1gInSc1	roztok
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bezbarvý	bezbarvý	k2eAgInSc4d1	bezbarvý
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
se	s	k7c7	s
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
tvoří	tvořit	k5eAaImIp3nS	tvořit
bílý	bílý	k2eAgInSc1d1	bílý
aerosol	aerosol	k1gInSc1	aerosol
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Chlorovodík	chlorovodík	k1gInSc1	chlorovodík
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgNnPc1d1	chlorovodíkové
jsou	být	k5eAaImIp3nP	být
průmyslově	průmyslově	k6eAd1	průmyslově
významné	významný	k2eAgFnPc1d1	významná
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnPc4d1	sírová
s	s	k7c7	s
chloridem	chlorid	k1gInSc7	chlorid
sodným	sodný	k2eAgInSc7d1	sodný
(	(	kIx(	(
<g/>
kuchyňskou	kuchyňský	k2eAgFnSc7d1	kuchyňská
solí	sůl	k1gFnSc7	sůl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
NaCl	NaCl	k1gInSc1	NaCl
+	+	kIx~	+
H2SO4	H2SO4	k1gFnSc1	H2SO4
→	→	k?	→
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
HClPrůmyslově	HClPrůmyslově	k1gMnSc1	HClPrůmyslově
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
buď	buď	k8xC	buď
reakcí	reakce	k1gFnSc7	reakce
chloru	chlor	k1gInSc2	chlor
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
současně	současně	k6eAd1	současně
s	s	k7c7	s
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
solanky	solanka	k1gFnSc2	solanka
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
též	též	k9	též
odpadním	odpadní	k2eAgInSc7d1	odpadní
produktem	produkt	k1gInSc7	produkt
chlorace	chlorace	k1gFnSc2	chlorace
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
H2	H2	k4	H2
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
2	[number]	k4	2
HCl	HCl	k1gFnPc2	HCl
</s>
</p>
<p>
<s>
==	==	k?	==
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vdechování	vdechování	k1gNnSc6	vdechování
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
i	i	k9	i
při	při	k7c6	při
jiném	jiný	k2eAgInSc6d1	jiný
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
(	(	kIx(	(
<g/>
též	též	k9	též
solná	solný	k2eAgFnSc1d1	solná
<g/>
,	,	kIx,	,
zast.	zast.	k?	zast.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silnou	silný	k2eAgFnSc7d1	silná
minerální	minerální	k2eAgFnSc7d1	minerální
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
plynný	plynný	k2eAgInSc1d1	plynný
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	s	k7c7	s
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
vysoce	vysoce	k6eAd1	vysoce
žíravý	žíravý	k2eAgMnSc1d1	žíravý
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
korozivní	korozivní	k2eAgInSc1d1	korozivní
aerosol	aerosol	k1gInSc1	aerosol
(	(	kIx(	(
<g/>
mikrokapky	mikrokapka	k1gFnSc2	mikrokapka
<g/>
)	)	kIx)	)
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
aerosol	aerosol	k1gInSc1	aerosol
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
sráží	srážet	k5eAaImIp3nS	srážet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
agresivita	agresivita	k1gFnSc1	agresivita
k	k	k7c3	k
živým	živý	k2eAgFnPc3d1	živá
tkáním	tkáň	k1gFnPc3	tkáň
i	i	k8xC	i
jeho	jeho	k3xOp3gInSc1	jeho
korozní	korozní	k2eAgInSc1d1	korozní
potenciál	potenciál	k1gInSc1	potenciál
významně	významně	k6eAd1	významně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
obsahu	obsah	k1gInSc6	obsah
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
</s>
</p>
<p>
<s>
Fluorovodík	fluorovodík	k1gInSc1	fluorovodík
</s>
</p>
<p>
<s>
Bromovodík	bromovodík	k1gInSc1	bromovodík
</s>
</p>
<p>
<s>
Jodovodík	jodovodík	k1gInSc1	jodovodík
</s>
</p>
<p>
<s>
Chloridy	chlorid	k1gInPc1	chlorid
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hydrogen	Hydrogen	k1gInSc1	Hydrogen
chloride	chlorid	k1gInSc5	chlorid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Thames	Thames	k1gInSc1	Thames
&	&	k?	&
Kosmos	kosmos	k1gInSc1	kosmos
Chem	Chem	k1gMnSc1	Chem
C2000	C2000	k1gFnPc2	C2000
Experiment	experiment	k1gInSc1	experiment
Manual	Manual	k1gMnSc1	Manual
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chlorovodík	chlorovodík	k1gInSc1	chlorovodík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
