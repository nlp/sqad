<s>
Chlorovodík	chlorovodík	k1gInSc1	chlorovodík
(	(	kIx(	(
<g/>
HCl	HCl	k1gFnSc1	HCl
<g/>
,	,	kIx,	,
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
chloran	chloran	k1gInSc1	chloran
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sloučenina	sloučenina	k1gFnSc1	sloučenina
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
