<p>
<s>
Panelový	panelový	k2eAgInSc1d1	panelový
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
panelák	panelák	k1gInSc1	panelák
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc4	dům
vybudovaný	vybudovaný	k2eAgInSc4d1	vybudovaný
z	z	k7c2	z
prefabrikovaných	prefabrikovaný	k2eAgInPc2d1	prefabrikovaný
panelů	panel	k1gInPc2	panel
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
říká	říkat	k5eAaImIp3nS	říkat
Prefab	Prefab	k1gInSc1	Prefab
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČSN	ČSN	kA	ČSN
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
stěnový	stěnový	k2eAgInSc1d1	stěnový
systém	systém	k1gInSc1	systém
z	z	k7c2	z
prefabrikovaných	prefabrikovaný	k2eAgInPc2d1	prefabrikovaný
panelů	panel	k1gInPc2	panel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
postaveno	postavit	k5eAaPmNgNnS	postavit
kolem	kolem	k7c2	kolem
80	[number]	k4	80
000	[number]	k4	000
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
s	s	k7c7	s
1,2	[number]	k4	1,2
miliony	milion	k4xCgInPc4	milion
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
bydlely	bydlet	k5eAaImAgInP	bydlet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
na	na	k7c4	na
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
blok	blok	k1gInSc4	blok
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
bloky	blok	k1gInPc1	blok
byly	být	k5eAaImAgInP	být
stavěny	stavit	k5eAaImNgInP	stavit
též	též	k9	též
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
rychlé	rychlý	k2eAgNnSc4d1	rychlé
a	a	k8xC	a
levné	levný	k2eAgNnSc4d1	levné
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
nestavěly	stavět	k5eNaImAgInP	stavět
v	v	k7c6	v
tak	tak	k6eAd1	tak
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
od	od	k7c2	od
jejich	jejich	k3xOp3gFnSc2	jejich
výstavby	výstavba	k1gFnSc2	výstavba
upustila	upustit	k5eAaPmAgFnS	upustit
již	již	k6eAd1	již
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
ale	ale	k9	ale
stavěly	stavět	k5eAaImAgInP	stavět
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
výstavbou	výstavba	k1gFnSc7	výstavba
se	se	k3xPyFc4	se
přestalo	přestat	k5eAaPmAgNnS	přestat
prakticky	prakticky	k6eAd1	prakticky
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
<g/>
.	.	kIx.	.
</s>
<s>
Idea	idea	k1gFnSc1	idea
takto	takto	k6eAd1	takto
stavěného	stavěný	k2eAgInSc2d1	stavěný
domu	dům	k1gInSc2	dům
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
dvojí	dvojit	k5eAaImIp3nS	dvojit
<g/>
:	:	kIx,	:
snížení	snížení	k1gNnSc1	snížení
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
pracnosti	pracnost	k1gFnSc2	pracnost
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
nutností	nutnost	k1gFnSc7	nutnost
rychle	rychle	k6eAd1	rychle
vybudovat	vybudovat	k5eAaPmF	vybudovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
bytů	byt	k1gInPc2	byt
pro	pro	k7c4	pro
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
městskou	městský	k2eAgFnSc4d1	městská
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Československu	Československo	k1gNnSc6	Československo
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
panelové	panelový	k2eAgFnSc2d1	panelová
výstavby	výstavba	k1gFnSc2	výstavba
rok	rok	k1gInSc4	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
vývoj	vývoj	k1gInSc1	vývoj
panelu	panel	k1gInSc2	panel
firmou	firma	k1gFnSc7	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
experimentální	experimentální	k2eAgInSc1d1	experimentální
domek	domek	k1gInSc1	domek
z	z	k7c2	z
litého	litý	k2eAgInSc2d1	litý
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
panelů	panel	k1gInPc2	panel
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
staveništi	staveniště	k1gNnSc6	staveniště
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
prvních	první	k4xOgInPc2	první
panelových	panelový	k2eAgInPc2d1	panelový
domků	domek	k1gInPc2	domek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1940	[number]	k4	1940
a	a	k8xC	a
1941	[number]	k4	1941
se	se	k3xPyFc4	se
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
postavily	postavit	k5eAaPmAgInP	postavit
první	první	k4xOgInSc4	první
dva	dva	k4xCgInPc4	dva
vícepodlažní	vícepodlažní	k2eAgInPc4d1	vícepodlažní
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
přibývat	přibývat	k5eAaImF	přibývat
další	další	k2eAgFnPc1d1	další
pokusné	pokusný	k2eAgFnPc1d1	pokusná
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
nahradit	nahradit	k5eAaPmF	nahradit
pracné	pracný	k2eAgNnSc4d1	pracné
vyzdívání	vyzdívání	k1gNnSc4	vyzdívání
příček	příčka	k1gFnPc2	příčka
a	a	k8xC	a
obvodových	obvodový	k2eAgFnPc2d1	obvodová
stěn	stěna	k1gFnPc2	stěna
cihlami	cihla	k1gFnPc7	cihla
se	se	k3xPyFc4	se
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
začalo	začít	k5eAaPmAgNnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
velkých	velký	k2eAgFnPc2d1	velká
betonových	betonový	k2eAgFnPc2d1	betonová
tvárnic	tvárnice	k1gFnPc2	tvárnice
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
strusky	struska	k1gFnSc2	struska
či	či	k8xC	či
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
montovaných	montovaný	k2eAgInPc2d1	montovaný
dvoupodlažních	dvoupodlažní	k2eAgInPc2d1	dvoupodlažní
domků	domek	k1gInPc2	domek
ze	z	k7c2	z
železobetonových	železobetonový	k2eAgInPc2d1	železobetonový
modulů	modul	k1gInPc2	modul
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
90	[number]	k4	90
×	×	k?	×
270	[number]	k4	270
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadné	snadný	k2eAgNnSc4d1	snadné
omítání	omítání	k1gNnSc4	omítání
měly	mít	k5eAaImAgInP	mít
nejstarší	starý	k2eAgInPc1d3	nejstarší
panely	panel	k1gInPc1	panel
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
povrchu	povrch	k1gInSc6	povrch
vrstvu	vrstva	k1gFnSc4	vrstva
lepenky	lepenka	k1gFnPc1	lepenka
či	či	k8xC	či
minerální	minerální	k2eAgFnPc1d1	minerální
plsti	plst	k1gFnPc1	plst
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
prototypy	prototyp	k1gInPc1	prototyp
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
kupříkladu	kupříkladu	k6eAd1	kupříkladu
izolační	izolační	k2eAgFnSc4d1	izolační
vrstvu	vrstva	k1gFnSc4	vrstva
z	z	k7c2	z
pazdeří	pazdeří	k1gNnSc2	pazdeří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
byly	být	k5eAaImAgInP	být
panely	panel	k1gInPc1	panel
upraveny	upravit	k5eAaPmNgInP	upravit
například	například	k6eAd1	například
vrstvou	vrstva	k1gFnSc7	vrstva
sololitu	sololit	k1gInSc2	sololit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
jedno	jeden	k4xCgNnSc1	jeden
až	až	k9	až
třípodlažních	třípodlažní	k2eAgFnPc6d1	třípodlažní
dvojdomů	dvojdomů	k6eAd1	dvojdomů
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
bylo	být	k5eAaImAgNnS	být
vývojové	vývojový	k2eAgNnSc1d1	vývojové
pracoviště	pracoviště	k1gNnSc1	pracoviště
postupně	postupně	k6eAd1	postupně
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
Ústavu	ústav	k1gInSc2	ústav
montovaných	montovaný	k2eAgFnPc2d1	montovaná
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
vývoj	vývoj	k1gInSc1	vývoj
celopanelového	celopanelový	k2eAgInSc2d1	celopanelový
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
technologii	technologie	k1gFnSc4	technologie
výstavby	výstavba	k1gFnSc2	výstavba
se	se	k3xPyFc4	se
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
zrodil	zrodit	k5eAaPmAgInS	zrodit
i	i	k9	i
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
kovového	kovový	k2eAgInSc2d1	kovový
jeřábu	jeřáb	k1gInSc2	jeřáb
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
na	na	k7c6	na
zlínském	zlínský	k2eAgNnSc6d1	zlínské
nábřeží	nábřeží	k1gNnSc6	nábřeží
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
první	první	k4xOgInSc4	první
prototyp	prototyp	k1gInSc4	prototyp
pětipodlažního	pětipodlažní	k2eAgInSc2d1	pětipodlažní
domu	dům	k1gInSc2	dům
smontovaný	smontovaný	k2eAgInSc4d1	smontovaný
z	z	k7c2	z
celostěnových	celostěnův	k2eAgInPc2d1	celostěnův
panelů	panel	k1gInPc2	panel
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
působících	působící	k2eAgMnPc2d1	působící
architektů	architekt	k1gMnPc2	architekt
H.	H.	kA	H.
Adamce	Adamec	k1gMnSc4	Adamec
a	a	k8xC	a
B.	B.	kA	B.
Kuly	kula	k1gFnPc4	kula
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
4	[number]	k4	4
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
dostal	dostat	k5eAaPmAgInS	dostat
označení	označení	k1gNnSc4	označení
G40	G40	k1gFnSc2	G40
(	(	kIx(	(
<g/>
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
40	[number]	k4	40
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
započala	započnout	k5eAaPmAgFnS	započnout
systémová	systémový	k2eAgFnSc1d1	systémová
výstavba	výstavba	k1gFnSc1	výstavba
systému	systém	k1gInSc2	systém
G40	G40	k1gMnPc2	G40
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
z	z	k7c2	z
panelu	panel	k1gInSc2	panel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pouze	pouze	k6eAd1	pouze
čtyřpodlažní	čtyřpodlažní	k2eAgNnSc1d1	čtyřpodlažní
a	a	k8xC	a
páté	pátý	k4xOgNnSc1	pátý
suterénní	suterénní	k2eAgNnSc1d1	suterénní
podlaží	podlaží	k1gNnSc1	podlaží
(	(	kIx(	(
<g/>
ztužující	ztužující	k2eAgNnSc1d1	ztužující
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
jako	jako	k8xC	jako
monolitické	monolitický	k2eAgFnPc1d1	monolitická
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
vznikaly	vznikat	k5eAaImAgInP	vznikat
i	i	k9	i
další	další	k2eAgInPc1d1	další
systémy	systém	k1gInPc1	systém
označené	označený	k2eAgInPc1d1	označený
G	G	kA	G
<g/>
55	[number]	k4	55
<g/>
,	,	kIx,	,
G57	G57	k1gFnSc1	G57
a	a	k8xC	a
G58	G58	k1gFnSc1	G58
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
již	již	k6eAd1	již
podle	podle	k7c2	podle
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
také	také	k9	také
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
(	(	kIx(	(
<g/>
Gottwaldově	Gottwaldov	k1gInSc6	Gottwaldov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
větší	veliký	k2eAgNnSc1d2	veliký
sídliště	sídliště	k1gNnSc1	sídliště
postavené	postavený	k2eAgNnSc1d1	postavené
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
domů	dům	k1gInPc2	dům
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
na	na	k7c6	na
Zelené	Zelené	k2eAgFnSc6d1	Zelené
lišce	liška	k1gFnSc6	liška
(	(	kIx(	(
<g/>
výstavba	výstavba	k1gFnSc1	výstavba
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specialitou	specialita	k1gFnSc7	specialita
tohoto	tento	k3xDgInSc2	tento
domu	dům	k1gInSc2	dům
byla	být	k5eAaImAgFnS	být
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
ze	z	k7c2	z
silikorku	silikorek	k1gInSc2	silikorek
a	a	k8xC	a
spoje	spoj	k1gInPc1	spoj
mezi	mezi	k7c7	mezi
obvodovými	obvodový	k2eAgInPc7d1	obvodový
panely	panel	k1gInPc7	panel
byly	být	k5eAaImAgInP	být
překrývány	překrývat	k5eAaImNgInP	překrývat
ozdobnými	ozdobný	k2eAgFnPc7d1	ozdobná
lizénami	lizéna	k1gFnPc7	lizéna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
první	první	k4xOgInPc1	první
paneláky	panelák	k1gInPc1	panelák
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
s	s	k7c7	s
"	"	kIx"	"
<g/>
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zdobeny	zdoben	k2eAgInPc1d1	zdoben
různými	různý	k2eAgInPc7d1	různý
detaily	detail	k1gInPc7	detail
<g/>
,	,	kIx,	,
domovními	domovní	k2eAgNnPc7d1	domovní
znameními	znamení	k1gNnPc7	znamení
<g/>
,	,	kIx,	,
arkádovými	arkádový	k2eAgInPc7d1	arkádový
vchody	vchod	k1gInPc7	vchod
<g/>
,	,	kIx,	,
mozaikami	mozaika	k1gFnPc7	mozaika
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tyto	tento	k3xDgInPc1	tento
detaily	detail	k1gInPc1	detail
se	se	k3xPyFc4	se
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
z	z	k7c2	z
našich	náš	k3xOp1gInPc2	náš
paneláků	panelák	k1gInPc2	panelák
vytratily	vytratit	k5eAaPmAgFnP	vytratit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rekordy	rekord	k1gInPc4	rekord
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
panelový	panelový	k2eAgInSc1d1	panelový
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Ubytovna	ubytovna	k1gFnSc1	ubytovna
Kupa	kupa	k1gFnSc1	kupa
<g/>
.	.	kIx.	.
81	[number]	k4	81
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
dům	dům	k1gInSc1	dům
s	s	k7c7	s
23	[number]	k4	23
podlažími	podlaží	k1gNnPc7	podlaží
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Správy	správa	k1gFnSc2	správa
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgInPc2d3	nejdelší
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
Bohnicích	Bohnice	k1gInPc6	Bohnice
v	v	k7c6	v
Zelenohorské	zelenohorský	k2eAgFnSc6d1	Zelenohorská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáctipodlažní	dvanáctipodlažní	k2eAgInSc1d1	dvanáctipodlažní
dům	dům	k1gInSc1	dům
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
se	s	k7c7	s
400	[number]	k4	400
byty	byt	k1gInPc7	byt
a	a	k8xC	a
asi	asi	k9	asi
1	[number]	k4	1
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
18	[number]	k4	18
vchodů	vchod	k1gInPc2	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
chodba	chodba	k1gFnSc1	chodba
<g/>
,	,	kIx,	,
rozvody	rozvod	k1gInPc1	rozvod
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
topení	topení	k1gNnSc2	topení
panelového	panelový	k2eAgInSc2d1	panelový
domu	dům	k1gInSc2	dům
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
o	o	k7c4	o
80	[number]	k4	80
metrů	metr	k1gInPc2	metr
delší	dlouhý	k2eAgInSc1d2	delší
panelový	panelový	k2eAgInSc1d1	panelový
dům	dům	k1gInSc1	dům
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Libereckých	liberecký	k2eAgFnPc6d1	liberecká
Ruprechticích	Ruprechtice	k1gFnPc6	Ruprechtice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
lomený	lomený	k2eAgInSc4d1	lomený
panelový	panelový	k2eAgInSc4d1	panelový
dům	dům	k1gInSc4	dům
přezdívaný	přezdívaný	k2eAgInSc4d1	přezdívaný
Hokejka	hokejka	k1gFnSc1	hokejka
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
tvaru	tvar	k1gInSc3	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
24	[number]	k4	24
vchodů	vchod	k1gInPc2	vchod
<g/>
,	,	kIx,	,
11	[number]	k4	11
pater	patro	k1gNnPc2	patro
a	a	k8xC	a
693	[number]	k4	693
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
===	===	k?	===
</s>
</p>
<p>
<s>
Panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
takřka	takřka	k6eAd1	takřka
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Nahradily	nahradit	k5eAaPmAgFnP	nahradit
tak	tak	k9	tak
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
budované	budovaný	k2eAgInPc1d1	budovaný
cihlové	cihlový	k2eAgInPc1d1	cihlový
domy	dům	k1gInPc1	dům
<g/>
;	;	kIx,	;
následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
výstavbu	výstavba	k1gFnSc4	výstavba
urychlit	urychlit	k5eAaPmF	urychlit
zcela	zcela	k6eAd1	zcela
revolučním	revoluční	k2eAgNnSc7d1	revoluční
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
měst	město	k1gNnPc2	město
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
byla	být	k5eAaImAgFnS	být
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
bydlení	bydlení	k1gNnSc6	bydlení
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
i	i	k9	i
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
stále	stále	k6eAd1	stále
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
poválečná	poválečný	k2eAgFnSc1d1	poválečná
obnova	obnova	k1gFnSc1	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
sídliště	sídliště	k1gNnPc1	sídliště
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
vznikala	vznikat	k5eAaImAgNnP	vznikat
na	na	k7c6	na
zelených	zelená	k1gFnPc6	zelená
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyobrazována	vyobrazovat	k5eAaImNgFnS	vyobrazovat
jako	jako	k8xC	jako
nové	nový	k2eAgNnSc1d1	nové
<g/>
,	,	kIx,	,
idylické	idylický	k2eAgNnSc1d1	idylické
<g/>
,	,	kIx,	,
socialistické	socialistický	k2eAgNnSc1d1	socialistické
bydlení	bydlení	k1gNnSc1	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k9	což
oproti	oproti	k7c3	oproti
starým	starý	k2eAgInPc3d1	starý
domům	dům	k1gInPc3	dům
bez	bez	k7c2	bez
zavedené	zavedený	k2eAgFnSc2d1	zavedená
teplé	teplý	k2eAgFnSc2d1	teplá
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
záchodů	záchod	k1gInPc2	záchod
z	z	k7c2	z
určitého	určitý	k2eAgNnSc2d1	určité
hlediska	hledisko	k1gNnSc2	hledisko
skutečně	skutečně	k6eAd1	skutečně
byla	být	k5eAaImAgNnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
budovala	budovat	k5eAaImAgNnP	budovat
i	i	k9	i
celá	celý	k2eAgNnPc1d1	celé
panelová	panelový	k2eAgNnPc1d1	panelové
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Pripjať	Pripjať	k1gFnPc2	Pripjať
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Most	most	k1gInSc1	most
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
tak	tak	k9	tak
k	k	k7c3	k
radikálním	radikální	k2eAgFnPc3d1	radikální
změnám	změna	k1gFnPc3	změna
celých	celý	k2eAgNnPc2d1	celé
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
různorodou	různorodý	k2eAgFnSc4d1	různorodá
zástavbu	zástavba	k1gFnSc4	zástavba
nyní	nyní	k6eAd1	nyní
nahradila	nahradit	k5eAaPmAgFnS	nahradit
uniformní	uniformní	k2eAgNnPc1d1	uniformní
<g/>
,	,	kIx,	,
standardizovaná	standardizovaný	k2eAgNnPc1d1	standardizované
a	a	k8xC	a
šablonizovaná	šablonizovaný	k2eAgNnPc1d1	šablonizovaný
panelová	panelový	k2eAgNnPc1d1	panelové
sídliště	sídliště	k1gNnPc1	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
samozřejmě	samozřejmě	k6eAd1	samozřejmě
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
negativním	negativní	k2eAgInPc3d1	negativní
jevům	jev	k1gInPc3	jev
jednak	jednak	k8xC	jednak
sociálním	sociální	k2eAgFnPc3d1	sociální
kvůli	kvůli	k7c3	kvůli
rychlému	rychlý	k2eAgNnSc3d1	rychlé
vytváření	vytváření	k1gNnSc3	vytváření
velkých	velký	k2eAgInPc2d1	velký
celků	celek	k1gInPc2	celek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
technickým	technický	k2eAgNnPc3d1	technické
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
kompenzována	kompenzovat	k5eAaBmNgFnS	kompenzovat
nízkou	nízký	k2eAgFnSc7d1	nízká
cenou	cena	k1gFnSc7	cena
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
bez	bez	k7c2	bez
řešení	řešení	k1gNnSc2	řešení
plýtvání	plýtvání	k1gNnSc2	plýtvání
<g/>
,	,	kIx,	,
nekvalitní	kvalitní	k2eNgNnSc1d1	nekvalitní
provedení	provedení	k1gNnSc1	provedení
mnohých	mnohý	k2eAgFnPc2d1	mnohá
součástí	součást	k1gFnPc2	součást
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
brzy	brzy	k6eAd1	brzy
stvořily	stvořit	k5eAaPmAgInP	stvořit
z	z	k7c2	z
domů	dům	k1gInPc2	dům
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
negativně	negativně	k6eAd1	negativně
vnímaných	vnímaný	k2eAgFnPc2d1	vnímaná
ikon	ikona	k1gFnPc2	ikona
socialistického	socialistický	k2eAgInSc2d1	socialistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revolucích	revoluce	k1gFnPc6	revoluce
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
"	"	kIx"	"
<g/>
paneláky	panelák	k1gInPc1	panelák
<g/>
"	"	kIx"	"
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
modernizovat	modernizovat	k5eAaBmF	modernizovat
tisíce	tisíc	k4xCgInPc4	tisíc
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
moderních	moderní	k2eAgInPc2d1	moderní
standardů	standard	k1gInPc2	standard
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgNnPc6d1	mnohé
místech	místo	k1gNnPc6	místo
však	však	k9	však
dochází	docházet	k5eAaImIp3nP	docházet
spíše	spíše	k9	spíše
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgInPc1d1	starý
domy	dům	k1gInPc1	dům
demolovány	demolován	k2eAgInPc1d1	demolován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
případ	případ	k1gInSc1	případ
zbourání	zbourání	k1gNnSc2	zbourání
panelového	panelový	k2eAgInSc2d1	panelový
domu	dům	k1gInSc2	dům
hyzdícího	hyzdící	k2eAgInSc2d1	hyzdící
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
z	z	k7c2	z
estetických	estetický	k2eAgInPc2d1	estetický
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
<g/>
Největším	veliký	k2eAgNnSc7d3	veliký
sídlištěm	sídliště	k1gNnSc7	sídliště
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
Petržalka	Petržalka	k1gFnSc1	Petržalka
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
117	[number]	k4	117
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c4	v
40	[number]	k4	40
000	[number]	k4	000
bytech	byt	k1gInPc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
panelový	panelový	k2eAgInSc1d1	panelový
dům	dům	k1gInSc1	dům
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Západní	západní	k2eAgInSc1d1	západní
blok	blok	k1gInSc1	blok
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
Západního	západní	k2eAgInSc2d1	západní
bloku	blok	k1gInSc2	blok
byly	být	k5eAaImAgInP	být
rovněž	rovněž	k9	rovněž
budovány	budován	k2eAgInPc1d1	budován
panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
ne	ne	k9	ne
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Paneláky	panelák	k1gInPc1	panelák
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
obydlím	obydlí	k1gNnSc7	obydlí
spíše	spíše	k9	spíše
chudších	chudý	k2eAgFnPc2d2	chudší
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Budovány	budován	k2eAgFnPc1d1	budována
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
výstavbu	výstavba	k1gFnSc4	výstavba
sídlišť	sídliště	k1gNnPc2	sídliště
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgFnPc3	jaký
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
Pruitt-Igoe	Pruitt-Igoe	k1gNnSc1	Pruitt-Igoe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
selhání	selhání	k1gNnSc3	selhání
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
ghett	ghetto	k1gNnPc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
západoevropská	západoevropský	k2eAgNnPc1d1	západoevropské
města	město	k1gNnPc1	město
s	s	k7c7	s
panelovými	panelový	k2eAgInPc7d1	panelový
domy	dům	k1gInPc7	dům
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
chudinských	chudinský	k2eAgFnPc2d1	chudinská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
rozvoj	rozvoj	k1gInSc4	rozvoj
zažila	zažít	k5eAaPmAgFnS	zažít
panelová	panelový	k2eAgNnPc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Miljonprogrammet	Miljonprogrammeta	k1gFnPc2	Miljonprogrammeta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
letech	let	k1gInPc6	let
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
650	[number]	k4	650
000	[number]	k4	000
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
v	v	k7c6	v
panelových	panelový	k2eAgInPc6d1	panelový
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
rovněž	rovněž	k9	rovněž
usídlili	usídlit	k5eAaPmAgMnP	usídlit
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
benevolentní	benevolentní	k2eAgFnSc2d1	benevolentní
imigrační	imigrační	k2eAgFnSc2d1	imigrační
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Panelové	panelový	k2eAgInPc4d1	panelový
domy	dům	k1gInPc4	dům
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
podélným	podélný	k2eAgInSc7d1	podélný
nosným	nosný	k2eAgInSc7d1	nosný
systémem	systém	k1gInSc7	systém
</s>
</p>
<p>
<s>
S	s	k7c7	s
příčným	příčný	k2eAgInSc7d1	příčný
nosným	nosný	k2eAgInSc7d1	nosný
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
využíval	využívat	k5eAaPmAgInS	využívat
výrazně	výrazně	k6eAd1	výrazně
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Typy	typ	k1gInPc1	typ
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
běžné	běžný	k2eAgNnSc1d1	běžné
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
typizovaných	typizovaný	k2eAgFnPc2d1	typizovaná
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
soustav	soustava	k1gFnPc2	soustava
panelové	panelový	k2eAgFnSc2d1	panelová
technologie	technologie	k1gFnSc2	technologie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
až	až	k9	až
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
převážně	převážně	k6eAd1	převážně
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnSc2	svůj
"	"	kIx"	"
<g/>
verze	verze	k1gFnSc2	verze
<g/>
"	"	kIx"	"
dle	dle	k7c2	dle
dané	daný	k2eAgFnSc2d1	daná
lokality	lokalita	k1gFnSc2	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Typ	typ	k1gInSc1	typ
T	T	kA	T
0	[number]	k4	0
<g/>
xB	xB	k?	xB
====	====	k?	====
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
příčném	příčný	k2eAgInSc6d1	příčný
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
modulové	modulový	k2eAgFnSc6d1	modulová
skladbě	skladba	k1gFnSc6	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Rozpon	rozpon	k1gInSc1	rozpon
nosných	nosný	k2eAgFnPc2d1	nosná
příčných	příčný	k2eAgFnPc2d1	příčná
stěn	stěna	k1gFnPc2	stěna
je	být	k5eAaImIp3nS	být
3600	[number]	k4	3600
mm	mm	kA	mm
u	u	k7c2	u
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
3600	[number]	k4	3600
a	a	k8xC	a
6000	[number]	k4	6000
mm	mm	kA	mm
u	u	k7c2	u
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
6000	[number]	k4	6000
mm	mm	kA	mm
u	u	k7c2	u
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
B.	B.	kA	B.
U	u	k7c2	u
systému	systém	k1gInSc2	systém
T06B	T06B	k1gFnSc2	T06B
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
dvouramenné	dvouramenný	k2eAgNnSc1d1	dvouramenné
schodiště	schodiště	k1gNnSc1	schodiště
s	s	k7c7	s
mezipodestou	mezipodesta	k1gFnSc7	mezipodesta
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
rameny	rameno	k1gNnPc7	rameno
vložena	vložen	k2eAgFnSc1d1	vložena
výtahová	výtahový	k2eAgFnSc1d1	výtahová
šachta	šachta	k1gFnSc1	šachta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skupiny	skupina	k1gFnSc2	skupina
T08B	T08B	k1gFnSc2	T08B
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
schodiště	schodiště	k1gNnSc1	schodiště
jednoramenné	jednoramenný	k2eAgNnSc1d1	jednoramenné
na	na	k7c4	na
rozpon	rozpon	k1gInSc4	rozpon
6000	[number]	k4	6000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Skladebná	skladebný	k2eAgFnSc1d1	skladebná
výška	výška	k1gFnSc1	výška
podlaží	podlaží	k1gNnSc2	podlaží
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
tj.	tj.	kA	tj.
2800	[number]	k4	2800
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
T07B	T07B	k1gFnPc2	T07B
a	a	k8xC	a
T08B	T08B	k1gFnPc2	T08B
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
předpjaté	předpjatý	k2eAgInPc1d1	předpjatý
dutinové	dutinový	k2eAgInPc1d1	dutinový
stropní	stropní	k2eAgInPc1d1	stropní
panely	panel	k1gInPc1	panel
<g/>
.	.	kIx.	.
</s>
<s>
Obvodový	obvodový	k2eAgInSc1d1	obvodový
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
možný	možný	k2eAgInSc1d1	možný
jen	jen	k9	jen
z	z	k7c2	z
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Typ	typ	k1gInSc1	typ
VVÚ-ETA	VVÚ-ETA	k1gFnSc2	VVÚ-ETA
====	====	k?	====
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
a	a	k8xC	a
vývojový	vývojový	k2eAgInSc1d1	vývojový
ústav	ústav	k1gInSc1	ústav
Stavebních	stavební	k2eAgInPc2d1	stavební
závodů	závod	k1gInPc2	závod
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
příčný	příčný	k2eAgInSc1d1	příčný
stěnový	stěnový	k2eAgInSc1d1	stěnový
systém	systém	k1gInSc1	systém
s	s	k7c7	s
rozponem	rozpon	k1gInSc7	rozpon
stěn	stěn	k1gInSc4	stěn
3000	[number]	k4	3000
nebo	nebo	k8xC	nebo
6000	[number]	k4	6000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Skladebná	skladebný	k2eAgFnSc1d1	skladebná
výška	výška	k1gFnSc1	výška
podlaží	podlaží	k1gNnSc2	podlaží
je	být	k5eAaImIp3nS	být
2800	[number]	k4	2800
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Stropní	stropní	k2eAgInPc1d1	stropní
panely	panel	k1gInPc1	panel
jsou	být	k5eAaImIp3nP	být
nepředpjaté	předpjatý	k2eNgFnPc1d1	předpjatý
<g/>
,	,	kIx,	,
vylehčené	vylehčený	k2eAgFnPc1d1	vylehčená
dutinami	dutina	k1gFnPc7	dutina
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tloušťku	tloušťka	k1gFnSc4	tloušťka
190	[number]	k4	190
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
stropní	stropní	k2eAgInPc1d1	stropní
panely	panel	k1gInPc1	panel
předpjaté	předpjatý	k2eAgInPc1d1	předpjatý
<g/>
.	.	kIx.	.
</s>
<s>
Obvodový	obvodový	k2eAgInSc1d1	obvodový
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
parapetními	parapetní	k2eAgInPc7d1	parapetní
panely	panel	k1gInPc7	panel
a	a	k8xC	a
meziokenními	meziokenní	k2eAgFnPc7d1	meziokenní
vložkami	vložka	k1gFnPc7	vložka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
obvodových	obvodový	k2eAgInPc2d1	obvodový
celostěnových	celostěnův	k2eAgInPc2d1	celostěnův
panelů	panel	k1gInPc2	panel
s	s	k7c7	s
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Lodžiové	lodžiový	k2eAgInPc1d1	lodžiový
panely	panel	k1gInPc1	panel
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
ŽB	ŽB	kA	ŽB
sendvičové	sendvičový	k2eAgNnSc1d1	sendvičové
<g/>
.	.	kIx.	.
</s>
<s>
Schodiště	schodiště	k1gNnPc1	schodiště
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
jednoramenná	jednoramenný	k2eAgNnPc1d1	jednoramenné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
dvouramenná	dvouramenný	k2eAgFnSc1d1	dvouramenná
s	s	k7c7	s
mezipodestou	mezipodesta	k1gFnSc7	mezipodesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Typ	typ	k1gInSc1	typ
P	P	kA	P
<g/>
1.11	[number]	k4	1.11
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejnovější	nový	k2eAgInSc4d3	nejnovější
typ	typ	k1gInSc4	typ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
dvanácti	dvanáct	k4xCc2	dvanáct
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
příčný	příčný	k2eAgInSc4d1	příčný
systém	systém	k1gInSc4	systém
o	o	k7c6	o
rozponech	rozpon	k1gInPc6	rozpon
2400	[number]	k4	2400
<g/>
,	,	kIx,	,
3000	[number]	k4	3000
a	a	k8xC	a
4200	[number]	k4	4200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Skladebná	skladebný	k2eAgFnSc1d1	skladebná
výška	výška	k1gFnSc1	výška
podlaží	podlaží	k1gNnSc2	podlaží
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
2800	[number]	k4	2800
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
stěnové	stěnový	k2eAgInPc1d1	stěnový
panely	panel	k1gInPc1	panel
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgInPc1d1	plný
nebo	nebo	k8xC	nebo
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Stropní	stropní	k2eAgInPc1d1	stropní
panely	panel	k1gInPc1	panel
jsou	být	k5eAaImIp3nP	být
dutinové	dutinový	k2eAgInPc1d1	dutinový
<g/>
.	.	kIx.	.
</s>
<s>
Schodiště	schodiště	k1gNnSc1	schodiště
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
dvouramenné	dvouramenný	k2eAgNnSc1d1	dvouramenné
s	s	k7c7	s
mezipodestou	mezipodesta	k1gFnSc7	mezipodesta
<g/>
.	.	kIx.	.
</s>
<s>
Obvodový	obvodový	k2eAgInSc1d1	obvodový
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
sendvičových	sendvičový	k2eAgInPc2d1	sendvičový
celostěnových	celostěnův	k2eAgInPc2d1	celostěnův
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Lodžiové	lodžiový	k2eAgInPc1d1	lodžiový
panely	panel	k1gInPc1	panel
jsou	být	k5eAaImIp3nP	být
panelové	panelový	k2eAgInPc1d1	panelový
nebo	nebo	k8xC	nebo
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgInSc1d1	stavební
postup	postup	k1gInSc1	postup
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
technické	technický	k2eAgNnSc1d1	technické
řešení	řešení	k1gNnSc1	řešení
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
systémy	systém	k1gInPc4	systém
obdobné	obdobný	k2eAgInPc4d1	obdobný
<g/>
.	.	kIx.	.
</s>
<s>
Napřed	napřed	k6eAd1	napřed
se	se	k3xPyFc4	se
vybudují	vybudovat	k5eAaPmIp3nP	vybudovat
základy	základ	k1gInPc1	základ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
monolitické	monolitický	k2eAgInPc1d1	monolitický
či	či	k8xC	či
polomontované	polomontovaný	k2eAgInPc1d1	polomontovaný
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
založení	založení	k1gNnSc2	založení
je	být	k5eAaImIp3nS	být
plošný	plošný	k2eAgInSc1d1	plošný
nebo	nebo	k8xC	nebo
hlubinný	hlubinný	k2eAgInSc1d1	hlubinný
(	(	kIx(	(
<g/>
piloty	pilota	k1gFnPc1	pilota
<g/>
,	,	kIx,	,
studny	studna	k1gFnPc1	studna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
základových	základový	k2eAgInPc6d1	základový
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
budoucí	budoucí	k2eAgFnSc1d1	budoucí
stavba	stavba	k1gFnSc1	stavba
opatří	opatřit	k5eAaPmIp3nS	opatřit
rozvodem	rozvod	k1gInSc7	rozvod
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
,	,	kIx,	,
vodovod	vodovod	k1gInSc1	vodovod
<g/>
,	,	kIx,	,
plynové	plynový	k2eAgInPc1d1	plynový
a	a	k8xC	a
elektrické	elektrický	k2eAgInPc1d1	elektrický
rozvody	rozvod	k1gInPc1	rozvod
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
montují	montovat	k5eAaImIp3nP	montovat
nosné	nosný	k2eAgFnPc1d1	nosná
stěny	stěna	k1gFnPc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
montáži	montáž	k1gFnSc6	montáž
stěn	stěna	k1gFnPc2	stěna
se	se	k3xPyFc4	se
osadí	osadit	k5eAaPmIp3nP	osadit
příčky	příčka	k1gFnPc1	příčka
a	a	k8xC	a
instalační	instalační	k2eAgNnPc1d1	instalační
bytová	bytový	k2eAgNnPc1d1	bytové
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zastropí	zastropit	k5eAaPmIp3nP	zastropit
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
podlaží	podlaží	k1gNnPc1	podlaží
<g/>
,	,	kIx,	,
osadí	osadit	k5eAaPmIp3nP	osadit
se	se	k3xPyFc4	se
prefabrikovaná	prefabrikovaný	k2eAgNnPc1d1	prefabrikované
schodišťová	schodišťový	k2eAgNnPc1d1	schodišťové
ramena	rameno	k1gNnPc1	rameno
a	a	k8xC	a
mezipodesty	mezipodest	k1gInPc1	mezipodest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastropení	zastropení	k1gNnSc6	zastropení
posledního	poslední	k2eAgNnSc2d1	poslední
podlaží	podlaží	k1gNnSc2	podlaží
se	se	k3xPyFc4	se
osadí	osadit	k5eAaPmIp3nS	osadit
obvodový	obvodový	k2eAgInSc1d1	obvodový
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ze	z	k7c2	z
sendvičových	sendvičový	k2eAgInPc2d1	sendvičový
panelů	panel	k1gInPc2	panel
nebo	nebo	k8xC	nebo
z	z	k7c2	z
lehkých	lehký	k2eAgInPc2d1	lehký
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hliníková	hliníkový	k2eAgNnPc1d1	hliníkové
okna	okno	k1gNnPc1	okno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
hrubé	hrubý	k2eAgFnSc2d1	hrubá
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
provedou	provést	k5eAaPmIp3nP	provést
elektro	elektro	k6eAd1	elektro
a	a	k8xC	a
vodo	voda	k1gFnSc5	voda
instalace	instalace	k1gFnSc1	instalace
a	a	k8xC	a
udělá	udělat	k5eAaPmIp3nS	udělat
se	se	k3xPyFc4	se
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
osadí	osadit	k5eAaPmIp3nP	osadit
trvalé	trvalý	k2eAgInPc1d1	trvalý
doplňky	doplněk	k1gInPc1	doplněk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
linka	linka	k1gFnSc1	linka
<g/>
)	)	kIx)	)
a	a	k8xC	a
provedou	provést	k5eAaPmIp3nP	provést
se	se	k3xPyFc4	se
případné	případný	k2eAgFnPc1d1	případná
povrchové	povrchový	k2eAgFnPc1d1	povrchová
úpravy	úprava	k1gFnPc1	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
zastřešeny	zastřešit	k5eAaPmNgInP	zastřešit
vždy	vždy	k6eAd1	vždy
plochou	plochý	k2eAgFnSc7d1	plochá
střechou	střecha	k1gFnSc7	střecha
dvouplášťovou	dvouplášťový	k2eAgFnSc7d1	dvouplášťová
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
statického	statický	k2eAgNnSc2d1	statické
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
několikapodlažní	několikapodlažní	k2eAgInSc4d1	několikapodlažní
rám	rám	k1gInSc4	rám
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
horizontální	horizontální	k2eAgFnSc3d1	horizontální
stabilitě	stabilita	k1gFnSc3	stabilita
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
ztužen	ztužen	k2eAgMnSc1d1	ztužen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příčného	příčný	k2eAgInSc2d1	příčný
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
ztužen	ztužit	k5eAaPmNgInS	ztužit
podélnou	podélný	k2eAgFnSc7d1	podélná
stěnou	stěna	k1gFnSc7	stěna
<g/>
,	,	kIx,	,
u	u	k7c2	u
podélného	podélný	k2eAgInSc2d1	podélný
systému	systém	k1gInSc2	systém
pak	pak	k6eAd1	pak
stěnou	stěna	k1gFnSc7	stěna
příčnou	příčný	k2eAgFnSc7d1	příčná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Styk	styk	k1gInSc1	styk
panelů	panel	k1gInPc2	panel
je	být	k5eAaImIp3nS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
svařením	svaření	k1gNnSc7	svaření
výztuže	výztuž	k1gFnSc2	výztuž
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
styčníkových	styčníkový	k2eAgFnPc2d1	styčníková
desek	deska	k1gFnPc2	deska
či	či	k8xC	či
třmínků	třmínek	k1gInPc2	třmínek
<g/>
.	.	kIx.	.
</s>
<s>
Spoj	spoj	k1gFnSc1	spoj
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zalit	zalít	k5eAaPmNgMnS	zalít
cementovou	cementový	k2eAgFnSc7d1	cementová
zálivkou	zálivka	k1gFnSc7	zálivka
nebo	nebo	k8xC	nebo
řídkým	řídký	k2eAgInSc7d1	řídký
betonem	beton	k1gInSc7	beton
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
nevznikal	vznikat	k5eNaImAgInS	vznikat
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
průhyb	průhyb	k1gInSc1	průhyb
sousedních	sousední	k2eAgInPc2d1	sousední
stropních	stropní	k2eAgInPc2d1	stropní
panelů	panel	k1gInPc2	panel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
styčných	styčný	k2eAgFnPc2d1	styčná
spár	spára	k1gFnPc2	spára
vložena	vložit	k5eAaPmNgFnS	vložit
tzv.	tzv.	kA	tzv.
kleštinová	kleštinový	k2eAgFnSc1d1	kleštinová
výztuž	výztuž	k1gFnSc1	výztuž
a	a	k8xC	a
spáry	spára	k1gFnPc1	spára
zality	zalít	k5eAaPmNgFnP	zalít
cementovou	cementový	k2eAgFnSc7d1	cementová
zálivkou	zálivka	k1gFnSc7	zálivka
<g/>
.	.	kIx.	.
</s>
<s>
Panely	panel	k1gInPc1	panel
obvodového	obvodový	k2eAgInSc2d1	obvodový
pláště	plášť	k1gInSc2	plášť
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
obdobně	obdobně	k6eAd1	obdobně
<g/>
,	,	kIx,	,
spáry	spára	k1gFnPc1	spára
se	se	k3xPyFc4	se
zvnějšku	zvnějšku	k6eAd1	zvnějšku
ještě	ještě	k6eAd1	ještě
těsní	těsnit	k5eAaImIp3nP	těsnit
vložkou	vložka	k1gFnSc7	vložka
z	z	k7c2	z
PVC	PVC	kA	PVC
nebo	nebo	k8xC	nebo
trvale	trvale	k6eAd1	trvale
pružného	pružný	k2eAgInSc2d1	pružný
tmelu	tmel	k1gInSc2	tmel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problémy	problém	k1gInPc1	problém
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
panelovými	panelový	k2eAgInPc7d1	panelový
domy	dům	k1gInPc7	dům
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
problémem	problém	k1gInSc7	problém
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
byla	být	k5eAaImAgFnS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
a	a	k8xC	a
tedy	tedy	k9	tedy
velké	velký	k2eAgFnPc4d1	velká
tepelné	tepelný	k2eAgFnPc4d1	tepelná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
ztrátám	ztráta	k1gFnPc3	ztráta
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
většina	většina	k1gFnSc1	většina
typů	typ	k1gInPc2	typ
má	mít	k5eAaImIp3nS	mít
izolovaný	izolovaný	k2eAgInSc1d1	izolovaný
obvodový	obvodový	k2eAgInSc1d1	obvodový
plášť	plášť	k1gInSc1	plášť
sendvičového	sendvičový	k2eAgInSc2d1	sendvičový
typu	typ	k1gInSc2	typ
tj.	tj.	kA	tj.
nosná	nosný	k2eAgFnSc1d1	nosná
<g/>
,	,	kIx,	,
tepelně	tepelně	k6eAd1	tepelně
izolační	izolační	k2eAgFnSc1d1	izolační
a	a	k8xC	a
krycí	krycí	k2eAgFnSc1d1	krycí
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obálce	obálka	k1gFnSc6	obálka
budovy	budova	k1gFnSc2	budova
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
tepelných	tepelný	k2eAgInPc2d1	tepelný
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
tepelný	tepelný	k2eAgInSc4d1	tepelný
most	most	k1gInSc4	most
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
styku	styk	k1gInSc3	styk
panelů	panel	k1gInPc2	panel
pláště	plášť	k1gInSc2	plášť
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
nebo	nebo	k8xC	nebo
stropu	strop	k1gInSc3	strop
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
špatným	špatný	k2eAgInSc7d1	špatný
technologickým	technologický	k2eAgInSc7d1	technologický
postupem	postup	k1gInSc7	postup
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
konstrukční	konstrukční	k2eAgFnSc7d1	konstrukční
chybou	chyba	k1gFnSc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
stavebně	stavebně	k6eAd1	stavebně
fyzikálním	fyzikální	k2eAgInSc7d1	fyzikální
nedostatkem	nedostatek	k1gInSc7	nedostatek
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nevyhovujících	vyhovující	k2eNgNnPc2d1	nevyhovující
dřevěných	dřevěný	k2eAgNnPc2d1	dřevěné
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
lodžiových	lodžiový	k2eAgFnPc2d1	lodžiová
sestav	sestava	k1gFnPc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
také	také	k9	také
vysoká	vysoký	k2eAgFnSc1d1	vysoká
schopnost	schopnost	k1gFnSc1	schopnost
betonu	beton	k1gInSc2	beton
vést	vést	k5eAaImF	vést
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
nedostatečně	dostatečně	k6eNd1	dostatečně
tlumen	tlumen	k2eAgInSc1d1	tlumen
hluk	hluk	k1gInSc1	hluk
z	z	k7c2	z
výtahové	výtahový	k2eAgFnSc2d1	výtahová
strojovny	strojovna	k1gFnSc2	strojovna
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
používání	používání	k1gNnSc2	používání
tzv.	tzv.	kA	tzv.
nulových	nulový	k2eAgFnPc2d1	nulová
podlah	podlaha	k1gFnPc2	podlaha
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
podlah	podlaha	k1gFnPc2	podlaha
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
položena	položen	k2eAgFnSc1d1	položena
nášlapná	nášlapný	k2eAgFnSc1d1	nášlapná
vrstva	vrstva	k1gFnSc1	vrstva
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
stropní	stropní	k2eAgFnSc6d1	stropní
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
slabé	slabý	k2eAgNnSc1d1	slabé
tlumení	tlumení	k1gNnSc1	tlumení
kročejového	kročejový	k2eAgInSc2d1	kročejový
hluku	hluk	k1gInSc2	hluk
(	(	kIx(	(
<g/>
hluk	hluk	k1gInSc1	hluk
vznikající	vznikající	k2eAgInSc1d1	vznikající
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
také	také	k9	také
mnohdy	mnohdy	k6eAd1	mnohdy
používání	používání	k1gNnSc1	používání
nekvalitních	kvalitní	k2eNgInPc2d1	nekvalitní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Používáním	používání	k1gNnSc7	používání
cementu	cement	k1gInSc2	cement
nižší	nízký	k2eAgFnSc2d2	nižší
kvality	kvalita	k1gFnSc2	kvalita
dochází	docházet	k5eAaImIp3nS	docházet
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
opadávání	opadávání	k1gNnSc3	opadávání
krycí	krycí	k2eAgFnSc2d1	krycí
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
i	i	k9	i
k	k	k7c3	k
rezavění	rezavění	k1gNnSc3	rezavění
výztuže	výztuž	k1gFnSc2	výztuž
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávně	správně	k6eNd1	správně
řešeny	řešen	k2eAgInPc1d1	řešen
či	či	k8xC	či
provedeny	proveden	k2eAgInPc1d1	proveden
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
ploché	plochý	k2eAgFnPc1d1	plochá
střechy	střecha	k1gFnPc1	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
vadou	vada	k1gFnSc7	vada
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
umakartových	umakartový	k2eAgNnPc2d1	umakartové
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sociální	sociální	k2eAgInPc4d1	sociální
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
silně	silně	k6eAd1	silně
toxického	toxický	k2eAgNnSc2d1	toxické
a	a	k8xC	a
tedy	tedy	k9	tedy
jedovatého	jedovatý	k2eAgInSc2d1	jedovatý
kouře	kouř	k1gInSc2	kouř
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
rozvody	rozvod	k1gInPc4	rozvod
jsou	být	k5eAaImIp3nP	být
užity	užit	k2eAgInPc4d1	užit
často	často	k6eAd1	často
nevyhovující	vyhovující	k2eNgInPc4d1	nevyhovující
materiály	materiál	k1gInPc4	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
karcinogenní	karcinogenní	k2eAgInPc4d1	karcinogenní
azbestocementové	azbestocementový	k2eAgInPc4d1	azbestocementový
odpadní	odpadní	k2eAgInPc4d1	odpadní
či	či	k8xC	či
vzduchotechnické	vzduchotechnický	k2eAgNnSc4d1	vzduchotechnické
potrubí	potrubí	k1gNnSc4	potrubí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
provádí	provádět	k5eAaImIp3nS	provádět
revitalizace	revitalizace	k1gFnSc1	revitalizace
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
zasklívání	zasklívání	k1gNnSc1	zasklívání
balkónů	balkón	k1gInPc2	balkón
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
dřevěných	dřevěný	k2eAgNnPc2d1	dřevěné
oken	okno	k1gNnPc2	okno
za	za	k7c4	za
plastová	plastový	k2eAgNnPc4d1	plastové
<g/>
,	,	kIx,	,
či	či	k8xC	či
nové	nový	k2eAgFnPc1d1	nová
kuchyňské	kuchyňský	k2eAgFnPc1d1	kuchyňská
linky	linka	k1gFnPc1	linka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
je	být	k5eAaImIp3nS	být
také	také	k9	také
zateplování	zateplování	k1gNnSc1	zateplování
paneláků	panelák	k1gInPc2	panelák
a	a	k8xC	a
i	i	k9	i
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
často	často	k6eAd1	často
prováděná	prováděný	k2eAgFnSc1d1	prováděná
změna	změna	k1gFnSc1	změna
fasády	fasáda	k1gFnSc2	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
vzhled	vzhled	k1gInSc4	vzhled
celých	celý	k2eAgNnPc2d1	celé
sídlišť	sídliště	k1gNnPc2	sídliště
a	a	k8xC	a
namísto	namísto	k7c2	namísto
chladných	chladný	k2eAgFnPc2d1	chladná
šedivých	šedivý	k2eAgFnPc2d1	šedivá
barev	barva	k1gFnPc2	barva
dnešní	dnešní	k2eAgNnSc1d1	dnešní
sídliště	sídliště	k1gNnSc1	sídliště
září	zářit	k5eAaImIp3nS	zářit
takřka	takřka	k6eAd1	takřka
všemi	všecek	k3xTgFnPc7	všecek
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
-	-	kIx~	-
2008	[number]	k4	2008
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedostatku	nedostatek	k1gInSc2	nedostatek
nových	nový	k2eAgInPc2d1	nový
levných	levný	k2eAgInPc2d1	levný
bytů	byt	k1gInPc2	byt
ve	v	k7c6	v
městech	město	k1gNnPc6	město
rozmohl	rozmoct	k5eAaPmAgInS	rozmoct
velký	velký	k2eAgInSc1d1	velký
stavební	stavební	k2eAgInSc1d1	stavební
boom	boom	k1gInSc1	boom
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
stavebních	stavební	k2eAgFnPc2d1	stavební
úprav	úprava	k1gFnPc2	úprava
v	v	k7c6	v
panelových	panelový	k2eAgInPc6d1	panelový
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dalo	dát	k5eAaPmAgNnS	dát
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
stavebních	stavební	k2eAgFnPc2d1	stavební
firem	firma	k1gFnPc2	firma
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
rekonstrukcemi	rekonstrukce	k1gFnPc7	rekonstrukce
bytových	bytový	k2eAgNnPc2d1	bytové
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
zateplování	zateplování	k1gNnSc2	zateplování
těchto	tento	k3xDgInPc2	tento
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Panelový	panelový	k2eAgInSc4d1	panelový
dům	dům	k1gInSc4	dům
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Narážky	narážka	k1gFnPc1	narážka
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
bydlení	bydlení	k1gNnSc2	bydlení
v	v	k7c6	v
panelových	panelový	k2eAgInPc6d1	panelový
domech	dům	k1gInPc6	dům
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
například	například	k6eAd1	například
v	v	k7c6	v
Janouškově	Janouškův	k2eAgFnSc6d1	Janouškova
písni	píseň	k1gFnSc6	píseň
Náš	náš	k3xOp1gInSc1	náš
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
obsažené	obsažený	k2eAgInPc1d1	obsažený
na	na	k7c6	na
albu	album	k1gNnSc6	album
Kdo	kdo	k3yInSc1	kdo
to	ten	k3xDgNnSc4	ten
zavinil	zavinit	k5eAaPmAgMnS	zavinit
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
refrén	refrén	k1gInSc1	refrén
začíná	začínat	k5eAaImIp3nS	začínat
textem	text	k1gInSc7	text
"	"	kIx"	"
<g/>
náš	náš	k3xOp1gInSc1	náš
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
různých	různý	k2eAgInPc2d1	různý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
nahrál	nahrát	k5eAaBmAgMnS	nahrát
také	také	k6eAd1	také
píseň	píseň	k1gFnSc4	píseň
Pohled	pohled	k1gInSc1	pohled
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
o	o	k7c6	o
výhledu	výhled	k1gInSc6	výhled
na	na	k7c4	na
160	[number]	k4	160
pokojů	pokoj	k1gInPc2	pokoj
<g/>
,	,	kIx,	,
kuchyní	kuchyně	k1gFnPc2	kuchyně
<g/>
,	,	kIx,	,
předsíní	předsíň	k1gFnPc2	předsíň
a	a	k8xC	a
koupelen	koupelna	k1gFnPc2	koupelna
v	v	k7c6	v
protějším	protější	k2eAgInSc6d1	protější
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
životě	život	k1gInSc6	život
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
panelovém	panelový	k2eAgNnSc6d1	panelové
sídlišti	sídliště	k1gNnSc6	sídliště
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
i	i	k8xC	i
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
Panelstory	Panelstor	k1gInPc1	Panelstor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
režisérky	režisérka	k1gFnSc2	režisérka
Věry	Věra	k1gFnSc2	Věra
Chytilové	Chytilová	k1gFnSc2	Chytilová
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
devítidílný	devítidílný	k2eAgInSc4d1	devítidílný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
domě	dům	k1gInSc6	dům
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
obyvatel	obyvatel	k1gMnPc2	obyvatel
nového	nový	k2eAgInSc2d1	nový
panelového	panelový	k2eAgInSc2d1	panelový
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
všechno	všechen	k3xTgNnSc4	všechen
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uslyšet	uslyšet	k5eAaPmF	uslyšet
cestou	cesta	k1gFnSc7	cesta
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
panelovém	panelový	k2eAgInSc6d1	panelový
domě	dům	k1gInSc6	dům
zpívá	zpívat	k5eAaImIp3nS	zpívat
Richard	Richard	k1gMnSc1	Richard
Müller	Müller	k1gMnSc1	Müller
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Po	po	k7c4	po
schodoch	schodoch	k1gInSc4	schodoch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velká	velký	k2eAgNnPc1d1	velké
sídliště	sídliště	k1gNnPc1	sídliště
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
sepsána	sepsán	k2eAgNnPc4d1	sepsáno
sídliště	sídliště	k1gNnPc4	sídliště
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
jejich	jejich	k3xOp3gFnPc1	jejich
konstrukční	konstrukční	k2eAgFnPc1d1	konstrukční
soustavy	soustava	k1gFnPc1	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Chodov	Chodov	k1gInSc1	Chodov
<g/>
,	,	kIx,	,
Háje	háj	k1gInPc1	háj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Severní	severní	k2eAgNnSc1d1	severní
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Bohnice	Bohnice	k1gInPc1	Bohnice
(	(	kIx(	(
<g/>
VVÚ-ETA	VVÚ-ETA	k1gFnSc1	VVÚ-ETA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čimice	Čimice	k1gInPc1	Čimice
<g/>
,	,	kIx,	,
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
<g/>
,	,	kIx,	,
Ďáblice	ďáblice	k1gFnPc1	ďáblice
<g/>
,	,	kIx,	,
Střížkov	Střížkov	k1gInSc1	Střížkov
<g/>
,	,	kIx,	,
Prosek	proséct	k5eAaPmDgInS	proséct
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Letňany	Letňan	k1gMnPc7	Letňan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Stodůlky	stodůlka	k1gFnPc1	stodůlka
<g/>
,	,	kIx,	,
Lužiny	lužina	k1gFnPc1	lužina
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
Butovice	Butovice	k1gFnPc1	Butovice
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Ohrada	ohrada	k1gFnSc1	ohrada
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
VVÚ-ETA	VVÚ-ETA	k1gFnSc1	VVÚ-ETA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Řepy	řepa	k1gFnPc1	řepa
(	(	kIx(	(
<g/>
VVÚ-ETA	VVÚ-ETA	k1gFnPc1	VVÚ-ETA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barrandov	Barrandov	k1gInSc1	Barrandov
(	(	kIx(	(
<g/>
především	především	k9	především
OP	op	k1gMnSc1	op
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
Lhotka	Lhotka	k1gFnSc1	Lhotka
<g/>
,	,	kIx,	,
Kamýk	Kamýk	k1gInSc1	Kamýk
<g/>
,	,	kIx,	,
Modřany	Modřany	k1gInPc1	Modřany
(	(	kIx(	(
<g/>
Larsen	larsena	k1gFnPc2	larsena
-	-	kIx~	-
Nielsen	Nielsen	k1gInSc1	Nielsen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zahradní	zahradní	k2eAgNnSc1d1	zahradní
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
z	z	k7c2	z
části	část	k1gFnSc2	část
G	G	kA	G
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malešice	Malešice	k1gFnPc1	Malešice
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc2d1	horní
Měcholupy	Měcholupa	k1gFnSc2	Měcholupa
(	(	kIx(	(
<g/>
VVÚ-ETA	VVÚ-ETA	k1gMnSc1	VVÚ-ETA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petrovice	Petrovice	k1gFnSc1	Petrovice
(	(	kIx(	(
<g/>
VVÚ-ETA	VVÚ-ETA	k1gFnSc1	VVÚ-ETA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Petřiny	Petřiny	k1gFnPc1	Petřiny
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
Vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Brno	Brno	k1gNnSc1	Brno
===	===	k?	===
</s>
</p>
<p>
<s>
Bystrc	Bystrc	k1gFnSc1	Bystrc
</s>
</p>
<p>
<s>
Líšeň	Líšeň	k1gFnSc1	Líšeň
</s>
</p>
<p>
<s>
Starý	starý	k2eAgInSc1d1	starý
Lískovec	Lískovec	k1gInSc1	Lískovec
</s>
</p>
<p>
<s>
Bohunice	Bohunice	k1gFnPc1	Bohunice
</s>
</p>
<p>
<s>
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
</s>
</p>
<p>
<s>
Lesná	lesný	k2eAgFnSc1d1	Lesná
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Lískovec	Lískovec	k1gInSc1	Lískovec
</s>
</p>
<p>
<s>
Kohoutovice	Kohoutovice	k1gFnPc1	Kohoutovice
</s>
</p>
<p>
<s>
Řečkovice	Řečkovice	k1gFnSc1	Řečkovice
</s>
</p>
<p>
<s>
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrava	Ostrava	k1gFnSc1	Ostrava
===	===	k?	===
</s>
</p>
<p>
<s>
Hrabůvka	Hrabůvka	k1gFnSc1	Hrabůvka
(	(	kIx(	(
<g/>
G-OS	G-OS	k1gMnSc1	G-OS
<g/>
,	,	kIx,	,
VM-OS	VM-OS	k1gMnSc1	VM-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-BTS	B-BTS	k1gFnPc2	B-BTS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poruba	Poruba	k1gFnSc1	Poruba
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
57	[number]	k4	57
<g/>
,	,	kIx,	,
G-OS	G-OS	k1gMnSc1	G-OS
<g/>
,	,	kIx,	,
V-OS	V-OS	k1gMnSc1	V-OS
<g/>
,	,	kIx,	,
VM-OS	VM-OS	k1gMnSc1	VM-OS
<g/>
,	,	kIx,	,
BP	BP	kA	BP
<g/>
70	[number]	k4	70
<g/>
-OS	-OS	k?	-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-BTS	B-BTS	k1gFnPc2	B-BTS
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.13	[number]	k4	1.13
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.31	[number]	k4	1.31
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dubina	dubina	k1gFnSc1	dubina
(	(	kIx(	(
<g/>
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.13	[number]	k4	1.13
<g/>
,	,	kIx,	,
VP-OS	VP-OS	k1gFnSc1	VP-OS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zábřeh	Zábřeh	k1gInSc1	Zábřeh
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
57	[number]	k4	57
<g/>
,	,	kIx,	,
G-OS	G-OS	k1gMnSc1	G-OS
<g/>
,	,	kIx,	,
V-OS	V-OS	k1gMnSc1	V-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-BTS	B-BTS	k1gFnPc2	B-BTS
<g/>
,	,	kIx,	,
VP-OS	VP-OS	k1gMnSc1	VP-OS
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výškovice	Výškovice	k1gFnSc1	Výškovice
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-BTS	B-BTS	k1gFnPc2	B-BTS
<g/>
,	,	kIx,	,
BP	BP	kA	BP
<g/>
70	[number]	k4	70
<g/>
-OS	-OS	k?	-OS
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bělský	Bělský	k2eAgInSc1d1	Bělský
Les	les	k1gInSc1	les
(	(	kIx(	(
<g/>
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.13	[number]	k4	1.13
<g/>
,	,	kIx,	,
VP-OS	VP-OS	k1gFnSc1	VP-OS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fifejdy	Fifejd	k1gInPc1	Fifejd
(	(	kIx(	(
<g/>
VM-OS	VM-OS	k1gFnSc1	VM-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-PSO	B-PSO	k1gFnPc2	B-PSO
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-BTS	B-BTS	k1gFnPc2	B-BTS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B-OS-R	B-OS-R	k1gFnPc2	B-OS-R
<g/>
,	,	kIx,	,
VP-OS	VP-OS	k1gMnSc1	VP-OS
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Havířov	Havířov	k1gInSc4	Havířov
===	===	k?	===
</s>
</p>
<p>
<s>
Havířov-Podlesí	Havířov-Podlese	k1gFnPc2	Havířov-Podlese
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
B-OS	B-OS	k1gFnPc2	B-OS
<g/>
,	,	kIx,	,
V-OS	V-OS	k1gFnPc2	V-OS
<g/>
,	,	kIx,	,
BP	BP	kA	BP
<g/>
70	[number]	k4	70
<g/>
-OS	-OS	k?	-OS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Havířov-Šumbark	Havířov-Šumbark	k1gInSc1	Havířov-Šumbark
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
BP	BP	kA	BP
<g/>
70	[number]	k4	70
<g/>
-OS	-OS	k?	-OS
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.13	[number]	k4	1.13
<g/>
,	,	kIx,	,
VP-OS	VP-OS	k1gFnSc1	VP-OS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Plzeň	Plzeň	k1gFnSc1	Plzeň
===	===	k?	===
</s>
</p>
<p>
<s>
Slovany	Slovan	k1gInPc1	Slovan
</s>
</p>
<p>
<s>
Doubravka	Doubravka	k1gFnSc1	Doubravka
</s>
</p>
<p>
<s>
Bory	bor	k1gInPc4	bor
</s>
</p>
<p>
<s>
Skvrňany	Skvrňan	k1gMnPc4	Skvrňan
</s>
</p>
<p>
<s>
Lochotín	Lochotín	k1gMnSc1	Lochotín
</s>
</p>
<p>
<s>
Bolevec	Bolevec	k1gMnSc1	Bolevec
</s>
</p>
<p>
<s>
Košutka	Košutka	k1gFnSc1	Košutka
</s>
</p>
<p>
<s>
Vinice	vinice	k1gFnSc1	vinice
</s>
</p>
<p>
<s>
===	===	k?	===
Liberec	Liberec	k1gInSc1	Liberec
===	===	k?	===
</s>
</p>
<p>
<s>
Rochlice	Rochlice	k1gFnSc1	Rochlice
</s>
</p>
<p>
<s>
Pavlovice	Pavlovice	k1gFnPc1	Pavlovice
</s>
</p>
<p>
<s>
Ruprechtice	Ruprechtice	k1gFnSc1	Ruprechtice
</s>
</p>
<p>
<s>
Králův	Králův	k2eAgInSc1d1	Králův
Háj	háj	k1gInSc1	háj
</s>
</p>
<p>
<s>
Kunratická	kunratický	k2eAgFnSc1d1	Kunratická
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
===	===	k?	===
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Máj	Mája	k1gFnPc2	Mája
</s>
</p>
<p>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
sídliště	sídliště	k1gNnSc1	sídliště
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Šumava	Šumava	k1gFnSc1	Šumava
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Vltava	Vltava	k1gFnSc1	Vltava
</s>
</p>
<p>
<s>
===	===	k?	===
Zlín	Zlín	k1gInSc1	Zlín
===	===	k?	===
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
Svahy	svah	k1gInPc1	svah
(	(	kIx(	(
<g/>
NKS-G	NKS-G	k1gMnSc1	NKS-G
<g/>
,	,	kIx,	,
OP	op	k1gMnSc1	op
<g/>
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Jihlava	Jihlava	k1gFnSc1	Jihlava
===	===	k?	===
</s>
</p>
<p>
<s>
Březinovy	Březinův	k2eAgFnPc1d1	Březinova
sady	sada	k1gFnPc1	sada
</s>
</p>
<p>
<s>
===	===	k?	===
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
pánev	pánev	k1gFnSc1	pánev
===	===	k?	===
</s>
</p>
<p>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Terasa	terasa	k1gFnSc1	terasa
<g/>
,	,	kIx,	,
Dobětice	Dobětice	k1gFnSc1	Dobětice
<g/>
,	,	kIx,	,
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krupka	krupka	k1gFnSc1	krupka
(	(	kIx(	(
<g/>
Bohosudov	Bohosudov	k1gInSc1	Bohosudov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Teplice	Teplice	k1gFnPc1	Teplice
(	(	kIx(	(
<g/>
Trnovany	Trnovan	k1gMnPc4	Trnovan
<g/>
,	,	kIx,	,
Prosetice	Prosetika	k1gFnSc6	Prosetika
<g/>
,	,	kIx,	,
Šanov	Šanov	k1gInSc1	Šanov
II	II	kA	II
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bílina	Bílina	k1gFnSc1	Bílina
(	(	kIx(	(
<g/>
Za	za	k7c7	za
Chlumem	chlum	k1gInSc7	chlum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Duchcov	Duchcov	k1gInSc1	Duchcov
(	(	kIx(	(
<g/>
Osecká	osecký	k2eAgFnSc1d1	Osecká
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
Liščí	liščí	k2eAgInSc1d1	liščí
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Litvínov	Litvínov	k1gInSc1	Litvínov
(	(	kIx(	(
<g/>
Janov	Janov	k1gInSc1	Janov
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jirkov	Jirkov	k1gInSc1	Jirkov
(	(	kIx(	(
<g/>
Vinařická	Vinařická	k1gFnSc1	Vinařická
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kadaň	Kadaň	k1gFnSc1	Kadaň
(	(	kIx(	(
<g/>
Sídliště	sídliště	k1gNnSc1	sídliště
B	B	kA	B
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chomutov	Chomutov	k1gInSc1	Chomutov
(	(	kIx(	(
<g/>
Písečná	písečný	k2eAgFnSc1d1	písečná
<g/>
,	,	kIx,	,
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
<g/>
,	,	kIx,	,
Kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
,	,	kIx,	,
Březenecká	Březenecký	k2eAgFnSc1d1	Březenecká
<g/>
,	,	kIx,	,
Jitřenka	Jitřenka	k1gFnSc1	Jitřenka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Pardubice	Pardubice	k1gInPc1	Pardubice
===	===	k?	===
</s>
</p>
<p>
<s>
Dubina	dubina	k1gFnSc1	dubina
</s>
</p>
<p>
<s>
Polabiny	Polabina	k1gFnPc1	Polabina
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
===	===	k?	===
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
Předměstí	předměstí	k1gNnSc1	předměstí
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sever	sever	k1gInSc1	sever
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
HK	HK	kA	HK
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Labská	labský	k2eAgFnSc1d1	Labská
Kotlina	kotlina	k1gFnSc1	kotlina
II	II	kA	II
(	(	kIx(	(
<g/>
HK	HK	kA	HK
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Břeclav	Břeclav	k1gFnSc1	Břeclav
===	===	k?	===
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Na	na	k7c6	na
Valtické	valtický	k2eAgFnSc6d1	Valtická
(	(	kIx(	(
<g/>
OP	op	k1gMnSc1	op
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Příbram	Příbram	k1gFnSc1	Příbram
===	===	k?	===
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Křižáky	křižák	k1gMnPc4	křižák
I	I	kA	I
<g/>
,	,	kIx,	,
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
<g/>
III	III	kA	III
(	(	kIx(	(
<g/>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
panelové	panelový	k2eAgInPc1d1	panelový
domy	dům	k1gInPc1	dům
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
ČR	ČR	kA	ČR
nemá	mít	k5eNaImIp3nS	mít
obdobu	obdoba	k1gFnSc4	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Vystaveny	vystaven	k2eAgInPc4d1	vystaven
Dle	dle	k7c2	dle
vzoru	vzor	k1gInSc2	vzor
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Drkolnov	Drkolnovo	k1gNnPc2	Drkolnovo
</s>
</p>
<p>
<s>
===	===	k?	===
Hodonín	Hodonín	k1gInSc1	Hodonín
===	===	k?	===
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
OP	op	k1gMnSc1	op
1.11	[number]	k4	1.11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chruščovka	Chruščovka	k1gFnSc1	Chruščovka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
panelový	panelový	k2eAgInSc4d1	panelový
dům	dům	k1gInSc4	dům
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
panelový	panelový	k2eAgMnSc1d1	panelový
dům	dům	k1gInSc4	dům
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Web	web	k1gInSc1	web
o	o	k7c6	o
panelových	panelový	k2eAgFnPc6d1	panelová
konstrukčních	konstrukční	k2eAgFnPc6d1	konstrukční
soustavách	soustava	k1gFnPc6	soustava
</s>
</p>
