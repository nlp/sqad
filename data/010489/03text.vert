<p>
<s>
Složené	složený	k2eAgNnSc1d1	složené
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
3	[number]	k4	3
různé	různý	k2eAgInPc1d1	různý
dělitele	dělitel	k1gInPc1	dělitel
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
dalšího	další	k2eAgMnSc4d1	další
dělitele	dělitel	k1gMnSc4	dělitel
kromě	kromě	k7c2	kromě
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
složené	složený	k2eAgNnSc1d1	složené
číslo	číslo	k1gNnSc1	číslo
lze	lze	k6eAd1	lze
napsat	napsat	k5eAaPmF	napsat
jako	jako	k9	jako
součin	součin	k1gInSc1	součin
dvou	dva	k4xCgFnPc2	dva
menších	malý	k2eAgFnPc2d2	menší
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
prvočíslo	prvočíslo	k1gNnSc1	prvočíslo
ani	ani	k8xC	ani
číslo	číslo	k1gNnSc1	číslo
složené	složený	k2eAgNnSc1d1	složené
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
jediného	jediný	k2eAgMnSc4d1	jediný
dělitele	dělitel	k1gMnSc4	dělitel
<g/>
,	,	kIx,	,
samo	sám	k3xTgNnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
prvočíslo	prvočíslo	k1gNnSc4	prvočíslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
dělitele	dělitel	k1gInPc4	dělitel
<g/>
,	,	kIx,	,
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
samo	sám	k3xTgNnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
21	[number]	k4	21
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
složené	složený	k2eAgFnSc2d1	složená
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
dělitele	dělitel	k1gInPc4	dělitel
<g/>
:	:	kIx,	:
jedničku	jednička	k1gFnSc4	jednička
<g/>
,	,	kIx,	,
trojku	trojka	k1gFnSc4	trojka
<g/>
,	,	kIx,	,
sedmičku	sedmička	k1gFnSc4	sedmička
a	a	k8xC	a
samo	sám	k3xTgNnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
napsat	napsat	k5eAaPmF	napsat
jako	jako	k9	jako
součin	součin	k1gInSc1	součin
dvou	dva	k4xCgNnPc2	dva
menších	malý	k2eAgNnPc2d2	menší
čísel	číslo	k1gNnPc2	číslo
<g/>
:	:	kIx,	:
21	[number]	k4	21
=	=	kIx~	=
3	[number]	k4	3
·	·	k?	·
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
Každé	každý	k3xTgNnSc1	každý
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
prvočíslem	prvočíslo	k1gNnSc7	prvočíslo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
číslem	číslo	k1gNnSc7	číslo
složeným	složený	k2eAgNnSc7d1	složené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každé	každý	k3xTgNnSc1	každý
složené	složený	k2eAgNnSc1d1	složené
číslo	číslo	k1gNnSc1	číslo
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
součin	součin	k1gInSc1	součin
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
–	–	k?	–
tzv.	tzv.	kA	tzv.
kanonický	kanonický	k2eAgInSc4d1	kanonický
rozklad	rozklad	k1gInSc4	rozklad
čísla	číslo	k1gNnSc2	číslo
na	na	k7c4	na
prvočinitele	prvočinitel	k1gMnSc4	prvočinitel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nebereme	brát	k5eNaImIp1nP	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
pořadí	pořadí	k1gNnSc2	pořadí
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
ve	v	k7c6	v
výrazu	výraz	k1gInSc6	výraz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
zápis	zápis	k1gInSc1	zápis
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
základní	základní	k2eAgFnSc1d1	základní
věta	věta	k1gFnSc1	věta
aritmetiky	aritmetika	k1gFnSc2	aritmetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Nejmenším	malý	k2eAgNnSc7d3	nejmenší
složeným	složený	k2eAgNnSc7d1	složené
číslem	číslo	k1gNnSc7	číslo
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
čtyři	čtyři	k4xCgNnPc1	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
n	n	k0	n
je	být	k5eAaImIp3nS	být
dělitelem	dělitel	k1gMnSc7	dělitel
(	(	kIx(	(
<g/>
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
složené	složený	k2eAgNnSc4d1	složené
číslo	číslo	k1gNnSc4	číslo
n	n	k0	n
>	>	kIx)	>
4	[number]	k4	4
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
