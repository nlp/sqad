<p>
<s>
Plch	Plch	k1gMnSc1	Plch
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Dryomys	Dryomys	k1gInSc1	Dryomys
nitedula	nitedulum	k1gNnSc2	nitedulum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
plch	plch	k1gMnSc1	plch
s	s	k7c7	s
huňatým	huňatý	k2eAgInSc7d1	huňatý
ocasem	ocas	k1gInSc7	ocas
bez	bez	k7c2	bez
koncové	koncový	k2eAgFnSc2d1	koncová
štětičky	štětička	k1gFnSc2	štětička
prodloužených	prodloužený	k2eAgInPc2d1	prodloužený
chlupů	chlup	k1gInPc2	chlup
a	a	k8xC	a
s	s	k7c7	s
celkově	celkově	k6eAd1	celkově
žlutohnědým	žlutohnědý	k2eAgNnSc7d1	žlutohnědé
či	či	k8xC	či
hnědožlutým	hnědožlutý	k2eAgNnSc7d1	hnědožluté
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
břišní	břišní	k2eAgFnSc1d1	břišní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
bývá	bývat	k5eAaImIp3nS	bývat
bílá	bílý	k2eAgFnSc1d1	bílá
nebo	nebo	k8xC	nebo
slabě	slabě	k6eAd1	slabě
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
oka	oko	k1gNnSc2	oko
k	k	k7c3	k
ušnímu	ušní	k2eAgInSc3d1	ušní
boltci	boltec	k1gInSc3	boltec
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
černý	černý	k2eAgInSc1d1	černý
pruh	pruh	k1gInSc1	pruh
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
uzdička	uzdička	k1gFnSc1	uzdička
<g/>
.	.	kIx.	.
</s>
<s>
Ušní	ušní	k2eAgInPc1d1	ušní
boltce	boltec	k1gInPc1	boltec
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
zakulacené	zakulacený	k2eAgInPc1d1	zakulacený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Plch	Plch	k1gMnSc1	Plch
lesní	lesní	k2eAgMnSc1d1	lesní
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
plch	plch	k1gMnSc1	plch
zahradní	zahradní	k2eAgMnSc1d1	zahradní
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
86	[number]	k4	86
až	až	k9	až
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
ocásek	ocásek	k1gInSc1	ocásek
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
60	[number]	k4	60
až	až	k8xS	až
113	[number]	k4	113
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
žlutohnědě	žlutohnědě	k6eAd1	žlutohnědě
či	či	k8xC	či
hnědožlutě	hnědožlutě	k6eAd1	hnědožlutě
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
bývá	bývat	k5eAaImIp3nS	bývat
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
žlutobílé	žlutobílý	k2eAgNnSc1d1	žlutobílé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
oko	oko	k1gNnSc4	oko
k	k	k7c3	k
ušnímu	ušní	k2eAgInSc3d1	ušní
boltci	boltec	k1gInSc3	boltec
černý	černý	k2eAgInSc1d1	černý
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
kulaté	kulatý	k2eAgInPc1d1	kulatý
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
g	g	kA	g
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
77	[number]	k4	77
<g/>
–	–	k?	–
<g/>
112	[number]	k4	112
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkáme	setkat	k5eAaPmIp1nP	setkat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
moravských	moravský	k2eAgFnPc6d1	Moravská
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
vrchovinách	vrchovina	k1gFnPc6	vrchovina
(	(	kIx(	(
<g/>
Moravskoslezské	moravskoslezský	k2eAgInPc1d1	moravskoslezský
Beskydy	Beskyd	k1gInPc1	Beskyd
<g/>
,	,	kIx,	,
Hostýnsko-vsetínská	hostýnskosetínský	k2eAgFnSc1d1	hostýnsko-vsetínský
hornatina	hornatina	k1gFnSc1	hornatina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
osídluje	osídlovat	k5eAaImIp3nS	osídlovat
Balkán	Balkán	k1gInSc1	Balkán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
,	,	kIx,	,
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Evropu	Evropa	k1gFnSc4	Evropa
až	až	k9	až
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
smíšených	smíšený	k2eAgInPc6d1	smíšený
a	a	k8xC	a
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
horských	horský	k2eAgInPc6d1	horský
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
osídluje	osídlovat	k5eAaImIp3nS	osídlovat
i	i	k9	i
čistě	čistě	k6eAd1	čistě
listnaté	listnatý	k2eAgInPc4d1	listnatý
porosty	porost	k1gInPc4	porost
<g/>
,	,	kIx,	,
keři	keř	k1gInPc7	keř
zarostlé	zarostlý	k2eAgFnSc2d1	zarostlá
meze	mez	k1gFnSc2	mez
<g/>
,	,	kIx,	,
vinice	vinice	k1gFnSc2	vinice
i	i	k8xC	i
vysloveně	vysloveně	k6eAd1	vysloveně
suchá	suchý	k2eAgNnPc1d1	suché
místa	místo	k1gNnPc1	místo
se	s	k7c7	s
sporým	sporý	k2eAgInSc7d1	sporý
křovinným	křovinný	k2eAgInSc7d1	křovinný
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
výskyt	výskyt	k1gInSc1	výskyt
četnější	četný	k2eAgFnSc2d2	četnější
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Doks	Doksy	k1gInPc2	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Českého	český	k2eAgNnSc2d1	české
středohoří	středohoří	k1gNnSc2	středohoří
mezi	mezi	k7c7	mezi
Litoměřicemi	Litoměřice	k1gInPc7	Litoměřice
a	a	k8xC	a
Ústím	ústit	k5eAaImIp1nS	ústit
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
dřeva	dřevo	k1gNnSc2	dřevo
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
i	i	k9	i
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Velkého	velký	k2eAgInSc2d1	velký
a	a	k8xC	a
Malého	Malého	k2eAgInSc2d1	Malého
Března	březen	k1gInSc2	březen
na	na	k7c6	na
Ústecku	Ústecko	k1gNnSc6	Ústecko
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
ovocných	ovocný	k2eAgInPc6d1	ovocný
stromech	strom	k1gInPc6	strom
(	(	kIx(	(
<g/>
třešních	třešeň	k1gFnPc6	třešeň
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vydlabaných	vydlabaný	k2eAgFnPc6d1	vydlabaná
dutinách	dutina	k1gFnPc6	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Také	také	k9	také
tento	tento	k3xDgMnSc1	tento
plch	plch	k1gMnSc1	plch
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
v	v	k7c6	v
hnízdech	hnízdo	k1gNnPc6	hnízdo
umístěných	umístěný	k2eAgFnPc2d1	umístěná
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
i	i	k8xC	i
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
druhy	druh	k1gInPc4	druh
vniká	vnikat	k5eAaImIp3nS	vnikat
do	do	k7c2	do
lesních	lesní	k2eAgNnPc2d1	lesní
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všežravý	všežravý	k2eAgMnSc1d1	všežravý
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
semeny	semeno	k1gNnPc7	semeno
a	a	k8xC	a
plody	plod	k1gInPc4	plod
lesních	lesní	k2eAgFnPc2d1	lesní
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
také	také	k9	také
zelenými	zelený	k2eAgInPc7d1	zelený
výhonky	výhonek	k1gInPc7	výhonek
<g/>
,	,	kIx,	,
listy	list	k1gInPc7	list
a	a	k8xC	a
trávou	tráva	k1gFnSc7	tráva
<g/>
.	.	kIx.	.
80	[number]	k4	80
%	%	kIx~	%
jídelníčku	jídelníček	k1gInSc2	jídelníček
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nS	tvořit
potrava	potrava	k1gFnSc1	potrava
živočišná	živočišný	k2eAgFnSc1d1	živočišná
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
larvy	larva	k1gFnPc1	larva
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severnějších	severní	k2eAgFnPc6d2	severnější
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
zimu	zima	k1gFnSc4	zima
přespává	přespávat	k5eAaImIp3nS	přespávat
(	(	kIx(	(
<g/>
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižnějších	jižní	k2eAgFnPc6d2	jižnější
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
čilý	čilý	k2eAgInSc1d1	čilý
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
chráněné	chráněný	k2eAgFnPc4d1	chráněná
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
plch	plch	k1gMnSc1	plch
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Dryomys	Dryomysa	k1gFnPc2	Dryomysa
nitedula	nitedulum	k1gNnSc2	nitedulum
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Ekolist	Ekolist	k1gInSc1	Ekolist
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
plchů	plch	k1gMnPc2	plch
<g/>
?	?	kIx.	?
</s>
</p>
