<s>
Plch	Plch	k1gMnSc1	Plch
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Dryomys	Dryomys	k1gInSc1	Dryomys
nitedula	nitedulum	k1gNnSc2	nitedulum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
plch	plch	k1gMnSc1	plch
s	s	k7c7	s
huňatým	huňatý	k2eAgInSc7d1	huňatý
ocasem	ocas	k1gInSc7	ocas
bez	bez	k7c2	bez
koncové	koncový	k2eAgFnSc2d1	koncová
štětičky	štětička	k1gFnSc2	štětička
prodloužených	prodloužený	k2eAgInPc2d1	prodloužený
chlupů	chlup	k1gInPc2	chlup
a	a	k8xC	a
s	s	k7c7	s
celkově	celkově	k6eAd1	celkově
žlutohnědým	žlutohnědý	k2eAgNnSc7d1	žlutohnědé
či	či	k8xC	či
hnědožlutým	hnědožlutý	k2eAgNnSc7d1	hnědožluté
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
břišní	břišní	k2eAgFnSc1d1	břišní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
bývá	bývat	k5eAaImIp3nS	bývat
bílá	bílý	k2eAgFnSc1d1	bílá
nebo	nebo	k8xC	nebo
slabě	slabě	k6eAd1	slabě
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
<g/>
.	.	kIx.	.
</s>
