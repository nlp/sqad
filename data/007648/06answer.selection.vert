<s>
Nejvíce	hodně	k6eAd3	hodně
však	však	k9	však
Erben	Erben	k1gMnSc1	Erben
proslul	proslout	k5eAaPmAgMnS	proslout
sbírkou	sbírka	k1gFnSc7	sbírka
Kytice	kytice	k1gFnSc2	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgInPc2d1	národní
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
Kytice	kytice	k1gFnSc1	kytice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
podruhé	podruhé	k6eAd1	podruhé
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
příležitostných	příležitostný	k2eAgFnPc2d1	příležitostná
písní	píseň	k1gFnPc2	píseň
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kytice	kytice	k1gFnSc1	kytice
z	z	k7c2	z
básní	báseň	k1gFnPc2	báseň
K.	K.	kA	K.
J.	J.	kA	J.
Erbena	Erben	k1gMnSc2	Erben
<g/>
.	.	kIx.	.
</s>
