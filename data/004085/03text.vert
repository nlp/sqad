<s>
William	William	k1gInSc1	William
Harvey	Harvea	k1gFnSc2	Harvea
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1578	[number]	k4	1578
<g/>
,	,	kIx,	,
Folkestone	Folkeston	k1gInSc5	Folkeston
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1657	[number]	k4	1657
<g/>
,	,	kIx,	,
Hampstead	Hampstead	k1gInSc1	Hampstead
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
objevem	objev	k1gInSc7	objev
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
teorii	teorie	k1gFnSc4	teorie
samoplození	samoplození	k1gNnSc2	samoplození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1603	[number]	k4	1603
si	se	k3xPyFc3	se
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
poznámek	poznámka	k1gFnPc2	poznámka
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc1	krev
bez	bez	k7c2	bez
přestání	přestání	k1gNnSc2	přestání
proudí	proudit	k5eAaImIp3nS	proudit
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
dokola	dokola	k6eAd1	dokola
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tlukotu	tlukot	k1gInSc2	tlukot
srdce	srdce	k1gNnSc2	srdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
úžasné	úžasný	k2eAgNnSc4d1	úžasné
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zcela	zcela	k6eAd1	zcela
odporovalo	odporovat	k5eAaImAgNnS	odporovat
Galenovým	Galenův	k2eAgFnPc3d1	Galenova
představám	představa	k1gFnPc3	představa
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
Harvey	Harvea	k1gFnSc2	Harvea
čekal	čekat	k5eAaImAgMnS	čekat
s	s	k7c7	s
uveřejněním	uveřejnění	k1gNnSc7	uveřejnění
poznatku	poznatek	k1gInSc2	poznatek
pětadvacet	pětadvacet	k4xCc1	pětadvacet
let	léto	k1gNnPc2	léto
<g/>
?	?	kIx.	?
</s>
<s>
Protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
protiřečila	protiřečit	k5eAaImAgFnS	protiřečit
Galenovi	Galen	k1gMnSc3	Galen
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
naprosto	naprosto	k6eAd1	naprosto
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celá	k1gFnSc1	celá
léta	léto	k1gNnSc2	léto
trpělivě	trpělivě	k6eAd1	trpělivě
prováděl	provádět	k5eAaImAgMnS	provádět
pokusy	pokus	k1gInPc4	pokus
a	a	k8xC	a
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
stříkačky	stříkačka	k1gFnSc2	stříkačka
vstřikoval	vstřikovat	k5eAaImAgMnS	vstřikovat
zvířatům	zvíře	k1gNnPc3	zvíře
do	do	k7c2	do
cév	céva	k1gFnPc2	céva
barviva	barvivo	k1gNnSc2	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
pitval	pitvat	k5eAaImAgMnS	pitvat
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozřezal	rozřezat	k5eAaPmAgMnS	rozřezat
jim	on	k3xPp3gMnPc3	on
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
srdeční	srdeční	k2eAgFnPc4d1	srdeční
chlopně	chlopeň	k1gFnPc4	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Logickou	logický	k2eAgFnSc7d1	logická
úvahou	úvaha	k1gFnSc7	úvaha
vyvodil	vyvodit	k5eAaPmAgMnS	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
srdce	srdce	k1gNnSc1	srdce
čerpá	čerpat	k5eAaImIp3nS	čerpat
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1628	[number]	k4	1628
publikoval	publikovat	k5eAaBmAgMnS	publikovat
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
Anatomické	anatomický	k2eAgNnSc1d1	anatomické
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
terčem	terč	k1gInSc7	terč
posměchu	posměch	k1gInSc2	posměch
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
lékaři	lékař	k1gMnPc1	lékař
ho	on	k3xPp3gNnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
potrhlého	potrhlý	k2eAgMnSc2d1	potrhlý
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
teorii	teorie	k1gFnSc4	teorie
za	za	k7c4	za
absurdní	absurdní	k2eAgFnSc4d1	absurdní
<g/>
,	,	kIx,	,
nemožnou	možný	k2eNgFnSc4d1	nemožná
a	a	k8xC	a
škodlivou	škodlivý	k2eAgFnSc4d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1657	[number]	k4	1657
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
uznat	uznat	k5eAaPmF	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
zakladatel	zakladatel	k1gMnSc1	zakladatel
embryologie	embryologie	k1gFnSc2	embryologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1616	[number]	k4	1616
objevil	objevit	k5eAaPmAgMnS	objevit
funkci	funkce	k1gFnSc4	funkce
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1628	[number]	k4	1628
tento	tento	k3xDgMnSc1	tento
objev	objev	k1gInSc4	objev
publikoval	publikovat	k5eAaBmAgMnS	publikovat
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
Exercitatio	Exercitatio	k1gMnSc1	Exercitatio
Anatomica	Anatomica	k1gMnSc1	Anatomica
de	de	k?	de
Motu	moto	k1gNnSc3	moto
Cordis	Cordis	k1gFnSc2	Cordis
et	et	k?	et
Sanguinis	Sanguinis	k1gInSc1	Sanguinis
in	in	k?	in
Animalibus	Animalibus	k1gInSc1	Animalibus
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
-	-	kIx~	-
Anatomické	anatomický	k2eAgNnSc1d1	anatomické
cvičení	cvičení	k1gNnSc1	cvičení
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
spisu	spis	k1gInSc6	spis
zvrátil	zvrátit	k5eAaPmAgMnS	zvrátit
Galénovo	Galénův	k2eAgNnSc4d1	Galénův
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
střídavě	střídavě	k6eAd1	střídavě
tam	tam	k6eAd1	tam
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
řídili	řídit	k5eAaImAgMnP	řídit
lékaři	lékař	k1gMnPc1	lékař
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
svoje	svůj	k3xOyFgNnPc4	svůj
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
pohyb	pohyb	k1gInSc4	pohyb
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
chlopně	chlopeň	k1gFnPc1	chlopeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
že	že	k8xS	že
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
pumpována	pumpovat	k5eAaImNgFnS	pumpovat
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
cévami	céva	k1gFnPc7	céva
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
popsal	popsat	k5eAaPmAgMnS	popsat
správně	správně	k6eAd1	správně
funkci	funkce	k1gFnSc4	funkce
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Harvey	Harvea	k1gMnSc2	Harvea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
