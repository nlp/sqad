<s>
Thugové	Thug	k1gMnPc1
</s>
<s>
Skupina	skupina	k1gFnSc1
thugů	thug	k1gMnPc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1863	#num#	k4
</s>
<s>
Thugové	Thug	k1gMnPc1
(	(	kIx(
<g/>
z	z	k7c2
hindského	hindský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
thag	thag	k1gInSc1
-	-	kIx~
zloděj	zloděj	k1gMnSc1
nebo	nebo	k8xC
ze	z	k7c2
sanskrtského	sanskrtský	k2eAgMnSc2d1
sthaga	sthag	k1gMnSc2
-	-	kIx~
lotr	lotr	k1gMnSc1
či	či	k8xC
sthagati	sthagat	k5eAaPmF,k5eAaBmF,k5eAaImF
-	-	kIx~
skrývat	skrývat	k5eAaImF
se	se	k3xPyFc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
indická	indický	k2eAgFnSc1d1
síť	síť	k1gFnSc1
tajných	tajný	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
okrádajících	okrádající	k2eAgFnPc2d1
a	a	k8xC
vraždících	vraždící	k2eAgFnPc2d1
cestující	cestující	k1gMnPc4
z	z	k7c2
náboženských	náboženský	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
působící	působící	k2eAgMnSc1d1
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
možná	možná	k9
i	i	k9
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Thugové	Thug	k1gMnPc1
praktikovali	praktikovat	k5eAaImAgMnP
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
vražd	vražda	k1gFnPc2
a	a	k8xC
loupeží	loupež	k1gFnPc2
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
k	k	k7c3
nic	nic	k6eAd1
netušícím	tušící	k2eNgFnPc3d1
pocestným	pocestná	k1gFnPc3
a	a	k8xC
získali	získat	k5eAaPmAgMnP
jejich	jejich	k3xOp3gFnSc4
důvěru	důvěra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
příhodném	příhodný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
zabili	zabít	k5eAaPmAgMnP
a	a	k8xC
okradli	okrást	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Způsobem	způsob	k1gInSc7
vraždy	vražda	k1gFnSc2
bylo	být	k5eAaImAgNnS
většinou	většina	k1gFnSc7
zardoušení	zardoušení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těla	tělo	k1gNnPc1
obětí	oběť	k1gFnPc2
byla	být	k5eAaImAgNnP
zpravidla	zpravidla	k6eAd1
zahrabána	zahrabat	k5eAaPmNgFnS
nebo	nebo	k8xC
hozena	hodit	k5eAaPmNgFnS
do	do	k7c2
studny	studna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oběti	oběť	k1gFnPc1
byly	být	k5eAaImAgFnP
většinou	většinou	k6eAd1
zabíjeny	zabíjet	k5eAaImNgFnP
na	na	k7c6
určitých	určitý	k2eAgInPc6d1
speciálních	speciální	k2eAgInPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
takovému	takový	k3xDgNnSc3
místu	místo	k1gNnSc3
se	se	k3xPyFc4
říkalo	říkat	k5eAaImAgNnS
bele	bela	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběti	oběť	k1gFnSc6
byly	být	k5eAaImAgInP
zabíjeny	zabíjet	k5eAaImNgInP
většinou	většina	k1gFnSc7
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
thugové	thug	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
hudbu	hudba	k1gFnSc4
nebo	nebo	k8xC
způsobili	způsobit	k5eAaPmAgMnP
hluk	hluk	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
znemožnili	znemožnit	k5eAaPmAgMnP
objevení	objevení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
člen	člen	k1gMnSc1
skupiny	skupina	k1gFnSc2
měl	mít	k5eAaImAgMnS
přesnou	přesný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
přivábit	přivábit	k5eAaPmF
cestující	cestující	k1gMnPc4
nebo	nebo	k8xC
držet	držet	k5eAaImF
hlídky	hlídka	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
předešlo	předejít	k5eAaPmAgNnS
útěkům	útěk	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůdci	vůdce	k1gMnPc7
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
říkalo	říkat	k5eAaImAgNnS
jamaadaar	jamaadaar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Thugové	Thug	k1gMnPc1
sami	sám	k3xTgMnPc1
odvozovali	odvozovat	k5eAaImAgMnP
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
od	od	k7c2
sedmi	sedm	k4xCc2
muslimských	muslimský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
později	pozdě	k6eAd2
přešli	přejít	k5eAaPmAgMnP
k	k	k7c3
hinduismu	hinduismus	k1gInSc3
a	a	k8xC
stali	stát	k5eAaPmAgMnP
se	se	k3xPyFc4
věrnými	věrný	k2eAgMnPc7d1
uctívači	uctívač	k1gMnPc7
bohyně	bohyně	k1gFnSc2
smrti	smrt	k1gFnSc2
Kálí	kálet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vraždy	vražda	k1gFnPc1
byly	být	k5eAaImAgInP
obětí	oběť	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušnost	příslušnost	k1gFnSc1
u	u	k7c2
thugů	thug	k1gMnPc2
se	se	k3xPyFc4
dědila	dědit	k5eAaImAgFnS
z	z	k7c2
otce	otec	k1gMnSc2
na	na	k7c4
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
unášeli	unášet	k5eAaImAgMnP
malé	malý	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
svých	svůj	k3xOyFgFnPc2
obětí	oběť	k1gFnPc2
a	a	k8xC
vychovávali	vychovávat	k5eAaImAgMnP
je	on	k3xPp3gMnPc4
jako	jako	k8xS,k8xC
thugy	thug	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
bylo	být	k5eAaImAgNnS
naučit	naučit	k5eAaPmF
se	se	k3xPyFc4
víře	víra	k1gFnSc3
od	od	k7c2
specializovaného	specializovaný	k2eAgMnSc2d1
instruktora	instruktor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovíto	takovýto	k3xDgMnPc1
thugové	thug	k1gMnPc1
byli	být	k5eAaImAgMnP
rekrutováni	rekrutovat	k5eAaImNgMnP
většinou	většinou	k6eAd1
z	z	k7c2
chudiny	chudina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Thugové	Thug	k1gMnPc1
se	se	k3xPyFc4
pohybovali	pohybovat	k5eAaImAgMnP
po	po	k7c6
Indii	Indie	k1gFnSc6
ve	v	k7c6
skupinkách	skupinka	k1gFnPc6
od	od	k7c2
10	#num#	k4
do	do	k7c2
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
dělali	dělat	k5eAaImAgMnP
<g/>
,	,	kIx,
mělo	mít	k5eAaImAgNnS
přesný	přesný	k2eAgInSc4d1
starobylý	starobylý	k2eAgInSc4d1
rituál	rituál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
vraždili	vraždit	k5eAaImAgMnP
žlutým	žlutý	k2eAgInSc7d1
šátkem	šátek	k1gInSc7
Rumaal	Rumaal	k1gMnSc1
<g/>
,	,	kIx,
symbolem	symbol	k1gInSc7
bohyně	bohyně	k1gFnSc2
Kálí	kálet	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
oloupení	oloupení	k1gNnSc6
své	svůj	k3xOyFgFnSc2
oběti	oběť	k1gFnSc2
pohřbili	pohřbít	k5eAaPmAgMnP
s	s	k7c7
přesnými	přesný	k2eAgInPc7d1
rituály	rituál	k1gInPc7
(	(	kIx(
<g/>
posvěcení	posvěcení	k1gNnSc3
krumpáčů	krumpáč	k1gInPc2
<g/>
,	,	kIx,
obětování	obětování	k1gNnSc1
cukru	cukr	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
rituály	rituál	k1gInPc1
byly	být	k5eAaImAgInP
prováděny	provádět	k5eAaImNgInP
k	k	k7c3
poctě	pocta	k1gFnSc3
bohyně	bohyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každou	každý	k3xTgFnSc4
vraždu	vražda	k1gFnSc4
prováděli	provádět	k5eAaImAgMnP
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
bohyně	bohyně	k1gFnSc1
nemusela	muset	k5eNaImAgFnS
vrátit	vrátit	k5eAaPmF
na	na	k7c4
tisíc	tisíc	k4xCgInSc4
let	let	k1gInSc4
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používali	používat	k5eAaImAgMnP
vlastní	vlastní	k2eAgInSc4d1
žargon	žargon	k1gInSc4
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgMnSc1d1
Ramasi	Ramas	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
thugové	thug	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
určitého	určitý	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
nepodíleli	podílet	k5eNaImAgMnP
se	se	k3xPyFc4
už	už	k9
na	na	k7c6
vraždách	vražda	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
pomáhali	pomáhat	k5eAaImAgMnP
skupině	skupina	k1gFnSc3
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
například	například	k6eAd1
jako	jako	k9
špioni	špion	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jejich	jejich	k3xOp3gFnSc1
organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
přísně	přísně	k6eAd1
utajená	utajený	k2eAgFnSc1d1
a	a	k8xC
protože	protože	k8xS
pro	pro	k7c4
své	své	k1gNnSc4
vraždy	vražda	k1gFnSc2
měli	mít	k5eAaImAgMnP
náboženskou	náboženský	k2eAgFnSc4d1
záminku	záminka	k1gFnSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
chápáni	chápat	k5eAaImNgMnP
jako	jako	k9
regulérní	regulérní	k2eAgFnPc1d1
<g/>
,	,	kIx,
daně	daň	k1gFnPc1
platící	platící	k2eAgNnSc1d1
řemeslo	řemeslo	k1gNnSc1
a	a	k8xC
chráněni	chráněn	k2eAgMnPc1d1
před	před	k7c7
pronásledováním	pronásledování	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
a	a	k8xC
potlačení	potlačení	k1gNnSc2
</s>
<s>
O	o	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
obětí	oběť	k1gFnPc2
nejsou	být	k5eNaImIp3nP
přesné	přesný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordů	rekord	k1gInPc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
organizace	organizace	k1gFnSc1
je	být	k5eAaImIp3nS
zodpovědná	zodpovědný	k2eAgFnSc1d1
za	za	k7c4
2	#num#	k4
000	#num#	k4
000	#num#	k4
vražd	vražda	k1gFnPc2
<g/>
,	,	kIx,
britský	britský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Mike	Mike	k1gInSc1
Dash	Dash	k1gMnSc1
odhaduje	odhadovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
asi	asi	k9
150	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vykořeněna	vykořenit	k5eAaPmNgFnS
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
thugu	thug	k1gMnSc6
Behramovi	Behram	k1gMnSc6
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejhoršího	zlý	k2eAgMnSc4d3
sériového	sériový	k2eAgMnSc4d1
vraha	vrah	k1gMnSc4
na	na	k7c6
světě	svět	k1gInSc6
-	-	kIx~
uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgMnSc1d1
za	za	k7c4
smrt	smrt	k1gFnSc4
931	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaj	údaj	k1gInSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
poměrně	poměrně	k6eAd1
sporný	sporný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kult	kult	k1gInSc1
thugů	thug	k1gMnPc2
byl	být	k5eAaImAgInS
Brity	Brit	k1gMnPc4
potlačen	potlačen	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1828	#num#	k4
<g/>
,	,	kIx,
velký	velký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
měl	mít	k5eAaImAgMnS
i	i	k9
generální	generální	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
lord	lord	k1gMnSc1
William	William	k1gInSc4
Bentinck	Bentinck	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zahájil	zahájit	k5eAaPmAgMnS
kampaň	kampaň	k1gFnSc4
zahrnující	zahrnující	k2eAgFnSc2d1
popravy	poprava	k1gFnSc2
a	a	k8xC
špionáže	špionáž	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
hlavním	hlavní	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
a	a	k8xC
organizátorem	organizátor	k1gMnSc7
britského	britský	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
proti	proti	k7c3
kultu	kult	k1gInSc3
byl	být	k5eAaImAgMnS
Bentinckův	Bentinckův	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
<g/>
,	,	kIx,
kapitán	kapitán	k1gMnSc1
William	William	k1gInSc1
Henry	Henry	k1gMnSc1
Sleeman	Sleeman	k1gMnSc1
(	(	kIx(
<g/>
1788	#num#	k4
<g/>
-	-	kIx~
<g/>
1856	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
stanovil	stanovit	k5eAaPmAgInS
strategii	strategie	k1gFnSc4
a	a	k8xC
rozbil	rozbít	k5eAaPmAgMnS
organizační	organizační	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
kultu	kult	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
zbytky	zbytek	k1gInPc1
kultu	kult	k1gInSc2
přetrvávaly	přetrvávat	k5eAaImAgInP
až	až	k9
do	do	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
navazující	navazující	k2eAgFnSc1d1
sekta	sekta	k1gFnSc1
vrahů	vrah	k1gMnPc2
dakoitů	dakoit	k1gInPc2
<g/>
,	,	kIx,
přežila	přežít	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
v	v	k7c6
Indii	Indie	k1gFnSc6
ale	ale	k8xC
stále	stále	k6eAd1
existuje	existovat	k5eAaImIp3nS
policejní	policejní	k2eAgInSc1d1
odbor	odbor	k1gInSc1
pro	pro	k7c4
potlačování	potlačování	k1gNnSc4
sekt	sekta	k1gFnPc2
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Anglii	Anglie	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
výraz	výraz	k1gInSc1
"	"	kIx"
<g/>
Thug	Thug	k1gInSc1
<g/>
"	"	kIx"
používán	používat	k5eAaImNgInS
pro	pro	k7c4
sportovního	sportovní	k2eAgMnSc4d1
fanouška	fanoušek	k1gMnSc4
<g/>
,	,	kIx,
mávajícího	mávající	k2eAgMnSc2d1
šálou	šála	k1gFnSc7
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Thuggee	Thugge	k1gInSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
