<s>
Republikánská	republikánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
radikální	radikální	k2eAgFnSc1d1
a	a	k8xC
radikálně-socialistická	radikálně-socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Parti	Parti	k1gNnSc1
Républicain	Républicain	k1gInSc1
Radical	Radical	k1gMnSc1
et	et	k?
Radical-Socialiste	Radical-Socialist	k1gMnSc5
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
francouzská	francouzský	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
existující	existující	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1901	#num#	k4
až	až	k9
1971	#num#	k4
<g/>
.	.	kIx.
</s>