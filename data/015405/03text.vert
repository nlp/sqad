<s>
Republikánská	republikánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
radikální	radikální	k2eAgFnSc1d1
a	a	k8xC
radikálně	radikálně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
radikální	radikální	k2eAgFnSc1d1
a	a	k8xC
radikálně-socialistická	radikálně-socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Parti	Parti	k1gNnSc1
Républicain	Républicain	k1gInSc1
Radical	Radical	k1gMnSc1
et	et	k?
Radical-Socialiste	Radical-Socialist	k1gMnSc5
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
francouzská	francouzský	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
existující	existující	k2eAgFnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1901	#num#	k4
až	až	k9
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poněkud	poněkud	k6eAd1
komplikovaný	komplikovaný	k2eAgInSc4d1
název	název	k1gInSc4
vypovídá	vypovídat	k5eAaPmIp3nS,k5eAaImIp3nS
o	o	k7c6
ideologickém	ideologický	k2eAgNnSc6d1
založení	založení	k1gNnSc6
strany	strana	k1gFnSc2
<g/>
:	:	kIx,
demokratický	demokratický	k2eAgInSc1d1
republikanismus	republikanismus	k1gInSc1
<g/>
,	,	kIx,
radikalismus	radikalismus	k1gInSc1
(	(	kIx(
<g/>
liberalismus	liberalismus	k1gInSc1
<g/>
,	,	kIx,
sekularismus	sekularismus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
nemarxistický	marxistický	k2eNgInSc1d1
socialismus	socialismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
strávila	strávit	k5eAaPmAgFnS
strana	strana	k1gFnSc1
na	na	k7c6
pomyslném	pomyslný	k2eAgInSc6d1
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
poválečných	poválečný	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
ale	ale	k9
spíše	spíše	k9
otočila	otočit	k5eAaPmAgFnS
k	k	k7c3
pravému	pravý	k2eAgInSc3d1
středu	střed	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názorový	názorový	k2eAgInSc1d1
rozpor	rozpor	k1gInSc1
vedl	vést	k5eAaImAgInS
k	k	k7c3
rozpadu	rozpad	k1gInSc3
strany	strana	k1gFnSc2
<g/>
:	:	kIx,
levice	levice	k1gFnSc1
založila	založit	k5eAaPmAgFnS
Hnutí	hnutí	k1gNnSc4
radikálně	radikálně	k6eAd1
socialistické	socialistický	k2eAgFnSc2d1
levice	levice	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Radikálně	radikálně	k6eAd1
levicová	levicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pravice	pravice	k1gFnSc1
založila	založit	k5eAaPmAgFnS
Radikální	radikální	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc7d1
nástupkyní	nástupkyně	k1gFnSc7
staré	starý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
číslování	číslování	k1gNnSc1
sjezdů	sjezd	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2297	#num#	k4
4410	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88612418	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
154317689	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88612418	#num#	k4
</s>
