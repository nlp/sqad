<s desamb="1">
Názorový	názorový	k2eAgInSc1d1
rozpor	rozpor	k1gInSc1
vedl	vést	k5eAaImAgInS
k	k	k7c3
rozpadu	rozpad	k1gInSc3
strany	strana	k1gFnSc2
<g/>
:	:	kIx,
levice	levice	k1gFnSc1
založila	založit	k5eAaPmAgFnS
Hnutí	hnutí	k1gNnSc4
radikálně	radikálně	k6eAd1
socialistické	socialistický	k2eAgFnSc2d1
levice	levice	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Radikálně	radikálně	k6eAd1
levicová	levicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pravice	pravice	k1gFnSc1
založila	založit	k5eAaPmAgFnS
Radikální	radikální	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc7d1
nástupkyní	nástupkyně	k1gFnSc7
staré	starý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
číslování	číslování	k1gNnSc1
sjezdů	sjezd	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>