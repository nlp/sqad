<s>
Smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
úmrtí	úmrtí	k1gNnPc1	úmrtí
<g/>
,	,	kIx,	,
skon	skon	k1gInSc1	skon
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
exitus	exitus	k1gInSc1	exitus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
z	z	k7c2	z
biologického	biologický	k2eAgNnSc2d1	biologické
a	a	k8xC	a
lékařského	lékařský	k2eAgNnSc2d1	lékařské
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
)	)	kIx)	)
zastavení	zastavení	k1gNnSc1	zastavení
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
nevratnými	vratný	k2eNgFnPc7d1	nevratná
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obnovení	obnovení	k1gNnSc1	obnovení
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
stav	stav	k1gInSc1	stav
organismu	organismus	k1gInSc2	organismus
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
úplná	úplný	k2eAgFnSc1d1	úplná
a	a	k8xC	a
trvalá	trvalý	k2eAgFnSc1d1	trvalá
ztráta	ztráta	k1gFnSc1	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Umírání	umírání	k1gNnSc1	umírání
je	být	k5eAaImIp3nS	být
postupný	postupný	k2eAgInSc4d1	postupný
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
nelze	lze	k6eNd1	lze
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
umíráním	umírání	k1gNnSc7	umírání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
umírání	umírání	k1gNnSc1	umírání
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
fází	fáze	k1gFnPc2	fáze
života	život	k1gInSc2	život
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
nastává	nastávat	k5eAaImIp3nS	nastávat
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
živého	živý	k2eAgInSc2d1	živý
organismu	organismus	k1gInSc2	organismus
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
věku	věk	k1gInSc6	věk
a	a	k8xC	a
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
ji	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
dopředu	dopředu	k6eAd1	dopředu
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
druh	druh	k1gInSc4	druh
organismu	organismus	k1gInSc2	organismus
<g/>
;	;	kIx,	;
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
genetického	genetický	k2eAgInSc2d1	genetický
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
výrazně	výrazně	k6eAd1	výrazně
ji	on	k3xPp3gFnSc4	on
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
i	i	k8xC	i
vnější	vnější	k2eAgFnPc4d1	vnější
okolnosti	okolnost	k1gFnPc4	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
-	-	kIx~	-
díky	díky	k7c3	díky
kvalitnější	kvalitní	k2eAgFnSc3d2	kvalitnější
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
péči	péče	k1gFnSc3	péče
<g/>
,	,	kIx,	,
hygieně	hygiena	k1gFnSc3	hygiena
a	a	k8xC	a
zdravějšímu	zdravý	k2eAgInSc3d2	zdravější
životnímu	životní	k2eAgInSc3d1	životní
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
má	mít	k5eAaImIp3nS	mít
smrtelné	smrtelný	k2eAgInPc4d1	smrtelný
následky	následek	k1gInPc4	následek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
určitá	určitý	k2eAgFnSc1d1	určitá
dávka	dávka	k1gFnSc1	dávka
jedu	jed	k1gInSc2	jed
<g/>
,	,	kIx,	,
nevhodný	vhodný	k2eNgInSc4d1	nevhodný
léčebný	léčebný	k2eAgInSc4d1	léčebný
postup	postup	k1gInSc4	postup
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
letální	letální	k2eAgNnSc1d1	letální
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
smrtelné	smrtelný	k2eAgFnPc1d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
umíráním	umírání	k1gNnSc7	umírání
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
thanatologie	thanatologie	k1gFnSc1	thanatologie
<g/>
.	.	kIx.	.
</s>
<s>
Usnadněním	usnadnění	k1gNnSc7	usnadnění
umírání	umírání	k1gNnSc2	umírání
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
paliativní	paliativní	k2eAgFnSc1d1	paliativní
péče	péče	k1gFnSc1	péče
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
smrt	smrt	k1gFnSc4	smrt
používá	používat	k5eAaImIp3nS	používat
prakticky	prakticky	k6eAd1	prakticky
výhradně	výhradně	k6eAd1	výhradně
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
organismu	organismus	k1gInSc3	organismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
a	a	k8xC	a
lékařství	lékařství	k1gNnSc1	lékařství
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
například	například	k6eAd1	například
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
buněčná	buněčný	k2eAgFnSc1d1	buněčná
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
smrt	smrt	k1gFnSc1	smrt
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
části	část	k1gFnSc3	část
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Zastavení	zastavení	k1gNnSc1	zastavení
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
včasným	včasný	k2eAgInSc7d1	včasný
vhodným	vhodný	k2eAgInSc7d1	vhodný
zásahem	zásah	k1gInSc7	zásah
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
klinická	klinický	k2eAgFnSc1d1	klinická
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
smrt	smrt	k1gFnSc1	smrt
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
pro	pro	k7c4	pro
zánik	zánik	k1gInSc4	zánik
-	-	kIx~	-
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
politického	politický	k2eAgInSc2d1	politický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
živočišného	živočišný	k2eAgInSc2d1	živočišný
druhu	druh	k1gInSc2	druh
apod.	apod.	kA	apod.
Expirace	Expirace	k1gFnSc2	Expirace
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgNnSc1d1	poslední
vydechnutí	vydechnutí	k1gNnSc1	vydechnutí
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
klinickou	klinický	k2eAgFnSc4d1	klinická
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
zástavou	zástava	k1gFnSc7	zástava
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
také	také	k9	také
exitus	exitus	k1gInSc1	exitus
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
smrt	smrt	k1gFnSc1	smrt
diagnostikovala	diagnostikovat	k5eAaBmAgFnS	diagnostikovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zástavy	zástava	k1gFnSc2	zástava
dechu	dech	k1gInSc2	dech
a	a	k8xC	a
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
ukazatelů	ukazatel	k1gInPc2	ukazatel
používá	používat	k5eAaImIp3nS	používat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Podstatný	podstatný	k2eAgInSc1d1	podstatný
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oproti	oproti	k7c3	oproti
dřívějšku	dřívějšek	k1gInSc3	dřívějšek
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zástava	zástava	k1gFnSc1	zástava
dechu	dech	k1gInSc2	dech
a	a	k8xC	a
srdce	srdce	k1gNnSc2	srdce
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
definitivní	definitivní	k2eAgNnSc1d1	definitivní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
smrt	smrt	k1gFnSc1	smrt
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgMnPc2	tento
ukazatelů	ukazatel	k1gMnPc2	ukazatel
určuje	určovat	k5eAaImIp3nS	určovat
teprve	teprve	k6eAd1	teprve
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
lékař	lékař	k1gMnSc1	lékař
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dané	daný	k2eAgFnPc4d1	daná
funkce	funkce	k1gFnPc4	funkce
nelze	lze	k6eNd1	lze
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
neexistují	existovat	k5eNaImIp3nP	existovat
jiné	jiný	k2eAgFnPc1d1	jiná
významné	významný	k2eAgFnPc1d1	významná
okolnosti	okolnost	k1gFnPc1	okolnost
prokazující	prokazující	k2eAgNnSc1d1	prokazující
úmrtí	úmrtí	k1gNnSc3	úmrtí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
obou	dva	k4xCgFnPc2	dva
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kardiopulmonální	kardiopulmonální	k2eAgFnSc1d1	kardiopulmonální
resuscitace	resuscitace	k1gFnSc1	resuscitace
<g/>
,	,	kIx,	,
zástava	zástava	k1gFnSc1	zástava
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
zástava	zástava	k1gFnSc1	zástava
dechu	dech	k1gInSc2	dech
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
zavedl	zavést	k5eAaPmAgInS	zavést
termín	termín	k1gInSc1	termín
mozková	mozkový	k2eAgFnSc1d1	mozková
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jako	jako	k8xS	jako
nevratné	vratný	k2eNgNnSc1d1	nevratné
vymizení	vymizení	k1gNnSc1	vymizení
všech	všecek	k3xTgFnPc2	všecek
funkcí	funkce	k1gFnPc2	funkce
celého	celý	k2eAgInSc2d1	celý
mozku	mozek	k1gInSc2	mozek
<g/>
..	..	k?	..
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
mozek	mozek	k1gInSc4	mozek
odumírá	odumírat	k5eAaImIp3nS	odumírat
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
dýchání	dýchání	k1gNnSc2	dýchání
nebo	nebo	k8xC	nebo
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
podchlazen	podchlazen	k2eAgMnSc1d1	podchlazen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
ledové	ledový	k2eAgFnSc6d1	ledová
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
mozková	mozkový	k2eAgFnSc1d1	mozková
smrt	smrt	k1gFnSc1	smrt
oddálit	oddálit	k5eAaPmF	oddálit
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
topící	topící	k2eAgFnSc2d1	topící
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
oživen	oživit	k5eAaPmNgMnS	oživit
až	až	k9	až
po	po	k7c6	po
20	[number]	k4	20
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
nějaké	nějaký	k3yIgInPc4	nějaký
následky	následek	k1gInPc4	následek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgInPc1d1	různý
negativní	negativní	k2eAgInPc1d1	negativní
vlivy	vliv	k1gInPc1	vliv
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hypoxie	hypoxie	k1gFnSc2	hypoxie
<g/>
,	,	kIx,	,
otřes	otřes	k1gInSc4	otřes
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
postihují	postihovat	k5eAaImIp3nP	postihovat
nejdříve	dříve	k6eAd3	dříve
vývojově	vývojově	k6eAd1	vývojově
mladší	mladý	k2eAgFnSc3d2	mladší
části	část	k1gFnSc3	část
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
šedou	šedý	k2eAgFnSc4d1	šedá
kůru	kůra	k1gFnSc4	kůra
mozkovou	mozkový	k2eAgFnSc4d1	mozková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
apalickému	apalický	k2eAgInSc3d1	apalický
syndromu	syndrom	k1gInSc3	syndrom
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ani	ani	k8xC	ani
EEG	EEG	kA	EEG
nezaznamenává	zaznamenávat	k5eNaImIp3nS	zaznamenávat
žádné	žádný	k3yNgFnPc4	žádný
mozkové	mozkový	k2eAgFnPc4d1	mozková
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
hluboká	hluboký	k2eAgNnPc1d1	hluboké
životně	životně	k6eAd1	životně
důležitá	důležitý	k2eAgNnPc1d1	důležité
mozková	mozkový	k2eAgNnPc1d1	mozkové
centra	centrum	k1gNnPc1	centrum
stále	stále	k6eAd1	stále
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
mozkové	mozkový	k2eAgFnSc2d1	mozková
smrti	smrt	k1gFnSc2	smrt
vyšetření	vyšetření	k1gNnSc2	vyšetření
EEG	EEG	kA	EEG
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
ještě	ještě	k9	ještě
např.	např.	kA	např.
vyšetřením	vyšetření	k1gNnSc7	vyšetření
kmenových	kmenový	k2eAgInPc2d1	kmenový
evokovaných	evokovaný	k2eAgInPc2d1	evokovaný
potenciálů	potenciál	k1gInPc2	potenciál
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
např.	např.	kA	např.
funkčnost	funkčnost	k1gFnSc4	funkčnost
sluchových	sluchový	k2eAgFnPc2d1	sluchová
drah	draha	k1gFnPc2	draha
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
(	(	kIx(	(
<g/>
BAEP	BAEP	kA	BAEP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
právně	právně	k6eAd1	právně
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zástavy	zástava	k1gFnSc2	zástava
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
dechové	dechový	k2eAgFnSc2d1	dechová
zástavy	zástava	k1gFnSc2	zástava
<g/>
,	,	kIx,	,
zjištěním	zjištění	k1gNnSc7	zjištění
dilatace	dilatace	k1gFnSc2	dilatace
(	(	kIx(	(
<g/>
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
)	)	kIx)	)
zorniček	zornička	k1gFnPc2	zornička
nereagujících	reagující	k2eNgFnPc2d1	nereagující
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
či	či	k8xC	či
diagnózou	diagnóza	k1gFnSc7	diagnóza
mozkové	mozkový	k2eAgFnSc2d1	mozková
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnSc1d1	informační
teorie	teorie	k1gFnSc1	teorie
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc4	termín
informačně	informačně	k6eAd1	informačně
teoretická	teoretický	k2eAgFnSc1d1	teoretická
smrt	smrt	k1gFnSc1	smrt
pro	pro	k7c4	pro
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
žádnými	žádný	k3yNgFnPc7	žádný
přístroji	přístroj	k1gInPc7	přístroj
obnovit	obnovit	k5eAaPmF	obnovit
či	či	k8xC	či
zachovat	zachovat	k5eAaPmF	zachovat
život	život	k1gInSc4	život
a	a	k8xC	a
u	u	k7c2	u
dané	daný	k2eAgFnSc2d1	daná
osoby	osoba	k1gFnSc2	osoba
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
obnovit	obnovit	k5eAaPmF	obnovit
mysl	mysl	k1gFnSc4	mysl
a	a	k8xC	a
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
legislativě	legislativa	k1gFnSc6	legislativa
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
definována	definovat	k5eAaBmNgFnS	definovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
§	§	k?	§
2	[number]	k4	2
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
e	e	k0	e
<g/>
)	)	kIx)	)
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
285	[number]	k4	285
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
darování	darování	k1gNnSc6	darování
<g/>
,	,	kIx,	,
odběrech	odběr	k1gInPc6	odběr
a	a	k8xC	a
transplantacích	transplantace	k1gFnPc6	transplantace
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
transplantační	transplantační	k2eAgInSc1d1	transplantační
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
§	§	k?	§
10	[number]	k4	10
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
téhož	týž	k3xTgInSc2	týž
zákona	zákon	k1gInSc2	zákon
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
dlouho	dlouho	k6eAd1	dlouho
nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
který	který	k3yIgMnSc1	který
zcela	zcela	k6eAd1	zcela
jistě	jistě	k6eAd1	jistě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
to	ten	k3xDgNnSc4	ten
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
příbuzných	příbuzný	k1gMnPc2	příbuzný
soudně	soudně	k6eAd1	soudně
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
smrti	smrt	k1gFnSc2	smrt
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
následující	následující	k2eAgNnSc4d1	následující
<g/>
:	:	kIx,	:
podlehnutí	podlehnutí	k1gNnSc4	podlehnutí
nemoci	moct	k5eNaImF	moct
smrt	smrt	k1gFnSc4	smrt
stářím	stáří	k1gNnSc7	stáří
a	a	k8xC	a
sešlostí	sešlost	k1gFnSc7	sešlost
-	-	kIx~	-
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
přirozená	přirozený	k2eAgFnSc1d1	přirozená
smrt	smrt	k1gFnSc1	smrt
smrt	smrt	k1gFnSc1	smrt
způsobená	způsobený	k2eAgFnSc1d1	způsobená
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vinou	vina	k1gFnSc7	vina
<g/>
:	:	kIx,	:
sebevražda	sebevražda	k1gFnSc1	sebevražda
-	-	kIx~	-
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
ukončení	ukončení	k1gNnSc1	ukončení
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
sebezabití	sebezabití	k1gNnSc1	sebezabití
-	-	kIx~	-
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
sám	sám	k3xTgMnSc1	sám
způsobí	způsobit	k5eAaPmIp3nS	způsobit
nedbalostí	nedbalost	k1gFnSc7	nedbalost
<g/>
,	,	kIx,	,
neopatrností	neopatrnost	k1gFnSc7	neopatrnost
<g/>
,	,	kIx,	,
nedopatřením	nedopatření	k1gNnSc7	nedopatření
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
např.	např.	kA	např.
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
otrava	otrava	k1gFnSc1	otrava
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
úraz	úraz	k1gInSc1	úraz
při	při	k7c6	při
sportu	sport	k1gInSc6	sport
eutanazie	eutanazie	k1gFnSc1	eutanazie
-	-	kIx~	-
asistovaná	asistovaný	k2eAgFnSc1d1	asistovaná
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
,	,	kIx,	,
usmrcení	usmrcení	k1gNnSc1	usmrcení
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
podmínek	podmínka	k1gFnPc2	podmínka
neslučitelných	slučitelný	k2eNgFnPc2d1	neslučitelná
se	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
:	:	kIx,	:
smrt	smrt	k1gFnSc1	smrt
hladem	hlad	k1gInSc7	hlad
smrt	smrt	k1gFnSc1	smrt
žízní	žíznit	k5eAaImIp3nP	žíznit
smrt	smrt	k1gFnSc4	smrt
podchlazením	podchlazení	k1gNnSc7	podchlazení
udušení	udušení	k1gNnSc4	udušení
utonutí	utonutí	k1gNnSc2	utonutí
uhoření	uhoření	k1gNnSc2	uhoření
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
úraz	úraz	k1gInSc1	úraz
-	-	kIx~	-
např.	např.	kA	např.
zavalení	zavalení	k1gNnSc1	zavalení
kamením	kamenit	k5eAaImIp1nS	kamenit
<g/>
,	,	kIx,	,
zabití	zabití	k1gNnSc1	zabití
<g />
.	.	kIx.	.
</s>
<s>
padajícím	padající	k2eAgInSc7d1	padající
stromem	strom	k1gInSc7	strom
apod.	apod.	kA	apod.
smrt	smrt	k1gFnSc1	smrt
způsobená	způsobený	k2eAgFnSc1d1	způsobená
jiným	jiný	k1gMnSc7	jiný
člověkem	člověk	k1gMnSc7	člověk
<g/>
:	:	kIx,	:
zabití	zabití	k1gNnSc3	zabití
-	-	kIx~	-
smrt	smrt	k1gFnSc1	smrt
způsobená	způsobený	k2eAgFnSc1d1	způsobená
jiným	jiný	k1gMnSc7	jiný
člověkem	člověk	k1gMnSc7	člověk
nedbalostí	nedbalost	k1gFnPc2	nedbalost
<g/>
,	,	kIx,	,
neopatrností	neopatrnost	k1gFnPc2	neopatrnost
<g/>
,	,	kIx,	,
nedopatřením	nedopatření	k1gNnSc7	nedopatření
-	-	kIx~	-
např.	např.	kA	např.
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
vražda	vražda	k1gFnSc1	vražda
-	-	kIx~	-
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
úmyslně	úmyslně	k6eAd1	úmyslně
jiným	jiný	k2eAgMnSc7d1	jiný
člověkem	člověk	k1gMnSc7	člověk
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
smrt	smrt	k1gFnSc1	smrt
způsobí	způsobit	k5eAaPmIp3nS	způsobit
člověku	člověk	k1gMnSc3	člověk
zvíře	zvíře	k1gNnSc4	zvíře
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
úmyslně	úmyslně	k6eAd1	úmyslně
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
spíše	spíše	k9	spíše
o	o	k7c6	o
zabití	zabití	k1gNnSc6	zabití
než	než	k8xS	než
vraždě	vražda	k1gFnSc6	vražda
poprava	poprava	k1gFnSc1	poprava
-	-	kIx~	-
vykonání	vykonání	k1gNnSc1	vykonání
rozsudku	rozsudek	k1gInSc2	rozsudek
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
smrt	smrt	k1gFnSc1	smrt
způsobená	způsobený	k2eAgFnSc1d1	způsobená
zvířetem	zvíře	k1gNnSc7	zvíře
(	(	kIx(	(
<g/>
predátor	predátor	k1gMnSc1	predátor
<g/>
,	,	kIx,	,
parazit	parazit	k1gMnSc1	parazit
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgMnSc1d1	bránící
se	se	k3xPyFc4	se
živočich	živočich	k1gMnSc1	živočich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rostlinou	rostlina	k1gFnSc7	rostlina
(	(	kIx(	(
<g/>
otrava	otrava	k1gFnSc1	otrava
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
způsoby	způsob	k1gInPc1	způsob
úmrtí	úmrtí	k1gNnSc2	úmrtí
mohou	moct	k5eAaImIp3nP	moct
někdy	někdy	k6eAd1	někdy
poněkud	poněkud	k6eAd1	poněkud
splývat	splývat	k5eAaImF	splývat
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
závažným	závažný	k2eAgInSc7d1	závažný
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
odlišení	odlišení	k1gNnSc1	odlišení
vraždy	vražda	k1gFnSc2	vražda
(	(	kIx(	(
<g/>
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
zabití	zabití	k1gNnSc1	zabití
<g/>
)	)	kIx)	)
od	od	k7c2	od
zabití	zabití	k1gNnSc2	zabití
(	(	kIx(	(
<g/>
neúmyslné	úmyslný	k2eNgNnSc1d1	neúmyslné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
někdy	někdy	k6eAd1	někdy
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
snadné	snadný	k2eAgNnSc1d1	snadné
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
smrt	smrt	k1gFnSc4	smrt
způsobil	způsobit	k5eAaPmAgMnS	způsobit
neopatrností	neopatrnost	k1gFnPc2	neopatrnost
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
vnějšími	vnější	k2eAgFnPc7d1	vnější
příčinami	příčina	k1gFnPc7	příčina
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
eutanazie	eutanazie	k1gFnPc1	eutanazie
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
blížit	blížit	k5eAaImF	blížit
vraždě	vražda	k1gFnSc3	vražda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
zabití	zabití	k1gNnSc1	zabití
a	a	k8xC	a
vražda	vražda	k1gFnSc1	vražda
taktéž	taktéž	k?	taktéž
splývají	splývat	k5eAaImIp3nP	splývat
atd.	atd.	kA	atd.
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
organizmus	organizmus	k1gInSc1	organizmus
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
kombinaci	kombinace	k1gFnSc4	kombinace
několika	několik	k4yIc2	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
zemře	zemřít	k5eAaPmIp3nS	zemřít
bez	bez	k7c2	bez
předchozích	předchozí	k2eAgInPc2d1	předchozí
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
náhlá	náhlý	k2eAgFnSc1d1	náhlá
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stárnutí	stárnutí	k1gNnSc2	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Stárnutí	stárnutí	k1gNnSc1	stárnutí
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
běžnou	běžný	k2eAgFnSc7d1	běžná
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
důvod	důvod	k1gInSc1	důvod
spíše	spíše	k9	spíše
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Vymezit	vymezit	k5eAaPmF	vymezit
stárnutí	stárnutí	k1gNnSc4	stárnutí
není	být	k5eNaImIp3nS	být
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
stárnutí	stárnutí	k1gNnSc1	stárnutí
začíná	začínat	k5eAaImIp3nS	začínat
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
již	již	k6eAd1	již
po	po	k7c6	po
konci	konec	k1gInSc6	konec
puberty	puberta	k1gFnSc2	puberta
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
hypotéz	hypotéza	k1gFnPc2	hypotéza
vysvětlujících	vysvětlující	k2eAgNnPc2d1	vysvětlující
stárnutí	stárnutí	k1gNnPc2	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
nahromaďují	nahromaďovat	k5eAaImIp3nP	nahromaďovat
mutace	mutace	k1gFnPc1	mutace
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
množením	množení	k1gNnSc7	množení
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
postupně	postupně	k6eAd1	postupně
opotřebují	opotřebovat	k5eAaPmIp3nP	opotřebovat
telomery	telomera	k1gFnPc1	telomera
na	na	k7c6	na
konci	konec	k1gInSc6	konec
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
biologického	biologický	k2eAgNnSc2d1	biologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
smrti	smrt	k1gFnSc2	smrt
odstranění	odstranění	k1gNnSc2	odstranění
opotřebovaných	opotřebovaný	k2eAgInPc2d1	opotřebovaný
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
uvolnění	uvolnění	k1gNnSc4	uvolnění
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
nastupující	nastupující	k2eAgFnSc3d1	nastupující
generaci	generace	k1gFnSc3	generace
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
sociální	sociální	k2eAgInSc4d1	sociální
význam	význam	k1gInSc4	význam
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
vede	vést	k5eAaImIp3nS	vést
smrt	smrt	k1gFnSc1	smrt
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
uspořádání	uspořádání	k1gNnSc3	uspořádání
její	její	k3xOp3gFnSc2	její
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
funkcí	funkce	k1gFnPc2	funkce
jejích	její	k3xOp3gInPc2	její
členů	člen	k1gInPc2	člen
-	-	kIx~	-
např.	např.	kA	např.
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
částečně	částečně	k6eAd1	částečně
nahradí	nahradit	k5eAaPmIp3nS	nahradit
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
otce	otec	k1gMnSc4	otec
apod.	apod.	kA	apod.
</s>
<s>
Chorobný	chorobný	k2eAgInSc4d1	chorobný
strach	strach	k1gInSc4	strach
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
thanatofobie	thanatofobie	k1gFnSc1	thanatofobie
<g/>
,	,	kIx,	,
strach	strach	k1gInSc1	strach
z	z	k7c2	z
mrtvol	mrtvola	k1gFnPc2	mrtvola
je	být	k5eAaImIp3nS	být
nekrofobie	nekrofobie	k1gFnSc1	nekrofobie
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
jako	jako	k8xS	jako
významná	významný	k2eAgFnSc1d1	významná
událost	událost	k1gFnSc1	událost
bývá	bývat	k5eAaImIp3nS	bývat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
oznamována	oznamován	k2eAgFnSc1d1	oznamována
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
,	,	kIx,	,
známým	známý	k1gMnPc3	známý
i	i	k8xC	i
lidem	člověk	k1gMnPc3	člověk
z	z	k7c2	z
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
nejčastěji	často	k6eAd3	často
oznámena	oznámen	k2eAgFnSc1d1	oznámena
vyvěšením	vyvěšení	k1gNnSc7	vyvěšení
a	a	k8xC	a
rozesláním	rozeslání	k1gNnSc7	rozeslání
tzv.	tzv.	kA	tzv.
parte	parte	k1gNnSc1	parte
<g/>
,	,	kIx,	,
oznámení	oznámení	k1gNnSc1	oznámení
o	o	k7c6	o
úmrtí	úmrtí	k1gNnSc6	úmrtí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pořizuje	pořizovat	k5eAaImIp3nS	pořizovat
rodina	rodina	k1gFnSc1	rodina
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
člověka	člověk	k1gMnSc2	člověk
nechalo	nechat	k5eAaPmAgNnS	nechat
vyzvánět	vyzvánět	k5eAaImF	vyzvánět
zvonem	zvon	k1gInSc7	zvon
-	-	kIx~	-
umíráčkem	umíráček	k1gInSc7	umíráček
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
frekvence	frekvence	k1gFnSc1	frekvence
zvonění	zvonění	k1gNnSc2	zvonění
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
dalo	dát	k5eAaPmAgNnS	dát
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
významná	významný	k2eAgFnSc1d1	významná
osoba	osoba	k1gFnSc1	osoba
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
úmrtí	úmrtí	k1gNnSc6	úmrtí
panovníka	panovník	k1gMnSc4	panovník
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
vyzvánělo	vyzvánět	k5eAaImAgNnS	vyzvánět
všemi	všecek	k3xTgInPc7	všecek
kostelními	kostelní	k2eAgInPc7d1	kostelní
zvony	zvon	k1gInPc7	zvon
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úkolům	úkol	k1gInPc3	úkol
zvoníka	zvoník	k1gMnSc2	zvoník
také	také	k9	také
patřilo	patřit	k5eAaImAgNnS	patřit
napsat	napsat	k5eAaPmF	napsat
jméno	jméno	k1gNnSc1	jméno
zemřelé	zemřelý	k2eAgFnSc2d1	zemřelá
osoby	osoba	k1gFnSc2	osoba
křídou	křída	k1gFnSc7	křída
na	na	k7c4	na
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
,	,	kIx,	,
vyvěšenou	vyvěšený	k2eAgFnSc4d1	vyvěšená
na	na	k7c6	na
kostelní	kostelní	k2eAgFnSc6d1	kostelní
věži	věž	k1gFnSc6	věž
-	-	kIx~	-
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
předchůdce	předchůdce	k1gMnSc4	předchůdce
dnešních	dnešní	k2eAgFnPc2d1	dnešní
parte	parte	k1gNnPc2	parte
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vyzvánění	vyzvánění	k1gNnSc2	vyzvánění
všemi	všecek	k3xTgInPc7	všecek
zvony	zvon	k1gInPc7	zvon
se	se	k3xPyFc4	se
smrt	smrt	k1gFnSc1	smrt
významné	významný	k2eAgFnSc2d1	významná
osoby	osoba	k1gFnSc2	osoba
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyvěšením	vyvěšení	k1gNnSc7	vyvěšení
černé	černý	k2eAgFnSc2d1	černá
vlajky	vlajka	k1gFnSc2	vlajka
nebo	nebo	k8xC	nebo
stažením	stažení	k1gNnSc7	stažení
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vzpomínce	vzpomínka	k1gFnSc3	vzpomínka
na	na	k7c4	na
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
drží	držet	k5eAaImIp3nS	držet
tzv.	tzv.	kA	tzv.
minuta	minuta	k1gFnSc1	minuta
ticha	ticho	k1gNnSc2	ticho
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
rituál	rituál	k1gInSc1	rituál
posledního	poslední	k2eAgNnSc2d1	poslední
rozloučení	rozloučení	k1gNnSc2	rozloučení
se	s	k7c7	s
zemřelým	zemřelý	k2eAgMnSc7d1	zemřelý
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
symbolické	symbolický	k2eAgNnSc1d1	symbolické
vymezení	vymezení	k1gNnSc1	vymezení
jeho	jeho	k3xOp3gInSc2	jeho
odchodu	odchod	k1gInSc2	odchod
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
každé	každý	k3xTgNnSc1	každý
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
mají	mít	k5eAaImIp3nP	mít
pohřební	pohřební	k2eAgInSc4d1	pohřební
ceremoniál	ceremoniál	k1gInSc4	ceremoniál
a	a	k8xC	a
pohřební	pohřební	k2eAgInPc1d1	pohřební
zvyky	zvyk	k1gInPc1	zvyk
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
rysech	rys	k1gInPc6	rys
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
podobají	podobat	k5eAaImIp3nP	podobat
-	-	kIx~	-
například	například	k6eAd1	například
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
poukáže	poukázat	k5eAaPmIp3nS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
zemřelého	zemřelý	k2eAgNnSc2d1	zemřelé
přetrvá	přetrvat	k5eAaPmIp3nS	přetrvat
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
duše	duše	k1gFnSc1	duše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
světonázoru	světonázor	k1gInSc6	světonázor
posuzujícího	posuzující	k2eAgInSc2d1	posuzující
ukončením	ukončení	k1gNnSc7	ukončení
života	život	k1gInSc2	život
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc7	jeho
proměnou	proměna	k1gFnSc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
jak	jak	k6eAd1	jak
dobově	dobově	k6eAd1	dobově
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
teritoriálně	teritoriálně	k6eAd1	teritoriálně
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
výchově	výchova	k1gFnSc6	výchova
<g/>
,	,	kIx,	,
víře	víra	k1gFnSc6	víra
i	i	k8xC	i
životní	životní	k2eAgFnSc3d1	životní
filozofii	filozofie	k1gFnSc3	filozofie
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
domácnosti	domácnost	k1gFnSc6	domácnost
více	hodně	k6eAd2	hodně
generací	generace	k1gFnPc2	generace
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
člena	člen	k1gMnSc2	člen
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
běžnější	běžný	k2eAgFnSc1d2	běžnější
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
celkem	celkem	k6eAd1	celkem
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
chápána	chápán	k2eAgFnSc1d1	chápána
jako	jako	k8xS	jako
nutná	nutný	k2eAgFnSc1d1	nutná
součást	součást	k1gFnSc1	součást
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc4	smrt
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
tabuizována	tabuizovat	k5eAaBmNgNnP	tabuizovat
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
chápána	chápán	k2eAgFnSc1d1	chápána
tragicky	tragicky	k6eAd1	tragicky
a	a	k8xC	a
očekávána	očekáván	k2eAgFnSc1d1	očekávána
s	s	k7c7	s
obavami	obava	k1gFnPc7	obava
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
náboženské	náboženský	k2eAgFnPc1d1	náboženská
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
filosofické	filosofický	k2eAgInPc1d1	filosofický
systémy	systém	k1gInPc1	systém
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
velice	velice	k6eAd1	velice
důkladně	důkladně	k6eAd1	důkladně
a	a	k8xC	a
konzistentně	konzistentně	k6eAd1	konzistentně
rozpracovány	rozpracován	k2eAgFnPc4d1	rozpracována
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
smrt	smrt	k1gFnSc1	smrt
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
bytí	bytí	k1gNnSc4	bytí
a	a	k8xC	a
jaký	jaký	k3yRgInSc1	jaký
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc1d1	další
osud	osud	k1gInSc1	osud
duše	duše	k1gFnSc2	duše
dotyčného	dotyčný	k2eAgInSc2d1	dotyčný
-	-	kIx~	-
pokud	pokud	k8xS	pokud
připouštějí	připouštět	k5eAaImIp3nP	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
jako	jako	k8xS	jako
nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
duše	duše	k1gFnSc1	duše
vůbec	vůbec	k9	vůbec
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnohá	mnohý	k2eAgNnPc4d1	mnohé
náboženství	náboženství	k1gNnPc4	náboženství
je	být	k5eAaImIp3nS	být
ostatně	ostatně	k6eAd1	ostatně
příprava	příprava	k1gFnSc1	příprava
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
"	"	kIx"	"
<g/>
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
"	"	kIx"	"
jejich	jejich	k3xOp3gFnPc2	jejich
hlavních	hlavní	k2eAgFnPc2d1	hlavní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Prožitek	prožitek	k1gInSc1	prožitek
blízké	blízký	k2eAgFnSc2d1	blízká
smrti	smrt	k1gFnSc2	smrt
-	-	kIx~	-
Oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgFnPc3d1	předchozí
variantám	varianta	k1gFnPc3	varianta
<g/>
,	,	kIx,	,
varianta	varianta	k1gFnSc1	varianta
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
je	být	k5eAaImIp3nS	být
nejvíc	nejvíc	k6eAd1	nejvíc
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
hodnověrných	hodnověrný	k2eAgFnPc2d1	hodnověrná
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teoretického	teoretický	k2eAgNnSc2d1	teoretické
ta	ten	k3xDgNnPc1	ten
nejvíce	nejvíce	k6eAd1	nejvíce
podložená	podložený	k2eAgNnPc1d1	podložené
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
forma	forma	k1gFnSc1	forma
bytí	bytí	k1gNnSc2	bytí
-	-	kIx~	-
Řada	řada	k1gFnSc1	řada
náboženských	náboženský	k2eAgInPc2d1	náboženský
systémů	systém	k1gInPc2	systém
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
existenci	existence	k1gFnSc4	existence
nehmotné	hmotný	k2eNgFnSc2d1	nehmotná
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přetrvá	přetrvat	k5eAaPmIp3nS	přetrvat
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mimo	mimo	k7c4	mimo
hmotný	hmotný	k2eAgInSc4d1	hmotný
svět	svět	k1gInSc4	svět
živých	živý	k2eAgMnPc2d1	živý
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
s	s	k7c7	s
koncepty	koncept	k1gInPc7	koncept
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
pohled	pohled	k1gInSc4	pohled
postulují	postulovat	k5eAaImIp3nP	postulovat
abrahámovská	abrahámovský	k2eAgNnPc1d1	abrahámovské
náboženství	náboženství	k1gNnPc1	náboženství
(	(	kIx(	(
<g/>
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
judaismus	judaismus	k1gInSc1	judaismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
,	,	kIx,	,
taoismus	taoismus	k1gInSc1	taoismus
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
odchodem	odchod	k1gInSc7	odchod
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
za	za	k7c7	za
lepší	dobrý	k2eAgFnSc7d2	lepší
a	a	k8xC	a
krásnější	krásný	k2eAgFnSc7d2	krásnější
částí	část	k1gFnSc7	část
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dočká	dočkat	k5eAaPmIp3nS	dočkat
spravedlivého	spravedlivý	k2eAgInSc2d1	spravedlivý
rozsudku	rozsudek	k1gInSc2	rozsudek
svých	svůj	k3xOyFgInPc2	svůj
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Reinkarnace	reinkarnace	k1gFnSc1	reinkarnace
-	-	kIx~	-
Zejména	zejména	k9	zejména
podle	podle	k7c2	podle
východních	východní	k2eAgNnPc2d1	východní
náboženství	náboženství	k1gNnPc2	náboženství
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
reinkarnaci	reinkarnace	k1gFnSc3	reinkarnace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
převtělení	převtělení	k1gNnSc4	převtělení
duše	duše	k1gFnSc2	duše
zemřelého	zemřelý	k1gMnSc2	zemřelý
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgMnSc4d1	jiný
živého	živý	k1gMnSc4	živý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
reinkarnace	reinkarnace	k1gFnSc2	reinkarnace
do	do	k7c2	do
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
nastává	nastávat	k5eAaImIp3nS	nastávat
až	až	k9	až
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
strávené	strávený	k2eAgFnSc6d1	strávená
na	na	k7c6	na
určitých	určitý	k2eAgNnPc6d1	určité
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
nebe	nebe	k1gNnPc1	nebe
<g/>
,	,	kIx,	,
peklo	peklo	k1gNnSc1	peklo
<g/>
)	)	kIx)	)
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
karmě	karma	k1gFnSc6	karma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
osvobození	osvobození	k1gNnSc6	osvobození
(	(	kIx(	(
<g/>
mókša	mókša	k6eAd1	mókša
<g/>
)	)	kIx)	)
zasloužilé	zasloužilý	k2eAgFnPc4d1	zasloužilá
duše	duše	k1gFnPc4	duše
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
samsára	samsár	k1gMnSc2	samsár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
zcela	zcela	k6eAd1	zcela
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
existenci	existence	k1gFnSc4	existence
daného	daný	k2eAgMnSc2d1	daný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
ateistické	ateistický	k2eAgInPc4d1	ateistický
a	a	k8xC	a
materialistické	materialistický	k2eAgInPc4d1	materialistický
náhledy	náhled	k1gInPc4	náhled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
popírají	popírat	k5eAaImIp3nP	popírat
existenci	existence	k1gFnSc4	existence
nehmotné	hmotný	k2eNgFnSc2d1	nehmotná
nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
entropie	entropie	k1gFnSc1	entropie
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
postupná	postupný	k2eAgFnSc1d1	postupná
dekompozice	dekompozice	k1gFnSc1	dekompozice
(	(	kIx(	(
<g/>
rozklad	rozklad	k1gInSc1	rozklad
<g/>
)	)	kIx)	)
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
náboženských	náboženský	k2eAgNnPc2d1	náboženské
společenství	společenství	k1gNnPc2	společenství
se	se	k3xPyFc4	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
existence	existence	k1gFnSc2	existence
přiklánějí	přiklánět	k5eAaImIp3nP	přiklánět
také	také	k9	také
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
přijde	přijít	k5eAaPmIp3nS	přijít
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
lidi	člověk	k1gMnPc4	člověk
vzkřísí	vzkřísit	k5eAaPmIp3nP	vzkřísit
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
v	v	k7c6	v
pozemském	pozemský	k2eAgInSc6d1	pozemský
ráji	ráj	k1gInSc6	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Dokládají	dokládat	k5eAaImIp3nP	dokládat
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
biblickými	biblický	k2eAgInPc7d1	biblický
verši	verš	k1gInPc7	verš
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Žalm	žalm	k1gInSc1	žalm
37	[number]	k4	37
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
Ale	ale	k8xC	ale
mírní	mírný	k2eAgMnPc1d1	mírný
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
budou	být	k5eAaImBp3nP	být
vlastnit	vlastnit	k5eAaImF	vlastnit
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
vskutku	vskutku	k9	vskutku
naleznou	nalézt	k5eAaBmIp3nP	nalézt
své	svůj	k3xOyFgNnSc4	svůj
největší	veliký	k2eAgNnSc4d3	veliký
potěšení	potěšení	k1gNnSc4	potěšení
v	v	k7c6	v
hojnosti	hojnost	k1gFnSc6	hojnost
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ezekiel	Ezekiel	k1gInSc1	Ezekiel
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
Duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
hřeší	hřešit	k5eAaImIp3nS	hřešit
–	–	k?	–
ta	ten	k3xDgFnSc1	ten
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Žalm	žalm	k1gInSc1	žalm
78	[number]	k4	78
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
...	...	k?	...
Nezadržel	zadržet	k5eNaPmAgInS	zadržet
jejich	jejich	k3xOp3gFnSc4	jejich
duši	duše	k1gFnSc4	duše
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
<g/>
;	;	kIx,	;
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
vydal	vydat	k5eAaPmAgInS	vydat
dokonce	dokonce	k9	dokonce
moru	mora	k1gFnSc4	mora
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Římanům	Říman	k1gMnPc3	Říman
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
právě	právě	k9	právě
jako	jako	k9	jako
skrze	skrze	k?	skrze
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
světa	svět	k1gInSc2	svět
hřích	hřích	k1gInSc1	hřích
a	a	k8xC	a
skrze	skrze	k?	skrze
hřích	hřích	k1gInSc1	hřích
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
smrt	smrt	k1gFnSc1	smrt
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všichni	všechen	k3xTgMnPc1	všechen
zhřešili	zhřešit	k5eAaPmAgMnP	zhřešit
Některé	některý	k3yIgInPc4	některý
filosofické	filosofický	k2eAgInPc4d1	filosofický
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc4d1	náboženský
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
,	,	kIx,	,
hlásají	hlásat	k5eAaImIp3nP	hlásat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kontinuitu	kontinuita	k1gFnSc4	kontinuita
bytí	bytí	k1gNnSc2	bytí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lidskou	lidský	k2eAgFnSc4d1	lidská
bytost	bytost	k1gFnSc4	bytost
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nehmotnou	hmotný	k2eNgFnSc4d1	nehmotná
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nikdy	nikdy	k6eAd1	nikdy
neumírá	umírat	k5eNaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
fyzický	fyzický	k2eAgInSc4d1	fyzický
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
schránka	schránka	k1gFnSc1	schránka
<g/>
,	,	kIx,	,
nádoba	nádoba	k1gFnSc1	nádoba
pro	pro	k7c4	pro
ducha	duch	k1gMnSc4	duch
<g/>
,	,	kIx,	,
fyzické	fyzický	k2eAgNnSc4d1	fyzické
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
člověk	člověk	k1gMnSc1	člověk
není	být	k5eNaImIp3nS	být
fyzické	fyzický	k2eAgNnSc4d1	fyzické
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
tudíž	tudíž	k8xC	tudíž
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
pak	pak	k6eAd1	pak
vnímají	vnímat	k5eAaImIp3nP	vnímat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
změnu	změna	k1gFnSc4	změna
stavu	stav	k1gInSc2	stav
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
tělu	tělo	k1gNnSc3	tělo
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
opuštění	opuštění	k1gNnSc1	opuštění
vozítka	vozítko	k1gNnSc2	vozítko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
duch	duch	k1gMnSc1	duch
pobýval	pobývat	k5eAaImAgMnS	pobývat
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
úrovni	úroveň	k1gFnSc6	úroveň
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
rozklad	rozklad	k1gInSc1	rozklad
fyzického	fyzický	k2eAgNnSc2d1	fyzické
těla	tělo	k1gNnSc2	tělo
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ducha	duch	k1gMnSc2	duch
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
způsoben	způsobit	k5eAaPmNgInS	způsobit
postupnou	postupný	k2eAgFnSc7d1	postupná
dezintegrací	dezintegrace	k1gFnSc7	dezintegrace
buněk	buňka	k1gFnPc2	buňka
vlivem	vlivem	k7c2	vlivem
absence	absence	k1gFnSc2	absence
organizující	organizující	k2eAgFnSc2d1	organizující
energie	energie	k1gFnSc2	energie
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
informacemi	informace	k1gFnPc7	informace
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c4	v
teosofie	teosofie	k1gFnPc4	teosofie
<g/>
,	,	kIx,	,
Učení	učení	k1gNnPc4	učení
odvěké	odvěký	k2eAgFnSc2d1	odvěká
moudrosti	moudrost	k1gFnSc2	moudrost
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
z	z	k7c2	z
tibetské	tibetský	k2eAgFnSc2d1	tibetská
větve	větev	k1gFnSc2	větev
mahájánového	mahájánový	k2eAgInSc2d1	mahájánový
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
výzkumech	výzkum	k1gInPc6	výzkum
například	například	k6eAd1	například
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Roberta	Robert	k1gMnSc2	Robert
A.	A.	kA	A.
Monroe	Monro	k1gMnSc2	Monro
Monroe	Monro	k1gMnSc2	Monro
například	například	k6eAd1	například
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
čtyřicátého	čtyřicátý	k4xOgInSc2	čtyřicátý
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
začaly	začít	k5eAaPmAgFnP	začít
dít	dít	k5eAaImF	dít
spontánní	spontánní	k2eAgInPc4d1	spontánní
mimotělesné	mimotělesný	k2eAgInPc4d1	mimotělesný
zážitky	zážitek	k1gInPc4	zážitek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k8xS	jako
uvolnění	uvolnění	k1gNnSc4	uvolnění
vědomí	vědomí	k1gNnSc2	vědomí
z	z	k7c2	z
fyzického	fyzický	k2eAgNnSc2d1	fyzické
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
astrálního	astrální	k2eAgNnSc2d1	astrální
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
těchto	tento	k3xDgInPc2	tento
prožitků	prožitek	k1gInPc2	prožitek
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
popsat	popsat	k5eAaPmF	popsat
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
fyzicko	fyzicko	k1gNnSc4	fyzicko
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původ	původ	k1gInSc1	původ
člověka	člověk	k1gMnSc4	člověk
nenašel	najít	k5eNaPmAgInS	najít
ve	v	k7c6	v
hmotné	hmotný	k2eAgFnSc6d1	hmotná
struktuře	struktura	k1gFnSc6	struktura
fyzického	fyzický	k2eAgNnSc2d1	fyzické
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
vědomého	vědomý	k2eAgNnSc2d1	vědomé
energetického	energetický	k2eAgNnSc2d1	energetické
bytí	bytí	k1gNnSc2	bytí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
kvanta	kvantum	k1gNnSc2	kvantum
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
disponujícího	disponující	k2eAgInSc2d1	disponující
určitými	určitý	k2eAgFnPc7d1	určitá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vědomí	vědomí	k1gNnSc1	vědomí
údajně	údajně	k6eAd1	údajně
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
dokáže	dokázat	k5eAaPmIp3nS	dokázat
cestovat	cestovat	k5eAaImF	cestovat
vesmírem	vesmír	k1gInSc7	vesmír
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
a	a	k8xC	a
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
"	"	kIx"	"
<g/>
přitaženo	přitažen	k2eAgNnSc4d1	přitaženo
<g/>
"	"	kIx"	"
atraktivitou	atraktivita	k1gFnSc7	atraktivita
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
panují	panovat	k5eAaImIp3nP	panovat
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
vtělit	vtělit	k5eAaPmF	vtělit
do	do	k7c2	do
fyzického	fyzický	k2eAgNnSc2d1	fyzické
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obývá	obývat	k5eAaImIp3nS	obývat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
opuštění	opuštění	k1gNnSc2	opuštění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
okamžik	okamžik	k1gInSc1	okamžik
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lidé	člověk	k1gMnPc1	člověk
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
smrt	smrt	k1gFnSc4	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vnímá	vnímat	k5eAaImIp3nS	vnímat
spíše	spíše	k9	spíše
jako	jako	k9	jako
odložení	odložení	k1gNnSc4	odložení
vysloužilého	vysloužilý	k2eAgInSc2d1	vysloužilý
fyzického	fyzický	k2eAgInSc2d1	fyzický
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vědomí	vědomí	k1gNnSc4	vědomí
projevovat	projevovat	k5eAaImF	projevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
úrovni	úroveň	k1gFnSc6	úroveň
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
život	život	k1gInSc1	život
člověka	člověk	k1gMnSc2	člověk
tímto	tento	k3xDgNnSc7	tento
odložením	odložení	k1gNnSc7	odložení
těla	tělo	k1gNnSc2	tělo
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nekončí	končit	k5eNaImIp3nS	končit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
nefyzické	fyzický	k2eNgFnSc2d1	nefyzická
roviny	rovina	k1gFnSc2	rovina
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
jemněji	jemně	k6eAd2	jemně
strukturována	strukturován	k2eAgFnSc1d1	strukturována
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
víceúrovňový	víceúrovňový	k2eAgInSc4d1	víceúrovňový
svět	svět	k1gInSc4	svět
oddělených	oddělený	k2eAgFnPc2d1	oddělená
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
lidská	lidský	k2eAgFnSc1d1	lidská
bytost	bytost	k1gFnSc1	bytost
v	v	k7c6	v
energetické	energetický	k2eAgFnSc6d1	energetická
formě	forma	k1gFnSc6	forma
řadu	řad	k1gInSc2	řad
dalších	další	k2eAgFnPc2d1	další
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
se	se	k3xPyFc4	se
v	v	k7c6	v
systému	systém	k1gInSc6	systém
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stavu	stav	k1gInSc6	stav
vedeny	veden	k2eAgInPc1d1	veden
dalšími	další	k2eAgInPc7d1	další
podobnými	podobný	k2eAgInPc7d1	podobný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mnohem	mnohem	k6eAd1	mnohem
pokročilejšími	pokročilý	k2eAgFnPc7d2	pokročilejší
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
pobýt	pobýt	k5eAaPmF	pobýt
v	v	k7c6	v
záhrobním	záhrobní	k2eAgInSc6d1	záhrobní
světě	svět	k1gInSc6	svět
a	a	k8xC	a
odpočinout	odpočinout	k5eAaPmF	odpočinout
si	se	k3xPyFc3	se
před	před	k7c7	před
<g />
.	.	kIx.	.
</s>
<s>
další	další	k2eAgFnSc7d1	další
inkarnací	inkarnace	k1gFnSc7	inkarnace
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
inkarnovat	inkarnovat	k5eAaBmF	inkarnovat
<g/>
,	,	kIx,	,
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
složky	složka	k1gFnSc2	složka
vlastní	vlastní	k2eAgFnSc2d1	vlastní
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
duše	duše	k1gFnSc1	duše
<g/>
)	)	kIx)	)
Dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
energetická	energetický	k2eAgFnSc1d1	energetická
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nezemře	zemřít	k5eNaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
pouze	pouze	k6eAd1	pouze
změnit	změnit	k5eAaPmF	změnit
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
stav	stav	k1gInSc4	stav
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
její	její	k3xOp3gFnSc1	její
podstata	podstata	k1gFnSc1	podstata
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
kontinuitě	kontinuita	k1gFnSc6	kontinuita
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
také	také	k9	také
Dr	dr	kA	dr
Joel	Joel	k1gInSc1	Joel
L.	L.	kA	L.
Whitton	Whitton	k1gInSc4	Whitton
PhD	PhD	k1gFnSc7	PhD
a	a	k8xC	a
Joe	Joe	k1gFnSc7	Joe
Fisher	Fishra	k1gFnPc2	Fishra
<g/>
.	.	kIx.	.
</s>
<s>
Provedený	provedený	k2eAgInSc1d1	provedený
výzkum	výzkum	k1gInSc1	výzkum
vědomí	vědomí	k1gNnSc2	vědomí
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
hypnotické	hypnotický	k2eAgFnPc4d1	hypnotická
seance	seance	k1gFnPc4	seance
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byli	být	k5eAaImAgMnP	být
pacienti	pacient	k1gMnPc1	pacient
s	s	k7c7	s
obtížnými	obtížný	k2eAgInPc7d1	obtížný
životními	životní	k2eAgInPc7d1	životní
problémy	problém	k1gInPc7	problém
uvedení	uvedení	k1gNnSc2	uvedení
zkušebně	zkušebně	k6eAd1	zkušebně
do	do	k7c2	do
hypnózy	hypnóza	k1gFnSc2	hypnóza
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vypovídali	vypovídat	k5eAaImAgMnP	vypovídat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
problém	problém	k1gInSc1	problém
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
náhodnému	náhodný	k2eAgNnSc3d1	náhodné
objevení	objevení	k1gNnSc3	objevení
existenčního	existenční	k2eAgInSc2d1	existenční
stavu	stav	k1gInSc2	stav
před	před	k7c7	před
stávajícím	stávající	k2eAgInSc7d1	stávající
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
odhaleny	odhalen	k2eAgFnPc4d1	odhalena
příčiny	příčina	k1gFnPc4	příčina
současných	současný	k2eAgInPc2d1	současný
problémů	problém	k1gInPc2	problém
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
předešlých	předešlý	k2eAgFnPc2d1	předešlá
inkarnací	inkarnace	k1gFnPc2	inkarnace
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
posmrtný	posmrtný	k2eAgInSc1d1	posmrtný
život	život	k1gInSc1	život
bývá	bývat	k5eAaImIp3nS	bývat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
i	i	k9	i
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
<g/>
)	)	kIx)	)
součástí	součást	k1gFnSc7	součást
různých	různý	k2eAgFnPc2d1	různá
mytologií	mytologie	k1gFnPc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Potýkají	potýkat	k5eAaImIp3nP	potýkat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
otázkami	otázka	k1gFnPc7	otázka
<g/>
:	:	kIx,	:
čas	čas	k1gInSc1	čas
smrti	smrt	k1gFnSc2	smrt
-	-	kIx~	-
lidé	člověk	k1gMnPc1	člověk
měli	mít	k5eAaImAgMnP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
smrt	smrt	k1gFnSc4	smrt
spojenu	spojen	k2eAgFnSc4d1	spojena
s	s	k7c7	s
pocity	pocit	k1gInPc7	pocit
úzkosti	úzkost	k1gFnSc2	úzkost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snažili	snažit	k5eAaImAgMnP	snažit
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
oddálit	oddálit	k5eAaPmF	oddálit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
člověku	člověk	k1gMnSc3	člověk
ještě	ještě	k9	ještě
zbývá	zbývat	k5eAaImIp3nS	zbývat
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
mechanismus	mechanismus	k1gInSc1	mechanismus
smrti	smrt	k1gFnSc2	smrt
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
zpravidla	zpravidla	k6eAd1	zpravidla
smrt	smrt	k1gFnSc4	smrt
představovali	představovat	k5eAaImAgMnP	představovat
jako	jako	k8xS	jako
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
oddělí	oddělit	k5eAaPmIp3nS	oddělit
duši	duše	k1gFnSc4	duše
zemřelého	zemřelý	k1gMnSc2	zemřelý
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
život	život	k1gInSc1	život
po	po	k7c6	po
životě	život	k1gInSc6	život
-	-	kIx~	-
mytologie	mytologie	k1gFnSc1	mytologie
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zodpovědět	zodpovědět	k5eAaPmF	zodpovědět
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
duší	duše	k1gFnSc7	duše
člověka	člověk	k1gMnSc2	člověk
děje	dít	k5eAaImIp3nS	dít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
pověstech	pověst	k1gFnPc6	pověst
se	se	k3xPyFc4	se
zemřelí	zemřelý	k1gMnPc1	zemřelý
mohou	moct	k5eAaImIp3nP	moct
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
vracet	vracet	k5eAaImF	vracet
do	do	k7c2	do
světa	svět	k1gInSc2	svět
živých	živá	k1gFnPc2	živá
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
kultur	kultura	k1gFnPc2	kultura
panovala	panovat	k5eAaImAgFnS	panovat
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
hodinu	hodina	k1gFnSc4	hodina
smrti	smrt	k1gFnSc2	smrt
dopředu	dopředu	k6eAd1	dopředu
přesně	přesně	k6eAd1	přesně
stanovenu	stanoven	k2eAgFnSc4d1	stanovena
<g/>
.	.	kIx.	.
</s>
<s>
Magickými	magický	k2eAgFnPc7d1	magická
praktikami	praktika	k1gFnPc7	praktika
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgInPc4d1	možný
okamžik	okamžik	k1gInSc4	okamžik
smrti	smrt	k1gFnSc2	smrt
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Užívala	užívat	k5eAaImAgFnS	užívat
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
nepřeberná	přeberný	k2eNgFnSc1d1	nepřeberná
škála	škála	k1gFnSc1	škála
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
sny	sen	k1gInPc1	sen
-	-	kIx~	-
sen	sen	k1gInSc1	sen
o	o	k7c6	o
černém	černý	k2eAgMnSc6d1	černý
psu	pes	k1gMnSc6	pes
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc6d1	bílá
koze	koza	k1gFnSc6	koza
<g/>
,	,	kIx,	,
kalné	kalný	k2eAgFnSc6d1	kalná
vodě	voda	k1gFnSc6	voda
atd.	atd.	kA	atd.
zvěstuje	zvěstovat	k5eAaImIp3nS	zvěstovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
nebo	nebo	k8xC	nebo
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
blízkých	blízký	k2eAgFnPc2d1	blízká
zemře	zemřít	k5eAaPmIp3nS	zemřít
zvířata	zvíře	k1gNnPc4	zvíře
-	-	kIx~	-
smrt	smrt	k1gFnSc4	smrt
ohlašují	ohlašovat	k5eAaImIp3nP	ohlašovat
některá	některý	k3yIgNnPc1	některý
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
:	:	kIx,	:
houkání	houkání	k1gNnSc1	houkání
sýčka	sýček	k1gMnSc2	sýček
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sýček	sýček	k1gMnSc1	sýček
svým	svůj	k3xOyFgMnPc3	svůj
hlasem	hlasem	k6eAd1	hlasem
volá	volat	k5eAaImIp3nS	volat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pojď	jít	k5eAaImRp2nS	jít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zve	zvát	k5eAaImIp3nS	zvát
lidi	člověk	k1gMnPc4	člověk
do	do	k7c2	do
záhrobí	záhrobí	k1gNnSc2	záhrobí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
kočka	kočka	k1gFnSc1	kočka
přeběhne	přeběhnout	k5eAaPmIp3nS	přeběhnout
přes	přes	k7c4	přes
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
žluva	žluva	k1gFnSc1	žluva
létá	létat	k5eAaImIp3nS	létat
kolem	kolem	k7c2	kolem
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
lišaj	lišaj	k1gMnSc1	lišaj
smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
můry	můra	k1gFnPc4	můra
<g/>
,	,	kIx,	,
havran	havran	k1gMnSc1	havran
<g/>
,	,	kIx,	,
vytí	vytí	k1gNnSc4	vytí
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
bučení	bučení	k1gNnSc1	bučení
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
vrzání	vrzání	k1gNnSc4	vrzání
červotoče	červotoč	k1gMnSc2	červotoč
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
člověk	člověk	k1gMnSc1	člověk
uslyší	uslyšet	k5eAaPmIp3nS	uslyšet
kukačku	kukačka	k1gFnSc4	kukačka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
počítat	počítat	k5eAaImF	počítat
<g/>
,	,	kIx,	,
kolikrát	kolikrát	k6eAd1	kolikrát
zakuká	zakukat	k5eAaPmIp3nS	zakukat
-	-	kIx~	-
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
určí	určit	k5eAaPmIp3nS	určit
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k6eAd1	ještě
zbývá	zbývat	k5eAaImIp3nS	zbývat
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
štědrovečerní	štědrovečerní	k2eAgFnSc6d1	štědrovečerní
půlnoci	půlnoc	k1gFnSc6	půlnoc
si	se	k3xPyFc3	se
domácí	domácí	k2eAgMnPc1d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
prý	prý	k9	prý
lidským	lidský	k2eAgInSc7d1	lidský
hlasem	hlas	k1gInSc7	hlas
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roka	rok	k1gInSc2	rok
zemře	zemřít	k5eAaPmIp3nS	zemřít
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poraní	poranit	k5eAaPmIp3nS	poranit
nebo	nebo	k8xC	nebo
zabije	zabít	k5eAaPmIp3nS	zabít
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
žijícího	žijící	k2eAgInSc2d1	žijící
pod	pod	k7c7	pod
prahem	práh	k1gInSc7	práh
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přeměněný	přeměněný	k2eAgMnSc1d1	přeměněný
skřítek	skřítek	k1gMnSc1	skřítek
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc1	ochránce
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
přírodní	přírodní	k2eAgInPc4d1	přírodní
jevy	jev	k1gInPc4	jev
-	-	kIx~	-
padající	padající	k2eAgFnSc1d1	padající
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
náhlé	náhlý	k2eAgNnSc1d1	náhlé
zhasnutí	zhasnutí	k1gNnSc1	zhasnutí
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
zvlhlá	zvlhlý	k2eAgFnSc1d1	zvlhlá
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
padající	padající	k2eAgFnSc1d1	padající
omítka	omítka	k1gFnSc1	omítka
<g/>
,	,	kIx,	,
bouchnutí	bouchnutí	k1gNnSc1	bouchnutí
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
narozené	narozený	k2eAgNnSc1d1	narozené
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
prý	prý	k9	prý
do	do	k7c2	do
roka	rok	k1gInSc2	rok
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
štědrovečerní	štědrovečerní	k2eAgInPc4d1	štědrovečerní
zvyky	zvyk	k1gInPc4	zvyk
mající	mající	k2eAgInPc4d1	mající
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
do	do	k7c2	do
roka	rok	k1gInSc2	rok
zemře	zemřít	k5eAaPmIp3nS	zemřít
-	-	kIx~	-
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
rozkrojeném	rozkrojený	k2eAgNnSc6d1	rozkrojené
jablku	jablko	k1gNnSc6	jablko
najde	najít	k5eAaPmIp3nS	najít
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
si	se	k3xPyFc3	se
rozlouskne	rozlousknout	k5eAaPmIp3nS	rozlousknout
prázdný	prázdný	k2eAgInSc1d1	prázdný
ořech	ořech	k1gInSc1	ořech
<g/>
,	,	kIx,	,
lití	lití	k1gNnSc1	lití
olova	olovo	k1gNnSc2	olovo
<g/>
:	:	kIx,	:
kdo	kdo	k3yRnSc1	kdo
si	se	k3xPyFc3	se
ulije	ulít	k5eAaPmIp3nS	ulít
rakev	rakev	k1gFnSc4	rakev
<g/>
,	,	kIx,	,
lopatu	lopata	k1gFnSc4	lopata
mytické	mytický	k2eAgFnSc2d1	mytická
bytosti	bytost	k1gFnSc2	bytost
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zjevení	zjevení	k1gNnSc1	zjevení
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
smrt	smrt	k1gFnSc4	smrt
<g/>
:	:	kIx,	:
bánší	bánší	k2eAgNnPc4d1	bánší
(	(	kIx(	(
<g/>
Banshee	Banshee	k1gNnPc4	Banshee
<g/>
)	)	kIx)	)
-	-	kIx~	-
víla	víla	k1gFnSc1	víla
z	z	k7c2	z
irských	irský	k2eAgFnPc2d1	irská
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prý	prý	k9	prý
svým	svůj	k3xOyFgNnSc7	svůj
kvílením	kvílení	k1gNnSc7	kvílení
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
smrt	smrt	k1gFnSc4	smrt
někoho	někdo	k3yInSc4	někdo
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
paní	paní	k1gFnSc1	paní
-	-	kIx~	-
duše	duše	k1gFnSc1	duše
zemřelého	zemřelý	k1gMnSc2	zemřelý
předka	předek	k1gMnSc2	předek
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
zjevení	zjevení	k1gNnSc1	zjevení
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
smrt	smrt	k1gFnSc1	smrt
někoho	někdo	k3yInSc4	někdo
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
meluzína	meluzína	k1gFnSc1	meluzína
-	-	kIx~	-
víla	víla	k1gFnSc1	víla
z	z	k7c2	z
francouzských	francouzský	k2eAgFnPc2d1	francouzská
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svým	svůj	k3xOyFgNnSc7	svůj
zjevením	zjevení	k1gNnSc7	zjevení
zvěstuje	zvěstovat	k5eAaImIp3nS	zvěstovat
smrt	smrt	k1gFnSc1	smrt
vlastníka	vlastník	k1gMnSc2	vlastník
hradu	hrad	k1gInSc2	hrad
Lusignan	Lusignan	k1gMnSc1	Lusignan
<g/>
.	.	kIx.	.
</s>
<s>
Nekrangelie	Nekrangelie	k1gFnSc1	Nekrangelie
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
složením	složení	k1gNnSc7	složení
řeckého	řecký	k2eAgNnSc2d1	řecké
ν	ν	k?	ν
(	(	kIx(	(
<g/>
nekrós	nekrós	k1gInSc1	nekrós
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
,	,	kIx,	,
zemřelý	zemřelý	k1gMnSc1	zemřelý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ά	ά	k?	ά
(	(	kIx(	(
<g/>
angelos	angelos	k1gMnSc1	angelos
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
posel	posít	k5eAaPmAgMnS	posít
<g/>
"	"	kIx"	"
<g/>
))	))	k?	))
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
okultní	okultní	k2eAgInSc1d1	okultní
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
umírající	umírající	k2eAgFnSc1d1	umírající
osoba	osoba	k1gFnSc1	osoba
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
<g />
.	.	kIx.	.
</s>
<s>
projeví	projevit	k5eAaPmIp3nS	projevit
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
příbuznému	příbuzný	k1gMnSc3	příbuzný
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
blízkému	blízký	k2eAgMnSc3d1	blízký
<g/>
,	,	kIx,	,
a	a	k8xC	a
podá	podat	k5eAaPmIp3nS	podat
mu	on	k3xPp3gMnSc3	on
tak	tak	k9	tak
zprávu	zpráva	k1gFnSc4	zpráva
-	-	kIx~	-
buď	buď	k8xC	buď
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
úmrtí	úmrtí	k1gNnSc6	úmrtí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
i	i	k9	i
jinou	jiný	k2eAgFnSc4d1	jiná
důležitou	důležitý	k2eAgFnSc4d1	důležitá
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mu	on	k3xPp3gMnSc3	on
za	za	k7c2	za
života	život	k1gInSc2	život
nestihla	stihnout	k5eNaPmAgFnS	stihnout
sdělit	sdělit	k5eAaPmF	sdělit
-	-	kIx~	-
kam	kam	k6eAd1	kam
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
poklad	poklad	k1gInSc4	poklad
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
majetek	majetek	k1gInSc1	majetek
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
nese	nést	k5eAaImIp3nS	nést
vinu	vina	k1gFnSc4	vina
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
varuje	varovat	k5eAaImIp3nS	varovat
příbuzného	příbuzný	k1gMnSc4	příbuzný
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
apod.	apod.	kA	apod.
Nekrangelie	Nekrangelie	k1gFnSc2	Nekrangelie
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Člověku	člověk	k1gMnSc3	člověk
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nP	zastavit
hodinky	hodinka	k1gFnPc1	hodinka
právě	právě	k9	právě
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc1	jeho
příbuzný	příbuzný	k1gMnSc1	příbuzný
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
slyší	slyšet	k5eAaImIp3nS	slyšet
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
hlas	hlas	k1gInSc1	hlas
umírajícího	umírající	k1gMnSc2	umírající
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdi	zeď	k1gFnSc2	zeď
spadne	spadnout	k5eAaPmIp3nS	spadnout
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
tak	tak	k6eAd1	tak
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc4	fotografia
umírajícího	umírající	k2eAgInSc2d1	umírající
náhle	náhle	k6eAd1	náhle
zamrká	zamrkat	k5eAaPmIp3nS	zamrkat
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnutý	odmítnutý	k2eAgMnSc1d1	odmítnutý
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
zjeví	zjevit	k5eAaPmIp3nS	zjevit
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
své	svůj	k3xOyFgFnSc2	svůj
vyvolené	vyvolená	k1gFnSc2	vyvolená
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
slyší	slyšet	k5eAaImIp3nS	slyšet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
nekrangelie	nekrangelie	k1gFnSc1	nekrangelie
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
-	-	kIx~	-
člověku	člověk	k1gMnSc6	člověk
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
o	o	k7c6	o
umírajícím	umírající	k2eAgInSc6d1	umírající
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
právě	právě	k6eAd1	právě
oné	onen	k3xDgFnSc2	onen
noci	noc	k1gFnSc2	noc
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
smrt	smrt	k1gFnSc4	smrt
oddálit	oddálit	k5eAaPmF	oddálit
<g/>
,	,	kIx,	,
oklamat	oklamat	k5eAaPmF	oklamat
<g/>
,	,	kIx,	,
odehnat	odehnat	k5eAaPmF	odehnat
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
představovali	představovat	k5eAaImAgMnP	představovat
jako	jako	k9	jako
hořící	hořící	k2eAgFnSc4d1	hořící
svíci	svíce	k1gFnSc4	svíce
-	-	kIx~	-
když	když	k8xS	když
svíce	svíce	k1gFnSc1	svíce
dohoří	dohořet	k5eAaPmIp3nS	dohořet
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc4	smrt
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
oklamat	oklamat	k5eAaPmF	oklamat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
svou	svůj	k3xOyFgFnSc4	svůj
svíci	svíce	k1gFnSc4	svíce
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
o	o	k7c4	o
svíci	svíce	k1gFnSc4	svíce
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
snažili	snažit	k5eAaImAgMnP	snažit
smrt	smrt	k1gFnSc4	smrt
zahnat	zahnat	k5eAaPmF	zahnat
od	od	k7c2	od
sebe	se	k3xPyFc2	se
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
naopak	naopak	k6eAd1	naopak
usilovali	usilovat	k5eAaImAgMnP	usilovat
přivolat	přivolat	k5eAaPmF	přivolat
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
pomocí	pomocí	k7c2	pomocí
kletby	kletba	k1gFnSc2	kletba
<g/>
.	.	kIx.	.
</s>
<s>
Přivolání	přivolání	k1gNnSc1	přivolání
smrti	smrt	k1gFnSc2	smrt
kletbou	kletba	k1gFnSc7	kletba
a	a	k8xC	a
pozvání	pozvánět	k5eAaImIp3nS	pozvánět
k	k	k7c3	k
božímu	boží	k2eAgInSc3d1	boží
soudu	soud	k1gInSc3	soud
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
thanatobolie	thanatobolie	k1gFnSc1	thanatobolie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kletba	kletba	k1gFnSc1	kletba
skutečně	skutečně	k6eAd1	skutečně
došla	dojít	k5eAaPmAgFnS	dojít
svého	svůj	k3xOyFgNnSc2	svůj
naplnění	naplnění	k1gNnSc2	naplnění
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámější	známý	k2eAgInSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
posledního	poslední	k2eAgMnSc2d1	poslední
velmistra	velmistr	k1gMnSc2	velmistr
řádu	řád	k1gInSc2	řád
templářů	templář	k1gMnPc2	templář
Jacqua	Jacquus	k1gMnSc2	Jacquus
de	de	k?	de
Molay	Molaa	k1gMnSc2	Molaa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
nespravedlivém	spravedlivý	k2eNgNnSc6d1	nespravedlivé
odsouzení	odsouzení	k1gNnSc6	odsouzení
k	k	k7c3	k
smrti	smrt	k1gFnSc6	smrt
upálením	upálení	k1gNnSc7	upálení
proklel	proklít	k5eAaPmAgMnS	proklít
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
papeže	papež	k1gMnSc4	papež
Klementa	Klement	k1gMnSc4	Klement
V.	V.	kA	V.
Oba	dva	k4xCgMnPc1	dva
prokletí	prokletí	k1gNnPc4	prokletí
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc1d3	nejznámější
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
vůdci	vůdce	k1gMnSc6	vůdce
Chodů	chod	k1gInPc2	chod
Janu	Jan	k1gMnSc6	Jan
Sladkém	Sladký	k1gMnSc6	Sladký
Kozinovi	Kozin	k1gMnSc6	Kozin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
údajně	údajně	k6eAd1	údajně
uvrhl	uvrhnout	k5eAaPmAgMnS	uvrhnout
kletbu	kletba	k1gFnSc4	kletba
na	na	k7c4	na
pána	pán	k1gMnSc4	pán
Wolfa	Wolf	k1gMnSc4	Wolf
Maxmiliána	Maxmilián	k1gMnSc4	Maxmilián
Lamingera	Laminger	k1gMnSc4	Laminger
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
do	do	k7c2	do
dne	den	k1gInSc2	den
setkají	setkat	k5eAaPmIp3nP	setkat
u	u	k7c2	u
božího	boží	k2eAgInSc2d1	boží
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Laminger	Laminger	k1gMnSc1	Laminger
skutečně	skutečně	k6eAd1	skutečně
za	za	k7c4	za
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Syndrom	syndrom	k1gInSc1	syndrom
thanatobolie	thanatobolie	k1gFnSc2	thanatobolie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
lamingerismus	lamingerismus	k1gInSc4	lamingerismus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
totéž	týž	k3xTgNnSc4	týž
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pokusil	pokusit	k5eAaPmAgMnS	pokusit
pětinásobný	pětinásobný	k2eAgMnSc1d1	pětinásobný
český	český	k2eAgMnSc1d1	český
vrah	vrah	k1gMnSc1	vrah
Ivan	Ivan	k1gMnSc1	Ivan
Roubal	Roubal	k1gMnSc1	Roubal
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
soudce	soudce	k1gMnSc1	soudce
na	na	k7c4	na
boží	boží	k2eAgInSc4d1	boží
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Roubal	Roubal	k1gMnSc1	Roubal
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
nemožnost	nemožnost	k1gFnSc1	nemožnost
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
věčný	věčný	k2eAgInSc1d1	věčný
život	život	k1gInSc1	život
ve	v	k7c6	v
fyzické	fyzický	k2eAgFnSc6d1	fyzická
nebo	nebo	k8xC	nebo
spirituální	spirituální	k2eAgFnSc6d1	spirituální
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
jako	jako	k9	jako
nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
označuje	označovat	k5eAaImIp3nS	označovat
nemožnost	nemožnost	k1gFnSc4	nemožnost
úmrtí	úmrť	k1gFnPc2	úmrť
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prozatím	prozatím	k6eAd1	prozatím
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
pověstech	pověst	k1gFnPc6	pověst
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
věčný	věčný	k2eAgInSc1d1	věčný
život	život	k1gInSc1	život
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
prokletí	prokletí	k1gNnSc4	prokletí
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
představovali	představovat	k5eAaImAgMnP	představovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
duši	duch	k1gMnPc1	duch
umírajícího	umírající	k1gMnSc2	umírající
si	se	k3xPyFc3	se
odnese	odnést	k5eAaPmIp3nS	odnést
nějaká	nějaký	k3yIgFnSc1	nějaký
bytost	bytost	k1gFnSc1	bytost
-	-	kIx~	-
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
mytická	mytický	k2eAgFnSc1d1	mytická
bytost	bytost	k1gFnSc1	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
bytosti	bytost	k1gFnPc1	bytost
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
provázely	provázet	k5eAaImAgFnP	provázet
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
soudily	soudit	k5eAaImAgInP	soudit
jeho	jeho	k3xOp3gInPc1	jeho
hříchy	hřích	k1gInPc1	hřích
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
dohlížely	dohlížet	k5eAaImAgFnP	dohlížet
v	v	k7c6	v
zásvětí	zásvětí	k1gNnSc6	zásvětí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
bytosti	bytost	k1gFnPc1	bytost
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
kultuře	kultura	k1gFnSc6	kultura
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Upanišady	Upanišada	k1gFnPc1	Upanišada
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
texty	text	k1gInPc1	text
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
duše	duše	k1gFnPc1	duše
jsou	být	k5eAaImIp3nP	být
odváděny	odvádět	k5eAaImNgFnP	odvádět
různými	různý	k2eAgNnPc7d1	různé
průvodci	průvodce	k1gMnPc1	průvodce
zvanými	zvaný	k2eAgFnPc7d1	zvaná
ativáhika	ativáhika	k1gFnSc1	ativáhika
dévatové	dévatový	k2eAgMnPc4d1	dévatový
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
místa	místo	k1gNnPc4	místo
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
individuální	individuální	k2eAgFnSc2d1	individuální
karmy	karma	k1gFnSc2	karma
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jim	on	k3xPp3gMnPc3	on
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
místo	místo	k1gNnSc1	místo
sdělí	sdělit	k5eAaPmIp3nS	sdělit
bůh	bůh	k1gMnSc1	bůh
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
soudce	soudce	k1gMnSc1	soudce
mrtvých	mrtvý	k1gMnPc2	mrtvý
Jama	Jama	k1gMnSc1	Jama
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
Janluo	Janluo	k1gMnSc1	Janluo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jako	jako	k8xS	jako
Emma	Emma	k1gFnSc1	Emma
<g/>
(	(	kIx(	(
<g/>
-ó	-ó	k?	-ó
<g/>
)	)	kIx)	)
či	či	k8xC	či
Enma	Enma	k1gFnSc1	Enma
<g/>
.	.	kIx.	.
</s>
<s>
Egyptským	egyptský	k2eAgMnSc7d1	egyptský
bohem	bůh	k1gMnSc7	bůh
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
mumifikace	mumifikace	k1gFnSc2	mumifikace
a	a	k8xC	a
pohřebišť	pohřebiště	k1gNnPc2	pohřebiště
je	být	k5eAaImIp3nS	být
Anup	Anup	k1gInSc1	Anup
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
se	s	k7c7	s
šakalí	šakalí	k2eAgFnSc7d1	šakalí
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
egyptských	egyptský	k2eAgMnPc2d1	egyptský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
připojil	připojit	k5eAaPmAgMnS	připojit
Usir	Usir	k1gMnSc1	Usir
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
života	život	k1gInSc2	život
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
mrtvých	mrtvý	k1gMnPc2	mrtvý
vládl	vládnout	k5eAaImAgMnS	vládnout
bůh	bůh	k1gMnSc1	bůh
Usire	Usir	k1gInSc5	Usir
(	(	kIx(	(
<g/>
Osiris	Osiris	k1gMnSc1	Osiris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
mrtvých	mrtvý	k1gMnPc2	mrtvý
žije	žít	k5eAaImIp3nS	žít
lidská	lidský	k2eAgFnSc1d1	lidská
duše	duše	k1gFnSc1	duše
dál	daleko	k6eAd2	daleko
plnohodnotných	plnohodnotný	k2eAgFnPc2d1	plnohodnotná
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
mumie	mumie	k1gFnSc2	mumie
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
arabského	arabský	k2eAgInSc2d1	arabský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
asfalt	asfalt	k1gInSc1	asfalt
nebo	nebo	k8xC	nebo
smůla	smůla	k1gFnSc1	smůla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
začalo	začít	k5eAaPmAgNnS	začít
užívat	užívat	k5eAaImF	užívat
pro	pro	k7c4	pro
nabalzamování	nabalzamování	k1gNnSc4	nabalzamování
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Mumifikace	mumifikace	k1gFnSc1	mumifikace
byla	být	k5eAaImAgFnS	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
starého	starý	k1gMnSc4	starý
Egypťana	Egypťan	k1gMnSc4	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Mumifikace	mumifikace	k1gFnSc1	mumifikace
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
podoby	podoba	k1gFnPc4	podoba
<g/>
,	,	kIx,	,
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
a	a	k8xC	a
umělou	umělý	k2eAgFnSc7d1	umělá
<g/>
.	.	kIx.	.
</s>
<s>
Bytost	bytost	k1gFnSc1	bytost
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
byla	být	k5eAaImAgFnS	být
součást	součást	k1gFnSc1	součást
elementů	element	k1gInPc2	element
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
centrem	centrum	k1gNnSc7	centrum
citů	cit	k1gInPc2	cit
<g/>
,	,	kIx,	,
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
racionálního	racionální	k2eAgNnSc2d1	racionální
uvažování	uvažování	k1gNnSc2	uvažování
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
pomyslným	pomyslný	k2eAgInSc7d1	pomyslný
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
ráje	ráj	k1gInSc2	ráj
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vah	váha	k1gFnPc2	váha
boha	bůh	k1gMnSc2	bůh
Anupa	Anup	k1gMnSc2	Anup
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Knihy	kniha	k1gFnSc2	kniha
mrtvých	mrtvý	k1gMnPc2	mrtvý
je	být	k5eAaImIp3nS	být
srdce	srdce	k1gNnSc1	srdce
položeno	položit	k5eAaPmNgNnS	položit
na	na	k7c4	na
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
misce	miska	k1gFnSc6	miska
je	být	k5eAaImIp3nS	být
položeno	položit	k5eAaPmNgNnS	položit
pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
tělo	tělo	k1gNnSc1	tělo
zemřelého	zemřelý	k1gMnSc2	zemřelý
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
projeví	projevit	k5eAaPmIp3nS	projevit
se	se	k3xPyFc4	se
tak	tak	k9	tak
vina	vina	k1gFnSc1	vina
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
Bohem	bůh	k1gMnSc7	bůh
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
je	být	k5eAaImIp3nS	být
Thanatos	Thanatos	k1gMnSc1	Thanatos
<g/>
,	,	kIx,	,
zobrazovaný	zobrazovaný	k2eAgMnSc1d1	zobrazovaný
jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přilétá	přilétat	k5eAaImIp3nS	přilétat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uhasil	uhasit	k5eAaPmAgMnS	uhasit
pochodeň	pochodeň	k1gFnSc4	pochodeň
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
-	-	kIx~	-
dvojčetem	dvojče	k1gNnSc7	dvojče
je	být	k5eAaImIp3nS	být
Hypnos	hypnosa	k1gFnPc2	hypnosa
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
synové	syn	k1gMnPc1	syn
bohyně	bohyně	k1gFnSc2	bohyně
noci	noc	k1gFnSc2	noc
-	-	kIx~	-
Nyx	Nyx	k1gMnSc1	Nyx
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
tím	ten	k3xDgNnSc7	ten
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
jednak	jednak	k8xC	jednak
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
spánek	spánek	k1gInSc4	spánek
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
podobné	podobný	k2eAgInPc4d1	podobný
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
umírají	umírat	k5eAaImIp3nP	umírat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
sourozencem	sourozenec	k1gMnSc7	sourozenec
je	být	k5eAaImIp3nS	být
také	také	k9	také
Charón	Charón	k1gMnSc1	Charón
<g/>
,	,	kIx,	,
převozník	převozník	k1gMnSc1	převozník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
převáží	převážet	k5eAaImIp3nS	převážet
duše	duše	k1gFnPc4	duše
zemřelých	zemřelý	k1gMnPc2	zemřelý
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Podsvětní	podsvětní	k2eAgFnSc4d1	podsvětní
říši	říše	k1gFnSc4	říše
vládne	vládnout	k5eAaImIp3nS	vládnout
bůh	bůh	k1gMnSc1	bůh
Hádés	Hádés	k1gInSc1	Hádés
<g/>
.	.	kIx.	.
</s>
<s>
Bránu	brána	k1gFnSc4	brána
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
hlídá	hlídat	k5eAaImIp3nS	hlídat
strašlivý	strašlivý	k2eAgMnSc1d1	strašlivý
tříhlavý	tříhlavý	k2eAgMnSc1d1	tříhlavý
pes	pes	k1gMnSc1	pes
Kerberos	Kerberos	k1gMnSc1	Kerberos
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
byly	být	k5eAaImAgFnP	být
bytosti	bytost	k1gFnPc1	bytost
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnSc6d1	podobná
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
,	,	kIx,	,
jen	jen	k9	jen
měly	mít	k5eAaImAgFnP	mít
jiná	jiný	k2eAgNnPc4d1	jiné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Bohem	bůh	k1gMnSc7	bůh
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
Mors	Mors	k1gInSc1	Mors
<g/>
,	,	kIx,	,
bohem	bůh	k1gMnSc7	bůh
římského	římský	k2eAgNnSc2d1	římské
podsvětí	podsvětí	k1gNnSc2	podsvětí
bůh	bůh	k1gMnSc1	bůh
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
podle	podle	k7c2	podle
keltské	keltský	k2eAgFnSc2d1	keltská
mytologie	mytologie	k1gFnSc2	mytologie
je	být	k5eAaImIp3nS	být
strážcem	strážce	k1gMnSc7	strážce
brány	brána	k1gFnSc2	brána
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
bůh	bůh	k1gMnSc1	bůh
Donn	donna	k1gFnPc2	donna
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
boha	bůh	k1gMnSc2	bůh
Míla	Míla	k1gMnSc1	Míla
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
Tech	Tech	k?	Tech
nPuinn	nPuinna	k1gFnPc2	nPuinna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
Donn	donna	k1gFnPc2	donna
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mrtví	mrtvý	k1gMnPc1	mrtvý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
padli	padnout	k5eAaImAgMnP	padnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
např.	např.	kA	např.
stářím	stáří	k1gNnSc7	stáří
nebo	nebo	k8xC	nebo
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
padlých	padlý	k2eAgMnPc2d1	padlý
hrdinů	hrdina	k1gMnPc2	hrdina
jsou	být	k5eAaImIp3nP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
ti	ten	k3xDgMnPc1	ten
nejudatnější	udatný	k2eAgFnSc1d3	nejudatnější
valkýrami	valkýra	k1gFnPc7	valkýra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	on	k3xPp3gFnPc4	on
odnesou	odnést	k5eAaPmIp3nP	odnést
do	do	k7c2	do
Valhaly	Valhala	k1gFnSc2	Valhala
-	-	kIx~	-
síně	síň	k1gFnPc1	síň
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
budou	být	k5eAaImBp3nP	být
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
ragnarok	ragnarok	k1gInSc4	ragnarok
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
zemřelí	zemřelý	k1gMnPc1	zemřelý
odcházejí	odcházet	k5eAaImIp3nP	odcházet
do	do	k7c2	do
temné	temný	k2eAgFnSc2d1	temná
říše	říš	k1gFnSc2	říš
Helheimu	Helheim	k1gInSc2	Helheim
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
vládne	vládnout	k5eAaImIp3nS	vládnout
bohyně	bohyně	k1gFnSc1	bohyně
Hel	Hela	k1gFnPc2	Hela
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
představují	představovat	k5eAaImIp3nP	představovat
ještě	ještě	k9	ještě
utopení	utopený	k2eAgMnPc1d1	utopený
námořníci	námořník	k1gMnPc1	námořník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
duše	duše	k1gFnPc1	duše
přebývají	přebývat	k5eAaImIp3nP	přebývat
u	u	k7c2	u
bohyně	bohyně	k1gFnSc2	bohyně
Rán	Rána	k1gFnPc2	Rána
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
mytologii	mytologie	k1gFnSc6	mytologie
postavu	postav	k1gInSc2	postav
smrti	smrt	k1gFnSc2	smrt
zosobňovala	zosobňovat	k5eAaImAgFnS	zosobňovat
bohyně	bohyně	k1gFnSc1	bohyně
Morana	Morana	k1gFnSc1	Morana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
velmi	velmi	k6eAd1	velmi
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vládkyní	vládkyně	k1gFnSc7	vládkyně
zimy	zima	k1gFnSc2	zima
<g/>
;	;	kIx,	;
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
vesnicích	vesnice	k1gFnPc6	vesnice
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
udržuje	udržovat	k5eAaImIp3nS	udržovat
zvyk	zvyk	k1gInSc1	zvyk
"	"	kIx"	"
<g/>
vynášení	vynášení	k1gNnSc1	vynášení
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
lidé	člověk	k1gMnPc1	člověk
vyrobí	vyrobit	k5eAaPmIp3nP	vyrobit
ze	z	k7c2	z
slámy	sláma	k1gFnSc2	sláma
a	a	k8xC	a
starých	starý	k2eAgInPc2d1	starý
hadrů	hadr	k1gInPc2	hadr
postavu	postav	k1gInSc2	postav
(	(	kIx(	(
<g/>
označovanou	označovaný	k2eAgFnSc7d1	označovaná
jako	jako	k8xS	jako
Morana	Morana	k1gFnSc1	Morana
<g/>
,	,	kIx,	,
Mařena	Mařena	k1gFnSc1	Mařena
<g/>
,	,	kIx,	,
Smrtholka	Smrtholka	k1gFnSc1	Smrtholka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pak	pak	k6eAd1	pak
hodí	hodit	k5eAaPmIp3nS	hodit
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
nebo	nebo	k8xC	nebo
z	z	k7c2	z
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ji	on	k3xPp3gFnSc4	on
spálí	spálit	k5eAaPmIp3nS	spálit
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
slavností	slavnost	k1gFnSc7	slavnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
oslavit	oslavit	k5eAaPmF	oslavit
příchod	příchod	k1gInSc4	příchod
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
tento	tento	k3xDgInSc4	tento
zvyk	zvyk	k1gInSc1	zvyk
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
pálením	pálení	k1gNnSc7	pálení
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc1	zvyk
vynášení	vynášení	k1gNnSc1	vynášení
smrti	smrt	k1gFnSc2	smrt
je	být	k5eAaImIp3nS	být
doložen	doložit	k5eAaPmNgInS	doložit
už	už	k6eAd1	už
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
představovali	představovat	k5eAaImAgMnP	představovat
smrt	smrt	k1gFnSc4	smrt
jako	jako	k8xS	jako
kostlivce	kostlivec	k1gMnPc4	kostlivec
s	s	k7c7	s
kosou	kosa	k1gFnSc7	kosa
nebo	nebo	k8xC	nebo
mečem	meč	k1gInSc7	meč
(	(	kIx(	(
<g/>
Smrtka	Smrtka	k1gFnSc1	Smrtka
<g/>
,	,	kIx,	,
Smrťák	smrťák	k1gInSc1	smrťák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
spíš	spíš	k9	spíš
za	za	k7c4	za
osobu	osoba	k1gFnSc4	osoba
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
spíše	spíše	k9	spíše
ženského	ženský	k2eAgMnSc4d1	ženský
(	(	kIx(	(
<g/>
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpodobnění	zpodobnění	k1gNnSc1	zpodobnění
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
tzv.	tzv.	kA	tzv.
tance	tanec	k1gInSc2	tanec
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
danse	dans	k1gMnSc5	dans
macabre	macabr	k1gMnSc5	macabr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
kostlivec-smrt	kostlivecmrt	k1gInSc1	kostlivec-smrt
tančí	tančit	k5eAaImIp3nS	tančit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
průvodu	průvod	k1gInSc2	průvod
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
zpodobnění	zpodobnění	k1gNnPc1	zpodobnění
smrti	smrt	k1gFnSc2	smrt
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
memento	memento	k1gNnSc4	memento
mori	mor	k1gFnSc2	mor
(	(	kIx(	(
<g/>
pamatuj	pamatovat	k5eAaImRp2nS	pamatovat
na	na	k7c6	na
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
zapomínat	zapomínat	k5eAaImF	zapomínat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
žít	žít	k5eAaImF	žít
ctnostným	ctnostný	k2eAgInSc7d1	ctnostný
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezemřel	zemřít	k5eNaPmAgMnS	zemřít
ve	v	k7c6	v
hříchu	hřích	k1gInSc6	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nemrtvý	mrtvý	k2eNgInSc1d1	nemrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
nedojde	dojít	k5eNaPmIp3nS	dojít
pokoje	pokoj	k1gInPc4	pokoj
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
do	do	k7c2	do
světa	svět	k1gInSc2	svět
živých	živý	k2eAgInPc2d1	živý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
nazývá	nazývat	k5eAaImIp3nS	nazývat
revenant	revenant	k1gInSc1	revenant
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
revenir	revenir	k1gMnSc1	revenir
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
nemrtvý	mrtvý	k2eNgMnSc1d1	nemrtvý
nebo	nebo	k8xC	nebo
oživlý	oživlý	k2eAgMnSc1d1	oživlý
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
zjevení	zjevení	k1gNnSc2	zjevení
pak	pak	k6eAd1	pak
dostávají	dostávat	k5eAaImIp3nP	dostávat
označení	označení	k1gNnSc4	označení
podle	podle	k7c2	podle
svých	svůj	k3xOyFgInPc2	svůj
projevů	projev	k1gInPc2	projev
-	-	kIx~	-
mezi	mezi	k7c7	mezi
revenanty	revenant	k1gMnPc7	revenant
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
přízrak	přízrak	k1gInSc1	přízrak
<g/>
,	,	kIx,	,
oživlý	oživlý	k2eAgMnSc1d1	oživlý
kostlivec	kostlivec	k1gMnSc1	kostlivec
<g/>
,	,	kIx,	,
ohnivý	ohnivý	k2eAgMnSc1d1	ohnivý
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
přízrak	přízrak	k1gInSc1	přízrak
podvodníka	podvodník	k1gMnSc2	podvodník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bludička	bludička	k1gFnSc1	bludička
(	(	kIx(	(
<g/>
nepokojná	pokojný	k2eNgFnSc1d1	nepokojná
duše	duše	k1gFnSc1	duše
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
upír	upír	k1gMnSc1	upír
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
oživlé	oživlý	k2eAgMnPc4d1	oživlý
mrtvé	mrtvý	k1gMnPc4	mrtvý
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
zombie	zombie	k1gFnSc1	zombie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nekromancie	nekromancie	k1gFnSc2	nekromancie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
pomocí	pomocí	k7c2	pomocí
magických	magický	k2eAgFnPc2d1	magická
praktik	praktika	k1gFnPc2	praktika
přivolat	přivolat	k5eAaPmF	přivolat
zemřelého	zemřelý	k1gMnSc4	zemřelý
a	a	k8xC	a
vyzvědět	vyzvědět	k5eAaPmF	vyzvědět
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
nějakou	nějaký	k3yIgFnSc4	nějaký
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
formu	forma	k1gFnSc4	forma
věštění	věštění	k1gNnSc2	věštění
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
nekromancie	nekromancie	k1gFnSc1	nekromancie
<g/>
.	.	kIx.	.
</s>
<s>
Záležitosti	záležitost	k1gFnPc1	záležitost
související	související	k2eAgFnPc1d1	související
se	se	k3xPyFc4	se
smrtí	smrt	k1gFnSc7	smrt
jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
motivem	motiv	k1gInSc7	motiv
strašidelných	strašidelný	k2eAgInPc2d1	strašidelný
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgInPc2d1	filmový
i	i	k8xC	i
knižních	knižní	k2eAgInPc2d1	knižní
hororů	horor	k1gInPc2	horor
a	a	k8xC	a
tajuplných	tajuplný	k2eAgFnPc2d1	tajuplná
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
<g/>
:	:	kIx,	:
Terry	Terr	k1gInPc1	Terr
Pratchett	Pratchetta	k1gFnPc2	Pratchetta
<g/>
:	:	kIx,	:
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
-	-	kIx~	-
Smrť	smrtit	k5eAaImRp2nS	smrtit
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
knihy	kniha	k1gFnPc1	kniha
Mort	Mort	k1gInSc1	Mort
<g/>
,	,	kIx,	,
Sekáč	sekáč	k1gInSc1	sekáč
<g/>
,	,	kIx,	,
Těžké	těžký	k2eAgNnSc1d1	těžké
melodično	melodično	k1gNnSc1	melodično
<g/>
,	,	kIx,	,
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
<g/>
,	,	kIx,	,
Zloděj	zloděj	k1gMnSc1	zloděj
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
:	:	kIx,	:
Silmarillion	Silmarillion	k1gInSc1	Silmarillion
-	-	kIx~	-
Mandos	Mandos	k1gInSc1	Mandos
Robert	Robert	k1gMnSc1	Robert
Fulghum	Fulghum	k1gInSc1	Fulghum
<g/>
:	:	kIx,	:
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
do	do	k7c2	do
konce	konec	k1gInSc2	konec
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
jedna	jeden	k4xCgFnSc1	jeden
kapitola	kapitola	k1gFnSc1	kapitola
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
:	:	kIx,	:
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
-	-	kIx~	-
některé	některý	k3yIgFnPc4	některý
strašidelné	strašidelný	k2eAgFnPc4d1	strašidelná
povídky	povídka	k1gFnPc4	povídka
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
)	)	kIx)	)
Film	film	k1gInSc1	film
<g/>
:	:	kIx,	:
Ingmar	Ingmar	k1gMnSc1	Ingmar
Bergman	Bergman	k1gMnSc1	Bergman
<g/>
:	:	kIx,	:
Sedmá	sedmý	k4xOgFnSc1	sedmý
pečeť	pečeť	k1gFnSc1	pečeť
-	-	kIx~	-
Smrt	smrt	k1gFnSc1	smrt
Martin	Martin	k1gMnSc1	Martin
Brest	Brest	k1gInSc1	Brest
<g/>
:	:	kIx,	:
Seznamte	seznámit	k5eAaPmRp2nP	seznámit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnPc1	Joe
Black	Blacka	k1gFnPc2	Blacka
<g />
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
:	:	kIx,	:
Dařbuján	Dařbuján	k1gMnSc1	Dařbuján
a	a	k8xC	a
Pandrhola	Pandrhola	k?	Pandrhola
-	-	kIx~	-
Smrťák	smrťák	k1gInSc1	smrťák
Vadim	Vadim	k?	Vadim
Jean	Jean	k1gMnSc1	Jean
<g/>
:	:	kIx,	:
Otec	otec	k1gMnSc1	otec
prasátek	prasátko	k1gNnPc2	prasátko
(	(	kIx(	(
<g/>
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
knihy	kniha	k1gFnSc2	kniha
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
<g/>
)	)	kIx)	)
-	-	kIx~	-
Smrť	smrtit	k5eAaImRp2nS	smrtit
Vadim	Vadim	k?	Vadim
Jean	Jean	k1gMnSc1	Jean
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
kouzel	kouzlo	k1gNnPc2	kouzlo
(	(	kIx(	(
<g/>
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
knihy	kniha	k1gFnSc2	kniha
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
<g/>
)	)	kIx)	)
-	-	kIx~	-
Smrť	smrtit	k5eAaImRp2nS	smrtit
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
:	:	kIx,	:
Smysl	smysl	k1gInSc1	smysl
života	život	k1gInSc2	život
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
John	John	k1gMnSc1	John
McTiernan	McTiernan	k1gMnSc1	McTiernan
<g/>
:	:	kIx,	:
Poslední	poslední	k2eAgMnSc1d1	poslední
akční	akční	k2eAgMnSc1d1	akční
hrdina	hrdina	k1gMnSc1	hrdina
-	-	kIx~	-
Smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
Bergmannovu	Bergmannův	k2eAgFnSc4d1	Bergmannův
Smrt	smrt	k1gFnSc4	smrt
<g/>
)	)	kIx)	)
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
Honza	Honza	k1gMnSc1	Honza
málem	málem	k6eAd1	málem
králem	král	k1gMnSc7	král
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
:	:	kIx,	:
Posel	posel	k1gMnSc1	posel
z	z	k7c2	z
Liptákova	Liptákův	k2eAgInSc2d1	Liptákův
-	-	kIx~	-
pan	pan	k1gMnSc1	pan
Smrtka	Smrtka	k1gFnSc1	Smrtka
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
:	:	kIx,	:
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
:	:	kIx,	:
Rytíř	Rytíř	k1gMnSc1	Rytíř
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
ďábel	ďábel	k1gMnSc1	ďábel
Literatura	literatura	k1gFnSc1	literatura
<g/>
:	:	kIx,	:
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
-	-	kIx~	-
když	když	k8xS	když
umíral	umírat	k5eAaImAgMnS	umírat
Boromir	Boromir	k1gMnSc1	Boromir
<g/>
,	,	kIx,	,
napadený	napadený	k2eAgMnSc1d1	napadený
skřety	skřet	k1gMnPc7	skřet
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
bratr	bratr	k1gMnSc1	bratr
Faramir	Faramir	k1gMnSc1	Faramir
slyšel	slyšet	k5eAaImAgMnS	slyšet
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
jeho	jeho	k3xOp3gInSc4	jeho
roh	roh	k1gInSc4	roh
Film	film	k1gInSc1	film
<g/>
:	:	kIx,	:
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
:	:	kIx,	:
Velká	velký	k2eAgFnSc1d1	velká
ryba	ryba	k1gFnSc1	ryba
-	-	kIx~	-
historka	historka	k1gFnSc1	historka
o	o	k7c6	o
černé	černý	k2eAgFnSc6d1	černá
vráně	vrána	k1gFnSc6	vrána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvěstuje	zvěstovat	k5eAaImIp3nS	zvěstovat
smrt	smrt	k1gFnSc4	smrt
<g/>
;	;	kIx,	;
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zemře	zemřít	k5eAaPmIp3nS	zemřít
Gore	Gore	k1gFnSc1	Gore
Verbinski	Verbinsk	k1gFnSc2	Verbinsk
<g/>
:	:	kIx,	:
Kruh	kruh	k1gInSc1	kruh
-	-	kIx~	-
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
lidem	lid	k1gInSc7	lid
zvěstována	zvěstovat	k5eAaImNgFnS	zvěstovat
telefonem	telefon	k1gInSc7	telefon
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
předem	předem	k6eAd1	předem
</s>
