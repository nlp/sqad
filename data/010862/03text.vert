<p>
<s>
Pär	Pär	k?	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1891	[number]	k4	1891
Växjö	Växjö	k1gFnSc2	Växjö
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1974	[number]	k4	1974
Danderyd	Danderyda	k1gFnPc2	Danderyda
u	u	k7c2	u
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
romanopisec	romanopisec	k1gMnSc1	romanopisec
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
esejí	esej	k1gFnPc2	esej
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pär	Pär	k?	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
ve	v	k7c6	v
Växjö	Växjö	k1gFnSc6	Växjö
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Švédsku	Švédsko	k1gNnSc6	Švédsko
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Små	Små	k1gFnSc2	Små
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
přednosty	přednosta	k1gMnSc2	přednosta
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
dětství	dětství	k1gNnSc2	dětství
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
silně	silně	k6eAd1	silně
religiózním	religiózní	k2eAgNnSc6d1	religiózní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
strávil	strávit	k5eAaPmAgInS	strávit
rok	rok	k1gInSc1	rok
studiem	studio	k1gNnSc7	studio
historie	historie	k1gFnSc2	historie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
moderní	moderní	k2eAgInPc1d1	moderní
expresionistické	expresionistický	k2eAgInPc1d1	expresionistický
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
novinářství	novinářství	k1gNnSc2	novinářství
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vyznavačem	vyznavač	k1gMnSc7	vyznavač
darwinistického	darwinistický	k2eAgNnSc2d1	darwinistické
pojetí	pojetí	k1gNnSc2	pojetí
světa	svět	k1gInSc2	svět
a	a	k8xC	a
radikálních	radikální	k2eAgInPc2d1	radikální
politických	politický	k2eAgInPc2d1	politický
názorů	názor	k1gInPc2	názor
blízkých	blízký	k2eAgInPc2d1	blízký
socialismu	socialismus	k1gInSc3	socialismus
(	(	kIx(	(
<g/>
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
několika	několik	k4yIc7	několik
levicovými	levicový	k2eAgInPc7d1	levicový
listy	list	k1gInPc7	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
zároveň	zároveň	k6eAd1	zároveň
natrvalo	natrvalo	k6eAd1	natrvalo
zakotvily	zakotvit	k5eAaPmAgFnP	zakotvit
i	i	k9	i
otázky	otázka	k1gFnPc1	otázka
metafyzické	metafyzický	k2eAgFnSc2d1	metafyzická
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
radikalismus	radikalismus	k1gInSc1	radikalismus
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
i	i	k9	i
estetické	estetický	k2eAgFnPc4d1	estetická
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
programové	programový	k2eAgFnSc6d1	programová
studii	studie	k1gFnSc6	studie
Slovesné	slovesný	k2eAgNnSc1d1	slovesné
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
evropskému	evropský	k2eAgInSc3d1	evropský
modernismu	modernismus	k1gInSc3	modernismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
lyrik	lyrika	k1gFnPc2	lyrika
se	se	k3xPyFc4	se
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
poprvé	poprvé	k6eAd1	poprvé
prosadil	prosadit	k5eAaPmAgMnS	prosadit
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
sbírkou	sbírka	k1gFnSc7	sbírka
Úzkost	úzkost	k1gFnSc1	úzkost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
však	však	k9	však
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
světového	světový	k2eAgInSc2d1	světový
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
osobní	osobní	k2eAgFnSc2d1	osobní
krize	krize	k1gFnSc2	krize
objevuje	objevovat	k5eAaImIp3nS	objevovat
téma	téma	k1gNnSc4	téma
bolesti	bolest	k1gFnSc2	bolest
<g/>
,	,	kIx,	,
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
beznaděje	beznaděj	k1gFnSc2	beznaděj
a	a	k8xC	a
rezignace	rezignace	k1gFnSc2	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
<g/>
,	,	kIx,	,
krajně	krajně	k6eAd1	krajně
pesimistickém	pesimistický	k2eAgMnSc6d1	pesimistický
duchu	duch	k1gMnSc6	duch
jsou	být	k5eAaImIp3nP	být
napsány	napsat	k5eAaBmNgInP	napsat
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
sbírky	sbírka	k1gFnPc1	sbírka
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
expresionistická	expresionistický	k2eAgFnSc1d1	expresionistická
<g/>
,	,	kIx,	,
absurditu	absurdita	k1gFnSc4	absurdita
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
manifestující	manifestující	k2eAgNnPc1d1	manifestující
dramata	drama	k1gNnPc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
témata	téma	k1gNnPc1	téma
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
i	i	k8xC	i
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
další	další	k2eAgFnSc4d1	další
prozaickou	prozaický	k2eAgFnSc4d1	prozaická
tvorbu	tvorba	k1gFnSc4	tvorba
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
básnických	básnický	k2eAgFnPc6d1	básnická
sbírkách	sbírka	k1gFnPc6	sbírka
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
projevuje	projevovat	k5eAaImIp3nS	projevovat
obnovená	obnovený	k2eAgFnSc1d1	obnovená
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
politickými	politický	k2eAgFnPc7d1	politická
událostmi	událost	k1gFnPc7	událost
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
fašismu	fašismus	k1gInSc2	fašismus
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Lagerkvist	Lagerkvist	k1gInSc1	Lagerkvist
aktivní	aktivní	k2eAgInSc4d1	aktivní
postoj	postoj	k1gInSc4	postoj
bojujícího	bojující	k2eAgMnSc2d1	bojující
humanisty	humanista	k1gMnSc2	humanista
<g/>
,	,	kIx,	,
odpůrce	odpůrce	k1gMnSc2	odpůrce
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
a	a	k8xC	a
obhájce	obhájce	k1gMnSc1	obhájce
demokratických	demokratický	k2eAgInPc2d1	demokratický
principů	princip	k1gInPc2	princip
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc1	jeho
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
varovná	varovný	k2eAgNnPc1d1	varovné
díla	dílo	k1gNnPc1	dílo
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
zla	zlo	k1gNnSc2	zlo
Kat	Kata	k1gFnPc2	Kata
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
a	a	k8xC	a
Trpaslík	trpaslík	k1gMnSc1	trpaslík
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dramat	drama	k1gNnPc2	drama
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
základní	základní	k2eAgInPc4d1	základní
problémy	problém	k1gInPc4	problém
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
neustálého	neustálý	k2eAgNnSc2d1	neustálé
ohrožení	ohrožení	k1gNnSc2	ohrožení
(	(	kIx(	(
<g/>
německý	německý	k2eAgInSc1d1	německý
nacismus	nacismus	k1gInSc1	nacismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Lagerkvist	Lagerkvist	k1gInSc1	Lagerkvist
vrátil	vrátit	k5eAaPmAgInS	vrátit
opět	opět	k6eAd1	opět
k	k	k7c3	k
próze	próza	k1gFnSc3	próza
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
stylisticky	stylisticky	k6eAd1	stylisticky
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
dokonalých	dokonalý	k2eAgInPc2d1	dokonalý
románů	román	k1gInPc2	román
a	a	k8xC	a
novel	novela	k1gFnPc2	novela
s	s	k7c7	s
biblickými	biblický	k2eAgInPc7d1	biblický
a	a	k8xC	a
náboženskými	náboženský	k2eAgInPc7d1	náboženský
náměty	námět	k1gInPc7	námět
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
zabýval	zabývat	k5eAaImAgInS	zabývat
všelidskými	všelidský	k2eAgInPc7d1	všelidský
etickými	etický	k2eAgInPc7d1	etický
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
objevit	objevit	k5eAaPmF	objevit
zdroj	zdroj	k1gInSc4	zdroj
věčného	věčný	k2eAgNnSc2d1	věčné
zápolení	zápolení	k1gNnSc2	zápolení
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
pečeť	pečeť	k1gFnSc4	pečeť
věčnosti	věčnost	k1gFnSc2	věčnost
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lagerkvist	Lagerkvist	k1gInSc1	Lagerkvist
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
literatuře	literatura	k1gFnSc6	literatura
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
svým	svůj	k3xOyFgMnPc3	svůj
rozsáhlým	rozsáhlý	k2eAgMnPc3d1	rozsáhlý
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
aktuálním	aktuální	k2eAgInSc7d1	aktuální
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
spojujícím	spojující	k2eAgInSc7d1	spojující
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
modernost	modernost	k1gFnSc4	modernost
<g/>
,	,	kIx,	,
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
směry	směr	k1gInPc4	směr
a	a	k8xC	a
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgMnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
osobnosti	osobnost	k1gFnPc4	osobnost
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
literární	literární	k2eAgInSc4d1	literární
projev	projev	k1gInSc4	projev
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
příznačná	příznačný	k2eAgFnSc1d1	příznačná
dramatičnost	dramatičnost	k1gFnSc1	dramatičnost
dějů	děj	k1gInPc2	děj
a	a	k8xC	a
strohé	strohý	k2eAgNnSc1d1	strohé
soustředění	soustředění	k1gNnSc1	soustředění
pozornosti	pozornost	k1gFnSc2	pozornost
na	na	k7c4	na
postižení	postižení	k1gNnSc4	postižení
nejvlastnější	vlastní	k2eAgFnSc2d3	nejvlastnější
tváře	tvář	k1gFnSc2	tvář
jejich	jejich	k3xOp3gMnPc2	jejich
hrdinů	hrdina	k1gMnPc2	hrdina
i	i	k8xC	i
úsilí	úsilí	k1gNnSc4	úsilí
proniknout	proniknout	k5eAaPmF	proniknout
k	k	k7c3	k
pramenům	pramen	k1gInPc3	pramen
záporu	zápor	k1gInSc2	zápor
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
v	v	k7c6	v
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
životě	život	k1gInSc6	život
i	i	k8xC	i
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
aby	aby	k9	aby
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
vynikly	vyniknout	k5eAaPmAgInP	vyniknout
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc4	jejich
protipól	protipól	k1gInSc4	protipól
klad	klad	k1gInSc1	klad
a	a	k8xC	a
dobro	dobro	k1gNnSc1	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
obdržel	obdržet	k5eAaPmAgInS	obdržet
prestižní	prestižní	k2eAgFnSc4d1	prestižní
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Devíti	devět	k4xCc2	devět
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
za	za	k7c4	za
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
závažnost	závažnost	k1gFnSc4	závažnost
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
literárním	literární	k2eAgNnSc6d1	literární
díle	dílo	k1gNnSc6	dílo
hledá	hledat	k5eAaImIp3nS	hledat
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
věčné	věčný	k2eAgFnPc4d1	věčná
otázky	otázka	k1gFnPc4	otázka
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pär	Pär	k?	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Människor	Människor	k1gInSc1	Människor
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Lagerkvistova	Lagerkvistův	k2eAgFnSc1d1	Lagerkvistův
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ordkonst	Ordkonst	k1gFnSc1	Ordkonst
och	och	k0	och
bildkonst	bildkonst	k1gMnSc1	bildkonst
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Slovesné	slovesný	k2eAgNnSc4d1	slovesné
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Motiv	motiv	k1gInSc1	motiv
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Lagerkvistova	Lagerkvistův	k2eAgFnSc1d1	Lagerkvistův
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Järn	Järn	k1gMnSc1	Järn
och	och	k0	och
människor	människor	k1gMnSc1	människor
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
Železo	železo	k1gNnSc1	železo
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Å	Å	k?	Å
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Úzkost	úzkost	k1gFnSc1	úzkost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
sista	sista	k1gMnSc1	sista
människan	människan	k1gMnSc1	människan
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Lagerkvistova	Lagerkvistův	k2eAgFnSc1d1	Lagerkvistův
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Teater	teatrum	k1gNnPc2	teatrum
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
na	na	k7c4	na
divadelní	divadelní	k2eAgNnSc4d1	divadelní
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
vznesl	vznést	k5eAaPmAgMnS	vznést
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ibsenovský	ibsenovský	k2eAgInSc1d1	ibsenovský
realismus	realismus	k1gInSc1	realismus
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
stylizované	stylizovaný	k2eAgNnSc1d1	stylizované
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
pozdního	pozdní	k2eAgInSc2d1	pozdní
Augusta	Augusta	k1gMnSc1	Augusta
Strindberga	Strindberg	k1gMnSc4	Strindberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
svå	svå	k?	svå
stunden	stundna	k1gFnPc2	stundna
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Těžká	těžký	k2eAgFnSc1d1	těžká
chvíle	chvíle	k1gFnSc1	chvíle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trojice	trojice	k1gFnSc1	trojice
jednoaktovek	jednoaktovka	k1gFnPc2	jednoaktovka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
snažil	snažit	k5eAaImAgMnS	snažit
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
uplatnit	uplatnit	k5eAaPmF	uplatnit
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaos	Kaos	k1gInSc1	Kaos
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Chaos	chaos	k1gInSc1	chaos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
prózu	próza	k1gFnSc4	próza
a	a	k8xC	a
drama	drama	k1gNnSc4	drama
Himlens	Himlensa	k1gFnPc2	Himlensa
hemlighet	hemligheta	k1gFnPc2	hemligheta
(	(	kIx(	(
<g/>
Tajemství	tajemství	k1gNnSc1	tajemství
nebes	nebesa	k1gNnPc2	nebesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Det	Det	k?	Det
eviga	eviga	k1gFnSc1	eviga
leendet	leendet	k1gMnSc1	leendet
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Věčný	věčný	k2eAgInSc1d1	věčný
úsměv	úsměv	k1gInSc1	úsměv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
lyckliges	lyckliges	k1gInSc1	lyckliges
väg	väg	k?	väg
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Šťastlivcova	šťastlivcův	k2eAgFnSc1d1	šťastlivcův
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
osynlige	osynlig	k1gFnSc2	osynlig
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Onda	Onda	k1gMnSc1	Onda
sagor	sagor	k1gMnSc1	sagor
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Zlé	zlý	k2eAgFnPc4d1	zlá
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
satirických	satirický	k2eAgFnPc2d1	satirická
kafkovských	kafkovský	k2eAgFnPc2d1	kafkovská
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Gäst	Gäst	k1gInSc1	Gäst
hos	hos	k?	hos
verkligheten	verklighést	k5eAaPmNgInS	verklighést
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hjärtats	Hjärtats	k1gInSc1	Hjärtats
så	så	k?	så
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
srdce	srdce	k1gNnSc2	srdce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Det	Det	k?	Det
besegrade	besegrad	k1gInSc5	besegrad
livet	livet	k1gMnSc1	livet
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Poražený	poražený	k2eAgInSc1d1	poražený
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Han	Hana	k1gFnPc2	Hana
som	soma	k1gFnPc2	soma
fick	fick	k1gInSc1	fick
leva	leva	k1gInSc2	leva
om	om	k?	om
sitt	sitt	k1gMnSc1	sitt
liv	liv	k?	liv
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
směl	smět	k5eAaImAgInS	smět
znovu	znovu	k6eAd1	znovu
žít	žít	k5eAaImF	žít
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Kämpande	Kämpand	k1gInSc5	Kämpand
ande	ande	k1gNnPc1	ande
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Bojující	bojující	k2eAgMnSc1d1	bojující
duch	duch	k1gMnSc1	duch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Vid	vid	k1gInSc1	vid
lägereld	lägereld	k1gInSc1	lägereld
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
U	u	k7c2	u
táborového	táborový	k2eAgInSc2d1	táborový
ohně	oheň	k1gInSc2	oheň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bödeln	Bödeln	k1gNnSc1	Bödeln
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Kat	kat	k1gMnSc1	kat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
i	i	k8xC	i
drama	drama	k1gNnSc1	drama
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
o	o	k7c6	o
nezničitelnosti	nezničitelnost	k1gFnSc6	nezničitelnost
a	a	k8xC	a
moci	moc	k1gFnSc6	moc
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
knutna	knutn	k1gInSc2	knutn
näven	nävna	k1gFnPc2	nävna
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Zaťatá	zaťatý	k2eAgFnSc1d1	zaťatá
pěst	pěst	k1gFnSc1	pěst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestopis	cestopis	k1gInSc1	cestopis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
befriage	befriag	k1gFnSc2	befriag
människan	människana	k1gFnPc2	människana
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Osvobozený	osvobozený	k2eAgMnSc1d1	osvobozený
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
I	i	k9	i
den	den	k1gInSc4	den
tiden	tiden	k1gInSc1	tiden
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Za	za	k7c2	za
těch	ten	k3xDgInPc2	ten
časů	čas	k1gInPc2	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mannen	Mannen	k2eAgInSc1d1	Mannen
utan	utan	k1gInSc1	utan
sjå	sjå	k?	sjå
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Muž	muž	k1gMnSc1	muž
bez	bez	k7c2	bez
duše	duše	k1gFnSc2	duše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Seger	Seger	k?	Seger
i	i	k9	i
mörker	mörker	k1gMnSc1	mörker
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Så	Så	k?	Så
och	och	k0	och
strid	strida	k1gFnPc2	strida
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
a	a	k8xC	a
boj	boj	k1gInSc1	boj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Midsommardröm	Midsommardröm	k6eAd1	Midsommardröm
i	i	k9	i
fattighuset	fattighuset	k1gMnSc1	fattighuset
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hemmet	Hemmet	k1gMnSc1	Hemmet
och	och	k0	och
stjärnan	stjärnan	k1gMnSc1	stjärnan
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Domov	domov	k1gInSc1	domov
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dvärgen	Dvärgen	k1gInSc1	Dvärgen
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
autorův	autorův	k2eAgInSc4d1	autorův
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
zlo	zlo	k1gNnSc4	zlo
a	a	k8xC	a
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
fašismus	fašismus	k1gInSc1	fašismus
zavalil	zavalit	k5eAaPmAgInS	zavalit
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
zosobněním	zosobnění	k1gNnSc7	zosobnění
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
lidská	lidský	k2eAgFnSc1d1	lidská
zrůda	zrůda	k1gFnSc1	zrůda
<g/>
,	,	kIx,	,
trpaslík	trpaslík	k1gMnSc1	trpaslík
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
kteréhosi	kterýsi	k3yIgNnSc2	kterýsi
severoitalského	severoitalský	k2eAgNnSc2d1	severoitalské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zvrhlým	zvrhlý	k2eAgInSc7d1	zvrhlý
zájmem	zájem	k1gInSc7	zájem
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
nejnižší	nízký	k2eAgInPc4d3	nejnižší
lidské	lidský	k2eAgInPc4d1	lidský
pudy	pud	k1gInPc4	pud
násilnictví	násilnictví	k1gNnSc2	násilnictví
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc6d2	veliký
moci	moc	k1gFnSc6	moc
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
knížecího	knížecí	k2eAgMnSc2d1	knížecí
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
De	De	k?	De
vises	vises	k1gInSc1	vises
sten	sten	k1gInSc1	sten
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lå	Lå	k?	Lå
människan	människan	k1gInSc1	människan
leva	leva	k1gInSc1	leva
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Nechte	nechat	k5eAaPmRp2nP	nechat
člověka	člověk	k1gMnSc4	člověk
žít	žít	k5eAaImF	žít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Barabbas	Barabbas	k1gInSc1	Barabbas
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Barabáš	Barabáš	k1gFnSc1	Barabáš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
dramatický	dramatický	k2eAgInSc4d1	dramatický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc4d1	další
autorovo	autorův	k2eAgNnSc4d1	autorovo
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
drsný	drsný	k2eAgMnSc1d1	drsný
starověký	starověký	k2eAgMnSc1d1	starověký
Barabáš	Barabáš	k1gMnSc1	Barabáš
<g/>
,	,	kIx,	,
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc6	smrt
ukřižováním	ukřižování	k1gNnSc7	ukřižování
a	a	k8xC	a
potom	potom	k6eAd1	potom
osvobozený	osvobozený	k2eAgInSc1d1	osvobozený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svědkem	svědek	k1gMnSc7	svědek
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
a	a	k8xC	a
rozumu	rozum	k1gInSc3	rozum
strhován	strhován	k2eAgMnSc1d1	strhován
příkladem	příklad	k1gInSc7	příklad
prvních	první	k4xOgInPc2	první
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Osamocen	osamocen	k2eAgMnSc1d1	osamocen
mimo	mimo	k7c4	mimo
jejich	jejich	k3xOp3gNnSc4	jejich
společenství	společenství	k1gNnSc4	společenství
hledá	hledat	k5eAaImIp3nS	hledat
jejich	jejich	k3xOp3gFnSc4	jejich
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
jistotu	jistota	k1gFnSc4	jistota
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
on	on	k3xPp3gMnSc1	on
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
na	na	k7c6	na
biblickém	biblický	k2eAgInSc6d1	biblický
příběhu	příběh	k1gInSc6	příběh
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
symboliku	symbolika	k1gFnSc4	symbolika
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
rozpolceného	rozpolcený	k2eAgMnSc2d1	rozpolcený
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
životní	životní	k2eAgFnSc4d1	životní
jistotu	jistota	k1gFnSc4	jistota
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
švédským	švédský	k2eAgMnSc7d1	švédský
režisérem	režisér	k1gMnSc7	režisér
Alfem	Alf	k1gMnSc7	Alf
Sjöbergem	Sjöberg	k1gMnSc7	Sjöberg
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
americkým	americký	k2eAgMnSc7d1	americký
režisérem	režisér	k1gMnSc7	režisér
Richardem	Richard	k1gMnSc7	Richard
Fleischerem	Fleischer	k1gMnSc7	Fleischer
s	s	k7c7	s
Anthonym	Anthonym	k1gInSc1	Anthonym
Quinnem	Quinn	k1gInSc7	Quinn
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aftonland	Aftonland	k1gInSc1	Aftonland
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Večerní	večerní	k2eAgFnSc1d1	večerní
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Sibyllan	Sibyllan	k1gInSc1	Sibyllan
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Sibyla	Sibyla	k1gFnSc1	Sibyla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
Věčném	věčný	k2eAgMnSc6d1	věčný
Židovi	Žid	k1gMnSc6	Žid
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ahasverus	Ahasverus	k1gInSc1	Ahasverus
död	död	k?	död
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Ahasverova	Ahasverův	k2eAgFnSc1d1	Ahasverův
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
dokončení	dokončení	k1gNnSc1	dokončení
vyprávění	vyprávění	k1gNnSc2	vyprávění
o	o	k7c6	o
Věčném	věčný	k2eAgMnSc6d1	věčný
Židovi	Žid	k1gMnSc6	Žid
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Ahasver	Ahasver	k1gMnSc1	Ahasver
konečně	konečně	k6eAd1	konečně
najde	najít	k5eAaPmIp3nS	najít
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pilgrim	Pilgrim	k1gMnSc1	Pilgrim
på	på	k?	på
havet	havet	k1gMnSc1	havet
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Poutník	poutník	k1gMnSc1	poutník
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Det	Det	k?	Det
heliga	heliga	k1gFnSc1	heliga
landet	landet	k1gMnSc1	landet
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Zaslíbená	zaslíbený	k2eAgFnSc1d1	zaslíbená
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mariamne	Mariamnout	k5eAaImIp3nS	Mariamnout
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
milostná	milostný	k2eAgFnSc1d1	milostná
novela	novela	k1gFnSc1	novela
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Heroda	Herod	k1gMnSc2	Herod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Vohryzek	Vohryzka	k1gFnPc2	Vohryzka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Radko	Radka	k1gFnSc5	Radka
Kejzlar	Kejzlar	k1gInSc1	Kejzlar
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Barabáš	Barabat	k5eAaImIp2nS	Barabat
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Dagmar	Dagmar	k1gFnSc1	Dagmar
Chvojková-Pallasová	Chvojková-Pallasová	k1gFnSc1	Chvojková-Pallasová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Kat	kat	k1gInSc1	kat
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
NPL	NPL	kA	NPL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Vohryzek	Vohryzka	k1gFnPc2	Vohryzka
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sedm	sedm	k4xCc4	sedm
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
:	:	kIx,	:
Kat	kat	k1gMnSc1	kat
<g/>
,	,	kIx,	,
Zlý	zlý	k1gMnSc1	zlý
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc3	Maurika
Fleury	Fleura	k1gFnSc2	Fleura
<g/>
,	,	kIx,	,
Smrt	smrt	k1gFnSc1	smrt
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
Rudá	rudý	k2eAgFnSc1d1	rudá
záře	záře	k1gFnSc1	záře
<g/>
,	,	kIx,	,
Svatební	svatební	k2eAgFnSc1d1	svatební
hostina	hostina	k1gFnSc1	hostina
a	a	k8xC	a
Poutník	poutník	k1gMnSc1	poutník
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
daleké	daleký	k2eAgFnSc6d1	daleká
pouti	pouť	k1gFnSc6	pouť
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Libor	Libor	k1gMnSc1	Libor
Štukavec	Štukavec	k1gMnSc1	Štukavec
a	a	k8xC	a
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgFnPc4	čtyři
novely	novela	k1gFnPc4	novela
<g/>
:	:	kIx,	:
Sibyla	Sibyla	k1gFnSc1	Sibyla
<g/>
,	,	kIx,	,
Ahasverova	Ahasverův	k2eAgFnSc1d1	Ahasverův
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Poutník	poutník	k1gMnSc1	poutník
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
Svatá	svatý	k2eAgFnSc1d1	svatá
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ČeskyVOHRYZEK	ČeskyVOHRYZEK	k?	ČeskyVOHRYZEK
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Doslov	doslov	k1gInSc1	doslov
in	in	k?	in
<g/>
:	:	kIx,	:
Kat	kat	k1gMnSc1	kat
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
NPL	NPL	kA	NPL
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
s.	s.	k?	s.
201	[number]	k4	201
<g/>
–	–	k?	–
<g/>
205	[number]	k4	205
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERNÍK	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
<g/>
.	.	kIx.	.
</s>
<s>
LAGERKVIST	LAGERKVIST	kA	LAGERKVIST
Pär	Pär	k1gFnSc1	Pär
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc1	heslo
in	in	k?	in
<g/>
:	:	kIx,	:
HARTLOVÁ	Hartlová	k1gFnSc1	Hartlová
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
severských	severský	k2eAgMnPc2d1	severský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgFnPc1d1	doplněná
a	a	k8xC	a
aktualizované	aktualizovaný	k2eAgNnSc1d1	aktualizované
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
302	[number]	k4	302
<g/>
–	–	k?	–
<g/>
303	[number]	k4	303
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HUMPÁL	HUMPÁL	kA	HUMPÁL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
skandinávské	skandinávský	k2eAgFnPc1d1	skandinávská
literatury	literatura	k1gFnPc1	literatura
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
s.	s.	k?	s.
182	[number]	k4	182
<g/>
–	–	k?	–
<g/>
185	[number]	k4	185
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1174	[number]	k4	1174
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
švédských	švédský	k2eAgMnPc2d1	švédský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pär	Pär	k1gFnSc2	Pär
Lagerkvist	Lagerkvist	k1gInSc1	Lagerkvist
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Pär	Pär	k1gMnSc1	Pär
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Official	Official	k1gMnSc1	Official
Homepage	Homepag	k1gInSc2	Homepag
of	of	k?	of
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
(	(	kIx(	(
<g/>
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pär	Pär	k1gMnSc1	Pär
Fabian	Fabian	k1gMnSc1	Fabian
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Pär	Pär	k1gFnSc1	Pär
Lagerkvist	Lagerkvist	k1gMnSc1	Lagerkvist
(	(	kIx(	(
<g/>
Barabáš	Barabáš	k1gMnSc1	Barabáš
<g/>
)	)	kIx)	)
</s>
</p>
