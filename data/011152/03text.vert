<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Leo	Leo	k1gMnSc1	Leo
Schneider	Schneider	k1gMnSc1	Schneider
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Kyjov	Kyjov	k1gInSc1	Kyjov
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
strojní	strojní	k2eAgMnSc1d1	strojní
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
lodního	lodní	k2eAgInSc2d1	lodní
šroubu	šroub	k1gInSc2	šroub
Voith-Schneider	Voith-Schneidra	k1gFnPc2	Voith-Schneidra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kyjově	kyjově	k6eAd1	kyjově
gymnazijnímu	gymnazijní	k2eAgMnSc3d1	gymnazijní
profesorovi	profesor	k1gMnSc3	profesor
Franzi	Franze	k1gFnSc4	Franze
Schneiderovi	Schneider	k1gMnSc3	Schneider
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
manželce	manželka	k1gFnSc3	manželka
Henrietě	Henrieta	k1gFnSc3	Henrieta
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Trutnova	Trutnov	k1gInSc2	Trutnov
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
babičky	babička	k1gFnPc1	babička
byly	být	k5eAaImAgFnP	být
Češky	Češka	k1gFnPc4	Češka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Gmundenu	Gmunden	k1gInSc6	Gmunden
a	a	k8xC	a
Linci	Linec	k1gInSc6	Linec
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
maturoval	maturovat	k5eAaBmAgMnS	maturovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
studoval	studovat	k5eAaImAgMnS	studovat
strojírenství	strojírenství	k1gNnSc4	strojírenství
a	a	k8xC	a
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
plachtění	plachtění	k1gNnSc2	plachtění
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
cykloidních	cykloidní	k2eAgFnPc2d1	cykloidní
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
lodní	lodní	k2eAgInSc4d1	lodní
šroub	šroub	k1gInSc4	šroub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojuje	spojovat	k5eAaImIp3nS	spojovat
pohon	pohon	k1gInSc4	pohon
i	i	k8xC	i
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Voith	Voitha	k1gFnPc2	Voitha
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
šroub	šroub	k1gInSc4	šroub
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
na	na	k7c6	na
člunu	člun	k1gInSc6	člun
Torqueo	Torqueo	k1gNnSc1	Torqueo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
instaloval	instalovat	k5eAaBmAgInS	instalovat
dva	dva	k4xCgInPc4	dva
takové	takový	k3xDgInPc4	takový
šrouby	šroub	k1gInPc4	šroub
do	do	k7c2	do
dunajského	dunajský	k2eAgInSc2d1	dunajský
remorkéru	remorkér	k1gInSc2	remorkér
Uhu	Uhu	k1gFnPc2	Uhu
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
šroubem	šroub	k1gInSc7	šroub
vybavena	vybaven	k2eAgFnSc1d1	vybavena
zaoceánská	zaoceánský	k2eAgFnSc1d1	zaoceánská
loď	loď	k1gFnSc1	loď
Makrele	makrela	k1gFnSc3	makrela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
svůj	svůj	k3xOyFgInSc4	svůj
výzkum	výzkum	k1gInSc4	výzkum
shrnul	shrnout	k5eAaPmAgMnS	shrnout
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
Cykloidová	Cykloidový	k2eAgNnPc1d1	Cykloidový
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
kinematika	kinematika	k1gFnSc1	kinematika
<g/>
,	,	kIx,	,
vztahy	vztah	k1gInPc1	vztah
proudění	proudění	k1gNnSc2	proudění
a	a	k8xC	a
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
souvislosti	souvislost	k1gFnSc2	souvislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
