<p>
<s>
Corn	Corn	k1gNnSc1	Corn
dog	doga	k1gFnPc2	doga
je	být	k5eAaImIp3nS	být
specialita	specialita	k1gFnSc1	specialita
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
kukuřičný	kukuřičný	k2eAgMnSc1d1	kukuřičný
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
variantu	varianta	k1gFnSc4	varianta
hot	hot	k0	hot
dogu	doga	k1gFnSc4	doga
<g/>
.	.	kIx.	.
</s>
<s>
Párek	párek	k1gInSc1	párek
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
uzenina	uzenina	k1gFnSc1	uzenina
(	(	kIx(	(
<g/>
klobása	klobása	k1gFnSc1	klobása
<g/>
,	,	kIx,	,
chorizo	choriza	k1gFnSc5	choriza
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
napíchne	napíchnout	k5eAaPmIp3nS	napíchnout
na	na	k7c4	na
dřívko	dřívko	k1gNnSc4	dřívko
<g/>
,	,	kIx,	,
obalí	obalit	k5eAaPmIp3nP	obalit
v	v	k7c6	v
těstíčku	těstíčko	k1gNnSc6	těstíčko
z	z	k7c2	z
kukuřičné	kukuřičný	k2eAgFnSc2d1	kukuřičná
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
usmaží	usmažit	k5eAaPmIp3nS	usmažit
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
do	do	k7c2	do
zlatohněda	zlatohnědo	k1gNnSc2	zlatohnědo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
se	se	k3xPyFc4	se
corn	corn	k1gInSc1	corn
dog	doga	k1gFnPc2	doga
drží	držet	k5eAaImIp3nS	držet
za	za	k7c4	za
dřívko	dřívko	k1gNnSc4	dřívko
<g/>
,	,	kIx,	,
podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
s	s	k7c7	s
kečupem	kečup	k1gInSc7	kečup
a	a	k8xC	a
hořčicí	hořčice	k1gFnSc7	hořčice
<g/>
,	,	kIx,	,
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
přílohou	příloha	k1gFnSc7	příloha
jsou	být	k5eAaImIp3nP	být
hranolky	hranolek	k1gInPc1	hranolek
nebo	nebo	k8xC	nebo
tater	tatra	k1gFnPc2	tatra
tots	tots	k6eAd1	tots
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokrm	pokrm	k1gInSc4	pokrm
začali	začít	k5eAaPmAgMnP	začít
připravovat	připravovat	k5eAaImF	připravovat
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
němečtí	německý	k2eAgMnPc1d1	německý
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Corn	Corn	k1gInSc4	Corn
dogy	doga	k1gFnPc1	doga
byly	být	k5eAaImAgFnP	být
typickým	typický	k2eAgNnSc7d1	typické
jídlem	jídlo	k1gNnSc7	jídlo
na	na	k7c6	na
státním	státní	k2eAgInSc6d1	státní
veletrhu	veletrh	k1gInSc6	veletrh
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
poutě	pouť	k1gFnPc4	pouť
a	a	k8xC	a
veřejné	veřejný	k2eAgFnPc4d1	veřejná
slavnosti	slavnost	k1gFnPc4	slavnost
po	po	k7c6	po
celých	celá	k1gFnPc6	celá
USA	USA	kA	USA
<g/>
,	,	kIx,	,
podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Disneylandu	Disneyland	k1gInSc6	Disneyland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
supermarketech	supermarket	k1gInPc6	supermarket
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
koupit	koupit	k5eAaPmF	koupit
jako	jako	k9	jako
polotovary	polotovar	k1gInPc1	polotovar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stačí	stačit	k5eAaBmIp3nP	stačit
pouze	pouze	k6eAd1	pouze
usmažit	usmažit	k5eAaPmF	usmažit
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
populárním	populární	k2eAgNnSc7d1	populární
pouličním	pouliční	k2eAgNnSc7d1	pouliční
občerstvením	občerstvení	k1gNnSc7	občerstvení
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
panchuker	panchukra	k1gFnPc2	panchukra
a	a	k8xC	a
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
jako	jako	k8xS	jako
pogo	pogo	k6eAd1	pogo
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
březnová	březnový	k2eAgFnSc1d1	březnová
sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
finále	finále	k1gNnSc7	finále
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
basketbalového	basketbalový	k2eAgNnSc2d1	basketbalové
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
National	National	k1gMnSc1	National
Corndog	Corndog	k1gMnSc1	Corndog
Day	Day	k1gMnSc1	Day
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Corn	Corna	k1gFnPc2	Corna
dog	doga	k1gFnPc2	doga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Science	Science	k1gFnSc1	Science
Of	Of	k1gFnSc2	Of
Corn	Corna	k1gFnPc2	Corna
Dogs	Dogsa	k1gFnPc2	Dogsa
<g/>
.	.	kIx.	.
</s>
<s>
CBS	CBS	kA	CBS
News	News	k1gInSc1	News
</s>
</p>
<p>
<s>
The	The	k?	The
Short	Short	k1gInSc1	Short
<g/>
,	,	kIx,	,
Sad	sad	k1gInSc1	sad
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Corn	Corn	k1gInSc1	Corn
Dog	doga	k1gFnPc2	doga
<g/>
.	.	kIx.	.
</s>
<s>
Village	Village	k1gFnSc1	Village
Voice	Voice	k1gFnSc2	Voice
</s>
</p>
<p>
<s>
National	Nationat	k5eAaImAgMnS	Nationat
Corndog	Corndog	k1gMnSc1	Corndog
Day	Day	k1gMnSc1	Day
</s>
</p>
