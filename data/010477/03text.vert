<p>
<s>
Kloub	kloub	k1gInSc1	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k6eAd1	articulatio
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc4	spojení
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ohebné	ohebný	k2eAgNnSc4d1	ohebné
místo	místo	k1gNnSc4	místo
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
stýkajících	stýkající	k2eAgFnPc2d1	stýkající
dvou	dva	k4xCgFnPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
určité	určitý	k2eAgFnSc2d1	určitá
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anatomie	anatomie	k1gFnSc2	anatomie
kloubu	kloub	k1gInSc2	kloub
==	==	k?	==
</s>
</p>
<p>
<s>
Kloub	kloub	k1gInSc1	kloub
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
styčných	styčný	k2eAgFnPc2d1	styčná
ploch	plocha	k1gFnPc2	plocha
dvou	dva	k4xCgFnPc2	dva
kostí	kost	k1gFnPc2	kost
krytých	krytý	k2eAgFnPc2d1	krytá
chrupavkou	chrupavka	k1gFnSc7	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
plocha	plocha	k1gFnSc1	plocha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kloubní	kloubní	k2eAgFnSc1d1	kloubní
hlavice	hlavice	k1gFnSc1	hlavice
(	(	kIx(	(
<g/>
vypouklý	vypouklý	k2eAgInSc1d1	vypouklý
konec	konec	k1gInSc1	konec
jedné	jeden	k4xCgFnSc2	jeden
kosti	kost	k1gFnSc2	kost
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
kloubní	kloubní	k2eAgFnSc1d1	kloubní
jamka	jamka	k1gFnSc1	jamka
(	(	kIx(	(
<g/>
vyhloubený	vyhloubený	k2eAgInSc1d1	vyhloubený
konec	konec	k1gInSc1	konec
druhé	druhý	k4xOgFnSc2	druhý
kosti	kost	k1gFnSc2	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
velkých	velký	k2eAgInPc2d1	velký
kloubů	kloub	k1gInPc2	kloub
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
meniskus	meniskus	k1gInSc1	meniskus
či	či	k8xC	či
labrum	labrum	k1gInSc1	labrum
(	(	kIx(	(
<g/>
kolenní	kolenní	k2eAgFnSc1d1	kolenní
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ramenní	ramenní	k2eAgInSc1d1	ramenní
kloub	kloub	k1gInSc1	kloub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
měkčí	měkký	k2eAgInPc1d2	měkčí
okraje	okraj	k1gInPc1	okraj
kloubních	kloubní	k2eAgFnPc2d1	kloubní
jamek	jamka	k1gFnPc2	jamka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
slouží	sloužit	k5eAaImIp3nP	sloužit
lepší	dobrý	k2eAgFnSc4d2	lepší
artikulaci	artikulace	k1gFnSc4	artikulace
kloubu	kloub	k1gInSc2	kloub
a	a	k8xC	a
jemnému	jemný	k2eAgNnSc3d1	jemné
dopružení	dopružení	k1gNnSc3	dopružení
při	při	k7c6	při
rotacích	rotace	k1gFnPc6	rotace
a	a	k8xC	a
torzích	torze	k1gFnPc6	torze
kloubu	kloub	k1gInSc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
kloubní	kloubní	k2eAgNnSc4d1	kloubní
pouzdro	pouzdro	k1gNnSc4	pouzdro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
kloub	kloub	k1gInSc4	kloub
obepíná	obepínat	k5eAaImIp3nS	obepínat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zesíleno	zesílen	k2eAgNnSc1d1	zesíleno
Vazy	vaz	k1gInPc7	vaz
<g/>
,	,	kIx,	,
z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
ho	on	k3xPp3gMnSc4	on
vystýlá	vystýlat	k5eAaImIp3nS	vystýlat
synoviální	synoviální	k2eAgFnSc1d1	synoviální
membrána	membrána	k1gFnSc1	membrána
produkující	produkující	k2eAgFnSc4d1	produkující
synoviální	synoviální	k2eAgFnSc4d1	synoviální
tekutinu	tekutina	k1gFnSc4	tekutina
(	(	kIx(	(
<g/>
kloubní	kloubní	k2eAgInSc1d1	kloubní
maz	maz	k1gInSc1	maz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
tření	tření	k1gNnSc4	tření
<g/>
,	,	kIx,	,
vyživuje	vyživovat	k5eAaImIp3nS	vyživovat
kloubní	kloubní	k2eAgFnPc4d1	kloubní
chrupavky	chrupavka	k1gFnPc4	chrupavka
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pevné	pevný	k2eAgNnSc1d1	pevné
přilnutí	přilnutí	k1gNnSc1	přilnutí
kloubních	kloubní	k2eAgFnPc2d1	kloubní
ploch	plocha	k1gFnPc2	plocha
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
dvě	dva	k4xCgFnPc1	dva
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
složených	složený	k2eAgInPc2d1	složený
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
kosti	kost	k1gFnPc4	kost
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
vsunuty	vsunut	k2eAgFnPc1d1	vsunuta
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
chrupavčité	chrupavčitý	k2eAgFnPc1d1	chrupavčitá
destičky	destička	k1gFnPc1	destička
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vyrovnávají	vyrovnávat	k5eAaImIp3nP	vyrovnávat
nerovnosti	nerovnost	k1gFnPc4	nerovnost
v	v	k7c6	v
zakřivení	zakřivení	k1gNnSc6	zakřivení
styčných	styčný	k2eAgFnPc2d1	styčná
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
současné	současný	k2eAgNnSc4d1	současné
provádění	provádění	k1gNnSc4	provádění
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
pohybů	pohyb	k1gInPc2	pohyb
v	v	k7c6	v
kloubu	kloub	k1gInSc6	kloub
(	(	kIx(	(
<g/>
ohýbání	ohýbání	k1gNnSc2	ohýbání
a	a	k8xC	a
otáčení	otáčení	k1gNnSc2	otáčení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Klouby	kloub	k1gInPc1	kloub
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
pohybu	pohyb	k1gInSc2	pohyb
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
lokomoci	lokomoce	k1gFnSc4	lokomoce
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
sebeobsluze	sebeobsluze	k1gFnSc2	sebeobsluze
apod	apod	kA	apod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
klouby	kloub	k1gInPc1	kloub
používány	používat	k5eAaImNgInP	používat
patřičně	patřičně	k6eAd1	patřičně
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přetěžování	přetěžování	k1gNnSc6	přetěžování
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
chronicky	chronicky	k6eAd1	chronicky
zkrácených	zkrácený	k2eAgInPc6d1	zkrácený
svalech	sval	k1gInPc6	sval
či	či	k8xC	či
nefyziologickém	fyziologický	k2eNgNnSc6d1	nefyziologické
používání	používání	k1gNnSc6	používání
kloubů	kloub	k1gInPc2	kloub
(	(	kIx(	(
<g/>
po	po	k7c6	po
nesprávných	správný	k2eNgFnPc6d1	nesprávná
drahách	draha	k1gFnPc6	draha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vlivem	vliv	k1gInSc7	vliv
špatného	špatný	k2eAgNnSc2d1	špatné
vyživení	vyživení	k1gNnSc2	vyživení
kloubu	kloub	k1gInSc2	kloub
či	či	k8xC	či
kloubními	kloubní	k2eAgNnPc7d1	kloubní
onemocněními	onemocnění	k1gNnPc7	onemocnění
<g/>
,	,	kIx,	,
chrupavčitý	chrupavčitý	k2eAgInSc4d1	chrupavčitý
povrch	povrch	k1gInSc4	povrch
kloubu	kloub	k1gInSc2	kloub
degeneruje	degenerovat	k5eAaBmIp3nS	degenerovat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
tuhnou	tuhnout	k5eAaImIp3nP	tuhnout
okolní	okolní	k2eAgInPc4d1	okolní
vazy	vaz	k1gInPc4	vaz
a	a	k8xC	a
šlachy	šlacha	k1gFnPc4	šlacha
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Kloub	kloub	k1gInSc1	kloub
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
své	svůj	k3xOyFgInPc4	svůj
rozsahy	rozsah	k1gInPc4	rozsah
<g/>
,	,	kIx,	,
pružnost	pružnost	k1gFnSc1	pružnost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
joint-play	jointla	k2eAgMnPc4d1	joint-pla
<g/>
)	)	kIx)	)
a	a	k8xC	a
bolest	bolest	k1gFnSc1	bolest
začíná	začínat	k5eAaImIp3nS	začínat
i	i	k9	i
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
tkáních	tkáň	k1gFnPc6	tkáň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
tíhových	tíhový	k2eAgInPc6d1	tíhový
váčcích	váček	k1gInPc6	váček
a	a	k8xC	a
svalech	sval	k1gInPc6	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
snížené	snížený	k2eAgFnSc3d1	snížená
pohyblivosti	pohyblivost	k1gFnSc3	pohyblivost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
kloubní	kloubní	k2eAgNnPc1d1	kloubní
omezení	omezení	k1gNnPc1	omezení
dále	daleko	k6eAd2	daleko
zacyklí	zacyklit	k5eAaPmIp3nP	zacyklit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časných	časný	k2eAgNnPc6d1	časné
stádiích	stádium	k1gNnPc6	stádium
kloubní	kloubní	k2eAgFnSc2d1	kloubní
bolesti	bolest	k1gFnSc2	bolest
a	a	k8xC	a
u	u	k7c2	u
raných	raný	k2eAgFnPc2d1	raná
artróz	artróza	k1gFnPc2	artróza
je	být	k5eAaImIp3nS	být
metodou	metoda	k1gFnSc7	metoda
volby	volba	k1gFnSc2	volba
manuální	manuální	k2eAgFnSc2d1	manuální
fyzioterapie	fyzioterapie	k1gFnSc2	fyzioterapie
-	-	kIx~	-
konkr.	konkr.	k?	konkr.
měkké	měkký	k2eAgFnSc2d1	měkká
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
kloubní	kloubní	k2eAgFnSc1d1	kloubní
mobilizace	mobilizace	k1gFnSc1	mobilizace
a	a	k8xC	a
individuální	individuální	k2eAgNnSc1d1	individuální
cvičení	cvičení	k1gNnSc1	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
i	i	k9	i
balneoléčba	balneoléčba	k1gFnSc1	balneoléčba
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
svižná	svižný	k2eAgFnSc1d1	svižná
chůze	chůze	k1gFnSc1	chůze
<g/>
,	,	kIx,	,
hluboká	hluboký	k2eAgFnSc1d1	hluboká
ventilace	ventilace	k1gFnSc1	ventilace
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
===	===	k?	===
Kloubní	kloubní	k2eAgFnSc2d1	kloubní
blokády	blokáda	k1gFnSc2	blokáda
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
náhlých	náhlý	k2eAgNnPc6d1	náhlé
přetíženích	přetížení	k1gNnPc6	přetížení
kloubu	kloub	k1gInSc2	kloub
či	či	k8xC	či
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
zaujímání	zaujímání	k1gNnSc6	zaujímání
nefyziologických	fyziologický	k2eNgFnPc2d1	nefyziologická
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
předsunuté	předsunutý	k2eAgNnSc1d1	předsunuté
držení	držení	k1gNnSc1	držení
hlavy	hlava	k1gFnSc2	hlava
u	u	k7c2	u
monitorů	monitor	k1gInPc2	monitor
<g/>
,	,	kIx,	,
vyjímání	vyjímání	k1gNnSc4	vyjímání
nákladů	náklad	k1gInPc2	náklad
z	z	k7c2	z
kufru	kufr	k1gInSc2	kufr
vozu	vůz	k1gInSc2	vůz
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
kloubním	kloubní	k2eAgFnPc3d1	kloubní
blokádám	blokáda	k1gFnPc3	blokáda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
bolestivým	bolestivý	k2eAgNnSc7d1	bolestivé
omezením	omezení	k1gNnSc7	omezení
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
stres	stres	k1gInSc4	stres
<g/>
,	,	kIx,	,
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
výživy	výživa	k1gFnSc2	výživa
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
blokují	blokovat	k5eAaImIp3nP	blokovat
krční	krční	k2eAgFnSc1d1	krční
a	a	k8xC	a
bederní	bederní	k2eAgFnSc1d1	bederní
páteř	páteř	k1gFnSc1	páteř
<g/>
,	,	kIx,	,
žebra	žebro	k1gNnPc1	žebro
a	a	k8xC	a
sakro-iliakální	sakroliakálnit	k5eAaPmIp3nP	sakro-iliakálnit
skloubení	skloubení	k1gNnSc3	skloubení
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
strukturálních	strukturální	k2eAgFnPc6d1	strukturální
příčinách	příčina	k1gFnPc6	příčina
blokád	blokáda	k1gFnPc2	blokáda
bylo	být	k5eAaImAgNnS	být
vysloveno	vyslovit	k5eAaPmNgNnS	vyslovit
více	hodně	k6eAd2	hodně
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
nejpřesvědčivěji	přesvědčivě	k6eAd3	přesvědčivě
působí	působit	k5eAaImIp3nS	působit
teorie	teorie	k1gFnSc1	teorie
mechanického	mechanický	k2eAgNnSc2d1	mechanické
slepení	slepení	k1gNnSc2	slepení
chrupavčitých	chrupavčitý	k2eAgFnPc2d1	chrupavčitá
ploch	plocha	k1gFnPc2	plocha
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
metodiku	metodika	k1gFnSc4	metodika
odstranění	odstranění	k1gNnSc3	odstranění
kloubních	kloubní	k2eAgFnPc2d1	kloubní
blokád	blokáda	k1gFnPc2	blokáda
<g/>
,	,	kIx,	,
cvičení	cvičení	k1gNnSc4	cvičení
a	a	k8xC	a
doporučení	doporučení	k1gNnSc4	doporučení
se	se	k3xPyFc4	se
zasadila	zasadit	k5eAaPmAgFnS	zasadit
tzv.	tzv.	kA	tzv.
Pražská	pražský	k2eAgFnSc1d1	Pražská
myoskeletální	myoskeletální	k2eAgFnSc1d1	myoskeletální
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
kloubů	kloub	k1gInPc2	kloub
==	==	k?	==
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
kloubů	kloub	k1gInPc2	kloub
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
</s>
</p>
<p>
<s>
Jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
klouby	kloub	k1gInPc1	kloub
–	–	k?	–
spojují	spojovat	k5eAaImIp3nP	spojovat
dvě	dva	k4xCgFnPc4	dva
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
ramenní	ramenní	k2eAgInSc4d1	ramenní
kloub	kloub	k1gInSc4	kloub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složené	složený	k2eAgInPc1d1	složený
klouby	kloub	k1gInPc1	kloub
–	–	k?	–
spojují	spojovat	k5eAaImIp3nP	spojovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
vsunuty	vsunut	k2eAgFnPc1d1	vsunuta
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
chrupavčité	chrupavčitý	k2eAgFnPc1d1	chrupavčitá
destičky	destička	k1gFnPc1	destička
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
klouby	kloub	k1gInPc1	kloub
zápěstí	zápěstí	k1gNnSc2	zápěstí
nebo	nebo	k8xC	nebo
kolenní	kolenní	k2eAgInSc4d1	kolenní
kloub	kloub	k1gInSc4	kloub
<g/>
.	.	kIx.	.
<g/>
Druhy	druh	k1gInPc1	druh
kloubů	kloub	k1gInPc2	kloub
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
kloubních	kloubní	k2eAgFnPc2d1	kloubní
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
Kulový	kulový	k2eAgInSc1d1	kulový
–	–	k?	–
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
možný	možný	k2eAgInSc1d1	možný
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
kloub	kloub	k1gInSc1	kloub
ramenní	ramenní	k2eAgInSc1d1	ramenní
nebo	nebo	k8xC	nebo
kyčelní	kyčelní	k2eAgInSc1d1	kyčelní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válcový	válcový	k2eAgInSc1d1	válcový
–	–	k?	–
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
jen	jen	k9	jen
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
ohybu	ohyb	k1gInSc2	ohyb
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
klouby	kloub	k1gInPc1	kloub
článků	článek	k1gInPc2	článek
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kladkový	kladkový	k2eAgInSc1d1	kladkový
–	–	k?	–
na	na	k7c6	na
hlavici	hlavice	k1gFnSc6	hlavice
jedné	jeden	k4xCgFnSc6	jeden
kosti	kost	k1gFnSc6	kost
je	být	k5eAaImIp3nS	být
rýha	rýha	k1gFnSc1	rýha
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavici	hlavice	k1gFnSc6	hlavice
druhé	druhý	k4xOgFnSc2	druhý
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
připojuje	připojovat	k5eAaImIp3nS	připojovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hrana	hrana	k1gFnSc1	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc1	spojení
kosti	kost	k1gFnSc2	kost
pažní	pažní	k2eAgFnSc2d1	pažní
a	a	k8xC	a
loketní	loketní	k2eAgFnSc2d1	loketní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elipsoidní	elipsoidní	k2eAgFnSc1d1	elipsoidní
–	–	k?	–
hlavice	hlavice	k1gFnSc1	hlavice
jedné	jeden	k4xCgFnSc2	jeden
kosti	kost	k1gFnSc2	kost
má	mít	k5eAaImIp3nS	mít
vejčitý	vejčitý	k2eAgInSc4d1	vejčitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
kosti	kost	k1gFnSc6	kost
je	být	k5eAaImIp3nS	být
eliptická	eliptický	k2eAgFnSc1d1	eliptická
dutina	dutina	k1gFnSc1	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgNnSc2	tento
spojení	spojení	k1gNnSc2	spojení
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc4	spojení
kosti	kost	k1gFnSc2	kost
vřetenní	vřetenní	k2eAgFnSc2d1	vřetenní
a	a	k8xC	a
kosti	kost	k1gFnSc2	kost
loďkovité	loďkovitý	k2eAgFnSc2d1	loďkovitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedlový	sedlový	k2eAgInSc1d1	sedlový
–	–	k?	–
spojované	spojovaný	k2eAgFnPc1d1	spojovaná
kosti	kost	k1gFnPc1	kost
mají	mít	k5eAaImIp3nP	mít
duté	dutý	k2eAgFnPc1d1	dutá
i	i	k8xC	i
vypouklé	vypouklý	k2eAgFnPc1d1	vypouklá
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
záprstní	záprstní	k2eAgFnPc4d1	záprstní
kosti	kost	k1gFnPc4	kost
palce	palec	k1gInSc2	palec
a	a	k8xC	a
karpální	karpální	k2eAgFnSc2d1	karpální
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čepový	čepový	k2eAgInSc1d1	čepový
kloub	kloub	k1gInSc1	kloub
–	–	k?	–
výběžek	výběžek	k1gInSc1	výběžek
jedné	jeden	k4xCgFnSc2	jeden
kosti	kost	k1gFnSc2	kost
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
v	v	k7c6	v
kruhovém	kruhový	k2eAgInSc6d1	kruhový
otvoru	otvor	k1gInSc6	otvor
jiné	jiný	k2eAgFnSc2d1	jiná
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgNnSc2	tento
spojení	spojení	k1gNnSc2	spojení
je	být	k5eAaImIp3nS	být
spojení	spojení	k1gNnSc1	spojení
nosiče	nosič	k1gInSc2	nosič
a	a	k8xC	a
čepovce	čepovec	k1gInSc2	čepovec
u	u	k7c2	u
krční	krční	k2eAgFnSc2d1	krční
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuhé	tuhý	k2eAgInPc1d1	tuhý
klouby	kloub	k1gInPc1	kloub
–	–	k?	–
ploché	plochý	k2eAgInPc1d1	plochý
<g/>
,	,	kIx,	,
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
klouby	kloub	k1gInPc1	kloub
spojující	spojující	k2eAgInPc1d1	spojující
obratle	obratel	k1gInPc1	obratel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Syndesmóza	syndesmóza	k1gFnSc1	syndesmóza
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
skloubení	skloubení	k1gNnSc6	skloubení
dvou	dva	k4xCgFnPc2	dva
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
velkým	velký	k2eAgInPc3d1	velký
kloubním	kloubní	k2eAgInPc3d1	kloubní
rozsahům	rozsah	k1gInPc3	rozsah
<g/>
,	,	kIx,	,
jen	jen	k9	jen
k	k	k7c3	k
'	'	kIx"	'
<g/>
nutacím	nutace	k1gFnPc3	nutace
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
k	k	k7c3	k
pružné	pružný	k2eAgFnSc3d1	pružná
viklavosti	viklavost	k1gFnSc3	viklavost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
sakro-iliakální	sakroliakální	k2eAgNnSc4d1	sakro-iliakální
skloubení	skloubení	k1gNnSc4	skloubení
<g/>
,	,	kIx,	,
kloub	kloub	k1gInSc4	kloub
sterno-klavikulární	sternolavikulární	k2eAgInSc4d1	sterno-klavikulární
<g/>
,	,	kIx,	,
akromioklavikulární	akromioklavikulární	k2eAgInSc4d1	akromioklavikulární
<g/>
,	,	kIx,	,
či	či	k8xC	či
tibio-fibulární	tibioibulární	k2eAgInSc1d1	tibio-fibulární
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klouby	kloub	k1gInPc7	kloub
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Kolenní	kolenní	k2eAgInSc1d1	kolenní
kloub	kloub	k1gInSc1	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k1gMnSc1	articulatio
genus	genus	k1gMnSc1	genus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Loketní	loketní	k2eAgInSc4d1	loketní
kloub	kloub	k1gInSc4	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k6eAd1	articulatio
cubiti	cubit	k5eAaImF	cubit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ramenní	ramenní	k2eAgInSc4d1	ramenní
kloub	kloub	k1gInSc4	kloub
(	(	kIx(	(
<g/>
articulatio	articulatia	k1gMnSc5	articulatia
humeri	humer	k1gMnSc5	humer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kyčelní	kyčelní	k2eAgInSc4d1	kyčelní
kloub	kloub	k1gInSc4	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k6eAd1	articulatio
coxae	coxaat	k5eAaPmIp3nS	coxaat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlezenní	hlezenní	k2eAgInSc1d1	hlezenní
kloub	kloub	k1gInSc1	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k1gNnSc1	articulatio
talocruralis	talocruralis	k1gFnSc2	talocruralis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zápěstní	zápěstní	k2eAgInSc1d1	zápěstní
kloub	kloub	k1gInSc1	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k1gNnSc1	articulatio
radiocarpalis	radiocarpalis	k1gFnSc2	radiocarpalis
<g/>
)	)	kIx)	)
<g/>
Menší	malý	k2eAgInPc1d2	menší
klouby	kloub	k1gInPc1	kloub
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
články	článek	k1gInPc7	článek
prstů	prst	k1gInPc2	prst
dolních	dolní	k2eAgFnPc2d1	dolní
a	a	k8xC	a
horních	horní	k2eAgFnPc2d1	horní
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vykloubení	vykloubení	k1gNnSc1	vykloubení
</s>
</p>
