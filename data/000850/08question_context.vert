<s>
Johannes	Johannes	k1gMnSc1	Johannes
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Davos	Davos	k1gMnSc1	Davos
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jader	jádro	k1gNnPc2	jádro
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
izoloval	izolovat	k5eAaBmAgInS	izolovat
směs	směs	k1gFnSc4	směs
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
fosfát	fosfát	k1gInSc4	fosfát
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
nuklein	nuklein	k2eAgInSc1d1	nuklein
a	a	k8xC	a
skrývaly	skrývat	k5eAaImAgInP	skrývat
se	se	k3xPyFc4	se
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
nukleové	nukleový	k2eAgFnSc2d1	nukleová
kyseliny	kyselina	k1gFnSc2	kyselina
DNA	DNA	kA	DNA
a	a	k8xC	a
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>

