<s>
Johannes	Johannes	k1gMnSc1	Johannes
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Davos	Davos	k1gMnSc1	Davos
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jader	jádro	k1gNnPc2	jádro
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
izoloval	izolovat	k5eAaBmAgInS	izolovat
směs	směs	k1gFnSc4	směs
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
fosfát	fosfát	k1gInSc4	fosfát
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
nuklein	nuklein	k2eAgInSc1d1	nuklein
a	a	k8xC	a
skrývaly	skrývat	k5eAaImAgInP	skrývat
se	se	k3xPyFc4	se
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
nukleové	nukleový	k2eAgFnSc2d1	nukleová
kyseliny	kyselina	k1gFnSc2	kyselina
DNA	DNA	kA	DNA
a	a	k8xC	a
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
objev	objev	k1gInSc1	objev
učinil	učinit	k5eAaPmAgInS	učinit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Tübingenské	Tübingenský	k2eAgFnSc2d1	Tübingenská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
Felix	Felix	k1gMnSc1	Felix
Hoppe-Seylera	Hoppe-Seyler	k1gMnSc4	Hoppe-Seyler
<g/>
.	.	kIx.	.
</s>
<s>
Miescherovi	Miescher	k1gMnSc3	Miescher
nebyl	být	k5eNaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
význam	význam	k1gInSc1	význam
DNA	DNA	kA	DNA
ani	ani	k8xC	ani
její	její	k3xOp3gFnSc1	její
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jednu	jeden	k4xCgFnSc4	jeden
chvíli	chvíle	k1gFnSc4	chvíle
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
dědičnosti	dědičnost	k1gFnSc6	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
byli	být	k5eAaImAgMnP	být
profesoři	profesor	k1gMnPc1	profesor
anatomie	anatomie	k1gFnSc2	anatomie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
chlapec	chlapec	k1gMnSc1	chlapec
byl	být	k5eAaImAgMnS	být
plachý	plachý	k2eAgMnSc1d1	plachý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgMnS	trpět
poruchou	porucha	k1gFnSc7	porucha
sluchu	sluch	k1gInSc2	sluch
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tyfu	tyf	k1gInSc2	tyf
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nezastavilo	zastavit	k5eNaPmAgNnS	zastavit
jeho	jeho	k3xOp3gInSc4	jeho
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
sám	sám	k3xTgInSc1	sám
studoval	studovat	k5eAaImAgMnS	studovat
medicínu	medicína	k1gFnSc4	medicína
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
Friedrich	Friedrich	k1gMnSc1	Friedrich
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
organického	organický	k2eAgMnSc4d1	organický
chemika	chemik	k1gMnSc4	chemik
Adolfa	Adolf	k1gMnSc2	Adolf
Steckera	Stecker	k1gMnSc2	Stecker
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc2	jeho
studia	studio	k1gNnSc2	studio
byla	být	k5eAaImAgFnS	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
z	z	k7c2	z
tyfu	tyf	k1gInSc2	tyf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
získal	získat	k5eAaPmAgMnS	získat
M.	M.	kA	M.
<g/>
D.	D.	kA	D.
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
částečná	částečný	k2eAgFnSc1d1	částečná
hluchota	hluchota	k1gFnSc1	hluchota
bude	být	k5eAaImBp3nS	být
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
profesi	profes	k1gFnSc6	profes
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
fyziologickou	fyziologický	k2eAgFnSc4d1	fyziologická
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
studovat	studovat	k5eAaImF	studovat
lymfocyty	lymfocyt	k1gInPc4	lymfocyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
povzbuzen	povzbudit	k5eAaPmNgInS	povzbudit
Felixem	Felix	k1gMnSc7	Felix
Hoppe-Seylerem	Hoppe-Seyler	k1gMnSc7	Hoppe-Seyler
(	(	kIx(	(
<g/>
německý	německý	k2eAgMnSc1d1	německý
fyziolog	fyziolog	k1gMnSc1	fyziolog
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
oboru	obor	k1gInSc2	obor
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
)	)	kIx)	)
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
leukocytů	leukocyt	k1gInPc2	leukocyt
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
měl	mít	k5eAaImAgMnS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
chemie	chemie	k1gFnSc2	chemie
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Lymfocyty	lymfocyt	k1gInPc1	lymfocyt
byly	být	k5eAaImAgInP	být
obtížné	obtížný	k2eAgInPc1d1	obtížný
získatelné	získatelný	k2eAgInPc1d1	získatelný
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
množství	množství	k1gNnSc6	množství
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
o	o	k7c6	o
dalších	další	k2eAgInPc6d1	další
leukocytech	leukocyt	k1gInPc6	leukocyt
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
hnisu	hnis	k1gInSc6	hnis
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
získány	získat	k5eAaPmNgFnP	získat
z	z	k7c2	z
nemocničních	nemocniční	k2eAgFnPc2d1	nemocniční
bandáží	bandáž	k1gFnPc2	bandáž
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
problémové	problémový	k2eAgNnSc1d1	problémové
bylo	být	k5eAaImAgNnS	být
vymytí	vymytí	k1gNnSc1	vymytí
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
obvazů	obvaz	k1gInPc2	obvaz
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gNnSc2	jejich
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
nad	nad	k7c7	nad
různými	různý	k2eAgInPc7d1	různý
roztoky	roztok	k1gInPc7	roztok
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
jeden	jeden	k4xCgInSc4	jeden
vzorek	vzorek	k1gInSc4	vzorek
smícháním	smíchání	k1gNnSc7	smíchání
leukocytů	leukocyt	k1gInPc2	leukocyt
se	s	k7c7	s
síranem	síran	k1gInSc7	síran
sodným	sodný	k2eAgInSc7d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc4	buňka
následně	následně	k6eAd1	následně
přefiltroval	přefiltrovat	k5eAaPmAgMnS	přefiltrovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstředivky	odstředivka	k1gFnPc1	odstředivka
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nepoužívaly	používat	k5eNaImAgInP	používat
<g/>
,	,	kIx,	,
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
maximálně	maximálně	k6eAd1	maximálně
nechat	nechat	k5eAaPmF	nechat
usadit	usadit	k5eAaPmF	usadit
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
kádinky	kádinka	k1gFnSc2	kádinka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Miescher	Mieschra	k1gFnPc2	Mieschra
pokusil	pokusit	k5eAaPmAgMnS	pokusit
izolovat	izolovat	k5eAaBmF	izolovat
čistá	čistý	k2eAgNnPc1d1	čisté
jádra	jádro	k1gNnPc1	jádro
bez	bez	k7c2	bez
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
Podrobil	podrobit	k5eAaPmAgInS	podrobit
vyčištěná	vyčištěný	k2eAgNnPc4d1	vyčištěné
jádra	jádro	k1gNnPc4	jádro
alkalické	alkalický	k2eAgFnSc2d1	alkalická
extrakci	extrakce	k1gFnSc4	extrakce
následované	následovaný	k2eAgNnSc4d1	následované
acidifikací	acidifikace	k1gFnSc7	acidifikace
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc4	vznik
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Miescher	Mieschra	k1gFnPc2	Mieschra
nazval	nazvat	k5eAaPmAgInS	nazvat
"	"	kIx"	"
<g/>
nuclein	nuclein	k1gInSc1	nuclein
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
chromatin	chromatin	k1gInSc1	chromatin
či	či	k8xC	či
vlastně	vlastně	k9	vlastně
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
nuklein	nuklein	k1gInSc1	nuklein
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
fosfor	fosfor	k1gInSc4	fosfor
a	a	k8xC	a
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
síru	síra	k1gFnSc4	síra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sraženina	sraženina	k1gFnSc1	sraženina
byla	být	k5eAaImAgFnS	být
nebílkovinné	bílkovinný	k2eNgFnPc4d1	nebílkovinná
povahy	povaha	k1gFnPc4	povaha
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgInS	dokázat
aplikací	aplikace	k1gFnSc7	aplikace
proteolytického	proteolytický	k2eAgInSc2d1	proteolytický
enzymu	enzym	k1gInSc2	enzym
pepsinu	pepsin	k1gInSc2	pepsin
-	-	kIx~	-
sraženina	sraženina	k1gFnSc1	sraženina
se	se	k3xPyFc4	se
nerozložila	rozložit	k5eNaPmAgFnS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
pokusů	pokus	k1gInPc2	pokus
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známých	známá	k1gFnPc2	známá
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hoppe-Seyler	Hoppe-Seyler	k1gMnSc1	Hoppe-Seyler
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
celý	celý	k2eAgInSc4d1	celý
Miescherův	Miescherův	k2eAgInSc4d1	Miescherův
výzkum	výzkum	k1gInSc4	výzkum
sám	sám	k3xTgMnSc1	sám
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
vlastním	vlastní	k2eAgNnSc7d1	vlastní
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
časopise	časopis	k1gInSc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
pak	pak	k6eAd1	pak
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
fyziologie	fyziologie	k1gFnSc2	fyziologie
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
Carla	Carl	k1gMnSc2	Carl
Ludwiga	Ludwig	k1gMnSc2	Ludwig
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
univerzitě	univerzita	k1gFnSc6	univerzita
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
fyziologie	fyziologie	k1gFnSc2	fyziologie
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
věnovali	věnovat	k5eAaImAgMnP	věnovat
výzkumu	výzkum	k1gInSc2	výzkum
chemie	chemie	k1gFnSc1	chemie
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
funkce	funkce	k1gFnSc1	funkce
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
neznámá	známý	k2eNgFnSc1d1	neznámá
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Averyho-MacLeodův-McCartyho	Averyho-MacLeodův-McCartyha	k1gMnSc5	Averyho-MacLeodův-McCartyha
experiment	experiment	k1gInSc1	experiment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nositelem	nositel	k1gMnSc7	nositel
dědičnosti	dědičnost	k1gFnSc2	dědičnost
jsou	být	k5eAaImIp3nP	být
proteiny	protein	k1gInPc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
hrál	hrát	k5eAaImAgInS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
určování	určování	k1gNnSc6	určování
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
jako	jako	k8xS	jako
nosiče	nosič	k1gInPc4	nosič
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
význam	význam	k1gInSc4	význam
Miescherova	Miescherův	k2eAgInSc2d1	Miescherův
objevu	objev	k1gInSc2	objev
až	až	k9	až
tak	tak	k6eAd1	tak
zjevný	zjevný	k2eAgMnSc1d1	zjevný
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
Albrecht	Albrecht	k1gMnSc1	Albrecht
Kossel	Kossel	k1gMnSc1	Kossel
(	(	kIx(	(
<g/>
německý	německý	k2eAgMnSc1d1	německý
fyziolog	fyziolog	k1gMnSc1	fyziolog
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
fyziologickou	fyziologický	k2eAgFnSc4d1	fyziologická
chemii	chemie	k1gFnSc4	chemie
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
neprovedl	provést	k5eNaPmAgInS	provést
detailnější	detailní	k2eAgInSc1d2	detailnější
výzkum	výzkum	k1gInSc1	výzkum
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
nukleinu	nuklein	k1gInSc2	nuklein
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
je	být	k5eAaImIp3nS	být
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
pro	pro	k7c4	pro
prokázání	prokázání	k1gNnSc4	prokázání
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncentraci	koncentrace	k1gFnSc3	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
reguluje	regulovat	k5eAaImIp3nS	regulovat
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Miescher	Mieschra	k1gFnPc2	Mieschra
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
51	[number]	k4	51
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
