<p>
<s>
Sýkořice	sýkořice	k1gFnSc1	sýkořice
vousatá	vousatý	k2eAgFnSc1d1	vousatá
(	(	kIx(	(
<g/>
Panurus	Panurus	k1gMnSc1	Panurus
biarmicus	biarmicus	k1gMnSc1	biarmicus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
druh	druh	k1gInSc1	druh
pěvce	pěvec	k1gMnSc2	pěvec
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
rodu	rod	k1gInSc2	rod
Panurus	Panurus	k1gMnSc1	Panurus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
rodem	rod	k1gInSc7	rod
čeledi	čeleď	k1gFnSc2	čeleď
sýkořicovití	sýkořicovitý	k2eAgMnPc1d1	sýkořicovitý
(	(	kIx(	(
<g/>
Panuridae	Panuridae	k1gNnSc7	Panuridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
2	[number]	k4	2
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
rozšíření	rozšíření	k1gNnSc1	rozšíření
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
velmi	velmi	k6eAd1	velmi
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc4d1	severní
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Středozemí	středozemí	k1gNnSc4	středozemí
<g/>
,	,	kIx,	,
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnSc4d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgNnSc4d1	jižní
Turecko	Turecko	k1gNnSc4	Turecko
až	až	k9	až
po	po	k7c4	po
jihozápadní	jihozápadní	k2eAgNnSc4d1	jihozápadní
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
Ázerbájdžán	Ázerbájdžán	k1gInSc4	Ázerbájdžán
obývá	obývat	k5eAaImIp3nS	obývat
sýkořice	sýkořice	k1gFnSc1	sýkořice
vousatá	vousatý	k2eAgFnSc1d1	vousatá
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
b.	b.	k?	b.
biarmicus	biarmicus	k1gInSc1	biarmicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litvu	Litva	k1gFnSc4	Litva
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
po	po	k7c6	po
ČR	ČR	kA	ČR
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
po	po	k7c4	po
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
Bosnu	Bosna	k1gFnSc4	Bosna
a	a	k8xC	a
Hercegovinu	Hercegovina	k1gFnSc4	Hercegovina
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc4d1	střední
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
Asii	Asie	k1gFnSc4	Asie
zase	zase	k9	zase
sýkořice	sýkořice	k1gFnSc2	sýkořice
vousatá	vousatý	k2eAgFnSc1d1	vousatá
eurasijská	eurasijský	k2eAgFnSc1d1	eurasijská
(	(	kIx(	(
<g/>
P.	P.	kA	P.
b.	b.	k?	b.
russicus	russicus	k1gInSc1	russicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
vrabce	vrabec	k1gMnSc2	vrabec
domácího	domácí	k1gMnSc2	domácí
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
15,5	[number]	k4	15,5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
světle	světle	k6eAd1	světle
žlutohnědým	žlutohnědý	k2eAgInSc7d1	žlutohnědý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
modrošedou	modrošedý	k2eAgFnSc4d1	modrošedá
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
černý	černý	k2eAgInSc1d1	černý
vous	vous	k1gInSc1	vous
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgNnSc1d1	bílé
hrdlo	hrdlo	k1gNnSc1	hrdlo
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
spodní	spodní	k2eAgFnPc1d1	spodní
ocasní	ocasní	k2eAgFnPc1d1	ocasní
krovky	krovka	k1gFnPc1	krovka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
hlavu	hlava	k1gFnSc4	hlava
běžovohnědou	běžovohnědý	k2eAgFnSc4d1	běžovohnědý
bez	bez	k7c2	bez
černého	černý	k2eAgInSc2d1	černý
vousu	vous	k1gInSc2	vous
<g/>
,	,	kIx,	,
hrdlo	hrdlo	k1gNnSc1	hrdlo
špinavě	špinavě	k6eAd1	špinavě
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
spodní	spodní	k2eAgFnSc2d1	spodní
ocasní	ocasní	k2eAgFnSc2d1	ocasní
krovky	krovka	k1gFnSc2	krovka
béžové	béžový	k2eAgFnSc2d1	béžová
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
samici	samice	k1gFnSc3	samice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
žlutobéžové	žlutobéžový	k2eAgNnSc1d1	žlutobéžový
<g/>
,	,	kIx,	,
s	s	k7c7	s
černým	černý	k2eAgMnSc7d1	černý
hřbetem	hřbet	k1gMnSc7	hřbet
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
černým	černý	k2eAgInSc7d1	černý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Létá	létat	k5eAaImIp3nS	létat
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgFnPc6d1	nepravidelná
<g/>
,	,	kIx,	,
mělkých	mělký	k2eAgFnPc6d1	mělká
vlnovkách	vlnovka	k1gFnPc6	vlnovka
s	s	k7c7	s
vířivým	vířivý	k2eAgNnSc7d1	vířivé
máváním	mávání	k1gNnSc7	mávání
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlas	hlas	k1gInSc1	hlas
==	==	k?	==
</s>
</p>
<p>
<s>
Vábení	vábení	k1gNnSc1	vábení
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
ping	pingo	k1gNnPc2	pingo
ping	pingo	k1gNnPc2	pingo
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
džong	džong	k1gMnSc1	džong
džong	džong	k1gMnSc1	džong
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
tiché	tichý	k2eAgNnSc4d1	tiché
libozvučné	libozvučný	k2eAgNnSc4d1	libozvučné
cvrlikání	cvrlikání	k1gNnSc4	cvrlikání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
po	po	k7c6	po
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Asie	Asie	k1gFnSc2	Asie
po	po	k7c4	po
Čínu	Čína	k1gFnSc4	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
240	[number]	k4	240
000	[number]	k4	000
–	–	k?	–
480	[number]	k4	480
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Hnízdění	hnízdění	k1gNnSc1	hnízdění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
poprvé	poprvé	k6eAd1	poprvé
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
na	na	k7c6	na
Novoveském	Novoveský	k2eAgInSc6d1	Novoveský
rybníce	rybník	k1gInSc6	rybník
u	u	k7c2	u
Pohořelic	Pohořelice	k1gFnPc2	Pohořelice
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
hnízdními	hnízdní	k2eAgFnPc7d1	hnízdní
oblastmi	oblast	k1gFnPc7	oblast
jižní	jižní	k2eAgFnPc1d1	jižní
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
Polabí	Polabí	k1gNnPc1	Polabí
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc1	početnost
velmi	velmi	k6eAd1	velmi
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
na	na	k7c4	na
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tah	tah	k1gInSc1	tah
==	==	k?	==
</s>
</p>
<p>
<s>
Sýkořice	sýkořice	k1gFnSc1	sýkořice
vousatá	vousatý	k2eAgFnSc1d1	vousatá
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
stálý	stálý	k2eAgInSc1d1	stálý
až	až	k8xS	až
přelétavý	přelétavý	k2eAgMnSc1d1	přelétavý
pták	pták	k1gMnSc1	pták
bez	bez	k7c2	bez
výrazného	výrazný	k2eAgInSc2d1	výrazný
tahu	tah	k1gInSc2	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Sociálně	sociálně	k6eAd1	sociálně
monogamní	monogamní	k2eAgInSc4d1	monogamní
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
věrné	věrný	k2eAgFnPc4d1	věrná
i	i	k9	i
několik	několik	k4yIc4	několik
sezón	sezóna	k1gFnPc2	sezóna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
mimopárové	mimopárový	k2eAgFnPc1d1	mimopárový
kopulace	kopulace	k1gFnPc1	kopulace
a	a	k8xC	a
vnitrodruhový	vnitrodruhový	k2eAgInSc1d1	vnitrodruhový
hnízdní	hnízdní	k2eAgInSc1d1	hnízdní
parazitismus	parazitismus	k1gInSc1	parazitismus
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
preferují	preferovat	k5eAaImIp3nP	preferovat
samce	samec	k1gInPc1	samec
s	s	k7c7	s
delším	dlouhý	k2eAgInSc7d2	delší
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
shora	shora	k6eAd1	shora
vždy	vždy	k6eAd1	vždy
přirozeně	přirozeně	k6eAd1	přirozeně
krytá	krytý	k2eAgFnSc1d1	krytá
miskovitá	miskovitý	k2eAgFnSc1d1	miskovitá
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
rákosů	rákos	k1gInPc2	rákos
nebo	nebo	k8xC	nebo
orobinců	orobinec	k1gInPc2	orobinec
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c4	v
rákosí	rákosí	k1gNnSc4	rákosí
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Staví	stavit	k5eAaPmIp3nS	stavit
jej	on	k3xPp3gMnSc4	on
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
2	[number]	k4	2
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
bílých	bílý	k2eAgFnPc2d1	bílá
<g/>
,	,	kIx,	,
řídce	řídce	k6eAd1	řídce
černě	černě	k6eAd1	černě
tečkovaných	tečkovaný	k2eAgNnPc2d1	tečkované
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
18,0	[number]	k4	18,0
x	x	k?	x
14,1	[number]	k4	14,1
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
asi	asi	k9	asi
12	[number]	k4	12
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
poté	poté	k6eAd1	poté
krmí	krmit	k5eAaImIp3nP	krmit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
společně	společně	k6eAd1	společně
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
páry	pár	k1gInPc7	pár
<g/>
,	,	kIx,	,
asi	asi	k9	asi
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
párů	pár	k1gInPc2	pár
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
ptáků	pták	k1gMnPc2	pták
dochází	docházet	k5eAaImIp3nS	docházet
již	již	k9	již
v	v	k7c6	v
zimních	zimní	k2eAgNnPc6d1	zimní
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc7	pavouk
a	a	k8xC	a
hlemýždi	hlemýžď	k1gMnPc7	hlemýžď
<g/>
,	,	kIx,	,
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
semeny	semeno	k1gNnPc7	semeno
rákosu	rákos	k1gInSc2	rákos
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sýkořice	sýkořice	k1gFnSc1	sýkořice
vousatá	vousatý	k2eAgFnSc1d1	vousatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
sýkořice	sýkořice	k1gFnSc2	sýkořice
vousatá	vousatý	k2eAgFnSc1d1	vousatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Panurus	Panurus	k1gInSc1	Panurus
biarmicus	biarmicus	k1gInSc1	biarmicus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
</s>
</p>
