<s>
Sýkořice	sýkořice	k1gFnSc1
vousatá	vousatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Panurus	Panurus	k1gMnSc1
biarmicus	biarmicus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
druh	druh	k1gInSc1
pěvce	pěvec	k1gMnSc2
a	a	k8xC
jediný	jediný	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
rodu	rod	k1gInSc2
Panurus	Panurus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
rodem	rod	k1gInSc7
čeledi	čeleď	k1gFnSc2
sýkořicovití	sýkořicovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Panuridae	Panuridae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>