<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Francis	Francis	k1gFnSc2	Francis
"	"	kIx"	"
<g/>
Buddy	Budda	k1gFnSc2	Budda
<g/>
"	"	kIx"	"
Davis	Davis	k1gInSc1	Davis
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Beaumont	Beaumont	k1gInSc1	Beaumont
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
atlet	atlet	k1gMnSc1	atlet
a	a	k8xC	a
basketbalista	basketbalista	k1gMnSc1	basketbalista
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
devět	devět	k4xCc4	devět
let	let	k1gInSc4	let
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
s	s	k7c7	s
dětskou	dětský	k2eAgFnSc7d1	dětská
obrnou	obrna	k1gFnSc7	obrna
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
druhého	druhý	k4xOgInSc2	druhý
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
světového	světový	k2eAgInSc2d1	světový
výkonu	výkon	k1gInSc2	výkon
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
205	[number]	k4	205
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
USA	USA	kA	USA
výkonem	výkon	k1gInSc7	výkon
209	[number]	k4	209
cm	cm	kA	cm
a	a	k8xC	a
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
olympijském	olympijský	k2eAgInSc6d1	olympijský
rekordu	rekord	k1gInSc6	rekord
204	[number]	k4	204
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
vylepšil	vylepšit	k5eAaPmAgInS	vylepšit
světový	světový	k2eAgInSc1d1	světový
rekord	rekord	k1gInSc1	rekord
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
na	na	k7c4	na
213	[number]	k4	213
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
ukončil	ukončit	k5eAaPmAgMnS	ukončit
atletickou	atletický	k2eAgFnSc4d1	atletická
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
basketbalu	basketbal	k1gInSc2	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
nejdříve	dříve	k6eAd3	dříve
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Warriors	Warriors	k1gInSc1	Warriors
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
Hawks	Hawks	k1gInSc1	Hawks
a	a	k8xC	a
stímto	stímto	k1gNnSc4	stímto
týmem	tým	k1gInSc7	tým
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
NBA	NBA	kA	NBA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
www.sports-reference.com	www.sportseference.com	k1gInSc4	www.sports-reference.com
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
basketball-reference	basketballeferenka	k1gFnSc6	basketball-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
