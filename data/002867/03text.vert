<s>
Generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
i.	i.	k?	i.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Heliodor	Heliodor	k1gMnSc1	Heliodor
Prokop	Prokop	k1gMnSc1	Prokop
Píka	píka	k1gFnSc1	píka
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
Štítina	Štítina	k1gFnSc1	Štítina
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
představitel	představitel	k1gMnSc1	představitel
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
československého	československý	k2eAgInSc2d1	československý
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
oběť	oběť	k1gFnSc1	oběť
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Heliodor	Heliodor	k1gInSc1	Heliodor
Píka	píka	k1gFnSc1	píka
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
v	v	k7c6	v
Štítině	Štítina	k1gFnSc6	Štítina
u	u	k7c2	u
Opavy	Opava	k1gFnSc2	Opava
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
vesnického	vesnický	k2eAgMnSc2d1	vesnický
koláře	kolář	k1gMnSc2	kolář
Ignáce	Ignác	k1gMnSc2	Ignác
Píky	píka	k1gFnSc2	píka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
studovat	studovat	k5eAaImF	studovat
farmacii	farmacie	k1gFnSc4	farmacie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
odveden	odvést	k5eAaPmNgMnS	odvést
k	k	k7c3	k
zeměbraneckému	zeměbranecký	k2eAgInSc3d1	zeměbranecký
regimentu	regiment	k1gInSc3	regiment
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
na	na	k7c4	na
haličskou	haličský	k2eAgFnSc4d1	Haličská
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
15	[number]	k4	15
<g/>
.	.	kIx.	.
zeměbraneckého	zeměbranecký	k2eAgInSc2d1	zeměbranecký
pluku	pluk	k1gInSc2	pluk
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
ve	v	k7c6	v
Volyňské	volyňský	k2eAgFnSc6d1	Volyňská
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
Berestečku	Beresteček	k1gInSc6	Beresteček
zajat	zajmout	k5eAaPmNgInS	zajmout
ruskými	ruský	k2eAgFnPc7d1	ruská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
v	v	k7c6	v
Babrujsku	Babrujsek	k1gInSc6	Babrujsek
do	do	k7c2	do
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Československé	československý	k2eAgFnSc2d1	Československá
střelecké	střelecký	k2eAgFnSc2d1	střelecká
brigády	brigáda	k1gFnSc2	brigáda
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
záložního	záložní	k2eAgInSc2d1	záložní
praporu	prapor	k1gInSc2	prapor
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
21.7	[number]	k4	21.7
<g/>
.1917	.1917	k4	.1917
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
vojína	vojín	k1gMnSc2	vojín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
s	s	k7c7	s
částí	část	k1gFnSc7	část
legií	legie	k1gFnPc2	legie
přesunul	přesunout	k5eAaPmAgMnS	přesunout
lodí	loď	k1gFnSc7	loď
z	z	k7c2	z
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
frontu	fronta	k1gFnSc4	fronta
-	-	kIx~	-
přes	přes	k7c4	přes
Anglii	Anglie	k1gFnSc4	Anglie
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Havre	Havr	k1gInSc5	Havr
prošel	projít	k5eAaPmAgInS	projít
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
vojenským	vojenský	k2eAgInSc7d1	vojenský
výcvikem	výcvik	k1gInSc7	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
jako	jako	k8xS	jako
vojín	vojín	k1gMnSc1	vojín
k	k	k7c3	k
33	[number]	k4	33
<g/>
.	.	kIx.	.
střeleckému	střelecký	k2eAgInSc3d1	střelecký
pluku	pluk	k1gInSc3	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
lékárenské	lékárenský	k2eAgFnSc3d1	lékárenská
praxi	praxe	k1gFnSc3	praxe
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
ke	k	k7c3	k
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
službě	služba	k1gFnSc3	služba
u	u	k7c2	u
21	[number]	k4	21
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
francouzských	francouzský	k2eAgFnPc2d1	francouzská
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
nejprve	nejprve	k6eAd1	nejprve
četaře	četař	k1gMnSc4	četař
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
poručíka	poručík	k1gMnSc2	poručík
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
plynům	plyn	k1gInPc3	plyn
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
plukem	pluk	k1gInSc7	pluk
prodělal	prodělat	k5eAaPmAgMnS	prodělat
těžké	těžký	k2eAgInPc4d1	těžký
boje	boj	k1gInPc4	boj
na	na	k7c6	na
alsaské	alsaský	k2eAgFnSc6d1	alsaská
frontě	fronta	k1gFnSc6	fronta
(	(	kIx(	(
<g/>
jaro	jaro	k1gNnSc4	jaro
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Champagni	Champageň	k1gFnSc6	Champageň
<g/>
,	,	kIx,	,
na	na	k7c6	na
Aisně	Aisna	k1gFnSc6	Aisna
<g/>
,	,	kIx,	,
u	u	k7c2	u
Terronu	Terron	k1gInSc2	Terron
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
<g/>
;	;	kIx,	;
obdivován	obdivován	k2eAgInSc1d1	obdivován
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
obětavou	obětavý	k2eAgFnSc4d1	obětavá
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
liniích	linie	k1gFnPc6	linie
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
Československa	Československo	k1gNnSc2	Československo
jako	jako	k8xS	jako
poručík	poručík	k1gMnSc1	poručík
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
květnu	květno	k1gNnSc6	květno
nasazen	nasadit	k5eAaPmNgInS	nasadit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
polské	polský	k2eAgFnSc3d1	polská
armádě	armáda	k1gFnSc3	armáda
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1919	[number]	k4	1919
byl	být	k5eAaImAgMnS	být
převelen	převelet	k5eAaPmNgMnS	převelet
na	na	k7c4	na
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgMnS	být
brzy	brzy	k6eAd1	brzy
vyslán	vyslán	k2eAgMnSc1d1	vyslán
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
École	École	k1gFnSc2	École
spéciale	spéciale	k6eAd1	spéciale
militaire	militair	k1gInSc5	militair
v	v	k7c4	v
Saint-Cyr	Saint-Cyr	k1gInSc1	Saint-Cyr
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
Vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Hranicích	Hranice	k1gFnPc6	Hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Sehnalovou	Sehnalová	k1gFnSc7	Sehnalová
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
povolán	povolat	k5eAaPmNgMnS	povolat
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
štáb	štáb	k1gInSc4	štáb
Československé	československý	k2eAgFnSc2d1	Československá
branné	branný	k2eAgFnSc2d1	Branná
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
československých	československý	k2eAgMnPc2d1	československý
důstojníků	důstojník	k1gMnPc2	důstojník
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vojenským	vojenský	k2eAgMnSc7d1	vojenský
atašé	atašé	k1gMnSc7	atašé
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
post	post	k1gInSc1	post
byl	být	k5eAaImAgInS	být
strategicky	strategicky	k6eAd1	strategicky
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc1d1	významný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
bylo	být	k5eAaImAgNnS	být
členem	člen	k1gInSc7	člen
Malé	Malé	k2eAgFnSc2d1	Malé
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c2	za
oporu	opor	k1gInSc2	opor
proti	proti	k7c3	proti
rostoucímu	rostoucí	k2eAgNnSc3d1	rostoucí
německému	německý	k2eAgNnSc3d1	německé
a	a	k8xC	a
maďarskému	maďarský	k2eAgInSc3d1	maďarský
tlaku	tlak	k1gInSc3	tlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
působil	působit	k5eAaImAgMnS	působit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
na	na	k7c4	na
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
plukovník	plukovník	k1gMnSc1	plukovník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
se	s	k7c7	s
služebním	služební	k2eAgNnSc7d1	služební
pořadím	pořadí	k1gNnSc7	pořadí
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
dohody	dohoda	k1gFnPc4	dohoda
pro	pro	k7c4	pro
případnou	případný	k2eAgFnSc4d1	případná
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
příslib	příslib	k1gInSc4	příslib
materiální	materiální	k2eAgFnSc2d1	materiální
pomoci	pomoc	k1gFnSc2	pomoc
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgMnS	být
nucen	nucen	k2eAgMnSc1d1	nucen
utéci	utéct	k5eAaPmF	utéct
přes	přes	k7c4	přes
Francii	Francie	k1gFnSc4	Francie
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
Janu	Jan	k1gMnSc3	Jan
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
ho	on	k3xPp3gInSc4	on
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
Bukurešti	Bukurešť	k1gFnSc2	Bukurešť
jako	jako	k8xS	jako
vojenského	vojenský	k2eAgMnSc2d1	vojenský
vyslance	vyslanec	k1gMnSc2	vyslanec
pro	pro	k7c4	pro
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
československým	československý	k2eAgMnSc7d1	československý
a	a	k8xC	a
maďarským	maďarský	k2eAgMnPc3d1	maďarský
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
z	z	k7c2	z
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
především	především	k9	především
na	na	k7c4	na
demobilizované	demobilizovaný	k2eAgMnPc4d1	demobilizovaný
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
fašistickém	fašistický	k2eAgInSc6d1	fašistický
puči	puč	k1gInSc6	puč
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
krátkém	krátký	k2eAgNnSc6d1	krátké
zatčení	zatčení	k1gNnSc6	zatčení
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
se	se	k3xPyFc4	se
Píka	píka	k1gFnSc1	píka
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
podplukovníkem	podplukovník	k1gMnSc7	podplukovník
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Svobodou	Svoboda	k1gMnSc7	Svoboda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
předání	předání	k1gNnSc4	předání
žádosti	žádost	k1gFnSc2	žádost
E.	E.	kA	E.
Benešovi	Beneš	k1gMnSc3	Beneš
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
založení	založení	k1gNnSc1	založení
vojenského	vojenský	k2eAgNnSc2d1	vojenské
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Beneš	Beneš	k1gMnSc1	Beneš
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
Svobodou	Svoboda	k1gMnSc7	Svoboda
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
schůzce	schůzka	k1gFnSc6	schůzka
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
generálem	generál	k1gMnSc7	generál
Fokinem	Fokin	k1gMnSc7	Fokin
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vytvoření	vytvoření	k1gNnSc4	vytvoření
československých	československý	k2eAgFnPc2d1	Československá
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c6	na
území	území	k1gNnSc6	území
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Moskvou	Moskva	k1gFnSc7	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
Československá	československý	k2eAgFnSc1d1	Československá
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
přijala	přijmout	k5eAaPmAgFnS	přijmout
a	a	k8xC	a
po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
sovětsko-československé	sovětsko-československý	k2eAgFnSc2d1	sovětsko-československý
vojenské	vojenský	k2eAgFnSc2d1	vojenská
dohody	dohoda	k1gFnSc2	dohoda
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
se	s	k7c7	s
H.	H.	kA	H.
Píka	píka	k1gFnSc1	píka
stal	stát	k5eAaPmAgInS	stát
atašé	atašé	k1gMnSc7	atašé
a	a	k8xC	a
velitelem	velitel	k1gMnSc7	velitel
mise	mise	k1gFnSc2	mise
Československé	československý	k2eAgFnPc1d1	Československá
armády	armáda	k1gFnPc1	armáda
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
SSSR	SSSR	kA	SSSR
neusiluje	usilovat	k5eNaImIp3nS	usilovat
o	o	k7c4	o
svobodné	svobodný	k2eAgNnSc4d1	svobodné
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
diktaturu	diktatura	k1gFnSc4	diktatura
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
varování	varování	k1gNnSc1	varování
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
proti	proti	k7c3	proti
Píkovu	Píkovo	k1gNnSc3	Píkovo
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
protestovali	protestovat	k5eAaBmAgMnP	protestovat
zástupci	zástupce	k1gMnPc1	zástupce
KSČ	KSČ	kA	KSČ
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
začal	začít	k5eAaPmAgInS	začít
Píka	píka	k1gFnSc1	píka
v	v	k7c6	v
Buzuluku	Buzuluk	k1gInSc6	Buzuluk
formovat	formovat	k5eAaImF	formovat
jednotku	jednotka	k1gFnSc4	jednotka
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
z	z	k7c2	z
československých	československý	k2eAgMnPc2d1	československý
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dleli	dlít	k5eAaImAgMnP	dlít
v	v	k7c6	v
sovětských	sovětský	k2eAgInPc6d1	sovětský
zajateckých	zajatecký	k2eAgInPc6d1	zajatecký
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
L.	L.	kA	L.
Svobodou	svoboda	k1gFnSc7	svoboda
dokázal	dokázat	k5eAaPmAgMnS	dokázat
čelit	čelit	k5eAaImF	čelit
tlaku	tlak	k1gInSc2	tlak
K.	K.	kA	K.
Gottwalda	Gottwald	k1gMnSc2	Gottwald
na	na	k7c6	na
zpolitizování	zpolitizování	k1gNnSc6	zpolitizování
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1943	[number]	k4	1943
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
samostatná	samostatný	k2eAgFnSc1d1	samostatná
brigáda	brigáda	k1gFnSc1	brigáda
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Kyjeva	Kyjev	k1gInSc2	Kyjev
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
se	se	k3xPyFc4	se
Píka	píka	k1gFnSc1	píka
účastnil	účastnit	k5eAaImAgMnS	účastnit
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
podpisu	podpis	k1gInSc2	podpis
československo-sovětské	československoovětský	k2eAgFnSc2d1	československo-sovětská
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1943	[number]	k4	1943
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
brigádního	brigádní	k2eAgMnSc2d1	brigádní
generála	generál	k1gMnSc2	generál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1944	[number]	k4	1944
obsadil	obsadit	k5eAaPmAgInS	obsadit
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
Píka	píka	k1gFnSc1	píka
požádal	požádat	k5eAaPmAgMnS	požádat
sovětské	sovětský	k2eAgNnSc1d1	sovětské
velení	velení	k1gNnSc1	velení
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
slovenských	slovenský	k2eAgMnPc2d1	slovenský
povstalců	povstalec	k1gMnPc2	povstalec
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
J.	J.	kA	J.
V.	V.	kA	V.
Stalin	Stalin	k1gMnSc1	Stalin
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgMnS	vydat
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
dodávce	dodávka	k1gFnSc3	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
zahájení	zahájení	k1gNnSc4	zahájení
karpatské	karpatský	k2eAgFnSc2d1	Karpatská
operace	operace	k1gFnSc2	operace
maršálem	maršál	k1gMnSc7	maršál
Koněvem	Koněv	k1gInSc7	Koněv
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c4	na
československé	československý	k2eAgNnSc4d1	Československé
území	území	k1gNnSc4	území
Píka	píka	k1gFnSc1	píka
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
velitelem	velitel	k1gMnSc7	velitel
osvobozovacích	osvobozovací	k2eAgFnPc2d1	osvobozovací
jednotek	jednotka	k1gFnPc2	jednotka
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
L.	L.	kA	L.
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
žádost	žádost	k1gFnSc4	žádost
SSSR	SSSR	kA	SSSR
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
neúspěšně	úspěšně	k6eNd1	úspěšně
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
postupu	postup	k1gInSc3	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
Zakarpatské	zakarpatský	k2eAgFnSc6d1	Zakarpatská
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc1	období
již	již	k6eAd1	již
sovětské	sovětský	k2eAgNnSc1d1	sovětské
velení	velení	k1gNnSc1	velení
plně	plně	k6eAd1	plně
spolupracovalo	spolupracovat	k5eAaImAgNnS	spolupracovat
s	s	k7c7	s
K.	K.	kA	K.
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
ani	ani	k8xC	ani
E.	E.	kA	E.
Beneš	Beneš	k1gMnSc1	Beneš
mu	on	k3xPp3gMnSc3	on
neposkytl	poskytnout	k5eNaPmAgMnS	poskytnout
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
sovětské	sovětský	k2eAgNnSc1d1	sovětské
vedení	vedení	k1gNnSc1	vedení
H.	H.	kA	H.
Píku	píka	k1gFnSc4	píka
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
H.	H.	kA	H.
Píka	píka	k1gFnSc1	píka
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
divizního	divizní	k2eAgMnSc4d1	divizní
generála	generál	k1gMnSc4	generál
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
s	s	k7c7	s
pořadím	pořadí	k1gNnSc7	pořadí
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
a	a	k8xC	a
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
náměstkem	náměstek	k1gMnSc7	náměstek
náčelníka	náčelník	k1gMnSc2	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
i	i	k8xC	i
dvěma	dva	k4xCgNnPc7	dva
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vyznamenáními	vyznamenání	k1gNnPc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
československé	československý	k2eAgFnSc2d1	Československá
delegace	delegace	k1gFnSc2	delegace
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
dojednávání	dojednávání	k1gNnSc4	dojednávání
mírových	mírový	k2eAgFnPc2d1	mírová
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
evropskými	evropský	k2eAgInPc7d1	evropský
státy	stát	k1gInPc7	stát
Osy	osa	k1gFnSc2	osa
během	během	k7c2	během
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
odeslán	odeslat	k5eAaPmNgMnS	odeslat
na	na	k7c4	na
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zotavoval	zotavovat	k5eAaImAgMnS	zotavovat
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
žlučníku	žlučník	k1gInSc2	žlučník
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nepravdivě	pravdivě	k6eNd1	pravdivě
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
vlastizrádné	vlastizrádný	k2eAgFnSc2d1	vlastizrádná
a	a	k8xC	a
velezrádné	velezrádný	k2eAgFnSc2d1	velezrádná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyslýchán	vyslýchán	k2eAgInSc1d1	vyslýchán
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Domečku	domeček	k1gInSc2	domeček
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vykonstruovaném	vykonstruovaný	k2eAgInSc6d1	vykonstruovaný
soudním	soudní	k2eAgInSc6d1	soudní
procesu	proces	k1gInSc6	proces
Státním	státní	k2eAgInSc6d1	státní
soudem	soud	k1gInSc7	soud
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
věznice	věznice	k1gFnSc2	věznice
Bory	bor	k1gInPc4	bor
<g/>
.	.	kIx.	.
</s>
<s>
Vyhovění	vyhovění	k1gNnSc1	vyhovění
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
nebylo	být	k5eNaImAgNnS	být
ministrem	ministr	k1gMnSc7	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Svobodou	Svoboda	k1gMnSc7	Svoboda
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
obhájce	obhájce	k1gMnSc1	obhájce
proti	proti	k7c3	proti
rozsudku	rozsudek	k1gInSc3	rozsudek
protestovali	protestovat	k5eAaBmAgMnP	protestovat
pouze	pouze	k6eAd1	pouze
představitelé	představitel	k1gMnPc1	představitel
MNV	MNV	kA	MNV
ve	v	k7c6	v
Štítině	Štítina	k1gFnSc6	Štítina
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
náčelník	náčelník	k1gMnSc1	náčelník
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
Louis	louis	k1gInSc4	louis
Faucher	Fauchra	k1gFnPc2	Fauchra
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
generála	generál	k1gMnSc2	generál
se	se	k3xPyFc4	se
po	po	k7c6	po
pitvě	pitva	k1gFnSc6	pitva
"	"	kIx"	"
<g/>
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
<g/>
"	"	kIx"	"
nepohřbeno	pohřben	k2eNgNnSc4d1	nepohřbeno
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
věznici	věznice	k1gFnSc6	věznice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
H.	H.	kA	H.
Píka	píka	k1gFnSc1	píka
čekal	čekat	k5eAaImAgMnS	čekat
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
války	válka	k1gFnSc2	válka
u	u	k7c2	u
Britského	britský	k2eAgNnSc2d1	Britské
královského	královský	k2eAgNnSc2d1	královské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pronásledování	pronásledování	k1gNnSc1	pronásledování
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
L.	L.	kA	L.
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
důkazů	důkaz	k1gInPc2	důkaz
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
prezidenta	prezident	k1gMnSc2	prezident
L.	L.	kA	L.
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
proces	proces	k1gInSc1	proces
s	s	k7c7	s
Píkou	píka	k1gFnSc7	píka
obnoven	obnovit	k5eAaPmNgInS	obnovit
a	a	k8xC	a
vojenský	vojenský	k2eAgInSc1d1	vojenský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
jeho	jeho	k3xOp3gFnSc4	jeho
nevinu	nevina	k1gFnSc4	nevina
a	a	k8xC	a
zrušil	zrušit	k5eAaPmAgMnS	zrušit
odsuzující	odsuzující	k2eAgInSc4d1	odsuzující
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
k	k	k7c3	k
plné	plný	k2eAgFnSc3d1	plná
rehabilitaci	rehabilitace	k1gFnSc3	rehabilitace
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Socialistický	socialistický	k2eAgInSc1d1	socialistický
režim	režim	k1gInSc1	režim
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
uznat	uznat	k5eAaPmF	uznat
jeho	jeho	k3xOp3gFnPc4	jeho
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
životem	život	k1gInSc7	život
mohla	moct	k5eAaImAgFnS	moct
seznámit	seznámit	k5eAaPmF	seznámit
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dokument	dokument	k1gInSc1	dokument
České	český	k2eAgFnSc2d1	Česká
Televize	televize	k1gFnSc2	televize
Proč	proč	k6eAd1	proč
vás	vy	k3xPp2nPc4	vy
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
,	,	kIx,	,
generále	generál	k1gMnSc5	generál
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
napsáno	napsat	k5eAaPmNgNnS	napsat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
udělil	udělit	k5eAaPmAgInS	udělit
H.	H.	kA	H.
Píkovi	Píka	k1gMnSc3	Píka
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
Řád	řád	k1gInSc1	řád
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
za	za	k7c4	za
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
vlasti	vlast	k1gFnSc2	vlast
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
mu	on	k3xPp3gMnSc3	on
odhalen	odhalit	k5eAaPmNgInS	odhalit
památník	památník	k1gInSc1	památník
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
i	i	k8xC	i
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Štítině	Štítina	k1gFnSc6	Štítina
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
domem	dům	k1gInSc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Píkovi	Píka	k1gMnSc6	Píka
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
i	i	k9	i
53	[number]	k4	53
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
průzkumu	průzkum	k1gInSc2	průzkum
a	a	k8xC	a
elektronického	elektronický	k2eAgInSc2d1	elektronický
boje	boj	k1gInSc2	boj
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
Plzni-Slovanech	Plzni-Slovan	k1gMnPc6	Plzni-Slovan
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
-	-	kIx~	-
Dejvicích	Dejvice	k1gFnPc6	Dejvice
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
na	na	k7c6	na
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Vaš	Vaš	k1gMnSc1	Vaš
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
ještě	ještě	k6eAd1	ještě
žijící	žijící	k2eAgMnSc1d1	žijící
aktér	aktér	k1gMnSc1	aktér
justiční	justiční	k2eAgFnSc2d1	justiční
vraždy	vražda	k1gFnSc2	vražda
generála	generál	k1gMnSc2	generál
Píky	píka	k1gFnSc2	píka
<g/>
,	,	kIx,	,
obžalován	obžalovat	k5eAaPmNgMnS	obžalovat
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejprve	nejprve	k6eAd1	nejprve
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
sedmi	sedm	k4xCc3	sedm
letům	let	k1gInPc3	let
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
rozsudek	rozsudek	k1gInSc4	rozsudek
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
trestní	trestní	k2eAgNnSc1d1	trestní
stíhání	stíhání	k1gNnSc1	stíhání
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
promlčení	promlčení	k1gNnSc2	promlčení
trestní	trestní	k2eAgFnSc2d1	trestní
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
budově	budova	k1gFnSc6	budova
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
Vítězném	vítězný	k2eAgNnSc6d1	vítězné
náměstí	náměstí	k1gNnSc6	náměstí
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
1500	[number]	k4	1500
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
<g/>
-Dejvicích	-Dejvice	k1gFnPc6	-Dejvice
(	(	kIx(	(
<g/>
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
hlavního	hlavní	k2eAgInSc2d1	hlavní
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
bronzové	bronzový	k2eAgFnPc4d1	bronzová
pamětní	pamětní	k2eAgFnPc4d1	pamětní
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
je	být	k5eAaImIp3nS	být
věnována	věnován	k2eAgFnSc1d1	věnována
Heliodoru	Heliodor	k1gMnSc3	Heliodor
Píkovi	Píka	k1gMnSc3	Píka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následující	následující	k2eAgInSc4d1	následující
text	text	k1gInSc4	text
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
PAMÁTCE	památka	k1gFnSc6	památka
VYNIKAJÍCÍCHO	VYNIKAJÍCÍCHO	kA	VYNIKAJÍCÍCHO
VOJÁKA	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
DIPLOMATA	diplomat	k1gMnSc2	diplomat
A	a	k8xC	a
VLASTENCE	vlastenec	k1gMnSc2	vlastenec
<g/>
,	,	kIx,	,
DIVIZNÍHO	divizní	k2eAgMnSc2d1	divizní
GENERÁLA	generál	k1gMnSc2	generál
HELIODORA	HELIODORA	kA	HELIODORA
PÍKY	píka	k1gFnSc2	píka
NEZÁKONNĚ	zákonně	k6eNd1	zákonně
POPRAVENÉHO	popravený	k2eAgNnSc2d1	popravené
21.6	[number]	k4	21.6
<g/>
.1949	.1949	k4	.1949
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
textem	text	k1gInSc7	text
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
ostnatého	ostnatý	k2eAgInSc2d1	ostnatý
drátu	drát	k1gInSc2	drát
s	s	k7c7	s
lipovými	lipový	k2eAgInPc7d1	lipový
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
deskou	deska	k1gFnSc7	deska
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
vojákům	voják	k1gMnPc3	voják
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
oběťmi	oběť	k1gFnPc7	oběť
komunistické	komunistický	k2eAgFnSc2d1	komunistická
zvůle	zvůle	k1gFnSc2	zvůle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
začíná	začínat	k5eAaImIp3nS	začínat
druhá	druhý	k4xOgFnSc1	druhý
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
TOMTO	tento	k3xDgInSc6	tento
KRUTÉM	krutý	k2eAgInSc6d1	krutý
OSUDU	osud	k1gInSc6	osud
BYL	být	k5eAaImAgInS	být
NÁSLEDOVÁN	následovat	k5eAaImNgInS	následovat
DALŠÍMI	další	k2eAgMnPc7d1	další
ČESKÝMI	český	k2eAgMnPc7d1	český
VOJÁKY	voják	k1gMnPc7	voják
Z	z	k7c2	z
POVOLÁNÍ	povolání	k1gNnSc2	povolání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
výčet	výčet	k1gInSc4	výčet
jmen	jméno	k1gNnPc2	jméno
včetně	včetně	k7c2	včetně
vojenských	vojenský	k2eAgFnPc2d1	vojenská
hodností	hodnost	k1gFnPc2	hodnost
a	a	k8xC	a
dne	den	k1gInSc2	den
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
druhé	druhý	k4xOgFnSc2	druhý
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
větou	věta	k1gFnSc7	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ČEST	čest	k1gFnSc4	čest
JEJICH	jejich	k3xOp3gFnSc3	jejich
PAMÁTCE	památka	k1gFnSc3	památka
I	i	k8xC	i
PAMÁTCE	památka	k1gFnSc6	památka
VŠECH	všecek	k3xTgMnPc2	všecek
DALŠÍCH	další	k2eAgMnPc2d1	další
PŘÍSLUŠNÍKŮ	příslušník	k1gMnPc2	příslušník
ČESKOSLOVENSKÉ	československý	k2eAgFnSc2d1	Československá
ARMÁDY	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
KTEŘÍ	který	k3yRgMnPc1	který
SE	se	k3xPyFc4	se
STALI	stát	k5eAaPmAgMnP	stát
OBĚTÍ	oběť	k1gFnSc7	oběť
KOMUNISTICKÉ	komunistický	k2eAgFnSc2d1	komunistická
ZVŮLE	zvůle	k1gFnSc2	zvůle
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
:	:	kIx,	:
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
;	;	kIx,	;
Opava	Opava	k1gFnSc1	Opava
:	:	kIx,	:
Optys	Optys	k1gInSc1	Optys
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
112	[number]	k4	112
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85819	[number]	k4	85819
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
.	.	kIx.	.
</s>
<s>
BENČÍK	BENČÍK	kA	BENČÍK
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
;	;	kIx,	;
RICHTER	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
generál	generál	k1gMnSc1	generál
Píka	píka	k1gFnSc1	píka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
307	[number]	k4	307
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85765	[number]	k4	85765
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
BENČÍK	BENČÍK	kA	BENČÍK
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
;	;	kIx,	;
RICHTER	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
jménem	jméno	k1gNnSc7	jméno
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Tragický	tragický	k2eAgInSc1d1	tragický
osud	osud	k1gInSc1	osud
generála	generál	k1gMnSc2	generál
Heliodora	Heliodor	k1gMnSc2	Heliodor
Píky	píka	k1gFnSc2	píka
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
383	[number]	k4	383
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86289	[number]	k4	86289
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
JIŘÍK	Jiřík	k1gMnSc1	Jiřík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
dopis	dopis	k1gInSc1	dopis
generála	generál	k1gMnSc2	generál
Heliodora	Heliodor	k1gMnSc2	Heliodor
Píky	píka	k1gFnSc2	píka
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
-	-	kIx~	-
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
obětí	oběť	k1gFnPc2	oběť
stalinských	stalinský	k2eAgFnPc2d1	stalinská
represí	represe	k1gFnPc2	represe
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědné	vlastivědný	k2eAgInPc1d1	vlastivědný
listy	list	k1gInPc1	list
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
dnešek	dnešek	k1gInSc4	dnešek
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1213	[number]	k4	1213
<g/>
-	-	kIx~	-
<g/>
3140	[number]	k4	3140
<g/>
.	.	kIx.	.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
K	k	k7c3	k
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
649	[number]	k4	649
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
571	[number]	k4	571
<g/>
.	.	kIx.	.
</s>
<s>
VÁHALA	váhat	k5eAaImAgFnS	váhat
<g/>
,	,	kIx,	,
Rastislav	Rastislav	k1gMnSc1	Rastislav
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
generála	generál	k1gMnSc2	generál
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
189	[number]	k4	189
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
124	[number]	k4	124
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Heliodor	Heliodora	k1gFnPc2	Heliodora
Píka	píka	k1gFnSc1	píka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Heliodor	Heliodor	k1gInSc4	Heliodor
Píka	píka	k1gFnSc1	píka
Milan	Milana	k1gFnPc2	Milana
Píka	píka	k1gFnSc1	píka
<g/>
:	:	kIx,	:
Životopis	životopis	k1gInSc1	životopis
armádního	armádní	k2eAgMnSc2d1	armádní
generála	generál	k1gMnSc2	generál
i.	i.	k?	i.
m.	m.	k?	m.
Heliodora	Heliodora	k1gFnSc1	Heliodora
PÍKY	píka	k1gFnSc2	píka
(	(	kIx(	(
<g/>
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
dole	dole	k6eAd1	dole
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
životopisu	životopis	k1gInSc2	životopis
a	a	k8xC	a
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Milanem	Milan	k1gMnSc7	Milan
Píkou	píka	k1gFnSc7	píka
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Jírovec	jírovec	k1gInSc1	jírovec
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
generála	generál	k1gMnSc2	generál
Heliodora	Heliodor	k1gMnSc2	Heliodor
Píky	píka	k1gFnSc2	píka
-	-	kIx~	-
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
,	,	kIx,	,
č.	č.	k?	č.
339	[number]	k4	339
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vališ	Vališ	k1gMnSc1	Vališ
<g/>
:	:	kIx,	:
Podplukovník	podplukovník	k1gMnSc1	podplukovník
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
JUDr.	JUDr.	kA	JUDr.
a	a	k8xC	a
PhDr.	PhDr.	kA	PhDr.
Karel	Karel	k1gMnSc1	Karel
Vaš	Vaš	k1gMnSc1	Vaš
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vališ	Vališ	k1gMnSc1	Vališ
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
"	"	kIx"	"
<g/>
zaprášených	zaprášený	k2eAgFnPc2d1	zaprášená
<g/>
"	"	kIx"	"
analýz	analýza	k1gFnPc2	analýza
sovětské	sovětský	k2eAgFnSc2d1	sovětská
politiky	politika	k1gFnSc2	politika
válečné	válečný	k2eAgFnSc2d1	válečná
i	i	k8xC	i
poválečné	poválečný	k2eAgFnSc2d1	poválečná
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
2005	[number]	k4	2005
Luděk	Luďka	k1gFnPc2	Luďka
Navara	Navara	k1gFnSc1	Navara
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
:	:	kIx,	:
Šťastný	šťastný	k2eAgInSc1d1	šťastný
příběh	příběh	k1gInSc1	příběh
Píkova	Píkův	k2eAgMnSc2d1	Píkův
vraha	vrah	k1gMnSc2	vrah
Karla	Karel	k1gMnSc2	Karel
Vaše	váš	k3xOp2gFnSc1	váš
(	(	kIx(	(
<g/>
neautorizovaná	autorizovaný	k2eNgFnSc1d1	neautorizovaná
kopie	kopie	k1gFnSc1	kopie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
in	in	k?	in
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
Konfederace	konfederace	k1gFnSc1	konfederace
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
-	-	kIx~	-
K	K	kA	K
<g/>
231	[number]	k4	231
<g/>
,	,	kIx,	,
pobočka	pobočka	k1gFnSc1	pobočka
35	[number]	k4	35
Mělník	Mělník	k1gInSc1	Mělník
<g/>
:	:	kIx,	:
Divizní	divizní	k2eAgMnSc1d1	divizní
generál	generál	k1gMnSc1	generál
Heliodor	Heliodor	k1gMnSc1	Heliodor
Píka	píka	k1gFnSc1	píka
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
rok	rok	k1gInSc4	rok
2002	[number]	k4	2002
Luděk	Luďka	k1gFnPc2	Luďka
Navara	Navara	k1gFnSc1	Navara
<g/>
:	:	kIx,	:
Generál	generál	k1gMnSc1	generál
Píka	píka	k1gFnSc1	píka
<g/>
:	:	kIx,	:
poprava	poprava	k1gFnSc1	poprava
statečného	statečný	k2eAgMnSc2d1	statečný
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
in	in	k?	in
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
neutorizovaná	utorizovaný	k2eNgFnSc1d1	utorizovaný
kopie	kopie	k1gFnSc1	kopie
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Gottwaldův	Gottwaldův	k2eAgInSc4d1	Gottwaldův
dvojí	dvojí	k4xRgInSc4	dvojí
metr	metr	k1gInSc4	metr
<g/>
:	:	kIx,	:
dal	dát	k5eAaPmAgMnS	dát
milost	milost	k1gFnSc4	milost
synovi	syn	k1gMnSc3	syn
generála	generál	k1gMnSc2	generál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
in	in	k?	in
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
Milan	Milan	k1gMnSc1	Milan
Krejčiřík	Krejčiřík	k1gMnSc1	Krejčiřík
<g/>
:	:	kIx,	:
Heliodor	Heliodor	k1gMnSc1	Heliodor
PÍKA	píka	k1gFnSc1	píka
<g/>
,	,	kIx,	,
in	in	k?	in
Totalita	totalita	k1gFnSc1	totalita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Milan	Milan	k1gMnSc1	Milan
Krejčiřík	Krejčiřík	k1gMnSc1	Krejčiřík
<g/>
:	:	kIx,	:
Proces	proces	k1gInSc1	proces
s	s	k7c7	s
Heliodorem	Heliodor	k1gInSc7	Heliodor
<g />
.	.	kIx.	.
</s>
<s>
Píkou	píka	k1gFnSc7	píka
<g/>
,	,	kIx,	,
in	in	k?	in
Totalita	totalita	k1gFnSc1	totalita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
in	in	k?	in
memoriam	memoriam	k1gInSc1	memoriam
Heliodor	Heliodor	k1gInSc1	Heliodor
PÍKA	píka	k1gFnSc1	píka
in	in	k?	in
Pocta	pocta	k1gFnSc1	pocta
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vzdorovali	vzdorovat	k5eAaImAgMnP	vzdorovat
<g/>
,	,	kIx,	,
katalog	katalog	k1gInSc4	katalog
výstavy	výstava	k1gFnSc2	výstava
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
archiv	archiv	k1gInSc1	archiv
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
Road	Roado	k1gNnPc2	Roado
movie	movie	k1gFnSc2	movie
Milana	Milan	k1gMnSc2	Milan
Píky	píka	k1gFnSc2	píka
za	za	k7c7	za
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
generálem	generál	k1gMnSc7	generál
-	-	kIx~	-
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
Karel	Karel	k1gMnSc1	Karel
Lukas	Lukas	k1gMnSc1	Lukas
-	-	kIx~	-
Heliodor	Heliodor	k1gMnSc1	Heliodor
Píka	píka	k1gFnSc1	píka
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
