<s>
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
navržené	navržený	k2eAgFnSc6d1
či	či	k8xC
plánované	plánovaný	k2eAgFnSc6d1
budoucí	budoucí	k2eAgFnSc6d1
stavbě	stavba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Měl	mít	k5eAaImAgMnS
by	by	kYmCp3nS
obsahovat	obsahovat	k5eAaImF
pouze	pouze	k6eAd1
ověřitelná	ověřitelný	k2eAgNnPc4d1
fakta	faktum	k1gNnPc4
o	o	k7c6
publikovaných	publikovaný	k2eAgInPc6d1
záměrech	záměr	k1gInPc6
<g/>
,	,	kIx,
plánech	plán	k1gInPc6
nebo	nebo	k8xC
přípravě	příprava	k1gFnSc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
o	o	k7c6
vývoji	vývoj	k1gInSc6
<g/>
,	,	kIx,
proměně	proměna	k1gFnSc6
<g/>
,	,	kIx,
projednávání	projednávání	k1gNnSc6
nebo	nebo	k8xC
kritice	kritika	k1gFnSc3
takových	takový	k3xDgInPc2
záměrů	záměr	k1gInPc2
a	a	k8xC
plánů	plán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
úpravách	úprava	k1gFnPc6
článku	článek	k1gInSc2
<g/>
,	,	kIx,
prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
dbejte	dbát	k5eAaImRp2nP
pečlivě	pečlivě	k6eAd1
na	na	k7c4
omezení	omezení	k1gNnSc4
vlastního	vlastní	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
</s>
<s>
Logo	logo	k1gNnSc1
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
</s>
<s>
StátFinsko	StátFinsko	k6eAd1
FinskoEstonsko	FinskoEstonsko	k1gNnSc1
EstonskoLotyšsko	EstonskoLotyšsko	k1gNnSc1
LotyšskoLitva	LotyšskoLitva	k1gFnSc1
LitvaPolsko	LitvaPolsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
Datum	datum	k1gNnSc1
otevření	otevření	k1gNnSc2
<g/>
2026	#num#	k4
</s>
<s>
Technické	technický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc4
stanic	stanice	k1gFnPc2
<g/>
7	#num#	k4
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
870	#num#	k4
km	km	kA
</s>
<s>
Rozchod	rozchod	k1gInSc1
koleje	kolej	k1gFnSc2
<g/>
1435	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
normální	normální	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Napájecí	napájecí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
25	#num#	k4
kV	kV	k?
50	#num#	k4
Hz	Hz	kA
AC	AC	kA
</s>
<s>
Počet	počet	k1gInSc4
kolejí	kolej	k1gFnPc2
<g/>
2	#num#	k4
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
249	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Mapa	mapa	k1gFnSc1
trati	trať	k1gFnSc2
</s>
<s>
Průběh	průběh	k1gInSc1
trati	trať	k1gFnSc2
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
Estonsku	Estonsko	k1gNnSc6
známo	znám	k2eAgNnSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
Rail	Rail	k1gInSc1
Baltic	Baltice	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
projektovaná	projektovaný	k2eAgFnSc1d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
v	v	k7c6
severojižní	severojižní	k2eAgFnSc6d1
ose	osa	k1gFnSc6
spojující	spojující	k2eAgNnSc4d1
Finsko	Finsko	k1gNnSc4
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc4
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc4
<g/>
,	,	kIx,
Litvu	Litva	k1gFnSc4
a	a	k8xC
Polsko	Polsko	k1gNnSc1
s	s	k7c7
evropskou	evropský	k2eAgFnSc7d1
normálněrozchodnou	normálněrozchodný	k2eAgFnSc7d1
železniční	železniční	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc1
části	část	k1gFnSc2
železnice	železnice	k1gFnSc2
vedoucí	vedoucí	k1gFnSc2
přes	přes	k7c4
Baltské	baltský	k2eAgInPc4d1
státy	stát	k1gInPc4
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
samostatně	samostatně	k6eAd1
jako	jako	k8xS,k8xC
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
Global	globat	k5eAaImAgMnS
Project	Project	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
bude	být	k5eAaImBp3nS
zajišťovat	zajišťovat	k5eAaImF
osobní	osobní	k2eAgFnSc4d1
a	a	k8xC
nákladní	nákladní	k2eAgFnSc4d1
železniční	železniční	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
mezi	mezi	k7c7
zeměmi	zem	k1gFnPc7
a	a	k8xC
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
propojení	propojení	k1gNnSc3
střední	střední	k2eAgFnSc2d1
a	a	k8xC
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
přímým	přímý	k2eAgNnSc7d1
spojením	spojení	k1gNnSc7
Tallinnu	Tallinn	k1gInSc2
s	s	k7c7
Varšavou	Varšava	k1gFnSc7
přes	přes	k7c4
Rigu	Riga	k1gFnSc4
a	a	k8xC
Kaunas	Kaunas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litevské	litevský	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Vilnius	Vilnius	k1gMnSc1
bude	být	k5eAaImBp3nS
se	s	k7c7
zbytkem	zbytek	k1gInSc7
sítě	síť	k1gFnSc2
spojeno	spojen	k2eAgNnSc4d1
odbočkou	odbočka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
je	být	k5eAaImIp3nS
prioritní	prioritní	k2eAgInSc4d1
projekt	projekt	k1gInSc4
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
transevropské	transevropský	k2eAgFnSc2d1
dopravní	dopravní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
TEN-T	TEN-T	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
</s>
<s>
Projektovaná	projektovaný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Baltských	baltský	k2eAgInPc6d1
státech	stát	k1gInPc6
870	#num#	k4
km	km	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
213	#num#	k4
km	km	kA
v	v	k7c6
Estonsku	Estonsko	k1gNnSc6
<g/>
,	,	kIx,
265	#num#	k4
km	km	kA
v	v	k7c6
Lotyšsku	Lotyšsko	k1gNnSc6
a	a	k8xC
392	#num#	k4
km	km	kA
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Trať	trať	k1gFnSc4
bude	být	k5eAaImBp3nS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
dvoukolejná	dvoukolejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1
napájecí	napájecí	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
25	#num#	k4
kV	kV	k?
AC	AC	kA
a	a	k8xC
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
1435	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Trasa	trasa	k1gFnSc1
je	být	k5eAaImIp3nS
projektována	projektovat	k5eAaBmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnula	vyhnout	k5eAaPmAgFnS
chráněným	chráněný	k2eAgNnSc7d1
územím	území	k1gNnSc7
soustavy	soustava	k1gFnSc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
a	a	k8xC
aby	aby	kYmCp3nS
nezasahovala	zasahovat	k5eNaImAgFnS
do	do	k7c2
stávající	stávající	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
o	o	k7c6
rozchodu	rozchod	k1gInSc6
1520	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládaná	předpokládaný	k2eAgFnSc1d1
provozní	provozní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
u	u	k7c2
vlaků	vlak	k1gInPc2
osobní	osobní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
dosahuje	dosahovat	k5eAaImIp3nS
235	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
maximální	maximální	k2eAgFnSc1d1
konstrukční	konstrukční	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
249	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
a	a	k8xC
120	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
u	u	k7c2
nákladních	nákladní	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Trať	trať	k1gFnSc1
bude	být	k5eAaImBp3nS
budována	budovat	k5eAaImNgFnS
zcela	zcela	k6eAd1
v	v	k7c6
nové	nový	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
jako	jako	k8xC,k8xS
veřejná	veřejný	k2eAgFnSc1d1
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1
konvenční	konvenční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
a	a	k8xC
bude	být	k5eAaImBp3nS
vybavena	vybavit	k5eAaPmNgFnS
prvky	prvek	k1gInPc7
systému	systém	k1gInSc2
ERTMS	ERTMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výstavbě	výstavba	k1gFnSc6
bude	být	k5eAaImBp3nS
užito	užít	k5eAaPmNgNnS
nejnovějších	nový	k2eAgFnPc2d3
technologií	technologie	k1gFnPc2
a	a	k8xC
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technické	technický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
odpovídají	odpovídat	k5eAaImIp3nP
technickým	technický	k2eAgFnPc3d1
specifikacím	specifikace	k1gFnPc3
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
pro	pro	k7c4
interoperabilitu	interoperabilita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Trasa	trasa	k1gFnSc1
</s>
<s>
V	v	k7c6
Baltských	baltský	k2eAgInPc6d1
státech	stát	k1gInPc6
zajistí	zajistit	k5eAaPmIp3nS
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
podmínky	podmínka	k1gFnSc2
pro	pro	k7c4
intermodalitu	intermodalita	k1gFnSc4
a	a	k8xC
multimodalitu	multimodalita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzniknou	vzniknout	k5eAaPmIp3nP
tři	tři	k4xCgInPc4
víceúčelové	víceúčelový	k2eAgInPc4d1
nákladní	nákladní	k2eAgInPc4d1
dopravní	dopravní	k2eAgInPc4d1
terminály	terminál	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
estonském	estonský	k2eAgInSc6d1
přístavu	přístav	k1gInSc6
Muuga	Muuga	k1gFnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
v	v	k7c6
lotyšském	lotyšský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Salaspils	Salaspilsa	k1gFnPc2
a	a	k8xC
v	v	k7c6
litevském	litevský	k2eAgInSc6d1
Kaunasu	Kaunas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
trase	trasa	k1gFnSc6
je	být	k5eAaImIp3nS
plánováno	plánovat	k5eAaImNgNnS
celkem	celkem	k6eAd1
sedm	sedm	k4xCc1
stanic	stanice	k1gFnPc2
osobní	osobní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
Tallinn	Tallinn	k1gNnSc4
<g/>
,	,	kIx,
Pärnu	Pärna	k1gFnSc4
<g/>
,	,	kIx,
Riga	Riga	k1gFnSc1
<g/>
,	,	kIx,
Riga	Riga	k1gFnSc1
letiště	letiště	k1gNnSc2
<g/>
,	,	kIx,
Panevė	Panevė	k1gInSc1
<g/>
,	,	kIx,
Kaunas	Kaunas	k1gInSc1
a	a	k8xC
Vilnius	Vilnius	k1gInSc1
s	s	k7c7
možností	možnost	k1gFnSc7
vzniku	vznik	k1gInSc2
regionálních	regionální	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
a	a	k8xC
železničních	železniční	k2eAgNnPc2d1
spojení	spojení	k1gNnPc2
na	na	k7c4
jiná	jiný	k2eAgNnPc4d1
letiště	letiště	k1gNnPc4
nebo	nebo	k8xC
k	k	k7c3
přístavům	přístav	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Plánovací	plánovací	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
a	a	k8xC
ukončena	ukončen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
a	a	k8xC
projekční	projekční	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
samotné	samotný	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
je	být	k5eAaImIp3nS
plánována	plánovat	k5eAaImNgFnS
na	na	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
2019	#num#	k4
až	až	k6eAd1
2026	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Odhadovaná	odhadovaný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
pět	pět	k4xCc4
miliard	miliarda	k4xCgFnPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úsek	úsek	k1gInSc1
železniční	železniční	k2eAgFnSc2d1
trati	trať	k1gFnSc2
mezi	mezi	k7c7
Helsinkami	Helsinky	k1gFnPc7
a	a	k8xC
Tallinnem	Tallinno	k1gNnSc7
bude	být	k5eAaImBp3nS
provozován	provozovat	k5eAaImNgInS
stávajícími	stávající	k2eAgInPc7d1
komerčními	komerční	k2eAgInPc7d1
trajekty	trajekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgInSc1d1
spojení	spojení	k1gNnSc4
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
zajistit	zajistit	k5eAaPmF
plánovaný	plánovaný	k2eAgInSc4d1
podmořský	podmořský	k2eAgInSc4d1
tunel	tunel	k1gInSc4
Helsinky	Helsinky	k1gFnPc4
<g/>
–	–	k?
<g/>
Tallinn	Tallinn	k1gInSc1
procházející	procházející	k2eAgInSc1d1
pod	pod	k7c7
Finským	finský	k2eAgInSc7d1
zálivem	záliv	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
–	–	k?
Project	Project	k1gMnSc1
of	of	k?
the	the	k?
Century	Centura	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
</s>
<s>
Technical	Technicat	k5eAaPmAgInS
Parametres	Parametres	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Project	Project	k1gMnSc1
Timeline	Timelin	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riga	Riga	k1gFnSc1
–	–	k?
Varšava	Varšava	k1gFnSc1
za	za	k7c4
pět	pět	k4xCc4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtveřice	čtveřice	k1gFnSc1
zemí	zem	k1gFnPc2
pokročila	pokročit	k5eAaPmAgFnS
s	s	k7c7
plány	plán	k1gInPc7
na	na	k7c4
rychlovlaky	rychlovlak	k1gInPc4
do	do	k7c2
Pobaltí	Pobaltí	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zdopravy	zdoprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
A	a	k8xC
tunnel	tunnet	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
to	ten	k3xDgNnSc4
Tallinn	Tallinn	k1gNnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Helsinki	Helsinki	k1gNnSc1
believes	believesa	k1gFnPc2
it	it	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
feasible	feasible	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
yle	yle	k?
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-01-05	2016-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vysokorychlostní	vysokorychlostní	k2eAgInSc1d1
vlak	vlak	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rail	Raila	k1gFnPc2
Baltica	Baltic	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltic	k1gInSc2
–	–	k?
estonská	estonský	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
estonsky	estonsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltic	k1gInSc2
–	–	k?
lotyšská	lotyšský	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
lotyšsky	lotyšsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltic	k1gInSc2
–	–	k?
litevská	litevský	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
litevsky	litevsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltic	k1gInSc2
–	–	k?
polská	polský	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
na	na	k7c6
Facebooku	Facebook	k1gInSc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Rail	Rail	k1gMnSc1
Baltica	Baltica	k1gMnSc1
na	na	k7c6
YouTube	YouTub	k1gInSc5
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
305382142	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
|	|	kIx~
Finsko	Finsko	k1gNnSc1
|	|	kIx~
Estonsko	Estonsko	k1gNnSc1
|	|	kIx~
Lotyšsko	Lotyšsko	k1gNnSc1
|	|	kIx~
Litva	Litva	k1gFnSc1
</s>
