<s>
Projektovaná	projektovaný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Baltských	baltský	k2eAgInPc6d1
státech	stát	k1gInPc6
870	#num#	k4
km	km	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
213	#num#	k4
km	km	kA
v	v	k7c6
Estonsku	Estonsko	k1gNnSc6
<g/>
,	,	kIx,
265	#num#	k4
km	km	kA
v	v	k7c6
Lotyšsku	Lotyšsko	k1gNnSc6
a	a	k8xC
392	#num#	k4
km	km	kA
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Trať	trať	k1gFnSc4
bude	být	k5eAaImBp3nS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
dvoukolejná	dvoukolejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1
napájecí	napájecí	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
25	#num#	k4
kV	kV	k?
AC	AC	kA
a	a	k8xC
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
normální	normální	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
1435	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Trasa	trasa	k1gFnSc1
je	být	k5eAaImIp3nS
projektována	projektovat	k5eAaBmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnula	vyhnout	k5eAaPmAgFnS
chráněným	chráněný	k2eAgNnSc7d1
územím	území	k1gNnSc7
soustavy	soustava	k1gFnSc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
a	a	k8xC
aby	aby	kYmCp3nS
nezasahovala	zasahovat	k5eNaImAgFnS
do	do	k7c2
stávající	stávající	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
o	o	k7c6
rozchodu	rozchod	k1gInSc6
1520	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>