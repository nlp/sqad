<s>
Jacob	Jacobit	k5eAaPmRp2nS	Jacobit
Bernoulli	Bernoulli	kA	Bernoulli
(	(	kIx(	(
<g/>
též	též	k9	též
Jacques	Jacques	k1gMnSc1	Jacques
<g/>
,	,	kIx,	,
Jakob	Jakob	k1gMnSc1	Jakob
či	či	k8xC	či
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
též	též	k9	též
s	s	k7c7	s
číslovkou	číslovka	k1gFnSc7	číslovka
I.	I.	kA	I.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1655	[number]	k4	1655
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1705	[number]	k4	1705
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
zákonitosti	zákonitost	k1gFnPc4	zákonitost
křivek	křivka	k1gFnPc2	křivka
či	či	k8xC	či
mocninné	mocninný	k2eAgFnSc2d1	mocninná
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
teorie	teorie	k1gFnSc2	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
(	(	kIx(	(
<g/>
Binomické	binomický	k2eAgNnSc1d1	binomické
rozdělení	rozdělení	k1gNnSc1	rozdělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
variačního	variační	k2eAgInSc2d1	variační
počtu	počet	k1gInSc2	počet
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Johannem	Johann	k1gMnSc7	Johann
Bernoullim	Bernoullim	k1gInSc4	Bernoullim
učinili	učinit	k5eAaPmAgMnP	učinit
rozhodující	rozhodující	k2eAgInPc4d1	rozhodující
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
rozvíjející	rozvíjející	k2eAgFnSc6d1	rozvíjející
se	se	k3xPyFc4	se
matematické	matematický	k2eAgFnSc3d1	matematická
analýze	analýza	k1gFnSc3	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
belgického	belgický	k2eAgMnSc2d1	belgický
obchodníka	obchodník	k1gMnSc2	obchodník
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Markéty	Markéta	k1gFnSc2	Markéta
Schönauerové	Schönauerová	k1gFnSc2	Schönauerová
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
teologii	teologie	k1gFnSc4	teologie
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1671	[number]	k4	1671
a	a	k8xC	a
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
licenciátu	licenciát	k1gInSc2	licenciát
v	v	k7c6	v
teologii	teologie	k1gFnSc6	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
otcově	otcův	k2eAgFnSc3d1	otcova
vůli	vůle	k1gFnSc3	vůle
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
především	především	k9	především
matematikou	matematika	k1gFnSc7	matematika
<g/>
,	,	kIx,	,
fyzikou	fyzika	k1gFnSc7	fyzika
a	a	k8xC	a
astronomií	astronomie	k1gFnSc7	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
využíval	využívat	k5eAaImAgInS	využívat
i	i	k9	i
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yIgFnSc4	který
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
po	po	k7c6	po
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
nebo	nebo	k8xC	nebo
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1681	[number]	k4	1681
až	až	k8xS	až
1682	[number]	k4	1682
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
kartézskou	kartézský	k2eAgFnSc7d1	kartézská
matematikou	matematika	k1gFnSc7	matematika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
učenci	učenec	k1gMnPc7	učenec
-	-	kIx~	-
Janem	Jan	k1gMnSc7	Jan
Huddem	Hudd	k1gMnSc7	Hudd
<g/>
,	,	kIx,	,
Robertem	Robert	k1gMnSc7	Robert
Boylem	Boyl	k1gMnSc7	Boyl
či	či	k8xC	či
Robertem	Robert	k1gMnSc7	Robert
Hookem	Hooek	k1gMnSc7	Hooek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
přednášel	přednášet	k5eAaImAgMnS	přednášet
experimentální	experimentální	k2eAgFnSc4d1	experimentální
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
studiu	studio	k1gNnSc3	studio
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
zastával	zastávat	k5eAaImAgMnS	zastávat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1699	[number]	k4	1699
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Pruské	pruský	k2eAgFnSc2d1	pruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Jacob	Jacobit	k5eAaPmRp2nS	Jacobit
Bernoulli	Bernoulli	kA	Bernoulli
měl	mít	k5eAaImAgInS	mít
pět	pět	k4xCc4	pět
dcer	dcera	k1gFnPc2	dcera
a	a	k8xC	a
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
bratrů	bratr	k1gMnPc2	bratr
Johanna	Johann	k1gInSc2	Johann
a	a	k8xC	a
Nicolause	Nicolause	k1gFnSc2	Nicolause
se	se	k3xPyFc4	se
však	však	k9	však
žádné	žádný	k3yNgFnPc1	žádný
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dětí	dítě	k1gFnPc2	dítě
nestalo	stát	k5eNaPmAgNnS	stát
později	pozdě	k6eAd2	pozdě
taktéž	taktéž	k?	taktéž
známými	známá	k1gFnPc7	známá
matematiky	matematika	k1gFnSc2	matematika
či	či	k8xC	či
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1705	[number]	k4	1705
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počest	počest	k1gFnSc6	počest
Jacoba	Jacoba	k1gFnSc1	Jacoba
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
Johanna	Johann	k1gMnSc2	Johann
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
měsíční	měsíční	k2eAgInSc1d1	měsíční
kráter	kráter	k1gInSc1	kráter
Bernoulli	Bernoulli	kA	Bernoulli
<g/>
.	.	kIx.	.
</s>
<s>
Jacobova	Jacobův	k2eAgFnSc1d1	Jacobova
vědecká	vědecký	k2eAgFnSc1d1	vědecká
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
přínosná	přínosný	k2eAgFnSc1d1	přínosná
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
teorie	teorie	k1gFnSc2	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
a	a	k8xC	a
kombinatoriky	kombinatorika	k1gFnSc2	kombinatorika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
právě	právě	k9	právě
jeho	jeho	k3xOp3gNnSc1	jeho
a	a	k8xC	a
Leibnizova	Leibnizův	k2eAgFnSc1d1	Leibnizova
terminologie	terminologie	k1gFnSc1	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gInPc2	jeho
poznatků	poznatek	k1gInPc2	poznatek
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
nedokončeném	dokončený	k2eNgNnSc6d1	nedokončené
díle	dílo	k1gNnSc6	dílo
Ars	Ars	k1gFnPc2	Ars
conjectandi	conjectand	k1gMnPc1	conjectand
(	(	kIx(	(
<g/>
Umění	umění	k1gNnSc1	umění
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
roku	rok	k1gInSc2	rok
1713	[number]	k4	1713
-	-	kIx~	-
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
již	již	k6eAd1	již
objevuje	objevovat	k5eAaImIp3nS	objevovat
např.	např.	kA	např.
zákon	zákon	k1gInSc1	zákon
velkých	velký	k2eAgNnPc2d1	velké
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnPc4	číslo
později	pozdě	k6eAd2	pozdě
nazvána	nazván	k2eAgNnPc4d1	nazváno
jako	jako	k8xS	jako
Bernoulliho	Bernoulli	k1gMnSc4	Bernoulli
či	či	k8xC	či
dokazování	dokazování	k1gNnSc4	dokazování
pomocí	pomocí	k7c2	pomocí
matematické	matematický	k2eAgFnSc2d1	matematická
indukce	indukce	k1gFnSc2	indukce
<g/>
.	.	kIx.	.
</s>
<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
zabýval	zabývat	k5eAaImAgMnS	zabývat
teorií	teorie	k1gFnSc7	teorie
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
některých	některý	k3yIgInPc2	některý
významných	významný	k2eAgInPc2d1	významný
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
poté	poté	k6eAd1	poté
taktéž	taktéž	k?	taktéž
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
k	k	k7c3	k
výpočtům	výpočet	k1gInPc3	výpočet
délek	délka	k1gFnPc2	délka
křivek	křivka	k1gFnPc2	křivka
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
obsahů	obsah	k1gInPc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
problémů	problém	k1gInPc2	problém
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
pohybu	pohyb	k1gInSc3	pohyb
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
pomocí	pomoc	k1gFnPc2	pomoc
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
mladého	mladý	k2eAgInSc2d1	mladý
oboru	obor	k1gInSc2	obor
-	-	kIx~	-
infinitezimálního	infinitezimální	k2eAgInSc2d1	infinitezimální
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
Jacob	Jacoba	k1gFnPc2	Jacoba
také	také	k9	také
přispěl	přispět	k5eAaPmAgMnS	přispět
cennými	cenný	k2eAgInPc7d1	cenný
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popud	popud	k1gInSc4	popud
Leibnize	Leibnize	k1gFnSc2	Leibnize
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1696	[number]	k4	1696
tzv	tzv	kA	tzv
izoperimetrický	izoperimetrický	k2eAgInSc4d1	izoperimetrický
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
řešení	řešení	k1gNnSc4	řešení
poté	poté	k6eAd1	poté
podal	podat	k5eAaPmAgMnS	podat
Jacobův	Jacobův	k2eAgMnSc1d1	Jacobův
bratr	bratr	k1gMnSc1	bratr
Johann	Johann	k1gMnSc1	Johann
Bernoulli	Bernoulli	kA	Bernoulli
-	-	kIx~	-
ovšem	ovšem	k9	ovšem
dopustil	dopustit	k5eAaPmAgInS	dopustit
se	se	k3xPyFc4	se
chyby	chyba	k1gFnSc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
sám	sám	k3xTgInSc4	sám
problém	problém	k1gInSc4	problém
po	po	k7c6	po
namáhavém	namáhavý	k2eAgNnSc6d1	namáhavé
úsilí	úsilí	k1gNnSc6	úsilí
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
položení	položení	k1gNnSc3	položení
základů	základ	k1gInPc2	základ
variačního	variační	k2eAgInSc2d1	variační
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
výrazněji	výrazně	k6eAd2	výrazně
rozvinut	rozvinout	k5eAaPmNgInS	rozvinout
o	o	k7c4	o
půlstoletí	půlstoletí	k1gNnSc4	půlstoletí
později	pozdě	k6eAd2	pozdě
Leonhardem	Leonhard	k1gMnSc7	Leonhard
Eulerem	Euler	k1gMnSc7	Euler
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Johannem	Johann	k1gMnSc7	Johann
Jacob	Jacoba	k1gFnPc2	Jacoba
často	často	k6eAd1	často
vedl	vést	k5eAaImAgMnS	vést
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
kruzích	kruh	k1gInPc6	kruh
ostré	ostrý	k2eAgInPc4d1	ostrý
odborné	odborný	k2eAgInPc4d1	odborný
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
matematiky	matematika	k1gFnSc2	matematika
k	k	k7c3	k
popisování	popisování	k1gNnSc3	popisování
přírodních	přírodní	k2eAgInPc2d1	přírodní
jevů	jev	k1gInPc2	jev
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
např.	např.	kA	např.
těmito	tento	k3xDgFnPc7	tento
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
</s>
