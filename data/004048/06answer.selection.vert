<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
sklízet	sklízet	k5eAaImF	sklízet
jahody	jahoda	k1gFnPc4	jahoda
dopoledne	dopoledne	k1gNnSc2	dopoledne
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
oschlé	oschlý	k2eAgFnPc1d1	oschlá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
stále	stále	k6eAd1	stále
lehce	lehko	k6eAd1	lehko
podchlazené	podchlazený	k2eAgFnPc1d1	podchlazená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
udržuje	udržovat	k5eAaImIp3nS	udržovat
jejich	jejich	k3xOp3gInSc4	jejich
stav	stav	k1gInSc4	stav
déle	dlouho	k6eAd2	dlouho
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
<g/>
.	.	kIx.	.
</s>
