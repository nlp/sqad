<s>
Rudolf	Rudolf	k1gMnSc1
Bunček	Bunček	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Bunček	Bunček	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1881	#num#	k4
<g/>
Jastrabá	Jastrabý	k2eAgFnSc1d1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1968	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
87	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Choceň	Choceň	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
malíř	malíř	k1gMnSc1
a	a	k8xC
sochař	sochař	k1gMnSc1
Znám	znát	k5eAaImIp1nS
jako	jako	k9
</s>
<s>
sochař	sochař	k1gMnSc1
a	a	k8xC
malíř	malíř	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Bunček	Bunček	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1881	#num#	k4
Jastrabá	Jastrabý	k2eAgFnSc1d1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1968	#num#	k4
Choceň	Choceň	k1gFnSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
a	a	k8xC
malíř	malíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
obci	obec	k1gFnSc6
Jastrabá	Jastrabý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
byl	být	k5eAaImAgMnS
Augustin	Augustin	k1gMnSc1
Bunček	Bunček	k1gMnSc1
a	a	k8xC
matkou	matka	k1gFnSc7
Irena	Irena	k1gFnSc1
Amália	Amália	k1gFnSc1
Anna	Anna	k1gFnSc1
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retzbachová	Retzbachová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Ružomberku	Ružomberk	k1gInSc6
a	a	k8xC
v	v	k7c6
Prievidzi	Prievidza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1896	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
odešel	odejít	k5eAaPmAgMnS
na	na	k7c4
studia	studio	k1gNnPc4
na	na	k7c4
Uměleckoprůmyslovou	uměleckoprůmyslový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
mědirytectví	mědirytectví	k1gNnPc4
a	a	k8xC
potom	potom	k6eAd1
sochařství	sochařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
studiích	studie	k1gFnPc6
pracoval	pracovat	k5eAaImAgMnS
ve	v	k7c6
fajánsové	fajánsový	k2eAgFnSc6d1
dílně	dílna	k1gFnSc6
Zsolnayovcov	Zsolnayovcov	k1gInSc4
v	v	k7c6
Pécsi	Pécse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
realizoval	realizovat	k5eAaBmAgMnS
menší	malý	k2eAgFnPc4d2
sochařské	sochařský	k2eAgFnPc4d1
studie	studie	k1gFnPc4
v	v	k7c4
Jastrabé	Jastrabý	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
středoškolský	středoškolský	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
kreslení	kreslení	k1gNnSc2
v	v	k7c6
Levici	levice	k1gFnSc6
a	a	k8xC
v	v	k7c6
Krupině	Krupina	k1gFnSc6
<g/>
,	,	kIx,
následně	následně	k6eAd1
jako	jako	k9
školní	školní	k2eAgMnSc1d1
inspektor	inspektor	k1gMnSc1
v	v	k7c6
Banské	banský	k2eAgFnSc6d1
Bystrici	Bystrica	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
jeho	jeho	k3xOp3gFnPc2
malířských	malířský	k2eAgFnPc2d1
prací	práce	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Středoslovenském	středoslovenský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Banské	banský	k2eAgFnSc6d1
Bystrici	Bystrica	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
mají	mít	k5eAaImIp3nP
realistické	realistický	k2eAgNnSc1d1
zaměření	zaměření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1901	#num#	k4
byl	být	k5eAaImAgInS
odměněn	odměnit	k5eAaPmNgInS
I.	I.	kA
cenou	cena	k1gFnSc7
za	za	k7c4
návrh	návrh	k1gInSc4
na	na	k7c4
dekorativní	dekorativní	k2eAgFnSc4d1
výzdobu	výzdoba	k1gFnSc4
brány	brána	k1gFnSc2
obytného	obytný	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rudolf	Rudolf	k1gMnSc1
Bunček	Bunček	k1gMnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Záznam	záznam	k1gInSc1
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
v	v	k7c6
matrice	matrika	k1gFnSc6
řádek	řádka	k1gFnPc2
32	#num#	k4
–	–	k?
farnost	farnost	k1gFnSc1
Jastrabá	Jastrabý	k2eAgFnSc1d1
<g/>
;	;	kIx,
pokřtěn	pokřtít	k5eAaPmNgInS
jako	jako	k8xS,k8xC
Rudolphuse	Rudolphuse	k1gFnSc1
Franciscus	Franciscus	k1gMnSc1
Buncsek	Buncsek	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1061092127	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
311600342	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
