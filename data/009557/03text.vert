<p>
<s>
Bukáček	Bukáček	k1gMnSc1	Bukáček
malý	malý	k1gMnSc1	malý
(	(	kIx(	(
<g/>
Ixobrychus	Ixobrychus	k1gMnSc1	Ixobrychus
minutus	minutus	k1gMnSc1	minutus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
volavka	volavka	k1gFnSc1	volavka
žijící	žijící	k2eAgFnSc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
asi	asi	k9	asi
jako	jako	k8xC	jako
hrdlička	hrdlička	k1gFnSc1	hrdlička
zahradní	zahradní	k2eAgFnSc1d1	zahradní
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
hřbet	hřbet	k1gInSc4	hřbet
černý	černý	k2eAgInSc4d1	černý
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
hnědavě	hnědavě	k6eAd1	hnědavě
skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
hnědá	hnědý	k2eAgNnPc1d1	hnědé
<g/>
,	,	kIx,	,
podélně	podélně	k6eAd1	podélně
čárkovaná	čárkovaný	k2eAgFnSc1d1	čárkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc4	noha
má	mít	k5eAaImIp3nS	mít
zelenožluté	zelenožlutý	k2eAgNnSc1d1	zelenožluté
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
velmi	velmi	k6eAd1	velmi
skrytě	skrytě	k6eAd1	skrytě
v	v	k7c6	v
rákosových	rákosový	k2eAgInPc6d1	rákosový
a	a	k8xC	a
vrbových	vrbový	k2eAgInPc6d1	vrbový
porostech	porost	k1gInPc6	porost
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejlépe	dobře	k6eAd3	dobře
zjistitelná	zjistitelný	k2eAgFnSc1d1	zjistitelná
po	po	k7c6	po
hlase	hlas	k1gInSc6	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlasový	hlasový	k2eAgInSc4d1	hlasový
projev	projev	k1gInSc4	projev
==	==	k?	==
</s>
</p>
<p>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
hlubokým	hluboký	k2eAgInSc7d1	hluboký
nepřetržitým	přetržitý	k2eNgInSc7d1	nepřetržitý
a	a	k8xC	a
monotónním	monotónní	k2eAgInSc7d1	monotónní
"	"	kIx"	"
<g/>
vru	vru	k?	vru
<g/>
,	,	kIx,	,
vru	vru	k?	vru
<g/>
,	,	kIx,	,
vru	vru	k?	vru
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
letu	let	k1gInSc6	let
občas	občas	k6eAd1	občas
nosovým	nosový	k2eAgMnSc7d1	nosový
"	"	kIx"	"
<g/>
kerak	kerak	k6eAd1	kerak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Eurasie	Eurasie	k1gFnSc2	Eurasie
až	až	k9	až
po	po	k7c4	po
západní	západní	k2eAgFnSc4d1	západní
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc4	Pákistán
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
populace	populace	k1gFnPc1	populace
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
a	a	k8xC	a
Nové	Nové	k2eAgFnSc3d1	Nové
Guineji	Guinea	k1gFnSc3	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jako	jako	k8xC	jako
hnízdící	hnízdící	k2eAgMnSc1d1	hnízdící
pták	pták	k1gMnSc1	pták
chybí	chybět	k5eAaImIp3nS	chybět
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Bukáček	Bukáček	k1gMnSc1	Bukáček
malý	malý	k1gMnSc1	malý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
vzácný	vzácný	k2eAgInSc1d1	vzácný
druh	druh	k1gInSc1	druh
s	s	k7c7	s
odhadem	odhad	k1gInSc7	odhad
početnosti	početnost	k1gFnSc2	početnost
60	[number]	k4	60
až	až	k9	až
80	[number]	k4	80
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prokázaných	prokázaný	k2eAgNnPc2d1	prokázané
hnízdění	hnízdění	k1gNnPc2	hnízdění
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pravidelně	pravidelně	k6eAd1	pravidelně
obsazovaných	obsazovaný	k2eAgFnPc2d1	obsazovaná
lokalit	lokalita	k1gFnPc2	lokalita
v	v	k7c6	v
Polabí	Polabí	k1gNnSc6	Polabí
<g/>
,	,	kIx,	,
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
výrazně	výrazně	k6eAd1	výrazně
hojnější	hojný	k2eAgMnSc1d2	hojnější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněný	chráněný	k2eAgInSc4d1	chráněný
zákonem	zákon	k1gInSc7	zákon
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Vzácně	vzácně	k6eAd1	vzácně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
močálech	močál	k1gInPc6	močál
<g/>
,	,	kIx,	,
bažinách	bažina	k1gFnPc6	bažina
a	a	k8xC	a
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
rybníků	rybník	k1gInPc2	rybník
zarostlých	zarostlý	k2eAgMnPc2d1	zarostlý
rákosem	rákos	k1gInSc7	rákos
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
miskovitá	miskovitý	k2eAgFnSc1d1	miskovitá
stavba	stavba	k1gFnSc1	stavba
ze	z	k7c2	z
stébel	stéblo	k1gNnPc2	stéblo
ostřice	ostřice	k1gFnSc2	ostřice
a	a	k8xC	a
rákosí	rákosí	k1gNnSc2	rákosí
umístěná	umístěný	k2eAgFnSc1d1	umístěná
těsně	těsna	k1gFnSc3	těsna
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
4-6	[number]	k4	4-6
bělavých	bělavý	k2eAgNnPc2d1	bělavé
vajec	vejce	k1gNnPc2	vejce
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
asi	asi	k9	asi
17-19	[number]	k4	17-19
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
ještě	ještě	k6eAd1	ještě
nevzletná	vzletný	k2eNgFnSc1d1	vzletný
po	po	k7c6	po
10-12	[number]	k4	10-12
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
obratně	obratně	k6eAd1	obratně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rákosí	rákosí	k1gNnSc6	rákosí
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Vzletnosti	vzletnost	k1gFnPc4	vzletnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
21	[number]	k4	21
–	–	k?	–
23	[number]	k4	23
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tah	tah	k1gInSc1	tah
==	==	k?	==
</s>
</p>
<p>
<s>
Bukáček	Bukáček	k1gMnSc1	Bukáček
malý	malý	k1gMnSc1	malý
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
odlétá	odlétat	k5eAaPmIp3nS	odlétat
během	během	k7c2	během
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
mokřadech	mokřad	k1gInPc6	mokřad
střední	střední	k2eAgFnSc2d1	střední
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc7	měkkýš
<g/>
,	,	kIx,	,
rybami	ryba	k1gFnPc7	ryba
nebo	nebo	k8xC	nebo
žábami	žába	k1gFnPc7	žába
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
chován	chovat	k5eAaImNgInS	chovat
ve	v	k7c4	v
30	[number]	k4	30
zoo	zoo	k1gFnPc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
českých	český	k2eAgFnPc6d1	Česká
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
chovu	chov	k1gInSc6	chov
bukáčků	bukáček	k1gInPc2	bukáček
existují	existovat	k5eAaImIp3nP	existovat
již	již	k6eAd1	již
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
počátcích	počátek	k1gInPc6	počátek
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Reálný	reálný	k2eAgInSc1d1	reálný
chov	chov	k1gInSc1	chov
však	však	k9	však
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přišel	přijít	k5eAaPmAgInS	přijít
pár	pár	k1gInSc1	pár
těchto	tento	k3xDgMnPc2	tento
brodivých	brodivý	k2eAgMnPc2d1	brodivý
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
mládě	mládě	k1gNnSc1	mládě
odchovali	odchovat	k5eAaPmAgMnP	odchovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
–	–	k?	–
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
odchov	odchov	k1gInSc4	odchov
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
českých	český	k2eAgFnPc2d1	Česká
zoo	zoo	k1gFnPc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ještě	ještě	k9	ještě
několikrát	několikrát	k6eAd1	několikrát
zopakovat	zopakovat	k5eAaPmF	zopakovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
vylíhla	vylíhnout	k5eAaPmAgFnS	vylíhnout
dvě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
odchováno	odchovat	k5eAaPmNgNnS	odchovat
pět	pět	k4xCc1	pět
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zoo	zoo	k1gFnSc1	zoo
chovala	chovat	k5eAaImAgFnS	chovat
šest	šest	k4xCc4	šest
jedinců	jedinec	k1gMnPc2	jedinec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bukáček	bukáček	k1gMnSc1	bukáček
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
bukáček	bukáček	k1gMnSc1	bukáček
malý	malý	k2eAgMnSc1d1	malý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.biolib.cz/cz/taxon/id8394/	[url]	k4	http://www.biolib.cz/cz/taxon/id8394/
</s>
</p>
<p>
<s>
http://www.naturfoto.cz/bukacek-maly-fotografie-2349.html	[url]	k1gMnSc1	http://www.naturfoto.cz/bukacek-maly-fotografie-2349.html
</s>
</p>
<p>
<s>
hlas	hlas	k1gInSc1	hlas
</s>
</p>
