<s>
Bukáček	Bukáček	k1gMnSc1	Bukáček
malý	malý	k1gMnSc1	malý
(	(	kIx(	(
<g/>
Ixobrychus	Ixobrychus	k1gMnSc1	Ixobrychus
minutus	minutus	k1gMnSc1	minutus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
volavka	volavka	k1gFnSc1	volavka
žijící	žijící	k2eAgFnSc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
asi	asi	k9	asi
jako	jako	k8xC	jako
hrdlička	hrdlička	k1gFnSc1	hrdlička
zahradní	zahradní	k2eAgFnSc1d1	zahradní
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
hřbet	hřbet	k1gInSc4	hřbet
černý	černý	k2eAgInSc4d1	černý
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
hnědavě	hnědavě	k6eAd1	hnědavě
skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
<g/>
.	.	kIx.	.
</s>
