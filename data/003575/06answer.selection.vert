<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
California	Californium	k1gNnSc2	Californium
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pacifických	pacifický	k2eAgInPc2d1	pacifický
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
