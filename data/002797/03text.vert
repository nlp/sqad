<s>
František	František	k1gMnSc1	František
Štorm	Štorm	k1gInSc1	Štorm
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
tvůrce	tvůrce	k1gMnSc1	tvůrce
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
typograf	typograf	k1gMnSc1	typograf
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
odborný	odborný	k2eAgMnSc1d1	odborný
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štorm	Štorm	k1gInSc4	Štorm
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
malířky	malířka	k1gFnSc2	malířka
Dany	Dana	k1gFnSc2	Dana
Puchnarové	Puchnarová	k1gFnSc2	Puchnarová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vnukem	vnuk	k1gMnSc7	vnuk
Břetislava	Břetislav	k1gMnSc2	Břetislav
Štorma	Štorm	k1gMnSc2	Štorm
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc2d1	český
architekta	architekt	k1gMnSc2	architekt
<g/>
,	,	kIx,	,
heraldika	heraldik	k1gMnSc2	heraldik
a	a	k8xC	a
grafika	grafik	k1gMnSc2	grafik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
SOŠV	SOŠV	kA	SOŠV
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
VŠUP	VŠUP	kA	VŠUP
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Milana	Milan	k1gMnSc2	Milan
Hegara	Hegar	k1gMnSc2	Hegar
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Jana	Jana	k1gFnSc1	Jana
Solpery	Solpera	k1gFnSc2	Solpera
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
Ateliéru	ateliér	k1gInSc6	ateliér
knižní	knižní	k2eAgFnSc2d1	knižní
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
písma	písmo	k1gNnSc2	písmo
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
ateliéru	ateliér	k1gInSc2	ateliér
Tvorba	tvorba	k1gFnSc1	tvorba
písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
typografie	typografia	k1gFnSc2	typografia
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Střešovickou	střešovický	k2eAgFnSc4d1	Střešovická
písmolijnu	písmolijna	k1gFnSc4	písmolijna
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Storm	Storm	k1gInSc1	Storm
Type	typ	k1gInSc5	typ
Foundry	Foundra	k1gFnPc1	Foundra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
distribucí	distribuce	k1gFnSc7	distribuce
digitálních	digitální	k2eAgNnPc2d1	digitální
typografických	typografický	k2eAgNnPc2d1	typografické
písem	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
písmolijna	písmolijna	k1gFnSc1	písmolijna
nabízí	nabízet	k5eAaImIp3nS	nabízet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
originálních	originální	k2eAgNnPc2d1	originální
písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
Štormova	Štormův	k2eAgNnPc1d1	Štormovo
písma	písmo	k1gNnPc1	písmo
autorská	autorský	k2eAgNnPc1d1	autorské
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Serapion	Serapion	k1gInSc1	Serapion
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
;	;	kIx,	;
Trivia	trivium	k1gNnPc4	trivium
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
;	;	kIx,	;
Academica	Academic	k1gInSc2	Academic
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modifikace	modifikace	k1gFnSc2	modifikace
písem	písmo	k1gNnPc2	písmo
historických	historický	k2eAgFnPc2d1	historická
i	i	k9	i
digitalizace	digitalizace	k1gFnPc1	digitalizace
písem	písmo	k1gNnPc2	písmo
významných	významný	k2eAgMnPc2d1	významný
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Preissiga	Preissig	k1gMnSc2	Preissig
<g/>
,	,	kIx,	,
Slavoboje	Slavoboj	k1gInPc1	Slavoboj
Tusara	Tusar	k1gMnSc2	Tusar
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Rathouského	Rathouský	k1gMnSc2	Rathouský
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Týfy	Týfa	k1gMnSc2	Týfa
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Solpery	Solpera	k1gFnSc2	Solpera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
např.	např.	kA	např.
typografického	typografický	k2eAgInSc2d1	typografický
stylu	styl	k1gInSc2	styl
restaurace	restaurace	k1gFnSc2	restaurace
Lokál	lokál	k1gInSc1	lokál
<g/>
.	.	kIx.	.
</s>
<s>
Ilustracemi	ilustrace	k1gFnPc7	ilustrace
doprovodil	doprovodit	k5eAaPmAgInS	doprovodit
např.	např.	kA	např.
publikace	publikace	k1gFnSc2	publikace
"	"	kIx"	"
<g/>
Neser	srát	k5eNaImRp2nS	srát
bohy	bůh	k1gMnPc4	bůh
<g/>
"	"	kIx"	"
Josefa	Josef	k1gMnSc4	Josef
Moníka	moník	k1gMnSc4	moník
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
či	či	k8xC	či
sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Howarda	Howard	k1gMnSc2	Howard
Phillipse	Phillips	k1gMnSc2	Phillips
Lovecrafta	Lovecraft	k1gMnSc2	Lovecraft
(	(	kIx(	(
<g/>
od	od	k7c2	od
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
"	"	kIx"	"
<g/>
Eseje	esej	k1gInSc2	esej
o	o	k7c6	o
typografii	typografia	k1gFnSc6	typografia
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Magnesia	magnesium	k1gNnSc2	magnesium
litera	litera	k1gFnSc1	litera
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
publicistika	publicistika	k1gFnSc1	publicistika
<g/>
.	.	kIx.	.
</s>
<s>
Štorm	Štorm	k1gInSc1	Štorm
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
metalové	metalový	k2eAgFnSc6d1	metalová
kapele	kapela	k1gFnSc6	kapela
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammer	k1gInSc1	Hammer
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
také	také	k9	také
píše	psát	k5eAaImIp3nS	psát
veškeré	veškerý	k3xTgInPc4	veškerý
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nahrál	nahrát	k5eAaPmAgInS	nahrát
album	album	k1gNnSc4	album
Airbrusher	Airbrushra	k1gFnPc2	Airbrushra
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
obnovila	obnovit	k5eAaPmAgFnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Mantras	Mantrasa	k1gFnPc2	Mantrasa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
též	též	k9	též
členem	člen	k1gMnSc7	člen
projektu	projekt	k1gInSc2	projekt
Mortal	Mortal	k1gMnSc1	Mortal
Cabinet	Cabinet	k1gMnSc1	Cabinet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
cena	cena	k1gFnSc1	cena
Revolver	revolver	k1gInSc4	revolver
Revue	revue	k1gFnSc2	revue
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Eseje	esej	k1gInSc2	esej
o	o	k7c6	o
typografii	typografia	k1gFnSc6	typografia
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
knih	kniha	k1gFnPc2	kniha
nominovaných	nominovaný	k2eAgFnPc2d1	nominovaná
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
publicistika	publicistika	k1gFnSc1	publicistika
<g/>
.	.	kIx.	.
</s>
<s>
PAVLIŇÁK	PAVLIŇÁK	kA	PAVLIŇÁK
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
Z.	Z.	kA	Z.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagalla	k1gFnPc2	Chagalla
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
Štorm	Štorm	k1gInSc4	Štorm
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
Typografický	typografický	k2eAgMnSc1d1	typografický
zápisník	zápisník	k1gMnSc1	zápisník
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
písmo	písmo	k1gNnSc1	písmo
Anselm	Anselma	k1gFnPc2	Anselma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Revolver	revolver	k1gInSc4	revolver
Revue	revue	k1gFnSc2	revue
<g/>
,	,	kIx,	,
69,200	[number]	k4	69,200
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
11,10	[number]	k4	11,10
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
Štorm	Štorm	k1gInSc1	Štorm
František	františek	k1gInSc4	františek
<g/>
,	,	kIx,	,
Typografický	typografický	k2eAgInSc1d1	typografický
zápisník	zápisník	k1gInSc1	zápisník
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
písmo	písmo	k1gNnSc1	písmo
Gallus	Gallus	k1gInSc1	Gallus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Revolver	revolver	k1gInSc4	revolver
Revue	revue	k1gFnSc2	revue
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
70,200	[number]	k4	70,200
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
3,184	[number]	k4	3,184
<g/>
-	-	kIx~	-
<g/>
186	[number]	k4	186
Štorm	Štorm	k1gInSc1	Štorm
František	františek	k1gInSc4	františek
<g/>
,	,	kIx,	,
Typografický	typografický	k2eAgInSc4d1	typografický
zápisník	zápisník	k1gInSc4	zápisník
(	(	kIx(	(
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
stáž	stáž	k1gFnSc1	stáž
podaří	podařit	k5eAaPmIp3nS	podařit
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Revolver	revolver	k1gInSc4	revolver
Revue	revue	k1gFnSc2	revue
<g/>
,	,	kIx,	,
72,200	[number]	k4	72,200
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9,106	[number]	k4	9,106
<g/>
-	-	kIx~	-
<g/>
108	[number]	k4	108
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaBmAgInS	dít
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Štorm	Štorm	k1gInSc4	Štorm
Informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
abART	abART	k?	abART
<g/>
:	:	kIx,	:
Štorm	Štorm	k1gInSc1	Štorm
František	františek	k1gInSc1	františek
Střešovická	střešovický	k2eAgFnSc1d1	Střešovická
písmolijna	písmolijna	k1gFnSc1	písmolijna
/	/	kIx~	/
Storm	Storm	k1gInSc1	Storm
Type	typ	k1gInSc5	typ
Foundry	Foundr	k1gInPc4	Foundr
Typoid	Typoid	k1gInSc1	Typoid
-	-	kIx~	-
habilitační	habilitační	k2eAgFnSc1d1	habilitační
práce	práce	k1gFnSc1	práce
Františka	František	k1gMnSc2	František
Štorma	Štorm	k1gMnSc2	Štorm
na	na	k7c6	na
VŠUP	VŠUP	kA	VŠUP
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
typografie	typografia	k1gFnSc2	typografia
Lido	Lido	k1gNnSc4	Lido
STF	STF	kA	STF
-	-	kIx~	-
písmo	písmo	k1gNnSc4	písmo
volně	volně	k6eAd1	volně
použitelné	použitelný	k2eAgNnSc4d1	použitelné
pro	pro	k7c4	pro
nekomerční	komerční	k2eNgInPc4d1	nekomerční
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgNnSc4d1	dostupné
v	v	k7c6	v
šesti	šest	k4xCc6	šest
řezech	řez	k1gInPc6	řez
a	a	k8xC	a
formátu	formát	k1gInSc6	formát
OpenType	OpenTyp	k1gInSc5	OpenTyp
</s>
