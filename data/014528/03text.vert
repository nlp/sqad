<s>
Seznam	seznam	k1gInSc1
okresů	okres	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Okresy	okres	k1gInPc1
jsou	být	k5eAaImIp3nP
územní	územní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
i	i	k8xC
některé	některý	k3yIgNnSc1
ostatní	ostatní	k2eAgNnSc1d1
(	(	kIx(
<g/>
zejména	zejména	k9
evropské	evropský	k2eAgFnSc6d1
<g/>
)	)	kIx)
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Okresy	okres	k1gInPc1
uvedené	uvedený	k2eAgInPc1d1
v	v	k7c6
tabulce	tabulka	k1gFnSc6
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
okres	okres	k1gInSc1
Jeseník	Jeseník	k1gInSc1
až	až	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
hranice	hranice	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
mnohokrát	mnohokrát	k6eAd1
měnily	měnit	k5eAaImAgFnP
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
optimalizace	optimalizace	k1gFnSc2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
naposledy	naposledy	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
korespondovaly	korespondovat	k5eAaImAgFnP
s	s	k7c7
hranicemi	hranice	k1gFnPc7
správních	správní	k2eAgMnPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
okresu	okres	k1gInSc2
</s>
<s>
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
km²	km²	k?
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
(	(	kIx(
<g/>
ČSÚ	ČSÚ	kA
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
<g/>
[	[	kIx(
<g/>
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obcí	obec	k1gFnPc2
</s>
<s>
Samosprávný	samosprávný	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
(	(	kIx(
<g/>
kraj	kraj	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Územní	územní	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
(	(	kIx(
<g/>
kraj	kraj	k1gInSc1
v	v	k7c6
letech	let	k1gInPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Okres	okres	k1gInSc1
Benešov	Benešov	k1gInSc1
</s>
<s>
BN1	BN1	k4
474,699	474,699	k4
<g/>
5	#num#	k4
459	#num#	k4
</s>
<s>
64,73	64,73	k4
</s>
<s>
114	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Beroun	Beroun	k1gInSc1
</s>
<s>
BE	BE	kA
<g/>
661,918	661,918	k4
<g/>
6	#num#	k4
160	#num#	k4
</s>
<s>
130,17	130,17	k4
</s>
<s>
85	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Blansko	Blansko	k1gNnSc1
</s>
<s>
BK	BK	kA
<g/>
862,651	862,651	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
708	#num#	k4
</s>
<s>
122,54	122,54	k4
</s>
<s>
116	#num#	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Brno-město	Brno-města	k1gFnSc5
</s>
<s>
BM	BM	kA
<g/>
230,223	230,223	k4
<g/>
85	#num#	k4
913	#num#	k4
</s>
<s>
1	#num#	k4
676,28	676,28	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Brno-venkov	Brno-venkov	k1gInSc1
</s>
<s>
BI1	BI1	k4
498,952	498,952	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
300	#num#	k4
</s>
<s>
137,63	137,63	k4
</s>
<s>
187	#num#	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Bruntál	Bruntál	k1gInSc1
</s>
<s>
BR1	BR1	k4
536,069	536,069	k4
<g/>
2	#num#	k4
693	#num#	k4
</s>
<s>
60,34	60,34	k4
</s>
<s>
67	#num#	k4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Břeclav	Břeclav	k1gFnSc1
</s>
<s>
BV1	BV1	k4
0	#num#	k4
<g/>
48,911	48,911	k4
<g/>
12	#num#	k4
828	#num#	k4
</s>
<s>
107,57	107,57	k4
</s>
<s>
63	#num#	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
CL1	CL1	k4
0	#num#	k4
<g/>
72,911	72,911	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
756	#num#	k4
</s>
<s>
93,91	93,91	k4
</s>
<s>
57	#num#	k4
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
CB1	CB1	k4
638,301	638,301	k4
<g/>
86	#num#	k4
462	#num#	k4
</s>
<s>
113,81	113,81	k4
</s>
<s>
109	#num#	k4
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Český	český	k2eAgInSc4d1
Krumlov	Krumlov	k1gInSc4
</s>
<s>
CK1	CK1	k4
615,036	615,036	k4
<g/>
0	#num#	k4
516	#num#	k4
</s>
<s>
37,47	37,47	k4
</s>
<s>
46	#num#	k4
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Děčín	Děčín	k1gInSc1
</s>
<s>
DC	DC	kA
<g/>
908,581	908,581	k4
<g/>
28	#num#	k4
834141,80	834141,80	k4
</s>
<s>
52	#num#	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Domažlice	Domažlice	k1gFnPc1
</s>
<s>
DO1	DO1	k4
123,465	123,465	k4
<g/>
9	#num#	k4
926	#num#	k4
</s>
<s>
53,34	53,34	k4
</s>
<s>
85	#num#	k4
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Frýdek-Místek	Frýdek-Místek	k1gInSc1
</s>
<s>
FM1	FM1	k4
208,492	208,492	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
756	#num#	k4
</s>
<s>
171,91	171,91	k4
</s>
<s>
72	#num#	k4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Havlíčkův	Havlíčkův	k2eAgInSc4d1
Brod	Brod	k1gInSc4
</s>
<s>
HB1	HB1	k4
264,959	264,959	k4
<g/>
4	#num#	k4
217	#num#	k4
</s>
<s>
74,48	74,48	k4
</s>
<s>
120	#num#	k4
</s>
<s>
Kraj	kraj	k1gInSc1
VysočinaVýchodočeský	VysočinaVýchodočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
Okres	okres	k1gInSc1
Hodonín	Hodonín	k1gInSc1
</s>
<s>
HO1	HO1	k4
0	#num#	k4
<g/>
99,131	99,131	k4
<g/>
53	#num#	k4
225	#num#	k4
</s>
<s>
139,41	139,41	k4
</s>
<s>
82	#num#	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
HK	HK	kA
<g/>
891,621	891,621	k4
<g/>
62	#num#	k4
661	#num#	k4
</s>
<s>
182,43	182,43	k4
</s>
<s>
104	#num#	k4
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Cheb	Cheb	k1gInSc1
</s>
<s>
CH1	CH1	k4
0	#num#	k4
<g/>
45,949	45,949	k4
<g/>
0	#num#	k4
188	#num#	k4
</s>
<s>
86,23	86,23	k4
</s>
<s>
40	#num#	k4
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Chomutov	Chomutov	k1gInSc1
</s>
<s>
CV	CV	kA
<g/>
935,301	935,301	k4
<g/>
22	#num#	k4
157	#num#	k4
</s>
<s>
130,61	130,61	k4
</s>
<s>
44	#num#	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Chrudim	Chrudim	k1gFnSc1
</s>
<s>
CR	cr	k0
<g/>
992,621	992,621	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
199	#num#	k4
</s>
<s>
103,97	103,97	k4
</s>
<s>
108	#num#	k4
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
JN	JN	kA
<g/>
402,308	402,308	k4
<g/>
8	#num#	k4
200	#num#	k4
</s>
<s>
219,24	219,24	k4
</s>
<s>
34	#num#	k4
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Jeseník	Jeseník	k1gInSc1
</s>
<s>
JE	být	k5eAaImIp3nS
</s>
<s>
718,963	718,963	k4
<g/>
8	#num#	k4
779	#num#	k4
</s>
<s>
53,94	53,94	k4
</s>
<s>
24	#num#	k4
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Jičín	Jičín	k1gInSc1
</s>
<s>
JC	JC	kA
<g/>
886,637	886,637	k4
<g/>
9	#num#	k4
702	#num#	k4
</s>
<s>
89,89	89,89	k4
</s>
<s>
111	#num#	k4
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
JI1	JI1	k4
199,321	199,321	k4
<g/>
10	#num#	k4
522	#num#	k4
</s>
<s>
92,15	92,15	k4
</s>
<s>
123	#num#	k4
</s>
<s>
Kraj	kraj	k1gInSc1
VysočinaJihomoravský	VysočinaJihomoravský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
Okres	okres	k1gInSc1
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
</s>
<s>
JH1	JH1	k4
943,699	943,699	k4
<g/>
0	#num#	k4
604	#num#	k4
</s>
<s>
46,61	46,61	k4
</s>
<s>
106	#num#	k4
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
KV1	KV1	k4
514,951	514,951	k4
<g/>
15	#num#	k4
446	#num#	k4
</s>
<s>
76,20	76,20	k4
</s>
<s>
54	#num#	k4
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Karviná	Karviná	k1gFnSc1
</s>
<s>
KI	KI	kA
<g/>
356,242	356,242	k4
<g/>
56	#num#	k4
394	#num#	k4
</s>
<s>
719,72	719,72	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Kladno	Kladno	k1gNnSc1
</s>
<s>
KD	KD	kA
<g/>
719,611	719,611	k4
<g/>
58	#num#	k4
799	#num#	k4
</s>
<s>
220,67	220,67	k4
</s>
<s>
100	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Klatovy	Klatovy	k1gInPc1
</s>
<s>
KT1	KT1	k4
945,698	945,698	k4
<g/>
5	#num#	k4
726	#num#	k4
</s>
<s>
44,06	44,06	k4
</s>
<s>
94	#num#	k4
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Kolín	Kolín	k1gInSc1
</s>
<s>
KO	KO	kA
<g/>
743,579	743,579	k4
<g/>
6	#num#	k4
001	#num#	k4
</s>
<s>
129,11	129,11	k4
</s>
<s>
89	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Kroměříž	Kroměříž	k1gFnSc1
</s>
<s>
KM	km	kA
<g/>
795,671	795,671	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
569	#num#	k4
</s>
<s>
132,68	132,68	k4
</s>
<s>
79	#num#	k4
</s>
<s>
Zlínský	zlínský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
KH	kh	k0
<g/>
916,937	916,937	k4
<g/>
3	#num#	k4
404	#num#	k4
</s>
<s>
80,05	80,05	k4
</s>
<s>
88	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Liberec	Liberec	k1gInSc1
</s>
<s>
LI	li	k9
<g/>
988,871	988,871	k4
<g/>
69	#num#	k4
878	#num#	k4
</s>
<s>
171,79	171,79	k4
</s>
<s>
59	#num#	k4
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Litoměřice	Litoměřice	k1gInPc1
</s>
<s>
LT1	LT1	k4
0	#num#	k4
<g/>
32,161	32,161	k4
<g/>
17	#num#	k4
278	#num#	k4
</s>
<s>
113,62	113,62	k4
</s>
<s>
105	#num#	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Louny	Louny	k1gInPc1
</s>
<s>
LN1	LN1	k4
117,658	117,658	k4
<g/>
5	#num#	k4
191	#num#	k4
</s>
<s>
76,22	76,22	k4
</s>
<s>
70	#num#	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
ME	ME	kA
<g/>
701,081	701,081	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
659	#num#	k4
</s>
<s>
149,28	149,28	k4
</s>
<s>
69	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
MB1	MB1	k4
0	#num#	k4
<g/>
22,831	22,831	k4
<g/>
23	#num#	k4
659	#num#	k4
</s>
<s>
120,90	120,90	k4
</s>
<s>
120	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Most	most	k1gInSc1
</s>
<s>
MO	MO	kA
<g/>
467,161	467,161	k4
<g/>
11	#num#	k4
775	#num#	k4
</s>
<s>
239,26	239,26	k4
</s>
<s>
26	#num#	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Náchod	Náchod	k1gInSc1
</s>
<s>
NA	na	k7c4
<g/>
851,571	851,571	k4
<g/>
0	#num#	k4
<g/>
9	#num#	k4
550	#num#	k4
</s>
<s>
128,64	128,64	k4
</s>
<s>
78	#num#	k4
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
NJ	NJ	kA
<g/>
881,591	881,591	k4
<g/>
48	#num#	k4
074	#num#	k4
</s>
<s>
167,96	167,96	k4
</s>
<s>
53	#num#	k4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
</s>
<s>
NB	NB	kA
<g/>
850,079	850,079	k4
<g/>
4	#num#	k4
884	#num#	k4
</s>
<s>
111,62	111,62	k4
</s>
<s>
87	#num#	k4
<g/>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
OC1	OC1	k4
620,282	620,282	k4
<g/>
30	#num#	k4
408	#num#	k4
</s>
<s>
142,20	142,20	k4
</s>
<s>
96	#num#	k4
<g/>
Olomoucký	olomoucký	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Opava	Opava	k1gFnSc1
</s>
<s>
OP1	OP1	k4
113,111	113,111	k4
<g/>
74	#num#	k4
899	#num#	k4
</s>
<s>
157,13	157,13	k4
</s>
<s>
77	#num#	k4
<g/>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Ostrava-město	Ostrava-města	k1gFnSc5
</s>
<s>
OV	OV	kA
<g/>
331,533	331,533	k4
<g/>
26	#num#	k4
018	#num#	k4
</s>
<s>
983,37	983,37	k4
</s>
<s>
13	#num#	k4
<g/>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Pardubice	Pardubice	k1gInPc1
</s>
<s>
PU	PU	kA
<g/>
880,091	880,091	k4
<g/>
68	#num#	k4
423	#num#	k4
</s>
<s>
191,37	191,37	k4
</s>
<s>
112	#num#	k4
<g/>
Pardubický	pardubický	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Pelhřimov	Pelhřimov	k1gInSc1
</s>
<s>
PE1	PE1	k4
290,007	290,007	k4
<g/>
1	#num#	k4
914	#num#	k4
</s>
<s>
55,75	55,75	k4
</s>
<s>
120	#num#	k4
<g/>
Kraj	kraj	k1gInSc1
VysočinaJihočeský	VysočinaJihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
Okres	okres	k1gInSc1
Písek	Písek	k1gInSc1
</s>
<s>
PI1	PI1	k4
126,846	126,846	k4
<g/>
9	#num#	k4
843	#num#	k4
</s>
<s>
61,98	61,98	k4
</s>
<s>
75	#num#	k4
<g/>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Plzeň-jih	Plzeň-jiha	k1gFnPc2
</s>
<s>
PJ	PJ	kA
<g/>
990,046	990,046	k4
<g/>
2	#num#	k4
389	#num#	k4
</s>
<s>
63,02	63,02	k4
</s>
<s>
90	#num#	k4
<g/>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Plzeň-město	Plzeň-města	k1gMnSc5
</s>
<s>
PM	PM	kA
<g/>
261,461	261,461	k4
<g/>
88	#num#	k4
045	#num#	k4
</s>
<s>
719,21	719,21	k4
</s>
<s>
15	#num#	k4
<g/>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Plzeň-sever	Plzeň-sevra	k1gFnPc2
</s>
<s>
PS1	PS1	k4
286,797	286,797	k4
<g/>
4	#num#	k4
940	#num#	k4
</s>
<s>
58,24	58,24	k4
</s>
<s>
98	#num#	k4
<g/>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
PHA	pha	k0wR
(	(	kIx(
<g/>
AB	AB	kA
<g/>
)	)	kIx)
<g/>
496,031	496,031	k4
268	#num#	k4
796	#num#	k4
</s>
<s>
2	#num#	k4
557,90	557,90	k4
</s>
<s>
1	#num#	k4
<g/>
PrahaPraha	PrahaPraha	k1gFnSc1
</s>
<s>
Okres	okres	k1gInSc1
Praha-východ	Praha-východ	k1gInSc1
</s>
<s>
PY	PY	kA
<g/>
754,911	754,911	k4
<g/>
57	#num#	k4
146	#num#	k4
</s>
<s>
208,17	208,17	k4
</s>
<s>
110	#num#	k4
<g/>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
</s>
<s>
PZ	PZ	kA
<g/>
580,631	580,631	k4
<g/>
31	#num#	k4
231	#num#	k4
</s>
<s>
226,01	226,01	k4
</s>
<s>
79	#num#	k4
<g/>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Prachatice	Prachatice	k1gFnPc1
</s>
<s>
PT1	PT1	k4
375,035	375,035	k4
<g/>
0	#num#	k4
010	#num#	k4
</s>
<s>
36,37	36,37	k4
</s>
<s>
65	#num#	k4
<g/>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Prostějov	Prostějov	k1gInSc1
</s>
<s>
PV	PV	kA
<g/>
777,321	777,321	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
859	#num#	k4
</s>
<s>
138,76	138,76	k4
</s>
<s>
97	#num#	k4
<g/>
Olomoucký	olomoucký	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Přerov	Přerov	k1gInSc1
</s>
<s>
PR	pr	k0
<g/>
844,741	844,741	k4
<g/>
30	#num#	k4
082	#num#	k4
</s>
<s>
153,99	153,99	k4
</s>
<s>
104	#num#	k4
<g/>
Olomoucký	olomoucký	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Příbram	Příbram	k1gFnSc1
</s>
<s>
PB1	PB1	k4
692,051	692,051	k4
<g/>
12	#num#	k4
816	#num#	k4
</s>
<s>
66,67	66,67	k4
</s>
<s>
121	#num#	k4
<g/>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Rakovník	Rakovník	k1gInSc1
</s>
<s>
RA	ra	k0
<g/>
896,305	896,305	k4
<g/>
4	#num#	k4
993	#num#	k4
</s>
<s>
61,36	61,36	k4
</s>
<s>
83	#num#	k4
<g/>
Středočeský	středočeský	k2eAgInSc1d1
krajStředočeský	krajStředočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Rokycany	Rokycany	k1gInPc1
</s>
<s>
RO	RO	kA
<g/>
575,114	575,114	k4
<g/>
7	#num#	k4
458	#num#	k4
</s>
<s>
82,52	82,52	k4
</s>
<s>
68	#num#	k4
<g/>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
</s>
<s>
RK	RK	kA
<g/>
981,787	981,787	k4
<g/>
7	#num#	k4
829	#num#	k4
</s>
<s>
79,27	79,27	k4
</s>
<s>
80	#num#	k4
<g/>
Královéhradecký	královéhradecký	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Semily	Semily	k1gInPc1
</s>
<s>
SM	SM	kA
<g/>
698,997	698,997	k4
<g/>
3	#num#	k4
605	#num#	k4
</s>
<s>
105,30	105,30	k4
</s>
<s>
65	#num#	k4
<g/>
Liberecký	liberecký	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Sokolov	Sokolov	k1gInSc1
</s>
<s>
SO	So	kA
<g/>
753,608	753,608	k4
<g/>
9	#num#	k4
961	#num#	k4
</s>
<s>
119,38	119,38	k4
</s>
<s>
38	#num#	k4
<g/>
Karlovarský	karlovarský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Strakonice	Strakonice	k1gFnPc1
</s>
<s>
ST1	ST1	k4
0	#num#	k4
<g/>
32,106	32,106	k4
<g/>
9	#num#	k4
786	#num#	k4
</s>
<s>
67,62	67,62	k4
</s>
<s>
112	#num#	k4
<g/>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Svitavy	Svitava	k1gFnSc2
</s>
<s>
SY1	SY1	k4
378,561	378,561	k4
<g/>
0	#num#	k4
<g/>
3	#num#	k4
245	#num#	k4
</s>
<s>
74,89	74,89	k4
</s>
<s>
116	#num#	k4
<g/>
Pardubický	pardubický	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Šumperk	Šumperk	k1gInSc1
</s>
<s>
SU1	SU1	k4
313,061	313,061	k4
<g/>
21	#num#	k4
299	#num#	k4
</s>
<s>
92,38	92,38	k4
</s>
<s>
77	#num#	k4
<g/>
Olomoucký	olomoucký	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Tábor	Tábor	k1gInSc1
</s>
<s>
TA1	TA1	k4
326,011	326,011	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
115	#num#	k4
</s>
<s>
76,26	76,26	k4
</s>
<s>
110	#num#	k4
<g/>
Jihočeský	jihočeský	k2eAgInSc1d1
krajJihočeský	krajJihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Tachov	Tachov	k1gInSc1
</s>
<s>
TC1	TC1	k4
378,685	378,685	k4
<g/>
1	#num#	k4
917	#num#	k4
</s>
<s>
37,66	37,66	k4
</s>
<s>
51	#num#	k4
<g/>
Plzeňský	plzeňský	k2eAgInSc1d1
krajZápadočeský	krajZápadočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Teplice	teplice	k1gFnSc2
</s>
<s>
TP	TP	kA
<g/>
469,271	469,271	k4
<g/>
25	#num#	k4
498	#num#	k4
</s>
<s>
267,43	267,43	k4
</s>
<s>
34	#num#	k4
<g/>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Trutnov	Trutnov	k1gInSc1
</s>
<s>
TU1	TU1	k4
146,781	146,781	k4
<g/>
18	#num#	k4
174	#num#	k4
</s>
<s>
103,05	103,05	k4
</s>
<s>
75	#num#	k4
<g/>
Královéhradecký	královéhradecký	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Třebíč	Třebíč	k1gFnSc1
</s>
<s>
TR1	TR1	k4
463,071	463,071	k4
<g/>
11	#num#	k4
693	#num#	k4
</s>
<s>
76,34	76,34	k4
</s>
<s>
167	#num#	k4
<g/>
Kraj	kraj	k1gInSc1
VysočinaJihomoravský	VysočinaJihomoravský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
Okres	okres	k1gInSc4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
UH	uh	k0
<g/>
991,371	991,371	k4
<g/>
41	#num#	k4
467	#num#	k4
</s>
<s>
142,70	142,70	k4
</s>
<s>
78	#num#	k4
<g/>
Zlínský	zlínský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
UL	ul	kA
<g/>
404,441	404,441	k4
<g/>
18	#num#	k4
228	#num#	k4
</s>
<s>
292,33	292,33	k4
</s>
<s>
23	#num#	k4
<g/>
Ústecký	ústecký	k2eAgInSc1d1
krajSeveročeský	krajSeveročeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
</s>
<s>
UO1	UO1	k4
258,311	258,311	k4
<g/>
36	#num#	k4
760	#num#	k4
</s>
<s>
108,69	108,69	k4
</s>
<s>
115	#num#	k4
<g/>
Pardubický	pardubický	k2eAgInSc1d1
krajVýchodočeský	krajVýchodočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Vsetín	Vsetín	k1gInSc1
</s>
<s>
VS1	VS1	k4
142,871	142,871	k4
<g/>
42	#num#	k4
420	#num#	k4
</s>
<s>
124,62	124,62	k4
</s>
<s>
59	#num#	k4
<g/>
Zlínský	zlínský	k2eAgInSc1d1
krajSeveromoravský	krajSeveromoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Vyškov	Vyškov	k1gInSc1
</s>
<s>
VY	vy	k3xPp2nPc1
<g/>
876,068	876,068	k4
<g/>
8	#num#	k4
154	#num#	k4
</s>
<s>
100,63	100,63	k4
</s>
<s>
80	#num#	k4
<g/>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Zlín	Zlín	k1gInSc1
</s>
<s>
ZL1	ZL1	k4
0	#num#	k4
<g/>
33,591	33,591	k4
<g/>
90	#num#	k4
488	#num#	k4
</s>
<s>
184,30	184,30	k4
</s>
<s>
89	#num#	k4
<g/>
Zlínský	zlínský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Znojmo	Znojmo	k1gNnSc1
</s>
<s>
ZN1	ZN1	k4
590,501	590,501	k4
<g/>
11	#num#	k4
380	#num#	k4
</s>
<s>
70,03	70,03	k4
</s>
<s>
144	#num#	k4
<g/>
Jihomoravský	jihomoravský	k2eAgInSc1d1
krajJihomoravský	krajJihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Okres	okres	k1gInSc1
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
</s>
<s>
ZR1	ZR1	k4
578,511	578,511	k4
<g/>
17	#num#	k4
219	#num#	k4
</s>
<s>
74,26	74,26	k4
</s>
<s>
174	#num#	k4
<g/>
Kraj	kraj	k1gInSc1
VysočinaJihomoravský	VysočinaJihomoravský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Okresy	okres	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Okres	okres	k1gInSc1
</s>
<s>
Kraje	kraj	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zkratky	zkratka	k1gFnPc1
krajů	kraj	k1gInPc2
a	a	k8xC
okresů	okres	k1gInPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
ČSÚ	ČSÚ	kA
</s>
<s>
↑	↑	k?
Územní	územní	k2eAgFnSc2d1
změny	změna	k1gFnSc2
ve	v	k7c6
vojenských	vojenský	k2eAgInPc6d1
újezdech	újezd	k1gInPc6
od	od	k7c2
1.1	1.1	k4
<g/>
.2016	.2016	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Územní	územní	k2eAgFnPc4d1
změny	změna	k1gFnPc4
ve	v	k7c6
vojenských	vojenský	k2eAgInPc6d1
újezdech	újezd	k1gInPc6
od	od	k7c2
1.1	1.1	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Obecně	obecně	k6eAd1
o	o	k7c6
novém	nový	k2eAgInSc6d1
zákonu	zákon	k1gInSc6
č.	č.	k?
51	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://www.mvcr.cz/clanek/obecne-o-novem-zakonu-c-51-2020-sb.aspx	https://www.mvcr.cz/clanek/obecne-o-novem-zakonu-c-51-2020-sb.aspx	k1gInSc1
</s>
