<s>
Daniela	Daniela	k1gFnSc1	Daniela
Bambasová	Bambasová	k1gFnSc1	Bambasová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1954	[number]	k4	1954
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
i	i	k8xC	i
divadelní	divadelní	k2eAgFnSc1d1	divadelní
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Státní	státní	k2eAgFnSc1d1	státní
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
obor	obor	k1gInSc4	obor
Hudebně-dramatický	Hudebněramatický	k2eAgInSc1d1	Hudebně-dramatický
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
angažmá	angažmá	k1gNnSc1	angažmá
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Slováckém	slovácký	k2eAgNnSc6d1	Slovácké
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
stálé	stálý	k2eAgNnSc4d1	stálé
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgNnSc6d1	Jihočeské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
velké	velký	k2eAgFnPc1d1	velká
dramatické	dramatický	k2eAgFnPc1d1	dramatická
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Antigona	Antigona	k1gFnSc1	Antigona
<g/>
,	,	kIx,	,
Elektra	Elektra	k1gFnSc1	Elektra
<g/>
,	,	kIx,	,
Maryša	Maryša	k1gFnSc1	Maryša
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
rolí	role	k1gFnSc7	role
Milady	Milada	k1gFnSc2	Milada
Černé	Černá	k1gFnSc2	Černá
<g/>
,	,	kIx,	,
matky	matka	k1gFnPc1	matka
právníka	právník	k1gMnSc2	právník
Petra	Petr	k1gMnSc2	Petr
Černého	Černý	k1gMnSc2	Černý
<g/>
,	,	kIx,	,
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgInPc4d1	křehký
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
Daniela	Daniel	k1gMnSc4	Daniel
Bambase	Bambasa	k1gFnSc6	Bambasa
<g/>
.	.	kIx.	.
</s>
<s>
Daniela	Daniela	k1gFnSc1	Daniela
Bambasová	Bambasová	k1gFnSc1	Bambasová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
