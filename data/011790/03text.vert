<p>
<s>
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Fridrich	Fridrich	k1gMnSc1	Fridrich
Fulda	Fulda	k1gMnSc1	Fulda
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1872	[number]	k4	1872
Těšín	Těšín	k1gInSc4	Těšín
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
stavitele	stavitel	k1gMnSc2	stavitel
Fridricha	Fridrich	k1gMnSc2	Fridrich
Eduarda	Eduard	k1gMnSc2	Eduard
Fuldy	Fulda	k1gMnSc2	Fulda
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
Frice	Fric	k1gMnSc2	Fric
Fuldy	Fulda	k1gMnSc2	Fulda
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Luisy	Luisa	k1gFnSc2	Luisa
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
převzal	převzít	k5eAaPmAgInS	převzít
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
stavební	stavební	k2eAgFnSc4d1	stavební
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Zbýval	zbývat	k5eAaImAgMnS	zbývat
se	s	k7c7	s
projekční	projekční	k2eAgFnSc7d1	projekční
a	a	k8xC	a
stavební	stavební	k2eAgFnSc7d1	stavební
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
firmu	firma	k1gFnSc4	firma
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
cihelnu	cihelna	k1gFnSc4	cihelna
v	v	k7c6	v
Mostech	most	k1gInPc6	most
(	(	kIx(	(
<g/>
Č.	Č.	kA	Č.
Těšín-Mosty	Těšín-Most	k1gInPc1	Těšín-Most
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pilu	pila	k1gFnSc4	pila
<g/>
,	,	kIx,	,
truhlářství	truhlářství	k1gNnSc4	truhlářství
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgMnS	zavést
výrobu	výroba	k1gFnSc4	výroba
betonového	betonový	k2eAgNnSc2d1	betonové
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
k	k	k7c3	k
hloubení	hloubení	k1gNnSc3	hloubení
studní	studna	k1gFnPc2	studna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
postavil	postavit	k5eAaPmAgInS	postavit
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
nám.	nám.	k?	nám.
Svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgFnP	být
kanceláře	kancelář	k1gFnPc1	kancelář
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
první	první	k4xOgInSc4	první
výtah	výtah	k1gInSc4	výtah
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
prodán	prodat	k5eAaPmNgInS	prodat
bance	banka	k1gFnSc3	banka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Těšína	Těšín	k1gInSc2	Těšín
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
část	část	k1gFnSc4	část
Těšína	Těšín	k1gInSc2	Těšín
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
(	(	kIx(	(
<g/>
Fuldova	Fuldův	k2eAgFnSc1d1	Fuldův
vila	vila	k1gFnSc1	vila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
postavil	postavit	k5eAaPmAgMnS	postavit
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Fridrich	Fridrich	k1gMnSc1	Fridrich
Fulda	Fulda	k1gMnSc1	Fulda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
firmu	firma	k1gFnSc4	firma
na	na	k7c4	na
komanditní	komanditní	k2eAgFnSc4d1	komanditní
společnost	společnost	k1gFnSc4	společnost
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc1	tři
manažeři	manažer	k1gMnPc1	manažer
(	(	kIx(	(
<g/>
T.	T.	kA	T.
Hauschild	Hauschild	k1gInSc1	Hauschild
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Lamatsch	Lamatsch	k1gInSc1	Lamatsch
a	a	k8xC	a
E.	E.	kA	E.
Kubisch	Kubisch	k1gInSc1	Kubisch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
firma	firma	k1gFnSc1	firma
zastavila	zastavit	k5eAaPmAgFnS	zastavit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Fulda	Fulda	k1gMnSc1	Fulda
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
živnostenského	živnostenský	k2eAgInSc2d1	živnostenský
spolku	spolek	k1gInSc2	spolek
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
těšínského	těšínský	k2eAgNnSc2d1	Těšínské
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
člen	člen	k1gInSc1	člen
Klubu	klub	k1gInSc2	klub
techniků	technik	k1gMnPc2	technik
a	a	k8xC	a
člen	člen	k1gInSc1	člen
různých	různý	k2eAgMnPc2d1	různý
dobročinných	dobročinný	k2eAgMnPc2d1	dobročinný
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
přednesl	přednést	k5eAaPmAgMnS	přednést
zástupcům	zástupce	k1gMnPc3	zástupce
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
požadavek	požadavek	k1gInSc1	požadavek
těšínských	těšínský	k2eAgMnPc2d1	těšínský
Němců	Němec	k1gMnPc2	Němec
o	o	k7c4	o
zasazení	zasazení	k1gNnSc4	zasazení
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
pod	pod	k7c7	pod
protektorátem	protektorát	k1gInSc7	protektorát
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
oblast	oblast	k1gFnSc4	oblast
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
požadavek	požadavek	k1gInSc1	požadavek
byl	být	k5eAaImAgInS	být
také	také	k9	také
předán	předat	k5eAaPmNgInS	předat
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
formě	forma	k1gFnSc6	forma
(	(	kIx(	(
<g/>
Denkschrift	Denkschrift	k1gInSc1	Denkschrift
<g/>
,	,	kIx,	,
betreffend	betreffend	k1gInSc1	betreffend
die	die	k?	die
künftige	künftigat	k5eAaPmIp3nS	künftigat
staatliche	staatliche	k6eAd1	staatliche
Zugehörigkeit	Zugehörigkeit	k2eAgInSc1d1	Zugehörigkeit
dieses	dieses	k1gInSc1	dieses
Gebietes	Gebietes	k1gInSc4	Gebietes
<g/>
)	)	kIx)	)
prezidentovi	prezident	k1gMnSc3	prezident
USA	USA	kA	USA
Woodrowovi	Woodrowa	k1gMnSc3	Woodrowa
Wilsonovi	Wilson	k1gMnSc3	Wilson
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
čelním	čelní	k2eAgMnSc7d1	čelní
představitelem	představitel	k1gMnSc7	představitel
Německé	německý	k2eAgFnSc2d1	německá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
létech	léto	k1gNnPc6	léto
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Fulda	Fulda	k1gMnSc1	Fulda
začal	začít	k5eAaPmAgMnS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
protistátní	protistátní	k2eAgFnSc4d1	protistátní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
Patscheiderově	Patscheiderův	k2eAgInSc6d1	Patscheiderův
procesu	proces	k1gInSc6	proces
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
byla	být	k5eAaImAgFnS	být
obžaloba	obžaloba	k1gFnSc1	obžaloba
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
vedoucím	vedoucí	k1gMnPc3	vedoucí
místních	místní	k2eAgFnPc2d1	místní
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
využíval	využívat	k5eAaImAgInS	využívat
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
před	před	k7c7	před
hitlerovským	hitlerovský	k2eAgInSc7d1	hitlerovský
terorem	teror	k1gInSc7	teror
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1904	[number]	k4	1904
se	se	k3xPyFc4	se
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
Kratschmerovou	Kratschmerová	k1gFnSc7	Kratschmerová
z	z	k7c2	z
Těchova	Těchův	k2eAgNnSc2d1	Těchův
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
svazku	svazek	k1gInSc2	svazek
vzešly	vzejít	k5eAaPmAgFnP	vzejít
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
Fryderyk	Fryderyk	k1gMnSc1	Fryderyk
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
a	a	k8xC	a
Goetz	Goetz	k1gMnSc1	Goetz
Gunter	Gunter	k1gMnSc1	Gunter
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Stavěl	stavět	k5eAaImAgMnS	stavět
především	především	k9	především
pro	pro	k7c4	pro
hutní	hutní	k2eAgInPc4d1	hutní
a	a	k8xC	a
bánské	bánský	k2eAgInPc4d1	bánský
závody	závod	k1gInPc4	závod
a	a	k8xC	a
obce	obec	k1gFnPc4	obec
(	(	kIx(	(
<g/>
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
tělocvičny	tělocvična	k1gFnPc4	tělocvična
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc4	budova
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
</s>
</p>
<p>
<s>
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
městské	městský	k2eAgInPc1d1	městský
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Fellner	Fellner	k1gMnSc1	Fellner
<g/>
)	)	kIx)	)
<g/>
1905	[number]	k4	1905
budova	budova	k1gFnSc1	budova
justičního	justiční	k2eAgInSc2d1	justiční
paláce	palác	k1gInSc2	palác
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
baroka	baroko	k1gNnSc2	baroko
</s>
</p>
<p>
<s>
1905	[number]	k4	1905
německé	německý	k2eAgNnSc1d1	německé
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Fellner	Fellner	k1gMnSc1	Fellner
a	a	k8xC	a
Hermann	Hermann	k1gMnSc1	Hermann
Helmer	Helmer	k1gMnSc1	Helmer
<g/>
,	,	kIx,	,
<g/>
novobarokní	novobarokní	k2eAgInSc4d1	novobarokní
objekt	objekt	k1gInSc4	objekt
se	s	k7c7	s
secesními	secesní	k2eAgInPc7d1	secesní
prvky	prvek	k1gInPc7	prvek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1912	[number]	k4	1912
secesní	secesní	k2eAgInSc1d1	secesní
dům	dům	k1gInSc1	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
1	[number]	k4	1
(	(	kIx(	(
<g/>
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
hotel	hotel	k1gInSc1	hotel
U	u	k7c2	u
hnědého	hnědý	k2eAgMnSc2d1	hnědý
jelena	jelen	k1gMnSc2	jelen
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Těšín	Těšín	k1gInSc1	Těšín
</s>
</p>
<p>
<s>
1912	[number]	k4	1912
přestavba	přestavba	k1gFnSc1	přestavba
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
Leopolda	Leopold	k1gMnSc2	Leopold
Jana	Jan	k1gMnSc2	Jan
Szersznika	Szersznik	k1gMnSc2	Szersznik
na	na	k7c4	na
secesní	secesní	k2eAgInSc4d1	secesní
čtyřpatrový	čtyřpatrový	k2eAgInSc4d1	čtyřpatrový
důmStavby	důmStavba	k1gFnSc2	důmStavba
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
Soubor	soubor	k1gInSc1	soubor
staveb	stavba	k1gFnPc2	stavba
hřbitova	hřbitov	k1gInSc2	hřbitov
(	(	kIx(	(
<g/>
Hřbitovní	hřbitovní	k2eAgFnPc1d1	hřbitovní
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
ZŠ	ZŠ	kA	ZŠ
a	a	k8xC	a
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
návrh	návrh	k1gInSc1	návrh
Alfred	Alfred	k1gMnSc1	Alfred
Wiederman	Wiederman	k1gMnSc1	Wiederman
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Sokola	Sokol	k1gMnSc2	Sokol
Tůmy	Tůma	k1gMnSc2	Tůma
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
Těšín	Těšín	k1gInSc4	Těšín
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
Městská	městský	k2eAgFnSc1d1	městská
nemocnice	nemocnice	k1gFnSc1	nemocnice
v	v	k7c6	v
Č.	Č.	kA	Č.
Těšíně	Těšín	k1gInSc6	Těšín
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
Karl	Karl	k1gMnSc1	Karl
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
,	,	kIx,	,
realizace	realizace	k1gFnSc1	realizace
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Nekvasilem	Nekvasil	k1gMnSc7	Nekvasil
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
činžovní	činžovní	k2eAgInPc1d1	činžovní
domy	dům	k1gInPc1	dům
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
Vilém	Vilém	k1gMnSc1	Vilém
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
ČSOB	ČSOB	kA	ČSOB
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
sídlo	sídlo	k1gNnSc1	sídlo
pobočky	pobočka	k1gFnSc2	pobočka
Centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
německých	německý	k2eAgFnPc2d1	německá
spořitelen	spořitelna	k1gFnPc2	spořitelna
<g/>
,	,	kIx,	,
Nádražní	nádražní	k2eAgMnSc1d1	nádražní
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
požární	požární	k2eAgFnPc4d1	požární
zbrojnice	zbrojnice	k1gFnPc4	zbrojnice
(	(	kIx(	(
<g/>
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
sady	sada	k1gFnSc2	sada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
Vila	vila	k1gFnSc1	vila
Gottlieba	Gottlieba	k1gFnSc1	Gottlieba
a	a	k8xC	a
Stelly	Stella	k1gFnPc1	Stella
Fantelovy	Fantelův	k2eAgFnPc1d1	Fantelův
nyní	nyní	k6eAd1	nyní
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Reinhardtem	Reinhardt	k1gMnSc7	Reinhardt
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Č.	Č.	kA	Č.
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
hotel	hotel	k1gInSc1	hotel
Piast	Piast	k1gInSc4	Piast
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
Eduard	Eduard	k1gMnSc1	Eduard
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Nádražní	nádražní	k2eAgFnSc2d1	nádražní
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
Těšín	Těšín	k1gInSc1	Těšín
<g/>
,	,	kIx,	,
památková	památkový	k2eAgFnSc1d1	památková
zóna	zóna	k1gFnSc1	zóna
<g/>
)	)	kIx)	)
<g/>
Stavby	stavba	k1gFnPc1	stavba
mimo	mimo	k7c4	mimo
Těšín	Těšín	k1gInSc4	Těšín
</s>
</p>
<p>
<s>
1906	[number]	k4	1906
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Rakousko	Rakousko	k1gNnSc1	Rakousko
Těšínského	Těšínského	k2eAgInPc1d1	Těšínského
<g/>
,	,	kIx,	,
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
zděný	zděný	k2eAgInSc4d1	zděný
(	(	kIx(	(
<g/>
Visla	Visla	k1gFnSc1	Visla
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Ernest	Ernest	k1gMnSc1	Ernest
Altmann	Altmann	k1gMnSc1	Altmann
<g/>
,	,	kIx,	,
užíván	užíván	k2eAgMnSc1d1	užíván
jako	jako	k8xC	jako
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1917	[number]	k4	1917
zděný	zděný	k2eAgInSc1d1	zděný
komín	komín	k1gInSc1	komín
s	s	k7c7	s
vodojemem	vodojem	k1gInSc7	vodojem
<g/>
,	,	kIx,	,
válcovny	válcovna	k1gFnPc1	válcovna
na	na	k7c4	na
tenký	tenký	k2eAgInSc4d1	tenký
plech	plech	k1gInSc4	plech
Fryštát	Fryštát	k1gInSc4	Fryštát
<g/>
,	,	kIx,	,
snesen	snesen	k2eAgInSc4d1	snesen
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1928	[number]	k4	1928
turistická	turistický	k2eAgFnSc1d1	turistická
chata	chata	k1gFnSc1	chata
Skalka	skalka	k1gFnSc1	skalka
(	(	kIx(	(
<g/>
Mosty	most	k1gInPc7	most
u	u	k7c2	u
Jablunkova	Jablunkův	k2eAgInSc2d1	Jablunkův
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1930	[number]	k4	1930
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
vila	vila	k1gFnSc1	vila
(	(	kIx(	(
<g/>
Školní	školní	k2eAgFnSc1d1	školní
474	[number]	k4	474
<g/>
,	,	kIx,	,
Jablunkov	Jablunkov	k1gInSc1	Jablunkov
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
hornická	hornický	k2eAgFnSc1d1	hornická
kolonie	kolonie	k1gFnSc1	kolonie
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
pro	pro	k7c4	pro
úředníky	úředník	k1gMnPc4	úředník
</s>
</p>
<p>
<s>
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
kolonie	kolonie	k1gFnSc1	kolonie
Mexiko	Mexiko	k1gNnSc1	Mexiko
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
(	(	kIx(	(
<g/>
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
</s>
</p>
