<p>
<s>
Flétna	flétna	k1gFnSc1	flétna
je	být	k5eAaImIp3nS	být
dechový	dechový	k2eAgInSc4d1	dechový
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
se	se	k3xPyFc4	se
flétna	flétna	k1gFnSc1	flétna
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
píšťaly	píšťala	k1gFnSc2	píšťala
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k6eAd1	tak
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
hudebním	hudební	k2eAgInPc3d1	hudební
nástrojům	nástroj	k1gInPc3	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Flétna	flétna	k1gFnSc1	flétna
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gInSc1	její
vynález	vynález	k1gInSc1	vynález
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
významnými	významný	k2eAgMnPc7d1	významný
bohy	bůh	k1gMnPc7	bůh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
Usirem	Usirma	k1gFnPc2	Usirma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
Antice	antika	k1gFnSc6	antika
se	se	k3xPyFc4	se
flétny	flétna	k1gFnPc1	flétna
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
podle	podle	k7c2	podle
tóniny	tónina	k1gFnSc2	tónina
na	na	k7c4	na
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
ztratila	ztratit	k5eAaPmAgFnS	ztratit
flétna	flétna	k1gFnSc1	flétna
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prakticky	prakticky	k6eAd1	prakticky
nepoužívána	používán	k2eNgFnSc1d1	nepoužívána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flétna	flétna	k1gFnSc1	flétna
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Flétna	flétna	k1gFnSc1	flétna
resp.	resp.	kA	resp.
hráči	hráč	k1gMnSc3	hráč
na	na	k7c4	na
flétnu	flétna	k1gFnSc4	flétna
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
častým	častý	k2eAgInSc7d1	častý
motivem	motiv	k1gInSc7	motiv
starověkých	starověký	k2eAgFnPc2d1	starověká
maleb	malba	k1gFnPc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
především	především	k9	především
opera	opera	k1gFnSc1	opera
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
flétna	flétna	k1gFnSc1	flétna
od	od	k7c2	od
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Panova	Panův	k2eAgFnSc1d1	Panova
flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Příčná	příčný	k2eAgFnSc1d1	příčná
flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Zobcová	zobcový	k2eAgFnSc1d1	zobcová
flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
flétny	flétna	k1gFnPc1	flétna
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
whistle	whistle	k6eAd1	whistle
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
příčná	příčný	k2eAgFnSc1d1	příčná
flétna	flétna	k1gFnSc1	flétna
</s>
</p>
<p>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
flétna	flétna	k1gFnSc1	flétna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
flétna	flétna	k1gFnSc1	flétna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
