<s>
Oracle	Oracle	k6eAd1	Oracle
Corporation	Corporation	k1gInSc1	Corporation
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
společností	společnost	k1gFnPc2	společnost
vyvíjejících	vyvíjející	k2eAgFnPc2d1	vyvíjející
relační	relační	k2eAgFnSc4d1	relační
databáze	databáze	k1gFnPc4	databáze
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
databází	databáze	k1gFnPc2	databáze
či	či	k8xC	či
customer	customer	k1gInSc1	customer
relationship	relationship	k1gMnSc1	relationship
management	management	k1gInSc1	management
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
CRM	CRM	kA	CRM
<g/>
)	)	kIx)	)
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
zastoupení	zastoupení	k1gNnSc4	zastoupení
naleznete	nalézt	k5eAaBmIp2nP	nalézt
ve	v	k7c6	v
145	[number]	k4	145
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
CEO	CEO	kA	CEO
firmy	firma	k1gFnSc2	firma
je	být	k5eAaImIp3nS	být
Lawrence	Lawrenec	k1gInPc4	Lawrenec
J.	J.	kA	J.
Ellison	Ellison	k1gInSc4	Ellison
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Databáze	databáze	k1gFnPc1	databáze
Oracle	Oracl	k1gMnSc2	Oracl
Oracle	Oracle	k1gFnSc1	Oracle
Fusion	Fusion	k1gInSc1	Fusion
Middleware	Middlewar	k1gMnSc5	Middlewar
Oracle	Oracla	k1gFnSc3	Oracla
Enterprise	Enterprise	k1gFnSc1	Enterprise
Manager	manager	k1gMnSc1	manager
Oracle	Oracle	k1gInSc1	Oracle
Secure	Secur	k1gMnSc5	Secur
Enterprise	Enterprise	k1gFnPc1	Enterprise
Search	Search	k1gInSc4	Search
Oracle	Oracle	k1gFnSc2	Oracle
eBusiness	eBusiness	k6eAd1	eBusiness
Suite	Suit	k1gInSc5	Suit
PeopleSoft	PeopleSoft	k1gMnSc1	PeopleSoft
Enterprise	Enterprise	k1gFnSc2	Enterprise
Siebel	Siebel	k1gMnSc1	Siebel
JD	JD	kA	JD
Edwards	Edwards	k1gInSc1	Edwards
EnterpriseOne	EnterpriseOn	k1gInSc5	EnterpriseOn
JD	JD	kA	JD
Edwards	Edwards	k1gInSc1	Edwards
World	World	k1gInSc1	World
Firma	firma	k1gFnSc1	firma
též	též	k9	též
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
věštění	věštění	k1gNnSc1	věštění
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oracle	Oracle	k1gFnSc2	Oracle
Corporation	Corporation	k1gInSc1	Corporation
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Oracle	Oracle	k1gFnSc2	Oracle
</s>
