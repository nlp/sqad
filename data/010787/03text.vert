<p>
<s>
LCM-1E	LCM-1E	k4	LCM-1E
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
vyloďovacích	vyloďovací	k2eAgInPc2d1	vyloďovací
člunů	člun	k1gInPc2	člun
Španělského	španělský	k2eAgNnSc2d1	španělské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Čluny	člun	k1gInPc1	člun
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
loděnicí	loděnice	k1gFnSc7	loděnice
Navantia	Navantium	k1gNnSc2	Navantium
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
španělské	španělský	k2eAgFnSc2d1	španělská
letadlové	letadlový	k2eAgFnSc2d1	letadlová
a	a	k8xC	a
výsadkové	výsadkový	k2eAgFnSc2d1	výsadková
lodě	loď	k1gFnSc2	loď
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
I	I	kA	I
<g/>
,	,	kIx,	,
výrobku	výrobek	k1gInSc2	výrobek
stejné	stejná	k1gFnSc2	stejná
loděnice	loděnice	k1gFnSc2	loděnice
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
uživatelem	uživatel	k1gMnSc7	uživatel
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
Australské	australský	k2eAgNnSc1d1	Australské
královské	královský	k2eAgNnSc1d1	královské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
nasadí	nasadit	k5eAaPmIp3nP	nasadit
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
výsadkových	výsadkový	k2eAgFnPc6d1	výsadková
lodích	loď	k1gFnPc6	loď
třídy	třída	k1gFnSc2	třída
Canberra	Canberr	k1gInSc2	Canberr
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
unese	unést	k5eAaPmIp3nS	unést
čtyři	čtyři	k4xCgInPc4	čtyři
kusy	kus	k1gInPc4	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plavidla	plavidlo	k1gNnPc1	plavidlo
jsou	být	k5eAaImIp3nP	být
určena	určen	k2eAgNnPc1d1	určeno
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
námořních	námořní	k2eAgInPc2d1	námořní
výsadků	výsadek	k1gInPc2	výsadek
za	za	k7c4	za
horizont	horizont	k1gInSc4	horizont
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Výrobcem	výrobce	k1gMnSc7	výrobce
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
člunů	člun	k1gInPc2	člun
je	být	k5eAaImIp3nS	být
španělská	španělský	k2eAgFnSc1d1	španělská
loděnice	loděnice	k1gFnSc1	loděnice
Navantia	Navantia	k1gFnSc1	Navantia
<g/>
.	.	kIx.	.
</s>
<s>
Španělsku	Španělsko	k1gNnSc3	Španělsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
dodáno	dodat	k5eAaPmNgNnS	dodat
celkem	celek	k1gInSc7	celek
12	[number]	k4	12
člunů	člun	k1gInPc2	člun
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
objednala	objednat	k5eAaPmAgFnS	objednat
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
plavidel	plavidlo	k1gNnPc2	plavidlo
LCM-	LCM-	k1gFnSc1	LCM-
<g/>
1	[number]	k4	1
<g/>
E.	E.	kA	E.
První	první	k4xOgFnPc1	první
čtyři	čtyři	k4xCgFnPc1	čtyři
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
dodány	dodat	k5eAaPmNgFnP	dodat
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
přitom	přitom	k6eAd1	přitom
budou	být	k5eAaImBp3nP	být
dodány	dodat	k5eAaPmNgInP	dodat
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Turecko	Turecko	k1gNnSc1	Turecko
objednalo	objednat	k5eAaPmAgNnS	objednat
licenční	licenční	k2eAgFnSc4d1	licenční
stavbu	stavba	k1gFnSc4	stavba
čtyř	čtyři	k4xCgFnPc2	čtyři
plavidel	plavidlo	k1gNnPc2	plavidlo
LCM-	LCM-	k1gFnSc1	LCM-
<g/>
1	[number]	k4	1
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
budou	být	k5eAaImBp3nP	být
operovat	operovat	k5eAaImF	operovat
z	z	k7c2	z
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
turecké	turecký	k2eAgFnSc2d1	turecká
výsadkové	výsadkový	k2eAgFnSc2d1	výsadková
lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bude	být	k5eAaImBp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
případě	případ	k1gInSc6	případ
derivátem	derivát	k1gInSc7	derivát
plavidla	plavidlo	k1gNnPc1	plavidlo
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Člun	člun	k1gInSc1	člun
unese	unést	k5eAaPmIp3nS	unést
jeden	jeden	k4xCgInSc4	jeden
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
či	či	k8xC	či
dva	dva	k4xCgInPc1	dva
obrněné	obrněný	k2eAgInPc1d1	obrněný
transportéry	transportér	k1gInPc1	transportér
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odpovídající	odpovídající	k2eAgNnSc1d1	odpovídající
množství	množství	k1gNnSc1	množství
jiného	jiný	k2eAgInSc2d1	jiný
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nákládku	nákládka	k1gFnSc4	nákládka
a	a	k8xC	a
vykládku	vykládka	k1gFnSc4	vykládka
nákladu	náklad	k1gInSc2	náklad
slouží	sloužit	k5eAaImIp3nS	sloužit
příďová	příďový	k2eAgFnSc1d1	příďová
a	a	k8xC	a
záďová	záďový	k2eAgFnSc1d1	záďová
rampa	rampa	k1gFnSc1	rampa
<g/>
.	.	kIx.	.
</s>
<s>
Pohonný	pohonný	k2eAgInSc1d1	pohonný
systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
dva	dva	k4xCgInPc4	dva
diesely	diesel	k1gInPc4	diesel
MAN	Man	k1gMnSc1	Man
D-2842	D-2842	k1gFnSc2	D-2842
LE	LE	kA	LE
402X	[number]	k4	402X
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
806	[number]	k4	806
<g/>
kW	kW	kA	kW
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
vodní	vodní	k2eAgFnPc1d1	vodní
trysky	tryska	k1gFnPc1	tryska
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
22	[number]	k4	22
uzlů	uzel	k1gInPc2	uzel
u	u	k7c2	u
plavidla	plavidlo	k1gNnSc2	plavidlo
bez	bez	k7c2	bez
nákladu	náklad	k1gInSc2	náklad
a	a	k8xC	a
13,5	[number]	k4	13,5
uzlu	uzel	k1gInSc2	uzel
při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Dosah	dosah	k1gInSc1	dosah
činí	činit	k5eAaImIp3nS	činit
190	[number]	k4	190
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
352	[number]	k4	352
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
LCM-1E	LCM-1E	k1gFnSc2	LCM-1E
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
