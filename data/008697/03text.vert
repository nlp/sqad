<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
koryto	koryto	k1gNnSc1	koryto
(	(	kIx(	(
<g/>
též	též	k9	též
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
)	)	kIx)	)
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Proudí	proudit	k5eAaImIp3nS	proudit
jím	on	k3xPp3gInSc7	on
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
poloh	poloha	k1gFnPc2	poloha
do	do	k7c2	do
nižších	nízký	k2eAgMnPc2d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
erozní	erozní	k2eAgFnSc3d1	erozní
síle	síla	k1gFnSc3	síla
vody	voda	k1gFnSc2	voda
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
zahlubování	zahlubování	k1gNnSc3	zahlubování
do	do	k7c2	do
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
koryto	koryto	k1gNnSc1	koryto
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poměrně	poměrně	k6eAd1	poměrně
mělké	mělký	k2eAgInPc1d1	mělký
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
okraji	okraj	k1gInPc7	okraj
jako	jako	k8xC	jako
například	například	k6eAd1	například
Labe	Labe	k1gNnSc4	Labe
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
soutěsky	soutěsk	k1gInPc4	soutěsk
se	s	k7c7	s
strmými	strmý	k2eAgInPc7d1	strmý
okraji	okraj	k1gInPc7	okraj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vypínat	vypínat	k5eAaImF	vypínat
až	až	k9	až
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
metrů	metr	k1gInPc2	metr
-	-	kIx~	-
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
kaňonech	kaňon	k1gInPc6	kaňon
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příkré	příkrý	k2eAgInPc1d1	příkrý
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
nejčastěji	často	k6eAd3	často
pro	pro	k7c4	pro
horní	horní	k2eAgFnPc4d1	horní
části	část	k1gFnPc4	část
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
koryto	koryto	k1gNnSc4	koryto
velký	velký	k2eAgInSc1d1	velký
spád	spád	k1gInSc1	spád
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
značnou	značný	k2eAgFnSc4d1	značná
erozní	erozní	k2eAgFnSc4d1	erozní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tvořeno	tvořen	k2eAgNnSc4d1	tvořeno
dnem	den	k1gInSc7	den
a	a	k8xC	a
postranními	postranní	k2eAgInPc7d1	postranní
břehy	břeh	k1gInPc7	břeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
koryto	koryto	k1gNnSc1	koryto
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
celoročně	celoročně	k6eAd1	celoročně
tekoucí	tekoucí	k2eAgFnSc4d1	tekoucí
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
českých	český	k2eAgFnPc2d1	Česká
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
dočasné	dočasný	k2eAgNnSc1d1	dočasné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
naplní	naplnit	k5eAaPmIp3nS	naplnit
například	například	k6eAd1	například
jen	jen	k9	jen
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
(	(	kIx(	(
<g/>
známá	známý	k2eAgNnPc4d1	známé
suchá	suchý	k2eAgNnPc4d1	suché
koryta	koryto	k1gNnPc4	koryto
jsou	být	k5eAaImIp3nP	být
vádí	vádí	k1gNnPc1	vádí
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
či	či	k8xC	či
creeky	creek	k1gInPc7	creek
v	v	k7c6	v
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgNnSc1d1	říční
koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uměle	uměle	k6eAd1	uměle
upravováno	upravován	k2eAgNnSc1d1	upravováno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
voda	voda	k1gFnSc1	voda
protékala	protékat	k5eAaImAgFnS	protékat
místy	místy	k6eAd1	místy
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
člověk	člověk	k1gMnSc1	člověk
přeje	přát	k5eAaImIp3nS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
formou	forma	k1gFnSc7	forma
úpravy	úprava	k1gFnSc2	úprava
je	být	k5eAaImIp3nS	být
zpevňování	zpevňování	k1gNnSc1	zpevňování
<g/>
,	,	kIx,	,
či	či	k8xC	či
navyšování	navyšování	k1gNnSc1	navyšování
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
má	mít	k5eAaImIp3nS	mít
zabránit	zabránit	k5eAaPmF	zabránit
rozlévání	rozlévání	k1gNnSc4	rozlévání
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
či	či	k8xC	či
zabránění	zabránění	k1gNnSc4	zabránění
nebo	nebo	k8xC	nebo
minimalizaci	minimalizace	k1gFnSc4	minimalizace
dopadu	dopad	k1gInSc2	dopad
povodní	povodeň	k1gFnPc2	povodeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
řečiště	řečiště	k1gNnSc2	řečiště
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
