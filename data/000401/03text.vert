<s>
Šimon	Šimon	k1gMnSc1	Šimon
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Petr	Petr	k1gMnSc1	Petr
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
původních	původní	k2eAgMnPc2d1	původní
apoštolů	apoštol	k1gMnPc2	apoštol
-	-	kIx~	-
učedníků	učedník	k1gMnPc2	učedník
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
života	život	k1gInSc2	život
měl	mít	k5eAaImAgMnS	mít
mezi	mezi	k7c7	mezi
apoštoly	apoštol	k1gMnPc7	apoštol
zřejmě	zřejmě	k6eAd1	zřejmě
jisté	jistý	k2eAgNnSc4d1	jisté
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
představených	představený	k1gMnPc2	představený
společenství	společenství	k1gNnSc2	společenství
raných	raný	k2eAgMnPc2d1	raný
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
tradice	tradice	k1gFnSc2	tradice
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
prvního	první	k4xOgMnSc2	první
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
(	(	kIx(	(
<g/>
cca	cca	kA	cca
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
a	a	k8xC	a
římští	římský	k2eAgMnPc1d1	římský
biskupové	biskup	k1gMnPc1	biskup
na	na	k7c6	na
základě	základ	k1gInSc6	základ
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
posloupnosti	posloupnost	k1gFnSc2	posloupnost
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
primát	primát	k1gInSc4	primát
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
domnělým	domnělý	k2eAgInSc7d1	domnělý
hrobem	hrob	k1gInSc7	hrob
stojí	stát	k5eAaImIp3nS	stát
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
katolický	katolický	k2eAgInSc1d1	katolický
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
biblických	biblický	k2eAgInPc2d1	biblický
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
Svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Betsaidě	Betsaida	k1gFnSc6	Betsaida
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
rybáře	rybář	k1gMnSc4	rybář
Jana	Jan	k1gMnSc4	Jan
nebo	nebo	k8xC	nebo
Jonáše	Jonáš	k1gMnSc4	Jonáš
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
rodné	rodný	k2eAgNnSc4d1	rodné
jméno	jméno	k1gNnSc4	jméno
bylo	být	k5eAaImAgNnS	být
Šimon	Šimon	k1gMnSc1	Šimon
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
otcovském	otcovský	k2eAgInSc6d1	otcovský
domě	dům	k1gInSc6	dům
a	a	k8xC	a
synagogální	synagogální	k2eAgFnSc3d1	synagogální
škole	škola	k1gFnSc3	škola
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kafarnau	Kafarnaum	k1gNnSc6	Kafarnaum
<g/>
,	,	kIx,	,
vesnici	vesnice	k1gFnSc6	vesnice
u	u	k7c2	u
Genezaretského	Genezaretský	k2eAgNnSc2d1	Genezaretské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ondřejem	Ondřej	k1gMnSc7	Ondřej
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
rybářstvím	rybářství	k1gNnSc7	rybářství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Perpetua	perpetuum	k1gNnPc1	perpetuum
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Petronila	Petronila	k1gFnSc1	Petronila
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jednou	jeden	k4xCgFnSc7	jeden
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ondřejem	Ondřej	k1gMnSc7	Ondřej
vrhal	vrhat	k5eAaImAgMnS	vrhat
síť	síť	k1gFnSc4	síť
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
šel	jít	k5eAaImAgMnS	jít
okolo	okolo	k6eAd1	okolo
Ježíš	Ježíš	k1gMnSc1	Ježíš
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
oba	dva	k4xCgMnPc4	dva
rybáře	rybář	k1gMnPc4	rybář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
uposlechli	uposlechnout	k5eAaPmAgMnP	uposlechnout
a	a	k8xC	a
Šimon	Šimon	k1gMnSc1	Šimon
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
stal	stát	k5eAaPmAgMnS	stát
nejvěrnějším	věrný	k2eAgMnSc7d3	nejvěrnější
Kristovým	Kristův	k2eAgMnSc7d1	Kristův
učedníkem	učedník	k1gMnSc7	učedník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
svědkem	svědek	k1gMnSc7	svědek
mnoha	mnoho	k4c2	mnoho
Ježíšových	Ježíšův	k2eAgInPc2d1	Ježíšův
činů	čin	k1gInPc2	čin
a	a	k8xC	a
zázraků	zázrak	k1gInPc2	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hned	hned	k6eAd1	hned
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Kafarnau	Kafarnaus	k1gInSc6	Kafarnaus
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
Ježíš	Ježíš	k1gMnSc1	Ježíš
Petrovu	Petrův	k2eAgFnSc4d1	Petrova
tchyni	tchyně	k1gFnSc4	tchyně
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
mu	on	k3xPp3gMnSc3	on
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
setkání	setkání	k1gNnSc6	setkání
udělil	udělit	k5eAaPmAgInS	udělit
přezdívku	přezdívka	k1gFnSc4	přezdívka
Kefa	Kef	k1gInSc2	Kef
(	(	kIx(	(
<g/>
Kefas	Kefas	k1gInSc1	Kefas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
originále	originál	k1gInSc6	originál
překládá	překládat	k5eAaImIp3nS	překládat
Petros	Petrosa	k1gFnPc2	Petrosa
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Kefa	Kefa	k1gFnSc1	Kefa
znamená	znamenat	k5eAaImIp3nS	znamenat
aramejsky	aramejsky	k6eAd1	aramejsky
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgNnSc1d1	řecké
petros	petrosa	k1gFnPc2	petrosa
je	být	k5eAaImIp3nS	být
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
rod	rod	k1gInSc1	rod
petra	petra	k6eAd1	petra
znamená	znamenat	k5eAaImIp3nS	znamenat
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
i	i	k9	i
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
petrus	petrus	k1gMnSc1	petrus
a	a	k8xC	a
petra	petra	k1gMnSc1	petra
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
biblický	biblický	k2eAgInSc1d1	biblický
citát	citát	k1gInSc1	citát
používá	používat	k5eAaImIp3nS	používat
význam	význam	k1gInSc4	význam
přezdívky	přezdívka	k1gFnSc2	přezdívka
ve	v	k7c6	v
slovní	slovní	k2eAgFnSc6d1	slovní
hříčce	hříčka	k1gFnSc6	hříčka
<g/>
:	:	kIx,	:
Ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
skále	skála	k1gFnSc6	skála
postavím	postavit	k5eAaPmIp1nS	postavit
svou	svůj	k3xOyFgFnSc4	svůj
Církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
brány	brána	k1gFnPc4	brána
pekla	péct	k5eAaImAgFnS	péct
ji	on	k3xPp3gFnSc4	on
nepřemohou	přemoct	k5eNaPmIp3nP	přemoct
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
citátu	citát	k1gInSc2	citát
se	se	k3xPyFc4	se
také	také	k9	také
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
právě	právě	k9	právě
Petra	Petr	k1gMnSc4	Petr
Ježíš	ježit	k5eAaImIp2nS	ježit
vyvolil	vyvolit	k5eAaPmAgMnS	vyvolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
vybral	vybrat	k5eAaPmAgMnS	vybrat
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
učedníky	učedník	k1gMnPc7	učedník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hlásali	hlásat	k5eAaImAgMnP	hlásat
evangelium	evangelium	k1gNnSc4	evangelium
a	a	k8xC	a
konali	konat	k5eAaImAgMnP	konat
zázraky	zázrak	k1gInPc4	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Petra	Petr	k1gMnSc2	Petr
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
i	i	k9	i
cosi	cosi	k3yInSc4	cosi
obecně	obecně	k6eAd1	obecně
lidského	lidský	k2eAgNnSc2d1	lidské
a	a	k8xC	a
nadčasového	nadčasový	k2eAgNnSc2d1	nadčasové
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
vyvolených	vyvolený	k1gMnPc2	vyvolený
a	a	k8xC	a
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
evangelií	evangelium	k1gNnPc2	evangelium
svědkem	svědek	k1gMnSc7	svědek
mnoha	mnoho	k4c2	mnoho
zázraků	zázrak	k1gInPc2	zázrak
<g/>
,	,	kIx,	,
neubránil	ubránit	k5eNaPmAgMnS	ubránit
se	se	k3xPyFc4	se
pochybnostem	pochybnost	k1gFnPc3	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
výzvu	výzva	k1gFnSc4	výzva
kráčel	kráčet	k5eAaImAgMnS	kráčet
Petr	Petr	k1gMnSc1	Petr
po	po	k7c6	po
vlnách	vlna	k1gFnPc6	vlna
a	a	k8xC	a
málem	málem	k6eAd1	málem
utonul	utonout	k5eAaPmAgMnS	utonout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zapochyboval	zapochybovat	k5eAaPmAgMnS	zapochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vojáci	voják	k1gMnPc1	voják
Krista	Kristus	k1gMnSc4	Kristus
zatkli	zatknout	k5eAaPmAgMnP	zatknout
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nezná	znát	k5eNaImIp3nS	znát
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
sám	sám	k3xTgMnSc1	sám
Kristus	Kristus	k1gMnSc1	Kristus
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
kohout	kohout	k1gMnSc1	kohout
zakokrhá	zakokrhat	k5eAaPmIp3nS	zakokrhat
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
mne	já	k3xPp1nSc4	já
zapřeš	zapřít	k5eAaPmIp2nS	zapřít
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c4	po
seslání	seslání	k1gNnPc4	seslání
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
apoštolové	apoštol	k1gMnPc1	apoštol
rozešli	rozejít	k5eAaPmAgMnP	rozejít
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Petr	Petr	k1gMnSc1	Petr
po	po	k7c4	po
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
dána	dán	k2eAgFnSc1d1	dána
moc	moc	k1gFnSc1	moc
uzdravovat	uzdravovat	k5eAaImF	uzdravovat
nemocné	mocný	k2eNgMnPc4d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
uzdraveným	uzdravený	k2eAgInSc7d1	uzdravený
byl	být	k5eAaImAgMnS	být
muž	muž	k1gMnSc1	muž
od	od	k7c2	od
narození	narození	k1gNnPc2	narození
chromý	chromý	k2eAgMnSc1d1	chromý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
chrámových	chrámový	k2eAgInPc6d1	chrámový
schodech	schod	k1gInPc6	schod
prosil	prosit	k5eAaImAgInS	prosit
o	o	k7c4	o
almužnu	almužna	k1gFnSc4	almužna
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
jej	on	k3xPp3gMnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
přikázal	přikázat	k5eAaPmAgMnS	přikázat
mu	on	k3xPp3gMnSc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vstaň	vstát	k5eAaPmRp2nS	vstát
a	a	k8xC	a
choď	chodit	k5eAaImRp2nS	chodit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
byl	být	k5eAaImAgMnS	být
muž	muž	k1gMnSc1	muž
zdráv	zdráv	k2eAgMnSc1d1	zdráv
<g/>
,	,	kIx,	,
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
děkoval	děkovat	k5eAaImAgMnS	děkovat
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
pro	pro	k7c4	pro
Petra	Petr	k1gMnSc4	Petr
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
krok	krok	k1gInSc1	krok
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
autority	autorita	k1gFnSc2	autorita
představitele	představitel	k1gMnSc2	představitel
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Kristově	Kristův	k2eAgFnSc6d1	Kristova
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc6	zmrtvýchvstání
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
ujal	ujmout	k5eAaPmAgInS	ujmout
řízení	řízení	k1gNnSc4	řízení
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
skupiny	skupina	k1gFnPc1	skupina
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Řídil	řídit	k5eAaImAgInS	řídit
volbu	volba	k1gFnSc4	volba
apoštola	apoštol	k1gMnSc2	apoštol
Matěje	Matěj	k1gMnSc2	Matěj
jako	jako	k8xS	jako
náhrady	náhrada	k1gFnSc2	náhrada
za	za	k7c4	za
zrádce	zrádce	k1gMnSc4	zrádce
Jidáše	Jidáš	k1gMnSc4	Jidáš
<g/>
,	,	kIx,	,
vynesl	vynést	k5eAaPmAgInS	vynést
rozsudek	rozsudek	k1gInSc4	rozsudek
nad	nad	k7c7	nad
Ananiášem	Ananiáš	k1gInSc7	Ananiáš
a	a	k8xC	a
Zafirou	Zafira	k1gFnSc7	Zafira
a	a	k8xC	a
zejména	zejména	k9	zejména
otevřel	otevřít	k5eAaPmAgMnS	otevřít
církev	církev	k1gFnSc4	církev
pro	pro	k7c4	pro
příslušníky	příslušník	k1gMnPc4	příslušník
nežidovských	židovský	k2eNgInPc2d1	nežidovský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
pokřtít	pokřtít	k5eAaPmF	pokřtít
Cornelia	Cornelius	k1gMnSc4	Cornelius
bez	bez	k7c2	bez
obřízky	obřízka	k1gFnSc2	obřízka
a	a	k8xC	a
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
církevním	církevní	k2eAgInSc6d1	církevní
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
50	[number]	k4	50
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věřící	věřící	k2eAgMnPc1d1	věřící
nemusejí	muset	k5eNaImIp3nP	muset
dodržovat	dodržovat	k5eAaImF	dodržovat
obřadní	obřadní	k2eAgInPc4d1	obřadní
předpisy	předpis	k1gInPc4	předpis
podle	podle	k7c2	podle
Mojžíšových	Mojžíšových	k2eAgFnPc2d1	Mojžíšových
knih	kniha	k1gFnPc2	kniha
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
judský	judský	k2eAgMnSc1d1	judský
král	král	k1gMnSc1	král
Herodes	Herodes	k1gMnSc1	Herodes
Agrippa	Agripp	k1gMnSc2	Agripp
viděl	vidět	k5eAaImAgInS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
Kristových	Kristův	k2eAgMnPc2d1	Kristův
stoupenců	stoupenec	k1gMnPc2	stoupenec
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
je	být	k5eAaImIp3nS	být
krutě	krutě	k6eAd1	krutě
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Petrovi	Petr	k1gMnSc3	Petr
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
uniknout	uniknout	k5eAaPmF	uniknout
-	-	kIx~	-
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
se	se	k3xPyFc4	se
v	v	k7c6	v
žaláři	žalář	k1gInSc6	žalář
zjevil	zjevit	k5eAaPmAgMnS	zjevit
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
zázračně	zázračně	k6eAd1	zázračně
rozlomil	rozlomit	k5eAaPmAgMnS	rozlomit
Petrovy	Petrův	k2eAgInPc4d1	Petrův
okovy	okov	k1gInPc4	okov
a	a	k8xC	a
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
potom	potom	k6eAd1	potom
navštívil	navštívit	k5eAaPmAgMnS	navštívit
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
apoštolem	apoštol	k1gMnSc7	apoštol
Pavlem	Pavel	k1gMnSc7	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
byl	být	k5eAaImAgMnS	být
římským	římský	k2eAgMnSc7d1	římský
občanem	občan	k1gMnSc7	občan
<g/>
,	,	kIx,	,
kázal	kázat	k5eAaImAgInS	kázat
proto	proto	k8xC	proto
Římanům	Říman	k1gMnPc3	Říman
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
jako	jako	k8xS	jako
Žid	Žid	k1gMnSc1	Žid
zase	zase	k9	zase
Židům	Žid	k1gMnPc3	Žid
a	a	k8xC	a
příslušníkům	příslušník	k1gMnPc3	příslušník
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
neměli	mít	k5eNaImAgMnP	mít
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
snadné	snadný	k2eAgNnSc1d1	snadné
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Římané	Říman	k1gMnPc1	Říman
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
náboženství	náboženství	k1gNnSc1	náboženství
velmi	velmi	k6eAd1	velmi
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
<g/>
.	.	kIx.	.
</s>
<s>
Považovali	považovat	k5eAaImAgMnP	považovat
však	však	k9	však
všechny	všechen	k3xTgMnPc4	všechen
cizince	cizinec	k1gMnPc4	cizinec
a	a	k8xC	a
jinověrce	jinověrec	k1gMnPc4	jinověrec
za	za	k7c4	za
poněkud	poněkud	k6eAd1	poněkud
nedůvěryhodné	důvěryhodný	k2eNgFnPc4d1	nedůvěryhodná
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dalšího	další	k2eAgInSc2d1	další
Petrova	Petrov	k1gInSc2	Petrov
působení	působení	k1gNnSc4	působení
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gInSc2	jeho
skonu	skon	k1gInSc2	skon
jsou	být	k5eAaImIp3nP	být
sporné	sporný	k2eAgInPc1d1	sporný
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
předsedal	předsedat	k5eAaImAgMnS	předsedat
jeruzalémské	jeruzalémský	k2eAgFnSc3d1	Jeruzalémská
obci	obec	k1gFnSc3	obec
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
neověřené	ověřený	k2eNgFnSc2d1	neověřená
tradice	tradice	k1gFnSc2	tradice
raně	raně	k6eAd1	raně
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
románu	román	k1gInSc2	román
Akta	akta	k1gNnPc1	akta
Petrova	Petrov	k1gInSc2	Petrov
měl	mít	k5eAaImAgMnS	mít
Petr	Petr	k1gMnSc1	Petr
později	pozdě	k6eAd2	pozdě
odcestovat	odcestovat	k5eAaPmF	odcestovat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
zajat	zajmout	k5eAaPmNgInS	zajmout
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
ukřižováním	ukřižování	k1gNnSc7	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
64	[number]	k4	64
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Trval	trvat	k5eAaImAgInS	trvat
téměř	téměř	k6eAd1	téměř
týden	týden	k1gInSc4	týden
a	a	k8xC	a
zničil	zničit	k5eAaPmAgMnS	zničit
deset	deset	k4xCc1	deset
ze	z	k7c2	z
čtrnácti	čtrnáct	k4xCc2	čtrnáct
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
<g/>
,	,	kIx,	,
hledal	hledat	k5eAaImAgMnS	hledat
se	se	k3xPyFc4	se
viník	viník	k1gMnSc1	viník
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Nero	Nero	k1gMnSc1	Nero
jej	on	k3xPp3gMnSc4	on
nalezl	nalézt	k5eAaBmAgMnS	nalézt
v	v	k7c6	v
křesťanech	křesťan	k1gMnPc6	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
široké	široký	k2eAgNnSc1d1	široké
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
ukřižováno	ukřižovat	k5eAaPmNgNnS	ukřižovat
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Tibery	Tibera	k1gFnSc2	Tibera
v	v	k7c6	v
Circu	Circus	k1gInSc6	Circus
Gai	Gai	k1gFnSc2	Gai
et	et	k?	et
Neronis	Neronis	k1gFnSc2	Neronis
<g/>
.	.	kIx.	.
</s>
<s>
Věřící	věřící	k1gMnPc1	věřící
prosili	prosit	k5eAaImAgMnP	prosit
Petra	Petr	k1gMnSc4	Petr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
unikl	uniknout	k5eAaPmAgInS	uniknout
pronásledování	pronásledování	k1gNnSc3	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
však	však	k9	však
vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
spatřil	spatřit	k5eAaPmAgMnS	spatřit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
kráčet	kráčet	k5eAaImF	kráčet
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Zeptal	zeptat	k5eAaPmAgMnS	zeptat
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Quo	Quo	k1gFnSc1	Quo
vadis	vadis	k1gFnSc1	vadis
<g/>
,	,	kIx,	,
Domine	Domin	k1gMnSc5	Domin
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kam	kam	k6eAd1	kam
kráčíš	kráčet	k5eAaImIp2nS	kráčet
<g/>
,	,	kIx,	,
Pane	Pan	k1gMnSc5	Pan
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Ježíš	Ježíš	k1gMnSc1	Ježíš
mu	on	k3xPp3gMnSc3	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
ukřižován	ukřižovat	k5eAaPmNgInS	ukřižovat
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
zastyděl	zastydět	k5eAaPmAgMnS	zastydět
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
zajat	zajmout	k5eAaPmNgInS	zajmout
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
ukřižováním	ukřižování	k1gNnPc3	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
hodna	hoden	k2eAgNnPc4d1	hodno
zemřít	zemřít	k5eAaPmF	zemřít
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
Pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gNnSc4	on
ukřižovali	ukřižovat	k5eAaPmAgMnP	ukřižovat
hlavou	hlava	k1gFnSc7	hlava
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
sňali	snít	k5eAaPmAgMnP	snít
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
pochovali	pochovat	k5eAaPmAgMnP	pochovat
u	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
na	na	k7c4	na
Via	via	k7c4	via
Cornelia	Cornelium	k1gNnPc4	Cornelium
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
30	[number]	k4	30
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
tam	tam	k6eAd1	tam
Anaklét	Anaklét	k1gMnSc1	Anaklét
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
malou	malý	k2eAgFnSc4d1	malá
kapli	kaple	k1gFnSc4	kaple
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
poutní	poutní	k2eAgNnSc1d1	poutní
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc4	císař
Konstantina	Konstantin	k1gMnSc4	Konstantin
I.	I.	kA	I.
Velikého	veliký	k2eAgMnSc4d1	veliký
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
Bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
První	první	k4xOgInSc1	první
list	list	k1gInSc1	list
Petrův	Petrův	k2eAgInSc1d1	Petrův
a	a	k8xC	a
Druhý	druhý	k4xOgInSc1	druhý
list	list	k1gInSc1	list
Petrův	Petrův	k2eAgInSc1d1	Petrův
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
listy	list	k1gInPc1	list
komunitám	komunita	k1gFnPc3	komunita
křesťanů	křesťan	k1gMnPc2	křesťan
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
odesilatel	odesilatel	k1gMnSc1	odesilatel
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
jeho	jeho	k3xOp3gNnSc4	jeho
autorství	autorství	k1gNnSc4	autorství
popírají	popírat	k5eAaImIp3nP	popírat
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
dílem	díl	k1gInSc7	díl
jeho	jeho	k3xOp3gMnPc2	jeho
následovníků	následovník	k1gMnPc2	následovník
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
řezníků	řezník	k1gMnPc2	řezník
<g/>
,	,	kIx,	,
sklářů	sklář	k1gMnPc2	sklář
<g/>
,	,	kIx,	,
truhlářů	truhlář	k1gMnPc2	truhlář
<g/>
,	,	kIx,	,
hodinářů	hodinář	k1gMnPc2	hodinář
<g/>
,	,	kIx,	,
zámečníků	zámečník	k1gMnPc2	zámečník
<g/>
,	,	kIx,	,
kovářů	kovář	k1gMnPc2	kovář
<g/>
,	,	kIx,	,
slévačů	slévač	k1gInPc2	slévač
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
hrnčířů	hrnčíř	k1gMnPc2	hrnčíř
<g/>
,	,	kIx,	,
zedníků	zedník	k1gMnPc2	zedník
<g/>
,	,	kIx,	,
cihlářů	cihlář	k1gMnPc2	cihlář
<g/>
,	,	kIx,	,
moštářů	moštář	k1gMnPc2	moštář
<g/>
,	,	kIx,	,
motorkářů	motorkář	k1gMnPc2	motorkář
<g/>
,	,	kIx,	,
kameníků	kameník	k1gMnPc2	kameník
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
síťařů	síťař	k1gMnPc2	síťař
<g/>
,	,	kIx,	,
soukeníků	soukeník	k1gMnPc2	soukeník
<g/>
,	,	kIx,	,
valchařů	valchař	k1gMnPc2	valchař
<g/>
,	,	kIx,	,
rybářů	rybář	k1gMnPc2	rybář
<g/>
,	,	kIx,	,
obchodníků	obchodník	k1gMnPc2	obchodník
s	s	k7c7	s
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
lodníků	lodník	k1gMnPc2	lodník
<g/>
,	,	kIx,	,
trosečníků	trosečník	k1gMnPc2	trosečník
<g/>
,	,	kIx,	,
kajícníků	kajícník	k1gMnPc2	kajícník
<g/>
,	,	kIx,	,
zpovídajících	zpovídající	k2eAgMnPc2d1	zpovídající
se	se	k3xPyFc4	se
a	a	k8xC	a
panen	panna	k1gFnPc2	panna
a	a	k8xC	a
měst	město	k1gNnPc2	město
Chartresu	Chartresa	k1gFnSc4	Chartresa
<g/>
,	,	kIx,	,
Lovani	Lovan	k1gMnPc1	Lovan
<g/>
,	,	kIx,	,
Leidenu	Leiden	k1gInSc2	Leiden
<g/>
,	,	kIx,	,
Peterboroughu	Peterborough	k1gInSc2	Peterborough
<g/>
,	,	kIx,	,
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
Trevíru	Trevír	k1gInSc2	Trevír
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
je	být	k5eAaImIp3nS	být
i	i	k9	i
ochráncem	ochránce	k1gMnSc7	ochránce
proti	proti	k7c3	proti
hadímu	hadí	k2eAgNnSc3d1	hadí
uštknutí	uštknutí	k1gNnSc3	uštknutí
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
krádeži	krádež	k1gFnSc3	krádež
a	a	k8xC	a
pomocníkem	pomocník	k1gMnSc7	pomocník
proti	proti	k7c3	proti
vzteklině	vzteklina	k1gFnSc3	vzteklina
<g/>
,	,	kIx,	,
posedlosti	posedlost	k1gFnSc3	posedlost
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnSc3	bolest
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
záštitou	záštita	k1gFnSc7	záštita
je	být	k5eAaImIp3nS	být
dlouhověkost	dlouhověkost	k1gFnSc1	dlouhověkost
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	Svátek	k1gMnSc1	Svátek
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
občanského	občanský	k2eAgInSc2d1	občanský
i	i	k8xC	i
církevního	církevní	k2eAgInSc2d1	církevní
kalendáře	kalendář	k1gInSc2	kalendář
slaví	slavit	k5eAaImIp3nS	slavit
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
společně	společně	k6eAd1	společně
se	s	k7c7	s
svátkem	svátek	k1gInSc7	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
kalendáři	kalendář	k1gInSc6	kalendář
najdeme	najít	k5eAaPmIp1nP	najít
ještě	ještě	k9	ještě
další	další	k2eAgInPc4d1	další
dny	den	k1gInPc4	den
zasvěcené	zasvěcený	k2eAgInPc4d1	zasvěcený
svatému	svatý	k1gMnSc3	svatý
Petru	Petra	k1gFnSc4	Petra
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
22	[number]	k4	22
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
zkřížené	zkřížený	k2eAgInPc1d1	zkřížený
klíče	klíč	k1gInPc1	klíč
<g/>
,	,	kIx,	,
obrácený	obrácený	k2eAgInSc1d1	obrácený
kříž	kříž	k1gInSc1	kříž
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
ukřižován	ukřižovat	k5eAaPmNgInS	ukřižovat
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
berla	berla	k1gFnSc1	berla
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
příčnými	příčný	k2eAgInPc7d1	příčný
břevny	břevno	k1gNnPc7	břevno
(	(	kIx(	(
<g/>
papežská	papežský	k2eAgFnSc1d1	Papežská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
evangelium	evangelium	k1gNnSc1	evangelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kohout	kohout	k1gMnSc1	kohout
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gNnSc1	jeho
zapření	zapření	k1gNnSc1	zapření
Krista	Kristus	k1gMnSc2	Kristus
<g/>
)	)	kIx)	)
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ryba	ryba	k1gFnSc1	ryba
nebo	nebo	k8xC	nebo
loď	loď	k1gFnSc1	loď
(	(	kIx(	(
<g/>
symboly	symbol	k1gInPc1	symbol
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rybářství	rybářství	k1gNnSc2	rybářství
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
mormonismu	mormonismus	k1gInSc2	mormonismus
byl	být	k5eAaImAgMnS	být
apoštol	apoštol	k1gMnSc1	apoštol
Petr	Petr	k1gMnSc1	Petr
předsedou	předseda	k1gMnSc7	předseda
kvóra	kvórum	k1gNnSc2	kvórum
12	[number]	k4	12
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
držel	držet	k5eAaImAgInS	držet
klíče	klíč	k1gInPc4	klíč
Melchisedechova	Melchisedechův	k2eAgNnSc2d1	Melchisedechovo
kněžství	kněžství	k1gNnSc2	kněžství
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
apoštolem	apoštol	k1gMnSc7	apoštol
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
Jakubem	Jakub	k1gMnSc7	Jakub
zjevil	zjevit	k5eAaPmAgInS	zjevit
proroku	prorok	k1gMnSc3	prorok
Josephu	Joseph	k1gMnSc3	Joseph
Smithovi	Smith	k1gMnSc3	Smith
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
předal	předat	k5eAaPmAgMnS	předat
klíče	klíč	k1gInPc1	klíč
vyššího	vysoký	k2eAgNnSc2d2	vyšší
kněžství	kněžství	k1gNnSc2	kněžství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mormonské	mormonský	k2eAgFnSc2d1	mormonská
víry	víra	k1gFnSc2	víra
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
základ	základ	k1gInSc4	základ
Boží	boží	k2eAgFnSc2d1	boží
pravomoci	pravomoc	k1gFnSc2	pravomoc
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
niž	jenž	k3xRgFnSc4	jenž
mohl	moct	k5eAaImAgMnS	moct
Joseph	Joseph	k1gMnSc1	Joseph
Smith	Smith	k1gMnSc1	Smith
založit	založit	k5eAaPmF	založit
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
znovuzřídit	znovuzřídit	k5eAaPmF	znovuzřídit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mormonského	mormonský	k2eAgInSc2d1	mormonský
chrámového	chrámový	k2eAgInSc2d1	chrámový
obřadu	obřad	k1gInSc2	obřad
Obdarování	obdarování	k1gNnSc2	obdarování
se	se	k3xPyFc4	se
Petr	Petr	k1gMnSc1	Petr
zjevil	zjevit	k5eAaPmAgMnS	zjevit
prvnímu	první	k4xOgMnSc3	první
člověku	člověk	k1gMnSc3	člověk
Adamovi	Adam	k1gMnSc3	Adam
a	a	k8xC	a
předal	předat	k5eAaPmAgInS	předat
mu	on	k3xPp3gMnSc3	on
tokeny	token	k1gInPc1	token
<g/>
,	,	kIx,	,
znamení	znamení	k1gNnSc2	znamení
a	a	k8xC	a
hesla	heslo	k1gNnSc2	heslo
kněžství	kněžství	k1gNnSc2	kněžství
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
Obdarování	obdarování	k1gNnSc4	obdarování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
