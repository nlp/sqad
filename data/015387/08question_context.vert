<s>
Obsazení	obsazení	k1gNnSc1
Říma	Řím	k1gInSc2
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Presa	Pres	k1gMnSc2
di	di	k?
Roma	Rom	k1gMnSc2
<g/>
)	)	kIx)
italskou	italský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1870	#num#	k4
znamenalo	znamenat	k5eAaImAgNnS
zánik	zánik	k1gInSc4
Papežského	papežský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
symbolický	symbolický	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
procesu	proces	k1gInSc2
sjednocení	sjednocení	k1gNnSc2
Itálie	Itálie	k1gFnSc2
(	(	kIx(
<g/>
risorgimenta	risorgimenta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tak	tak	k6eAd1
získala	získat	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
tradiční	tradiční	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
</s>