<s>
Steyr	Steyr	k1gInSc1
SK	Sk	kA
105	#num#	k4
Kürassier	Kürassiero	k1gNnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Steyr	Steyr	k1gInSc1
SK	Sk	kA
105	#num#	k4
Kürassier	Kürassier	k1gInSc1
Steyr	Steyr	k1gInSc4
SK	Sk	kA
105	#num#	k4
Kürassier	Kürassier	k1gInSc4
Bolivijské	bolivijský	k2eAgFnSc2d1
armádyTyp	armádyTypit	k5eAaPmRp2nS
vozidla	vozidlo	k1gNnPc1
</s>
<s>
Lehký	lehký	k2eAgInSc1d1
tank	tank	k1gInSc1
<g/>
,	,	kIx,
stíhač	stíhač	k1gMnSc1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
průzkumné	průzkumný	k2eAgNnSc1d1
vozidlo	vozidlo	k1gNnSc1
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc4
Základní	základní	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
Posádka	posádka	k1gFnSc1
</s>
<s>
3	#num#	k4
Délka	délka	k1gFnSc1
</s>
<s>
5,58	5,58	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
2,5	2,5	k4
m	m	kA
Výška	výška	k1gFnSc1
</s>
<s>
2,52	2,52	k4
m	m	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
17,5	17,5	k4
t	t	k?
Pancéřování	pancéřování	k1gNnSc4
a	a	k8xC
výzbroj	výzbroj	k1gFnSc4
Pancéřování	pancéřování	k1gNnSc2
</s>
<s>
40	#num#	k4
<g/>
mm	mm	kA
Hlavní	hlavní	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
105	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
Sekundární	sekundární	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
7,62	7,62	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
Pohon	pohon	k1gInSc1
a	a	k8xC
pohyb	pohyb	k1gInSc1
Pohon	pohon	k1gInSc1
</s>
<s>
Steyr	Steyr	k1gInSc1
7FA	7FA	k4
-	-	kIx~
6	#num#	k4
<g/>
válcový	válcový	k2eAgInSc1d1
<g/>
320	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
238	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
Odpružení	odpružení	k1gNnSc1
</s>
<s>
torzní	torzní	k2eAgFnPc1d1
tyče	tyč	k1gFnPc1
Max	max	kA
<g/>
.	.	kIx.
rychlost	rychlost	k1gFnSc1
</s>
<s>
65	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
Poměr	poměr	k1gInSc1
výkon	výkon	k1gInSc1
<g/>
/	/	kIx~
<g/>
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
13,4	13,4	k4
kW	kW	kA
<g/>
/	/	kIx~
<g/>
t	t	k?
Dojezd	dojezd	k1gInSc1
</s>
<s>
500	#num#	k4
km	km	kA
</s>
<s>
Steyr	Steyr	k1gInSc1
SK	Sk	kA
105	#num#	k4
Kürassier	Kürassier	k1gInPc1
je	být	k5eAaImIp3nS
rakouský	rakouský	k2eAgInSc1d1
lehký	lehký	k2eAgInSc1d1
tank	tank	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
spíše	spíše	k9
používaný	používaný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
stíhač	stíhač	k1gMnSc1
tanků	tank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc4
podobná	podobný	k2eAgFnSc1d1
AMX-13	AMX-13	k1gFnSc1
má	mít	k5eAaImIp3nS
pancíř	pancíř	k1gInSc4
silný	silný	k2eAgInSc4d1
až	až	k6eAd1
40	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kanónu	kanón	k1gInSc2
ráže	ráže	k1gFnSc2
105	#num#	k4
milimetrů	milimetr	k1gInPc2
se	se	k3xPyFc4
střílí	střílet	k5eAaImIp3nS
střelami	střela	k1gFnPc7
HEAT	HEAT	kA
nebo	nebo	k8xC
APFSDS	APFSDS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
měla	mít	k5eAaImAgFnS
asi	asi	k9
150	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
vyřazují	vyřazovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Zbraně	zbraň	k1gFnPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
600	#num#	k4
nejznámějších	známý	k2eAgFnPc2d3
zbraní	zbraň	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7181	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Steyr	Steyra	k1gFnPc2
SK	Sk	kA
105	#num#	k4
Kürassier	Kürassira	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
