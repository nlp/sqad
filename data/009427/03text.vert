<p>
<s>
Říční	říční	k2eAgFnSc1d1	říční
delta	delta	k1gFnSc1	delta
Leny	Lena	k1gFnSc2	Lena
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
Sacha	Sach	k1gMnSc2	Sach
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
zhruba	zhruba	k6eAd1	zhruba
61	[number]	k4	61
tisíc	tisíc	k4xCgInPc2	tisíc
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
samotná	samotný	k2eAgFnSc1d1	samotná
delta	delta	k1gFnSc1	delta
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
)	)	kIx)	)
přiléhající	přiléhající	k2eAgInSc1d1	přiléhající
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
Laptěvů	Laptěv	k1gInPc2	Laptěv
patřícímu	patřící	k2eAgInSc3d1	patřící
do	do	k7c2	do
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
deltu	delta	k1gFnSc4	delta
na	na	k7c6	na
světě	svět	k1gInSc6	svět
po	po	k7c6	po
deltě	delta	k1gFnSc6	delta
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
deltu	delta	k1gFnSc4	delta
s	s	k7c7	s
trvale	trvale	k6eAd1	trvale
zamrzlou	zamrzlý	k2eAgFnSc7d1	zamrzlá
spodní	spodní	k2eAgFnSc7d1	spodní
vrstvou	vrstva	k1gFnSc7	vrstva
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tu	ten	k3xDgFnSc4	ten
do	do	k7c2	do
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
pravidelně	pravidelně	k6eAd1	pravidelně
smývána	smýván	k2eAgNnPc4d1	smýván
obrovská	obrovský	k2eAgNnPc4d1	obrovské
kvanta	kvantum	k1gNnPc4	kvantum
jílu	jíl	k1gInSc2	jíl
a	a	k8xC	a
bahna	bahno	k1gNnSc2	bahno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
delta	delta	k1gFnSc1	delta
stále	stále	k6eAd1	stále
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lena	Lena	k1gFnSc1	Lena
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
využívána	využívat	k5eAaImNgNnP	využívat
pro	pro	k7c4	pro
trvalou	trvalý	k2eAgFnSc4d1	trvalá
dopravu	doprava	k1gFnSc4	doprava
ani	ani	k8xC	ani
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
zde	zde	k6eAd1	zde
sídlící	sídlící	k2eAgMnPc4d1	sídlící
ptáky	pták	k1gMnPc4	pták
patří	patřit	k5eAaImIp3nP	patřit
labutě	labuť	k1gFnPc1	labuť
<g/>
,	,	kIx,	,
potáplice	potáplice	k1gFnPc1	potáplice
<g/>
,	,	kIx,	,
bahňáci	bahňák	k1gMnPc1	bahňák
a	a	k8xC	a
rackovití	rackovitý	k2eAgMnPc1d1	rackovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
Tiksi	Tikse	k1gFnSc4	Tikse
<g/>
.	.	kIx.	.
</s>
</p>
