<s>
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1930	[number]	k4	1930
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejčastějším	častý	k2eAgMnSc7d3	nejčastější
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Miloš	Miloš	k1gMnSc1	Miloš
Macourek	Macourek	k1gMnSc1	Macourek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
studoval	studovat	k5eAaImAgMnS	studovat
režii	režie	k1gFnSc4	režie
na	na	k7c4	na
FAMU	FAMU	kA	FAMU
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
absolventským	absolventský	k2eAgInSc7d1	absolventský
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Direktiva	direktiva	k1gFnSc1	direktiva
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc1	režie
do	do	k7c2	do
Filmového	filmový	k2eAgNnSc2d1	filmové
studia	studio	k1gNnSc2	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
pomocným	pomocný	k2eAgMnSc7d1	pomocný
režisérem	režisér	k1gMnSc7	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
samostatným	samostatný	k2eAgInSc7d1	samostatný
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
dětský	dětský	k2eAgInSc1d1	dětský
barevný	barevný	k2eAgInSc1d1	barevný
snímek	snímek	k1gInSc1	snímek
Případ	případ	k1gInSc1	případ
Lupínek	lupínek	k1gInSc1	lupínek
<g/>
.	.	kIx.	.
</s>
<s>
Bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
Roztržka	roztržka	k1gFnSc1	roztržka
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
Touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
Ořechovce	ořechovka	k1gFnSc6	ořechovka
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
Neděle	neděle	k1gFnSc1	neděle
ve	v	k7c4	v
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
Poslední	poslední	k2eAgInSc1d1	poslední
mejdan	mejdan	k1gInSc1	mejdan
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
–	–	k?	–
supervize	supervize	k1gFnSc2	supervize
Atrakce	atrakce	k1gFnSc2	atrakce
švédského	švédský	k2eAgInSc2d1	švédský
zájezdu	zájezd	k1gInSc2	zájezd
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
–	–	k?	–
umělecký	umělecký	k2eAgInSc4d1	umělecký
dohled	dohled	k1gInSc4	dohled
Muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
–	–	k?	–
(	(	kIx(	(
<g/>
FAMU	FAMU	kA	FAMU
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
)	)	kIx)	)
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Direktiva	direktiva	k1gFnSc1	direktiva
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
–	–	k?	–
(	(	kIx(	(
<g/>
FAMU	FAMU	kA	FAMU
<g/>
)	)	kIx)	)
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Případ	případ	k1gInSc1	případ
Lupínek	lupínek	k1gInSc1	lupínek
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
technický	technický	k2eAgInSc1d1	technický
scénář	scénář	k1gInSc1	scénář
+	+	kIx~	+
námět	námět	k1gInSc1	námět
Kuřata	kuře	k1gNnPc1	kuře
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Kdo	kdo	k3yInSc1	kdo
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
Jessii	Jessie	k1gFnSc4	Jessie
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
+	+	kIx~	+
námět	námět	k1gInSc1	námět
Konec	konec	k1gInSc1	konec
agenta	agent	k1gMnSc2	agent
W4C	W4C	k1gMnSc2	W4C
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
psa	pes	k1gMnSc2	pes
pana	pan	k1gMnSc2	pan
Foustky	Foustka	k1gFnSc2	Foustka
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
vdova	vdova	k1gFnSc1	vdova
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
+	+	kIx~	+
scénář	scénář	k1gInSc4	scénář
+	+	kIx~	+
námět	námět	k1gInSc4	námět
Dívka	dívka	k1gFnSc1	dívka
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
+	+	kIx~	+
námět	námět	k1gInSc1	námět
Smrt	smrt	k1gFnSc1	smrt
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
Tři	tři	k4xCgMnPc4	tři
oříšky	oříšek	k1gMnPc4	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Jak	jak	k6eAd1	jak
utopit	utopit	k5eAaPmF	utopit
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mráčka	Mráček	k1gMnSc2	Mráček
aneb	aneb	k?	aneb
Konec	konec	k1gInSc1	konec
vodníků	vodník	k1gMnPc2	vodník
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
<g />
.	.	kIx.	.
</s>
<s>
hlásí	hlásit	k5eAaImIp3nS	hlásit
příchod	příchod	k1gInSc1	příchod
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
+	+	kIx~	+
scénář	scénář	k1gInSc4	scénář
+	+	kIx~	+
námět	námět	k1gInSc4	námět
Bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Což	což	k3yQnSc4	což
takhle	takhle	k6eAd1	takhle
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
špenát	špenát	k1gInSc4	špenát
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nS	budit
princezny	princezna	k1gFnSc2	princezna
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Princ	princa	k1gFnPc2	princa
a	a	k8xC	a
Večernice	večernice	k1gFnSc2	večernice
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Arabela	Arabel	k1gMnSc2	Arabel
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Zralé	zralý	k2eAgNnSc1d1	zralé
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Zelená	zelený	k2eAgFnSc1d1	zelená
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Létající	létající	k2eAgMnPc1d1	létající
Čestmír	Čestmír	k1gMnSc1	Čestmír
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Rumburak	Rumburak	k1gInSc1	Rumburak
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Já	já	k3xPp1nSc1	já
nejsem	být	k5eNaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
+	+	kIx~	+
scénář	scénář	k1gInSc4	scénář
Mladé	mladá	k1gFnSc2	mladá
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Dědečkův	dědečkův	k2eAgInSc1d1	dědečkův
odkaz	odkaz	k1gInSc1	odkaz
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Křeček	křeček	k1gMnSc1	křeček
v	v	k7c6	v
noční	noční	k2eAgFnSc6d1	noční
košili	košile	k1gFnSc6	košile
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Arabela	Arabela	k1gFnSc1	Arabela
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
aneb	aneb	k?	aneb
Rumburak	Rumburak	k1gInSc1	Rumburak
králem	král	k1gMnSc7	král
Říše	říš	k1gFnSc2	říš
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
měšec	měšec	k1gInSc1	měšec
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
+	+	kIx~	+
spolupráce	spolupráce	k1gFnSc2	spolupráce
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
Pták	pták	k1gMnSc1	pták
Ohnivák	Ohnivák	k1gMnSc1	Ohnivák
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Jezerní	jezerní	k2eAgFnSc1d1	jezerní
královna	královna	k1gFnSc1	královna
1997	[number]	k4	1997
Král	Král	k1gMnSc1	Král
sokolů	sokol	k1gMnPc2	sokol
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Mach	macha	k1gFnPc2	macha
<g/>
,	,	kIx,	,
Šebestová	Šebestová	k1gFnSc1	Šebestová
a	a	k8xC	a
kouzelné	kouzelný	k2eAgNnSc1d1	kouzelné
sluchátko	sluchátko	k1gNnSc1	sluchátko
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
On	on	k3xPp3gInSc1	on
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Saxána	Saxán	k2eAgFnSc1d1	Saxán
a	a	k8xC	a
Lexikon	lexikon	k1gInSc1	lexikon
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
+	+	kIx~	+
scénář	scénář	k1gInSc1	scénář
Na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
stopě	stopa	k1gFnSc6	stopa
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
–	–	k?	–
skaut	skaut	k1gMnSc1	skaut
Bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
–	–	k?	–
dělník	dělník	k1gMnSc1	dělník
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
smích	smích	k1gInSc1	smích
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
Neviditelní	viditelný	k2eNgMnPc1d1	Neviditelný
–	–	k?	–
člen	člen	k1gInSc4	člen
vodní	vodní	k2eAgFnSc2d1	vodní
rady	rada	k1gFnSc2	rada
Mach	macha	k1gFnPc2	macha
<g/>
,	,	kIx,	,
Šebestová	Šebestová	k1gFnSc1	Šebestová
a	a	k8xC	a
kouzelné	kouzelný	k2eAgNnSc1d1	kouzelné
sluchátko	sluchátko	k1gNnSc1	sluchátko
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
–	–	k?	–
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
1976	[number]	k4	1976
–	–	k?	–
Státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
za	za	k7c4	za
film	film	k1gInSc4	film
Bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
víno	víno	k1gNnSc1	víno
2017	[number]	k4	2017
–	–	k?	–
Cena	cena	k1gFnSc1	cena
prezidenta	prezident	k1gMnSc2	prezident
MFF	MFF	kA	MFF
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
české	český	k2eAgFnSc3d1	Česká
kinematografii	kinematografie	k1gFnSc3	kinematografie
<g/>
.	.	kIx.	.
</s>
