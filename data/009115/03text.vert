<p>
<s>
Lope	Lope	k1gNnSc1	Lope
de	de	k?	de
Vega	Veg	k1gInSc2	Veg
(	(	kIx(	(
<g/>
též	též	k9	též
Félix	Félix	k1gInSc1	Félix
Lope	Lop	k1gFnSc2	Lop
de	de	k?	de
Vega	Vega	k1gMnSc1	Vega
Carpio	Carpio	k1gMnSc1	Carpio
či	či	k8xC	či
Lope	Lope	k1gFnSc1	Lope
Félix	Félix	k1gInSc1	Félix
de	de	k?	de
Vega	Vega	k1gMnSc1	Vega
Carpio	Carpio	k1gMnSc1	Carpio
<g/>
;	;	kIx,	;
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1562	[number]	k4	1562
Madrid	Madrid	k1gInSc1	Madrid
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1635	[number]	k4	1635
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisováno	připisován	k2eAgNnSc1d1	připisováno
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
425	[number]	k4	425
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnPc1	jeho
historická	historický	k2eAgNnPc1d1	historické
dramata	drama	k1gNnPc1	drama
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
složité	složitý	k2eAgInPc4d1	složitý
zápletky	zápletek	k1gInPc4	zápletek
a	a	k8xC	a
rušný	rušný	k2eAgInSc4d1	rušný
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
s	s	k7c7	s
častým	častý	k2eAgInSc7d1	častý
motivem	motiv	k1gInSc7	motiv
cti	čest	k1gFnSc2	čest
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
dramata	drama	k1gNnPc4	drama
"	"	kIx"	"
<g/>
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
kordu	kord	k1gInSc2	kord
<g/>
"	"	kIx"	"
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
nového	nový	k2eAgNnSc2d1	nové
dramatu	drama	k1gNnSc2	drama
jménem	jméno	k1gNnSc7	jméno
španělská	španělský	k2eAgFnSc1d1	španělská
komedie	komedie	k1gFnPc1	komedie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
veršované	veršovaný	k2eAgNnSc1d1	veršované
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nS	mísit
tragické	tragický	k2eAgInPc4d1	tragický
a	a	k8xC	a
komické	komický	k2eAgInPc4d1	komický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
rozdělené	rozdělený	k2eAgFnSc2d1	rozdělená
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgInPc4d1	životní
osudy	osud	k1gInPc4	osud
==	==	k?	==
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
jezuitské	jezuitský	k2eAgFnSc6d1	jezuitská
koleji	kolej	k1gFnSc6	kolej
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
domova	domov	k1gInSc2	domov
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
ávilského	ávilský	k2eAgMnSc2d1	ávilský
biskupa	biskup	k1gMnSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
napsal	napsat	k5eAaBmAgMnS	napsat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgInS	projít
i	i	k9	i
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
invaze	invaze	k1gFnPc4	invaze
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
<g/>
.	.	kIx.	.
</s>
<s>
Prožíval	prožívat	k5eAaImAgMnS	prožívat
mnoho	mnoho	k4c4	mnoho
milostných	milostný	k2eAgInPc2d1	milostný
vztahů	vztah	k1gInPc2	vztah
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
knězem	kněz	k1gMnSc7	kněz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc7d1	hlavní
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
královský	královský	k2eAgMnSc1d1	královský
komorník	komorník	k1gMnSc1	komorník
unesl	unést	k5eAaPmAgMnS	unést
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Lope	Lope	k1gNnPc7	Lope
de	de	k?	de
Vega	Vega	k1gMnSc1	Vega
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnPc2	jeho
rakví	rakev	k1gFnPc2	rakev
šel	jít	k5eAaImAgInS	jít
celý	celý	k2eAgInSc1d1	celý
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Lopemu	Lopemat	k5eAaPmIp1nS	Lopemat
de	de	k?	de
Vegovi	Vega	k1gMnSc6	Vega
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
asi	asi	k9	asi
470	[number]	k4	470
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
autorství	autorství	k1gNnSc1	autorství
je	být	k5eAaImIp3nS	být
však	však	k9	však
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
jen	jen	k6eAd1	jen
u	u	k7c2	u
314	[number]	k4	314
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
náboženské	náboženský	k2eAgFnPc4d1	náboženská
hry	hra	k1gFnPc4	hra
(	(	kIx(	(
<g/>
Narození	narození	k1gNnSc4	narození
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
<g/>
,	,	kIx,	,
Stvoření	stvoření	k1gNnSc4	stvoření
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pastýřské	pastýřský	k2eAgFnSc2d1	pastýřská
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
Amor	Amor	k1gMnSc1	Amor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
komedie	komedie	k1gFnSc1	komedie
s	s	k7c7	s
náměty	námět	k1gInPc7	námět
antické	antický	k2eAgFnSc2d1	antická
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
Vypálený	vypálený	k2eAgInSc1d1	vypálený
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Velikost	velikost	k1gFnSc1	velikost
Alexandrova	Alexandrův	k2eAgFnSc1d1	Alexandrova
<g/>
,	,	kIx,	,
Císařská	císařský	k2eAgFnSc1d1	císařská
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
Koruna	koruna	k1gFnSc1	koruna
<g />
.	.	kIx.	.
</s>
<s>
Otakarova	Otakarův	k2eAgFnSc1d1	Otakarova
<g/>
,	,	kIx,	,
Hvězda	hvězda	k1gFnSc1	hvězda
sevillská	sevillský	k2eAgFnSc1d1	sevillská
<g/>
,	,	kIx,	,
Ovčí	ovčí	k2eAgInSc1d1	ovčí
pramen	pramen	k1gInSc1	pramen
<g/>
,	,	kIx,	,
Velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
moskevský	moskevský	k2eAgMnSc1d1	moskevský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mravoličné	mravoličný	k2eAgFnSc2d1	mravoličná
komedie	komedie	k1gFnSc2	komedie
(	(	kIx(	(
<g/>
Zahradníkův	Zahradníkův	k2eAgMnSc1d1	Zahradníkův
Pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Chytrá	chytrý	k2eAgFnSc1d1	chytrá
milenka	milenka	k1gFnSc1	milenka
<g/>
,	,	kIx,	,
Nevšímavost	nevšímavost	k1gFnSc1	nevšímavost
dělá	dělat	k5eAaImIp3nS	dělat
divy	div	k1gInPc4	div
<g/>
,	,	kIx,	,
Učitel	učitel	k1gMnSc1	učitel
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
Vzbouření	vzbouření	k1gNnSc1	vzbouření
v	v	k7c6	v
blázinci	blázinec	k1gInSc6	blázinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
dýky	dýka	k1gFnSc2	dýka
(	(	kIx(	(
<g/>
Rytíř	Rytíř	k1gMnSc1	Rytíř
Olmeda	Olmeda	k1gMnSc1	Olmeda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pastýřské	pastýřský	k2eAgInPc1d1	pastýřský
romány	román	k1gInPc1	román
(	(	kIx(	(
<g/>
Arcadia	Arcadium	k1gNnPc1	Arcadium
<g/>
,	,	kIx,	,
Betlemští	Betlemský	k2eAgMnPc1d1	Betlemský
pastýři	pastýř	k1gMnPc1	pastýř
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Dorotea	Dorote	k1gInSc2	Dorote
<g/>
)	)	kIx)	)
a	a	k8xC	a
sonety	sonet	k1gInPc1	sonet
(	(	kIx(	(
<g/>
Sonet	sonet	k1gInSc1	sonet
na	na	k7c4	na
sonet	sonet	k1gInSc4	sonet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lope	Lope	k6eAd1	Lope
de	de	k?	de
Vega	Vega	k1gMnSc1	Vega
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
tvůrce	tvůrce	k1gMnSc4	tvůrce
tzv.	tzv.	kA	tzv.
komedie	komedie	k1gFnSc2	komedie
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
dýky	dýka	k1gFnSc2	dýka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
čerpá	čerpat	k5eAaImIp3nS	čerpat
náměty	námět	k1gInPc4	námět
ze	z	k7c2	z
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
<g/>
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc1	čest
<g/>
,	,	kIx,	,
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
.	.	kIx.	.
</s>
<s>
Nezajímá	zajímat	k5eNaImIp3nS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
okolnosti	okolnost	k1gFnPc4	okolnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
zápletku	zápletka	k1gFnSc4	zápletka
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
autor	autor	k1gMnSc1	autor
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
společenskou	společenský	k2eAgFnSc4d1	společenská
morálku	morálka	k1gFnSc4	morálka
<g/>
,	,	kIx,	,
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
nerovné	rovný	k2eNgNnSc4d1	nerovné
postavení	postavení	k1gNnSc4	postavení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
této	tento	k3xDgFnSc2	tento
komedie	komedie	k1gFnSc2	komedie
je	být	k5eAaImIp3nS	být
Rytíř	Rytíř	k1gMnSc1	Rytíř
z	z	k7c2	z
Olmeda	Olmed	k1gMnSc2	Olmed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovčí	ovčí	k2eAgInSc1d1	ovčí
Pramen	pramen	k1gInSc1	pramen
(	(	kIx(	(
<g/>
Fuenteovejuna	Fuenteovejuna	k1gFnSc1	Fuenteovejuna
[	[	kIx(	[
<g/>
fwente	fwent	k1gInSc5	fwent
o.	o.	k?	o.
<g/>
β	β	k?	β
<g/>
.	.	kIx.	.
<g/>
xuna	xuna	k1gMnSc1	xuna
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
–	–	k?	–
Lope	Lope	k1gInSc1	Lope
de	de	k?	de
Vega	Vega	k1gFnSc1	Vega
čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
skutečných	skutečný	k2eAgFnPc2d1	skutečná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
v	v	k7c6	v
r.	r.	kA	r.
1476	[number]	k4	1476
–	–	k?	–
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
sjednocení	sjednocení	k1gNnSc6	sjednocení
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tříaktovou	tříaktový	k2eAgFnSc4d1	tříaktová
veršovanou	veršovaný	k2eAgFnSc4d1	veršovaná
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
jak	jak	k8xS	jak
tragickými	tragický	k2eAgFnPc7d1	tragická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
komickými	komický	k2eAgFnPc7d1	komická
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
španělská	španělský	k2eAgFnSc1d1	španělská
komedie	komedie	k1gFnSc1	komedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
svobodné	svobodný	k2eAgFnSc2d1	svobodná
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vzbouří	vzbouřit	k5eAaPmIp3nS	vzbouřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
komtur	komtur	k1gMnSc1	komtur
Goméz	Goméza	k1gFnPc2	Goméza
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
rytířského	rytířský	k2eAgInSc2d1	rytířský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
neuznává	uznávat	k5eNaImIp3nS	uznávat
svobodná	svobodný	k2eAgNnPc4d1	svobodné
práva	právo	k1gNnPc4	právo
vesničanů	vesničan	k1gMnPc2	vesničan
<g/>
,	,	kIx,	,
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
její	její	k3xOp3gMnPc4	její
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
znásilňuje	znásilňovat	k5eAaImIp3nS	znásilňovat
ženy	žena	k1gFnPc4	žena
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
vyšle	vyslat	k5eAaPmIp3nS	vyslat
do	do	k7c2	do
vsi	ves	k1gFnSc2	ves
soudce	soudce	k1gMnSc2	soudce
dona	don	k1gMnSc2	don
Rodriga	Rodrig	k1gMnSc2	Rodrig
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vraha	vrah	k1gMnSc4	vrah
potrestal	potrestat	k5eAaPmAgInS	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
soud	soud	k1gInSc4	soud
připraví	připravit	k5eAaPmIp3nS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
vrahem	vrah	k1gMnSc7	vrah
<g/>
,	,	kIx,	,
zazní	zaznět	k5eAaImIp3nS	zaznět
společná	společný	k2eAgFnSc1d1	společná
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Fuente	Fuent	k1gInSc5	Fuent
Ovejuna	Ovejuna	k1gFnSc1	Ovejuna
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Autor	autor	k1gMnSc1	autor
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
kolektivitu	kolektivita	k1gFnSc4	kolektivita
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
morálku	morálka	k1gFnSc4	morálka
vesničanů	vesničan	k1gMnPc2	vesničan
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
důstojnost	důstojnost	k1gFnSc4	důstojnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednota	jednota	k1gFnSc1	jednota
vesničanů	vesničan	k1gMnPc2	vesničan
tak	tak	k6eAd1	tak
zmaří	zmařit	k5eAaPmIp3nS	zmařit
vykonání	vykonání	k1gNnSc4	vykonání
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Dorotea	Dorote	k1gInSc2	Dorote
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejcennějším	cenný	k2eAgInSc7d3	nejcennější
románem	román	k1gInSc7	román
Lope	Lop	k1gMnSc2	Lop
de	de	k?	de
Vegy	Vega	k1gMnSc2	Vega
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
zrcadlí	zrcadlit	k5eAaImIp3nP	zrcadlit
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Vegovy	Vegův	k2eAgInPc1d1	Vegův
skandály	skandál	k1gInPc1	skandál
s	s	k7c7	s
Elenou	Elena	k1gFnSc7	Elena
Osoriovou	Osoriový	k2eAgFnSc7d1	Osoriový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lope	Lop	k1gMnSc2	Lop
de	de	k?	de
Vega	Vegus	k1gMnSc2	Vegus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Lope	Lope	k1gFnSc1	Lope
de	de	k?	de
Vega	Vega	k1gFnSc1	Vega
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Lope	Lope	k1gNnSc2	Lope
de	de	k?	de
Vega	Vegum	k1gNnSc2	Vegum
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
