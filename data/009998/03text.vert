<p>
<s>
Jako	jako	k9	jako
ovoce	ovoce	k1gNnPc1	ovoce
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
zpravidla	zpravidla	k6eAd1	zpravidla
sladké	sladký	k2eAgInPc1d1	sladký
jedlé	jedlý	k2eAgInPc1d1	jedlý
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
plodenství	plodenství	k1gNnPc1	plodenství
nebo	nebo	k8xC	nebo
semena	semeno	k1gNnPc1	semeno
převážně	převážně	k6eAd1	převážně
víceletých	víceletý	k2eAgFnPc2d1	víceletá
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
biologického	biologický	k2eAgNnSc2d1	biologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
ovocným	ovocný	k2eAgInSc7d1	ovocný
druhem	druh	k1gInSc7	druh
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
<g/>
)	)	kIx)	)
jedlá	jedlý	k2eAgFnSc1d1	jedlá
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
plodem	plod	k1gInSc7	plod
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
sladkých	sladký	k2eAgInPc2d1	sladký
dužinatých	dužinatý	k2eAgInPc2d1	dužinatý
plodů	plod	k1gInPc2	plod
užitkových	užitkový	k2eAgInPc2d1	užitkový
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lesního	lesní	k2eAgNnSc2d1	lesní
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
posuzováním	posuzování	k1gNnSc7	posuzování
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
odrůd	odrůda	k1gFnPc2	odrůda
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pomologie	pomologie	k1gFnSc1	pomologie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
starořímské	starořímský	k2eAgFnSc2d1	starořímská
bohyně	bohyně	k1gFnSc2	bohyně
ovoce	ovoce	k1gNnSc2	ovoce
či	či	k8xC	či
ovocnářství	ovocnářství	k1gNnSc2	ovocnářství
Pomony	Pomona	k1gFnSc2	Pomona
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
chápané	chápaný	k2eAgNnSc1d1	chápané
jako	jako	k8xS	jako
personifikace	personifikace	k1gFnSc1	personifikace
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
ovoce	ovoce	k1gNnSc2	ovoce
jsou	být	k5eAaImIp3nP	být
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
je	být	k5eAaImIp3nS	být
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
je	být	k5eAaImIp3nS	být
nutričními	nutriční	k2eAgMnPc7d1	nutriční
poradci	poradce	k1gMnPc7	poradce
často	často	k6eAd1	často
doporučováno	doporučovat	k5eAaImNgNnS	doporučovat
jako	jako	k8xC	jako
bohatý	bohatý	k2eAgInSc1d1	bohatý
zdroj	zdroj	k1gInSc1	zdroj
vitamínů	vitamín	k1gInPc2	vitamín
i	i	k8xC	i
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
korelace	korelace	k1gFnSc1	korelace
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
údajně	údajně	k6eAd1	údajně
i	i	k8xC	i
například	například	k6eAd1	například
v	v	k7c6	v
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Prehistorická	prehistorický	k2eAgFnSc1d1	prehistorická
evoluce	evoluce	k1gFnSc1	evoluce
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
trávicí	trávicí	k2eAgInSc1d1	trávicí
systém	systém	k1gInSc1	systém
ptáků	pták	k1gMnPc2	pták
většinu	většina	k1gFnSc4	většina
semen	semeno	k1gNnPc2	semeno
rozloží	rozložit	k5eAaPmIp3nP	rozložit
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
konzumace	konzumace	k1gFnSc1	konzumace
plodů	plod	k1gInPc2	plod
drobnými	drobný	k2eAgInPc7d1	drobný
ptáky	pták	k1gMnPc4	pták
nápomocná	nápomocný	k2eAgFnSc1d1	nápomocná
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
housenkám	housenka	k1gFnPc3	housenka
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
častá	častý	k2eAgFnSc1d1	častá
produkce	produkce	k1gFnSc1	produkce
toxinů	toxin	k1gInPc2	toxin
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
semínko	semínko	k1gNnSc1	semínko
např.	např.	kA	např.
ptákům	pták	k1gMnPc3	pták
při	při	k7c6	při
přeletu	přelet	k1gInSc6	přelet
ze	z	k7c2	z
zobáku	zobák	k1gInSc2	zobák
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
nebo	nebo	k8xC	nebo
přecijen	přecijit	k5eAaPmNgInS	přecijit
odolá	odolat	k5eAaPmIp3nS	odolat
metabolickým	metabolický	k2eAgInPc3d1	metabolický
procesům	proces	k1gInPc3	proces
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
pomůže	pomoct	k5eAaPmIp3nS	pomoct
rozšíření	rozšíření	k1gNnSc1	rozšíření
semen	semeno	k1gNnPc2	semeno
stromu	strom	k1gInSc2	strom
-	-	kIx~	-
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
lákavých	lákavý	k2eAgInPc2d1	lákavý
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
hrál	hrát	k5eAaImAgInS	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
primární	primární	k2eAgFnSc6d1	primární
evoluci	evoluce	k1gFnSc6	evoluce
ovocných	ovocný	k2eAgInPc2d1	ovocný
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
vzniku	vznik	k1gInSc6	vznik
sladkých	sladký	k2eAgFnPc2d1	sladká
(	(	kIx(	(
<g/>
kaloricky	kaloricky	k6eAd1	kaloricky
bohatých	bohatý	k2eAgInPc2d1	bohatý
<g/>
)	)	kIx)	)
lesních	lesní	k2eAgInPc2d1	lesní
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
pěstování	pěstování	k1gNnSc1	pěstování
===	===	k?	===
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
ovocných	ovocný	k2eAgInPc2d1	ovocný
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
ze	z	k7c2	z
zvyklostí	zvyklost	k1gFnPc2	zvyklost
lovců	lovec	k1gMnPc2	lovec
a	a	k8xC	a
sběračů	sběrač	k1gMnPc2	sběrač
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc4	první
záměrnou	záměrný	k2eAgFnSc4d1	záměrná
selekci	selekce	k1gFnSc4	selekce
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
druhů	druh	k1gInPc2	druh
můžeme	moct	k5eAaImIp1nP	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
v	v	k7c6	v
perském	perský	k2eAgInSc6d1	perský
listu	list	k1gInSc6	list
India	indium	k1gNnSc2	indium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
ovoce	ovoce	k1gNnSc2	ovoce
následně	následně	k6eAd1	následně
docházelo	docházet	k5eAaImAgNnS	docházet
vlivem	vlivem	k7c2	vlivem
lidského	lidský	k2eAgInSc2d1	lidský
umělého	umělý	k2eAgInSc2d1	umělý
výběru	výběr	k1gInSc2	výběr
ke	k	k7c3	k
zvětšování	zvětšování	k1gNnSc3	zvětšování
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
zmenšování	zmenšování	k1gNnSc4	zmenšování
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
křížení	křížení	k1gNnSc2	křížení
také	také	k9	také
byly	být	k5eAaImAgInP	být
šlechtěny	šlechtěn	k2eAgInPc1d1	šlechtěn
nové	nový	k2eAgInPc1d1	nový
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
můžeme	moct	k5eAaImIp1nP	moct
dnes	dnes	k6eAd1	dnes
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
rozdílu	rozdíl	k1gInSc6	rozdíl
mezi	mezi	k7c7	mezi
planými	planý	k2eAgInPc7d1	planý
a	a	k8xC	a
kulturními	kulturní	k2eAgInPc7d1	kulturní
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalovaly	zdokonalovat	k5eAaImAgInP	zdokonalovat
se	se	k3xPyFc4	se
i	i	k9	i
samotné	samotný	k2eAgFnPc1d1	samotná
techniky	technika	k1gFnPc1	technika
pěstování	pěstování	k1gNnSc2	pěstování
od	od	k7c2	od
speciálního	speciální	k2eAgNnSc2d1	speciální
náčiní	náčiní	k1gNnSc2	náčiní
na	na	k7c4	na
sběr	sběr	k1gInSc4	sběr
<g/>
,	,	kIx,	,
až	až	k9	až
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
hmyzími	hmyzí	k2eAgMnPc7d1	hmyzí
škůdci	škůdce	k1gMnPc7	škůdce
i	i	k8xC	i
ptactvem	ptactvo	k1gNnSc7	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kolumbovské	kolumbovský	k2eAgFnSc2d1	kolumbovská
výměny	výměna	k1gFnSc2	výměna
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dostalo	dostat	k5eAaPmAgNnS	dostat
mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
ananas	ananas	k1gInSc1	ananas
nebo	nebo	k8xC	nebo
kukuřice	kukuřice	k1gFnSc1	kukuřice
a	a	k8xC	a
vedle	vedle	k7c2	vedle
zlepšení	zlepšení	k1gNnSc2	zlepšení
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
faktory	faktor	k1gInPc1	faktor
pomohly	pomoct	k5eAaPmAgInP	pomoct
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
výraznému	výrazný	k2eAgInSc3d1	výrazný
nárůstu	nárůst	k1gInSc3	nárůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
aplikaci	aplikace	k1gFnSc6	aplikace
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
na	na	k7c4	na
hospodaření	hospodaření	k1gNnSc4	hospodaření
s	s	k7c7	s
rostlinami	rostlina	k1gFnPc7	rostlina
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
zelená	zelený	k2eAgFnSc1d1	zelená
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
globalizace	globalizace	k1gFnSc2	globalizace
se	se	k3xPyFc4	se
ovocné	ovocný	k2eAgInPc1d1	ovocný
druhy	druh	k1gInPc1	druh
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
a	a	k8xC	a
velikosti	velikost	k1gFnSc3	velikost
plodů	plod	k1gInPc2	plod
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
adaptačně	adaptačně	k6eAd1	adaptačně
plastičtější	plastický	k2eAgFnPc1d2	plastičtější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Situace	situace	k1gFnSc1	situace
dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
pokojové	pokojový	k2eAgFnPc1d1	pokojová
rostliny	rostlina	k1gFnPc1	rostlina
většinou	většinou	k6eAd1	většinou
původně	původně	k6eAd1	původně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tropů	trop	k1gInPc2	trop
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
stejná	stejný	k2eAgFnSc1d1	stejná
teplota	teplota	k1gFnSc1	teplota
kolem	kolem	k7c2	kolem
20	[number]	k4	20
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
zakoupit	zakoupit	k5eAaPmF	zakoupit
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
sady	sada	k1gFnPc1	sada
často	často	k6eAd1	často
odkupují	odkupovat	k5eAaImIp3nP	odkupovat
semena	semeno	k1gNnPc1	semeno
od	od	k7c2	od
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
sběr	sběr	k1gInSc1	sběr
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
zautomatizován	zautomatizován	k2eAgInSc1d1	zautomatizován
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
například	například	k6eAd1	například
bezpeckatý	bezpeckatý	k2eAgInSc1d1	bezpeckatý
pomeranč	pomeranč	k1gInSc1	pomeranč
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
větve	větev	k1gFnPc1	větev
byly	být	k5eAaImAgFnP	být
nařízkovány	nařízkovat	k5eAaImNgFnP	nařízkovat
na	na	k7c4	na
pomerančovníky	pomerančovník	k1gInPc4	pomerančovník
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
zemědělci	zemědělec	k1gMnPc7	zemědělec
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
i	i	k9	i
metody	metoda	k1gFnPc1	metoda
využívající	využívající	k2eAgFnSc2d1	využívající
mutageneze	mutageneze	k1gFnSc2	mutageneze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
týmy	tým	k1gInPc1	tým
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
pěstování	pěstování	k1gNnSc4	pěstování
<g/>
"	"	kIx"	"
mutací	mutace	k1gFnSc7	mutace
proti	proti	k7c3	proti
suchu	sucho	k1gNnSc3	sucho
a	a	k8xC	a
vedru	vedro	k1gNnSc3	vedro
s	s	k7c7	s
očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
aplikací	aplikace	k1gFnSc7	aplikace
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
adaptací	adaptace	k1gFnPc2	adaptace
na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
kolonizaci	kolonizace	k1gFnSc6	kolonizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ovocné	ovocný	k2eAgInPc1d1	ovocný
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pomologické	pomologický	k2eAgNnSc4d1	pomologické
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Pomologie	pomologie	k1gFnSc1	pomologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
odrůdách	odrůda	k1gFnPc6	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pomologického	pomologický	k2eAgNnSc2d1	pomologické
členění	členění	k1gNnSc2	členění
i	i	k8xC	i
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
ovocné	ovocný	k2eAgInPc1d1	ovocný
druhy	druh	k1gInPc1	druh
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
jádroviny	jádrovina	k1gFnPc4	jádrovina
<g/>
,	,	kIx,	,
peckoviny	peckovina	k1gFnPc4	peckovina
<g/>
,	,	kIx,	,
skořápkoviny	skořápkovina	k1gFnPc4	skořápkovina
a	a	k8xC	a
drobné	drobný	k2eAgNnSc4d1	drobné
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Jádroviny	jádrovina	k1gFnSc2	jádrovina
====	====	k?	====
</s>
</p>
<p>
<s>
Jádroviny	jádrovina	k1gFnPc1	jádrovina
jsou	být	k5eAaImIp3nP	být
skupinou	skupina	k1gFnSc7	skupina
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
ovocnářství	ovocnářství	k1gNnSc6	ovocnářství
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
označení	označení	k1gNnSc4	označení
některých	některý	k3yIgInPc2	některý
ovocných	ovocný	k2eAgInPc2d1	ovocný
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
čeledi	čeleď	k1gFnSc6	čeleď
růžovitých	růžovitý	k2eAgMnPc2d1	růžovitý
(	(	kIx(	(
<g/>
Rosaceae	Rosacea	k1gMnSc2	Rosacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
podle	podle	k7c2	podle
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
poznání	poznání	k1gNnSc4	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
plod	plod	k1gInSc4	plod
rostliny	rostlina	k1gFnSc2	rostlina
je	být	k5eAaImIp3nS	být
jedlý	jedlý	k2eAgMnSc1d1	jedlý
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
pevnou	pevný	k2eAgFnSc7d1	pevná
jedlou	jedlý	k2eAgFnSc7d1	jedlá
dužninou	dužnina	k1gFnSc7	dužnina
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
jádrovin	jádrovina	k1gFnPc2	jádrovina
je	být	k5eAaImIp3nS	být
malvice	malvice	k1gFnSc1	malvice
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
shrnout	shrnout	k5eAaPmF	shrnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
jedlé	jedlý	k2eAgFnPc4d1	jedlá
malvice	malvice	k1gFnPc4	malvice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
jako	jako	k8xC	jako
jádroviny	jádrovina	k1gFnPc1	jádrovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jádrovin	jádrovina	k1gFnPc2	jádrovina
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jabloň	jabloň	k1gFnSc1	jabloň
(	(	kIx(	(
<g/>
Malus	Malus	k1gInSc1	Malus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hrušeň	hrušeň	k1gFnSc1	hrušeň
(	(	kIx(	(
<g/>
Pyrus	Pyrus	k1gInSc1	Pyrus
communis	communis	k1gFnSc2	communis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kdoule	kdoule	k1gFnSc2	kdoule
(	(	kIx(	(
<g/>
Cydonia	Cydonium	k1gNnSc2	Cydonium
oblonga	oblonga	k1gFnSc1	oblonga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mišpule	mišpule	k1gFnSc1	mišpule
(	(	kIx(	(
<g/>
Mespilus	Mespilus	k1gMnSc1	Mespilus
germanica	germanica	k1gMnSc1	germanica
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
Crataegus	Crataegus	k1gMnSc1	Crataegus
germanica	germanica	k1gMnSc1	germanica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jeřáb	jeřáb	k1gInSc1	jeřáb
oskeruše	oskeruše	k1gFnSc2	oskeruše
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gMnSc1	Sorbus
domestica	domestica	k1gMnSc1	domestica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
temnoplodec	temnoplodec	k1gMnSc1	temnoplodec
(	(	kIx(	(
<g/>
Aronia	Aronium	k1gNnSc2	Aronium
melanocarpa	melanocarpa	k1gFnSc1	melanocarpa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
muchovník	muchovník	k1gInSc1	muchovník
(	(	kIx(	(
<g/>
např	např	kA	např
Amelanchier	Amelanchier	k1gInSc1	Amelanchier
ovalis	ovalis	k1gFnSc1	ovalis
<g/>
)	)	kIx)	)
<g/>
Podobné	podobný	k2eAgNnSc1d1	podobné
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
používáno	používat	k5eAaImNgNnS	používat
například	například	k6eAd1	například
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgMnPc4	tento
druhy	druh	k1gMnPc4	druh
používán	používán	k2eAgInSc4d1	používán
název	název	k1gInSc4	název
kernobst	kernobst	k5eAaPmF	kernobst
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jádrovin	jádrovina	k1gFnPc2	jádrovina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
muchovník	muchovník	k1gInSc1	muchovník
<g/>
,	,	kIx,	,
temnoplodec	temnoplodec	k1gMnSc1	temnoplodec
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gMnSc1	jeřáb
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
drobného	drobný	k2eAgNnSc2d1	drobné
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Kdoulovec	kdoulovec	k1gInSc1	kdoulovec
(	(	kIx(	(
<g/>
Chaenomeles	Chaenomeles	k1gMnSc1	Chaenomeles
superba	superba	k1gMnSc1	superba
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
jádrovin	jádrovina	k1gFnPc2	jádrovina
ani	ani	k8xC	ani
do	do	k7c2	do
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
plod	plod	k1gInSc1	plod
(	(	kIx(	(
<g/>
malvice	malvice	k1gFnSc1	malvice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
jedlý	jedlý	k2eAgInSc1d1	jedlý
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
skalníku	skalník	k1gInSc2	skalník
Franchetova	Franchetův	k2eAgInSc2d1	Franchetův
jsou	být	k5eAaImIp3nP	být
malvice	malvice	k1gFnSc2	malvice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pojídány	pojídán	k2eAgMnPc4d1	pojídán
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plody	plod	k1gInPc4	plod
nejedlých	jedlý	k2eNgInPc2d1	nejedlý
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
"	"	kIx"	"
pouze	pouze	k6eAd1	pouze
přeneseně	přeneseně	k6eAd1	přeneseně
<g/>
,	,	kIx,	,
v	v	k7c6	v
lyrickém	lyrický	k2eAgInSc6d1	lyrický
popisu	popis	k1gInSc6	popis
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jako	jako	k8xC	jako
odborný	odborný	k2eAgInSc4d1	odborný
termín	termín	k1gInSc4	termín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Peckoviny	peckovina	k1gFnSc2	peckovina
====	====	k?	====
</s>
</p>
<p>
<s>
Peckoviny	peckovina	k1gFnPc4	peckovina
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
ovocných	ovocný	k2eAgFnPc2d1	ovocná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Peckoviny	peckovina	k1gFnPc1	peckovina
mají	mít	k5eAaImIp3nP	mít
pětičetné	pětičetný	k2eAgInPc4d1	pětičetný
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Slupka	slupka	k1gFnSc1	slupka
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
ojíněná	ojíněný	k2eAgFnSc1d1	ojíněná
(	(	kIx(	(
<g/>
švestky	švestka	k1gFnPc1	švestka
<g/>
,	,	kIx,	,
slivoně	slivoň	k1gFnPc1	slivoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
třešně	třešně	k1gFnSc1	třešně
<g/>
,	,	kIx,	,
višně	višně	k1gFnSc1	višně
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
typicky	typicky	k6eAd1	typicky
plstnatá	plstnatý	k2eAgFnSc1d1	plstnatá
(	(	kIx(	(
<g/>
meruňky	meruňka	k1gFnSc2	meruňka
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgFnSc2d1	pravá
broskvoně	broskvoň	k1gFnSc2	broskvoň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedlý	jedlý	k2eAgInSc1d1	jedlý
plod	plod	k1gInSc1	plod
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velkou	velký	k2eAgFnSc4d1	velká
pevnou	pevný	k2eAgFnSc4d1	pevná
strukturu	struktura	k1gFnSc4	struktura
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
pecku	pecka	k1gFnSc4	pecka
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
používáno	používat	k5eAaImNgNnS	používat
spíše	spíše	k9	spíše
k	k	k7c3	k
zjednodušenému	zjednodušený	k2eAgNnSc3d1	zjednodušené
vyjádření	vyjádření	k1gNnSc3	vyjádření
pro	pro	k7c4	pro
zobecnění	zobecnění	k1gNnSc4	zobecnění
společných	společný	k2eAgFnPc2d1	společná
vlastností	vlastnost	k1gFnPc2	vlastnost
některých	některý	k3yIgInPc2	některý
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
druhů	druh	k1gInPc2	druh
dřevin	dřevina	k1gFnPc2	dřevina
(	(	kIx(	(
<g/>
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
řez	řez	k1gInSc4	řez
<g/>
,	,	kIx,	,
statistika	statistika	k1gFnSc1	statistika
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
,	,	kIx,	,
apod	apod	kA	apod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
peckovin	peckovina	k1gFnPc2	peckovina
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
višeň	višeň	k1gFnSc1	višeň
<g/>
,	,	kIx,	,
slivoň	slivoň	k1gFnSc1	slivoň
<g/>
,	,	kIx,	,
třešeň	třešeň	k1gFnSc1	třešeň
<g/>
,	,	kIx,	,
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
,	,	kIx,	,
broskvoň	broskvoň	k1gFnSc1	broskvoň
a	a	k8xC	a
mandloň	mandloň	k1gFnSc1	mandloň
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
peckovin	peckovina	k1gFnPc2	peckovina
je	být	k5eAaImIp3nS	být
peckovice	peckovice	k1gFnSc1	peckovice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
peckovin	peckovina	k1gFnPc2	peckovina
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
například	například	k6eAd1	například
řazen	řadit	k5eAaImNgInS	řadit
Ořešák	ořešák	k1gInSc1	ořešák
královský	královský	k2eAgInSc1d1	královský
<g/>
,	,	kIx,	,
Kokosová	kokosový	k2eAgFnSc1d1	kokosová
palma	palma	k1gFnSc1	palma
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
černý	černý	k2eAgInSc1d1	černý
bez	bez	k1gInSc1	bez
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
peckovice	peckovice	k1gFnSc1	peckovice
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
maliník	maliník	k1gInSc1	maliník
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gNnSc1	jeho
souplodí	souplodí	k1gNnSc1	souplodí
tvoří	tvořit	k5eAaImIp3nS	tvořit
rovněž	rovněž	k9	rovněž
peckovice	peckovice	k1gFnSc1	peckovice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
ovoce	ovoce	k1gNnSc2	ovoce
plodících	plodící	k2eAgInPc2d1	plodící
peckovice	peckovice	k1gFnPc4	peckovice
nelze	lze	k6eNd1	lze
řadit	řadit	k5eAaImF	řadit
do	do	k7c2	do
peckovin	peckovina	k1gFnPc2	peckovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Skořápkoviny	Skořápkovina	k1gFnSc2	Skořápkovina
====	====	k?	====
</s>
</p>
<p>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výživná	výživný	k2eAgNnPc4d1	výživné
a	a	k8xC	a
olejnatá	olejnatý	k2eAgNnPc4d1	olejnaté
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kromě	kromě	k7c2	kromě
tuku	tuk	k1gInSc2	tuk
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
skořápkovin	skořápkovina	k1gFnPc2	skořápkovina
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
peckovice	peckovice	k1gFnSc1	peckovice
(	(	kIx(	(
<g/>
ořešák	ořešák	k1gInSc1	ořešák
královský	královský	k2eAgInSc1d1	královský
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
oříšky	oříšek	k1gInPc1	oříšek
(	(	kIx(	(
<g/>
líska	líska	k1gFnSc1	líska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
různopohlavné	různopohlavný	k2eAgInPc1d1	různopohlavný
<g/>
,	,	kIx,	,
jednodomé	jednodomý	k2eAgInPc1d1	jednodomý
a	a	k8xC	a
větrosnubné	větrosnubný	k2eAgInPc1d1	větrosnubný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Drobné	drobný	k2eAgNnSc1d1	drobné
ovoce	ovoce	k1gNnSc1	ovoce
====	====	k?	====
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
řazené	řazený	k2eAgInPc1d1	řazený
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
drobné	drobný	k2eAgNnSc4d1	drobné
ovoce	ovoce	k1gNnSc4	ovoce
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
malé	malý	k2eAgFnPc1d1	malá
bobule	bobule	k1gFnPc1	bobule
(	(	kIx(	(
<g/>
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
angrešt	angrešt	k1gInSc1	angrešt
<g/>
,	,	kIx,	,
borůvka	borůvka	k1gFnSc1	borůvka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
souplodí	souplodí	k1gNnSc1	souplodí
(	(	kIx(	(
<g/>
jahoda	jahoda	k1gFnSc1	jahoda
<g/>
,	,	kIx,	,
malina	malina	k1gFnSc1	malina
<g/>
,	,	kIx,	,
ostružina	ostružina	k1gFnSc1	ostružina
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
plodenství	plodenství	k1gNnSc1	plodenství
(	(	kIx(	(
<g/>
moruše	moruše	k1gFnSc1	moruše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
kromě	kromě	k7c2	kromě
jahodníku	jahodník	k1gInSc2	jahodník
rostou	růst	k5eAaImIp3nP	růst
keřovitě	keřovitě	k6eAd1	keřovitě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Botanické	botanický	k2eAgNnSc1d1	botanické
hledisko	hledisko	k1gNnSc1	hledisko
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
botaniky	botanik	k1gMnPc4	botanik
je	být	k5eAaImIp3nS	být
ovoce	ovoce	k1gNnPc1	ovoce
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
jedlým	jedlý	k2eAgInSc7d1	jedlý
plodem	plod	k1gInSc7	plod
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
–	–	k?	–
tedy	tedy	k8xC	tedy
včetně	včetně	k7c2	včetně
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c4	za
ovoce	ovoce	k1gNnSc4	ovoce
nepovažovaných	považovaný	k2eNgInPc2d1	nepovažovaný
<g/>
,	,	kIx,	,
luštěnin	luštěnina	k1gFnPc2	luštěnina
nebo	nebo	k8xC	nebo
rajčat	rajče	k1gNnPc2	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
botanické	botanický	k2eAgFnSc2d1	botanická
charakteristiky	charakteristika	k1gFnSc2	charakteristika
plodu	plod	k1gInSc2	plod
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jádroviny	jádrovina	k1gFnPc1	jádrovina
(	(	kIx(	(
<g/>
malvice	malvice	k1gFnPc1	malvice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
peckoviny	peckovina	k1gFnPc1	peckovina
(	(	kIx(	(
<g/>
peckovice	peckovice	k1gFnPc1	peckovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
skořápkoviny	skořápkovina	k1gFnPc1	skořápkovina
(	(	kIx(	(
<g/>
uměle	uměle	k6eAd1	uměle
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
plodem	plod	k1gInSc7	plod
skořápkovin	skořápkovina	k1gFnPc2	skořápkovina
je	být	k5eAaImIp3nS	být
peckovice	peckovice	k1gFnSc1	peckovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bobuloviny	bobulovina	k1gFnPc1	bobulovina
(	(	kIx(	(
<g/>
bobule	bobule	k1gFnSc1	bobule
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jahodník	jahodník	k1gInSc1	jahodník
(	(	kIx(	(
<g/>
nažka	nažka	k1gFnSc1	nažka
<g/>
,	,	kIx,	,
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
se	se	k3xPyFc4	se
zdužnatělé	zdužnatělý	k2eAgNnSc1d1	zdužnatělé
lůžko	lůžko	k1gNnSc1	lůžko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Podle	podle	k7c2	podle
čeledí	čeleď	k1gFnPc2	čeleď
====	====	k?	====
</s>
</p>
<p>
<s>
Z	z	k7c2	z
taxonomického	taxonomický	k2eAgNnSc2d1	taxonomické
hlediska	hledisko	k1gNnSc2	hledisko
mezi	mezi	k7c4	mezi
ovoce	ovoce	k1gNnSc4	ovoce
patří	patřit	k5eAaImIp3nP	patřit
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
růžovité	růžovitý	k2eAgInPc4d1	růžovitý
(	(	kIx(	(
<g/>
Rosaceae	Rosacea	k1gInPc4	Rosacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
tam	tam	k6eAd1	tam
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
jabloň	jabloň	k1gFnSc1	jabloň
<g/>
,	,	kIx,	,
hrušeň	hrušeň	k1gFnSc1	hrušeň
<g/>
,	,	kIx,	,
kdouloň	kdouloň	k1gFnSc1	kdouloň
<g/>
,	,	kIx,	,
mišpule	mišpule	k1gFnSc1	mišpule
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gInSc1	jeřáb
<g/>
,	,	kIx,	,
švestka	švestka	k1gFnSc1	švestka
<g/>
,	,	kIx,	,
mirabelky	mirabelka	k1gFnPc1	mirabelka
<g/>
,	,	kIx,	,
ringle	ringle	k1gFnSc1	ringle
<g/>
,	,	kIx,	,
slíva	slíva	k1gFnSc1	slíva
<g/>
,	,	kIx,	,
višeň	višeň	k1gFnSc1	višeň
<g/>
,	,	kIx,	,
třešeň	třešeň	k1gFnSc1	třešeň
<g/>
,	,	kIx,	,
broskvoň	broskvoň	k1gFnSc1	broskvoň
<g/>
,	,	kIx,	,
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
,	,	kIx,	,
mandloň	mandloň	k1gFnSc1	mandloň
<g/>
,	,	kIx,	,
ostružiník	ostružiník	k1gInSc1	ostružiník
<g/>
,	,	kIx,	,
maliník	maliník	k1gInSc1	maliník
<g/>
,	,	kIx,	,
šípková	šípkový	k2eAgFnSc1d1	šípková
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
jahoda	jahoda	k1gFnSc1	jahoda
</s>
</p>
<p>
<s>
ořešákovité	ořešákovitý	k2eAgInPc4d1	ořešákovitý
(	(	kIx(	(
<g/>
Juglandaceae	Juglandacea	k1gInPc4	Juglandacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
opadavé	opadavý	k2eAgInPc1d1	opadavý
stromy	strom	k1gInPc1	strom
se	s	k7c7	s
zpeřenými	zpeřený	k2eAgInPc7d1	zpeřený
střídavými	střídavý	k2eAgInPc7d1	střídavý
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jedlé	jedlý	k2eAgInPc1d1	jedlý
ořechy	ořech	k1gInPc1	ořech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lískovité	lískovitý	k2eAgInPc4d1	lískovitý
(	(	kIx(	(
<g/>
Corylaceae	Corylacea	k1gInPc4	Corylacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
čeleď	čeleď	k1gFnSc1	čeleď
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c6	na
břízovité	břízovitý	k2eAgFnSc6d1	břízovitý
(	(	kIx(	(
<g/>
Betulaceae	Betulaceae	k1gFnSc6	Betulaceae
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
keře	keř	k1gInPc1	keř
a	a	k8xC	a
stromy	strom	k1gInPc1	strom
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
střídavými	střídavý	k2eAgInPc7d1	střídavý
listy	list	k1gInPc7	list
a	a	k8xC	a
květy	květ	k1gInPc7	květ
v	v	k7c6	v
převislých	převislý	k2eAgFnPc6d1	převislá
nebo	nebo	k8xC	nebo
vzpřímených	vzpřímený	k2eAgFnPc6d1	vzpřímená
jednopohlavných	jednopohlavný	k2eAgFnPc6d1	jednopohlavný
jehnědách	jehněda	k1gFnPc6	jehněda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
meruzalkovité	meruzalkovitý	k2eAgInPc4d1	meruzalkovitý
(	(	kIx(	(
<g/>
Grossulariaceae	Grossulariacea	k1gInPc4	Grossulariacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čeleď	čeleď	k1gFnSc1	čeleď
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jednodomé	jednodomý	k2eAgFnSc3d1	jednodomá
nebo	nebo	k8xC	nebo
dvoudomé	dvoudomý	k2eAgFnSc3d1	dvoudomá
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgFnSc3d1	opadavá
nebo	nebo	k8xC	nebo
ojediněle	ojediněle	k6eAd1	ojediněle
i	i	k9	i
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
keře	keř	k1gInPc1	keř
s	s	k7c7	s
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
bobule	bobule	k1gFnSc1	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
olejnatým	olejnatý	k2eAgInSc7d1	olejnatý
endospermem	endosperm	k1gInSc7	endosperm
<g/>
.	.	kIx.	.
<g/>
Neexistuje	existovat	k5eNaImIp3nS	existovat
ovšem	ovšem	k9	ovšem
jednotná	jednotný	k2eAgFnSc1d1	jednotná
definice	definice	k1gFnSc1	definice
pojmu	pojem	k1gInSc2	pojem
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
co	co	k3yInSc1	co
není	být	k5eNaImIp3nS	být
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určit	určit	k5eAaPmF	určit
toto	tento	k3xDgNnSc1	tento
dohodou	dohoda	k1gFnSc7	dohoda
a	a	k8xC	a
taxativně	taxativně	k6eAd1	taxativně
vyjmenovat	vyjmenovat	k5eAaPmF	vyjmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nerozděluje	rozdělovat	k5eNaImIp3nS	rozdělovat
botanicky	botanicky	k6eAd1	botanicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hospodářsky	hospodářsky	k6eAd1	hospodářsky
anebo	anebo	k8xC	anebo
podle	podle	k7c2	podle
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
na	na	k7c4	na
ovoce	ovoce	k1gNnSc4	ovoce
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgNnSc4d1	tropické
a	a	k8xC	a
subtropické	subtropický	k2eAgNnSc4d1	subtropické
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
dělit	dělit	k5eAaImF	dělit
také	také	k9	také
podle	podle	k7c2	podle
využití	využití	k1gNnSc2	využití
(	(	kIx(	(
<g/>
stolní	stolní	k2eAgFnPc1d1	stolní
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
<g/>
,	,	kIx,	,
moštové	moštový	k2eAgFnPc1d1	moštová
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
zrání	zrání	k1gNnSc2	zrání
(	(	kIx(	(
<g/>
letní	letní	k2eAgInPc1d1	letní
<g/>
,	,	kIx,	,
podzimní	podzimní	k2eAgInPc1d1	podzimní
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgInPc1d1	zimní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Látkové	látkový	k2eAgNnSc1d1	látkové
složení	složení	k1gNnSc1	složení
ovoce	ovoce	k1gNnSc2	ovoce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Voda	voda	k1gFnSc1	voda
===	===	k?	===
</s>
</p>
<p>
<s>
Podíl	podíl	k1gInSc1	podíl
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
kolísá	kolísat	k5eAaImIp3nS	kolísat
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
90	[number]	k4	90
%	%	kIx~	%
i	i	k8xC	i
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
nejvíce	hodně	k6eAd3	hodně
vody	voda	k1gFnSc2	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ovoce	ovoce	k1gNnSc1	ovoce
jako	jako	k8xC	jako
broskev	broskev	k1gFnSc1	broskev
<g/>
,	,	kIx,	,
meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
naopak	naopak	k6eAd1	naopak
nejméně	málo	k6eAd3	málo
vody	voda	k1gFnPc4	voda
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vláknina	vláknina	k1gFnSc1	vláknina
===	===	k?	===
</s>
</p>
<p>
<s>
Vláknina	vláknina	k1gFnSc1	vláknina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
celulózou	celulóza	k1gFnSc7	celulóza
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nestravitelná	stravitelný	k2eNgFnSc1d1	nestravitelná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
funkci	funkce	k1gFnSc4	funkce
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
celulózy	celulóza	k1gFnSc2	celulóza
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
stupni	stupeň	k1gInSc6	stupeň
zralosti	zralost	k1gFnSc2	zralost
<g/>
.	.	kIx.	.
</s>
<s>
Celulóza	celulóza	k1gFnSc1	celulóza
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
konzistenční	konzistenční	k2eAgFnPc4d1	konzistenční
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cukry	cukr	k1gInPc4	cukr
===	===	k?	===
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
cukru	cukr	k1gInSc2	cukr
se	se	k3xPyFc4	se
během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
mění	měnit	k5eAaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Škrob	škrob	k1gInSc4	škrob
obsažený	obsažený	k2eAgInSc4d1	obsažený
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
se	se	k3xPyFc4	se
během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
oleje	olej	k1gInPc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
především	především	k9	především
fruktóza	fruktóza	k1gFnSc1	fruktóza
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sacharóza	sacharóza	k1gFnSc1	sacharóza
a	a	k8xC	a
méně	málo	k6eAd2	málo
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
škrobu	škrob	k1gInSc2	škrob
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
na	na	k7c4	na
stanovení	stanovení	k1gNnSc4	stanovení
optimální	optimální	k2eAgFnSc2d1	optimální
doby	doba	k1gFnSc2	doba
sklizně	sklizeň	k1gFnSc2	sklizeň
pro	pro	k7c4	pro
skladování	skladování	k1gNnSc4	skladování
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tuky	tuk	k1gInPc1	tuk
a	a	k8xC	a
oleje	olej	k1gInPc1	olej
===	===	k?	===
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
obsah	obsah	k1gInSc1	obsah
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
zrání	zrání	k1gNnSc2	zrání
<g/>
.	.	kIx.	.
</s>
<s>
Tuky	tuk	k1gInPc1	tuk
a	a	k8xC	a
oleje	olej	k1gInPc1	olej
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
semenech	semeno	k1gNnPc6	semeno
a	a	k8xC	a
ve	v	k7c6	v
slupce	slupka	k1gFnSc6	slupka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ochranný	ochranný	k2eAgInSc4d1	ochranný
vliv	vliv	k1gInSc4	vliv
před	před	k7c7	před
chorobami	choroba	k1gFnPc7	choroba
(	(	kIx(	(
<g/>
olejnatá	olejnatý	k2eAgFnSc1d1	olejnatá
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pektiny	pektin	k1gInPc4	pektin
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rosolovité	rosolovitý	k2eAgFnPc1d1	rosolovitá
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pektiny	pektin	k1gInPc1	pektin
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
nezralém	zralý	k2eNgNnSc6d1	nezralé
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zažívaní	zažívaný	k2eAgMnPc1d1	zažívaný
a	a	k8xC	a
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
pektinu	pektin	k1gInSc2	pektin
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jablkách	jablko	k1gNnPc6	jablko
<g/>
,	,	kIx,	,
meruňkách	meruňka	k1gFnPc6	meruňka
<g/>
,	,	kIx,	,
jahodách	jahoda	k1gFnPc6	jahoda
a	a	k8xC	a
angreštu	angrešt	k1gInSc6	angrešt
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
pektinu	pektin	k1gInSc2	pektin
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
je	být	k5eAaImIp3nS	být
0,2	[number]	k4	0,2
<g/>
–	–	k?	–
<g/>
0,6	[number]	k4	0,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vitamíny	vitamín	k1gInPc4	vitamín
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
skupin	skupina	k1gFnPc2	skupina
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
neumí	umět	k5eNaImIp3nS	umět
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
omezeně	omezeně	k6eAd1	omezeně
<g/>
.	.	kIx.	.
</s>
<s>
Vitamíny	vitamín	k1gInPc1	vitamín
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vitamín	vitamín	k1gInSc4	vitamín
C	C	kA	C
====	====	k?	====
</s>
</p>
<p>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
vitaminu	vitamin	k1gInSc2	vitamin
C	C	kA	C
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kurděje	kurděje	k1gFnPc4	kurděje
(	(	kIx(	(
<g/>
skorbut	skorbut	k1gInSc1	skorbut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
ovoce	ovoce	k1gNnSc2	ovoce
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
obsahem	obsah	k1gInSc7	obsah
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
patří	patřit	k5eAaImIp3nS	patřit
černý	černý	k2eAgInSc1d1	černý
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
rakytník	rakytník	k1gInSc1	rakytník
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
jeřáb	jeřáb	k1gInSc1	jeřáb
<g/>
,	,	kIx,	,
jahody	jahoda	k1gFnPc1	jahoda
<g/>
,	,	kIx,	,
kiwi	kiwi	k1gNnPc1	kiwi
<g/>
,	,	kIx,	,
plody	plod	k1gInPc1	plod
růže	růž	k1gFnSc2	růž
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gFnSc1	Rosa
pomifera	pomifera	k1gFnSc1	pomifera
<g/>
,	,	kIx,	,
Rosa	Rosa	k1gFnSc1	Rosa
rugosa	rugosa	k1gFnSc1	rugosa
<g/>
,	,	kIx,	,
Rosa	Rosa	k1gFnSc1	Rosa
canina	canina	k1gFnSc1	canina
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Varem	var	k1gInSc7	var
se	se	k3xPyFc4	se
vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Provitamín	provitamín	k1gInSc4	provitamín
A	a	k8xC	a
(	(	kIx(	(
<g/>
beta	beta	k1gNnSc1	beta
karoten	karoten	k1gInSc1	karoten
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Tělo	tělo	k1gNnSc1	tělo
si	se	k3xPyFc3	se
vitamín	vitamín	k1gInSc1	vitamín
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
ovoce	ovoce	k1gNnSc1	ovoce
s	s	k7c7	s
provitamínem	provitamín	k1gInSc7	provitamín
A	A	kA	A
<g/>
:	:	kIx,	:
meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vitamín	vitamín	k1gInSc1	vitamín
B6	B6	k1gFnSc2	B6
====	====	k?	====
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
ovoce	ovoce	k1gNnSc1	ovoce
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
vitamínu	vitamín	k1gInSc2	vitamín
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
hrušky	hruška	k1gFnPc1	hruška
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
aktivita	aktivita	k1gFnSc1	aktivita
a	a	k8xC	a
využitelnost	využitelnost	k1gFnSc1	využitelnost
synteticky	synteticky	k6eAd1	synteticky
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
vitamínů	vitamín	k1gInPc2	vitamín
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
přirozených	přirozený	k2eAgFnPc2d1	přirozená
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
prvky	prvek	k1gInPc1	prvek
nebo	nebo	k8xC	nebo
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
minerální	minerální	k2eAgFnSc1d1	minerální
látka	látka	k1gFnSc1	látka
jsou	být	k5eAaImIp3nP	být
železo	železo	k1gNnSc1	železo
(	(	kIx(	(
<g/>
tvorba	tvorba	k1gFnSc1	tvorba
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
kosti	kost	k1gFnPc4	kost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
(	(	kIx(	(
<g/>
krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
<g/>
,	,	kIx,	,
růst	růst	k1gInSc1	růst
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třísloviny	tříslovina	k1gFnSc2	tříslovina
===	===	k?	===
</s>
</p>
<p>
<s>
Třísloviny	tříslovina	k1gFnPc1	tříslovina
jsou	být	k5eAaImIp3nP	být
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
mající	mající	k2eAgFnSc4d1	mající
svíravou	svíravý	k2eAgFnSc4d1	svíravá
<g/>
,	,	kIx,	,
trpkou	trpký	k2eAgFnSc4d1	trpká
chuť	chuť	k1gFnSc4	chuť
(	(	kIx(	(
<g/>
jeřáby	jeřáb	k1gInPc4	jeřáb
<g/>
.	.	kIx.	.
pecky	pecka	k1gFnPc4	pecka
vinné	vinný	k2eAgFnSc2d1	vinná
révy	réva	k1gFnSc2	réva
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poškození	poškození	k1gNnSc6	poškození
zdravého	zdravý	k2eAgNnSc2d1	zdravé
pletiva	pletivo	k1gNnSc2	pletivo
ovoce	ovoce	k1gNnSc2	ovoce
se	se	k3xPyFc4	se
třísloviny	tříslovina	k1gFnPc1	tříslovina
rychle	rychle	k6eAd1	rychle
okysličují	okysličovat	k5eAaImIp3nP	okysličovat
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přiměřeném	přiměřený	k2eAgNnSc6d1	přiměřené
množství	množství	k1gNnSc6	množství
dodávají	dodávat	k5eAaImIp3nP	dodávat
ovoci	ovoce	k1gNnSc6	ovoce
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
chuť	chuť	k1gFnSc4	chuť
</s>
</p>
<p>
<s>
===	===	k?	===
Aromatické	aromatický	k2eAgFnPc1d1	aromatická
látky	látka	k1gFnPc1	látka
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vonné	vonný	k2eAgFnPc1d1	vonná
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Aromatické	aromatický	k2eAgFnPc1d1	aromatická
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
převážně	převážně	k6eAd1	převážně
éterickými	éterický	k2eAgInPc7d1	éterický
oleji	olej	k1gInPc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Způsobují	způsobovat	k5eAaImIp3nP	způsobovat
typické	typický	k2eAgFnPc1d1	typická
vůně	vůně	k1gFnPc1	vůně
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Zráním	zrání	k1gNnSc7	zrání
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
obsah	obsah	k1gInSc1	obsah
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ovoce	ovoce	k1gNnSc4	ovoce
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
==	==	k?	==
</s>
</p>
<p>
<s>
Jádroviny	jádrovina	k1gFnPc1	jádrovina
(	(	kIx(	(
<g/>
botanicky	botanicky	k6eAd1	botanicky
především	především	k9	především
malvice	malvice	k1gFnSc1	malvice
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jabloň	jabloň	k1gFnSc1	jabloň
–	–	k?	–
jablka	jablko	k1gNnSc2	jablko
<g/>
,	,	kIx,	,
hrušeň	hrušeň	k1gFnSc1	hrušeň
–	–	k?	–
hrušky	hruška	k1gFnSc2	hruška
</s>
</p>
<p>
<s>
Peckoviny	peckovina	k1gFnPc1	peckovina
(	(	kIx(	(
<g/>
peckovice	peckovice	k1gFnSc1	peckovice
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
třešeň	třešeň	k1gFnSc1	třešeň
–	–	k?	–
třešně	třešně	k1gFnSc1	třešně
<g/>
,	,	kIx,	,
broskvoň	broskvoň	k1gFnSc1	broskvoň
–	–	k?	–
broskve	broskev	k1gFnSc2	broskev
<g/>
,	,	kIx,	,
slivoň	slivoň	k1gFnSc4	slivoň
–	–	k?	–
švestky	švestka	k1gFnPc1	švestka
<g/>
,	,	kIx,	,
meruňky	meruňka	k1gFnPc1	meruňka
a	a	k8xC	a
ringle	ringle	k1gFnPc1	ringle
</s>
</p>
<p>
<s>
Skořápkoviny	Skořápkovina	k1gFnPc1	Skořápkovina
(	(	kIx(	(
<g/>
líska	líska	k1gFnSc1	líska
–	–	k?	–
lískový	lískový	k2eAgInSc1d1	lískový
ořech	ořech	k1gInSc1	ořech
<g/>
,	,	kIx,	,
ořešák	ořešák	k1gInSc1	ořešák
–	–	k?	–
vlašský	vlašský	k2eAgInSc1d1	vlašský
ořech	ořech	k1gInSc1	ořech
<g/>
,	,	kIx,	,
jedlý	jedlý	k2eAgInSc1d1	jedlý
kaštan	kaštan	k1gInSc1	kaštan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bobule	bobule	k1gFnSc1	bobule
(	(	kIx(	(
<g/>
borůvka	borůvka	k1gFnSc1	borůvka
<g/>
,	,	kIx,	,
brusinka	brusinka	k1gFnSc1	brusinka
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
rybíz	rybíz	k1gInSc1	rybíz
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
angrešt	angrešt	k1gInSc1	angrešt
<g/>
,	,	kIx,	,
klikva	klikva	k1gFnSc1	klikva
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
–	–	k?	–
hrozny	hrozen	k1gInPc1	hrozen
<g/>
,	,	kIx,	,
maliník	maliník	k1gInSc1	maliník
–	–	k?	–
malina	malina	k1gFnSc1	malina
<g/>
,	,	kIx,	,
ostružiník	ostružiník	k1gInSc1	ostružiník
–	–	k?	–
ostružina	ostružina	k1gFnSc1	ostružina
<g/>
,	,	kIx,	,
jahodník	jahodník	k1gInSc1	jahodník
–	–	k?	–
jahoda	jahoda	k1gFnSc1	jahoda
<g/>
,	,	kIx,	,
moruše	moruše	k1gFnSc1	moruše
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
nejběžnějších	běžný	k2eAgInPc2d3	nejběžnější
druhů	druh	k1gInPc2	druh
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Tropické	tropický	k2eAgNnSc4d1	tropické
a	a	k8xC	a
subtropické	subtropický	k2eAgNnSc4d1	subtropické
ovoce	ovoce	k1gNnSc4	ovoce
==	==	k?	==
</s>
</p>
<p>
<s>
Pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
,	,	kIx,	,
citron	citron	k1gInSc1	citron
<g/>
,	,	kIx,	,
banán	banán	k1gInSc1	banán
<g/>
,	,	kIx,	,
oliva	oliva	k1gFnSc1	oliva
<g/>
,	,	kIx,	,
kokos	kokos	k1gInSc1	kokos
<g/>
,	,	kIx,	,
mango	mango	k1gNnSc1	mango
<g/>
,	,	kIx,	,
kakichurma	kakichurma	k1gNnSc1	kakichurma
<g/>
,	,	kIx,	,
marakuja	marakuja	k1gFnSc1	marakuja
<g/>
,	,	kIx,	,
kiwi	kiwi	k1gNnSc1	kiwi
<g/>
,	,	kIx,	,
ananas	ananas	k1gInSc1	ananas
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
nejběžnějších	běžný	k2eAgInPc2d3	nejběžnější
druhů	druh	k1gInPc2	druh
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
odpor	odpor	k1gInSc4	odpor
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
</s>
</p>
<p>
<s>
Zelenina	zelenina	k1gFnSc1	zelenina
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ovoce	ovoce	k1gNnSc2	ovoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ovoce	ovoce	k1gNnSc2	ovoce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
