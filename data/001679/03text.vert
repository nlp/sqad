<s>
René-François-Armand	René-François-Armand	k1gInSc1	René-François-Armand
(	(	kIx(	(
<g/>
Sully	Sulla	k1gFnPc1	Sulla
<g/>
)	)	kIx)	)
Prudhomme	Prudhomme	k1gFnPc1	Prudhomme
[	[	kIx(	[
<g/>
syly	syl	k2eAgFnPc1d1	syl
prydomm	prydomm	k6eAd1	prydomm
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Châtenay-Malabry	Châtenay-Malabr	k1gInPc4	Châtenay-Malabr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
první	první	k4xOgFnSc2	první
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Sully	Sulla	k1gFnPc1	Sulla
Prudhomme	Prudhomme	k1gMnSc2	Prudhomme
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k9	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mu	on	k3xPp3gMnSc3	on
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
studoval	studovat	k5eAaImAgMnS	studovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
matematiku	matematika	k1gFnSc4	matematika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	časem	k6eAd1	časem
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
převážil	převážit	k5eAaPmAgInS	převážit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc4d1	klasická
filologii	filologie	k1gFnSc4	filologie
a	a	k8xC	a
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Záliba	záliba	k1gFnSc1	záliba
ve	v	k7c4	v
filozofii	filozofie	k1gFnSc4	filozofie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
projevila	projevit	k5eAaPmAgFnS	projevit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jeho	jeho	k3xOp3gFnSc2	jeho
poetické	poetický	k2eAgFnSc2d1	poetická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
parnasistického	parnasistický	k2eAgNnSc2d1	parnasistický
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
obnovit	obnovit	k5eAaPmF	obnovit
eleganci	elegance	k1gFnSc4	elegance
poezie	poezie	k1gFnSc2	poezie
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
romantismus	romantismus	k1gInSc4	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
snem	sen	k1gInSc7	sen
bylo	být	k5eAaImAgNnS	být
spojení	spojení	k1gNnSc1	spojení
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
by	by	k9	by
členem	člen	k1gMnSc7	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
Académie	Académie	k1gFnSc1	Académie
française	française	k1gFnSc2	française
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
mu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
prvnímu	první	k4xOgNnSc3	první
spisovateli	spisovatel	k1gMnSc6	spisovatel
vůbec	vůbec	k9	vůbec
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jako	jako	k8xC	jako
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
vynikající	vynikající	k2eAgFnSc4d1	vynikající
básnickou	básnický	k2eAgFnSc4d1	básnická
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
až	až	k9	až
do	do	k7c2	do
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
přináší	přinášet	k5eAaImIp3nS	přinášet
svědectví	svědectví	k1gNnSc1	svědectví
o	o	k7c6	o
vznešeném	vznešený	k2eAgInSc6d1	vznešený
idealismu	idealismus	k1gInSc6	idealismus
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgFnSc3d1	umělecká
dokonalosti	dokonalost	k1gFnSc3	dokonalost
a	a	k8xC	a
o	o	k7c6	o
vzácném	vzácný	k2eAgNnSc6d1	vzácné
spojení	spojení	k1gNnSc6	spojení
kvalit	kvalita	k1gFnPc2	kvalita
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
intelektu	intelekt	k1gInSc2	intelekt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sully	Sulla	k1gMnSc2	Sulla
Prudhomme	Prudhomme	k1gMnSc1	Prudhomme
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
v	v	k7c4	v
Châtenay-Malabry	Châtenay-Malabr	k1gInPc4	Châtenay-Malabr
nedaleko	nedaleko	k7c2	nedaleko
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
<g/>
.	.	kIx.	.
</s>
<s>
Stances	Stances	k1gMnSc1	Stances
et	et	k?	et
Poè	Poè	k1gMnSc1	Poè
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Stance	stance	k1gFnSc1	stance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
épreuves	épreuves	k1gInSc1	épreuves
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
Zkoušky	zkouška	k1gFnPc1	zkouška
osudu	osud	k1gInSc2	osud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
solitudes	solitudes	k1gInSc1	solitudes
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
Samoty	samota	k1gFnPc1	samota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Les	les	k1gInSc1	les
destins	destins	k1gInSc1	destins
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
Osudy	osud	k1gInPc1	osud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc7	la
France	Franc	k1gMnSc2	Franc
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
vaines	vaines	k1gMnSc1	vaines
tendresses	tendresses	k1gMnSc1	tendresses
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Marná	marný	k2eAgNnPc1d1	marné
laskání	laskání	k1gNnPc1	laskání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Zénith	Zénith	k1gMnSc1	Zénith
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
justice	justice	k1gFnSc2	justice
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc4	Le
prisme	prisme	k5eAaPmRp1nP	prisme
<g/>
,	,	kIx,	,
poésies	poésies	k1gMnSc1	poésies
diverses	diverses	k1gMnSc1	diverses
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
bonheur	bonheur	k1gMnSc1	bonheur
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Štěstí	štěstí	k1gNnSc1	štěstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Épaves	Épaves	k1gMnSc1	Épaves
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Trosky	troska	k1gFnPc1	troska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Que	Que	k?	Que
sais-je	saise	k1gFnSc1	sais-je
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Co	co	k3yQnSc4	co
vím	vědět	k5eAaImIp1nS	vědět
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofické	filozofický	k2eAgNnSc1d1	filozofické
pojednání	pojednání	k1gNnSc1	pojednání
<g/>
,	,	kIx,	,
Testament	testament	k1gInSc1	testament
poétique	poétique	k1gInSc1	poétique
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Básnická	básnický	k2eAgFnSc1d1	básnická
závěť	závěť	k1gFnSc1	závěť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
vraie	vraie	k1gFnSc2	vraie
religion	religion	k1gInSc1	religion
selon	selon	k1gMnSc1	selon
Pascal	Pascal	k1gMnSc1	Pascal
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Pravé	pravý	k2eAgNnSc1d1	pravé
náboženství	náboženství	k1gNnSc1	náboženství
podle	podle	k7c2	podle
Pascala	Pascal	k1gMnSc2	Pascal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
Journal	Journal	k1gMnSc5	Journal
intime	intimus	k1gMnSc5	intimus
<g/>
:	:	kIx,	:
lettres-pensée	lettresenséat	k5eAaPmIp3nS	lettres-penséat
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
český	český	k2eAgInSc1d1	český
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
básníkova	básníkův	k2eAgNnSc2d1	básníkovo
díla	dílo	k1gNnSc2	dílo
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
v	v	k7c6	v
Nakladatelském	nakladatelský	k2eAgNnSc6d1	nakladatelské
družstvu	družstvo	k1gNnSc6	družstvo
Máje	máj	k1gFnSc2	máj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Marná	marný	k2eAgNnPc4d1	marné
laskání	laskání	k1gNnSc4	laskání
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Kadlece	Kadlec	k1gMnSc2	Kadlec
a	a	k8xC	a
s	s	k7c7	s
úvodem	úvod	k1gInSc7	úvod
Václava	Václav	k1gMnSc2	Václav
Černého	Černý	k1gMnSc2	Černý
<g/>
.	.	kIx.	.
</s>
