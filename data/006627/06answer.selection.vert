<s>
Pět	pět	k4xCc1	pět
členů	člen	k1gMnPc2	člen
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
stálí	stálý	k2eAgMnPc1d1	stálý
členové	člen	k1gMnPc1	člen
se	s	k7c7	s
zvláštními	zvláštní	k2eAgFnPc7d1	zvláštní
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgMnPc2d1	zbylý
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
nestálí	stálit	k5eNaImIp3nP	stálit
členové	člen	k1gMnPc1	člen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
OSN	OSN	kA	OSN
na	na	k7c6	na
dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
období	období	k1gNnSc4	období
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
obmění	obměnit	k5eAaPmIp3nS	obměnit
pět	pět	k4xCc1	pět
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
