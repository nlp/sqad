<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
začal	začít	k5eAaPmAgMnS	začít
Rolland	Rolland	k1gMnSc1	Rolland
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
stěžejním	stěžejní	k2eAgNnSc6d1	stěžejní
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
románu	román	k1gInSc6	román
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
a	a	k8xC	a
za	za	k7c4	za
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgMnS	být
oceněn	oceněn	k2eAgInSc1d1	oceněn
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
"	"	kIx"	"
<g/>
...	...	k?	...
jako	jako	k8xC	jako
projev	projev	k1gInSc1	projev
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pocty	pocta	k1gFnSc2	pocta
za	za	k7c4	za
vznešený	vznešený	k2eAgInSc4d1	vznešený
idealismus	idealismus	k1gInSc4	idealismus
jeho	jeho	k3xOp3gFnPc2	jeho
literárních	literární	k2eAgFnPc2d1	literární
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
za	za	k7c4	za
sympatii	sympatie	k1gFnSc4	sympatie
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
líčil	líčit	k5eAaImAgMnS	líčit
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
