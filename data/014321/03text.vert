<s>
Alžírská	alžírský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
válečná	válečná	k1gFnSc1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Alžírska	Alžírsko	k1gNnSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
stejně	stejně	k6eAd1
širokých	široký	k2eAgInPc2d1
svislých	svislý	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
–	–	k?
zeleného	zelený	k2eAgNnSc2d1
a	a	k8xC
bílého	bílý	k2eAgNnSc2d1
–	–	k?
s	s	k7c7
červeným	červený	k2eAgMnSc7d1
půlměsicem	půlměsic	k1gMnSc7
a	a	k8xC
hvězdou	hvězda	k1gFnSc7
uprostřed	uprostřed	k7c2
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podoba	podoba	k1gFnSc1
vlajky	vlajka	k1gFnSc2
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
podoba	podoba	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
dřívější	dřívější	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
vlajky	vlajka	k1gFnSc2
užívané	užívaný	k2eAgInPc1d1
Národní	národní	k2eAgFnSc7d1
osvobozeneckou	osvobozenecký	k2eAgFnSc7d1
frontou	fronta	k1gFnSc7
od	od	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
značí	značit	k5eAaImIp3nS
čistotu	čistota	k1gFnSc4
<g/>
,	,	kIx,
zelená	zelená	k1gFnSc1
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
Islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půlměsíc	půlměsíc	k1gInSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
islámský	islámský	k2eAgInSc1d1
symbol	symbol	k1gInSc1
a	a	k8xC
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
turecké	turecký	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
je	být	k5eAaImIp3nS
stejná	stejný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
národní	národní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
má	můj	k3xOp1gFnSc1
však	však	k9
navíc	navíc	k6eAd1
v	v	k7c6
horním	horní	k2eAgInSc6d1
rohu	roh	k1gInSc6
dvě	dva	k4xCgFnPc4
zkřížené	zkřížený	k2eAgFnPc4d1
kotvy	kotva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Rozměry	rozměra	k1gFnPc1
alžírské	alžírský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
alžírského	alžírský	k2eAgInSc2d1
prezidentaPoměr	prezidentaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
lodní	lodní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
Naval	navalit	k5eAaPmRp2nS
Jack	Jack	k1gInSc1
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.crwflags.com/fotw/flags/dz.html	http://www.crwflags.com/fotw/flags/dz.htmla	k1gFnPc2
Alžírská	alžírský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Flags	Flags	k1gInSc4
Of	Of	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Alžírský	alžírský	k2eAgInSc1d1
znak	znak	k1gInSc1
</s>
<s>
Alžírská	alžírský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Alžírska	Alžírsko	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alžírská	alžírský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vlajky	vlajka	k1gFnSc2
afrických	africký	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
Angola	Angola	k1gFnSc1
•	•	k?
Benin	Benin	k1gInSc1
•	•	k?
Botswana	Botswana	k1gFnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Burundi	Burundi	k1gNnSc4
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Eritrea	Eritrea	k1gFnSc1
•	•	k?
Etiopie	Etiopie	k1gFnSc1
•	•	k?
Gabon	Gabon	k1gInSc1
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Ghana	Ghana	k1gFnSc1
•	•	k?
Guinea	guinea	k1gFnPc2
•	•	k?
Guinea-Bissau	Guinea-Bissaum	k1gNnSc6
•	•	k?
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Kapverdy	Kapverda	k1gFnSc2
•	•	k?
Keňa	Keňa	k1gFnSc1
•	•	k?
Komory	komora	k1gFnSc2
•	•	k?
Kongo	Kongo	k1gNnSc1
•	•	k?
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
republika	republika	k1gFnSc1
•	•	k?
Lesotho	Lesot	k1gMnSc2
•	•	k?
Libérie	Libérie	k1gFnSc2
•	•	k?
Libye	Libye	k1gFnSc2
•	•	k?
Madagaskar	Madagaskar	k1gInSc1
•	•	k?
Malawi	Malawi	k1gNnSc2
•	•	k?
Mali	Mali	k1gNnSc2
•	•	k?
Maroko	Maroko	k1gNnSc1
•	•	k?
Mauricius	Mauricius	k1gMnSc1
•	•	k?
Mauritánie	Mauritánie	k1gFnSc2
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Namibie	Namibie	k1gFnSc2
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc1
•	•	k?
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Rwanda	Rwanda	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Seychely	Seychely	k1gFnPc4
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Somálsko	Somálsko	k1gNnSc4
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Súdán	Súdán	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
Togo	Togo	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Zambie	Zambie	k1gFnSc1
•	•	k?
Zimbabwe	Zimbabwe	k1gNnSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Ceuta	Ceuta	k1gMnSc1
(	(	kIx(
<g/>
ESP	ESP	kA
<g/>
)	)	kIx)
•	•	k?
Kanárské	kanárský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
(	(	kIx(
<g/>
ESP	ESP	kA
<g/>
)	)	kIx)
•	•	k?
Madeira	Madeira	k1gFnSc1
(	(	kIx(
<g/>
PRT	PRT	kA
<g/>
)	)	kIx)
•	•	k?
Mayotte	Mayott	k1gInSc5
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Melilla	Melilla	k1gMnSc1
(	(	kIx(
<g/>
ESP	ESP	kA
<g/>
)	)	kIx)
•	•	k?
Réunion	Réunion	k1gInSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Saharská	saharský	k2eAgFnSc1d1
arabská	arabský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
MAR	MAR	kA
<g/>
)	)	kIx)
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Francouzská	francouzský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
a	a	k8xC
antarktická	antarktický	k2eAgNnPc1d1
území	území	k1gNnPc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
</s>
