<p>
<s>
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
někdy	někdy	k6eAd1	někdy
též	též	k9	též
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xC	jako
Goldbergovské	Goldbergovský	k2eAgFnPc1d1	Goldbergovský
variace	variace	k1gFnPc1	variace
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Goldberg-Variationen	Goldberg-Variationen	k1gInSc1	Goldberg-Variationen
<g/>
,	,	kIx,	,
BWV	BWV	kA	BWV
988	[number]	k4	988
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc1	cyklus
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
německého	německý	k2eAgMnSc2d1	německý
skladatele	skladatel	k1gMnSc2	skladatel
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Clavierübung	Clavierübung	k1gInSc1	Clavierübung
<g/>
"	"	kIx"	"
-	-	kIx~	-
Klavírní	klavírní	k2eAgNnSc1d1	klavírní
cvičení	cvičení	k1gNnSc1	cvičení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
Bachových	Bachová	k1gFnPc2	Bachová
děl	dělo	k1gNnPc2	dělo
vydaných	vydaný	k2eAgMnPc2d1	vydaný
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Passacagliou	Passacaglia	k1gFnSc7	Passacaglia
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
a	a	k8xC	a
Chaconnou	Chaconný	k2eAgFnSc4d1	Chaconný
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
příkladům	příklad	k1gInPc3	příklad
Bachova	Bachův	k2eAgNnSc2d1	Bachovo
variačního	variační	k2eAgNnSc2d1	variační
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolnosti	okolnost	k1gFnSc3	okolnost
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Theophilus	Theophilus	k1gMnSc1	Theophilus
Goldberg	Goldberg	k1gMnSc1	Goldberg
byl	být	k5eAaImAgMnS	být
mimořádně	mimořádně	k6eAd1	mimořádně
nadaný	nadaný	k2eAgMnSc1d1	nadaný
mladý	mladý	k2eAgMnSc1d1	mladý
klavírista	klavírista	k1gMnSc1	klavírista
i	i	k8xC	i
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Johanna	Johann	k1gMnSc2	Johann
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Goldberg	Goldberg	k1gMnSc1	Goldberg
hrával	hrávat	k5eAaImAgMnS	hrávat
knížeti	kníže	k1gMnSc3	kníže
Hermannovi	Hermann	k1gMnSc3	Hermann
Carlovi	Carl	k1gMnSc3	Carl
von	von	k1gInSc4	von
Keyserlingovi	Keyserling	k1gMnSc3	Keyserling
za	za	k7c2	za
bezesných	bezesný	k2eAgFnPc2d1	bezesná
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
objednal	objednat	k5eAaPmAgMnS	objednat
u	u	k7c2	u
Bacha	Bacha	k?	Bacha
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
nočním	noční	k2eAgInPc3d1	noční
koncertům	koncert	k1gInPc3	koncert
a	a	k8xC	a
Bach	Bach	k1gMnSc1	Bach
napsal	napsat	k5eAaPmAgMnS	napsat
variace	variace	k1gFnPc4	variace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
začátkem	začátkem	k7c2	začátkem
novodobých	novodobý	k2eAgFnPc2d1	novodobá
dějin	dějiny	k1gFnPc2	dějiny
variací	variace	k1gFnPc2	variace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
a	a	k8xC	a
největších	veliký	k2eAgNnPc2d3	veliký
děl	dělo	k1gNnPc2	dělo
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
vydal	vydat	k5eAaPmAgMnS	vydat
Goldbergovy	Goldbergův	k2eAgFnPc4d1	Goldbergova
variace	variace	k1gFnPc4	variace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Balthasara	Balthasar	k1gMnSc2	Balthasar
Schmida	Schmid	k1gMnSc2	Schmid
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Clavier	Clavier	k1gMnSc1	Clavier
Ubung	Ubung	k1gMnSc1	Ubung
bestehend	bestehend	k1gMnSc1	bestehend
in	in	k?	in
einer	einer	k1gMnSc1	einer
ARIA	ARIA	kA	ARIA
mit	mit	k?	mit
verschiedenen	verschiedenen	k2eAgMnSc1d1	verschiedenen
Veræ	Veræ	k1gMnSc1	Veræ
vors	vorsa	k1gFnPc2	vorsa
Clavicimbal	Clavicimbal	k1gInSc4	Clavicimbal
mit	mit	k?	mit
2	[number]	k4	2
Manualen	Manualen	k2eAgInSc1d1	Manualen
(	(	kIx(	(
<g/>
Klavírní	klavírní	k2eAgNnSc1d1	klavírní
cvičení	cvičení	k1gNnSc1	cvičení
sestávající	sestávající	k2eAgNnSc1d1	sestávající
z	z	k7c2	z
árie	árie	k1gFnSc2	árie
s	s	k7c7	s
rozmanitými	rozmanitý	k2eAgFnPc7d1	rozmanitá
variacemi	variace	k1gFnPc7	variace
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
manuály	manuál	k1gInPc7	manuál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
19	[number]	k4	19
výtisků	výtisk	k1gInPc2	výtisk
tohoto	tento	k3xDgMnSc2	tento
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Árie	árie	k1gFnSc1	árie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tématem	téma	k1gNnSc7	téma
těchto	tento	k3xDgFnPc2	tento
variací	variace	k1gFnPc2	variace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
už	už	k6eAd1	už
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
Knížce	knížka	k1gFnSc6	knížka
pro	pro	k7c4	pro
Annu	Anna	k1gFnSc4	Anna
Magdalenu	Magdalena	k1gFnSc4	Magdalena
Bachovou	Bachová	k1gFnSc4	Bachová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
melodicky	melodicky	k6eAd1	melodicky
tak	tak	k6eAd1	tak
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
už	už	k6eAd1	už
variací	variace	k1gFnSc7	variace
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Bach	Bach	k1gMnSc1	Bach
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
doprovod	doprovod	k1gInSc1	doprovod
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
v	v	k7c6	v
26	[number]	k4	26
<g/>
.	.	kIx.	.
variaci	variace	k1gFnSc6	variace
<g/>
.	.	kIx.	.
</s>
<s>
Golbergovy	Golbergův	k2eAgFnPc1d1	Golbergův
variace	variace	k1gFnPc1	variace
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tuto	tento	k3xDgFnSc4	tento
árii	árie	k1gFnSc4	árie
a	a	k8xC	a
30	[number]	k4	30
variací	variace	k1gFnPc2	variace
<g/>
.	.	kIx.	.
</s>
<s>
Variace	variace	k1gFnSc1	variace
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
klavírní	klavírní	k2eAgFnPc4d1	klavírní
a	a	k8xC	a
kontrapunktické	kontrapunktický	k2eAgFnPc4d1	kontrapunktická
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
variace	variace	k1gFnPc1	variace
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
tak	tak	k9	tak
melodie	melodie	k1gFnPc4	melodie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
basová	basový	k2eAgFnSc1d1	basová
linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Bach	Bach	k1gMnSc1	Bach
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
variacích	variace	k1gFnPc6	variace
značně	značně	k6eAd1	značně
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
základní	základní	k2eAgFnSc1d1	základní
harmonie	harmonie	k1gFnSc1	harmonie
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sled	sled	k1gInSc4	sled
harmonií	harmonie	k1gFnPc2	harmonie
<g/>
,	,	kIx,	,
daných	daný	k2eAgFnPc2d1	daná
árií	árie	k1gFnPc2	árie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
třicetkrát	třicetkrát	k6eAd1	třicetkrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Árie	árie	k1gFnSc1	árie
má	mít	k5eAaImIp3nS	mít
32	[number]	k4	32
taktů	takt	k1gInPc2	takt
<g/>
,	,	kIx,	,
dvojdílnou	dvojdílný	k2eAgFnSc4d1	dvojdílná
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
tónina	tónina	k1gFnSc1	tónina
je	být	k5eAaImIp3nS	být
G	G	kA	G
dur	dur	k1gNnSc1	dur
<g/>
.	.	kIx.	.
</s>
<s>
Bach	Bach	k1gMnSc1	Bach
přísně	přísně	k6eAd1	přísně
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
i	i	k9	i
formu	forma	k1gFnSc4	forma
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
variací	variace	k1gFnPc2	variace
<g/>
,	,	kIx,	,
jen	jen	k9	jen
šest	šest	k4xCc1	šest
variací	variace	k1gFnPc2	variace
nedodržuje	dodržovat	k5eNaImIp3nS	dodržovat
počet	počet	k1gInSc1	počet
taktů	takt	k1gInPc2	takt
árie	árie	k1gFnSc2	árie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
variace	variace	k1gFnPc1	variace
ornamentální	ornamentální	k2eAgFnSc1d1	ornamentální
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
patrná	patrný	k2eAgFnSc1d1	patrná
velká	velká	k1gFnSc1	velká
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
variační	variační	k2eAgNnSc1d1	variační
umění	umění	k1gNnSc1	umění
J.	J.	kA	J.
<g/>
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
<g/>
.	.	kIx.	.
</s>
<s>
Variace	variace	k1gFnPc1	variace
ani	ani	k8xC	ani
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nevyznívají	vyznívat	k5eNaImIp3nP	vyznívat
fádně	fádně	k6eAd1	fádně
a	a	k8xC	a
jednotvárně	jednotvárně	k6eAd1	jednotvárně
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
čistě	čistě	k6eAd1	čistě
kontrapunkticky	kontrapunkticky	k6eAd1	kontrapunkticky
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
kontrapunktickou	kontrapunktický	k2eAgFnSc7d1	kontrapunktická
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
následující	následující	k2eAgFnPc4d1	následující
generace	generace	k1gFnPc4	generace
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
1	[number]	k4	1
–	–	k?	–
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduše	jednoduše	k6eAd1	jednoduše
imitačně	imitačně	k6eAd1	imitačně
zpracovaná	zpracovaný	k2eAgFnSc1d1	zpracovaná
<g/>
,	,	kIx,	,
Allegro	allegro	k6eAd1	allegro
začíná	začínat	k5eAaImIp3nS	začínat
postupně	postupně	k6eAd1	postupně
vtahovat	vtahovat	k5eAaImF	vtahovat
posluchače	posluchač	k1gMnPc4	posluchač
do	do	k7c2	do
nálady	nálada	k1gFnSc2	nálada
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
2	[number]	k4	2
–	–	k?	–
je	být	k5eAaImIp3nS	být
také	také	k9	také
volná	volný	k2eAgFnSc1d1	volná
imitace	imitace	k1gFnSc1	imitace
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
už	už	k6eAd1	už
trojhlasná	trojhlasný	k2eAgFnSc1d1	trojhlasný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
3	[number]	k4	3
–	–	k?	–
je	být	k5eAaImIp3nS	být
kánonem	kánon	k1gInSc7	kánon
v	v	k7c6	v
unisonu	unisono	k1gNnSc6	unisono
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lyrická	lyrický	k2eAgFnSc1d1	lyrická
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
prvních	první	k4xOgInPc2	první
a	a	k8xC	a
posledních	poslední	k2eAgFnPc2d1	poslední
tří	tři	k4xCgFnPc2	tři
variací	variace	k1gFnPc2	variace
Bach	Bach	k1gMnSc1	Bach
seskupuje	seskupovat	k5eAaImIp3nS	seskupovat
variace	variace	k1gFnPc4	variace
do	do	k7c2	do
trojic	trojice	k1gFnPc2	trojice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnPc1	první
variace	variace	k1gFnPc1	variace
každé	každý	k3xTgFnSc2	každý
trojice	trojice	k1gFnSc2	trojice
má	mít	k5eAaImIp3nS	mít
ráz	ráz	k1gInSc4	ráz
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
ve	v	k7c4	v
druhé	druhý	k4xOgFnPc4	druhý
z	z	k7c2	z
trojice	trojice	k1gFnSc2	trojice
převládá	převládat	k5eAaImIp3nS	převládat
virtuózní	virtuózní	k2eAgFnSc1d1	virtuózní
složka	složka	k1gFnSc1	složka
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
je	být	k5eAaImIp3nS	být
variace	variace	k1gFnSc1	variace
kánonická	kánonický	k2eAgFnSc1d1	kánonický
(	(	kIx(	(
<g/>
kontrapunktická	kontrapunktický	k2eAgFnSc1d1	kontrapunktická
studie	studie	k1gFnSc1	studie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
4	[number]	k4	4
–	–	k?	–
je	být	k5eAaImIp3nS	být
taneční	taneční	k2eAgInSc1d1	taneční
v	v	k7c6	v
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
taktu	takt	k1gInSc2	takt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
5	[number]	k4	5
–	–	k?	–
melodie	melodie	k1gFnSc1	melodie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
basu	bas	k1gInSc6	bas
a	a	k8xC	a
pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
zde	zde	k6eAd1	zde
představuje	představovat	k5eAaImIp3nS	představovat
virtuózní	virtuózní	k2eAgFnSc4d1	virtuózní
složku	složka	k1gFnSc4	složka
–	–	k?	–
figurace	figurace	k1gFnPc1	figurace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jiskřivé	jiskřivý	k2eAgNnSc4d1	jiskřivé
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
6	[number]	k4	6
–	–	k?	–
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
sekundě	sekunda	k1gFnSc6	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
7	[number]	k4	7
–	–	k?	–
je	být	k5eAaImIp3nS	být
také	také	k9	také
taneční	taneční	k2eAgInSc1d1	taneční
<g/>
,	,	kIx,	,
v	v	k7c6	v
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
taktu	takt	k1gInSc2	takt
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
siciliana	sicilian	k1gMnSc2	sicilian
<g/>
.	.	kIx.	.
</s>
<s>
Připomíná	připomínat	k5eAaImIp3nS	připomínat
symfonii	symfonie	k1gFnSc4	symfonie
z	z	k7c2	z
Vánočního	vánoční	k2eAgNnSc2d1	vánoční
oratoria	oratorium	k1gNnSc2	oratorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
8	[number]	k4	8
–	–	k?	–
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
ostinátních	ostinátní	k2eAgFnPc6d1	ostinátní
figuracích	figurace	k1gFnPc6	figurace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
9	[number]	k4	9
–	–	k?	–
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
tercii	tercie	k1gFnSc6	tercie
<g/>
,	,	kIx,	,
trojhlasný	trojhlasný	k2eAgInSc1d1	trojhlasný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
10	[number]	k4	10
–	–	k?	–
je	být	k5eAaImIp3nS	být
Fughetta	Fughetta	k1gFnSc1	Fughetta
I.	I.	kA	I.
alla	alla	k6eAd1	alla
breve	breve	k1gNnPc7	breve
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bach	Bach	k1gInSc1	Bach
velmi	velmi	k6eAd1	velmi
přehledným	přehledný	k2eAgInSc7d1	přehledný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
efektivním	efektivní	k2eAgInSc7d1	efektivní
způsobem	způsob	k1gInSc7	způsob
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
téma	téma	k1gNnSc1	téma
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
trojhlasné	trojhlasný	k2eAgNnSc1d1	trojhlasné
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
11	[number]	k4	11
–	–	k?	–
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
osmé	osmý	k4xOgFnSc2	osmý
variace	variace	k1gFnSc2	variace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rytmicky	rytmicky	k6eAd1	rytmicky
obohacena	obohacen	k2eAgFnSc1d1	obohacena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
12	[number]	k4	12
–	–	k?	–
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
kvartě	kvarta	k1gFnSc6	kvarta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
13	[number]	k4	13
–	–	k?	–
je	být	k5eAaImIp3nS	být
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
áriovou	áriový	k2eAgFnSc7d1	áriový
melodikou	melodika	k1gFnSc7	melodika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stylizována	stylizován	k2eAgFnSc1d1	stylizována
jako	jako	k8xS	jako
sólo	sólo	k1gNnSc1	sólo
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
melodický	melodický	k2eAgInSc4d1	melodický
doprovod	doprovod	k1gInSc4	doprovod
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnPc4	variace
č.	č.	k?	č.
14	[number]	k4	14
–	–	k?	–
vidíme	vidět	k5eAaImIp1nP	vidět
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
krásné	krásný	k2eAgFnSc6d1	krásná
prolínání	prolínání	k1gNnSc4	prolínání
melodie	melodie	k1gFnSc2	melodie
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
vyměňující	vyměňující	k2eAgFnSc2d1	vyměňující
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
15	[number]	k4	15
–	–	k?	–
je	být	k5eAaImIp3nS	být
kánon	kánon	k1gInSc4	kánon
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
kvinty	kvinta	k1gFnSc2	kvinta
<g/>
.	.	kIx.	.
</s>
<s>
Připomíná	připomínat	k5eAaImIp3nS	připomínat
Bachův	Bachův	k2eAgInSc4d1	Bachův
chorál	chorál	k1gInSc4	chorál
"	"	kIx"	"
<g/>
O	o	k7c4	o
Lamm	Lamm	k1gInSc4	Lamm
Gottes	Gottes	k1gMnSc1	Gottes
unschuldig	unschuldig	k1gMnSc1	unschuldig
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
16	[number]	k4	16
–	–	k?	–
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
francouzská	francouzský	k2eAgFnSc1d1	francouzská
ouvertura	ouvertura	k1gFnSc1	ouvertura
bez	bez	k7c2	bez
pomalého	pomalý	k2eAgInSc2d1	pomalý
závěru	závěr	k1gInSc2	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Prvý	prvý	k4xOgInSc1	prvý
díl	díl	k1gInSc1	díl
je	být	k5eAaImIp3nS	být
grave	grave	k6eAd1	grave
s	s	k7c7	s
figuracemi	figurace	k1gFnPc7	figurace
typickými	typický	k2eAgFnPc7d1	typická
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
quasi	quasi	k6eAd1	quasi
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
tvoří	tvořit	k5eAaImIp3nS	tvořit
fughetta	fughetta	k1gMnSc1	fughetta
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tempu	tempo	k1gNnSc6	tempo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
17	[number]	k4	17
–	–	k?	–
mohli	moct	k5eAaImAgMnP	moct
bychom	by	kYmCp1nP	by
ji	on	k3xPp3gFnSc4	on
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
etudu	etuda	k1gFnSc4	etuda
lomených	lomený	k2eAgInPc2d1	lomený
intervalů	interval	k1gInPc2	interval
(	(	kIx(	(
<g/>
tercií	tercie	k1gFnPc2	tercie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
18	[number]	k4	18
–	–	k?	–
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
sextě	sexta	k1gFnSc6	sexta
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
giusto	giusto	k6eAd1	giusto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kánony	kánon	k1gInPc4	kánon
dost	dost	k6eAd1	dost
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
19	[number]	k4	19
–	–	k?	–
je	být	k5eAaImIp3nS	být
stylizována	stylizovat	k5eAaImNgFnS	stylizovat
do	do	k7c2	do
třídobého	třídobý	k2eAgInSc2d1	třídobý
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
připomíná	připomínat	k5eAaImIp3nS	připomínat
nám	my	k3xPp1nPc3	my
ländler	ländler	k1gInSc4	ländler
–	–	k?	–
tanec	tanec	k1gInSc1	tanec
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
neexistující	existující	k2eNgFnSc1d1	neexistující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
20	[number]	k4	20
–	–	k?	–
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
připomenout	připomenout	k5eAaPmF	připomenout
variaci	variace	k1gFnSc4	variace
č.	č.	k?	č.
14	[number]	k4	14
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rytmicky	rytmicky	k6eAd1	rytmicky
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgFnSc1d2	složitější
(	(	kIx(	(
<g/>
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
synkopická	synkopický	k2eAgFnSc1d1	synkopická
úprava	úprava	k1gFnSc1	úprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
permutačním	permutační	k2eAgInSc6d1	permutační
kontrapunktu	kontrapunkt	k1gInSc6	kontrapunkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
21	[number]	k4	21
–	–	k?	–
je	být	k5eAaImIp3nS	být
chromatický	chromatický	k2eAgInSc1d1	chromatický
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
septimy	septima	k1gFnSc2	septima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
22	[number]	k4	22
–	–	k?	–
je	být	k5eAaImIp3nS	být
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
italskou	italský	k2eAgFnSc7d1	italská
vokální	vokální	k2eAgFnSc7d1	vokální
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
fugato	fugato	k1gNnSc1	fugato
(	(	kIx(	(
<g/>
čtyřhlas	čtyřhlas	k1gInSc1	čtyřhlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
23	[number]	k4	23
–	–	k?	–
je	být	k5eAaImIp3nS	být
studií	studie	k1gFnSc7	studie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
efektivně	efektivně	k6eAd1	efektivně
vyřešit	vyřešit	k5eAaPmF	vyřešit
problémy	problém	k1gInPc4	problém
tercií	tercie	k1gFnPc2	tercie
paralelních	paralelní	k2eAgFnPc2d1	paralelní
<g/>
,	,	kIx,	,
v	v	k7c6	v
protipohybu	protipohyb	k1gInSc6	protipohyb
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
dvojitých	dvojitý	k2eAgFnPc2d1	dvojitá
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Bacha	Bacha	k?	Bacha
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
technický	technický	k2eAgInSc1d1	technický
problém	problém	k1gInSc1	problém
samoúčelný	samoúčelný	k2eAgInSc1d1	samoúčelný
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
formotvornou	formotvorný	k2eAgFnSc7d1	formotvorný
součástí	součást	k1gFnSc7	součást
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
24	[number]	k4	24
–	–	k?	–
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
oktávě	oktáva	k1gFnSc6	oktáva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
25	[number]	k4	25
–	–	k?	–
je	být	k5eAaImIp3nS	být
adagio	adagio	k1gNnSc1	adagio
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
duchovní	duchovní	k2eAgFnSc7d1	duchovní
pašijovou	pašijový	k2eAgFnSc7d1	pašijová
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
výrazová	výrazový	k2eAgFnSc1d1	výrazová
stránka	stránka	k1gFnSc1	stránka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
variaci	variace	k1gFnSc6	variace
dominantní	dominantní	k2eAgMnSc1d1	dominantní
<g/>
,	,	kIx,	,
charakterově	charakterově	k6eAd1	charakterově
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
velkou	velký	k2eAgFnSc4d1	velká
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
26	[number]	k4	26
–	–	k?	–
muzikologové	muzikolog	k1gMnPc1	muzikolog
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
původní	původní	k2eAgNnSc4d1	původní
téma	téma	k1gNnSc4	téma
těchto	tento	k3xDgFnPc2	tento
variací	variace	k1gFnPc2	variace
(	(	kIx(	(
<g/>
doprovod	doprovod	k1gInSc1	doprovod
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technickým	technický	k2eAgInSc7d1	technický
problémem	problém	k1gInSc7	problém
pro	pro	k7c4	pro
interpreta	interpret	k1gMnSc4	interpret
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
překřížení	překřížení	k1gNnSc1	překřížení
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
27	[number]	k4	27
–	–	k?	–
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
nóny	nóna	k1gFnSc2	nóna
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
této	tento	k3xDgFnSc2	tento
variace	variace	k1gFnSc2	variace
tvoří	tvořit	k5eAaImIp3nS	tvořit
inverze	inverze	k1gFnSc1	inverze
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
28	[number]	k4	28
–	–	k?	–
řeší	řešit	k5eAaImIp3nS	řešit
efektivním	efektivní	k2eAgInSc7d1	efektivní
způsobem	způsob	k1gInSc7	způsob
problém	problém	k1gInSc1	problém
trylku	trylek	k1gInSc2	trylek
i	i	k9	i
dvojitého	dvojitý	k2eAgNnSc2d1	dvojité
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
trylek	trylek	k1gInSc1	trylek
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nosným	nosný	k2eAgInSc7d1	nosný
výrazovým	výrazový	k2eAgInSc7d1	výrazový
prostředkem	prostředek	k1gInSc7	prostředek
(	(	kIx(	(
<g/>
prvkem	prvek	k1gInSc7	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
29	[number]	k4	29
–	–	k?	–
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
rytmické	rytmický	k2eAgFnSc6d1	rytmická
stránce	stránka	k1gFnSc6	stránka
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
,	,	kIx,	,
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
problém	problém	k1gInSc4	problém
akordického	akordický	k2eAgInSc2d1	akordický
trylku	trylek	k1gInSc2	trylek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variace	variace	k1gFnSc1	variace
č.	č.	k?	č.
30	[number]	k4	30
–	–	k?	–
quodlibet	quodlibet	k1gInSc1	quodlibet
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yRgFnPc4	který
Bach	Bach	k1gMnSc1	Bach
kánonicky	kánonicky	k6eAd1	kánonicky
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
údajně	údajně	k6eAd1	údajně
dvě	dva	k4xCgFnPc1	dva
lidové	lidový	k2eAgFnPc1d1	lidová
písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
harmonickým	harmonický	k2eAgInSc7d1	harmonický
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
<g/>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
opět	opět	k6eAd1	opět
hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
–	–	k?	–
Árií	árie	k1gFnPc2	árie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
interpreta	interpret	k1gMnSc4	interpret
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
velmi	velmi	k6eAd1	velmi
"	"	kIx"	"
<g/>
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
totiž	totiž	k9	totiž
všechny	všechen	k3xTgInPc4	všechen
technické	technický	k2eAgInPc4d1	technický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
obrovský	obrovský	k2eAgInSc4d1	obrovský
citový	citový	k2eAgInSc4d1	citový
vklad	vklad	k1gInSc4	vklad
<g/>
,	,	kIx,	,
pochopení	pochopení	k1gNnSc4	pochopení
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
intelektuální	intelektuální	k2eAgInSc4d1	intelektuální
nadhled	nadhled	k1gInSc4	nadhled
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
časově	časově	k6eAd1	časově
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
rozsah	rozsah	k1gInSc1	rozsah
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kromě	kromě	k7c2	kromě
maximální	maximální	k2eAgFnSc2d1	maximální
koncentrace	koncentrace	k1gFnSc2	koncentrace
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
i	i	k9	i
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
fyzických	fyzický	k2eAgFnPc2d1	fyzická
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
klavíristu	klavírista	k1gMnSc4	klavírista
je	být	k5eAaImIp3nS	být
psychické	psychický	k2eAgNnSc1d1	psychické
a	a	k8xC	a
fyzické	fyzický	k2eAgNnSc1d1	fyzické
napětí	napětí	k1gNnSc1	napětí
při	při	k7c6	při
interpretaci	interpretace	k1gFnSc6	interpretace
Golbergových	Golbergův	k2eAgFnPc2d1	Golbergův
variací	variace	k1gFnPc2	variace
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
při	při	k7c6	při
klavírním	klavírní	k2eAgInSc6d1	klavírní
recitálu	recitál	k1gInSc6	recitál
<g/>
.	.	kIx.	.
</s>
<s>
Ferruccio	Ferruccio	k6eAd1	Ferruccio
Busoni	Buson	k1gMnPc1	Buson
ještě	ještě	k6eAd1	ještě
"	"	kIx"	"
<g/>
upravil	upravit	k5eAaPmAgInS	upravit
<g/>
"	"	kIx"	"
–	–	k?	–
značně	značně	k6eAd1	značně
zkomplikoval	zkomplikovat	k5eAaPmAgMnS	zkomplikovat
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potřebné	potřebný	k2eAgNnSc1d1	potřebné
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
v	v	k7c6	v
Busoniho	Busoni	k1gMnSc2	Busoni
úpravě	úprava	k1gFnSc6	úprava
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Goldbergovy	Goldbergův	k2eAgFnSc2d1	Goldbergova
variace	variace	k1gFnSc2	variace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Goldbergovy	Goldbergův	k2eAgFnPc1d1	Goldbergova
variace	variace	k1gFnPc1	variace
BWV	BWV	kA	BWV
988	[number]	k4	988
Oregon	Oregona	k1gFnPc2	Oregona
Bach	Bacha	k1gFnPc2	Bacha
Festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
Flash	Flash	k1gInSc1	Flash
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
nahrávky	nahrávka	k1gFnPc1	nahrávka
a	a	k8xC	a
partitury	partitura	k1gFnPc1	partitura
Goldbergových	Goldbergův	k2eAgFnPc2d1	Goldbergova
variací	variace	k1gFnPc2	variace
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
