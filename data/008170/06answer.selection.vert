<s>
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
vydal	vydat	k5eAaPmAgMnS	vydat
Goldbergovy	Goldbergův	k2eAgFnPc4d1	Goldbergova
variace	variace	k1gFnPc4	variace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1741	[number]	k4	1741
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Balthasara	Balthasar	k1gMnSc2	Balthasar
Schmida	Schmid	k1gMnSc2	Schmid
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Clavier	Clavier	k1gMnSc1	Clavier
Ubung	Ubung	k1gMnSc1	Ubung
bestehend	bestehend	k1gMnSc1	bestehend
in	in	k?	in
einer	einer	k1gMnSc1	einer
ARIA	ARIA	kA	ARIA
mit	mit	k?	mit
verschiedenen	verschiedenen	k2eAgMnSc1d1	verschiedenen
Veræ	Veræ	k1gMnSc1	Veræ
vors	vorsa	k1gFnPc2	vorsa
Clavicimbal	Clavicimbal	k1gInSc4	Clavicimbal
mit	mit	k?	mit
2	[number]	k4	2
Manualen	Manualen	k2eAgInSc1d1	Manualen
(	(	kIx(	(
<g/>
Klavírní	klavírní	k2eAgNnSc1d1	klavírní
cvičení	cvičení	k1gNnSc1	cvičení
sestávající	sestávající	k2eAgNnSc1d1	sestávající
z	z	k7c2	z
árie	árie	k1gFnSc2	árie
s	s	k7c7	s
rozmanitými	rozmanitý	k2eAgFnPc7d1	rozmanitá
variacemi	variace	k1gFnPc7	variace
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
manuály	manuál	k1gInPc7	manuál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
