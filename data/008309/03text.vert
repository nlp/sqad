<p>
<s>
Arktida	Arktida	k1gFnSc1	Arktida
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
okolo	okolo	k7c2	okolo
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
α	α	k?	α
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
znamená	znamenat	k5eAaImIp3nS	znamenat
medvěd	medvěd	k1gMnSc1	medvěd
a	a	k8xC	a
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
souhvězdí	souhvězdí	k1gNnPc1	souhvězdí
Malý	Malý	k1gMnSc1	Malý
Medvěd	medvěd	k1gMnSc1	medvěd
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Medvědice	medvědice	k1gFnSc1	medvědice
a	a	k8xC	a
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
Polárku	Polárka	k1gFnSc4	Polárka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vymezení	vymezení	k1gNnPc4	vymezení
oblasti	oblast	k1gFnSc2	oblast
==	==	k?	==
</s>
</p>
<p>
<s>
Hranice	hranice	k1gFnSc1	hranice
Arktidy	Arktida	k1gFnSc2	Arktida
lze	lze	k6eAd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
definuje	definovat	k5eAaBmIp3nS	definovat
buď	buď	k8xC	buď
jako	jako	k9	jako
oblast	oblast	k1gFnSc4	oblast
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
66	[number]	k4	66
<g/>
°	°	k?	°
<g/>
32	[number]	k4	32
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
(	(	kIx(	(
<g/>
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
zabírá	zabírat	k5eAaImIp3nS	zabírat
oblast	oblast	k1gFnSc1	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
21,18	[number]	k4	21,18
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
ani	ani	k8xC	ani
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
10	[number]	k4	10
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
takto	takto	k6eAd1	takto
vymezená	vymezený	k2eAgFnSc1d1	vymezená
Arktida	Arktida	k1gFnSc1	Arktida
rozlohu	rozloha	k1gFnSc4	rozloha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
26	[number]	k4	26
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Arktida	Arktida	k1gFnSc1	Arktida
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
oblast	oblast	k1gFnSc1	oblast
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
území	území	k1gNnSc6	území
osmi	osm	k4xCc2	osm
arktických	arktický	k2eAgInPc2d1	arktický
států	stát	k1gInPc2	stát
–	–	k?	–
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
Grónska	Grónsko	k1gNnSc2	Grónsko
(	(	kIx(	(
<g/>
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Laponska	Laponsko	k1gNnSc2	Laponsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
plochy	plocha	k1gFnSc2	plocha
Arktidy	Arktida	k1gFnSc2	Arktida
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
zamrzlý	zamrzlý	k2eAgMnSc1d1	zamrzlý
nebo	nebo	k8xC	nebo
s	s	k7c7	s
plovoucími	plovoucí	k2eAgFnPc7d1	plovoucí
ledovými	ledový	k2eAgFnPc7d1	ledová
krami	kra	k1gFnPc7	kra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
cirkulují	cirkulovat	k5eAaImIp3nP	cirkulovat
kolem	kolem	k7c2	kolem
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
severní	severní	k2eAgInPc4d1	severní
okraje	okraj	k1gInPc4	okraj
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
včetně	včetně	k7c2	včetně
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
arktické	arktický	k2eAgInPc1d1	arktický
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
souostroví	souostroví	k1gNnSc1	souostroví
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Arktické	arktický	k2eAgNnSc1d1	arktické
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
relativním	relativní	k2eAgInSc7d1	relativní
oteplujícím	oteplující	k2eAgInSc7d1	oteplující
vlivem	vliv	k1gInSc7	vliv
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
mořské	mořský	k2eAgInPc1d1	mořský
proudy	proud	k1gInPc1	proud
<g/>
)	)	kIx)	)
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
ochlazujícím	ochlazující	k2eAgInSc7d1	ochlazující
vlivem	vliv	k1gInSc7	vliv
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
pevninské	pevninský	k2eAgInPc1d1	pevninský
ledovce	ledovec	k1gInPc1	ledovec
a	a	k8xC	a
studené	studený	k2eAgNnSc1d1	studené
vnitrozemské	vnitrozemský	k2eAgNnSc1d1	vnitrozemské
klima	klima	k1gNnSc1	klima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
nejzřetelněji	zřetelně	k6eAd3	zřetelně
projevuje	projevovat	k5eAaImIp3nS	projevovat
právě	právě	k9	právě
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tání	tání	k1gNnSc1	tání
mořského	mořský	k2eAgInSc2d1	mořský
kerného	kerný	k2eAgInSc2d1	kerný
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
významně	významně	k6eAd1	významně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
celosvětové	celosvětový	k2eAgFnPc4d1	celosvětová
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
působení	působení	k1gNnSc1	působení
a	a	k8xC	a
tras	tras	k1gInSc1	tras
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
převládá	převládat	k5eAaImIp3nS	převládat
vysoký	vysoký	k2eAgInSc1d1	vysoký
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
Arktická	arktický	k2eAgFnSc1d1	arktická
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
málo	málo	k1gNnSc1	málo
oblačnosti	oblačnost	k1gFnSc2	oblačnost
i	i	k8xC	i
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
ovšem	ovšem	k9	ovšem
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
i	i	k9	i
2	[number]	k4	2
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
tlakové	tlakový	k2eAgFnSc2d1	tlaková
níže	níže	k1gFnSc2	níže
–	–	k?	–
Aleutská	aleutský	k2eAgFnSc1d1	Aleutská
a	a	k8xC	a
Islandská	islandský	k2eAgFnSc1d1	islandská
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zdroji	zdroj	k1gInPc7	zdroj
silného	silný	k2eAgNnSc2d1	silné
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
proudění	proudění	k1gNnSc2	proudění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aleutská	aleutský	k2eAgFnSc1d1	Aleutská
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c4	nad
Aleuty	Aleuty	k1gFnPc4	Aleuty
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
nad	nad	k7c4	nad
nejzazší	zadní	k2eAgInSc4d3	nejzazší
západ	západ	k1gInSc4	západ
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
východ	východ	k1gInSc4	východ
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
posouvá	posouvat	k5eAaImIp3nS	posouvat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Islandská	islandský	k2eAgFnSc1d1	islandská
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
Grónskem	Grónsko	k1gNnSc7	Grónsko
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
nad	nad	k7c7	nad
severní	severní	k2eAgFnSc7d1	severní
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
severní	severní	k2eAgFnSc7d1	severní
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
významně	významně	k6eAd1	významně
slábne	slábnout	k5eAaImIp3nS	slábnout
<g/>
,	,	kIx,	,
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
se	se	k3xPyFc4	se
a	a	k8xC	a
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
se	se	k3xPyFc4	se
na	na	k7c4	na
západ	západ	k1gInSc4	západ
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srážky	srážka	k1gFnPc1	srážka
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
převládajícímu	převládající	k2eAgInSc3d1	převládající
vysokému	vysoký	k2eAgInSc3d1	vysoký
tlaku	tlak	k1gInSc3	tlak
vzduchu	vzduch	k1gInSc2	vzduch
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
všeobecně	všeobecně	k6eAd1	všeobecně
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
sněhových	sněhový	k2eAgFnPc2d1	sněhová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
pólu	pól	k1gInSc6	pól
je	být	k5eAaImIp3nS	být
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
průměr	průměr	k1gInSc4	průměr
149	[number]	k4	149
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Arktidy	Arktida	k1gFnSc2	Arktida
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
výběžek	výběžek	k1gInSc1	výběžek
Grónska	Grónsko	k1gNnSc2	Grónsko
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
1300	[number]	k4	1300
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teplota	teplota	k1gFnSc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Teplotní	teplotní	k2eAgFnPc1d1	teplotní
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jsou	být	k5eAaImIp3nP	být
střední	střední	k2eAgFnPc1d1	střední
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
od	od	k7c2	od
+2	+2	k4	+2
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgInSc1d1	jižní
okraj	okraj	k1gInSc1	okraj
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
)	)	kIx)	)
do	do	k7c2	do
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Arktidě	Arktida	k1gFnSc6	Arktida
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
střední	střední	k2eAgFnPc1d1	střední
červencové	červencový	k2eAgFnPc1d1	červencová
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
+10	+10	k4	+10
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
po	po	k7c4	po
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
teplota	teplota	k1gFnSc1	teplota
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
−	−	k?	−
až	až	k8xS	až
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
+6	+6	k4	+6
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Extrémní	extrémní	k2eAgFnPc1d1	extrémní
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zejména	zejména	k9	zejména
v	v	k7c6	v
subarktických	subarktický	k2eAgFnPc6d1	subarktická
oblastech	oblast	k1gFnPc6	oblast
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
např.	např.	kA	např.
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
východosibiřského	východosibiřský	k2eAgInSc2d1	východosibiřský
Ojmjakonu	Ojmjakon	k1gInSc2	Ojmjakon
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
teplota	teplota	k1gFnSc1	teplota
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
letní	letní	k2eAgNnPc4d1	letní
maxima	maximum	k1gNnPc4	maximum
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
+30	+30	k4	+30
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kanadského	kanadský	k2eAgInSc2d1	kanadský
Snagu	Snag	k1gInSc2	Snag
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
teplota	teplota	k1gFnSc1	teplota
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Arktidy	Arktida	k1gFnSc2	Arktida
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
po	po	k7c4	po
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
Inuité	Inuitý	k2eAgNnSc4d1	Inuité
(	(	kIx(	(
<g/>
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
řeči	řeč	k1gFnSc6	řeč
Lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k8xS	jako
Eskymáci	Eskymák	k1gMnPc1	Eskymák
<g/>
.	.	kIx.	.
</s>
<s>
Naučili	naučit	k5eAaPmAgMnP	naučit
se	se	k3xPyFc4	se
žít	žít	k5eAaImF	žít
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
nehostinných	hostinný	k2eNgInPc6d1	nehostinný
polárních	polární	k2eAgInPc6d1	polární
krajích	kraj	k1gInPc6	kraj
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
na	na	k7c6	na
Aleutských	aleutský	k2eAgInPc6d1	aleutský
ostrovech	ostrov	k1gInPc6	ostrov
žijí	žít	k5eAaImIp3nP	žít
Aleuté	Aleutý	k2eAgFnPc1d1	Aleutý
<g/>
,	,	kIx,	,
Sámové	Sámo	k1gMnPc1	Sámo
(	(	kIx(	(
<g/>
Laponci	Laponec	k1gMnPc1	Laponec
<g/>
)	)	kIx)	)
obývají	obývat	k5eAaImIp3nP	obývat
severní	severní	k2eAgFnSc4d1	severní
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
a	a	k8xC	a
ruský	ruský	k2eAgInSc4d1	ruský
poloostrov	poloostrov	k1gInSc4	poloostrov
Kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
Čukčové	Čukčová	k1gFnSc2	Čukčová
osídlili	osídlit	k5eAaPmAgMnP	osídlit
severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chovají	chovat	k5eAaImIp3nP	chovat
stáda	stádo	k1gNnPc4	stádo
sobů	sob	k1gMnPc2	sob
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgMnPc1d1	polární
Inuité	Inuita	k1gMnPc1	Inuita
<g/>
,	,	kIx,	,
nejseverněji	severně	k6eAd3	severně
žijící	žijící	k2eAgMnPc1d1	žijící
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
obývají	obývat	k5eAaImIp3nP	obývat
zemi	zem	k1gFnSc4	zem
věčného	věčný	k2eAgInSc2d1	věčný
ledu	led	k1gInSc2	led
v	v	k7c6	v
Qaanaaqu	Qaanaaqus	k1gInSc6	Qaanaaqus
v	v	k7c6	v
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
1	[number]	k4	1
600	[number]	k4	600
km	km	kA	km
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
osídlení	osídlení	k1gNnSc6	osídlení
člověkem	člověk	k1gMnSc7	člověk
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgFnSc2d1	nízká
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
osoba	osoba	k1gFnSc1	osoba
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
zejména	zejména	k9	zejména
na	na	k7c4	na
oblasti	oblast	k1gFnPc4	oblast
těžby	těžba	k1gFnSc2	těžba
nerostů	nerost	k1gInPc2	nerost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k8xC	i
města	město	k1gNnSc2	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Murmansk	Murmansk	k1gInSc1	Murmansk
<g/>
,	,	kIx,	,
Norilsk	Norilsk	k1gInSc1	Norilsk
či	či	k8xC	či
Vorkuta	Vorkut	k2eAgFnSc1d1	Vorkuta
<g/>
.	.	kIx.	.
</s>
<s>
Nejseverněji	severně	k6eAd3	severně
položené	položený	k2eAgNnSc1d1	položené
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Ny	Ny	k1gMnSc1	Ny
Alesund	Alesund	k1gMnSc1	Alesund
(	(	kIx(	(
<g/>
78	[number]	k4	78
<g/>
°	°	k?	°
<g/>
56	[number]	k4	56
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
na	na	k7c4	na
Svalbardu	Svalbarda	k1gFnSc4	Svalbarda
<g/>
,	,	kIx,	,
nejsevernější	severní	k2eAgNnSc4d3	nejsevernější
sídlo	sídlo	k1gNnSc4	sídlo
je	být	k5eAaImIp3nS	být
Alert	Alert	k1gInSc1	Alert
(	(	kIx(	(
<g/>
82	[number]	k4	82
<g/>
°	°	k?	°
<g/>
30	[number]	k4	30
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
na	na	k7c6	na
Ellsmerově	Ellsmerův	k2eAgInSc6d1	Ellsmerův
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
osídlení	osídlení	k1gNnSc1	osídlení
Arktidy	Arktida	k1gFnSc2	Arktida
čítá	čítat	k5eAaImIp3nS	čítat
asi	asi	k9	asi
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Arktida	Arktida	k1gFnSc1	Arktida
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
oblast	oblast	k1gFnSc4	oblast
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
nezalesněné	zalesněný	k2eNgFnSc2d1	nezalesněná
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nehostinné	hostinný	k2eNgNnSc4d1	nehostinné
prostředí	prostředí	k1gNnSc4	prostředí
však	však	k9	však
není	být	k5eNaImIp3nS	být
Arktida	Arktida	k1gFnSc1	Arktida
mrtvou	mrtvý	k2eAgFnSc7d1	mrtvá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
překypuje	překypovat	k5eAaImIp3nS	překypovat
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
organismů	organismus	k1gInPc2	organismus
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
ledu	led	k1gInSc6	led
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
pestrou	pestrý	k2eAgFnSc4d1	pestrá
skladbu	skladba	k1gFnSc4	skladba
mořského	mořský	k2eAgInSc2d1	mořský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
mořských	mořský	k2eAgMnPc2d1	mořský
a	a	k8xC	a
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
savců	savec	k1gMnPc2	savec
až	až	k9	až
po	po	k7c4	po
lidské	lidský	k2eAgNnSc4d1	lidské
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Půdy	půda	k1gFnSc2	půda
Arktidy	Arktida	k1gFnSc2	Arktida
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nejseverněji	severně	k6eAd3	severně
položených	položený	k2eAgInPc6d1	položený
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
půdy	půda	k1gFnPc1	půda
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mrazová	mrazový	k2eAgFnSc1d1	mrazová
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
svahové	svahový	k2eAgInPc1d1	svahový
a	a	k8xC	a
soliflukční	soliflukční	k2eAgInPc1d1	soliflukční
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
ptačího	ptačí	k2eAgInSc2d1	ptačí
trusu	trus	k1gInSc2	trus
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
mechy	mech	k1gInPc1	mech
a	a	k8xC	a
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
strukturní	strukturní	k2eAgFnPc4d1	strukturní
půdy	půda	k1gFnPc4	půda
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
díky	díky	k7c3	díky
kryogenním	kryogenní	k2eAgInPc3d1	kryogenní
jevům	jev	k1gInPc3	jev
<g/>
.	.	kIx.	.
</s>
<s>
Jižněji	jižně	k6eAd2	jižně
potom	potom	k6eAd1	potom
málo	málo	k6eAd1	málo
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
tundrové	tundrový	k2eAgFnPc4d1	tundrová
<g/>
,	,	kIx,	,
bažinaté	bažinatý	k2eAgFnPc4d1	bažinatá
<g/>
,	,	kIx,	,
močálové	močálový	k2eAgFnPc4d1	močálová
a	a	k8xC	a
rašelinné	rašelinný	k2eAgFnPc4d1	rašelinná
půdy	půda	k1gFnPc4	půda
s	s	k7c7	s
mechy	mech	k1gInPc7	mech
<g/>
,	,	kIx,	,
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kvetoucími	kvetoucí	k2eAgFnPc7d1	kvetoucí
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
rozšíření	rozšíření	k1gNnSc1	rozšíření
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
permafrost	permafrost	k1gFnSc1	permafrost
<g/>
,	,	kIx,	,
trvale	trvale	k6eAd1	trvale
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
aktivní	aktivní	k2eAgFnSc1d1	aktivní
vrstva	vrstva	k1gFnSc1	vrstva
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roztává	roztávat	k5eAaImIp3nS	roztávat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
1	[number]	k4	1
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
bažina	bažina	k1gFnSc1	bažina
a	a	k8xC	a
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
mírného	mírný	k2eAgInSc2d1	mírný
sklonu	sklon	k1gInSc2	sklon
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
°	°	k?	°
<g/>
-	-	kIx~	-
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
soliflukci	soliflukce	k1gFnSc4	soliflukce
(	(	kIx(	(
<g/>
půdotok	půdotok	k1gInSc4	půdotok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souvislý	souvislý	k2eAgInSc4d1	souvislý
permafrost	permafrost	k1gFnSc4	permafrost
najdeme	najít	k5eAaPmIp1nP	najít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
nesouvislých	souvislý	k2eNgInPc6d1	nesouvislý
pokryvech	pokryv	k1gInPc6	pokryv
ho	on	k3xPp3gInSc2	on
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
až	až	k9	až
u	u	k7c2	u
Bajkalu	Bajkal	k1gInSc2	Bajkal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
arktické	arktický	k2eAgFnSc6d1	arktická
mrazové	mrazový	k2eAgFnSc6d1	mrazová
poušti	poušť	k1gFnSc6	poušť
je	být	k5eAaImIp3nS	být
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
velmi	velmi	k6eAd1	velmi
chudé	chudý	k2eAgNnSc1d1	chudé
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
málo	málo	k4c1	málo
druhů	druh	k1gInPc2	druh
i	i	k9	i
málo	málo	k4c1	málo
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
nalézáme	nalézat	k5eAaImIp1nP	nalézat
kvetoucí	kvetoucí	k2eAgFnSc4d1	kvetoucí
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přechodné	přechodný	k2eAgFnSc6d1	přechodná
zóně	zóna	k1gFnSc6	zóna
pouštní	pouštní	k2eAgFnSc2d1	pouštní
tundry	tundra	k1gFnSc2	tundra
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
mechy	mech	k1gInPc1	mech
<g/>
,	,	kIx,	,
lišejníky	lišejník	k1gInPc1	lišejník
i	i	k8xC	i
kvetoucí	kvetoucí	k2eAgFnPc1d1	kvetoucí
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
vegetace	vegetace	k1gFnSc1	vegetace
je	být	k5eAaImIp3nS	být
však	však	k9	však
přerušovaná	přerušovaný	k2eAgFnSc1d1	přerušovaná
<g/>
,	,	kIx,	,
vzrůst	vzrůst	k1gInSc1	vzrůst
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
například	například	k6eAd1	například
lomikámeny	lomikámen	k2eAgFnPc1d1	lomikámen
<g/>
,	,	kIx,	,
řeřišnice	řeřišnice	k1gFnSc1	řeřišnice
<g/>
,	,	kIx,	,
dryádka	dryádka	k1gFnSc1	dryádka
polární	polární	k2eAgFnSc1d1	polární
<g/>
,	,	kIx,	,
trávnička	trávnička	k1gFnSc1	trávnička
<g/>
,	,	kIx,	,
polární	polární	k2eAgInSc1d1	polární
vřes	vřes	k1gInSc1	vřes
<g/>
,	,	kIx,	,
pryskyřník	pryskyřník	k1gInSc1	pryskyřník
<g/>
,	,	kIx,	,
rdesno	rdesno	k1gNnSc4	rdesno
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
trávy	tráva	k1gFnPc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dřevin	dřevina	k1gFnPc2	dřevina
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
vrbu	vrba	k1gFnSc4	vrba
bylinnou	bylinný	k2eAgFnSc4d1	bylinná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
souvislá	souvislý	k2eAgFnSc1d1	souvislá
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
bohatší	bohatý	k2eAgNnSc4d2	bohatší
druhové	druhový	k2eAgNnSc4d1	druhové
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
např.	např.	kA	např.
břízu	bříza	k1gFnSc4	bříza
zakrslou	zakrslý	k2eAgFnSc7d1	zakrslá
<g/>
,	,	kIx,	,
vrbu	vrba	k1gFnSc4	vrba
laponskou	laponský	k2eAgFnSc4d1	laponská
<g/>
,	,	kIx,	,
pěnišník	pěnišník	k1gInSc4	pěnišník
laponský	laponský	k2eAgInSc4d1	laponský
<g/>
,	,	kIx,	,
zakrslou	zakrslý	k2eAgFnSc4d1	zakrslá
olši	olše	k1gFnSc4	olše
či	či	k8xC	či
zakrslé	zakrslý	k2eAgInPc4d1	zakrslý
jeřáby	jeřáb	k1gInPc4	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
keřovitého	keřovitý	k2eAgInSc2d1	keřovitý
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
do	do	k7c2	do
Arktidy	Arktida	k1gFnSc2	Arktida
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
tajga	tajga	k1gFnSc1	tajga
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
Sewardův	Sewardův	k2eAgInSc1d1	Sewardův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
<g/>
,	,	kIx,	,
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Velkého	velký	k2eAgNnSc2d1	velké
medvědího	medvědí	k2eAgNnSc2d1	medvědí
jezera	jezero	k1gNnSc2	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
Tajmyr	Tajmyr	k1gInSc1	Tajmyr
<g/>
,	,	kIx,	,
ústí	ústit	k5eAaImIp3nS	ústit
řek	řek	k1gMnSc1	řek
Chatanga	Chatanga	k1gFnSc1	Chatanga
<g/>
,	,	kIx,	,
Lena	Lena	k1gFnSc1	Lena
<g/>
,	,	kIx,	,
Kolyma	Kolyma	k1gFnSc1	Kolyma
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
Indigirka	Indigirka	k1gFnSc1	Indigirka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Živočišstvo	živočišstvo	k1gNnSc1	živočišstvo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
chudé	chudý	k2eAgFnSc6d1	chudá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bohatší	bohatý	k2eAgMnPc1d2	bohatší
než	než	k8xS	než
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zcela	zcela	k6eAd1	zcela
chybí	chybět	k5eAaImIp3nP	chybět
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
stromoví	stromový	k2eAgMnPc1d1	stromový
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejseverněji	severně	k6eAd3	severně
žijící	žijící	k2eAgMnPc1d1	žijící
živočichové	živočich	k1gMnPc1	živočich
jsou	být	k5eAaImIp3nP	být
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
a	a	k8xC	a
polární	polární	k2eAgFnSc1d1	polární
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kožešinových	kožešinový	k2eAgMnPc2d1	kožešinový
živočichů	živočich	k1gMnPc2	živočich
zde	zde	k6eAd1	zde
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
hranostaje	hranostaj	k1gMnSc4	hranostaj
<g/>
,	,	kIx,	,
rosomáka	rosomák	k1gMnSc4	rosomák
<g/>
,	,	kIx,	,
lumíky	lumík	k1gMnPc4	lumík
<g/>
,	,	kIx,	,
sviště	svišť	k1gMnPc4	svišť
<g/>
,	,	kIx,	,
polární	polární	k2eAgMnPc4d1	polární
vlky	vlk	k1gMnPc4	vlk
<g/>
,	,	kIx,	,
polární	polární	k2eAgMnPc4d1	polární
zajíce	zajíc	k1gMnPc4	zajíc
či	či	k8xC	či
ovci	ovce	k1gFnSc4	ovce
sněžnou	sněžný	k2eAgFnSc4d1	sněžná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
soby	sob	k1gMnPc4	sob
či	či	k8xC	či
pižmoně	pižmoň	k1gMnPc4	pižmoň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jen	jen	k9	jen
málo	málo	k6eAd1	málo
jich	on	k3xPp3gNnPc2	on
zde	zde	k6eAd1	zde
ale	ale	k9	ale
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
racky	racek	k1gMnPc4	racek
<g/>
,	,	kIx,	,
buřňáky	buřňák	k1gMnPc4	buřňák
<g/>
,	,	kIx,	,
chaluhy	chaluha	k1gFnSc2	chaluha
<g/>
,	,	kIx,	,
rybáka	rybák	k1gMnSc2	rybák
dlouhoocasého	dlouhoocasý	k2eAgMnSc2d1	dlouhoocasý
(	(	kIx(	(
<g/>
migruje	migrovat	k5eAaImIp3nS	migrovat
z	z	k7c2	z
Antarktidy	Antarktida	k1gFnSc2	Antarktida
až	až	k9	až
22	[number]	k4	22
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polární	polární	k2eAgFnSc2d1	polární
kachny	kachna	k1gFnSc2	kachna
<g/>
,	,	kIx,	,
polární	polární	k2eAgFnSc2d1	polární
husy	husa	k1gFnSc2	husa
a	a	k8xC	a
kura	kur	k1gMnSc2	kur
sněžného	sněžný	k2eAgMnSc2d1	sněžný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
komáři	komár	k1gMnPc1	komár
v	v	k7c6	v
bažinatých	bažinatý	k2eAgNnPc6d1	bažinaté
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
vázané	vázaný	k2eAgFnPc1d1	vázaná
živočichy	živočich	k1gMnPc4	živočich
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
3	[number]	k4	3
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
:	:	kIx,	:
sladkovodní	sladkovodní	k2eAgMnSc1d1	sladkovodní
(	(	kIx(	(
<g/>
pstruh	pstruh	k1gMnSc1	pstruh
<g/>
,	,	kIx,	,
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
štika	štika	k1gFnSc1	štika
<g/>
,	,	kIx,	,
lipan	lipan	k1gMnSc1	lipan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mořští	mořský	k2eAgMnPc1d1	mořský
(	(	kIx(	(
<g/>
treska	treska	k1gFnSc1	treska
<g/>
,	,	kIx,	,
sleď	sleď	k1gMnSc1	sleď
<g/>
,	,	kIx,	,
platýs	platýs	k1gMnSc1	platýs
<g/>
,	,	kIx,	,
žralok	žralok	k1gMnSc1	žralok
grónský	grónský	k2eAgMnSc1d1	grónský
<g/>
,	,	kIx,	,
kapelín	kapelín	k1gInSc1	kapelín
<g/>
)	)	kIx)	)
a	a	k8xC	a
velcí	velký	k2eAgMnPc1d1	velký
mořští	mořský	k2eAgMnPc1d1	mořský
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
,	,	kIx,	,
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
,	,	kIx,	,
mroži	mrož	k1gMnPc1	mrož
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
poznávání	poznávání	k1gNnSc2	poznávání
Arktidy	Arktida	k1gFnSc2	Arktida
==	==	k?	==
</s>
</p>
<p>
<s>
Poznávání	poznávání	k1gNnSc1	poznávání
Arktidy	Arktida	k1gFnSc2	Arktida
začalo	začít	k5eAaPmAgNnS	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
330	[number]	k4	330
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Pýtheás	Pýtheás	k1gInSc1	Pýtheás
z	z	k7c2	z
Massalie	Massalie	k1gFnSc2	Massalie
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
Thule	Thule	k1gFnSc2	Thule
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
za	za	k7c4	za
zemi	zem	k1gFnSc4	zem
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
"	"	kIx"	"
<g/>
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvními	první	k4xOgMnPc7	první
skutečnými	skutečný	k2eAgMnPc7d1	skutečný
objeviteli	objevitel	k1gMnPc7	objevitel
byli	být	k5eAaImAgMnP	být
ovšem	ovšem	k9	ovšem
až	až	k9	až
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
850	[number]	k4	850
n.	n.	k?	n.
l.	l.	k?	l.
objevili	objevit	k5eAaPmAgMnP	objevit
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
865	[number]	k4	865
objevili	objevit	k5eAaPmAgMnP	objevit
a	a	k8xC	a
osídlili	osídlit	k5eAaPmAgMnP	osídlit
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
Gunnbjörn	Gunnbjörn	k1gInSc1	Gunnbjörn
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Grónska	Grónsko	k1gNnSc2	Grónsko
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
sem	sem	k6eAd1	sem
roku	rok	k1gInSc2	rok
981	[number]	k4	981
se	s	k7c7	s
14	[number]	k4	14
loděmi	loď	k1gFnPc7	loď
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Erik	Erik	k1gMnSc1	Erik
Rudý	rudý	k1gMnSc1	rudý
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1001	[number]	k4	1001
se	se	k3xPyFc4	se
Leif	Leif	k1gMnSc1	Leif
Eriksson	Eriksson	k1gMnSc1	Eriksson
vylodil	vylodit	k5eAaPmAgMnS	vylodit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Labradoru	Labrador	k1gInSc2	Labrador
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1114	[number]	k4	1114
objevili	objevit	k5eAaPmAgMnP	objevit
souostroví	souostroví	k1gNnSc4	souostroví
Svalbard	Svalbarda	k1gFnPc2	Svalbarda
(	(	kIx(	(
<g/>
Špicberky	Špicberky	k1gFnPc1	Špicberky
<g/>
;	;	kIx,	;
dánsky	dánsky	k6eAd1	dánsky
"	"	kIx"	"
<g/>
Holé	Holé	k2eAgNnSc1d1	Holé
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zamrzal	zamrzat	k5eAaImAgMnS	zamrzat
severní	severní	k2eAgInSc4d1	severní
Atlantik	Atlantik	k1gInSc4	Atlantik
(	(	kIx(	(
<g/>
od	od	k7c2	od
Islandu	Island	k1gInSc2	Island
ke	k	k7c3	k
Grónsku	Grónsko	k1gNnSc3	Grónsko
byl	být	k5eAaImAgInS	být
souvislý	souvislý	k2eAgInSc1d1	souvislý
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
úpadek	úpadek	k1gInSc4	úpadek
a	a	k8xC	a
zánik	zánik	k1gInSc4	zánik
vikinských	vikinský	k2eAgFnPc2d1	vikinská
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
a	a	k8xC	a
Newfoundlandu	Newfoundlando	k1gNnSc6	Newfoundlando
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgMnPc7d1	další
objeviteli	objevitel	k1gMnPc7	objevitel
v	v	k7c6	v
arktické	arktický	k2eAgFnSc6d1	arktická
oblasti	oblast	k1gFnSc6	oblast
byli	být	k5eAaImAgMnP	být
především	především	k9	především
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
jižní	jižní	k2eAgNnSc4d1	jižní
Grónsko	Grónsko	k1gNnSc4	Grónsko
<g/>
,	,	kIx,	,
Newfoundland	Newfoundland	k1gInSc4	Newfoundland
a	a	k8xC	a
Labrador	Labrador	k1gInSc4	Labrador
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hledání	hledání	k1gNnSc1	hledání
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
cesty	cesta	k1gFnSc2	cesta
===	===	k?	===
</s>
</p>
<p>
<s>
Hledání	hledání	k1gNnSc1	hledání
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
průjezdu	průjezd	k1gInSc2	průjezd
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
zejména	zejména	k9	zejména
z	z	k7c2	z
ekonomických	ekonomický	k2eAgNnPc2d1	ekonomické
hledisek	hledisko	k1gNnPc2	hledisko
kratšího	krátký	k2eAgNnSc2d2	kratší
námořního	námořní	k2eAgNnSc2d1	námořní
spojení	spojení	k1gNnSc2	spojení
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
začaly	začít	k5eAaPmAgFnP	začít
již	již	k6eAd1	již
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
vrátil	vrátit	k5eAaPmAgMnS	vrátit
např.	např.	kA	např.
Holanďan	Holanďan	k1gMnSc1	Holanďan
Willem	Will	k1gInSc7	Will
Barents	Barentsa	k1gFnPc2	Barentsa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
plout	plout	k5eAaImF	plout
přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
pól	pól	k1gInSc4	pól
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
Rus	Rus	k1gMnSc1	Rus
Semjon	Semjon	k1gMnSc1	Semjon
Děžňov	Děžňov	k1gInSc4	Děžňov
plující	plující	k2eAgInSc4d1	plující
z	z	k7c2	z
ústí	ústí	k1gNnPc2	ústí
Kolymy	Kolyma	k1gFnSc2	Kolyma
objevil	objevit	k5eAaPmAgInS	objevit
nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
obeplul	obeplout	k5eAaPmAgMnS	obeplout
ho	on	k3xPp3gInSc4	on
(	(	kIx(	(
<g/>
Děžňovův	Děžňovův	k2eAgInSc4d1	Děžňovův
mys	mys	k1gInSc4	mys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
průlivem	průliv	k1gInSc7	průliv
však	však	k9	však
neproplul	proplout	k5eNaPmAgMnS	proplout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
průliv	průliv	k1gInSc4	průliv
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgInS	objevit
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
1730	[number]	k4	1730
Dán	dát	k5eAaPmNgMnS	dát
v	v	k7c6	v
ruských	ruský	k2eAgFnPc6d1	ruská
službách	služba	k1gFnPc6	služba
Vitus	Vitus	k1gMnSc1	Vitus
Bering	Bering	k1gMnSc1	Bering
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
jméno	jméno	k1gNnSc1	jméno
dnes	dnes	k6eAd1	dnes
průliv	průliv	k1gInSc1	průliv
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
celkový	celkový	k2eAgInSc4d1	celkový
průjezd	průjezd	k1gInSc4	průjezd
severovýchodní	severovýchodní	k2eAgFnSc7d1	severovýchodní
cestou	cesta	k1gFnSc7	cesta
vykonala	vykonat	k5eAaPmAgFnS	vykonat
v	v	k7c6	v
letech	let	k1gInPc6	let
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
švédská	švédský	k2eAgFnSc1d1	švédská
výprava	výprava	k1gFnSc1	výprava
lodi	loď	k1gFnSc2	loď
Vega	Vega	k1gFnSc1	Vega
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
A.	A.	kA	A.
E.	E.	kA	E.
Nordenskjöld	Nordenskjöld	k1gMnSc1	Nordenskjöld
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hledání	hledání	k1gNnSc1	hledání
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
cesty	cesta	k1gFnSc2	cesta
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
průjezdu	průjezd	k1gInSc2	průjezd
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
mnoho	mnoho	k4c1	mnoho
neúspěšných	úspěšný	k2eNgFnPc2d1	neúspěšná
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
objevitelé	objevitel	k1gMnPc1	objevitel
jako	jako	k8xC	jako
např.	např.	kA	např.
Martin	Martin	k1gInSc4	Martin
Frobisher	Frobishra	k1gFnPc2	Frobishra
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Hudson	Hudson	k1gMnSc1	Hudson
<g/>
,	,	kIx,	,
James	James	k1gInSc1	James
Clark	Clark	k1gInSc1	Clark
Ross	Ross	k1gInSc4	Ross
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
objevil	objevit	k5eAaPmAgInS	objevit
severní	severní	k2eAgInSc1d1	severní
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Franklin	Franklina	k1gFnPc2	Franklina
<g/>
,	,	kIx,	,
McClure	McClur	k1gMnSc5	McClur
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
výpravou	výprava	k1gFnSc7	výprava
byla	být	k5eAaImAgFnS	být
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
výprava	výprava	k1gFnSc1	výprava
Roalda	Roalda	k1gFnSc1	Roalda
Amundsena	Amundsen	k2eAgFnSc1d1	Amundsena
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
motorové	motorový	k2eAgFnSc6d1	motorová
plachetnici	plachetnice	k1gFnSc6	plachetnice
Gjø	Gjø	k1gFnSc2	Gjø
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
sice	sice	k8xC	sice
musela	muset	k5eAaImAgFnS	muset
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
přezimovat	přezimovat	k5eAaBmF	přezimovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
cestou	cesta	k1gFnSc7	cesta
proplout	proplout	k5eAaPmF	proplout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dobývání	dobývání	k1gNnSc6	dobývání
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
===	===	k?	===
</s>
</p>
<p>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
dobytí	dobytí	k1gNnSc4	dobytí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
vyniká	vynikat	k5eAaImIp3nS	vynikat
pokus	pokus	k1gInSc1	pokus
Fridtjofa	Fridtjof	k1gMnSc2	Fridtjof
Nansena	Nansen	k2eAgFnSc1d1	Nansena
o	o	k7c4	o
driftování	driftování	k1gNnSc4	driftování
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zkonstruovanou	zkonstruovaný	k2eAgFnSc7d1	zkonstruovaná
lodí	loď	k1gFnSc7	loď
Fram	Frama	k1gFnPc2	Frama
<g/>
,	,	kIx,	,
skončily	skončit	k5eAaPmAgFnP	skončit
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dobytí	dobytí	k1gNnSc1	dobytí
přiznáno	přiznán	k2eAgNnSc1d1	přiznáno
Robertu	Roberta	k1gFnSc4	Roberta
Pearymu	Pearym	k1gInSc2	Pearym
(	(	kIx(	(
<g/>
a	a	k8xC	a
neuznáno	uznán	k2eNgNnSc1d1	neuznáno
F.	F.	kA	F.
Cookovi	Cooek	k1gMnSc6	Cooek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgInSc7d1	přímý
důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
Amundsenův	Amundsenův	k2eAgInSc1d1	Amundsenův
vítězný	vítězný	k2eAgInSc1d1	vítězný
souboj	souboj	k1gInSc1	souboj
o	o	k7c4	o
jižní	jižní	k2eAgInSc4d1	jižní
pól	pól	k1gInSc4	pól
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Roald	Roald	k1gInSc1	Roald
Amundsen	Amundsen	k1gInSc1	Amundsen
byl	být	k5eAaImAgInS	být
také	také	k9	také
velitelem	velitel	k1gMnSc7	velitel
výpravy	výprava	k1gFnSc2	výprava
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
Norge	Norge	k1gFnSc7	Norge
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
zpochybněným	zpochybněný	k2eAgFnPc3d1	zpochybněná
Pearyho	Pearyha	k1gFnSc5	Pearyha
důkazům	důkaz	k1gInPc3	důkaz
dnes	dnes	k6eAd1	dnes
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
první	první	k4xOgNnSc4	první
doložené	doložený	k2eAgNnSc4d1	doložené
dosažení	dosažení	k1gNnSc4	dosažení
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soupeření	soupeření	k1gNnPc1	soupeření
velmocí	velmoc	k1gFnPc2	velmoc
===	===	k?	===
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
strategickou	strategický	k2eAgFnSc4d1	strategická
i	i	k8xC	i
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přelet	přelet	k1gInSc1	přelet
přes	přes	k7c4	přes
Arktidu	Arktida	k1gFnSc4	Arktida
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
spojnicí	spojnice	k1gFnSc7	spojnice
mezi	mezi	k7c7	mezi
soupeřícími	soupeřící	k2eAgFnPc7d1	soupeřící
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechala	nechat	k5eNaPmAgNnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
také	také	k9	také
otázka	otázka	k1gFnSc1	otázka
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ložiska	ložisko	k1gNnPc4	ložisko
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgInPc4	jenž
jeví	jevit	k5eAaImIp3nS	jevit
zájem	zájem	k1gInSc1	zájem
všechny	všechen	k3xTgInPc4	všechen
státy	stát	k1gInPc4	stát
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Rusko	Rusko	k1gNnSc1	Rusko
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
jeho	jeho	k3xOp3gInPc4	jeho
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
až	až	k9	až
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
pólu	pólo	k1gNnSc3	pólo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
změny	změna	k1gFnPc1	změna
==	==	k?	==
</s>
</p>
<p>
<s>
Mořský	mořský	k2eAgInSc1d1	mořský
led	led	k1gInSc1	led
se	se	k3xPyFc4	se
prvně	prvně	k?	prvně
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
Arktidě	Arktida	k1gFnSc6	Arktida
asi	asi	k9	asi
před	před	k7c7	před
47	[number]	k4	47
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
2	[number]	k4	2
až	až	k8xS	až
3	[number]	k4	3
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
Arktidu	Arktida	k1gFnSc4	Arktida
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc1d1	zásadní
dopad	dopad	k1gInSc1	dopad
globální	globální	k2eAgInSc1d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
roztáváním	roztávání	k1gNnSc7	roztávání
ledovců	ledovec	k1gInPc2	ledovec
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
za	za	k7c4	za
poslední	poslední	k2eAgNnPc4d1	poslední
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Klimatické	klimatický	k2eAgInPc1d1	klimatický
modely	model	k1gInPc1	model
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgNnSc4d2	veliký
oteplování	oteplování	k1gNnSc4	oteplování
u	u	k7c2	u
pólů	pól	k1gInPc2	pól
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
globální	globální	k2eAgInSc4d1	globální
průměr	průměr	k1gInSc4	průměr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
velkého	velký	k2eAgInSc2d1	velký
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc1	úbytek
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
není	být	k5eNaImIp3nS	být
rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
<g/>
.	.	kIx.	.
</s>
<s>
Tání	tání	k1gNnSc1	tání
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
ledu	led	k1gInSc2	led
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
však	však	k9	však
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
klimatických	klimatický	k2eAgInPc2d1	klimatický
modelů	model	k1gInPc2	model
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
sezónnímu	sezónní	k2eAgNnSc3d1	sezónní
roztání	roztání	k1gNnSc3	roztání
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
dojde	dojít	k5eAaPmIp3nS	dojít
v	v	k7c4	v
září	září	k1gNnSc4	září
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2040	[number]	k4	2040
a	a	k8xC	a
2100	[number]	k4	2100
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
analyzovaných	analyzovaný	k2eAgInPc2d1	analyzovaný
modelů	model	k1gInPc2	model
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
téměř	téměř	k6eAd1	téměř
úplné	úplný	k2eAgNnSc4d1	úplné
roztání	roztání	k1gNnSc4	roztání
v	v	k7c6	v
září	září	k1gNnSc6	září
2100	[number]	k4	2100
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
úbytek	úbytek	k1gInSc1	úbytek
ledu	led	k1gInSc2	led
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
obavě	obava	k1gFnSc3	obava
z	z	k7c2	z
uvolnění	uvolnění	k1gNnSc2	uvolnění
arktického	arktický	k2eAgInSc2d1	arktický
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Uvolnění	uvolnění	k1gNnSc1	uvolnění
methanu	methan	k1gInSc2	methan
z	z	k7c2	z
arktického	arktický	k2eAgInSc2d1	arktický
permafrostu	permafrost	k1gInSc2	permafrost
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
náhlému	náhlý	k2eAgNnSc3d1	náhlé
a	a	k8xC	a
vážnému	vážný	k2eAgNnSc3d1	vážné
globálnímu	globální	k2eAgNnSc3d1	globální
oteplení	oteplení	k1gNnSc3	oteplení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
methan	methan	k1gInSc1	methan
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
skleníkové	skleníkový	k2eAgInPc4d1	skleníkový
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Uvolnění	uvolnění	k1gNnSc1	uvolnění
methanu	methan	k1gInSc2	methan
je	být	k5eAaImIp3nS	být
spojováno	spojován	k2eAgNnSc1d1	spojováno
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
vymíráním	vymírání	k1gNnSc7	vymírání
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
permu	perm	k1gInSc2	perm
a	a	k8xC	a
triasu	trias	k1gInSc2	trias
<g/>
.	.	kIx.	.
</s>
<s>
Uvolňování	uvolňování	k1gNnSc1	uvolňování
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
arktických	arktický	k2eAgNnPc2d1	arktické
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
však	však	k9	však
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Arktidy	Arktida	k1gFnSc2	Arktida
je	být	k5eAaImIp3nS	být
ohniskem	ohnisko	k1gNnSc7	ohnisko
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
politického	politický	k2eAgInSc2d1	politický
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
měřítku	měřítko	k1gNnSc6	měřítko
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
10	[number]	k4	10
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
arktický	arktický	k2eAgInSc1d1	arktický
vědecký	vědecký	k2eAgInSc1d1	vědecký
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
IASC	IASC	kA	IASC
<g/>
)	)	kIx)	)
a	a	k8xC	a
stovky	stovka	k1gFnPc4	stovka
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
specialistů	specialista	k1gMnPc2	specialista
"	"	kIx"	"
<g/>
Arktické	arktický	k2eAgFnSc2d1	arktická
rady	rada	k1gFnSc2	rada
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
neustále	neustále	k6eAd1	neustále
získávat	získávat	k5eAaImF	získávat
co	co	k9	co
nejpřesnější	přesný	k2eAgFnPc4d3	nejpřesnější
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územní	územní	k2eAgInPc1d1	územní
nároky	nárok	k1gInPc1	nárok
===	===	k?	===
</s>
</p>
<p>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
geografický	geografický	k2eAgInSc4d1	geografický
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
nebo	nebo	k8xC	nebo
oblast	oblast	k1gFnSc4	oblast
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgInPc1d1	okolní
arktické	arktický	k2eAgInPc1d1	arktický
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
hraničí	hraničit	k5eAaImIp3nP	hraničit
se	s	k7c7	s
Severním	severní	k2eAgInSc7d1	severní
ledovým	ledový	k2eAgInSc7d1	ledový
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Grónsko	Grónsko	k1gNnSc4	Grónsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
370	[number]	k4	370
km	km	kA	km
zónou	zóna	k1gFnSc7	zóna
okolo	okolo	k7c2	okolo
jejich	jejich	k3xOp3gNnSc2	jejich
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
Úmluvy	úmluva	k1gFnSc2	úmluva
OSN	OSN	kA	OSN
o	o	k7c6	o
mořském	mořský	k2eAgNnSc6d1	mořské
právu	právo	k1gNnSc6	právo
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
10	[number]	k4	10
let	léto	k1gNnPc2	léto
na	na	k7c6	na
podání	podání	k1gNnSc6	podání
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
o	o	k7c4	o
zónu	zóna	k1gFnSc4	zóna
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
370	[number]	k4	370
km	km	kA	km
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
Norsko	Norsko	k1gNnSc4	Norsko
(	(	kIx(	(
<g/>
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
ratifikovala	ratifikovat	k5eAaBmAgFnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
i	i	k8xC	i
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
projekty	projekt	k1gInPc1	projekt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
požádaly	požádat	k5eAaPmAgFnP	požádat
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
ruské	ruský	k2eAgInPc1d1	ruský
batyskafy	batyskaf	k1gInPc1	batyskaf
MIR-1	MIR-1	k1gMnPc2	MIR-1
a	a	k8xC	a
MIR-2	MIR-2	k1gMnPc2	MIR-2
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ponořily	ponořit	k5eAaPmAgFnP	ponořit
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
oceánu	oceán	k1gInSc2	oceán
pod	pod	k7c7	pod
severním	severní	k2eAgInSc7d1	severní
pólem	pól	k1gInSc7	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umístily	umístit	k5eAaPmAgInP	umístit
ruskou	ruský	k2eAgFnSc4d1	ruská
vlajku	vlajka	k1gFnSc4	vlajka
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
z	z	k7c2	z
nerezavějící	rezavějící	k2eNgFnSc2d1	nerezavějící
titanové	titanový	k2eAgFnSc2d1	titanová
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
expedicí	expedice	k1gFnSc7	expedice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umístění	umístění	k1gNnSc1	umístění
vlajky	vlajka	k1gFnSc2	vlajka
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
starosti	starost	k1gFnPc4	starost
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
o	o	k7c6	o
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
ropných	ropný	k2eAgNnPc2d1	ropné
ložisek	ložisko	k1gNnPc2	ložisko
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministři	ministr	k1gMnPc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
oficiální	oficiální	k2eAgMnPc1d1	oficiální
zástupci	zástupce	k1gMnPc1	zástupce
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
USA	USA	kA	USA
se	s	k7c7	s
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
setkali	setkat	k5eAaPmAgMnP	setkat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ilulissat	Ilulissat	k1gFnSc2	Ilulissat
na	na	k7c6	na
Arkticko-oceánské	arktickoceánský	k2eAgFnSc6d1	arkticko-oceánský
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
Ilulissatskou	Ilulissatský	k2eAgFnSc4d1	Ilulissatský
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
Arktidy	Arktida	k1gFnSc2	Arktida
rozsáhle	rozsáhle	k6eAd1	rozsáhle
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
a	a	k8xC	a
ruskými	ruský	k2eAgMnPc7d1	ruský
polárníky	polárník	k1gMnPc7	polárník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1937	[number]	k4	1937
a	a	k8xC	a
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
88	[number]	k4	88
polárních	polární	k2eAgFnPc2d1	polární
posádek	posádka	k1gFnPc2	posádka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pracovaly	pracovat	k5eAaImAgFnP	pracovat
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
stanicích	stanice	k1gFnPc6	stanice
budovaných	budovaný	k2eAgFnPc6d1	budovaná
na	na	k7c6	na
plovoucím	plovoucí	k2eAgInSc6d1	plovoucí
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stanice	stanice	k1gFnPc1	stanice
putovaly	putovat	k5eAaImAgFnP	putovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
tisíce	tisíc	k4xCgInPc1	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
po	po	k7c6	po
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Arktické	arktický	k2eAgFnSc2d1	arktická
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Baffinův	Baffinův	k2eAgInSc1d1	Baffinův
záliv	záliv	k1gInSc1	záliv
</s>
</p>
<p>
<s>
Barentsovo	Barentsův	k2eAgNnSc1d1	Barentsovo
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Beaufortovo	Beaufortův	k2eAgNnSc1d1	Beaufortovo
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Beringovo	Beringův	k2eAgNnSc1d1	Beringovo
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Beringův	Beringův	k2eAgInSc1d1	Beringův
průliv	průliv	k1gInSc1	průliv
</s>
</p>
<p>
<s>
Čukotské	čukotský	k2eAgNnSc1d1	Čukotské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Dánský	dánský	k2eAgInSc1d1	dánský
průliv	průliv	k1gInSc1	průliv
</s>
</p>
<p>
<s>
Davisův	Davisův	k2eAgInSc1d1	Davisův
průliv	průliv	k1gInSc1	průliv
</s>
</p>
<p>
<s>
Grónské	grónský	k2eAgNnSc1d1	grónské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Hudsonův	Hudsonův	k2eAgInSc1d1	Hudsonův
záliv	záliv	k1gInSc1	záliv
</s>
</p>
<p>
<s>
Karské	karský	k2eAgNnSc1d1	Karské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Moře	moře	k1gNnSc1	moře
Laptěvů	Laptěv	k1gInPc2	Laptěv
</s>
</p>
<p>
<s>
Norské	norský	k2eAgNnSc1d1	norské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Průliv	průliv	k1gInSc1	průliv
Nares	Naresa	k1gFnPc2	Naresa
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
</s>
</p>
<p>
<s>
Východosibiřské	východosibiřský	k2eAgNnSc1d1	Východosibiřské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
==	==	k?	==
Arktické	arktický	k2eAgFnSc2d1	arktická
krajiny	krajina	k1gFnSc2	krajina
==	==	k?	==
</s>
</p>
<p>
<s>
Aleuty	Aleuty	k1gFnPc1	Aleuty
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
/	/	kIx~	/
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
/	/	kIx~	/
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kanadské	kanadský	k2eAgNnSc1d1	kanadské
arktické	arktický	k2eAgNnSc1d1	arktické
souostroví	souostroví	k1gNnSc1	souostroví
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diomédovy	Diomédův	k2eAgInPc1d1	Diomédův
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
/	/	kIx~	/
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finnmark	Finnmark	k1gInSc1	Finnmark
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mayen	Mayen	k2eAgMnSc1d1	Mayen
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Medvědí	medvědí	k2eAgInSc1d1	medvědí
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Newfoundland	Newfoundland	k1gInSc1	Newfoundland
a	a	k8xC	a
Labrador	Labrador	k1gInSc1	Labrador
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Novosibiřské	Novosibiřský	k2eAgInPc1d1	Novosibiřský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nunavik	Nunavik	k1gInSc1	Nunavik
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nunavut	Nunavut	k1gInSc1	Nunavut
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ruské	ruský	k2eAgInPc1d1	ruský
arktické	arktický	k2eAgInPc1d1	arktický
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Severozápadní	severozápadní	k2eAgNnPc1d1	severozápadní
teritoria	teritorium	k1gNnPc1	teritorium
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sibiř	Sibiř	k1gFnSc1	Sibiř
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Špicberky	Špicberky	k1gFnPc1	Špicberky
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vajgač	Vajgač	k1gInSc1	Vajgač
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Yukon	Yukon	k1gNnSc1	Yukon
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wrangelův	Wrangelův	k2eAgInSc1d1	Wrangelův
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
==	==	k?	==
</s>
</p>
<p>
<s>
Plošina	plošina	k1gFnSc1	plošina
Prirazlomnaja	Prirazlomnaja	k1gFnSc1	Prirazlomnaja
/	/	kIx~	/
GazpromGazprom	GazpromGazprom	k1gInSc1	GazpromGazprom
(	(	kIx(	(
<g/>
společnost	společnost	k1gFnSc1	společnost
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
oznámila	oznámit	k5eAaPmAgFnS	oznámit
stav	stav	k1gInSc4	stav
připravenosti	připravenost	k1gFnSc2	připravenost
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
těžby	těžba	k1gFnSc2	těžba
ropy	ropa	k1gFnSc2	ropa
na	na	k7c4	na
Prirazlomnaja	Prirazlomnajum	k1gNnPc4	Prirazlomnajum
v	v	k7c6	v
Barentsově	barentsově	k6eAd1	barentsově
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
o	o	k7c4	o
první	první	k4xOgNnSc4	první
naleziště	naleziště	k1gNnSc4	naleziště
na	na	k7c6	na
arktickém	arktický	k2eAgInSc6d1	arktický
šelfu	šelf	k1gInSc6	šelf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
6	[number]	k4	6
aktivistů	aktivista	k1gMnPc2	aktivista
včele	včela	k1gFnSc6	včela
s	s	k7c7	s
výkonným	výkonný	k2eAgMnSc7d1	výkonný
ředitelem	ředitel	k1gMnSc7	ředitel
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
International	International	k1gFnPc2	International
Kumi	kum	k1gMnPc1	kum
Naidoo	Naidoo	k6eAd1	Naidoo
připoutalo	připoutat	k5eAaPmAgNnS	připoutat
ke	k	k7c3	k
stěně	stěna	k1gFnSc3	stěna
plošiny	plošina	k1gFnSc2	plošina
Prirazlomnaja	Prirazlomnaj	k1gInSc2	Prirazlomnaj
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
protestujících	protestující	k2eAgMnPc2d1	protestující
nebyl	být	k5eNaImAgMnS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
Gazprom	Gazprom	k1gInSc1	Gazprom
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
Prirazlomnaja	Prirazlomnaj	k1gInSc2	Prirazlomnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arktická	arktický	k2eAgFnSc1d1	arktická
třicítka	třicítka	k1gFnSc1	třicítka
<g/>
/	/	kIx~	/
Arctic	Arctice	k1gFnPc2	Arctice
3018	[number]	k4	3018
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
organizace	organizace	k1gFnSc1	organizace
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
plošině	plošina	k1gFnSc3	plošina
Prirazlomnaja	Prirazlomnajum	k1gNnSc2	Prirazlomnajum
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
novináři	novinář	k1gMnPc7	novinář
a	a	k8xC	a
28	[number]	k4	28
aktivisty	aktivista	k1gMnPc7	aktivista
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
informovat	informovat	k5eAaBmF	informovat
svět	svět	k1gInSc4	svět
o	o	k7c6	o
nebezpečích	nebezpečí	k1gNnPc6	nebezpečí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
Arktidu	Arktida	k1gFnSc4	Arktida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
lodi	loď	k1gFnSc2	loď
Arctic	Arctice	k1gFnPc2	Arctice
Sunrise	Sunrise	k1gFnSc2	Sunrise
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
pohraničníci	pohraničník	k1gMnPc1	pohraničník
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obsadili	obsadit	k5eAaPmAgMnP	obsadit
bez	bez	k7c2	bez
varovného	varovný	k2eAgInSc2d1	varovný
výstřelu	výstřel	k1gInSc2	výstřel
v	v	k7c6	v
Pečorském	Pečorský	k2eAgNnSc6d1	Pečorský
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
posádka	posádka	k1gFnSc1	posádka
nenásilně	násilně	k6eNd1	násilně
zvedala	zvedat	k5eAaImAgFnS	zvedat
ruce	ruka	k1gFnPc4	ruka
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nenásilném	násilný	k2eNgInSc6d1	nenásilný
protestu	protest	k1gInSc6	protest
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
Prirazlomnaja	Prirazlomnajus	k1gMnSc2	Prirazlomnajus
<g/>
,	,	kIx,	,
pohraničníci	pohraničník	k1gMnPc1	pohraničník
na	na	k7c4	na
posádku	posádka	k1gFnSc4	posádka
lodě	loď	k1gFnSc2	loď
Arctic	Arctice	k1gFnPc2	Arctice
Sunrise	Sunrise	k1gFnSc2	Sunrise
mířili	mířit	k5eAaImAgMnP	mířit
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
noži	nůž	k1gInPc7	nůž
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
opustil	opustit	k5eAaPmAgMnS	opustit
loď	loď	k1gFnSc4	loď
Arctic	Arctice	k1gFnPc2	Arctice
Sunrise	Sunrise	k1gFnSc2	Sunrise
kapitán	kapitán	k1gMnSc1	kapitán
lodi	loď	k1gFnSc2	loď
američan	američan	k1gMnSc1	američan
Peter	Peter	k1gMnSc1	Peter
Wilcox	Wilcox	k1gInSc4	Wilcox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byla	být	k5eAaImAgFnS	být
Arktická	arktický	k2eAgFnSc1d1	arktická
třicítka	třicítka	k1gFnSc1	třicítka
<g/>
/	/	kIx~	/
Arctic	Arctice	k1gFnPc2	Arctice
30	[number]	k4	30
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
pirátství	pirátství	k1gNnSc2	pirátství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Murmansku	Murmansk	k1gInSc6	Murmansk
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
lodi	loď	k1gFnSc2	loď
Arctic	Arctice	k1gFnPc2	Arctice
Sunrise	Sunrise	k1gFnSc2	Sunrise
vsazeni	vsadit	k5eAaPmNgMnP	vsadit
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
následně	následně	k6eAd1	následně
postupně	postupně	k6eAd1	postupně
všichni	všechen	k3xTgMnPc1	všechen
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
pirátství	pirátství	k1gNnSc2	pirátství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
týdnech	týden	k1gInPc6	týden
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
Murmansku	Murmansk	k1gInSc2	Murmansk
byli	být	k5eAaImAgMnP	být
dvěma	dva	k4xCgFnPc7	dva
novináři	novinář	k1gMnPc7	novinář
a	a	k8xC	a
28	[number]	k4	28
aktivistů	aktivista	k1gMnPc2	aktivista
obviněno	obvinit	k5eAaPmNgNnS	obvinit
z	z	k7c2	z
výtržnictví	výtržnictví	k1gNnSc2	výtržnictví
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
taky	taky	k9	taky
závažné	závažný	k2eAgNnSc4d1	závažné
obvinění	obvinění	k1gNnSc4	obvinění
se	s	k7c7	s
sazbou	sazba	k1gFnSc7	sazba
až	až	k9	až
7	[number]	k4	7
let	léto	k1gNnPc2	léto
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
pirátství	pirátství	k1gNnSc2	pirátství
bylo	být	k5eAaImAgNnS	být
zmírněno	zmírnit	k5eAaPmNgNnS	zmírnit
na	na	k7c4	na
výtržnictví	výtržnictví	k1gNnSc4	výtržnictví
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
propuštěni	propustit	k5eAaPmNgMnP	propustit
z	z	k7c2	z
vazební	vazební	k2eAgFnSc2d1	vazební
věznice	věznice	k1gFnSc2	věznice
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
opustit	opustit	k5eAaPmF	opustit
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
prezident	prezident	k1gMnSc1	prezident
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
amnestii	amnestie	k1gFnSc3	amnestie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
trestní	trestní	k2eAgNnSc1d1	trestní
stíhání	stíhání	k1gNnSc1	stíhání
28	[number]	k4	28
aktivistů	aktivista	k1gMnPc2	aktivista
a	a	k8xC	a
2	[number]	k4	2
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
na	na	k7c6	na
plošině	plošina	k1gFnSc6	plošina
Prirazlomnaja	Prirazlomnajum	k1gNnSc2	Prirazlomnajum
ozbrojenou	ozbrojený	k2eAgFnSc7d1	ozbrojená
pohraniční	pohraniční	k2eAgFnSc7d1	pohraniční
hlídkou	hlídka	k1gFnSc7	hlídka
a	a	k8xC	a
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
pirátství	pirátství	k1gNnSc2	pirátství
<g/>
,	,	kIx,	,
obvinění	obvinění	k1gNnSc1	obvinění
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zmírněno	zmírnit	k5eAaPmNgNnS	zmírnit
na	na	k7c4	na
výtržnictví	výtržnictví	k1gNnSc4	výtržnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
opustil	opustit	k5eAaPmAgMnS	opustit
poslední	poslední	k2eAgMnSc1d1	poslední
člen	člen	k1gMnSc1	člen
posádky	posádka	k1gFnSc2	posádka
Arctic	Arctice	k1gFnPc2	Arctice
Sunrise	Sunrise	k1gFnSc2	Sunrise
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
únik	únik	k1gInSc1	únik
ropy	ropa	k1gFnSc2	ropa
==	==	k?	==
</s>
</p>
<p>
<s>
Únik	únik	k1gInSc1	únik
ropy	ropa	k1gFnSc2	ropa
u	u	k7c2	u
města	město	k1gNnSc2	město
Usinsk	Usinsk	k1gInSc1	Usinsk
</s>
</p>
<p>
<s>
Kolva	Kolva	k1gFnSc1	Kolva
/	/	kIx~	/
Lukoil	Lukoil	k1gInSc1	Lukoil
<g/>
,	,	kIx,	,
RusVjetPetro	RusVjetPetro	k1gNnSc1	RusVjetPetro
</s>
</p>
<p>
<s>
CHMAO	CHMAO	kA	CHMAO
/	/	kIx~	/
Lukoil	Lukoil	k1gMnSc1	Lukoil
<g/>
,	,	kIx,	,
Rosněft	Rosněft	k1gMnSc1	Rosněft
</s>
</p>
<p>
<s>
Únik	únik	k1gInSc1	únik
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
Samotloru	Samotlor	k1gInSc6	Samotlor
</s>
</p>
<p>
<s>
Mamontovské	Mamontovský	k2eAgNnSc4d1	Mamontovský
naleziště	naleziště	k1gNnSc4	naleziště
Rosněft	Rosněfta	k1gFnPc2	Rosněfta
</s>
</p>
<p>
<s>
Urmanské	Urmanský	k2eAgNnSc1d1	Urmanský
naleziště	naleziště	k1gNnSc1	naleziště
/	/	kIx~	/
Gazprom	Gazprom	k1gInSc1	Gazprom
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Kamčatka	Kamčatka	k1gFnSc1	Kamčatka
/	/	kIx~	/
Rosněft	Rosněft	k1gInSc1	Rosněft
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
Morošečnaja	Morošečnaj	k1gInSc2	Morošečnaj
</s>
</p>
<p>
<s>
Sachalin-	Sachalin-	k?	Sachalin-
<g/>
1,2	[number]	k4	1,2
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
ostrov	ostrov	k1gInSc1	ostrov
/	/	kIx~	/
Anadarko	Anadarka	k1gFnSc5	Anadarka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Arktída	Arktído	k1gNnSc2	Arktído
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Arktická	arktický	k2eAgFnSc1d1	arktická
rada	rada	k1gFnSc1	rada
</s>
</p>
<p>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
</s>
</p>
<p>
<s>
Polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Arktida	Arktida	k1gFnSc1	Arktida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Arktida	Arktida	k1gFnSc1	Arktida
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Arktida	Arktida	k1gFnSc1	Arktida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Arktida	Arktida	k1gFnSc1	Arktida
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc1	téma
Britských	britský	k2eAgInPc2d1	britský
listů	list	k1gInPc2	list
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
jména	jméno	k1gNnPc1	jméno
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
podmořských	podmořský	k2eAgInPc2d1	podmořský
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
</s>
</p>
<p>
<s>
Hrozí	hrozit	k5eAaImIp3nS	hrozit
konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
,	,	kIx,	,
varuje	varovat	k5eAaImIp3nS	varovat
EU	EU	kA	EU
<g/>
,	,	kIx,	,
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
,	,	kIx,	,
12.3	[number]	k4	12.3
<g/>
.2008	.2008	k4	.2008
</s>
</p>
