<s>
Georgina	Georgina	k1gFnSc1
Singletonová	Singletonový	k2eAgFnSc1d1
</s>
<s>
Georgina	Georgina	k1gFnSc1
Singletonová	Singletonový	k2eAgFnSc1d1
Narození	narození	k1gNnSc4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Ascot	Ascot	k1gInSc1
(	(	kIx(
<g/>
Berkshire	Berkshir	k1gInSc5
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc6
</s>
<s>
judistka	judistka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
judu	judo	k1gNnSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
2002	#num#	k4
</s>
<s>
pololehká	pololehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
ME	ME	kA
1998	#num#	k4
</s>
<s>
pololehká	pololehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
ME	ME	kA
2000	#num#	k4
</s>
<s>
pololehká	pololehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
2003	#num#	k4
</s>
<s>
pololehká	pololehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Georgina	Georgina	k1gFnSc1
Singleton	Singleton	k1gInSc1
<g/>
,	,	kIx,
provdaná	provdaný	k2eAgFnSc1d1
Bevan	Bevan	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1977	#num#	k4
Ascot	Ascota	k1gFnPc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
reprezentantka	reprezentantka	k1gFnSc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
v	v	k7c6
judu	judo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
S	s	k7c7
judem	judo	k1gNnSc7
začala	začít	k5eAaPmAgFnS
v	v	k7c6
5	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
klubu	klub	k1gInSc6
Pinewood	Pinewooda	k1gFnPc2
(	(	kIx(
<g/>
Wokingham	Wokingham	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
trenérem	trenér	k1gMnSc7
byl	být	k5eAaImAgMnS
Don	Don	k1gMnSc1
Werner	Werner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
dostala	dostat	k5eAaPmAgFnS
poprvé	poprvé	k6eAd1
možnost	možnost	k1gFnSc1
startu	start	k1gInSc2
na	na	k7c6
velké	velký	k2eAgFnSc6d1
akci	akce	k1gFnSc6
ve	v	k7c6
váze	váha	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
leta	leta	k1gFnSc1
zastupovala	zastupovat	k5eAaImAgFnS
Sharon	Sharon	k1gInSc4
Rendleová	Rendleový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
však	však	k9
její	její	k3xOp3gNnPc4
místo	místo	k6eAd1
zaujala	zaujmout	k5eAaPmAgFnS
Deborah	Deborah	k1gInSc4
Allanová	Allanová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
olympijské	olympijský	k2eAgFnSc6d1
roce	rok	k1gInSc6
objížděla	objíždět	k5eAaImAgFnS
světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
dosahovala	dosahovat	k5eAaImAgFnS
kvalitních	kvalitní	k2eAgMnPc2d1
výsledků	výsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nominaci	nominace	k1gFnSc6
na	na	k7c4
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
v	v	k7c6
Sydney	Sydney	k1gNnPc6
však	však	k9
dostala	dostat	k5eAaPmAgFnS
opět	opět	k6eAd1
přednost	přednost	k1gFnSc1
Allanová	Allanová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
problematicky	problematicky	k6eAd1
do	do	k7c2
pololehké	pololehký	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
shazovala	shazovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenéři	trenér	k1gMnPc1
tohoto	tento	k3xDgInSc2
kroku	krok	k1gInSc2
následně	následně	k6eAd1
litovali	litovat	k5eAaImAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
Allanová	Allanová	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
olympijském	olympijský	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
nedokázala	dokázat	k5eNaPmAgFnS
vejít	vejít	k5eAaPmF
pod	pod	k7c4
požadovanou	požadovaný	k2eAgFnSc4d1
váhu	váha	k1gFnSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
diskvalifikována	diskvalifikován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
olympijským	olympijský	k2eAgInSc7d1
hrách	hra	k1gFnPc6
v	v	k7c4
Sydney	Sydney	k1gNnSc4
si	se	k3xPyFc3
doma	doma	k6eAd1
upevnila	upevnit	k5eAaPmAgFnS
pozici	pozice	k1gFnSc4
a	a	k8xC
s	s	k7c7
přehledem	přehled	k1gInSc7
si	se	k3xPyFc3
zajistila	zajistit	k5eAaPmAgFnS
účast	účast	k1gFnSc4
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Athénách	Athéna	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedokázala	dokázat	k5eNaPmAgFnS
však	však	k9
vyladit	vyladit	k5eAaPmF
formu	forma	k1gFnSc4
a	a	k8xC
prohrála	prohrát	k5eAaPmAgFnS
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
v	v	k7c6
boji	boj	k1gInSc6
na	na	k7c6
zemi	zem	k1gFnSc6
s	s	k7c7
Japonkou	Japonka	k1gFnSc7
Jokosawa	Jokosaw	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
oprav	oprava	k1gFnPc2
do	do	k7c2
bojů	boj	k1gInPc2
o	o	k7c4
medaile	medaile	k1gFnPc4
nepostoupila	postoupit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dalších	další	k2eAgInPc6d1
letech	let	k1gInPc6
šla	jít	k5eAaImAgFnS
se	s	k7c7
sportovní	sportovní	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
níž	nízce	k6eAd2
a	a	k8xC
objížděla	objíždět	k5eAaImAgFnS
hlavně	hlavně	k9
turnaje	turnaj	k1gInSc2
evropského	evropský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
lehké	lehký	k2eAgFnSc6d1
váze	váha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byl	být	k5eAaImAgInS
bankrot	bankrot	k1gInSc1
tréninkové	tréninkový	k2eAgNnSc1d1
centra	centrum	k1gNnPc1
v	v	k7c6
Bishamu	Bisham	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vrcholově	vrcholově	k6eAd1
připravovala	připravovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
blížícími	blížící	k2eAgFnPc7d1
olympijskými	olympijský	k2eAgFnPc7d1
hrami	hra	k1gFnPc7
v	v	k7c6
Pekingu	Peking	k1gInSc6
našla	najít	k5eAaPmAgFnS
sponozory	sponozor	k1gInPc4
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
to	ten	k3xDgNnSc1
nakonec	nakonec	k6eAd1
nestačilo	stačit	k5eNaBmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
ukončila	ukončit	k5eAaPmAgFnS
sportovní	sportovní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadu	řad	k1gInSc2
let	léto	k1gNnPc2
pracovala	pracovat	k5eAaImAgFnS
jako	jako	k9
učitelka	učitelka	k1gFnSc1
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
pololehká	pololehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
—	—	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
—	—	k?
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.5	.5	k4
<g/>
.	.	kIx.
<g/>
—	—	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
<g/>
—	—	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
—	—	k?
<g/>
2.5.1.3	2.5.1.3	k4
<g/>
.	.	kIx.
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
—	—	k?
<g/>
úč	úč	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MS	MS	kA
juniorů	junior	k1gMnPc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ME	ME	kA
juniorůúč	juniorůúč	k1gFnSc1
<g/>
.3	.3	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
startovala	startovat	k5eAaBmAgFnS
v	v	k7c4
lehké	lehký	k2eAgNnSc4d1
vázet	vázet	k5eAaBmF,k5eAaImF,k5eAaPmF
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
a	a	k8xC
novinky	novinka	k1gFnPc1
Georginy	Georgina	k1gFnSc2
Singletonové	Singletonový	k2eAgFnPc1d1
na	na	k7c4
Judoinside	Judoinsid	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistryně	mistryně	k1gFnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
judu	judo	k1gNnSc6
-	-	kIx~
pololehká	pololehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
1975	#num#	k4
Christiane	Christian	k1gMnSc5
Herzogová	Herzogový	k2eAgFnSc1d1
•	•	k?
1976	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1977	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1978	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1979	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1980	#num#	k4
Patricia	Patricius	k1gMnSc2
Montagutiová	Montagutiový	k2eAgFnSc1d1
•	•	k?
1981	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1982	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1983	#num#	k4
Loretta	Loretta	k1gFnSc1
Doyleová	Doyleová	k1gFnSc1
•	•	k?
1984	#num#	k4
Edith	Editha	k1gFnPc2
Hrovatová	Hrovatový	k2eAgFnSc1d1
•	•	k?
1985	#num#	k4
Pascale	Pascal	k1gMnSc5
Dogerová	Dogerová	k1gFnSc1
•	•	k?
1986	#num#	k4
Dominique	Dominique	k1gFnSc1
Brunová	Brunová	k1gFnSc1
•	•	k?
1987	#num#	k4
Dominique	Dominique	k1gFnSc1
Brunová	Brunová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1988	#num#	k4
Alessandra	Alessandra	k1gFnSc1
Giungiová	Giungiový	k2eAgFnSc1d1
•	•	k?
1989	#num#	k4
Jaana	Jaaen	k2eAgFnSc1d1
Ronkainenová	Ronkainenová	k1gFnSc1
•	•	k?
1990	#num#	k4
Sharon	Sharon	k1gInSc1
Rendleová	Rendleová	k1gFnSc1
•	•	k?
1991	#num#	k4
Jessica	Jessic	k2eAgFnSc1d1
Galová	Galová	k1gFnSc1
•	•	k?
1992	#num#	k4
Loretta	Loretta	k1gFnSc1
Doyleová	Doyleová	k1gFnSc1
•	•	k?
1993	#num#	k4
Almudena	Almuden	k2eAgFnSc1d1
Muñ	Muñ	k1gFnSc1
•	•	k?
1994	#num#	k4
Larysa	Larysa	k1gFnSc1
Krauseová	Krauseová	k1gFnSc1
•	•	k?
1995	#num#	k4
Alessandra	Alessandra	k1gFnSc1
Giungiová	Giungiový	k2eAgFnSc1d1
•	•	k?
1996	#num#	k4
Sharon	Sharon	k1gInSc1
Rendleová	Rendleová	k1gFnSc1
•	•	k?
1997	#num#	k4
Inge	Inge	k1gFnSc4
Clementová	Clementový	k2eAgFnSc1d1
•	•	k?
1998	#num#	k4
Raffaella	Raffaell	k1gMnSc2
Imbrianiová	Imbrianiový	k2eAgFnSc1d1
•	•	k?
1999	#num#	k4
Deborah	Deboraha	k1gFnPc2
Allanová	Allanová	k1gFnSc1
•	•	k?
2000	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Laëtitia	Laëtitia	k1gFnSc1
Tignolaová	Tignolaová	k1gFnSc1
•	•	k?
2001	#num#	k4
Inge	Inge	k1gFnSc4
Clementová	Clementový	k2eAgFnSc1d1
•	•	k?
2002	#num#	k4
Georgina	Georgina	k1gFnSc1
Singletonová	Singletonový	k2eAgFnSc1d1
•	•	k?
2003	#num#	k4
Annabelle	Annabelle	k1gFnSc1
Euranieová	Euranieová	k1gFnSc1
•	•	k?
2004	#num#	k4
Ioana	Ioaen	k2eAgFnSc1d1
Aluaș	Aluaș	k1gFnSc1
•	•	k?
2005	#num#	k4
Ilse	Ilse	k1gFnSc1
Heylenová	Heylenová	k1gFnSc1
•	•	k?
2006	#num#	k4
Telma	Telma	k1gFnSc1
Monteirová	Monteirová	k1gFnSc1
•	•	k?
2007	#num#	k4
Telma	Telma	k1gFnSc1
Monteirová	Monteirová	k1gFnSc1
•	•	k?
2008	#num#	k4
Ana	Ana	k1gFnSc1
Carrascosaová	Carrascosaová	k1gFnSc1
•	•	k?
2009	#num#	k4
Natalija	Natalij	k2eAgFnSc1d1
Kuzjutinová	Kuzjutinový	k2eAgFnSc1d1
•	•	k?
2010	#num#	k4
Natalija	Natalij	k2eAgFnSc1d1
Kuzjutinová	Kuzjutinový	k2eAgFnSc1d1
•	•	k?
2011	#num#	k4
Pénélope	Pénélop	k1gInSc5
Bonnaová	Bonnaová	k1gFnSc1
•	•	k?
2012	#num#	k4
Andreea	Andree	k2eAgFnSc1d1
Chiț	Chiț	k1gFnSc1
•	•	k?
2013	#num#	k4
Natalija	Natalij	k2eAgFnSc1d1
Kuzjutinová	Kuzjutinový	k2eAgFnSc1d1
•	•	k?
2014	#num#	k4
Majlinda	Majlind	k1gMnSc2
Kelmendiová	Kelmendiový	k2eAgFnSc1d1
•	•	k?
2015	#num#	k4
Andreea	Andree	k2eAgFnSc1d1
Chiț	Chiț	k1gFnSc1
•	•	k?
2016	#num#	k4
Majlinda	Majlind	k1gMnSc2
Kelmendiová	Kelmendiový	k2eAgFnSc1d1
•	•	k?
2017	#num#	k4
Majlinda	Majlind	k1gMnSc2
Kelmendiová	Kelmendiový	k2eAgFnSc1d1
•	•	k?
2018	#num#	k4
Natalija	Natalij	k2eAgFnSc1d1
Kuzjutinová	Kuzjutinový	k2eAgFnSc1d1
•	•	k?
2019	#num#	k4
Majlinda	Majlind	k1gMnSc2
Kelmendiová	Kelmendiový	k2eAgFnSc1d1
•	•	k?
2020	#num#	k4
Odette	Odett	k1gInSc5
Giuffridaová	Giuffridaová	k1gFnSc1
•	•	k?
2021	#num#	k4
Amandine	Amandin	k1gMnSc5
Buchardová	Buchardový	k2eAgNnPc4d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
