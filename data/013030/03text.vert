<p>
<s>
Mikulášovická	Mikulášovický	k2eAgFnSc1d1	Mikulášovická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
též	též	k9	též
Mikulášovická	Mikulášovický	k2eAgFnSc1d1	Mikulášovická
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
geomorfologická	geomorfologický	k2eAgFnSc1d1	geomorfologická
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
okrsek	okrsek	k1gInSc1	okrsek
podcelku	podcelka	k1gFnSc4	podcelka
Šenovská	šenovský	k2eAgFnSc1d1	Šenovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
a	a	k8xC	a
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
celku	celek	k1gInSc2	celek
Šluknovská	šluknovský	k2eAgFnSc1d1	Šluknovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
85	[number]	k4	85
km2	km2	k4	km2
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Tanečnice	tanečnice	k1gFnSc1	tanečnice
(	(	kIx(	(
<g/>
597	[number]	k4	597
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
Mikulášovická	Mikulášovický	k2eAgFnSc1d1	Mikulášovická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
je	být	k5eAaImIp3nS	být
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
okrsek	okrsek	k1gInSc1	okrsek
podcelku	podcelka	k1gFnSc4	podcelka
Šenovská	šenovský	k2eAgFnSc1d1	Šenovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
a	a	k8xC	a
celku	celek	k1gInSc3	celek
Šluknovská	šluknovský	k2eAgFnSc1d1	Šluknovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
85	[number]	k4	85
km2	km2	k4	km2
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Šluknovského	šluknovský	k2eAgInSc2d1	šluknovský
výběžku	výběžek	k1gInSc2	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
okrsku	okrsek	k1gInSc2	okrsek
přibližně	přibližně	k6eAd1	přibližně
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Mikulášovickou	Mikulášovický	k2eAgFnSc4d1	Mikulášovická
pahorkatinu	pahorkatina	k1gFnSc4	pahorkatina
od	od	k7c2	od
Hrazenské	Hrazenský	k2eAgFnSc2d1	Hrazenský
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
po	po	k7c6	po
lužickém	lužický	k2eAgInSc6d1	lužický
zlomu	zlom	k1gInSc6	zlom
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
osadě	osada	k1gFnSc3	osada
Kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
na	na	k7c4	na
sever	sever	k1gInSc4	sever
údolím	údolí	k1gNnSc7	údolí
Černého	Černého	k2eAgInSc2d1	Černého
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
Vilémovského	vilémovský	k2eAgInSc2d1	vilémovský
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
Velkošenovského	Velkošenovský	k2eAgInSc2d1	Velkošenovský
potoka	potok	k1gInSc2	potok
až	až	k9	až
ke	k	k7c3	k
státní	státní	k2eAgFnSc3d1	státní
hranici	hranice	k1gFnSc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Tanečnice	tanečnice	k1gFnSc2	tanečnice
(	(	kIx(	(
<g/>
597	[number]	k4	597
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgNnPc3d3	nejnižší
hladina	hladina	k1gFnSc1	hladina
Sebnice	Sebnice	k1gFnPc4	Sebnice
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Poustevně	poustevna	k1gFnSc6	poustevna
(	(	kIx(	(
<g/>
296	[number]	k4	296
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Geologické	geologický	k2eAgNnSc1d1	geologické
podloží	podloží	k1gNnSc1	podloží
tvoří	tvořit	k5eAaImIp3nS	tvořit
jemně	jemně	k6eAd1	jemně
až	až	k9	až
středně	středně	k6eAd1	středně
zrnitý	zrnitý	k2eAgInSc1d1	zrnitý
biotitický	biotitický	k2eAgInSc1d1	biotitický
lužický	lužický	k2eAgInSc1d1	lužický
granodiorit	granodiorit	k1gInSc1	granodiorit
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
žílami	žíla	k1gFnPc7	žíla
lamprofyru	lamprofyr	k1gInSc2	lamprofyr
<g/>
,	,	kIx,	,
doleritu	dolerit	k1gInSc2	dolerit
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
častými	častý	k2eAgInPc7d1	častý
průniky	průnik	k1gInPc7	průnik
třetihorních	třetihorní	k2eAgInPc2d1	třetihorní
neovulkanitů	neovulkanit	k1gInPc2	neovulkanit
(	(	kIx(	(
<g/>
čedič	čedič	k1gInSc1	čedič
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
horniny	hornina	k1gFnPc1	hornina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
povodí	povodí	k1gNnSc3	povodí
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
úmoří	úmoří	k1gNnSc2	úmoří
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jižních	jižní	k2eAgInPc2d1	jižní
svahů	svah	k1gInPc2	svah
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
odvodňovány	odvodňovat	k5eAaImNgInP	odvodňovat
do	do	k7c2	do
Křinice	Křinice	k1gFnSc2	Křinice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povodím	povodí	k1gNnSc7	povodí
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
Sebnice	Sebnice	k1gFnSc2	Sebnice
<g/>
.	.	kIx.	.
</s>
<s>
Středně	středně	k6eAd1	středně
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
krajina	krajina	k1gFnSc1	krajina
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
lesnímu	lesní	k2eAgInSc3d1	lesní
vegetačnímu	vegetační	k2eAgInSc3d1	vegetační
stupni	stupeň	k1gInSc3	stupeň
(	(	kIx(	(
<g/>
bukový	bukový	k2eAgInSc1d1	bukový
a	a	k8xC	a
jedlo-bukový	jedloukový	k2eAgInSc1d1	jedlo-bukový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převažují	převažovat	k5eAaImIp3nP	převažovat
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
lesy	les	k1gInPc4	les
s	s	k7c7	s
dominantním	dominantní	k2eAgInSc7d1	dominantní
výskytem	výskyt	k1gInSc7	výskyt
smrku	smrk	k1gInSc2	smrk
ztepilého	ztepilý	k2eAgInSc2d1	ztepilý
(	(	kIx(	(
<g/>
Picea	Pice	k1gInSc2	Pice
abies	abies	k1gInSc1	abies
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněným	doplněný	k2eAgInSc7d1	doplněný
modřínem	modřín	k1gInSc7	modřín
opadavým	opadavý	k2eAgInSc7d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bukem	buk	k1gInSc7	buk
lesním	lesní	k2eAgInSc7d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc1	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
či	či	k8xC	či
jedlí	jedle	k1gFnSc7	jedle
bělokorou	bělokorý	k2eAgFnSc7d1	bělokorá
(	(	kIx(	(
<g/>
Abies	Abiesa	k1gFnPc2	Abiesa
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
původní	původní	k2eAgFnPc1d1	původní
údolní	údolní	k2eAgFnPc1d1	údolní
jasanovo-olšové	jasanovolšová	k1gFnPc1	jasanovo-olšová
luhy	luh	k1gInPc4	luh
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
území	území	k1gNnSc6	území
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
6,5	[number]	k4	6,5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
pak	pak	k6eAd1	pak
na	na	k7c4	na
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
okrsku	okrsek	k1gInSc2	okrsek
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Labské	labský	k2eAgInPc1d1	labský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrcholy	vrchol	k1gInPc4	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pojmenované	pojmenovaný	k2eAgInPc4d1	pojmenovaný
vrchy	vrch	k1gInPc4	vrch
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
nad	nad	k7c7	nad
400	[number]	k4	400
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Zařazení	zařazení	k1gNnSc1	zařazení
Mikulášovické	Mikulášovický	k2eAgFnSc2d1	Mikulášovická
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
podle	podle	k7c2	podle
Demkovy	Demkův	k2eAgFnSc2d1	Demkův
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
provincie	provincie	k1gFnSc1	provincie
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
vysočina	vysočina	k1gFnSc1	vysočina
</s>
</p>
<p>
<s>
subprovincie	subprovincie	k1gFnSc1	subprovincie
<g/>
:	:	kIx,	:
IV	IV	kA	IV
Krkonošsko-jesenická	krkonošskoesenický	k2eAgFnSc1d1	krkonošsko-jesenický
</s>
</p>
<p>
<s>
oblast	oblast	k1gFnSc1	oblast
<g/>
:	:	kIx,	:
IVA	Iva	k1gFnSc1	Iva
Krkonošská	krkonošský	k2eAgFnSc1d1	Krkonošská
</s>
</p>
<p>
<s>
celek	celek	k1gInSc1	celek
<g/>
:	:	kIx,	:
IVA-1	IVA-1	k1gFnSc1	IVA-1
Šluknovská	šluknovský	k2eAgFnSc1d1	Šluknovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
</s>
</p>
<p>
<s>
podcelek	podcelek	k1gInSc1	podcelek
<g/>
:	:	kIx,	:
IVA-1-A	IVA-1-A	k1gFnSc1	IVA-1-A
Šenovská	šenovský	k2eAgFnSc1d1	Šenovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
</s>
</p>
<p>
<s>
okrsek	okrsek	k1gInSc1	okrsek
<g/>
:	:	kIx,	:
IVA-	IVA-	k1gFnSc1	IVA-
<g/>
1	[number]	k4	1
<g/>
-A-a	-A-	k1gInSc2	-A-
Hrazenská	Hrazenský	k2eAgFnSc1d1	Hrazenský
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
</s>
</p>
<p>
<s>
okrsek	okrsek	k1gInSc1	okrsek
<g/>
:	:	kIx,	:
IVA-	IVA-	k1gFnSc1	IVA-
<g/>
1	[number]	k4	1
<g/>
-A-b	-Aa	k1gFnPc2	-A-ba
Mikulášovická	Mikulášovický	k2eAgFnSc1d1	Mikulášovická
pahorkatinaBalatkovo	pahorkatinaBalatkův	k2eAgNnSc4d1	pahorkatinaBalatkův
geomorfologické	geomorfologický	k2eAgNnSc4d1	Geomorfologické
členění	členění	k1gNnSc4	členění
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
jednotku	jednotka	k1gFnSc4	jednotka
neklasifikuje	klasifikovat	k5eNaImIp3nS	klasifikovat
jako	jako	k9	jako
pahorkatinu	pahorkatina	k1gFnSc4	pahorkatina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jí	on	k3xPp3gFnSc3	on
neřadí	řadit	k5eNaImIp3nS	řadit
jako	jako	k9	jako
geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
okrsed	okrsed	k1gInSc1	okrsed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
podokrsek	podokrsek	k1gInSc4	podokrsek
přičemž	přičemž	k6eAd1	přičemž
podcelky	podcelka	k1gFnPc1	podcelka
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
členění	členění	k1gNnSc4	členění
Šluknovské	šluknovský	k2eAgFnSc2d1	Šluknovská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
zcela	zcela	k6eAd1	zcela
vynechány	vynechán	k2eAgFnPc1d1	vynechána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DEMEK	DEMEK	kA	DEMEK
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
lexikon	lexikon	k1gInSc1	lexikon
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnPc1	hora
a	a	k8xC	a
nížiny	nížina	k1gFnPc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
584	[number]	k4	584
s.	s.	k?	s.
</s>
</p>
<p>
<s>
GLÖCKNER	GLÖCKNER	kA	GLÖCKNER
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Fyzickogeografické	fyzickogeografický	k2eAgFnPc1d1	fyzickogeografická
a	a	k8xC	a
geologické	geologický	k2eAgFnPc1d1	geologická
poměry	poměra	k1gFnPc1	poměra
okresu	okres	k1gInSc2	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Děčín	Děčín	k1gInSc1	Děčín
<g/>
:	:	kIx,	:
PS	PS	kA	PS
Děčín	Děčín	k1gInSc1	Děčín
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902071	[number]	k4	902071
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOPECKÝ	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlivky	vysvětlivka	k1gFnPc4	vysvětlivka
k	k	k7c3	k
přehledné	přehledný	k2eAgFnSc3d1	přehledná
geologické	geologický	k2eAgFnSc3d1	geologická
mapě	mapa	k1gFnSc3	mapa
ČSSR	ČSSR	kA	ČSSR
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
M	M	kA	M
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
<g/>
–	–	k?	–
<g/>
IX	IX	kA	IX
Děčín	Děčín	k1gInSc1	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
176	[number]	k4	176
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MELICHAR	Melichar	k1gMnSc1	Melichar
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivěda	vlastivěda	k1gFnSc1	vlastivěda
Šluknovského	šluknovský	k2eAgInSc2d1	šluknovský
výběžku	výběžek	k1gInSc2	výběžek
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Šluknov	Šluknov	k1gInSc1	Šluknov
<g/>
:	:	kIx,	:
Sdružení	sdružení	k1gNnSc1	sdružení
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
Šluknovska	Šluknovsko	k1gNnSc2	Šluknovsko
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
1704	[number]	k4	1704
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Šluknovská	šluknovský	k2eAgFnSc1d1	Šluknovská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vrcholů	vrchol	k1gInPc2	vrchol
ve	v	k7c6	v
Šluknovské	šluknovský	k2eAgFnSc6d1	Šluknovská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mikulášovická	Mikulášovický	k2eAgFnSc1d1	Mikulášovická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
