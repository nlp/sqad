<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odesílá	odesílat	k5eAaImIp3nS	odesílat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
dovnitř	dovnitř	k6eAd1	dovnitř
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
?	?	kIx.	?
</s>
