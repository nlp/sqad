<s>
Vstup	vstup	k1gInSc1	vstup
<g/>
/	/	kIx~	/
<g/>
výstup	výstup	k1gInSc1	výstup
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
input	input	k2eAgInSc1d1	input
<g/>
/	/	kIx~	/
<g/>
output	output	k1gInSc1	output
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
ve	v	k7c6	v
výpočetní	výpočetní	k2eAgFnSc6d1	výpočetní
technice	technika	k1gFnSc6	technika
hardwarové	hardwarový	k2eAgNnSc1d1	hardwarové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
kontakt	kontakt	k1gInSc4	kontakt
počítače	počítač	k1gInSc2	počítač
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gNnSc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
vstupní	vstupní	k2eAgNnPc4d1	vstupní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
odesílají	odesílat	k5eAaImIp3nP	odesílat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
dovnitř	dovnitř	k6eAd1	dovnitř
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
výstupní	výstupní	k2eAgNnPc1d1	výstupní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
naopak	naopak	k6eAd1	naopak
informace	informace	k1gFnPc4	informace
předávají	předávat	k5eAaImIp3nP	předávat
zevnitř	zevnitř	k6eAd1	zevnitř
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
označujeme	označovat	k5eAaImIp1nP	označovat
tato	tento	k3xDgNnPc1	tento
zařízení	zařízení	k1gNnPc1	zařízení
jako	jako	k8xS	jako
vstupně-výstupní	vstupněýstupní	k2eAgFnPc1d1	vstupně-výstupní
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
pomoci	pomoct	k5eAaPmF	pomoct
počítače	počítač	k1gInPc4	počítač
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
přes	přes	k7c4	přes
sběrnici	sběrnice	k1gFnSc4	sběrnice
se	se	k3xPyFc4	se
vstupně	vstupně	k6eAd1	vstupně
<g/>
/	/	kIx~	/
<g/>
výstupními	výstupní	k2eAgNnPc7d1	výstupní
zařízeními	zařízení	k1gNnPc7	zařízení
pomocí	pomocí	k7c2	pomocí
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Registr	registr	k1gInSc1	registr
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
velikost	velikost	k1gFnSc4	velikost
jeden	jeden	k4xCgInSc4	jeden
bajt	bajt	k1gInSc4	bajt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
soustavy	soustava	k1gFnSc2	soustava
klopných	klopný	k2eAgInPc2d1	klopný
obvodů	obvod	k1gInPc2	obvod
nebo	nebo	k8xC	nebo
kusu	kus	k1gInSc2	kus
běžné	běžný	k2eAgFnSc2d1	běžná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
příslušném	příslušný	k2eAgMnSc6d1	příslušný
vstupně	vstupně	k6eAd1	vstupně
<g/>
/	/	kIx~	/
<g/>
výstupním	výstupní	k2eAgNnSc6d1	výstupní
zařízení	zařízení	k1gNnSc6	zařízení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
i	i	k8xC	i
zápis	zápis	k1gInSc4	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Registr	registr	k1gInSc1	registr
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
paměť	paměť	k1gFnSc4	paměť
(	(	kIx(	(
<g/>
hardwarová	hardwarový	k2eAgFnSc1d1	hardwarová
cache	cache	k1gFnSc1	cache
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
udrží	udržet	k5eAaPmIp3nS	udržet
data	datum	k1gNnPc4	datum
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
přenesena	přenesen	k2eAgNnPc4d1	přeneseno
do	do	k7c2	do
procesoru	procesor	k1gInSc2	procesor
nebo	nebo	k8xC	nebo
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
naopak	naopak	k6eAd1	naopak
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
samotným	samotný	k2eAgNnSc7d1	samotné
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Registry	registr	k1gInPc1	registr
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
izolované	izolovaný	k2eAgInPc1d1	izolovaný
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgFnPc2d1	speciální
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
IN	IN	kA	IN
a	a	k8xC	a
OUT	OUT	kA	OUT
<g/>
)	)	kIx)	)
adresní	adresní	k2eAgFnPc1d1	adresní
prostory	prostora	k1gFnPc1	prostora
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
vstupně	vstupně	k6eAd1	vstupně
<g/>
/	/	kIx~	/
<g/>
výstupních	výstupní	k2eAgNnPc2d1	výstupní
zařízení	zařízení	k1gNnPc2	zařízení
jsou	být	k5eAaImIp3nP	být
oddělené	oddělený	k2eAgInPc1d1	oddělený
paměťově	paměťově	k6eAd1	paměťově
mapované	mapovaný	k2eAgInPc1d1	mapovaný
registry	registr	k1gInPc1	registr
jsou	být	k5eAaImIp3nP	být
adresovány	adresován	k2eAgInPc1d1	adresován
jako	jako	k8xS	jako
paměť	paměť	k1gFnSc1	paměť
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
pomocí	pomocí	k7c2	pomocí
běžných	běžný	k2eAgFnPc2d1	běžná
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
Vstupní	vstupní	k2eAgNnPc1d1	vstupní
zařízení	zařízení	k1gNnPc1	zařízení
Výstupní	výstupní	k2eAgNnPc1d1	výstupní
zařízení	zařízení	k1gNnPc1	zařízení
Standardní	standardní	k2eAgInPc4d1	standardní
proudy	proud	k1gInPc4	proud
–	–	k?	–
vstup	vstup	k1gInSc4	vstup
a	a	k8xC	a
výstup	výstup	k1gInSc4	výstup
procesů	proces	k1gInPc2	proces
spuštěných	spuštěný	k2eAgInPc2d1	spuštěný
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
</s>
