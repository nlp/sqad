<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gNnSc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
vstupní	vstupní	k2eAgNnPc4d1	vstupní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
odesílají	odesílat	k5eAaImIp3nP	odesílat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
dovnitř	dovnitř	k6eAd1	dovnitř
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
výstupní	výstupní	k2eAgNnPc1d1	výstupní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naopak	naopak	k6eAd1	naopak
informace	informace	k1gFnPc4	informace
předávají	předávat	k5eAaImIp3nP	předávat
zevnitř	zevnitř	k6eAd1	zevnitř
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
