<s>
Teploměr	teploměr	k1gInSc1	teploměr
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
sloužící	sloužící	k2eAgNnSc1d1	sloužící
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
měří	měřit	k5eAaImIp3nS	měřit
kalorimetr	kalorimetr	k1gInSc4	kalorimetr
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
princip	princip	k1gInSc1	princip
teploměru	teploměr	k1gInSc2	teploměr
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
tepelné	tepelný	k2eAgFnPc4d1	tepelná
roztažnosti	roztažnost	k1gFnPc4	roztažnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
objem	objem	k1gInSc1	objem
měrné	měrný	k2eAgFnSc2d1	měrná
látky	látka	k1gFnSc2	látka
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
rozměru	rozměr	k1gInSc2	rozměr
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
dodaném	dodaný	k2eAgNnSc6d1	dodané
teplu	teplo	k1gNnSc6	teplo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
teploměry	teploměr	k1gInPc1	teploměr
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
dilatační	dilatační	k2eAgFnPc1d1	dilatační
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
metody	metoda	k1gFnPc4	metoda
zjišťování	zjišťování	k1gNnSc2	zjišťování
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
měřením	měření	k1gNnSc7	měření
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
termometrie	termometrie	k1gFnSc1	termometrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
teploměrů	teploměr	k1gInPc2	teploměr
<g/>
:	:	kIx,	:
Kapalinový	kapalinový	k2eAgInSc1d1	kapalinový
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
využívá	využívat	k5eAaPmIp3nS	využívat
teplotní	teplotní	k2eAgFnSc3d1	teplotní
roztažnosti	roztažnost	k1gFnSc3	roztažnost
teploměrné	teploměrný	k2eAgFnSc2d1	teploměrná
kapaliny	kapalina	k1gFnSc2	kapalina
(	(	kIx(	(
<g/>
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
líh	líh	k1gInSc1	líh
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bimetalový	bimetalový	k2eAgInSc1d1	bimetalový
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
využívá	využívat	k5eAaPmIp3nS	využívat
bimetalový	bimetalový	k2eAgInSc4d1	bimetalový
(	(	kIx(	(
<g/>
dvojkovový	dvojkovový	k2eAgInSc4d1	dvojkovový
<g/>
)	)	kIx)	)
pásek	pásek	k1gInSc4	pásek
složený	složený	k2eAgInSc4d1	složený
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
teplotními	teplotní	k2eAgInPc7d1	teplotní
součiniteli	součinitel	k1gInPc7	součinitel
délkové	délkový	k2eAgFnSc2d1	délková
roztažnosti	roztažnost	k1gFnSc2	roztažnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
změně	změna	k1gFnSc6	změna
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
pásek	pásek	k1gInSc1	pásek
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
ručku	ručka	k1gFnSc4	ručka
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Plynový	plynový	k2eAgInSc1d1	plynový
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
využívá	využívat	k5eAaPmIp3nS	využívat
závislost	závislost	k1gFnSc4	závislost
tlaku	tlak	k1gInSc2	tlak
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
při	při	k7c6	při
stálém	stálý	k2eAgInSc6d1	stálý
objemu	objem	k1gInSc6	objem
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
závislost	závislost	k1gFnSc1	závislost
objemu	objem	k1gInSc2	objem
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
při	při	k7c6	při
stálém	stálý	k2eAgInSc6d1	stálý
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Odporový	odporový	k2eAgInSc1d1	odporový
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
využívá	využívat	k5eAaPmIp3nS	využívat
závislost	závislost	k1gFnSc4	závislost
elektrického	elektrický	k2eAgInSc2d1	elektrický
odporu	odpor	k1gInSc2	odpor
vodiče	vodič	k1gInSc2	vodič
nebo	nebo	k8xC	nebo
polovodiče	polovodič	k1gInSc2	polovodič
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
součástka	součástka	k1gFnSc1	součástka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
teplotně	teplotně	k6eAd1	teplotně
závislé	závislý	k2eAgFnSc3d1	závislá
regulaci	regulace	k1gFnSc3	regulace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
termistor	termistor	k1gInSc1	termistor
<g/>
.	.	kIx.	.
</s>
<s>
Termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
teploměr	teploměr	k1gInSc1	teploměr
(	(	kIx(	(
<g/>
také	také	k9	také
termočlánek	termočlánek	k1gInSc1	termočlánek
<g/>
)	)	kIx)	)
-	-	kIx~	-
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
využívá	využívat	k5eAaImIp3nS	využívat
termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nositeli	nositel	k1gMnPc7	nositel
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
tepla	teplo	k1gNnSc2	teplo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
teploty	teplota	k1gFnSc2	teplota
spoje	spoj	k1gFnSc2	spoj
dvou	dva	k4xCgInPc2	dva
různých	různý	k2eAgInPc2d1	různý
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
termoelektrické	termoelektrický	k2eAgNnSc1d1	termoelektrické
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Polovodičový	polovodičový	k2eAgInSc1d1	polovodičový
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
využívá	využívat	k5eAaImIp3nS	využívat
závislosti	závislost	k1gFnPc4	závislost
charakteristik	charakteristika	k1gFnPc2	charakteristika
polovodičového	polovodičový	k2eAgInSc2d1	polovodičový
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
zesilovací	zesilovací	k2eAgMnSc1d1	zesilovací
činitel	činitel	k1gMnSc1	činitel
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc1	napětí
přechodu	přechod	k1gInSc2	přechod
P-N	P-N	k1gFnSc2	P-N
<g/>
)	)	kIx)	)
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
.	.	kIx.	.
</s>
<s>
Radiační	radiační	k2eAgInSc1d1	radiační
teploměr	teploměr	k1gInSc1	teploměr
(	(	kIx(	(
<g/>
Infrateploměr	Infrateploměr	k1gInSc1	Infrateploměr
<g/>
)	)	kIx)	)
-	-	kIx~	-
teploměr	teploměr	k1gInSc4	teploměr
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
založený	založený	k2eAgMnSc1d1	založený
na	na	k7c6	na
zákonech	zákon	k1gInPc6	zákon
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
Planckův	Planckův	k2eAgInSc1d1	Planckův
vyzařovací	vyzařovací	k2eAgInSc1d1	vyzařovací
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
Wienův	Wienův	k2eAgInSc1d1	Wienův
posunovací	posunovací	k2eAgInSc1d1	posunovací
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
Stefanův-Boltzmannův	Stefanův-Boltzmannův	k2eAgInSc1d1	Stefanův-Boltzmannův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
záření	záření	k1gNnSc1	záření
vysílané	vysílaný	k2eAgFnSc2d1	vysílaná
tělesy	těleso	k1gNnPc7	těleso
do	do	k7c2	do
okolí	okolí	k1gNnSc4	okolí
(	(	kIx(	(
<g/>
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
pracují	pracovat	k5eAaImIp3nP	pracovat
i	i	k9	i
světelná	světelný	k2eAgNnPc4d1	světelné
infračidla	infračidlo	k1gNnPc4	infračidlo
či	či	k8xC	či
naváděné	naváděný	k2eAgFnPc4d1	naváděná
střely	střela	k1gFnPc4	střela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktní	kontaktní	k2eAgInSc1d1	kontaktní
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
sepne	sepnout	k5eAaPmIp3nS	sepnout
kontakt	kontakt	k1gInSc4	kontakt
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
nastavené	nastavený	k2eAgFnSc2d1	nastavená
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c4	v
regulaci	regulace	k1gFnSc4	regulace
a	a	k8xC	a
automatizaci	automatizace	k1gFnSc4	automatizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
termostat	termostat	k1gInSc1	termostat
pro	pro	k7c4	pro
klimatizaci	klimatizace	k1gFnSc4	klimatizace
nebo	nebo	k8xC	nebo
akvárium	akvárium	k1gNnSc4	akvárium
<g/>
.	.	kIx.	.
</s>
<s>
Maximo-minimální	Maximoinimální	k2eAgInSc1d1	Maximo-minimální
teploměr	teploměr	k1gInSc1	teploměr
-	-	kIx~	-
teploměr	teploměr	k1gInSc1	teploměr
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
maximální	maximální	k2eAgFnSc4d1	maximální
a	a	k8xC	a
minimální	minimální	k2eAgFnSc4d1	minimální
dosaženou	dosažený	k2eAgFnSc4d1	dosažená
teplotu	teplota	k1gFnSc4	teplota
za	za	k7c4	za
sledované	sledovaný	k2eAgNnSc4d1	sledované
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
nulování	nulování	k1gNnSc2	nulování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
teploměry	teploměr	k1gInPc1	teploměr
snad	snad	k9	snad
nejznámějším	známý	k2eAgInSc7d3	nejznámější
fyzikálním	fyzikální	k2eAgInSc7d1	fyzikální
přístrojem	přístroj	k1gInSc7	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ještě	ještě	k9	ještě
před	před	k7c7	před
několika	několik	k4yIc7	několik
staletími	staletí	k1gNnPc7	staletí
byly	být	k5eAaImAgFnP	být
úplně	úplně	k6eAd1	úplně
neznámé	známý	k2eNgFnPc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
určovala	určovat	k5eAaImAgFnS	určovat
podle	podle	k7c2	podle
tělesných	tělesný	k2eAgInPc2d1	tělesný
pocitů	pocit	k1gInPc2	pocit
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
řídili	řídit	k5eAaImAgMnP	řídit
barvou	barva	k1gFnSc7	barva
rozžhavených	rozžhavený	k2eAgInPc2d1	rozžhavený
předmětů	předmět	k1gInPc2	předmět
nebo	nebo	k8xC	nebo
roztavením	roztavení	k1gNnSc7	roztavení
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
měření	měření	k1gNnSc1	měření
teploty	teplota	k1gFnSc2	teplota
pomocí	pomocí	k7c2	pomocí
roztažnosti	roztažnost	k1gFnSc2	roztažnost
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
doklady	doklad	k1gInPc1	doklad
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Hérón	Hérón	k1gInSc1	Hérón
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
popsal	popsat	k5eAaPmAgMnS	popsat
zařízení	zařízení	k1gNnSc1	zařízení
pracující	pracující	k1gFnSc2	pracující
na	na	k7c6	na
roztažnosti	roztažnost	k1gFnSc6	roztažnost
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
vzduchový	vzduchový	k2eAgInSc4d1	vzduchový
termoskop	termoskop	k1gInSc4	termoskop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
doloženým	doložený	k2eAgInSc7d1	doložený
přístrojem	přístroj	k1gInSc7	přístroj
k	k	k7c3	k
indikaci	indikace	k1gFnSc3	indikace
tepelných	tepelný	k2eAgInPc2d1	tepelný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc6	Galilei
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
profesor	profesor	k1gMnSc1	profesor
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
využil	využít	k5eAaPmAgMnS	využít
tepelné	tepelný	k2eAgFnSc2d1	tepelná
roztažnosti	roztažnost	k1gFnSc2	roztažnost
vzduchu	vzduch	k1gInSc2	vzduch
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
primitivní	primitivní	k2eAgInSc1d1	primitivní
teploměr	teploměr	k1gInSc1	teploměr
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
tenkou	tenký	k2eAgFnSc7d1	tenká
skleněnou	skleněný	k2eAgFnSc7d1	skleněná
trubičkou	trubička	k1gFnSc7	trubička
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
a	a	k8xC	a
zakončenou	zakončený	k2eAgFnSc7d1	zakončená
baňkou	baňka	k1gFnSc7	baňka
<g/>
.	.	kIx.	.
</s>
<s>
Baňku	baňka	k1gFnSc4	baňka
zahřál	zahřát	k5eAaPmAgMnS	zahřát
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
"	"	kIx"	"
<g/>
teploměr	teploměr	k1gInSc1	teploměr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
vzduchový	vzduchový	k2eAgInSc4d1	vzduchový
termoskop	termoskop	k1gInSc4	termoskop
<g/>
)	)	kIx)	)
vložil	vložit	k5eAaPmAgInS	vložit
otevřeným	otevřený	k2eAgInSc7d1	otevřený
koncem	konec	k1gInSc7	konec
trubičky	trubička	k1gFnSc2	trubička
do	do	k7c2	do
nádobky	nádobka	k1gFnSc2	nádobka
s	s	k7c7	s
obarvenou	obarvený	k2eAgFnSc7d1	obarvená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Chladnoucí	chladnoucí	k2eAgInSc1d1	chladnoucí
vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
smršťoval	smršťovat	k5eAaImAgInS	smršťovat
a	a	k8xC	a
vlivem	vlivem	k7c2	vlivem
tlaku	tlak	k1gInSc2	tlak
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
voda	voda	k1gFnSc1	voda
vnikala	vnikat	k5eAaImAgFnS	vnikat
do	do	k7c2	do
trubičky	trubička	k1gFnSc2	trubička
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vychladnutí	vychladnutí	k1gNnSc6	vychladnutí
přejímala	přejímat	k5eAaImAgFnS	přejímat
baňka	baňka	k1gFnSc1	baňka
teplotu	teplota	k1gFnSc4	teplota
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
v	v	k7c6	v
trubičce	trubička	k1gFnSc6	trubička
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
podle	podle	k7c2	podle
změn	změna	k1gFnPc2	změna
objemu	objem	k1gInSc2	objem
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
baňce	baňka	k1gFnSc6	baňka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zase	zase	k9	zase
měnil	měnit	k5eAaImAgMnS	měnit
podle	podle	k7c2	podle
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dnešních	dnešní	k2eAgInPc2d1	dnešní
teploměrů	teploměr	k1gInPc2	teploměr
při	při	k7c6	při
oteplení	oteplení	k1gNnSc6	oteplení
hladina	hladina	k1gFnSc1	hladina
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
při	při	k7c6	při
ochlazení	ochlazení	k1gNnSc6	ochlazení
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
ještě	ještě	k9	ještě
neměl	mít	k5eNaImAgInS	mít
stupnici	stupnice	k1gFnSc3	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Galileim	Galileim	k1gInSc4	Galileim
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
teploměry	teploměr	k1gInPc7	teploměr
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Guericke	Guerick	k1gInSc2	Guerick
a	a	k8xC	a
Gaspar	Gaspar	k1gMnSc1	Gaspar
Schott	Schott	k1gMnSc1	Schott
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
termoskop	termoskop	k1gInSc4	termoskop
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
použili	použít	k5eAaPmAgMnP	použít
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
systému	systém	k1gInSc2	systém
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
baňkami	baňka	k1gFnPc7	baňka
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
spojovací	spojovací	k2eAgFnSc1d1	spojovací
trubička	trubička	k1gFnSc1	trubička
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
U	U	kA	U
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
teploměry	teploměr	k1gInPc1	teploměr
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
teploměrnou	teploměrný	k2eAgFnSc7d1	teploměrná
látkou	látka	k1gFnSc7	látka
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgFnSc7	první
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lékař	lékař	k1gMnSc1	lékař
Jean	Jean	k1gMnSc1	Jean
Rey	Rea	k1gFnSc2	Rea
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k9	jako
teploměrnou	teploměrný	k2eAgFnSc4d1	teploměrná
látku	látka	k1gFnSc4	látka
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
teploměru	teploměr	k1gInSc2	teploměr
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
roztažnost	roztažnost	k1gFnSc1	roztažnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hledaly	hledat	k5eAaImAgFnP	hledat
jiné	jiný	k2eAgFnPc1d1	jiná
vhodné	vhodný	k2eAgFnPc1d1	vhodná
tekutiny	tekutina	k1gFnPc1	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgInP	ukázat
líh	líh	k1gInSc4	líh
a	a	k8xC	a
rtuť	rtuť	k1gFnSc4	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
lihový	lihový	k2eAgInSc1d1	lihový
teploměr	teploměr	k1gInSc1	teploměr
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
roku	rok	k1gInSc2	rok
1641	[number]	k4	1641
toskánský	toskánský	k2eAgMnSc1d1	toskánský
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Lékařský	lékařský	k2eAgInSc1d1	lékařský
teploměr	teploměr	k1gInSc1	teploměr
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
Clifford	Clifford	k1gMnSc1	Clifford
Allbutt	Allbutt	k1gMnSc1	Allbutt
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
teploměr	teploměr	k1gInSc1	teploměr
měří	měřit	k5eAaImIp3nS	měřit
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
ne	ne	k9	ne
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
korektní	korektní	k2eAgInSc1d1	korektní
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
přístroje	přístroj	k1gInSc2	přístroj
být	být	k5eAaImF	být
teplotoměr	teplotoměr	k1gInSc4	teplotoměr
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
označení	označení	k1gNnSc1	označení
teploměr	teploměr	k1gInSc1	teploměr
je	být	k5eAaImIp3nS	být
zažité	zažitý	k2eAgNnSc4d1	zažité
a	a	k8xC	a
běžné	běžný	k2eAgNnSc4d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sice	sice	k8xC	sice
teploměry	teploměr	k1gInPc1	teploměr
již	již	k6eAd1	již
měly	mít	k5eAaImAgFnP	mít
stupnice	stupnice	k1gFnPc1	stupnice
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
jednotné	jednotný	k2eAgFnPc1d1	jednotná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
údaje	údaj	k1gInPc4	údaj
změřené	změřený	k2eAgInPc4d1	změřený
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
teploměry	teploměr	k1gInPc7	teploměr
se	se	k3xPyFc4	se
nemohly	moct	k5eNaImAgInP	moct
porovnat	porovnat	k5eAaPmF	porovnat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
teploměry	teploměr	k1gInPc1	teploměr
s	s	k7c7	s
"	"	kIx"	"
<g/>
normalizovanou	normalizovaný	k2eAgFnSc7d1	normalizovaná
<g/>
"	"	kIx"	"
stupnicí	stupnice	k1gFnSc7	stupnice
byly	být	k5eAaImAgInP	být
sestrojeny	sestrojit	k5eAaPmNgInP	sestrojit
až	až	k6eAd1	až
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
kapitolou	kapitola	k1gFnSc7	kapitola
vývoje	vývoj	k1gInSc2	vývoj
teploměrů	teploměr	k1gInPc2	teploměr
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
stanovení	stanovení	k1gNnSc1	stanovení
teplotní	teplotní	k2eAgFnSc2d1	teplotní
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgNnSc4	který
měření	měření	k1gNnSc4	měření
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
stupnic	stupnice	k1gFnPc2	stupnice
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
stanovením	stanovení	k1gNnSc7	stanovení
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
dilatačních	dilatační	k2eAgInPc2d1	dilatační
teploměrů	teploměr	k1gInPc2	teploměr
prakticky	prakticky	k6eAd1	prakticky
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
modifikovány	modifikovat	k5eAaBmNgFnP	modifikovat
a	a	k8xC	a
vylepšovány	vylepšovat	k5eAaImNgFnP	vylepšovat
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gInPc1	jejich
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
rtuti	rtuť	k1gFnSc2	rtuť
(	(	kIx(	(
<g/>
356	[number]	k4	356
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
1100	[number]	k4	1100
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
rtuťovým	rtuťový	k2eAgInSc7d1	rtuťový
teploměrem	teploměr	k1gInSc7	teploměr
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
kapilára	kapilára	k1gFnSc1	kapilára
plní	plnit	k5eAaImIp3nS	plnit
např.	např.	kA	např.
dusíkem	dusík	k1gInSc7	dusík
a	a	k8xC	a
teploměr	teploměr	k1gInSc1	teploměr
je	být	k5eAaImIp3nS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
z	z	k7c2	z
křemenného	křemenný	k2eAgNnSc2d1	křemenné
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lékařských	lékařský	k2eAgInPc2d1	lékařský
rtuťových	rtuťový	k2eAgInPc2d1	rtuťový
teploměrů	teploměr	k1gInPc2	teploměr
(	(	kIx(	(
<g/>
35	[number]	k4	35
až	až	k9	až
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapilára	kapilára	k1gFnSc1	kapilára
nad	nad	k7c7	nad
nádobkou	nádobka	k1gFnSc7	nádobka
se	se	k3xPyFc4	se
rtutí	rtuť	k1gFnSc7	rtuť
zúžena	zúžen	k2eAgNnPc1d1	zúženo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
teploty	teplota	k1gFnSc2	teplota
rtuťový	rtuťový	k2eAgInSc4d1	rtuťový
sloupec	sloupec	k1gInSc4	sloupec
přetrhne	přetrhnout	k5eAaPmIp3nS	přetrhnout
a	a	k8xC	a
teploměr	teploměr	k1gInSc1	teploměr
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
maximální	maximální	k2eAgFnSc4d1	maximální
naměřenou	naměřený	k2eAgFnSc4d1	naměřená
teplotu	teplota	k1gFnSc4	teplota
(	(	kIx(	(
<g/>
před	před	k7c7	před
dalším	další	k2eAgNnSc7d1	další
použitím	použití	k1gNnSc7	použití
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
"	"	kIx"	"
<g/>
sklepnout	sklepnout	k5eAaPmF	sklepnout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snímač	snímač	k1gInSc4	snímač
teploty	teplota	k1gFnSc2	teplota
Kelvin	kelvin	k1gInSc1	kelvin
stupeň	stupeň	k1gInSc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
stupeň	stupeň	k1gInSc4	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
stupeň	stupeň	k1gInSc4	stupeň
Réaumura	Réaumur	k1gMnSc2	Réaumur
termograf	termograf	k1gInSc4	termograf
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
teploměr	teploměr	k1gInSc1	teploměr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Měření	měření	k1gNnSc2	měření
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
teploměry	teploměr	k1gInPc4	teploměr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Teploměr	teploměr	k1gInSc4	teploměr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
