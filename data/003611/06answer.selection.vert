<s>
Instant	Instant	k1gInSc1	Instant
messaging	messaging	k1gInSc1	messaging
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
IM	IM	kA	IM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
internetová	internetový	k2eAgFnSc1d1	internetová
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgMnSc1d1	umožňující
svým	svůj	k3xOyFgMnPc3	svůj
uživatelům	uživatel	k1gMnPc3	uživatel
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jejich	jejich	k3xOp3gMnPc1	jejich
přátelé	přítel	k1gMnPc1	přítel
jsou	být	k5eAaImIp3nP	být
právě	právě	k6eAd1	právě
připojeni	připojen	k2eAgMnPc1d1	připojen
<g/>
,	,	kIx,	,
a	a	k8xC	a
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
jim	on	k3xPp3gMnPc3	on
posílat	posílat	k5eAaImF	posílat
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
chatovat	chatovat	k5eAaImF	chatovat
<g/>
,	,	kIx,	,
přeposílat	přeposílat	k5eAaImF	přeposílat
soubory	soubor	k1gInPc4	soubor
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
a	a	k8xC	a
i	i	k9	i
jinak	jinak	k6eAd1	jinak
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
