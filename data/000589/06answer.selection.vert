<s>
Gallium	Gallium	k1gNnSc1	Gallium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ga	Ga	k1gFnSc2	Ga
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Gallium	Gallium	k1gNnSc4	Gallium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
modrošedým	modrošedý	k2eAgInSc7d1	modrošedý
nádechem	nádech	k1gInSc7	nádech
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgMnSc1d1	měkký
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
.	.	kIx.	.
</s>
