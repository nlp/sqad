<s>
Gallium	Gallium	k1gNnSc1	Gallium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ga	Ga	k1gFnSc2	Ga
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Gallium	Gallium	k1gNnSc4	Gallium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
modrošedým	modrošedý	k2eAgInSc7d1	modrošedý
nádechem	nádech	k1gInSc7	nádech
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgMnSc1d1	měkký
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
jako	jako	k8xS	jako
složka	složka	k1gFnSc1	složka
polovodičových	polovodičový	k2eAgInPc2d1	polovodičový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
nalézající	nalézající	k2eAgMnSc1d1	nalézající
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgNnSc1d1	běžné
mocenství	mocenství	k1gNnSc1	mocenství
je	být	k5eAaImIp3nS	být
Ga	Ga	k1gFnSc7	Ga
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
a	a	k8xC	a
Ga	Ga	k1gMnSc1	Ga
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
jako	jako	k9	jako
Ga	Ga	k1gFnSc1	Ga
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Gallium	Gallium	k1gNnSc1	Gallium
je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
1,07	[number]	k4	1,07
K.	K.	kA	K.
Gallium	Gallium	k1gNnSc1	Gallium
je	být	k5eAaImIp3nS	být
neušlechtilý	ušlechtilý	k2eNgInSc4d1	neušlechtilý
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
tvořen	tvořit	k5eAaImNgInS	tvořit
molekulami	molekula	k1gFnPc7	molekula
Ga	Ga	k1gFnPc7	Ga
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
gallitých	gallitý	k2eAgFnPc2d1	gallitý
solí	sůl	k1gFnPc2	sůl
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc4d1	podobné
solím	solit	k5eAaImIp1nS	solit
hlinítým	hlinítý	k2eAgInSc7d1	hlinítý
<g/>
.	.	kIx.	.
</s>
<s>
Gallium	Gallium	k1gNnSc4	Gallium
objevil	objevit	k5eAaPmAgMnS	objevit
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
pomocí	pomocí	k7c2	pomocí
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Paul	Paul	k1gMnSc1	Paul
È	È	k?	È
Lecoq	Lecoq	k1gMnSc1	Lecoq
de	de	k?	de
Boisbaudran	Boisbaudran	k1gInSc1	Boisbaudran
v	v	k7c6	v
pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
sfaleritu	sfalerit	k1gInSc6	sfalerit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jej	on	k3xPp3gMnSc4	on
po	po	k7c4	po
své	své	k1gNnSc4	své
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
však	však	k9	však
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
předpovězena	předpovězen	k2eAgFnSc1d1	předpovězena
tvůrcem	tvůrce	k1gMnSc7	tvůrce
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
ruským	ruský	k2eAgMnSc7d1	ruský
chemikem	chemik	k1gMnSc7	chemik
Dmitrijem	Dmitrij	k1gMnSc7	Dmitrij
Ivanovičem	Ivanovič	k1gMnSc7	Ivanovič
Mendělejevem	Mendělejev	k1gInSc7	Mendělejev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
nazval	nazvat	k5eAaPmAgInS	nazvat
ekaaluminium	ekaaluminium	k1gNnSc4	ekaaluminium
<g/>
.	.	kIx.	.
</s>
<s>
Gallium	Gallium	k1gNnSc1	Gallium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
poměrně	poměrně	k6eAd1	poměrně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
mimořádně	mimořádně	k6eAd1	mimořádně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
0,03	[number]	k4	0,03
mikrogramů	mikrogram	k1gInPc2	mikrogram
gallia	gallia	k1gFnSc1	gallia
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
a	a	k8xC	a
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
gallia	gallium	k1gNnSc2	gallium
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
miliarda	miliarda	k4xCgFnSc1	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
rudách	ruda	k1gFnPc6	ruda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
bauxitu	bauxit	k1gInSc6	bauxit
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3.2	[number]	k4	3.2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
a	a	k8xC	a
sfaleritu	sfalerit	k1gInSc2	sfalerit
ZnS	ZnS	k1gFnSc2	ZnS
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
0,002	[number]	k4	0,002
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nejbohatší	bohatý	k2eAgInSc4d3	nejbohatší
nerost	nerost	k1gInSc4	nerost
na	na	k7c4	na
gallium	gallium	k1gNnSc4	gallium
je	být	k5eAaImIp3nS	být
germanit	germanit	k5eAaPmF	germanit
ze	z	k7c2	z
Tsumebu	Tsumeb	k1gInSc2	Tsumeb
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
0,6	[number]	k4	0,6
<g/>
-	-	kIx~	-
<g/>
0,7	[number]	k4	0,7
%	%	kIx~	%
gallia	gallium	k1gNnSc2	gallium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevu	objev	k1gInSc6	objev
gallia	gallium	k1gNnSc2	gallium
bylo	být	k5eAaImAgNnS	být
vypracováno	vypracovat	k5eAaPmNgNnS	vypracovat
několik	několik	k4yIc1	několik
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ho	on	k3xPp3gMnSc4	on
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc4	první
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgFnSc4d1	laboratorní
přípravu	příprava	k1gFnSc4	příprava
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
srazit	srazit	k5eAaPmF	srazit
gallium	gallium	k1gNnSc4	gallium
jako	jako	k8xS	jako
hexakyanoželeznatan	hexakyanoželeznatan	k1gInSc1	hexakyanoželeznatan
a	a	k8xC	a
silným	silný	k2eAgNnSc7d1	silné
žíháním	žíhání	k1gNnSc7	žíhání
převést	převést	k5eAaPmF	převést
ve	v	k7c4	v
směs	směs	k1gFnSc4	směs
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgInSc2d1	železitý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
gallitého	gallitý	k2eAgInSc2d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
tavením	tavení	k1gNnSc7	tavení
s	s	k7c7	s
hydrogensíranem	hydrogensíran	k1gInSc7	hydrogensíran
draselným	draselný	k2eAgInSc7d1	draselný
převede	převést	k5eAaPmIp3nS	převést
v	v	k7c4	v
rozpustnou	rozpustný	k2eAgFnSc4d1	rozpustná
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Srážením	srážení	k1gNnSc7	srážení
roztoku	roztok	k1gInSc2	roztok
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
nadbytkem	nadbytek	k1gInSc7	nadbytek
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
gallitý	gallitý	k2eAgInSc1d1	gallitý
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
železo	železo	k1gNnSc1	železo
<g/>
.	.	kIx.	.
</s>
<s>
Gallium	Gallium	k1gNnSc1	Gallium
se	se	k3xPyFc4	se
ze	z	k7c2	z
zásaditého	zásaditý	k2eAgInSc2d1	zásaditý
roztoku	roztok	k1gInSc2	roztok
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
tavit	tavit	k5eAaImF	tavit
hexakyanoželeznatan	hexakyanoželeznatan	k1gInSc1	hexakyanoželeznatan
gallitý	gallitý	k2eAgInSc1d1	gallitý
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
draselným	draselný	k2eAgInSc7d1	draselný
ve	v	k7c6	v
stříbrném	stříbrný	k1gInSc6	stříbrný
kelímku	kelímek	k1gInSc2	kelímek
a	a	k8xC	a
taveninu	tavenina	k1gFnSc4	tavenina
rozpustit	rozpustit	k5eAaPmF	rozpustit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hydroxidu	hydroxid	k1gInSc2	hydroxid
a	a	k8xC	a
z	z	k7c2	z
filtrátu	filtrát	k1gInSc2	filtrát
se	se	k3xPyFc4	se
po	po	k7c4	po
okyselení	okyselení	k1gNnSc4	okyselení
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
srazí	srazit	k5eAaPmIp3nP	srazit
gallium	gallium	k1gNnSc4	gallium
amoniakem	amoniak	k1gInSc7	amoniak
jako	jako	k8xS	jako
hydroxid	hydroxid	k1gInSc4	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
gallitý	gallitý	k2eAgInSc1d1	gallitý
přechází	přecházet	k5eAaImIp3nS	přecházet
žíháním	žíhání	k1gNnSc7	žíhání
v	v	k7c4	v
oxid	oxid	k1gInSc4	oxid
gallitý	gallitý	k2eAgInSc4d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
redukcí	redukce	k1gFnSc7	redukce
vodíkem	vodík	k1gInSc7	vodík
převede	převést	k5eAaPmIp3nS	převést
v	v	k7c4	v
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
gallium	gallium	k1gNnSc1	gallium
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
z	z	k7c2	z
hutnických	hutnický	k2eAgMnPc2d1	hutnický
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
gallia	gallius	k1gMnSc2	gallius
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k8xC	i
hliník	hliník	k1gInSc1	hliník
a	a	k8xC	a
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
odpad	odpad	k1gInSc4	odpad
se	se	k3xPyFc4	se
působí	působit	k5eAaImIp3nS	působit
hydroxidem	hydroxid	k1gInSc7	hydroxid
sodným	sodný	k2eAgInSc7d1	sodný
a	a	k8xC	a
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
hydroxidů	hydroxid	k1gInPc2	hydroxid
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
filtrací	filtrace	k1gFnPc2	filtrace
se	se	k3xPyFc4	se
roztok	roztok	k1gInSc1	roztok
neutralizuje	neutralizovat	k5eAaBmIp3nS	neutralizovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
sraženina	sraženina	k1gFnSc1	sraženina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
již	již	k6eAd1	již
jen	jen	k9	jen
gallium	gallium	k1gNnSc1	gallium
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
a	a	k8xC	a
cín	cín	k1gInSc1	cín
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svých	svůj	k3xOyFgInPc2	svůj
fosforečnanů	fosforečnan	k1gInPc2	fosforečnan
a	a	k8xC	a
síranů	síran	k1gInPc2	síran
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
rozpouštěným	rozpouštěný	k2eAgInSc7d1	rozpouštěný
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
frakčním	frakční	k2eAgNnSc7d1	frakční
srážením	srážení	k1gNnSc7	srážení
zřeďováním	zřeďování	k1gNnSc7	zřeďování
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
gallium	gallium	k1gNnSc1	gallium
značně	značně	k6eAd1	značně
zkoncentruje	zkoncentrovat	k5eAaPmIp3nS	zkoncentrovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
sulfanem	sulfan	k1gInSc7	sulfan
odstraní	odstranit	k5eAaPmIp3nS	odstranit
cín	cín	k1gInSc1	cín
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
k	k	k7c3	k
roztoku	roztok	k1gInSc3	roztok
přidá	přidat	k5eAaPmIp3nS	přidat
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
se	se	k3xPyFc4	se
fosforečnan	fosforečnan	k1gInSc1	fosforečnan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydroxidu	hydroxid	k1gInSc6	hydroxid
sodném	sodný	k2eAgInSc6d1	sodný
téměř	téměř	k6eAd1	téměř
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
<g/>
,	,	kIx,	,
a	a	k8xC	a
roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
elektrolyzuje	elektrolyzovat	k5eAaPmIp3nS	elektrolyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
velmi	velmi	k6eAd1	velmi
čisté	čistý	k2eAgNnSc4d1	čisté
gallium	gallium	k1gNnSc4	gallium
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
gallium	gallium	k1gNnSc1	gallium
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
germanitu	germanit	k1gInSc2	germanit
jeho	jeho	k3xOp3gNnSc7	jeho
převedením	převedení	k1gNnSc7	převedení
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
gallitý	gallitý	k2eAgInSc4d1	gallitý
GaCl	GaCl	k1gInSc4	GaCl
<g/>
3	[number]	k4	3
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
jeho	jeho	k3xOp3gFnSc2	jeho
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
současného	současný	k2eAgNnSc2d1	současné
využití	využití	k1gNnSc2	využití
nachází	nacházet	k5eAaImIp3nS	nacházet
gallium	gallium	k1gNnSc4	gallium
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mnoha	mnoho	k4c2	mnoho
typů	typ	k1gInPc2	typ
tranzistorů	tranzistor	k1gInPc2	tranzistor
a	a	k8xC	a
především	především	k9	především
světlo	světlo	k1gNnSc1	světlo
emitujících	emitující	k2eAgFnPc2d1	emitující
diod	dioda	k1gFnPc2	dioda
v	v	k7c6	v
polovodičových	polovodičový	k2eAgFnPc6d1	polovodičová
technologiích	technologie	k1gFnPc6	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
arsenid	arsenid	k1gInSc4	arsenid
<g/>
,	,	kIx,	,
fosfid	fosfid	k1gInSc4	fosfid
a	a	k8xC	a
fosfoarzenid	fosfoarzenid	k1gInSc4	fosfoarzenid
gallia	gallius	k1gMnSc2	gallius
mají	mít	k5eAaImIp3nP	mít
polovodičové	polovodičový	k2eAgFnPc4d1	polovodičová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
diod	dioda	k1gFnPc2	dioda
také	také	k9	také
v	v	k7c6	v
tranzistorech	tranzistor	k1gInPc6	tranzistor
<g/>
,	,	kIx,	,
laserech	laser	k1gInPc6	laser
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgFnSc3d1	počítačová
a	a	k8xC	a
kopírovací	kopírovací	k2eAgFnSc3d1	kopírovací
technice	technika	k1gFnSc3	technika
<g/>
.	.	kIx.	.
</s>
<s>
GaAs	GaAs	k6eAd1	GaAs
převádí	převádět	k5eAaImIp3nS	převádět
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
koherentní	koherentní	k2eAgNnSc4d1	koherentní
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
laserové	laserový	k2eAgFnPc1d1	laserová
dioda	dioda	k1gFnSc1	dioda
<g/>
,	,	kIx,	,
LED	LED	kA	LED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
dopant	dopant	k1gInSc1	dopant
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
ferritů	ferrit	k1gInPc2	ferrit
a	a	k8xC	a
granátoidu	granátoid	k1gInSc2	granátoid
GGG	GGG	kA	GGG
(	(	kIx(	(
<g/>
Gadolinium	gadolinium	k1gNnSc1	gadolinium
Gallium	Gallium	k1gNnSc1	Gallium
Garnet	Garnet	k1gInSc1	Garnet
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
laserovou	laserový	k2eAgFnSc4d1	laserová
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Plní	plnit	k5eAaImIp3nS	plnit
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
speciální	speciální	k2eAgInPc1d1	speciální
křemenné	křemenný	k2eAgInPc1d1	křemenný
teploměry	teploměr	k1gInPc1	teploměr
<g/>
,	,	kIx,	,
použitelné	použitelný	k2eAgInPc1d1	použitelný
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
1000	[number]	k4	1000
°	°	k?	°
<g/>
C.	C.	kA	C.
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgNnPc2d1	speciální
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
Jak	jak	k8xS	jak
gallium	gallium	k1gNnSc1	gallium
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
GaAs	GaAsa	k1gFnPc2	GaAsa
jsou	být	k5eAaImIp3nP	být
surovinou	surovina	k1gFnSc7	surovina
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
monokrystalových	monokrystalův	k2eAgInPc2d1	monokrystalův
waferů	wafer	k1gInPc2	wafer
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Czochralského	Czochralský	k2eAgMnSc4d1	Czochralský
metodou	metoda	k1gFnSc7	metoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
a	a	k8xC	a
bismutem	bismut	k1gInSc7	bismut
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
zubní	zubní	k2eAgFnSc2d1	zubní
plomby	plomba	k1gFnSc2	plomba
místo	místo	k7c2	místo
amalgámu	amalgám	k1gInSc2	amalgám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
amalgámu	amalgám	k1gInSc2	amalgám
je	být	k5eAaImIp3nS	být
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lehko	lehko	k6eAd1	lehko
tavitelných	tavitelný	k2eAgFnPc2d1	tavitelná
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
Galinstanu	Galinstan	k1gInSc2	Galinstan
<g/>
,	,	kIx,	,
náhrady	náhrada	k1gFnSc2	náhrada
rtuti	rtuť	k1gFnSc2	rtuť
pro	pro	k7c4	pro
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
techniku	technika	k1gFnSc4	technika
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
teploměr	teploměr	k1gInSc1	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
gallité	gallitý	k2eAgFnPc1d1	gallitý
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
hydrolyticky	hydrolyticky	k6eAd1	hydrolyticky
štěpí	štěpit	k5eAaImIp3nS	štěpit
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
galnaté	galnatý	k2eAgFnPc1d1	galnatý
tvoří	tvořit	k5eAaImIp3nP	tvořit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sloučeninách	sloučenina	k1gFnPc6	sloučenina
dimerní	dimernit	k5eAaPmIp3nS	dimernit
kation	kation	k1gInSc1	kation
Ga	Ga	k1gFnSc2	Ga
4	[number]	k4	4
<g/>
+	+	kIx~	+
2	[number]	k4	2
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
gallitý	gallitý	k2eAgInSc4d1	gallitý
GaCl	GaCl	k1gInSc4	GaCl
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
urychlování	urychlování	k1gNnSc6	urychlování
některých	některý	k3yIgFnPc2	některý
organických	organický	k2eAgFnPc2d1	organická
syntéz	syntéza	k1gFnPc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
chloridu	chlorid	k1gInSc2	chlorid
gallitého	gallitý	k2eAgInSc2d1	gallitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
kysele	kysele	k6eAd1	kysele
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nS	tvořit
hydroxid	hydroxid	k1gInSc1	hydroxid
gallitý	gallitý	k2eAgInSc1d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
zahříváním	zahřívání	k1gNnSc7	zahřívání
gallia	gallium	k1gNnSc2	gallium
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
chloru	chlor	k1gInSc2	chlor
nebo	nebo	k8xC	nebo
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
gallitý	gallitý	k2eAgInSc4d1	gallitý
GaBr	GaBr	k1gInSc4	GaBr
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
jodid	jodid	k1gInSc4	jodid
gallitý	gallitý	k2eAgInSc4d1	gallitý
GaI	GaI	k1gFnSc7	GaI
<g/>
3	[number]	k4	3
ani	ani	k8xC	ani
fluorid	fluorid	k1gInSc4	fluorid
gallitý	gallitý	k2eAgInSc4d1	gallitý
GaF	GaF	k1gFnSc7	GaF
<g/>
3	[number]	k4	3
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
významné	významný	k2eAgFnPc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
gallitý	gallitý	k2eAgInSc4d1	gallitý
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
oxid	oxid	k1gInSc4	oxid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
gallitý	gallitý	k2eAgMnSc1d1	gallitý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
krystalických	krystalický	k2eAgFnPc6d1	krystalická
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
termickým	termický	k2eAgInSc7d1	termický
rozkladem	rozklad	k1gInSc7	rozklad
dusičnanu	dusičnan	k1gInSc2	dusičnan
gallitého	gallitý	k2eAgInSc2d1	gallitý
nebo	nebo	k8xC	nebo
síranu	síran	k1gInSc2	síran
gallitého	gallitý	k2eAgInSc2d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
gallitý	gallitý	k2eAgInSc4d1	gallitý
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
gallitý	gallitý	k2eAgInSc4d1	gallitý
a	a	k8xC	a
sulfan	sulfan	k1gInSc4	sulfan
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
gallitý	gallitý	k2eAgInSc1d1	gallitý
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
redukovat	redukovat	k5eAaBmF	redukovat
na	na	k7c4	na
sulfid	sulfid	k1gInSc4	sulfid
galnatý	galnatý	k2eAgInSc4d1	galnatý
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
gallný	gallný	k2eAgInSc4d1	gallný
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
při	při	k7c6	při
redukci	redukce	k1gFnSc6	redukce
také	také	k9	také
vzniká	vznikat	k5eAaImIp3nS	vznikat
je	on	k3xPp3gInPc4	on
hnědočerný	hnědočerný	k2eAgInSc4d1	hnědočerný
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
gallitý	gallitý	k2eAgInSc1d1	gallitý
vzniká	vznikat	k5eAaImIp3nS	vznikat
zahříváním	zahřívání	k1gNnSc7	zahřívání
gallia	gallius	k1gMnSc2	gallius
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
gallitý	gallitý	k2eAgInSc4d1	gallitý
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
soli	sůl	k1gFnPc1	sůl
-	-	kIx~	-
kamence	kamenec	k1gInPc1	kamenec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
novověku	novověk	k1gInSc6	novověk
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dnes	dnes	k6eAd1	dnes
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kamenec	kamenec	k1gInSc1	kamenec
gallitoamonný	gallitoamonný	k2eAgInSc1d1	gallitoamonný
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
Ga	Ga	k1gFnPc1	Ga
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nitrid	nitrid	k1gInSc1	nitrid
gallitý	gallitý	k2eAgInSc1d1	gallitý
GaN	GaN	k1gFnSc4	GaN
je	být	k5eAaImIp3nS	být
tmavošedý	tmavošedý	k2eAgInSc1d1	tmavošedý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahříváním	zahřívání	k1gNnSc7	zahřívání
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
oxid	oxid	k1gInSc4	oxid
gallitý	gallitý	k2eAgInSc4d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
zahříváním	zahřívání	k1gNnSc7	zahřívání
amoniaku	amoniak	k1gInSc2	amoniak
s	s	k7c7	s
galliem	gallium	k1gNnSc7	gallium
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc4	chlorid
galnatý	galnatý	k2eAgInSc4d1	galnatý
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnPc2	Cl
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dobrý	dobrý	k2eAgInSc1d1	dobrý
vodič	vodič	k1gInSc1	vodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
nedokonalým	dokonalý	k2eNgNnSc7d1	nedokonalé
spálením	spálení	k1gNnSc7	spálení
gallia	gallium	k1gNnSc2	gallium
v	v	k7c6	v
chloru	chlor	k1gInSc6	chlor
nebo	nebo	k8xC	nebo
redukcí	redukce	k1gFnSc7	redukce
chloridu	chlorid	k1gInSc2	chlorid
gallitého	gallitý	k2eAgInSc2d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
gallný	gallný	k2eAgInSc4d1	gallný
Ga	Ga	k1gFnSc7	Ga
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
je	být	k5eAaImIp3nS	být
tmavěhnědý	tmavěhnědý	k2eAgInSc1d1	tmavěhnědý
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
gallitého	gallitý	k2eAgInSc2d1	gallitý
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
gallitého	gallitý	k2eAgInSc2d1	gallitý
s	s	k7c7	s
galliem	gallium	k1gNnSc7	gallium
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc4	hydrid
gallitý	gallitý	k2eAgInSc4d1	gallitý
GaH	GaH	k1gFnSc7	GaH
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
snadno	snadno	k6eAd1	snadno
polymeruje	polymerovat	k5eAaBmIp3nS	polymerovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
hydrid	hydrid	k1gInSc1	hydrid
gallitý	gallitý	k2eAgInSc1d1	gallitý
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
gallitého	gallitý	k2eAgInSc2d1	gallitý
<g/>
.	.	kIx.	.
</s>
<s>
COTON	COTON	kA	COTON
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
WILKINSON	WILKINSON	kA	WILKINSON
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
REMY	remy	k1gNnSc4	remy
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
GREENWOOD	GREENWOOD	kA	GREENWOOD
N.	N.	kA	N.
N.	N.	kA	N.
<g/>
,	,	kIx,	,
EARNSHAW	EARNSHAW	kA	EARNSHAW
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gallium	gallium	k1gNnSc4	gallium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gallium	gallium	k1gNnSc4	gallium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
