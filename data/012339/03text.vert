<p>
<s>
Sluchátka	sluchátko	k1gNnPc1	sluchátko
jsou	být	k5eAaImIp3nP	být
párem	pár	k1gInSc7	pár
speciálních	speciální	k2eAgInPc2d1	speciální
malých	malý	k2eAgInPc2d1	malý
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
nebo	nebo	k8xC	nebo
do	do	k7c2	do
uší	ucho	k1gNnPc2	ucho
posluchače	posluchač	k1gMnSc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgNnPc4d1	skutečné
sluchátka	sluchátko	k1gNnPc4	sluchátko
(	(	kIx(	(
<g/>
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
membrána	membrána	k1gFnSc1	membrána
součástí	součást	k1gFnSc7	součást
magnetického	magnetický	k2eAgInSc2d1	magnetický
obvodu	obvod	k1gInSc2	obvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
připojitelná	připojitelný	k2eAgNnPc1d1	připojitelné
pomocí	pomocí	k7c2	pomocí
konektoru	konektor	k1gInSc2	konektor
jack	jacka	k1gFnPc2	jacka
<g/>
.	.	kIx.	.
</s>
<s>
Připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
k	k	k7c3	k
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
walkman	walkman	k1gInSc4	walkman
<g/>
,	,	kIx,	,
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
CD	CD	kA	CD
přehrávač	přehrávač	k1gInSc1	přehrávač
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgInSc1d1	digitální
audio	audio	k2eAgInSc1d1	audio
přehrávač	přehrávač	k1gInSc1	přehrávač
(	(	kIx(	(
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
přehrávač	přehrávač	k1gInSc1	přehrávač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
sluchátka	sluchátko	k1gNnPc1	sluchátko
v	v	k7c6	v
sobě	se	k3xPyFc3	se
integrují	integrovat	k5eAaBmIp3nP	integrovat
přijímač	přijímač	k1gInSc4	přijímač
rádia	rádio	k1gNnSc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
bezdrátová	bezdrátový	k2eAgNnPc4d1	bezdrátové
sluchátka	sluchátko	k1gNnPc4	sluchátko
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnPc4d1	využívající
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
analogové	analogový	k2eAgInPc1d1	analogový
FM	FM	kA	FM
<g/>
,	,	kIx,	,
digitální	digitální	k2eAgMnSc1d1	digitální
bluetooth	bluetooth	k1gMnSc1	bluetooth
<g/>
,	,	kIx,	,
Kleer	Kleer	k1gMnSc1	Kleer
<g/>
,	,	kIx,	,
Wi-Fi	Wi-F	k1gMnPc1	Wi-F
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
světla	světlo	k1gNnSc2	světlo
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
se	s	k7c7	s
"	"	kIx"	"
<g/>
základnou	základna	k1gFnSc7	základna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sluchátka	sluchátko	k1gNnSc2	sluchátko
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Američan	Američan	k1gMnSc1	Američan
Nathaniel	Nathaniel	k1gMnSc1	Nathaniel
Baldwin	Baldwin	k1gMnSc1	Baldwin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
sluchátek	sluchátko	k1gNnPc2	sluchátko
==	==	k?	==
</s>
</p>
<p>
<s>
Sluchátka	sluchátko	k1gNnSc2	sluchátko
dělíme	dělit	k5eAaImIp1nP	dělit
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
usazení	usazení	k1gNnSc2	usazení
na	na	k7c4	na
ucho	ucho	k1gNnSc4	ucho
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
ucha	ucho	k1gNnSc2	ucho
na	na	k7c4	na
circumaurální	circumaurální	k2eAgFnSc4d1	circumaurální
<g/>
,	,	kIx,	,
supraaurální	supraaurální	k2eAgFnSc4d1	supraaurální
a	a	k8xC	a
intraaurální	intraaurální	k2eAgFnSc4d1	intraaurální
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
provedení	provedení	k1gNnSc2	provedení
mušlí	mušle	k1gFnPc2	mušle
na	na	k7c4	na
otevřená	otevřený	k2eAgNnPc4d1	otevřené
a	a	k8xC	a
uzavřená	uzavřený	k2eAgNnPc4d1	uzavřené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
usazení	usazení	k1gNnSc2	usazení
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Circumaurální	Circumaurální	k2eAgNnPc4d1	Circumaurální
sluchátka	sluchátko	k1gNnPc4	sluchátko
====	====	k?	====
</s>
</p>
<p>
<s>
Circumaurální	Circumaurální	k2eAgNnPc1d1	Circumaurální
sluchátka	sluchátko	k1gNnPc1	sluchátko
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
full-size	fullize	k1gFnSc1	full-size
sluchátka	sluchátko	k1gNnSc2	sluchátko
<g/>
,	,	kIx,	,
obepínají	obepínat	k5eAaImIp3nP	obepínat
celé	celý	k2eAgNnSc4d1	celé
ucho	ucho	k1gNnSc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Náušníky	náušník	k1gInPc1	náušník
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
koženky	koženka	k1gFnSc2	koženka
<g/>
,	,	kIx,	,
textilu	textil	k1gInSc2	textil
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
dražších	drahý	k2eAgNnPc2d2	dražší
sluchátek	sluchátko	k1gNnPc2	sluchátko
z	z	k7c2	z
veluru	velur	k1gInSc2	velur
nebo	nebo	k8xC	nebo
z	z	k7c2	z
umělé	umělý	k2eAgFnSc2d1	umělá
či	či	k8xC	či
pravé	pravý	k2eAgFnSc2d1	pravá
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
může	moct	k5eAaImIp3nS	moct
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
izolovat	izolovat	k5eAaBmF	izolovat
posluchače	posluchač	k1gMnPc4	posluchač
od	od	k7c2	od
okolních	okolní	k2eAgInPc2d1	okolní
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
pocení	pocení	k1gNnSc2	pocení
uší	ucho	k1gNnPc2	ucho
u	u	k7c2	u
modelů	model	k1gInPc2	model
s	s	k7c7	s
uzavřenými	uzavřený	k2eAgFnPc7d1	uzavřená
mušlemi	mušle	k1gFnPc7	mušle
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
domácích	domácí	k2eAgInPc2d1	domácí
a	a	k8xC	a
studiových	studiový	k2eAgInPc2d1	studiový
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Supraaurální	Supraaurální	k2eAgNnPc4d1	Supraaurální
sluchátka	sluchátko	k1gNnPc4	sluchátko
====	====	k?	====
</s>
</p>
<p>
<s>
Sluchátka	sluchátko	k1gNnPc1	sluchátko
leží	ležet	k5eAaImIp3nP	ležet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
uchu	ucho	k1gNnSc6	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Náušníky	náušník	k1gInPc1	náušník
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
molitanu	molitan	k1gInSc2	molitan
nebo	nebo	k8xC	nebo
koženky	koženka	k1gFnSc2	koženka
<g/>
.	.	kIx.	.
</s>
<s>
Supraaurální	Supraaurální	k2eAgFnSc1d1	Supraaurální
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
přenosných	přenosný	k2eAgMnPc2d1	přenosný
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
u	u	k7c2	u
domácích	domácí	k1gMnPc2	domácí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Intraaurální	Intraaurální	k2eAgNnPc4d1	Intraaurální
sluchátka	sluchátko	k1gNnPc4	sluchátko
====	====	k?	====
</s>
</p>
<p>
<s>
Intraaurální	Intraaurální	k2eAgNnPc1d1	Intraaurální
sluchátka	sluchátko	k1gNnPc1	sluchátko
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
pecky	pecek	k1gInPc4	pecek
a	a	k8xC	a
špunty	špunty	k?	špunty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Pecky	pecek	k1gInPc4	pecek
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Pecky	pecky	k6eAd1	pecky
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
earbuds	earbuds	k6eAd1	earbuds
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
vyústění	vyústění	k1gNnSc2	vyústění
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
dodávány	dodávat	k5eAaImNgFnP	dodávat
k	k	k7c3	k
přenosným	přenosný	k2eAgInPc3d1	přenosný
přehrávačům	přehrávač	k1gInPc3	přehrávač
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
zvuková	zvukový	k2eAgFnSc1d1	zvuková
kvalita	kvalita	k1gFnSc1	kvalita
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgFnSc1d1	začínající
už	už	k9	už
u	u	k7c2	u
dvanácti	dvanáct	k4xCc2	dvanáct
českých	český	k2eAgFnPc2d1	Česká
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Yuin	Yuina	k1gFnPc2	Yuina
ale	ale	k8xC	ale
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
pecky	pecek	k1gInPc4	pecek
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
sehnat	sehnat	k5eAaPmF	sehnat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
tisíce	tisíc	k4xCgInSc2	tisíc
korun	koruna	k1gFnPc2	koruna
výše	výše	k1gFnPc4	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Špunty	Špunty	k?	Špunty
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Špunty	Špunty	k?	Špunty
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
in-ear	inar	k1gInSc1	in-ear
sluchátka	sluchátko	k1gNnSc2	sluchátko
či	či	k8xC	či
in-ear	inar	k1gInSc4	in-ear
monitory	monitor	k1gInPc7	monitor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zasouvají	zasouvat	k5eAaImIp3nP	zasouvat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
<s>
Univerzální	univerzální	k2eAgInSc1d1	univerzální
špunty	špunty	k?	špunty
mají	mít	k5eAaImIp3nP	mít
silikonový	silikonový	k2eAgInSc4d1	silikonový
nebo	nebo	k8xC	nebo
pěnový	pěnový	k2eAgInSc4d1	pěnový
nástavec	nástavec	k1gInSc4	nástavec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ucpe	ucpat	k5eAaPmIp3nS	ucpat
zvukovod	zvukovod	k1gInSc4	zvukovod
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
posluchač	posluchač	k1gMnSc1	posluchač
není	být	k5eNaImIp3nS	být
rušen	rušen	k2eAgInSc4d1	rušen
zvuky	zvuk	k1gInPc4	zvuk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Špunty	Špunty	k?	Špunty
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
podle	podle	k7c2	podle
otisků	otisk	k1gInPc2	otisk
uší	ucho	k1gNnPc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
vhodné	vhodný	k2eAgFnPc4d1	vhodná
především	především	k9	především
do	do	k7c2	do
hlučného	hlučný	k2eAgNnSc2d1	hlučné
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
je	on	k3xPp3gNnSc4	on
také	také	k9	také
například	například	k6eAd1	například
hudebníci	hudebník	k1gMnPc1	hudebník
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Levné	levný	k2eAgInPc1d1	levný
špunty	špunty	k?	špunty
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
kvalitou	kvalita	k1gFnSc7	kvalita
nepřekypují	překypovat	k5eNaImIp3nP	překypovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
i	i	k9	i
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
<g/>
,	,	kIx,	,
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
nad	nad	k7c4	nad
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
českých	český	k2eAgFnPc2d1	Česká
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Špunty	Špunty	k?	Špunty
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
více	hodně	k6eAd2	hodně
měničů	měnič	k1gInPc2	měnič
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
sluchátku	sluchátko	k1gNnSc6	sluchátko
-	-	kIx~	-
univerzální	univerzální	k2eAgFnSc2d1	univerzální
jeden	jeden	k4xCgInSc1	jeden
až	až	k9	až
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
špunty	špunty	k?	špunty
na	na	k7c6	na
míru	mír	k1gInSc6	mír
až	až	k9	až
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
provedení	provedení	k1gNnSc2	provedení
mušlí	mušle	k1gFnPc2	mušle
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Otevřená	otevřený	k2eAgNnPc4d1	otevřené
sluchátka	sluchátko	k1gNnPc4	sluchátko
====	====	k?	====
</s>
</p>
<p>
<s>
Mušle	mušle	k1gFnPc1	mušle
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
ze	z	k7c2	z
zvukově	zvukově	k6eAd1	zvukově
propustného	propustný	k2eAgInSc2d1	propustný
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
mřížka	mřížka	k1gFnSc1	mřížka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přirozenější	přirozený	k2eAgFnSc4d2	přirozenější
reprodukci	reprodukce	k1gFnSc4	reprodukce
<g/>
,	,	kIx,	,
posluchač	posluchač	k1gMnSc1	posluchač
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
rušen	rušen	k2eAgInSc4d1	rušen
zvuky	zvuk	k1gInPc4	zvuk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poslouchat	poslouchat	k5eAaImF	poslouchat
v	v	k7c6	v
tichu	ticho	k1gNnSc6	ticho
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
ruší	rušit	k5eAaImIp3nS	rušit
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
-	-	kIx~	-
sluchátka	sluchátko	k1gNnSc2	sluchátko
hrají	hrát	k5eAaImIp3nP	hrát
nejen	nejen	k6eAd1	nejen
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
uší	ucho	k1gNnPc2	ucho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
i	i	k9	i
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
-	-	kIx~	-
člověk	člověk	k1gMnSc1	člověk
není	být	k5eNaImIp3nS	být
zvukově	zvukově	k6eAd1	zvukově
izolován	izolovat	k5eAaBmNgMnS	izolovat
od	od	k7c2	od
dění	dění	k1gNnSc2	dění
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
slyší	slyšet	k5eAaImIp3nP	slyšet
např.	např.	kA	např.
projíždějící	projíždějící	k2eAgNnSc1d1	projíždějící
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Uzavřená	uzavřený	k2eAgNnPc4d1	uzavřené
sluchátka	sluchátko	k1gNnPc4	sluchátko
====	====	k?	====
</s>
</p>
<p>
<s>
Mušle	mušle	k1gFnPc1	mušle
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
ze	z	k7c2	z
zvukově	zvukově	k6eAd1	zvukově
nepropustného	propustný	k2eNgInSc2d1	nepropustný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Posluchač	posluchač	k1gMnSc1	posluchač
neslyší	slyšet	k5eNaImIp3nS	slyšet
či	či	k8xC	či
jen	jen	k9	jen
špatně	špatně	k6eAd1	špatně
slyší	slyšet	k5eAaImIp3nS	slyšet
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
)	)	kIx)	)
zvuky	zvuk	k1gInPc7	zvuk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Sluchátka	sluchátko	k1gNnPc1	sluchátko
také	také	k9	také
nehrají	hrát	k5eNaImIp3nP	hrát
"	"	kIx"	"
<g/>
ven	ven	k6eAd1	ven
<g/>
"	"	kIx"	"
-	-	kIx~	-
zvuk	zvuk	k1gInSc1	zvuk
slyší	slyšet	k5eAaImIp3nS	slyšet
jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
cenových	cenový	k2eAgFnPc6d1	cenová
kategoriích	kategorie	k1gFnPc6	kategorie
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uzavřená	uzavřený	k2eAgNnPc1d1	uzavřené
sluchátka	sluchátko	k1gNnPc1	sluchátko
mají	mít	k5eAaImIp3nP	mít
horší	zlý	k2eAgInSc4d2	horší
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
než	než	k8xS	než
stejně	stejně	k6eAd1	stejně
drahá	drahá	k1gFnSc1	drahá
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Polootevřená	polootevřený	k2eAgNnPc4d1	polootevřené
<g/>
/	/	kIx~	/
<g/>
polouzavřená	polouzavřený	k2eAgNnPc4d1	polouzavřené
sluchátka	sluchátko	k1gNnPc4	sluchátko
====	====	k?	====
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
také	také	k9	také
s	s	k7c7	s
termínem	termín	k1gInSc7	termín
polootevřená	polootevřený	k2eAgNnPc4d1	polootevřené
<g/>
/	/	kIx~	/
<g/>
polouzavřená	polouzavřený	k2eAgNnPc4d1	polouzavřené
sluchátka	sluchátko	k1gNnPc4	sluchátko
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
takto	takto	k6eAd1	takto
označují	označovat	k5eAaImIp3nP	označovat
otevřená	otevřený	k2eAgNnPc1d1	otevřené
sluchátka	sluchátko	k1gNnPc1	sluchátko
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gFnPc6	jejichž
mušlích	mušle	k1gFnPc6	mušle
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
méně	málo	k6eAd2	málo
otvorů	otvor	k1gInPc2	otvor
než	než	k8xS	než
u	u	k7c2	u
plně	plně	k6eAd1	plně
otevřených	otevřený	k2eAgFnPc2d1	otevřená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
impedance	impedance	k1gFnSc2	impedance
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Sluchátka	sluchátko	k1gNnSc2	sluchátko
nízkoimpedanční	nízkoimpedanční	k2eAgNnSc1d1	nízkoimpedanční
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
typ	typ	k1gInSc4	typ
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
k	k	k7c3	k
moderním	moderní	k2eAgInPc3d1	moderní
přístrojům	přístroj	k1gInPc3	přístroj
-	-	kIx~	-
počítačům	počítač	k1gInPc3	počítač
<g/>
,	,	kIx,	,
přehrávačům	přehrávač	k1gInPc3	přehrávač
<g/>
,	,	kIx,	,
mobilním	mobilní	k2eAgInPc3d1	mobilní
telefonům	telefon	k1gInPc3	telefon
<g/>
,	,	kIx,	,
televizním	televizní	k2eAgMnPc3d1	televizní
či	či	k8xC	či
rozhlasovým	rozhlasový	k2eAgMnPc3d1	rozhlasový
přijímačům	přijímač	k1gMnPc3	přijímač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Impedance	impedance	k1gFnSc1	impedance
jejich	jejich	k3xOp3gInSc2	jejich
elektroakustického	elektroakustický	k2eAgInSc2d1	elektroakustický
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
až	až	k8xS	až
desítkách	desítka	k1gFnPc6	desítka
ohmů	ohm	k1gInPc2	ohm
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
impedancí	impedance	k1gFnSc7	impedance
běžných	běžný	k2eAgInPc2d1	běžný
reproduktorů	reproduktor	k1gInPc2	reproduktor
i	i	k8xC	i
s	s	k7c7	s
výstupní	výstupní	k2eAgFnSc7d1	výstupní
impedancí	impedance	k1gFnSc7	impedance
běžných	běžný	k2eAgInPc2d1	běžný
polovodičových	polovodičový	k2eAgInPc2d1	polovodičový
zesilovačů	zesilovač	k1gInPc2	zesilovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektroakustický	elektroakustický	k2eAgInSc1d1	elektroakustický
měnič	měnič	k1gInSc1	měnič
těchto	tento	k3xDgNnPc2	tento
sluchátek	sluchátko	k1gNnPc2	sluchátko
je	být	k5eAaImIp3nS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
na	na	k7c6	na
magnetodynamickém	magnetodynamický	k2eAgInSc6d1	magnetodynamický
principu	princip	k1gInSc6	princip
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
běžné	běžný	k2eAgInPc1d1	běžný
dynamické	dynamický	k2eAgInPc1d1	dynamický
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
plastová	plastový	k2eAgFnSc1d1	plastová
membrána	membrána	k1gFnSc1	membrána
sluchátka	sluchátko	k1gNnSc2	sluchátko
je	být	k5eAaImIp3nS	být
mechanicky	mechanicky	k6eAd1	mechanicky
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
prstencovou	prstencový	k2eAgFnSc7d1	prstencová
válcovou	válcový	k2eAgFnSc7d1	válcová
cívkou	cívka	k1gFnSc7	cívka
kmitající	kmitající	k2eAgFnSc7d1	kmitající
v	v	k7c6	v
kruhové	kruhový	k2eAgFnSc6d1	kruhová
vzduchové	vzduchový	k2eAgFnSc6d1	vzduchová
mezeře	mezera	k1gFnSc6	mezera
silného	silný	k2eAgInSc2d1	silný
permanentního	permanentní	k2eAgInSc2d1	permanentní
magnetu	magnet	k1gInSc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
závitů	závit	k1gInPc2	závit
drátu	drát	k1gInSc2	drát
navinutého	navinutý	k2eAgInSc2d1	navinutý
na	na	k7c6	na
cívce	cívka	k1gFnSc6	cívka
je	být	k5eAaImIp3nS	být
omezený	omezený	k2eAgInSc1d1	omezený
úzkým	úzký	k2eAgInSc7d1	úzký
prostorem	prostor	k1gInSc7	prostor
magnetické	magnetický	k2eAgFnSc2d1	magnetická
mezery	mezera	k1gFnSc2	mezera
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
navinout	navinout	k5eAaPmF	navinout
cívku	cívka	k1gFnSc4	cívka
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
závitá	závitat	k5eAaPmIp3nS	závitat
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
impedancí	impedance	k1gFnSc7	impedance
<g/>
.	.	kIx.	.
</s>
<s>
Naoplátku	Naoplátko	k1gNnSc6	Naoplátko
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
vyniká	vynikat	k5eAaImIp3nS	vynikat
velkým	velký	k2eAgInSc7d1	velký
kmitočtovým	kmitočtový	k2eAgInSc7d1	kmitočtový
rozsahem	rozsah	k1gInSc7	rozsah
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
slyšitelném	slyšitelný	k2eAgNnSc6d1	slyšitelné
spektru	spektrum	k1gNnSc6	spektrum
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
věrným	věrný	k2eAgInSc7d1	věrný
přednesem	přednes	k1gInSc7	přednes
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sluchátka	sluchátko	k1gNnSc2	sluchátko
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
bývají	bývat	k5eAaImIp3nP	bývat
nejčastěji	často	k6eAd3	často
konstruovaná	konstruovaný	k2eAgNnPc1d1	konstruované
jako	jako	k8xS	jako
stereofonní	stereofonní	k2eAgNnPc1d1	stereofonní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
každého	každý	k3xTgInSc2	každý
systému	systém	k1gInSc2	systém
přiváděn	přiváděn	k2eAgInSc4d1	přiváděn
signál	signál	k1gInSc4	signál
samostatně	samostatně	k6eAd1	samostatně
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sluchátka	sluchátko	k1gNnSc2	sluchátko
vysokoimpedanční	vysokoimpedanční	k2eAgNnSc1d1	vysokoimpedanční
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
často	často	k6eAd1	často
nepřesně	přesně	k6eNd1	přesně
nazývána	nazýván	k2eAgNnPc1d1	nazýváno
"	"	kIx"	"
<g/>
sluchátka	sluchátko	k1gNnPc1	sluchátko
vysokoohmová	vysokoohmový	k2eAgNnPc1d1	vysokoohmový
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nP	patřit
převážně	převážně	k6eAd1	převážně
starší	starý	k2eAgInPc1d2	starší
typy	typ	k1gInPc1	typ
sluchátek	sluchátko	k1gNnPc2	sluchátko
používaných	používaný	k2eAgNnPc2d1	používané
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
u	u	k7c2	u
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vysílací	vysílací	k2eAgFnSc2d1	vysílací
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
komunikačním	komunikační	k2eAgInPc3d1	komunikační
přijímačům	přijímač	k1gInPc3	přijímač
a	a	k8xC	a
k	k	k7c3	k
jednoduchým	jednoduchý	k2eAgInPc3d1	jednoduchý
přijímačům	přijímač	k1gInPc3	přijímač
systému	systém	k1gInSc2	systém
Jednolampovka	jednolampovka	k1gFnSc1	jednolampovka
či	či	k8xC	či
Krystalka	krystalka	k1gFnSc1	krystalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Impedance	impedance	k1gFnSc1	impedance
jejich	jejich	k3xOp3gInSc2	jejich
elektroakustického	elektroakustický	k2eAgInSc2d1	elektroakustický
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
až	až	k9	až
tisících	tisící	k4xOgInPc2	tisící
ohmů	ohm	k1gInPc2	ohm
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
4000	[number]	k4	4000
ohmů	ohm	k1gInPc2	ohm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
výstupní	výstupní	k2eAgFnSc7d1	výstupní
impedancí	impedance	k1gFnSc7	impedance
elektronkových	elektronkový	k2eAgInPc2d1	elektronkový
zesilovacích	zesilovací	k2eAgInPc2d1	zesilovací
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektroakustický	elektroakustický	k2eAgInSc1d1	elektroakustický
měnič	měnič	k1gInSc1	měnič
těchto	tento	k3xDgNnPc2	tento
sluchátek	sluchátko	k1gNnPc2	sluchátko
je	být	k5eAaImIp3nS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
na	na	k7c6	na
magnetoelektrickém	magnetoelektrický	k2eAgInSc6d1	magnetoelektrický
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kovovou	kovový	k2eAgFnSc4d1	kovová
membránu	membrána	k1gFnSc4	membrána
přes	přes	k7c4	přes
pólové	pólový	k2eAgInPc4d1	pólový
nástavce	nástavec	k1gInPc4	nástavec
trvale	trvale	k6eAd1	trvale
mírně	mírně	k6eAd1	mírně
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
permanentní	permanentní	k2eAgInSc4d1	permanentní
magnet	magnet	k1gInSc4	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pólových	pólový	k2eAgInPc6d1	pólový
nástavcích	nástavec	k1gInPc6	nástavec
jsou	být	k5eAaImIp3nP	být
navinuty	navinut	k2eAgFnPc1d1	navinuta
budící	budící	k2eAgFnPc1d1	budící
cívky	cívka	k1gFnPc1	cívka
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
závitů	závit	k1gInPc2	závit
jemného	jemný	k2eAgInSc2d1	jemný
měděného	měděný	k2eAgInSc2d1	měděný
drátu	drát	k1gInSc2	drát
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
0,02	[number]	k4	0,02
až	až	k9	až
0,05	[number]	k4	0,05
<g/>
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
připojení	připojení	k1gNnSc6	připojení
signálu	signál	k1gInSc2	signál
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
cívkám	cívka	k1gFnPc3	cívka
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
druhotné	druhotný	k2eAgNnSc1d1	druhotné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
sčítá	sčítat	k5eAaImIp3nS	sčítat
nebo	nebo	k8xC	nebo
odečítá	odečítat	k5eAaImIp3nS	odečítat
s	s	k7c7	s
polem	polem	k6eAd1	polem
vytvářeným	vytvářený	k2eAgInSc7d1	vytvářený
trvalým	trvalý	k2eAgInSc7d1	trvalý
magnetem	magnet	k1gInSc7	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
membrána	membrána	k1gFnSc1	membrána
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
k	k	k7c3	k
magnetu	magnet	k1gInSc2	magnet
přitahována	přitahovat	k5eAaImNgFnS	přitahovat
a	a	k8xC	a
chvěje	chvět	k5eAaImIp3nS	chvět
se	se	k3xPyFc4	se
v	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
přiváděného	přiváděný	k2eAgInSc2d1	přiváděný
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
citlivý	citlivý	k2eAgInSc1d1	citlivý
a	a	k8xC	a
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
slyšitelného	slyšitelný	k2eAgInSc2d1	slyšitelný
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
sluchátkách	sluchátko	k1gNnPc6	sluchátko
stačí	stačit	k5eAaBmIp3nS	stačit
velmi	velmi	k6eAd1	velmi
slabý	slabý	k2eAgInSc4d1	slabý
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Abyste	aby	kYmCp2nP	aby
ve	v	k7c6	v
sluchátkách	sluchátko	k1gNnPc6	sluchátko
uslyšeli	uslyšet	k5eAaPmAgMnP	uslyšet
slabě	slabě	k6eAd1	slabě
hudbu	hudba	k1gFnSc4	hudba
nebo	nebo	k8xC	nebo
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
napětí	napětí	k1gNnSc1	napětí
už	už	k6eAd1	už
od	od	k7c2	od
0,01	[number]	k4	0,01
voltu	volt	k1gInSc2	volt
a	a	k8xC	a
výkon	výkon	k1gInSc1	výkon
0,000	[number]	k4	0,000
000	[number]	k4	000
025	[number]	k4	025
wattu	watt	k1gInSc2	watt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
pro	pro	k7c4	pro
srozumitelnou	srozumitelný	k2eAgFnSc4d1	srozumitelná
reprodukci	reprodukce	k1gFnSc4	reprodukce
použít	použít	k5eAaPmF	použít
nevýkonný	výkonný	k2eNgInSc4d1	nevýkonný
zesilovací	zesilovací	k2eAgInSc4d1	zesilovací
stupeň	stupeň	k1gInSc4	stupeň
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
krystalku	krystalka	k1gFnSc4	krystalka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
bez	bez	k7c2	bez
vnějšího	vnější	k2eAgInSc2d1	vnější
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
magnetoelektrický	magnetoelektrický	k2eAgInSc1d1	magnetoelektrický
systém	systém	k1gInSc1	systém
trpí	trpět	k5eAaImIp3nS	trpět
nerovnoměrnou	rovnoměrný	k2eNgFnSc7d1	nerovnoměrná
frekvenční	frekvenční	k2eAgFnSc7d1	frekvenční
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
rozsah	rozsah	k1gInSc1	rozsah
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
sluchátek	sluchátko	k1gNnPc2	sluchátko
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
250	[number]	k4	250
do	do	k7c2	do
3000	[number]	k4	3000
<g/>
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
typický	typický	k2eAgInSc4d1	typický
plechový	plechový	k2eAgInSc4d1	plechový
telefonní	telefonní	k2eAgInSc4d1	telefonní
zvuk	zvuk	k1gInSc4	zvuk
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
hodí	hodit	k5eAaImIp3nS	hodit
spíš	spíš	k9	spíš
pro	pro	k7c4	pro
komunikační	komunikační	k2eAgInPc4d1	komunikační
účely	účel	k1gInPc4	účel
-	-	kIx~	-
reprodukci	reprodukce	k1gFnSc4	reprodukce
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
nebo	nebo	k8xC	nebo
telegrafního	telegrafní	k2eAgInSc2d1	telegrafní
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kvalitní	kvalitní	k2eAgFnSc3d1	kvalitní
reprodukci	reprodukce	k1gFnSc3	reprodukce
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sluchátka	sluchátko	k1gNnSc2	sluchátko
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
konstruována	konstruovat	k5eAaImNgNnP	konstruovat
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k9	jako
supraaurální	supraaurální	k2eAgFnSc1d1	supraaurální
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
monofoní	monofoň	k1gFnSc7	monofoň
(	(	kIx(	(
<g/>
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
mušlí	mušle	k1gFnPc2	mušle
je	být	k5eAaImIp3nS	být
přiváděn	přiváděn	k2eAgInSc1d1	přiváděn
stejný	stejný	k2eAgInSc1d1	stejný
signál	signál	k1gInSc1	signál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
získání	získání	k1gNnSc4	získání
vysoké	vysoký	k2eAgFnSc2d1	vysoká
impedance	impedance	k1gFnSc2	impedance
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
propojeny	propojen	k2eAgInPc1d1	propojen
do	do	k7c2	do
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
nastavení	nastavení	k1gNnSc1	nastavení
správné	správný	k2eAgFnSc2d1	správná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
membrány	membrána	k1gFnSc2	membrána
od	od	k7c2	od
pólových	pólový	k2eAgInPc2d1	pólový
nástavců	nástavec	k1gInPc2	nástavec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
sluchátek	sluchátko	k1gNnPc2	sluchátko
lze	lze	k6eAd1	lze
tuto	tento	k3xDgFnSc4	tento
mezeru	mezera	k1gFnSc4	mezera
otáčením	otáčení	k1gNnSc7	otáčení
mušle	mušle	k1gFnSc2	mušle
regulovat	regulovat	k5eAaImF	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezera	mezera	k1gFnSc1	mezera
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
co	co	k9	co
nejmenší	malý	k2eAgInPc1d3	nejmenší
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
hranicí	hranice	k1gFnSc7	hranice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
membrána	membrána	k1gFnSc1	membrána
začala	začít	k5eAaPmAgFnS	začít
dotýkat	dotýkat	k5eAaImF	dotýkat
magnetu	magnet	k1gInSc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
sluchátko	sluchátko	k1gNnSc4	sluchátko
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Nástavce	nástavec	k1gInPc1	nástavec
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
naprosto	naprosto	k6eAd1	naprosto
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
koroze	koroze	k1gFnSc2	koroze
a	a	k8xC	a
bez	bez	k7c2	bez
přimagnetovaných	přimagnetovaný	k2eAgFnPc2d1	přimagnetovaný
pilinek	pilinka	k1gFnPc2	pilinka
či	či	k8xC	či
jiného	jiný	k2eAgNnSc2d1	jiné
smetí	smetí	k1gNnSc2	smetí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
funkci	funkce	k1gFnSc4	funkce
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
magnet	magnet	k1gInSc1	magnet
stářím	stář	k1gFnPc3	stář
zesláblý	zesláblý	k2eAgInSc4d1	zesláblý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
internetových	internetový	k2eAgInPc6d1	internetový
návodech	návod	k1gInPc6	návod
nebo	nebo	k8xC	nebo
starých	starý	k2eAgInPc6d1	starý
pláncích	plánek	k1gInPc6	plánek
předepsáno	předepsat	k5eAaPmNgNnS	předepsat
použití	použití	k1gNnSc2	použití
"	"	kIx"	"
<g/>
vysokoohmových	vysokoohmův	k2eAgNnPc2d1	vysokoohmův
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
úspěšně	úspěšně	k6eAd1	úspěšně
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
náhradu	náhrada	k1gFnSc4	náhrada
sluchátka	sluchátko	k1gNnSc2	sluchátko
nízkoimpedanční	nízkoimpedanční	k2eAgInSc1d1	nízkoimpedanční
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
před	před	k7c4	před
ně	on	k3xPp3gMnPc4	on
bude	být	k5eAaImBp3nS	být
vřazen	vřazen	k2eAgInSc1d1	vřazen
malý	malý	k2eAgInSc1d1	malý
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
<g/>
)	)	kIx)	)
převodní	převodní	k2eAgInSc4d1	převodní
transformátor	transformátor	k1gInSc4	transformátor
<g/>
.	.	kIx.	.
</s>
<s>
Vyhoví	vyhovit	k5eAaPmIp3nP	vyhovit
např.	např.	kA	např.
malý	malý	k2eAgInSc4d1	malý
transformátorek	transformátorek	k1gInSc4	transformátorek
230	[number]	k4	230
<g/>
V	V	kA	V
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
V	V	kA	V
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
W	W	kA	W
<g/>
,	,	kIx,	,
připojený	připojený	k2eAgInSc1d1	připojený
primárním	primární	k2eAgNnSc7d1	primární
vinutím	vinutí	k1gNnSc7	vinutí
230V	[number]	k4	230V
ke	k	k7c3	k
zdroji	zdroj	k1gInSc3	zdroj
signálu	signál	k1gInSc2	signál
a	a	k8xC	a
sekundárním	sekundární	k2eAgNnSc7d1	sekundární
vinutím	vinutí	k1gNnSc7	vinutí
24V	[number]	k4	24V
k	k	k7c3	k
nízkoimpedančním	nízkoimpedanční	k2eAgNnPc3d1	nízkoimpedanční
sluchátkům	sluchátko	k1gNnPc3	sluchátko
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Častá	častý	k2eAgFnSc1d1	častá
snaha	snaha	k1gFnSc1	snaha
<g/>
,	,	kIx,	,
nahradit	nahradit	k5eAaPmF	nahradit
chybějící	chybějící	k2eAgFnSc4d1	chybějící
impedanci	impedance	k1gFnSc4	impedance
sluchátek	sluchátko	k1gNnPc2	sluchátko
vřazením	vřazení	k1gNnPc3	vřazení
dodatečného	dodatečný	k2eAgInSc2d1	dodatečný
sériového	sériový	k2eAgInSc2d1	sériový
rezistoru	rezistor	k1gInSc2	rezistor
je	být	k5eAaImIp3nS	být
technický	technický	k2eAgInSc1d1	technický
nesmysl	nesmysl	k1gInSc1	nesmysl
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
použijete	použít	k5eAaPmIp2nP	použít
nízkoimpedanční	nízkoimpedanční	k2eAgNnPc4d1	nízkoimpedanční
sluchátka	sluchátko	k1gNnPc4	sluchátko
přímo	přímo	k6eAd1	přímo
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
transformátoru	transformátor	k1gInSc2	transformátor
<g/>
)	)	kIx)	)
u	u	k7c2	u
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
výslovně	výslovně	k6eAd1	výslovně
předepsáno	předepsán	k2eAgNnSc1d1	předepsáno
použití	použití	k1gNnSc1	použití
sluchátek	sluchátko	k1gNnPc2	sluchátko
vysokoimpedančních	vysokoimpedanční	k2eAgInPc2d1	vysokoimpedanční
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
nebude	být	k5eNaImBp3nS	být
fungovat	fungovat	k5eAaImF	fungovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžete	moct	k5eAaImIp2nP	moct
je	on	k3xPp3gNnSc4	on
v	v	k7c6	v
krajním	krajní	k2eAgInSc6d1	krajní
případě	případ	k1gInSc6	případ
zařízení	zařízení	k1gNnPc2	zařízení
i	i	k8xC	i
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Opačná	opačný	k2eAgFnSc1d1	opačná
záměna	záměna	k1gFnSc1	záměna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
použití	použití	k1gNnSc1	použití
vysokoimpedančních	vysokoimpedanční	k2eAgNnPc2d1	vysokoimpedanční
sluchátek	sluchátko	k1gNnPc2	sluchátko
u	u	k7c2	u
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
předepsána	předepsán	k2eAgNnPc4d1	předepsáno
sluchátka	sluchátko	k1gNnPc4	sluchátko
nízkoimpedanční	nízkoimpedanční	k2eAgFnSc2d1	nízkoimpedanční
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značky	značka	k1gFnPc4	značka
sluchátek	sluchátko	k1gNnPc2	sluchátko
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
světe	svět	k1gInSc5	svět
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
značek	značka	k1gFnPc2	značka
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
AIAIAI	AIAIAI	kA	AIAIAI
<g/>
,	,	kIx,	,
AKG	AKG	kA	AKG
<g/>
,	,	kIx,	,
Audio-Technica	Audio-Technicum	k1gNnSc2	Audio-Technicum
<g/>
,	,	kIx,	,
Beats	Beats	k1gInSc4	Beats
by	by	kYmCp3nS	by
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc2	Dre
<g/>
,	,	kIx,	,
Bloody	Blooda	k1gFnSc2	Blooda
<g/>
,	,	kIx,	,
Beyerdynamic	Beyerdynamic	k1gMnSc1	Beyerdynamic
<g/>
,	,	kIx,	,
Denon	Denon	kA	Denon
<g/>
,	,	kIx,	,
Elecom	Elecom	k1gInSc1	Elecom
<g/>
,	,	kIx,	,
Foxtex	Foxtex	k1gInSc1	Foxtex
<g/>
,	,	kIx,	,
Grado	Grado	k1gNnSc1	Grado
<g/>
,	,	kIx,	,
Jays	Jays	k1gInSc1	Jays
<g/>
,	,	kIx,	,
Klipsch	Klipsch	k1gInSc1	Klipsch
<g/>
,	,	kIx,	,
Koss	Koss	k1gInSc1	Koss
<g/>
,	,	kIx,	,
Marshall	Marshall	k1gInSc1	Marshall
<g/>
,	,	kIx,	,
Phonak	Phonak	k1gInSc1	Phonak
<g/>
,	,	kIx,	,
Plantronics	Plantronics	k1gInSc1	Plantronics
<g/>
,	,	kIx,	,
Razer	Razer	k1gInSc1	Razer
<g/>
,	,	kIx,	,
Sennheiser	Sennheiser	kA	Sennheiser
<g/>
,	,	kIx,	,
Shure	Shur	k1gMnSc5	Shur
<g/>
,	,	kIx,	,
Skullcandy	Skullcand	k1gInPc1	Skullcand
<g/>
,	,	kIx,	,
Souhl	Souhl	k1gFnPc1	Souhl
<g/>
,	,	kIx,	,
SoundMagis	SoundMagis	k1gFnPc1	SoundMagis
<g/>
,	,	kIx,	,
SteelSeries	SteelSeries	k1gInSc1	SteelSeries
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
Ears	Ears	k1gInSc1	Ears
<g/>
,	,	kIx,	,
Ultrasone	Ultrason	k1gMnSc5	Ultrason
<g/>
,	,	kIx,	,
Urbanears	Urbanearsa	k1gFnPc2	Urbanearsa
<g/>
,	,	kIx,	,
Vsonic	Vsonice	k1gFnPc2	Vsonice
<g/>
,	,	kIx,	,
WeSC	WeSC	k1gFnPc2	WeSC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysokoimpedanční	Vysokoimpedanční	k2eAgNnPc4d1	Vysokoimpedanční
sluchátka	sluchátko	k1gNnPc4	sluchátko
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tytéž	týž	k3xTgFnPc1	týž
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
i	i	k8xC	i
výrobou	výroba	k1gFnSc7	výroba
radiopřijímačů	radiopřijímač	k1gInPc2	radiopřijímač
<g/>
,	,	kIx,	,
např.	např.	kA	např.
firmy	firma	k1gFnPc1	firma
Telefunken	Telefunken	k1gInSc1	Telefunken
<g/>
,	,	kIx,	,
Telegraphia	Telegraphia	k1gFnSc1	Telegraphia
<g/>
,	,	kIx,	,
Philips	Philips	kA	Philips
aj.	aj.	kA	aj.
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jejich	jejich	k3xOp3gMnSc1	jejich
dodavatel	dodavatel	k1gMnSc1	dodavatel
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podání	podání	k1gNnSc2	podání
prostoru	prostor	k1gInSc2	prostor
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
poslechu	poslech	k1gInSc6	poslech
stereofonní	stereofonní	k2eAgFnSc2d1	stereofonní
nahrávky	nahrávka	k1gFnSc2	nahrávka
je	být	k5eAaImIp3nS	být
prostorový	prostorový	k2eAgInSc1d1	prostorový
vjem	vjem	k1gInSc1	vjem
ve	v	k7c6	v
sluchátkách	sluchátko	k1gNnPc6	sluchátko
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
při	při	k7c6	při
poslechu	poslech	k1gInSc6	poslech
přes	přes	k7c4	přes
reproduktorovou	reproduktorový	k2eAgFnSc4d1	reproduktorová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
daleko	daleko	k6eAd1	daleko
a	a	k8xC	a
jakým	jaký	k3yRgInSc7	jaký
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
zařízení	zařízení	k1gNnSc2	zařízení
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poslech	poslech	k1gInSc4	poslech
přes	přes	k7c4	přes
sluchátka	sluchátko	k1gNnPc4	sluchátko
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
binaurální	binaurální	k2eAgFnPc1d1	binaurální
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nové	Nové	k2eAgFnPc1d1	Nové
technologie	technologie	k1gFnPc1	technologie
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sluchátek	sluchátko	k1gNnPc2	sluchátko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
výrobců	výrobce	k1gMnPc2	výrobce
se	se	k3xPyFc4	se
chlubí	chlubit	k5eAaImIp3nS	chlubit
použitým	použitý	k2eAgInSc7d1	použitý
materiálem	materiál	k1gInSc7	materiál
na	na	k7c4	na
kabely	kabel	k1gInPc4	kabel
<g/>
,	,	kIx,	,
konektory	konektor	k1gInPc4	konektor
nebo	nebo	k8xC	nebo
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
technické	technický	k2eAgInPc4d1	technický
parametry	parametr	k1gInPc4	parametr
nebo	nebo	k8xC	nebo
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
nedostatky	nedostatek	k1gInPc1	nedostatek
dané	daný	k2eAgInPc1d1	daný
technologií	technologie	k1gFnSc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
oborech	obor	k1gInPc6	obor
poslední	poslední	k2eAgInSc4d1	poslední
trend	trend	k1gInSc4	trend
využití	využití	k1gNnSc2	využití
nanotechnologií	nanotechnologie	k1gFnPc2	nanotechnologie
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
Plantronics	Plantronicsa	k1gFnPc2	Plantronicsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
u	u	k7c2	u
vybraných	vybraný	k2eAgInPc2d1	vybraný
modelů	model	k1gInPc2	model
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
nanovrstvu	nanovrstvo	k1gNnSc3	nanovrstvo
jako	jako	k9	jako
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
vlhkostí	vlhkost	k1gFnSc7	vlhkost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
deštěm	dešť	k1gInSc7	dešť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sluchátka	sluchátko	k1gNnSc2	sluchátko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sluchátko	sluchátko	k1gNnSc4	sluchátko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
sluchátka	sluchátko	k1gNnSc2	sluchátko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
<g/>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Sluchátkový	sluchátkový	k2eAgInSc1d1	sluchátkový
svět	svět	k1gInSc1	svět
pod	pod	k7c7	pod
drobnohledem	drobnohled	k1gInSc7	drobnohled
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Svět	svět	k1gInSc1	svět
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
:	:	kIx,	:
konstrukce	konstrukce	k1gFnPc1	konstrukce
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Svět	svět	k1gInSc1	svět
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
:	:	kIx,	:
drát	drát	k1gInSc1	drát
či	či	k8xC	či
bezdrát	bezdrát	k1gInSc1	bezdrát
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Svět	svět	k1gInSc4	svět
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
:	:	kIx,	:
vybíráme	vybírat	k5eAaImIp1nP	vybírat
si	se	k3xPyFc3	se
ta	ten	k3xDgNnPc4	ten
pravá	pravá	k1gFnSc5	pravá
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Svět	svět	k1gInSc1	svět
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
:	:	kIx,	:
stísněný	stísněný	k2eAgInSc1d1	stísněný
prostor	prostor	k1gInSc1	prostor
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Sluchátka	sluchátko	k1gNnPc1	sluchátko
<g/>
:	:	kIx,	:
zásady	zásada	k1gFnSc2	zásada
správného	správný	k2eAgInSc2d1	správný
poslechu	poslech	k1gInSc2	poslech
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Jak	jak	k6eAd1	jak
správně	správně	k6eAd1	správně
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
sluchátka	sluchátko	k1gNnPc4	sluchátko
na	na	k7c4	na
AVmania	AVmanium	k1gNnPc4	AVmanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
sluchátek	sluchátko	k1gNnPc2	sluchátko
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ochranná	ochranný	k2eAgNnPc4d1	ochranné
nanovrstva	nanovrstvo	k1gNnPc4	nanovrstvo
Plantronics	Plantronicsa	k1gFnPc2	Plantronicsa
sluchátek	sluchátko	k1gNnPc2	sluchátko
</s>
</p>
