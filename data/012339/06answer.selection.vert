<s>
Sluchátka	sluchátko	k1gNnPc1	sluchátko
jsou	být	k5eAaImIp3nP	být
párem	pár	k1gInSc7	pár
speciálních	speciální	k2eAgInPc2d1	speciální
malých	malý	k2eAgInPc2d1	malý
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
nebo	nebo	k8xC	nebo
do	do	k7c2	do
uší	ucho	k1gNnPc2	ucho
posluchače	posluchač	k1gMnSc2	posluchač
<g/>
.	.	kIx.	.
</s>
