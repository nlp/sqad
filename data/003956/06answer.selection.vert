<s>
Křeček	křeček	k1gMnSc1	křeček
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
hlodavce	hlodavec	k1gMnPc4	hlodavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
myšovití	myšovitý	k2eAgMnPc1d1	myšovitý
(	(	kIx(	(
<g/>
Muridae	Muridae	k1gNnSc7	Muridae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
křečkovití	křečkovitý	k2eAgMnPc1d1	křečkovitý
<g/>
.	.	kIx.	.
</s>
