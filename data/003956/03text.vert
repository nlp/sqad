<s>
Křeček	křeček	k1gMnSc1	křeček
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
hlodavce	hlodavec	k1gMnPc4	hlodavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
myšovití	myšovitý	k2eAgMnPc1d1	myšovitý
(	(	kIx(	(
<g/>
Muridae	Muridae	k1gNnSc7	Muridae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
křečkovití	křečkovitý	k2eAgMnPc1d1	křečkovitý
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
názvosloví	názvosloví	k1gNnSc1	názvosloví
je	být	k5eAaImIp3nS	být
však	však	k9	však
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
současného	současný	k2eAgInSc2d1	současný
názoru	názor	k1gInSc2	názor
na	na	k7c4	na
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
příbuznost	příbuznost	k1gFnSc4	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
křeček	křeček	k1gMnSc1	křeček
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označuje	označovat	k5eAaImIp3nS	označovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
zejména	zejména	k9	zejména
křeček	křeček	k1gMnSc1	křeček
polní	polní	k2eAgMnSc1d1	polní
(	(	kIx(	(
<g/>
Cricetus	Cricetus	k1gMnSc1	Cricetus
cricetus	cricetus	k1gMnSc1	cricetus
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Cricetus	Cricetus	k1gInSc1	Cricetus
<g/>
.	.	kIx.	.
</s>
<s>
Křeček	křeček	k1gMnSc1	křeček
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
svými	svůj	k3xOyFgInPc7	svůj
zásobními	zásobní	k2eAgInPc7d1	zásobní
vaky	vak	k1gInPc7	vak
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
pusy	pusa	k1gFnSc2	pusa
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
naplnit	naplnit	k5eAaPmF	naplnit
od	od	k7c2	od
tlamičky	tlamička	k1gFnSc2	tlamička
až	až	k9	až
po	po	k7c4	po
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
zrak	zrak	k1gInSc1	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
sluch	sluch	k1gInSc4	sluch
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
křečků	křeček	k1gMnPc2	křeček
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
pořadí	pořadí	k1gNnSc1	pořadí
obrácené	obrácený	k2eAgNnSc1d1	obrácené
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
my	my	k3xPp1nPc1	my
poznáme	poznat	k5eAaPmIp1nP	poznat
člověka	člověk	k1gMnSc4	člověk
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
ukládá	ukládat	k5eAaImIp3nS	ukládat
si	se	k3xPyFc3	se
křeček	křeček	k1gMnSc1	křeček
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
vzory	vzor	k1gInPc4	vzor
pachů	pach	k1gInPc2	pach
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
pak	pak	k6eAd1	pak
i	i	k9	i
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
rozezná	rozeznat	k5eAaPmIp3nS	rozeznat
příslušníky	příslušník	k1gMnPc4	příslušník
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
nepřátele	nepřítel	k1gMnPc4	nepřítel
i	i	k8xC	i
své	svůj	k3xOyFgMnPc4	svůj
chovatele	chovatel	k1gMnSc4	chovatel
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
nebo	nebo	k8xC	nebo
příbuzenstvo	příbuzenstvo	k1gNnSc4	příbuzenstvo
se	se	k3xPyFc4	se
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
podle	podle	k7c2	podle
skupinového	skupinový	k2eAgInSc2d1	skupinový
pachu	pach	k1gInSc2	pach
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
i	i	k9	i
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gFnPc4	jejich
přirozené	přirozený	k2eAgFnPc4d1	přirozená
agresivní	agresivní	k2eAgFnPc4d1	agresivní
sklony	sklona	k1gFnPc4	sklona
zajistí	zajistit	k5eAaPmIp3nS	zajistit
mírné	mírný	k2eAgNnSc1d1	mírné
chování	chování	k1gNnSc1	chování
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
je	být	k5eAaImIp3nS	být
křeček	křeček	k1gMnSc1	křeček
schopen	schopen	k2eAgMnSc1d1	schopen
ucítit	ucítit	k5eAaPmF	ucítit
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pachovým	pachový	k2eAgFnPc3d1	pachová
značkám	značka	k1gFnPc3	značka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
třením	tření	k1gNnSc7	tření
svých	svůj	k3xOyFgFnPc2	svůj
mazových	mazový	k2eAgFnPc2d1	mazová
žláz	žláza	k1gFnPc2	žláza
o	o	k7c4	o
důležité	důležitý	k2eAgInPc4d1	důležitý
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
výkalům	výkal	k1gInPc3	výkal
a	a	k8xC	a
moči	moč	k1gFnSc6	moč
je	být	k5eAaImIp3nS	být
křeček	křeček	k1gMnSc1	křeček
schopen	schopen	k2eAgMnSc1d1	schopen
orientovat	orientovat	k5eAaBmF	orientovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
i	i	k9	i
potmě	potmě	k6eAd1	potmě
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
tak	tak	k6eAd1	tak
označuje	označovat	k5eAaImIp3nS	označovat
své	své	k1gNnSc4	své
území	území	k1gNnSc2	území
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
možným	možný	k2eAgMnPc3d1	možný
vetřelcům	vetřelec	k1gMnPc3	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
akustického	akustický	k2eAgNnSc2d1	akustické
vnímání	vnímání	k1gNnSc2	vnímání
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
křečků	křeček	k1gMnPc2	křeček
velice	velice	k6eAd1	velice
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
také	také	k9	také
frekvence	frekvence	k1gFnPc4	frekvence
ze	z	k7c2	z
spektra	spektrum	k1gNnSc2	spektrum
ultrazvuku	ultrazvuk	k1gInSc2	ultrazvuk
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
jim	on	k3xPp3gMnPc3	on
neslouží	sloužit	k5eNaImIp3nP	sloužit
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
k	k	k7c3	k
vnitrodruhovému	vnitrodruhový	k2eAgNnSc3d1	vnitrodruhové
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
všechna	všechen	k3xTgNnPc4	všechen
mláďata	mládě	k1gNnPc4	mládě
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
neslyšitelnými	slyšitelný	k2eNgFnPc7d1	neslyšitelná
zvuky	zvuk	k1gInPc4	zvuk
přivolat	přivolat	k5eAaPmF	přivolat
svoji	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
i	i	k8xC	i
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
rodného	rodný	k2eAgNnSc2d1	rodné
hnízda	hnízdo	k1gNnSc2	hnízdo
příliš	příliš	k6eAd1	příliš
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
hlad	hlad	k1gInSc4	hlad
či	či	k8xC	či
žízeň	žízeň	k1gFnSc4	žízeň
<g/>
.	.	kIx.	.
</s>
<s>
Křečci	křeček	k1gMnPc1	křeček
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozeznat	rozeznat	k5eAaPmF	rozeznat
různé	různý	k2eAgInPc4d1	různý
tóny	tón	k1gInPc4	tón
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
či	či	k8xC	či
po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
poznat	poznat	k5eAaPmF	poznat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gFnPc4	on
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dne	den	k1gInSc2	den
křeček	křeček	k1gMnSc1	křeček
složí	složit	k5eAaPmIp3nS	složit
své	svůj	k3xOyFgInPc4	svůj
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
nerušeně	nerušeně	k6eAd1	nerušeně
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
noční	noční	k2eAgMnSc1d1	noční
tvor	tvor	k1gMnSc1	tvor
nedisponuje	disponovat	k5eNaBmIp3nS	disponovat
křeček	křeček	k1gMnSc1	křeček
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
dobrým	dobrý	k2eAgInSc7d1	dobrý
zrakem	zrak	k1gInSc7	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
pouze	pouze	k6eAd1	pouze
nedostatečně	dostatečně	k6eNd1	dostatečně
rozvinuto	rozvinut	k2eAgNnSc1d1	rozvinuto
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
křeček	křeček	k1gMnSc1	křeček
se	s	k7c7	s
svýma	svůj	k3xOyFgFnPc7	svůj
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
posazenýma	posazený	k2eAgFnPc7d1	posazená
kulovitýma	kulovitý	k2eAgFnPc7d1	kulovitá
očima	oko	k1gNnPc7	oko
schopen	schopen	k2eAgMnSc1d1	schopen
jedním	jeden	k4xCgInSc7	jeden
pohledem	pohled	k1gInSc7	pohled
zachytit	zachytit	k5eAaPmF	zachytit
celé	celý	k2eAgNnSc4d1	celé
své	své	k1gNnSc4	své
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
křeček	křeček	k1gMnSc1	křeček
má	mít	k5eAaImIp3nS	mít
opravdu	opravdu	k6eAd1	opravdu
široký	široký	k2eAgInSc4d1	široký
rozhled	rozhled	k1gInSc4	rozhled
-	-	kIx~	-
asi	asi	k9	asi
110	[number]	k4	110
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naše	náš	k3xOp1gInPc4	náš
poměry	poměr	k1gInPc4	poměr
je	být	k5eAaImIp3nS	být
křeček	křeček	k1gMnSc1	křeček
ovšem	ovšem	k9	ovšem
poněkud	poněkud	k6eAd1	poněkud
krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
<g/>
,	,	kIx,	,
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
1	[number]	k4	1
metr	metr	k1gInSc1	metr
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
stěží	stěží	k6eAd1	stěží
schopen	schopen	k2eAgMnSc1d1	schopen
rozeznat	rozeznat	k5eAaPmF	rozeznat
kontury	kontura	k1gFnPc4	kontura
<g/>
.	.	kIx.	.
</s>
<s>
Pohyby	pohyb	k1gInPc1	pohyb
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
blížícího	blížící	k2eAgInSc2d1	blížící
se	se	k3xPyFc4	se
nepřítele	nepřítel	k1gMnSc4	nepřítel
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
rozeznat	rozeznat	k5eAaPmF	rozeznat
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
denním	denní	k2eAgNnSc6d1	denní
světle	světlo	k1gNnSc6	světlo
je	být	k5eAaImIp3nS	být
křeček	křeček	k1gMnSc1	křeček
téměř	téměř	k6eAd1	téměř
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jeho	jeho	k3xOp3gFnSc4	jeho
často	často	k6eAd1	často
pozorovatelnou	pozorovatelný	k2eAgFnSc4d1	pozorovatelná
lekavost	lekavost	k1gFnSc4	lekavost
<g/>
,	,	kIx,	,
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
v	v	k7c6	v
neznámém	známý	k2eNgNnSc6d1	neznámé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
hmatové	hmatový	k2eAgFnPc1d1	hmatová
vousky	vouska	k1gFnPc1	vouska
slouží	sloužit	k5eAaImIp3nP	sloužit
rovněž	rovněž	k9	rovněž
jemné	jemný	k2eAgInPc4d1	jemný
chloupky	chloupek	k1gInPc4	chloupek
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
a	a	k8xC	a
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
i	i	k8xC	i
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
k	k	k7c3	k
orientaci	orientace	k1gFnSc3	orientace
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
těchto	tento	k3xDgInPc2	tento
speciálních	speciální	k2eAgInPc2d1	speciální
chloupků	chloupek	k1gInPc2	chloupek
a	a	k8xC	a
vousků	vousek	k1gInPc2	vousek
je	být	k5eAaImIp3nS	být
křeček	křeček	k1gMnSc1	křeček
schopen	schopen	k2eAgMnSc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
překážky	překážka	k1gFnPc4	překážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
postaví	postavit	k5eAaPmIp3nS	postavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
lépe	dobře	k6eAd2	dobře
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
otvor	otvor	k1gInSc4	otvor
či	či	k8xC	či
průlez	průlez	k1gInSc4	průlez
dostatečně	dostatečně	k6eAd1	dostatečně
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
systémy	systém	k1gInPc1	systém
uznávají	uznávat	k5eAaImIp3nP	uznávat
čeleď	čeleď	k1gFnSc4	čeleď
křečkovití	křečkovitý	k2eAgMnPc1d1	křečkovitý
(	(	kIx(	(
<g/>
Cricetidae	Cricetidae	k1gNnSc7	Cricetidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc4d1	jiné
ji	on	k3xPp3gFnSc4	on
opouští	opouštět	k5eAaImIp3nP	opouštět
a	a	k8xC	a
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
všechny	všechen	k3xTgMnPc4	všechen
její	její	k3xOp3gMnPc4	její
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
myšovití	myšovitý	k2eAgMnPc1d1	myšovitý
(	(	kIx(	(
<g/>
Muridae	Muridae	k1gNnSc7	Muridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
křečkovití	křečkovitý	k2eAgMnPc1d1	křečkovitý
(	(	kIx(	(
<g/>
Cricetidae	Cricetidae	k1gNnSc7	Cricetidae
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
nejen	nejen	k6eAd1	nejen
"	"	kIx"	"
<g/>
křečci	křeček	k1gMnPc1	křeček
<g/>
"	"	kIx"	"
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
například	například	k6eAd1	například
podčeleď	podčeleď	k1gFnSc4	podčeleď
hrabošovití	hrabošovití	k1gMnPc1	hrabošovití
(	(	kIx(	(
<g/>
Arvicolinae	Arvicolinae	k1gInSc1	Arvicolinae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typičtí	typický	k2eAgMnPc1d1	typický
křečci	křeček	k1gMnPc1	křeček
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
podčeledí	podčeleď	k1gFnPc2	podčeleď
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
křečci	křeček	k1gMnPc1	křeček
praví	pravý	k2eAgMnPc1d1	pravý
(	(	kIx(	(
<g/>
Cricetinae	Cricetinae	k1gNnSc7	Cricetinae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tolweb	Tolwba	k1gFnPc2	Tolwba
(	(	kIx(	(
<g/>
Tree	Tree	k1gInSc1	Tree
of	of	k?	of
life	life	k1gInSc1	life
<g/>
)	)	kIx)	)
uznává	uznávat	k5eAaImIp3nS	uznávat
v	v	k7c6	v
čeledi	čeleď	k1gFnSc6	čeleď
křečkovití	křečkovitý	k2eAgMnPc1d1	křečkovitý
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
Arvicolinae	Arvicolinae	k1gInSc1	Arvicolinae
–	–	k?	–
hrabošovití	hrabošovití	k1gMnPc1	hrabošovití
Cricetinae	Cricetina	k1gFnSc2	Cricetina
<g/>
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
praví	pravit	k5eAaBmIp3nP	pravit
Neotominae	Neotominae	k1gFnSc4	Neotominae
Sigmodontinae	Sigmodontinae	k1gFnPc2	Sigmodontinae
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
američtí	americký	k2eAgMnPc1d1	americký
(	(	kIx(	(
<g/>
sensu	sens	k1gInSc6	sens
stricto	strict	k2eAgNnSc1d1	stricto
<g/>
)	)	kIx)	)
Tylomyinae	Tylomyinae	k1gNnSc1	Tylomyinae
Biolib	Bioliba	k1gFnPc2	Bioliba
naopak	naopak	k6eAd1	naopak
křečkovité	křečkovitý	k2eAgNnSc1d1	křečkovitý
neuznává	uznávat	k5eNaImIp3nS	uznávat
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
skupinách	skupina	k1gFnPc6	skupina
myšovitých	myšovitý	k2eAgMnPc2d1	myšovitý
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
:	:	kIx,	:
Calomyscinae	Calomyscinae	k1gFnPc2	Calomyscinae
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
myší	myší	k2eAgFnSc2d1	myší
Cricetinae	Cricetina	k1gFnSc2	Cricetina
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
praví	pravit	k5eAaImIp3nP	pravit
Mystromyinae	Mystromyinae	k1gFnPc2	Mystromyinae
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
nepraví	pravit	k5eNaBmIp3nP	pravit
Nesomyinae	Nesomyinae	k1gFnPc2	Nesomyinae
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
madagaskarští	madagaskarský	k2eAgMnPc1d1	madagaskarský
Sigmodontinae	Sigmodontinae	k1gFnPc2	Sigmodontinae
–	–	k?	–
křečci	křeček	k1gMnPc1	křeček
američtí	americký	k2eAgMnPc1d1	americký
křeček	křeček	k1gMnSc1	křeček
zlatý	zlatý	k1gInSc1	zlatý
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
k.	k.	k?	k.
syrský	syrský	k2eAgInSc1d1	syrský
<g/>
,	,	kIx,	,
k.	k.	k?	k.
medvědí	medvědí	k2eAgFnPc1d1	medvědí
(	(	kIx(	(
<g/>
Mesocricetus	Mesocricetus	k1gMnSc1	Mesocricetus
auratus	auratus	k1gMnSc1	auratus
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Cricetus	Cricetus	k1gMnSc1	Cricetus
auratus	auratus	k1gMnSc1	auratus
<g/>
)	)	kIx)	)
křečík	křečík	k1gMnSc1	křečík
džungarský	džungarský	k2eAgMnSc1d1	džungarský
(	(	kIx(	(
<g/>
Phodopus	Phodopus	k1gMnSc1	Phodopus
sungorus	sungorus	k1gMnSc1	sungorus
<g/>
)	)	kIx)	)
křečík	křečík	k1gInSc1	křečík
Campbellův	Campbellův	k2eAgInSc1d1	Campbellův
(	(	kIx(	(
<g/>
Phodopus	Phodopus	k1gInSc1	Phodopus
campbelli	campbelle	k1gFnSc4	campbelle
<g/>
)	)	kIx)	)
křečík	křečík	k1gInSc1	křečík
Roborovského	Roborovský	k2eAgMnSc2d1	Roborovský
(	(	kIx(	(
<g/>
Phodopus	Phodopus	k1gInSc1	Phodopus
roborovskii	roborovskie	k1gFnSc4	roborovskie
<g/>
)	)	kIx)	)
Křečík	Křečík	k1gMnSc1	Křečík
čínský	čínský	k2eAgMnSc1d1	čínský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
k.	k.	k?	k.
daurský	daurský	k2eAgInSc4d1	daurský
(	(	kIx(	(
<g/>
Cricetulus	Cricetulus	k1gInSc4	Cricetulus
barabensis	barabensis	k1gFnSc2	barabensis
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
griseus	griseus	k1gInSc1	griseus
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
křeček	křeček	k1gMnSc1	křeček
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Křeček	křeček	k1gMnSc1	křeček
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
