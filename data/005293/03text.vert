<s>
Otec	otec	k1gMnSc1	otec
Goriot	Goriot	k1gMnSc1	Goriot
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Le	Le	k1gMnSc1	Le
Pè	Pè	k1gMnSc1	Pè
Goriot	Goriot	k1gMnSc1	Goriot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
realistický	realistický	k2eAgInSc4d1	realistický
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
děl	dělo	k1gNnPc2	dělo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Honoré	Honorý	k2eAgFnSc2d1	Honorý
de	de	k?	de
Balzaca	Balzac	k1gMnSc2	Balzac
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pozastavuje	pozastavovat	k5eAaImIp3nS	pozastavovat
zejména	zejména	k9	zejména
nad	nad	k7c7	nad
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
lidskými	lidský	k2eAgInPc7d1	lidský
osudy	osud	k1gInPc7	osud
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
představuje	představovat	k5eAaImIp3nS	představovat
kritiku	kritika	k1gFnSc4	kritika
úpadku	úpadek	k1gInSc2	úpadek
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
realistický	realistický	k2eAgInSc1d1	realistický
obraz	obraz	k1gInSc1	obraz
francouzské	francouzský	k2eAgFnSc2d1	francouzská
společnosti	společnost	k1gFnSc2	společnost
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
důležitější	důležitý	k2eAgFnSc1d2	důležitější
než	než	k8xS	než
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Balzac	Balzac	k1gMnSc1	Balzac
si	se	k3xPyFc3	se
potrpěl	potrpět	k5eAaPmAgMnS	potrpět
na	na	k7c4	na
zevrubné	zevrubný	k2eAgInPc4d1	zevrubný
popisy	popis	k1gInPc4	popis
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
citů	cit	k1gInPc2	cit
a	a	k8xC	a
pocitů	pocit	k1gInPc2	pocit
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
autor	autor	k1gMnSc1	autor
často	často	k6eAd1	často
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
metaforicky	metaforicky	k6eAd1	metaforicky
(	(	kIx(	(
<g/>
nos	nos	k1gInSc1	nos
jako	jako	k8xS	jako
zobák	zobák	k1gInSc1	zobák
papouška	papoušek	k1gMnSc2	papoušek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
povětšinou	povětšinou	k6eAd1	povětšinou
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
penzionátu	penzionát	k1gInSc6	penzionát
vdovy	vdova	k1gFnSc2	vdova
Vauquerové	Vauquerová	k1gFnSc2	Vauquerová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přebývá	přebývat	k5eAaImIp3nS	přebývat
řada	řada	k1gFnSc1	řada
nájemníků	nájemník	k1gMnPc2	nájemník
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
však	však	k9	však
nejvíce	nejvíce	k6eAd1	nejvíce
sleduje	sledovat	k5eAaImIp3nS	sledovat
osudy	osud	k1gInPc4	osud
dvou	dva	k4xCgInPc2	dva
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
Evžena	Evžen	k1gMnSc2	Evžen
de	de	k?	de
Rastignaca	Rastignacus	k1gMnSc2	Rastignacus
<g/>
,	,	kIx,	,
mladého	mladý	k2eAgMnSc2d1	mladý
studenta	student	k1gMnSc2	student
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
otce	otec	k1gMnSc2	otec
Goriota	Goriot	k1gMnSc2	Goriot
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
významného	významný	k2eAgMnSc2d1	významný
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
nudlemi	nudle	k1gFnPc7	nudle
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
nich	on	k3xPp3gInPc2	on
v	v	k7c6	v
penzionátu	penzionát	k1gInSc6	penzionát
přebývají	přebývat	k5eAaImIp3nP	přebývat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
osoby	osoba	k1gFnPc1	osoba
-	-	kIx~	-
slečna	slečna	k1gFnSc1	slečna
Vauquerová	Vauquerová	k1gFnSc1	Vauquerová
(	(	kIx(	(
<g/>
hamižná	hamižný	k2eAgFnSc1d1	hamižná
stará	starý	k2eAgFnSc1d1	stará
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
výdělek	výdělek	k1gInSc4	výdělek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vautrin	Vautrin	k1gInSc1	Vautrin
(	(	kIx(	(
<g/>
cynický	cynický	k2eAgMnSc1d1	cynický
uprchlý	uprchlý	k1gMnSc1	uprchlý
galejník	galejník	k1gMnSc1	galejník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
Evženovi	Evžen	k1gMnSc3	Evžen
řadu	řada	k1gFnSc4	řada
rad	rada	k1gFnPc2	rada
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poiret	Poiret	k1gMnSc1	Poiret
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
starší	starší	k1gMnSc1	starší
málo	málo	k6eAd1	málo
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slečna	slečna	k1gFnSc1	slečna
Michonneauová	Michonneauová	k1gFnSc1	Michonneauová
(	(	kIx(	(
<g/>
stará	starý	k2eAgFnSc1d1	stará
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
přičinění	přičinění	k1gNnSc2	přičinění
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
Vautrin	Vautrin	k1gInSc1	Vautrin
dopaden	dopadnout	k5eAaPmNgInS	dopadnout
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
zradě	zrada	k1gFnSc6	zrada
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
penzionátu	penzionát	k1gInSc2	penzionát
vykázána	vykázán	k2eAgFnSc1d1	vykázána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
medik	medik	k1gMnSc1	medik
Bianchon	Bianchon	k1gMnSc1	Bianchon
(	(	kIx(	(
<g/>
obětavý	obětavý	k2eAgMnSc1d1	obětavý
student	student	k1gMnSc1	student
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
Evžena	Evžen	k1gMnSc2	Evžen
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vdova	vdova	k1gFnSc1	vdova
Couturová	Couturový	k2eAgFnSc1d1	Couturový
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
schovanka	schovanka	k1gFnSc1	schovanka
Viktorína	Viktorín	k1gMnSc2	Viktorín
(	(	kIx(	(
<g/>
Viktoríně	Viktorín	k1gInSc6	Viktorín
zemřela	zemřít	k5eAaPmAgFnS	zemřít
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
ujala	ujmout	k5eAaPmAgFnS	ujmout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
zbožné	zbožný	k2eAgFnPc1d1	zbožná
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
dědictví	dědictví	k1gNnSc3	dědictví
po	po	k7c6	po
synu	syn	k1gMnSc6	syn
Viktorínina	Viktorínin	k2eAgInSc2d1	Viktorínin
otce	otec	k1gMnSc2	otec
stávají	stávat	k5eAaImIp3nP	stávat
bohatými	bohatý	k2eAgMnPc7d1	bohatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
sloužící	sloužící	k1gMnPc1	sloužící
-	-	kIx~	-
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Sylvie	Sylvie	k1gFnSc1	Sylvie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
děje	děj	k1gInSc2	děj
sledujeme	sledovat	k5eAaImIp1nP	sledovat
dva	dva	k4xCgInPc4	dva
životní	životní	k2eAgInPc4d1	životní
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
de	de	k?	de
Rastignac	Rastignac	k1gInSc1	Rastignac
plný	plný	k2eAgInSc1d1	plný
ideálů	ideál	k1gInPc2	ideál
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
pohoršen	pohoršit	k5eAaPmNgInS	pohoršit
cynismem	cynismus	k1gInSc7	cynismus
a	a	k8xC	a
zločinností	zločinnost	k1gFnSc7	zločinnost
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
svých	svůj	k3xOyFgInPc2	svůj
ideálů	ideál	k1gInPc2	ideál
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
a	a	k8xC	a
ctižádostivě	ctižádostivě	k6eAd1	ctižádostivě
míří	mířit	k5eAaImIp3nS	mířit
za	za	k7c7	za
uznáním	uznání	k1gNnSc7	uznání
<g/>
,	,	kIx,	,
bohatstvím	bohatství	k1gNnSc7	bohatství
a	a	k8xC	a
vlivným	vlivný	k2eAgNnSc7d1	vlivné
společenským	společenský	k2eAgNnSc7d1	společenské
postavením	postavení	k1gNnSc7	postavení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c4	o
celoživotní	celoživotní	k2eAgFnPc4d1	celoživotní
úspory	úspora	k1gFnPc4	úspora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
mu	on	k3xPp3gNnSc3	on
mnohokrát	mnohokrát	k6eAd1	mnohokrát
pomůže	pomoct	k5eAaPmIp3nS	pomoct
svými	svůj	k3xOyFgFnPc7	svůj
radami	rada	k1gFnPc7	rada
i	i	k8xC	i
Vautrin	Vautrin	k1gInSc1	Vautrin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
příbuzná	příbuzná	k1gFnSc1	příbuzná
<g/>
,	,	kIx,	,
komtesa	komtesa	k1gFnSc1	komtesa
de	de	k?	de
Beauséant	Beauséant	k1gInSc1	Beauséant
<g/>
.	.	kIx.	.
</s>
<s>
Dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
smetánku	smetánka	k1gFnSc4	smetánka
-	-	kIx~	-
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
plesy	ples	k1gInPc1	ples
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnSc1	setkání
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
navazuje	navazovat	k5eAaImIp3nS	navazovat
vlivné	vlivný	k2eAgInPc4d1	vlivný
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zištných	zištný	k2eAgInPc2d1	zištný
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
milencem	milenec	k1gMnSc7	milenec
hraběnky	hraběnka	k1gFnSc2	hraběnka
de	de	k?	de
Nucingen	Nucingen	k1gInSc1	Nucingen
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Goriotových	Goriotův	k2eAgFnPc2d1	Goriotův
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
z	z	k7c2	z
penzionu	penzion	k1gInSc2	penzion
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Goriot	Goriot	k1gMnSc1	Goriot
představuje	představovat	k5eAaImIp3nS	představovat
jeho	on	k3xPp3gInSc4	on
opak	opak	k1gInSc4	opak
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
společenský	společenský	k2eAgInSc1d1	společenský
pád	pád	k1gInSc1	pád
a	a	k8xC	a
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
prosperující	prosperující	k2eAgMnSc1d1	prosperující
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
bídě	bída	k1gFnSc6	bída
<g/>
.	.	kIx.	.
</s>
<s>
Okolím	okolí	k1gNnSc7	okolí
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hlupáka	hlupák	k1gMnSc4	hlupák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
jedinou	jediný	k2eAgFnSc7d1	jediná
životní	životní	k2eAgFnSc7d1	životní
vášní	vášeň	k1gFnSc7	vášeň
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc4	jeho
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
splnil	splnit	k5eAaPmAgInS	splnit
každý	každý	k3xTgInSc1	každý
rozmar	rozmar	k1gInSc1	rozmar
(	(	kIx(	(
<g/>
kupoval	kupovat	k5eAaImAgInS	kupovat
jim	on	k3xPp3gMnPc3	on
šperky	šperk	k1gInPc7	šperk
<g/>
,	,	kIx,	,
od	od	k7c2	od
mlada	mlado	k1gNnSc2	mlado
jezdily	jezdit	k5eAaImAgFnP	jezdit
v	v	k7c6	v
kočáře	kočár	k1gInSc6	kočár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anastázii	Anastázie	k1gFnSc4	Anastázie
(	(	kIx(	(
<g/>
de	de	k?	de
Restaud	Restaud	k1gInSc1	Restaud
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
zachránit	zachránit	k5eAaPmF	zachránit
svého	svůj	k3xOyFgMnSc4	svůj
milence	milenec	k1gMnSc4	milenec
de	de	k?	de
Treilles	Treilles	k1gMnSc1	Treilles
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodá	prodat	k5eAaPmIp3nS	prodat
manželovy	manželův	k2eAgInPc4d1	manželův
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
díla	dílo	k1gNnSc2	dílo
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
přichází	přicházet	k5eAaImIp3nS	přicházet
teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
<g />
.	.	kIx.	.
</s>
<s>
ztratí	ztratit	k5eAaPmIp3nS	ztratit
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Delfínu	Delfín	k1gMnSc3	Delfín
(	(	kIx(	(
<g/>
de	de	k?	de
Nucingen	Nucingen	k1gInSc1	Nucingen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
Rastignacem	Rastignaec	k1gInSc7	Rastignaec
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
stýkat	stýkat	k5eAaImF	stýkat
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Goriot	Goriot	k1gMnSc1	Goriot
je	být	k5eAaImIp3nS	být
obě	dva	k4xCgFnPc4	dva
provdal	provdat	k5eAaPmAgMnS	provdat
s	s	k7c7	s
obrovským	obrovský	k2eAgNnSc7d1	obrovské
věnem	věno	k1gNnSc7	věno
za	za	k7c2	za
vlivného	vlivný	k2eAgMnSc2d1	vlivný
hraběte	hrabě	k1gMnSc2	hrabě
de	de	k?	de
Restauda	Restaud	k1gMnSc2	Restaud
a	a	k8xC	a
neméně	málo	k6eNd2	málo
vlivného	vlivný	k2eAgMnSc2d1	vlivný
bankéře	bankéř	k1gMnSc2	bankéř
de	de	k?	de
Nucingena	Nucingen	k1gMnSc2	Nucingen
<g/>
.	.	kIx.	.
</s>
<s>
Zeťové	zeť	k1gMnPc1	zeť
však	však	k9	však
nemají	mít	k5eNaImIp3nP	mít
Goriota	Goriot	k1gMnSc4	Goriot
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jej	on	k3xPp3gMnSc4	on
brzy	brzy	k6eAd1	brzy
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
rodin	rodina	k1gFnPc2	rodina
vyštvou	vyštvat	k5eAaPmIp3nP	vyštvat
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgMnSc1d1	starý
pán	pán	k1gMnSc1	pán
se	se	k3xPyFc4	se
tak	tak	k9	tak
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dcerami	dcera	k1gFnPc7	dcera
musí	muset	k5eAaImIp3nS	muset
stýkat	stýkat	k5eAaImF	stýkat
pouze	pouze	k6eAd1	pouze
osamotě	osamota	k1gFnSc3	osamota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
jim	on	k3xPp3gMnPc3	on
neodmítne	odmítnout	k5eNaPmIp3nS	odmítnout
poskytnout	poskytnout	k5eAaPmF	poskytnout
další	další	k2eAgFnSc4d1	další
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
dcery	dcera	k1gFnPc1	dcera
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pěkného	pěkný	k2eAgInSc2d1	pěkný
bytu	byt	k1gInSc2	byt
do	do	k7c2	do
malé	malý	k2eAgFnSc2d1	malá
<g/>
,	,	kIx,	,
zatuchlé	zatuchlý	k2eAgFnSc2d1	zatuchlá
podkrovní	podkrovní	k2eAgFnSc2d1	podkrovní
světničky	světnička	k1gFnSc2	světnička
v	v	k7c6	v
penzionátu	penzionát	k1gInSc6	penzionát
Vauqurové	Vauqurový	k2eAgNnSc1d1	Vauqurový
<g/>
.	.	kIx.	.
</s>
<s>
Goriot	Goriot	k1gInSc1	Goriot
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
jak	jak	k8xS	jak
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
prodává	prodávat	k5eAaImIp3nS	prodávat
poslední	poslední	k2eAgInPc4d1	poslední
zbytky	zbytek	k1gInPc4	zbytek
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
utápí	utápět	k5eAaImIp3nP	utápět
v	v	k7c6	v
citových	citový	k2eAgInPc6d1	citový
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc4	štěstí
mu	on	k3xPp3gMnSc3	on
stačí	stačit	k5eAaBmIp3nS	stačit
vidět	vidět	k5eAaImF	vidět
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dcer	dcera	k1gFnPc2	dcera
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
dotknout	dotknout	k5eAaPmF	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
citové	citový	k2eAgNnSc1d1	citové
přepětí	přepětí	k1gNnSc1	přepětí
však	však	k9	však
starý	starý	k2eAgMnSc1d1	starý
pán	pán	k1gMnSc1	pán
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vydržet	vydržet	k5eAaPmF	vydržet
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
obětí	oběť	k1gFnSc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
pečují	pečovat	k5eAaImIp3nP	pečovat
Rastignac	Rastignac	k1gFnSc4	Rastignac
a	a	k8xC	a
Bianchon	Bianchon	k1gNnSc4	Bianchon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
zoufale	zoufale	k6eAd1	zoufale
bojují	bojovat	k5eAaImIp3nP	bojovat
do	do	k7c2	do
úplného	úplný	k2eAgInSc2d1	úplný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Goriot	Goriot	k1gInSc1	Goriot
touží	toužit	k5eAaImIp3nS	toužit
naposledy	naposledy	k6eAd1	naposledy
vidět	vidět	k5eAaImF	vidět
své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
přes	přes	k7c4	přes
Rastignacovo	Rastignacův	k2eAgNnSc4d1	Rastignacův
naléhání	naléhání	k1gNnSc4	naléhání
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnPc1	jejich
manželé	manžel	k1gMnPc1	manžel
nechtějí	chtít	k5eNaImIp3nP	chtít
pustit	pustit	k5eAaPmF	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Anastázie	Anastázie	k1gFnSc1	Anastázie
nakonec	nakonec	k6eAd1	nakonec
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
přichází	přicházet	k5eAaImIp3nS	přicházet
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
Delfína	Delfín	k1gMnSc4	Delfín
po	po	k7c6	po
hádce	hádka	k1gFnSc6	hádka
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
omdlévá	omdlévat	k5eAaImIp3nS	omdlévat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
minutách	minuta	k1gFnPc6	minuta
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
prožívá	prožívat	k5eAaImIp3nS	prožívat
muka	muka	k1gFnSc1	muka
<g/>
,	,	kIx,	,
proklíná	proklínat	k5eAaImIp3nS	proklínat
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
dal	dát	k5eAaPmAgInS	dát
vše	všechen	k3xTgNnSc1	všechen
a	a	k8xC	a
ony	onen	k3xDgFnPc1	onen
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
poslední	poslední	k2eAgFnSc6d1	poslední
hodině	hodina	k1gFnSc6	hodina
ani	ani	k8xC	ani
nenavštíví	navštívit	k5eNaPmIp3nS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Umírá	umírat	k5eAaImIp3nS	umírat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
nezískává	získávat	k5eNaImIp3nS	získávat
Rastignac	Rastignac	k1gFnSc1	Rastignac
peníze	peníz	k1gInSc2	peníz
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
zeťů	zeť	k1gMnPc2	zeť
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
spravují	spravovat	k5eAaImIp3nP	spravovat
majetky	majetek	k1gInPc4	majetek
svých	svůj	k3xOyFgFnPc2	svůj
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
s	s	k7c7	s
Bianchonem	Bianchon	k1gInSc7	Bianchon
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
Goriotovi	Goriot	k1gMnSc3	Goriot
levný	levný	k2eAgInSc4d1	levný
obřad	obřad	k1gInSc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
starého	starý	k2eAgMnSc2d1	starý
pána	pán	k1gMnSc2	pán
nevyprovodí	vyprovodit	k5eNaPmIp3nS	vyprovodit
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
