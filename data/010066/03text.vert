<p>
<s>
Bellinghamský	Bellinghamský	k2eAgInSc1d1	Bellinghamský
festival	festival	k1gInSc1	festival
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc1	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každé	každý	k3xTgNnSc4	každý
léto	léto	k1gNnSc4	léto
v	v	k7c6	v
Bellinghamu	Bellingham	k1gInSc6	Bellingham
<g/>
,	,	kIx,	,
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
dirigent	dirigent	k1gMnSc1	dirigent
Michael	Michael	k1gMnSc1	Michael
Palmer	Palmer	k1gMnSc1	Palmer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
koncerty	koncert	k1gInPc1	koncert
orchestrů	orchestr	k1gInPc2	orchestr
a	a	k8xC	a
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
sálech	sál	k1gInPc6	sál
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nP	patřit
Western	Western	kA	Western
Washington	Washington	k1gInSc4	Washington
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Mount	Mount	k1gMnSc1	Mount
Baker	Baker	k1gMnSc1	Baker
Theatre	Theatr	k1gInSc5	Theatr
<g/>
,	,	kIx,	,
McIntyrova	McIntyrův	k2eAgFnSc1d1	McIntyrův
hala	hala	k1gFnSc1	hala
v	v	k7c4	v
Mount	Mount	k1gInSc4	Mount
Vernonu	Vernon	k1gInSc2	Vernon
nebo	nebo	k8xC	nebo
ubytovna	ubytovna	k1gFnSc1	ubytovna
White	Whit	k1gInSc5	Whit
Salmon	Salmon	k1gInSc4	Salmon
Lodge	Lodg	k1gFnPc1	Lodg
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
sopky	sopka	k1gFnSc2	sopka
Mount	Mount	k1gMnSc1	Mount
Baker	Baker	k1gMnSc1	Baker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Festivalový	festivalový	k2eAgInSc1d1	festivalový
orchestrový	orchestrový	k2eAgInSc1d1	orchestrový
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
hudebníků	hudebník	k1gMnPc2	hudebník
ze	z	k7c2	z
známých	známý	k2eAgInPc2d1	známý
evropských	evropský	k2eAgInPc2d1	evropský
i	i	k8xC	i
severoamerických	severoamerický	k2eAgInPc2d1	severoamerický
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
festivalu	festival	k1gInSc6	festival
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
také	také	k9	také
festivalový	festivalový	k2eAgInSc4d1	festivalový
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hudebníků	hudebník	k1gMnPc2	hudebník
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
Whatcom	Whatcom	k1gInSc1	Whatcom
<g/>
,	,	kIx,	,
Skagit	Skagit	k1gInSc1	Skagit
a	a	k8xC	a
Snohomish	Snohomish	k1gInSc1	Snohomish
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vybíráni	vybírán	k2eAgMnPc1d1	vybírán
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
článku	článek	k1gInSc6	článek
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
informoval	informovat	k5eAaBmAgInS	informovat
deník	deník	k1gInSc4	deník
The	The	k1gMnPc2	The
Bellingham	Bellingham	k1gInSc1	Bellingham
Herald	Heralda	k1gFnPc2	Heralda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
organizátorů	organizátor	k1gMnPc2	organizátor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
festival	festival	k1gInSc1	festival
neuskuteční	uskutečnit	k5eNaPmIp3nP	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
deníku	deník	k1gInSc6	deník
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ale	ale	k8xC	ale
odhalil	odhalit	k5eAaPmAgMnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
organizátorům	organizátor	k1gMnPc3	organizátor
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
festival	festival	k1gInSc1	festival
opět	opět	k6eAd1	opět
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
festival	festival	k1gInSc1	festival
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
jak	jak	k6eAd1	jak
po	po	k7c6	po
umělecké	umělecký	k2eAgFnSc6d1	umělecká
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
finanční	finanční	k2eAgFnSc6d1	finanční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
festivale	festival	k1gInSc6	festival
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
houslista	houslista	k1gMnSc1	houslista
Joshua	Joshua	k1gMnSc1	Joshua
Bell	bell	k1gInSc1	bell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bellingham	Bellingham	k1gInSc1	Bellingham
Festival	festival	k1gInSc1	festival
of	of	k?	of
Music	Musice	k1gFnPc2	Musice
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
