<s>
Bellinghamský	Bellinghamský	k2eAgInSc1d1	Bellinghamský
festival	festival	k1gInSc1	festival
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc1	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každé	každý	k3xTgNnSc4	každý
léto	léto	k1gNnSc4	léto
v	v	k7c6	v
Bellinghamu	Bellingham	k1gInSc6	Bellingham
<g/>
,	,	kIx,	,
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
