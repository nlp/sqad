<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gMnSc5
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Epic	Epic	k6eAd1
Games	Games	k1gInSc4
První	první	k4xOgNnPc4
vydání	vydání	k1gNnPc4
</s>
<s>
1996	#num#	k4
Aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
4.26	4.26	k4
<g/>
.2	.2	k4
(	(	kIx(
<g/>
13.04	13.04	k4
<g/>
.2021	.2021	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Microsoft	Microsoft	kA
WindowsGNU	WindowsGNU	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
Vyvíjeno	vyvíjen	k2eAgNnSc1d1
v	v	k7c4
</s>
<s>
C	C	kA
<g/>
++	++	k?
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
herní	herní	k2eAgFnSc1d1
engine	enginout	k5eAaPmIp3nS
Licence	licence	k1gFnSc1
</s>
<s>
Open	Open	k1gMnSc1
source	source	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
www.unrealengine.com	www.unrealengine.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
je	on	k3xPp3gNnSc4
herní	herní	k2eAgFnSc1d1
engine	enginout	k5eAaPmIp3nS
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
firmou	firma	k1gFnSc7
Epic	Epic	k1gFnSc4
Games	Games	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
ve	v	k7c6
hře	hra	k1gFnSc6
Unreal	Unreal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
Unreal	Unreal	k1gInSc1
Engine	Engin	k1gInSc5
několikrát	několikrát	k6eAd1
vylepšen	vylepšit	k5eAaPmNgMnS
a	a	k8xC
doplněn	doplnit	k5eAaPmNgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
v	v	k7c6
několika	několik	k4yIc6
desítkách	desítka	k1gFnPc6
novějších	nový	k2eAgInPc2d2
herních	herní	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
engine	enginout	k5eAaPmIp3nS
určen	určit	k5eAaPmNgInS
pouze	pouze	k6eAd1
pro	pro	k7c4
střílečky	střílečka	k1gFnPc4
z	z	k7c2
pohledu	pohled	k1gInSc2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
našel	najít	k5eAaPmAgMnS
využití	využití	k1gNnSc4
i	i	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
MMORPG	MMORPG	kA
<g/>
,	,	kIx,
RPG	RPG	kA
a	a	k8xC
adventurách	adventura	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Jádro	jádro	k1gNnSc1
Unreal	Unreal	k1gInSc1
Enginu	Engina	k1gMnSc4
<g/>
,	,	kIx,
napsané	napsaný	k2eAgInPc1d1
v	v	k7c6
programovacím	programovací	k2eAgInSc6d1
jazyku	jazyk	k1gInSc6
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
podporuje	podporovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
různých	různý	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
,	,	kIx,
Linux	Linux	kA
<g/>
,	,	kIx,
Mac	Mac	kA
OS	OS	kA
<g/>
,	,	kIx,
Mac	Mac	kA
OS	OS	kA
X	X	kA
na	na	k7c6
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnovější	nový	k2eAgFnSc1d3
verze	verze	k1gFnSc1
Unreal	Unreal	k1gInSc1
Enginu	Engin	k1gInSc2
podporuje	podporovat	k5eAaImIp3nS
také	také	k9
platformy	platforma	k1gFnPc4
herních	herní	k2eAgNnPc2d1
konzolí	konzolí	k1gNnPc2
Dreamcast	Dreamcast	k1gFnSc1
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
i	i	k8xC
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
ostatních	ostatní	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
enginu	enginout	k5eAaPmIp1nS
ve	v	k7c6
hře	hra	k1gFnSc6
je	být	k5eAaImIp3nS
napsána	napsat	k5eAaPmNgFnS,k5eAaBmNgFnS
ve	v	k7c6
speciálním	speciální	k2eAgInSc6d1
kódu	kód	k1gInSc6
UnrealScript	UnrealScripta	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgNnSc3,k3yRgNnSc3,k3yIgNnSc3
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
při	při	k7c6
vytváření	vytváření	k1gNnSc6
případných	případný	k2eAgInPc2d1
herních	herní	k2eAgInPc2d1
modů	modus	k1gInPc2
zasahovat	zasahovat	k5eAaImF
hluboko	hluboko	k6eAd1
do	do	k7c2
jádra	jádro	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
postačí	postačit	k5eAaPmIp3nS
pouze	pouze	k6eAd1
změnit	změnit	k5eAaPmF
skripty	skript	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
obsahuje	obsahovat	k5eAaImIp3nS
užitečný	užitečný	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
UnrealEd	UnrealEda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
1	#num#	k4
</s>
<s>
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
Unreal	Unreal	k1gInSc1
Enginu	Engin	k1gInSc2
spatřila	spatřit	k5eAaPmAgFnS
světlo	světlo	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
květnu	květen	k1gInSc6
1998	#num#	k4
ve	v	k7c6
hře	hra	k1gFnSc6
Unreal	Unreal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
sjednotila	sjednotit	k5eAaPmAgFnS
do	do	k7c2
jediného	jediný	k2eAgInSc2d1
enginu	engin	k1gInSc2
rendering	rendering	k1gInSc4
<g/>
,	,	kIx,
detekci	detekce	k1gFnSc3
kolizí	kolize	k1gFnPc2
<g/>
,	,	kIx,
umělou	umělý	k2eAgFnSc7d1
inteligenci	inteligence	k1gFnSc4
<g/>
,	,	kIx,
viditelnost	viditelnost	k1gFnSc4
<g/>
,	,	kIx,
networking	networking	k1gInSc4
i	i	k8xC
správu	správa	k1gFnSc4
souborového	souborový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engine	Engin	k1gInSc5
fungoval	fungovat	k5eAaImAgInS
pouze	pouze	k6eAd1
na	na	k7c6
platformách	platforma	k1gFnPc6
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInSc6
vzniku	vznik	k1gInSc6
měl	mít	k5eAaImAgMnS
nejvyšší	vysoký	k2eAgFnSc4d3
zásluhu	zásluha	k1gFnSc4
Tim	Tim	k?
Sweeney	Sweenea	k1gFnSc2
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
jádra	jádro	k1gNnSc2
Unreal	Unreal	k1gInSc1
Enginu	Engina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
1999	#num#	k4
byl	být	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
Unreal	Unreal	k1gInSc1
engine	enginout	k5eAaPmIp3nS
vylepšován	vylepšován	k2eAgMnSc1d1
a	a	k8xC
poslední	poslední	k2eAgFnSc7d1
oficiální	oficiální	k2eAgFnSc7d1
záplatou	záplata	k1gFnSc7
byla	být	k5eAaImAgFnS
build-verze	build-verze	k1gFnSc1
226	#num#	k4
<g/>
f.	f.	k?
Pozdější	pozdní	k2eAgFnSc2d2
verze	verze	k1gFnSc2
vyvíjely	vyvíjet	k5eAaImAgInP
už	už	k6eAd1
jenom	jenom	k9
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
zakoupily	zakoupit	k5eAaPmAgFnP
na	na	k7c4
tento	tento	k3xDgInSc4
engine	enginout	k5eAaPmIp3nS
licenci	licence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Engine	Enginout	k5eAaPmIp3nS
podporuje	podporovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
šest	šest	k4xCc4
renderovacích	renderovací	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
doba	doba	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
vyvíjen	vyvíjen	k2eAgInSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
masivnímu	masivní	k2eAgNnSc3d1
rozšíření	rozšíření	k1gNnSc3
užívání	užívání	k1gNnSc2
grafických	grafický	k2eAgInPc2d1
akcelerátorů	akcelerátor	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
ovladače	ovladač	k1gInPc1
nebyly	být	k5eNaImAgInP
mezi	mezi	k7c7
sebou	se	k3xPyFc7
kompatibilní	kompatibilní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavěn	stavěn	k2eAgInSc1d1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
pro	pro	k7c4
technologii	technologie	k1gFnSc4
3	#num#	k4
<g/>
Dfx	Dfx	k1gMnSc5
Glide	Glid	k1gMnSc5
<g/>
,	,	kIx,
S3	S3	k1gFnSc4
Metal	metal	k1gInSc4
a	a	k8xC
PowerVR	PowerVR	k1gFnSc4
SGL	SGL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časem	časem	k6eAd1
ale	ale	k8xC
začala	začít	k5eAaPmAgFnS
růst	růst	k1gInSc4
obliba	obliba	k1gFnSc1
Direct	Directum	k1gNnPc2
3D	3D	k4
(	(	kIx(
<g/>
verze	verze	k1gFnSc1
5	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
byl	být	k5eAaImAgInS
těsně	těsně	k6eAd1
před	před	k7c7
vydáním	vydání	k1gNnSc7
přidán	přidán	k2eAgInSc4d1
k	k	k7c3
původním	původní	k2eAgFnPc3d1
třem	tři	k4xCgFnPc3
ještě	ještě	k9
renderer	renderer	k1gMnSc1
pracující	pracující	k2eAgMnSc1d1
na	na	k7c6
technologii	technologie	k1gFnSc6
DirectX	DirectX	k1gFnSc2
<g/>
,	,	kIx,
jenže	jenže	k8xC
tento	tento	k3xDgInSc1
renderer	renderer	k1gInSc1
nebyl	být	k5eNaImAgInS
kvůli	kvůli	k7c3
časové	časový	k2eAgFnSc3d1
tísni	tíseň	k1gFnSc3
dobře	dobře	k6eAd1
naprogramován	naprogramovat	k5eAaPmNgInS
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
pomalejší	pomalý	k2eAgFnSc4d2
než	než	k8xS
3	#num#	k4
<g/>
Dfx	Dfx	k1gFnPc2
a	a	k8xC
obsahoval	obsahovat	k5eAaImAgInS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
chyb	chyba	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
v	v	k7c6
záplatách	záplata	k1gFnPc6
enginu	enginout	k5eAaPmIp1nS
opravován	opravován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
ještě	ještě	k9
jedna	jeden	k4xCgFnSc1
technologie	technologie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
podporuje	podporovat	k5eAaImIp3nS
OpenGL	OpenGL	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unreal	Unreal	k1gInSc1
Engine	Engin	k1gInSc5
1	#num#	k4
OpenGL	OpenGL	k1gFnPc6
sice	sice	k8xC
také	také	k9
podporuje	podporovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
nedostatku	nedostatek	k1gInSc3
ovladačů	ovladač	k1gInPc2
dostupných	dostupný	k2eAgInPc2d1
tehdejšímu	tehdejší	k2eAgMnSc3d1
hardwaru	hardware	k1gInSc2
se	se	k3xPyFc4
od	od	k7c2
dalšího	další	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
na	na	k7c6
bázi	báze	k1gFnSc6
OpenGL	OpenGL	k1gFnSc2
upustilo	upustit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzálním	univerzální	k2eAgNnSc7d1
rendererem	renderero	k1gNnSc7
byl	být	k5eAaImAgInS
starý	starý	k2eAgInSc1d1
software	software	k1gInSc1
rendering	rendering	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
schopný	schopný	k2eAgInSc1d1
fungovat	fungovat	k5eAaImF
s	s	k7c7
jakýmkoliv	jakýkoliv	k3yIgInSc7
hardwarem	hardware	k1gInSc7
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
počítač	počítač	k1gMnSc1
neměl	mít	k5eNaImAgMnS
nainstalovaný	nainstalovaný	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
grafický	grafický	k2eAgInSc4d1
akcelerátor	akcelerátor	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Engine	Enginout	k5eAaPmIp3nS
podporuje	podporovat	k5eAaImIp3nS
barevné	barevný	k2eAgNnSc1d1
(	(	kIx(
<g/>
až	až	k6eAd1
32	#num#	k4
<g/>
bitová	bitový	k2eAgFnSc1d1
<g/>
)	)	kIx)
dynamické	dynamický	k2eAgNnSc1d1
osvětlení	osvětlení	k1gNnSc1
s	s	k7c7
možností	možnost	k1gFnSc7
překrývání	překrývání	k1gNnSc2
barev	barva	k1gFnPc2
a	a	k8xC
mícháním	míchání	k1gNnSc7
rozmazaných	rozmazaný	k2eAgInPc2d1
stínů	stín	k1gInPc2
a	a	k8xC
paprskové	paprskový	k2eAgNnSc4d1
osvětlení	osvětlení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
podporuje	podporovat	k5eAaImIp3nS
paprskové	paprskový	k2eAgNnSc4d1
<g/>
,	,	kIx,
válcové	válcový	k2eAgNnSc4d1
<g/>
,	,	kIx,
kuželové	kuželový	k2eAgNnSc4d1
<g/>
,	,	kIx,
světlometné	světlometný	k2eAgNnSc4d1
<g/>
,	,	kIx,
kolovité	kolovitý	k2eAgNnSc4d1
<g/>
,	,	kIx,
ambientní	ambientní	k2eAgNnSc4d1
(	(	kIx(
<g/>
okolní	okolní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgInPc1d1
světelné	světelný	k2eAgInPc1d1
efekty	efekt	k1gInPc1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
přes	přes	k7c4
20	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
efekty	efekt	k1gInPc1
záře	zář	k1gFnSc2
<g/>
,	,	kIx,
mlhy	mlha	k1gFnSc2
a	a	k8xC
coronas	coronasa	k1gFnPc2
(	(	kIx(
<g/>
světelný	světelný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
dálky	dálka	k1gFnSc2
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
jakoby	jakoby	k8xS
byl	být	k5eAaImAgInS
obklopený	obklopený	k2eAgInSc1d1
kulatou	kulatý	k2eAgFnSc7d1
září	zář	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engine	Engin	k1gInSc5
též	též	k9
umožňuje	umožňovat	k5eAaImIp3nS
i	i	k9
zrcadlový	zrcadlový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
několikanásobného	několikanásobný	k2eAgInSc2d1
odrazu	odraz	k1gInSc2
zrcadel	zrcadlo	k1gNnPc2
umístěných	umístěný	k2eAgNnPc2d1
naproti	naproti	k7c3
sobě	sebe	k3xPyFc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
částečnou	částečný	k2eAgFnSc4d1
odrazivost	odrazivost	k1gFnSc4
světla	světlo	k1gNnSc2
některých	některý	k3yIgInPc2
povrchů	povrch	k1gInPc2
<g/>
,	,	kIx,
portálové	portálový	k2eAgFnSc2d1
a	a	k8xC
„	„	k?
<g/>
warpové	warpová	k1gFnSc2
<g/>
“	“	k?
efekty	efekt	k1gInPc4
(	(	kIx(
<g/>
umožňující	umožňující	k2eAgFnSc1d1
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
za	za	k7c7
nimi	on	k3xPp3gMnPc7
nachází	nacházet	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zobrazení	zobrazení	k1gNnSc6
objektů	objekt	k1gInPc2
zdánlivě	zdánlivě	k6eAd1
se	se	k3xPyFc4
nacházejících	nacházející	k2eAgFnPc2d1
v	v	k7c6
nekonečné	konečný	k2eNgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
přes	přes	k7c4
"	"	kIx"
<g/>
okno	okno	k1gNnSc4
na	na	k7c6
obloze	obloha	k1gFnSc6
<g/>
"	"	kIx"
(	(	kIx(
<g/>
např.	např.	kA
při	při	k7c6
zobrazení	zobrazení	k1gNnSc6
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
obou	dva	k4xCgNnPc2
sluncí	slunce	k1gNnPc2
<g/>
,	,	kIx,
hvězd	hvězda	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
zobrazení	zobrazení	k1gNnSc1
oblohy	obloha	k1gFnSc2
a	a	k8xC
pozadí	pozadí	k1gNnSc2
map	mapa	k1gFnPc2
nezávisle	závisle	k6eNd1
na	na	k7c6
poloze	poloha	k1gFnSc6
a	a	k8xC
orientaci	orientace	k1gFnSc6
pozorovatele	pozorovatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
engine	enginout	k5eAaPmIp3nS
umožňoval	umožňovat	k5eAaImAgInS
zobrazit	zobrazit	k5eAaPmF
zakulacený	zakulacený	k2eAgInSc1d1
tvar	tvar	k1gInSc1
povrchu	povrch	k1gInSc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
velký	velký	k2eAgInSc4d1
počet	počet	k1gInSc4
mnohoúhelníků	mnohoúhelník	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
zobrazování	zobrazování	k1gNnSc4
textur	textura	k1gFnPc2
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
512	#num#	k4
x	x	k?
512	#num#	k4
pixelů	pixel	k1gInPc2
(	(	kIx(
<g/>
s	s	k7c7
funkcemi	funkce	k1gFnPc7
imitujícími	imitující	k2eAgFnPc7d1
vypuklý	vypuklý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
jinak	jinak	k6eAd1
rovného	rovný	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
(	(	kIx(
<g/>
Emboss	Emboss	k1gInSc1
bump	bump	k1gMnSc1
mapping	mapping	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
multi-texturing	multi-texturing	k1gInSc4
<g/>
,	,	kIx,
dynamiku	dynamika	k1gFnSc4
nanesených	nanesený	k2eAgFnPc2d1
detailních	detailní	k2eAgFnPc2d1
textur	textura	k1gFnPc2
a	a	k8xC
procedurně	procedurně	k6eAd1
animovaných	animovaný	k2eAgFnPc2d1
textur	textura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Engine	Enginout	k5eAaPmIp3nS
podporuje	podporovat	k5eAaImIp3nS
i	i	k9
negrafické	grafický	k2eNgFnPc4d1
součásti	součást	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
digitalizovaný	digitalizovaný	k2eAgInSc4d1
zvukový	zvukový	k2eAgInSc4d1
systém	systém	k1gInSc4
s	s	k7c7
podporou	podpora	k1gFnSc7
formátů	formát	k1gInPc2
mp	mp	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
CD-audia	CD-audium	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
hudby	hudba	k1gFnSc2
vytvořené	vytvořený	k2eAgFnSc2d1
pomocí	pomoc	k1gFnSc7
trackera	trackero	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
(	(	kIx(
<g/>
software	software	k1gInSc1
a	a	k8xC
hardware	hardware	k1gInSc1
rendering	rendering	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvukový	zvukový	k2eAgInSc1d1
systém	systém	k1gInSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
k	k	k7c3
dokreslení	dokreslení	k1gNnSc3
trojrozměrných	trojrozměrný	k2eAgInPc2d1
zvukových	zvukový	k2eAgInPc2d1
efektů	efekt	k1gInPc2
při	při	k7c6
software	software	k1gInSc4
renderingu	rendering	k1gInSc2
podporujícím	podporující	k2eAgInSc7d1
nejvýše	vysoce	k6eAd3,k6eAd1
stereo	stereo	k2eAgInSc1d1
fázový	fázový	k2eAgInSc1d1
posun	posun	k1gInSc1
a	a	k8xC
Dopplerův	Dopplerův	k2eAgInSc1d1
jev	jev	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Implementace	implementace	k1gFnSc1
hudebního	hudební	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c4
Unreal	Unreal	k1gInSc4
enginu	enginout	k5eAaPmIp1nS
pomocí	pomocí	k7c2
trackera	trackero	k1gNnSc2
umožňuje	umožňovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
plynulý	plynulý	k2eAgInSc1d1
přechod	přechod	k1gInSc1
mezi	mezi	k7c7
hudebními	hudební	k2eAgFnPc7d1
stopami	stopa	k1gFnPc7
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
ve	v	k7c6
hře	hra	k1gFnSc6
zrovna	zrovna	k6eAd1
děje	dít	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
výrazně	výrazně	k6eAd1
zmenšuje	zmenšovat	k5eAaImIp3nS
velikost	velikost	k1gFnSc4
hudebních	hudební	k2eAgInPc2d1
souborů	soubor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gInSc1
engine	enginout	k5eAaPmIp3nS
1	#num#	k4
však	však	k9
obsahuje	obsahovat	k5eAaImIp3nS
málo	málo	k6eAd1
výkonný	výkonný	k2eAgInSc1d1
detektor	detektor	k1gInSc1
kolizí	kolize	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tehdejší	tehdejší	k2eAgInPc1d1
procesory	procesor	k1gInPc1
zvládly	zvládnout	k5eAaPmAgInP
stíhat	stíhat	k5eAaImF
výpočty	výpočet	k1gInPc1
v	v	k7c6
přijatelné	přijatelný	k2eAgFnSc6d1
rychlosti	rychlost	k1gFnSc6
a	a	k8xC
hra	hra	k1gFnSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
tomu	ten	k3xDgMnSc3
neškubala	škubat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předností	přednost	k1gFnPc2
Unreal	Unreal	k1gInSc4
Enginu	Engina	k1gFnSc4
byla	být	k5eAaImAgFnS
na	na	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
velmi	velmi	k6eAd1
vyspělá	vyspělý	k2eAgFnSc1d1
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
dokázali	dokázat	k5eAaPmAgMnP
uhýbat	uhýbat	k5eAaImF
vystřeleným	vystřelený	k2eAgInPc3d1
projektilům	projektil	k1gInPc3
nebo	nebo	k8xC
v	v	k7c6
Unrealu	Unreal	k1gInSc6
občas	občas	k6eAd1
předstírat	předstírat	k5eAaImF
vlastní	vlastní	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
a	a	k8xC
následně	následně	k6eAd1
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
nic	nic	k6eAd1
netušícího	tušící	k2eNgMnSc4d1
hráče	hráč	k1gMnSc4
zezadu	zezadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
1.5	1.5	k4
</s>
<s>
Základem	základ	k1gInSc7
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
verzi	verze	k1gFnSc4
Unreal	Unreal	k1gInSc4
enginu	engin	k1gInSc2
byla	být	k5eAaImAgFnS
ta	ten	k3xDgFnSc1
předcházející	předcházející	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgNnSc7
enginem	engino	k1gNnSc7
byla	být	k5eAaImAgFnS
uvedena	uveden	k2eAgFnSc1d1
hra	hra	k1gFnSc1
Unreal	Unreal	k1gMnSc1
Tournament	Tournament	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verzování	Verzování	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
na	na	k7c6
hodnotě	hodnota	k1gFnSc6
338	#num#	k4
a	a	k8xC
skončilo	skončit	k5eAaPmAgNnS
oficiálně	oficiálně	k6eAd1
na	na	k7c6
build-verzi	build-verze	k1gFnSc6
436	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgFnSc1d2
verze	verze	k1gFnSc1
byly	být	k5eAaImAgFnP
opět	opět	k6eAd1
vyvinuté	vyvinutý	k2eAgInPc1d1
jinými	jiný	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
se	s	k7c7
zakoupenou	zakoupený	k2eAgFnSc7d1
licencí	licence	k1gFnSc7
na	na	k7c4
tento	tento	k3xDgInSc4
engine	enginout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
nástroje	nástroj	k1gInSc2
UnrealEd	UnrealEda	k1gFnPc2
ale	ale	k8xC
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
verzi	verze	k1gFnSc4
enginu	enginout	k5eAaPmIp1nS
kompletně	kompletně	k6eAd1
přepsán	přepsán	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
enginu	engina	k1gFnSc4
podporovala	podporovat	k5eAaImAgFnS
již	již	k6eAd1
i	i	k8xC
první	první	k4xOgFnPc4
konzolové	konzolový	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
Dreamcast	Dreamcast	k1gFnSc1
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
(	(	kIx(
<g/>
build-verze	build-verze	k1gFnSc1
613	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahovala	obsahovat	k5eAaImAgFnS
všechny	všechen	k3xTgInPc4
prvky	prvek	k1gInPc4
předchozí	předchozí	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
,	,	kIx,
opravila	opravit	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnPc4
případné	případný	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
<g/>
,	,	kIx,
vylepšila	vylepšit	k5eAaPmAgFnS
mnohé	mnohý	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
a	a	k8xC
obsahovala	obsahovat	k5eAaImAgFnS
i	i	k9
některé	některý	k3yIgMnPc4
nové	nový	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
podporu	podpora	k1gFnSc4
zobrazení	zobrazení	k1gNnSc2
obtisků	obtisk	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
buil-verze	buil-verze	k1gFnSc2
400	#num#	k4
i	i	k8xC
S3TC	S3TC	k1gMnSc1
kompresi	komprese	k1gFnSc4
textur	textura	k1gFnPc2
nebo	nebo	k8xC
zobrazení	zobrazení	k1gNnSc2
textur	textura	k1gFnPc2
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
1024	#num#	k4
x	x	k?
1024	#num#	k4
pixelů	pixel	k1gInPc2
nebo	nebo	k8xC
systém	systém	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
drobných	drobný	k2eAgFnPc2d1
částeček	částečka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylepšení	vylepšení	k1gNnSc2
doznaly	doznat	k5eAaPmAgFnP
i	i	k9
zvukové	zvukový	k2eAgInPc4d1
efekty	efekt	k1gInPc4
<g/>
,	,	kIx,
zejména	zejména	k9
díky	díky	k7c3
vylepšením	vylepšení	k1gNnSc7
podpory	podpora	k1gFnSc2
A	a	k9
<g/>
3	#num#	k4
<g/>
D	D	kA
<g/>
,	,	kIx,
EAX	EAX	kA
<g/>
,	,	kIx,
DS	DS	kA
<g/>
3	#num#	k4
<g/>
D.	D.	kA
Největších	veliký	k2eAgFnPc2d3
změn	změna	k1gFnPc2
a	a	k8xC
vylepšení	vylepšení	k1gNnPc2
ale	ale	k9
doznala	doznat	k5eAaPmAgFnS
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
nepřátel	nepřítel	k1gMnPc2
(	(	kIx(
<g/>
boti	bot	k1gMnPc1
se	se	k3xPyFc4
dokázali	dokázat	k5eAaPmAgMnP
více	hodně	k6eAd2
semknout	semknout	k5eAaPmF
během	během	k7c2
týmové	týmový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
podpora	podpora	k1gFnSc1
síťové	síťový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
2	#num#	k4
</s>
<s>
Hra	hra	k1gFnSc1
Killing	Killing	k1gInSc1
Floor	Floor	k1gInSc1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c4
Unreal	Unreal	k1gInSc4
Enginu	Engin	k1gInSc2
2	#num#	k4
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
2	#num#	k4
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
ve	v	k7c6
hře	hra	k1gFnSc6
America	Americ	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Army	Arm	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
enginu	engin	k1gInSc2
byl	být	k5eAaImAgInS
kompletně	kompletně	k6eAd1
celý	celý	k2eAgInSc1d1
přepsán	přepsán	k2eAgInSc1d1
a	a	k8xC
upraven	upraven	k2eAgInSc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
použitelný	použitelný	k2eAgInSc1d1
nejen	nejen	k6eAd1
v	v	k7c6
žánru	žánr	k1gInSc6
FPS	FPS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verzování	Verzování	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
číslem	číslo	k1gNnSc7
633	#num#	k4
a	a	k8xC
skončilo	skončit	k5eAaPmAgNnS
na	na	k7c4
build-verzi	build-verze	k1gFnSc4
3186	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engine	Engin	k1gInSc5
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
DirectX	DirectX	k1gFnSc7
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
a	a	k8xC
OpenGL	OpenGL	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rendering	Rendering	k1gInSc1
je	být	k5eAaImIp3nS
koncipován	koncipovat	k5eAaBmNgInS
pro	pro	k7c4
vysokou	vysoký	k2eAgFnSc4d1
výkonnost	výkonnost	k1gFnSc4
a	a	k8xC
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pěti	pět	k4xCc2
základních	základní	k2eAgInPc2d1
renderovacích	renderovací	k2eAgInPc2d1
prvků	prvek	k1gInPc2
(	(	kIx(
<g/>
BSP	BSP	kA
geometry	geometr	k1gInPc4
(	(	kIx(
<g/>
na	na	k7c6
tvarování	tvarování	k1gNnSc6
terénu	terén	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Static	statice	k1gFnPc2
meshes	meshesa	k1gFnPc2
(	(	kIx(
<g/>
na	na	k7c6
tvarování	tvarování	k1gNnSc6
a	a	k8xC
texturování	texturování	k1gNnSc6
objektů	objekt	k1gInPc2
s	s	k7c7
velkým	velký	k2eAgInSc7d1
počtem	počet	k1gInSc7
polygonů	polygon	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Animated	Animated	k1gMnSc1
skeletal	skeletat	k5eAaImAgMnS,k5eAaPmAgMnS
meshes	meshes	k1gInSc4
(	(	kIx(
<g/>
tvarování	tvarování	k1gNnSc1
a	a	k8xC
texturování	texturování	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
herních	herní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
a	a	k8xC
zbraní	zbraň	k1gFnPc2
importováno	importovat	k5eAaBmNgNnS
většinou	většinou	k6eAd1
z	z	k7c2
externích	externí	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
jako	jako	k9
např.	např.	kA
3D	3D	k4
Studio	studio	k1gNnSc1
Max	Max	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vertex	vertex	k1gInSc1
meshes	meshes	k1gInSc1
(	(	kIx(
<g/>
animování	animování	k1gNnSc1
vrcholu	vrchol	k1gInSc2
<g/>
;	;	kIx,
pouze	pouze	k6eAd1
když	když	k8xS
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Height-Mapped	Height-Mapped	k1gMnSc1
Terrain	Terrain	k1gMnSc1
(	(	kIx(
<g/>
terén	terén	k1gInSc1
uložen	uložit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
pole	pole	k1gNnSc1
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
výšky	výška	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
dobře	dobře	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
manipulovat	manipulovat	k5eAaImF
v	v	k7c6
editoru	editor	k1gInSc6
<g/>
))	))	k?
<g/>
.	.	kIx.
</s>
<s>
Engine	Enginout	k5eAaPmIp3nS
podporuje	podporovat	k5eAaImIp3nS
zlepšené	zlepšený	k2eAgNnSc4d1
osvětlení	osvětlení	k1gNnSc4
všech	všecek	k3xTgInPc2
geometrických	geometrický	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
<g/>
,	,	kIx,
i	i	k9
stínování	stínování	k1gNnSc4
<g/>
,	,	kIx,
hardwarovou	hardwarový	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pixel	pixel	k1gInSc1
shaderům	shader	k1gMnPc3
a	a	k8xC
vertex	vertex	k1gInSc4
shaderům	shader	k1gMnPc3
<g/>
,	,	kIx,
vylepšenou	vylepšený	k2eAgFnSc4d1
detekci	detekce	k1gFnSc4
kolizí	kolize	k1gFnPc2
a	a	k8xC
kompresi	komprese	k1gFnSc4
textur	textura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
zcela	zcela	k6eAd1
nové	nový	k2eAgInPc4d1
podporované	podporovaný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
patří	patřit	k5eAaImIp3nS
mapování	mapování	k1gNnSc1
okolí	okolí	k1gNnSc2
(	(	kIx(
<g/>
environmental	environmentat	k5eAaPmAgInS,k5eAaImAgInS
mapping	mapping	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
velkého	velký	k2eAgInSc2d1
rozsahu	rozsah	k1gInSc2
terénu	terén	k1gInSc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
zobrazení	zobrazení	k1gNnSc2
textur	textura	k1gFnPc2
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
2048	#num#	k4
x	x	k?
2048	#num#	k4
pixelů	pixel	k1gInPc2
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
rozlišení	rozlišení	k1gNnSc6
až	až	k9
4096	#num#	k4
x	x	k?
4096	#num#	k4
pixelů	pixel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplně	úplně	k6eAd1
nová	nový	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
část	část	k1gFnSc1
enginu	engin	k1gInSc2
starající	starající	k2eAgFnSc1d1
se	se	k3xPyFc4
o	o	k7c4
on-line	on-lin	k1gInSc5
hraní	hraní	k1gNnSc4
<g/>
,	,	kIx,
umělou	umělý	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
<g/>
,	,	kIx,
editaci	editace	k1gFnSc4
terénu	terén	k1gInSc2
a	a	k8xC
nahrávání	nahrávání	k1gNnSc4
a	a	k8xC
zobrazování	zobrazování	k1gNnSc4
videí	video	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
novinkou	novinka	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
implementace	implementace	k1gFnSc1
Karma-physics	Karma-physicsa	k1gFnPc2
SDK	SDK	kA
na	na	k7c6
propočítání	propočítání	k1gNnSc6
fyziky	fyzika	k1gFnSc2
hýbajících	hýbající	k2eAgInPc2d1
se	se	k3xPyFc4
objektů	objekt	k1gInPc2
(	(	kIx(
<g/>
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
strojů	stroj	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
přesnosti	přesnost	k1gFnSc2
kolizí	kolize	k1gFnPc2
a	a	k8xC
odrazů	odraz	k1gInPc2
drobných	drobný	k2eAgFnPc2d1
částeček	částečka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gMnSc5
2.5	2.5	k4
</s>
<s>
Verze	verze	k1gFnSc1
2.5	2.5	k4
Unreal	Unreal	k1gMnSc1
Enginu	Engin	k1gInSc2
byla	být	k5eAaImAgNnP
vydána	vydat	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
vylepšené	vylepšený	k2eAgNnSc1d1
a	a	k8xC
optimalizované	optimalizovaný	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
Unreal	Unreal	k1gInSc1
Enginu	Engin	k1gInSc6
2	#num#	k4
s	s	k7c7
přidanými	přidaný	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gMnSc5
2X	2X	k4
</s>
<s>
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
enginu	engin	k1gInSc2
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
totožná	totožný	k2eAgFnSc1d1
s	s	k7c7
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
2.5	2.5	k4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
optimalizovaná	optimalizovaný	k2eAgFnSc1d1
speciálně	speciálně	k6eAd1
pro	pro	k7c4
hry	hra	k1gFnPc4
vydané	vydaný	k2eAgFnPc4d1
na	na	k7c6
Xboxu	Xbox	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
3	#num#	k4
</s>
<s>
Hra	hra	k1gFnSc1
Unmechanical	Unmechanical	k1gMnPc2
pracující	pracující	k2eAgFnSc1d1
na	na	k7c4
Unreal	Unreal	k1gInSc4
Enginu	Engin	k1gInSc2
3	#num#	k4
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
3	#num#	k4
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
jako	jako	k8xS,k8xC
build-verze	build-verze	k1gFnPc1
3376	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
otevřen	otevřít	k5eAaPmNgInS
pro	pro	k7c4
kompletní	kompletní	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
oblíbeným	oblíbený	k2eAgInSc7d1
základem	základ	k1gInSc7
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hojně	hojně	k6eAd1
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
byl	být	k5eAaImAgInS
ještě	ještě	k9
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
využit	využít	k5eAaPmNgInS
například	například	k6eAd1
ve	v	k7c6
hře	hra	k1gFnSc6
Medal	Medal	k1gMnSc1
of	of	k?
honor	honor	k1gInSc1
airborne	airbornout	k5eAaPmIp3nS
nebo	nebo	k8xC
v	v	k7c6
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Asylum	Asylum	k1gNnSc1
a	a	k8xC
v	v	k7c4
Brothers	Brothers	k1gInSc4
in	in	k?
Arms	Arms	k1gInSc1
-	-	kIx~
Hell	Hell	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Highway	Highwa	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
verzi	verze	k1gFnSc4
Unreal	Unreal	k1gInSc4
Enginu	Engin	k1gInSc2
používají	používat	k5eAaImIp3nP
i	i	k9
velké	velký	k2eAgInPc4d1
herní	herní	k2eAgInPc4d1
tituly	titul	k1gInPc4
vydané	vydaný	k2eAgInPc4d1
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
-	-	kIx~
například	například	k6eAd1
BioShock	BioShock	k1gInSc1
Infinite	Infinit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
také	také	k9
stojí	stát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
v	v	k7c6
MMORPG	MMORPG	kA
TERA	TERA	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
Exiled	Exiled	k1gMnSc1
Realm	Realm	k1gMnSc1
of	of	k?
Arborea	Arborea	k1gMnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
populární	populární	k2eAgFnSc4d1
i	i	k9
mezi	mezi	k7c7
vývojáři	vývojář	k1gMnPc7
těchto	tento	k3xDgFnPc2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
mnoha	mnoho	k4c6
různých	různý	k2eAgFnPc6d1
platformách	platforma	k1gFnPc6
na	na	k7c6
bázi	báze	k1gFnSc6
DirectX	DirectX	k1gFnSc1
verze	verze	k1gFnSc1
9	#num#	k4
až	až	k9
11	#num#	k4
(	(	kIx(
<g/>
Windows	Windows	kA
a	a	k8xC
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
bázi	báze	k1gFnSc6
OpenGL	OpenGL	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
pracují	pracovat	k5eAaImIp3nP
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
<g/>
,	,	kIx,
IOS	IOS	kA
(	(	kIx(
<g/>
Apple	Apple	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mac	Mac	kA
OS	OS	kA
<g/>
,	,	kIx,
Android	android	k1gInSc4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc4
Vita	vit	k2eAgFnSc1d1
a	a	k8xC
Wii	Wii	k1gFnSc1
U.	U.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
2011	#num#	k4
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
verzí	verze	k1gFnPc2
tohoto	tento	k3xDgInSc2
enginu	engin	k1gInSc2
upravena	upravit	k5eAaPmNgFnS
pro	pro	k7c4
běh	běh	k1gInSc4
v	v	k7c6
Adobe	Adobe	kA
Flash	Flash	k1gMnSc1
Player	Player	k1gMnSc1
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
engine	enginout	k5eAaPmIp3nS
byl	být	k5eAaImAgMnS
kromě	kromě	k7c2
her	hra	k1gFnPc2
využit	využít	k5eAaPmNgInS
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
v	v	k7c6
umění	umění	k1gNnSc6
nebo	nebo	k8xC
v	v	k7c6
simulátorech	simulátor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gInSc1
Development	Development	k1gInSc1
Kit	kit	k1gInSc1
(	(	kIx(
<g/>
UDK	UDK	kA
<g/>
)	)	kIx)
</s>
<s>
UDK	UDK	kA
je	být	k5eAaImIp3nS
volně	volně	k6eAd1
šiřitelné	šiřitelný	k2eAgNnSc1d1
vývojové	vývojový	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
map	mapa	k1gFnPc2
<g/>
,	,	kIx,
módů	mód	k1gInPc2
a	a	k8xC
her	hra	k1gFnPc2
v	v	k7c4
Unreal	Unreal	k1gInSc4
Enginu	Engin	k1gInSc2
3	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
poprvé	poprvé	k6eAd1
uvolněno	uvolněn	k2eAgNnSc1d1
v	v	k7c6
listopadu	listopad	k1gInSc6
2009	#num#	k4
s	s	k7c7
každoměsíčními	každoměsíční	k2eAgFnPc7d1
aktualizacemi	aktualizace	k1gFnPc7
<g/>
,	,	kIx,
opravami	oprava	k1gFnPc7
a	a	k8xC
novými	nový	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nekomerční	komerční	k2eNgNnSc4d1
využití	využití	k1gNnSc4
je	být	k5eAaImIp3nS
zdarma	zdarma	k6eAd1
<g/>
,	,	kIx,
avšak	avšak	k8xC
dle	dle	k7c2
licence	licence	k1gFnSc2
se	se	k3xPyFc4
pro	pro	k7c4
komerční	komerční	k2eAgNnSc4d1
využití	využití	k1gNnSc4
musel	muset	k5eAaImAgMnS
zaplatit	zaplatit	k5eAaPmF
Epic	Epic	k1gFnSc4
Games	Gamesa	k1gFnPc2
prvotní	prvotní	k2eAgInSc4d1
poplatek	poplatek	k1gInSc4
99	#num#	k4
USD	USD	kA
a	a	k8xC
pak	pak	k6eAd1
následně	následně	k6eAd1
25	#num#	k4
%	%	kIx~
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
zisku	zisk	k1gInSc2
nad	nad	k7c4
výdělek	výdělek	k1gInSc4
50	#num#	k4
000	#num#	k4
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gMnSc5
4	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
nové	nový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
,	,	kIx,
představené	představený	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
započal	započnout	k5eAaPmAgInS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
novinek	novinka	k1gFnPc2
je	být	k5eAaImIp3nS
globální	globální	k2eAgNnSc4d1
osvětlení	osvětlení	k1gNnSc4
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
za	za	k7c4
použití	použití	k1gNnSc4
technologie	technologie	k1gFnSc2
Cone	Cone	k1gNnSc2
tracing	tracing	k1gInSc1
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
výpočtu	výpočet	k1gInSc2
osvětlení	osvětlení	k1gNnSc2
předem	předem	k6eAd1
při	při	k7c6
načítání	načítání	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnSc7d1
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgFnSc1d2
podpora	podpora	k1gFnSc1
pro	pro	k7c4
vývojáře	vývojář	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mohou	moct	k5eAaImIp3nP
v	v	k7c6
tomto	tento	k3xDgInSc6
enginu	engin	k1gInSc6
implementovat	implementovat	k5eAaImF
kód	kód	k1gInSc4
C	C	kA
<g/>
++	++	k?
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
dokonalejší	dokonalý	k2eAgInSc4d2
debugger	debugger	k1gInSc4
„	„	k?
<g/>
Kismet	kismet	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
vývojářům	vývojář	k1gMnPc3
zároveň	zároveň	k6eAd1
zobrazit	zobrazit	k5eAaPmF
animaci	animace	k1gFnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
při	při	k7c6
testování	testování	k1gNnSc6
kód	kód	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
při	při	k7c6
testování	testování	k1gNnSc6
rovnou	rovnou	k6eAd1
skočit	skočit	k5eAaPmF
do	do	k7c2
editace	editace	k1gFnSc2
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
a	a	k8xC
provést	provést	k5eAaPmF
změny	změna	k1gFnPc4
ve	v	k7c4
Visual	Visual	k1gInSc4
Studiu	studio	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dřívějších	dřívější	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
kód	kód	k1gInSc1
zkompilovat	zkompilovat	k5eAaPmF
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
může	moct	k5eAaImIp3nS
trvat	trvat	k5eAaImF
několik	několik	k4yIc4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pak	pak	k6eAd1
přímo	přímo	k6eAd1
ve	v	k7c6
hře	hra	k1gFnSc6
nebo	nebo	k8xC
aplikaci	aplikace	k1gFnSc6
bylo	být	k5eAaImAgNnS
nutno	nutno	k6eAd1
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
testování	testování	k1gNnSc1
provedeno	provést	k5eAaPmNgNnS
<g/>
,	,	kIx,
pak	pak	k6eAd1
hru	hra	k1gFnSc4
či	či	k8xC
aplikaci	aplikace	k1gFnSc4
vypnout	vypnout	k5eAaPmF
<g/>
,	,	kIx,
provést	provést	k5eAaPmF
změny	změna	k1gFnPc4
a	a	k8xC
proces	proces	k1gInSc4
neustále	neustále	k6eAd1
opakovat	opakovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
toto	tento	k3xDgNnSc1
zdržení	zdržení	k1gNnSc1
zcela	zcela	k6eAd1
odpadá	odpadat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Unreal	Unreal	k1gMnSc1
</s>
<s>
UnrealEd	UnrealEd	k6eAd1
</s>
<s>
UnrealScript	UnrealScript	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.unrealengine.com	www.unrealengine.com	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
seznam	seznam	k1gInSc4
platforem	platforma	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgMnPc7
je	být	k5eAaImIp3nS
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
3	#num#	k4
kompatibilní	kompatibilní	k2eAgMnSc1d1
<g/>
↑	↑	k?
www.geforce.com	www.geforce.com	k1gInSc1
-	-	kIx~
Andre	Andr	k1gInSc5
Burnes	Burnes	k1gInSc4
<g/>
,	,	kIx,
nVidia	nVidium	k1gNnPc4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
<g/>
↑	↑	k?
http://www.unrealengine.com/unreal_engine_4	http://www.unrealengine.com/unreal_engine_4	k4
Archivováno	archivován	k2eAgNnSc4d1
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
nové	nový	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Unreal	Unreal	k1gInSc1
Engine	Engin	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c4
Unreal	Unreal	k1gInSc4
Enginu	Engin	k1gInSc2
</s>
<s>
www.unrealtechnology.com	www.unrealtechnology.com	k1gInSc1
-	-	kIx~
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
o	o	k7c4
Unreal	Unreal	k1gInSc4
Enginu	Engin	k1gInSc2
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
1	#num#	k4
-	-	kIx~
první	první	k4xOgFnSc2
verze	verze	k1gFnSc2
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
2	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
<g/>
X	X	kA
</s>
<s>
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gMnSc5
3	#num#	k4
</s>
<s>
Unreal	Unreal	k1gInSc1
Wiki	Wik	k1gFnSc2
-	-	kIx~
Legacy	Legaca	k1gFnSc2
<g/>
:	:	kIx,
<g/>
Unreal	Unreal	k1gInSc1
Engine	Engin	k1gInSc5
Versions	Versionsa	k1gFnPc2
</s>
<s>
Unreal	Unreal	k1gMnSc1
Development	Development	k1gMnSc1
Kit	kit	k1gInSc4
</s>
