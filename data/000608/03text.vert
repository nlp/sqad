<s>
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Swietla	Swietla	k1gFnSc4	Swietla
ob	ob	k7c4	ob
der	drát	k5eAaImRp2nS	drát
Sasau	Sasaus	k1gInSc3	Sasaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
14	[number]	k4	14
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Sázavy	Sázava	k1gFnSc2	Sázava
a	a	k8xC	a
Sázavky	Sázavka	k1gFnSc2	Sázavka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
15	[number]	k4	15
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
1	[number]	k4	1
582	[number]	k4	582
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
samotné	samotný	k2eAgFnSc3d1	samotná
Světlé	světlý	k2eAgFnSc3d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
1020	[number]	k4	1020
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
zdejší	zdejší	k2eAgFnSc2d1	zdejší
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
sklo	sklo	k1gNnSc1	sklo
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
1207	[number]	k4	1207
První	první	k4xOgFnPc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
Světlé	světlý	k2eAgMnPc4d1	světlý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
náležela	náležet	k5eAaImAgFnS	náležet
Vilémovskému	vilémovský	k2eAgInSc3d1	vilémovský
klášteru	klášter	k1gInSc3	klášter
1343	[number]	k4	1343
Kostel	kostel	k1gInSc1	kostel
světelský	světelský	k2eAgInSc1d1	světelský
povýšen	povýšen	k2eAgInSc1d1	povýšen
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Arnoštem	Arnošt	k1gMnSc7	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
farní	farní	k2eAgFnSc4d1	farní
1363	[number]	k4	1363
Dle	dle	k7c2	dle
spisů	spis	k1gInPc2	spis
kláštera	klášter	k1gInSc2	klášter
jmenován	jmenován	k2eAgMnSc1d1	jmenován
opat	opat	k1gMnSc1	opat
Ondřej	Ondřej	k1gMnSc1	Ondřej
patronem	patron	k1gMnSc7	patron
kostela	kostel	k1gInSc2	kostel
1365	[number]	k4	1365
Světelský	Světelský	k2eAgInSc1d1	Světelský
kostel	kostel	k1gInSc1	kostel
náležel	náležet	k5eAaImAgInS	náležet
k	k	k7c3	k
německobrodskému	německobrodský	k2eAgInSc3d1	německobrodský
děkanátu	děkanát	k1gInSc3	děkanát
1381	[number]	k4	1381
Bratři	bratr	k1gMnPc1	bratr
<g />
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Domoslav	Domoslav	k1gMnSc1	Domoslav
z	z	k7c2	z
Lipničky	Lipnička	k1gFnSc2	Lipnička
věnují	věnovat	k5eAaPmIp3nP	věnovat
světelskému	světelský	k2eAgInSc3d1	světelský
kostelu	kostel	k1gInSc3	kostel
oltář	oltář	k1gInSc4	oltář
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
1385	[number]	k4	1385
Pan	Pan	k1gMnSc1	Pan
Albert	Albert	k1gMnSc1	Albert
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
pánem	pán	k1gMnSc7	pán
na	na	k7c4	na
Světlé	světlý	k2eAgInPc4d1	světlý
1392	[number]	k4	1392
Hrabě	Hrabě	k1gMnSc1	Hrabě
Štěpán	Štěpán	k1gMnSc1	Štěpán
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
tzv.	tzv.	kA	tzv.
Šternberskou	šternberský	k2eAgFnSc4d1	šternberská
část	část	k1gFnSc4	část
zámku	zámek	k1gInSc2	zámek
1417	[number]	k4	1417
Město	město	k1gNnSc1	město
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
připadá	připadat	k5eAaImIp3nS	připadat
opět	opět	k6eAd1	opět
Vilémovskému	vilémovský	k2eAgInSc3d1	vilémovský
klášteru	klášter	k1gInSc3	klášter
1420	[number]	k4	1420
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Světlé	světlý	k2eAgFnSc2d1	světlá
<g/>
,	,	kIx,	,
<g/>
Lipnice	Lipnice	k1gFnSc2	Lipnice
a	a	k8xC	a
Německého	německý	k2eAgInSc2d1	německý
brodu	brod	k1gInSc2	brod
povstala	povstat	k5eAaPmAgFnS	povstat
sekta	sekta	k1gFnSc1	sekta
<g />
.	.	kIx.	.
</s>
<s>
Orebitů	orebita	k1gMnPc2	orebita
1421	[number]	k4	1421
Následkem	následkem	k7c2	následkem
rozboření	rozboření	k1gNnSc2	rozboření
kláštera	klášter	k1gInSc2	klášter
Vilémovského	vilémovský	k2eAgInSc2d1	vilémovský
spadá	spadat	k5eAaImIp3nS	spadat
Světlá	světlý	k2eAgFnSc1d1	světlá
i	i	k8xC	i
panství	panství	k1gNnSc1	panství
v	v	k7c4	v
majetek	majetek	k1gInSc4	majetek
Koruny	koruna	k1gFnSc2	koruna
1423	[number]	k4	1423
Husité	husita	k1gMnPc1	husita
táhnou	táhnout	k5eAaImIp3nP	táhnout
kolem	kolem	k7c2	kolem
Světlé	světlý	k2eAgFnSc2d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
při	při	k7c6	při
výpravě	výprava	k1gFnSc6	výprava
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
1429	[number]	k4	1429
Světlou	světlý	k2eAgFnSc4d1	světlá
i	i	k9	i
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1435	[number]	k4	1435
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
Světlé	světlý	k2eAgFnSc2d1	světlá
<g/>
,	,	kIx,	,
koupil	koupit	k5eAaPmAgMnS	koupit
Lipnici	Lipnice	k1gFnSc4	Lipnice
a	a	k8xC	a
Německý	německý	k2eAgInSc4d1	německý
Brod	Brod	k1gInSc4	Brod
1436	[number]	k4	1436
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
koupil	koupit	k5eAaPmAgMnS	koupit
Habry	habr	k1gInPc4	habr
<g/>
.	.	kIx.	.
1453	[number]	k4	1453
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
3	[number]	k4	3
syny	syn	k1gMnPc7	syn
<g/>
:	:	kIx,	:
Buriana	Burian	k1gMnSc2	Burian
<g/>
,	,	kIx,	,
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
a	a	k8xC	a
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
<g/>
.	.	kIx.	.
</s>
<s>
Světlá	světlat	k5eAaImIp3nS	světlat
i	i	k9	i
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
majetkem	majetek	k1gInSc7	majetek
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Buriana	Burian	k1gMnSc2	Burian
I.	I.	kA	I.
Trčky	Trčka	k1gMnSc2	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1457	[number]	k4	1457
Burian	Burian	k1gMnSc1	Burian
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
na	na	k7c6	na
Světlé	světlý	k2eAgFnSc6d1	světlá
<g/>
,	,	kIx,	,
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
zemským	zemský	k2eAgInPc3d1	zemský
písařem	písař	k1gMnSc7	písař
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
1469	[number]	k4	1469
Burian	Burian	k1gMnSc1	Burian
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
odkázal	odkázat	k5eAaPmAgInS	odkázat
Světlou	světlý	k2eAgFnSc4d1	světlá
i	i	k9	i
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
svým	svůj	k3xOyFgInSc7	svůj
synům	syn	k1gMnPc3	syn
Mikolášovi	Mikoláš	k1gMnSc6	Mikoláš
a	a	k8xC	a
Melchisedekovi	Melchisedeek	k1gMnSc6	Melchisedeek
Trčkovi	Trčka	k1gMnSc6	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1497	[number]	k4	1497
Mikuláš	mikuláš	k1gInSc1	mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
samostatným	samostatný	k2eAgMnSc7d1	samostatný
pánem	pán	k1gMnSc7	pán
na	na	k7c4	na
Světlé	světlý	k2eAgInPc4d1	světlý
1506	[number]	k4	1506
Prostřední	prostřední	k2eAgInSc1d1	prostřední
zvon	zvon	k1gInSc1	zvon
světelského	světelský	k2eAgInSc2d1	světelský
kostela	kostel	k1gInSc2	kostel
darován	darován	k2eAgInSc1d1	darován
tomuto	tento	k3xDgInSc3	tento
Mikulášem	Mikuláš	k1gMnSc7	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trčkou	Trčka	k1gMnSc7	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1509	[number]	k4	1509
Mikuláš	mikuláš	k1gInSc1	mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
následoval	následovat	k5eAaImAgMnS	následovat
syn	syn	k1gMnSc1	syn
jeho	on	k3xPp3gInSc4	on
Mikuláš	mikuláš	k1gInSc4	mikuláš
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
jako	jako	k8xC	jako
pán	pán	k1gMnSc1	pán
na	na	k7c4	na
Světlé	světlý	k2eAgFnPc4d1	světlá
1515	[number]	k4	1515
Mikuláš	mikuláš	k1gInSc4	mikuláš
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
Světlou	světlý	k2eAgFnSc4d1	světlá
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
panstvími	panství	k1gNnPc7	panství
synu	syn	k1gMnSc3	syn
svému	svůj	k3xOyFgMnSc3	svůj
Buriánu	Burián	k1gMnSc3	Burián
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trčkovi	Trčkův	k2eAgMnPc1d1	Trčkův
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1522	[number]	k4	1522
Burián	Burián	k1gMnSc1	Burián
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
lipnickém	lipnický	k2eAgInSc6d1	lipnický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Světlé	světlý	k2eAgFnSc2d1	světlá
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1525	[number]	k4	1525
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
ze	z	k7c2	z
Šellenberku	Šellenberk	k1gInSc2	Šellenberk
1540	[number]	k4	1540
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Světlé	světlý	k2eAgFnSc2d1	světlá
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Burian	Burian	k1gMnSc1	Burian
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1567	[number]	k4	1567
Tak	tak	k9	tak
zvané	zvaný	k2eAgFnPc1d1	zvaná
Trčkovské	Trčkovský	k2eAgFnPc1d1	Trčkovská
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc1	oddělení
světelského	světelský	k2eAgInSc2d1	světelský
zámku	zámek	k1gInSc2	zámek
postaveno	postaven	k2eAgNnSc1d1	postaveno
a	a	k8xC	a
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
jako	jako	k8xC	jako
lovecký	lovecký	k2eAgInSc1d1	lovecký
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Burian	Burian	k1gMnSc1	Burian
udělil	udělit	k5eAaPmAgMnS	udělit
měšťanům	měšťan	k1gMnPc3	měšťan
právo	právo	k1gNnSc4	právo
pečetit	pečetit	k5eAaImF	pečetit
červený	červený	k2eAgInSc4d1	červený
voskem	vosk	k1gInSc7	vosk
1567	[number]	k4	1567
a	a	k8xC	a
daroval	darovat	k5eAaPmAgMnS	darovat
jim	on	k3xPp3gMnPc3	on
pozemky	pozemek	k1gInPc4	pozemek
s	s	k7c7	s
lesem	les	k1gInSc7	les
u	u	k7c2	u
Lán	lán	k1gInSc4	lán
<g/>
,	,	kIx,	,
důchod	důchod	k1gInSc4	důchod
z	z	k7c2	z
jarmarečného	jarmarečný	k2eAgNnSc2d1	jarmarečný
<g/>
,	,	kIx,	,
z	z	k7c2	z
koňských	koňský	k2eAgInPc2d1	koňský
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
z	z	k7c2	z
obecní	obecní	k2eAgFnSc2d1	obecní
váhy	váha	k1gFnSc2	váha
a	a	k8xC	a
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
1569	[number]	k4	1569
Velký	velký	k2eAgInSc1d1	velký
zvon	zvon	k1gInSc1	zvon
na	na	k7c6	na
kostele	kostel	k1gInSc6	kostel
Světelském	Světelský	k2eAgInSc6d1	Světelský
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
Buriana	Burian	k1gMnSc2	Burian
Trčky	Trčka	k1gMnSc2	Trčka
kostelu	kostel	k1gInSc3	kostel
tomuto	tento	k3xDgInSc3	tento
darován	darovat	k5eAaPmNgInS	darovat
<g/>
.	.	kIx.	.
1577	[number]	k4	1577
Burian	Burian	k1gMnSc1	Burian
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
koupil	koupit	k5eAaPmAgMnS	koupit
Novou	nový	k2eAgFnSc4d1	nová
Ves	ves	k1gFnSc4	ves
a	a	k8xC	a
připojil	připojit	k5eAaPmAgInS	připojit
ji	on	k3xPp3gFnSc4	on
ku	k	k7c3	k
Světlé	světlý	k2eAgFnPc1d1	světlá
1578	[number]	k4	1578
Burian	Burian	k1gMnSc1	Burian
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
založil	založit	k5eAaPmAgMnS	založit
ve	v	k7c4	v
Světlé	světlý	k2eAgInPc4d1	světlý
špitál	špitál	k1gInSc4	špitál
pro	pro	k7c4	pro
5	[number]	k4	5
chudých	chudý	k1gMnPc2	chudý
měšťanův	měšťanův	k2eAgMnSc1d1	měšťanův
a	a	k8xC	a
současně	současně	k6eAd1	současně
založil	založit	k5eAaPmAgMnS	založit
zde	zde	k6eAd1	zde
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
1591	[number]	k4	1591
Burian	Burian	k1gMnSc1	Burian
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Trčka	Trčka	k1gMnSc1	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
Pán	pán	k1gMnSc1	pán
na	na	k7c4	na
Světlé	světlý	k2eAgNnSc4d1	světlé
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
odkázal	odkázat	k5eAaPmAgInS	odkázat
majetek	majetek	k1gInSc1	majetek
svůj	svůj	k3xOyFgInSc4	svůj
svému	svůj	k3xOyFgMnSc3	svůj
synu	syn	k1gMnSc3	syn
Janu	Jan	k1gMnSc3	Jan
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
<g/>
,,	,,	k?	,,
svobodnému	svobodný	k2eAgMnSc3d1	svobodný
pánu	pán	k1gMnSc3	pán
Trčkovi	Trčka	k1gMnSc3	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
1594	[number]	k4	1594
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
jeho	jeho	k3xOp3gNnSc4	jeho
bratr	bratr	k1gMnSc1	bratr
Maximilián	Maximilián	k1gMnSc1	Maximilián
<g/>
,	,	kIx,	,
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
1604	[number]	k4	1604
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g />
.	.	kIx.	.
</s>
<s>
<g/>
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgMnSc1d1	císařský
rada	rada	k1gMnSc1	rada
<g/>
,	,	kIx,	,
komoří	komoří	k1gMnSc1	komoří
<g/>
,	,	kIx,	,
zemský	zemský	k2eAgMnSc1d1	zemský
sudí	sudí	k1gMnSc1	sudí
a	a	k8xC	a
místodržící	místodržící	k1gMnSc1	místodržící
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
převzal	převzít	k5eAaPmAgMnS	převzít
Světlou	světlý	k2eAgFnSc4d1	světlá
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Maximiliana	Maximilian	k1gMnSc4	Maximilian
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
jako	jako	k9	jako
pán	pán	k1gMnSc1	pán
na	na	k7c4	na
Světlé	světlý	k2eAgMnPc4d1	světlý
<g/>
,	,	kIx,	,
Lipnici	Lipnice	k1gFnSc4	Lipnice
<g/>
,	,	kIx,	,
Ledči	Ledeč	k1gFnSc6	Ledeč
<g/>
,	,	kIx,	,
Opočně	Opočno	k1gNnSc6	Opočno
<g/>
,	,	kIx,	,
Smiřicích	Smiřice	k1gFnPc6	Smiřice
<g/>
,	,	kIx,	,
Žlebech	žleb	k1gInPc6	žleb
a	a	k8xC	a
České	český	k2eAgInPc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
Kamenici	Kamenice	k1gFnSc3	Kamenice
<g/>
,	,	kIx,	,
Týž	týž	k3xTgMnSc1	týž
daroval	darovat	k5eAaPmAgMnS	darovat
městu	město	k1gNnSc3	město
58	[number]	k4	58
jiter	jitro	k1gNnPc2	jitro
1283	[number]	k4	1283
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
□	□	k?	□
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
obsahu	obsah	k1gInSc6	obsah
privilegie	privilegie	k1gFnSc2	privilegie
města	město	k1gNnSc2	město
Světlé	světlý	k2eAgNnSc1d1	světlé
<g/>
.	.	kIx.	.
1610	[number]	k4	1610
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
pánů	pan	k1gMnPc2	pan
Českých	český	k2eAgMnPc2d1	český
1629	[number]	k4	1629
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
Světlé	světlý	k2eAgNnSc4d1	světlé
<g/>
,	,	kIx,	,
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
říšským	říšský	k2eAgMnSc7d1	říšský
hrabětem	hrabě	k1gMnSc7	hrabě
1631	[number]	k4	1631
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
na	na	k7c6	na
Světlé	světlý	k2eAgFnSc6d1	světlá
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
Českým	český	k2eAgMnSc7d1	český
hrabětem	hrabě	k1gMnSc7	hrabě
1633	[number]	k4	1633
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Trčků	Trčka	k1gMnPc2	Trčka
zprostil	zprostit	k5eAaPmAgInS	zprostit
Světlou	světlý	k2eAgFnSc4d1	světlá
roboty	robota	k1gFnPc4	robota
a	a	k8xC	a
daroval	darovat	k5eAaPmAgMnS	darovat
městu	město	k1gNnSc3	město
některá	některý	k3yIgNnPc1	některý
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
1634	[number]	k4	1634
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manželky	manželka	k1gFnSc2	manželka
své	své	k1gNnSc4	své
a	a	k8xC	a
po	po	k7c6	po
<g />
.	.	kIx.	.
</s>
<s>
pádu	pád	k1gInSc3	pád
syna	syn	k1gMnSc4	syn
svého	svůj	k3xOyFgMnSc4	svůj
Adama	Adam	k1gMnSc4	Adam
Erdmana	Erdman	k1gMnSc4	Erdman
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
odkázal	odkázat	k5eAaPmAgMnS	odkázat
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Trčka	Trčka	k1gMnSc1	Trčka
kšaftem	kšaft	k1gInSc7	kšaft
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Světlou	světlý	k2eAgFnSc7d1	světlá
s	s	k7c7	s
Lipnicí	Lipnice	k1gFnSc7	Lipnice
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
Čáslavské	Čáslavské	k2eAgFnSc1d1	Čáslavské
<g/>
,	,	kIx,	,
Studenec	Studenec	k1gInSc1	Studenec
<g/>
,	,	kIx,	,
Větrný	větrný	k2eAgInSc1d1	větrný
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
a	a	k8xC	a
Žleby	žleb	k1gInPc1	žleb
-	-	kIx~	-
Petrovi	Petr	k1gMnSc3	Petr
Vokovi	Voka	k1gMnSc3	Voka
Švihovskému	Švihovský	k2eAgMnSc3d1	Švihovský
z	z	k7c2	z
Ryzmberku	Ryzmberk	k1gInSc6	Ryzmberk
<g/>
,	,	kIx,	,
Ladislavovi	Ladislav	k1gMnSc3	Ladislav
Burianovi	Burian	k1gMnSc3	Burian
hraběti	hrabě	k1gMnSc3	hrabě
z	z	k7c2	z
Wadsteina	Wadsteino	k1gNnSc2	Wadsteino
a	a	k8xC	a
Matyašovi	Matyašův	k2eAgMnPc1d1	Matyašův
<g />
.	.	kIx.	.
</s>
<s>
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
hraběti	hrabě	k1gMnSc3	hrabě
Berkovi	Berka	k1gMnSc3	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
od	od	k7c2	od
statku	statek	k1gInSc2	statek
světelského	světelský	k2eAgInSc2d1	světelský
a	a	k8xC	a
ledečského	ledečský	k2eAgInSc2d1	ledečský
odkázal	odkázat	k5eAaPmAgInS	odkázat
Trčka	Trčka	k1gMnSc1	Trčka
důchody	důchod	k1gInPc4	důchod
z	z	k7c2	z
Hamrů	Hamry	k1gInPc2	Hamry
železných	železný	k2eAgInPc2d1	železný
kostelu	kostel	k1gInSc3	kostel
světelskému	světelský	k2eAgInSc3d1	světelský
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
rodiče	rodič	k1gMnPc1	rodič
jeho	jeho	k3xOp3gInPc2	jeho
<g/>
,	,	kIx,	,
bratří	bratr	k1gMnPc2	bratr
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
příbuzní	příbuzný	k1gMnPc1	příbuzný
odpočívali	odpočívat	k5eAaImAgMnP	odpočívat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
odpočívati	odpočívat	k5eAaImF	odpočívat
žádostiv	žádostiv	k2eAgInSc4d1	žádostiv
byl	být	k5eAaImAgInS	být
<g/>
)	)	kIx)	)
1635	[number]	k4	1635
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Trčka	Trčka	k1gMnSc1	Trčka
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	své	k1gNnSc4	své
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
pro	pro	k7c4	pro
rozličná	rozličný	k2eAgNnPc4d1	rozličné
provinění	provinění	k1gNnPc4	provinění
veliká	veliký	k2eAgNnPc4d1	veliké
proti	proti	k7c3	proti
císaři	císař	k1gMnPc1	císař
spáchaná	spáchaný	k2eAgNnPc4d1	spáchané
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
statky	statek	k1gInPc1	statek
vládě	vláda	k1gFnSc3	vláda
(	(	kIx(	(
<g/>
fisku	fiskus	k1gInSc3	fiskus
<g/>
)	)	kIx)	)
připadly	připadnout	k5eAaPmAgFnP	připadnout
<g/>
.	.	kIx.	.
1636	[number]	k4	1636
Panství	panství	k1gNnPc2	panství
světelské	světelský	k2eAgFnSc2d1	světelská
bylo	být	k5eAaImAgNnS	být
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
253.452	[number]	k4	253.452
zl	zl	k?	zl
<g/>
.	.	kIx.	.
</s>
<s>
Šejn	Šejn	k1gMnSc1	Šejn
<g/>
.	.	kIx.	.
</s>
<s>
Odhadnuto	odhadnut	k2eAgNnSc1d1	odhadnuto
a	a	k8xC	a
hraběti	hrabě	k1gMnSc6	hrabě
Ladislavovi	Ladislav	k1gMnSc6	Ladislav
Burianovi	Burian	k1gMnSc6	Burian
z	z	k7c2	z
Waldšteina	Waldšteino	k1gNnSc2	Waldšteino
proti	proti	k7c3	proti
zapravení	zapravení	k1gNnSc3	zapravení
dluhu	dluh	k1gInSc2	dluh
p.	p.	k?	p.
60.000	[number]	k4	60.000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dědičům	dědič	k1gMnPc3	dědič
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
knížeti	kníže	k1gNnSc6wR	kníže
Donu	Don	k1gInSc2	Don
Aldobrandini	Aldobrandin	k2eAgMnPc1d1	Aldobrandin
<g/>
,	,	kIx,	,
p.	p.	k?	p.
100.000	[number]	k4	100.000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Adamovi	Adam	k1gMnSc6	Adam
hraběti	hrabě	k1gMnSc6	hrabě
z	z	k7c2	z
Papenheimu	Papenheim	k1gInSc2	Papenheim
<g/>
,	,	kIx,	,
p.	p.	k?	p.
20.000	[number]	k4	20.000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
</s>
<s>
Brunovi	Bruna	k1gMnSc3	Bruna
hraběti	hrabě	k1gMnSc3	hrabě
z	z	k7c2	z
Mansfeldu	Mansfeld	k1gInSc2	Mansfeld
<g/>
,	,	kIx,	,
úhrnem	úhrnem	k6eAd1	úhrnem
za	za	k7c4	za
230.000	[number]	k4	230.000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
přenecháno	přenechat	k5eAaPmNgNnS	přenechat
<g/>
.	.	kIx.	.
1639	[number]	k4	1639
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1639	[number]	k4	1639
nájezdy	nájezd	k1gInPc7	nájezd
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
objevili	objevit	k5eAaPmAgMnP	objevit
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
16	[number]	k4	16
Banérových	Banérův	k2eAgInPc2d1	Banérův
oddílů	oddíl	k1gInPc2	oddíl
přepadlo	přepadnout	k5eAaPmAgNnS	přepadnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Lipnici	Lipnice	k1gFnSc4	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Zničili	zničit	k5eAaPmAgMnP	zničit
městské	městský	k2eAgNnSc4d1	Městské
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
,	,	kIx,	,
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
městečko	městečko	k1gNnSc4	městečko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hrad	hrad	k1gInSc4	hrad
nedobyli	dobýt	k5eNaPmAgMnP	dobýt
<g/>
.	.	kIx.	.
1645	[number]	k4	1645
Špitál	špitál	k1gInSc1	špitál
světelský	světelský	k2eAgInSc1d1	světelský
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
tento	tento	k3xDgInSc1	tento
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
jest	být	k5eAaImIp3nS	být
dům	dům	k1gInSc4	dům
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
,	,	kIx,	,
neodvracej	odvracet	k5eNaImRp2nS	odvracet
tváře	tvář	k1gFnPc1	tvář
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
páně	páně	k2eAgNnPc1d1	páně
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Krista	Kristus	k1gMnSc2	Kristus
Pána	pán	k1gMnSc2	pán
1645	[number]	k4	1645
1648	[number]	k4	1648
Švédové	Švéd	k1gMnPc1	Švéd
opustili	opustit	k5eAaPmAgMnP	opustit
Lipnici	Lipnice	k1gFnSc4	Lipnice
a	a	k8xC	a
náš	náš	k3xOp1gInSc4	náš
kraj	kraj	k1gInSc4	kraj
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
veškerou	veškerý	k3xTgFnSc4	veškerý
nashromážděnou	nashromážděný	k2eAgFnSc4d1	nashromážděná
kořist	kořist	k1gFnSc4	kořist
odvezli	odvézt	k5eAaPmAgMnP	odvézt
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
1667	[number]	k4	1667
Podíly	podíl	k1gInPc4	podíl
na	na	k7c6	na
Světlé	světlý	k2eAgFnSc6d1	světlá
a	a	k8xC	a
světelském	světelský	k2eAgNnSc6d1	světelské
panství	panství	k1gNnSc6	panství
připadnou	připadnout	k5eAaPmIp3nP	připadnout
pí	pí	k1gNnSc4	pí
<g/>
.	.	kIx.	.
</s>
<s>
Marii	Maria	k1gFnSc3	Maria
Huscheine	Huschein	k1gMnSc5	Huschein
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
baronce	baronka	k1gFnSc3	baronka
Geran	Gerana	k1gFnPc2	Gerana
<g/>
,	,	kIx,	,
Theodorovi	Theodor	k1gMnSc6	Theodor
Mültzerovi	Mültzer	k1gMnSc6	Mültzer
z	z	k7c2	z
Rosenthalu	Rosenthal	k1gInSc2	Rosenthal
a	a	k8xC	a
baronu	baron	k1gMnSc3	baron
Vernierovi	Vernier	k1gMnSc3	Vernier
<g/>
;	;	kIx,	;
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
vládne	vládnout	k5eAaImIp3nS	vládnout
posaváde	posaváde	k?	posaváde
hrabě	hrabě	k1gMnSc1	hrabě
Burian	Burian	k1gMnSc1	Burian
z	z	k7c2	z
Waldstejnu	Waldstejn	k1gInSc2	Waldstejn
<g/>
.	.	kIx.	.
1672	[number]	k4	1672
Panství	panství	k1gNnSc2	panství
světelské	světelský	k2eAgNnSc1d1	světelské
i	i	k8xC	i
s	s	k7c7	s
městem	město	k1gNnSc7	město
Světlou	světlý	k2eAgFnSc4d1	světlá
koupil	koupit	k5eAaPmAgMnS	koupit
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rabatta	Rabatta	k1gMnSc1	Rabatta
<g/>
,	,	kIx,	,
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Dornberku	Dornberk	k1gInSc2	Dornberk
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
na	na	k7c6	na
Světlé	světlý	k2eAgFnSc6d1	světlá
a	a	k8xC	a
Tisu	tis	k1gInSc2	tis
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
pluku	pluk	k1gInSc2	pluk
kyrysníků	kyrysník	k1gMnPc2	kyrysník
a	a	k8xC	a
generalvachtmistr	generalvachtmistr	k1gMnSc1	generalvachtmistr
<g/>
.	.	kIx.	.
1679	[number]	k4	1679
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
hrabě	hrabě	k1gMnSc1	hrabě
Rabatta	Rabatta	k1gMnSc1	Rabatta
prodal	prodat	k5eAaPmAgMnS	prodat
Světlou	světlý	k2eAgFnSc4d1	světlá
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
i	i	k8xC	i
se	s	k7c7	s
statkem	statek	k1gInSc7	statek
Vrbicí	vrbice	k1gFnSc7	vrbice
Janovi	Jan	k1gMnSc3	Jan
Kašparu	Kašpar	k1gMnSc3	Kašpar
<g/>
,	,	kIx,	,
svobodnému	svobodný	k2eAgMnSc3d1	svobodný
pánu	pán	k1gMnSc3	pán
z	z	k7c2	z
Montány	Montán	k2eAgFnPc1d1	Montána
1686	[number]	k4	1686
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roku	rok	k1gInSc6	rok
koupil	koupit	k5eAaPmAgInS	koupit
Světlou	světlý	k2eAgFnSc4d1	světlá
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
Jan	Jan	k1gMnSc1	Jan
Bartolemeus	Bartolemeus	k1gMnSc1	Bartolemeus
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Vernierů	vernier	k1gInPc2	vernier
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
choť	choť	k1gFnSc1	choť
Marie	Maria	k1gFnSc2	Maria
Rosalie	Rosalie	k1gFnSc2	Rosalie
1704	[number]	k4	1704
Světlou	světlý	k2eAgFnSc4d1	světlá
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
koupil	koupit	k5eAaPmAgMnS	koupit
hrabě	hrabě	k1gMnSc1	hrabě
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Pöttingu	Pötting	k1gInSc2	Pötting
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
na	na	k7c6	na
Lipnici	Lipnice	k1gFnSc6	Lipnice
a	a	k8xC	a
Habrech	habr	k1gInPc6	habr
1714	[number]	k4	1714
Největší	veliký	k2eAgFnSc4d3	veliký
povodeň	povodeň	k1gFnSc4	povodeň
na	na	k7c6	na
Sázavě	Sázava	k1gFnSc6	Sázava
1722	[number]	k4	1722
Město	město	k1gNnSc1	město
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
Světlou	světlý	k2eAgFnSc4d1	světlá
koupil	koupit	k5eAaPmAgMnS	koupit
hrabě	hrabě	k1gMnSc1	hrabě
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Černín	Černín	k1gMnSc1	Černín
1737	[number]	k4	1737
Zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
světelského	světelský	k2eAgInSc2d1	světelský
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
strana	strana	k1gFnSc1	strana
východní	východní	k2eAgFnSc1d1	východní
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
hrabětem	hrabě	k1gMnSc7	hrabě
Černínem	Černín	k1gMnSc7	Černín
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1743	[number]	k4	1743
Hrabě	Hrabě	k1gMnSc1	Hrabě
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
z	z	k7c2	z
Černínů	Černín	k1gMnPc2	Černín
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
města	město	k1gNnSc2	město
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
Světlé	světlý	k2eAgNnSc1d1	světlé
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následovala	následovat	k5eAaImAgFnS	následovat
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
panství	panství	k1gNnSc2	panství
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
Antonie	Antonie	k1gFnSc1	Antonie
z	z	k7c2	z
Černínů	Černín	k1gMnPc2	Černín
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Kumburku	Kumburk	k1gInSc2	Kumburk
<g/>
,	,	kIx,	,
paní	paní	k1gFnSc1	paní
na	na	k7c6	na
Drhovli	Drhovli	k1gFnSc6	Drhovli
<g/>
,	,	kIx,	,
Čížově	Čížov	k1gInSc6	Čížov
a	a	k8xC	a
Sedleci	Sedleec	k1gInSc6	Sedleec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1748	[number]	k4	1748
Město	město	k1gNnSc1	město
i	i	k8xC	i
panství	panství	k1gNnSc1	panství
Světlá	světlat	k5eAaImIp3nS	světlat
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c4	v
Majetek	majetek	k1gInSc4	majetek
Filipa	Filip	k1gMnSc2	Filip
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
Kolovrat-	Kolovrat-	k1gMnSc2	Kolovrat-
Krakovského	krakovský	k2eAgMnSc2d1	krakovský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
choti	choť	k1gFnSc2	choť
Barbory	Barbora	k1gFnSc2	Barbora
hraběnky	hraběnka	k1gFnSc2	hraběnka
z	z	k7c2	z
Kolovratů	kolovrat	k1gInPc2	kolovrat
1758	[number]	k4	1758
Farní	farní	k2eAgInSc4d1	farní
kostel	kostel	k1gInSc4	kostel
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
děkanský	děkanský	k2eAgInSc4d1	děkanský
chrám	chrám	k1gInSc4	chrám
1760	[number]	k4	1760
Hrabě	Hrabě	k1gMnSc1	Hrabě
Leopold	Leopold	k1gMnSc1	Leopold
Kolovrat-Krakovský	Kolovrat-Krakovský	k2eAgMnSc1d1	Kolovrat-Krakovský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
zemřelého	zemřelý	k2eAgMnSc2d1	zemřelý
hraběte	hrabě	k1gMnSc2	hrabě
Filipa	Filip	k1gMnSc2	Filip
Kolovrata	Kolovrat	k1gMnSc2	Kolovrat
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
<g/>
,	,	kIx,	,
byv	být	k5eAaPmDgInS	být
za	za	k7c4	za
plnoletého	plnoletý	k2eAgInSc2d1	plnoletý
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
<g/>
,	,	kIx,	,
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
panství	panství	k1gNnPc2	panství
světelské	světelský	k2eAgFnSc2d1	světelská
a	a	k8xC	a
statek	statek	k1gInSc1	statek
Vrbici	vrbice	k1gFnSc3	vrbice
<g/>
.	.	kIx.	.
1765	[number]	k4	1765
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roku	rok	k1gInSc6	rok
byly	být	k5eAaImAgFnP	být
varhany	varhany	k1gFnPc1	varhany
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
světelském	světelský	k2eAgInSc6d1	světelský
kutnohorským	kutnohorský	k2eAgInPc3d1	kutnohorský
varhanářem	varhanář	k1gMnSc7	varhanář
J.	J.	kA	J.
Horákem	Horák	k1gMnSc7	Horák
za	za	k7c4	za
kupní	kupní	k2eAgFnSc4d1	kupní
cenu	cena	k1gFnSc4	cena
pr	pr	k0	pr
<g/>
.	.	kIx.	.
400	[number]	k4	400
zl	zl	k?	zl
<g/>
.	.	kIx.	.
</s>
<s>
Postaveny	postaven	k2eAgFnPc1d1	postavena
1773	[number]	k4	1773
Hrabě	Hrabě	k1gMnSc4	Hrabě
Kolovrat	kolovrat	k1gInSc1	kolovrat
odevzdal	odevzdat	k5eAaPmAgInS	odevzdat
panské	panský	k2eAgFnPc4d1	Panská
brusírny	brusírna	k1gFnPc4	brusírna
na	na	k7c4	na
granáty	granát	k1gInPc4	granát
brusičům	brusič	k1gMnPc3	brusič
k	k	k7c3	k
svobodnému	svobodný	k2eAgNnSc3d1	svobodné
používání	používání	k1gNnSc3	používání
<g/>
,	,	kIx,	,
a	a	k8xC	a
ponechal	ponechat	k5eAaPmAgMnS	ponechat
jim	on	k3xPp3gMnPc3	on
zároveň	zároveň	k6eAd1	zároveň
kapitál	kapitál	k1gInSc1	kapitál
pr	pr	k0	pr
<g/>
.	.	kIx.	.
6000	[number]	k4	6000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
k	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
.	.	kIx.	.
1774	[number]	k4	1774
Do	do	k7c2	do
Světlé	světlý	k2eAgFnSc2d1	světlá
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
tohoto	tento	k3xDgInSc2	tento
první	první	k4xOgFnPc1	první
brambory	brambora	k1gFnPc1	brambora
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
a	a	k8xC	a
zde	zde	k6eAd1	zde
sázeny	sázen	k2eAgInPc4d1	sázen
1775	[number]	k4	1775
Jako	jako	k8xS	jako
všude	všude	k6eAd1	všude
,	,	kIx,	,
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
i	i	k9	i
zde	zde	k6eAd1	zde
nepokoje	nepokoj	k1gInPc4	nepokoj
<g />
.	.	kIx.	.
</s>
<s>
mezi	mezi	k7c4	mezi
sedláky	sedlák	k1gInPc4	sedlák
za	za	k7c7	za
příčinou	příčina	k1gFnSc7	příčina
robotní	robotní	k2eAgFnSc2d1	robotní
povinnosti	povinnost	k1gFnSc2	povinnost
1782	[number]	k4	1782
Dolování	dolování	k1gNnSc2	dolování
na	na	k7c4	na
stříbro	stříbro	k1gNnSc4	stříbro
u	u	k7c2	u
Dlužin	Dlužina	k1gFnPc2	Dlužina
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Dvora	Dvůr	k1gInSc2	Dvůr
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
hrabětem	hrabě	k1gMnSc7	hrabě
Kolovratem	kolovrat	k1gInSc7	kolovrat
a	a	k8xC	a
sice	sice	k8xC	sice
počalo	počnout	k5eAaPmAgNnS	počnout
pracovati	pracovat	k5eAaImF	pracovat
11	[number]	k4	11
horníků	horník	k1gMnPc2	horník
a	a	k8xC	a
důl	důl	k1gInSc1	důl
nazván	nazván	k2eAgInSc1d1	nazván
jest	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Štolou	štola	k1gFnSc7	štola
Leopoldovou	Leopoldová	k1gFnSc7	Leopoldová
<g/>
"	"	kIx"	"
1786	[number]	k4	1786
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roku	rok	k1gInSc6	rok
stávalo	stávat	k5eAaImAgNnS	stávat
již	již	k6eAd1	již
10	[number]	k4	10
brusíren	brusírna	k1gFnPc2	brusírna
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
ve	v	k7c6	v
Světlé	světlý	k2eAgFnSc6d1	světlá
a	a	k8xC	a
Březince	březinka	k1gFnSc6	březinka
1791	[number]	k4	1791
<g />
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
Leopold	Leopold	k1gMnSc1	Leopold
Kolovrat-Krakovský	Kolovrat-Krakovský	k2eAgMnSc1d1	Kolovrat-Krakovský
nařídil	nařídit	k5eAaPmAgMnS	nařídit
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
světelští	světelský	k2eAgMnPc1d1	světelský
úředníci	úředník	k1gMnPc1	úředník
a	a	k8xC	a
služebníci	služebník	k1gMnPc1	služebník
musí	muset	k5eAaImIp3nP	muset
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
za	za	k7c7	za
příčinou	příčina	k1gFnSc7	příčina
naučení	naučení	k1gNnSc2	naučení
se	se	k3xPyFc4	se
německé	německý	k2eAgFnPc1d1	německá
řeči	řeč	k1gFnPc1	řeč
poslati	poslat	k5eAaPmF	poslat
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
1795	[number]	k4	1795
Světelská	Světelský	k2eAgFnSc1d1	Světelská
radnice	radnice	k1gFnSc1	radnice
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
1792	[number]	k4	1792
V	v	k7c6	v
lese	les	k1gInSc6	les
u	u	k7c2	u
Rosinova	Rosinův	k2eAgFnSc1d1	Rosinův
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
obora	obora	k1gFnSc1	obora
pro	pro	k7c4	pro
černou	černý	k2eAgFnSc4d1	černá
zvěř	zvěř	k1gFnSc4	zvěř
1796	[number]	k4	1796
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roku	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
tak	tak	k8xC	tak
zvaná	zvaný	k2eAgFnSc1d1	zvaná
granátnická	granátnický	k2eAgFnSc1d1	granátnická
společnost	společnost	k1gFnSc1	společnost
založena	založen	k2eAgFnSc1d1	založena
1800	[number]	k4	1800
Dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
hřbitov	hřbitov	k1gInSc1	hřbitov
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
světelského	světelský	k2eAgInSc2d1	světelský
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
založen	založen	k2eAgInSc1d1	založen
za	za	k7c4	za
Světlou	světlý	k2eAgFnSc4d1	světlá
u	u	k7c2	u
Březinky	březinka	k1gFnSc2	březinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roku	rok	k1gInSc6	rok
udělena	udělen	k2eAgFnSc1d1	udělena
byla	být	k5eAaImAgFnS	být
ve	v	k7c4	v
Světlé	světlý	k2eAgFnPc4d1	světlá
první	první	k4xOgFnPc4	první
koncese	koncese	k1gFnPc4	koncese
ku	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
úplného	úplný	k2eAgInSc2d1	úplný
obchodu	obchod	k1gInSc2	obchod
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
Markusovi	Markus	k1gMnSc3	Markus
Seewaldovi	Seewaldův	k2eAgMnPc1d1	Seewaldův
1805	[number]	k4	1805
Následkem	následkem	k7c2	následkem
srážky	srážka	k1gFnSc2	srážka
mezi	mezi	k7c7	mezi
vojskem	vojsko	k1gNnSc7	vojsko
Bavorským	bavorský	k2eAgNnSc7d1	bavorské
a	a	k8xC	a
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
kyrysníky	kyrysník	k1gMnPc7	kyrysník
u	u	k7c2	u
Habrů	habr	k1gInPc2	habr
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
velmi	velmi	k6eAd1	velmi
i	i	k9	i
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
světelské	světelský	k2eAgFnSc6d1	světelská
1809	[number]	k4	1809
Zřízena	zřízen	k2eAgFnSc1d1	zřízena
vojenská	vojenský	k2eAgFnSc1d1	vojenská
nemocnice	nemocnice	k1gFnSc1	nemocnice
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
zámku	zámek	k1gInSc6	zámek
1810	[number]	k4	1810
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
hrabě	hrabě	k1gMnSc1	hrabě
Kolovrat-Krakovský	Kolovrat-Krakovský	k2eAgMnSc1d1	Kolovrat-Krakovský
<g/>
.	.	kIx.	.
1813	[number]	k4	1813
Silnice	silnice	k1gFnSc1	silnice
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
Světlé	světlý	k2eAgFnSc2d1	světlá
do	do	k7c2	do
Humpolce	Humpolec	k1gInSc2	Humpolec
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
1817	[number]	k4	1817
Město	město	k1gNnSc1	město
Světlá	světlat	k5eAaImIp3nS	světlat
a	a	k8xC	a
velkostatek	velkostatek	k1gInSc4	velkostatek
světelský	světelský	k2eAgInSc4d1	světelský
přešly	přejít	k5eAaPmAgInP	přejít
vyrovnáním	vyrovnání	k1gNnSc7	vyrovnání
ze	z	k7c2	z
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
Zichy	Zich	k1gMnPc4	Zich
z	z	k7c2	z
Vaszoniköe	Vaszonikö	k1gInSc2	Vaszonikö
1821	[number]	k4	1821
Walpurga	Walpurga	k1gFnSc1	Walpurga
starohraběnka	starohraběnka	k1gFnSc1	starohraběnka
ze	z	k7c2	z
Salm-Reifferscheidtů	Salm-Reifferscheidta	k1gMnPc2	Salm-Reifferscheidta
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
výměnou	výměna	k1gFnSc7	výměna
svého	svůj	k3xOyFgNnSc2	svůj
panství	panství	k1gNnSc2	panství
Dioszegh	Dioszegha	k1gFnPc2	Dioszegha
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
za	za	k7c7	za
Světlou	světlý	k2eAgFnSc7d1	světlá
<g/>
,	,	kIx,	,
majitelkou	majitelka	k1gFnSc7	majitelka
panství	panství	k1gNnSc2	panství
i	i	k8xC	i
města	město	k1gNnSc2	město
Světlé	světlý	k2eAgFnSc2d1	světlá
1822	[number]	k4	1822
Jan	Jan	k1gMnSc1	Jan
starohrabě	starohrabě	k1gMnSc1	starohrabě
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
Salm-	Salm-	k1gFnSc2	Salm-
Reifferscheidtů	Reifferscheidt	k1gInPc2	Reifferscheidt
<g/>
,	,	kIx,	,
c.k.	c.k.	k?	c.k.
podplukovník	podplukovník	k1gMnSc1	podplukovník
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
panství	panství	k1gNnSc4	panství
Světlou	světlý	k2eAgFnSc4d1	světlá
za	za	k7c4	za
vklad	vklad	k1gInSc4	vklad
700.000	[number]	k4	700.000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
v.	v.	k?	v.
<g/>
m.	m.	k?	m.
dědictvím	dědictví	k1gNnSc7	dědictví
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
1825	[number]	k4	1825
Kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
světelském	světelský	k2eAgNnSc6d1	světelské
náměstí	náměstí	k1gNnSc6	náměstí
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
kamenickým	kamenický	k2eAgMnSc7d1	kamenický
mistrem	mistr	k1gMnSc7	mistr
Schlesingerem	Schlesinger	k1gMnSc7	Schlesinger
1833	[number]	k4	1833
Silnice	silnice	k1gFnSc1	silnice
ze	z	k7c2	z
Světlé	světlý	k2eAgFnSc2d1	světlá
do	do	k7c2	do
Habrů	habr	k1gInPc2	habr
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
panství	panství	k1gNnSc2	panství
společně	společně	k6eAd1	společně
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
stavěna	stavit	k5eAaImNgNnP	stavit
1834	[number]	k4	1834
Světelský	Světelský	k2eAgInSc1d1	Světelský
špitál	špitál	k1gInSc1	špitál
<g />
.	.	kIx.	.
</s>
<s>
musel	muset	k5eAaImAgMnS	muset
býti	být	k5eAaImF	být
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
přestavěn	přestavěn	k2eAgInSc4d1	přestavěn
1843	[number]	k4	1843
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
zdejším	zdejší	k2eAgInSc6d1	zdejší
byly	být	k5eAaImAgFnP	být
uvnitř	uvnitř	k7c2	uvnitř
značné	značný	k2eAgFnSc2d1	značná
opravy	oprava	k1gFnSc2	oprava
provedeny	proveden	k2eAgInPc1d1	proveden
1847	[number]	k4	1847
František	František	k1gMnSc1	František
starohrabě	starohrabě	k1gMnSc1	starohrabě
Salm-Reifferscheidt	Salm-Reifferscheidt	k1gMnSc1	Salm-Reifferscheidt
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Jana	Jan	k1gMnSc2	Jan
starohraběte	starohrabě	k1gMnSc2	starohrabě
ze	z	k7c2	z
Salm-Reifferscheidtů	Salm-Reifferscheidt	k1gInPc2	Salm-Reifferscheidt
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
panství	panství	k1gNnSc1	panství
Světelské	Světelský	k2eAgFnSc2d1	Světelská
<g/>
.	.	kIx.	.
1850	[number]	k4	1850
Dle	dle	k7c2	dle
nového	nový	k2eAgNnSc2d1	nové
rozdělení	rozdělení	k1gNnSc2	rozdělení
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
7	[number]	k4	7
krajů	kraj	k1gInPc2	kraj
byla	být	k5eAaImAgFnS	být
Světlá	světlý	k2eAgFnSc1d1	světlá
a	a	k8xC	a
území	území	k1gNnPc4	území
bývalého	bývalý	k2eAgNnSc2d1	bývalé
panství	panství	k1gNnSc2	panství
<g />
.	.	kIx.	.
</s>
<s>
světelského	světelský	k2eAgInSc2d1	světelský
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
ku	k	k7c3	k
kraji	kraj	k1gInSc3	kraj
pardubickému	pardubický	k2eAgInSc3d1	pardubický
<g/>
.	.	kIx.	.
1860	[number]	k4	1860
Kostelní	kostelní	k2eAgFnSc1d1	kostelní
věž	věž	k1gFnSc1	věž
musila	musit	k5eAaImAgFnS	musit
býti	být	k5eAaImF	být
za	za	k7c7	za
příčinou	příčina	k1gFnSc7	příčina
schátralosti	schátralost	k1gFnSc2	schátralost
hořejší	hořejší	k2eAgNnSc4d1	hořejší
své	své	k1gNnSc4	své
části	část	k1gFnSc2	část
podepřena	podepřít	k5eAaPmNgFnS	podepřít
trámovým	trámový	k2eAgNnSc7d1	trámové
lešením	lešení	k1gNnSc7	lešení
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
Plavba	plavba	k1gFnSc1	plavba
dříví	dříví	k1gNnSc2	dříví
po	po	k7c6	po
Sázavě	Sázava	k1gFnSc6	Sázava
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
1866	[number]	k4	1866
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Světlé	světlý	k2eAgFnSc2d1	světlá
první	první	k4xOgFnSc2	první
divise	divis	k1gInSc5	divis
pruské	pruský	k2eAgFnSc2d1	pruská
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
svob.	svob.	k?	svob.
Pánem	pán	k1gMnSc7	pán
z	z	k7c2	z
Kernsteinů	Kernstein	k1gInPc2	Kernstein
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
omezila	omezit	k5eAaPmAgFnS	omezit
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
vzpurně	vzpurně	k6eAd1	vzpurně
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c4	na
rekvisici	rekvisice	k1gFnSc4	rekvisice
menáže	menáže	k?	menáže
a	a	k8xC	a
krmiva	krmivo	k1gNnSc2	krmivo
pro	pro	k7c4	pro
koňstvo	koňstvo	k1gNnSc4	koňstvo
1867	[number]	k4	1867
Roku	rok	k1gInSc2	rok
tohoto	tento	k3xDgMnSc4	tento
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
dráhy	dráha	k1gFnSc2	dráha
1869	[number]	k4	1869
S	s	k7c7	s
přestavbou	přestavba	k1gFnSc7	přestavba
světelského	světelský	k2eAgInSc2d1	světelský
zámku	zámek	k1gInSc2	zámek
bylo	být	k5eAaImAgNnS	být
započato	započnout	k5eAaPmNgNnS	započnout
1870	[number]	k4	1870
J.O.	J.O.	k1gMnPc2	J.O.
starohrabě	starohrabě	k1gMnSc1	starohrabě
František	František	k1gMnSc1	František
Salm-Reifferscheidt	Salm-Reifferscheidt	k1gMnSc1	Salm-Reifferscheidt
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
statky	statek	k1gInPc4	statek
své	svůj	k3xOyFgInPc4	svůj
u	u	k7c2	u
Světlé	světlý	k2eAgFnSc2d1	světlá
přikoupením	přikoupení	k1gNnSc7	přikoupení
Habrů	habr	k1gInPc2	habr
<g/>
.	.	kIx.	.
1879	[number]	k4	1879
Na	na	k7c6	na
světelském	světelský	k2eAgInSc6d1	světelský
<g />
.	.	kIx.	.
</s>
<s>
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
děkanství	děkanství	k1gNnPc2	děkanství
provedeny	provést	k5eAaPmNgFnP	provést
značné	značný	k2eAgFnPc1d1	značná
opravy	oprava	k1gFnPc1	oprava
1881	[number]	k4	1881
Světelský	Světelský	k2eAgInSc1d1	Světelský
špitál	špitál	k1gInSc1	špitál
byl	být	k5eAaImAgInS	být
za	za	k7c7	za
příčinou	příčina	k1gFnSc7	příčina
schátralosti	schátralost	k1gFnSc2	schátralost
celé	celý	k2eAgFnSc2d1	celá
stavby	stavba	k1gFnSc2	stavba
rozbořen	rozbořen	k2eAgMnSc1d1	rozbořen
a	a	k8xC	a
dle	dle	k7c2	dle
stavebního	stavební	k2eAgInSc2d1	stavební
plánu	plán	k1gInSc2	plán
architekta	architekt	k1gMnSc4	architekt
Linsbaura	Linsbaur	k1gMnSc4	Linsbaur
stavitelem	stavitel	k1gMnSc7	stavitel
J.	J.	kA	J.
Zemanem	Zeman	k1gMnSc7	Zeman
nákladem	náklad	k1gInSc7	náklad
7960	[number]	k4	7960
zl	zl	k?	zl
<g/>
.	.	kIx.	.
97	[number]	k4	97
kr.	kr.	k?	kr.
znovu	znovu	k6eAd1	znovu
vystavěn	vystavět	k5eAaPmNgInS	vystavět
1887	[number]	k4	1887
František	František	k1gMnSc1	František
starohrabě	starohrabě	k1gMnSc1	starohrabě
ze	z	k7c2	z
Salm-Reifferscheidtů	Salm-Reifferscheidt	k1gInPc2	Salm-Reifferscheidt
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1887	[number]	k4	1887
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
dědí	dědit	k5eAaImIp3nS	dědit
jeho	on	k3xPp3gMnSc4	on
setra	setr	k1gMnSc4	setr
Johanna	Johann	k1gMnSc4	Johann
<g/>
,	,	kIx,	,
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
hraběti	hrabě	k1gMnSc6	hrabě
Josefu	Josef	k1gMnSc6	Josef
Osvaldu	Osvald	k1gMnSc6	Osvald
I.	I.	kA	I.
Thun-Hohensteinovi	Thun-Hohenstein	k1gMnSc3	Thun-Hohenstein
1892	[number]	k4	1892
Panství	panství	k1gNnSc2	panství
dědí	dědit	k5eAaImIp3nS	dědit
hrabě	hrabě	k1gMnSc1	hrabě
Josef	Josef	k1gMnSc1	Josef
Osvald	Osvald	k1gMnSc1	Osvald
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1	Thun-Hohenstein
<g/>
/	/	kIx~	/
<g/>
Salm-Reifferscheidt	Salm-Reifferscheidt	k1gMnSc1	Salm-Reifferscheidt
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
hraběnky	hraběnka	k1gFnSc2	hraběnka
Johanny	Johanna	k1gFnSc2	Johanna
Thun-Hohensteinové	Thun-Hohensteinová	k1gFnSc2	Thun-Hohensteinová
1913	[number]	k4	1913
Převzala	převzít	k5eAaPmAgFnS	převzít
zámek	zámek	k1gInSc4	zámek
i	i	k8xC	i
panství	panství	k1gNnSc2	panství
Pozemková	pozemkový	k2eAgFnSc1d1	pozemková
banka	banka	k1gFnSc1	banka
1914	[number]	k4	1914
Koupil	koupit	k5eAaPmAgInS	koupit
zámek	zámek	k1gInSc1	zámek
továrník	továrník	k1gMnSc1	továrník
Richard	Richard	k1gMnSc1	Richard
Moravetz	Moravetz	k1gMnSc1	Moravetz
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
-	-	kIx~	-
kasárna	kasárna	k1gNnPc1	kasárna
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
1945	[number]	k4	1945
převzal	převzít	k5eAaPmAgInS	převzít
zámek	zámek	k1gInSc1	zámek
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
<g/>
stát	stát	k1gInSc1	stát
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
zprvu	zprvu	k6eAd1	zprvu
jako	jako	k8xS	jako
kasárna	kasárna	k1gNnPc4	kasárna
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
<g/>
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
učňovské	učňovský	k2eAgNnSc4d1	učňovské
středisko	středisko	k1gNnSc4	středisko
mládeže	mládež	k1gFnSc2	mládež
1950	[number]	k4	1950
zámek	zámek	k1gInSc1	zámek
pronajat	pronajat	k2eAgInSc1d1	pronajat
Státní	státní	k2eAgFnSc3d1	státní
pojišťovně	pojišťovna	k1gFnSc3	pojišťovna
<g/>
,	,	kIx,	,
<g/>
v	v	k7c6	v
části	část	k1gFnSc6	část
byty	byt	k1gInPc4	byt
1952	[number]	k4	1952
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
středisko	středisko	k1gNnSc1	středisko
pracujícího	pracující	k2eAgInSc2d1	pracující
dorostu	dorost	k1gInSc2	dorost
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vernierovské	vernierovský	k2eAgFnSc6d1	vernierovský
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
1960	[number]	k4	1960
převzalo	převzít	k5eAaPmAgNnS	převzít
zámek	zámek	k1gInSc1	zámek
ZOU	zoo	k1gNnSc3	zoo
(	(	kIx(	(
<g/>
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nástupce	nástupce	k1gMnSc1	nástupce
učňovské	učňovský	k2eAgFnSc2d1	učňovská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
<g/>
Státní	státní	k2eAgFnSc1d1	státní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
budovu	budova	k1gFnSc4	budova
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
<g/>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
vnitřním	vnitřní	k2eAgFnPc3d1	vnitřní
rekonstrukcím	rekonstrukce	k1gFnPc3	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sloužil	sloužit	k5eAaImAgMnS	sloužit
potřebám	potřeba	k1gFnPc3	potřeba
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
muzeem	muzeum	k1gNnSc7	muzeum
Světelska	Světelsko	k1gNnSc2	Světelsko
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
pod	pod	k7c4	pod
Světlou	světlý	k2eAgFnSc4d1	světlá
jako	jako	k8xS	jako
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
patří	patřit	k5eAaImIp3nS	patřit
Benetice	Benetika	k1gFnSc3	Benetika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
starostou	starosta	k1gMnSc7	starosta
Světlé	světlý	k2eAgFnSc6d1	světlá
funkci	funkce	k1gFnSc6	funkce
zastává	zastávat	k5eAaImIp3nS	zastávat
Jan	Jan	k1gMnSc1	Jan
Tourek	Tourek	k1gMnSc1	Tourek
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
Rada	rada	k1gFnSc1	rada
města	město	k1gNnSc2	město
prodala	prodat	k5eAaPmAgFnS	prodat
zámek	zámek	k1gInSc4	zámek
rodině	rodina	k1gFnSc3	rodina
Dégermée	Dégermée	k1gNnSc1	Dégermée
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Benetice	Benetika	k1gFnSc6	Benetika
Dolní	dolní	k2eAgFnSc1d1	dolní
Březinka	březinka	k1gFnSc1	březinka
Dolní	dolní	k2eAgFnSc1d1	dolní
Dlužiny	Dlužina	k1gFnSc2	Dlužina
Horní	horní	k2eAgFnSc1d1	horní
Březinka	březinka	k1gFnSc1	březinka
Horní	horní	k2eAgFnSc2d1	horní
Dlužiny	Dlužina	k1gFnSc2	Dlužina
Josefodol	Josefodol	k1gInSc1	Josefodol
Kochánov	Kochánov	k1gInSc1	Kochánov
Leštinka	Leštinka	k1gFnSc1	Leštinka
Lipnička	Lipnička	k1gFnSc1	Lipnička
Mrzkovice	Mrzkovice	k1gFnSc1	Mrzkovice
Opatovice	Opatovice	k1gFnSc1	Opatovice
Radostovice	Radostovice	k1gFnSc1	Radostovice
Závidkovice	Závidkovice	k1gFnSc1	Závidkovice
Žebrákov	Žebrákov	k1gInSc4	Žebrákov
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
i	i	k8xC	i
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
u	u	k7c2	u
Světlé	světlý	k2eAgFnSc2d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
domov	domov	k1gInSc1	domov
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
a	a	k8xC	a
dům	dům	k1gInSc4	dům
s	s	k7c7	s
pečovatelskou	pečovatelský	k2eAgFnSc7d1	pečovatelská
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
jednu	jeden	k4xCgFnSc4	jeden
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
Lánecká	Lánecká	k1gFnSc1	Lánecká
<g/>
,	,	kIx,	,
Komenského	Komenského	k2eAgFnSc1d1	Komenského
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dům	dům	k1gInSc4	dům
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
základní	základní	k2eAgFnSc4d1	základní
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
Akademie	akademie	k1gFnSc2	akademie
-	-	kIx~	-
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
Světlá	světlý	k2eAgFnSc1d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
pět	pět	k4xCc4	pět
maturitních	maturitní	k2eAgInPc2d1	maturitní
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
uměleckořemeslné	uměleckořemeslný	k2eAgNnSc4d1	uměleckořemeslné
zpracování	zpracování	k1gNnSc4	zpracování
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
skla	sklo	k1gNnPc1	sklo
<g/>
)	)	kIx)	)
a	a	k8xC	a
devět	devět	k4xCc4	devět
učebních	učební	k2eAgInPc2d1	učební
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
ošetřovatel	ošetřovatel	k1gMnSc1	ošetřovatel
<g/>
,	,	kIx,	,
strojník	strojník	k1gMnSc1	strojník
<g/>
,	,	kIx,	,
aranžér	aranžér	k1gMnSc1	aranžér
<g/>
,	,	kIx,	,
zlatník	zlatník	k1gMnSc1	zlatník
a	a	k8xC	a
klenotník	klenotník	k1gMnSc1	klenotník
<g/>
,	,	kIx,	,
umělecký	umělecký	k2eAgMnSc1d1	umělecký
kovář	kovář	k1gMnSc1	kovář
a	a	k8xC	a
zámečník	zámečník	k1gMnSc1	zámečník
<g/>
,	,	kIx,	,
umělecký	umělecký	k2eAgMnSc1d1	umělecký
truhlář	truhlář	k1gMnSc1	truhlář
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
,	,	kIx,	,
umělecký	umělecký	k2eAgMnSc1d1	umělecký
keramik	keramik	k1gMnSc1	keramik
<g/>
,	,	kIx,	,
kameník	kameník	k1gMnSc1	kameník
a	a	k8xC	a
sklář	sklář	k1gMnSc1	sklář
-	-	kIx~	-
výrobce	výrobce	k1gMnSc1	výrobce
a	a	k8xC	a
zušlechťovatel	zušlechťovatel	k1gMnSc1	zušlechťovatel
skla	sklo	k1gNnSc2	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
instituce	instituce	k1gFnSc2	instituce
je	být	k5eAaImIp3nS	být
i	i	k9	i
vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
dva	dva	k4xCgInPc4	dva
obory	obor	k1gInPc4	obor
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc3	řízení
výroby	výroba	k1gFnSc2	výroba
zpracování	zpracování	k1gNnSc2	zpracování
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
sklářské	sklářský	k2eAgFnSc2d1	sklářská
a	a	k8xC	a
keramické	keramický	k2eAgFnSc2d1	keramická
výroby	výroba	k1gFnSc2	výroba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
zařízení	zařízení	k1gNnSc1	zařízení
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Kytice	kytice	k1gFnSc2	kytice
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
kina	kino	k1gNnSc2	kino
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Na	na	k7c6	na
Půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
turistického	turistický	k2eAgNnSc2d1	turistické
informačního	informační	k2eAgNnSc2d1	informační
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
společnost	společnost	k1gFnSc1	společnost
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
zařízení	zařízení	k1gNnSc1	zařízení
města	město	k1gNnSc2	město
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
spravuje	spravovat	k5eAaImIp3nS	spravovat
tři	tři	k4xCgNnPc4	tři
sportovní	sportovní	k2eAgNnPc4d1	sportovní
zařízení	zařízení	k1gNnPc4	zařízení
-	-	kIx~	-
Sportovní	sportovní	k2eAgNnSc4d1	sportovní
centrum	centrum	k1gNnSc4	centrum
Pěšinky	pěšinka	k1gFnSc2	pěšinka
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
areál	areál	k1gInSc4	areál
a	a	k8xC	a
tenisovou	tenisový	k2eAgFnSc4d1	tenisová
halu	hala	k1gFnSc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Vojenský	vojenský	k2eAgInSc1d1	vojenský
hřbitov	hřbitov	k1gInSc1	hřbitov
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
Březinka	březinka	k1gFnSc1	březinka
<g/>
)	)	kIx)	)
Světelské	Světelský	k2eAgNnSc1d1	Světelské
podzemí	podzemí	k1gNnSc1	podzemí
Muzeum	muzeum	k1gNnSc1	muzeum
Světelska	Světelska	k1gFnSc1	Světelska
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
zámku	zámek	k1gInSc2	zámek
Synagoga	synagoga	k1gFnSc1	synagoga
ve	v	k7c6	v
Světlé	světlý	k2eAgFnSc2d1	světlá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
Alois	Alois	k1gMnSc1	Alois
Jelen	Jelen	k1gMnSc1	Jelen
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
Oskar	Oskar	k1gMnSc1	Oskar
Moravec	Moravec	k1gMnSc1	Moravec
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
</s>
