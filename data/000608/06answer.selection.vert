<s>
Světlá	světlat	k5eAaImIp3nS	světlat
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Swietla	Swietla	k1gFnSc4	Swietla
ob	ob	k7c4	ob
der	drát	k5eAaImRp2nS	drát
Sasau	Sasaus	k1gInSc3	Sasaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
14	[number]	k4	14
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Sázavy	Sázava	k1gFnSc2	Sázava
a	a	k8xC	a
Sázavky	Sázavka	k1gFnSc2	Sázavka
<g/>
.	.	kIx.	.
</s>
