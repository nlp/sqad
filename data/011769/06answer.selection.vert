<s>
Kmenové	kmenový	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
jsou	být	k5eAaImIp3nP
nediferencované	diferencovaný	k2eNgFnPc1d1
živočišné	živočišný	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1
mají	mít	k5eAaImIp3nP
schopnost	schopnost	k1gFnSc4
se	se	k3xPyFc4
dělit	dělit	k5eAaImF
(	(	kIx(
<g/>
proliferovat	proliferovat	k5eAaImF
<g/>
)	)	kIx)
a	a	k8xC
přeměnit	přeměnit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c4
jiný	jiný	k2eAgInSc4d1
buněčný	buněčný	k2eAgInSc4d1
typ	typ	k1gInSc4
(	(	kIx(
<g/>
diferencovat	diferencovat	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>