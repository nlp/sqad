<p>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
nediferencované	diferencovaný	k2eNgFnPc1d1	nediferencovaná
živočišné	živočišný	k2eAgFnPc1d1	živočišná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
dělit	dělit	k5eAaImF	dělit
(	(	kIx(	(
<g/>
proliferovat	proliferovat	k5eAaImF	proliferovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeměnit	přeměnit	k5eAaPmF	přeměnit
se	se	k3xPyFc4	se
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
buněčný	buněčný	k2eAgInSc4d1	buněčný
typ	typ	k1gInSc4	typ
(	(	kIx(	(
<g/>
diferencovat	diferencovat	k5eAaImF	diferencovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tělu	tělo	k1gNnSc3	tělo
vytvořit	vytvořit	k5eAaPmF	vytvořit
nové	nový	k2eAgFnPc4d1	nová
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
opravit	opravit	k5eAaPmF	opravit
tak	tak	k6eAd1	tak
poškozené	poškozený	k2eAgFnPc4d1	poškozená
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
např.	např.	kA	např.
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
neumí	umět	k5eNaImIp3nS	umět
dělit	dělit	k5eAaImF	dělit
<g/>
.	.	kIx.	.
</s>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
více	hodně	k6eAd2	hodně
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
objevené	objevený	k2eAgInPc1d1	objevený
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Embryonální	embryonální	k2eAgFnPc1d1	embryonální
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
oddělené	oddělený	k2eAgFnPc1d1	oddělená
od	od	k7c2	od
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
masy	masa	k1gFnSc2	masa
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
ICM	ICM	kA	ICM
<g/>
)	)	kIx)	)
v	v	k7c6	v
blastocystě	blastocyst	k1gInSc6	blastocyst
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
dospělé	dospělý	k2eAgFnPc1d1	dospělá
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
dospělých	dospělý	k2eAgMnPc2d1	dospělý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
a	a	k8xC	a
prekurzorové	prekurzorový	k2eAgFnPc1d1	prekurzorový
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
organismu	organismus	k1gInSc6	organismus
začleňují	začleňovat	k5eAaImIp3nP	začleňovat
do	do	k7c2	do
mechanismů	mechanismus	k1gInPc2	mechanismus
opravy	oprava	k1gFnSc2	oprava
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
přerušené	přerušený	k2eAgFnPc4d1	přerušená
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
embrya	embryo	k1gNnSc2	embryo
se	se	k3xPyFc4	se
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
mohou	moct	k5eAaImIp3nP	moct
diferencovat	diferencovat	k5eAaImF	diferencovat
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
již	již	k6eAd1	již
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ektoderm	ektoderm	k1gInSc4	ektoderm
<g/>
,	,	kIx,	,
endoderm	endoderm	k1gInSc4	endoderm
<g/>
,	,	kIx,	,
mesoderm	mesoderm	k1gInSc4	mesoderm
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
soustav	soustava	k1gFnPc2	soustava
podílejících	podílející	k2eAgFnPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c4	na
regeneraci	regenerace	k1gFnSc4	regenerace
<g/>
,	,	kIx,	,
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
buněk	buňka	k1gFnPc2	buňka
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
střevní	střevní	k2eAgFnSc2d1	střevní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
využívané	využívaný	k2eAgFnPc1d1	využívaná
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
lékařských	lékařský	k2eAgFnPc6d1	lékařská
terapiích	terapie	k1gFnPc6	terapie
a	a	k8xC	a
metodách	metoda	k1gFnPc6	metoda
léčby	léčba	k1gFnSc2	léčba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
transplantace	transplantace	k1gFnSc2	transplantace
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
kmenovými	kmenový	k2eAgFnPc7d1	kmenová
buňkami	buňka	k1gFnPc7	buňka
těží	těžet	k5eAaImIp3nS	těžet
především	především	k6eAd1	především
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
poznatků	poznatek	k1gInPc2	poznatek
Ernesta	Ernest	k1gMnSc2	Ernest
A.	A.	kA	A.
McCullocha	McCulloch	k1gMnSc2	McCulloch
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc2	Jamese
E.	E.	kA	E.
Tilla	Till	k1gMnSc4	Till
z	z	k7c2	z
období	období	k1gNnSc2	období
po	po	k7c6	po
roku	rok	k1gInSc6	rok
1960	[number]	k4	1960
z	z	k7c2	z
Torontské	torontský	k2eAgFnSc2d1	Torontská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Embryonální	embryonální	k2eAgFnSc1d1	embryonální
buněčná	buněčný	k2eAgFnSc1d1	buněčná
linie	linie	k1gFnSc1	linie
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
jako	jako	k9	jako
HeLa	Hela	k1gFnSc1	Hela
<g/>
)	)	kIx)	)
a	a	k8xC	a
autogenické	autogenický	k2eAgFnPc1d1	autogenický
embryonální	embryonální	k2eAgFnPc1d1	embryonální
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
generované	generovaný	k2eAgFnSc2d1	generovaná
SCNT	SCNT	kA	SCNT
-	-	kIx~	-
přenosem	přenos	k1gInSc7	přenos
jádra	jádro	k1gNnSc2	jádro
somatických	somatický	k2eAgFnPc2d1	somatická
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
proces	proces	k1gInSc1	proces
podstatou	podstata	k1gFnSc7	podstata
opačný	opačný	k2eAgInSc1d1	opačný
buněčné	buněčný	k2eAgFnSc3d1	buněčná
diferenciaci	diferenciace	k1gFnSc3	diferenciace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
navrhnuté	navrhnutý	k2eAgFnPc1d1	navrhnutá
za	za	k7c4	za
slibné	slibný	k2eAgFnPc4d1	slibná
metody	metoda	k1gFnPc4	metoda
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
lékařské	lékařský	k2eAgFnPc4d1	lékařská
terapie	terapie	k1gFnPc4	terapie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
možná	možný	k2eAgFnSc1d1	možná
kultivace	kultivace	k1gFnSc1	kultivace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
lékařských	lékařský	k2eAgFnPc6d1	lékařská
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
kultivace	kultivace	k1gFnSc2	kultivace
buňky	buňka	k1gFnPc1	buňka
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Diferencují	diferencovat	k5eAaImIp3nP	diferencovat
se	se	k3xPyFc4	se
v	v	k7c4	v
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
nervová	nervový	k2eAgFnSc1d1	nervová
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
svalová	svalový	k2eAgFnSc1d1	svalová
tkáň	tkáň	k1gFnSc1	tkáň
etc	etc	k?	etc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
–	–	k?	–
profesor	profesor	k1gMnSc1	profesor
Martin	Martin	k1gMnSc1	Martin
Evans	Evans	k1gInSc1	Evans
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
izoloval	izolovat	k5eAaBmAgInS	izolovat
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
z	z	k7c2	z
myší	myš	k1gFnPc2	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
–	–	k?	–
Gail	Gaila	k1gFnPc2	Gaila
Martinová	Martinová	k1gFnSc1	Martinová
poprvé	poprvé	k6eAd1	poprvé
použila	použít	k5eAaPmAgFnS	použít
termín	termín	k1gInSc4	termín
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
stem	sto	k4xCgNnSc7	sto
cells	cellsa	k1gFnPc2	cellsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
američtí	americký	k2eAgMnPc1d1	americký
vědci	vědec	k1gMnPc1	vědec
(	(	kIx(	(
<g/>
Wisconsin	Wisconsin	k1gMnSc1	Wisconsin
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
jako	jako	k9	jako
první	první	k4xOgFnPc4	první
embryonální	embryonální	k2eAgFnPc4d1	embryonální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
z	z	k7c2	z
makaků	makak	k1gMnPc2	makak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Thomson	Thomson	k1gMnSc1	Thomson
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tým	tým	k1gInSc4	tým
izolovali	izolovat	k5eAaBmAgMnP	izolovat
lidské	lidský	k2eAgFnPc4d1	lidská
embryonální	embryonální	k2eAgFnPc4d1	embryonální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
legalizace	legalizace	k1gFnSc2	legalizace
klonování	klonování	k1gNnSc2	klonování
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
pro	pro	k7c4	pro
získávání	získávání	k1gNnSc4	získávání
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
první	první	k4xOgFnSc2	první
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
blastocysty	blastocyst	k1gInPc1	blastocyst
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
zničit	zničit	k5eAaPmF	zničit
do	do	k7c2	do
věku	věk	k1gInSc2	věk
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
založena	založit	k5eAaPmNgFnS	založit
UK	UK	kA	UK
Stem	sto	k4xCgNnSc7	sto
Cell	cello	k1gNnPc2	cello
Bank	bank	k1gInSc1	bank
–	–	k?	–
první	první	k4xOgFnSc4	první
evropská	evropský	k2eAgFnSc1d1	Evropská
banka	banka	k1gFnSc1	banka
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
jsou	být	k5eAaImIp3nP	být
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
zmrazené	zmrazený	k2eAgInPc1d1	zmrazený
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
dusíku	dusík	k1gInSc6	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
vědci	vědec	k1gMnPc1	vědec
(	(	kIx(	(
<g/>
londýnská	londýnský	k2eAgFnSc1d1	londýnská
King	King	k1gInSc1	King
College	Colleg	k1gInPc1	Colleg
<g/>
)	)	kIx)	)
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
první	první	k4xOgFnSc4	první
linii	linie	k1gFnSc4	linie
lidských	lidský	k2eAgFnPc2d1	lidská
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
britský	britský	k2eAgMnSc1d1	britský
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Cardiffské	Cardiffský	k2eAgFnSc2d1	Cardiffská
univerzity	univerzita	k1gFnSc2	univerzita
Martin	Martin	k1gMnSc1	Martin
Evans	Evansa	k1gFnPc2	Evansa
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
titul	titul	k1gInSc4	titul
sir	sir	k1gMnSc1	sir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
jihokorejští	jihokorejský	k2eAgMnPc1d1	jihokorejský
vědci	vědec	k1gMnPc1	vědec
naklonovali	naklonovat	k5eAaPmAgMnP	naklonovat
30	[number]	k4	30
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
<g/>
,	,	kIx,	,
nechali	nechat	k5eAaPmAgMnP	nechat
je	on	k3xPp3gInPc4	on
dorůst	dorůst	k5eAaPmF	dorůst
do	do	k7c2	do
stadia	stadion	k1gNnSc2	stadion
blastocysty	blastocysta	k1gFnSc2	blastocysta
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
klonové	klonové	k2eAgMnPc2d1	klonové
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
–	–	k?	–
v	v	k7c6	v
jihokorejském	jihokorejský	k2eAgInSc6d1	jihokorejský
Soulu	Soul	k1gInSc6	Soul
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
banka	banka	k1gFnSc1	banka
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
a	a	k8xC	a
dodávání	dodávání	k1gNnSc4	dodávání
nových	nový	k2eAgFnPc2d1	nová
linií	linie	k1gFnPc2	linie
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Banka	banka	k1gFnSc1	banka
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
umožnit	umožnit	k5eAaPmF	umožnit
expertům	expert	k1gMnPc3	expert
obcházet	obcházet	k5eAaImF	obcházet
omezení	omezení	k1gNnSc4	omezení
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
některé	některý	k3yIgNnSc4	některý
vlády	vláda	k1gFnPc1	vláda
zavedly	zavést	k5eAaPmAgFnP	zavést
(	(	kIx(	(
<g/>
např.	např.	kA	např.
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
si	se	k3xPyFc3	se
nehodlá	hodlat	k5eNaImIp3nS	hodlat
nové	nový	k2eAgFnPc4d1	nová
linie	linie	k1gFnPc4	linie
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
nechat	nechat	k5eAaPmF	nechat
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Totipotentní	Totipotentní	k2eAgMnSc1d1	Totipotentní
–	–	k?	–
totipotentní	totipotentní	k2eAgFnSc4d1	totipotentní
schopnost	schopnost	k1gFnSc4	schopnost
buňky	buňka	k1gFnSc2	buňka
=	=	kIx~	=
totipotence	totipotence	k1gFnSc1	totipotence
=	=	kIx~	=
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
jiný	jiný	k2eAgInSc4d1	jiný
typ	typ	k1gInSc4	typ
buněk	buňka	k1gFnPc2	buňka
včetně	včetně	k7c2	včetně
totipotentní	totipotentní	k2eAgFnSc2d1	totipotentní
buňky	buňka	k1gFnSc2	buňka
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Zygota	zygota	k1gFnSc1	zygota
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
splynutím	splynutí	k1gNnSc7	splynutí
vajíčka	vajíčko	k1gNnSc2	vajíčko
a	a	k8xC	a
spermie	spermie	k1gFnSc2	spermie
je	být	k5eAaImIp3nS	být
totipotentní	totipotentní	k2eAgNnSc1d1	totipotentní
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
buňky	buňka	k1gFnSc2	buňka
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
jejím	její	k3xOp3gInSc7	její
prvním	první	k4xOgNnSc7	první
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pluripotentní	Pluripotentní	k2eAgFnSc1d1	Pluripotentní
–	–	k?	–
pluripotence	pluripotence	k1gFnSc1	pluripotence
=	=	kIx~	=
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
potomci	potomek	k1gMnPc1	potomek
totipotentních	totipotentní	k2eAgFnPc2d1	totipotentní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
jiné	jiný	k2eAgFnPc4d1	jiná
buňky	buňka	k1gFnPc4	buňka
kromě	kromě	k7c2	kromě
buňky	buňka	k1gFnSc2	buňka
totipotentní	totipotentní	k2eAgFnSc2d1	totipotentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Multipotentní	Multipotentní	k2eAgFnSc1d1	Multipotentní
–	–	k?	–
multipotence	multipotence	k1gFnSc1	multipotence
=	=	kIx~	=
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
pouze	pouze	k6eAd1	pouze
buňky	buňka	k1gFnSc2	buňka
příbuzné	příbuzná	k1gFnSc2	příbuzná
danému	daný	k2eAgInSc3d1	daný
typu	typ	k1gInSc3	typ
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krevní	krevní	k2eAgFnPc4d1	krevní
buňky	buňka	k1gFnPc4	buňka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Progenitor	Progenitor	k1gInSc1	Progenitor
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zvané	zvaný	k2eAgFnSc3d1	zvaná
unipotentní	unipotentní	k2eAgFnSc3d1	unipotentní
<g/>
)	)	kIx)	)
–	–	k?	–
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc4d1	jediný
typ	typ	k1gInSc4	typ
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
samy	sám	k3xTgInPc1	sám
obnovit	obnovit	k5eAaPmF	obnovit
–	–	k?	–
tak	tak	k9	tak
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
kmenové	kmenový	k2eAgFnPc1d1	kmenová
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
množení	množení	k1gNnSc6	množení
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
proliferaci	proliferace	k1gFnSc4	proliferace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
tři	tři	k4xCgInPc1	tři
způsoby	způsob	k1gInPc1	způsob
dělení	dělení	k1gNnSc2	dělení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Symetricky	symetricky	k6eAd1	symetricky
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
dvě	dva	k4xCgFnPc4	dva
identické	identický	k2eAgFnPc4d1	identická
buňky	buňka	k1gFnPc4	buňka
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
tedy	tedy	k9	tedy
k	k	k7c3	k
množení	množení	k1gNnSc3	množení
jejich	jejich	k3xOp3gFnSc2	jejich
populace	populace	k1gFnSc2	populace
</s>
</p>
<p>
<s>
Asymetricky	asymetricky	k6eAd1	asymetricky
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
původní	původní	k2eAgInSc4d1	původní
fenotyp	fenotyp	k1gInSc4	fenotyp
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
jiná	jiný	k2eAgFnSc1d1	jiná
(	(	kIx(	(
<g/>
diferenciovaná	diferenciovaný	k2eAgFnSc1d1	diferenciovaná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Diferenciační	diferenciační	k2eAgInSc1d1	diferenciační
–	–	k?	–
obě	dva	k4xCgFnPc1	dva
nově	nova	k1gFnSc3	nova
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
buňky	buňka	k1gFnSc2	buňka
mají	mít	k5eAaImIp3nP	mít
nový	nový	k2eAgInSc4d1	nový
fenotyp	fenotyp	k1gInSc4	fenotyp
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dalším	další	k2eAgInSc7d1	další
stupněm	stupeň	k1gInSc7	stupeň
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
diferenciační	diferenciační	k2eAgFnSc6d1	diferenciační
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dá	dát	k5eAaPmIp3nS	dát
vzniku	vznik	k1gInSc3	vznik
dvěma	dva	k4xCgFnPc3	dva
diferenciovaným	diferenciovaný	k2eAgFnPc3d1	diferenciovaná
buňkám	buňka	k1gFnPc3	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
dvěma	dva	k4xCgFnPc3	dva
kmenovým	kmenový	k2eAgFnPc3d1	kmenová
buňkám	buňka	k1gFnPc3	buňka
(	(	kIx(	(
<g/>
symetrické	symetrický	k2eAgNnSc1d1	symetrické
dělení	dělení	k1gNnSc1	dělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Identifikace	identifikace	k1gFnSc2	identifikace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jsou	být	k5eAaImIp3nP	být
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
rozpoznávány	rozpoznáván	k2eAgFnPc1d1	rozpoznávána
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
zda	zda	k9	zda
dokáží	dokázat	k5eAaPmIp3nP	dokázat
regenerovat	regenerovat	k5eAaBmF	regenerovat
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
určující	určující	k2eAgInSc1d1	určující
test	test	k1gInSc1	test
u	u	k7c2	u
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
hematopoetických	hematopoetický	k2eAgFnPc2d1	hematopoetický
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
HSCs	HSCs	k1gInSc1	HSCs
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
transplantace	transplantace	k1gFnSc1	transplantace
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
regenerace	regenerace	k1gFnSc2	regenerace
jedince	jedinec	k1gMnSc2	jedinec
bez	bez	k7c2	bez
přítomných	přítomný	k2eAgFnPc2d1	přítomná
HSCs	HSCsa	k1gFnPc2	HSCsa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
nové	nový	k2eAgFnPc4d1	nová
buňky	buňka	k1gFnPc4	buňka
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
časovém	časový	k2eAgNnSc6d1	časové
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
také	také	k9	také
fungovat	fungovat	k5eAaImF	fungovat
mechanismus	mechanismus	k1gInSc4	mechanismus
izolace	izolace	k1gFnSc2	izolace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
od	od	k7c2	od
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
byly	být	k5eAaImAgFnP	být
transplantovány	transplantovat	k5eAaBmNgFnP	transplantovat
k	k	k7c3	k
následné	následný	k2eAgFnSc3d1	následná
transplantaci	transplantace	k1gFnSc3	transplantace
těchto	tento	k3xDgFnPc2	tento
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
buněk	buňka	k1gFnPc2	buňka
dalšímu	další	k2eAgMnSc3d1	další
jedinci	jedinec	k1gMnSc3	jedinec
bez	bez	k7c2	bez
HSCs	HSCsa	k1gFnPc2	HSCsa
<g/>
,	,	kIx,	,
demonstrující	demonstrující	k2eAgNnSc4d1	demonstrující
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
vlastního	vlastní	k2eAgNnSc2d1	vlastní
udržování	udržování	k1gNnSc2	udržování
a	a	k8xC	a
obnovy	obnova	k1gFnSc2	obnova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
in	in	k?	in
vitro	vitro	k6eAd1	vitro
<g/>
,	,	kIx,	,
při	při	k7c6	při
metodě	metoda	k1gFnSc6	metoda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
buňky	buňka	k1gFnPc1	buňka
posuzovány	posuzovat	k5eAaImNgFnP	posuzovat
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
jejich	jejich	k3xOp3gFnSc2	jejich
míry	míra	k1gFnSc2	míra
diferenciace	diferenciace	k1gFnSc2	diferenciace
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
obnovy	obnova	k1gFnSc2	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnSc1	izolace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
možná	možná	k9	možná
podle	podle	k7c2	podle
určení	určení	k1gNnSc2	určení
distinktivních	distinktivní	k2eAgInPc2d1	distinktivní
specifických	specifický	k2eAgInPc2d1	specifický
povrchových	povrchový	k2eAgInPc2d1	povrchový
znaků	znak	k1gInPc2	znak
odlišující	odlišující	k2eAgFnSc2d1	odlišující
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
chovají	chovat	k5eAaImIp3nP	chovat
dle	dle	k7c2	dle
určujících	určující	k2eAgFnPc2d1	určující
podmínek	podmínka	k1gFnPc2	podmínka
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
in	in	k?	in
vitro	vitro	k1gNnSc1	vitro
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
in	in	k?	in
vivo	vivo	k6eAd1	vivo
chovají	chovat	k5eAaImIp3nP	chovat
velmi	velmi	k6eAd1	velmi
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
debata	debata	k1gFnSc1	debata
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
některé	některý	k3yIgFnPc4	některý
kolonie	kolonie	k1gFnPc4	kolonie
dospělých	dospělí	k1gMnPc2	dospělí
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
za	za	k7c4	za
modelové	modelový	k2eAgFnPc4d1	modelová
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
obecně	obecně	k6eAd1	obecně
dostupné	dostupný	k2eAgInPc1d1	dostupný
zdroje	zdroj	k1gInPc1	zdroj
dospělých	dospělí	k1gMnPc2	dospělí
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
autogenické	autogenický	k2eAgFnSc2d1	autogenický
transplantace	transplantace	k1gFnSc2	transplantace
<g/>
)	)	kIx)	)
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
pro	pro	k7c4	pro
extrakci	extrakce	k1gFnSc4	extrakce
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
vyvrtat	vyvrtat	k5eAaPmF	vyvrtat
díru	díra	k1gFnSc4	díra
do	do	k7c2	do
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stehenní	stehenní	k2eAgFnSc4d1	stehenní
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
crista	crista	k1gMnSc1	crista
iliaca	iliaca	k1gMnSc1	iliaca
u	u	k7c2	u
pánevní	pánevní	k2eAgFnSc2d1	pánevní
kosti	kost	k1gFnSc2	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuková	tukový	k2eAgFnSc1d1	tuková
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
.	.	kIx.	.
tvořena	tvořit	k5eAaImNgFnS	tvořit
adipocyty	adipocyt	k1gInPc7	adipocyt
,	,	kIx,	,
uchovávajícími	uchovávající	k2eAgInPc7d1	uchovávající
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
extrakce	extrakce	k1gFnSc1	extrakce
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
liposukce	liposukce	k1gFnSc2	liposukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
po	po	k7c6	po
extrakci	extrakce	k1gFnSc6	extrakce
formou	forma	k1gFnSc7	forma
odběru	odběr	k1gInSc2	odběr
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
krev	krev	k1gFnSc4	krev
od	od	k7c2	od
dárce	dárce	k1gMnSc2	dárce
hnána	hnán	k2eAgFnSc1d1	hnána
přes	přes	k7c4	přes
extrakční	extrakční	k2eAgInSc4d1	extrakční
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgFnPc4d1	oddělující
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
vracení	vracení	k1gNnSc4	vracení
jiných	jiný	k2eAgFnPc2d1	jiná
složek	složka	k1gFnPc2	složka
krve	krev	k1gFnSc2	krev
zpět	zpět	k6eAd1	zpět
dárci	dárce	k1gMnPc7	dárce
<g/>
.	.	kIx.	.
<g/>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
možné	možný	k2eAgInPc1d1	možný
zdroje	zdroj	k1gInPc1	zdroj
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krev	krev	k1gFnSc1	krev
z	z	k7c2	z
pupečníkové	pupečníkový	k2eAgFnSc2d1	pupečníková
šňůry	šňůra	k1gFnSc2	šňůra
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
z	z	k7c2	z
placenty	placenta	k1gFnSc2	placenta
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
získávané	získávaný	k2eAgNnSc1d1	získávané
s	s	k7c7	s
nejnižším	nízký	k2eAgNnSc7d3	nejnižší
rizikem	riziko	k1gNnSc7	riziko
u	u	k7c2	u
autogenické	autogenický	k2eAgFnSc2d1	autogenický
transplantace	transplantace	k1gFnSc2	transplantace
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
odebrané	odebraný	k2eAgNnSc1d1	odebrané
a	a	k8xC	a
následně	následně	k6eAd1	následně
uchované	uchovaný	k2eAgInPc1d1	uchovaný
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
navrácení	navrácení	k1gNnSc4	navrácení
stejnému	stejný	k2eAgInSc3d1	stejný
dárci	dárce	k1gMnPc1	dárce
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgInSc2	který
byly	být	k5eAaImAgFnP	být
odebrány	odebrán	k2eAgFnPc1d1	odebrána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tkáň	tkáň	k1gFnSc1	tkáň
amniového	amniový	k2eAgInSc2d1	amniový
vaku	vak	k1gInSc2	vak
v	v	k7c6	v
placentě	placenta	k1gFnSc6	placenta
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
gen	gen	k1gInSc4	gen
Oct	Oct	k1gFnSc2	Oct
4	[number]	k4	4
a	a	k8xC	a
gen	gen	k1gInSc1	gen
nanog	nanoga	k1gFnPc2	nanoga
<g/>
)	)	kIx)	)
–	–	k?	–
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Stephen	Stephen	k2eAgInSc1d1	Stephen
Strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bazální	bazální	k2eAgFnSc1d1	bazální
vrstva	vrstva	k1gFnSc1	vrstva
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Embryonální	embryonální	k2eAgFnSc2d1	embryonální
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
–	–	k?	–
ES	ES	kA	ES
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
Embryonic	Embryonice	k1gFnPc2	Embryonice
Stem	sto	k4xCgNnSc7	sto
Cells	Cells	k1gInSc1	Cells
<g/>
,	,	kIx,	,
ESCs	ESCs	k1gInSc1	ESCs
<g/>
)	)	kIx)	)
–	–	k?	–
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
ze	z	k7c2	z
zárodku	zárodek	k1gInSc2	zárodek
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
starého	starý	k1gMnSc2	starý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
<g/>
)	)	kIx)	)
s	s	k7c7	s
nejširším	široký	k2eAgNnSc7d3	nejširší
využitím	využití	k1gNnSc7	využití
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
schopností	schopnost	k1gFnSc7	schopnost
se	se	k3xPyFc4	se
diferenciovat	diferenciovat	k5eAaImF	diferenciovat
téměř	téměř	k6eAd1	téměř
v	v	k7c4	v
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
specifickou	specifický	k2eAgFnSc4d1	specifická
buňku	buňka	k1gFnSc4	buňka
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Získávání	získávání	k1gNnSc2	získávání
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nepoužitých	použitý	k2eNgFnPc2d1	nepoužitá
(	(	kIx(	(
<g/>
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
<g/>
)	)	kIx)	)
několikadenních	několikadenní	k2eAgInPc2d1	několikadenní
lidských	lidský	k2eAgInPc2d1	lidský
zárodků	zárodek	k1gInPc2	zárodek
(	(	kIx(	(
<g/>
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
z	z	k7c2	z
klinik	klinika	k1gFnPc2	klinika
pro	pro	k7c4	pro
umělé	umělý	k2eAgNnSc4d1	umělé
oplodnění	oplodnění	k1gNnSc4	oplodnění
–	–	k?	–
embryo	embryo	k1gNnSc1	embryo
se	se	k3xPyFc4	se
odběrem	odběr	k1gInSc7	odběr
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
etické	etický	k2eAgInPc4d1	etický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
z	z	k7c2	z
nadbytečných	nadbytečný	k2eAgNnPc2d1	nadbytečné
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Terapeutické	terapeutický	k2eAgNnSc1d1	terapeutické
klonování	klonování	k1gNnSc1	klonování
(	(	kIx(	(
<g/>
odběr	odběr	k1gInSc1	odběr
buňky	buňka	k1gFnSc2	buňka
z	z	k7c2	z
pacienta	pacient	k1gMnSc2	pacient
→	→	k?	→
vložení	vložení	k1gNnSc4	vložení
do	do	k7c2	do
ženského	ženský	k2eAgNnSc2d1	ženské
vajíčka	vajíčko	k1gNnSc2	vajíčko
bez	bez	k7c2	bez
jádra	jádro	k1gNnSc2	jádro
→	→	k?	→
zárodek	zárodek	k1gInSc1	zárodek
→	→	k?	→
blastocysta	blastocyst	k1gMnSc2	blastocyst
→	→	k?	→
z	z	k7c2	z
blastocysty	blastocysta	k1gFnSc2	blastocysta
se	se	k3xPyFc4	se
vyjmou	vyjmout	k5eAaPmIp3nP	vyjmout
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
→	→	k?	→
kultivace	kultivace	k1gFnSc2	kultivace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
–	–	k?	–
eticky	eticky	k6eAd1	eticky
spornější	sporný	k2eAgFnSc1d2	spornější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
příslušná	příslušný	k2eAgFnSc1d1	příslušná
legislativa	legislativa	k1gFnSc1	legislativa
je	být	k5eAaImIp3nS	být
teprve	teprve	k6eAd1	teprve
připravována	připravován	k2eAgFnSc1d1	připravována
<g/>
.	.	kIx.	.
</s>
<s>
Povoleno	povolen	k2eAgNnSc1d1	povoleno
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
(	(	kIx(	(
<g/>
a	a	k8xC	a
prováděno	provádět	k5eAaImNgNnS	provádět
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
terapeutické	terapeutický	k2eAgNnSc4d1	terapeutické
klonování	klonování	k1gNnSc4	klonování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojení	spojení	k1gNnSc1	spojení
embryonální	embryonální	k2eAgFnSc2d1	embryonální
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
normální	normální	k2eAgFnSc2d1	normální
lidské	lidský	k2eAgFnSc2d1	lidská
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
buňky	buňka	k1gFnPc1	buňka
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgInPc4	některý
vlastnosti	vlastnost	k1gFnPc1	vlastnost
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
čtyři	čtyři	k4xCgFnPc1	čtyři
sady	sada	k1gFnPc1	sada
chromozómů	chromozóm	k1gInPc2	chromozóm
<g/>
.	.	kIx.	.
</s>
<s>
Publikovali	publikovat	k5eAaBmAgMnP	publikovat
to	ten	k3xDgNnSc1	ten
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
mezenchymální	mezenchymální	k2eAgFnSc1d1	mezenchymální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
odběrem	odběr	k1gInSc7	odběr
žádané	žádaný	k2eAgFnSc2d1	žádaná
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
např.	např.	kA	např.
punkcí	punkce	k1gFnSc7	punkce
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
nebo	nebo	k8xC	nebo
odběrem	odběr	k1gInSc7	odběr
tukové	tukový	k2eAgFnSc2d1	tuková
tkáně	tkáň	k1gFnSc2	tkáň
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozrušení	rozrušení	k1gNnSc6	rozrušení
mezibuněčných	mezibuněčný	k2eAgFnPc2d1	mezibuněčná
vazeb	vazba	k1gFnPc2	vazba
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
vytřídit	vytřídit	k5eAaPmF	vytřídit
na	na	k7c6	na
přístrojích	přístroj	k1gInPc6	přístroj
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vloží	vložit	k5eAaPmIp3nS	vložit
do	do	k7c2	do
kultivačních	kultivační	k2eAgFnPc2d1	kultivační
nádob	nádoba	k1gFnPc2	nádoba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
známé	známý	k2eAgFnPc1d1	známá
schopností	schopnost	k1gFnSc7	schopnost
adheze	adheze	k1gFnSc2	adheze
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
přichytí	přichytit	k5eAaPmIp3nP	přichytit
a	a	k8xC	a
kultivují	kultivovat	k5eAaImIp3nP	kultivovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc1d1	ostatní
zralé	zralý	k2eAgFnPc1d1	zralá
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
odmyjí	odmýt	k5eAaPmIp3nP	odmýt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
typ	typ	k1gInSc4	typ
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
také	také	k9	také
odkazováno	odkazován	k2eAgNnSc1d1	odkazováno
jako	jako	k9	jako
na	na	k7c4	na
somatické	somatický	k2eAgFnPc4d1	somatická
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
σ	σ	k?	σ
"	"	kIx"	"
<g/>
patřící	patřící	k2eAgFnSc1d1	patřící
tělu	tělo	k1gNnSc3	tělo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
primární	primární	k2eAgFnSc1d1	primární
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
udržování	udržování	k1gNnSc4	udržování
a	a	k8xC	a
oprava	oprava	k1gFnSc1	oprava
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
i	i	k8xC	i
starších	starší	k1gMnPc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Pluripotentní	Pluripotentní	k2eAgFnPc1d1	Pluripotentní
dospělé	dospělý	k2eAgFnPc1d1	dospělá
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
v	v	k7c6	v
pupečníkové	pupečníkový	k2eAgFnSc6d1	pupečníková
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
tkáních	tkáň	k1gFnPc6	tkáň
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
získávané	získávaný	k2eAgFnPc1d1	získávaná
z	z	k7c2	z
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
cirhózy	cirhóza	k1gFnSc2	cirhóza
<g/>
,	,	kIx,	,
chronická	chronický	k2eAgFnSc1d1	chronická
ischemie	ischemie	k1gFnSc1	ischemie
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
srdečná	srdečný	k2eAgNnPc4d1	srdečné
selhání	selhání	k1gNnPc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
škod	škoda	k1gFnPc2	škoda
způsobených	způsobený	k2eAgFnPc2d1	způsobená
infarktem	infarkt	k1gInSc7	infarkt
myokardu	myokard	k1gInSc2	myokard
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
ubývá	ubývat	k5eAaImIp3nS	ubývat
s	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mužů	muž	k1gMnPc2	muž
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
přítomných	přítomný	k2eAgFnPc2d1	přítomná
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
během	během	k7c2	během
reprodukčního	reprodukční	k2eAgNnSc2d1	reprodukční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
dospělých	dospělí	k1gMnPc2	dospělí
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
schopnosti	schopnost	k1gFnSc2	schopnost
obnovy	obnova	k1gFnSc2	obnova
a	a	k8xC	a
omlazení	omlazení	k1gNnSc2	omlazení
<g/>
.	.	kIx.	.
</s>
<s>
Narušení	narušení	k1gNnSc1	narušení
DNA	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
hromadí	hromadit	k5eAaImIp3nP	hromadit
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
i	i	k9	i
u	u	k7c2	u
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
kompromitujících	kompromitující	k2eAgInPc2d1	kompromitující
okolní	okolní	k2eAgNnSc4d1	okolní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Navyšující	Navyšující	k2eAgMnSc1d1	Navyšující
se	se	k3xPyFc4	se
výskyt	výskyt	k1gInSc1	výskyt
poruchové	poruchový	k2eAgFnSc2d1	poruchová
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
předpokladů	předpoklad	k1gInPc2	předpoklad
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
neschopnost	neschopnost	k1gFnSc4	neschopnost
funkcí	funkce	k1gFnPc2	funkce
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stárnutím	stárnutí	k1gNnSc7	stárnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
dospělých	dospělí	k1gMnPc2	dospělí
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
multipotentních	multipotentní	k2eAgInPc2d1	multipotentní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
odkazováno	odkazovat	k5eAaImNgNnS	odkazovat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
(	(	kIx(	(
<g/>
mezenchymální	mezenchymální	k2eAgFnPc1d1	mezenchymální
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
oddělené	oddělený	k2eAgFnPc1d1	oddělená
z	z	k7c2	z
tukové	tukový	k2eAgFnSc2d1	tuková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
endotheliální	endotheliální	k2eAgFnPc4d1	endotheliální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
(	(	kIx(	(
<g/>
ESCs	ESCs	k1gInSc1	ESCs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
získávané	získávaný	k2eAgFnPc1d1	získávaná
z	z	k7c2	z
zubní	zubní	k2eAgFnSc2d1	zubní
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
DPSCs	DPSCs	k1gInSc1	DPSCs
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
novým	nový	k2eAgInSc7d1	nový
typem	typ	k1gInSc7	typ
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
jsou	být	k5eAaImIp3nP	být
buňky	buňka	k1gFnPc1	buňka
multi-linearní	multiinearní	k2eAgFnSc2d1	multi-linearní
diferencované	diferencovaný	k2eAgFnSc2d1	diferencovaná
buňky	buňka	k1gFnSc2	buňka
vytrvávající	vytrvávající	k2eAgInSc4d1	vytrvávající
vliv	vliv	k1gInSc4	vliv
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pluripotentní	pluripotentní	k2eAgFnPc4d1	pluripotentní
buňky	buňka	k1gFnPc4	buňka
nacházející	nacházející	k2eAgFnPc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
tkáních	tkáň	k1gFnPc6	tkáň
včetně	včetně	k7c2	včetně
tukové	tukový	k2eAgFnSc2d1	tuková
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
kostní	kostní	k2eAgFnSc3d1	kostní
dřeni	dřeň	k1gFnSc3	dřeň
<g/>
,	,	kIx,	,
také	také	k9	také
dermálních	dermální	k2eAgInPc2d1	dermální
fibroblastů	fibroblast	k1gInPc2	fibroblast
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
těchto	tento	k3xDgFnPc2	tento
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
Muse	Musa	k1gFnSc6	Musa
cells	cellsa	k1gFnPc2	cellsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
svým	svůj	k3xOyFgNnSc7	svůj
biochemickým	biochemický	k2eAgNnSc7d1	biochemické
vyjádřením	vyjádření	k1gNnSc7	vyjádření
(	(	kIx(	(
<g/>
antigenu	antigen	k1gInSc2	antigen
<g/>
)	)	kIx)	)
SSEA-	SSEA-	k1gFnSc2	SSEA-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vytyčujícího	vytyčující	k2eAgInSc2d1	vytyčující
nediferencované	diferencovaný	k2eNgFnPc4d1	nediferencovaná
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
zvýrazňovačem	zvýrazňovač	k1gInSc7	zvýrazňovač
obecných	obecný	k2eAgFnPc2d1	obecná
mezenchymálních	mezenchymální	k2eAgFnPc2d1	mezenchymální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
CD	CD	kA	CD
<g/>
105	[number]	k4	105
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
momentu	moment	k1gInSc6	moment
vystavení	vystavení	k1gNnSc2	vystavení
napínání	napínání	k1gNnSc2	napínání
buněčné	buněčný	k2eAgFnSc2d1	buněčná
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
shluknou	shluknout	k5eAaPmIp3nP	shluknout
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
podobnému	podobný	k2eAgNnSc3d1	podobné
embryonálnímu	embryonální	k2eAgNnSc3d1	embryonální
formování	formování	k1gNnSc3	formování
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
genových	genový	k2eAgInPc6d1	genový
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
samotné	samotný	k2eAgFnSc3d1	samotná
morfologii	morfologie	k1gFnSc3	morfologie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
činnost	činnost	k1gFnSc4	činnost
Oct	Oct	k1gFnSc1	Oct
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Sox	Sox	k1gFnSc1	Sox
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
and	and	k?	and
Nanog	Nanog	k1gInSc1	Nanog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsoby	způsob	k1gInPc1	způsob
léčby	léčba	k1gFnSc2	léčba
za	za	k7c4	za
využití	využití	k1gNnSc4	využití
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
úspěšně	úspěšně	k6eAd1	úspěšně
praktikovány	praktikován	k2eAgFnPc1d1	praktikována
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
leukemii	leukemie	k1gFnSc4	leukemie
a	a	k8xC	a
typy	typ	k1gInPc4	typ
rakoviny	rakovina	k1gFnSc2	rakovina
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
kostní	kostní	k2eAgFnSc7d1	kostní
tkání	tkáň	k1gFnSc7	tkáň
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
transplantaci	transplantace	k1gFnSc4	transplantace
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
veterinární	veterinární	k2eAgFnSc6d1	veterinární
medicíně	medicína	k1gFnSc6	medicína
využívané	využívaný	k2eAgFnSc6d1	využívaná
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
poranění	poranění	k1gNnSc2	poranění
šlach	šlacha	k1gFnPc2	šlacha
a	a	k8xC	a
vazů	vaz	k1gInPc2	vaz
u	u	k7c2	u
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
využívání	využívání	k1gNnSc2	využívání
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
využívání	využívání	k1gNnSc4	využívání
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
získávání	získávání	k1gNnSc2	získávání
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
není	být	k5eNaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
destrukce	destrukce	k1gFnSc1	destrukce
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odběrech	odběr	k1gInPc6	odběr
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
získávání	získávání	k1gNnSc1	získávání
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
od	od	k7c2	od
určitého	určitý	k2eAgMnSc2d1	určitý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
riziko	riziko	k1gNnSc4	riziko
odmítnutí	odmítnutí	k1gNnSc2	odmítnutí
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
při	při	k7c6	při
navrácení	navrácení	k1gNnSc6	navrácení
těchto	tento	k3xDgFnPc2	tento
buněk	buňka	k1gFnPc2	buňka
tomu	ten	k3xDgNnSc3	ten
samému	samý	k3xTgMnSc3	samý
jedinci	jedinec	k1gMnSc3	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
poznatky	poznatek	k1gInPc7	poznatek
je	být	k5eAaImIp3nS	být
vládou	vláda	k1gFnSc7	vláda
USA	USA	kA	USA
podporované	podporovaný	k2eAgNnSc1d1	podporované
financování	financování	k1gNnSc1	financování
výzkumu	výzkum	k1gInSc2	výzkum
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Buňky	buňka	k1gFnPc1	buňka
fetální	fetálnit	k5eAaPmIp3nP	fetálnit
==	==	k?	==
</s>
</p>
<p>
<s>
Primitivní	primitivní	k2eAgFnPc1d1	primitivní
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
orgánových	orgánový	k2eAgFnPc2d1	orgánová
soustav	soustava	k1gFnPc2	soustava
plodu	plod	k1gInSc2	plod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
odkazováno	odkazován	k2eAgNnSc4d1	odkazováno
jako	jako	k8xS	jako
na	na	k7c4	na
buňky	buňka	k1gFnPc4	buňka
fetální	fetální	k2eAgFnPc4d1	fetální
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
získávány	získávat	k5eAaImNgInP	získávat
po	po	k7c6	po
potratu	potrat	k1gInSc6	potrat
foetu	foet	k1gInSc2	foet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buňky	buňka	k1gFnPc1	buňka
kmenové	kmenový	k2eAgFnPc1d1	kmenová
typu	typ	k1gInSc2	typ
z	z	k7c2	z
foetu	foet	k1gInSc2	foet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
získávány	získávat	k5eAaImNgInP	získávat
po	po	k7c6	po
potratu	potrat	k1gInSc6	potrat
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
schopnosti	schopnost	k1gFnSc2	schopnost
dělení	dělení	k1gNnSc2	dělení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
multipotentní	multipotentní	k2eAgFnPc1d1	multipotentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
membrán	membrána	k1gFnPc2	membrána
zúčastňujících	zúčastňující	k2eAgFnPc2d1	zúčastňující
se	se	k3xPyFc4	se
vývoje	vývoj	k1gInSc2	vývoj
plodu	plod	k1gInSc2	plod
pochází	pocházet	k5eAaImIp3nS	pocházet
tzv.	tzv.	kA	tzv.
Extraembryonální	Extraembryonální	k2eAgFnSc1d1	Extraembryonální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
fetální	fetálnit	k5eAaPmIp3nP	fetálnit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
rozpoznávány	rozpoznáván	k2eAgInPc1d1	rozpoznáván
z	z	k7c2	z
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
rozsahu	rozsah	k1gInSc2	rozsah
dělení	dělení	k1gNnSc2	dělení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pluripotentní	pluripotentní	k2eAgFnPc1d1	pluripotentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Buňky	buňka	k1gFnPc1	buňka
amniotické	amniotický	k2eAgFnPc1d1	amniotický
==	==	k?	==
</s>
</p>
<p>
<s>
Multipotentní	Multipotentní	k2eAgFnPc1d1	Multipotentní
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc6d1	aktivní
formě	forma	k1gFnSc6	forma
přítomné	přítomný	k2eAgFnSc2d1	přítomná
v	v	k7c6	v
plodové	plodový	k2eAgFnSc6d1	plodová
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
expandují	expandovat	k5eAaImIp3nP	expandovat
a	a	k8xC	a
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
bez	bez	k7c2	bez
znaků	znak	k1gInPc2	znak
přítomnosti	přítomnost	k1gFnSc2	přítomnost
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
bujení	bujení	k1gNnSc2	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
amniotické	amniotický	k2eAgFnPc1d1	amniotický
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
diferencovat	diferencovat	k5eAaImF	diferencovat
do	do	k7c2	do
linií	linie	k1gFnPc2	linie
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
tukových	tukový	k2eAgFnPc2d1	tuková
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
kostních	kostní	k2eAgFnPc2d1	kostní
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc2d1	srdeční
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
jaterní	jaterní	k2eAgFnSc2d1	jaterní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
endothelia	endothelium	k1gNnSc2	endothelium
(	(	kIx(	(
<g/>
jednovrtevnatého	jednovrtevnatý	k2eAgInSc2d1	jednovrtevnatý
epitelu	epitel	k1gInSc2	epitel
povrchu	povrch	k1gInSc2	povrch
krevních	krevní	k2eAgFnPc2d1	krevní
a	a	k8xC	a
lymfatických	lymfatický	k2eAgFnPc2d1	lymfatická
cév	céva	k1gFnPc2	céva
a	a	k8xC	a
srdce	srdce	k1gNnSc2	srdce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
zájmu	zájem	k1gInSc6	zájem
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
získávat	získávat	k5eAaImF	získávat
amniotické	amniotický	k2eAgFnPc4d1	amniotický
buňky	buňka	k1gFnPc4	buňka
pro	pro	k7c4	pro
dárce	dárce	k1gMnSc4	dárce
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
autogenické	autogenický	k2eAgFnSc2d1	autogenický
transplantace	transplantace	k1gFnSc2	transplantace
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
odebírání	odebírání	k1gNnSc2	odebírání
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
plodové	plodový	k2eAgFnSc2d1	plodová
vody	voda	k1gFnSc2	voda
obchází	obcházet	k5eAaImIp3nS	obcházet
etické	etický	k2eAgInPc4d1	etický
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
využívání	využívání	k1gNnSc4	využívání
jako	jako	k8xC	jako
zdroje	zdroj	k1gInPc4	zdroj
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
přímo	přímo	k6eAd1	přímo
lidská	lidský	k2eAgNnPc4d1	lidské
embrya	embryo	k1gNnPc4	embryo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Medfordu	Medfordo	k1gNnSc6	Medfordo
v	v	k7c6	v
USA	USA	kA	USA
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
americká	americký	k2eAgFnSc1d1	americká
banka	banka	k1gFnSc1	banka
pro	pro	k7c4	pro
amniotické	amniotický	k2eAgFnPc4d1	amniotický
buňky	buňka	k1gFnPc4	buňka
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Biocell	Biocella	k1gFnPc2	Biocella
Center	centrum	k1gNnPc2	centrum
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
několika	několik	k4yIc7	několik
nemocnicemi	nemocnice	k1gFnPc7	nemocnice
a	a	k8xC	a
výzkumnými	výzkumný	k2eAgNnPc7d1	výzkumné
středisky	středisko	k1gNnPc7	středisko
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Římsko-katolická	římskoatolický	k2eAgFnSc1d1	římsko-katolická
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
od	od	k7c2	od
využívání	využívání	k1gNnSc2	využívání
embrionálních	embrionální	k2eAgFnPc2d1	embrionální
buněk	buňka	k1gFnPc2	buňka
distancovala	distancovat	k5eAaBmAgFnS	distancovat
a	a	k8xC	a
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
využívání	využívání	k1gNnSc1	využívání
těchto	tento	k3xDgFnPc2	tento
buněk	buňka	k1gFnPc2	buňka
k	k	k7c3	k
výzkumným	výzkumný	k2eAgInPc3d1	výzkumný
experimentům	experiment	k1gInPc3	experiment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
linie	linie	k1gFnPc1	linie
==	==	k?	==
</s>
</p>
<p>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
udržování	udržování	k1gNnSc2	udržování
a	a	k8xC	a
obnovy	obnova	k1gFnSc2	obnova
<g/>
,	,	kIx,	,
podstupují	podstupovat	k5eAaImIp3nP	podstupovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dělení	dělení	k1gNnSc2	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
symetrického	symetrický	k2eAgNnSc2d1	symetrické
dělení	dělení	k1gNnSc2	dělení
vyvstávají	vyvstávat	k5eAaImIp3nP	vyvstávat
dvě	dva	k4xCgFnPc1	dva
identické	identický	k2eAgFnPc1d1	identická
dceřiné	dceřiný	k2eAgFnPc1d1	dceřiná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
původní	původní	k2eAgFnSc2d1	původní
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Asymetrické	asymetrický	k2eAgNnSc1d1	asymetrické
dělení	dělení	k1gNnSc1	dělení
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
kmenovou	kmenový	k2eAgFnSc4d1	kmenová
buňku	buňka	k1gFnSc4	buňka
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
buňku	buňka	k1gFnSc4	buňka
prekurzorovou	prekurzorový	k2eAgFnSc4d1	prekurzorový
s	s	k7c7	s
limitovaným	limitovaný	k2eAgInSc7d1	limitovaný
potenciálem	potenciál	k1gInSc7	potenciál
neustálé	neustálý	k2eAgFnSc2d1	neustálá
obnovy	obnova	k1gFnSc2	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Prekurzorová	Prekurzorový	k2eAgFnSc1d1	Prekurzorový
buňka	buňka	k1gFnSc1	buňka
může	moct	k5eAaImIp3nS	moct
před	před	k7c7	před
trvalou	trvalý	k2eAgFnSc7d1	trvalá
změnou	změna	k1gFnSc7	změna
v	v	k7c4	v
zralou	zralý	k2eAgFnSc4d1	zralá
dospělou	dospělý	k2eAgFnSc4d1	dospělá
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
podstoupit	podstoupit	k5eAaPmF	podstoupit
několik	několik	k4yIc4	několik
procesů	proces	k1gInPc2	proces
dělení	dělení	k1gNnSc2	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
podstata	podstata	k1gFnSc1	podstata
molekulární	molekulární	k2eAgFnSc2d1	molekulární
odlišnosti	odlišnost	k1gFnSc2	odlišnost
mezi	mezi	k7c7	mezi
symetrickým	symetrický	k2eAgNnSc7d1	symetrické
a	a	k8xC	a
asymetrickým	asymetrický	k2eAgNnSc7d1	asymetrické
dělením	dělení	k1gNnSc7	dělení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
v	v	k7c6	v
diferencovaném	diferencovaný	k2eAgNnSc6d1	diferencované
oddělení	oddělení	k1gNnSc6	oddělení
bílkovin	bílkovina	k1gFnPc2	bílkovina
buněčné	buněčný	k2eAgFnSc2d1	buněčná
membrány	membrána	k1gFnSc2	membrána
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
receptory	receptor	k1gInPc4	receptor
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
dceřinými	dceřin	k2eAgFnPc7d1	dceřina
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
teorie	teorie	k1gFnSc1	teorie
určuje	určovat	k5eAaImIp3nS	určovat
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nezměné	změný	k2eNgFnPc1d1	změný
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
podmínkám	podmínka	k1gFnPc3	podmínka
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
si	se	k3xPyFc3	se
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
začnou	začít	k5eAaPmIp3nP	začít
navzájem	navzájem	k6eAd1	navzájem
určovat	určovat	k5eAaImF	určovat
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
zamezí	zamezit	k5eAaPmIp3nS	zamezit
tok	tok	k1gInSc1	tok
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
na	na	k7c4	na
Drosophila	Drosophil	k1gMnSc4	Drosophil
germanium	germanium	k1gNnSc1	germanium
identifikovala	identifikovat	k5eAaBmAgFnS	identifikovat
vjem	vjem	k1gInSc1	vjem
(	(	kIx(	(
<g/>
morfogen	morfogen	k1gInSc1	morfogen
-	-	kIx~	-
Dpp	Dpp	k1gFnSc1	Dpp
<g/>
)	)	kIx)	)
junctio	junctio	k6eAd1	junctio
adhaesionis	adhaesionis	k1gInSc1	adhaesionis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
brání	bránit	k5eAaImIp3nS	bránit
diferencování	diferencování	k1gNnSc4	diferencování
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
octomilky	octomilka	k1gFnSc2	octomilka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
používané	používaný	k2eAgFnSc2d1	používaná
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
tzv.	tzv.	kA	tzv.
hematopoetické	hematopoetický	k2eAgFnSc2d1	hematopoetický
kmenové	kmenový	k2eAgFnSc2d1	kmenová
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
HSC	HSC	kA	HSC
<g/>
)	)	kIx)	)
z	z	k7c2	z
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
,	,	kIx,	,
periferní	periferní	k2eAgFnSc2d1	periferní
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
pupečníkové	pupečníkový	k2eAgFnSc2d1	pupečníková
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
lymfoproliferativních	lymfoproliferativní	k2eAgNnPc6d1	lymfoproliferativní
onemocněních	onemocnění	k1gNnPc6	onemocnění
nebo	nebo	k8xC	nebo
vrozených	vrozený	k2eAgInPc6d1	vrozený
imunodeficitech	imunodeficit	k1gInPc6	imunodeficit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klinických	klinický	k2eAgNnPc6d1	klinické
studiích	studio	k1gNnPc6	studio
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
mezenchymální	mezenchymální	k2eAgFnPc4d1	mezenchymální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
z	z	k7c2	z
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
nebo	nebo	k8xC	nebo
pupečníkové	pupečníkový	k2eAgFnSc2d1	pupečníková
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
vyzkoušeny	vyzkoušen	k2eAgFnPc1d1	vyzkoušena
embryonální	embryonální	k2eAgFnPc1d1	embryonální
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
studie	studie	k1gFnPc1	studie
využívající	využívající	k2eAgInSc1d1	využívající
prasečí	prasečí	k2eAgInSc4d1	prasečí
xenograft	xenograft	k1gInSc4	xenograft
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
primárně	primárně	k6eAd1	primárně
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
náhradu	náhrada	k1gFnSc4	náhrada
nefunkční	funkční	k2eNgFnSc2d1	nefunkční
nebo	nebo	k8xC	nebo
zničené	zničený	k2eAgFnSc2d1	zničená
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
mladé	mladý	k2eAgFnPc4d1	mladá
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
produkce	produkce	k1gFnSc2	produkce
mnoha	mnoho	k4c2	mnoho
růstových	růstový	k2eAgFnPc2d1	růstová
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
působků	působek	k1gInPc2	působek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potenciálně	potenciálně	k6eAd1	potenciálně
může	moct	k5eAaImIp3nS	moct
potlačit	potlačit	k5eAaPmF	potlačit
odumírání	odumírání	k1gNnSc4	odumírání
narušené	narušený	k2eAgFnSc2d1	narušená
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
pomáhat	pomáhat	k5eAaImF	pomáhat
v	v	k7c6	v
regeneraci	regenerace	k1gFnSc6	regenerace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratorních	laboratorní	k2eAgFnPc6d1	laboratorní
podmínkách	podmínka	k1gFnPc6	podmínka
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
chováni	chován	k2eAgMnPc1d1	chován
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
Axolotl	axolotl	k1gMnSc1	axolotl
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
specifické	specifický	k2eAgFnSc2d1	specifická
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgFnSc2d1	účinná
formy	forma	k1gFnSc2	forma
regenerace	regenerace	k1gFnSc2	regenerace
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
transplantace	transplantace	k1gFnSc1	transplantace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
:	:	kIx,	:
<g/>
akutní	akutní	k2eAgFnSc1d1	akutní
lymfoblastická	lymfoblastický	k2eAgFnSc1d1	lymfoblastická
leukémie	leukémie	k1gFnSc1	leukémie
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rakovina	rakovina	k1gFnSc1	rakovina
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
s	s	k7c7	s
metastázami	metastáza	k1gFnPc7	metastáza
</s>
</p>
<p>
<s>
neuroblastom	neuroblastom	k1gInSc1	neuroblastom
(	(	kIx(	(
<g/>
=	=	kIx~	=
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
nádor	nádor	k1gInSc1	nádor
z	z	k7c2	z
centrálního	centrální	k2eAgNnSc2d1	centrální
a	a	k8xC	a
periferního	periferní	k2eAgNnSc2d1	periferní
nervstva	nervstvo	k1gNnSc2	nervstvo
<g/>
)	)	kIx)	)
<g/>
experimentální	experimentální	k2eAgFnSc1d1	experimentální
léčba	léčba	k1gFnSc1	léčba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
amyloidóza	amyloidóza	k1gFnSc1	amyloidóza
</s>
</p>
<p>
<s>
zánět	zánět	k1gInSc1	zánět
kloubů	kloub	k1gInPc2	kloub
mladistvých	mladistvý	k2eAgInPc2d1	mladistvý
</s>
</p>
<p>
<s>
různé	různý	k2eAgFnPc1d1	různá
rozvinuté	rozvinutý	k2eAgFnPc1d1	rozvinutá
nádoryPředpokládá	nádoryPředpokládat	k5eAaPmIp3nS	nádoryPředpokládat
se	se	k3xPyFc4	se
možnost	možnost	k1gFnSc1	možnost
použití	použití	k1gNnSc2	použití
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
člověka	člověk	k1gMnSc2	člověk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jakákoliv	jakýkoliv	k3yIgNnPc1	jakýkoliv
autoimunitní	autoimunitní	k2eAgNnPc1d1	autoimunitní
onemocnění	onemocnění	k1gNnPc1	onemocnění
se	s	k7c7	s
závažným	závažný	k2eAgInSc7d1	závažný
průběhem	průběh	k1gInSc7	průběh
</s>
</p>
<p>
<s>
lymfatická	lymfatický	k2eAgFnSc1d1	lymfatická
leukémie	leukémie	k1gFnSc1	leukémie
</s>
</p>
<p>
<s>
akutní	akutní	k2eAgFnSc1d1	akutní
myeloidní	myeloidní	k2eAgFnSc1d1	myeloidní
leukémie	leukémie	k1gFnSc1	leukémie
</s>
</p>
<p>
<s>
rozvinuté	rozvinutý	k2eAgInPc4d1	rozvinutý
nádory	nádor	k1gInPc4	nádor
rakoviny	rakovina	k1gFnSc2	rakovina
prsu	prs	k1gInSc2	prs
a	a	k8xC	a
děložního	děložní	k2eAgNnSc2d1	děložní
hrdla	hrdlo	k1gNnSc2	hrdlo
</s>
</p>
<p>
<s>
chronická	chronický	k2eAgFnSc1d1	chronická
myeloidní	myeloidní	k2eAgFnSc1d1	myeloidní
leukémie	leukémie	k1gFnSc1	leukémie
(	(	kIx(	(
<g/>
lymfom	lymfom	k1gInSc1	lymfom
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
náhrady	náhrada	k1gFnPc1	náhrada
zubů	zub	k1gInPc2	zub
</s>
</p>
<p>
<s>
střevní	střevní	k2eAgInPc1d1	střevní
záněty	zánět	k1gInPc1	zánět
-	-	kIx~	-
m.	m.	k?	m.
Crohnve	Crohnev	k1gFnSc2	Crohnev
vzdálenější	vzdálený	k2eAgFnSc2d2	vzdálenější
budoucnosti	budoucnost	k1gFnSc2	budoucnost
také	také	k9	také
</s>
</p>
<p>
<s>
znovuvytvoření	znovuvytvoření	k1gNnSc1	znovuvytvoření
B-buněk	Buňka	k1gFnPc2	B-buňka
Langerhansových	Langerhansův	k2eAgInPc2d1	Langerhansův
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
slinivce	slinivka	k1gFnSc6	slinivka
břišní	břišní	k2eAgFnSc6d1	břišní
<g/>
,	,	kIx,	,
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
postižených	postižený	k2eAgMnPc2d1	postižený
cukrovkou	cukrovka	k1gFnSc7	cukrovka
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
ochrnutí	ochrnutí	k1gNnSc2	ochrnutí
po	po	k7c6	po
úrazu	úraz	k1gInSc6	úraz
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
srdce	srdce	k1gNnSc2	srdce
po	po	k7c6	po
infarktu	infarkt	k1gInSc6	infarkt
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1	Parkinsonova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
možná	možná	k9	možná
i	i	k9	i
léčba	léčba	k1gFnSc1	léčba
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
<g/>
amyotrofické	amyotrofický	k2eAgFnSc2d1	amyotrofická
laterální	laterální	k2eAgFnSc2d1	laterální
sklerózy	skleróza	k1gFnSc2	skleróza
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
závažných	závažný	k2eAgNnPc2d1	závažné
degenerativních	degenerativní	k2eAgNnPc2d1	degenerativní
onemocnění	onemocnění	k1gNnPc2	onemocnění
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
<g/>
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
diabetu	diabetes	k1gInSc2	diabetes
1	[number]	k4	1
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
dětské	dětský	k2eAgFnSc2d1	dětská
mozkové	mozkový	k2eAgFnSc2d1	mozková
obrny	obrna	k1gFnSc2	obrna
(	(	kIx(	(
<g/>
DMO	DMO	kA	DMO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
roztroušená	roztroušený	k2eAgFnSc1d1	roztroušená
skleróza	skleróza	k1gFnSc1	skleróza
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
komplikací	komplikace	k1gFnPc2	komplikace
diabetu	diabetes	k1gInSc2	diabetes
-	-	kIx~	-
diabetická	diabetický	k2eAgFnSc1d1	diabetická
noha	noha	k1gFnSc1	noha
</s>
</p>
<p>
<s>
ruptura	ruptura	k1gFnSc1	ruptura
rotátorové	rotátorový	k2eAgFnSc2d1	rotátorová
manžety	manžeta	k1gFnSc2	manžeta
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
amyotrofická	amyotrofický	k2eAgFnSc1d1	amyotrofická
laterální	laterální	k2eAgFnSc1d1	laterální
skleróza	skleróza	k1gFnSc1	skleróza
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republikyKmenové	republikyKmenový	k2eAgFnSc2d1	republikyKmenový
buňky	buňka	k1gFnSc2	buňka
myší	myš	k1gFnSc7	myš
dokážou	dokázat	k5eAaPmIp3nP	dokázat
léčit	léčit	k5eAaImF	léčit
poškozená	poškozený	k2eAgNnPc4d1	poškozené
srdce	srdce	k1gNnPc4	srdce
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Legislativa	legislativa	k1gFnSc1	legislativa
===	===	k?	===
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
–	–	k?	–
povolen	povolit	k5eAaPmNgInS	povolit
výzkum	výzkum	k1gInSc1	výzkum
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
z	z	k7c2	z
potratů	potrat	k1gInPc2	potrat
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
zákaz	zákaz	k1gInSc1	zákaz
výzkumu	výzkum	k1gInSc2	výzkum
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
vytvářených	vytvářený	k2eAgNnPc2d1	vytvářené
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
platí	platit	k5eAaImIp3nS	platit
zákaz	zákaz	k1gInSc4	zákaz
výzkumu	výzkum	k1gInSc2	výzkum
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
a	a	k8xC	a
zákaz	zákaz	k1gInSc1	zákaz
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
reprodukce	reprodukce	k1gFnSc2	reprodukce
a	a	k8xC	a
zákaz	zákaz	k1gInSc4	zákaz
zmrazování	zmrazování	k1gNnSc2	zmrazování
embryí	embryo	k1gNnPc2	embryo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schválil	schválit	k5eAaPmAgMnS	schválit
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2004	[number]	k4	2004
italský	italský	k2eAgInSc1d1	italský
parlament	parlament	k1gInSc1	parlament
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
povolen	povolit	k5eAaPmNgInS	povolit
dovoz	dovoz	k1gInSc1	dovoz
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
zákaz	zákaz	k1gInSc1	zákaz
vytváření	vytváření	k1gNnSc2	vytváření
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
USA	USA	kA	USA
neplatí	platit	k5eNaImIp3nS	platit
ze	z	k7c2	z
státních	státní	k2eAgInPc2d1	státní
peněz	peníze	k1gInPc2	peníze
výzkum	výzkum	k1gInSc4	výzkum
na	na	k7c6	na
nových	nový	k2eAgFnPc6d1	nová
buňkách	buňka	k1gFnPc6	buňka
z	z	k7c2	z
lidských	lidský	k2eAgInPc2d1	lidský
zárodků	zárodek	k1gInPc2	zárodek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
při	při	k7c6	při
vynětí	vynětí	k1gNnSc6	vynětí
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
využívat	využívat	k5eAaImF	využívat
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
odebrané	odebraný	k2eAgFnPc4d1	odebraná
ze	z	k7c2	z
zárodků	zárodek	k1gInPc2	zárodek
před	před	k7c7	před
r.	r.	kA	r.
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
uchovávány	uchováván	k2eAgFnPc4d1	uchovávána
a	a	k8xC	a
rozmnožovány	rozmnožován	k2eAgFnPc4d1	rozmnožována
živé	živá	k1gFnPc4	živá
v	v	k7c6	v
živném	živný	k2eAgInSc6d1	živný
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
tedy	tedy	k9	tedy
nelze	lze	k6eNd1	lze
získat	získat	k5eAaPmF	získat
nové	nový	k2eAgFnPc1d1	nová
linie	linie	k1gFnPc1	linie
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
odpůrcem	odpůrce	k1gMnSc7	odpůrce
výzkumu	výzkum	k1gInSc2	výzkum
embryonálních	embryonální	k2eAgFnPc2d1	embryonální
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
náboženské	náboženský	k2eAgFnPc1d1	náboženská
skupiny	skupina	k1gFnPc1	skupina
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
omezení	omezení	k1gNnSc1	omezení
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
přísnější	přísný	k2eAgNnPc1d2	přísnější
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
restrikcí	restrikce	k1gFnPc2	restrikce
zklamáni	zklamat	k5eAaPmNgMnP	zklamat
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
začal	začít	k5eAaPmAgInS	začít
platit	platit	k5eAaImF	platit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
vytváření	vytváření	k1gNnSc1	vytváření
lidských	lidský	k2eAgNnPc2d1	lidské
embryí	embryo	k1gNnPc2	embryo
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
</s>
</p>
<p>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
tento	tento	k3xDgInSc4	tento
Bushův	Bushův	k2eAgInSc4d1	Bushův
zákon	zákon	k1gInSc4	zákon
9	[number]	k4	9
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
vetoval	vetovat	k5eAaBmAgMnS	vetovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
na	na	k7c4	na
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
z	z	k7c2	z
pupečníkové	pupečníkový	k2eAgFnSc2d1	pupečníková
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
nevztahují	vztahovat	k5eNaImIp3nP	vztahovat
žádná	žádný	k3yNgNnPc1	žádný
legislativní	legislativní	k2eAgNnPc1d1	legislativní
omezení	omezení	k1gNnPc1	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
požehnání	požehnání	k1gNnSc4	požehnání
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
Vatikán	Vatikán	k1gInSc1	Vatikán
investoval	investovat	k5eAaBmAgInS	investovat
do	do	k7c2	do
výzkumu	výzkum	k1gInSc2	výzkum
dospělých	dospělý	k2eAgFnPc2d1	dospělá
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nádorové	nádorový	k2eAgFnPc1d1	nádorová
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
==	==	k?	==
</s>
</p>
<p>
<s>
Nádorová	nádorový	k2eAgFnSc1d1	nádorová
kmenová	kmenový	k2eAgFnSc1d1	kmenová
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
nádorová	nádorový	k2eAgFnSc1d1	nádorová
buňka	buňka	k1gFnSc1	buňka
schopná	schopný	k2eAgFnSc1d1	schopná
intenzívního	intenzívní	k2eAgNnSc2d1	intenzívní
dělení	dělení	k1gNnSc2	dělení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vytvořit	vytvořit	k5eAaPmF	vytvořit
nádor	nádor	k1gInSc4	nádor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
existenci	existence	k1gFnSc6	existence
nádorových	nádorový	k2eAgFnPc2d1	nádorová
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
donedávna	donedávna	k6eAd1	donedávna
nevědělo	vědět	k5eNaImAgNnS	vědět
-	-	kIx~	-
svými	svůj	k3xOyFgMnPc7	svůj
experimenty	experiment	k1gInPc1	experiment
jejich	jejich	k3xOp3gFnSc4	jejich
existenci	existence	k1gFnSc4	existence
uvnitř	uvnitř	k7c2	uvnitř
nádorů	nádor	k1gInPc2	nádor
prokázal	prokázat	k5eAaPmAgMnS	prokázat
až	až	k9	až
John	John	k1gMnSc1	John
E.	E.	kA	E.
Dick	Dick	k1gMnSc1	Dick
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nalezl	naleznout	k5eAaPmAgInS	naleznout
populaci	populace	k1gFnSc4	populace
nádorových	nádorový	k2eAgFnPc2d1	nádorová
buněk	buňka	k1gFnPc2	buňka
akutní	akutní	k2eAgFnSc2d1	akutní
myeloidní	myeloidní	k2eAgFnSc2d1	myeloidní
leukemie	leukemie	k1gFnSc2	leukemie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
atypický	atypický	k2eAgInSc4d1	atypický
receptor	receptor	k1gInSc4	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
něco	něco	k6eAd1	něco
velmi	velmi	k6eAd1	velmi
podobného	podobný	k2eAgInSc2d1	podobný
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
typů	typ	k1gInPc2	typ
rakoviny	rakovina	k1gFnSc2	rakovina
a	a	k8xC	a
ač	ač	k8xS	ač
objev	objev	k1gInSc4	objev
nádorových	nádorový	k2eAgFnPc2d1	nádorová
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
existenci	existence	k1gFnSc4	existence
většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
nepochybuje	pochybovat	k5eNaImIp3nS	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
zacílit	zacílit	k5eAaPmF	zacílit
léčbu	léčba	k1gFnSc4	léčba
právě	právě	k9	právě
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
významný	významný	k2eAgInSc1d1	významný
pokrok	pokrok	k1gInSc1	pokrok
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
mnoha	mnoho	k4c2	mnoho
onkologickým	onkologický	k2eAgMnPc3d1	onkologický
pacientům	pacient	k1gMnPc3	pacient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Indukovaná	indukovaný	k2eAgFnSc1d1	indukovaná
pluripotentní	pluripotentní	k2eAgFnSc1d1	pluripotentní
kmenová	kmenový	k2eAgFnSc1d1	kmenová
buňka	buňka	k1gFnSc1	buňka
</s>
</p>
<p>
<s>
Oprava	oprava	k1gFnSc1	oprava
DNA	DNA	kA	DNA
</s>
</p>
<p>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
plazma	plazma	k1gFnSc1	plazma
</s>
</p>
<p>
<s>
Krevní	krevní	k2eAgNnSc4d1	krevní
sérum	sérum	k1gNnSc4	sérum
</s>
</p>
<p>
<s>
Erytrocyty	erytrocyt	k1gInPc1	erytrocyt
</s>
</p>
<p>
<s>
Leukocyty	leukocyt	k1gInPc1	leukocyt
</s>
</p>
<p>
<s>
Trombocyty	trombocyt	k1gInPc1	trombocyt
</s>
</p>
<p>
<s>
Klonování	klonování	k1gNnSc1	klonování
</s>
</p>
<p>
<s>
Partenogeneze	partenogeneze	k1gFnSc1	partenogeneze
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kmenová	kmenový	k2eAgFnSc1d1	kmenová
buňka	buňka	k1gFnSc1	buňka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pupečníková	pupečníkový	k2eAgFnSc1d1	pupečníková
krev	krev	k1gFnSc1	krev
–	–	k?	–
zdroj	zdroj	k1gInSc1	zdroj
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
</s>
</p>
<p>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
panenské	panenský	k2eAgNnSc1d1	panenské
embryo	embryo	k1gNnSc1	embryo
–	–	k?	–
oplodnění	oplodnění	k1gNnSc1	oplodnění
vajíčka	vajíčko	k1gNnSc2	vajíčko
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
samčí	samčí	k2eAgFnPc4d1	samčí
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
(	(	kIx(	(
<g/>
partenogeneze	partenogeneze	k1gFnPc4	partenogeneze
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Embryonální	embryonální	k2eAgFnPc1d1	embryonální
kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
trápí	trápit	k5eAaImIp3nP	trápit
mutace	mutace	k1gFnPc4	mutace
</s>
</p>
<p>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
léčí	léčit	k5eAaImIp3nP	léčit
srdce	srdce	k1gNnSc4	srdce
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
–	–	k?	–
růstový	růstový	k2eAgInSc1d1	růstový
faktor	faktor	k1gInSc1	faktor
IGF1	IGF1	k1gFnSc2	IGF1
a	a	k8xC	a
protein	protein	k1gInSc1	protein
WNT	WNT	kA	WNT
<g/>
5	[number]	k4	5
<g/>
a	a	k8xC	a
produkované	produkovaný	k2eAgInPc1d1	produkovaný
kmenovými	kmenový	k2eAgFnPc7d1	kmenová
buňkami	buňka	k1gFnPc7	buňka
(	(	kIx(	(
<g/>
vstříknutými	vstříknutý	k2eAgFnPc7d1	vstříknutá
do	do	k7c2	do
myši	myš	k1gFnSc2	myš
<g/>
)	)	kIx)	)
léčí	léčit	k5eAaImIp3nP	léčit
embrya	embryo	k1gNnPc1	embryo
myší	myší	k2eAgNnPc1d1	myší
bez	bez	k7c2	bez
tří	tři	k4xCgInPc2	tři
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
normální	normální	k2eAgNnSc4d1	normální
srdce	srdce	k1gNnSc4	srdce
</s>
</p>
<p>
<s>
Banka	banka	k1gFnSc1	banka
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
</s>
</p>
<p>
<s>
Koně	kůň	k1gMnPc1	kůň
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nP	léčit
kmenovými	kmenový	k2eAgFnPc7d1	kmenová
buňkami	buňka	k1gFnPc7	buňka
</s>
</p>
<p>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
–	–	k?	–
Vajíčka	vajíčko	k1gNnSc2	vajíčko
z	z	k7c2	z
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Lidským	lidský	k2eAgFnPc3d1	lidská
kmenovým	kmenový	k2eAgFnPc3d1	kmenová
buňkám	buňka	k1gFnPc3	buňka
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
napravit	napravit	k5eAaPmF	napravit
porušenou	porušený	k2eAgFnSc4d1	porušená
míchu	mícha	k1gFnSc4	mícha
vhodné	vhodný	k2eAgNnSc4d1	vhodné
"	"	kIx"	"
<g/>
lešení	lešení	k1gNnSc4	lešení
<g/>
"	"	kIx"	"
v	v	k7c6	v
internetovém	internetový	k2eAgInSc6d1	internetový
časopise	časopis	k1gInSc6	časopis
OSEL	osel	k1gMnSc1	osel
(	(	kIx(	(
<g/>
Open	Open	k1gMnSc1	Open
Source	Source	k1gMnSc1	Source
E-Learning	E-Learning	k1gInSc1	E-Learning
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
nádorech	nádor	k1gInPc6	nádor
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
<g/>
Etická	etický	k2eAgFnSc1d1	etická
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
využití	využití	k1gNnSc6	využití
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.mrc.ac.uk	[url]	k6eAd1	http://www.mrc.ac.uk
–	–	k?	–
Medical	Medical	k1gMnSc1	Medical
Research	Research	k1gMnSc1	Research
Council	Council	k1gMnSc1	Council
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.linacre.org/	[url]	k?	http://www.linacre.org/
–	–	k?	–
katolické	katolický	k2eAgInPc4d1	katolický
názory	názor	k1gInPc4	názor
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
</s>
</p>
