<s>
Kleptomanie	kleptomanie	k1gFnSc1	kleptomanie
(	(	kIx(	(
<g/>
Řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
κ	κ	k?	κ
<g/>
,	,	kIx,	,
kleptein	kleptein	k1gInSc1	kleptein
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
krást	krást	k5eAaImF	krást
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
μ	μ	k?	μ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
mánie	mánie	k1gFnSc1	mánie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chorobná	chorobný	k2eAgFnSc1d1	chorobná
a	a	k8xC	a
nezadržitelná	zadržitelný	k2eNgFnSc1d1	nezadržitelná
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
odcizování	odcizování	k1gNnSc6	odcizování
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
poruchou	porucha	k1gFnSc7	porucha
cítí	cítit	k5eAaImIp3nS	cítit
nutkání	nutkání	k1gNnSc1	nutkání
odcizovat	odcizovat	k5eAaImF	odcizovat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
malou	malý	k2eAgFnSc4d1	malá
či	či	k8xC	či
nulovou	nulový	k2eAgFnSc4d1	nulová
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
pera	pero	k1gNnSc2	pero
<g/>
,	,	kIx,	,
balíčky	balíček	k1gInPc1	balíček
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
kancelářské	kancelářský	k2eAgFnSc2d1	kancelářská
svorky	svorka	k1gFnSc2	svorka
atd.	atd.	kA	atd.
Někteří	některý	k3yIgMnPc1	některý
si	se	k3xPyFc3	se
svého	svůj	k3xOyFgNnSc2	svůj
jednání	jednání	k1gNnSc2	jednání
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
.	.	kIx.	.
</s>
<s>
Porucha	porucha	k1gFnSc1	porucha
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
pubertě	puberta	k1gFnSc6	puberta
a	a	k8xC	a
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
do	do	k7c2	do
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
je	být	k5eAaImIp3nS	být
kleptomanie	kleptomanie	k1gFnSc1	kleptomanie
provázena	provázet	k5eAaImNgFnS	provázet
dalšími	další	k2eAgFnPc7d1	další
psychickými	psychický	k2eAgFnPc7d1	psychická
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
poruchy	porucha	k1gFnSc2	porucha
je	být	k5eAaImIp3nS	být
doporučována	doporučovat	k5eAaImNgFnS	doporučovat
kognitivně	kognitivně	k6eAd1	kognitivně
behaviorální	behaviorální	k2eAgFnSc1d1	behaviorální
terapie	terapie	k1gFnSc1	terapie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
léčba	léčba	k1gFnSc1	léčba
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
indikují	indikovat	k5eAaBmIp3nP	indikovat
psychofarmaka	psychofarmakum	k1gNnPc4	psychofarmakum
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krádež	krádež	k1gFnSc4	krádež
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
obohacení	obohacení	k1gNnSc4	obohacení
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
adrenalinu	adrenalin	k1gInSc2	adrenalin
při	při	k7c6	při
činu	čin	k1gInSc6	čin
<g/>
.	.	kIx.	.
</s>
<s>
Krádež	krádež	k1gFnSc1	krádež
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kleptomanie	kleptomanie	k1gFnSc2	kleptomanie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
