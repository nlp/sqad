<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Carlo	Carla	k1gMnSc5	Carla
Borromeo	Borromea	k1gMnSc5	Borromea
<g/>
,	,	kIx,	,
latinizovaně	latinizovaně	k6eAd1	latinizovaně
<g/>
:	:	kIx,	:
Carolus	Carolus	k1gMnSc1	Carolus
Borromeus	Borromeus	k1gMnSc1	Borromeus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1538	[number]	k4	1538
<g/>
,	,	kIx,	,
Arona	Arona	k1gFnSc1	Arona
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1584	[number]	k4	1584
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
