<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Carlo	Carla	k1gMnSc5	Carla
Borromeo	Borromea	k1gMnSc5	Borromea
<g/>
,	,	kIx,	,
latinizovaně	latinizovaně	k6eAd1	latinizovaně
<g/>
:	:	kIx,	:
Carolus	Carolus	k1gMnSc1	Carolus
Borromeus	Borromeus	k1gMnSc1	Borromeus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1538	[number]	k4	1538
<g/>
,	,	kIx,	,
Arona	Arona	k1gFnSc1	Arona
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1584	[number]	k4	1584
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
biskupství	biskupství	k1gNnSc2	biskupství
Lugano	Lugana	k1gFnSc5	Lugana
<g/>
,	,	kIx,	,
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Boromejského	Boromejský	k2eAgNnSc2d1	Boromejský
sdružení	sdružení	k1gNnSc2	sdružení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
duchovních	duchovní	k2eAgMnPc2d1	duchovní
správců	správce	k1gMnPc2	správce
<g/>
,	,	kIx,	,
seminářů	seminář	k1gInPc2	seminář
<g/>
,	,	kIx,	,
boromejek	boromejka	k1gFnPc2	boromejka
a	a	k8xC	a
proti	proti	k7c3	proti
moru	mor	k1gInSc3	mor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
spřízněním	spříznění	k1gNnSc7	spříznění
se	se	k3xPyFc4	se
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Piem	Pius	k1gMnSc7	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
vysoké	vysoký	k2eAgInPc4d1	vysoký
církevní	církevní	k2eAgInPc4d1	církevní
posty	post	k1gInPc4	post
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
chudým	chudý	k1gMnPc3	chudý
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
46	[number]	k4	46
letech	léto	k1gNnPc6	léto
vyčerpán	vyčerpán	k2eAgMnSc1d1	vyčerpán
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
hraběte	hrabě	k1gMnSc4	hrabě
Gilberta	Gilbert	k1gMnSc4	Gilbert
Borromeo	Borromeo	k6eAd1	Borromeo
a	a	k8xC	a
Margherity	Margherit	k1gInPc1	Margherit
Medici	medik	k1gMnPc1	medik
v	v	k7c6	v
Lombardii	Lombardie	k1gFnSc6	Lombardie
na	na	k7c6	na
Boromejském	Boromejský	k2eAgInSc6d1	Boromejský
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Arona	Aron	k1gInSc2	Aron
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mala	mal	k1gInSc2	mal
ministroval	ministrovat	k5eAaImAgInS	ministrovat
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
majitelem	majitel	k1gMnSc7	majitel
tamějšího	tamější	k2eAgInSc2d1	tamější
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mu	on	k3xPp3gMnSc3	on
náležely	náležet	k5eAaImAgFnP	náležet
desátky	desátka	k1gFnPc1	desátka
z	z	k7c2	z
obročí	obročí	k1gNnSc2	obročí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
příjmy	příjem	k1gInPc4	příjem
Karel	Karla	k1gFnPc2	Karla
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
rozdával	rozdávat	k5eAaImAgInS	rozdávat
chudým	chudý	k2eAgInSc7d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Pavie	Pavie	k1gFnSc2	Pavie
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaImAgMnP	věnovat
spíše	spíše	k9	spíše
zahálce	zahálka	k1gFnSc3	zahálka
a	a	k8xC	a
světskému	světský	k2eAgNnSc3d1	světské
hýření	hýření	k1gNnSc3	hýření
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
ponořil	ponořit	k5eAaPmAgInS	ponořit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
tak	tak	k6eAd1	tak
hluboko	hluboko	k6eAd1	hluboko
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
musel	muset	k5eAaImAgMnS	muset
několikrát	několikrát	k6eAd1	několikrát
přerušit	přerušit	k5eAaPmF	přerušit
kvůli	kvůli	k7c3	kvůli
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1559	[number]	k4	1559
Karel	Karla	k1gFnPc2	Karla
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
obhájil	obhájit	k5eAaPmAgMnS	obhájit
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
teze	teze	k1gFnSc2	teze
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
doktorem	doktor	k1gMnSc7	doktor
obojího	obojí	k4xRgMnSc2	obojí
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
prorocky	prorocky	k6eAd1	prorocky
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Karel	Karel	k1gMnSc1	Karel
udělá	udělat	k5eAaPmIp3nS	udělat
velké	velký	k2eAgFnPc4d1	velká
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
zářit	zářit	k5eAaImF	zářit
jako	jako	k9	jako
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Karlův	Karlův	k2eAgMnSc1d1	Karlův
strýc	strýc	k1gMnSc1	strýc
Jan	Jan	k1gMnSc1	Jan
Angelo	Angela	k1gFnSc5	Angela
Medici	medik	k1gMnPc1	medik
dosedl	dosednout	k5eAaPmAgInS	dosednout
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
stolec	stolec	k1gInSc4	stolec
jako	jako	k8xC	jako
Pius	Pius	k1gMnSc1	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
byl	být	k5eAaImAgMnS	být
odmala	odmala	k6eAd1	odmala
strýcovým	strýcův	k2eAgMnSc7d1	strýcův
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
papež	papež	k1gMnSc1	papež
brzy	brzy	k6eAd1	brzy
pozval	pozvat	k5eAaPmAgMnS	pozvat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
nejprve	nejprve	k6eAd1	nejprve
svým	svůj	k3xOyFgInSc7	svůj
tajným	tajný	k2eAgInSc7d1	tajný
sekretářem	sekretář	k1gInSc7	sekretář
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
kardinálem-jáhnem	kardinálemáhno	k1gNnSc7	kardinálem-jáhno
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgMnS	svěřit
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
mnišskými	mnišský	k2eAgInPc7d1	mnišský
řády	řád	k1gInPc7	řád
františkánů	františkán	k1gMnPc2	františkán
<g/>
,	,	kIx,	,
karmelitánů	karmelitán	k1gMnPc2	karmelitán
a	a	k8xC	a
Řádem	řád	k1gInSc7	řád
Maltézských	maltézský	k2eAgMnPc2d1	maltézský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
sledovala	sledovat	k5eAaImAgFnS	sledovat
Karlův	Karlův	k2eAgInSc4d1	Karlův
rychlý	rychlý	k2eAgInSc4d1	rychlý
karierní	karierní	k2eAgInSc4d1	karierní
postup	postup	k1gInSc4	postup
s	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
obavami	obava	k1gFnPc7	obava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čas	čas	k1gInSc1	čas
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
starosti	starost	k1gFnPc1	starost
byly	být	k5eAaImAgFnP	být
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Federica	Federicus	k1gMnSc2	Federicus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1562	[number]	k4	1562
Karel	Karel	k1gMnSc1	Karel
ještě	ještě	k9	ještě
zpřísnil	zpřísnit	k5eAaPmAgMnS	zpřísnit
svůj	svůj	k3xOyFgInSc4	svůj
asketický	asketický	k2eAgInSc4d1	asketický
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
prováděl	provádět	k5eAaImAgInS	provádět
kající	kající	k2eAgNnSc4d1	kající
duchovní	duchovní	k2eAgNnSc4d1	duchovní
cvičení	cvičení	k1gNnSc4	cvičení
podle	podle	k7c2	podle
sv.	sv.	kA	sv.
Ignáce	Ignác	k1gMnSc4	Ignác
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
spal	spát	k5eAaImAgMnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
stát	stát	k5eAaImF	stát
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1563	[number]	k4	1563
přijal	přijmout	k5eAaPmAgInS	přijmout
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
kněžské	kněžský	k2eAgFnSc2d1	kněžská
dráhy	dráha	k1gFnSc2	dráha
musel	muset	k5eAaImAgMnS	muset
překonat	překonat	k5eAaPmF	překonat
vadu	vada	k1gFnSc4	vada
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
úspěšně	úspěšně	k6eAd1	úspěšně
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
katolických	katolický	k2eAgMnPc2d1	katolický
kazatelů	kazatel	k1gMnPc2	kazatel
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobových	dobový	k2eAgNnPc2d1	dobové
svědectví	svědectví	k1gNnPc2	svědectví
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jemný	jemný	k2eAgInSc1d1	jemný
a	a	k8xC	a
distingovaný	distingovaný	k2eAgMnSc1d1	distingovaný
člověk	člověk	k1gMnSc1	člověk
zdvořilého	zdvořilý	k2eAgNnSc2d1	zdvořilé
vystupování	vystupování	k1gNnSc2	vystupování
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nerad	nerad	k2eAgMnSc1d1	nerad
pouštěl	pouštět	k5eAaImAgMnS	pouštět
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
příčilo	příčit	k5eAaImAgNnS	příčit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
fyzické	fyzický	k2eAgNnSc1d1	fyzické
násilí	násilí	k1gNnSc1	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
tichým	tichý	k2eAgInSc7d1	tichý
a	a	k8xC	a
laskavým	laskavý	k2eAgInSc7d1	laskavý
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prý	prý	k9	prý
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nesmál	smát	k5eNaImAgMnS	smát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
byl	být	k5eAaImAgMnS	být
vždy	vždy	k6eAd1	vždy
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
neústupný	ústupný	k2eNgInSc4d1	neústupný
<g/>
,	,	kIx,	,
neznal	neznat	k5eAaImAgMnS	neznat
kompromisy	kompromis	k1gInPc4	kompromis
<g/>
;	;	kIx,	;
v	v	k7c6	v
jednání	jednání	k1gNnSc6	jednání
byl	být	k5eAaImAgInS	být
důsledný	důsledný	k2eAgMnSc1d1	důsledný
<g/>
,	,	kIx,	,
až	až	k9	až
pedantský	pedantský	k2eAgInSc1d1	pedantský
<g/>
,	,	kIx,	,
systematický	systematický	k2eAgInSc1d1	systematický
<g/>
.	.	kIx.	.
</s>
<s>
Askezí	askeze	k1gFnPc2	askeze
a	a	k8xC	a
dobročinností	dobročinnost	k1gFnPc2	dobročinnost
si	se	k3xPyFc3	se
získával	získávat	k5eAaImAgMnS	získávat
oblibu	obliba	k1gFnSc4	obliba
prostého	prostý	k2eAgInSc2d1	prostý
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
ho	on	k3xPp3gMnSc4	on
vzápětí	vzápětí	k6eAd1	vzápětí
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
biskupem	biskup	k1gMnSc7	biskup
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
oficiálně	oficiálně	k6eAd1	oficiálně
roku	rok	k1gInSc2	rok
1565	[number]	k4	1565
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
sboru	sbor	k1gInSc2	sbor
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
životním	životní	k2eAgNnSc7d1	životní
působištěm	působiště	k1gNnSc7	působiště
a	a	k8xC	a
posledním	poslední	k2eAgNnSc7d1	poslední
zastavením	zastavení	k1gNnSc7	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
a	a	k8xC	a
vizitaci	vizitace	k1gFnSc6	vizitace
své	svůj	k3xOyFgFnSc2	svůj
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
Karel	Karel	k1gMnSc1	Karel
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
velmi	velmi	k6eAd1	velmi
špatný	špatný	k2eAgInSc4d1	špatný
stav	stav	k1gInSc4	stav
jak	jak	k8xC	jak
po	po	k7c4	po
morální	morální	k2eAgNnSc4d1	morální
<g/>
,	,	kIx,	,
tak	tak	k9	tak
po	po	k7c6	po
správní	správní	k2eAgFnSc6d1	správní
a	a	k8xC	a
finanční	finanční	k2eAgFnSc6d1	finanční
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
začal	začít	k5eAaPmAgInS	začít
provádět	provádět	k5eAaImF	provádět
mnohé	mnohý	k2eAgFnPc4d1	mnohá
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
psát	psát	k5eAaImF	psát
předpisy	předpis	k1gInPc4	předpis
a	a	k8xC	a
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Zakládal	zakládat	k5eAaImAgMnS	zakládat
semináře	seminář	k1gInPc4	seminář
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
kněze	kněz	k1gMnPc4	kněz
založil	založit	k5eAaPmAgMnS	založit
teologickou	teologický	k2eAgFnSc4d1	teologická
fakultu	fakulta	k1gFnSc4	fakulta
v	v	k7c4	v
Pavii	Pavie	k1gFnSc4	Pavie
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
po	po	k7c6	po
Justině	Justina	k1gFnSc6	Justina
z	z	k7c2	z
Padovy	Padova	k1gFnSc2	Padova
sepsal	sepsat	k5eAaPmAgMnS	sepsat
reformní	reformní	k2eAgInPc4d1	reformní
předpisy	předpis	k1gInPc4	předpis
pro	pro	k7c4	pro
milánskou	milánský	k2eAgFnSc4d1	Milánská
diecézi	diecéze	k1gFnSc4	diecéze
Acta	Act	k1gInSc2	Act
ecclesiae	ecclesiae	k1gNnSc2	ecclesiae
Mediolanensis	Mediolanensis	k1gFnSc2	Mediolanensis
sepsal	sepsat	k5eAaPmAgMnS	sepsat
příručku	příručka	k1gFnSc4	příručka
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
chrámové	chrámový	k2eAgNnSc4d1	chrámové
vybavení	vybavení	k1gNnSc4	vybavení
(	(	kIx(	(
<g/>
preciosa	preciosa	k1gFnSc1	preciosa
a	a	k8xC	a
paramenta	paramenta	k1gNnPc4	paramenta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
zavedl	zavést	k5eAaPmAgMnS	zavést
zpovědnice	zpovědnice	k1gFnPc4	zpovědnice
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
kabiny	kabina	k1gFnSc2	kabina
s	s	k7c7	s
mřížkou	mřížka	k1gFnSc7	mřížka
mezi	mezi	k7c7	mezi
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
věřícím	věřící	k1gMnSc7	věřící
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
při	při	k7c6	při
zpovědi	zpověď	k1gFnSc6	zpověď
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
potřebné	potřebný	k2eAgNnSc4d1	potřebné
soukromí	soukromí	k1gNnSc4	soukromí
až	až	k8xS	až
anonymitu	anonymita	k1gFnSc4	anonymita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zpovídalo	zpovídat	k5eAaImAgNnS	zpovídat
zpravidla	zpravidla	k6eAd1	zpravidla
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
v	v	k7c6	v
boční	boční	k2eAgFnSc6d1	boční
chrámové	chrámový	k2eAgFnSc6d1	chrámová
lodi	loď	k1gFnSc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
reformní	reformní	k2eAgFnSc6d1	reformní
práci	práce	k1gFnSc6	práce
musel	muset	k5eAaImAgMnS	muset
Karel	Karel	k1gMnSc1	Karel
překonávat	překonávat	k5eAaImF	překonávat
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
nenávist	nenávist	k1gFnSc4	nenávist
svých	svůj	k3xOyFgMnPc2	svůj
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Reformoval	reformovat	k5eAaBmAgMnS	reformovat
ženské	ženský	k2eAgInPc4d1	ženský
kláštery	klášter	k1gInPc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
reformě	reforma	k1gFnSc3	reforma
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
řeholníci	řeholník	k1gMnPc1	řeholník
humiliáti	humiliát	k1gMnPc1	humiliát
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Breře	Brera	k1gFnSc6	Brera
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1569	[number]	k4	1569
se	se	k3xPyFc4	se
čtveřice	čtveřice	k1gFnSc1	čtveřice
humiliátů	humiliát	k1gMnPc2	humiliát
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
oděvu	oděv	k1gInSc6	oděv
pokusila	pokusit	k5eAaPmAgFnS	pokusit
v	v	k7c6	v
arcibiskupově	arcibiskupův	k2eAgFnSc6d1	arcibiskupova
kapli	kaple	k1gFnSc6	kaple
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Karla	Karel	k1gMnSc4	Karel
Boromejského	Boromejský	k2eAgMnSc4d1	Boromejský
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
společné	společný	k2eAgFnSc2d1	společná
modlitby	modlitba	k1gFnSc2	modlitba
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Girolamo	Girolama	k1gFnSc5	Girolama
Donati	Donat	k2eAgMnPc1d1	Donat
zvaný	zvaný	k2eAgInSc1d1	zvaný
il	il	k?	il
Farina	Farina	k1gFnSc1	Farina
na	na	k7c4	na
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
zezadu	zezadu	k6eAd1	zezadu
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
z	z	k7c2	z
arkebuzy	arkebuza	k1gFnSc2	arkebuza
<g/>
.	.	kIx.	.
</s>
<s>
Kulka	kulka	k1gFnSc1	kulka
prostřelila	prostřelit	k5eAaPmAgFnS	prostřelit
Karlův	Karlův	k2eAgInSc4d1	Karlův
oděv	oděv	k1gInSc4	oděv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezranila	zranit	k5eNaPmAgFnS	zranit
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Atentátník	atentátník	k1gMnSc1	atentátník
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
komplicové	komplic	k1gMnPc1	komplic
byli	být	k5eAaImAgMnP	být
dopadeni	dopadnout	k5eAaPmNgMnP	dopadnout
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
oběšeni	oběsit	k5eAaPmNgMnP	oběsit
<g/>
,	,	kIx,	,
řeholní	řeholní	k2eAgFnSc2d1	řeholní
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
Breře	Brera	k1gFnSc6	Brera
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1571	[number]	k4	1571
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nejobtížnější	obtížný	k2eAgNnSc1d3	nejobtížnější
bylo	být	k5eAaImAgNnS	být
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
alpských	alpský	k2eAgNnPc2d1	alpské
údolí	údolí	k1gNnPc2	údolí
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
pokleslá	pokleslý	k2eAgFnSc1d1	pokleslá
morálka	morálka	k1gFnSc1	morálka
a	a	k8xC	a
rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
různé	různý	k2eAgFnPc1d1	různá
nešvary	nešvara	k1gFnPc1	nešvara
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
konkubinát	konkubinát	k1gInSc1	konkubinát
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
hereze	hereze	k1gFnSc2	hereze
či	či	k8xC	či
používání	používání	k1gNnSc2	používání
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
Karel	Karel	k1gMnSc1	Karel
vždy	vždy	k6eAd1	vždy
tvrdě	tvrdě	k6eAd1	tvrdě
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
,	,	kIx,	,
provinilce	provinilec	k1gMnSc2	provinilec
neváhal	váhat	k5eNaImAgMnS	váhat
posílat	posílat	k5eAaImF	posílat
na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
problémové	problémový	k2eAgFnSc6d1	problémová
oblasti	oblast	k1gFnSc6	oblast
děkan	děkan	k1gMnSc1	děkan
velšského	velšský	k2eAgInSc2d1	velšský
původu	původ	k1gInSc2	původ
Owen	Owena	k1gFnPc2	Owena
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
šířilo	šířit	k5eAaImAgNnS	šířit
učení	učení	k1gNnSc1	učení
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kalvinismus	kalvinismus	k1gInSc1	kalvinismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Karel	Karel	k1gMnSc1	Karel
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
herezi	hereze	k1gFnSc4	hereze
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
odvracet	odvracet	k5eAaImF	odvracet
a	a	k8xC	a
přivádět	přivádět	k5eAaImF	přivádět
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
kázáním	kázání	k1gNnSc7	kázání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zastrašováním	zastrašování	k1gNnSc7	zastrašování
a	a	k8xC	a
pronásledováním	pronásledování	k1gNnSc7	pronásledování
nekatolických	katolický	k2eNgMnPc2d1	nekatolický
kazatelů	kazatel	k1gMnPc2	kazatel
<g/>
.	.	kIx.	.
</s>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
militantní	militantní	k2eAgInSc1d1	militantní
postoj	postoj	k1gInSc1	postoj
se	se	k3xPyFc4	se
nejtragičtěji	tragicky	k6eAd3	tragicky
projevil	projevit	k5eAaPmAgInS	projevit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
antverpskému	antverpský	k2eAgInSc3d1	antverpský
obchodníku	obchodník	k1gMnSc6	obchodník
s	s	k7c7	s
hedvábím	hedvábí	k1gNnSc7	hedvábí
Georgovi	Georgův	k2eAgMnPc1d1	Georgův
de	de	k?	de
Ghese	Ghese	k1gFnPc4	Ghese
<g/>
,	,	kIx,	,
působícímu	působící	k2eAgInSc3d1	působící
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Ghese	Ghese	k1gFnSc1	Ghese
byl	být	k5eAaImAgMnS	být
kalvinista	kalvinista	k1gMnSc1	kalvinista
a	a	k8xC	a
udržoval	udržovat	k5eAaImAgMnS	udržovat
úzké	úzký	k2eAgInPc4d1	úzký
styky	styk	k1gInPc4	styk
s	s	k7c7	s
protestantskou	protestantský	k2eAgFnSc7d1	protestantská
komunitou	komunita	k1gFnSc7	komunita
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
Karel	Karel	k1gMnSc1	Karel
nechal	nechat	k5eAaPmAgMnS	nechat
dvakrát	dvakrát	k6eAd1	dvakrát
uvěznit	uvěznit	k5eAaPmF	uvěznit
a	a	k8xC	a
po	po	k7c6	po
útěku	útěk	k1gInSc6	útěk
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
za	za	k7c4	za
kacířství	kacířství	k1gNnSc4	kacířství
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
byl	být	k5eAaImAgInS	být
Ghese	Ghese	k1gFnSc2	Ghese
uškrcen	uškrcen	k2eAgInSc1d1	uškrcen
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
spáleno	spálit	k5eAaPmNgNnS	spálit
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
o	o	k7c4	o
justiční	justiční	k2eAgFnSc4d1	justiční
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Ghese	Ghese	k1gFnSc1	Ghese
je	být	k5eAaImIp3nS	být
protestantskými	protestantský	k2eAgFnPc7d1	protestantská
církvemi	církev	k1gFnPc7	církev
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k9	jako
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
neblahé	blahý	k2eNgInPc4d1	neblahý
důsledky	důsledek	k1gInPc4	důsledek
měla	mít	k5eAaImAgFnS	mít
Karlova	Karlův	k2eAgFnSc1d1	Karlova
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	s	k7c7	s
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
Ludwigem	Ludwig	k1gMnSc7	Ludwig
Pfyfferem	Pfyffer	k1gMnSc7	Pfyffer
<g/>
.	.	kIx.	.
</s>
<s>
Karlova	Karlův	k2eAgFnSc1d1	Karlova
podpora	podpora	k1gFnSc1	podpora
krutého	krutý	k2eAgNnSc2d1	kruté
a	a	k8xC	a
mocichtivého	mocichtivý	k2eAgMnSc2d1	mocichtivý
kondotiéra	kondotiér	k1gMnSc2	kondotiér
Pfyffera	Pfyffer	k1gMnSc2	Pfyffer
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
vleklé	vleklý	k2eAgInPc4d1	vleklý
pohraniční	pohraniční	k2eAgInPc4d1	pohraniční
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
po	po	k7c6	po
Karlově	Karlův	k2eAgFnSc6d1	Karlova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
roku	rok	k1gInSc2	rok
1586	[number]	k4	1586
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
appenzellského	appenzellský	k2eAgInSc2d1	appenzellský
kantonu	kanton	k1gInSc2	kanton
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
znesvářené	znesvářený	k2eAgFnPc4d1	znesvářená
části	část	k1gFnPc4	část
-	-	kIx~	-
protestanstkou	protestanstka	k1gFnSc7	protestanstka
a	a	k8xC	a
katolickou	katolický	k2eAgFnSc7d1	katolická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1576	[number]	k4	1576
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
propukla	propuknout	k5eAaPmAgFnS	propuknout
obrovská	obrovský	k2eAgFnSc1d1	obrovská
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
během	během	k7c2	během
téměř	téměř	k6eAd1	téměř
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
na	na	k7c4	na
10.000	[number]	k4	10.000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
největší	veliký	k2eAgFnSc7d3	veliký
zatěžkávací	zatěžkávací	k2eAgFnSc7d1	zatěžkávací
zkouškou	zkouška	k1gFnSc7	zkouška
Karlových	Karlův	k2eAgFnPc2d1	Karlova
organizačních	organizační	k2eAgFnPc2d1	organizační
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
morální	morální	k2eAgFnSc2d1	morální
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
zavedl	zavést	k5eAaPmAgMnS	zavést
přísný	přísný	k2eAgInSc4d1	přísný
hygienický	hygienický	k2eAgInSc4d1	hygienický
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
dezinfekci	dezinfekce	k1gFnSc4	dezinfekce
(	(	kIx(	(
<g/>
ocet	ocet	k1gInSc1	ocet
<g/>
,	,	kIx,	,
nehašené	hašený	k2eNgNnSc1d1	nehašené
vápno	vápno	k1gNnSc1	vápno
<g/>
,	,	kIx,	,
vykuřování	vykuřování	k1gNnSc1	vykuřování
<g/>
,	,	kIx,	,
vypalování	vypalování	k1gNnSc1	vypalování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izolaci	izolace	k1gFnSc4	izolace
nakažených	nakažený	k2eAgFnPc2d1	nakažená
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
provizorní	provizorní	k2eAgFnPc4d1	provizorní
stavby	stavba	k1gFnPc4	stavba
špitálů	špitál	k1gInPc2	špitál
a	a	k8xC	a
hromadné	hromadný	k2eAgNnSc1d1	hromadné
pohřbívání	pohřbívání	k1gNnSc1	pohřbívání
do	do	k7c2	do
šachet	šachta	k1gFnPc2	šachta
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdě	tvrdě	k6eAd1	tvrdě
a	a	k8xC	a
nekompromisně	kompromisně	k6eNd1	kompromisně
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
plnění	plnění	k1gNnSc4	plnění
a	a	k8xC	a
střetával	střetávat	k5eAaImAgMnS	střetávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
zejména	zejména	k9	zejména
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
majetných	majetný	k2eAgFnPc2d1	majetná
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
izolaci	izolace	k1gFnSc4	izolace
nakažených	nakažený	k2eAgMnPc2d1	nakažený
a	a	k8xC	a
naprostou	naprostý	k2eAgFnSc4d1	naprostá
likvidaci	likvidace	k1gFnSc4	likvidace
jejich	jejich	k3xOp3gInPc2	jejich
osobních	osobní	k2eAgInPc2d1	osobní
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
vniřního	vniřní	k2eAgNnSc2d1	vniřní
vybavení	vybavení	k1gNnSc2	vybavení
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
zámků	zámek	k1gInPc2	zámek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
postupně	postupně	k6eAd1	postupně
rozprodal	rozprodat	k5eAaPmAgMnS	rozprodat
veškerý	veškerý	k3xTgInSc4	veškerý
rodinný	rodinný	k2eAgInSc4d1	rodinný
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
financoval	financovat	k5eAaBmAgInS	financovat
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
zbídačelých	zbídačelý	k2eAgMnPc2d1	zbídačelý
obyvatel	obyvatel	k1gMnPc2	obyvatel
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
špitály	špitál	k1gInPc7	špitál
a	a	k8xC	a
novými	nový	k2eAgInPc7d1	nový
předpisy	předpis	k1gInPc7	předpis
přikazoval	přikazovat	k5eAaImAgMnS	přikazovat
kněžím	kněz	k1gMnPc3	kněz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kdykoliv	kdykoliv	k6eAd1	kdykoliv
sloužili	sloužit	k5eAaImAgMnP	sloužit
nemocným	mocný	k2eNgMnPc3d1	nemocný
a	a	k8xC	a
zaopatřovali	zaopatřovat	k5eAaImAgMnP	zaopatřovat
umírající	umírající	k1gFnSc4	umírající
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
ke	k	k7c3	k
službě	služba	k1gFnSc3	služba
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
kapucíny	kapucín	k1gMnPc4	kapucín
a	a	k8xC	a
jezuity	jezuita	k1gMnPc4	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
přísnosti	přísnost	k1gFnSc3	přísnost
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
generál	generál	k1gMnSc1	generál
řádu	řád	k1gInSc2	řád
jezuitů	jezuita	k1gMnPc2	jezuita
Everard	Everard	k1gMnSc1	Everard
Mercurian	Mercurian	k1gMnSc1	Mercurian
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezuitští	jezuitský	k2eAgMnPc1d1	jezuitský
kněží	kněz	k1gMnPc1	kněz
mají	mít	k5eAaImIp3nP	mít
především	především	k6eAd1	především
sloužit	sloužit	k5eAaImF	sloužit
živým	živý	k2eAgInSc7d1	živý
a	a	k8xC	a
zdravým	zdravý	k2eAgInSc7d1	zdravý
<g/>
,	,	kIx,	,
ne	ne	k9	ne
umírat	umírat	k5eAaImF	umírat
s	s	k7c7	s
nakaženými	nakažený	k2eAgMnPc7d1	nakažený
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
příkladně	příkladně	k6eAd1	příkladně
<g/>
,	,	kIx,	,
nemocným	nemocný	k1gMnSc7	nemocný
obstarával	obstarávat	k5eAaImAgMnS	obstarávat
ošacení	ošacení	k1gNnSc4	ošacení
<g/>
,	,	kIx,	,
léky	lék	k1gInPc4	lék
i	i	k8xC	i
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
jednání	jednání	k1gNnSc4	jednání
tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
znovuobnovení	znovuobnovení	k1gNnSc4	znovuobnovení
rozhovorů	rozhovor	k1gInPc2	rozhovor
mezi	mezi	k7c7	mezi
soupeřícími	soupeřící	k2eAgFnPc7d1	soupeřící
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
přímluvu	přímluva	k1gFnSc4	přímluva
nechal	nechat	k5eAaPmAgMnS	nechat
papež	papež	k1gMnSc1	papež
okamžitě	okamžitě	k6eAd1	okamžitě
závěry	závěra	k1gFnSc2	závěra
koncilu	koncil	k1gInSc2	koncil
zavést	zavést	k5eAaPmF	zavést
do	do	k7c2	do
života	život	k1gInSc2	život
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
protesty	protest	k1gInPc4	protest
některých	některý	k3yIgMnPc2	některý
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Akta	akta	k1gNnPc1	akta
koncilu	koncil	k1gInSc2	koncil
Karel	Karel	k1gMnSc1	Karel
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
arcidiecézi	arcidiecéze	k1gFnSc6	arcidiecéze
tvrdě	tvrdě	k6eAd1	tvrdě
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
za	za	k7c2	za
morové	morový	k2eAgFnSc2d1	morová
rány	rána	k1gFnSc2	rána
mnohé	mnohý	k2eAgMnPc4d1	mnohý
lidi	člověk	k1gMnPc4	člověk
udivovalo	udivovat	k5eAaImAgNnS	udivovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bere	brát	k5eAaImIp3nS	brát
Karel	Karel	k1gMnSc1	Karel
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
chatrnou	chatrný	k2eAgFnSc7d1	chatrná
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
konstitucí	konstituce	k1gFnSc7	konstituce
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
jen	jen	k9	jen
o	o	k7c6	o
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
o	o	k7c6	o
chlebu	chléb	k1gInSc6	chléb
<g/>
,	,	kIx,	,
-	-	kIx~	-
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgFnSc4d1	každodenní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nadlidským	nadlidský	k2eAgNnSc7d1	nadlidské
úsilím	úsilí	k1gNnSc7	úsilí
a	a	k8xC	a
hladověním	hladovění	k1gNnSc7	hladovění
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vyčerpával	vyčerpávat	k5eAaImAgInS	vyčerpávat
a	a	k8xC	a
trpěl	trpět	k5eAaImAgInS	trpět
záchvaty	záchvat	k1gInPc4	záchvat
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
dostal	dostat	k5eAaPmAgInS	dostat
poslední	poslední	k2eAgInSc1d1	poslední
záchvat	záchvat	k1gInSc1	záchvat
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
již	již	k6eAd1	již
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
odolat	odolat	k5eAaPmF	odolat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
legendy	legenda	k1gFnSc2	legenda
vydechl	vydechnout	k5eAaPmAgInS	vydechnout
naposledy	naposledy	k6eAd1	naposledy
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
Podívej	podívat	k5eAaPmRp2nS	podívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
přicházím	přicházet	k5eAaImIp1nS	přicházet
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgInSc2d1	oficiální
životopisu	životopis	k1gInSc2	životopis
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
svého	svůj	k3xOyFgMnSc2	svůj
zpovědníka	zpovědník	k1gMnSc2	zpovědník
<g/>
,	,	kIx,	,
pátera	páter	k1gMnSc2	páter
Adorna	Adorno	k1gNnSc2	Adorno
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
přijmout	přijmout	k5eAaPmF	přijmout
poslední	poslední	k2eAgFnSc4d1	poslední
svátost	svátost	k1gFnSc4	svátost
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
Hned	hned	k6eAd1	hned
a	a	k8xC	a
po	po	k7c6	po
zaopatření	zaopatření	k1gNnSc6	zaopatření
zemřel	zemřít	k5eAaPmAgMnS	zemřít
tiše	tiš	k1gFnPc4	tiš
v	v	k7c4	v
náručí	náručí	k1gNnSc4	náručí
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Owena	Owen	k1gMnSc2	Owen
Lewise	Lewise	k1gFnSc2	Lewise
<g/>
..	..	k?	..
Po	po	k7c6	po
slavném	slavný	k2eAgInSc6d1	slavný
pohřbu	pohřeb	k1gInSc6	pohřeb
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
poctami	pocta	k1gFnPc7	pocta
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
milánského	milánský	k2eAgInSc2d1	milánský
dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
mnohde	mnohde	k6eAd1	mnohde
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
oslavován	oslavovat	k5eAaImNgMnS	oslavovat
jako	jako	k9	jako
světec	světec	k1gMnSc1	světec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1602	[number]	k4	1602
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
blahořečen	blahořečen	k2eAgMnSc1d1	blahořečen
a	a	k8xC	a
o	o	k7c4	o
8	[number]	k4	8
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
Pavlem	Pavel	k1gMnSc7	Pavel
V.	V.	kA	V.
svatořečen	svatořečit	k5eAaBmNgInS	svatořečit
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
kostelů	kostel	k1gInPc2	kostel
distribuovány	distribuovat	k5eAaBmNgFnP	distribuovat
drobné	drobný	k2eAgFnPc1d1	drobná
částečky	částečka	k1gFnPc1	částečka
z	z	k7c2	z
Karlova	Karlův	k2eAgNnSc2d1	Karlovo
těla	tělo	k1gNnSc2	tělo
jako	jako	k8xS	jako
primární	primární	k2eAgFnSc2d1	primární
relikvie	relikvie	k1gFnSc2	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
adjustovány	adjustován	k2eAgInPc1d1	adjustován
v	v	k7c6	v
zasklené	zasklený	k2eAgFnSc6d1	zasklená
kapsli	kapsle	k1gFnSc6	kapsle
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
5-10	[number]	k4	5-10
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Druhotnými	druhotný	k2eAgFnPc7d1	druhotná
relikviemi	relikvie	k1gFnPc7	relikvie
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
Karlovy	Karlův	k2eAgInPc1d1	Karlův
osobní	osobní	k2eAgInPc1d1	osobní
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
kněžský	kněžský	k2eAgInSc1d1	kněžský
oděv	oděv	k1gInSc1	oděv
<g/>
,	,	kIx,	,
střevíce	střevíc	k1gInPc1	střevíc
<g/>
,	,	kIx,	,
štóla	štóla	k1gFnSc1	štóla
<g/>
,	,	kIx,	,
kardinálský	kardinálský	k2eAgInSc1d1	kardinálský
klobouk	klobouk	k1gInSc1	klobouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
i	i	k9	i
s	s	k7c7	s
autentikou	autentika	k1gFnSc7	autentika
o	o	k7c6	o
původu	původ	k1gInSc6	původ
získal	získat	k5eAaPmAgInS	získat
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
Karlskirche	Karlskirche	k1gInSc1	Karlskirche
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
v	v	k7c6	v
chrámovém	chrámový	k2eAgNnSc6d1	chrámové
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
s	s	k7c7	s
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
fyziognomií	fyziognomie	k1gFnSc7	fyziognomie
(	(	kIx(	(
<g/>
orlí	orlí	k2eAgInSc1d1	orlí
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
přísná	přísný	k2eAgFnSc1d1	přísná
tvář	tvář	k1gFnSc1	tvář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oděn	oděn	k2eAgInSc1d1	oděn
v	v	k7c6	v
kardinálském	kardinálský	k2eAgInSc6d1	kardinálský
purpuru	purpur	k1gInSc6	purpur
nebo	nebo	k8xC	nebo
v	v	k7c6	v
arcibiskupské	arcibiskupský	k2eAgFnSc6d1	arcibiskupská
kasuli	kasule	k1gFnSc6	kasule
a	a	k8xC	a
pluviálu	pluviál	k1gInSc6	pluviál
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
atributy	atribut	k1gInPc1	atribut
bývají	bývat	k5eAaImIp3nP	bývat
krucifix	krucifix	k1gInSc4	krucifix
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
někdy	někdy	k6eAd1	někdy
důtky	důtka	k1gFnPc4	důtka
nebo	nebo	k8xC	nebo
provaz	provaz	k1gInSc4	provaz
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
jako	jako	k8xS	jako
symboly	symbol	k1gInPc4	symbol
pokání	pokání	k1gNnSc2	pokání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
legendárních	legendární	k2eAgFnPc6d1	legendární
scénách	scéna	k1gFnPc6	scéna
často	často	k6eAd1	často
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
ve	v	k7c6	v
špitálu	špitál	k1gInSc6	špitál
nemocné	nemocná	k1gFnSc2	nemocná
morem	mor	k1gInSc7	mor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
obraz	obraz	k1gInSc1	obraz
Karla	Karel	k1gMnSc2	Karel
Škréty	Škréta	k1gMnSc2	Škréta
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
ve	v	k7c6	v
zpovědnici	zpovědnice	k1gFnSc6	zpovědnice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
klečí	klečet	k5eAaImIp3nS	klečet
před	před	k7c7	před
oltářem	oltář	k1gInSc7	oltář
s	s	k7c7	s
Pannou	Panna	k1gFnSc7	Panna
Marií	Maria	k1gFnPc2	Maria
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc7	svůj
přímluvkyní	přímluvkyně	k1gFnSc7	přímluvkyně
<g/>
,	,	kIx,	,
na	na	k7c6	na
oblaku	oblak	k1gInSc6	oblak
bývá	bývat	k5eAaImIp3nS	bývat
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
při	při	k7c6	při
Nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
<g/>
,	,	kIx,	,
klaní	klanit	k5eAaImIp3nS	klanit
se	se	k3xPyFc4	se
Kristovu	Kristův	k2eAgInSc3d1	Kristův
kříži	kříž	k1gInSc3	kříž
a	a	k8xC	a
nástrojům	nástroj	k1gInPc3	nástroj
jeho	jeho	k3xOp3gNnPc2	jeho
umučení	umučení	k1gNnSc2	umučení
<g/>
,	,	kIx,	,
či	či	k8xC	či
Nejsvětější	nejsvětější	k2eAgFnSc4d1	nejsvětější
Trojici	trojice	k1gFnSc4	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Karlu	Karel	k1gMnSc3	Karel
Boromejskému	Boromejský	k2eAgMnSc3d1	Boromejský
je	být	k5eAaImIp3nS	být
zasvěceno	zasvětit	k5eAaPmNgNnS	zasvětit
mnoho	mnoho	k4c1	mnoho
kostelů	kostel	k1gInPc2	kostel
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
;	;	kIx,	;
kněžské	kněžský	k2eAgInPc1d1	kněžský
semináře	seminář	k1gInPc1	seminář
a	a	k8xC	a
chlapecká	chlapecký	k2eAgNnPc1d1	chlapecké
gymnasia	gymnasion	k1gNnPc1	gymnasion
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
dóm	dóm	k1gInSc1	dóm
<g/>
,	,	kIx,	,
pohřební	pohřební	k2eAgFnSc1d1	pohřební
krypta	krypta	k1gFnSc1	krypta
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
ostatky	ostatek	k1gInPc7	ostatek
v	v	k7c6	v
sarkofágu	sarkofág	k1gInSc6	sarkofág
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
chrámy	chrám	k1gInPc1	chrám
<g/>
:	:	kIx,	:
San	San	k1gMnSc5	San
Carlo	Carla	k1gMnSc5	Carla
alle	allus	k1gMnSc5	allus
Quattro	Quattra	k1gMnSc5	Quattra
Fontane	Fontan	k1gInSc5	Fontan
řádu	řád	k1gInSc2	řád
trinitářů	trinitář	k1gMnPc2	trinitář
<g/>
;	;	kIx,	;
bazilika	bazilika	k1gFnSc1	bazilika
San	San	k1gMnSc1	San
Ambroggio	Ambroggio	k1gMnSc1	Ambroggio
e	e	k0	e
Carlo	Carlo	k1gNnSc1	Carlo
al	ala	k1gFnPc2	ala
corso	corsa	k1gFnSc5	corsa
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Carlo	Carlo	k1gNnSc1	Carlo
ai	ai	k?	ai
Catinari	Catinari	k1gNnSc1	Catinari
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
chrám	chrám	k1gInSc1	chrám
svatých	svatý	k1gMnPc2	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1730-1783	[number]	k4	1730-1783
katolický	katolický	k2eAgInSc4d1	katolický
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
špitálem	špitál	k1gInSc7	špitál
pro	pro	k7c4	pro
přestárlé	přestárlý	k2eAgMnPc4d1	přestárlý
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
špitální	špitální	k2eAgFnSc1d1	špitální
kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
ve	v	k7c6	v
Vlašské	vlašský	k2eAgFnSc6d1	vlašská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
proměněná	proměněný	k2eAgFnSc1d1	proměněná
v	v	k7c4	v
obytný	obytný	k2eAgInSc4d1	obytný
dům	dům	k1gInSc4	dům
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
Italského	italský	k2eAgNnSc2d1	italské
kulturního	kulturní	k2eAgNnSc2d1	kulturní
střediska	středisko	k1gNnSc2	středisko
<g/>
)	)	kIx)	)
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
slavný	slavný	k2eAgInSc1d1	slavný
Karlskirche	Karlskirche	k1gInSc1	Karlskirche
na	na	k7c4	na
Karlsplatz	Karlsplatz	k1gInSc4	Karlsplatz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založil	založit	k5eAaPmAgInS	založit
pražský	pražský	k2eAgInSc1d1	pražský
Řád	řád	k1gInSc1	řád
křižovníků	křižovník	k1gMnPc2	křižovník
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
hvězdou	hvězda	k1gFnSc7	hvězda
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Odkaz	odkaz	k1gInSc1	odkaz
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sdílí	sdílet	k5eAaImIp3nS	sdílet
Kongregace	kongregace	k1gFnSc1	kongregace
Milosrdných	milosrdný	k2eAgFnPc2d1	milosrdná
sester	sestra	k1gFnPc2	sestra
svatého	svatý	k2eAgMnSc2d1	svatý
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
tři	tři	k4xCgInPc4	tři
kláštery	klášter	k1gInPc1	klášter
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
sídlí	sídlet	k5eAaImIp3nS	sídlet
jejich	jejich	k3xOp3gFnSc4	jejich
nemocnici	nemocnice	k1gFnSc4	nemocnice
Pod	pod	k7c7	pod
Petřínem	Petřín	k1gInSc7	Petřín
<g/>
.	.	kIx.	.
</s>
<s>
Boromejky	boromejka	k1gFnPc1	boromejka
působí	působit	k5eAaImIp3nP	působit
také	také	k9	také
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
koleji	kolej	k1gFnSc6	kolej
Nepomucenum	Nepomucenum	k1gInSc4	Nepomucenum
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Arona	Arona	k1gFnSc1	Arona
<g/>
,	,	kIx,	,
kolosální	kolosální	k2eAgFnSc1d1	kolosální
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Karel	Karel	k1gMnSc1	Karel
Boromejského	Boromejský	k2eAgMnSc4d1	Boromejský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
<g/>
,	,	kIx,	,
Sloup	sloup	k1gInSc4	sloup
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc2d1	Boromejský
</s>
