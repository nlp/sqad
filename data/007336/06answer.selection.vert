<s>
Kočky	kočka	k1gFnPc1	kočka
žijící	žijící	k2eAgFnPc1d1	žijící
divoce	divoce	k6eAd1	divoce
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
doma	doma	k6eAd1	doma
chované	chovaný	k2eAgFnPc1d1	chovaná
kastrované	kastrovaný	k2eAgFnPc1d1	kastrovaná
kočky	kočka	k1gFnPc1	kočka
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
