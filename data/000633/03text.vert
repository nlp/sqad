<s>
Antonín	Antonín	k1gMnSc1	Antonín
Jan	Jan	k1gMnSc1	Jan
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Antonín	Antonín	k1gMnSc1	Antonín
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1775	[number]	k4	1775
Hudlice	Hudlice	k1gFnPc4	Hudlice
u	u	k7c2	u
Berouna	Beroun	k1gInSc2	Beroun
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1854	[number]	k4	1854
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
rektor	rektor	k1gMnSc1	rektor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Jan	Jan	k1gMnSc1	Jan
Jungmann	Jungmann	k1gMnSc1	Jungmann
byl	být	k5eAaImAgMnS	být
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
českého	český	k2eAgMnSc2d1	český
obrozeneckého	obrozenecký	k2eAgMnSc2d1	obrozenecký
jazykovědce	jazykovědec	k1gMnSc2	jazykovědec
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
české	český	k2eAgFnSc2d1	Česká
odborné	odborný	k2eAgFnSc2d1	odborná
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
sanskrt	sanskrt	k1gInSc4	sanskrt
a	a	k8xC	a
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
staré	starý	k2eAgFnSc2d1	stará
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
i	i	k9	i
ukázku	ukázka	k1gFnSc4	ukázka
originálních	originální	k2eAgFnPc2d1	originální
"	"	kIx"	"
<g/>
liter	litera	k1gFnPc2	litera
dévanágarických	dévanágarický	k2eAgFnPc2d1	dévanágarický
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
příklad	příklad	k1gInSc1	příklad
čtení	čtení	k1gNnSc2	čtení
původního	původní	k2eAgInSc2d1	původní
sanskrtského	sanskrtský	k2eAgInSc2d1	sanskrtský
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Významný	významný	k2eAgInSc1d1	významný
překlad	překlad	k1gInSc1	překlad
nejstarší	starý	k2eAgFnSc2d3	nejstarší
části	část	k1gFnSc2	část
eposu	epos	k1gInSc2	epos
Mahabharaty	Mahabharat	k1gMnPc4	Mahabharat
<g/>
,	,	kIx,	,
romantický	romantický	k2eAgInSc1d1	romantický
příběh	příběh	k1gInSc1	příběh
O	o	k7c6	o
Nalovi	Nala	k1gMnSc6	Nala
a	a	k8xC	a
Damajantí	Damajantí	k1gNnPc1	Damajantí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
Orientu	Orient	k1gInSc2	Orient
věnoval	věnovat	k5eAaImAgMnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
též	též	k9	též
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
obsáhlé	obsáhlý	k2eAgFnSc6d1	obsáhlá
obecné	obecný	k2eAgFnSc6d1	obecná
studii	studie	k1gFnSc6	studie
o	o	k7c4	o
antropologii	antropologie	k1gFnSc4	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
studentům	student	k1gMnPc3	student
patřil	patřit	k5eAaImAgInS	patřit
František	František	k1gMnSc1	František
Jan	Jan	k1gMnSc1	Jan
Mošner	Mošner	k1gMnSc1	Mošner
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
olomouckých	olomoucký	k2eAgFnPc2d1	olomoucká
lékařských	lékařský	k2eAgFnPc2d1	lékařská
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
významný	významný	k2eAgMnSc1d1	významný
inovátor	inovátor	k1gMnSc1	inovátor
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
porodnictví	porodnictví	k1gNnSc2	porodnictví
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
k	k	k7c3	k
babení	babení	k1gNnSc3	babení
<g/>
,	,	kIx,	,
1804	[number]	k4	1804
Umění	umění	k1gNnSc2	umění
babické	babický	k2eAgNnSc1d1	babické
<g/>
,	,	kIx,	,
1814	[number]	k4	1814
O	o	k7c6	o
sanskrtu	sanskrt	k1gInSc6	sanskrt
<g/>
,	,	kIx,	,
Krok	krok	k1gInSc1	krok
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
<g/>
:	:	kIx,	:
65-81	[number]	k4	65-81
</s>
