<s>
Antonín	Antonín	k1gMnSc1	Antonín
Jan	Jan	k1gMnSc1	Jan
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Antonín	Antonín	k1gMnSc1	Antonín
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1775	[number]	k4	1775
Hudlice	Hudlice	k1gFnPc4	Hudlice
u	u	k7c2	u
Berouna	Beroun	k1gInSc2	Beroun
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1854	[number]	k4	1854
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
rektor	rektor	k1gMnSc1	rektor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
