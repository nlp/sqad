<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
charakteristika	charakteristika	k1gFnSc1	charakteristika
tepelného	tepelný	k2eAgInSc2d1	tepelný
stavu	stav	k1gInSc2	stav
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
významu	význam	k1gInSc6	význam
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastnost	vlastnost	k1gFnSc1	vlastnost
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
schopen	schopen	k2eAgMnSc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
a	a	k8xC	a
přiřadit	přiřadit	k5eAaPmF	přiřadit
jí	on	k3xPp3gFnSc7	on
pocity	pocit	k1gInPc4	pocit
studeného	studený	k2eAgNnSc2d1	studené
<g/>
,	,	kIx,	,
teplého	teplý	k2eAgNnSc2d1	teplé
či	či	k8xC	či
horkého	horký	k2eAgNnSc2d1	horké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
a	a	k8xC	a
technických	technický	k2eAgFnPc6d1	technická
vědách	věda	k1gFnPc6	věda
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc6	jejich
aplikacích	aplikace	k1gFnPc6	aplikace
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skalární	skalární	k2eAgFnSc1d1	skalární
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
pravděpodobnostnímu	pravděpodobnostní	k2eAgInSc3d1	pravděpodobnostní
charakteru	charakter	k1gInSc3	charakter
vhodná	vhodný	k2eAgFnSc1d1	vhodná
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
stavu	stav	k1gInSc2	stav
ustálených	ustálený	k2eAgInPc2d1	ustálený
makroskopických	makroskopický	k2eAgInPc2d1	makroskopický
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kinetickou	kinetický	k2eAgFnSc7d1	kinetická
energií	energie	k1gFnSc7	energie
částic	částice	k1gFnPc2	částice
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
veličinou	veličina	k1gFnSc7	veličina
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc4	si
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
kelvin	kelvin	k1gInSc4	kelvin
(	(	kIx(	(
<g/>
K	k	k7c3	k
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
jednotkou	jednotka	k1gFnSc7	jednotka
stupeň	stupeň	k1gInSc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
(	(	kIx(	(
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc7d3	nejnižší
možnou	možný	k2eAgFnSc7d1	možná
teplotou	teplota	k1gFnSc7	teplota
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
(	(	kIx(	(
<g/>
0	[number]	k4	0
K	k	k7c3	k
<g/>
;	;	kIx,	;
-273,15	-273,15	k4	-273,15
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
libovolně	libovolně	k6eAd1	libovolně
přiblížit	přiblížit	k5eAaPmF	přiblížit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nelze	lze	k6eNd1	lze
jí	on	k3xPp3gFnSc3	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
měření	měření	k1gNnSc3	měření
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
teploměry	teploměr	k1gInPc1	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
pojmem	pojem	k1gInSc7	pojem
termiky	termika	k1gFnSc2	termika
a	a	k8xC	a
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
veličinou	veličina	k1gFnSc7	veličina
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
tepelných	tepelný	k2eAgInPc2d1	tepelný
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
jevech	jev	k1gInPc6	jev
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
mnohé	mnohý	k2eAgFnPc4d1	mnohá
makroskopické	makroskopický	k2eAgFnPc4d1	makroskopická
mechanické	mechanický	k2eAgFnPc4d1	mechanická
<g/>
,	,	kIx,	,
elektromagnetické	elektromagnetický	k2eAgFnPc4d1	elektromagnetická
i	i	k8xC	i
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
spektra	spektrum	k1gNnSc2	spektrum
oborů	obor	k1gInPc2	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
pojmem	pojem	k1gInSc7	pojem
např.	např.	kA	např.
v	v	k7c6	v
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
ekologii	ekologie	k1gFnSc6	ekologie
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
jako	jako	k9	jako
pojem	pojem	k1gInSc1	pojem
byla	být	k5eAaImAgFnS	být
primárně	primárně	k6eAd1	primárně
zavedena	zavést	k5eAaPmNgFnS	zavést
pro	pro	k7c4	pro
podnět	podnět	k1gInSc4	podnět
či	či	k8xC	či
příčinu	příčina	k1gFnSc4	příčina
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
smyslových	smyslový	k2eAgInPc2d1	smyslový
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
podráždění	podráždění	k1gNnSc2	podráždění
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
i	i	k9	i
její	její	k3xOp3gInSc1	její
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
latinské	latinský	k2eAgNnSc1d1	latinské
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
temperatura	temperatura	k1gFnSc1	temperatura
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
příjemný	příjemný	k2eAgInSc4d1	příjemný
pocit	pocit	k1gInSc4	pocit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
potřeba	potřeba	k6eAd1	potřeba
popsat	popsat	k5eAaPmF	popsat
lépe	dobře	k6eAd2	dobře
tyto	tento	k3xDgInPc4	tento
pocity	pocit	k1gInPc4	pocit
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
snahám	snaha	k1gFnPc3	snaha
kvantifikaci	kvantifikace	k1gFnSc4	kvantifikace
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
působí	působit	k5eAaImIp3nS	působit
změnu	změna	k1gFnSc4	změna
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
nebo	nebo	k8xC	nebo
skupenství	skupenství	k1gNnSc2	skupenství
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
tak	tak	k6eAd1	tak
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
pomocí	pomocí	k7c2	pomocí
viditelných	viditelný	k2eAgInPc2d1	viditelný
projevů	projev	k1gInPc2	projev
indikovat	indikovat	k5eAaBmF	indikovat
velikost	velikost	k1gFnSc4	velikost
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
ji	on	k3xPp3gFnSc4	on
měřit	měřit	k5eAaImF	měřit
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
měření	měření	k1gNnSc1	měření
teploty	teplota	k1gFnSc2	teplota
pomocí	pomocí	k7c2	pomocí
roztažnosti	roztažnost	k1gFnSc2	roztažnost
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
doklady	doklad	k1gInPc1	doklad
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Hérón	Hérón	k1gInSc1	Hérón
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
popsal	popsat	k5eAaPmAgInS	popsat
vzduchový	vzduchový	k2eAgInSc1d1	vzduchový
termoskop	termoskop	k1gInSc1	termoskop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
doloženým	doložený	k2eAgInSc7d1	doložený
přístrojem	přístroj	k1gInSc7	přístroj
k	k	k7c3	k
indikaci	indikace	k1gFnSc3	indikace
tepelných	tepelný	k2eAgInPc2d1	tepelný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
moderní	moderní	k2eAgNnSc4d1	moderní
a	a	k8xC	a
přesné	přesný	k2eAgNnSc4d1	přesné
měření	měření	k1gNnPc4	měření
později	pozdě	k6eAd2	pozdě
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
závislosti	závislost	k1gFnSc3	závislost
elektrických	elektrický	k2eAgInPc2d1	elektrický
jevů	jev	k1gInPc2	jev
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
,	,	kIx,	,
:	:	kIx,	:
T	T	kA	T
,	,	kIx,	,
:	:	kIx,	:
θ	θ	k?	θ
,	,	kIx,	,
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
Theta	Theto	k1gNnPc1	Theto
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
velké	velký	k2eAgInPc4d1	velký
symboly	symbol	k1gInPc4	symbol
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
absolutní	absolutní	k2eAgFnSc4d1	absolutní
(	(	kIx(	(
<g/>
termodynamickou	termodynamický	k2eAgFnSc4d1	termodynamická
<g/>
)	)	kIx)	)
teplotu	teplota	k1gFnSc4	teplota
Jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnPc2	si
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
této	tento	k3xDgFnSc2	tento
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
kelvin	kelvin	k1gInSc1	kelvin
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
K.	K.	kA	K.
1	[number]	k4	1
kelvin	kelvin	k1gInSc1	kelvin
je	být	k5eAaImIp3nS	být
273,16	[number]	k4	273,16
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
používanými	používaný	k2eAgFnPc7d1	používaná
jednotkami	jednotka	k1gFnPc7	jednotka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
stupeň	stupeň	k1gInSc1	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
;	;	kIx,	;
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
=	=	kIx~	=
1	[number]	k4	1
K	K	kA	K
stupeň	stupeň	k1gInSc1	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
<g/>
;	;	kIx,	;
1	[number]	k4	1
°	°	k?	°
<g/>
F	F	kA	F
=	=	kIx~	=
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Oba	dva	k4xCgInPc1	dva
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc1d1	uvedený
vztahy	vztah	k1gInPc1	vztah
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
jednotky	jednotka	k1gFnPc4	jednotka
teplotního	teplotní	k2eAgInSc2d1	teplotní
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
odlišných	odlišný	k2eAgFnPc6d1	odlišná
stupnicích	stupnice	k1gFnPc6	stupnice
s	s	k7c7	s
jinak	jinak	k6eAd1	jinak
nastaveným	nastavený	k2eAgInSc7d1	nastavený
nulovým	nulový	k2eAgInSc7d1	nulový
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
přirozených	přirozený	k2eAgFnPc2d1	přirozená
soustav	soustava	k1gFnPc2	soustava
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
Planckovu	Planckův	k2eAgFnSc4d1	Planckova
teplotu	teplota	k1gFnSc4	teplota
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
Gk	Gk	k1gFnSc1	Gk
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
ħ	ħ	k?	ħ
,	,	kIx,	,
G	G	kA	G
,	,	kIx,	,
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gMnSc1	hbar
,	,	kIx,	,
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
označují	označovat	k5eAaImIp3nP	označovat
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
Planckovu	Planckův	k2eAgFnSc4d1	Planckova
<g/>
,	,	kIx,	,
Newtonovu	Newtonův	k2eAgFnSc4d1	Newtonova
gravitační	gravitační	k2eAgFnSc4d1	gravitační
resp.	resp.	kA	resp.
Boltzmannovu	Boltzmannův	k2eAgFnSc4d1	Boltzmannova
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
CODATA	CODATA	kA	CODATA
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgFnSc1d1	aktuální
hodnota	hodnota	k1gFnSc1	hodnota
TP	TP	kA	TP
=	=	kIx~	=
1,416	[number]	k4	1,416
808	[number]	k4	808
<g/>
(	(	kIx(	(
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
<g/>
·	·	k?	·
<g/>
1032	[number]	k4	1032
K.	K.	kA	K.
Z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnímání	vnímání	k1gNnSc1	vnímání
teplejšího	teplý	k2eAgNnSc2d2	teplejší
a	a	k8xC	a
chladnějšího	chladný	k2eAgNnSc2d2	chladnější
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
objemu	objem	k1gInSc2	objem
látek	látka	k1gFnPc2	látka
<g/>
:	:	kIx,	:
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
pocitem	pocit	k1gInSc7	pocit
teplého	teplé	k1gNnSc2	teplé
se	se	k3xPyFc4	se
objem	objem	k1gInSc1	objem
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Roztažnost	roztažnost	k1gFnSc1	roztažnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
měřítkem	měřítko	k1gNnSc7	měřítko
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc1	průběh
se	se	k3xPyFc4	se
však	však	k9	však
lišil	lišit	k5eAaImAgMnS	lišit
podle	podle	k7c2	podle
použité	použitý	k2eAgFnSc2d1	použitá
teploměrné	teploměrný	k2eAgFnSc2d1	teploměrná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
však	však	k9	však
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
průběhy	průběh	k1gInPc1	průběh
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
při	při	k7c6	při
konstantním	konstantní	k2eAgInSc6d1	konstantní
tlaku	tlak	k1gInSc6	tlak
<g/>
)	)	kIx)	)
vzájemně	vzájemně	k6eAd1	vzájemně
lineární	lineární	k2eAgInSc1d1	lineární
pro	pro	k7c4	pro
zředěné	zředěný	k2eAgInPc4d1	zředěný
plyny	plyn	k1gInPc4	plyn
–	–	k?	–
ty	ten	k3xDgMnPc4	ten
se	se	k3xPyFc4	se
tak	tak	k9	tak
staly	stát	k5eAaPmAgFnP	stát
nejvhodnější	vhodný	k2eAgFnSc7d3	nejvhodnější
teploměrnou	teploměrný	k2eAgFnSc7d1	teploměrná
látkou	látka	k1gFnSc7	látka
pro	pro	k7c4	pro
přesná	přesný	k2eAgNnPc4d1	přesné
měření	měření	k1gNnPc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Lineární	lineární	k2eAgFnSc1d1	lineární
závislost	závislost	k1gFnSc1	závislost
umožnila	umožnit	k5eAaPmAgFnS	umožnit
přesnou	přesný	k2eAgFnSc4d1	přesná
kvantifikaci	kvantifikace	k1gFnSc4	kvantifikace
teploty	teplota	k1gFnSc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jako	jako	k8xS	jako
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
veličiny	veličina	k1gFnPc4	veličina
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
dobře	dobře	k6eAd1	dobře
definovaných	definovaný	k2eAgFnPc2d1	definovaná
teplotních	teplotní	k2eAgFnPc2d1	teplotní
stupnic	stupnice	k1gFnPc2	stupnice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Celsiova	Celsiův	k2eAgFnSc1d1	Celsiova
teplotní	teplotní	k2eAgFnSc1d1	teplotní
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
dostatečně	dostatečně	k6eAd1	dostatečně
zředěné	zředěný	k2eAgInPc4d1	zředěný
plyny	plyn	k1gInPc4	plyn
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
směrnice	směrnice	k1gFnSc1	směrnice
závislosti	závislost	k1gFnSc2	závislost
objemu	objem	k1gInSc2	objem
na	na	k7c6	na
Celsiově	Celsiův	k2eAgFnSc6d1	Celsiova
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
při	při	k7c6	při
konstantním	konstantní	k2eAgInSc6d1	konstantní
tlaku	tlak	k1gInSc6	tlak
<g/>
)	)	kIx)	)
stejná	stejný	k2eAgFnSc1d1	stejná
a	a	k8xC	a
rovná	rovnat	k5eAaImIp3nS	rovnat
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
273,15	[number]	k4	273,15
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
plynů	plyn	k1gInPc2	plyn
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
stejné	stejný	k2eAgNnSc4d1	stejné
chování	chování	k1gNnSc4	chování
včetně	včetně	k7c2	včetně
číselné	číselný	k2eAgFnSc2d1	číselná
hodnoty	hodnota	k1gFnSc2	hodnota
směrnice	směrnice	k1gFnSc2	směrnice
závislosti	závislost	k1gFnSc2	závislost
tlaku	tlak	k1gInSc2	tlak
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
při	při	k7c6	při
konstantním	konstantní	k2eAgInSc6d1	konstantní
objemu	objem	k1gInSc6	objem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
výhodami	výhoda	k1gFnPc7	výhoda
zavést	zavést	k5eAaPmF	zavést
tzv.	tzv.	kA	tzv.
absolutní	absolutní	k2eAgFnSc4d1	absolutní
teplotu	teplota	k1gFnSc4	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
(	(	kIx(	(
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
:	:	kIx,	:
∘	∘	k?	∘
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
+	+	kIx~	+
273	[number]	k4	273
,	,	kIx,	,
15	[number]	k4	15
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
/	/	kIx~	/
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
+273,15	+273,15	k4	+273,15
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
K	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
se	s	k7c7	s
stupnicí	stupnice	k1gFnSc7	stupnice
počínající	počínající	k2eAgFnSc7d1	počínající
hodnotou	hodnota	k1gFnSc7	hodnota
0	[number]	k4	0
K	K	kA	K
(	(	kIx(	(
<g/>
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
nulovému	nulový	k2eAgInSc3d1	nulový
objemu	objem	k1gInSc3	objem
a	a	k8xC	a
tlaku	tlak	k1gInSc3	tlak
zředěného	zředěný	k2eAgInSc2d1	zředěný
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
objemu	objem	k1gInSc2	objem
ideálního	ideální	k2eAgInSc2d1	ideální
plynu	plyn	k1gInSc2	plyn
při	při	k7c6	při
konstantním	konstantní	k2eAgInSc6d1	konstantní
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
tlaku	tlak	k1gInSc2	tlak
ideálního	ideální	k2eAgInSc2d1	ideální
plynu	plyn	k1gInSc2	plyn
při	při	k7c6	při
konstantním	konstantní	k2eAgInSc6d1	konstantní
objemu	objem	k1gInSc6	objem
<g/>
.	.	kIx.	.
</s>
<s>
Kinetická	kinetický	k2eAgFnSc1d1	kinetická
teorie	teorie	k1gFnSc1	teorie
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vyjádření	vyjádření	k1gNnSc4	vyjádření
tlaku	tlak	k1gInSc2	tlak
pomocí	pomocí	k7c2	pomocí
pohybových	pohybový	k2eAgFnPc2d1	pohybová
vlastností	vlastnost	k1gFnPc2	vlastnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
stavovou	stavový	k2eAgFnSc7d1	stavová
rovnicí	rovnice	k1gFnSc7	rovnice
dala	dát	k5eAaPmAgFnS	dát
absolutní	absolutní	k2eAgFnSc4d1	absolutní
teplotě	teplota	k1gFnSc6	teplota
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
interpretaci	interpretace	k1gFnSc4	interpretace
–	–	k?	–
absolutní	absolutní	k2eAgFnSc6d1	absolutní
teplotě	teplota	k1gFnSc6	teplota
je	být	k5eAaImIp3nS	být
úměrná	úměrný	k2eAgFnSc1d1	úměrná
střední	střední	k2eAgFnSc1d1	střední
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
neuspořádaného	uspořádaný	k2eNgInSc2d1	neuspořádaný
posuvného	posuvný	k2eAgInSc2d1	posuvný
pohybu	pohyb	k1gInSc2	pohyb
molekuly	molekula	k1gFnSc2	molekula
jednoatomového	jednoatomový	k2eAgInSc2d1	jednoatomový
(	(	kIx(	(
<g/>
ideálního	ideální	k2eAgInSc2d1	ideální
<g/>
)	)	kIx)	)
plynu	plyn	k1gInSc2	plyn
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⟩	⟩	k?	⟩
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k2eAgInSc4d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc4	langle
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
kT	kT	k?	kT
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
je	být	k5eAaImIp3nS	být
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
závorkou	závorka	k1gFnSc7	závorka
značena	značen	k2eAgFnSc1d1	značena
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
jednoatomového	jednoatomový	k2eAgInSc2d1	jednoatomový
(	(	kIx(	(
<g/>
ideálního	ideální	k2eAgInSc2d1	ideální
<g/>
)	)	kIx)	)
plynu	plyn	k1gInSc2	plyn
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
R	R	kA	R
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
nRT	nRT	k?	nRT
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
látkové	látkový	k2eAgNnSc1d1	látkové
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
molární	molární	k2eAgFnSc1d1	molární
plynová	plynový	k2eAgFnSc1d1	plynová
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
mírou	míra	k1gFnSc7	míra
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
ideálního	ideální	k2eAgInSc2d1	ideální
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
interpretace	interpretace	k1gFnSc1	interpretace
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
svá	svůj	k3xOyFgNnPc4	svůj
omezení	omezení	k1gNnPc4	omezení
<g/>
:	:	kIx,	:
Teplota	teplota	k1gFnSc1	teplota
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
mírou	míra	k1gFnSc7	míra
velikosti	velikost	k1gFnSc2	velikost
pouze	pouze	k6eAd1	pouze
neuspořádaného	uspořádaný	k2eNgInSc2d1	neuspořádaný
posuvného	posuvný	k2eAgInSc2d1	posuvný
pohybu	pohyb	k1gInSc2	pohyb
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
u	u	k7c2	u
víceatomových	víceatomový	k2eAgFnPc2d1	víceatomový
molekul	molekula	k1gFnPc2	molekula
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
rotace	rotace	k1gFnPc1	rotace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vibrace	vibrace	k1gFnPc4	vibrace
se	s	k7c7	s
stupni	stupeň	k1gInPc7	stupeň
volnosti	volnost	k1gFnSc2	volnost
odpovídajícími	odpovídající	k2eAgFnPc7d1	odpovídající
potenciální	potenciální	k2eAgFnSc7d1	potenciální
energii	energie	k1gFnSc4	energie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začnou	začít	k5eAaPmIp3nP	začít
převládat	převládat	k5eAaImF	převládat
kvantové	kvantový	k2eAgFnPc4d1	kvantová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
ekvipartiční	ekvipartiční	k2eAgInSc1d1	ekvipartiční
teorém	teorém	k1gInSc1	teorém
přestává	přestávat	k5eAaImIp3nS	přestávat
platit	platit	k5eAaImF	platit
<g/>
:	:	kIx,	:
některé	některý	k3yIgInPc4	některý
stupně	stupeň	k1gInPc4	stupeň
volnosti	volnost	k1gFnSc2	volnost
kvůli	kvůli	k7c3	kvůli
diskrétnosti	diskrétnost	k1gFnSc3	diskrétnost
energetických	energetický	k2eAgInPc2d1	energetický
stavů	stav	k1gInPc2	stav
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
dodávaná	dodávaný	k2eAgFnSc1d1	dodávaná
soustavě	soustava	k1gFnSc3	soustava
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
předávat	předávat	k5eAaImF	předávat
nejen	nejen	k6eAd1	nejen
pohybu	pohyb	k1gInSc2	pohyb
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přeměnit	přeměnit	k5eAaPmF	přeměnit
ve	v	k7c4	v
změnu	změna	k1gFnSc4	změna
jejich	jejich	k3xOp3gFnSc2	jejich
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
vůči	vůči	k7c3	vůči
vnějšímu	vnější	k2eAgNnSc3d1	vnější
působení	působení	k1gNnSc3	působení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vnitřním	vnitřní	k2eAgFnPc3d1	vnitřní
silám	síla	k1gFnPc3	síla
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c4	v
nárůst	nárůst	k1gInSc4	nárůst
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
rozmrzání	rozmrzání	k1gNnSc1	rozmrzání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
stupňů	stupeň	k1gInPc2	stupeň
volnosti	volnost	k1gFnSc2	volnost
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
přírůstek	přírůstek	k1gInSc4	přírůstek
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
bez	bez	k7c2	bez
zvýšení	zvýšení	k1gNnSc2	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
;	;	kIx,	;
druhý	druhý	k4xOgInSc1	druhý
případ	případ	k1gInSc1	případ
známý	známý	k2eAgInSc1d1	známý
u	u	k7c2	u
skupenských	skupenský	k2eAgFnPc2d1	skupenská
přeměn	přeměna	k1gFnPc2	přeměna
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
závislosti	závislost	k1gFnSc3	závislost
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
velmi	velmi	k6eAd1	velmi
lišit	lišit	k5eAaImF	lišit
<g/>
:	:	kIx,	:
Zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
jednoatomový	jednoatomový	k2eAgInSc4d1	jednoatomový
plyn	plyn	k1gInSc4	plyn
platí	platit	k5eAaImIp3nS	platit
závislost	závislost	k1gFnSc4	závislost
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
první	první	k4xOgFnSc6	první
mocnině	mocnina	k1gFnSc6	mocnina
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
u	u	k7c2	u
energie	energie	k1gFnSc2	energie
elektronového	elektronový	k2eAgInSc2d1	elektronový
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc6	mocnina
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
u	u	k7c2	u
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
u	u	k7c2	u
fotonového	fotonový	k2eAgInSc2d1	fotonový
plynu	plyn	k1gInSc2	plyn
dokonce	dokonce	k9	dokonce
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
mocnině	mocnina	k1gFnSc3	mocnina
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Kinetická	kinetický	k2eAgFnSc1d1	kinetická
teorie	teorie	k1gFnSc1	teorie
také	také	k9	také
dokázala	dokázat	k5eAaPmAgFnS	dokázat
kvantifikovat	kvantifikovat	k5eAaBmF	kvantifikovat
lokální	lokální	k2eAgFnPc4d1	lokální
fluktuace	fluktuace	k1gFnPc4	fluktuace
v	v	k7c6	v
ustáleném	ustálený	k2eAgInSc6d1	ustálený
systému	systém	k1gInSc6	systém
a	a	k8xC	a
stanovit	stanovit	k5eAaPmF	stanovit
kvadratickou	kvadratický	k2eAgFnSc4d1	kvadratická
neurčitost	neurčitost	k1gFnSc4	neurčitost
teploty	teplota	k1gFnSc2	teplota
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
jednoatomový	jednoatomový	k2eAgInSc4d1	jednoatomový
plyn	plyn	k1gInSc4	plyn
např.	např.	kA	např.
platí	platit	k5eAaImIp3nP	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⟩	⟩	k?	⟩
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
T	T	kA	T
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gFnSc1	langle
<g />
.	.	kIx.	.
</s>
<s>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
tedy	tedy	k8xC	tedy
relativní	relativní	k2eAgFnSc1d1	relativní
kvadratická	kvadratický	k2eAgFnSc1d1	kvadratická
neurčitost	neurčitost	k1gFnSc1	neurčitost
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
odmocninou	odmocnina	k1gFnSc7	odmocnina
počtu	počet	k1gInSc2	počet
částic	částice	k1gFnPc2	částice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
statistický	statistický	k2eAgInSc4d1	statistický
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
chování	chování	k1gNnSc2	chování
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yRgInSc4	který
jsou	být	k5eAaImIp3nP	být
lokální	lokální	k2eAgFnPc1d1	lokální
fluktuace	fluktuace	k1gFnPc1	fluktuace
zanedbatelné	zanedbatelný	k2eAgFnPc1d1	zanedbatelná
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
stavovou	stavový	k2eAgFnSc7d1	stavová
<g/>
,	,	kIx,	,
intenzivní	intenzivní	k2eAgFnSc7d1	intenzivní
termodynamickou	termodynamický	k2eAgFnSc7d1	termodynamická
veličinou	veličina	k1gFnSc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Fenomenologická	fenomenologický	k2eAgFnSc1d1	fenomenologická
termodynamika	termodynamika	k1gFnSc1	termodynamika
zavádí	zavádět	k5eAaImIp3nS	zavádět
obecnou	obecný	k2eAgFnSc4d1	obecná
teplotu	teplota	k1gFnSc4	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
vartheta	vartheto	k1gNnSc2	vartheto
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
a	a	k8xC	a
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
ustálené	ustálený	k2eAgInPc4d1	ustálený
termodynamické	termodynamický	k2eAgInPc4d1	termodynamický
systémy	systém	k1gInPc4	systém
zavést	zavést	k5eAaPmF	zavést
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
monotónní	monotónní	k2eAgFnSc4d1	monotónní
funkci	funkce	k1gFnSc4	funkce
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
homogenního	homogenní	k2eAgInSc2d1	homogenní
ustáleného	ustálený	k2eAgInSc2d1	ustálený
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
parametru	parametr	k1gInSc6	parametr
systému	systém	k1gInSc2	systém
a	a	k8xC	a
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
jeho	jeho	k3xOp3gInPc6	jeho
subsystémech	subsystém	k1gInPc6	subsystém
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
konvence	konvence	k1gFnSc2	konvence
se	se	k3xPyFc4	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
jako	jako	k9	jako
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
funkce	funkce	k1gFnSc1	funkce
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpřesňuje	zpřesňovat	k5eAaImIp3nS	zpřesňovat
pojem	pojem	k1gInSc1	pojem
měření	měření	k1gNnSc2	měření
teploty	teplota	k1gFnSc2	teplota
jako	jako	k8xC	jako
uvedení	uvedení	k1gNnSc2	uvedení
soustavy	soustava	k1gFnSc2	soustava
"	"	kIx"	"
<g/>
měřený	měřený	k2eAgInSc4d1	měřený
systém	systém	k1gInSc4	systém
–	–	k?	–
teploměr	teploměr	k1gInSc1	teploměr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
tepelná	tepelný	k2eAgFnSc1d1	tepelná
výměna	výměna	k1gFnSc1	výměna
<g/>
,	,	kIx,	,
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
postuluje	postulovat	k5eAaImIp3nS	postulovat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
nultá	nultý	k4xOgFnSc1	nultý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
věta	věta	k1gFnSc1	věta
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dva	dva	k4xCgInPc4	dva
systémy	systém	k1gInPc4	systém
v	v	k7c6	v
tepelném	tepelný	k2eAgInSc6d1	tepelný
kontaktu	kontakt	k1gInSc6	kontakt
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
stejnou	stejný	k2eAgFnSc4d1	stejná
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Rovnost	rovnost	k1gFnSc1	rovnost
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
vlastností	vlastnost	k1gFnSc7	vlastnost
tranzitivní	tranzitivní	k2eAgFnSc7d1	tranzitivní
<g/>
:	:	kIx,	:
Rovná	rovnat	k5eAaImIp3nS	rovnat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
prvního	první	k4xOgNnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
systému	systém	k1gInSc2	systém
v	v	k7c6	v
tepelném	tepelný	k2eAgInSc6d1	tepelný
kontaktu	kontakt	k1gInSc6	kontakt
a	a	k8xC	a
rovná	rovnat	k5eAaImIp3nS	rovnat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
druhého	druhý	k4xOgNnSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
systému	systém	k1gInSc2	systém
v	v	k7c6	v
tepelném	tepelný	k2eAgInSc6d1	tepelný
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
se	se	k3xPyFc4	se
i	i	k9	i
teplota	teplota	k1gFnSc1	teplota
prvního	první	k4xOgInSc2	první
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
systému	systém	k1gInSc2	systém
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
budou	být	k5eAaImBp3nP	být
při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
tepelného	tepelný	k2eAgInSc2d1	tepelný
kontaktu	kontakt	k1gInSc2	kontakt
také	také	k9	také
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
stavová	stavový	k2eAgFnSc1d1	stavová
veličina	veličina	k1gFnSc1	veličina
charakterizující	charakterizující	k2eAgFnSc4d1	charakterizující
termodynamickou	termodynamický	k2eAgFnSc4d1	termodynamická
rovnováhu	rovnováha	k1gFnSc4	rovnováha
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
teplot	teplota	k1gFnPc2	teplota
dvou	dva	k4xCgInPc2	dva
systémů	systém	k1gInPc2	systém
můžeme	moct	k5eAaImIp1nP	moct
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
tepelného	tepelný	k2eAgInSc2d1	tepelný
kontaktu	kontakt	k1gInSc2	kontakt
budou	být	k5eAaImBp3nP	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
a	a	k8xC	a
jakým	jaký	k3yIgInSc7	jaký
směrem	směr	k1gInSc7	směr
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
přenos	přenos	k1gInSc4	přenos
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
hlavní	hlavní	k2eAgFnSc2d1	hlavní
věty	věta	k1gFnSc2	věta
termodynamické	termodynamický	k2eAgNnSc1d1	termodynamické
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Carnotova	Carnotův	k2eAgFnSc1d1	Carnotova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
účinnost	účinnost	k1gFnSc1	účinnost
všech	všecek	k3xTgInPc2	všecek
vratných	vratný	k2eAgInPc2d1	vratný
Carnotových	Carnotův	k2eAgInPc2d1	Carnotův
cyklů	cyklus	k1gInPc2	cyklus
pracujících	pracující	k1gFnPc2	pracující
mezi	mezi	k7c7	mezi
danými	daný	k2eAgInPc7d1	daný
dvěma	dva	k4xCgFnPc7	dva
teplotami	teplota	k1gFnPc7	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
či	či	k8xC	či
vnějších	vnější	k2eAgInPc6d1	vnější
parametrech	parametr	k1gInPc6	parametr
ani	ani	k8xC	ani
pracovní	pracovní	k2eAgFnSc6d1	pracovní
látce	látka	k1gFnSc6	látka
<g/>
)	)	kIx)	)
–	–	k?	–
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
<g />
.	.	kIx.	.
</s>
<s hack="1">
funkci	funkce	k1gFnSc4	funkce
pouze	pouze	k6eAd1	pouze
těchto	tento	k3xDgFnPc2	tento
teplot	teplota	k1gFnPc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
=	=	kIx~	=
η	η	k?	η
(	(	kIx(	(
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obecných	obecný	k2eAgFnPc2d1	obecná
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
vratných	vratný	k2eAgInPc6d1	vratný
Carnotových	Carnotův	k2eAgInPc6d1	Carnotův
cyklech	cyklus	k1gInPc6	cyklus
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
psát	psát	k5eAaImF	psát
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
Q	Q	kA	Q
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
(	(	kIx(	(
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
−	−	k?	−
F	F	kA	F
(	(	kIx(	(
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
(	(	kIx(	(
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
-	-	kIx~	-
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gFnSc1	Q_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-F	-F	k?	-F
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
<g/>
Q_	Q_	k1gFnSc2	Q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
teplo	teplo	k1gNnSc4	teplo
přijaté	přijatý	k2eAgNnSc4d1	přijaté
od	od	k7c2	od
ohřívače	ohřívač	k1gInSc2	ohřívač
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
Q_	Q_	k1gFnSc3	Q_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
teplo	teplo	k1gNnSc4	teplo
odevzdané	odevzdaný	k2eAgInPc4d1	odevzdaný
chladiči	chladič	k1gInPc7	chladič
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
(	(	kIx(	(
θ	θ	k?	θ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
)	)	kIx)	)
<g/>
}	}	kIx)	}
nějaká	nějaký	k3yIgFnSc1	nějaký
funkce	funkce	k1gFnSc1	funkce
obecné	obecný	k2eAgFnSc2d1	obecná
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
=	=	kIx~	=
F	F	kA	F
(	(	kIx(	(
θ	θ	k?	θ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Theta	Theta	k1gMnSc1	Theta
=	=	kIx~	=
<g/>
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
)	)	kIx)	)
<g/>
}	}	kIx)	}
nazval	nazvat	k5eAaPmAgMnS	nazvat
Kelvin	kelvin	k1gInSc4	kelvin
termodynamickou	termodynamický	k2eAgFnSc7d1	termodynamická
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
namísto	namísto	k7c2	namísto
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
obecné	obecný	k2eAgFnSc6d1	obecná
teplotě	teplota	k1gFnSc6	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
vartheta	varthet	k1gMnSc2	varthet
}	}	kIx)	}
ji	on	k3xPp3gFnSc4	on
definoval	definovat	k5eAaBmAgMnS	definovat
právě	právě	k6eAd1	právě
pomocí	pomocí	k7c2	pomocí
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
účinnost	účinnost	k1gFnSc4	účinnost
přepsaného	přepsaný	k2eAgInSc2d1	přepsaný
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
dvou	dva	k4xCgFnPc2	dva
termodynamických	termodynamický	k2eAgFnPc2d1	termodynamická
teplot	teplota	k1gFnPc2	teplota
je	být	k5eAaImIp3nS	být
rovný	rovný	k2eAgInSc4d1	rovný
poměru	poměr	k1gInSc6	poměr
tepla	teplo	k1gNnSc2	teplo
odevzdaného	odevzdaný	k2eAgInSc2d1	odevzdaný
chladiči	chladič	k1gInPc7	chladič
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
přijatého	přijatý	k2eAgInSc2d1	přijatý
od	od	k7c2	od
ohřívače	ohřívač	k1gInSc2	ohřívač
při	při	k7c6	při
vratném	vratný	k2eAgInSc6d1	vratný
Carnotově	Carnotův	k2eAgInSc6d1	Carnotův
cyklu	cyklus	k1gInSc6	cyklus
pracujícím	pracující	k2eAgInSc6d1	pracující
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
bodů	bod	k1gInPc2	bod
lze	lze	k6eAd1	lze
takto	takto	k6eAd1	takto
definovat	definovat	k5eAaBmF	definovat
celou	celý	k2eAgFnSc4d1	celá
termodynamickou	termodynamický	k2eAgFnSc4d1	termodynamická
teplotní	teplotní	k2eAgFnSc4d1	teplotní
stupnici	stupnice	k1gFnSc4	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
–	–	k?	–
nulová	nulový	k2eAgFnSc1d1	nulová
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
teplota	teplota	k1gFnSc1	teplota
–	–	k?	–
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
implicitně	implicitně	k6eAd1	implicitně
jako	jako	k8xC	jako
teplota	teplota	k1gFnSc1	teplota
chladiče	chladič	k1gInSc2	chladič
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
má	mít	k5eAaImIp3nS	mít
vratný	vratný	k2eAgInSc1d1	vratný
Carnotův	Carnotův	k2eAgInSc1d1	Carnotův
cyklus	cyklus	k1gInSc1	cyklus
účinnost	účinnost	k1gFnSc1	účinnost
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
však	však	k9	však
podle	podle	k7c2	podle
třetí	třetí	k4xOgFnSc2	třetí
hlavní	hlavní	k2eAgFnSc2d1	hlavní
věty	věta	k1gFnSc2	věta
termodynamické	termodynamický	k2eAgFnPc4d1	termodynamická
nedosažitelná	dosažitelný	k2eNgNnPc4d1	nedosažitelné
<g/>
.	.	kIx.	.
</s>
<s>
Definiční	definiční	k2eAgInSc4d1	definiční
vztah	vztah	k1gInSc4	vztah
lze	lze	k6eAd1	lze
přepsat	přepsat	k5eAaPmF	přepsat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
Θ	Θ	k?	Θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
Q_	Q_	k1gMnSc1	Q_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
obecných	obecný	k2eAgFnPc2d1	obecná
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
vratných	vratný	k2eAgInPc6d1	vratný
cyklech	cyklus	k1gInPc6	cyklus
zobecnit	zobecnit	k5eAaPmF	zobecnit
na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∮	∮	k?	∮
:	:	kIx,	:
↺	↺	k?	↺
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
oint	oint	k2eAgInSc4d1	oint
\	\	kIx~	\
<g/>
limits	limits	k1gInSc4	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circlearrowleft	circlearrowleft	k5eAaPmF	circlearrowleft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
Q	Q	kA	Q
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
platný	platný	k2eAgInSc4d1	platný
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
vratný	vratný	k2eAgInSc4d1	vratný
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
alternativní	alternativní	k2eAgFnSc1d1	alternativní
formulace	formulace	k1gFnSc1	formulace
definice	definice	k1gFnSc2	definice
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
Termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
převrácenou	převrácený	k2eAgFnSc7d1	převrácená
hodnotou	hodnota	k1gFnSc7	hodnota
integračního	integrační	k2eAgInSc2d1	integrační
faktoru	faktor	k1gInSc2	faktor
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vynásobit	vynásobit	k5eAaPmF	vynásobit
diferenciální	diferenciální	k2eAgInSc4d1	diferenciální
přírůstek	přírůstek	k1gInSc4	přírůstek
tepla	teplo	k1gNnSc2	teplo
přijatého	přijatý	k2eAgInSc2d1	přijatý
termodynamickým	termodynamický	k2eAgInSc7d1	termodynamický
systémem	systém	k1gInSc7	systém
při	při	k7c6	při
vratném	vratný	k2eAgInSc6d1	vratný
ději	děj	k1gInSc6	děj
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
dostali	dostat	k5eAaPmAgMnP	dostat
totální	totální	k2eAgInSc4d1	totální
diferenciál	diferenciál	k1gInSc4	diferenciál
(	(	kIx(	(
<g/>
diferenciál	diferenciál	k1gInSc4	diferenciál
entropie	entropie	k1gFnSc2	entropie
<g/>
,	,	kIx,	,
exaktní	exaktní	k2eAgFnSc2d1	exaktní
diferenciální	diferenciální	k2eAgFnSc2d1	diferenciální
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nekoná	konat	k5eNaImIp3nS	konat
práce	práce	k1gFnSc1	práce
(	(	kIx(	(
<g/>
zobecněné	zobecněný	k2eAgFnPc4d1	zobecněná
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vnější	vnější	k2eAgInPc1d1	vnější
extenzivní	extenzivní	k2eAgInPc1d1	extenzivní
parametry	parametr	k1gInPc1	parametr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
x	x	k?	x
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ext	ext	k?	ext
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
např.	např.	kA	např.
objem	objem	k1gInSc1	objem
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
konstantní	konstantní	k2eAgMnPc1d1	konstantní
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemění	měnit	k5eNaImIp3nP	měnit
se	se	k3xPyFc4	se
počty	počet	k1gInPc4	počet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N_	N_	k1gMnSc6	N_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
částic	částice	k1gFnPc2	částice
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vztah	vztah	k1gInSc4	vztah
přepsat	přepsat	k5eAaPmF	přepsat
jako	jako	k8xS	jako
definiční	definiční	k2eAgInSc4d1	definiční
vztah	vztah	k1gInSc4	vztah
teploty	teplota	k1gFnSc2	teplota
pomocí	pomocí	k7c2	pomocí
entropie	entropie	k1gFnSc2	entropie
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
S	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
x	x	k?	x
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
S	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partiat	k5eAaBmAgMnS	partiat
<g />
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ext	ext	k?	ext
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
N_	N_	k1gFnSc1	N_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
\	\	kIx~	\
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
termodynamice	termodynamika	k1gFnSc6	termodynamika
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
obecný	obecný	k2eAgInSc4d1	obecný
vztah	vztah	k1gInSc4	vztah
pro	pro	k7c4	pro
závislost	závislost	k1gFnSc4	závislost
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
na	na	k7c6	na
obecné	obecný	k2eAgFnSc6d1	obecná
teplotě	teplota	k1gFnSc6	teplota
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
vartheta	vartheta	k1gFnSc1	vartheta
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
objem	objem	k1gInSc4	objem
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
vlastností	vlastnost	k1gFnPc2	vlastnost
ideálního	ideální	k2eAgInSc2d1	ideální
plynu	plyn	k1gInSc2	plyn
vyjádřených	vyjádřený	k2eAgFnPc2d1	vyjádřená
pomocí	pomoc	k1gFnPc2	pomoc
absolutní	absolutní	k2eAgFnSc2d1	absolutní
teploty	teplota	k1gFnSc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spočtením	spočtení	k1gNnSc7	spočtení
účinnosti	účinnost	k1gFnSc2	účinnost
vratného	vratný	k2eAgInSc2d1	vratný
Carnotova	Carnotův	k2eAgInSc2d1	Carnotův
cyklu	cyklus	k1gInSc2	cyklus
pro	pro	k7c4	pro
ideální	ideální	k2eAgInSc4d1	ideální
plyn	plyn	k1gInSc4	plyn
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
koherentní	koherentní	k2eAgFnSc6d1	koherentní
volbě	volba	k1gFnSc6	volba
jednotky	jednotka	k1gFnSc2	jednotka
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
rovny	roven	k2eAgFnPc1d1	rovna
(	(	kIx(	(
<g/>
níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
používáno	používat	k5eAaImNgNnS	používat
jen	jen	k6eAd1	jen
označení	označení	k1gNnSc1	označení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
definice	definice	k1gFnPc1	definice
požadují	požadovat	k5eAaImIp3nP	požadovat
systém	systém	k1gInSc4	systém
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
(	(	kIx(	(
<g/>
u	u	k7c2	u
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
skryto	skrýt	k5eAaPmNgNnS	skrýt
ve	v	k7c6	v
"	"	kIx"	"
<g/>
vratnosti	vratnost	k1gFnSc6	vratnost
<g/>
"	"	kIx"	"
děje	dít	k5eAaImIp3nS	dít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nemá	mít	k5eNaImIp3nS	mít
pojem	pojem	k1gInSc1	pojem
teploty	teplota	k1gFnSc2	teplota
dobrý	dobrý	k2eAgInSc4d1	dobrý
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1	statistická
fyzika	fyzika	k1gFnSc1	fyzika
pracuje	pracovat	k5eAaImIp3nS	pracovat
pojmem	pojem	k1gInSc7	pojem
počtu	počet	k1gInSc2	počet
možných	možný	k2eAgInPc2d1	možný
rozlišitelných	rozlišitelný	k2eAgInPc2d1	rozlišitelný
mikrostavů	mikrostav	k1gInPc2	mikrostav
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Omega	omega	k1gNnPc3	omega
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
kterými	který	k3yQgFnPc7	který
lze	lze	k6eAd1	lze
realizovat	realizovat	k5eAaBmF	realizovat
daný	daný	k2eAgInSc4d1	daný
makroskopický	makroskopický	k2eAgInSc4d1	makroskopický
stav	stav	k1gInSc4	stav
systému	systém	k1gInSc2	systém
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
běžných	běžný	k2eAgInPc2d1	běžný
systémů	systém	k1gInPc2	systém
prudce	prudko	k6eAd1	prudko
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
funkcí	funkce	k1gFnSc7	funkce
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pravděpodobnostního	pravděpodobnostní	k2eAgInSc2d1	pravděpodobnostní
rozboru	rozbor	k1gInSc2	rozbor
myšleného	myšlený	k2eAgNnSc2d1	myšlené
rozdělení	rozdělení	k1gNnSc2	rozdělení
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
systému	systém	k1gInSc2	systém
v	v	k7c6	v
rovnovážném	rovnovážný	k2eAgInSc6d1	rovnovážný
stavu	stav	k1gInSc6	stav
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
energií	energie	k1gFnSc7	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U_	U_	k1gMnSc6	U_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
subsystémy	subsystém	k1gInPc4	subsystém
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
rovnováze	rovnováha	k1gFnSc6	rovnováha
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
(	(	kIx(	(
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
(	(	kIx(	(
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
(	(	kIx(	(
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
konst	konst	k5eAaPmF	konst
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
(	(	kIx(	(
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
U_	U_	k1gFnSc1	U_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
(	(	kIx(	(
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
(	(	kIx(	(
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
konst	konst	k1gFnSc1	konst
<g/>
.	.	kIx.	.
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
(	(	kIx(	(
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
(	(	kIx(	(
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
(	(	kIx(	(
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
(	(	kIx(	(
<g/>
U_	U_	k1gFnSc1	U_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
U_	U_	k1gMnSc1	U_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
resp.	resp.	kA	resp.
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
(	(	kIx(	(
U	u	k7c2	u
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Boltzmann	Boltzmann	k1gMnSc1	Boltzmann
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
možné	možný	k2eAgFnPc4d1	možná
analogie	analogie	k1gFnPc4	analogie
statistických	statistický	k2eAgFnPc2d1	statistická
veličin	veličina	k1gFnPc2	veličina
s	s	k7c7	s
termodynamickými	termodynamický	k2eAgFnPc7d1	termodynamická
a	a	k8xC	a
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
možné	možný	k2eAgFnSc2d1	možná
korespondence	korespondence	k1gFnSc2	korespondence
odvodil	odvodit	k5eAaPmAgInS	odvodit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
k	k	k7c3	k
⋅	⋅	k?	⋅
ln	ln	k?	ln
:	:	kIx,	:
Ω	Ω	k?	Ω
+	+	kIx~	+
k	k	k7c3	k
o	o	k7c4	o
n	n	k0	n
s	s	k7c7	s
t	t	k?	t
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
+	+	kIx~	+
<g/>
konst	konst	k1gFnSc1	konst
<g/>
.	.	kIx.	.
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
plyne	plynout	k5eAaImIp3nS	plynout
statistická	statistický	k2eAgFnSc1d1	statistická
definice	definice	k1gFnSc1	definice
absolutní	absolutní	k2eAgFnSc2d1	absolutní
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
ln	ln	k?	ln
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
U	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Omega	omega	k1gNnPc3	omega
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc4	počet
možných	možný	k2eAgInPc2d1	možný
rozlišitelných	rozlišitelný	k2eAgInPc2d1	rozlišitelný
mikrostavů	mikrostav	k1gInPc2	mikrostav
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Boltzmann	Boltzmann	k1gMnSc1	Boltzmann
také	také	k9	také
odvodil	odvodit	k5eAaPmAgMnS	odvodit
pro	pro	k7c4	pro
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
a	a	k8xC	a
proto	proto	k8xC	proto
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
soubory	soubor	k1gInPc4	soubor
neinteragujících	interagující	k2eNgFnPc2d1	interagující
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
Boltzmannův	Boltzmannův	k2eAgInSc1d1	Boltzmannův
distribuční	distribuční	k2eAgInSc1d1	distribuční
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
relativní	relativní	k2eAgNnSc4d1	relativní
zastoupení	zastoupení	k1gNnSc4	zastoupení
mikrostavů	mikrostav	k1gInPc2	mikrostav
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc5	E_
<g/>
{	{	kIx(	{
<g/>
i	i	k8xC	i
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
degenerací	degenerace	k1gFnSc7	degenerace
(	(	kIx(	(
<g/>
statistickou	statistický	k2eAgFnSc7d1	statistická
váhou	váha	k1gFnSc7	váha
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
g	g	kA	g
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
N_	N_	k1gFnSc1	N_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}}}}	}}}}	k?	}}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
exponentu	exponent	k1gInSc2	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1	statistická
definice	definice	k1gFnSc1	definice
teploty	teplota	k1gFnSc2	teplota
nebo	nebo	k8xC	nebo
Boltzmannův	Boltzmannův	k2eAgInSc4d1	Boltzmannův
zákon	zákon	k1gInSc4	zákon
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rozšíření	rozšíření	k1gNnSc3	rozšíření
pojmu	pojem	k1gInSc2	pojem
teploty	teplota	k1gFnSc2	teplota
i	i	k9	i
na	na	k7c4	na
systémy	systém	k1gInPc4	systém
s	s	k7c7	s
konečným	konečný	k2eAgInSc7d1	konečný
počtem	počet	k1gInSc7	počet
energetických	energetický	k2eAgInPc2d1	energetický
stavů	stav	k1gInPc2	stav
příslušným	příslušný	k2eAgNnSc7d1	příslušné
danému	daný	k2eAgInSc3d1	daný
stupni	stupeň	k1gInSc3	stupeň
volnosti	volnost	k1gFnSc2	volnost
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
na	na	k7c4	na
kvantové	kvantový	k2eAgInPc4d1	kvantový
systémy	systém	k1gInPc4	systém
s	s	k7c7	s
diskrétním	diskrétní	k2eAgNnSc7d1	diskrétní
spektrem	spektrum	k1gNnSc7	spektrum
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
konečnou	konečný	k2eAgFnSc7d1	konečná
dolní	dolní	k2eAgFnSc7d1	dolní
a	a	k8xC	a
horní	horní	k2eAgFnSc7d1	horní
mezí	mez	k1gFnSc7	mez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
omezenu	omezen	k2eAgFnSc4d1	omezena
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
stupni	stupeň	k1gInPc7	stupeň
volnosti	volnost	k1gFnSc2	volnost
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
je	být	k5eAaImIp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
"	"	kIx"	"
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
"	"	kIx"	"
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jim	on	k3xPp3gMnPc3	on
připsat	připsat	k5eAaPmF	připsat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teplotu	teplota	k1gFnSc4	teplota
pomocí	pomocí	k7c2	pomocí
statistické	statistický	k2eAgFnSc2d1	statistická
definice	definice	k1gFnSc2	definice
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
obsazenosti	obsazenost	k1gFnSc2	obsazenost
jeho	jeho	k3xOp3gFnPc2	jeho
energetických	energetický	k2eAgFnPc2d1	energetická
hladin	hladina	k1gFnPc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
teplota	teplota	k1gFnSc1	teplota
spinů	spin	k1gInPc2	spin
částic	částice	k1gFnPc2	částice
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
tato	tento	k3xDgFnSc1	tento
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
hodnoty	hodnota	k1gFnSc2	hodnota
(	(	kIx(	(
<g/>
obsazenost	obsazenost	k1gFnSc1	obsazenost
hladin	hladina	k1gFnPc2	hladina
vyšší	vysoký	k2eAgFnSc2d2	vyšší
energie	energie	k1gFnSc2	energie
bude	být	k5eAaImBp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
hladin	hladina	k1gFnPc2	hladina
nižší	nízký	k2eAgFnSc2d2	nižší
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
dokonce	dokonce	k9	dokonce
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
záporných	záporný	k2eAgFnPc2d1	záporná
absolutních	absolutní	k2eAgFnPc2d1	absolutní
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
případě	případ	k1gInSc6	případ
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
inverzním	inverzní	k2eAgNnSc7d1	inverzní
obsazením	obsazení	k1gNnSc7	obsazení
hladin	hladina	k1gFnPc2	hladina
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
podmínce	podmínka	k1gFnSc3	podmínka
omezené	omezený	k2eAgFnPc4d1	omezená
interakce	interakce	k1gFnPc4	interakce
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
stupni	stupeň	k1gInPc7	stupeň
volnosti	volnost	k1gFnSc2	volnost
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
rychlé	rychlý	k2eAgFnSc3d1	rychlá
relaxaci	relaxace	k1gFnSc3	relaxace
do	do	k7c2	do
rovnovážného	rovnovážný	k2eAgInSc2d1	rovnovážný
stavu	stav	k1gInSc2	stav
s	s	k7c7	s
kladnou	kladný	k2eAgFnSc7d1	kladná
teplotou	teplota	k1gFnSc7	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1	posloupnost
stoupajících	stoupající	k2eAgFnPc2d1	stoupající
absolutních	absolutní	k2eAgFnPc2d1	absolutní
teplot	teplota	k1gFnPc2	teplota
tedy	tedy	k9	tedy
začíná	začínat	k5eAaImIp3nS	začínat
"	"	kIx"	"
<g/>
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
<g/>
"	"	kIx"	"
nedosažitelnou	dosažitelný	k2eNgFnSc7d1	nedosažitelná
absolutní	absolutní	k2eAgFnSc7d1	absolutní
nulou	nula	k1gFnSc7	nula
T	T	kA	T
=	=	kIx~	=
0	[number]	k4	0
K	k	k7c3	k
a	a	k8xC	a
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
systémů	systém	k1gInPc2	systém
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
stoupat	stoupat	k5eAaImF	stoupat
až	až	k9	až
k	k	k7c3	k
nekonečné	konečný	k2eNgFnSc3d1	nekonečná
hodnotě	hodnota	k1gFnSc3	hodnota
T	T	kA	T
=	=	kIx~	=
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
K	k	k7c3	k
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
K	k	k7c3	k
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
dále	daleko	k6eAd2	daleko
až	až	k6eAd1	až
"	"	kIx"	"
<g/>
těsně	těsně	k6eAd1	těsně
pod	pod	k7c4	pod
<g/>
"	"	kIx"	"
nedosažitelnou	dosažitelný	k2eNgFnSc4d1	nedosažitelná
absolutní	absolutní	k2eAgFnSc4d1	absolutní
nulu	nula	k1gFnSc4	nula
T	T	kA	T
=	=	kIx~	=
0	[number]	k4	0
K.	K.	kA	K.
Existence	existence	k1gFnSc1	existence
záporných	záporný	k2eAgFnPc2d1	záporná
teplot	teplota	k1gFnPc2	teplota
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
mylnou	mylný	k2eAgFnSc4d1	mylná
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ze	z	k7c2	z
školních	školní	k2eAgFnPc2d1	školní
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nekonzistentně	konzistentně	k6eNd1	konzistentně
použita	použit	k2eAgFnSc1d1	použita
Boltzmannova	Boltzmannův	k2eAgFnSc1d1	Boltzmannova
definice	definice	k1gFnSc1	definice
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
normální	normální	k2eAgFnPc4d1	normální
distribuce	distribuce	k1gFnPc4	distribuce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
Gibbsovy	Gibbsův	k2eAgFnSc2d1	Gibbsova
definice	definice	k1gFnSc2	definice
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
entropie	entropie	k1gFnSc2	entropie
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
záporných	záporný	k2eAgFnPc2d1	záporná
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
Boltzmannovy	Boltzmannův	k2eAgFnSc2d1	Boltzmannova
definice	definice	k1gFnSc2	definice
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
účinnosti	účinnost	k1gFnSc3	účinnost
větší	veliký	k2eAgFnSc3d2	veliký
než	než	k8xS	než
100	[number]	k4	100
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
Carnotova	Carnotův	k2eAgFnSc1d1	Carnotova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Relativisticky	relativisticky	k6eAd1	relativisticky
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zavedení	zavedení	k1gNnSc1	zavedení
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
transformační	transformační	k2eAgInPc1d1	transformační
vztahy	vztah	k1gInPc1	vztah
představují	představovat	k5eAaImIp3nP	představovat
doposud	doposud	k6eAd1	doposud
pro	pro	k7c4	pro
fyziku	fyzika	k1gFnSc4	fyzika
nevyřešený	vyřešený	k2eNgInSc4d1	nevyřešený
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Planck	Planck	k6eAd1	Planck
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
Einsteinem	Einstein	k1gMnSc7	Einstein
odvodil	odvodit	k5eAaPmAgInS	odvodit
v	v	k7c6	v
r.	r.	kA	r.
1908	[number]	k4	1908
transformační	transformační	k2eAgInSc4d1	transformační
vztah	vztah	k1gInSc4	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnPc4	gammum
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}}	}}}}}}	k?	}}}}}}
a	a	k8xC	a
index	index	k1gInSc1	index
0	[number]	k4	0
značí	značit	k5eAaImIp3nS	značit
klidovou	klidový	k2eAgFnSc4d1	klidová
souřadnou	souřadný	k2eAgFnSc4d1	souřadná
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ott	Ott	k?	Ott
odvodil	odvodit	k5eAaPmAgInS	odvodit
v	v	k7c6	v
r.	r.	kA	r.
1963	[number]	k4	1963
právě	právě	k6eAd1	právě
"	"	kIx"	"
<g/>
opačný	opačný	k2eAgInSc1d1	opačný
<g/>
"	"	kIx"	"
transformační	transformační	k2eAgInSc1d1	transformační
vztah	vztah	k1gInSc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
γ	γ	k?	γ
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Landsberg	Landsberg	k1gMnSc1	Landsberg
pak	pak	k6eAd1	pak
v	v	k7c6	v
r.	r.	kA	r.
1970	[number]	k4	1970
přidal	přidat	k5eAaPmAgInS	přidat
odvození	odvození	k1gNnSc4	odvození
transformačního	transformační	k2eAgInSc2d1	transformační
vztahu	vztah	k1gInSc2	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
univerzální	univerzální	k2eAgFnSc1d1	univerzální
spojitá	spojitý	k2eAgFnSc1d1	spojitá
transformace	transformace	k1gFnSc1	transformace
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
konzistentní	konzistentní	k2eAgFnSc1d1	konzistentní
pro	pro	k7c4	pro
záření	záření	k1gNnSc4	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Rozpory	rozpor	k1gInPc1	rozpor
vznikají	vznikat	k5eAaImIp3nP	vznikat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
použita	použit	k2eAgNnPc4d1	použito
odlišná	odlišný	k2eAgNnPc4d1	odlišné
zavedení	zavedení	k1gNnPc4	zavedení
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nerelativistickém	relativistický	k2eNgInSc6d1	nerelativistický
případě	případ	k1gInSc6	případ
rovnají	rovnat	k5eAaImIp3nP	rovnat
<g/>
,	,	kIx,	,
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
kovariantní	kovariantní	k2eAgInPc1d1	kovariantní
tvary	tvar	k1gInPc1	tvar
termodynamických	termodynamický	k2eAgInPc2d1	termodynamický
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
pojmu	pojmout	k5eAaPmIp1nS	pojmout
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
relativistickém	relativistický	k2eAgInSc6d1	relativistický
případě	případ	k1gInSc6	případ
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
několik	několik	k4yIc4	několik
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgInSc1d1	experimentální
průkaz	průkaz	k1gInSc1	průkaz
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
současných	současný	k2eAgFnPc2d1	současná
schopností	schopnost	k1gFnPc2	schopnost
nemožný	možný	k2eNgInSc4d1	nemožný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
teploty	teplota	k1gFnSc2	teplota
zrychlením	zrychlení	k1gNnSc7	zrychlení
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
hypoteticky	hypoteticky	k6eAd1	hypoteticky
tzv.	tzv.	kA	tzv.
Unruhův	Unruhův	k2eAgInSc4d1	Unruhův
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uvede	uvést	k5eAaPmIp3nS	uvést
do	do	k7c2	do
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
styku	styk	k1gInSc2	styk
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
teplotu	teplota	k1gFnSc4	teplota
chceme	chtít	k5eAaImIp1nP	chtít
měřit	měřit	k5eAaImF	měřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
srovnávací	srovnávací	k2eAgNnSc1d1	srovnávací
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
tepelné	tepelný	k2eAgFnSc2d1	tepelná
rovnováhy	rovnováha	k1gFnSc2	rovnováha
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
tělesa	těleso	k1gNnSc2	těleso
rovna	roven	k2eAgFnSc1d1	rovna
teplotě	teplota	k1gFnSc6	teplota
srovnávacího	srovnávací	k2eAgNnSc2d1	srovnávací
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
teploměrem	teploměr	k1gInSc7	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
určování	určování	k1gNnSc3	určování
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
závislosti	závislost	k1gFnSc2	závislost
vhodně	vhodně	k6eAd1	vhodně
zvolených	zvolený	k2eAgFnPc2d1	zvolená
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převést	převést	k5eAaPmF	převést
měření	měření	k1gNnSc4	měření
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
jiné	jiný	k2eAgNnSc4d1	jiné
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
veličiny	veličina	k1gFnPc4	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
teplotně	teplotně	k6eAd1	teplotně
závislé	závislý	k2eAgFnPc1d1	závislá
veličiny	veličina	k1gFnPc1	veličina
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
délkové	délkový	k2eAgInPc4d1	délkový
rozměry	rozměr	k1gInPc4	rozměr
a	a	k8xC	a
objem	objem	k1gInSc4	objem
pevných	pevný	k2eAgNnPc2d1	pevné
a	a	k8xC	a
kapalných	kapalný	k2eAgNnPc2d1	kapalné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
teplotní	teplotní	k2eAgFnSc1d1	teplotní
roztažnost	roztažnost	k1gFnSc1	roztažnost
a	a	k8xC	a
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc4d1	elektrický
odpor	odpor	k1gInSc4	odpor
vodičů	vodič	k1gInPc2	vodič
nebo	nebo	k8xC	nebo
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
,	,	kIx,	,
elektromotorické	elektromotorický	k2eAgNnSc1d1	elektromotorické
napětí	napětí	k1gNnSc1	napětí
termoelektrických	termoelektrický	k2eAgInPc2d1	termoelektrický
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
detekce	detekce	k1gFnSc1	detekce
radiometrických	radiometrický	k2eAgFnPc2d1	radiometrická
vlastností	vlastnost	k1gFnPc2	vlastnost
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tělesa	těleso	k1gNnPc1	těleso
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
<g/>
.	.	kIx.	.
</s>
<s>
Teploměry	teploměr	k1gInPc1	teploměr
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nazývají	nazývat	k5eAaImIp3nP	nazývat
pyrometry	pyrometr	k1gInPc1	pyrometr
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
např.	např.	kA	např.
v	v	k7c6	v
keramických	keramický	k2eAgFnPc6d1	keramická
pecích	pec	k1gFnPc6	pec
lze	lze	k6eAd1	lze
přibližně	přibližně	k6eAd1	přibližně
měřit	měřit	k5eAaImF	měřit
pomocí	pomocí	k7c2	pomocí
Segerových	Segerův	k2eAgInPc2d1	Segerův
jehlánků	jehlánek	k1gInPc2	jehlánek
<g/>
.	.	kIx.	.
</s>
<s>
Měřením	měření	k1gNnSc7	měření
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
termometrie	termometrie	k1gFnSc1	termometrie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Teploměr	teploměr	k1gInSc1	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
základní	základní	k2eAgFnSc1d1	základní
veličina	veličina	k1gFnSc1	veličina
SI	si	k1gNnSc2	si
-	-	kIx~	-
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
princip	princip	k1gInSc4	princip
"	"	kIx"	"
<g/>
kolikrát	kolikrát	k6eAd1	kolikrát
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
jednotka	jednotka	k1gFnSc1	jednotka
do	do	k7c2	do
měřené	měřený	k2eAgFnSc2d1	měřená
veličiny	veličina	k1gFnSc2	veličina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
kelvin	kelvin	k1gInSc4	kelvin
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
definována	definován	k2eAgFnSc1d1	definována
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
měření	měření	k1gNnSc4	měření
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
metrologii	metrologie	k1gFnSc6	metrologie
a	a	k8xC	a
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
teploměrných	teploměrný	k2eAgInPc2d1	teploměrný
etalonů	etalon	k1gInPc2	etalon
proto	proto	k8xC	proto
definice	definice	k1gFnSc1	definice
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
praktickou	praktický	k2eAgFnSc4d1	praktická
realizaci	realizace	k1gFnSc4	realizace
celého	celý	k2eAgInSc2d1	celý
rozsahu	rozsah	k1gInSc2	rozsah
měřených	měřený	k2eAgFnPc2d1	měřená
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Stanovovat	stanovovat	k5eAaImF	stanovovat
teplotu	teplota	k1gFnSc4	teplota
přímo	přímo	k6eAd1	přímo
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
ani	ani	k9	ani
žádný	žádný	k3yNgInSc1	žádný
vhodný	vhodný	k2eAgInSc1d1	vhodný
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
rozsah	rozsah	k1gInSc4	rozsah
měřených	měřený	k2eAgFnPc2d1	měřená
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
je	být	k5eAaImIp3nS	být
proto	proto	k6eAd1	proto
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
teplotní	teplotní	k2eAgFnSc1d1	teplotní
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
přesně	přesně	k6eAd1	přesně
definovaných	definovaný	k2eAgInPc2d1	definovaný
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
realizovatelných	realizovatelný	k2eAgInPc6d1	realizovatelný
referenčních	referenční	k2eAgInPc6d1	referenční
bodech	bod	k1gInPc6	bod
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
na	na	k7c6	na
přesné	přesný	k2eAgFnSc6d1	přesná
metodice	metodika	k1gFnSc6	metodika
použité	použitý	k2eAgFnSc6d1	použitá
pro	pro	k7c4	pro
interpolaci	interpolace	k1gFnSc4	interpolace
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
referenčními	referenční	k2eAgInPc7d1	referenční
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Referenčními	referenční	k2eAgInPc7d1	referenční
body	bod	k1gInPc7	bod
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
teploty	teplota	k1gFnPc4	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
kondenzace	kondenzace	k1gFnSc2	kondenzace
(	(	kIx(	(
<g/>
zjišťované	zjišťovaný	k2eAgNnSc1d1	zjišťované
ve	v	k7c6	v
výparnících	výparník	k1gInPc6	výparník
-	-	kIx~	-
hypsometrech	hypsometr	k1gInPc6	hypsometr
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
teploty	teplota	k1gFnPc1	teplota
trojných	trojný	k2eAgInPc2d1	trojný
bodů	bod	k1gInPc2	bod
vhodných	vhodný	k2eAgFnPc2d1	vhodná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Interpolace	interpolace	k1gFnSc1	interpolace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
etalonových	etalonův	k2eAgInPc2d1	etalonův
odporových	odporový	k2eAgInPc2d1	odporový
teploměrů	teploměr	k1gInPc2	teploměr
a	a	k8xC	a
termoelektrických	termoelektrický	k2eAgInPc2d1	termoelektrický
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
u	u	k7c2	u
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
pomocí	pomocí	k7c2	pomocí
spektrální	spektrální	k2eAgFnSc2d1	spektrální
hustoty	hustota	k1gFnSc2	hustota
zářivé	zářivý	k2eAgFnSc2d1	zářivá
energie	energie	k1gFnSc2	energie
nebo	nebo	k8xC	nebo
spektrální	spektrální	k2eAgFnSc2d1	spektrální
hustoty	hustota	k1gFnSc2	hustota
zářivosti	zářivost	k1gFnSc2	zářivost
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
teplotní	teplotní	k2eAgFnSc1d1	teplotní
stupnice	stupnice	k1gFnSc1	stupnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
ITS-	ITS-	k1gFnSc1	ITS-
<g/>
90	[number]	k4	90
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
rozsah	rozsah	k1gInSc4	rozsah
teplot	teplota	k1gFnPc2	teplota
již	již	k6eAd1	již
od	od	k7c2	od
0,65	[number]	k4	0,65
K.	K.	kA	K.
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
definice	definice	k1gFnSc1	definice
historických	historický	k2eAgFnPc2d1	historická
i	i	k8xC	i
současných	současný	k2eAgFnPc2d1	současná
teplotních	teplotní	k2eAgFnPc2d1	teplotní
stupnic	stupnice	k1gFnPc2	stupnice
pomocí	pomocí	k7c2	pomocí
referenčních	referenční	k2eAgInPc2d1	referenční
bodů	bod	k1gInPc2	bod
teploty	teplota	k1gFnSc2	teplota
<g/>
:	:	kIx,	:
*	*	kIx~	*
Původně	původně	k6eAd1	původně
definována	definován	k2eAgFnSc1d1	definována
pomocí	pomocí	k7c2	pomocí
Celsiovy	Celsiův	k2eAgFnSc2d1	Celsiova
stupnice	stupnice	k1gFnSc2	stupnice
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
teplotní	teplotní	k2eAgInSc4d1	teplotní
rozdíl	rozdíl	k1gInSc4	rozdíl
1	[number]	k4	1
K	K	kA	K
≡	≡	k?	≡
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
**	**	k?	**
Referenční	referenční	k2eAgInPc1d1	referenční
body	bod	k1gInPc1	bod
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
návrhu	návrh	k1gInSc6	návrh
obrácené	obrácený	k2eAgNnSc1d1	obrácené
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
Delisleovy	Delisleův	k2eAgFnSc2d1	Delisleův
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
definována	definován	k2eAgFnSc1d1	definována
pomocí	pomocí	k7c2	pomocí
Kelvinovy	Kelvinův	k2eAgFnSc2d1	Kelvinova
stupnice	stupnice	k1gFnSc2	stupnice
(	(	kIx(	(
<g/>
teplotní	teplotní	k2eAgInSc1d1	teplotní
rozdíl	rozdíl	k1gInSc1	rozdíl
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
≡	≡	k?	≡
1	[number]	k4	1
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
***	***	k?	***
Teplota	teplota	k1gFnSc1	teplota
chladicí	chladicí	k2eAgFnSc2d1	chladicí
směsi	směs	k1gFnSc2	směs
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
salmiaku	salmiak	k1gInSc2	salmiak
nebo	nebo	k8xC	nebo
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
17,8	[number]	k4	17,8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
36,5	[number]	k4	36,5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
****	****	k?	****
Bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc1	tání
solanky	solanka	k1gFnSc2	solanka
(	(	kIx(	(
<g/>
nasycený	nasycený	k2eAgInSc1d1	nasycený
roztok	roztok	k1gInSc1	roztok
soli	sůl	k1gFnSc2	sůl
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
vodě	voda	k1gFnSc3	voda
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
14,3	[number]	k4	14,3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
Pomocí	pomocí	k7c2	pomocí
následující	následující	k2eAgFnSc2d1	následující
tabulky	tabulka	k1gFnSc2	tabulka
lze	lze	k6eAd1	lze
jednoduše	jednoduše	k6eAd1	jednoduše
přepočítat	přepočítat	k5eAaPmF	přepočítat
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
teploty	teplota	k1gFnSc2	teplota
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
teplotní	teplotní	k2eAgFnSc2d1	teplotní
stupnice	stupnice	k1gFnSc2	stupnice
(	(	kIx(	(
<g/>
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
stupnice	stupnice	k1gFnPc4	stupnice
používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
hodnoty	hodnota	k1gFnSc2	hodnota
vybraných	vybraný	k2eAgInPc2d1	vybraný
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
teplotních	teplotní	k2eAgInPc2d1	teplotní
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
stupnicích	stupnice	k1gFnPc6	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
veličinou	veličina	k1gFnSc7	veličina
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
veličinových	veličinův	k2eAgInPc2d1	veličinův
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
důležitým	důležitý	k2eAgInSc7d1	důležitý
způsobem	způsob	k1gInSc7	způsob
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jako	jako	k9	jako
důležité	důležitý	k2eAgNnSc1d1	důležité
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
:	:	kIx,	:
stavové	stavový	k2eAgFnPc1d1	stavová
rovnice	rovnice	k1gFnPc1	rovnice
hlavní	hlavní	k2eAgFnSc2d1	hlavní
věty	věta	k1gFnSc2	věta
termodynamické	termodynamický	k2eAgNnSc1d1	termodynamické
<g/>
:	:	kIx,	:
nultá	nultý	k4xOgFnSc1	nultý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
věta	věta	k1gFnSc1	věta
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∧	∧	k?	∧
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
⇒	⇒	k?	⇒
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
T_	T_	k1gMnSc6	T_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
land	land	k1gMnSc1	land
(	(	kIx(	(
<g/>
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gMnSc1	Rightarrow
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
druhá	druhý	k4xOgFnSc1	druhý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
věta	věta	k1gFnSc1	věta
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∮	∮	k?	∮
:	:	kIx,	:
↺	↺	k?	↺
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
≧	≧	k?	≧
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
oint	oint	k2eAgInSc4d1	oint
\	\	kIx~	\
<g/>
limits	limits	k1gInSc4	limits
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circlearrowleft	circlearrowleft	k5eAaPmF	circlearrowleft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
Q	Q	kA	Q
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
geqq	geqq	k?	geqq
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
třetí	třetí	k4xOgFnSc1	třetí
hlavní	hlavní	k2eAgFnSc1d1	hlavní
věta	věta	k1gFnSc1	věta
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
T	T	kA	T
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
vztahy	vztah	k1gInPc1	vztah
pro	pro	k7c4	pro
diferenciály	diferenciál	k1gInPc4	diferenciál
termodynamických	termodynamický	k2eAgInPc2d1	termodynamický
potenciálů	potenciál	k1gInPc2	potenciál
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
U	u	k7c2	u
=	=	kIx~	=
T	T	kA	T
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
S	s	k7c7	s
−	−	k?	−
p	p	k?	p
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
V	V	kA	V
+	+	kIx~	+
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
U	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S-p	S	k1gMnSc1	S-p
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
H	H	kA	H
=	=	kIx~	=
T	T	kA	T
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
S	s	k7c7	s
+	+	kIx~	+
V	V	kA	V
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
p	p	k?	p
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
H	H	kA	H
<g/>
=	=	kIx~	=
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
S	s	k7c7	s
<g/>
+	+	kIx~	+
<g/>
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
p	p	k?	p
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
N_	N_	k1gMnSc1	N_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
F	F	kA	F
=	=	kIx~	=
−	−	k?	−
S	s	k7c7	s
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
T	T	kA	T
−	−	k?	−
p	p	k?	p
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
V	V	kA	V
+	+	kIx~	+
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
-S	-S	k?	-S
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
T-p	T	k1gMnSc1	T-p
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
G	G	kA	G
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
−	−	k?	−
S	s	k7c7	s
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
T	T	kA	T
+	+	kIx~	+
V	V	kA	V
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
p	p	k?	p
+	+	kIx~	+
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
G	G	kA	G
<g/>
=	=	kIx~	=
<g/>
-S	-S	k?	-S
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
T	T	kA	T
<g/>
+	+	kIx~	+
<g/>
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
p	p	k?	p
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
i	i	k8xC	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
plynoucí	plynoucí	k2eAgFnPc1d1	plynoucí
vztahy	vztah	k1gInPc4	vztah
včetně	včetně	k7c2	včetně
tzv.	tzv.	kA	tzv.
Maxwellových	Maxwellův	k2eAgFnPc2d1	Maxwellova
relací	relace	k1gFnPc2	relace
a	a	k8xC	a
některé	některý	k3yIgNnSc1	některý
další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
T	T	kA	T
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
T	T	kA	T
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
:	:	kIx,	:
U	u	k7c2	u
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
nekoná	konat	k5eNaImIp3nS	konat
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
teploty	teplota	k1gFnSc2	teplota
mírou	míra	k1gFnSc7	míra
tepla	teplo	k1gNnSc2	teplo
předávaného	předávaný	k2eAgNnSc2d1	předávané
při	při	k7c6	při
tepelném	tepelný	k2eAgInSc6d1	tepelný
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
využívána	využívat	k5eAaImNgFnS	využívat
v	v	k7c6	v
kalorimetrii	kalorimetrie	k1gFnSc6	kalorimetrie
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
ze	z	k7c2	z
znalosti	znalost	k1gFnSc2	znalost
chování	chování	k1gNnSc2	chování
tepelných	tepelný	k2eAgFnPc2d1	tepelná
kapacit	kapacita	k1gFnPc2	kapacita
při	při	k7c6	při
změnách	změna	k1gFnPc6	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ekvipartičního	ekvipartiční	k2eAgInSc2d1	ekvipartiční
teorému	teorém	k1gInSc2	teorém
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
jednoatomové	jednoatomový	k2eAgFnSc2d1	jednoatomový
<g/>
,	,	kIx,	,
dvouatomové	dvouatomový	k2eAgFnSc3d1	dvouatomová
<g/>
,	,	kIx,	,
víceatomové	víceatomový	k2eAgFnSc3d1	víceatomový
lineární	lineární	k2eAgFnPc4d1	lineární
a	a	k8xC	a
víceatomové	víceatomový	k2eAgFnPc4d1	víceatomový
nelineární	lineární	k2eNgFnPc4d1	nelineární
<g/>
)	)	kIx)	)
odvodit	odvodit	k5eAaPmF	odvodit
v	v	k7c6	v
kinetické	kinetický	k2eAgFnSc6d1	kinetická
teorii	teorie	k1gFnSc6	teorie
hodnoty	hodnota	k1gFnSc2	hodnota
molární	molární	k2eAgFnSc2d1	molární
tepelné	tepelný	k2eAgFnSc2d1	tepelná
kapacity	kapacita	k1gFnSc2	kapacita
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
u	u	k7c2	u
víceatomových	víceatomový	k2eAgFnPc2d1	víceatomový
molekul	molekula	k1gFnPc2	molekula
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
růstu	růst	k1gInSc3	růst
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
určité	určitý	k2eAgFnSc6d1	určitá
teplotě	teplota	k1gFnSc6	teplota
začínají	začínat	k5eAaImIp3nP	začínat
"	"	kIx"	"
<g/>
rozmrzat	rozmrzat	k5eAaImF	rozmrzat
<g/>
"	"	kIx"	"
nové	nový	k2eAgInPc4d1	nový
stupně	stupeň	k1gInPc4	stupeň
volnosti	volnost	k1gFnSc2	volnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
díky	díky	k7c3	díky
kvantové	kvantový	k2eAgFnSc3d1	kvantová
mechanice	mechanika	k1gFnSc3	mechanika
mají	mít	k5eAaImIp3nP	mít
diskrétní	diskrétní	k2eAgFnSc2d1	diskrétní
úrovně	úroveň	k1gFnSc2	úroveň
energie	energie	k1gFnSc2	energie
až	až	k9	až
od	od	k7c2	od
určité	určitý	k2eAgFnSc2d1	určitá
minimální	minimální	k2eAgFnSc2d1	minimální
hladiny	hladina	k1gFnSc2	hladina
<g/>
:	:	kIx,	:
nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
dosažení	dosažení	k1gNnSc3	dosažení
minimálních	minimální	k2eAgFnPc2d1	minimální
úrovní	úroveň	k1gFnPc2	úroveň
rotačních	rotační	k2eAgInPc2d1	rotační
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
řád	řád	k1gInSc4	řád
výše	výše	k1gFnSc2	výše
i	i	k8xC	i
stavů	stav	k1gInPc2	stav
vibračních	vibrační	k2eAgInPc2d1	vibrační
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
teploty	teplota	k1gFnPc4	teplota
vybuzení	vybuzení	k1gNnSc2	vybuzení
těchto	tento	k3xDgInPc2	tento
stavů	stav	k1gInPc2	stav
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
o	o	k7c6	o
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
I	I	kA	I
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
rot	rota	k1gFnPc2	rota
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
kI	kI	k?	kI
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c4	v
i	i	k8xC	i
b	b	k?	b
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
vib	vib	k?	vib
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
hbar	hbar	k1gInSc1	hbar
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
I	i	k9	i
,	,	kIx,	,
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
I	i	k9	i
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
moment	moment	k1gInSc4	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
jejích	její	k3xOp3gInPc2	její
vlastních	vlastní	k2eAgInPc2d1	vlastní
kmitů	kmit	k1gInPc2	kmit
(	(	kIx(	(
<g/>
spočtená	spočtený	k2eAgFnSc1d1	spočtená
pro	pro	k7c4	pro
klasický	klasický	k2eAgInSc4d1	klasický
oscilátor	oscilátor	k1gInSc4	oscilátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvantové	kvantový	k2eAgInPc1d1	kvantový
jevy	jev	k1gInPc1	jev
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nP	projevit
také	také	k9	také
u	u	k7c2	u
teplotní	teplotní	k2eAgFnSc2d1	teplotní
závislosti	závislost	k1gFnSc2	závislost
molární	molární	k2eAgFnSc2d1	molární
tepelné	tepelný	k2eAgFnSc2d1	tepelná
kapacity	kapacita	k1gFnSc2	kapacita
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kvantovou	kvantový	k2eAgFnSc4d1	kvantová
teorii	teorie	k1gFnSc4	teorie
tepelných	tepelný	k2eAgFnPc2d1	tepelná
kapacit	kapacita	k1gFnPc2	kapacita
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
Debye	Debye	k1gInSc1	Debye
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nízké	nízký	k2eAgFnPc4d1	nízká
teploty	teplota	k1gFnPc4	teplota
odvodil	odvodit	k5eAaPmAgMnS	odvodit
závislost	závislost	k1gFnSc1	závislost
molární	molární	k2eAgFnSc1d1	molární
tepelné	tepelný	k2eAgFnPc4d1	tepelná
kapacity	kapacita	k1gFnPc4	kapacita
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
mocnině	mocnina	k1gFnSc6	mocnina
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pro	pro	k7c4	pro
teploty	teplota	k1gFnPc4	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
≫	≫	k?	≫
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
gg	gg	k?	gg
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
začíná	začínat	k5eAaImIp3nS	začínat
platit	platit	k5eAaImF	platit
Dulongův-Petitův	Dulongův-Petitův	k2eAgInSc1d1	Dulongův-Petitův
<g />
.	.	kIx.	.
</s>
<s hack="1">
zákon	zákon	k1gInSc1	zákon
rovnosti	rovnost	k1gFnSc2	rovnost
a	a	k8xC	a
konstantnosti	konstantnost	k1gFnSc2	konstantnost
molárních	molární	k2eAgFnPc2d1	molární
tepelných	tepelný	k2eAgFnPc2d1	tepelná
kapacit	kapacita	k1gFnPc2	kapacita
krystalických	krystalický	k2eAgFnPc2d1	krystalická
látek	látka	k1gFnPc2	látka
<g/>
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
podílu	podíl	k1gInSc3	podíl
energie	energie	k1gFnSc2	energie
fononu	fonon	k1gInSc2	fonon
elastických	elastický	k2eAgFnPc2d1	elastická
vln	vlna	k1gFnPc2	vlna
v	v	k7c6	v
krystalu	krystal	k1gInSc6	krystal
a	a	k8xC	a
Boltzmannovy	Boltzmannův	k2eAgFnPc1d1	Boltzmannova
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
ustáleného	ustálený	k2eAgInSc2d1	ustálený
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
stavovou	stavový	k2eAgFnSc7d1	stavová
rovnicí	rovnice	k1gFnSc7	rovnice
reálného	reálný	k2eAgInSc2d1	reálný
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
příkladem	příklad	k1gInSc7	příklad
dobrého	dobrý	k2eAgNnSc2d1	dobré
přiblížení	přiblížení	k1gNnSc2	přiblížení
je	být	k5eAaImIp3nS	být
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsova	Waalsův	k2eAgFnSc1d1	Waalsova
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
změnu	změna	k1gFnSc4	změna
teploty	teplota	k1gFnSc2	teplota
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
rovnovážných	rovnovážný	k2eAgInPc6d1	rovnovážný
(	(	kIx(	(
<g/>
kvazistatických	kvazistatický	k2eAgInPc6d1	kvazistatický
<g/>
)	)	kIx)	)
tepelných	tepelný	k2eAgInPc6d1	tepelný
dějích	děj	k1gInPc6	děj
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ideálnímu	ideální	k2eAgInSc3d1	ideální
plynu	plyn	k1gInSc3	plyn
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
nejvíce	nejvíce	k6eAd1	nejvíce
u	u	k7c2	u
nízkých	nízký	k2eAgInPc2d1	nízký
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
souvisejí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
skupenskou	skupenský	k2eAgFnSc7d1	skupenská
přeměnou	přeměna	k1gFnSc7	přeměna
do	do	k7c2	do
kondenzované	kondenzovaný	k2eAgFnSc2d1	kondenzovaná
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
oproti	oproti	k7c3	oproti
ideálnímu	ideální	k2eAgInSc3d1	ideální
plynu	plyn	k1gInSc3	plyn
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jevech	jev	k1gInPc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
je	být	k5eAaImIp3nS	být
Joulův-Thomsonův	Joulův-Thomsonův	k2eAgInSc1d1	Joulův-Thomsonův
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
při	při	k7c6	při
adiabatické	adiabatický	k2eAgFnSc6d1	adiabatická
expanzi	expanze	k1gFnSc6	expanze
do	do	k7c2	do
vakua	vakuum	k1gNnSc2	vakuum
přes	přes	k7c4	přes
pórovitou	pórovitý	k2eAgFnSc4d1	pórovitá
přepážku	přepážka	k1gFnSc4	přepážka
mění	měnit	k5eAaImIp3nS	měnit
teplota	teplota	k1gFnSc1	teplota
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
plyn	plyn	k1gInSc4	plyn
(	(	kIx(	(
<g/>
a	a	k8xC	a
daný	daný	k2eAgInSc1d1	daný
tlak	tlak	k1gInSc1	tlak
<g/>
)	)	kIx)	)
existuje	existovat	k5eAaImIp3nS	existovat
tzv.	tzv.	kA	tzv.
inverzní	inverzní	k2eAgFnSc1d1	inverzní
teplota	teplota	k1gFnSc1	teplota
<g/>
;	;	kIx,	;
expanduje	expandovat	k5eAaImIp3nS	expandovat
<g/>
-li	i	k?	-li
přes	přes	k7c4	přes
přepážku	přepážka	k1gFnSc4	přepážka
plyn	plyn	k1gInSc1	plyn
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
nižší	nízký	k2eAgFnSc7d2	nižší
než	než	k8xS	než
inverzní	inverzní	k2eAgFnSc7d1	inverzní
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
plyn	plyn	k1gInSc1	plyn
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
ke	k	k7c3	k
zkapalňování	zkapalňování	k1gNnSc3	zkapalňování
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
odstavci	odstavec	k1gInSc6	odstavec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
neznačí	značit	k5eNaImIp3nS	značit
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
koeficienty	koeficient	k1gInPc4	koeficient
difuze	difuze	k1gFnSc2	difuze
a	a	k8xC	a
(	(	kIx(	(
<g/>
dynamické	dynamický	k2eAgFnPc4d1	dynamická
<g/>
)	)	kIx)	)
viskozity	viskozita	k1gFnPc4	viskozita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
difuzi	difuze	k1gFnSc4	difuze
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koeficient	koeficient	k1gInSc1	koeficient
difuze	difuze	k1gFnSc2	difuze
roste	růst	k5eAaImIp3nS	růst
přímo	přímo	k6eAd1	přímo
úměrně	úměrně	k7c3	úměrně
druhé	druhý	k4xOgFnSc3	druhý
odmocnině	odmocnina	k1gFnSc3	odmocnina
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
∝	∝	k?	∝
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D	D	kA	D
<g/>
\	\	kIx~	\
<g/>
propto	propt	k2eAgNnSc1d1	propt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
U	u	k7c2	u
difuze	difuze	k1gFnSc2	difuze
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
roste	růst	k5eAaImIp3nS	růst
koeficient	koeficient	k1gInSc1	koeficient
difuze	difuze	k1gFnSc2	difuze
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
u	u	k7c2	u
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
viskozita	viskozita	k1gFnSc1	viskozita
plynů	plyn	k1gInPc2	plyn
roste	růst	k5eAaImIp3nS	růst
přímo	přímo	k6eAd1	přímo
úměrně	úměrně	k7c3	úměrně
druhé	druhý	k4xOgFnSc3	druhý
odmocnině	odmocnina	k1gFnSc3	odmocnina
teploty	teplota	k1gFnSc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
∝	∝	k?	∝
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
\	\	kIx~	\
<g/>
propto	propto	k1gNnSc1	propto
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
u	u	k7c2	u
kapalin	kapalina	k1gFnPc2	kapalina
naopak	naopak	k6eAd1	naopak
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
Boltzmannovým	Boltzmannův	k2eAgInSc7d1	Boltzmannův
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
exponenciálně	exponenciálně	k6eAd1	exponenciálně
klesá	klesat	k5eAaImIp3nS	klesat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
∝	∝	k?	∝
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
\	\	kIx~	\
<g/>
propto	propto	k1gNnSc1	propto
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnSc1	epsilon
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc3	epsilon
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k9	jako
aktivační	aktivační	k2eAgFnSc4d1	aktivační
energii	energie	k1gFnSc4	energie
k	k	k7c3	k
přesunutí	přesunutí	k1gNnSc3	přesunutí
molekuly	molekula	k1gFnSc2	molekula
kapaliny	kapalina	k1gFnSc2	kapalina
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
rovnovážné	rovnovážný	k2eAgFnSc2d1	rovnovážná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
o	o	k7c6	o
viskozitě	viskozita	k1gFnSc6	viskozita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
η	η	k?	η
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
eta	eta	k?	eta
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
kvadratická	kvadratický	k2eAgFnSc1d1	kvadratická
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
kulové	kulový	k2eAgFnSc2d1	kulová
částice	částice	k1gFnSc2	částice
poloměru	poloměr	k1gInSc2	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
unášené	unášený	k2eAgNnSc1d1	unášené
neuspořádaným	uspořádaný	k2eNgInSc7d1	neuspořádaný
pohybem	pohyb	k1gInSc7	pohyb
molekul	molekula	k1gFnPc2	molekula
od	od	k7c2	od
počátečního	počáteční	k2eAgInSc2d1	počáteční
bodu	bod	k1gInSc2	bod
za	za	k7c4	za
čas	čas	k1gInSc4	čas
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
rovna	roven	k2eAgFnSc1d1	rovna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⟩	⟩	k?	⟩
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
π	π	k?	π
r	r	kA	r
η	η	k?	η
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langla	k1gFnSc3	langla
l	l	kA	l
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
eta	eta	k?	eta
}}	}}	k?	}}
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
hybným	hybný	k2eAgInSc7d1	hybný
potenciálem	potenciál	k1gInSc7	potenciál
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
elektrického	elektrický	k2eAgInSc2d1	elektrický
potenciálu	potenciál	k1gInSc2	potenciál
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
el.	el.	k?	el.
proudu	proud	k1gInSc2	proud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
ustáleném	ustálený	k2eAgNnSc6d1	ustálené
teplotním	teplotní	k2eAgNnSc6d1	teplotní
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
tepelný	tepelný	k2eAgInSc1d1	tepelný
výkon	výkon	k1gInSc1	výkon
šířený	šířený	k2eAgInSc1d1	šířený
přes	přes	k7c4	přes
elementární	elementární	k2eAgFnSc4d1	elementární
plochu	plocha	k1gFnSc4	plocha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
přímo	přímo	k6eAd1	přímo
úměrný	úměrný	k2eAgInSc1d1	úměrný
záporně	záporně	k6eAd1	záporně
vzatému	vzatý	k2eAgInSc3d1	vzatý
gradientu	gradient	k1gInSc3	gradient
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
koeficientem	koeficient	k1gInSc7	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
je	být	k5eAaImIp3nS	být
tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnPc3	lambda
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
λ	λ	k?	λ
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
S	s	k7c7	s
∇	∇	k?	∇
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
Q	Q	kA	Q
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k1gMnSc1	nabla
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
neustálené	ustálený	k2eNgNnSc4d1	neustálené
teplotní	teplotní	k2eAgNnSc4d1	teplotní
pole	pole	k1gNnSc4	pole
platí	platit	k5eAaImIp3nS	platit
rovnice	rovnice	k1gFnSc1	rovnice
vedení	vedení	k1gNnSc2	vedení
tepla	teplo	k1gNnSc2	teplo
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
∇	∇	k?	∇
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
partial	partial	k1gInSc1	partial
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
nabla	nabla	k1gFnSc1	nabla
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
c	c	k0	c
ρ	ρ	k?	ρ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
teplotní	teplotní	k2eAgFnSc1d1	teplotní
vodivost	vodivost	k1gFnSc1	vodivost
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Fázový	fázový	k2eAgInSc1d1	fázový
přechod	přechod	k1gInSc1	přechod
je	být	k5eAaImIp3nS	být
skokovou	skokový	k2eAgFnSc7d1	skoková
změnou	změna	k1gFnSc7	změna
nějaké	nějaký	k3yIgFnSc2	nějaký
vlastnosti	vlastnost	k1gFnSc2	vlastnost
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc1	její
derivace	derivace	k1gFnSc1	derivace
<g/>
)	)	kIx)	)
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
nějaké	nějaký	k3yIgFnSc2	nějaký
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžný	běžný	k2eAgInSc4d1	běžný
jednosložkový	jednosložkový	k2eAgInSc4d1	jednosložkový
systém	systém	k1gInSc4	systém
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
změny	změna	k1gFnPc4	změna
jeho	jeho	k3xOp3gNnPc2	jeho
skupenství	skupenství	k1gNnPc2	skupenství
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
změny	změna	k1gFnPc1	změna
jeho	jeho	k3xOp3gFnSc2	jeho
krystalické	krystalický	k2eAgFnSc2d1	krystalická
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
skupenství	skupenství	k1gNnSc2	skupenství
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
Clausiovu-Clapeyronovu	Clausiovu-Clapeyronův	k2eAgFnSc4d1	Clausiovu-Clapeyronův
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
T	T	kA	T
Δ	Δ	k?	Δ
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
T	T	kA	T
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H	H	kA	H
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
správněji	správně	k6eAd2	správně
entalpie	entalpie	k1gFnSc1	entalpie
skupenské	skupenský	k2eAgFnSc2d1	skupenská
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
změna	změna	k1gFnSc1	změna
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fázovém	fázový	k2eAgInSc6d1	fázový
diagramu	diagram	k1gInSc6	diagram
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
důležitých	důležitý	k2eAgInPc2d1	důležitý
teplotních	teplotní	k2eAgInPc2d1	teplotní
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
specifických	specifický	k2eAgInPc2d1	specifický
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kritickou	kritický	k2eAgFnSc4d1	kritická
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
níž	jenž	k3xRgFnSc7	jenž
již	již	k9	již
nelze	lze	k6eNd1	lze
změnou	změna	k1gFnSc7	změna
tlaku	tlak	k1gInSc2	tlak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
kapalného	kapalný	k2eAgNnSc2d1	kapalné
skupenství	skupenství	k1gNnSc2	skupenství
teplotu	teplota	k1gFnSc4	teplota
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
současně	současně	k6eAd1	současně
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vícesložkových	vícesložkový	k2eAgInPc2d1	vícesložkový
systémů	systém	k1gInPc2	systém
má	mít	k5eAaImIp3nS	mít
teplota	teplota	k1gFnSc1	teplota
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mísení	mísení	k1gNnSc4	mísení
resp.	resp.	kA	resp.
oddělování	oddělování	k1gNnSc4	oddělování
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
koncentracemi	koncentrace	k1gFnPc7	koncentrace
těchto	tento	k3xDgFnPc2	tento
složek	složka	k1gFnPc2	složka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncentrace	koncentrace	k1gFnSc1	koncentrace
příměsí	příměs	k1gFnPc2	příměs
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
teploty	teplota	k1gFnPc4	teplota
-	-	kIx~	-
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
např.	např.	kA	např.
změnu	změna	k1gFnSc4	změna
teploty	teplota	k1gFnSc2	teplota
skupenských	skupenský	k2eAgFnPc2d1	skupenská
přeměn	přeměna	k1gFnPc2	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dostatečně	dostatečně	k6eAd1	dostatečně
zředěné	zředěný	k2eAgInPc4d1	zředěný
roztoky	roztok	k1gInPc4	roztok
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
vliv	vliv	k1gInSc1	vliv
popsán	popsat	k5eAaPmNgInS	popsat
Raoultovým	Raoultův	k2eAgInSc7d1	Raoultův
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
rozpuštěnou	rozpuštěný	k2eAgFnSc7d1	rozpuštěná
příměsí	příměs	k1gFnSc7	příměs
sníží	snížit	k5eAaPmIp3nS	snížit
teplota	teplota	k1gFnSc1	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
roztoku	roztok	k1gInSc2	roztok
oproti	oproti	k7c3	oproti
teplotě	teplota	k1gFnSc3	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
čistého	čistý	k2eAgNnSc2d1	čisté
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
úměrně	úměrně	k6eAd1	úměrně
molární	molární	k2eAgFnSc3d1	molární
koncentraci	koncentrace	k1gFnSc3	koncentrace
příměsi	příměs	k1gFnSc2	příměs
<g/>
.	.	kIx.	.
</s>
<s>
Koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kryoskopická	kryoskopický	k2eAgFnSc1d1	kryoskopický
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
úměrně	úměrně	k6eAd1	úměrně
molární	molární	k2eAgFnSc4d1	molární
koncentraci	koncentrace	k1gFnSc4	koncentrace
příměsi	příměs	k1gFnSc2	příměs
<g/>
,	,	kIx,	,
koeficient	koeficient	k1gInSc1	koeficient
úměrnosti	úměrnost	k1gFnSc2	úměrnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ebulioskopická	ebulioskopický	k2eAgFnSc1d1	ebulioskopický
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
kryoskopický	kryoskopický	k2eAgInSc1d1	kryoskopický
jev	jev	k1gInSc1	jev
má	mít	k5eAaImIp3nS	mít
široké	široký	k2eAgNnSc4d1	široké
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
-	-	kIx~	-
známou	známý	k2eAgFnSc7d1	známá
aplikací	aplikace	k1gFnSc7	aplikace
je	být	k5eAaImIp3nS	být
zimní	zimní	k2eAgNnSc1d1	zimní
solení	solení	k1gNnSc1	solení
vozovek	vozovka	k1gFnPc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
podstaty	podstata	k1gFnSc2	podstata
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vlastnosti	vlastnost	k1gFnPc4	vlastnost
spojitých	spojitý	k2eAgNnPc2d1	spojité
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
,	,	kIx,	,
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
mechanice	mechanika	k1gFnSc6	mechanika
kontinua	kontinuum	k1gNnSc2	kontinuum
(	(	kIx(	(
<g/>
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
úzce	úzko	k6eAd1	úzko
spojené	spojený	k2eAgFnSc3d1	spojená
akustice	akustika	k1gFnSc3	akustika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
poměru	poměr	k1gInSc2	poměr
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
částic	částice	k1gFnPc2	částice
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
jejich	jejich	k3xOp3gFnPc2	jejich
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
<g/>
:	:	kIx,	:
zvýšení	zvýšení	k1gNnSc4	zvýšení
mezičásticových	mezičásticův	k2eAgFnPc2d1	mezičásticův
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
změny	změna	k1gFnSc2	změna
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
struktury	struktura	k1gFnSc2	struktura
částečné	částečný	k2eAgNnSc4d1	částečné
nebo	nebo	k8xC	nebo
úplné	úplný	k2eAgNnSc4d1	úplné
uvolnění	uvolnění	k1gNnSc4	uvolnění
částic	částice	k1gFnPc2	částice
překonání	překonání	k1gNnSc2	překonání
potenciálových	potenciálový	k2eAgFnPc2d1	potenciálová
bariér	bariéra	k1gFnPc2	bariéra
pro	pro	k7c4	pro
chemické	chemický	k2eAgFnPc4d1	chemická
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopicky	makroskopicky	k6eAd1	makroskopicky
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
projeví	projevit	k5eAaPmIp3nP	projevit
jako	jako	k9	jako
<g/>
:	:	kIx,	:
zvýšení	zvýšení	k1gNnSc1	zvýšení
objemu	objem	k1gInSc2	objem
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
zvýšení	zvýšení	k1gNnSc3	zvýšení
objemu	objem	k1gInSc2	objem
nebo	nebo	k8xC	nebo
tlaku	tlak	k1gInSc2	tlak
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
teplotní	teplotní	k2eAgFnSc1d1	teplotní
roztažnost	roztažnost	k1gFnSc1	roztažnost
a	a	k8xC	a
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
změny	změna	k1gFnPc1	změna
teplot	teplota	k1gFnPc2	teplota
budou	být	k5eAaImBp3nP	být
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c6	v
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
odstupu	odstup	k1gInSc6	odstup
od	od	k7c2	od
teplot	teplota	k1gFnPc2	teplota
fázových	fázový	k2eAgInPc2d1	fázový
přechodů	přechod	k1gInPc2	přechod
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
změny	změna	k1gFnPc4	změna
objemu	objem	k1gInSc2	objem
přiblížit	přiblížit	k5eAaPmF	přiblížit
u	u	k7c2	u
kondenzovaných	kondenzovaný	k2eAgFnPc2d1	kondenzovaná
<g />
.	.	kIx.	.
</s>
<s hack="1">
látek	látka	k1gFnPc2	látka
lineární	lineární	k2eAgFnSc7d1	lineární
závislostí	závislost	k1gFnSc7	závislost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
V	V	kA	V
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
γ	γ	k?	γ
⋅	⋅	k?	⋅
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
V	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
plyny	plyn	k1gInPc4	plyn
přímou	přímý	k2eAgFnSc7d1	přímá
úměrností	úměrnost	k1gFnSc7	úměrnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
pV	pV	k?	pV
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p_	p_	k?	p_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
V_	V_	k1gFnSc2	V_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
indexem	index	k1gInSc7	index
0	[number]	k4	0
značeny	značen	k2eAgFnPc1d1	značena
referenční	referenční	k2eAgFnPc1d1	referenční
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Přesně	přesně	k6eAd1	přesně
a	a	k8xC	a
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rozsahu	rozsah	k1gInSc6	rozsah
teplot	teplota	k1gFnPc2	teplota
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
ideální	ideální	k2eAgInSc4d1	ideální
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
změny	změna	k1gFnPc1	změna
reologických	reologický	k2eAgFnPc2d1	reologická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
změny	změna	k1gFnPc1	změna
tuhostí	tuhost	k1gFnSc7	tuhost
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
snížení	snížení	k1gNnSc1	snížení
u	u	k7c2	u
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc2	zvýšení
např.	např.	kA	např.
u	u	k7c2	u
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gFnPc2	jejich
převrácených	převrácený	k2eAgFnPc2d1	převrácená
hodnot	hodnota	k1gFnPc2	hodnota
-	-	kIx~	-
modulů	modul	k1gInPc2	modul
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
změny	změna	k1gFnPc1	změna
jejich	jejich	k3xOp3gFnSc2	jejich
anizotropie	anizotropie	k1gFnSc2	anizotropie
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
změny	změna	k1gFnSc2	změna
mezí	mez	k1gFnPc2	mez
úměrnosti	úměrnost	k1gFnSc2	úměrnost
<g/>
,	,	kIx,	,
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
,	,	kIx,	,
kluzu	kluz	k1gInSc2	kluz
a	a	k8xC	a
pevnosti	pevnost	k1gFnSc2	pevnost
u	u	k7c2	u
deformace	deformace	k1gFnSc2	deformace
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
a	a	k8xC	a
obdobné	obdobný	k2eAgFnPc4d1	obdobná
změny	změna	k1gFnPc4	změna
mezních	mezní	k2eAgInPc2d1	mezní
bodů	bod	k1gInPc2	bod
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
deformace	deformace	k1gFnSc2	deformace
změny	změna	k1gFnSc2	změna
viskozity	viskozita	k1gFnSc2	viskozita
tekutin	tekutina	k1gFnPc2	tekutina
změny	změna	k1gFnSc2	změna
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
objemů	objem	k1gInPc2	objem
a	a	k8xC	a
tuhostí	tuhost	k1gFnPc2	tuhost
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
kapalin	kapalina	k1gFnPc2	kapalina
se	se	k3xPyFc4	se
také	také	k9	také
projeví	projevit	k5eAaPmIp3nS	projevit
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
rychlosti	rychlost	k1gFnSc2	rychlost
šíření	šíření	k1gNnSc2	šíření
mechanických	mechanický	k2eAgFnPc2d1	mechanická
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vln	vlna	k1gFnPc2	vlna
zvukových	zvukový	k2eAgFnPc2d1	zvuková
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
teploty	teplota	k1gFnSc2	teplota
změní	změnit	k5eAaPmIp3nS	změnit
modul	modul	k1gInSc1	modul
pružnosti	pružnost	k1gFnSc2	pružnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
-krát	rát	k1gInSc1	-krát
a	a	k8xC	a
objem	objem	k1gInSc1	objem
(	(	kIx(	(
<g/>
izotropně	izotropně	k6eAd1	izotropně
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
gamma	gammum	k1gNnSc2	gammum
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
-krát	rát	k1gInSc1	-krát
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
mechanických	mechanický	k2eAgFnPc2d1	mechanická
a	a	k8xC	a
zvukových	zvukový	k2eAgFnPc2d1	zvuková
vln	vlna	k1gFnPc2	vlna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}}}	}}}	k?	}}}
-krát	rát	k1gInSc1	-krát
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
u	u	k7c2	u
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
podélným	podélný	k2eAgFnPc3d1	podélná
vlnám	vlna	k1gFnPc3	vlna
modul	modul	k1gInSc1	modul
pružnosti	pružnost	k1gFnSc2	pružnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
<g/>
,	,	kIx,	,
příčným	příčný	k2eAgFnPc3d1	příčná
vlnám	vlna	k1gFnPc3	vlna
modul	modul	k1gInSc1	modul
pružnosti	pružnost	k1gFnSc2	pružnost
ve	v	k7c6	v
smyku	smyk	k1gInSc6	smyk
a	a	k8xC	a
podélným	podélný	k2eAgFnPc3d1	podélná
vlnám	vlna	k1gFnPc3	vlna
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
modul	modul	k1gInSc1	modul
objemové	objemový	k2eAgFnSc2d1	objemová
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
suchý	suchý	k2eAgInSc4d1	suchý
vzduch	vzduch	k1gInSc4	vzduch
lze	lze	k6eAd1	lze
závislost	závislost	k1gFnSc4	závislost
rychlosti	rychlost	k1gFnSc2	rychlost
šíření	šíření	k1gNnSc2	šíření
zvuku	zvuk	k1gInSc2	zvuk
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
zapsat	zapsat	k5eAaPmF	zapsat
vztahem	vztah	k1gInSc7	vztah
(	(	kIx(	(
<g/>
indexem	index	k1gInSc7	index
0	[number]	k4	0
značeny	značen	k2eAgFnPc1d1	značena
referenční	referenční	k2eAgFnPc1d1	referenční
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
c	c	k0	c
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}}	}}}}}	k?	}}}}}
.	.	kIx.	.
</s>
<s>
Polarizační	polarizační	k2eAgFnPc1d1	polarizační
a	a	k8xC	a
magnetizační	magnetizační	k2eAgFnPc1d1	magnetizační
vlastnosti	vlastnost	k1gFnPc1	vlastnost
dielektrik	dielektrikum	k1gNnPc2	dielektrikum
a	a	k8xC	a
magnetik	magnetikum	k1gNnPc2	magnetikum
lze	lze	k6eAd1	lze
vhodně	vhodně	k6eAd1	vhodně
popsat	popsat	k5eAaPmF	popsat
pomocí	pomocí	k7c2	pomocí
elektrické	elektrický	k2eAgFnSc2d1	elektrická
a	a	k8xC	a
magnetické	magnetický	k2eAgFnSc2d1	magnetická
susceptibility	susceptibilita	k1gFnSc2	susceptibilita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
dielektrik	dielektrikum	k1gNnPc2	dielektrikum
lze	lze	k6eAd1	lze
teplotní	teplotní	k2eAgFnSc1d1	teplotní
závislost	závislost	k1gFnSc1	závislost
elektrické	elektrický	k2eAgFnSc2d1	elektrická
susceptibility	susceptibilita	k1gFnSc2	susceptibilita
zapsat	zapsat	k5eAaPmF	zapsat
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
látkově	látkově	k6eAd1	látkově
specifické	specifický	k2eAgFnPc1d1	specifická
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
diamagnetik	diamagnetika	k1gFnPc2	diamagnetika
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
paramagnetika	paramagnetikum	k1gNnPc4	paramagnetikum
je	být	k5eAaImIp3nS	být
magnetická	magnetický	k2eAgFnSc1d1	magnetická
susceptibilita	susceptibilita	k1gFnSc1	susceptibilita
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
některá	některý	k3yIgNnPc4	některý
paramagnetika	paramagnetikum	k1gNnPc4	paramagnetikum
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
Curieovým	Curieův	k2eAgInSc7d1	Curieův
zákonem	zákon	k1gInSc7	zákon
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
tedy	tedy	k9	tedy
teplotní	teplotní	k2eAgFnSc4d1	teplotní
závislost	závislost	k1gFnSc4	závislost
magnetické	magnetický	k2eAgFnSc2d1	magnetická
susceptibility	susceptibilita	k1gFnSc2	susceptibilita
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
látkově	látkově	k6eAd1	látkově
specifické	specifický	k2eAgFnPc1d1	specifická
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
elektricky	elektricky	k6eAd1	elektricky
a	a	k8xC	a
magneticky	magneticky	k6eAd1	magneticky
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
feroelektrika	feroelektrika	k1gFnSc1	feroelektrika
<g/>
,	,	kIx,	,
feromagnetika	feromagnetika	k1gFnSc1	feromagnetika
a	a	k8xC	a
antiferomagnetika	antiferomagnetika	k1gFnSc1	antiferomagnetika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
jistá	jistý	k2eAgFnSc1d1	jistá
mezní	mezní	k2eAgFnSc1d1	mezní
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
feroelektrická	feroelektrický	k2eAgFnSc1d1	feroelektrická
Curieova	Curieův	k2eAgFnSc1d1	Curieova
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
feromagnetická	feromagnetický	k2eAgFnSc1d1	feromagnetická
Curieova	Curieův	k2eAgFnSc1d1	Curieova
teplota	teplota	k1gFnSc1	teplota
resp.	resp.	kA	resp.
Néelova	Néelův	k2eAgFnSc1d1	Néelův
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
antiferomagnetika	antiferomagnetik	k1gMnSc4	antiferomagnetik
<g/>
)	)	kIx)	)
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
spontánní	spontánní	k2eAgNnSc4d1	spontánní
uspořádání	uspořádání	k1gNnSc4	uspořádání
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
spontánní	spontánní	k2eAgFnSc1d1	spontánní
polarizace	polarizace	k1gFnSc1	polarizace
resp.	resp.	kA	resp.
magnetizace	magnetizace	k1gFnSc1	magnetizace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
nad	nad	k7c7	nad
mezní	mezní	k2eAgFnSc7d1	mezní
teplotou	teplota	k1gFnSc7	teplota
tyto	tento	k3xDgFnPc4	tento
látky	látka	k1gFnPc4	látka
přecházejí	přecházet	k5eAaImIp3nP	přecházet
na	na	k7c6	na
paraelektrické	paraelektrický	k2eAgFnSc6d1	paraelektrický
a	a	k8xC	a
paramagnetické	paramagnetický	k2eAgFnSc6d1	paramagnetická
<g/>
.	.	kIx.	.
</s>
<s>
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
lze	lze	k6eAd1	lze
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
okolí	okolí	k1gNnSc2	okolí
mezní	mezní	k2eAgFnSc2d1	mezní
teploty	teplota	k1gFnSc2	teplota
popsat	popsat	k5eAaPmF	popsat
obdobnými	obdobný	k2eAgInPc7d1	obdobný
vztahy	vztah	k1gInPc7	vztah
pro	pro	k7c4	pro
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
i	i	k8xC	i
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
susceptibilitu	susceptibilita	k1gFnSc4	susceptibilita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
teploty	teplota	k1gFnPc4	teplota
pod	pod	k7c7	pod
mezní	mezní	k2eAgFnSc7d1	mezní
teplotou	teplota	k1gFnSc7	teplota
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tzv.	tzv.	kA	tzv.
Blochův	Blochův	k2eAgInSc4d1	Blochův
zákon	zákon	k1gInSc4	zákon
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
T	T	kA	T
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
C	C	kA	C
''	''	k?	''
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
<g/>
''	''	k?	''
<g/>
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
resp.	resp.	kA	resp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
T	T	kA	T
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
C	C	kA	C
''	''	k?	''
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
C	C	kA	C
<g/>
''	''	k?	''
<g/>
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
''	''	k?	''
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
''	''	k?	''
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
látkově	látkově	k6eAd1	látkově
specifická	specifický	k2eAgFnSc1d1	specifická
konstanta	konstanta	k1gFnSc1	konstanta
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
chi	chi	k0	chi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
susceptibility	susceptibilita	k1gFnPc1	susceptibilita
při	při	k7c6	při
maximálním	maximální	k2eAgNnSc6d1	maximální
uspořádání	uspořádání	k1gNnSc6	uspořádání
(	(	kIx(	(
<g/>
při	při	k7c6	při
nulové	nulový	k2eAgFnSc6d1	nulová
absolutní	absolutní	k2eAgFnSc6d1	absolutní
teplotě	teplota	k1gFnSc6	teplota
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
teploty	teplota	k1gFnPc4	teplota
nad	nad	k7c7	nad
mezní	mezní	k2eAgFnSc7d1	mezní
teplotou	teplota	k1gFnSc7	teplota
<g />
.	.	kIx.	.
</s>
<s hack="1">
vztah	vztah	k1gInSc1	vztah
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
feromagnetika	feromagnetik	k1gMnSc4	feromagnetik
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Curieův-Weissův	Curieův-Weissův	k2eAgInSc1d1	Curieův-Weissův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
−	−	k?	−
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T-	T-	k1gMnSc1	T-
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gMnSc1	Theta
}}}	}}}	k?	}}}
resp.	resp.	kA	resp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
m	m	kA	m
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
−	−	k?	−
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T-	T-	k1gMnSc1	T-
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gMnSc1	Theta
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
látkově	látkově	k6eAd1	látkově
specifická	specifický	k2eAgFnSc1d1	specifická
konstanta	konstanta	k1gFnSc1	konstanta
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Theta	Theto	k1gNnSc2	Theto
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
parametr	parametr	k1gInSc1	parametr
s	s	k7c7	s
rozměrem	rozměr	k1gInSc7	rozměr
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
feroelektrika	feroelektrik	k1gMnSc4	feroelektrik
a	a	k8xC	a
feromagnetika	feromagnetik	k1gMnSc4	feromagnetik
blízký	blízký	k2eAgInSc1d1	blízký
Curieově	Curieův	k2eAgFnSc3d1	Curieova
teplotě	teplota	k1gFnSc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
plazmatu	plazma	k1gNnSc2	plazma
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vodivé	vodivý	k2eAgNnSc4d1	vodivé
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možné	možný	k2eAgNnSc1d1	možné
zavést	zavést	k5eAaPmF	zavést
dielektrickou	dielektrický	k2eAgFnSc4d1	dielektrická
konstantu	konstanta	k1gFnSc4	konstanta
jako	jako	k8xC	jako
obdobu	obdoba	k1gFnSc4	obdoba
permitivity	permitivita	k1gFnSc2	permitivita
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Langmuirovské	Langmuirovský	k2eAgInPc1d1	Langmuirovský
kmity	kmit	k1gInPc1	kmit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obdobu	obdoba	k1gFnSc4	obdoba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
susceptibility	susceptibilita	k1gFnSc2	susceptibilita
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
vlny	vlna	k1gFnPc4	vlna
s	s	k7c7	s
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
frekvencí	frekvence	k1gFnSc7	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc3	omega
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
úhlovým	úhlový	k2eAgInSc7d1	úhlový
vlnočtem	vlnočet	k1gInSc7	vlnočet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc7	kappa
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
}	}	kIx)	}
v	v	k7c6	v
elektronovém	elektronový	k2eAgNnSc6d1	elektronové
plazmatu	plazma	k1gNnSc6	plazma
závislost	závislost	k1gFnSc4	závislost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
χ	χ	k?	χ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
ω	ω	k?	ω
,	,	kIx,	,
κ	κ	k?	κ
)	)	kIx)	)
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
chi	chi	k0	chi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc2	kappa
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
kT	kT	k?	kT
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
,	,	kIx,	,
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
permitivita	permitivita	k1gFnSc1	permitivita
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
početní	početní	k2eAgFnSc1d1	početní
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgFnSc1d1	teplotní
závislost	závislost	k1gFnSc1	závislost
rezistivity	rezistivita	k1gFnSc2	rezistivita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
vodičů	vodič	k1gInPc2	vodič
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
rozsah	rozsah	k1gInSc4	rozsah
teplot	teplota	k1gFnPc2	teplota
přiblížit	přiblížit	k5eAaPmF	přiblížit
lineárním	lineární	k2eAgInSc7d1	lineární
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
=	=	kIx~	=
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
+	+	kIx~	+
α	α	k?	α
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
závislost	závislost	k1gFnSc4	závislost
lze	lze	k6eAd1	lze
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
v	v	k7c6	v
kinetické	kinetický	k2eAgFnSc6d1	kinetická
teorii	teorie	k1gFnSc6	teorie
a	a	k8xC	a
statistické	statistický	k2eAgFnSc3d1	statistická
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
:	:	kIx,	:
Rezistivita	Rezistivita	k1gFnSc1	Rezistivita
vodičů	vodič	k1gInPc2	vodič
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
příspěvky	příspěvek	k1gInPc4	příspěvek
třech	tři	k4xCgInPc2	tři
mechanismů	mechanismus	k1gInPc2	mechanismus
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
:	:	kIx,	:
příspěvek	příspěvek	k1gInSc1	příspěvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
im	im	k?	im
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
způsobený	způsobený	k2eAgInSc4d1	způsobený
rozptylem	rozptyl	k1gInSc7	rozptyl
elektronů	elektron	k1gInPc2	elektron
na	na	k7c6	na
příměsových	příměsův	k2eAgInPc6d1	příměsův
atomech	atom	k1gInPc6	atom
je	být	k5eAaImIp3nS	být
teplotně	teplotně	k6eAd1	teplotně
přibližně	přibližně	k6eAd1	přibližně
nezávislý	závislý	k2eNgInSc1d1	nezávislý
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
koncentraci	koncentrace	k1gFnSc4	koncentrace
příměsových	příměsův	k2eAgInPc2d1	příměsův
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
příspěvek	příspěvek	k1gInSc1	příspěvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ph	ph	kA	ph
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
způsobený	způsobený	k2eAgInSc4d1	způsobený
rozptylem	rozptyl	k1gInSc7	rozptyl
elektronů	elektron	k1gInPc2	elektron
kmitáním	kmitání	k1gNnPc3	kmitání
mřížky	mřížka	k1gFnSc2	mřížka
kovového	kovový	k2eAgInSc2d1	kovový
krystalu	krystal	k1gInSc2	krystal
<g/>
,	,	kIx,	,
příspěvek	příspěvek	k1gInSc1	příspěvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
el	ela	k1gFnPc2	ela
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
způsobený	způsobený	k2eAgInSc1d1	způsobený
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
působením	působení	k1gNnSc7	působení
elektronů	elektron	k1gInPc2	elektron
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
příspěvky	příspěvek	k1gInPc1	příspěvek
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
teploty	teplota	k1gFnPc4	teplota
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
tzv.	tzv.	kA	tzv.
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
teplota	teplota	k1gFnSc1	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
rovná	rovnat	k5eAaImIp3nS	rovnat
podílu	podíl	k1gInSc3	podíl
energie	energie	k1gFnSc2	energie
<g />
.	.	kIx.	.
</s>
<s hack="1">
fononu	fonon	k1gInSc2	fonon
elastických	elastický	k2eAgFnPc2d1	elastická
vln	vlna	k1gFnPc2	vlna
v	v	k7c6	v
krystalu	krystal	k1gInSc6	krystal
a	a	k8xC	a
Boltzmannovy	Boltzmannův	k2eAgFnPc1d1	Boltzmannova
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
několik	několik	k4yIc1	několik
setin	setina	k1gFnPc2	setina
kelvinu	kelvin	k1gInSc2	kelvin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
příspěvek	příspěvek	k1gInSc4	příspěvek
od	od	k7c2	od
kmitů	kmit	k1gInPc2	kmit
mřížky	mřížka	k1gFnSc2	mřížka
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∝	∝	k?	∝
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ph	ph	kA	ph
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
propto	propto	k1gNnSc1	propto
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
,	,	kIx,	,
e	e	k0	e
,	,	kIx,	,
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
hbar	hbara	k1gFnPc2	hbara
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgInSc1d1	elementární
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
početní	početní	k2eAgFnSc1d1	početní
hustota	hustota	k1gFnSc1	hustota
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
teploty	teplota	k1gFnPc4	teplota
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
Debyeova	Debyeův	k2eAgFnSc1d1	Debyeova
teplota	teplota	k1gFnSc1	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
příspěvek	příspěvek	k1gInSc1	příspěvek
od	od	k7c2	od
rozptylu	rozptyl	k1gInSc2	rozptyl
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s hack="1">
příměsích	příměs	k1gFnPc6	příměs
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zbylé	zbylý	k2eAgInPc4d1	zbylý
dva	dva	k4xCgInPc4	dva
příspěvky	příspěvek	k1gInPc4	příspěvek
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
h	h	k?	h
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
ph	ph	kA	ph
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
<g />
.	.	kIx.	.
</s>
<s>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
resp.	resp.	kA	resp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s hack="1">
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
k	k	k7c3	k
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ξ	ξ	k?	ξ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
el	ela	k1gFnPc2	ela
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
<g />
.	.	kIx.	.
</s>
<s>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
kT	kT	k?	kT
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}}}	}}}	k?	}}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
ξ	ξ	k?	ξ
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
F	F	kA	F
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T_	T_	k1gFnSc1	T_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}	}	kIx)	}
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
Fermiho	Fermi	k1gMnSc4	Fermi
energie	energie	k1gFnSc2	energie
elektronového	elektronový	k2eAgInSc2d1	elektronový
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
opravný	opravný	k2eAgInSc1d1	opravný
faktor	faktor	k1gInSc1	faktor
řádu	řád	k1gInSc2	řád
nejvýše	nejvýše	k6eAd1	nejvýše
1	[number]	k4	1
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rezistivitu	rezistivita	k1gFnSc4	rezistivita
polovodičů	polovodič	k1gInPc2	polovodič
(	(	kIx(	(
<g/>
při	při	k7c6	při
uvažování	uvažování	k1gNnSc6	uvažování
mechanismu	mechanismus	k1gInSc2	mechanismus
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vodivosti	vodivost	k1gFnSc2	vodivost
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
z	z	k7c2	z
pásového	pásový	k2eAgInSc2d1	pásový
modelu	model	k1gInSc2	model
odvodit	odvodit	k5eAaPmF	odvodit
následující	následující	k2eAgFnSc4d1	následující
teplotní	teplotní	k2eAgFnSc4d1	teplotní
závislost	závislost	k1gFnSc4	závislost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
=	=	kIx~	=
Ξ	Ξ	k?	Ξ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Xi	Xi	k1gFnSc1	Xi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
kT	kT	k?	kT
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
Ξ	Ξ	k?	Ξ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Xi	Xi	k1gFnSc1	Xi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
energetická	energetický	k2eAgFnSc1d1	energetická
šířka	šířka	k1gFnSc1	šířka
zakázaného	zakázaný	k2eAgInSc2d1	zakázaný
pásu	pás	k1gInSc2	pás
resp.	resp.	kA	resp.
materiálová	materiálový	k2eAgFnSc1d1	materiálová
konstanta	konstanta	k1gFnSc1	konstanta
slabě	slabě	k6eAd1	slabě
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Závislost	závislost	k1gFnSc1	závislost
rezistivity	rezistivita	k1gFnSc2	rezistivita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
elektronového	elektronový	k2eAgNnSc2d1	elektronové
plazmatu	plazma	k1gNnSc2	plazma
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
Drudeho	Drudeha	k1gFnSc5	Drudeha
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
explicitně	explicitně	k6eAd1	explicitně
vyjádřenou	vyjádřený	k2eAgFnSc7d1	vyjádřená
střední	střední	k2eAgFnSc7d1	střední
volnou	volný	k2eAgFnSc7d1	volná
dráhou	dráha	k1gFnSc7	dráha
elektronu	elektron	k1gInSc2	elektron
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
∝	∝	k?	∝
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
propto	propto	k1gNnSc1	propto
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}}}	}}}}	k?	}}}}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
kT	kT	k?	kT
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
e	e	k0	e
,	,	kIx,	,
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
permitivita	permitivita	k1gFnSc1	permitivita
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc4	hmotnost
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
proudu	proud	k1gInSc2	proud
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
vakuových	vakuový	k2eAgFnPc6d1	vakuová
elektronkách	elektronka	k1gFnPc6	elektronka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
uvolnění	uvolnění	k1gNnSc1	uvolnění
nosičů	nosič	k1gInPc2	nosič
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mechanismů	mechanismus	k1gInPc2	mechanismus
je	být	k5eAaImIp3nS	být
termická	termický	k2eAgFnSc1d1	termická
emise	emise	k1gFnSc1	emise
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
termoelektronová	termoelektronový	k2eAgFnSc1d1	termoelektronový
emise	emise	k1gFnSc1	emise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
statistické	statistický	k2eAgFnSc6d1	statistická
fyzice	fyzika	k1gFnSc6	fyzika
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
závislosti	závislost	k1gFnPc1	závislost
počtu	počet	k1gInSc2	počet
elektronů	elektron	k1gInPc2	elektron
ve	v	k7c6	v
vodivostním	vodivostní	k2eAgInSc6d1	vodivostní
pásu	pás	k1gInSc6	pás
schopných	schopný	k2eAgMnPc2d1	schopný
opustit	opustit	k5eAaPmF	opustit
katodu	katoda	k1gFnSc4	katoda
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
plyne	plynout	k5eAaImIp3nS	plynout
pro	pro	k7c4	pro
hustotu	hustota	k1gFnSc4	hustota
nasyceného	nasycený	k2eAgInSc2d1	nasycený
emisního	emisní	k2eAgInSc2d1	emisní
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
z	z	k7c2	z
jednotkové	jednotkový	k2eAgFnSc2d1	jednotková
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
následující	následující	k2eAgFnSc1d1	následující
teplotní	teplotní	k2eAgFnSc1d1	teplotní
závislost	závislost	k1gFnSc1	závislost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Richardsonův-Dushmanův	Richardsonův-Dushmanův	k2eAgInSc1d1	Richardsonův-Dushmanův
vztah	vztah	k1gInSc1	vztah
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
π	π	k?	π
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
j_	j_	k?	j_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
em_	em_	k?	em_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc4	hbar
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
W	W	kA	W
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
,	,	kIx,	,
ħ	ħ	k?	ħ
,	,	kIx,	,
e	e	k0	e
,	,	kIx,	,
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W	W	kA	W
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
výstupní	výstupní	k2eAgFnPc1d1	výstupní
práce	práce	k1gFnPc1	práce
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
daného	daný	k2eAgInSc2d1	daný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc4	hmotnost
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
termoelektrických	termoelektrický	k2eAgInPc2d1	termoelektrický
a	a	k8xC	a
termomagnetických	termomagnetický	k2eAgInPc2d1	termomagnetický
jevů	jev	k1gInPc2	jev
mnohé	mnohý	k2eAgInPc1d1	mnohý
přímo	přímo	k6eAd1	přímo
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
rozdílu	rozdíl	k1gInSc6	rozdíl
teplot	teplota	k1gFnPc2	teplota
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
teplotní	teplotní	k2eAgInSc4d1	teplotní
rozdíl	rozdíl	k1gInSc4	rozdíl
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
<g/>
:	:	kIx,	:
Seebeckův	Seebeckův	k2eAgInSc1d1	Seebeckův
termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
koncovými	koncový	k2eAgInPc7d1	koncový
průřezy	průřez	k1gInPc7	průřez
kovového	kovový	k2eAgInSc2d1	kovový
vodiče	vodič	k1gInSc2	vodič
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
udržován	udržován	k2eAgInSc4d1	udržován
teplotní	teplotní	k2eAgInSc4d1	teplotní
rozdíl	rozdíl	k1gInSc4	rozdíl
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
elektromotorické	elektromotorický	k2eAgNnSc1d1	elektromotorické
napětí	napětí	k1gNnSc1	napětí
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
elektrického	elektrický	k2eAgInSc2d1	elektrický
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gNnPc3	delta
\	\	kIx~	\
<g/>
varphi	varph	k1gMnSc3	varph
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
φ	φ	k?	φ
=	=	kIx~	=
α	α	k?	α
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gNnPc3	delta
\	\	kIx~	\
<g/>
varphi	varph	k1gMnSc3	varph
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
koeficient	koeficient	k1gInSc1	koeficient
<g/>
.	.	kIx.	.
</s>
<s>
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
termoelektrický	termoelektrický	k2eAgInSc1d1	termoelektrický
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Prochází	procházet	k5eAaImIp3nS	procházet
<g/>
-li	i	k?	-li
homogenním	homogenní	k2eAgInSc7d1	homogenní
vodičem	vodič	k1gInSc7	vodič
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
místech	místo	k1gNnPc6	místo
stejná	stejný	k2eAgFnSc1d1	stejná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
hustoty	hustota	k1gFnSc2	hustota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
(	(	kIx(	(
<g/>
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
Jouleova	Jouleův	k2eAgNnSc2d1	Jouleovo
tepla	teplo	k1gNnSc2	teplo
<g/>
)	)	kIx)	)
teplo	teplo	k1gNnSc1	teplo
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
výkonu	výkon	k1gInSc2	výkon
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
t	t	k?	t
:	:	kIx,	:
∂	∂	k?	∂
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
−	−	k?	−
ξ	ξ	k?	ξ
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
⋅	⋅	k?	⋅
∇	∇	k?	∇
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
partial	partiat	k5eAaImAgMnS	partiat
V	V	kA	V
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
nabla	nabla	k1gMnSc1	nabla
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ξ	ξ	k?	ξ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
xi	xi	k?	xi
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
koeficient	koeficient	k1gInSc1	koeficient
<g/>
.	.	kIx.	.
</s>
<s>
Ettingshausenův	Ettingshausenův	k2eAgInSc1d1	Ettingshausenův
termomagnetický	termomagnetický	k2eAgInSc1d1	termomagnetický
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
kolmo	kolmo	k6eAd1	kolmo
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
vektoru	vektor	k1gInSc2	vektor
magnetické	magnetický	k2eAgFnSc2d1	magnetická
indukce	indukce	k1gFnSc2	indukce
vzniká	vznikat	k5eAaImIp3nS	vznikat
vedle	vedle	k7c2	vedle
příčného	příčný	k2eAgNnSc2d1	příčné
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
(	(	kIx(	(
<g/>
Hallův	Hallův	k2eAgInSc1d1	Hallův
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
také	také	k9	také
příčný	příčný	k2eAgInSc1d1	příčný
teplotní	teplotní	k2eAgInSc1d1	teplotní
rozdíl	rozdíl	k1gInSc1	rozdíl
úměrný	úměrný	k2eAgInSc1d1	úměrný
proudové	proudový	k2eAgFnSc3d1	proudová
hustotě	hustota	k1gFnSc3	hustota
a	a	k8xC	a
magnetické	magnetický	k2eAgFnSc3d1	magnetická
indukci	indukce	k1gFnSc3	indukce
<g/>
.	.	kIx.	.
</s>
<s>
Righiův-Leducův	Righiův-Leducův	k2eAgInSc1d1	Righiův-Leducův
termomagnetický	termomagnetický	k2eAgInSc1d1	termomagnetický
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
kolmo	kolmo	k6eAd1	kolmo
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
vektoru	vektor	k1gInSc2	vektor
magnetické	magnetický	k2eAgFnSc2d1	magnetická
indukce	indukce	k1gFnSc2	indukce
vzniká	vznikat	k5eAaImIp3nS	vznikat
vedle	vedle	k7c2	vedle
příčného	příčný	k2eAgNnSc2d1	příčné
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
(	(	kIx(	(
<g/>
Nernstův	Nernstův	k2eAgInSc1d1	Nernstův
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
také	také	k9	také
příčný	příčný	k2eAgInSc1d1	příčný
teplotní	teplotní	k2eAgInSc1d1	teplotní
rozdíl	rozdíl	k1gInSc1	rozdíl
úměrný	úměrný	k2eAgInSc1d1	úměrný
proudové	proudový	k2eAgFnSc3d1	proudová
hustotě	hustota	k1gFnSc3	hustota
a	a	k8xC	a
hustotě	hustota	k1gFnSc3	hustota
tepelného	tepelný	k2eAgInSc2d1	tepelný
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Spinový	Spinový	k2eAgInSc1d1	Spinový
Seebeckův	Seebeckův	k2eAgInSc1d1	Seebeckův
termomagnetický	termomagnetický	k2eAgInSc1d1	termomagnetický
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
koncovými	koncový	k2eAgInPc7d1	koncový
průřezy	průřez	k1gInPc7	průřez
zmagnetované	zmagnetovaný	k2eAgFnSc2d1	zmagnetovaná
kovové	kovový	k2eAgFnSc2d1	kovová
tyče	tyč	k1gFnSc2	tyč
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
udržován	udržován	k2eAgInSc4d1	udržován
teplotní	teplotní	k2eAgInSc4d1	teplotní
rozdíl	rozdíl	k1gInSc4	rozdíl
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
(	(	kIx(	(
<g/>
uspořádáním	uspořádání	k1gNnSc7	uspořádání
elektronových	elektronový	k2eAgInPc2d1	elektronový
spinů	spin	k1gInPc2	spin
<g/>
)	)	kIx)	)
přídavný	přídavný	k2eAgInSc1d1	přídavný
magnetický	magnetický	k2eAgInSc1d1	magnetický
indukční	indukční	k2eAgInSc1d1	indukční
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
magnetomotorické	magnetomotorický	k2eAgNnSc1d1	magnetomotorický
napětí	napětí	k1gNnSc1	napětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tepelným	tepelný	k2eAgInSc7d1	tepelný
pohybem	pohyb	k1gInSc7	pohyb
nosičů	nosič	k1gMnPc2	nosič
náboje	náboj	k1gInSc2	náboj
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
elektrických	elektrický	k2eAgInPc6d1	elektrický
obvodech	obvod	k1gInPc6	obvod
fluktuace	fluktuace	k1gFnSc1	fluktuace
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
těchto	tento	k3xDgFnPc2	tento
odchylek	odchylka	k1gFnPc2	odchylka
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
jejich	jejich	k3xOp3gInPc2	jejich
kvadrátů	kvadrát	k1gInPc2	kvadrát
již	již	k6eAd1	již
nulová	nulový	k2eAgFnSc1d1	nulová
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Fluktuace	fluktuace	k1gFnSc1	fluktuace
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
popisují	popisovat	k5eAaImIp3nP	popisovat
tzv.	tzv.	kA	tzv.
korelační	korelační	k2eAgFnSc7d1	korelační
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
středovanou	středovaný	k2eAgFnSc7d1	středovaný
hodnotou	hodnota	k1gFnSc7	hodnota
součinu	součin	k1gInSc2	součin
proudu	proud	k1gInSc2	proud
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
časových	časový	k2eAgInPc6d1	časový
okamžicích	okamžik	k1gInPc6	okamžik
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
I	i	k9	i
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
⋅	⋅	k?	⋅
I	i	k9	i
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k2eAgInSc4d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc4	langle
I	I	kA	I
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
I	I	kA	I
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gInSc1	rangle
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
teplotě	teplota	k1gFnSc3	teplota
a	a	k8xC	a
exponenciálně	exponenciálně	k6eAd1	exponenciálně
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
časem	čas	k1gInSc7	čas
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
okamžiky	okamžik	k1gInPc7	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
časové	časový	k2eAgFnSc3d1	časová
proměnlivosti	proměnlivost	k1gFnSc3	proměnlivost
fluktuací	fluktuace	k1gFnPc2	fluktuace
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
přídavné	přídavný	k2eAgInPc1d1	přídavný
střídavé	střídavý	k2eAgInPc1d1	střídavý
proudy	proud	k1gInPc1	proud
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
provést	provést	k5eAaPmF	provést
spektrální	spektrální	k2eAgInSc4d1	spektrální
rozklad	rozklad	k1gInSc4	rozklad
těchto	tento	k3xDgFnPc2	tento
fluktuací	fluktuace	k1gFnPc2	fluktuace
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
I	i	k9	i
(	(	kIx(	(
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
)	)	kIx)	)
⋅	⋅	k?	⋅
I	I	kA	I
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
⟩	⟩	k?	⟩
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∫	∫	k?	∫
:	:	kIx,	:
−	−	k?	−
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
:	:	kIx,	:
I	I	kA	I
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟩	⟩	k?	⟩
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
ω	ω	k?	ω
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k2eAgInSc4d1	left
<g/>
\	\	kIx~	\
<g/>
langle	langle	k1gInSc4	langle
I	I	kA	I
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
I	I	kA	I
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gFnSc1	rangle
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
limits	limits	k1gInSc1	limits
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
I	i	k9	i
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
t	t	k?	t
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
:	:	kIx,	:
I	I	kA	I
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟩	⟩	k?	⟩
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
I	i	k9	i
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nP	platit
tzv.	tzv.	kA	tzv.
Nyquistův	Nyquistův	k2eAgInSc4d1	Nyquistův
vztah	vztah	k1gInSc4	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
I	i	k8xC	i
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟩	⟩	k?	⟩
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k6eAd1	langle
I	i	k9	i
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
Z	z	k7c2	z
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
Z	z	k7c2	z
=	=	kIx~	=
R	R	kA	R
+	+	kIx~	+
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
R	R	kA	R
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
komplexní	komplexní	k2eAgFnSc1d1	komplexní
<g/>
)	)	kIx)	)
impedance	impedance	k1gFnSc1	impedance
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
obvody	obvod	k1gInPc4	obvod
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
odporem	odpor	k1gInSc7	odpor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
fluktuace	fluktuace	k1gFnPc4	fluktuace
významné	významný	k2eAgFnPc4d1	významná
pro	pro	k7c4	pro
rezonanční	rezonanční	k2eAgFnPc4d1	rezonanční
frekvence	frekvence	k1gFnPc4	frekvence
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
reaktance	reaktance	k1gFnSc1	reaktance
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
<s>
Tepelné	tepelný	k2eAgFnPc1d1	tepelná
fluktuace	fluktuace	k1gFnPc1	fluktuace
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
Johnsonova-Nyquistova	Johnsonova-Nyquistův	k2eAgInSc2d1	Johnsonova-Nyquistův
elektronického	elektronický	k2eAgInSc2d1	elektronický
šumu	šum	k1gInSc2	šum
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
některé	některý	k3yIgFnPc4	některý
optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
dány	dán	k2eAgFnPc1d1	dána
změnami	změna	k1gFnPc7	změna
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vlastností	vlastnost	k1gFnPc2	vlastnost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
optická	optický	k2eAgFnSc1d1	optická
hustota	hustota	k1gFnSc1	hustota
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc6d1	uvedená
změnách	změna	k1gFnPc6	změna
elektrické	elektrický	k2eAgFnSc2d1	elektrická
permitivity	permitivita	k1gFnSc2	permitivita
(	(	kIx(	(
<g/>
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
magnetické	magnetický	k2eAgFnSc6d1	magnetická
permeabilitě	permeabilita	k1gFnSc6	permeabilita
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
u	u	k7c2	u
běžných	běžný	k2eAgFnPc2d1	běžná
optických	optický	k2eAgFnPc2d1	optická
prostředí	prostředí	k1gNnSc2	prostředí
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
tvar	tvar	k1gInSc1	tvar
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
převrácené	převrácený	k2eAgFnSc2d1	převrácená
hodnotě	hodnota	k1gFnSc6	hodnota
druhé	druhý	k4xOgFnSc2	druhý
odmocniny	odmocnina	k1gFnSc2	odmocnina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
permitivity	permitivita	k1gFnSc2	permitivita
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
čárové	čárový	k2eAgNnSc1d1	čárové
optické	optický	k2eAgNnSc1d1	optické
spektrum	spektrum	k1gNnSc1	spektrum
pomocí	pomocí	k7c2	pomocí
kvantových	kvantový	k2eAgInPc2d1	kvantový
energetických	energetický	k2eAgInPc2d1	energetický
přechodů	přechod	k1gInPc2	přechod
v	v	k7c6	v
elektronových	elektronový	k2eAgInPc6d1	elektronový
obalech	obal	k1gInPc6	obal
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
šířka	šířka	k1gFnSc1	šířka
spektrální	spektrální	k2eAgFnSc2d1	spektrální
čáry	čára	k1gFnSc2	čára
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
rozdíl	rozdíl	k1gInSc1	rozdíl
úhlových	úhlový	k2eAgFnPc2d1	úhlová
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
spektrální	spektrální	k2eAgFnSc1d1	spektrální
hustota	hustota	k1gFnSc1	hustota
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
poloviční	poloviční	k2eAgMnSc1d1	poloviční
oproti	oproti	k7c3	oproti
maximální	maximální	k2eAgFnSc3d1	maximální
spektrální	spektrální	k2eAgFnSc3d1	spektrální
hustotě	hustota	k1gFnSc3	hustota
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
dané	daný	k2eAgFnPc4d1	daná
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
o	o	k7c6	o
úhlové	úhlový	k2eAgFnSc6d1	úhlová
frekvenci	frekvence	k1gFnSc6	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnSc3	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
součiniteli	součinitel	k1gInSc6	součinitel
tlumení	tlumení	k1gNnSc2	tlumení
emitujícího	emitující	k2eAgInSc2d1	emitující
oscilátoru	oscilátor	k1gInSc2	oscilátor
<g/>
.	.	kIx.	.
</s>
<s>
Reálná	reálný	k2eAgFnSc1d1	reálná
emise	emise	k1gFnSc1	emise
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
však	však	k9	však
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
neuspořádaným	uspořádaný	k2eNgInSc7d1	neuspořádaný
tepelným	tepelný	k2eAgInSc7d1	tepelný
pohybem	pohyb	k1gInSc7	pohyb
částic	částice	k1gFnPc2	částice
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
úhlové	úhlový	k2eAgFnPc1d1	úhlová
frekvence	frekvence	k1gFnPc1	frekvence
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
zdroje	zdroj	k1gInSc2	zdroj
vlnění	vlnění	k1gNnSc4	vlnění
vůči	vůči	k7c3	vůči
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
Dopplerova	Dopplerův	k2eAgInSc2d1	Dopplerův
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
celkové	celkový	k2eAgNnSc1d1	celkové
záření	záření	k1gNnSc1	záření
látky	látka	k1gFnSc2	látka
pocházející	pocházející	k2eAgFnSc2d1	pocházející
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
částic	částice	k1gFnPc2	částice
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
danou	daný	k2eAgFnSc4d1	daná
spektrální	spektrální	k2eAgFnSc4d1	spektrální
čáru	čára	k1gFnSc4	čára
rozšířenu	rozšířen	k2eAgFnSc4d1	rozšířena
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Dopplerovské	Dopplerovský	k2eAgNnSc1d1	Dopplerovské
rozšíření	rozšíření	k1gNnSc1	rozšíření
spektrální	spektrální	k2eAgFnSc2d1	spektrální
čáry	čára	k1gFnSc2	čára
<g/>
)	)	kIx)	)
o	o	k7c4	o
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
ω	ω	k?	ω
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
ω	ω	k?	ω
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
ln	ln	k?	ln
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gNnSc2	omega
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
2	[number]	k4	2
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}}	}}}}}	k?	}}}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
úměrná	úměrný	k2eAgFnSc1d1	úměrná
odmocnině	odmocnina	k1gFnSc6	odmocnina
absolutní	absolutní	k2eAgFnPc4d1	absolutní
teploty	teplota	k1gFnPc4	teplota
<g/>
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
emitujícího	emitující	k2eAgInSc2d1	emitující
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Zahřátá	zahřátý	k2eAgNnPc1d1	zahřáté
hmotná	hmotný	k2eAgNnPc1d1	hmotné
prostředí	prostředí	k1gNnPc1	prostředí
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
sáláním	sálání	k1gNnSc7	sálání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vydáváním	vydávání	k1gNnSc7	vydávání
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
iniciováno	iniciovat	k5eAaBmNgNnS	iniciovat
jiným	jiný	k2eAgNnSc7d1	jiné
dopadajícím	dopadající	k2eAgNnSc7d1	dopadající
zářením	záření	k1gNnSc7	záření
nebo	nebo	k8xC	nebo
vnějšími	vnější	k2eAgFnPc7d1	vnější
změnami	změna	k1gFnPc7	změna
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
dutině	dutina	k1gFnSc6	dutina
uvnitř	uvnitř	k7c2	uvnitř
zahřátého	zahřátý	k2eAgNnSc2d1	zahřáté
prostředí	prostředí	k1gNnSc2	prostředí
se	se	k3xPyFc4	se
ustálí	ustálit	k5eAaPmIp3nS	ustálit
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
rovnováha	rovnováha	k1gFnSc1	rovnováha
zahřátých	zahřátý	k2eAgFnPc2d1	zahřátá
stěn	stěna	k1gFnPc2	stěna
a	a	k8xC	a
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zvaného	zvaný	k2eAgNnSc2d1	zvané
záření	záření	k1gNnSc2	záření
absolutně	absolutně	k6eAd1	absolutně
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Kirchhoff	Kirchhoff	k1gInSc1	Kirchhoff
v	v	k7c6	v
r.	r.	kA	r.
1862	[number]	k4	1862
odvodil	odvodit	k5eAaPmAgInS	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spektrální	spektrální	k2eAgFnSc1d1	spektrální
hustota	hustota	k1gFnSc1	hustota
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
závisí	záviset	k5eAaImIp3nS	záviset
jen	jen	k9	jen
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
druhu	druh	k1gInSc3	druh
látkového	látkový	k2eAgNnSc2d1	látkové
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kirchhoffův	Kirchhoffův	k2eAgInSc1d1	Kirchhoffův
zákon	zákon	k1gInSc1	zákon
tepelného	tepelný	k2eAgNnSc2d1	tepelné
vyzařování	vyzařování	k1gNnSc2	vyzařování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Planck	Planck	k6eAd1	Planck
v	v	k7c6	v
r.	r.	kA	r.
1900	[number]	k4	1900
podal	podat	k5eAaPmAgInS	podat
teoretické	teoretický	k2eAgNnSc4d1	teoretické
zdůvodnění	zdůvodnění	k1gNnSc4	zdůvodnění
tvaru	tvar	k1gInSc2	tvar
spektra	spektrum	k1gNnSc2	spektrum
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
např.	např.	kA	např.
pro	pro	k7c4	pro
spektrální	spektrální	k2eAgFnSc4d1	spektrální
hustotu	hustota	k1gFnSc4	hustota
energie	energie	k1gFnSc2	energie
následujícím	následující	k2eAgInSc7d1	následující
vztahem	vztah	k1gInSc7	vztah
(	(	kIx(	(
<g/>
Planckův	Planckův	k2eAgInSc1d1	Planckův
vyzařovací	vyzařovací	k2eAgInSc1d1	vyzařovací
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
ω	ω	k?	ω
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ω	ω	k?	ω
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ω	ω	k?	ω
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
omega	omega	k1gNnSc1	omega
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
kT	kT	k?	kT
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
,	,	kIx,	,
ħ	ħ	k?	ħ
,	,	kIx,	,
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gMnSc1	hbar
,	,	kIx,	,
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
označují	označovat	k5eAaImIp3nP	označovat
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
Planckovu	Planckov	k1gInSc3	Planckov
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Boltzmannovu	Boltzmannův	k2eAgFnSc4d1	Boltzmannova
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
w	w	k?	w
(	(	kIx(	(
ω	ω	k?	ω
)	)	kIx)	)
:	:	kIx,	:
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
w	w	k?	w
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
w	w	k?	w
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
objemová	objemový	k2eAgFnSc1d1	objemová
hustota	hustota	k1gFnSc1	hustota
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyzařovacího	vyzařovací	k2eAgInSc2d1	vyzařovací
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozšířit	rozšířit	k5eAaPmF	rozšířit
pojem	pojem	k1gInSc4	pojem
teploty	teplota	k1gFnSc2	teplota
i	i	k9	i
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesplňují	splňovat	k5eNaImIp3nP	splňovat
podmínky	podmínka	k1gFnPc4	podmínka
klasického	klasický	k2eAgInSc2d1	klasický
termodynamického	termodynamický	k2eAgInSc2d1	termodynamický
rovnovážného	rovnovážný	k2eAgInSc2d1	rovnovážný
systému	systém	k1gInSc2	systém
<g/>
:	:	kIx,	:
Z	z	k7c2	z
Planckova	Planckův	k2eAgInSc2d1	Planckův
vyzařovacího	vyzařovací	k2eAgInSc2d1	vyzařovací
zákona	zákon	k1gInSc2	zákon
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
závislost	závislost	k1gFnSc4	závislost
výkonu	výkon	k1gInSc2	výkon
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
vyzařovaného	vyzařovaný	k2eAgInSc2d1	vyzařovaný
<g />
.	.	kIx.	.
</s>
<s hack="1">
plochou	plochý	k2eAgFnSc7d1	plochá
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Stefanův-Boltzmannův	Stefanův-Boltzmannův	k2eAgInSc1d1	Stefanův-Boltzmannův
zákon	zákon	k1gInSc1	zákon
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
σ	σ	k?	σ
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
π	π	k?	π
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
60	[number]	k4	60
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc3	sigma
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
60	[number]	k4	60
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
hbar	hbar	k1gInSc1	hbar
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Stefanova-Boltzmannova	Stefanova-Boltzmannův	k2eAgFnSc1d1	Stefanova-Boltzmannův
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnPc3	sigma
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
=	=	kIx~	=
5,670	[number]	k4	5,670
400	[number]	k4	400
<g/>
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
·	·	k?	·
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
efektivní	efektivní	k2eAgFnSc1d1	efektivní
teplota	teplota	k1gFnSc1	teplota
povrchu	povrch	k1gInSc2	povrch
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
výkon	výkon	k1gInSc1	výkon
vyzařovaný	vyzařovaný	k2eAgInSc1d1	vyzařovaný
jednotkovou	jednotkový	k2eAgFnSc7d1	jednotková
plochou	plocha	k1gFnSc7	plocha
absolutně	absolutně	k6eAd1	absolutně
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
výkonu	výkon	k1gInSc3	výkon
vyzařovanému	vyzařovaný	k2eAgInSc3d1	vyzařovaný
jednotkovou	jednotkový	k2eAgFnSc7d1	jednotková
plochou	plocha	k1gFnSc7	plocha
povrchu	povrch	k1gInSc2	povrch
daného	daný	k2eAgNnSc2d1	dané
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozšíření	rozšíření	k1gNnSc1	rozšíření
pojmu	pojmout	k5eAaPmIp1nS	pojmout
teplota	teplota	k1gFnSc1	teplota
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
i	i	k9	i
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
povrch	povrch	k1gInSc4	povrch
lokálně	lokálně	k6eAd1	lokálně
neplatí	platit	k5eNaImIp3nS	platit
podmínka	podmínka	k1gFnSc1	podmínka
rovnovážného	rovnovážný	k2eAgInSc2d1	rovnovážný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
dostatečně	dostatečně	k6eAd1	dostatečně
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgFnSc1d1	efektivní
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
proto	proto	k6eAd1	proto
používána	používat	k5eAaImNgFnS	používat
např.	např.	kA	např.
v	v	k7c4	v
astronomii	astronomie	k1gFnSc4	astronomie
pro	pro	k7c4	pro
charakteristiku	charakteristika	k1gFnSc4	charakteristika
povrchů	povrch	k1gInPc2	povrch
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
maximum	maximum	k1gNnSc4	maximum
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
rozdělovací	rozdělovací	k2eAgFnSc2d1	rozdělovací
funkce	funkce	k1gFnSc2	funkce
přepsané	přepsaný	k2eAgFnSc2d1	přepsaná
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
rozdělení	rozdělení	k1gNnSc2	rozdělení
podle	podle	k7c2	podle
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
tzv.	tzv.	kA	tzv.
Wienův	Wienův	k2eAgInSc1d1	Wienův
posunovací	posunovací	k2eAgInSc1d1	posunovací
zákon	zákon	k1gInSc1	zákon
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
a	a	k8xC	a
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
řešení	řešení	k1gNnSc1	řešení
rovnice	rovnice	k1gFnSc2	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
5	[number]	k4	5
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
=	=	kIx~	=
4,965	[number]	k4	4,965
114	[number]	k4	114
231	[number]	k4	231
<g/>
...	...	k?	...
Hmotnému	hmotný	k2eAgNnSc3d1	hmotné
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vydává	vydávat	k5eAaPmIp3nS	vydávat
záření	záření	k1gNnSc1	záření
podobného	podobný	k2eAgNnSc2d1	podobné
spektra	spektrum	k1gNnSc2	spektrum
jako	jako	k8xS	jako
absolutně	absolutně	k6eAd1	absolutně
černé	černý	k2eAgNnSc1d1	černé
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
přiřadit	přiřadit	k5eAaPmF	přiřadit
radiační	radiační	k2eAgFnSc4d1	radiační
teplotu	teplota	k1gFnSc4	teplota
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
maximu	maxima	k1gFnSc4	maxima
tohoto	tento	k3xDgNnSc2	tento
spektra	spektrum	k1gNnSc2	spektrum
podle	podle	k7c2	podle
Wienova	Wienův	k2eAgInSc2d1	Wienův
posunovacího	posunovací	k2eAgInSc2d1	posunovací
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozšíření	rozšíření	k1gNnSc1	rozšíření
pojmu	pojmout	k5eAaPmIp1nS	pojmout
teplota	teplota	k1gFnSc1	teplota
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
i	i	k9	i
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hustota	hustota	k1gFnSc1	hustota
částic	částice	k1gFnPc2	částice
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
splněny	splněn	k2eAgFnPc4d1	splněna
podmínky	podmínka	k1gFnPc4	podmínka
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
rovnováhy	rovnováha	k1gFnSc2	rovnováha
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Někdy	někdy	k6eAd1	někdy
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
kosmologii	kosmologie	k1gFnSc6	kosmologie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
radiační	radiační	k2eAgFnSc1d1	radiační
teplota	teplota	k1gFnSc1	teplota
definuje	definovat	k5eAaBmIp3nS	definovat
rovností	rovnost	k1gFnSc7	rovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
=	=	kIx~	=
ħ	ħ	k?	ħ
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
kT	kT	k?	kT
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
-krát	rát	k5eAaPmNgInS	-krát
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
podle	podle	k7c2	podle
předchozí	předchozí	k2eAgFnSc2d1	předchozí
definice	definice	k1gFnSc2	definice
<g/>
.	.	kIx.	.
</s>
<s>
Analogie	analogie	k1gFnSc1	analogie
se	se	k3xPyFc4	se
zářením	záření	k1gNnSc7	záření
absolutně	absolutně	k6eAd1	absolutně
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
ve	v	k7c6	v
fotometrii	fotometrie	k1gFnSc6	fotometrie
a	a	k8xC	a
optice	optika	k1gFnSc6	optika
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pro	pro	k7c4	pro
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
část	část	k1gFnSc4	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
vlastností	vlastnost	k1gFnPc2	vlastnost
světelných	světelný	k2eAgInPc2d1	světelný
zářičů	zářič	k1gInPc2	zářič
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
:	:	kIx,	:
Černá	černý	k2eAgFnSc1d1	černá
(	(	kIx(	(
<g/>
jasová	jasový	k2eAgFnSc1d1	jasová
<g/>
)	)	kIx)	)
teplota	teplota	k1gFnSc1	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
l	l	kA	l
a	a	k8xC	a
c	c	k0	c
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Theta	Theto	k1gNnPc4	Theto
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
black	black	k1gInSc1	black
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
daného	daný	k2eAgInSc2d1	daný
zářiče	zářič	k1gInSc2	zářič
při	při	k7c6	při
určité	určitý	k2eAgFnSc6d1	určitá
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnPc3	lambda
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
jas	jas	k1gInSc1	jas
(	(	kIx(	(
<g/>
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
)	)	kIx)	)
stejný	stejný	k2eAgInSc4d1	stejný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
spektrální	spektrální	k2eAgInSc1d1	spektrální
jas	jas	k1gInSc1	jas
daného	daný	k2eAgInSc2d1	daný
zářiče	zářič	k1gInSc2	zářič
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
l	l	kA	l
a	a	k8xC	a
c	c	k0	c
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
h	h	k?	h
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ln	ln	k?	ln
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Theta	Theta	k1gFnSc1	Theta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
black	black	k1gInSc1	black
<g/>
}	}	kIx)	}
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc4	lambda
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
hc	hc	k?	hc
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgFnSc1d1	relativní
spektrální	spektrální	k2eAgFnSc1d1	spektrální
pohltivost	pohltivost	k1gFnSc1	pohltivost
světla	světlo	k1gNnSc2	světlo
u	u	k7c2	u
povrchu	povrch	k1gInSc2	povrch
zářiče	zářič	k1gInSc2	zářič
Barevná	barevný	k2eAgFnSc1d1	barevná
teplota	teplota	k1gFnSc1	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
Θ	Θ	k?	Θ
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
o	o	k7c4	o
l	l	kA	l
o	o	k7c6	o
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Theta	Theto	k1gNnPc4	Theto
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
color	color	k1gInSc1	color
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
daného	daný	k2eAgInSc2d1	daný
zářiče	zářič	k1gInSc2	zářič
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
teplota	teplota	k1gFnSc1	teplota
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
by	by	kYmCp3nS	by
vysílalo	vysílat	k5eAaImAgNnS	vysílat
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
v	v	k7c6	v
normálním	normální	k2eAgNnSc6d1	normální
lidském	lidský	k2eAgNnSc6d1	lidské
oku	oko	k1gNnSc6	oko
stejný	stejný	k2eAgInSc4d1	stejný
barevný	barevný	k2eAgInSc4d1	barevný
vjem	vjem	k1gInSc4	vjem
jako	jako	k8xS	jako
daný	daný	k2eAgInSc4d1	daný
zářič	zářič	k1gInSc4	zářič
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
podstatu	podstata	k1gFnSc4	podstata
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teplot	teplota	k1gFnPc2	teplota
blízkých	blízký	k2eAgFnPc2d1	blízká
absolutní	absolutní	k2eAgFnSc4d1	absolutní
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
jevy	jev	k1gInPc4	jev
s	s	k7c7	s
makroskopickými	makroskopický	k2eAgInPc7d1	makroskopický
projevy	projev	k1gInPc7	projev
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
skupinou	skupina	k1gFnSc7	skupina
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
jsou	být	k5eAaImIp3nP	být
jevy	jev	k1gInPc1	jev
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
kolektivním	kolektivní	k2eAgNnSc6d1	kolektivní
kvantovém	kvantový	k2eAgNnSc6d1	kvantové
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
částice	částice	k1gFnSc1	částice
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
fermiony	fermion	k1gInPc1	fermion
<g/>
)	)	kIx)	)
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Cooperových	Cooperův	k2eAgInPc2d1	Cooperův
párů	pár	k1gInPc2	pár
-	-	kIx~	-
bosonů	boson	k1gInPc2	boson
<g/>
.	.	kIx.	.
</s>
<s>
Projeví	projevit	k5eAaPmIp3nS	projevit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
makroskopickými	makroskopický	k2eAgFnPc7d1	makroskopická
charakteristikami	charakteristika	k1gFnPc7	charakteristika
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
jevy	jev	k1gInPc4	jev
patří	patřit	k5eAaImIp3nS	patřit
supravodivost	supravodivost	k1gFnSc4	supravodivost
<g/>
,	,	kIx,	,
supratekutost	supratekutost	k1gFnSc4	supratekutost
a	a	k8xC	a
příbuzné	příbuzný	k2eAgNnSc4d1	příbuzné
"	"	kIx"	"
<g/>
suprapevné	suprapevný	k2eAgNnSc4d1	suprapevný
<g/>
"	"	kIx"	"
chování	chování	k1gNnSc4	chování
<g/>
,,	,,	k?	,,
fermionová	fermionový	k2eAgFnSc1d1	fermionový
kondenzace	kondenzace	k1gFnSc1	kondenzace
<g/>
,	,	kIx,	,
kvantový	kvantový	k2eAgInSc1d1	kvantový
Hallův	Hallův	k2eAgInSc1d1	Hallův
jev	jev	k1gInSc1	jev
a	a	k8xC	a
kvantový	kvantový	k2eAgInSc1d1	kvantový
spinový	spinový	k2eAgInSc1d1	spinový
Hallův	Hallův	k2eAgInSc1d1	Hallův
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Niels	Niels	k6eAd1	Niels
Bohr	Bohr	k1gInSc1	Bohr
zavedl	zavést	k5eAaPmAgInS	zavést
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
jaderné	jaderný	k2eAgFnPc4d1	jaderná
reakce	reakce	k1gFnPc4	reakce
model	model	k1gInSc1	model
složeného	složený	k2eAgNnSc2d1	složené
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
záchytem	záchyt	k1gInSc7	záchyt
částice	částice	k1gFnSc2	částice
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
"	"	kIx"	"
<g/>
rozpuštěním	rozpuštění	k1gNnPc3	rozpuštění
<g/>
"	"	kIx"	"
její	její	k3xOp3gFnSc2	její
energie	energie	k1gFnSc2	energie
mezi	mezi	k7c4	mezi
nukleony	nukleon	k1gInPc4	nukleon
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc4	který
přebytek	přebytek	k1gInSc4	přebytek
energie	energie	k1gFnSc2	energie
odnášejí	odnášet	k5eAaImIp3nP	odnášet
"	"	kIx"	"
<g/>
vypařující	vypařující	k2eAgInSc1d1	vypařující
se	s	k7c7	s
<g/>
"	"	kIx"	"
částice	částice	k1gFnSc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
spektrum	spektrum	k1gNnSc1	spektrum
lze	lze	k6eAd1	lze
dobře	dobře	k6eAd1	dobře
popsat	popsat	k5eAaPmF	popsat
Maxwellovým	Maxwellův	k2eAgNnSc7d1	Maxwellovo
spektrem	spektrum	k1gNnSc7	spektrum
pro	pro	k7c4	pro
tok	tok	k1gInSc4	tok
molekul	molekula	k1gFnPc2	molekula
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
teplé	teplý	k2eAgFnSc2d1	teplá
kapaliny	kapalina	k1gFnSc2	kapalina
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
neutrony	neutron	k1gInPc4	neutron
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
protony	proton	k1gInPc4	proton
je	být	k5eAaImIp3nS	být
narušeno	narušit	k5eAaPmNgNnS	narušit
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
působením	působení	k1gNnSc7	působení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
evaporačním	evaporační	k2eAgNnSc7d1	evaporační
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
stanovení	stanovení	k1gNnSc3	stanovení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
znát	znát	k5eAaImF	znát
jadernou	jaderný	k2eAgFnSc4d1	jaderná
teplotu	teplota	k1gFnSc4	teplota
(	(	kIx(	(
<g/>
teplotu	teplota	k1gFnSc4	teplota
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
pomocí	pomocí	k7c2	pomocí
statistické	statistický	k2eAgFnSc2d1	statistická
definice	definice	k1gFnSc2	definice
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
koncepce	koncepce	k1gFnSc1	koncepce
popisu	popis	k1gInSc2	popis
jaderných	jaderný	k2eAgNnPc2d1	jaderné
spekter	spektrum	k1gNnPc2	spektrum
excitovaných	excitovaný	k2eAgNnPc2d1	excitované
jader	jádro	k1gNnPc2	jádro
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
r.	r.	kA	r.
1936-37	[number]	k4	1936-37
současně	současně	k6eAd1	současně
několika	několik	k4yIc7	několik
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
přiblížení	přiblížení	k1gNnSc6	přiblížení
z	z	k7c2	z
ní	on	k3xPp3gFnSc3	on
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
střední	střední	k2eAgFnSc2d1	střední
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
1	[number]	k4	1
excitovaný	excitovaný	k2eAgInSc4d1	excitovaný
nukleon	nukleon	k1gInSc4	nukleon
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
jaderné	jaderný	k2eAgFnSc3d1	jaderná
teplotě	teplota	k1gFnSc3	teplota
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
energie	energie	k1gFnSc2	energie
excitovaného	excitovaný	k2eAgNnSc2d1	excitované
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
kvadrátu	kvadrát	k1gInSc2	kvadrát
jaderné	jaderný	k2eAgFnSc2d1	jaderná
teploty	teplota	k1gFnSc2	teplota
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
jádra	jádro	k1gNnPc4	jádro
s	s	k7c7	s
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
A	a	k9	a
<g/>
≈	≈	k?	≈
<g/>
100	[number]	k4	100
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
excitační	excitační	k2eAgFnSc4d1	excitační
energii	energie	k1gFnSc4	energie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
=	=	kIx~	=
10	[number]	k4	10
MeV	MeV	k1gFnSc4	MeV
jaderná	jaderný	k2eAgFnSc1d1	jaderná
teplota	teplota	k1gFnSc1	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
kT	kT	k?	kT
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
≈	≈	k?	≈
1	[number]	k4	1
<g/>
÷	÷	k?	÷
<g/>
1,5	[number]	k4	1,5
MeV	MeV	k1gFnPc2	MeV
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
a	a	k8xC	a
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
k	k	k7c3	k
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
aT	aT	k?	aT
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-kT	T	k?	-kT
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
radioaktivní	radioaktivní	k2eAgFnPc4d1	radioaktivní
přeměny	přeměna	k1gFnPc4	přeměna
s	s	k7c7	s
emisí	emise	k1gFnSc7	emise
záření	záření	k1gNnSc2	záření
γ	γ	k?	γ
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
jaderné	jaderný	k2eAgFnPc4d1	jaderná
reakce	reakce	k1gFnPc4	reakce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
prostředí	prostředí	k1gNnSc2	prostředí
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
Dopplerovské	Dopplerovský	k2eAgNnSc4d1	Dopplerovské
rozšíření	rozšíření	k1gNnSc4	rozšíření
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
křivky	křivka	k1gFnSc2	křivka
spektra	spektrum	k1gNnSc2	spektrum
záření	záření	k1gNnSc2	záření
γ	γ	k?	γ
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
účinného	účinný	k2eAgInSc2d1	účinný
průřezu	průřez	k1gInSc2	průřez
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
částečná	částečný	k2eAgFnSc1d1	částečná
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
absorpce	absorpce	k1gFnSc1	absorpce
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
jader	jádro	k1gNnPc2	jádro
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
energetický	energetický	k2eAgInSc1d1	energetický
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
energií	energie	k1gFnSc7	energie
pro	pro	k7c4	pro
emisi	emise	k1gFnSc4	emise
a	a	k8xC	a
absorpci	absorpce	k1gFnSc4	absorpce
(	(	kIx(	(
<g/>
způsobený	způsobený	k2eAgInSc1d1	způsobený
zpětným	zpětný	k2eAgInSc7d1	zpětný
rázem	ráz	k1gInSc7	ráz
jádra	jádro	k1gNnSc2	jádro
plynoucím	plynoucí	k2eAgInSc7d1	plynoucí
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
veličinou	veličina	k1gFnSc7	veličina
také	také	k9	také
v	v	k7c6	v
aplikované	aplikovaný	k2eAgFnSc6d1	aplikovaná
jaderné	jaderný	k2eAgFnSc3d1	jaderná
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
štěpných	štěpný	k2eAgFnPc2d1	štěpná
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
vývin	vývin	k1gInSc1	vývin
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
teplotní	teplotní	k2eAgNnPc4d1	teplotní
pole	pole	k1gNnPc4	pole
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
naopak	naopak	k6eAd1	naopak
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
hustoty	hustota	k1gFnPc4	hustota
materiálů	materiál	k1gInPc2	materiál
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
zóně	zóna	k1gFnSc6	zóna
reaktoru	reaktor	k1gInSc2	reaktor
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc2	jejich
jaderné	jaderný	k2eAgFnSc2d1	jaderná
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
bilance	bilance	k1gFnSc1	bilance
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
teplota	teplota	k1gFnSc1	teplota
paliva	palivo	k1gNnSc2	palivo
(	(	kIx(	(
<g/>
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
tedy	tedy	k9	tedy
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
interakcí	interakce	k1gFnPc2	interakce
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
Dopplerovské	Dopplerovský	k2eAgNnSc1d1	Dopplerovské
rozšíření	rozšíření	k1gNnSc1	rozšíření
rezonančních	rezonanční	k2eAgFnPc2d1	rezonanční
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
)	)	kIx)	)
i	i	k9	i
teplota	teplota	k1gFnSc1	teplota
moderátoru	moderátor	k1gInSc2	moderátor
(	(	kIx(	(
<g/>
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
zpomalovací	zpomalovací	k2eAgFnPc4d1	zpomalovací
a	a	k8xC	a
difuzní	difuzní	k2eAgFnPc4d1	difuzní
charakteristiky	charakteristika	k1gFnPc4	charakteristika
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
spektrum	spektrum	k1gNnSc1	spektrum
neutronů	neutron	k1gInPc2	neutron
potřebných	potřebný	k2eAgInPc2d1	potřebný
pro	pro	k7c4	pro
štěpení	štěpení	k1gNnSc4	štěpení
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
závisí	záviset	k5eAaImIp3nS	záviset
účinný	účinný	k2eAgInSc1d1	účinný
průřez	průřez	k1gInSc1	průřez
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
veličinou	veličina	k1gFnSc7	veličina
reaktorové	reaktorový	k2eAgFnSc2d1	reaktorová
kinetiky	kinetika	k1gFnSc2	kinetika
je	být	k5eAaImIp3nS	být
reaktivita	reaktivita	k1gFnSc1	reaktivita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
charakterizující	charakterizující	k2eAgFnSc4d1	charakterizující
násobící	násobící	k2eAgFnSc4d1	násobící
schopnost	schopnost	k1gFnSc4	schopnost
štěpné	štěpný	k2eAgFnSc2d1	štěpná
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
pomocí	pomocí	k7c2	pomocí
teplotních	teplotní	k2eAgInPc2d1	teplotní
koeficientů	koeficient	k1gInPc2	koeficient
reaktivity	reaktivita	k1gFnSc2	reaktivita
<g/>
,	,	kIx,	,
definovaných	definovaný	k2eAgFnPc2d1	definovaná
jako	jako	k9	jako
derivace	derivace	k1gFnPc1	derivace
reaktivity	reaktivita	k1gFnSc2	reaktivita
podle	podle	k7c2	podle
teploty	teplota	k1gFnSc2	teplota
dané	daný	k2eAgFnSc2d1	daná
složky	složka	k1gFnSc2	složka
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zóny	zóna	k1gFnSc2	zóna
reaktoru	reaktor	k1gInSc2	reaktor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gMnSc1	partial
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
stabilitu	stabilita	k1gFnSc4	stabilita
reaktoru	reaktor	k1gInSc2	reaktor
je	být	k5eAaImIp3nS	být
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
nutné	nutný	k2eAgNnSc4d1	nutné
navrhnout	navrhnout	k5eAaPmF	navrhnout
aktivní	aktivní	k2eAgFnSc4d1	aktivní
zónu	zóna	k1gFnSc4	zóna
se	s	k7c7	s
zápornými	záporný	k2eAgInPc7d1	záporný
teplotními	teplotní	k2eAgInPc7d1	teplotní
koeficienty	koeficient	k1gInPc7	koeficient
reaktivity	reaktivita	k1gFnSc2	reaktivita
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pak	pak	k6eAd1	pak
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nekontrolovatelnému	kontrolovatelný	k2eNgInSc3d1	nekontrolovatelný
nárůstu	nárůst	k1gInSc3	nárůst
štěpných	štěpný	k2eAgFnPc2d1	štěpná
reakcí	reakce	k1gFnPc2	reakce
(	(	kIx(	(
<g/>
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
zajistí	zajistit	k5eAaPmIp3nS	zajistit
při	při	k7c6	při
rostoucí	rostoucí	k2eAgFnSc6d1	rostoucí
teplotě	teplota	k1gFnSc6	teplota
od	od	k7c2	od
vyvíjeného	vyvíjený	k2eAgNnSc2d1	vyvíjené
tepla	teplo	k1gNnSc2	teplo
zpomalení	zpomalení	k1gNnSc1	zpomalení
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
běžně	běžně	k6eAd1	běžně
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
reliktního	reliktní	k2eAgNnSc2d1	reliktní
záření	záření	k1gNnSc2	záření
apod.	apod.	kA	apod.
Na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
např.	např.	kA	např.
závisí	záviset	k5eAaImIp3nS	záviset
spektrální	spektrální	k2eAgFnPc4d1	spektrální
třídy	třída	k1gFnPc4	třída
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
klasických	klasický	k2eAgInPc2d1	klasický
objektů	objekt	k1gInPc2	objekt
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
teplotu	teplota	k1gFnSc4	teplota
i	i	k9	i
pro	pro	k7c4	pro
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hawkingovy	Hawkingův	k2eAgFnSc2d1	Hawkingova
teorie	teorie	k1gFnSc2	teorie
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
8	[number]	k4	8
π	π	k?	π
G	G	kA	G
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
Gk	Gk	k1gMnPc6	Gk
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
závislost	závislost	k1gFnSc1	závislost
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
-	-	kIx~	-
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
energii	energie	k1gFnSc3	energie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
teplota	teplota	k1gFnSc1	teplota
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
energií	energie	k1gFnSc7	energie
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přehled	přehled	k1gInSc1	přehled
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
výběr	výběr	k1gInSc1	výběr
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
jevů	jev	k1gInPc2	jev
a	a	k8xC	a
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
jako	jako	k8xC	jako
pojem	pojem	k1gInSc1	pojem
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
veličina	veličina	k1gFnSc1	veličina
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stručnou	stručný	k2eAgFnSc7d1	stručná
charakteristikou	charakteristika	k1gFnSc7	charakteristika
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
teplota	teplota	k1gFnSc1	teplota
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
spíše	spíše	k9	spíše
jako	jako	k9	jako
rozcestník	rozcestník	k1gInSc4	rozcestník
ke	k	k7c3	k
speciálním	speciální	k2eAgFnPc3d1	speciální
stránkám	stránka	k1gFnPc3	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
pojmem	pojem	k1gInSc7	pojem
ve	v	k7c6	v
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
termochemii	termochemie	k1gFnSc6	termochemie
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšování	zvyšování	k1gNnSc1	zvyšování
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
uvolňováním	uvolňování	k1gNnSc7	uvolňování
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
kondenzovaných	kondenzovaný	k2eAgFnPc6d1	kondenzovaná
látkách	látka	k1gFnPc6	látka
a	a	k8xC	a
zvyšováním	zvyšování	k1gNnSc7	zvyšování
jejich	jejich	k3xOp3gFnSc2	jejich
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
vzájemných	vzájemný	k2eAgFnPc6d1	vzájemná
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
schopnost	schopnost	k1gFnSc4	schopnost
překonání	překonání	k1gNnSc3	překonání
potenciálových	potenciálový	k2eAgFnPc2d1	potenciálová
bariér	bariéra	k1gFnPc2	bariéra
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
-	-	kIx~	-
teplota	teplota	k1gFnSc1	teplota
má	mít	k5eAaImIp3nS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
chemické	chemický	k2eAgFnPc4d1	chemická
reakce	reakce	k1gFnPc4	reakce
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
následující	následující	k2eAgFnSc3d1	následující
závislosti	závislost	k1gFnSc3	závislost
<g/>
:	:	kIx,	:
Závislost	závislost	k1gFnSc1	závislost
rovnovážné	rovnovážný	k2eAgFnSc2d1	rovnovážná
konstanty	konstanta	k1gFnSc2	konstanta
reakce	reakce	k1gFnSc2	reakce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
van	van	k1gInSc1	van
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hoffova	Hoffův	k2eAgFnSc1d1	Hoffova
rovnice	rovnice	k1gFnSc1	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
ln	ln	k?	ln
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
Δ	Δ	k?	Δ
H	H	kA	H
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
\	\	kIx~	\
<g/>
ln	ln	k?	ln
K	K	kA	K
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
H	H	kA	H
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
R	R	kA	R
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
;	;	kIx,	;
pro	pro	k7c4	pro
endotermické	endotermický	k2eAgFnPc4d1	endotermická
reakce	reakce	k1gFnPc4	reakce
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
H	H	kA	H
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
H	H	kA	H
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
)	)	kIx)	)
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
konstanta	konstanta	k1gFnSc1	konstanta
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
teplotou	teplota	k1gFnSc7	teplota
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
exotermické	exotermický	k2eAgMnPc4d1	exotermický
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
H	H	kA	H
<	<	kIx(	<
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
H	H	kA	H
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rychlostní	rychlostní	k2eAgFnSc4d1	rychlostní
konstantu	konstanta	k1gFnSc4	konstanta
reakce	reakce	k1gFnSc2	reakce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
Arrheniova	Arrheniův	k2eAgFnSc1d1	Arrheniova
rovnice	rovnice	k1gFnSc1	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
A	a	k9	a
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
A	a	k9	a
<g/>
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
RT	RT	kA	RT
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
'	'	kIx"	'
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
aktivační	aktivační	k2eAgFnSc1d1	aktivační
energie	energie	k1gFnSc1	energie
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
teploty	teplota	k1gFnSc2	teplota
mezi	mezi	k7c7	mezi
zemským	zemský	k2eAgNnSc7d1	zemské
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
vzniku	vznik	k1gInSc2	vznik
konvexních	konvexní	k2eAgInPc2d1	konvexní
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
plasticko-tekutých	plastickoekutý	k2eAgFnPc6d1	plasticko-tekutý
vrstvách	vrstva	k1gFnPc6	vrstva
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
důsledku	důsledek	k1gInSc6	důsledek
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pohybům	pohyb	k1gInPc3	pohyb
i	i	k8xC	i
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
a	a	k8xC	a
souvisejícím	související	k2eAgInPc3d1	související
geologickým	geologický	k2eAgInPc3d1	geologický
jevům	jev	k1gInPc3	jev
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
působících	působící	k2eAgInPc2d1	působící
faktorů	faktor	k1gInPc2	faktor
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
nebo	nebo	k8xC	nebo
regionální	regionální	k2eAgFnSc2d1	regionální
metamorfózy	metamorfóza	k1gFnSc2	metamorfóza
<g/>
.	.	kIx.	.
</s>
<s>
Procesy	proces	k1gInPc1	proces
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
založeny	založen	k2eAgInPc4d1	založen
na	na	k7c6	na
teplotních	teplotní	k2eAgInPc6d1	teplotní
rozdílech	rozdíl	k1gInPc6	rozdíl
(	(	kIx(	(
<g/>
způsobené	způsobený	k2eAgFnPc4d1	způsobená
zahříváním	zahřívání	k1gNnSc7	zahřívání
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
Sluncem	slunce	k1gNnSc7	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
jimi	on	k3xPp3gMnPc7	on
vyvolanému	vyvolaný	k2eAgNnSc3d1	vyvolané
proudění	proudění	k1gNnSc3	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
příčinou	příčina	k1gFnSc7	příčina
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
podnebí	podnebí	k1gNnSc1	podnebí
a	a	k8xC	a
počasí	počasí	k1gNnSc1	počasí
jsou	být	k5eAaImIp3nP	být
důležitými	důležitý	k2eAgInPc7d1	důležitý
faktory	faktor	k1gInPc7	faktor
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
oblastí	oblast	k1gFnPc2	oblast
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
jejich	jejich	k3xOp3gNnSc3	jejich
pravidelnému	pravidelný	k2eAgNnSc3d1	pravidelné
sledování	sledování	k1gNnSc3	sledování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
i	i	k9	i
předpověď	předpověď	k1gFnSc1	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
sledovaným	sledovaný	k2eAgInPc3d1	sledovaný
meteorologickým	meteorologický	k2eAgInPc3d1	meteorologický
údajům	údaj	k1gInPc3	údaj
patří	patřit	k5eAaImIp3nS	patřit
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2	[number]	k4	2
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
(	(	kIx(	(
<g/>
v	v	k7c6	v
meteorologické	meteorologický	k2eAgFnSc6d1	meteorologická
budce	budka	k1gFnSc6	budka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
denní	denní	k2eAgFnSc1d1	denní
minimální	minimální	k2eAgFnSc1d1	minimální
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
denní	denní	k2eAgFnSc1d1	denní
maximální	maximální	k2eAgFnSc1d1	maximální
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
průměr	průměr	k1gInSc4	průměr
z	z	k7c2	z
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
naměřené	naměřený	k2eAgFnPc4d1	naměřená
v	v	k7c6	v
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnPc1	teplota
ve	v	k7c4	v
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
započtené	započtený	k2eAgFnPc4d1	započtená
teploty	teplota	k1gFnPc4	teplota
v	v	k7c4	v
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
místního	místní	k2eAgInSc2d1	místní
středního	střední	k2eAgInSc2d1	střední
slunečního	sluneční	k2eAgInSc2d1	sluneční
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
sleduje	sledovat	k5eAaImIp3nS	sledovat
přízemní	přízemní	k2eAgFnSc1d1	přízemní
minimální	minimální	k2eAgFnSc1d1	minimální
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
minimum	minimum	k1gNnSc1	minimum
naměřené	naměřený	k2eAgNnSc1d1	naměřené
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
5	[number]	k4	5
cm	cm	kA	cm
nad	nad	k7c7	nad
zemským	zemský	k2eAgInSc7d1	zemský
povrchem	povrch	k1gInSc7	povrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rosný	rosný	k2eAgInSc1d1	rosný
bod	bod	k1gInSc1	bod
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vzduch	vzduch	k1gInSc4	vzduch
maximální	maximální	k2eAgFnSc2d1	maximální
možné	možný	k2eAgFnSc2d1	možná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
)	)	kIx)	)
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
klimatickým	klimatický	k2eAgInPc3d1	klimatický
údajům	údaj	k1gInPc3	údaj
patří	patřit	k5eAaImIp3nS	patřit
roční	roční	k2eAgInPc1d1	roční
průběhy	průběh	k1gInPc1	průběh
maximální	maximální	k2eAgFnSc2d1	maximální
a	a	k8xC	a
minimální	minimální	k2eAgFnSc2d1	minimální
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
maximální	maximální	k2eAgFnPc1d1	maximální
a	a	k8xC	a
minimální	minimální	k2eAgFnPc1d1	minimální
povrchové	povrchový	k2eAgFnPc1d1	povrchová
teploty	teplota	k1gFnPc1	teplota
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Biochemické	biochemický	k2eAgInPc1d1	biochemický
procesy	proces	k1gInPc1	proces
mohou	moct	k5eAaImIp3nP	moct
probíhat	probíhat	k5eAaImF	probíhat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
rozsahu	rozsah	k1gInSc6	rozsah
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
teplota	teplota	k1gFnSc1	teplota
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
přízemní	přízemní	k2eAgFnSc2d1	přízemní
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
vodstva	vodstvo	k1gNnSc2	vodstvo
klíčovým	klíčový	k2eAgInSc7d1	klíčový
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
ekologii	ekologie	k1gFnSc4	ekologie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
schopnost	schopnost	k1gFnSc1	schopnost
vnímání	vnímání	k1gNnSc2	vnímání
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
nebo	nebo	k8xC	nebo
okolní	okolní	k2eAgFnSc2d1	okolní
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
termorecepce	termorecepec	k1gInSc2	termorecepec
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
schopnost	schopnost	k1gFnSc1	schopnost
aktivní	aktivní	k2eAgFnSc1d1	aktivní
regulace	regulace	k1gFnSc1	regulace
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
teplokrevnost	teplokrevnost	k1gFnSc1	teplokrevnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
indikátorem	indikátor	k1gInSc7	indikátor
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
organismech	organismus	k1gInPc6	organismus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
měření	měření	k1gNnSc1	měření
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lékařské	lékařský	k2eAgFnSc6d1	lékařská
diagnostice	diagnostika	k1gFnSc6	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
či	či	k8xC	či
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
nebo	nebo	k8xC	nebo
změny	změna	k1gFnPc1	změna
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
léčebných	léčebný	k2eAgFnPc6d1	léčebná
<g/>
,	,	kIx,	,
rehabilitačních	rehabilitační	k2eAgFnPc6d1	rehabilitační
a	a	k8xC	a
kosmetických	kosmetický	k2eAgFnPc6d1	kosmetická
terapiích	terapie	k1gFnPc6	terapie
(	(	kIx(	(
<g/>
studené	studený	k2eAgInPc1d1	studený
zábaly	zábal	k1gInPc1	zábal
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
<g/>
,	,	kIx,	,
laserové	laserový	k2eAgNnSc1d1	laserové
vypalování	vypalování	k1gNnSc1	vypalování
<g/>
,	,	kIx,	,
zahřívání	zahřívání	k1gNnSc1	zahřívání
infralampou	infralampa	k1gFnSc7	infralampa
<g/>
,	,	kIx,	,
teplotní	teplotní	k2eAgFnSc1d1	teplotní
léčba	léčba	k1gFnSc1	léčba
prostaty	prostata	k1gFnSc2	prostata
<g/>
,	,	kIx,	,
vymrazování	vymrazování	k1gNnSc1	vymrazování
bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
studené	studený	k2eAgFnPc1d1	studená
a	a	k8xC	a
teplé	teplý	k2eAgFnPc1d1	teplá
koupele	koupel	k1gFnPc1	koupel
<g/>
,	,	kIx,	,
horký	horký	k2eAgInSc1d1	horký
parafín	parafín	k1gInSc1	parafín
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přípravě	příprava	k1gFnSc3	příprava
lékařských	lékařský	k2eAgInPc2d1	lékařský
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
sterilizace	sterilizace	k1gFnSc2	sterilizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Petrochemie	petrochemie	k1gFnSc1	petrochemie
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
-	-	kIx~	-
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
v	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
<g/>
:	:	kIx,	:
frakční	frakční	k2eAgFnSc2d1	frakční
destilace	destilace	k1gFnSc2	destilace
a	a	k8xC	a
rektifikace	rektifikace	k1gFnSc2	rektifikace
<g/>
,	,	kIx,	,
krakování	krakování	k1gNnSc2	krakování
<g/>
.	.	kIx.	.
</s>
<s>
Metalurgie	metalurgie	k1gFnSc1	metalurgie
<g/>
,	,	kIx,	,
sváření	sváření	k1gNnSc1	sváření
<g/>
,	,	kIx,	,
sklářství	sklářství	k1gNnSc1	sklářství
-	-	kIx~	-
využití	využití	k1gNnSc1	využití
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
k	k	k7c3	k
tavení	tavení	k1gNnSc3	tavení
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
skla	sklo	k1gNnSc2	sklo
Keramický	keramický	k2eAgInSc4d1	keramický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc4	průmysl
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
-	-	kIx~	-
využití	využití	k1gNnSc1	využití
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
k	k	k7c3	k
vypalování	vypalování	k1gNnSc3	vypalování
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
(	(	kIx(	(
<g/>
cement	cement	k1gInSc1	cement
<g/>
,	,	kIx,	,
cihly	cihla	k1gFnPc1	cihla
<g/>
,	,	kIx,	,
tvárnice	tvárnice	k1gFnPc1	tvárnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
Dopravní	dopravní	k2eAgInPc1d1	dopravní
a	a	k8xC	a
energetický	energetický	k2eAgInSc1d1	energetický
průmysl	průmysl	k1gInSc1	průmysl
-	-	kIx~	-
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
teplotním	teplotní	k2eAgInPc3d1	teplotní
rozdílu	rozdíl	k1gInSc2	rozdíl
závisí	záviset	k5eAaImIp3nS	záviset
účinnost	účinnost	k1gFnSc4	účinnost
tepelných	tepelný	k2eAgInPc2d1	tepelný
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
parní	parní	k2eAgInPc4d1	parní
a	a	k8xC	a
spalovací	spalovací	k2eAgInPc4d1	spalovací
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
parní	parní	k2eAgFnPc4d1	parní
a	a	k8xC	a
plynové	plynový	k2eAgFnPc4d1	plynová
turbíny	turbína	k1gFnPc4	turbína
<g/>
,	,	kIx,	,
tepelná	tepelný	k2eAgNnPc4d1	tepelné
čerpadla	čerpadlo	k1gNnPc4	čerpadlo
<g/>
)	)	kIx)	)
Stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
-	-	kIx~	-
teplota	teplota	k1gFnSc1	teplota
klíčový	klíčový	k2eAgInSc4d1	klíčový
pojem	pojem	k1gInSc4	pojem
pro	pro	k7c4	pro
navrhování	navrhování	k1gNnSc4	navrhování
tepelné	tepelný	k2eAgFnSc2d1	tepelná
izolace	izolace	k1gFnSc2	izolace
staveb	stavba	k1gFnPc2	stavba
Požární	požární	k2eAgFnSc1d1	požární
ochrana	ochrana	k1gFnSc1	ochrana
-	-	kIx~	-
teplota	teplota	k1gFnSc1	teplota
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
faktorů	faktor	k1gInPc2	faktor
podmínek	podmínka	k1gFnPc2	podmínka
hoření	hoření	k2eAgFnPc1d1	hoření
<g/>
;	;	kIx,	;
zavádí	zavádět	k5eAaImIp3nP	zavádět
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
pojmy	pojem	k1gInPc1	pojem
<g/>
:	:	kIx,	:
teplota	teplota	k1gFnSc1	teplota
hoření	hoření	k1gNnSc2	hoření
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
vzplanutí	vzplanutí	k1gNnSc2	vzplanutí
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
vznícení	vznícení	k1gNnSc2	vznícení
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
u	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
surovin	surovina	k1gFnPc2	surovina
mechanické	mechanický	k2eAgFnPc4d1	mechanická
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc4d1	chemická
<g/>
,	,	kIx,	,
aromatické	aromatický	k2eAgFnPc4d1	aromatická
a	a	k8xC	a
chuťové	chuťový	k2eAgFnPc4d1	chuťová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
a	a	k8xC	a
gastronomii	gastronomie	k1gFnSc6	gastronomie
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
úpravě	úprava	k1gFnSc3	úprava
jídel	jídlo	k1gNnPc2	jídlo
mnoho	mnoho	k4c1	mnoho
postupů	postup	k1gInPc2	postup
založených	založený	k2eAgInPc2d1	založený
na	na	k7c4	na
zahřívání	zahřívání	k1gNnSc4	zahřívání
(	(	kIx(	(
<g/>
vaření	vaření	k1gNnSc1	vaření
<g/>
,	,	kIx,	,
pečení	pečení	k1gNnSc1	pečení
<g/>
,	,	kIx,	,
smažení	smažení	k1gNnSc1	smažení
<g/>
,	,	kIx,	,
pražení	pražení	k1gNnSc1	pražení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
chladem	chlad	k1gInSc7	chlad
či	či	k8xC	či
zmrazením	zmrazení	k1gNnSc7	zmrazení
lze	lze	k6eAd1	lze
zvýšit	zvýšit	k5eAaPmF	zvýšit
dobu	doba	k1gFnSc4	doba
uchovatelnosti	uchovatelnost	k1gFnSc2	uchovatelnost
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
konzervace	konzervace	k1gFnSc1	konzervace
potravin	potravina	k1gFnPc2	potravina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
rozvinutí	rozvinutí	k1gNnSc4	rozvinutí
řádných	řádný	k2eAgInPc2d1	řádný
chuťových	chuťový	k2eAgInPc2d1	chuťový
a	a	k8xC	a
aromatických	aromatický	k2eAgInPc2d1	aromatický
vjemů	vjem	k1gInPc2	vjem
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
jídel	jídlo	k1gNnPc2	jídlo
a	a	k8xC	a
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
gastronomii	gastronomie	k1gFnSc6	gastronomie
dbá	dbát	k5eAaImIp3nS	dbát
na	na	k7c4	na
správnou	správný	k2eAgFnSc4d1	správná
teplotu	teplota	k1gFnSc4	teplota
jídel	jídlo	k1gNnPc2	jídlo
a	a	k8xC	a
nápojů	nápoj	k1gInPc2	nápoj
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
podávání	podávání	k1gNnSc6	podávání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
závislých	závislý	k2eAgInPc2d1	závislý
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
oborech	obor	k1gInPc6	obor
významně	významně	k6eAd1	významně
projevuje	projevovat	k5eAaImIp3nS	projevovat
závislost	závislost	k1gFnSc4	závislost
rychlosti	rychlost	k1gFnSc2	rychlost
šíření	šíření	k1gNnSc2	šíření
zvukových	zvukový	k2eAgFnPc2d1	zvuková
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
teplotní	teplotní	k2eAgFnSc4d1	teplotní
roztažnost	roztažnost	k1gFnSc4	roztažnost
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
teploty	teplota	k1gFnSc2	teplota
mění	měnit	k5eAaImIp3nS	měnit
i	i	k9	i
rezonanční	rezonanční	k2eAgFnPc4d1	rezonanční
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
frekvence	frekvence	k1gFnPc4	frekvence
-	-	kIx~	-
tedy	tedy	k9	tedy
objektivní	objektivní	k2eAgFnSc1d1	objektivní
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
<g/>
)	)	kIx)	)
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
ozvučných	ozvučný	k2eAgFnPc2d1	ozvučná
desek	deska	k1gFnPc2	deska
i	i	k8xC	i
vzduchových	vzduchový	k2eAgInPc2d1	vzduchový
sloupců	sloupec	k1gInPc2	sloupec
-	-	kIx~	-
změna	změna	k1gFnSc1	změna
teploty	teplota	k1gFnSc2	teplota
si	se	k3xPyFc3	se
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
nové	nový	k2eAgNnSc4d1	nové
naladění	naladění	k1gNnSc4	naladění
většiny	většina	k1gFnSc2	většina
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
veličiny	veličina	k1gFnPc1	veličina
zpravidla	zpravidla	k6eAd1	zpravidla
nebývají	bývat	k5eNaImIp3nP	bývat
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
<g/>
;	;	kIx,	;
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
i	i	k9	i
teploty	teplota	k1gFnPc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
názvech	název	k1gInPc6	název
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
najde	najít	k5eAaPmIp3nS	najít
několik	několik	k4yIc1	několik
výjimek	výjimka	k1gFnPc2	výjimka
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
naznačeno	naznačen	k2eAgNnSc1d1	naznačeno
"	"	kIx"	"
<g/>
zdůvodnění	zdůvodnění	k1gNnSc1	zdůvodnění
<g/>
"	"	kIx"	"
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Romány	román	k1gInPc1	román
<g/>
:	:	kIx,	:
Ray	Ray	k1gFnSc1	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
<g/>
:	:	kIx,	:
451	[number]	k4	451
stupňů	stupeň	k1gInPc2	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
vznícení	vznícení	k1gNnSc2	vznícení
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
Filmy	film	k1gInPc4	film
<g/>
:	:	kIx,	:
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
<g />
.	.	kIx.	.
</s>
<s>
451	[number]	k4	451
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Bradburyho	Bradbury	k1gMnSc2	Bradbury
románu	román	k1gInSc2	román
<g/>
)	)	kIx)	)
37	[number]	k4	37
<g/>
°	°	k?	°
<g/>
2	[number]	k4	2
le	le	k?	le
matin	matin	k1gMnSc1	matin
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
lehce	lehko	k6eAd1	lehko
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
)	)	kIx)	)
8	[number]	k4	8
Grad	grad	k1gInSc1	grad
Celsius	Celsius	k1gMnSc1	Celsius
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
25	[number]	k4	25
degrés	degrés	k1gInSc1	degrés
en	en	k?	en
hiver	hiver	k1gInSc1	hiver
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Celsius	Celsius	k1gMnSc1	Celsius
41.11	[number]	k4	41.11
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Temperature	Temperatur	k1gMnSc5	Temperatur
at	at	k?	at
Which	Which	k1gInSc1	Which
the	the	k?	the
Brain	Brain	k1gInSc1	Brain
<g/>
...	...	k?	...
Begins	Begins	k1gInSc1	Begins
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnPc1	Die
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc3	který
mozek	mozek	k1gInSc1	mozek
začíná	začínat	k5eAaImIp3nS	začínat
umírat	umírat	k5eAaImF	umírat
<g/>
)	)	kIx)	)
Divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
Vedle	vedle	k7c2	vedle
stejnojmenných	stejnojmenný	k2eAgFnPc2d1	stejnojmenná
adaptací	adaptace	k1gFnPc2	adaptace
Bradburyho	Bradbury	k1gMnSc2	Bradbury
románu	román	k1gInSc2	román
lze	lze	k6eAd1	lze
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
uvést	uvést	k5eAaPmF	uvést
i	i	k9	i
postřeh	postřeh	k1gInSc4	postřeh
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
z	z	k7c2	z
úvodního	úvodní	k2eAgInSc2d1	úvodní
semináře	seminář	k1gInSc2	seminář
hry	hra	k1gFnSc2	hra
Posel	posít	k5eAaPmAgMnS	posít
z	z	k7c2	z
Liptákova	Liptákův	k2eAgMnSc2d1	Liptákův
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Teplé	Teplé	k2eAgNnSc1d1	Teplé
pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
studená	studený	k2eAgFnSc1d1	studená
Němka	Němka	k1gFnSc1	Němka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
