<s>
Micubiši	Micubiš	k1gMnPc1
K3M	K3M	k1gFnSc2
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
K3M	K3M	k1gFnSc3
Micubiši	Micubiše	k1gFnSc4
K	k	k7c3
<g/>
3	#num#	k4
<g/>
MUrčení	MUrčení	k1gNnSc3
</s>
<s>
cvičný	cvičný	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Mitsubishi	mitsubishi	k1gNnSc1
Jukogyo	Jukogyo	k1gMnSc1
K.	K.	kA
K.	K.	kA
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Hattori	Hattori	k1gNnSc1
První	první	k4xOgFnSc1
let	léto	k1gNnPc2
</s>
<s>
květen	květen	k1gInSc4
1930	#num#	k4
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
námořní	námořní	k2eAgNnSc1d1
letectvoJaponské	letectvoJaponský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
Výroba	výroba	k1gFnSc1
</s>
<s>
1930-1941	1930-1941	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
624	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
K3M	K3M	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
spojeneckém	spojenecký	k2eAgInSc6d1
kódu	kód	k1gInSc6
Pine	pin	k1gInSc5
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
japonský	japonský	k2eAgInSc1d1
jednomotorový	jednomotorový	k2eAgInSc1d1
hornoplošník	hornoplošník	k1gInSc1
smíšené	smíšený	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
s	s	k7c7
pevným	pevný	k2eAgInSc7d1
podvozkem	podvozek	k1gInSc7
ostruhového	ostruhový	k2eAgInSc2d1
typu	typ	k1gInSc2
z	z	k7c2
období	období	k1gNnSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
projevilo	projevit	k5eAaPmAgNnS
námořní	námořní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
zájem	zájem	k1gInSc1
o	o	k7c4
letoun	letoun	k1gInSc4
pro	pro	k7c4
výcvik	výcvik	k1gInSc4
posádek	posádka	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
vývoj	vývoj	k1gInSc4
svěřilo	svěřit	k5eAaPmAgNnS
společnosti	společnost	k1gFnSc3
Micubiši	Micubiše	k1gFnSc3
se	se	k3xPyFc4
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Nagoji	Nagoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstruktér	konstruktér	k1gMnSc1
Ing.	ing.	kA
Hattori	Hattor	k1gFnSc2
zde	zde	k6eAd1
navázal	navázat	k5eAaPmAgInS
na	na	k7c4
nepřijatý	přijatý	k2eNgInSc4d1
projekt	projekt	k1gInSc4
M-13	M-13	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
Herberta	Herbert	k1gMnSc2
Smithe	Smith	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
působil	působit	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
britské	britský	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
objednávce	objednávka	k1gFnSc6
dvou	dva	k4xCgInPc2
prototypů	prototyp	k1gInPc2
námořním	námořní	k2eAgMnPc3d1
letectvem	letectvo	k1gNnSc7
byl	být	k5eAaImAgInS
první	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
zalétán	zalétán	k2eAgMnSc1d1
v	v	k7c6
květnu	květen	k1gInSc6
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohon	pohon	k1gInSc1
zajišťoval	zajišťovat	k5eAaImAgInS
vidlicový	vidlicový	k2eAgInSc1d1
osmiválec	osmiválec	k1gInSc1
Hispano-Suiza	Hispano-Suiza	k1gFnSc1
8G	8G	k4
o	o	k7c6
výkonu	výkon	k1gInSc6
250	#num#	k4
kW	kW	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
Micubiši	Micubiš	k1gInSc6
licenčně	licenčně	k6eAd1
vyráběn	vyráběn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
dva	dva	k4xCgInPc1
stroje	stroj	k1gInPc1
s	s	k7c7
námořním	námořní	k2eAgNnSc7d1
označením	označení	k1gNnSc7
K3M1	K3M1	k1gFnSc2
se	se	k3xPyFc4
lišily	lišit	k5eAaImAgFnP
vzepětím	vzepětí	k1gNnPc3
křídla	křídlo	k1gNnSc2
pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
stability	stabilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémy	problém	k1gInPc4
s	s	k7c7
vibracemi	vibrace	k1gFnPc7
motorů	motor	k1gInPc2
včetně	včetně	k7c2
závad	závada	k1gFnPc2
jejich	jejich	k3xOp3gFnSc2
chladicí	chladicí	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
vedly	vést	k5eAaImAgFnP
k	k	k7c3
nepřijetí	nepřijetí	k1gNnSc3
nového	nový	k2eAgInSc2d1
typu	typ	k1gInSc2
námořním	námořní	k2eAgNnSc7d1
letectvem	letectvo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
požadovalo	požadovat	k5eAaImAgNnS
instalaci	instalace	k1gFnSc4
hvězdicové	hvězdicový	k2eAgFnSc2d1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
Hitači	Hitač	k1gMnPc1
Amakaze	Amakaz	k1gInSc6
11	#num#	k4
o	o	k7c6
identickém	identický	k2eAgInSc6d1
výkonu	výkon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Zabudování	zabudování	k1gNnSc1
požadovaného	požadovaný	k2eAgInSc2d1
motoru	motor	k1gInSc2
Amakaze	Amakaz	k1gInSc6
11	#num#	k4
do	do	k7c2
draku	drak	k1gInSc2
vznikla	vzniknout	k5eAaPmAgFnS
verze	verze	k1gFnSc1
K	K	kA
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
kromě	kromě	k7c2
mateřské	mateřský	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
stavěla	stavět	k5eAaImAgFnS
také	také	k9
firma	firma	k1gFnSc1
Aiči	Aič	k1gFnSc2
Tokei	Toke	k1gFnSc2
Denki	Denk	k1gFnSc2
K.K.	K.K.	k1gFnSc2
ve	v	k7c6
Funatace	Funatace	k1gFnSc1
(	(	kIx(
<g/>
247	#num#	k4
kusů	kus	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
výrobu	výrob	k1gInSc2
převzala	převzít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
K.	K.	kA
K.	K.	kA
Watanabe	Watanab	k1gMnSc5
Tekkosho	Tekkosha	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
postaven	postaven	k2eAgInSc4d1
301	#num#	k4
kus	kus	k1gInSc4
upravené	upravený	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
K	K	kA
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stroje	stroj	k1gInSc2
poháněl	pohánět	k5eAaImAgInS
hvězdicový	hvězdicový	k2eAgInSc1d1
motor	motor	k1gInSc1
Nakadžima	Nakadžimum	k1gNnSc2
Kotobuki	Kotobuk	k1gFnSc2
2	#num#	k4
KAI	KAI	kA
2	#num#	k4
o	o	k7c6
výkonu	výkon	k1gInSc6
426	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
této	tento	k3xDgFnSc2
verze	verze	k1gFnSc2
bylo	být	k5eAaImAgNnS
mírně	mírně	k6eAd1
zvětšeno	zvětšen	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
a	a	k8xC
ocasní	ocasní	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c4
hřbětě	hřbětě	k1gNnSc4
trupu	trup	k1gInSc2
pak	pak	k6eAd1
přibylo	přibýt	k5eAaPmAgNnS
střeliště	střeliště	k1gNnSc1
s	s	k7c7
kulometem	kulomet	k1gInSc7
vz.	vz.	k?
92	#num#	k4
ráže	ráže	k1gFnSc1
7,7	7,7	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
trup	trup	k1gInSc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
zavěsit	zavěsit	k5eAaPmF
čtyři	čtyři	k4xCgFnPc4
pumy	puma	k1gFnPc4
po	po	k7c4
30	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pilotem	pilot	k1gMnSc7
létal	létat	k5eAaImAgMnS
také	také	k9
instruktor	instruktor	k1gMnSc1
a	a	k8xC
tři	tři	k4xCgMnPc1
žáci	žák	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
dopravní	dopravní	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
K3M3-L	K3M3-L	k1gFnPc2
se	se	k3xPyFc4
do	do	k7c2
kabiny	kabina	k1gFnSc2
vešlo	vejít	k5eAaPmAgNnS
čtyři	čtyři	k4xCgNnPc4
až	až	k9
pět	pět	k4xCc4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařské	císařský	k2eAgNnSc1d1
armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
objednalo	objednat	k5eAaPmAgNnS
pro	pro	k7c4
své	své	k1gNnSc4
potřeby	potřeba	k1gFnSc2
dva	dva	k4xCgInPc4
prototypy	prototyp	k1gInPc4
<g/>
,	,	kIx,
požadovalo	požadovat	k5eAaImAgNnS
však	však	k9
určité	určitý	k2eAgFnSc2d1
konstrukční	konstrukční	k2eAgFnSc2d1
změny	změna	k1gFnSc2
námořních	námořní	k2eAgFnPc2d1
K	k	k7c3
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úpravy	úprava	k1gFnSc2
vedl	vést	k5eAaImAgMnS
konstruktér	konstruktér	k1gMnSc1
Masakiši	Masakiše	k1gFnSc4
Mizumo	Mizuma	k1gFnSc5
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
zesílil	zesílit	k5eAaPmAgMnS
motorové	motorový	k2eAgNnSc4d1
lože	lože	k1gNnSc4
a	a	k8xC
příď	příď	k1gFnSc4
trupu	trup	k1gInSc2
pro	pro	k7c4
instalaci	instalace	k1gFnSc4
motoru	motor	k1gInSc2
Micubiši	Micubiše	k1gFnSc3
vz.	vz.	k?
92	#num#	k4
o	o	k7c4
546	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1933	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
letectvo	letectvo	k1gNnSc1
převzalo	převzít	k5eAaPmAgNnS
pod	pod	k7c7
označením	označení	k1gNnSc7
Ki-	Ki-	k1gFnSc2
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
druhý	druhý	k4xOgInSc4
prototyp	prototyp	k1gInSc4
poháněný	poháněný	k2eAgInSc4d1
motorem	motor	k1gInSc7
Nakadžima	Nakadžima	k1gNnSc1
Kotobuki	Kotobuk	k1gFnSc2
o	o	k7c6
výkonu	výkon	k1gInSc6
330	#num#	k4
kW	kW	kA
již	již	k6eAd1
armádní	armádní	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
nejevilo	jevit	k5eNaImAgNnS
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
proto	proto	k6eAd1
předán	předat	k5eAaPmNgInS
společnosti	společnost	k1gFnSc3
Tokyo	Tokyo	k1gNnSc1
Koku	kok	k1gInSc2
K.	K.	kA
K.	K.	kA
k	k	k7c3
přestavbě	přestavba	k1gFnSc3
na	na	k7c6
civilní	civilní	k2eAgFnSc6d1
k	k	k7c3
přepravě	přeprava	k1gFnSc3
pěti	pět	k4xCc3
cestujících	cestující	k1gMnPc2
s	s	k7c7
otevřeným	otevřený	k2eAgInSc7d1
pilotním	pilotní	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zabudování	zabudování	k1gNnSc6
licenčního	licenční	k2eAgMnSc2d1
devítiválce	devítiválec	k1gMnSc2
Nakadžima	Nakadžima	k1gFnSc1
Jupiter	Jupiter	k1gMnSc1
IV	IV	kA
o	o	k7c4
309	#num#	k4
kW	kW	kA
nesl	nést	k5eAaImAgMnS
označení	označení	k1gNnSc4
MS-1	MS-1	k1gFnSc2
(	(	kIx(
<g/>
imatrikulace	imatrikulace	k1gFnSc1
J-BABQ	J-BABQ	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
kolový	kolový	k2eAgInSc1d1
podvozek	podvozek	k1gInSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
zaměněn	zaměnit	k5eAaPmNgInS
za	za	k7c4
plováky	plovák	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
K3M3	K3M3	k1gFnSc2
</s>
<s>
Údaje	údaj	k1gInPc1
platí	platit	k5eAaImIp3nP
pro	pro	k7c4
K	K	kA
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
15,78	15,78	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
9,54	9,54	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
3,82	3,82	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
34,50	34,50	k4
m²	m²	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
letounu	letoun	k1gInSc2
<g/>
:	:	kIx,
1360	#num#	k4
kg	kg	kA
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
2200	#num#	k4
kg	kg	kA
</s>
<s>
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
rychlost	rychlost	k1gFnSc1
v	v	k7c6
1000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
235	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Výstup	výstup	k1gInSc1
do	do	k7c2
5000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
9,5	9,5	k4
min	mina	k1gFnPc2
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
6390	#num#	k4
m	m	kA
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
800	#num#	k4
km	km	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Němeček	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mitsubishi	mitsubishi	k1gNnPc1
K	k	k7c3
<g/>
3	#num#	k4
<g/>
M.	M.	kA
Letectví	letectví	k1gNnSc2
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Micubiši	Micubiš	k1gMnPc1
K3M	K3M	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Micubiši	Micubiš	k1gMnPc1
K3M	K3M	k1gFnSc2
Pine	pin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Letouny	letoun	k1gInPc7
japonského	japonský	k2eAgNnSc2d1
císařského	císařský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
A4N	A4N	k4
•	•	k?
A5M	A5M	k1gMnSc1
•	•	k?
A6M	A6M	k1gMnSc1
•	•	k?
A6M2-N	A6M2-N	k1gMnSc1
•	•	k?
A7M	A7M	k1gMnSc1
•	•	k?
J1N	J1N	k1gMnSc1
•	•	k?
J2M	J2M	k1gMnSc1
•	•	k?
J5N	J5N	k1gMnSc1
•	•	k?
N1K	N1K	k1gMnSc1
Bombardovací	bombardovací	k2eAgInPc4d1
a	a	k8xC
torpédové	torpédový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
B5M	B5M	k4
•	•	k?
B5N	B5N	k1gMnSc1
•	•	k?
B6N	B6N	k1gMnSc1
•	•	k?
B7A	B7A	k1gMnSc1
•	•	k?
D1A	D1A	k1gMnSc1
•	•	k?
D3A	D3A	k1gMnSc1
•	•	k?
D4Y	D4Y	k1gMnSc1
•	•	k?
G3M	G3M	k1gMnSc1
•	•	k?
G4M	G4M	k1gMnSc1
•	•	k?
G5N	G5N	k1gMnSc1
•	•	k?
G8N	G8N	k1gMnSc1
•	•	k?
M6A	M6A	k1gMnSc1
•	•	k?
P1Y	P1Y	k1gMnSc1
Průzkumné	průzkumný	k2eAgInPc4d1
a	a	k8xC
spojovací	spojovací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
C3N	C3N	k4
•	•	k?
C5M	C5M	k1gMnSc1
•	•	k?
C6N	C6N	k1gMnSc1
•	•	k?
E7K	E7K	k1gMnSc1
•	•	k?
E8N	E8N	k1gMnSc1
•	•	k?
E11A	E11A	k1gMnSc1
•	•	k?
E13A	E13A	k1gMnSc1
•	•	k?
E14Y	E14Y	k1gMnSc1
•	•	k?
E15K	E15K	k1gMnSc1
•	•	k?
E16A	E16A	k1gMnSc1
•	•	k?
F1M	F1M	k1gMnSc1
•	•	k?
H5Y	H5Y	k1gMnSc1
•	•	k?
H6K	H6K	k1gMnSc1
•	•	k?
H8K	H8K	k1gMnSc1
•	•	k?
H9A	H9A	k1gMnSc1
•	•	k?
Q1W	Q1W	k1gMnSc1
Zbývající	zbývající	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Baika	Baika	k1gFnSc1
•	•	k?
MXY	MXY	kA
5	#num#	k4
•	•	k?
MXY7	MXY7	k1gMnSc1
Transportní	transportní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
L2D	L2D	k4
•	•	k?
L3Y	L3Y	k1gMnSc1
•	•	k?
L4M	L4M	k1gMnSc1
Experimentální	experimentální	k2eAgInPc4d1
letouny	letoun	k1gInPc4
a	a	k8xC
prototypy	prototyp	k1gInPc4
</s>
<s>
J7W	J7W	k4
•	•	k?
J8M	J8M	k1gMnSc1
•	•	k?
J9N	J9N	k1gMnSc1
•	•	k?
R2Y	R2Y	k1gMnSc1
•	•	k?
S1A	S1A	k1gFnSc2
Značení	značení	k1gNnSc2
japonských	japonský	k2eAgNnPc2d1
vojenských	vojenský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
