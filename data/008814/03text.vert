<p>
<s>
Emanuele	Emanuela	k1gFnSc3	Emanuela
Barbella	Barbella	k1gMnSc1	Barbella
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1718	[number]	k4	1718
Neapol	Neapol	k1gFnSc1	Neapol
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1777	[number]	k4	1777
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc4d1	základní
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
houslovou	houslový	k2eAgFnSc4d1	houslová
vituozitu	vituozita	k1gFnSc4	vituozita
získal	získat	k5eAaPmAgInS	získat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Francesca	Francescus	k1gMnSc2	Francescus
Barbelly	Barbella	k1gMnSc2	Barbella
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
neapolské	neapolský	k2eAgFnSc6d1	neapolská
kontervatoři	kontervator	k1gMnPc1	kontervator
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
di	di	k?	di
Loreto	Loreto	k1gNnSc1	Loreto
<g/>
.	.	kIx.	.
</s>
<s>
Kompozici	kompozice	k1gFnSc4	kompozice
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
proslulého	proslulý	k2eAgMnSc2d1	proslulý
neapolského	neapolský	k2eAgMnSc2d1	neapolský
skladatele	skladatel	k1gMnSc2	skladatel
Leonarda	Leonardo	k1gMnSc2	Leonardo
Lea	Leo	k1gMnSc2	Leo
a	a	k8xC	a
Michela	Michel	k1gMnSc2	Michel
Gabelloneho	Gabellone	k1gMnSc2	Gabellone
(	(	kIx(	(
<g/>
1692	[number]	k4	1692
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
houslistou	houslista	k1gMnSc7	houslista
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Teatro	Teatro	k1gNnSc4	Teatro
Nuovo	Nuovo	k1gNnSc4	Nuovo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1756	[number]	k4	1756
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Capella	Capello	k1gNnSc2	Capello
Reale	Real	k1gInSc6	Real
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1761	[number]	k4	1761
působil	působit	k5eAaImAgMnS	působit
v	v	k7c4	v
Teatro	Teatro	k1gNnSc4	Teatro
San	San	k1gFnSc2	San
Carlo	Carlo	k1gNnSc1	Carlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k9	i
pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
studenty	student	k1gMnPc7	student
byli	být	k5eAaImAgMnP	být
Pasquale	Pasquala	k1gFnSc3	Pasquala
Bini	Bin	k1gFnSc3	Bin
a	a	k8xC	a
Giuseppe	Giusepp	k1gInSc5	Giusepp
Tartini	Tartin	k2eAgMnPc5d1	Tartin
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
průvodcem	průvodce	k1gMnSc7	průvodce
Charlese	Charles	k1gMnSc2	Charles
Burneyho	Burney	k1gMnSc2	Burney
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
informací	informace	k1gFnPc2	informace
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
známý	známý	k2eAgInSc4d1	známý
hudební	hudební	k2eAgInSc4d1	hudební
cestopis	cestopis	k1gInSc4	cestopis
The	The	k1gMnSc1	The
Present	Present	k1gMnSc1	Present
State	status	k1gInSc5	status
of	of	k?	of
Music	Musice	k1gInPc2	Musice
in	in	k?	in
France	Franc	k1gMnSc2	Franc
and	and	k?	and
Italy	Ital	k1gMnPc7	Ital
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
patrně	patrně	k6eAd1	patrně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Londýn	Londýn	k1gInSc4	Londýn
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
vydána	vydán	k2eAgFnSc1d1	vydána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Barbellovo	Barbellův	k2eAgNnSc1d1	Barbellův
dílo	dílo	k1gNnSc1	dílo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
široké	široký	k2eAgNnSc1d1	široké
spektrum	spektrum	k1gNnSc1	spektrum
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
smyčcové	smyčcový	k2eAgInPc4d1	smyčcový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
dueta	dueto	k1gNnSc2	dueto
<g/>
,	,	kIx,	,
tria	trio	k1gNnSc2	trio
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnSc1	kombinace
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
a	a	k8xC	a
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
i	i	k9	i
koncerty	koncert	k1gInPc4	koncert
pro	pro	k7c4	pro
mandolínu	mandolína	k1gFnSc4	mandolína
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnPc7	jeho
skladbami	skladba	k1gFnPc7	skladba
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Elmira	Elmira	k1gFnSc1	Elmira
generosa	generosa	k1gFnSc1	generosa
<g/>
,	,	kIx,	,
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaPmAgInS	napsat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Nicolou	Nicola	k1gMnSc7	Nicola
Logroscinem	Logroscin	k1gMnSc7	Logroscin
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnSc1	kompozice
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
melodie	melodie	k1gFnPc1	melodie
snadno	snadno	k6eAd1	snadno
zapamatovatelné	zapamatovatelný	k2eAgFnPc1d1	zapamatovatelná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
značné	značný	k2eAgFnPc4d1	značná
popularity	popularita	k1gFnPc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
houslových	houslový	k2eAgFnPc6d1	houslová
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elmira	Elmira	k1gFnSc1	Elmira
generosa	generosa	k1gFnSc1	generosa
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
buffa	buffa	k1gFnSc1	buffa
<g/>
,	,	kIx,	,
libreto	libreto	k1gNnSc1	libreto
Pietro	Pietro	k1gNnSc1	Pietro
Trinchera	Trinchera	k1gFnSc1	Trinchera
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
Nicola	Nicola	k1gFnSc1	Nicola
Bonifacio	Bonifacio	k6eAd1	Bonifacio
Logroscino	Logroscino	k1gNnSc1	Logroscino
<g/>
,	,	kIx,	,
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
Concerti	Concert	k1gMnPc1	Concert
</s>
</p>
<p>
<s>
12	[number]	k4	12
triových	triový	k2eAgFnPc2d1	triová
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k1gNnSc5	continuo
</s>
</p>
<p>
<s>
6	[number]	k4	6
trii	trio	k1gNnPc7	trio
per	pero	k1gNnPc2	pero
violino	violina	k1gFnSc5	violina
e	e	k0	e
violoncello	violoncello	k1gNnSc4	violoncello
</s>
</p>
<p>
<s>
33	[number]	k4	33
Duetti	Duetti	k1gNnSc6	Duetti
per	pero	k1gNnPc2	pero
2	[number]	k4	2
Violini	Violin	k2eAgMnPc1d1	Violin
</s>
</p>
<p>
<s>
12	[number]	k4	12
Soli	sůl	k1gFnPc4	sůl
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k1gNnSc5	continuo
</s>
</p>
<p>
<s>
17	[number]	k4	17
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
</s>
</p>
<p>
<s>
6	[number]	k4	6
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
cembalo	cembalo	k1gNnSc4	cembalo
</s>
</p>
<p>
<s>
6	[number]	k4	6
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
</s>
</p>
<p>
<s>
4	[number]	k4	4
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
mandolíny	mandolína	k1gFnPc4	mandolína
</s>
</p>
<p>
<s>
3	[number]	k4	3
triové	triový	k2eAgFnPc4d1	triová
sonáty	sonáta	k1gFnPc4	sonáta
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k1gNnSc5	continuo
</s>
</p>
<p>
<s>
3	[number]	k4	3
sonáty	sonáta	k1gFnPc4	sonáta
pro	pro	k7c4	pro
dvoje	dvoje	k4xRgFnPc4	dvoje
housle	housle	k1gFnPc4	housle
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
dvě	dva	k4xCgFnPc4	dva
mandolíny	mandolína	k1gFnPc4	mandolína
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Emanuele	Emanuela	k1gFnSc3	Emanuela
Barbella	Barbello	k1gNnSc2	Barbello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaBmAgMnS	dít
od	od	k7c2	od
Emanuela	Emanuel	k1gMnSc2	Emanuel
Barbelly	Barbella	k1gMnSc2	Barbella
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dila	Dila	k1gFnSc1	Dila
Emanuela	Emanuel	k1gMnSc2	Emanuel
Brebelly	Brebella	k1gMnSc2	Brebella
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Bibliothè	Bibliothè	k1gMnSc2	Bibliothè
Nationale	Nationale	k1gMnSc2	Nationale
de	de	k?	de
France	Franc	k1gMnSc2	Franc
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
dostupný	dostupný	k2eAgInSc1d1	dostupný
notový	notový	k2eAgInSc1d1	notový
materiál	materiál	k1gInSc1	materiál
</s>
</p>
