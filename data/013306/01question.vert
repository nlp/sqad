<s>
Proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kovové	kovový	k2eAgFnPc1d1	kovová
součástky	součástka	k1gFnPc1	součástka
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
tlaky	tlak	k1gInPc7	tlak
v	v	k7c6	v
agresivním	agresivní	k2eAgNnSc6d1	agresivní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
?	?	kIx.	?
</s>
