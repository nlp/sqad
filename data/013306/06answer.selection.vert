<s>
Z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kovové	kovový	k2eAgFnPc1d1	kovová
součástky	součástka	k1gFnPc1	součástka
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
tlaky	tlak	k1gInPc7	tlak
v	v	k7c6	v
agresivním	agresivní	k2eAgNnSc6d1	agresivní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kluzná	kluzný	k2eAgNnPc1d1	kluzné
ložiska	ložisko	k1gNnPc1	ložisko
<g/>
,	,	kIx,	,
pružinová	pružinový	k2eAgNnPc1d1	pružinové
pera	pero	k1gNnPc1	pero
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
součásti	součást	k1gFnPc1	součást
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odolávají	odolávat	k5eAaImIp3nP	odolávat
působení	působení	k1gNnSc4	působení
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
