<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
(	(	kIx(	(
<g/>
ע	ע	k?	ע
<g/>
ִ	ִ	k?	ִ
<g/>
ב	ב	k?	ב
<g/>
ְ	ְ	k?	ְ
<g/>
ר	ר	k?	ר
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
,	,	kIx,	,
ivrit	ivrit	k1gInSc1	ivrit
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
souhrnně	souhrnně	k6eAd1	souhrnně
všechny	všechen	k3xTgFnPc4	všechen
vývojové	vývojový	k2eAgFnPc4d1	vývojová
varianty	varianta	k1gFnPc4	varianta
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
jazyka	jazyk	k1gInSc2	jazyk
od	od	k7c2	od
starověké	starověký	k2eAgFnSc2d1	starověká
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
přes	přes	k7c4	přes
biblickou	biblický	k2eAgFnSc4d1	biblická
(	(	kIx(	(
<g/>
ע	ע	k?	ע
ת	ת	k?	ת
<g/>
״	״	k?	״
<g/>
ך	ך	k?	ך
ivrit	ivrit	k2eAgInSc4d1	ivrit
Tanach	Tanach	k1gInSc4	Tanach
<g/>
,	,	kIx,	,
též	též	k9	též
ל	ל	k?	ל
ק	ק	k?	ק
<g/>
,	,	kIx,	,
lešon	lešon	k1gInSc1	lešon
kodeš	kodat	k5eAaPmIp2nS	kodat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
svatý	svatý	k2eAgInSc1d1	svatý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
moderní	moderní	k2eAgFnSc4d1	moderní
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
(	(	kIx(	(
<g/>
ע	ע	k?	ע
<g/>
,	,	kIx,	,
ivrit	ivrit	k1gMnSc1	ivrit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
hovoří	hovořit	k5eAaImIp3nS	hovořit
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ke	k	k7c3	k
kanaánské	kanaánský	k2eAgFnSc3d1	kanaánská
severozápadní	severozápadní	k2eAgFnSc3d1	severozápadní
skupině	skupina	k1gFnSc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
dochované	dochovaný	k2eAgInPc1d1	dochovaný
hebrejské	hebrejský	k2eAgInPc1d1	hebrejský
texty	text	k1gInPc1	text
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
gezerský	gezerský	k2eAgInSc4d1	gezerský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
starších	starý	k2eAgInPc2d2	starší
textů	text	k1gInPc2	text
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
plné	plný	k2eAgFnSc6d1	plná
diferenciaci	diferenciace	k1gFnSc6	diferenciace
severozápadních	severozápadní	k2eAgInPc2d1	severozápadní
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
mezi	mezi	k7c4	mezi
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
jazyky	jazyk	k1gInPc4	jazyk
či	či	k8xC	či
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
protosemitštině	protosemitština	k1gFnSc6	protosemitština
nebo	nebo	k8xC	nebo
protokeneanštině	protokeneanština	k1gFnSc6	protokeneanština
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
základní	základní	k2eAgNnSc4d1	základní
řazení	řazení	k1gNnSc4	řazení
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
mezi	mezi	k7c4	mezi
semitské	semitský	k2eAgInPc4d1	semitský
jazyky	jazyk	k1gInPc4	jazyk
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
moderní	moderní	k2eAgFnSc1d1	moderní
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
semitským	semitský	k2eAgInSc7d1	semitský
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
indoevropským	indoevropský	k2eAgInSc7d1	indoevropský
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
zásadnímu	zásadní	k2eAgInSc3d1	zásadní
vlivu	vliv	k1gInSc3	vliv
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
strukturu	struktura	k1gFnSc4	struktura
původní	původní	k2eAgFnSc2d1	původní
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
zcela	zcela	k6eAd1	zcela
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Výraz	výraz	k1gInSc1	výraz
hebrejština	hebrejština	k1gFnSc1	hebrejština
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
'	'	kIx"	'
<g/>
Ε	Ε	k?	Ε
(	(	kIx(	(
<g/>
hebrajos	hebrajos	k1gMnSc1	hebrajos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
občas	občas	k6eAd1	občas
označovali	označovat	k5eAaImAgMnP	označovat
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
Židé	Žid	k1gMnPc1	Žid
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k8xS	jako
ivrit	ivrit	k1gInSc4	ivrit
(	(	kIx(	(
<g/>
ע	ע	k?	ע
<g/>
ִ	ִ	k?	ִ
<g/>
ב	ב	k?	ב
<g/>
ְ	ְ	k?	ְ
<g/>
ר	ר	k?	ר
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
jazyk	jazyk	k1gInSc1	jazyk
Hebrejů	Hebrej	k1gMnPc2	Hebrej
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
"	"	kIx"	"
<g/>
svatý	svatý	k2eAgInSc1d1	svatý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ל	ל	k?	ל
<g/>
ְ	ְ	k?	ְ
<g/>
ש	ש	k?	ש
<g/>
ׁ	ׁ	k?	ׁ
<g/>
ו	ו	k?	ו
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ן	ן	k?	ן
ה	ה	k?	ה
<g/>
ַ	ַ	k?	ַ
<g/>
ק	ק	k?	ק
<g/>
ּ	ּ	k?	ּ
<g/>
ו	ו	k?	ו
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ד	ד	k?	ד
<g/>
ֶ	ֶ	k?	ֶ
<g/>
ש	ש	k?	ש
<g/>
ׁ	ׁ	k?	ׁ
lešon	lešon	k1gInSc1	lešon
ha-kodeš	haodat	k5eAaPmIp2nS	ha-kodat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
hebrejská	hebrejský	k2eAgNnPc4d1	hebrejské
označení	označení	k1gNnPc4	označení
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
doložena	doložit	k5eAaPmNgFnS	doložit
až	až	k9	až
z	z	k7c2	z
Mišny	Mišna	k1gFnSc2	Mišna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hebrejské	hebrejský	k2eAgNnSc4d1	hebrejské
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
proměňovala	proměňovat	k5eAaImAgFnS	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
10	[number]	k4	10
<g/>
.	.	kIx.	.
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
používalo	používat	k5eAaImAgNnS	používat
tzv.	tzv.	kA	tzv.
paleohebrejské	paleohebrejský	k2eAgNnSc1d1	paleohebrejský
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
fénického	fénický	k2eAgNnSc2d1	fénické
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
vlivem	vliv	k1gInSc7	vliv
aramejštiny	aramejština	k1gFnSc2	aramejština
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
textu	text	k1gInSc2	text
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
užívaly	užívat	k5eAaImAgFnP	užívat
aramejské	aramejský	k2eAgInPc4d1	aramejský
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
uzpůsobeny	uzpůsobit	k5eAaPmNgFnP	uzpůsobit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
kvadrátního	kvadrátní	k2eAgNnSc2d1	kvadrátní
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
výsadní	výsadní	k2eAgFnSc7d1	výsadní
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
však	však	k9	však
kvadrátní	kvadrátní	k2eAgInSc4d1	kvadrátní
typ	typ	k1gInSc4	typ
písma	písmo	k1gNnSc2	písmo
dochoval	dochovat	k5eAaPmAgMnS	dochovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
22	[number]	k4	22
konsonantů	konsonant	k1gInPc2	konsonant
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
akcenty	akcent	k1gInPc1	akcent
a	a	k8xC	a
případně	případně	k6eAd1	případně
další	další	k2eAgInPc4d1	další
znaky	znak	k1gInPc4	znak
do	do	k7c2	do
abecedy	abeceda	k1gFnSc2	abeceda
nepatří	patřit	k5eNaImIp3nP	patřit
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
jako	jako	k8xS	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
správného	správný	k2eAgNnSc2d1	správné
čtení	čtení	k1gNnSc2	čtení
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
posvátných	posvátný	k2eAgInPc2d1	posvátný
hebrejských	hebrejský	k2eAgInPc2d1	hebrejský
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejské	hebrejský	k2eAgFnPc4d1	hebrejská
číslice	číslice	k1gFnPc4	číslice
je	být	k5eAaImIp3nS	být
kvazi-desítková	kvaziesítkový	k2eAgFnSc1d1	kvazi-desítkový
abecední	abecední	k2eAgFnSc1d1	abecední
číselná	číselný	k2eAgFnSc1d1	číselná
soustava	soustava	k1gFnSc1	soustava
používající	používající	k2eAgFnSc1d1	používající
písmena	písmeno	k1gNnPc4	písmeno
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
upraven	upraven	k2eAgInSc1d1	upraven
z	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
číslic	číslice	k1gFnPc2	číslice
na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
hebrejské	hebrejský	k2eAgFnPc4d1	hebrejská
písemné	písemný	k2eAgFnPc4d1	písemná
památky	památka	k1gFnPc4	památka
pocházející	pocházející	k2eAgFnPc4d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
izraelského	izraelský	k2eAgMnSc2d1	izraelský
a	a	k8xC	a
judského	judský	k2eAgNnSc2d1	Judské
království	království	k1gNnSc2	království
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
Gezerský	Gezerský	k2eAgInSc4d1	Gezerský
kalendář	kalendář	k1gInSc4	kalendář
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Samařská	samařský	k2eAgFnSc1d1	samařská
ostraka	ostrak	k1gMnSc4	ostrak
(	(	kIx(	(
<g/>
cca	cca	kA	cca
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šíloašský	Šíloašský	k2eAgInSc1d1	Šíloašský
nápis	nápis	k1gInSc1	nápis
(	(	kIx(	(
<g/>
cca	cca	kA	cca
700	[number]	k4	700
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Lakíšská	Lakíšský	k2eAgFnSc1d1	Lakíšský
ostraka	ostrak	k1gMnSc2	ostrak
z	z	k7c2	z
Lachiše	Lachiše	k1gFnSc2	Lachiše
a	a	k8xC	a
Tel	tel	kA	tel
Aradu	Arad	k1gInSc2	Arad
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
pádem	pád	k1gInSc7	pád
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
v	v	k7c6	v
r.	r.	kA	r.
586	[number]	k4	586
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
texty	text	k1gInPc1	text
mají	mít	k5eAaImIp3nP	mít
oproti	oproti	k7c3	oproti
biblické	biblický	k2eAgFnSc3d1	biblická
hebrejštině	hebrejština	k1gFnSc3	hebrejština
jednodušší	jednoduchý	k2eAgFnSc4d2	jednodušší
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gNnPc2	on
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
písmena	písmeno	k1gNnPc1	písmeno
sloužící	sloužící	k1gFnSc2	sloužící
jako	jako	k8xC	jako
podklady	podklad	k1gInPc4	podklad
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
defektivní	defektivní	k2eAgNnSc1d1	defektivní
psaní	psaní	k1gNnSc1	psaní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
צ	צ	k?	צ
oproti	oproti	k7c3	oproti
צ	צ	k?	צ
(	(	kIx(	(
<g/>
skála	skála	k1gFnSc1	skála
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
א	א	k?	א
oproti	oproti	k7c3	oproti
א	א	k?	א
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
nápisech	nápis	k1gInPc6	nápis
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
pozdější	pozdní	k2eAgInPc1d2	pozdější
diftongizované	diftongizovaný	k2eAgInPc1d1	diftongizovaný
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
י	י	k?	י
jn	jn	k?	jn
oproti	oproti	k7c3	oproti
י	י	k?	י
jajin	jajina	k1gFnPc2	jajina
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Gezerském	Gezerský	k2eAgInSc6d1	Gezerský
kalendáři	kalendář	k1gInSc6	kalendář
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vzácná	vzácný	k2eAgFnSc1d1	vzácná
forma	forma	k1gFnSc1	forma
duálového	duálový	k2eAgInSc2d1	duálový
tvaru	tvar	k1gInSc2	tvar
ve	v	k7c6	v
statu	status	k1gInSc6	status
konstruktu	konstrukt	k1gInSc2	konstrukt
zakončená	zakončený	k2eAgFnSc1d1	zakončená
na	na	k7c4	na
-o	-o	k?	-o
(	(	kIx(	(
<g/>
י	י	k?	י
jarcho	jarcho	k6eAd1	jarcho
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
tyto	tento	k3xDgInPc1	tento
nápisy	nápis	k1gInPc1	nápis
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
v	v	k7c6	v
biblické	biblický	k2eAgFnSc6d1	biblická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasická	klasický	k2eAgFnSc1d1	klasická
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Biblická	biblický	k2eAgFnSc1d1	biblická
hebrejština	hebrejština	k1gFnSc1	hebrejština
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
spisovnou	spisovný	k2eAgFnSc7d1	spisovná
formou	forma	k1gFnSc7	forma
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgInPc3d1	předchozí
textům	text	k1gInPc3	text
se	se	k3xPyFc4	se
biblický	biblický	k2eAgInSc1d1	biblický
text	text	k1gInSc1	text
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
používáním	používání	k1gNnSc7	používání
hlásek	hlásek	k1gInSc1	hlásek
alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
jod	jod	k1gInSc1	jod
<g/>
,	,	kIx,	,
vav	vav	k?	vav
a	a	k8xC	a
he	he	k0	he
jako	jako	k8xC	jako
podkladů	podklad	k1gInPc2	podklad
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
podklady	podklad	k1gInPc4	podklad
užívány	užíván	k2eAgInPc4d1	užíván
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
prodloužení	prodloužení	k1gNnSc4	prodloužení
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
rovněž	rovněž	k6eAd1	rovněž
např.	např.	kA	např.
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
díky	díky	k7c3	díky
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgInS	být
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
text	text	k1gInSc1	text
kanonizován	kanonizován	k2eAgInSc1d1	kanonizován
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
různých	různý	k2eAgNnPc2d1	různé
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jednodušším	jednoduchý	k2eAgFnPc3d2	jednodušší
a	a	k8xC	a
obvyklejším	obvyklý	k2eAgFnPc3d2	obvyklejší
variantám	varianta	k1gFnPc3	varianta
(	(	kIx(	(
<g/>
nahrazování	nahrazování	k1gNnSc2	nahrazování
písmena	písmeno	k1gNnSc2	písmeno
sin	sin	kA	sin
písmenem	písmeno	k1gNnSc7	písmeno
samech	samech	k1gInSc1	samech
<g/>
,	,	kIx,	,
nahrazování	nahrazování	k1gNnSc1	nahrazování
méně	málo	k6eAd2	málo
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
ת	ת	k?	ת
<g/>
,	,	kIx,	,
tachteni	tachten	k2eAgMnPc1d1	tachten
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pode	pod	k7c7	pod
mnou	já	k3xPp1nSc7	já
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
obvyklejšími	obvyklý	k2eAgInPc7d2	obvyklejší
a	a	k8xC	a
jednoduššími	jednoduchý	k2eAgInPc7d2	jednodušší
(	(	kIx(	(
<g/>
ת	ת	k?	ת
<g/>
,	,	kIx,	,
tachtaj	tachtaj	k1gMnSc1	tachtaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
domněnek	domněnka	k1gFnPc2	domněnka
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
změněna	změnit	k5eAaPmNgFnS	změnit
pozdější	pozdní	k2eAgFnSc7d2	pozdější
masoretskou	masoretský	k2eAgFnSc7d1	masoretský
redakcí	redakce	k1gFnSc7	redakce
starozákonního	starozákonní	k2eAgInSc2d1	starozákonní
kánonu	kánon	k1gInSc2	kánon
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
nejdříve	dříve	k6eAd3	dříve
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
r.	r.	kA	r.
100	[number]	k4	100
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Tóry	tóra	k1gFnSc2	tóra
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
masoretským	masoretský	k2eAgInSc7d1	masoretský
textem	text	k1gInSc7	text
a	a	k8xC	a
samaritánským	samaritánský	k2eAgInSc7d1	samaritánský
pentateuchem	pentateuch	k1gInSc7	pentateuch
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
spíše	spíše	k9	spíše
teorii	teorie	k1gFnSc4	teorie
o	o	k7c4	o
původně	původně	k6eAd1	původně
jednotně	jednotně	k6eAd1	jednotně
redigovaném	redigovaný	k2eAgInSc6d1	redigovaný
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
starším	starý	k2eAgInSc6d2	starší
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vývoji	vývoj	k1gInSc6	vývoj
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starší	starý	k2eAgFnSc2d2	starší
starobabylónštiny	starobabylónština	k1gFnSc2	starobabylónština
např.	např.	kA	např.
ztratila	ztratit	k5eAaPmAgFnS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
skloňovat	skloňovat	k5eAaImF	skloňovat
pomocí	pomocí	k7c2	pomocí
pádových	pádový	k2eAgFnPc2d1	pádová
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
zjednodušil	zjednodušit	k5eAaPmAgInS	zjednodušit
i	i	k9	i
systém	systém	k1gInSc1	systém
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
skloňování	skloňování	k1gNnPc4	skloňování
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k8xC	i
vzhledem	vzhledem	k7c3	vzhledem
např.	např.	kA	např.
k	k	k7c3	k
arabštině	arabština	k1gFnSc3	arabština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
spisovné	spisovný	k2eAgFnSc6d1	spisovná
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
zachovala	zachovat	k5eAaPmAgFnS	zachovat
skloňování	skloňování	k1gNnSc4	skloňování
a	a	k8xC	a
také	také	k9	také
starší	starý	k2eAgFnSc4d2	starší
vokalizaci	vokalizace	k1gFnSc4	vokalizace
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
kořen	kořen	k1gInSc1	kořen
slovesa	sloveso	k1gNnSc2	sloveso
určený	určený	k2eAgInSc1d1	určený
většinou	většinou	k6eAd1	většinou
třemi	tři	k4xCgFnPc7	tři
souhláskami	souhláska	k1gFnPc7	souhláska
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
čtyřmi	čtyři	k4xCgNnPc7	čtyři
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgFnPc7	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
tvar	tvar	k1gInSc1	tvar
slovesa	sloveso	k1gNnSc2	sloveso
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
sg.	sg.	k?	sg.
m.	m.	k?	m.
perfekta	perfektum	k1gNnSc2	perfektum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
prostému	prostý	k2eAgInSc3d1	prostý
kořeni	kořen	k1gInSc3	kořen
bez	bez	k7c2	bez
přípon	přípona	k1gFnPc2	přípona
a	a	k8xC	a
předpon	předpona	k1gFnPc2	předpona
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
časuje	časovat	k5eAaBmIp3nS	časovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
také	také	k9	také
většinou	většinou	k6eAd1	většinou
uveden	uvést	k5eAaPmNgInS	uvést
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
jako	jako	k8xC	jako
základní	základní	k2eAgFnPc4d1	základní
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
např.	např.	kA	např.
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
základní	základní	k2eAgInSc1d1	základní
tvar	tvar	k1gInSc1	tvar
užívá	užívat	k5eAaImIp3nS	užívat
infinitiv	infinitiv	k1gInSc4	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
trojpísmenného	trojpísmenný	k2eAgInSc2d1	trojpísmenný
kořene	kořen	k1gInSc2	kořen
je	být	k5eAaImIp3nS	být
také	také	k9	také
odvozena	odvodit	k5eAaPmNgFnS	odvodit
většina	většina	k1gFnSc1	většina
hebrejských	hebrejský	k2eAgNnPc2d1	hebrejské
substantiv	substantivum	k1gNnPc2	substantivum
a	a	k8xC	a
adjektiv	adjektivum	k1gNnPc2	adjektivum
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
nezná	neznat	k5eAaImIp3nS	neznat
české	český	k2eAgNnSc1d1	české
(	(	kIx(	(
<g/>
či	či	k8xC	či
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
indoevropských	indoevropský	k2eAgInPc6d1	indoevropský
jazycích	jazyk	k1gInPc6	jazyk
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
)	)	kIx)	)
pojetí	pojetí	k1gNnSc1	pojetí
slovesných	slovesný	k2eAgInPc2d1	slovesný
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biblické	biblický	k2eAgFnSc6d1	biblická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
neexistuje	existovat	k5eNaImIp3nS	existovat
přítomný	přítomný	k2eAgInSc4d1	přítomný
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
nebo	nebo	k8xC	nebo
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
v	v	k7c6	v
absolutním	absolutní	k2eAgInSc6d1	absolutní
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biblické	biblický	k2eAgFnSc6d1	biblická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
naopak	naopak	k6eAd1	naopak
existují	existovat	k5eAaImIp3nP	existovat
jiné	jiný	k2eAgFnPc4d1	jiná
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgInPc4d1	vyjadřující
jiné	jiný	k2eAgInPc4d1	jiný
aspekty	aspekt	k1gInPc4	aspekt
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
sloveso	sloveso	k1gNnSc1	sloveso
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
forma	forma	k1gFnSc1	forma
časování	časování	k1gNnSc4	časování
<g/>
:	:	kIx,	:
prefixní	prefixní	k2eAgFnSc1d1	prefixní
-	-	kIx~	-
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
též	též	k9	též
imperfektum	imperfektum	k1gNnSc1	imperfektum
<g/>
,	,	kIx,	,
preformativní	preformativní	k2eAgNnSc1d1	preformativní
či	či	k8xC	či
jednoduše	jednoduše	k6eAd1	jednoduše
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
vzorového	vzorový	k2eAgNnSc2d1	vzorové
slovesa	sloveso	k1gNnSc2	sloveso
Yiqtol	Yiqtola	k1gFnPc2	Yiqtola
<g/>
.	.	kIx.	.
</s>
<s>
Ohýbání	ohýbání	k1gNnSc1	ohýbání
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
<g/>
.	.	kIx.	.
</s>
<s>
Konvenčně	konvenčně	k6eAd1	konvenčně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
překládá	překládat	k5eAaImIp3nS	překládat
budoucím	budoucí	k2eAgInSc7d1	budoucí
časem	čas	k1gInSc7	čas
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
též	též	k9	též
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
י	י	k?	י
jiqtol	jiqtol	k1gInSc1	jiqtol
-	-	kIx~	-
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
zabije	zabít	k5eAaPmIp3nS	zabít
ת	ת	k?	ת
tiqtol	tiqtol	k1gInSc1	tiqtol
-	-	kIx~	-
(	(	kIx(	(
<g/>
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
zabije	zabít	k5eAaPmIp3nS	zabít
ת	ת	k?	ת
tiqtol	tiqtol	k1gInSc1	tiqtol
-	-	kIx~	-
zabiješ	zabít	k5eAaPmIp2nS	zabít
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
ת	ת	k?	ת
tiqtolna	tiqtolna	k1gFnSc1	tiqtolna
-	-	kIx~	-
zabiješ	zabít	k5eAaPmIp2nS	zabít
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
א	א	k?	א
eqtol	eqtol	k1gInSc1	eqtol
-	-	kIx~	-
zabiji	zabít	k5eAaPmIp1nS	zabít
atd.	atd.	kA	atd.
sufixní	sufixní	k2eAgInSc1d1	sufixní
-	-	kIx~	-
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
též	též	k9	též
perfektum	perfektum	k1gNnSc1	perfektum
<g/>
,	,	kIx,	,
aformativní	aformativní	k2eAgNnSc1d1	aformativní
či	či	k8xC	či
jednoduše	jednoduše	k6eAd1	jednoduše
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
vzorového	vzorový	k2eAgNnSc2d1	vzorové
slovesa	sloveso	k1gNnSc2	sloveso
Qatal	Qatal	k1gInSc1	Qatal
<g/>
.	.	kIx.	.
</s>
<s>
Ohýbání	ohýbání	k1gNnSc1	ohýbání
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
pomocí	pomocí	k7c2	pomocí
přípon	přípona	k1gFnPc2	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Konvenčně	konvenčně	k6eAd1	konvenčně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
překládá	překládat	k5eAaImIp3nS	překládat
minulým	minulý	k2eAgInSc7d1	minulý
časem	čas	k1gInSc7	čas
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
též	též	k9	též
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
ק	ק	k?	ק
qatal	qatal	k1gInSc1	qatal
-	-	kIx~	-
zabil	zabít	k5eAaPmAgInS	zabít
ק	ק	k?	ק
qatela	qatel	k1gMnSc2	qatel
-	-	kIx~	-
zabila	zabít	k5eAaPmAgFnS	zabít
ק	ק	k?	ק
qatalta	qatalta	k1gFnSc1	qatalta
-	-	kIx~	-
zabil	zabít	k5eAaPmAgMnS	zabít
jsi	být	k5eAaImIp2nS	být
ק	ק	k?	ק
qatalt	qatalt	k1gInSc1	qatalt
-	-	kIx~	-
zabila	zabít	k5eAaPmAgFnS	zabít
jsi	být	k5eAaImIp2nS	být
ק	ק	k?	ק
qatalti	qatalti	k1gNnSc4	qatalti
-	-	kIx~	-
zabil	zabít	k5eAaPmAgInS	zabít
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
jsem	být	k5eAaImIp1nS	být
atd.	atd.	kA	atd.
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
další	další	k2eAgInPc4d1	další
slovesné	slovesný	k2eAgInPc4d1	slovesný
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
imperativy	imperativ	k1gInPc1	imperativ
<g/>
,	,	kIx,	,
infinitivy	infinitiv	k1gInPc1	infinitiv
<g/>
,	,	kIx,	,
participia	participium	k1gNnSc2	participium
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
imperfektního	imperfektní	k2eAgInSc2d1	imperfektní
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
způsobů	způsob	k1gInPc2	způsob
časování	časování	k1gNnSc2	časování
nenese	nést	k5eNaImIp3nS	nést
samostatný	samostatný	k2eAgInSc4d1	samostatný
význam	význam	k1gInSc4	význam
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
až	až	k9	až
postavením	postavení	k1gNnSc7	postavení
slovesa	sloveso	k1gNnSc2	sloveso
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
forma	forma	k1gFnSc1	forma
ו	ו	k?	ו
weqatal	weqatal	k1gMnSc1	weqatal
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
perfektum	perfektum	k1gNnSc4	perfektum
následující	následující	k2eAgNnSc4d1	následující
hned	hned	k6eAd1	hned
za	za	k7c7	za
spojkou	spojka	k1gFnSc7	spojka
'	'	kIx"	'
<g/>
we	we	k?	we
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
následné	následný	k2eAgNnSc4d1	následné
dění	dění	k1gNnSc4	dění
či	či	k8xC	či
činnost	činnost	k1gFnSc4	činnost
<g/>
;	;	kIx,	;
forma	forma	k1gFnSc1	forma
ו	ו	k?	ו
wejiqtol	wejiqtol	k1gInSc1	wejiqtol
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
imperfektum	imperfektum	k1gNnSc4	imperfektum
následující	následující	k2eAgNnSc4d1	následující
hned	hned	k6eAd1	hned
za	za	k7c7	za
spojkou	spojka	k1gFnSc7	spojka
ו	ו	k?	ו
we	we	k?	we
<g/>
)	)	kIx)	)
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
účel	účel	k1gInSc4	účel
či	či	k8xC	či
přání	přání	k1gNnSc4	přání
a	a	k8xC	a
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
často	často	k6eAd1	často
větou	věta	k1gFnSc7	věta
účelovou	účelový	k2eAgFnSc7d1	účelová
(	(	kIx(	(
<g/>
se	s	k7c7	s
spojkou	spojka	k1gFnSc7	spojka
aby	aby	kYmCp3nP	aby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
zná	znát	k5eAaImIp3nS	znát
také	také	k9	také
tzv.	tzv.	kA	tzv.
narativ	narativ	k1gInSc1	narativ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
formu	forma	k1gFnSc4	forma
ו	ו	k?	ו
wajjiqtol	wajjiqtola	k1gFnPc2	wajjiqtola
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základu	základ	k1gInSc6	základ
stojí	stát	k5eAaImIp3nS	stát
imperfektum	imperfektum	k1gNnSc1	imperfektum
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
vyprávění	vyprávění	k1gNnSc6	vyprávění
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
sekvenci	sekvence	k1gFnSc4	sekvence
událostí	událost	k1gFnPc2	událost
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vyprávění	vyprávění	k1gNnSc2	vyprávění
pak	pak	k6eAd1	pak
větné	větný	k2eAgFnSc2d1	větná
formy	forma	k1gFnSc2	forma
typu	typ	k1gInSc2	typ
we-x-qatal	weatat	k5eAaImAgInS	we-x-qatat
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
spojka	spojka	k1gFnSc1	spojka
ו	ו	k?	ו
we	we	k?	we
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
věty	věta	k1gFnSc2	věta
a	a	k8xC	a
sloveso	sloveso	k1gNnSc4	sloveso
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
doplňující	doplňující	k2eAgFnPc4d1	doplňující
informace	informace	k1gFnPc4	informace
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
dějové	dějový	k2eAgFnSc3d1	dějová
linii	linie	k1gFnSc3	linie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovesné	slovesný	k2eAgInPc1d1	slovesný
způsoby	způsob	k1gInPc1	způsob
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
nevyjadřují	vyjadřovat	k5eNaImIp3nP	vyjadřovat
absolutní	absolutní	k2eAgInSc4d1	absolutní
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
události	událost	k1gFnSc3	událost
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
/	/	kIx~	/
<g/>
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
relativní	relativní	k2eAgInSc4d1	relativní
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
částem	část	k1gFnPc3	část
výpovědi	výpověď	k1gFnSc2	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
nemůže	moct	k5eNaImIp3nS	moct
odvozovat	odvozovat	k5eAaImF	odvozovat
příbuzné	příbuzný	k2eAgInPc4d1	příbuzný
významy	význam	k1gInPc4	význam
slovesa	sloveso	k1gNnSc2	sloveso
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
u	u	k7c2	u
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgInPc4d1	další
semitské	semitský	k2eAgInPc4d1	semitský
jazyky	jazyk	k1gInPc4	jazyk
si	se	k3xPyFc3	se
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
tvořením	tvoření	k1gNnSc7	tvoření
odvozených	odvozený	k2eAgInPc2d1	odvozený
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Odvozování	odvozování	k1gNnSc1	odvozování
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
pomocí	pomocí	k7c2	pomocí
předpon	předpona	k1gFnPc2	předpona
či	či	k8xC	či
zdvojování	zdvojování	k1gNnSc4	zdvojování
kořenových	kořenový	k2eAgFnPc2d1	kořenová
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Biblická	biblický	k2eAgFnSc1d1	biblická
hebrejština	hebrejština	k1gFnSc1	hebrejština
zná	znát	k5eAaImIp3nS	znát
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
základních	základní	k2eAgInPc2d1	základní
slovesných	slovesný	k2eAgInPc2d1	slovesný
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
sporadicky	sporadicky	k6eAd1	sporadicky
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
odvozený	odvozený	k2eAgInSc1d1	odvozený
kmen	kmen	k1gInSc1	kmen
upravuje	upravovat	k5eAaImIp3nS	upravovat
význam	význam	k1gInSc4	význam
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biblické	biblický	k2eAgFnSc6d1	biblická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
význam	význam	k1gInSc1	význam
dá	dát	k5eAaPmIp3nS	dát
často	často	k6eAd1	často
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
nutně	nutně	k6eAd1	nutně
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
konzultovat	konzultovat	k5eAaImF	konzultovat
skutečný	skutečný	k2eAgInSc4d1	skutečný
význam	význam	k1gInSc4	význam
se	s	k7c7	s
slovníkem	slovník	k1gInSc7	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
žádné	žádný	k3yNgNnSc1	žádný
sloveso	sloveso	k1gNnSc1	sloveso
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
možných	možný	k2eAgInPc6d1	možný
kmenech	kmen	k1gInPc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kmeny	kmen	k1gInPc1	kmen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
následující	následující	k2eAgNnSc4d1	následující
<g/>
:	:	kIx,	:
ק	ק	k?	ק
–	–	k?	–
Kal	kal	k1gInSc1	kal
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
פ	פ	k?	פ
Pa	Pa	kA	Pa
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
<g/>
)	)	kIx)	)
-	-	kIx~	-
prostý	prostý	k2eAgInSc4d1	prostý
aktivní	aktivní	k2eAgInSc4d1	aktivní
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
lámal	lámat	k5eAaImAgMnS	lámat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
נ	נ	k?	נ
–	–	k?	–
Nif	Nif	k1gFnPc2	Nif
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
–	–	k?	–
prostý	prostý	k2eAgInSc4d1	prostý
pasivní	pasivní	k2eAgInSc4d1	pasivní
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgInS	být
lámán	lámat	k5eAaImNgInS	lámat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
פ	פ	k?	פ
–	–	k?	–
Pi	pi	k0	pi
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
–	–	k?	–
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
aktivní	aktivní	k2eAgInSc4d1	aktivní
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
zlomil	zlomit	k5eAaPmAgMnS	zlomit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
פ	פ	k?	פ
–	–	k?	–
Pu	Pu	k1gFnPc2	Pu
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
–	–	k?	–
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
pasivní	pasivní	k2eAgInSc1d1	pasivní
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgInS	být
zlomen	zlomen	k2eAgInSc1d1	zlomen
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ה	ה	k?	ה
–	–	k?	–
Hif	Hif	k1gFnSc1	Hif
<g/>
'	'	kIx"	'
<g/>
il	il	k?	il
–	–	k?	–
kauzativní	kauzativní	k2eAgInSc4d1	kauzativní
aktivní	aktivní	k2eAgInSc4d1	aktivní
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zlomil	zlomit	k5eAaPmAgMnS	zlomit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
ה	ה	k?	ה
–	–	k?	–
Hof	Hof	k1gFnPc2	Hof
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
–	–	k?	–
kauzativní	kauzativní	k2eAgInSc4d1	kauzativní
pasivní	pasivní	k2eAgInSc4d1	pasivní
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
zlomit	zlomit	k5eAaPmF	zlomit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
ה	ה	k?	ה
–	–	k?	–
Hitpa	Hitpa	k1gFnSc1	Hitpa
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
–	–	k?	–
reflexní	reflexní	k2eAgInSc1d1	reflexní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
zvratný	zvratný	k2eAgInSc4d1	zvratný
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
zlomil	zlomit	k5eAaPmAgMnS	zlomit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
ale	ale	k9	ale
také	také	k9	také
pozabijel	pozabijet	k5eAaPmAgMnS	pozabijet
si	se	k3xPyFc3	se
či	či	k8xC	či
zabil	zabít	k5eAaPmAgMnS	zabít
se	se	k3xPyFc4	se
Názvy	název	k1gInPc7	název
odvozených	odvozený	k2eAgInPc2d1	odvozený
kmenů	kmen	k1gInPc2	kmen
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
tvarů	tvar	k1gInPc2	tvar
slovesa	sloveso	k1gNnSc2	sloveso
פ	פ	k?	פ
pa	pa	k0	pa
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
(	(	kIx(	(
<g/>
dělat	dělat	k5eAaImF	dělat
<g/>
)	)	kIx)	)
v	v	k7c6	v
patřičném	patřičný	k2eAgInSc6d1	patřičný
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
každého	každý	k3xTgMnSc2	každý
takového	takový	k3xDgInSc2	takový
kmene	kmen	k1gInSc2	kmen
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
všechny	všechen	k3xTgInPc4	všechen
další	další	k2eAgInPc4d1	další
gramatické	gramatický	k2eAgInPc4d1	gramatický
tvary	tvar	k1gInPc4	tvar
<g/>
:	:	kIx,	:
imperfekta	imperfektum	k1gNnSc2	imperfektum
<g/>
,	,	kIx,	,
perfekta	perfektum	k1gNnSc2	perfektum
<g/>
,	,	kIx,	,
imperativy	imperativ	k1gInPc1	imperativ
<g/>
,	,	kIx,	,
infinitivy	infinitiv	k1gInPc1	infinitiv
<g/>
,	,	kIx,	,
participia	participium	k1gNnPc1	participium
atd.	atd.	kA	atd.
Osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
formě	forma	k1gFnSc6	forma
existují	existovat	k5eAaImIp3nP	existovat
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
pozdní	pozdní	k2eAgFnPc4d1	pozdní
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
jen	jen	k9	jen
jako	jako	k8xS	jako
vyjádření	vyjádření	k1gNnSc1	vyjádření
podmětu	podmět	k1gInSc2	podmět
věty	věta	k1gFnSc2	věta
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
tomu	ten	k3xDgMnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zájmenných	zájmenný	k2eAgInPc2d1	zájmenný
sufixů	sufix	k1gInPc2	sufix
připojovaných	připojovaný	k2eAgInPc2d1	připojovaný
za	za	k7c2	za
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
substantiva	substantivum	k1gNnPc1	substantivum
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnPc1	předložka
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
atd.	atd.	kA	atd.
Přivlastňovací	přivlastňovací	k2eAgFnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
jako	jako	k9	jako
taková	takový	k3xDgNnPc4	takový
proto	proto	k6eAd1	proto
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
zájmena	zájmeno	k1gNnSc2	zájmeno
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
obvykle	obvykle	k6eAd1	obvykle
rod	rod	k1gInSc4	rod
(	(	kIx(	(
<g/>
m.	m.	k?	m.
<g/>
,	,	kIx,	,
ž.	ž.	k?	ž.
<g/>
)	)	kIx)	)
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
j	j	k?	j
č.	č.	k?	č.
<g/>
,	,	kIx,	,
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
větné	větný	k2eAgFnSc6d1	větná
syntaxi	syntax	k1gFnSc6	syntax
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
probíhá	probíhat	k5eAaImIp3nS	probíhat
skloňování	skloňování	k1gNnSc4	skloňování
substantiv	substantivum	k1gNnPc2	substantivum
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
skloňování	skloňování	k1gNnSc1	skloňování
pomocí	pomoc	k1gFnPc2	pomoc
koncovek	koncovka	k1gFnPc2	koncovka
biblická	biblický	k2eAgFnSc1d1	biblická
hebrejština	hebrejština	k1gFnSc1	hebrejština
již	již	k6eAd1	již
nezná	neznat	k5eAaImIp3nS	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Větná	větný	k2eAgFnSc1d1	větná
struktura	struktura	k1gFnSc1	struktura
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
nezná	znát	k5eNaImIp3nS	znát
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
vět	věta	k1gFnPc2	věta
ani	ani	k8xC	ani
pomocných	pomocný	k2eAgNnPc2d1	pomocné
sloves	sloveso	k1gNnPc2	sloveso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
využívá	využívat	k5eAaPmIp3nS	využívat
hojně	hojně	k6eAd1	hojně
jmenných	jmenný	k2eAgInPc2d1	jmenný
tvarů	tvar	k1gInPc2	tvar
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
biblickou	biblický	k2eAgFnSc4d1	biblická
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
<g/>
,	,	kIx,	,
než	než	k8xS	než
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
vav	vav	k?	vav
conversivum	conversivum	k1gInSc1	conversivum
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
konsekutivní	konsekutivní	k2eAgInSc1d1	konsekutivní
vav	vav	k?	vav
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
využívaný	využívaný	k2eAgInSc4d1	využívaný
při	při	k7c6	při
souvislém	souvislý	k2eAgNnSc6d1	souvislé
vyprávění	vyprávění	k1gNnSc6	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
všude	všude	k6eAd1	všude
parataxi	parataxe	k1gFnSc4	parataxe
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
souřadná	souřadný	k2eAgNnPc1d1	souřadné
souvětí	souvětí	k1gNnPc1	souvětí
spojené	spojený	k2eAgInPc4d1	spojený
spojkou	spojka	k1gFnSc7	spojka
vav	vav	k?	vav
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
slučovacím	slučovací	k2eAgMnSc7d1	slučovací
i	i	k8xC	i
vylučovacím	vylučovací	k2eAgInSc7d1	vylučovací
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
prostého	prostý	k2eAgInSc2d1	prostý
sledu	sled	k1gInSc2	sled
událostí	událost	k1gFnPc2	událost
vyprávění	vyprávění	k1gNnSc2	vyprávění
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloveso	sloveso	k1gNnSc1	sloveso
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
předchází	předcházet	k5eAaImIp3nS	předcházet
tato	tento	k3xDgFnSc1	tento
spojka	spojka	k1gFnSc1	spojka
"	"	kIx"	"
<g/>
vav	vav	k?	vav
<g/>
"	"	kIx"	"
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
čas	čas	k1gInSc4	čas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tzn.	tzn.	kA	tzn.
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
toto	tento	k3xDgNnSc4	tento
sloveso	sloveso	k1gNnSc4	sloveso
v	v	k7c6	v
perfektu	perfektum	k1gNnSc6	perfektum
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
imperfekta	imperfektum	k1gNnSc2	imperfektum
a	a	k8xC	a
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
třetí	třetí	k4xOgFnSc2	třetí
slovesné	slovesný	k2eAgFnSc2d1	slovesná
formy	forma	k1gFnSc2	forma
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
konjunktivu	konjunktiva	k1gFnSc4	konjunktiva
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
předložce	předložka	k1gFnSc6	předložka
"	"	kIx"	"
<g/>
vav	vav	k?	vav
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
splynula	splynout	k5eAaPmAgFnS	splynout
s	s	k7c7	s
imperfektem	imperfektum	k1gNnSc7	imperfektum
<g/>
.	.	kIx.	.
</s>
<s>
Známkou	známka	k1gFnSc7	známka
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
sufixy	sufix	k1gInPc1	sufix
a	a	k8xC	a
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
sloveso	sloveso	k1gNnSc1	sloveso
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
konsekutivním	konsekutivní	k2eAgNnSc7d1	konsekutivní
vav	vav	k?	vav
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
vlivů	vliv	k1gInPc2	vliv
starších	starý	k2eAgInPc2d2	starší
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
především	především	k9	především
akkadštiny	akkadština	k1gFnSc2	akkadština
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
jev	jev	k1gInSc4	jev
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
východosemitské	východosemitský	k2eAgInPc4d1	východosemitský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
v	v	k7c6	v
západosemitské	západosemitský	k2eAgFnSc6d1	západosemitský
biblické	biblický	k2eAgFnSc6d1	biblická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Masoretský	Masoretský	k2eAgInSc4d1	Masoretský
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
masoretské	masoretský	k2eAgFnSc2d1	masoretský
redakce	redakce	k1gFnSc2	redakce
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
některých	některý	k3yIgNnPc2	některý
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
zájmenných	zájmenný	k2eAgFnPc2d1	zájmenná
přípon	přípona	k1gFnPc2	přípona
(	(	kIx(	(
<g/>
např.	např.	kA	např.
původní	původní	k2eAgInSc1d1	původní
א	א	k?	א
ati	ati	k?	ati
(	(	kIx(	(
<g/>
ty	ty	k0	ty
<g/>
,	,	kIx,	,
f.	f.	k?	f.
<g/>
)	)	kIx)	)
na	na	k7c6	na
א	א	k?	א
at	at	k?	at
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
aramejštiny	aramejština	k1gFnSc2	aramejština
<g/>
)	)	kIx)	)
a	a	k8xC	a
ustanovením	ustanovení	k1gNnSc7	ustanovení
tzv.	tzv.	kA	tzv.
ketiv	ketiva	k1gFnPc2	ketiva
(	(	kIx(	(
<g/>
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
:	:	kIx,	:
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
psáno	psán	k2eAgNnSc1d1	psáno
<g/>
)	)	kIx)	)
a	a	k8xC	a
kere	kere	k6eAd1	kere
(	(	kIx(	(
<g/>
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
:	:	kIx,	:
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
číst	číst	k5eAaImF	číst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
kere	kere	k6eAd1	kere
starší	starý	k2eAgInSc4d2	starší
tvar	tvar	k1gInSc4	tvar
vyslovování	vyslovování	k1gNnSc2	vyslovování
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
přednost	přednost	k1gFnSc1	přednost
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
starší	starý	k2eAgInSc1d2	starší
tvar	tvar	k1gInSc1	tvar
zachován	zachovat	k5eAaPmNgInS	zachovat
v	v	k7c4	v
ketiv	ketiv	k6eAd1	ketiv
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
abeceda	abeceda	k1gFnSc1	abeceda
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
určitou	určitý	k2eAgFnSc4d1	určitá
redukci	redukce	k1gFnSc4	redukce
konsonantů	konsonant	k1gInPc2	konsonant
oproti	oproti	k7c3	oproti
protosemitským	protosemitský	k2eAgInPc3d1	protosemitský
textům	text	k1gInPc3	text
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
konsonantů	konsonant	k1gInPc2	konsonant
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
původní	původní	k2eAgFnSc6d1	původní
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
dodnes	dodnes	k6eAd1	dodnes
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
z	z	k7c2	z
představuje	představovat	k5eAaImIp3nS	představovat
původní	původní	k2eAgNnSc1d1	původní
z	z	k7c2	z
i	i	k9	i
dz	dz	k?	dz
<g/>
,	,	kIx,	,
hebrejské	hebrejský	k2eAgFnPc1d1	hebrejská
c	c	k0	c
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
emfatické	emfatický	k2eAgInPc1d1	emfatický
s	s	k7c7	s
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
emfatické	emfatický	k2eAgFnPc4d1	emfatická
hlásky	hláska	k1gFnPc4	hláska
d	d	k?	d
<g/>
,	,	kIx,	,
z	z	k7c2	z
i	i	k8xC	i
s	s	k7c7	s
<g/>
,	,	kIx,	,
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
ajin	ajin	k1gInSc1	ajin
představuje	představovat	k5eAaImIp3nS	představovat
jak	jak	k6eAd1	jak
původní	původní	k2eAgInSc1d1	původní
ajn	ajn	k?	ajn
tak	tak	k6eAd1	tak
i	i	k9	i
ghajn	ghajn	k1gMnSc1	ghajn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
např.	např.	kA	např.
písmeno	písmeno	k1gNnSc1	písmeno
šin	šin	k?	šin
se	se	k3xPyFc4	se
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
na	na	k7c6	na
šin	šin	k?	šin
a	a	k8xC	a
sin	sin	kA	sin
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
příběh	příběh	k1gInSc1	příběh
ze	z	k7c2	z
Soudců	soudce	k1gMnPc2	soudce
12	[number]	k4	12
<g/>
,	,	kIx,	,
6	[number]	k4	6
(	(	kIx(	(
<g/>
sibolet	sibolet	k1gInSc4	sibolet
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgNnSc3d1	běžné
šibolet	šibolet	k1gInSc1	šibolet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
také	také	k9	také
jediný	jediný	k2eAgInSc1d1	jediný
záznam	záznam	k1gInSc1	záznam
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
různých	různý	k2eAgInPc2d1	různý
dialektů	dialekt	k1gInPc2	dialekt
v	v	k7c6	v
biblickém	biblický	k2eAgNnSc6d1	biblické
období	období	k1gNnSc6	období
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgNnSc1d1	definitivní
rozdělení	rozdělení	k1gNnSc1	rozdělení
mezi	mezi	k7c7	mezi
šin	šin	k?	šin
a	a	k8xC	a
sin	sin	kA	sin
bylo	být	k5eAaImAgNnS	být
provedeno	proveden	k2eAgNnSc1d1	provedeno
až	až	k9	až
během	během	k7c2	během
tiberiadské	tiberiadský	k2eAgFnSc2d1	tiberiadský
vokalizace	vokalizace	k1gFnSc2	vokalizace
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
zdvojování	zdvojování	k1gNnSc2	zdvojování
hrdelnic	hrdelnice	k1gFnPc2	hrdelnice
(	(	kIx(	(
<g/>
laryngál	laryngála	k1gFnPc2	laryngála
a	a	k8xC	a
faryngál	faryngála	k1gFnPc2	faryngála
<g/>
)	)	kIx)	)
a	a	k8xC	a
posledních	poslední	k2eAgInPc2d1	poslední
konsonantů	konsonant	k1gInPc2	konsonant
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
výslovnost	výslovnost	k1gFnSc1	výslovnost
vokálů	vokál	k1gInPc2	vokál
nebyla	být	k5eNaImAgFnS	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
<g/>
,	,	kIx,	,
v	v	k7c6	v
babylónské	babylónský	k2eAgFnSc6d1	Babylónská
výslovnosti	výslovnost	k1gFnSc6	výslovnost
neexistoval	existovat	k5eNaImAgInS	existovat
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
cere	cer	k1gInSc5	cer
a	a	k8xC	a
segol	segola	k1gFnPc2	segola
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
neexistovala	existovat	k5eNaImAgFnS	existovat
jednotná	jednotný	k2eAgFnSc1d1	jednotná
výslovnost	výslovnost	k1gFnSc1	výslovnost
vokalizačního	vokalizační	k2eAgNnSc2d1	vokalizační
znaménka	znaménko	k1gNnSc2	znaménko
kamac	kamac	k1gFnSc1	kamac
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
obecně	obecně	k6eAd1	obecně
vyslovovaného	vyslovovaný	k2eAgInSc2d1	vyslovovaný
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
rovněž	rovněž	k9	rovněž
i	i	k9	i
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
o	o	k0	o
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
tvary	tvar	k1gInPc1	tvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
tvary	tvar	k1gInPc7	tvar
jednoduššími	jednoduchý	k2eAgInPc7d2	jednodušší
nebo	nebo	k8xC	nebo
arameizovanými	arameizovaný	k2eAgInPc7d1	arameizovaný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
ve	v	k7c6	v
svitcích	svitek	k1gInPc6	svitek
nalezených	nalezený	k2eAgFnPc2d1	nalezená
v	v	k7c6	v
Kumránu	Kumrán	k1gInSc6	Kumrán
u	u	k7c2	u
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Svitky	svitek	k1gInPc1	svitek
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
pozoruhodné	pozoruhodný	k2eAgFnPc4d1	pozoruhodná
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc7d2	veliký
výskytem	výskyt	k1gInSc7	výskyt
písmene	písmeno	k1gNnSc2	písmeno
sin	sin	kA	sin
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
samech	samech	k1gInSc4	samech
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
masoretském	masoretský	k2eAgInSc6d1	masoretský
biblickém	biblický	k2eAgInSc6d1	biblický
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
odlišném	odlišný	k2eAgInSc6d1	odlišný
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
biblickém	biblický	k2eAgInSc6d1	biblický
textu	text	k1gInSc6	text
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
velekněz	velekněz	k1gMnSc1	velekněz
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
kohen	kohen	k2eAgMnSc1d1	kohen
ha-roš	haoš	k1gMnSc1	ha-roš
namísto	namísto	k7c2	namísto
běžného	běžný	k2eAgMnSc2d1	běžný
kohen	kohen	k2eAgInSc4d1	kohen
gadol	gadol	k1gInSc4	gadol
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
odlišný	odlišný	k2eAgInSc1d1	odlišný
je	být	k5eAaImIp3nS	být
i	i	k9	i
způsob	způsob	k1gInSc1	způsob
vokalizace	vokalizace	k1gFnSc2	vokalizace
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc4	systém
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
laryngál	laryngála	k1gFnPc2	laryngála
je	být	k5eAaImIp3nS	být
oslabena	oslabit	k5eAaPmNgFnS	oslabit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
vypadnou	vypadnout	k5eAaPmIp3nP	vypadnout
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
úplně	úplně	k6eAd1	úplně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
alef	alef	k1gInSc1	alef
a	a	k8xC	a
ajin	ajin	k1gInSc1	ajin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
(	(	kIx(	(
<g/>
chet	chet	k1gInSc1	chet
je	být	k5eAaImIp3nS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
he	he	k0	he
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
mj.	mj.	kA	mj.
na	na	k7c6	na
informacích	informace	k1gFnPc6	informace
z	z	k7c2	z
Talmudu	talmud	k1gInSc2	talmud
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
Židů	Žid	k1gMnPc2	Žid
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
konzonanty	konzonanta	k1gFnSc2	konzonanta
jako	jako	k9	jako
ajin	ajin	k1gMnSc1	ajin
nebo	nebo	k8xC	nebo
chet	chet	k1gMnSc1	chet
řádně	řádně	k6eAd1	řádně
vyslovit	vyslovit	k5eAaPmF	vyslovit
a	a	k8xC	a
vynechávali	vynechávat	k5eAaImAgMnP	vynechávat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pisatelé	pisatel	k1gMnPc1	pisatel
svitků	svitek	k1gInPc2	svitek
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
svitků	svitek	k1gInPc2	svitek
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
slepou	slepý	k2eAgFnSc4d1	slepá
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
větev	větev	k1gFnSc4	větev
biblické	biblický	k2eAgFnSc2d1	biblická
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
Mišny	Mišna	k1gFnSc2	Mišna
a	a	k8xC	a
Gemary	Gemara	k1gFnSc2	Gemara
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
vývojovým	vývojový	k2eAgInSc7d1	vývojový
stupněm	stupeň	k1gInSc7	stupeň
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
období	období	k1gNnSc3	období
druhého	druhý	k4xOgInSc2	druhý
Chrámu	chrám	k1gInSc2	chrám
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
aramejštiny	aramejština	k1gFnSc2	aramejština
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
hlavního	hlavní	k2eAgInSc2d1	hlavní
dorozumívacího	dorozumívací	k2eAgInSc2d1	dorozumívací
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
perštiny	perština	k1gFnSc2	perština
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
především	především	k9	především
arameizovanými	arameizovaný	k2eAgInPc7d1	arameizovaný
tvary	tvar	k1gInPc7	tvar
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
(	(	kIx(	(
<g/>
י	י	k?	י
-in	n	k?	-in
namísto	namísto	k7c2	namísto
י	י	k?	י
–	–	k?	–
<g/>
im	im	k?	im
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používáním	používání	k1gNnSc7	používání
arameizovaných	arameizovaný	k2eAgMnPc2d1	arameizovaný
tvarů	tvar	k1gInPc2	tvar
zájmen	zájmeno	k1gNnPc2	zájmeno
(	(	kIx(	(
<g/>
א	א	k?	א
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
oproti	oproti	k7c3	oproti
א	א	k?	א
<g/>
,	,	kIx,	,
anochi	anochi	k6eAd1	anochi
–	–	k?	–
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
א	א	k?	א
<g/>
,	,	kIx,	,
anu	anu	k?	anu
oproti	oproti	k7c3	oproti
א	א	k?	א
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
anachnu	anachnout	k5eAaPmIp1nS	anachnout
–	–	k?	–
"	"	kIx"	"
<g/>
my	my	k3xPp1nPc1	my
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkracováním	zkracování	k1gNnSc7	zkracování
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
י	י	k?	י
<g/>
,	,	kIx,	,
jehe	jehat	k5eAaPmIp3nS	jehat
oproti	oproti	k7c3	oproti
י	י	k?	י
<g/>
,	,	kIx,	,
jihje	jihje	k1gFnSc1	jihje
–	–	k?	–
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
frekvencí	frekvence	k1gFnSc7	frekvence
užívání	užívání	k1gNnSc2	užívání
tvarů	tvar	k1gInPc2	tvar
slovesných	slovesný	k2eAgNnPc2d1	slovesné
participií	participium	k1gNnPc2	participium
(	(	kIx(	(
<g/>
která	který	k3yIgNnPc1	který
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
přítomný	přítomný	k2eAgInSc4d1	přítomný
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
je	být	k5eAaImIp3nS	být
obohacena	obohatit	k5eAaPmNgFnS	obohatit
o	o	k7c4	o
výpůjčky	výpůjčka	k1gFnPc4	výpůjčka
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
–	–	k?	–
א	א	k?	א
(	(	kIx(	(
<g/>
aspaklarja	aspaklarja	k1gFnSc1	aspaklarja
<g/>
,	,	kIx,	,
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
מ	מ	k?	מ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ה	ה	k?	ה
(	(	kIx(	(
<g/>
hediot	hediot	k1gMnSc1	hediot
<g/>
,	,	kIx,	,
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ι	ι	k?	ι
idiotés	idiotés	k1gInSc1	idiotés
<g/>
,	,	kIx,	,
idiot	idiot	k1gMnSc1	idiot
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
nezkušený	zkušený	k2eNgMnSc1d1	nezkušený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
amatér	amatér	k1gMnSc1	amatér
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
opak	opak	k1gInSc1	opak
experta	expert	k1gMnSc2	expert
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
oboru	obor	k1gInSc6	obor
<g/>
)	)	kIx)	)
א	א	k?	א
(	(	kIx(	(
<g/>
avir	avir	k1gInSc1	avir
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
aer	aero	k1gNnPc2	aero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
výrazů	výraz	k1gInPc2	výraz
slouží	sloužit	k5eAaImIp3nS	sloužit
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
slovních	slovní	k2eAgFnPc2d1	slovní
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
hebrejštině	hebrejština	k1gFnSc6	hebrejština
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
rodů	rod	k1gInPc2	rod
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
z	z	k7c2	z
maskulina	maskulinum	k1gNnSc2	maskulinum
na	na	k7c4	na
femininum	femininum	k1gNnSc4	femininum
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
ryze	ryze	k6eAd1	ryze
abstraktní	abstraktní	k2eAgInSc1d1	abstraktní
význam	význam	k1gInSc1	význam
v	v	k7c6	v
biblické	biblický	k2eAgFnSc6d1	biblická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
(	(	kIx(	(
<g/>
צ	צ	k?	צ
cdaka	cdaka	k1gFnSc1	cdaka
<g/>
,	,	kIx,	,
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c4	v
mišnické	mišnický	k2eAgInPc4d1	mišnický
význam	význam	k1gInSc4	význam
konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
(	(	kIx(	(
<g/>
charita	charita	k1gFnSc1	charita
<g/>
,	,	kIx,	,
dobročinnost	dobročinnost	k1gFnSc1	dobročinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntax	syntax	k1gFnSc1	syntax
mišnické	mišnický	k2eAgFnSc2d1	mišnický
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
než	než	k8xS	než
u	u	k7c2	u
biblické	biblický	k2eAgFnSc2d1	biblická
<g/>
,	,	kIx,	,
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
konsekutivní	konsekutivní	k2eAgMnPc1d1	konsekutivní
vav	vav	k?	vav
<g/>
,	,	kIx,	,
tvary	tvar	k1gInPc1	tvar
i	i	k8xC	i
věty	věta	k1gFnPc1	věta
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
zkracovány	zkracován	k2eAgFnPc1d1	zkracována
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
hebrejština	hebrejština	k1gFnSc1	hebrejština
Mišny	Mišna	k1gFnSc2	Mišna
více	hodně	k6eAd2	hodně
stavbou	stavba	k1gFnSc7	stavba
věty	věta	k1gFnSc2	věta
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
moderní	moderní	k2eAgInSc1d1	moderní
hebrejštině	hebrejština	k1gFnSc3	hebrejština
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
částečně	částečně	k6eAd1	částečně
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
čistě	čistě	k6eAd1	čistě
mišnické	mišnický	k2eAgFnPc4d1	mišnický
formulace	formulace	k1gFnPc4	formulace
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
velmi	velmi	k6eAd1	velmi
řídké	řídký	k2eAgNnSc1d1	řídké
užívání	užívání	k1gNnSc1	užívání
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
nevyjádřena	vyjádřit	k5eNaPmNgFnS	vyjádřit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
slovesného	slovesný	k2eAgInSc2d1	slovesný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
slovesné	slovesný	k2eAgInPc4d1	slovesný
tvary	tvar	k1gInPc4	tvar
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
pro	pro	k7c4	pro
mišnickou	mišnický	k2eAgFnSc4d1	mišnický
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
patří	patřit	k5eAaImIp3nS	patřit
kombinace	kombinace	k1gFnSc1	kombinace
slovesa	sloveso	k1gNnSc2	sloveso
v	v	k7c6	v
perfektu	perfektum	k1gNnSc6	perfektum
a	a	k8xC	a
participiu	participium	k1gNnSc6	participium
-	-	kIx~	-
např.	např.	kA	např.
ה	ה	k?	ה
ה	ה	k?	ה
א	א	k?	א
(	(	kIx(	(
<g/>
hu	hu	k0	hu
haja	hajum	k1gNnPc1	hajum
omer	omer	k1gMnSc1	omer
<g/>
,	,	kIx,	,
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgInS	být
říkající	říkající	k2eAgMnSc1d1	říkající
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgMnS	být
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
říkat	říkat	k5eAaImF	říkat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
říkával	říkávat	k5eAaImAgMnS	říkávat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
užívání	užívání	k1gNnSc4	užívání
vazby	vazba	k1gFnSc2	vazba
י	י	k?	י
מ	מ	k?	מ
(	(	kIx(	(
<g/>
joter	joter	k1gMnSc1	joter
mi-	mi-	k?	mi-
více	hodně	k6eAd2	hodně
než	než	k8xS	než
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
jednodušší	jednoduchý	k2eAgFnSc3d2	jednodušší
biblické	biblický	k2eAgFnSc3d1	biblická
vazbě	vazba	k1gFnSc3	vazba
vyjádřené	vyjádřený	k2eAgFnPc1d1	vyjádřená
pouze	pouze	k6eAd1	pouze
מ	מ	k?	מ
mi	já	k3xPp1nSc3	já
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
Vliv	vliv	k1gInSc1	vliv
mišnické	mišnický	k2eAgFnSc2d1	mišnický
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
již	již	k6eAd1	již
nejmladších	mladý	k2eAgInPc6d3	nejmladší
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
Ester	Ester	k1gFnSc2	Ester
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnSc2	kniha
Kronik	kronika	k1gFnPc2	kronika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
Šim	Šim	k1gFnSc2	Šim
<g/>
'	'	kIx"	'
<g/>
ona	onen	k3xDgFnSc1	onen
bar	bar	k1gInSc1	bar
Kochby	Kochba	k1gFnPc1	Kochba
a	a	k8xC	a
pochopitelně	pochopitelně	k6eAd1	pochopitelně
i	i	k9	i
v	v	k7c6	v
Mišně	Mišna	k1gFnSc6	Mišna
<g/>
,	,	kIx,	,
Gemaře	Gemara	k1gFnSc6	Gemara
<g/>
,	,	kIx,	,
Toseftě	Tosefto	k1gNnSc6	Tosefto
a	a	k8xC	a
Midraších	Midrach	k1gInPc6	Midrach
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
zničení	zničení	k1gNnSc2	zničení
Chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
r.	r.	kA	r.
70	[number]	k4	70
nebyla	být	k5eNaImAgFnS	být
užívána	užíván	k2eAgFnSc1d1	užívána
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
biblických	biblický	k2eAgInPc2d1	biblický
textů	text	k1gInPc2	text
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
psaný	psaný	k2eAgInSc4d1	psaný
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
(	(	kIx(	(
<g/>
kodifikace	kodifikace	k1gFnSc1	kodifikace
textu	text	k1gInSc2	text
Talmudu	talmud	k1gInSc2	talmud
<g/>
)	)	kIx)	)
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
používána	používat	k5eAaImNgFnS	používat
jako	jako	k8xS	jako
jazyk	jazyk	k1gInSc1	jazyk
mluvený	mluvený	k2eAgInSc1d1	mluvený
<g/>
.	.	kIx.	.
</s>
<s>
Mišnickou	Mišnický	k2eAgFnSc4d1	Mišnický
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
Jazyk	jazyk	k1gInSc1	jazyk
tanaitů	tanaita	k1gMnPc2	tanaita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odlišnosti	odlišnost	k1gFnPc1	odlišnost
ve	v	k7c6	v
vokalizaci	vokalizace	k1gFnSc6	vokalizace
vyhrazuje	vyhrazovat	k5eAaImIp3nS	vyhrazovat
palestinská	palestinský	k2eAgFnSc1d1	palestinská
a	a	k8xC	a
babylónská	babylónský	k2eAgFnSc1d1	Babylónská
podoba	podoba	k1gFnSc1	podoba
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
podoba	podoba	k1gFnSc1	podoba
<g/>
,	,	kIx,	,
v	v	k7c4	v
jaké	jaký	k3yIgFnPc4	jaký
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Babylonie	Babylonie	k1gFnSc2	Babylonie
přenesena	přenesen	k2eAgFnSc1d1	přenesena
<g/>
)	)	kIx)	)
Jazyk	jazyk	k1gInSc1	jazyk
amoraitů	amoraita	k1gMnPc2	amoraita
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
s	s	k7c7	s
odlišnostmi	odlišnost	k1gFnPc7	odlišnost
v	v	k7c6	v
palestinské	palestinský	k2eAgFnSc6d1	palestinská
a	a	k8xC	a
babylónské	babylónský	k2eAgFnSc6d1	Babylónská
výslovnosti	výslovnost	k1gFnSc6	výslovnost
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hodnověrnou	hodnověrný	k2eAgFnSc4d1	hodnověrná
v	v	k7c6	v
případě	případ	k1gInSc6	případ
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
již	již	k6eAd1	již
z	z	k7c2	z
mluveného	mluvený	k2eAgNnSc2d1	mluvené
prostředí	prostředí	k1gNnSc2	prostředí
vytrácela	vytrácet	k5eAaImAgFnS	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
hebrejština	hebrejština	k1gFnSc1	hebrejština
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
mrtvým	mrtvý	k2eAgInSc7d1	mrtvý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
komunikaci	komunikace	k1gFnSc6	komunikace
jazykem	jazyk	k1gInSc7	jazyk
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
Židé	Žid	k1gMnPc1	Žid
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
(	(	kIx(	(
<g/>
aramejština	aramejština	k1gFnSc1	aramejština
<g/>
,	,	kIx,	,
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
perština	perština	k1gFnSc1	perština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
např.	např.	kA	např.
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
jazyka	jazyk	k1gMnSc2	jazyk
psaného	psaný	k2eAgMnSc2d1	psaný
-	-	kIx~	-
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
byly	být	k5eAaImAgInP	být
psány	psán	k2eAgInPc1d1	psán
komentáře	komentář	k1gInPc1	komentář
k	k	k7c3	k
Bibli	bible	k1gFnSc3	bible
<g/>
,	,	kIx,	,
Talmudu	talmud	k1gInSc3	talmud
a	a	k8xC	a
midrašům	midraš	k1gInPc3	midraš
<g/>
,	,	kIx,	,
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
psána	psát	k5eAaImNgFnS	psát
responza	responza	k1gFnSc1	responza
(	(	kIx(	(
<g/>
halachická	halachický	k2eAgFnSc1d1	halachická
rabínská	rabínský	k2eAgFnSc1d1	rabínská
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
halachická	halachický	k2eAgFnSc1d1	halachická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
kabaly	kabala	k1gFnPc1	kabala
byly	být	k5eAaImAgFnP	být
hebrejsky	hebrejsky	k6eAd1	hebrejsky
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
aramejsky	aramejsky	k6eAd1	aramejsky
<g/>
)	)	kIx)	)
psány	psán	k2eAgInPc4d1	psán
mystické	mystický	k2eAgInPc4d1	mystický
kabalistické	kabalistický	k2eAgInPc4d1	kabalistický
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neustálé	neustálý	k2eAgNnSc1d1	neustálé
používání	používání	k1gNnSc1	používání
nutilo	nutit	k5eAaImAgNnS	nutit
hebrejsky	hebrejsky	k6eAd1	hebrejsky
píšící	píšící	k2eAgMnPc4d1	píšící
autory	autor	k1gMnPc4	autor
jazyk	jazyk	k1gMnSc1	jazyk
neustále	neustále	k6eAd1	neustále
zdokonalovat	zdokonalovat	k5eAaImF	zdokonalovat
a	a	k8xC	a
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgFnS	vznikat
potřeba	potřeba	k1gFnSc1	potřeba
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
použitelné	použitelný	k2eAgFnPc1d1	použitelná
pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
např.	např.	kA	např.
některých	některý	k3yIgInPc2	některý
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgFnPc2d1	vyskytující
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
neznámých	známý	k2eNgNnPc2d1	neznámé
hebrejských	hebrejský	k2eAgNnPc2d1	hebrejské
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vznikaly	vznikat	k5eAaImAgInP	vznikat
jak	jak	k9	jak
novotvary	novotvar	k1gInPc1	novotvar
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
nová	nový	k2eAgNnPc1d1	nové
pohebrejštěná	pohebrejštěný	k2eAgNnPc1d1	pohebrejštěný
slova	slovo	k1gNnPc1	slovo
převzatá	převzatý	k2eAgNnPc1d1	převzaté
z	z	k7c2	z
cizích	cizí	k2eAgMnPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pijut	Pijut	k1gInSc1	Pijut
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
použití	použití	k1gNnPc2	použití
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
jako	jako	k8xC	jako
literárně	literárně	k6eAd1	literárně
aktivního	aktivní	k2eAgInSc2d1	aktivní
jazyka	jazyk	k1gInSc2	jazyk
po	po	k7c6	po
vymizení	vymizení	k1gNnSc6	vymizení
mišnické	mišnický	k2eAgFnSc2d1	mišnický
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
liturgické	liturgický	k2eAgFnSc6d1	liturgická
poezii	poezie	k1gFnSc6	poezie
-	-	kIx~	-
Pijut	Pijut	k1gInSc1	Pijut
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
pijutim	pijutim	k6eAd1	pijutim
vychází	vycházet	k5eAaImIp3nS	vycházet
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
biblického	biblický	k2eAgInSc2d1	biblický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
měly	mít	k5eAaImAgFnP	mít
naznačit	naznačit	k5eAaPmF	naznačit
kontinuitu	kontinuita	k1gFnSc4	kontinuita
a	a	k8xC	a
snáze	snadno	k6eAd2	snadno
se	se	k3xPyFc4	se
ujmout	ujmout	k5eAaPmF	ujmout
jako	jako	k9	jako
liturgický	liturgický	k2eAgInSc4d1	liturgický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
mišnické	mišnický	k2eAgFnSc2d1	mišnický
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bez	bez	k7c2	bez
její	její	k3xOp3gFnSc2	její
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
a	a	k8xC	a
vyjadřovacích	vyjadřovací	k2eAgFnPc2d1	vyjadřovací
možností	možnost	k1gFnPc2	možnost
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
pijutu	pijut	k1gInSc2	pijut
vkomponovat	vkomponovat	k5eAaPmF	vkomponovat
homiletické	homiletický	k2eAgInPc4d1	homiletický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c4	v
pijutim	pijutim	k1gInSc4	pijutim
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
posunům	posun	k1gInPc3	posun
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
náhodně	náhodně	k6eAd1	náhodně
nebo	nebo	k8xC	nebo
záměrně	záměrně	k6eAd1	záměrně
<g/>
.	.	kIx.	.
</s>
<s>
Pijut	Pijut	k1gInSc1	Pijut
nebyl	být	k5eNaImAgInS	být
pouze	pouze	k6eAd1	pouze
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
přijímán	přijímat	k5eAaImNgMnS	přijímat
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
největší	veliký	k2eAgMnPc4d3	veliký
kritiky	kritik	k1gMnPc4	kritik
patřil	patřit	k5eAaImAgMnS	patřit
slavný	slavný	k2eAgMnSc1d1	slavný
židovský	židovský	k2eAgMnSc1d1	židovský
učenec	učenec	k1gMnSc1	učenec
Abraham	Abraham	k1gMnSc1	Abraham
ibn	ibn	k?	ibn
Ezra	Ezra	k1gMnSc1	Ezra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
pijut	pijut	k1gInSc4	pijut
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
pijut	pijut	k1gInSc1	pijut
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
cizími	cizí	k2eAgInPc7d1	cizí
jazyky	jazyk	k1gInPc7	jazyk
plný	plný	k2eAgInSc4d1	plný
gramatických	gramatický	k2eAgFnPc2d1	gramatická
chyb	chyba	k1gFnPc2	chyba
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
modliteb	modlitba	k1gFnPc2	modlitba
je	být	k5eAaImIp3nS	být
význam	význam	k1gInSc4	význam
některých	některý	k3yIgFnPc2	některý
formulací	formulace	k1gFnPc2	formulace
a	a	k8xC	a
veršů	verš	k1gInPc2	verš
v	v	k7c4	v
pijutim	pijutim	k1gInSc4	pijutim
záhadný	záhadný	k2eAgInSc4d1	záhadný
nebo	nebo	k8xC	nebo
nedávající	dávající	k2eNgInSc4d1	nedávající
smysl	smysl	k1gInSc4	smysl
spíše	spíše	k9	spíše
homilií	homilie	k1gFnSc7	homilie
než	než	k8xS	než
poezií	poezie	k1gFnSc7	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pijutim	pijuti	k1gNnSc7	pijuti
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikaly	vznikat	k5eAaImAgInP	vznikat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
v	v	k7c4	v
Erec	Erec	k1gFnSc4	Erec
Jisra	Jisr	k1gInSc2	Jisr
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc7d2	pozdější
hebrejskou	hebrejský	k2eAgFnSc7d1	hebrejská
liturgickou	liturgický	k2eAgFnSc7d1	liturgická
poezií	poezie	k1gFnSc7	poezie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
centrum	centrum	k1gNnSc1	centrum
bylo	být	k5eAaImAgNnS	být
především	především	k9	především
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
významech	význam	k1gInPc6	význam
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
španělští	španělský	k2eAgMnPc1d1	španělský
středověcí	středověký	k2eAgMnPc1d1	středověký
Židé	Žid	k1gMnPc1	Žid
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
rozuměli	rozumět	k5eAaImAgMnP	rozumět
starším	starý	k2eAgMnSc7d2	starší
izraelským	izraelský	k2eAgMnSc7d1	izraelský
pijutům	pijut	k1gMnPc3	pijut
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
španělskými	španělský	k2eAgMnPc7d1	španělský
Židy	Žid	k1gMnPc7	Žid
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
praxe	praxe	k1gFnSc1	praxe
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
prózu	próza	k1gFnSc4	próza
a	a	k8xC	a
filosofii	filosofie	k1gFnSc4	filosofie
arabštinu	arabština	k1gFnSc4	arabština
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
poezii	poezie	k1gFnSc4	poezie
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
světskou	světský	k2eAgFnSc4d1	světská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
liturgickou	liturgický	k2eAgFnSc4d1	liturgická
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
židovskými	židovská	k1gFnPc7	židovská
autory	autor	k1gMnPc4	autor
hodnocena	hodnocen	k2eAgFnSc1d1	hodnocena
jako	jako	k8xC	jako
krásnější	krásný	k2eAgFnSc1d2	krásnější
<g/>
,	,	kIx,	,
vznešenější	vznešený	k2eAgFnSc1d2	vznešenější
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mnohem	mnohem	k6eAd1	mnohem
bohatší	bohatý	k2eAgInSc1d2	bohatší
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
než	než	k8xS	než
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
filozofických	filozofický	k2eAgNnPc2d1	filozofické
děl	dělo	k1gNnPc2	dělo
židovských	židovský	k2eAgMnPc2d1	židovský
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
Sa	Sa	k1gMnSc1	Sa
<g/>
'	'	kIx"	'
<g/>
adja	adja	k1gMnSc1	adja
Gaon	Gaon	k1gMnSc1	Gaon
<g/>
,	,	kIx,	,
Jehuda	Jehuda	k1gFnSc1	Jehuda
Halevi	Haleev	k1gFnSc3	Haleev
nebo	nebo	k8xC	nebo
Maimonides	Maimonidesa	k1gFnPc2	Maimonidesa
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
odvedla	odvést	k5eAaPmAgFnS	odvést
rodina	rodina	k1gFnSc1	rodina
Tibonidů	Tibonid	k1gMnPc2	Tibonid
<g/>
.	.	kIx.	.
</s>
<s>
Jehuda	Jehuda	k1gMnSc1	Jehuda
ibn	ibn	k?	ibn
Tibon	Tibon	k1gMnSc1	Tibon
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
překladech	překlad	k1gInPc6	překlad
vycházel	vycházet	k5eAaImAgInS	vycházet
jak	jak	k8xS	jak
z	z	k7c2	z
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
biblické	biblický	k2eAgFnSc2d1	biblická
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
z	z	k7c2	z
mišnické	mišnický	k2eAgFnSc2d1	mišnický
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdála	zdát	k5eAaImAgFnS	zdát
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
či	či	k8xC	či
onu	onen	k3xDgFnSc4	onen
chvíli	chvíle	k1gFnSc4	chvíle
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
specifická	specifický	k2eAgFnSc1d1	specifická
forma	forma	k1gFnSc1	forma
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
nazývat	nazývat	k5eAaImF	nazývat
podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
pět	pět	k4xCc4	pět
generací	generace	k1gFnPc2	generace
příslušníků	příslušník	k1gMnPc2	příslušník
rodiny	rodina	k1gFnSc2	rodina
ibn	ibn	k?	ibn
Tibonů	Tibon	k1gInPc2	Tibon
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
stovky	stovka	k1gFnSc2	stovka
arabských	arabský	k2eAgFnPc2d1	arabská
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
židovských	židovský	k2eAgMnPc2d1	židovský
nebo	nebo	k8xC	nebo
arabských	arabský	k2eAgMnPc2d1	arabský
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Tibonidovská	Tibonidovský	k2eAgFnSc1d1	Tibonidovský
hebrejština	hebrejština	k1gFnSc1	hebrejština
nezapře	zapřít	k5eNaPmIp3nS	zapřít
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
uzpůsobení	uzpůsobení	k1gNnSc2	uzpůsobení
některých	některý	k3yIgNnPc2	některý
arabských	arabský	k2eAgNnPc2d1	arabské
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
frází	fráze	k1gFnPc2	fráze
jejich	jejich	k3xOp3gInSc3	jejich
hebrejskému	hebrejský	k2eAgInSc3d1	hebrejský
ekivalentu	ekivalent	k1gInSc3	ekivalent
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
někdy	někdy	k6eAd1	někdy
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
spisovnosti	spisovnost	k1gFnSc2	spisovnost
nebo	nebo	k8xC	nebo
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
přeložených	přeložený	k2eAgNnPc2d1	přeložené
děl	dělo	k1gNnPc2	dělo
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
i	i	k8xC	i
pozdější	pozdní	k2eAgMnSc1d2	pozdější
hebrejsky	hebrejsky	k6eAd1	hebrejsky
píšící	píšící	k2eAgMnPc4d1	píšící
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tibonidovská	tibonidovský	k2eAgFnSc1d1	tibonidovský
podoba	podoba	k1gFnSc1	podoba
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
byla	být	k5eAaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
i	i	k9	i
u	u	k7c2	u
originálních	originální	k2eAgNnPc2d1	originální
hebrejských	hebrejský	k2eAgNnPc2d1	hebrejské
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
hebrejština	hebrejština	k1gFnSc1	hebrejština
použitá	použitý	k2eAgFnSc1d1	použitá
Maimonidem	Maimonid	k1gMnSc7	Maimonid
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
Mišne	Mišn	k1gInSc5	Mišn
Tora	Tor	k1gMnSc4	Tor
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
dalšího	další	k2eAgNnSc2d1	další
epochálního	epochální	k2eAgNnSc2d1	epochální
díla	dílo	k1gNnSc2	dílo
More	mor	k1gInSc5	mor
nevuchim	nevuchim	k1gInSc1	nevuchim
je	on	k3xPp3gMnPc4	on
Mišne	Mišn	k1gInSc5	Mišn
Tora	Tor	k1gMnSc2	Tor
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
originále	originál	k1gInSc6	originál
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
stopy	stopa	k1gFnPc4	stopa
arabského	arabský	k2eAgInSc2d1	arabský
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
o	o	k7c4	o
vlivy	vliv	k1gInPc4	vliv
dřívějších	dřívější	k2eAgInPc2d1	dřívější
hebrejských	hebrejský	k2eAgInPc2d1	hebrejský
překladů	překlad	k1gInPc2	překlad
arabských	arabský	k2eAgInPc2d1	arabský
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
tibonidovskou	tibonidovský	k2eAgFnSc7d1	tibonidovský
hebejštinou	hebejština	k1gFnSc7	hebejština
je	být	k5eAaImIp3nS	být
Maimonidova	Maimonidův	k2eAgFnSc1d1	Maimonidova
hebrejština	hebrejština	k1gFnSc1	hebrejština
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
"	"	kIx"	"
<g/>
vznešenější	vznešený	k2eAgFnSc4d2	vznešenější
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
učenou	učený	k2eAgFnSc4d1	učená
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
spisy	spis	k1gInPc4	spis
Abrahama	Abraham	k1gMnSc2	Abraham
ibn	ibn	k?	ibn
Ezry	Ezra	k1gMnSc2	Ezra
za	za	k7c4	za
typickou	typický	k2eAgFnSc4d1	typická
ukázku	ukázka	k1gFnSc4	ukázka
rabínské	rabínský	k2eAgFnSc2d1	rabínská
středověké	středověký	k2eAgFnSc2d1	středověká
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Aškenázští	Aškenázský	k2eAgMnPc1d1	Aškenázský
židé	žid	k1gMnPc1	žid
udržovali	udržovat	k5eAaImAgMnP	udržovat
úzké	úzký	k2eAgInPc4d1	úzký
svazky	svazek	k1gInPc4	svazek
s	s	k7c7	s
Erec	Erec	k1gFnSc1	Erec
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
-	-	kIx~	-
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c4	na
podobnosti	podobnost	k1gFnPc4	podobnost
izraelských	izraelský	k2eAgNnPc2d1	izraelské
a	a	k8xC	a
aškenázských	aškenázský	k2eAgNnPc2d1	aškenázský
pijutim	pijutimo	k1gNnPc2	pijutimo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
sefardském	sefardský	k2eAgNnSc6d1	sefardský
kulturním	kulturní	k2eAgNnSc6d1	kulturní
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
hebrejština	hebrejština	k1gFnSc1	hebrejština
dělila	dělit	k5eAaImAgFnS	dělit
o	o	k7c4	o
post	post	k1gInSc4	post
hlavního	hlavní	k2eAgInSc2d1	hlavní
psaného	psaný	k2eAgInSc2d1	psaný
jazyka	jazyk	k1gInSc2	jazyk
s	s	k7c7	s
arabštinou	arabština	k1gFnSc7	arabština
<g/>
,	,	kIx,	,
v	v	k7c6	v
aškenázském	aškenázský	k2eAgInSc6d1	aškenázský
světě	svět	k1gInSc6	svět
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
pozice	pozice	k1gFnSc1	pozice
neotřesitelná	otřesitelný	k2eNgFnSc1d1	neotřesitelná
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
se	se	k3xPyFc4	se
aškenázští	aškenázský	k2eAgMnPc1d1	aškenázský
židé	žid	k1gMnPc1	žid
dorozumívali	dorozumívat	k5eAaImAgMnP	dorozumívat
-	-	kIx~	-
nejvíce	nejvíce	k6eAd1	nejvíce
němčinou	němčina	k1gFnSc7	němčina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
přesunem	přesun	k1gInSc7	přesun
aškenázského	aškenázský	k2eAgNnSc2d1	aškenázský
centra	centrum	k1gNnSc2	centrum
na	na	k7c4	na
východ	východ	k1gInSc4	východ
i	i	k8xC	i
polštinou	polština	k1gFnSc7	polština
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
užívaná	užívaný	k2eAgFnSc1d1	užívaná
německými	německý	k2eAgMnPc7d1	německý
a	a	k8xC	a
polskými	polský	k2eAgMnPc7d1	polský
Židy	Žid	k1gMnPc7	Žid
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
syntaxí	syntax	k1gFnSc7	syntax
němčiny	němčina	k1gFnSc2	němčina
-	-	kIx~	-
např.	např.	kA	např.
י	י	k?	י
מ	מ	k?	מ
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
vědět	vědět	k5eAaImF	vědět
z.	z.	k?	z.
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
wissen	wissen	k2eAgInSc4d1	wissen
von	von	k1gInSc4	von
-	-	kIx~	-
"	"	kIx"	"
<g/>
vědět	vědět	k5eAaImF	vědět
o.	o.	k?	o.
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ע	ע	k?	ע
ה	ה	k?	ה
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
"	"	kIx"	"
-	-	kIx~	-
z	z	k7c2	z
auf	auf	k?	auf
den	den	k1gInSc1	den
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
tradičnější	tradiční	k2eAgFnSc2d2	tradičnější
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
vazby	vazba	k1gFnSc2	vazba
ל	ל	k?	ל
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Rabínská	rabínský	k2eAgFnSc1d1	rabínská
středověká	středověký	k2eAgFnSc1d1	středověká
hebrejština	hebrejština	k1gFnSc1	hebrejština
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
pružné	pružný	k2eAgFnSc3d1	pružná
a	a	k8xC	a
nestanovené	stanovený	k2eNgFnSc3d1	nestanovená
gramatice	gramatika	k1gFnSc3	gramatika
později	pozdě	k6eAd2	pozdě
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
v	v	k7c6	v
období	období	k1gNnSc6	období
haskaly	haskat	k5eAaPmAgFnP	haskat
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
biblické	biblický	k2eAgFnSc2d1	biblická
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgNnSc1	některý
arabštinou	arabština	k1gFnSc7	arabština
ovlivněné	ovlivněný	k2eAgFnPc1d1	ovlivněná
variace	variace	k1gFnPc1	variace
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
zrod	zrod	k1gInSc4	zrod
moderního	moderní	k2eAgInSc2d1	moderní
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
moderní	moderní	k2eAgFnSc2d1	moderní
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
souhrnně	souhrnně	k6eAd1	souhrnně
jako	jako	k9	jako
ivrit	ivrit	k5eAaImF	ivrit
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
se	s	k7c7	s
zrodem	zrod	k1gInSc7	zrod
sionistického	sionistický	k2eAgNnSc2d1	sionistické
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
obnovy	obnova	k1gFnSc2	obnova
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
jako	jako	k8xS	jako
moderního	moderní	k2eAgInSc2d1	moderní
živého	živý	k2eAgInSc2d1	živý
jazyka	jazyk	k1gInSc2	jazyk
začíná	začínat	k5eAaImIp3nS	začínat
rokem	rok	k1gInSc7	rok
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
integrální	integrální	k2eAgFnPc1d1	integrální
součásti	součást	k1gFnPc1	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
z	z	k7c2	z
Litvy	Litva	k1gFnSc2	Litva
Eli	Eli	k1gFnSc2	Eli
<g/>
'	'	kIx"	'
<g/>
ezer	ezer	k1gInSc1	ezer
Ben	Ben	k1gInSc1	Ben
Jehuda	Jehuda	k1gFnSc1	Jehuda
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Eli	Eli	k1gFnSc2	Eli
<g/>
'	'	kIx"	'
<g/>
ezer	ezer	k1gMnSc1	ezer
Jicchak	Jicchak	k1gMnSc1	Jicchak
Perlman	Perlman	k1gMnSc1	Perlman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
propagátorem	propagátor	k1gMnSc7	propagátor
užívání	užívání	k1gNnSc2	užívání
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
v	v	k7c6	v
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
hlavními	hlavní	k2eAgMnPc7d1	hlavní
jazyky	jazyk	k1gMnPc7	jazyk
jišuvu	jišuv	k1gInSc2	jišuv
(	(	kIx(	(
<g/>
židovského	židovský	k2eAgNnSc2d1	Židovské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
)	)	kIx)	)
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
ladino	ladino	k1gNnSc1	ladino
a	a	k8xC	a
jidiš	jidiš	k1gNnSc1	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
nově	nově	k6eAd1	nově
příchozích	příchozí	k1gFnPc2	příchozí
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
hovořila	hovořit	k5eAaImAgFnS	hovořit
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
o	o	k7c6	o
společném	společný	k2eAgInSc6d1	společný
jazyku	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
též	též	k9	též
o	o	k7c6	o
němčině	němčina	k1gFnSc6	němčina
nebo	nebo	k8xC	nebo
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
tisk	tisk	k1gInSc1	tisk
vycházel	vycházet	k5eAaImAgInS	vycházet
rovněž	rovněž	k9	rovněž
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
italsky	italsky	k6eAd1	italsky
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
společný	společný	k2eAgInSc1d1	společný
dorozumívací	dorozumívací	k2eAgInSc1d1	dorozumívací
jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
židovské	židovský	k2eAgNnSc4d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Palestiny	Palestina	k1gFnSc2	Palestina
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
nutný	nutný	k2eAgInSc1d1	nutný
<g/>
.	.	kIx.	.
</s>
<s>
Úkol	úkol	k1gInSc1	úkol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
Ben	Ben	k1gInSc4	Ben
Jehuda	Jehuda	k1gMnSc1	Jehuda
předsevzal	předsevzít	k5eAaPmAgMnS	předsevzít
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
usnadněn	usnadnit	k5eAaPmNgInS	usnadnit
několika	několik	k4yIc7	několik
faktory	faktor	k1gInPc7	faktor
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
nutnosti	nutnost	k1gFnSc2	nutnost
společného	společný	k2eAgInSc2d1	společný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
hlavní	hlavní	k2eAgMnPc1d1	hlavní
představitelé	představitel	k1gMnPc1	představitel
sionistického	sionistický	k2eAgNnSc2d1	sionistické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Palestiny	Palestina	k1gFnSc2	Palestina
(	(	kIx(	(
<g/>
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
"	"	kIx"	"
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
neexistoval	existovat	k5eNaImAgMnS	existovat
žádný	žádný	k3yNgInSc4	žádný
ryze	ryze	k6eAd1	ryze
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
neexistoval	existovat	k5eNaImAgInS	existovat
ryze	ryze	k6eAd1	ryze
palestinský	palestinský	k2eAgInSc1d1	palestinský
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
Arabština	arabština	k1gFnSc1	arabština
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
jako	jako	k8xC	jako
dorozumívací	dorozumívací	k2eAgInSc4d1	dorozumívací
jazyk	jazyk	k1gInSc4	jazyk
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
syrsko-palestinského	syrskoalestinský	k2eAgInSc2d1	syrsko-palestinský
dialektu	dialekt	k1gInSc2	dialekt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
podobě	podoba	k1gFnSc6	podoba
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
spisovná	spisovný	k2eAgFnSc1d1	spisovná
hebrejština	hebrejština	k1gFnSc1	hebrejština
odkázána	odkázán	k2eAgFnSc1d1	odkázána
k	k	k7c3	k
bohoslužebným	bohoslužebný	k2eAgInPc3d1	bohoslužebný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jazyk	jazyk	k1gInSc4	jazyk
úřední	úřední	k2eAgInSc4d1	úřední
a	a	k8xC	a
vojenský	vojenský	k2eAgInSc4d1	vojenský
sloužila	sloužit	k5eAaImAgFnS	sloužit
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vhodně	vhodně	k6eAd1	vhodně
zvolená	zvolený	k2eAgFnSc1d1	zvolená
jako	jako	k8xS	jako
ideologický	ideologický	k2eAgInSc1d1	ideologický
nástroj	nástroj	k1gInSc1	nástroj
podporující	podporující	k2eAgFnSc2d1	podporující
sionistické	sionistický	k2eAgFnSc2d1	sionistická
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
ustanovení	ustanovení	k1gNnSc4	ustanovení
židovského	židovský	k2eAgInSc2d1	židovský
-	-	kIx~	-
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
tak	tak	k6eAd1	tak
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
období	období	k1gNnSc6	období
archeologických	archeologický	k2eAgInPc2d1	archeologický
objevů	objev	k1gInPc2	objev
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
umocňovaly	umocňovat	k5eAaImAgFnP	umocňovat
spojení	spojení	k1gNnSc4	spojení
moderních	moderní	k2eAgMnPc2d1	moderní
"	"	kIx"	"
<g/>
Hebrejců	Hebrejec	k1gMnPc2	Hebrejec
<g/>
"	"	kIx"	"
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
starověkými	starověký	k2eAgInPc7d1	starověký
předky	předek	k1gInPc7	předek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
nové	nový	k2eAgFnSc2d1	nová
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
potřebám	potřeba	k1gFnPc3	potřeba
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
sloužily	sloužit	k5eAaImAgInP	sloužit
jak	jak	k6eAd1	jak
hebrejské	hebrejský	k2eAgInPc4d1	hebrejský
novotvary	novotvar	k1gInPc4	novotvar
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
slova	slovo	k1gNnPc1	slovo
převzatá	převzatý	k2eAgNnPc1d1	převzaté
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
nebo	nebo	k8xC	nebo
aramejštiny	aramejština	k1gFnSc2	aramejština
<g/>
.	.	kIx.	.
</s>
<s>
Gramatický	gramatický	k2eAgInSc1d1	gramatický
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
přejat	přejat	k2eAgInSc4d1	přejat
z	z	k7c2	z
biblické	biblický	k2eAgFnSc2d1	biblická
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
systém	systém	k1gInSc1	systém
vidů	vid	k1gInPc2	vid
byl	být	k5eAaImAgInS	být
opuštěn	opustit	k5eAaPmNgInS	opustit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
pro	pro	k7c4	pro
indoevropské	indoevropský	k2eAgInPc4d1	indoevropský
jazyky	jazyk	k1gInPc4	jazyk
typickým	typický	k2eAgInSc7d1	typický
systémem	systém	k1gInSc7	systém
časů	čas	k1gInPc2	čas
(	(	kIx(	(
<g/>
perfektum	perfektum	k1gNnSc1	perfektum
-	-	kIx~	-
minulý	minulý	k2eAgInSc1d1	minulý
<g/>
,	,	kIx,	,
imperfektum	imperfektum	k1gNnSc1	imperfektum
-	-	kIx~	-
budoucí	budoucí	k2eAgNnPc1d1	budoucí
<g/>
,	,	kIx,	,
participium	participium	k1gNnSc1	participium
-	-	kIx~	-
přítomný	přítomný	k2eAgMnSc1d1	přítomný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
biblická	biblický	k2eAgFnSc1d1	biblická
anomálie	anomálie	k1gFnSc1	anomálie
konsekutivní	konsekutivní	k2eAgFnSc2d1	konsekutivní
vav	vav	k?	vav
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
biblické	biblický	k2eAgInPc1d1	biblický
tvary	tvar	k1gInPc1	tvar
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přestaly	přestat	k5eAaPmAgFnP	přestat
používat	používat	k5eAaImF	používat
(	(	kIx(	(
<g/>
infinitiv	infinitiv	k1gInSc1	infinitiv
absolutní	absolutní	k2eAgInSc1d1	absolutní
<g/>
,	,	kIx,	,
participium	participium	k1gNnSc1	participium
nif	nif	k?	nif
<g/>
'	'	kIx"	'
<g/>
alu	ala	k1gFnSc4	ala
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k8xC	jako
nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntax	syntax	k1gFnSc1	syntax
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
biblické	biblický	k2eAgFnSc3d1	biblická
hebrejštině	hebrejština	k1gFnSc3	hebrejština
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
inspirace	inspirace	k1gFnSc1	inspirace
pro	pro	k7c4	pro
syntax	syntax	k1gFnSc4	syntax
byly	být	k5eAaImAgFnP	být
brány	brána	k1gFnPc1	brána
jak	jak	k6eAd1	jak
z	z	k7c2	z
maimonidovské	maimonidovský	k2eAgFnSc2d1	maimonidovský
středověké	středověký	k2eAgFnSc2d1	středověká
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
výslovnosti	výslovnost	k1gFnSc2	výslovnost
se	se	k3xPyFc4	se
Ben	Ben	k1gInSc1	Ben
Jehuda	Jehudo	k1gNnSc2	Jehudo
přikláněl	přiklánět	k5eAaImAgInS	přiklánět
k	k	k7c3	k
sefardské	sefardský	k2eAgFnSc3d1	sefardská
výslovnosti	výslovnost	k1gFnSc3	výslovnost
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
aškenázské	aškenázský	k2eAgNnSc1d1	aškenázský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narážel	narážet	k5eAaImAgInS	narážet
na	na	k7c4	na
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
určité	určitý	k2eAgFnPc4d1	určitá
hlásky	hláska	k1gFnSc2	hláska
(	(	kIx(	(
<g/>
především	především	k9	především
ajin	ajin	k1gMnSc1	ajin
a	a	k8xC	a
chet	chet	k1gMnSc1	chet
<g/>
)	)	kIx)	)
vyslovit	vyslovit	k5eAaPmF	vyslovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
Va	va	k0wR	va
<g/>
'	'	kIx"	'
<g/>
ad	ad	k7c4	ad
ha-lašon	haašon	k1gInSc4	ha-lašon
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
následující	následující	k2eAgNnSc4d1	následující
doporučení	doporučení	k1gNnSc4	doporučení
<g/>
:	:	kIx,	:
ב	ב	k?	ב
bez	bez	k1gInSc1	bez
dageš	dagat	k5eAaBmIp2nS	dagat
=	=	kIx~	=
[	[	kIx(	[
<g/>
v	v	k7c6	v
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
ו	ו	k?	ו
=	=	kIx~	=
arabské	arabský	k2eAgInPc1d1	arabský
و	و	k?	و
(	(	kIx(	(
<g/>
anglické	anglický	k2eAgFnSc2d1	anglická
[	[	kIx(	[
<g/>
w	w	k?	w
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
ח	ח	k?	ח
=	=	kIx~	=
arabské	arabský	k2eAgInPc1d1	arabský
ح	ح	k?	ح
(	(	kIx(	(
<g/>
hluboké	hluboký	k2eAgFnSc2d1	hluboká
ch	ch	k0	ch
<g/>
)	)	kIx)	)
odlišné	odlišný	k2eAgInPc4d1	odlišný
od	od	k7c2	od
כ	כ	k?	כ
bez	bez	k7c2	bez
dageš	dagat	k5eAaPmIp2nS	dagat
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
totožné	totožný	k2eAgNnSc1d1	totožné
s	s	k7c7	s
arabským	arabský	k2eAgMnSc7d1	arabský
خ	خ	k?	خ
[	[	kIx(	[
<g/>
ch	ch	k0	ch
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
ט	ט	k?	ט
=	=	kIx~	=
arabské	arabský	k2eAgInPc1d1	arabský
ط	ط	k?	ط
(	(	kIx(	(
<g/>
emfatické	emfatický	k2eAgInPc1d1	emfatický
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ע	ע	k?	ע
=	=	kIx~	=
arabské	arabský	k2eAgInPc1d1	arabský
ع	ع	k?	ع
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cajn	cajn	k1gNnSc1	cajn
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
צ	צ	k?	צ
=	=	kIx~	=
c	c	k0	c
<g/>
;	;	kIx,	;
ק	ק	k?	ק
=	=	kIx~	=
arabské	arabský	k2eAgInPc1d1	arabský
ق	ق	k?	ق
(	(	kIx(	(
<g/>
q	q	k?	q
<g/>
,	,	kIx,	,
zadopatrové	zadopatrové	k2eAgFnSc1d1	zadopatrové
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ת	ת	k?	ת
bez	bez	k1gInSc1	bez
dageš	dagat	k5eAaBmIp2nS	dagat
=	=	kIx~	=
arabské	arabský	k2eAgNnSc1d1	arabské
ث	ث	k?	ث
(	(	kIx(	(
<g/>
jako	jako	k9	jako
anglické	anglický	k2eAgFnSc3d1	anglická
[	[	kIx(	[
<g/>
th	th	k?	th
<g/>
]	]	kIx)	]
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
thin	thin	k1gMnSc1	thin
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
nenásleduje	následovat	k5eNaImIp3nS	následovat
zcela	zcela	k6eAd1	zcela
sefardskou	sefardský	k2eAgFnSc4d1	sefardská
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
některé	některý	k3yIgInPc1	některý
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
znaky	znak	k1gInPc1	znak
aškenázské	aškenázský	k2eAgFnSc2d1	aškenázská
výslovnosti	výslovnost	k1gFnSc2	výslovnost
byly	být	k5eAaImAgInP	být
vynechány	vynechán	k2eAgInPc1d1	vynechán
-	-	kIx~	-
podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
ustanovení	ustanovení	k1gNnSc2	ustanovení
byl	být	k5eAaImAgInS	být
kamac	kamac	k6eAd1	kamac
gadol	gadol	k1gInSc1	gadol
vokalizován	vokalizován	k2eAgInSc1d1	vokalizován
jako	jako	k9	jako
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
a	a	k8xC	a
ת	ת	k?	ת
bez	bez	k1gInSc1	bez
dageš	dagat	k5eAaBmIp2nS	dagat
bylo	být	k5eAaImAgNnS	být
nadále	nadále	k6eAd1	nadále
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
již	již	k9	již
hebrejština	hebrejština	k1gFnSc1	hebrejština
jako	jako	k9	jako
živý	živý	k2eAgInSc4d1	živý
jazyk	jazyk	k1gInSc4	jazyk
přijata	přijat	k2eAgFnSc1d1	přijata
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
mandátní	mandátní	k2eAgFnSc2d1	mandátní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
