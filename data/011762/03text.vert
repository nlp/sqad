<p>
<s>
Rijád	Rijáda	k1gFnPc2	Rijáda
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
krále	král	k1gMnSc2	král
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
900	[number]	k4	900
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
regionu	region	k1gInSc6	region
Nadžd	Nadžd	k1gInSc1	Nadžd
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
1300	[number]	k4	1300
km2	km2	k4	km2
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
celé	celý	k2eAgFnSc2d1	celá
aglomerace	aglomerace	k1gFnSc2	aglomerace
činí	činit	k5eAaImIp3nS	činit
1815	[number]	k4	1815
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Rijád	Rijáda	k1gFnPc2	Rijáda
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
v	v	k7c6	v
předislámských	předislámský	k2eAgInPc6d1	předislámský
časech	čas	k1gInPc6	čas
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Haždar	Haždar	k1gInSc1	Haždar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
několika	několik	k4yIc2	několik
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
wádí	wádit	k5eAaImIp3nS	wádit
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
malá	malý	k2eAgFnSc1d1	malá
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
prvního	první	k4xOgInSc2	první
Saúdského	saúdský	k2eAgInSc2d1	saúdský
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Turci	Turek	k1gMnPc1	Turek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
původní	původní	k2eAgNnSc4d1	původní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Diríja	Diríj	k1gInSc2	Diríj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
sídlili	sídlit	k5eAaImAgMnP	sídlit
Wahhábité	Wahhábita	k1gMnPc1	Wahhábita
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
ho	on	k3xPp3gMnSc4	on
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Ibn	Ibn	k1gMnSc1	Ibn
Saúd	Saúd	k1gMnSc1	Saúd
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
král	král	k1gMnSc1	král
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
Abd	Abd	k1gMnSc1	Abd
al-Azíz	al-Azíz	k1gMnSc1	al-Azíz
ibn	ibn	k?	ibn
Saúd	Saúd	k1gMnSc1	Saúd
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
vybral	vybrat	k5eAaPmAgInS	vybrat
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
nového	nový	k2eAgNnSc2d1	nové
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
tu	tu	k6eAd1	tu
byly	být	k5eAaImAgFnP	být
strženy	stržen	k2eAgFnPc1d1	stržena
hradby	hradba	k1gFnPc1	hradba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
budoucí	budoucí	k2eAgInSc4d1	budoucí
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
veškerá	veškerý	k3xTgFnSc1	veškerý
administrativa	administrativa	k1gFnSc1	administrativa
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Džiddě	Džidd	k1gInSc6	Džidd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
zažívá	zažívat	k5eAaImIp3nS	zažívat
prudký	prudký	k2eAgInSc1d1	prudký
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vybudované	vybudovaný	k2eAgInPc1d1	vybudovaný
nové	nový	k2eAgInPc1d1	nový
bulváry	bulvár	k1gInPc1	bulvár
a	a	k8xC	a
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakter	charakter	k1gInSc1	charakter
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Rijád	Rijáda	k1gFnPc2	Rijáda
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgNnSc1d1	moderní
velkoměsto	velkoměsto	k1gNnSc1	velkoměsto
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
politické	politický	k2eAgNnSc1d1	politické
a	a	k8xC	a
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
centrum	centrum	k1gNnSc1	centrum
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
leží	ležet	k5eAaImIp3nS	ležet
uprostřed	uprostřed	k7c2	uprostřed
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
vody	voda	k1gFnPc1	voda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dostatek	dostatek	k1gInSc4	dostatek
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
sem	sem	k6eAd1	sem
přiváděna	přivádět	k5eAaImNgFnS	přivádět
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
potrubím	potrubí	k1gNnSc7	potrubí
z	z	k7c2	z
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odsoluje	odsolovat	k5eAaImIp3nS	odsolovat
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
se	se	k3xPyFc4	se
Rijád	Rijáda	k1gFnPc2	Rijáda
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
17	[number]	k4	17
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Rijádu	Rijád	k1gInSc2	Rijád
se	se	k3xPyFc4	se
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
zde	zde	k6eAd1	zde
sídlilo	sídlit	k5eAaImAgNnS	sídlit
pouze	pouze	k6eAd1	pouze
7,5	[number]	k4	7,5
tisíce	tisíc	k4xCgInSc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
30	[number]	k4	30
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
půl	půl	k1xP	půl
miliónu	milión	k4xCgInSc2	milión
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
již	již	k6eAd1	již
5,89	[number]	k4	5,89
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
příměstská	příměstský	k2eAgNnPc4d1	příměstské
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
celých	celý	k2eAgInPc2d1	celý
sedm	sedm	k4xCc1	sedm
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Krále	Král	k1gMnSc2	Král
Chálida	Chálid	k1gMnSc2	Chálid
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
pětadvacet	pětadvacet	k4xCc4	pětadvacet
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
otevření	otevření	k1gNnSc2	otevření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
civilní	civilní	k2eAgNnSc4d1	civilní
letiště	letiště	k1gNnSc4	letiště
dnešní	dnešní	k2eAgFnSc1d1	dnešní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
letecká	letecký	k2eAgFnSc1d1	letecká
základna	základna	k1gFnSc1	základna
Rijád	Rijáda	k1gFnPc2	Rijáda
Saúdského	saúdský	k2eAgNnSc2d1	Saúdské
královského	královský	k2eAgNnSc2d1	královské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
hrají	hrát	k5eAaImIp3nP	hrát
tři	tři	k4xCgInPc1	tři
prvoligové	prvoligový	k2eAgInPc1d1	prvoligový
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
Al	ala	k1gFnPc2	ala
Hilal	Hilal	k1gInSc4	Hilal
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
Nassr	Nassra	k1gFnPc2	Nassra
FC	FC	kA	FC
a	a	k8xC	a
Al	ala	k1gFnPc2	ala
Shabab	Shababa	k1gFnPc2	Shababa
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Rijád	Rijáda	k1gFnPc2	Rijáda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rijád	Rijáda	k1gFnPc2	Rijáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
