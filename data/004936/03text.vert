<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
(	(	kIx(	(
<g/>
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
odvozeného	odvozený	k2eAgMnSc4d1	odvozený
přes	přes	k7c4	přes
latinské	latinský	k2eAgNnSc4d1	latinské
christianos	christianosa	k1gFnPc2	christianosa
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
χ	χ	k?	χ
s	s	k7c7	s
významem	význam	k1gInSc7	význam
náležící	náležící	k2eAgFnSc4d1	náležící
Kristu	Krista	k1gFnSc4	Krista
či	či	k8xC	či
kristovec	kristovec	k1gInSc4	kristovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
abrahámovské	abrahámovský	k2eAgNnSc1d1	abrahámovské
<g/>
,	,	kIx,	,
monoteistické	monoteistický	k2eAgFnPc1d1	monoteistická
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnPc1d1	univerzální
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
(	(	kIx(	(
<g/>
založené	založený	k2eAgFnPc1d1	založená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
misijní	misijní	k2eAgNnSc1d1	misijní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgNnSc1d1	soustředěné
kolem	kolem	k7c2	kolem
života	život	k1gInSc2	život
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k8xS	jako
mesiáše	mesiáš	k1gMnSc2	mesiáš
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Krista	Kristus	k1gMnSc2	Kristus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spasitele	spasitel	k1gMnSc4	spasitel
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Božího	boží	k2eAgMnSc2d1	boží
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
antického	antický	k2eAgInSc2d1	antický
judaismu	judaismus	k1gInSc2	judaismus
na	na	k7c6	na
území	území	k1gNnSc6	území
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ježíš	Ježíš	k1gMnSc1	Ježíš
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
působení	působení	k1gNnSc4	působení
<g/>
,	,	kIx,	,
počátky	počátek	k1gInPc4	počátek
a	a	k8xC	a
základy	základ	k1gInPc4	základ
křesťanství	křesťanství	k1gNnSc2	křesťanství
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
židovským	židovský	k2eAgInSc7d1	židovský
Starým	starý	k2eAgInSc7d1	starý
zákonem	zákon	k1gInSc7	zákon
tvoří	tvořit	k5eAaImIp3nP	tvořit
bibli	bible	k1gFnSc4	bible
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc4d1	základní
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
knihu	kniha	k1gFnSc4	kniha
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Příslušnost	příslušnost	k1gFnSc1	příslušnost
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
není	být	k5eNaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
původem	původ	k1gInSc7	původ
nebo	nebo	k8xC	nebo
narozením	narození	k1gNnSc7	narození
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
křtem	křest	k1gInSc7	křest
a	a	k8xC	a
osobním	osobní	k2eAgNnSc7d1	osobní
přijetím	přijetí	k1gNnSc7	přijetí
určitého	určitý	k2eAgNnSc2d1	určité
vyznání	vyznání	k1gNnSc2	vyznání
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
nauky	nauka	k1gFnSc2	nauka
<g/>
)	)	kIx)	)
a	a	k8xC	a
životní	životní	k2eAgFnSc2d1	životní
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
křesťané	křesťan	k1gMnPc1	křesťan
věří	věřit	k5eAaImIp3nP	věřit
v	v	k7c6	v
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
trojjediného	trojjediný	k2eAgMnSc2d1	trojjediný
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
jako	jako	k8xS	jako
Spasitele	spasitel	k1gMnSc2	spasitel
a	a	k8xC	a
Božího	boží	k2eAgMnSc2d1	boží
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
uznávají	uznávat	k5eAaImIp3nP	uznávat
křest	křest	k1gInSc4	křest
(	(	kIx(	(
<g/>
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
i	i	k9	i
další	další	k2eAgFnPc1d1	další
svátosti	svátost	k1gFnPc1	svátost
<g/>
)	)	kIx)	)
a	a	k8xC	a
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
možnost	možnost	k1gFnSc4	možnost
odpuštění	odpuštění	k1gNnSc2	odpuštění
hříchů	hřích	k1gInPc2	hřích
a	a	k8xC	a
spásy	spása	k1gFnSc2	spása
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
chápou	chápat	k5eAaImIp3nP	chápat
život	život	k1gInSc4	život
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
jako	jako	k9	jako
Boží	božit	k5eAaImIp3nS	božit
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
člověka	člověk	k1gMnSc4	člověk
zachraňuje	zachraňovat	k5eAaImIp3nS	zachraňovat
z	z	k7c2	z
duchovní	duchovní	k2eAgFnSc2d1	duchovní
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
mu	on	k3xPp3gNnSc3	on
odpuštění	odpuštění	k1gNnSc3	odpuštění
a	a	k8xC	a
smíření	smíření	k1gNnSc3	smíření
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
<g/>
,	,	kIx,	,
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
byl	být	k5eAaImAgMnS	být
vzkříšen	vzkříšen	k2eAgMnSc1d1	vzkříšen
<g/>
,	,	kIx,	,
40	[number]	k4	40
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
některým	některý	k3yIgMnPc3	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
stoupenců	stoupenec	k1gMnPc2	stoupenec
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
bible	bible	k1gFnSc2	bible
a	a	k8xC	a
víry	víra	k1gFnSc2	víra
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
učedníkům	učedník	k1gMnPc3	učedník
o	o	k7c6	o
letnicích	letnice	k1gFnPc6	letnice
seslán	seslat	k5eAaPmNgMnS	seslat
Duch	duch	k1gMnSc1	duch
svatý	svatý	k1gMnSc1	svatý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nadále	nadále	k6eAd1	nadále
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
očekávají	očekávat	k5eAaImIp3nP	očekávat
druhý	druhý	k4xOgInSc4	druhý
příchod	příchod	k1gInSc4	příchod
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
jako	jako	k8xC	jako
soudce	soudce	k1gMnSc2	soudce
světa	svět	k1gInSc2	svět
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Kristovi	Kristův	k2eAgMnPc1d1	Kristův
následovníci	následovník	k1gMnPc1	následovník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
apoštoly	apoštol	k1gMnPc7	apoštol
tvořili	tvořit	k5eAaImAgMnP	tvořit
prvotní	prvotní	k2eAgFnSc4d1	prvotní
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
společenství	společenství	k1gNnSc1	společenství
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
rozešlo	rozejít	k5eAaPmAgNnS	rozejít
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
zpravidla	zpravidla	k6eAd1	zpravidla
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
církve	církev	k1gFnPc1	církev
nebo	nebo	k8xC	nebo
denominace	denominace	k1gFnPc1	denominace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
křesťanů	křesťan	k1gMnPc2	křesťan
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
církví	církev	k1gFnPc2	církev
<g/>
:	:	kIx,	:
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
církví	církev	k1gFnPc2	církev
vzešlých	vzešlý	k2eAgFnPc2d1	vzešlá
z	z	k7c2	z
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
církve	církev	k1gFnPc1	církev
ovšem	ovšem	k9	ovšem
různě	různě	k6eAd1	různě
chápou	chápat	k5eAaImIp3nP	chápat
samy	sám	k3xTgFnPc1	sám
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
své	svůj	k3xOyFgNnSc4	svůj
pochopení	pochopení	k1gNnSc4	pochopení
pravé	pravý	k2eAgFnSc2d1	pravá
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byly	být	k5eAaImAgFnP	být
tedy	tedy	k9	tedy
často	často	k6eAd1	často
konfliktní	konfliktní	k2eAgMnSc1d1	konfliktní
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
církve	církev	k1gFnPc1	církev
nějak	nějak	k6eAd1	nějak
podílely	podílet	k5eAaImAgFnP	podílet
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
moci	moc	k1gFnSc6	moc
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
i	i	k9	i
násilné	násilný	k2eAgNnSc1d1	násilné
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
naopak	naopak	k6eAd1	naopak
rostou	růst	k5eAaImIp3nP	růst
ekumenické	ekumenický	k2eAgFnPc1d1	ekumenická
snahy	snaha	k1gFnPc1	snaha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
sblížení	sblížení	k1gNnSc4	sblížení
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
světovým	světový	k2eAgNnSc7d1	světové
náboženstvím	náboženství	k1gNnSc7	náboženství
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliardami	miliarda	k4xCgFnPc7	miliarda
věřících	věřící	k1gFnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
křesťanství	křesťanství	k1gNnSc1	křesťanství
mělo	mít	k5eAaImAgNnS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
antickým	antický	k2eAgNnSc7d1	antické
kulturním	kulturní	k2eAgNnSc7d1	kulturní
dědictvím	dědictví	k1gNnSc7	dědictví
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
formování	formování	k1gNnSc4	formování
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
je	být	k5eAaImIp3nS	být
monoteistické	monoteistický	k2eAgNnSc1d1	monoteistické
a	a	k8xC	a
univerzální	univerzální	k2eAgNnSc1d1	univerzální
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
ke	k	k7c3	k
každému	každý	k3xTgMnSc3	každý
člověku	člověk	k1gMnSc3	člověk
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
k	k	k7c3	k
jednotlivé	jednotlivý	k2eAgFnSc3d1	jednotlivá
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
příslušnost	příslušnost	k1gFnSc1	příslušnost
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
není	být	k5eNaImIp3nS	být
předem	předem	k6eAd1	předem
dána	dát	k5eAaPmNgFnS	dát
původem	původ	k1gInSc7	původ
<g/>
,	,	kIx,	,
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
vymezovat	vymezovat	k5eAaImF	vymezovat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
přijetím	přijetí	k1gNnSc7	přijetí
jistého	jistý	k2eAgNnSc2d1	jisté
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
také	také	k9	také
náboženství	náboženství	k1gNnSc4	náboženství
historické	historický	k2eAgNnSc4d1	historické
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
života	život	k1gInSc2	život
a	a	k8xC	a
působení	působení	k1gNnSc2	působení
určité	určitý	k2eAgFnSc2d1	určitá
historické	historický	k2eAgFnSc2d1	historická
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
křesťané	křesťan	k1gMnPc1	křesťan
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
jako	jako	k9	jako
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
svým	svůj	k3xOyFgNnSc7	svůj
původem	původ	k1gInSc7	původ
křesťanství	křesťanství	k1gNnSc2	křesťanství
vědomě	vědomě	k6eAd1	vědomě
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
židovské	židovský	k2eAgNnSc4d1	Židovské
náboženství	náboženství	k1gNnSc4	náboženství
(	(	kIx(	(
<g/>
judaismus	judaismus	k1gInSc1	judaismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
kdysi	kdysi	k6eAd1	kdysi
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
a	a	k8xC	a
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
sdílí	sdílet	k5eAaImIp3nS	sdílet
mnoho	mnoho	k4c4	mnoho
rysů	rys	k1gInPc2	rys
<g/>
:	:	kIx,	:
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
jednoho	jeden	k4xCgMnSc4	jeden
Boha	bůh	k1gMnSc4	bůh
Stvořitele	Stvořitel	k1gMnSc4	Stvořitel
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
jeho	jeho	k3xOp3gNnSc1	jeho
zjevení	zjevení	k1gNnSc1	zjevení
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
jeho	jeho	k3xOp3gInSc4	jeho
poslední	poslední	k2eAgInSc4d1	poslední
soud	soud	k1gInSc4	soud
a	a	k8xC	a
slaví	slavit	k5eAaImIp3nP	slavit
řadu	řada	k1gFnSc4	řada
podobných	podobný	k2eAgInPc2d1	podobný
svátků	svátek	k1gInPc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
židovství	židovství	k1gNnSc1	židovství
klade	klást	k5eAaImIp3nS	klást
také	také	k9	také
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
události	událost	k1gFnPc4	událost
izraelských	izraelský	k2eAgFnPc2d1	izraelská
i	i	k8xC	i
vlastních	vlastní	k2eAgFnPc2d1	vlastní
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
vidí	vidět	k5eAaImIp3nS	vidět
součást	součást	k1gFnSc4	součást
dějin	dějiny	k1gFnPc2	dějiny
spásy	spása	k1gFnSc2	spása
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
χ	χ	k?	χ
christianismos	christianismos	k1gInSc1	christianismos
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
listech	list	k1gInPc6	list
syrského	syrský	k2eAgInSc2d1	syrský
biskupa	biskup	k1gInSc2	biskup
Ignáce	Ignác	k1gMnSc2	Ignác
z	z	k7c2	z
Antiochie	Antiochie	k1gFnSc2	Antiochie
(	(	kIx(	(
<g/>
†	†	k?	†
asi	asi	k9	asi
106	[number]	k4	106
<g/>
)	)	kIx)	)
a	a	k8xC	a
Skutky	skutek	k1gInPc1	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
líčí	líčit	k5eAaImIp3nP	líčit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
Antiochii	Antiochie	k1gFnSc6	Antiochie
dostali	dostat	k5eAaPmAgMnP	dostat
Ježíšovi	Ježíšův	k2eAgMnPc1d1	Ježíšův
stoupenci	stoupenec	k1gMnPc1	stoupenec
poprvé	poprvé	k6eAd1	poprvé
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vlastně	vlastně	k9	vlastně
"	"	kIx"	"
<g/>
kristovci	kristovec	k1gMnSc6	kristovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
např.	např.	kA	např.
německy	německy	k6eAd1	německy
Christen	Christen	k2eAgInSc1d1	Christen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
Krédo	Krédo	k1gNnSc1	Krédo
<g/>
,	,	kIx,	,
Teologie	teologie	k1gFnSc1	teologie
a	a	k8xC	a
Bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
určitým	určitý	k2eAgInPc3d1	určitý
rozdílům	rozdíl	k1gInPc3	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
církvemi	církev	k1gFnPc7	církev
se	se	k3xPyFc4	se
křesťané	křesťan	k1gMnPc1	křesťan
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
následujících	následující	k2eAgInPc6d1	následující
bodech	bod	k1gInPc6	bod
<g/>
:	:	kIx,	:
Jest	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
věčný	věčný	k2eAgInSc1d1	věčný
a	a	k8xC	a
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
osobách	osoba	k1gFnPc6	osoba
jako	jako	k8xC	jako
Bůh	bůh	k1gMnSc1	bůh
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Syn	syn	k1gMnSc1	syn
a	a	k8xC	a
Duch	duch	k1gMnSc1	duch
svatý	svatý	k1gMnSc1	svatý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
sdílejí	sdílet	k5eAaImIp3nP	sdílet
jediné	jediný	k2eAgNnSc4d1	jediné
božství	božství	k1gNnSc4	božství
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
však	však	k9	však
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
celý	celý	k2eAgInSc1d1	celý
vesmír	vesmír	k1gInSc1	vesmír
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc7	jeho
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
dostávají	dostávat	k5eAaImIp3nP	dostávat
dar	dar	k1gInSc4	dar
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stvořeném	stvořený	k2eAgInSc6d1	stvořený
světě	svět	k1gInSc6	svět
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
hodné	hodný	k2eAgFnPc4d1	hodná
božské	božský	k2eAgFnPc4d1	božská
úcty	úcta	k1gFnPc4	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
s	s	k7c7	s
lidstvem	lidstvo	k1gNnSc7	lidstvo
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
a	a	k8xC	a
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
mu	on	k3xPp3gMnSc3	on
svou	svůj	k3xOyFgFnSc4	svůj
podstatu	podstata	k1gFnSc4	podstata
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
však	však	k9	však
narušil	narušit	k5eAaPmAgMnS	narušit
původní	původní	k2eAgInSc4d1	původní
harmonický	harmonický	k2eAgInSc4d1	harmonický
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
zhřešil	zhřešit	k5eAaPmAgInS	zhřešit
a	a	k8xC	a
propadl	propadnout	k5eAaPmAgInS	propadnout
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
však	však	k9	však
nepřestal	přestat	k5eNaPmAgMnS	přestat
o	o	k7c4	o
člověka	člověk	k1gMnSc4	člověk
pečovat	pečovat	k5eAaImF	pečovat
<g/>
,	,	kIx,	,
uzavíral	uzavírat	k5eAaPmAgInS	uzavírat
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
smlouvy	smlouva	k1gFnPc4	smlouva
(	(	kIx(	(
<g/>
Noe	Noe	k1gMnSc1	Noe
<g/>
,	,	kIx,	,
Abrahám	Abrahám	k1gMnSc1	Abrahám
<g/>
,	,	kIx,	,
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
a	a	k8xC	a
sliboval	slibovat	k5eAaImAgMnS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jednou	k6eAd1	jednou
pošle	poslat	k5eAaPmIp3nS	poslat
lidstvu	lidstvo	k1gNnSc3	lidstvo
vykupitele	vykupitel	k1gMnSc2	vykupitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
otroctví	otroctví	k1gNnSc2	otroctví
hříchu	hřích	k1gInSc2	hřích
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
vyvede	vyvést	k5eAaPmIp3nS	vyvést
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgMnSc7	tento
vykupitelem	vykupitel	k1gMnSc7	vykupitel
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
křesťanů	křesťan	k1gMnPc2	křesťan
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Bůh	bůh	k1gMnSc1	bůh
sám	sám	k3xTgMnSc1	sám
stal	stát	k5eAaPmAgMnS	stát
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
vtělení	vtělení	k1gNnSc3	vtělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
kvůli	kvůli	k7c3	kvůli
záchraně	záchrana	k1gFnSc3	záchrana
člověka	člověk	k1gMnSc2	člověk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
člověku	člověk	k1gMnSc3	člověk
přinesl	přinést	k5eAaPmAgInS	přinést
odpuštění	odpuštění	k1gNnSc4	odpuštění
a	a	k8xC	a
vykoupení	vykoupení	k1gNnSc4	vykoupení
<g/>
,	,	kIx,	,
a	a	k8xC	a
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
hříchu	hřích	k1gInSc2	hřích
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ježíše	Ježíš	k1gMnPc4	Ježíš
pak	pak	k6eAd1	pak
dle	dle	k7c2	dle
bible	bible	k1gFnSc2	bible
Bůh	bůh	k1gMnSc1	bůh
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
vzkřísil	vzkřísit	k5eAaPmAgInS	vzkřísit
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
čtyřicet	čtyřicet	k4xCc4	čtyřicet
dní	den	k1gInPc2	den
pobýval	pobývat	k5eAaImAgMnS	pobývat
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
učedníky	učedník	k1gMnPc7	učedník
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
učedníkům	učedník	k1gMnPc3	učedník
seslán	seslán	k2eAgMnSc1d1	seslán
Duch	duch	k1gMnSc1	duch
svatý	svatý	k1gMnSc1	svatý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
stále	stále	k6eAd1	stále
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
pak	pak	k6eAd1	pak
očekávají	očekávat	k5eAaImIp3nP	očekávat
druhý	druhý	k4xOgInSc4	druhý
příchod	příchod	k1gInSc4	příchod
Kristův	Kristův	k2eAgInSc4d1	Kristův
na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Kristus	Kristus	k1gMnSc1	Kristus
chápán	chápat	k5eAaImNgMnS	chápat
jako	jako	k9	jako
postava	postava	k1gFnSc1	postava
soudce	soudce	k1gMnSc2	soudce
živých	živý	k2eAgMnPc2d1	živý
i	i	k8xC	i
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Vyvoleným	vyvolený	k2eAgMnSc7d1	vyvolený
je	být	k5eAaImIp3nS	být
slíben	slíben	k2eAgInSc1d1	slíben
věčný	věčný	k2eAgInSc1d1	věčný
život	život	k1gInSc1	život
v	v	k7c6	v
blaženosti	blaženost	k1gFnSc6	blaženost
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
se	se	k3xPyFc4	se
scházejí	scházet	k5eAaImIp3nP	scházet
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místních	místní	k2eAgNnPc6d1	místní
společenstvích	společenství	k1gNnPc6	společenství
(	(	kIx(	(
<g/>
farnosti	farnost	k1gFnPc1	farnost
<g/>
,	,	kIx,	,
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc1	sbor
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
historických	historický	k2eAgInPc2d1	historický
i	i	k8xC	i
názorových	názorový	k2eAgInPc2d1	názorový
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
konfesí	konfese	k1gFnPc2	konfese
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
uloženo	uložen	k2eAgNnSc1d1	uloženo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
církev	církev	k1gFnSc1	církev
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
ekumenismus	ekumenismus	k1gInSc4	ekumenismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ekleziologie	ekleziologie	k1gFnSc1	ekleziologie
<g/>
.	.	kIx.	.
</s>
<s>
Postoj	postoj	k1gInSc1	postoj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
církví	církev	k1gFnPc2	církev
k	k	k7c3	k
druhým	druhý	k4xOgMnPc3	druhý
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
chápaní	chápaný	k2eAgMnPc1d1	chápaný
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
jediné	jediný	k2eAgFnSc3d1	jediná
Kristově	Kristův	k2eAgFnSc3d1	Kristova
Církvi	církev	k1gFnSc3	církev
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
samu	sám	k3xTgFnSc4	sám
sebe	sebe	k3xPyFc4	sebe
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
pokračovatelku	pokračovatelka	k1gFnSc4	pokračovatelka
původní	původní	k2eAgFnSc2d1	původní
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
"	"	kIx"	"
<g/>
subsistuje	subsistovat	k5eAaPmIp3nS	subsistovat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
apoštolskou	apoštolský	k2eAgFnSc4d1	apoštolská
posloupnost	posloupnost	k1gFnSc4	posloupnost
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
reformované	reformovaný	k2eAgInPc1d1	reformovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dle	dle	k7c2	dle
ní	on	k3xPp3gFnSc2	on
nejsou	být	k5eNaImIp3nP	být
církvemi	církev	k1gFnPc7	církev
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
společenství	společenství	k1gNnSc2	společenství
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
již	již	k6eAd1	již
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
blízko	blízko	k6eAd1	blízko
Kristu	Krista	k1gFnSc4	Krista
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
jako	jako	k9	jako
ke	k	k7c3	k
svatým	svatý	k2eAgFnPc3d1	svatá
mnozí	mnohý	k2eAgMnPc1d1	mnohý
křesťané	křesťan	k1gMnPc1	křesťan
obracejí	obracet	k5eAaImIp3nP	obracet
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
přímluvu	přímluva	k1gFnSc4	přímluva
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
patří	patřit	k5eAaImIp3nS	patřit
podle	podle	k7c2	podle
katolických	katolický	k2eAgFnPc2d1	katolická
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
církví	církev	k1gFnPc2	církev
Ježíšově	Ježíšův	k2eAgFnSc3d1	Ježíšova
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
Písmo	písmo	k1gNnSc1	písmo
svaté	svatá	k1gFnSc2	svatá
<g/>
,	,	kIx,	,
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
slovo	slovo	k1gNnSc1	slovo
o	o	k7c6	o
Božím	boží	k2eAgNnSc6d1	boží
jednání	jednání	k1gNnSc6	jednání
se	s	k7c7	s
světem	svět	k1gInSc7	svět
v	v	k7c6	v
Ježíši	Ježíš	k1gMnSc3	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
zdrojem	zdroj	k1gInSc7	zdroj
a	a	k8xC	a
poslední	poslední	k2eAgFnSc7d1	poslední
normou	norma	k1gFnSc7	norma
jejich	jejich	k3xOp3gNnSc2	jejich
učení	učení	k1gNnSc2	učení
i	i	k8xC	i
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
etika	etika	k1gFnSc1	etika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechna	všechen	k3xTgNnPc1	všechen
univerzální	univerzální	k2eAgNnPc1d1	univerzální
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
klade	klást	k5eAaImIp3nS	klást
i	i	k9	i
křesťanství	křesťanství	k1gNnSc1	křesťanství
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
věřícího	věřící	k2eAgInSc2d1	věřící
určité	určitý	k2eAgInPc4d1	určitý
nároky	nárok	k1gInPc4	nárok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jednak	jednak	k8xC	jednak
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
vůči	vůči	k7c3	vůči
druhým	druhý	k4xOgInSc7	druhý
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
morálky	morálka	k1gFnSc2	morálka
je	být	k5eAaImIp3nS	být
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
horská	horský	k2eAgFnSc1d1	horská
řeč	řeč	k1gFnSc1	řeč
a	a	k8xC	a
blahoslavenství	blahoslavenství	k1gNnSc1	blahoslavenství
<g/>
.	.	kIx.	.
</s>
<s>
Plněním	plnění	k1gNnSc7	plnění
těchto	tento	k3xDgInPc2	tento
nároků	nárok	k1gInPc2	nárok
si	se	k3xPyFc3	se
však	však	k9	však
křesťan	křesťan	k1gMnSc1	křesťan
nezískává	získávat	k5eNaImIp3nS	získávat
nějaký	nějaký	k3yIgInSc4	nějaký
nárok	nárok	k1gInSc4	nárok
vůči	vůči	k7c3	vůči
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
naopak	naopak	k6eAd1	naopak
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
svoji	svůj	k3xOyFgFnSc4	svůj
vděčnost	vděčnost	k1gFnSc4	vděčnost
za	za	k7c4	za
dar	dar	k1gInSc4	dar
života	život	k1gInSc2	život
a	a	k8xC	a
spásy	spása	k1gFnSc2	spása
<g/>
,	,	kIx,	,
splácí	splácet	k5eAaImIp3nS	splácet
své	svůj	k3xOyFgInPc4	svůj
dluhy	dluh	k1gInPc4	dluh
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
prosté	prostý	k2eAgInPc1d1	prostý
nároky	nárok	k1gInPc1	nárok
Mojžíšova	Mojžíšův	k2eAgNnSc2d1	Mojžíšovo
Desatera	desatero	k1gNnSc2	desatero
přikázání	přikázání	k1gNnSc2	přikázání
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Ježíšově	Ježíšův	k2eAgInSc6d1	Ježíšův
výkladu	výklad	k1gInSc6	výklad
prakticky	prakticky	k6eAd1	prakticky
nesplnitelné	splnitelný	k2eNgNnSc4d1	nesplnitelné
<g/>
,	,	kIx,	,
křesťan	křesťan	k1gMnSc1	křesťan
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
spoléhat	spoléhat	k5eAaImF	spoléhat
jen	jen	k9	jen
na	na	k7c4	na
Boží	boží	k2eAgNnSc4d1	boží
odpuštění	odpuštění	k1gNnSc4	odpuštění
a	a	k8xC	a
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
příkazy	příkaz	k1gInPc1	příkaz
proto	proto	k8xC	proto
Ježíš	Ježíš	k1gMnSc1	Ježíš
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
přikázání	přikázání	k1gNnPc2	přikázání
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
povinnost	povinnost	k1gFnSc4	povinnost
odpouštět	odpouštět	k5eAaImF	odpouštět
<g/>
:	:	kIx,	:
jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
doufat	doufat	k5eAaImF	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
odpuštěno	odpustit	k5eAaPmNgNnS	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Církev	církev	k1gFnSc1	církev
a	a	k8xC	a
Svátosti	svátost	k1gFnPc1	svátost
<g/>
.	.	kIx.	.
</s>
<s>
Jakkoli	jakkoli	k8xS	jakkoli
se	se	k3xPyFc4	se
praxe	praxe	k1gFnSc1	praxe
různých	různý	k2eAgFnPc2d1	různá
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
od	od	k7c2	od
sebe	se	k3xPyFc2	se
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
shodují	shodovat	k5eAaImIp3nP	shodovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
stává	stávat	k5eAaImIp3nS	stávat
křesťanem	křesťan	k1gMnSc7	křesťan
přijetím	přijetí	k1gNnSc7	přijetí
křtu	křest	k1gInSc2	křest
jako	jako	k8xS	jako
viditelného	viditelný	k2eAgNnSc2d1	viditelné
znamení	znamení	k1gNnSc2	znamení
odpuštění	odpuštění	k1gNnSc2	odpuštění
a	a	k8xC	a
vykoupení	vykoupení	k1gNnSc2	vykoupení
v	v	k7c6	v
Ježíši	Ježíš	k1gMnSc6	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Křesťan	Křesťan	k1gMnSc1	Křesťan
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
účastnit	účastnit	k5eAaImF	účastnit
života	život	k1gInSc2	život
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
milovat	milovat	k5eAaImF	milovat
bližní	bližní	k1gMnPc4	bližní
<g/>
,	,	kIx,	,
odpouštět	odpouštět	k5eAaImF	odpouštět
jim	on	k3xPp3gMnPc3	on
a	a	k8xC	a
šířit	šířit	k5eAaImF	šířit
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
je	být	k5eAaImIp3nS	být
slavení	slavení	k1gNnSc3	slavení
eucharistie	eucharistie	k1gFnSc2	eucharistie
<g/>
,	,	kIx,	,
připomínka	připomínka	k1gFnSc1	připomínka
či	či	k8xC	či
obnovení	obnovení	k1gNnSc1	obnovení
Večeře	večeře	k1gFnSc2	večeře
Páně	páně	k2eAgMnSc1d1	páně
<g/>
,,	,,	k?	,,
kterou	který	k3yQgFnSc7	který
většina	většina	k1gFnSc1	většina
křesťanů	křesťan	k1gMnPc2	křesťan
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
svátost	svátost	k1gFnSc1	svátost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
viditelné	viditelný	k2eAgNnSc1d1	viditelné
znamení	znamení	k1gNnSc1	znamení
Ježíšovy	Ježíšův	k2eAgFnSc2d1	Ježíšova
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Svátostí	svátost	k1gFnSc7	svátost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
také	také	k9	také
manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
v	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
církvích	církev	k1gFnPc6	církev
i	i	k9	i
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
<g/>
,	,	kIx,	,
svátost	svátost	k1gFnSc1	svátost
smíření	smíření	k1gNnSc2	smíření
<g/>
,	,	kIx,	,
biřmování	biřmování	k1gNnSc2	biřmování
a	a	k8xC	a
svátost	svátost	k1gFnSc1	svátost
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
křesťané	křesťan	k1gMnPc1	křesťan
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
náboženských	náboženský	k2eAgFnPc6d1	náboženská
činnostech	činnost	k1gFnPc6	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgFnPc4d1	společná
modlitby	modlitba	k1gFnPc4	modlitba
<g/>
,	,	kIx,	,
pouti	pouť	k1gFnSc6	pouť
a	a	k8xC	a
zejména	zejména	k9	zejména
charitativní	charitativní	k2eAgFnSc4d1	charitativní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
často	často	k6eAd1	často
jako	jako	k9	jako
první	první	k4xOgFnPc1	první
zavedly	zavést	k5eAaPmAgFnP	zavést
řeholní	řeholní	k2eAgInPc4d1	řeholní
řády	řád	k1gInPc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Špitály	špitál	k1gInPc1	špitál
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
nemocnice	nemocnice	k1gFnPc1	nemocnice
<g/>
,	,	kIx,	,
útulky	útulek	k1gInPc1	útulek
pro	pro	k7c4	pro
sirotky	sirotka	k1gFnPc4	sirotka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
staré	starý	k2eAgMnPc4d1	starý
a	a	k8xC	a
opuštěné	opuštěný	k2eAgMnPc4d1	opuštěný
lidi	člověk	k1gMnPc4	člověk
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
různě	různě	k6eAd1	různě
postižené	postižený	k2eAgFnPc1d1	postižená
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
dlouho	dlouho	k6eAd1	dlouho
doménou	doména	k1gFnSc7	doména
církevních	církevní	k2eAgFnPc2d1	církevní
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
sdružení	sdružení	k1gNnPc2	sdružení
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kongregací	kongregace	k1gFnSc7	kongregace
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
organizované	organizovaný	k2eAgNnSc1d1	organizované
školství	školství	k1gNnSc1	školství
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
církvemi	církev	k1gFnPc7	církev
(	(	kIx(	(
<g/>
věnovali	věnovat	k5eAaPmAgMnP	věnovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zejména	zejména	k9	zejména
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
piaristé	piarista	k1gMnPc1	piarista
nebo	nebo	k8xC	nebo
voršilky	voršilka	k1gFnPc1	voršilka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
zemích	zem	k1gFnPc6	zem
církve	církev	k1gFnSc2	církev
podporovaly	podporovat	k5eAaImAgInP	podporovat
také	také	k9	také
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
i	i	k8xC	i
hudebních	hudební	k2eAgNnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
věnována	věnovat	k5eAaImNgFnS	věnovat
náboženským	náboženský	k2eAgNnPc3d1	náboženské
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1	řecká
symbolon	symbolon	k1gInSc4	symbolon
i	i	k8xC	i
latinské	latinský	k2eAgNnSc4d1	latinské
symbolum	symbolum	k1gNnSc4	symbolum
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
shrnutí	shrnutí	k1gNnSc2	shrnutí
<g/>
"	"	kIx"	"
znamenají	znamenat	k5eAaImIp3nP	znamenat
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
krédo	krédo	k1gNnSc1	krédo
<g/>
,	,	kIx,	,
stručné	stručný	k2eAgNnSc1d1	stručné
shrnutí	shrnutí	k1gNnSc1	shrnutí
obsahu	obsah	k1gInSc2	obsah
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
apoštolské	apoštolský	k2eAgNnSc4d1	apoštolské
a	a	k8xC	a
nicejsko-konstantinopolské	nicejskoonstantinopolský	k2eAgNnSc4d1	nicejsko-konstantinopolské
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgFnSc1	obojí
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
reformačních	reformační	k2eAgInPc2d1	reformační
augsburské	augsburský	k2eAgNnSc4d1	Augsburské
vyznání	vyznání	k1gNnSc4	vyznání
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
konfese	konfese	k1gFnSc1	konfese
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
výtvarným	výtvarný	k2eAgInSc7d1	výtvarný
symbolem	symbol	k1gInSc7	symbol
křesťanství	křesťanství	k1gNnSc2	křesťanství
je	být	k5eAaImIp3nS	být
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
znamená	znamenat	k5eAaImIp3nS	znamenat
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgInSc7d1	starý
symbolem	symbol	k1gInSc7	symbol
je	být	k5eAaImIp3nS	být
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
ἰ	ἰ	k?	ἰ
ichthys	ichthysa	k1gFnPc2	ichthysa
(	(	kIx(	(
<g/>
ryba	ryba	k1gFnSc1	ryba
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
akrostich	akrostich	k1gInSc4	akrostich
spojení	spojení	k1gNnSc2	spojení
Ἰ	Ἰ	k?	Ἰ
Χ	Χ	k?	Χ
Θ	Θ	k?	Θ
Υ	Υ	k?	Υ
Σ	Σ	k?	Σ
(	(	kIx(	(
<g/>
Iésús	Iésús	k1gInSc1	Iésús
Christos	Christos	k1gMnSc1	Christos
Theú	Theú	k1gMnSc1	Theú
hyios	hyios	k1gMnSc1	hyios
Sótér	Sótér	k1gMnSc1	Sótér
<g/>
:	:	kIx,	:
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
Syn	syn	k1gMnSc1	syn
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
Spasitel	spasitel	k1gMnSc1	spasitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
symboly	symbol	k1gInPc7	symbol
jsou	být	k5eAaImIp3nP	být
řecká	řecký	k2eAgNnPc4d1	řecké
písmena	písmeno	k1gNnPc4	písmeno
A	a	k8xC	a
Ω	Ω	k?	Ω
(	(	kIx(	(
<g/>
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
ómega	ómega	k1gFnSc1	ómega
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
písmeno	písmeno	k1gNnSc4	písmeno
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
Krista	Krista	k1gFnSc1	Krista
jako	jako	k9	jako
"	"	kIx"	"
<g/>
počátek	počátek	k1gInSc4	počátek
i	i	k8xC	i
konec	konec	k1gInSc4	konec
<g/>
"	"	kIx"	"
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
kniha	kniha	k1gFnSc1	kniha
Zjevení	zjevení	k1gNnSc2	zjevení
<g/>
,	,	kIx,	,
Kristův	Kristův	k2eAgInSc1d1	Kristův
monogram	monogram	k1gInSc1	monogram
XP	XP	kA	XP
nebo	nebo	k8xC	nebo
IHS	IHS	kA	IHS
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
IΗ	IΗ	k1gMnSc2	IΗ
<g/>
,	,	kIx,	,
Iésús	Iésús	k1gInSc1	Iésús
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
beránek	beránek	k1gInSc1	beránek
<g/>
,	,	kIx,	,
kalich	kalich	k1gInSc1	kalich
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
z	z	k7c2	z
nepříliš	příliš	k6eNd1	příliš
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
veřejného	veřejný	k2eAgNnSc2d1	veřejné
působení	působení	k1gNnSc2	působení
židovského	židovský	k2eAgMnSc2d1	židovský
proroka	prorok	k1gMnSc2	prorok
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
řemeslnické	řemeslnický	k2eAgFnSc2d1	řemeslnická
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
galilejského	galilejský	k2eAgInSc2d1	galilejský
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
věku	věk	k1gInSc6	věk
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
pocestný	pocestný	k1gMnSc1	pocestný
kazatel	kazatel	k1gMnSc1	kazatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
též	též	k9	též
uzdravoval	uzdravovat	k5eAaImAgMnS	uzdravovat
nemocné	nemocný	k2eAgNnSc4d1	nemocný
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
skupinu	skupina	k1gFnSc4	skupina
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
a	a	k8xC	a
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
mu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
Ježíšových	Ježíšův	k2eAgMnPc2d1	Ježíšův
přívrženců	přívrženec	k1gMnPc2	přívrženec
tvořilo	tvořit	k5eAaImAgNnS	tvořit
"	"	kIx"	"
<g/>
dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
"	"	kIx"	"
učedníků	učedník	k1gMnPc2	učedník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
apoštolové	apoštol	k1gMnPc1	apoštol
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
smrti	smrt	k1gFnSc6	smrt
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
důležité	důležitý	k2eAgNnSc4d1	důležité
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vznikajícím	vznikající	k2eAgNnSc6d1	vznikající
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
zakladatelem	zakladatel	k1gMnSc7	zakladatel
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
reformu	reforma	k1gFnSc4	reforma
židovství	židovství	k1gNnSc2	židovství
<g/>
.	.	kIx.	.
</s>
<s>
Kázal	kázat	k5eAaImAgMnS	kázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
dějinný	dějinný	k2eAgInSc1d1	dějinný
zvrat	zvrat	k1gInSc1	zvrat
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
bude	být	k5eAaImBp3nS	být
nastoleno	nastolen	k2eAgNnSc1d1	nastoleno
boží	boží	k2eAgNnSc1d1	boží
království	království	k1gNnSc1	království
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
musí	muset	k5eAaImIp3nP	muset
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Blížící	blížící	k2eAgMnSc1d1	blížící
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ba	ba	k9	ba
již	již	k6eAd1	již
začínající	začínající	k2eAgNnSc1d1	začínající
boží	boží	k2eAgNnSc1d1	boží
království	království	k1gNnSc1	království
v	v	k7c6	v
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
podání	podání	k1gNnSc6	podání
znamenalo	znamenat	k5eAaImAgNnS	znamenat
také	také	k6eAd1	také
radikalizaci	radikalizace	k1gFnSc3	radikalizace
etiky	etika	k1gFnSc2	etika
<g/>
:	:	kIx,	:
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
dobovém	dobový	k2eAgInSc6d1	dobový
smyslu	smysl	k1gInSc6	smysl
přísnějšího	přísný	k2eAgNnSc2d2	přísnější
plnění	plnění	k1gNnSc2	plnění
náboženských	náboženský	k2eAgInPc2d1	náboženský
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
celkové	celkový	k2eAgFnPc1d1	celková
změny	změna	k1gFnPc1	změna
smýšlení	smýšlení	k1gNnSc2	smýšlení
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
obrácení	obrácení	k1gNnSc1	obrácení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
důrazu	důraz	k1gInSc2	důraz
na	na	k7c6	na
odpuštění	odpuštění	k1gNnSc6	odpuštění
<g/>
,	,	kIx,	,
smíření	smíření	k1gNnSc6	smíření
a	a	k8xC	a
účinnou	účinný	k2eAgFnSc4d1	účinná
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
i	i	k8xC	i
k	k	k7c3	k
druhým	druhý	k4xOgMnPc3	druhý
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blahoslavenstvích	blahoslavenství	k1gNnPc6	blahoslavenství
představoval	představovat	k5eAaImAgMnS	představovat
Ježíš	Ježíš	k1gMnSc1	Ježíš
boží	boží	k2eAgNnSc4d1	boží
království	království	k1gNnSc4	království
jako	jako	k8xS	jako
naději	naděje	k1gFnSc4	naděje
chudých	chudý	k2eAgFnPc2d1	chudá
<g/>
,	,	kIx,	,
truchlících	truchlící	k2eAgFnPc2d1	truchlící
<g/>
,	,	kIx,	,
pronásledovaných	pronásledovaný	k2eAgFnPc2d1	pronásledovaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobenstvích	podobenství	k1gNnPc6	podobenství
(	(	kIx(	(
<g/>
metaforických	metaforický	k2eAgInPc6d1	metaforický
příbězích	příběh	k1gInPc6	příběh
<g/>
)	)	kIx)	)
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
praktické	praktický	k2eAgFnSc2d1	praktická
stránky	stránka	k1gFnSc2	stránka
tohoto	tento	k3xDgNnSc2	tento
království	království	k1gNnSc2	království
a	a	k8xC	a
charismatickým	charismatický	k2eAgNnSc7d1	charismatické
léčitelstvím	léčitelství	k1gNnSc7	léčitelství
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
jeho	jeho	k3xOp3gFnSc4	jeho
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
působení	působení	k1gNnSc1	působení
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
30	[number]	k4	30
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
judského	judský	k2eAgNnSc2d1	Judské
města	město	k1gNnSc2	město
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
na	na	k7c4	na
židovské	židovský	k2eAgInPc4d1	židovský
svátky	svátek	k1gInPc4	svátek
Pesach	pesach	k1gInSc1	pesach
<g/>
,	,	kIx,	,
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
posvátného	posvátný	k2eAgNnSc2d1	posvátné
města	město	k1gNnSc2	město
judských	judský	k2eAgMnPc2d1	judský
králů	král	k1gMnPc2	král
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
podle	podle	k7c2	podle
Zachariášova	Zachariášův	k2eAgNnSc2d1	Zachariášovo
proroctví	proroctví	k1gNnSc2	proroctví
jako	jako	k8xS	jako
triumfální	triumfální	k2eAgInSc1d1	triumfální
vjezd	vjezd	k1gInSc1	vjezd
očekávaného	očekávaný	k2eAgMnSc2d1	očekávaný
Mesiáše	Mesiáš	k1gMnSc2	Mesiáš
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
vystupování	vystupování	k1gNnPc1	vystupování
však	však	k9	však
náboženské	náboženský	k2eAgFnSc2d1	náboženská
i	i	k8xC	i
politické	politický	k2eAgFnSc2d1	politická
autority	autorita	k1gFnSc2	autorita
vyhodnotily	vyhodnotit	k5eAaPmAgInP	vyhodnotit
jako	jako	k9	jako
podvratné	podvratný	k2eAgInPc1d1	podvratný
a	a	k8xC	a
Ježíš	ježit	k5eAaImIp2nS	ježit
byl	být	k5eAaImAgMnS	být
brzy	brzy	k6eAd1	brzy
zadržen	zadržet	k5eAaPmNgMnS	zadržet
<g/>
,	,	kIx,	,
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
a	a	k8xC	a
popraven	popravit	k5eAaPmNgInS	popravit
ukřižováním	ukřižování	k1gNnSc7	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
evangelií	evangelium	k1gNnPc2	evangelium
ho	on	k3xPp3gInSc4	on
však	však	k9	však
Bůh	bůh	k1gMnSc1	bůh
vzkřísil	vzkřísit	k5eAaPmAgMnS	vzkřísit
a	a	k8xC	a
Ježíšovi	Ježíšův	k2eAgMnPc1d1	Ježíšův
žáci	žák	k1gMnPc1	žák
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
několikrát	několikrát	k6eAd1	několikrát
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
v	v	k7c4	v
Ježíše	Ježíš	k1gMnSc4	Ježíš
jako	jako	k8xC	jako
Krista	Kristus	k1gMnSc4	Kristus
uvěřili	uvěřit	k5eAaPmAgMnP	uvěřit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
scházeli	scházet	k5eAaImAgMnP	scházet
a	a	k8xC	a
sdružovali	sdružovat	k5eAaImAgMnP	sdružovat
ve	v	k7c6	v
sborech	sbor	k1gInPc6	sbor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgInPc4	jenž
přijali	přijmout	k5eAaPmAgMnP	přijmout
běžné	běžný	k2eAgNnSc4d1	běžné
řecké	řecký	k2eAgNnSc4d1	řecké
označení	označení	k1gNnSc4	označení
ekklésiá	ekklésiá	k1gFnSc1	ekklésiá
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgNnSc1d1	veřejné
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
uspořádání	uspořádání	k1gNnSc6	uspořádání
prvních	první	k4xOgFnPc2	první
církevních	církevní	k2eAgFnPc2d1	církevní
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
zejména	zejména	k9	zejména
z	z	k7c2	z
listů	list	k1gInPc2	list
apoštola	apoštol	k1gMnSc2	apoštol
Pavla	Pavel	k1gMnSc2	Pavel
a	a	k8xC	a
z	z	k7c2	z
popisu	popis	k1gInSc2	popis
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Skutků	skutek	k1gInPc2	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jaké	jaký	k3yIgFnSc2	jaký
míry	míra	k1gFnSc2	míra
se	se	k3xPyFc4	se
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
církve	církev	k1gFnSc2	církev
přičinil	přičinit	k5eAaPmAgMnS	přičinit
právě	právě	k9	právě
Pavel	Pavel	k1gMnSc1	Pavel
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
předmětem	předmět	k1gInSc7	předmět
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
,	,	kIx,	,
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
obce	obec	k1gFnPc1	obec
však	však	k9	však
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
například	například	k6eAd1	například
Tomáš	Tomáš	k1gMnSc1	Tomáš
Halík	Halík	k1gMnSc1	Halík
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
Pavla	Pavel	k1gMnSc2	Pavel
by	by	kYmCp3nS	by
křesťanství	křesťanství	k1gNnSc1	křesťanství
zřejmě	zřejmě	k6eAd1	zřejmě
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
sekt	sekta	k1gFnPc2	sekta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
židovství	židovství	k1gNnSc2	židovství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
jde	jít	k5eAaImIp3nS	jít
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
provokativně	provokativně	k6eAd1	provokativně
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pavel	Pavel	k1gMnSc1	Pavel
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgMnSc7	ten
nejvlastnějším	vlastní	k2eAgMnSc7d3	nejvlastnější
tvůrcem	tvůrce	k1gMnSc7	tvůrce
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
křesťanství	křesťanství	k1gNnSc4	křesťanství
od	od	k7c2	od
židovství	židovství	k1gNnSc2	židovství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
Pavlovu	Pavlův	k2eAgFnSc4d1	Pavlova
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
údajně	údajně	k6eAd1	údajně
hlásá	hlásat	k5eAaImIp3nS	hlásat
svobodu	svoboda	k1gFnSc4	svoboda
od	od	k7c2	od
Zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
Pěti	pět	k4xCc6	pět
knihách	kniha	k1gFnPc6	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
závazné	závazný	k2eAgFnPc1d1	závazná
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
pojetím	pojetí	k1gNnSc7	pojetí
Pavlovy	Pavlův	k2eAgFnSc2d1	Pavlova
teologie	teologie	k1gFnSc2	teologie
ale	ale	k8xC	ale
zásadně	zásadně	k6eAd1	zásadně
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
vyznavači	vyznavač	k1gMnPc7	vyznavač
mesiánského	mesiánský	k2eAgInSc2d1	mesiánský
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
křesťanů	křesťan	k1gMnPc2	křesťan
předávala	předávat	k5eAaImAgFnS	předávat
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
pouze	pouze	k6eAd1	pouze
ústním	ústní	k2eAgNnSc7d1	ústní
podáním	podání	k1gNnSc7	podání
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
se	se	k3xPyFc4	se
však	však	k9	však
–	–	k?	–
především	především	k6eAd1	především
díky	dík	k1gInPc7	dík
odvážné	odvážný	k2eAgFnSc2d1	odvážná
misijní	misijní	k2eAgFnSc2d1	misijní
činnosti	činnost	k1gFnSc2	činnost
Pavla	Pavel	k1gMnSc2	Pavel
z	z	k7c2	z
Tarsu	Tars	k1gInSc2	Tars
-	-	kIx~	-
šířilo	šířit	k5eAaImAgNnS	šířit
i	i	k9	i
do	do	k7c2	do
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
chtělo	chtít	k5eAaImAgNnS	chtít
udržet	udržet	k5eAaPmF	udržet
jistou	jistý	k2eAgFnSc4d1	jistá
názorovou	názorový	k2eAgFnSc4d1	názorová
i	i	k8xC	i
praktickou	praktický	k2eAgFnSc4d1	praktická
jednotu	jednota	k1gFnSc4	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
potřeba	potřeba	k6eAd1	potřeba
jednak	jednak	k8xC	jednak
uchovávat	uchovávat	k5eAaImF	uchovávat
příležitostné	příležitostný	k2eAgInPc4d1	příležitostný
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
listy	list	k1gInPc4	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
zachytit	zachytit	k5eAaPmF	zachytit
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
působení	působení	k1gNnSc4	působení
písemně	písemně	k6eAd1	písemně
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čtyři	čtyři	k4xCgNnPc4	čtyři
evangelia	evangelium	k1gNnPc4	evangelium
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
Ježíšově	Ježíšův	k2eAgNnSc6d1	Ježíšovo
působení	působení	k1gNnSc6	působení
<g/>
,	,	kIx,	,
sepsané	sepsaný	k2eAgInPc1d1	sepsaný
zřejmě	zřejmě	k6eAd1	zřejmě
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
60	[number]	k4	60
a	a	k8xC	a
90	[number]	k4	90
<g/>
,	,	kIx,	,
Skutky	skutek	k1gInPc4	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
s	s	k7c7	s
vylíčením	vylíčení	k1gNnSc7	vylíčení
počátků	počátek	k1gInPc2	počátek
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
listy	list	k1gInPc1	list
apoštola	apoštol	k1gMnSc2	apoštol
Pavla	Pavel	k1gMnSc2	Pavel
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
<g/>
,	,	kIx,	,
Zjevení	zjevení	k1gNnSc1	zjevení
Janovo	Janův	k2eAgNnSc1d1	Janovo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
křesťané	křesťan	k1gMnPc1	křesťan
dostávali	dostávat	k5eAaImAgMnP	dostávat
do	do	k7c2	do
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
židovskými	židovský	k2eAgFnPc7d1	židovská
obcemi	obec	k1gFnPc7	obec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
Pavel	Pavel	k1gMnSc1	Pavel
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
poselství	poselství	k1gNnSc1	poselství
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
i	i	k9	i
jiným	jiný	k2eAgInPc3d1	jiný
národům	národ	k1gInPc3	národ
a	a	k8xC	a
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
nezavazuje	zavazovat	k5eNaImIp3nS	zavazovat
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
židovské	židovský	k2eAgFnSc2d1	židovská
náboženské	náboženský	k2eAgFnSc2d1	náboženská
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
křesťanství	křesťanství	k1gNnSc2	křesťanství
se	se	k3xPyFc4	se
také	také	k9	také
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
společným	společný	k2eAgInSc7d1	společný
jazykem	jazyk	k1gInSc7	jazyk
křesťanů	křesťan	k1gMnPc2	křesťan
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
řečtina	řečtina	k1gFnSc1	řečtina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
oddělení	oddělení	k1gNnSc1	oddělení
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
křesťanství	křesťanství	k1gNnSc4	křesťanství
s	s	k7c7	s
judaismem	judaismus	k1gInSc7	judaismus
spjato	spjat	k2eAgNnSc1d1	spjato
společnou	společný	k2eAgFnSc7d1	společná
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
jednoho	jeden	k4xCgMnSc4	jeden
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
uznáním	uznání	k1gNnSc7	uznání
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
židovské	židovský	k2eAgFnSc2d1	židovská
bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navázáním	navázání	k1gNnSc7	navázání
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
mravní	mravní	k2eAgInPc4d1	mravní
postoje	postoj	k1gInPc4	postoj
<g/>
;	;	kIx,	;
také	také	k9	také
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
liturgie	liturgie	k1gFnSc1	liturgie
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
židovské	židovský	k2eAgFnSc2d1	židovská
synagogální	synagogální	k2eAgFnSc2d1	synagogální
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sociologického	sociologický	k2eAgNnSc2d1	sociologické
hlediska	hledisko	k1gNnSc2	hledisko
byly	být	k5eAaImAgFnP	být
počátky	počátek	k1gInPc4	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
podobné	podobný	k2eAgFnPc4d1	podobná
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
sekt	sekta	k1gFnPc2	sekta
<g/>
:	:	kIx,	:
Jediný	jediný	k2eAgMnSc1d1	jediný
vůdce	vůdce	k1gMnSc1	vůdce
okolo	okolo	k7c2	okolo
sebe	se	k3xPyFc2	se
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
stoupence	stoupenec	k1gMnPc4	stoupenec
a	a	k8xC	a
hlásal	hlásat	k5eAaImAgMnS	hlásat
své	svůj	k3xOyFgFnPc4	svůj
vize	vize	k1gFnPc4	vize
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
se	se	k3xPyFc4	se
však	však	k9	však
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgFnPc3d1	jiná
tehdejším	tehdejší	k2eAgFnPc3d1	tehdejší
sektám	sekta	k1gFnPc3	sekta
a	a	k8xC	a
směrům	směr	k1gInPc3	směr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Juda	judo	k1gNnPc4	judo
Makabejský	makabejský	k2eAgInSc1d1	makabejský
<g/>
)	)	kIx)	)
také	také	k9	také
odlišilo	odlišit	k5eAaPmAgNnS	odlišit
<g/>
:	:	kIx,	:
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
za	za	k7c2	za
Mesiáše	Mesiáš	k1gMnSc2	Mesiáš
prohlašovali	prohlašovat	k5eAaImAgMnP	prohlašovat
i	i	k9	i
jiní	jiný	k1gMnPc1	jiný
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
obvykle	obvykle	k6eAd1	obvykle
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
i	i	k9	i
jejich	jejich	k3xOp3gFnPc1	jejich
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
křesťané	křesťan	k1gMnPc1	křesťan
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
"	"	kIx"	"
<g/>
přemohl	přemoct	k5eAaPmAgInS	přemoct
smrt	smrt	k1gFnSc4	smrt
<g/>
"	"	kIx"	"
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
ho	on	k3xPp3gInSc4	on
vzkřísil	vzkřísit	k5eAaPmAgMnS	vzkřísit
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
svědčí	svědčit	k5eAaImIp3nS	svědčit
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
zárukou	záruka	k1gFnSc7	záruka
jejich	jejich	k3xOp3gFnSc2	jejich
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
možnost	možnost	k1gFnSc4	možnost
spasení	spasení	k1gNnSc2	spasení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
svědectví	svědectví	k1gNnSc4	svědectví
evangelií	evangelium	k1gNnPc2	evangelium
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
církvi	církev	k1gFnSc6	církev
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
přítomen	přítomen	k2eAgInSc4d1	přítomen
jako	jako	k8xC	jako
živý	živý	k2eAgInSc4d1	živý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
slaví	slavit	k5eAaImIp3nP	slavit
jako	jako	k9	jako
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
svátek	svátek	k1gInSc1	svátek
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
významnější	významný	k2eAgFnPc1d2	významnější
než	než	k8xS	než
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgNnSc4d1	další
šíření	šíření	k1gNnSc4	šíření
evangelia	evangelium	k1gNnSc2	evangelium
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
postarat	postarat	k5eAaPmF	postarat
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
přímí	přímit	k5eAaImIp3nS	přímit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Ježíše	Ježíš	k1gMnSc4	Ježíš
osobně	osobně	k6eAd1	osobně
nikdy	nikdy	k6eAd1	nikdy
nepoznali	poznat	k5eNaPmAgMnP	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Skutky	skutek	k1gInPc1	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
líčí	líčit	k5eAaImIp3nP	líčit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
na	na	k7c6	na
zrušení	zrušení	k1gNnSc6	zrušení
jazykových	jazykový	k2eAgFnPc2d1	jazyková
bariér	bariéra	k1gFnPc2	bariéra
(	(	kIx(	(
<g/>
Sk	Sk	kA	Sk
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
(	(	kIx(	(
<g/>
Kral	Kral	k1gMnSc1	Kral
<g/>
,	,	kIx,	,
ČEP	Čep	k1gMnSc1	Čep
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Křesťanství	křesťanství	k1gNnSc1	křesťanství
se	se	k3xPyFc4	se
odlišilo	odlišit	k5eAaPmAgNnS	odlišit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijímalo	přijímat	k5eAaImAgNnS	přijímat
konvertity	konvertita	k1gMnPc4	konvertita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
předtím	předtím	k6eAd1	předtím
nebyli	být	k5eNaImAgMnP	být
židy	žid	k1gMnPc7	žid
<g/>
,	,	kIx,	,
a	a	k8xC	a
výslovně	výslovně	k6eAd1	výslovně
tak	tak	k6eAd1	tak
překročilo	překročit	k5eAaPmAgNnS	překročit
okruh	okruh	k1gInSc4	okruh
určitého	určitý	k2eAgInSc2d1	určitý
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
křesťanství	křesťanství	k1gNnSc2	křesťanství
za	za	k7c4	za
"	"	kIx"	"
<g/>
víru	víra	k1gFnSc4	víra
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
"	"	kIx"	"
a	a	k8xC	a
zrušení	zrušení	k1gNnSc3	zrušení
všech	všecek	k3xTgNnPc2	všecek
ostatních	ostatní	k2eAgNnPc2d1	ostatní
omezení	omezení	k1gNnPc2	omezení
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
světovým	světový	k2eAgNnSc7d1	světové
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
a	a	k8xC	a
mezikulturní	mezikulturní	k2eAgNnSc4d1	mezikulturní
propojení	propojení	k1gNnSc4	propojení
<g/>
,	,	kIx,	,
se	s	k7c7	s
závažnými	závažný	k2eAgInPc7d1	závažný
důsledky	důsledek	k1gInPc7	důsledek
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rané	raný	k2eAgNnSc4d1	rané
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
velké	velký	k2eAgFnSc2d1	velká
světové	světový	k2eAgFnSc2d1	světová
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
křesťané	křesťan	k1gMnPc1	křesťan
museli	muset	k5eAaImAgMnP	muset
vymezovat	vymezovat	k5eAaImF	vymezovat
vůči	vůči	k7c3	vůči
řecké	řecký	k2eAgFnSc3d1	řecká
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
jiným	jiný	k2eAgInPc3d1	jiný
myšlenkovým	myšlenkový	k2eAgInPc3d1	myšlenkový
a	a	k8xC	a
náboženským	náboženský	k2eAgInPc3d1	náboženský
proudům	proud	k1gInPc3	proud
a	a	k8xC	a
obhájit	obhájit	k5eAaPmF	obhájit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
útokům	útok	k1gInPc3	útok
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
museli	muset	k5eAaImAgMnP	muset
překonat	překonat	k5eAaPmF	překonat
zklamání	zklamání	k1gNnSc1	zklamání
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
po	po	k7c6	po
Ježíšově	Ježíšův	k2eAgFnSc6d1	Ježíšova
smrti	smrt	k1gFnSc6	smrt
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
opustit	opustit	k5eAaPmF	opustit
tedy	tedy	k9	tedy
představu	představa	k1gFnSc4	představa
provizoria	provizorium	k1gNnSc2	provizorium
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
občasným	občasný	k2eAgNnPc3d1	občasné
pronásledováním	pronásledování	k1gNnPc3	pronásledování
je	on	k3xPp3gMnPc4	on
hájili	hájit	k5eAaImAgMnP	hájit
apologeti	apologet	k1gMnPc1	apologet
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
apologia	apologia	k1gFnSc1	apologia
obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
)	)	kIx)	)
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
křesťanství	křesťanství	k1gNnSc2	křesťanství
proniklo	proniknout	k5eAaPmAgNnS	proniknout
i	i	k9	i
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
vrstev	vrstva	k1gFnPc2	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Gaius	Gaius	k1gMnSc1	Gaius
Galerius	Galerius	k1gMnSc1	Galerius
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
dříve	dříve	k6eAd2	dříve
odpůrcem	odpůrce	k1gMnSc7	odpůrce
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
pronásledování	pronásledování	k1gNnSc6	pronásledování
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
ediktem	edikt	k1gInSc7	edikt
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
311	[number]	k4	311
objasnil	objasnit	k5eAaPmAgMnS	objasnit
motivy	motiv	k1gInPc4	motiv
pronásledování	pronásledování	k1gNnSc2	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
přiznal	přiznat	k5eAaPmAgInS	přiznat
neúspěšnost	neúspěšnost	k1gFnSc4	neúspěšnost
pronásledování	pronásledování	k1gNnSc2	pronásledování
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
veřejnoprávní	veřejnoprávní	k2eAgNnSc4d1	veřejnoprávní
uznání	uznání	k1gNnSc4	uznání
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
kultu	kult	k1gInSc2	kult
-	-	kIx~	-
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
křesťanům	křesťan	k1gMnPc3	křesťan
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
platného	platný	k2eAgNnSc2d1	platné
práva	právo	k1gNnSc2	právo
veškerou	veškerý	k3xTgFnSc4	veškerý
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
jako	jako	k9	jako
členům	člen	k1gMnPc3	člen
povoleného	povolený	k2eAgInSc2d1	povolený
kultu	kult	k1gInSc2	kult
(	(	kIx(	(
<g/>
religio	religio	k1gMnSc1	religio
licita	licita	k1gMnSc1	licita
<g/>
)	)	kIx)	)
příslušela	příslušet	k5eAaImAgFnS	příslušet
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
křesťané	křesťan	k1gMnPc1	křesťan
nebudou	být	k5eNaImBp3nP	být
podnikat	podnikat	k5eAaImF	podnikat
nic	nic	k6eAd1	nic
proti	proti	k7c3	proti
veřejnému	veřejný	k2eAgInSc3d1	veřejný
pořádku	pořádek	k1gInSc3	pořádek
a	a	k8xC	a
zahrnou	zahrnout	k5eAaPmIp3nP	zahrnout
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
modliteb	modlitba	k1gFnPc2	modlitba
panovníka	panovník	k1gMnSc4	panovník
a	a	k8xC	a
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
313	[number]	k4	313
následoval	následovat	k5eAaImAgInS	následovat
edikt	edikt	k1gInSc1	edikt
milánský	milánský	k2eAgInSc1d1	milánský
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc4	Konstantin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zařadil	zařadit	k5eAaPmAgMnS	zařadit
křesťanství	křesťanství	k1gNnSc4	křesťanství
(	(	kIx(	(
<g/>
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
Konstantinova	Konstantinův	k2eAgInSc2d1	Konstantinův
vlivu	vliv	k1gInSc2	vliv
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
dovolená	dovolený	k2eAgNnPc4d1	dovolené
náboženství	náboženství	k1gNnPc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
doktrinálních	doktrinální	k2eAgFnPc2d1	doktrinální
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
325	[number]	k4	325
svolává	svolávat	k5eAaImIp3nS	svolávat
do	do	k7c2	do
Nikaie	Nikaie	k1gFnSc2	Nikaie
první	první	k4xOgMnSc1	první
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
koncil	koncil	k1gInSc1	koncil
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
První	první	k4xOgInSc4	první
nikajský	nikajský	k2eAgInSc4d1	nikajský
koncil	koncil	k1gInSc4	koncil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakládá	zakládat	k5eAaImIp3nS	zakládat
také	také	k9	také
nové	nový	k2eAgNnSc1d1	nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
,	,	kIx,	,
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
nahradit	nahradit	k5eAaPmF	nahradit
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
převážně	převážně	k6eAd1	převážně
pohanské	pohanský	k2eAgNnSc1d1	pohanské
<g/>
.	.	kIx.	.
</s>
<s>
Znevýhodněny	znevýhodněn	k2eAgInPc1d1	znevýhodněn
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pohanské	pohanský	k2eAgFnPc1d1	pohanská
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
však	však	k9	však
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
do	do	k7c2	do
svobody	svoboda	k1gFnSc2	svoboda
kultu	kult	k1gInSc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
pokřtít	pokřtít	k5eAaPmF	pokřtít
až	až	k9	až
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nástupci	nástupce	k1gMnPc7	nástupce
budou	být	k5eAaImBp3nP	být
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
císaře	císař	k1gMnSc2	císař
Juliána	Julián	k1gMnSc2	Julián
církev	církev	k1gFnSc4	církev
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc4	křesťanství
uznávat	uznávat	k5eAaImF	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Theodosius	Theodosius	k1gInSc1	Theodosius
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
380	[number]	k4	380
zakázal	zakázat	k5eAaPmAgInS	zakázat
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
směr	směr	k1gInSc1	směr
Áriánství	Áriánství	k1gNnSc2	Áriánství
a	a	k8xC	a
roku	rok	k1gInSc2	rok
391	[number]	k4	391
zakázal	zakázat	k5eAaPmAgInS	zakázat
na	na	k7c4	na
popud	popud	k1gInSc4	popud
milánského	milánský	k2eAgMnSc2d1	milánský
biskupa	biskup	k1gMnSc2	biskup
Ambrože	Ambrož	k1gMnSc2	Ambrož
veškerý	veškerý	k3xTgInSc4	veškerý
pohanský	pohanský	k2eAgInSc4d1	pohanský
kult	kult	k1gInSc4	kult
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
křesťanství	křesťanství	k1gNnSc2	křesťanství
zajistil	zajistit	k5eAaPmAgMnS	zajistit
pozici	pozice	k1gFnSc4	pozice
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
(	(	kIx(	(
<g/>
státního	státní	k2eAgNnSc2d1	státní
<g/>
)	)	kIx)	)
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
křesťanství	křesťanství	k1gNnSc1	křesťanství
vnitřně	vnitřně	k6eAd1	vnitřně
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
závazné	závazný	k2eAgNnSc1d1	závazné
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
krédo	krédo	k1gNnSc1	krédo
<g/>
)	)	kIx)	)
a	a	k8xC	a
nauka	nauka	k1gFnSc1	nauka
dogma	dogma	k1gNnSc1	dogma
a	a	k8xC	a
také	také	k9	také
církevní	církevní	k2eAgFnPc1d1	církevní
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgInPc1d1	navazující
na	na	k7c6	na
územní	územní	k2eAgFnSc6d1	územní
organizaci	organizace	k1gFnSc6	organizace
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
teologové	teolog	k1gMnPc1	teolog
a	a	k8xC	a
filosofové	filosof	k1gMnPc1	filosof
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
na	na	k7c6	na
církevních	církevní	k2eAgInPc6d1	církevní
koncilech	koncil	k1gInPc6	koncil
přesně	přesně	k6eAd1	přesně
formulovat	formulovat	k5eAaImF	formulovat
obsah	obsah	k1gInSc4	obsah
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
církev	církev	k1gFnSc1	církev
musela	muset	k5eAaImAgFnS	muset
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nestala	stát	k5eNaPmAgFnS	stát
jen	jen	k9	jen
nástrojem	nástroj	k1gInSc7	nástroj
císařské	císařský	k2eAgFnSc2d1	císařská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spisech	spis	k1gInPc6	spis
církevních	církevní	k2eAgMnPc2d1	církevní
Otců	otec	k1gMnPc2	otec
(	(	kIx(	(
<g/>
patristika	patristika	k1gFnSc1	patristika
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
i	i	k9	i
křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
myšlení	myšlení	k1gNnSc1	myšlení
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
(	(	kIx(	(
<g/>
řecké	řecký	k2eAgFnSc6d1	řecká
<g/>
)	)	kIx)	)
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
(	(	kIx(	(
<g/>
latinské	latinský	k2eAgFnSc2d1	Latinská
<g/>
)	)	kIx)	)
a	a	k8xC	a
jak	jak	k6eAd1	jak
narůstají	narůstat	k5eAaImIp3nP	narůstat
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
až	až	k9	až
do	do	k7c2	do
definitivního	definitivní	k2eAgInSc2d1	definitivní
rozchodu	rozchod	k1gInSc2	rozchod
roku	rok	k1gInSc2	rok
1054	[number]	k4	1054
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
mezitím	mezitím	k6eAd1	mezitím
rozbili	rozbít	k5eAaPmAgMnP	rozbít
barbarští	barbarský	k2eAgMnPc1d1	barbarský
nájezdníci	nájezdník	k1gMnPc1	nájezdník
a	a	k8xC	a
obnovy	obnova	k1gFnPc1	obnova
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
jeho	jeho	k3xOp3gMnPc1	jeho
biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
papežové	papež	k1gMnPc1	papež
(	(	kIx(	(
<g/>
Lev	Lev	k1gMnSc1	Lev
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
Řehoř	Řehoř	k1gMnSc1	Řehoř
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
))	))	k?	))
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
jejich	jejich	k3xOp3gFnSc1	jejich
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
a	a	k8xC	a
střední	střední	k2eAgFnSc7d1	střední
Itálií	Itálie	k1gFnSc7	Itálie
(	(	kIx(	(
<g/>
Církevní	církevní	k2eAgInSc1d1	církevní
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osvícení	osvícený	k2eAgMnPc1d1	osvícený
biskupové	biskup	k1gMnPc1	biskup
a	a	k8xC	a
především	především	k6eAd1	především
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
kláštery	klášter	k1gInPc1	klášter
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
uchovávat	uchovávat	k5eAaImF	uchovávat
zbytky	zbytek	k1gInPc4	zbytek
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
zakládali	zakládat	k5eAaImAgMnP	zakládat
církevní	církevní	k2eAgFnPc4d1	církevní
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zápasili	zápasit	k5eAaImAgMnP	zápasit
o	o	k7c4	o
jistou	jistý	k2eAgFnSc4d1	jistá
míru	míra	k1gFnSc4	míra
církevní	církevní	k2eAgFnSc2d1	církevní
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
panovnících	panovník	k1gMnPc6	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
misie	misie	k1gFnPc1	misie
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
když	když	k8xS	když
franský	franský	k2eAgMnSc1d1	franský
panovník	panovník	k1gMnSc1	panovník
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
koncem	koncem	k7c2	koncem
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zakládá	zakládat	k5eAaImIp3nS	zakládat
novou	nový	k2eAgFnSc4d1	nová
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
opírá	opírat	k5eAaImIp3nS	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
západní	západní	k2eAgFnSc4d1	západní
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
století	století	k1gNnSc2	století
proniklo	proniknout	k5eAaPmAgNnS	proniknout
křesťanství	křesťanství	k1gNnSc4	křesťanství
na	na	k7c6	na
území	území	k1gNnSc6	území
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Čech	Čechy	k1gFnPc2	Čechy
-	-	kIx~	-
působení	působení	k1gNnSc1	působení
iroskotske	iroskotsk	k1gInSc2	iroskotsk
misie	misie	k1gFnSc2	misie
zde	zde	k6eAd1	zde
nezanechalo	zanechat	k5eNaPmAgNnS	zanechat
větších	veliký	k2eAgFnPc2d2	veliký
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnSc4	území
měly	mít	k5eAaImAgFnP	mít
misie	misie	k1gFnPc1	misie
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
831	[number]	k4	831
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
pasovský	pasovský	k1gMnSc1	pasovský
biskup	biskup	k1gMnSc1	biskup
Reginhar	Reginhar	k1gMnSc1	Reginhar
velkomoravského	velkomoravský	k2eAgMnSc2d1	velkomoravský
knížete	kníže	k1gMnSc2	kníže
Mojimíra	Mojimír	k1gMnSc2	Mojimír
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
845	[number]	k4	845
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Řezně	řezně	k6eAd1	řezně
pokřtěno	pokřtít	k5eAaPmNgNnS	pokřtít
14	[number]	k4	14
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
-	-	kIx~	-
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gInPc4	jehož
motivy	motiv	k1gInPc4	motiv
není	být	k5eNaImIp3nS	být
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
pohled	pohled	k1gInSc1	pohled
-	-	kIx~	-
podle	podle	k7c2	podle
Dušana	Dušan	k1gMnSc2	Dušan
Třeštíka	Třeštík	k1gMnSc2	Třeštík
si	se	k3xPyFc3	se
Čechové	Čech	k1gMnPc1	Čech
naivně	naivně	k6eAd1	naivně
představovali	představovat	k5eAaImAgMnP	představovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stanou	stanout	k5eAaPmIp3nP	stanout
<g/>
-li	i	k?	-li
se	s	k7c7	s
křesťany	křesťan	k1gMnPc7	křesťan
<g/>
,	,	kIx,	,
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
císař	císař	k1gMnSc1	císař
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nezaútočí	zaútočit	k5eNaPmIp3nP	zaútočit
a	a	k8xC	a
proto	proto	k8xC	proto
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
následném	následný	k2eAgInSc6d1	následný
útoku	útok	k1gInSc6	útok
tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
epizoda	epizoda	k1gFnSc1	epizoda
v	v	k7c4	v
naši	náš	k3xOp1gFnSc4	náš
historii	historie	k1gFnSc4	historie
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
863	[number]	k4	863
působila	působit	k5eAaImAgFnS	působit
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
misie	misie	k1gFnSc2	misie
bratří	bratr	k1gMnPc2	bratr
Konstantina	Konstantin	k1gMnSc2	Konstantin
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
vyslaných	vyslaný	k2eAgNnPc2d1	vyslané
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
knížete	kníže	k1gMnSc2	kníže
Rostislava	Rostislav	k1gMnSc2	Rostislav
byzantským	byzantský	k2eAgMnSc7d1	byzantský
císařem	císař	k1gMnSc7	císař
Michalem	Michal	k1gMnSc7	Michal
III	III	kA	III
-	-	kIx~	-
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
878	[number]	k4	878
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
Metodějem	Metoděj	k1gMnSc7	Metoděj
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
se	se	k3xPyFc4	se
křesťanství	křesťanství	k1gNnSc1	křesťanství
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
misie	misie	k1gFnSc1	misie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
až	až	k9	až
do	do	k7c2	do
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
reformě	reforma	k1gFnSc3	reforma
klášterů	klášter	k1gInPc2	klášter
(	(	kIx(	(
<g/>
clunyjská	clunyjský	k2eAgFnSc1d1	clunyjská
reforma	reforma	k1gFnSc1	reforma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
získaly	získat	k5eAaPmAgFnP	získat
politickou	politický	k2eAgFnSc4d1	politická
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
síť	síť	k1gFnSc4	síť
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
prošlo	projít	k5eAaPmAgNnS	projít
podobnou	podobný	k2eAgFnSc7d1	podobná
reformou	reforma	k1gFnSc7	reforma
i	i	k8xC	i
papežství	papežství	k1gNnPc4	papežství
<g/>
,	,	kIx,	,
a	a	k8xC	a
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
císařstvím	císařství	k1gNnSc7	císařství
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
neklidné	klidný	k2eNgNnSc1d1	neklidné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
plodné	plodný	k2eAgNnSc4d1	plodné
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
rozmach	rozmach	k1gInSc4	rozmach
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
rozvoje	rozvoj	k1gInSc2	rozvoj
měst	město	k1gNnPc2	město
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
univerzity	univerzita	k1gFnPc1	univerzita
(	(	kIx(	(
<g/>
Bologna	Bologna	k1gFnSc1	Bologna
1080	[number]	k4	1080
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
začalo	začít	k5eAaPmAgNnS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
politice	politika	k1gFnSc6	politika
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc6d2	veliký
roli	role	k1gFnSc6	role
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ani	ani	k8xC	ani
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1000	[number]	k4	1000
nenastal	nastat	k5eNaPmAgInS	nastat
mnohými	mnohé	k1gNnPc7	mnohé
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
někteří	některý	k3yIgMnPc1	některý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
"	"	kIx"	"
<g/>
nevěřících	věřící	k2eNgFnPc6d1	nevěřící
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
nábožensky	nábožensky	k6eAd1	nábožensky
vzrušené	vzrušený	k2eAgFnSc6d1	vzrušená
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
evropští	evropský	k2eAgMnPc1d1	evropský
panovníci	panovník	k1gMnPc1	panovník
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgFnPc2d1	zvaná
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
úspěchu	úspěch	k1gInSc6	úspěch
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
osmdesát	osmdesát	k4xCc4	osmdesát
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgFnPc1d1	další
výpravy	výprava	k1gFnPc1	výprava
byly	být	k5eAaImAgFnP	být
stále	stále	k6eAd1	stále
problematičtější	problematický	k2eAgFnPc1d2	problematičtější
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
však	však	k9	však
získali	získat	k5eAaPmAgMnP	získat
nový	nový	k2eAgInSc4d1	nový
rozhled	rozhled	k1gInSc4	rozhled
i	i	k8xC	i
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
a	a	k8xC	a
do	do	k7c2	do
evropské	evropský	k2eAgFnSc2d1	Evropská
politiky	politika	k1gFnSc2	politika
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
i	i	k9	i
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
probíhal	probíhat	k5eAaImAgInS	probíhat
tak	tak	k9	tak
zvaný	zvaný	k2eAgInSc1d1	zvaný
boj	boj	k1gInSc1	boj
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
papežů	papež	k1gMnPc2	papež
s	s	k7c7	s
císaři	císař	k1gMnPc7	císař
o	o	k7c4	o
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
;	;	kIx,	;
první	první	k4xOgNnSc1	první
papežské	papežský	k2eAgNnSc1d1	papežské
vítězství	vítězství	k1gNnSc1	vítězství
(	(	kIx(	(
<g/>
Canossa	Canossa	k1gFnSc1	Canossa
1077	[number]	k4	1077
<g/>
)	)	kIx)	)
sice	sice	k8xC	sice
časem	časem	k6eAd1	časem
vystřídaly	vystřídat	k5eAaPmAgFnP	vystřídat
porážky	porážka	k1gFnPc4	porážka
<g/>
,	,	kIx,	,
přímému	přímý	k2eAgNnSc3d1	přímé
spojení	spojení	k1gNnSc3	spojení
náboženské	náboženský	k2eAgFnSc2d1	náboženská
autority	autorita	k1gFnSc2	autorita
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
mocí	moc	k1gFnSc7	moc
se	se	k3xPyFc4	se
však	však	k9	však
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
i	i	k8xC	i
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
vzestup	vzestup	k1gInSc1	vzestup
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
posílil	posílit	k5eAaPmAgInS	posílit
moc	moc	k6eAd1	moc
i	i	k9	i
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
sílila	sílit	k5eAaImAgFnS	sílit
i	i	k9	i
kritika	kritika	k1gFnSc1	kritika
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
opravdovost	opravdovost	k1gFnSc4	opravdovost
a	a	k8xC	a
chudobu	chudoba	k1gFnSc4	chudoba
ještě	ještě	k6eAd1	ještě
zachytily	zachytit	k5eAaPmAgInP	zachytit
tzv.	tzv.	kA	tzv.
žebravé	žebravý	k2eAgInPc1d1	žebravý
řády	řád	k1gInPc1	řád
(	(	kIx(	(
<g/>
dominikáni	dominikán	k1gMnPc1	dominikán
<g/>
,	,	kIx,	,
františkáni	františkán	k1gMnPc1	františkán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
povstáním	povstání	k1gNnPc3	povstání
nespokojenců	nespokojenec	k1gMnPc2	nespokojenec
<g/>
.	.	kIx.	.
</s>
<s>
Kritikové	kritik	k1gMnPc1	kritik
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
dovolávali	dovolávat	k5eAaImAgMnP	dovolávat
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
církev	církev	k1gFnSc4	církev
reformovali	reformovat	k5eAaBmAgMnP	reformovat
oni	onen	k3xDgMnPc1	onen
(	(	kIx(	(
<g/>
William	William	k1gInSc1	William
Ockham	Ockham	k1gInSc1	Ockham
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Wycliffe	Wycliff	k1gInSc5	Wycliff
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
králové	král	k1gMnPc1	král
si	se	k3xPyFc3	se
na	na	k7c4	na
70	[number]	k4	70
let	léto	k1gNnPc2	léto
podrobili	podrobit	k5eAaPmAgMnP	podrobit
papeže	papež	k1gMnSc4	papež
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
soupeři	soupeř	k1gMnPc1	soupeř
však	však	k9	však
volili	volit	k5eAaImAgMnP	volit
jiné	jiný	k2eAgMnPc4d1	jiný
papeže	papež	k1gMnPc4	papež
a	a	k8xC	a
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
spory	spor	k1gInPc4	spor
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
papeži	papež	k1gMnPc7	papež
silně	silně	k6eAd1	silně
otřásly	otřást	k5eAaPmAgInP	otřást
autoritou	autorita	k1gFnSc7	autorita
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Koncily	koncil	k1gInPc1	koncil
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
a	a	k8xC	a
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
sice	sice	k8xC	sice
papežský	papežský	k2eAgInSc4d1	papežský
rozkol	rozkol	k1gInSc4	rozkol
odstranily	odstranit	k5eAaPmAgFnP	odstranit
<g/>
,	,	kIx,	,
snahy	snaha	k1gFnSc2	snaha
konciliaristů	konciliarista	k1gMnPc2	konciliarista
o	o	k7c4	o
méně	málo	k6eAd2	málo
monarchické	monarchický	k2eAgNnSc4d1	monarchické
pojetí	pojetí	k1gNnSc4	pojetí
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
neprosadily	prosadit	k5eNaPmAgFnP	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Oslabenou	oslabený	k2eAgFnSc4d1	oslabená
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
říši	říše	k1gFnSc4	říše
ohrozili	ohrozit	k5eAaPmAgMnP	ohrozit
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
turečtí	turecký	k2eAgMnPc1d1	turecký
nájezdníci	nájezdník	k1gMnPc1	nájezdník
a	a	k8xC	a
byzantští	byzantský	k2eAgMnPc1d1	byzantský
zástupci	zástupce	k1gMnPc1	zástupce
sjednali	sjednat	k5eAaPmAgMnP	sjednat
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
smír	smír	k1gInSc4	smír
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
východní	východní	k2eAgMnPc1d1	východní
patriarchové	patriarcha	k1gMnPc1	patriarcha
však	však	k8xC	však
smír	smír	k1gInSc4	smír
neuznali	uznat	k5eNaPmAgMnP	uznat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1453	[number]	k4	1453
Byzanc	Byzanc	k1gFnSc1	Byzanc
padla	padnout	k5eAaPmAgFnS	padnout
<g/>
;	;	kIx,	;
za	za	k7c4	za
její	její	k3xOp3gMnPc4	její
pokračovatele	pokračovatel	k1gMnPc4	pokračovatel
se	se	k3xPyFc4	se
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
ruští	ruský	k2eAgMnPc1d1	ruský
carové	car	k1gMnPc1	car
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
západní	západní	k2eAgFnSc1d1	západní
církev	církev	k1gFnSc1	církev
nežila	žít	k5eNaImAgFnS	žít
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Vypjatá	vypjatý	k2eAgFnSc1d1	vypjatá
osobní	osobní	k2eAgFnSc1d1	osobní
zbožnost	zbožnost	k1gFnSc1	zbožnost
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
lidovým	lidový	k2eAgNnPc3d1	lidové
povstáním	povstání	k1gNnPc3	povstání
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
husitství	husitství	k1gNnSc4	husitství
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nebo	nebo	k8xC	nebo
Savonarolův	Savonarolův	k2eAgInSc4d1	Savonarolův
režim	režim	k1gInSc4	režim
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
hluboký	hluboký	k2eAgInSc1d1	hluboký
protest	protest	k1gInSc1	protest
německého	německý	k2eAgMnSc2d1	německý
mnicha	mnich	k1gMnSc2	mnich
a	a	k8xC	a
teologa	teolog	k1gMnSc2	teolog
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
(	(	kIx(	(
<g/>
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
Římem	Řím	k1gInSc7	Řím
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozdělení	rozdělení	k1gNnSc3	rozdělení
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
se	se	k3xPyFc4	se
především	především	k6eAd1	především
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
zbytnělé	zbytnělý	k2eAgFnSc3d1	zbytnělá
náboženské	náboženský	k2eAgFnSc3d1	náboženská
praxi	praxe	k1gFnSc3	praxe
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
každého	každý	k3xTgMnSc2	každý
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
závaznost	závaznost	k1gFnSc4	závaznost
evangelia	evangelium	k1gNnSc2	evangelium
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediná	jediný	k2eAgFnSc1d1	jediná
naděje	naděje	k1gFnSc1	naděje
křesťana	křesťan	k1gMnSc2	křesťan
je	být	k5eAaImIp3nS	být
boží	boží	k2eAgFnSc1d1	boží
milost	milost	k1gFnSc1	milost
<g/>
.	.	kIx.	.
</s>
<s>
Křesťan	Křesťan	k1gMnSc1	Křesťan
má	mít	k5eAaImIp3nS	mít
zejména	zejména	k9	zejména
žít	žít	k5eAaImF	žít
podle	podle	k7c2	podle
evangelia	evangelium	k1gNnSc2	evangelium
a	a	k8xC	a
podle	podle	k7c2	podle
rady	rada	k1gFnSc2	rada
apoštola	apoštol	k1gMnSc2	apoštol
Pavla	Pavel	k1gMnSc2	Pavel
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
protivit	protivit	k5eAaImF	protivit
světské	světský	k2eAgFnPc4d1	světská
vrchnosti	vrchnost	k1gFnPc4	vrchnost
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
naopak	naopak	k6eAd1	naopak
nesmí	smět	k5eNaImIp3nS	smět
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
věcí	věc	k1gFnPc2	věc
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Luther	Luthra	k1gFnPc2	Luthra
nově	nově	k6eAd1	nově
přeložil	přeložit	k5eAaPmAgMnS	přeložit
bibli	bible	k1gFnSc4	bible
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
tak	tak	k6eAd1	tak
vlnu	vlna	k1gFnSc4	vlna
nových	nový	k2eAgInPc2d1	nový
překladů	překlad	k1gInPc2	překlad
a	a	k8xC	a
čtení	čtení	k1gNnSc2	čtení
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
autoritu	autorita	k1gFnSc4	autorita
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc7	klášter
a	a	k8xC	a
kněžský	kněžský	k2eAgInSc4d1	kněžský
celibát	celibát	k1gInSc4	celibát
a	a	k8xC	a
omezil	omezit	k5eAaPmAgMnS	omezit
svátosti	svátost	k1gFnPc4	svátost
na	na	k7c4	na
křest	křest	k1gInSc4	křest
a	a	k8xC	a
večeři	večeře	k1gFnSc4	večeře
Páně	páně	k2eAgFnSc4d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Lutherovu	Lutherův	k2eAgFnSc4d1	Lutherova
reformaci	reformace	k1gFnSc4	reformace
dále	daleko	k6eAd2	daleko
radikalizoval	radikalizovat	k5eAaBmAgMnS	radikalizovat
Jean	Jean	k1gMnSc1	Jean
Calvin	Calvina	k1gFnPc2	Calvina
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
teologů	teolog	k1gMnPc2	teolog
nově	nově	k6eAd1	nově
formulovala	formulovat	k5eAaImAgFnS	formulovat
základy	základ	k1gInPc4	základ
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
formou	forma	k1gFnSc7	forma
katechismu	katechismus	k1gInSc2	katechismus
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Reformace	reformace	k1gFnSc1	reformace
výrazně	výrazně	k6eAd1	výrazně
proměnila	proměnit	k5eAaPmAgFnS	proměnit
evropské	evropský	k2eAgFnPc4d1	Evropská
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zdůraznila	zdůraznit	k5eAaPmAgFnS	zdůraznit
mravní	mravní	k2eAgFnSc4d1	mravní
stránku	stránka	k1gFnSc4	stránka
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
oslabila	oslabit	k5eAaPmAgFnS	oslabit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
kněžími	kněz	k1gMnPc7	kněz
a	a	k8xC	a
laiky	laik	k1gMnPc7	laik
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
uložila	uložit	k5eAaPmAgFnS	uložit
četbu	četba	k1gFnSc4	četba
bible	bible	k1gFnSc2	bible
a	a	k8xC	a
podpořila	podpořit	k5eAaPmAgFnS	podpořit
tak	tak	k9	tak
potřebu	potřeba	k1gFnSc4	potřeba
obecné	obecný	k2eAgFnSc2d1	obecná
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
Církve	církev	k1gFnPc1	církev
začaly	začít	k5eAaPmAgFnP	začít
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
při	při	k7c6	při
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
národní	národní	k2eAgInPc4d1	národní
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
součástmi	součást	k1gFnPc7	součást
národních	národní	k2eAgFnPc2d1	národní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
majetek	majetek	k1gInSc1	majetek
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
účely	účel	k1gInPc4	účel
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
panovníci	panovník	k1gMnPc1	panovník
využili	využít	k5eAaPmAgMnP	využít
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zbavili	zbavit	k5eAaPmAgMnP	zbavit
církevní	církevní	k2eAgMnPc1d1	církevní
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Kalvínova	Kalvínův	k2eAgFnSc1d1	Kalvínova
reformace	reformace	k1gFnSc1	reformace
<g/>
,	,	kIx,	,
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
a	a	k8xC	a
převážně	převážně	k6eAd1	převážně
městských	městský	k2eAgFnPc6d1	městská
společnostech	společnost	k1gFnPc6	společnost
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
přísné	přísný	k2eAgInPc4d1	přísný
mravy	mrav	k1gInPc4	mrav
včetně	včetně	k7c2	včetně
veřejného	veřejný	k2eAgInSc2d1	veřejný
dohledu	dohled	k1gInSc2	dohled
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
podpořila	podpořit	k5eAaPmAgFnS	podpořit
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
kolébkou	kolébka	k1gFnSc7	kolébka
moderních	moderní	k2eAgFnPc2d1	moderní
demokracií	demokracie	k1gFnPc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
,	,	kIx,	,
katolická	katolický	k2eAgFnSc1d1	katolická
reformace	reformace	k1gFnSc1	reformace
nebo	nebo	k8xC	nebo
protireformace	protireformace	k1gFnSc1	protireformace
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
formuloval	formulovat	k5eAaImAgInS	formulovat
Tridentský	tridentský	k2eAgInSc1d1	tridentský
koncil	koncil	k1gInSc1	koncil
a	a	k8xC	a
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
zejména	zejména	k9	zejména
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
(	(	kIx(	(
<g/>
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
utužení	utužení	k1gNnSc3	utužení
církevní	církevní	k2eAgFnSc2d1	církevní
nauky	nauka	k1gFnSc2	nauka
i	i	k8xC	i
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
posílila	posílit	k5eAaPmAgFnS	posílit
postavení	postavení	k1gNnSc4	postavení
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Postarala	postarat	k5eAaPmAgFnS	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
nové	nový	k2eAgNnSc4d1	nové
vydání	vydání	k1gNnSc4	vydání
latinské	latinský	k2eAgFnSc2d1	Latinská
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
předepsala	předepsat	k5eAaPmAgFnS	předepsat
povinné	povinný	k2eAgNnSc4d1	povinné
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
srozumitelné	srozumitelný	k2eAgFnPc4d1	srozumitelná
učebnice	učebnice	k1gFnPc4	učebnice
křesťanství	křesťanství	k1gNnSc2	křesťanství
(	(	kIx(	(
<g/>
katechismy	katechismus	k1gInPc7	katechismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
latinskou	latinský	k2eAgFnSc4d1	Latinská
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
i	i	k9	i
reforma	reforma	k1gFnSc1	reforma
kalendáře	kalendář	k1gInSc2	kalendář
(	(	kIx(	(
<g/>
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
snahu	snaha	k1gFnSc4	snaha
řady	řada	k1gFnSc2	řada
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
o	o	k7c6	o
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
(	(	kIx(	(
<g/>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Rotterdamský	rotterdamský	k2eAgMnSc1d1	rotterdamský
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Bodin	Bodin	k1gMnSc1	Bodin
<g/>
)	)	kIx)	)
i	i	k8xC	i
politické	politický	k2eAgNnSc4d1	politické
úsilí	úsilí	k1gNnSc4	úsilí
císařů	císař	k1gMnPc2	císař
vedla	vést	k5eAaImAgFnS	vést
ostrá	ostrý	k2eAgNnPc1d1	ostré
stanoviska	stanovisko	k1gNnPc4	stanovisko
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
hluboce	hluboko	k6eAd1	hluboko
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
dlouhotrvajícím	dlouhotrvající	k2eAgMnPc3d1	dlouhotrvající
a	a	k8xC	a
krvavým	krvavý	k2eAgMnPc3d1	krvavý
konfliktům	konflikt	k1gInPc3	konflikt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
politické	politický	k2eAgInPc1d1	politický
zájmy	zájem	k1gInPc1	zájem
často	často	k6eAd1	často
kryly	krýt	k5eAaImAgInP	krýt
náboženskými	náboženský	k2eAgInPc7d1	náboženský
<g/>
.	.	kIx.	.
</s>
<s>
Selské	selské	k1gNnSc1	selské
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
pronásledování	pronásledování	k1gNnSc1	pronásledování
jinověrců	jinověrec	k1gMnPc2	jinověrec
<g/>
,	,	kIx,	,
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
zejména	zejména	k9	zejména
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
přinesly	přinést	k5eAaPmAgFnP	přinést
obrovské	obrovský	k2eAgNnSc4d1	obrovské
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
silně	silně	k6eAd1	silně
otřásly	otřást	k5eAaPmAgFnP	otřást
důvěrou	důvěra	k1gFnSc7	důvěra
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
Evropanů	Evropan	k1gMnPc2	Evropan
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
náboženství	náboženství	k1gNnSc2	náboženství
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
křesťanství	křesťanství	k1gNnSc2	křesťanství
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
značně	značně	k6eAd1	značně
lišil	lišit	k5eAaImAgMnS	lišit
<g/>
,	,	kIx,	,
společným	společný	k2eAgNnSc7d1	společné
hnutím	hnutí	k1gNnSc7	hnutí
byl	být	k5eAaImAgInS	být
obrat	obrat	k1gInSc1	obrat
k	k	k7c3	k
osobní	osobní	k2eAgFnSc3d1	osobní
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
zbožnosti	zbožnost	k1gFnSc3	zbožnost
v	v	k7c6	v
pietismu	pietismus	k1gInSc6	pietismus
<g/>
,	,	kIx,	,
bohatá	bohatý	k2eAgFnSc1d1	bohatá
lidová	lidový	k2eAgFnSc1d1	lidová
barokní	barokní	k2eAgFnSc1d1	barokní
zbožnost	zbožnost	k1gFnSc1	zbožnost
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
příklon	příklon	k1gInSc1	příklon
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
racionalismu	racionalismus	k1gInSc3	racionalismus
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
k	k	k7c3	k
deismu	deismus	k1gInSc3	deismus
a	a	k8xC	a
osvícenství	osvícenství	k1gNnSc4	osvícenství
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Misijní	misijní	k2eAgFnSc1d1	misijní
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
mimoevropských	mimoevropský	k2eAgFnPc6d1	mimoevropská
zemích	zem	k1gFnPc6	zem
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
různé	různý	k2eAgInPc4d1	různý
vnější	vnější	k2eAgInPc4d1	vnější
úspěchy	úspěch	k1gInPc4	úspěch
se	se	k3xPyFc4	se
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
ocitají	ocitat	k5eAaImIp3nP	ocitat
v	v	k7c6	v
defenzivě	defenziva	k1gFnSc6	defenziva
a	a	k8xC	a
čelí	čelit	k5eAaImIp3nS	čelit
jednak	jednak	k8xC	jednak
ostré	ostrý	k2eAgFnSc3d1	ostrá
kritice	kritika	k1gFnSc3	kritika
mnohých	mnohý	k2eAgMnPc2d1	mnohý
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
faktickému	faktický	k2eAgNnSc3d1	faktické
odcizování	odcizování	k1gNnSc3	odcizování
lidových	lidový	k2eAgInPc2d1	lidový
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěma	dva	k4xCgInPc7	dva
miliardami	miliarda	k4xCgFnPc7	miliarda
věřících	věřící	k2eAgMnPc2d1	věřící
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
světovým	světový	k2eAgNnSc7d1	světové
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
křesťanů	křesťan	k1gMnPc2	křesťan
přitom	přitom	k6eAd1	přitom
roste	růst	k5eAaImIp3nS	růst
asi	asi	k9	asi
o	o	k7c4	o
2	[number]	k4	2
procenta	procento	k1gNnSc2	procento
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
populace	populace	k1gFnSc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
absolutních	absolutní	k2eAgInPc6d1	absolutní
počtech	počet	k1gInPc6	počet
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
křesťanství	křesťanství	k1gNnSc1	křesťanství
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
velké	velký	k2eAgNnSc1d1	velké
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
v	v	k7c6	v
procentuálním	procentuální	k2eAgInSc6d1	procentuální
přírůstku	přírůstek	k1gInSc6	přírůstek
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
až	až	k9	až
za	za	k7c4	za
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
není	být	k5eNaImIp3nS	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
světadílech	světadíl	k1gInPc6	světadíl
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
náboženstvími	náboženství	k1gNnPc7	náboženství
tří	tři	k4xCgMnPc2	tři
kontinentů	kontinent	k1gInPc2	kontinent
přesto	přesto	k6eAd1	přesto
dominuje	dominovat	k5eAaImIp3nS	dominovat
(	(	kIx(	(
<g/>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gNnSc4	jeho
zastoupení	zastoupení	k1gNnSc4	zastoupení
zhruba	zhruba	k6eAd1	zhruba
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
u	u	k7c2	u
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Nepříliš	příliš	k6eNd1	příliš
početnou	početný	k2eAgFnSc4d1	početná
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
menšinu	menšina	k1gFnSc4	menšina
tvoří	tvořit	k5eAaImIp3nP	tvořit
křesťané	křesťan	k1gMnPc1	křesťan
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
křesťanů	křesťan	k1gMnPc2	křesťan
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jednomu	jeden	k4xCgNnSc3	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
směrů	směr	k1gInPc2	směr
<g/>
:	:	kIx,	:
západní	západní	k2eAgNnPc4d1	západní
katolictví	katolictví	k1gNnPc4	katolictví
<g/>
,	,	kIx,	,
východní	východní	k2eAgNnPc4d1	východní
pravoslaví	pravoslaví	k1gNnPc4	pravoslaví
a	a	k8xC	a
protestantství	protestantství	k1gNnPc4	protestantství
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
z	z	k7c2	z
reformace	reformace	k1gFnSc2	reformace
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odlišným	odlišný	k2eAgNnSc7d1	odlišné
chápáním	chápání	k1gNnSc7	chápání
vztahu	vztah	k1gInSc2	vztah
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
i	i	k9	i
odlišná	odlišný	k2eAgFnSc1d1	odlišná
církevní	církevní	k2eAgFnSc1d1	církevní
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
starší	starší	k1gMnSc1	starší
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
katolické	katolický	k2eAgFnSc2d1	katolická
a	a	k8xC	a
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k9	jako
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
mezi	mezi	k7c7	mezi
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
protestantských	protestantský	k2eAgFnPc2d1	protestantská
(	(	kIx(	(
<g/>
presbyteriánských	presbyteriánský	k2eAgFnPc2d1	presbyteriánská
<g/>
)	)	kIx)	)
církví	církev	k1gFnSc7	církev
se	se	k3xPyFc4	se
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
sdružení	sdružení	k1gNnSc1	sdružení
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
místní	místní	k2eAgInPc4d1	místní
sbory	sbor	k1gInPc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
hierarchicky	hierarchicky	k6eAd1	hierarchicky
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
buď	buď	k8xC	buď
s	s	k7c7	s
jediným	jediný	k2eAgMnSc7d1	jediný
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
(	(	kIx(	(
<g/>
papež	papež	k1gMnSc1	papež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
obvykle	obvykle	k6eAd1	obvykle
územních	územní	k2eAgInPc6d1	územní
patriarchátech	patriarchát	k1gInPc6	patriarchát
(	(	kIx(	(
<g/>
pravoslaví	pravoslaví	k1gNnPc2	pravoslaví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
církve	církev	k1gFnSc2	církev
episkopální	episkopální	k2eAgMnPc1d1	episkopální
(	(	kIx(	(
<g/>
anglikáni	anglikán	k1gMnPc1	anglikán
<g/>
,	,	kIx,	,
luteráni	luterán	k1gMnPc1	luterán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
skupina	skupina	k1gFnSc1	skupina
místních	místní	k2eAgFnPc2d1	místní
církví	církev	k1gFnPc2	církev
podřízena	podřízen	k2eAgFnSc1d1	podřízena
biskupovi	biskup	k1gMnSc3	biskup
<g/>
,	,	kIx,	,
a	a	k8xC	a
církve	církev	k1gFnPc1	církev
presbyteriální	presbyteriální	k2eAgFnPc1d1	presbyteriální
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
sbor	sbor	k1gInSc1	sbor
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
samostatný	samostatný	k2eAgInSc4d1	samostatný
a	a	k8xC	a
jen	jen	k9	jen
někde	někde	k6eAd1	někde
si	se	k3xPyFc3	se
zástupci	zástupce	k1gMnPc1	zástupce
sborů	sbor	k1gInPc2	sbor
volí	volit	k5eAaImIp3nP	volit
představenstva	představenstvo	k1gNnPc4	představenstvo
například	například	k6eAd1	například
národních	národní	k2eAgFnPc2d1	národní
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
je	být	k5eAaImIp3nS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
podle	podle	k7c2	podle
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
rozlišením	rozlišení	k1gNnSc7	rozlišení
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
odlišné	odlišný	k2eAgNnSc1d1	odlišné
chápání	chápání	k1gNnSc1	chápání
církevní	církevní	k2eAgFnSc2d1	církevní
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
a	a	k8xC	a
pravoslavných	pravoslavný	k2eAgFnPc6d1	pravoslavná
církvích	církev	k1gFnPc6	církev
se	se	k3xPyFc4	se
povolání	povolání	k1gNnSc1	povolání
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
případně	případně	k6eAd1	případně
řeholníků	řeholník	k1gMnPc2	řeholník
(	(	kIx(	(
<g/>
mnichů	mnich	k1gMnPc2	mnich
a	a	k8xC	a
jeptišek	jeptiška	k1gFnPc2	jeptiška
<g/>
)	)	kIx)	)
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
<g/>
,	,	kIx,	,
u	u	k7c2	u
kněží	kněz	k1gMnPc2	kněz
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
svěcení	svěcení	k1gNnSc6	svěcení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
celibátu	celibát	k1gInSc2	celibát
(	(	kIx(	(
<g/>
biskupové	biskup	k1gMnPc1	biskup
a	a	k8xC	a
mniši	mnich	k1gMnPc1	mnich
<g/>
,	,	kIx,	,
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
i	i	k9	i
kněží	kněz	k1gMnPc1	kněz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protestantských	protestantský	k2eAgFnPc6d1	protestantská
církvích	církev	k1gFnPc6	církev
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
obvykle	obvykle	k6eAd1	obvykle
také	také	k9	také
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
ordinace	ordinace	k1gFnPc4	ordinace
<g/>
,	,	kIx,	,
kazatele	kazatel	k1gMnPc4	kazatel
nebo	nebo	k8xC	nebo
duchovní	duchovní	k1gMnSc1	duchovní
si	se	k3xPyFc3	se
však	však	k9	však
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
sbory	sbor	k1gInPc1	sbor
samy	sám	k3xTgMnPc4	sám
vybírají	vybírat	k5eAaImIp3nP	vybírat
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
služba	služba	k1gFnSc1	služba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
dočasná	dočasný	k2eAgFnSc1d1	dočasná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
protestantských	protestantský	k2eAgFnPc2d1	protestantská
církví	církev	k1gFnPc2	církev
mohou	moct	k5eAaImIp3nP	moct
církevní	církevní	k2eAgMnPc1d1	církevní
službu	služba	k1gFnSc4	služba
vykonávat	vykonávat	k5eAaImF	vykonávat
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgFnPc2d1	zvaná
svobodných	svobodný	k2eAgFnPc2d1	svobodná
církví	církev	k1gFnPc2	církev
(	(	kIx(	(
<g/>
free	freat	k5eAaPmIp3nS	freat
church	church	k1gMnSc1	church
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
samostatné	samostatný	k2eAgInPc1d1	samostatný
místní	místní	k2eAgInPc1d1	místní
sbory	sbor	k1gInPc1	sbor
bez	bez	k7c2	bez
dalších	další	k2eAgFnPc2d1	další
organizačních	organizační	k2eAgFnPc2d1	organizační
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
moderní	moderní	k2eAgFnPc4d1	moderní
ekumenické	ekumenický	k2eAgFnPc4d1	ekumenická
snahy	snaha	k1gFnPc4	snaha
jsou	být	k5eAaImIp3nP	být
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
církvemi	církev	k1gFnPc7	církev
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
konfliktní	konfliktní	k2eAgMnSc1d1	konfliktní
a	a	k8xC	a
pokud	pokud	k6eAd1	pokud
měly	mít	k5eAaImAgFnP	mít
nějaký	nějaký	k3yIgInSc4	nějaký
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
i	i	k9	i
násilné	násilný	k2eAgNnSc1d1	násilné
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
menšinových	menšinový	k2eAgFnPc2d1	menšinová
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
kacíři	kacíř	k1gMnPc1	kacíř
a	a	k8xC	a
heretici	heretik	k1gMnPc1	heretik
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
lišili	lišit	k5eAaImAgMnP	lišit
i	i	k9	i
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
i	i	k9	i
politicky	politicky	k6eAd1	politicky
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
