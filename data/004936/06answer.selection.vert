<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
(	(	kIx(	(
<g/>
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
odvozeného	odvozený	k2eAgMnSc4d1	odvozený
přes	přes	k7c4	přes
latinské	latinský	k2eAgNnSc4d1	latinské
christianos	christianosa	k1gFnPc2	christianosa
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
χ	χ	k?	χ
s	s	k7c7	s
významem	význam	k1gInSc7	význam
náležící	náležící	k2eAgFnSc4d1	náležící
Kristu	Krista	k1gFnSc4	Krista
či	či	k8xC	či
kristovec	kristovec	k1gInSc4	kristovec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
abrahámovské	abrahámovský	k2eAgNnSc1d1	abrahámovské
<g/>
,	,	kIx,	,
monoteistické	monoteistický	k2eAgFnPc1d1	monoteistická
<g/>
,	,	kIx,	,
univerzální	univerzální	k2eAgFnPc1d1	univerzální
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
(	(	kIx(	(
<g/>
založené	založený	k2eAgFnPc1d1	založená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
misijní	misijní	k2eAgNnSc1d1	misijní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgNnSc1d1	soustředěné
kolem	kolem	k7c2	kolem
života	život	k1gInSc2	život
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazaretu	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k8xC	jako
mesiáše	mesiáš	k1gMnSc2	mesiáš
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Krista	Kristus	k1gMnSc2	Kristus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spasitele	spasitel	k1gMnSc4	spasitel
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Božího	boží	k2eAgMnSc2d1	boží
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
