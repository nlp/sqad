<p>
<s>
Kittitas	Kittitas	k1gInSc1	Kittitas
County	Counta	k1gFnSc2	Counta
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
40	[number]	k4	40
915	[number]	k4	915
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Správním	správní	k2eAgNnSc7d1	správní
městem	město	k1gNnSc7	město
okresu	okres	k1gInSc2	okres
je	být	k5eAaImIp3nS	být
Ellensburg	Ellensburg	k1gInSc1	Ellensburg
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
okresu	okres	k1gInSc2	okres
činí	činit	k5eAaImIp3nS	činit
6	[number]	k4	6
042	[number]	k4	042
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kittitas	Kittitasa	k1gFnPc2	Kittitasa
County	Counta	k1gFnSc2	Counta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
