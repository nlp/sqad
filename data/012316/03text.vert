<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
dýmka	dýmka	k1gFnSc1	dýmka
<g/>
,	,	kIx,	,
slangově	slangově	k6eAd1	slangově
vodárna	vodárna	k1gFnSc1	vodárna
<g/>
,	,	kIx,	,
farec	farec	k1gInSc1	farec
<g/>
,	,	kIx,	,
vodnice	vodnice	k1gFnSc1	vodnice
<g/>
,	,	kIx,	,
šíša	šíša	k1gFnSc1	šíša
(	(	kIx(	(
<g/>
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
taktéž	taktéž	k?	taktéž
nargila	nargila	k1gFnSc1	nargila
<g/>
,	,	kIx,	,
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
hookah	hookaha	k1gFnPc2	hookaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
používané	používaný	k2eAgNnSc1d1	používané
ke	k	k7c3	k
kouření	kouření	k1gNnSc3	kouření
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
filtrace	filtrace	k1gFnSc2	filtrace
a	a	k8xC	a
ochlazování	ochlazování	k1gNnSc3	ochlazování
kouře	kouř	k1gInSc2	kouř
přes	přes	k7c4	přes
vodní	vodní	k2eAgInSc4d1	vodní
filtr	filtr	k1gInSc4	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
původu	původ	k1gInSc2	původ
není	být	k5eNaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
na	na	k7c4	na
Střední	střední	k2eAgInSc4d1	střední
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
kuřivo	kuřivo	k1gNnSc1	kuřivo
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
dýmce	dýmka	k1gFnSc6	dýmka
používá	používat	k5eAaImIp3nS	používat
tabák	tabák	k1gInSc1	tabák
smíšený	smíšený	k2eAgInSc1d1	smíšený
s	s	k7c7	s
melasou	melasa	k1gFnSc7	melasa
a	a	k8xC	a
glycerinem	glycerin	k1gInSc7	glycerin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vlhký	vlhký	k2eAgInSc1d1	vlhký
a	a	k8xC	a
mazlavý	mazlavý	k2eAgInSc1d1	mazlavý
a	a	k8xC	a
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
silně	silně	k6eAd1	silně
dýmá	dýmat	k5eAaImIp3nS	dýmat
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
se	se	k3xPyFc4	se
dodává	dodávat	k5eAaImIp3nS	dodávat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
příchutích	příchuť	k1gFnPc6	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zahřívání	zahřívání	k1gNnSc3	zahřívání
tabáku	tabák	k1gInSc2	tabák
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kokosové	kokosový	k2eAgNnSc4d1	kokosové
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejzazším	zadní	k2eAgInSc6d3	nejzazší
možném	možný	k2eAgInSc6d1	možný
případě	případ	k1gInSc6	případ
rychlozápalné	rychlozápalný	k2eAgFnSc2d1	rychlozápalný
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
RZU	RZU	kA	RZU
<g/>
)	)	kIx)	)
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgInPc1d1	stejný
uhlíky	uhlík	k1gInPc1	uhlík
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
rychlozápalných	rychlozápalný	k2eAgFnPc2d1	rychlozápalný
směsí	směs	k1gFnPc2	směs
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
do	do	k7c2	do
kadidelnic	kadidelnice	k1gFnPc2	kadidelnice
a	a	k8xC	a
vonných	vonný	k2eAgFnPc2d1	vonná
lamp	lampa	k1gFnPc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
kouření	kouření	k1gNnSc2	kouření
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
výdrži	výdrž	k1gFnSc3	výdrž
uhlíků	uhlík	k1gInPc2	uhlík
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
intenzitě	intenzita	k1gFnSc3	intenzita
kouření	kouření	k1gNnSc2	kouření
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
výdrž	výdrž	k1gFnSc4	výdrž
kokosového	kokosový	k2eAgNnSc2d1	kokosové
uhlí	uhlí	k1gNnSc2	uhlí
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
90-120	[number]	k4	90-120
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
rychlozápalných	rychlozápalný	k2eAgInPc2d1	rychlozápalný
uhlíků	uhlík	k1gInPc2	uhlík
okolo	okolo	k7c2	okolo
30-60	[number]	k4	30-60
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
dýmky	dýmka	k1gFnPc1	dýmka
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
asi	asi	k9	asi
bong	bongo	k1gNnPc2	bongo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
ke	k	k7c3	k
kouření	kouření	k1gNnSc3	kouření
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
od	od	k7c2	od
15	[number]	k4	15
cm	cm	kA	cm
(	(	kIx(	(
<g/>
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
nefunkční	funkční	k2eNgInPc1d1	nefunkční
exempláře	exemplář	k1gInPc1	exemplář
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
2	[number]	k4	2
metry	metr	k1gInPc4	metr
(	(	kIx(	(
<g/>
varianta	varianta	k1gFnSc1	varianta
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
kuřáků	kuřák	k1gInPc2	kuřák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžná	běžný	k2eAgFnSc1d1	běžná
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
varianty	varianta	k1gFnPc1	varianta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
dýmky	dýmka	k1gFnSc2	dýmka
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
6	[number]	k4	6
šlauchů	šlauchů	k?	šlauchů
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kouřit	kouřit	k5eAaImF	kouřit
více	hodně	k6eAd2	hodně
lidem	člověk	k1gMnPc3	člověk
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
dýmky	dýmka	k1gFnPc1	dýmka
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
zdobné	zdobný	k2eAgFnPc1d1	zdobná
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
cenově	cenově	k6eAd1	cenově
značně	značně	k6eAd1	značně
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nejlevnější	levný	k2eAgInPc1d3	nejlevnější
exempláře	exemplář	k1gInPc1	exemplář
mívají	mívat	k5eAaImIp3nP	mívat
alespoň	alespoň	k9	alespoň
zdobný	zdobný	k2eAgInSc4d1	zdobný
šlauch	šlauch	k?	šlauch
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nejdražší	drahý	k2eAgInPc1d3	nejdražší
mají	mít	k5eAaImIp3nP	mít
jemné	jemný	k2eAgNnSc4d1	jemné
tepání	tepání	k1gNnSc4	tepání
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tácku	tácek	k1gInSc2	tácek
a	a	k8xC	a
kleštiček	kleštička	k1gFnPc2	kleštička
a	a	k8xC	a
složitě	složitě	k6eAd1	složitě
malovanou	malovaný	k2eAgFnSc4d1	malovaná
a	a	k8xC	a
tvarovanou	tvarovaný	k2eAgFnSc4d1	tvarovaná
karafu	karafa	k1gFnSc4	karafa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnPc4	část
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Karafa	karafa	k1gFnSc1	karafa
(	(	kIx(	(
<g/>
také	také	k9	také
váza	váza	k1gFnSc1	váza
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Většinou	většinou	k6eAd1	většinou
skleněná	skleněný	k2eAgFnSc1d1	skleněná
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
či	či	k8xC	či
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Plní	plnit	k5eAaImIp3nS	plnit
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
trubice	trubice	k1gFnSc1	trubice
(	(	kIx(	(
<g/>
downstem	downst	k1gInSc7	downst
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ponořena	ponořit	k5eAaPmNgFnS	ponořit
přibližně	přibližně	k6eAd1	přibližně
1-2	[number]	k4	1-2
cm	cm	kA	cm
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc4d1	různý
tvary	tvar	k1gInPc4	tvar
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
vázy	váza	k1gFnSc2	váza
dochází	docházet	k5eAaImIp3nS	docházet
vsunutím	vsunutí	k1gNnSc7	vsunutí
přes	přes	k7c4	přes
těsnění	těsnění	k1gNnSc4	těsnění
<g/>
,	,	kIx,	,
šroubováním	šroubování	k1gNnSc7	šroubování
či	či	k8xC	či
bajonetovým	bajonetový	k2eAgInSc7d1	bajonetový
spojem	spoj	k1gInSc7	spoj
(	(	kIx(	(
<g/>
click	click	k1gInSc1	click
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karafy	karafa	k1gFnPc1	karafa
bývají	bývat	k5eAaImIp3nP	bývat
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
nejen	nejen	k6eAd1	nejen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dobře	dobře	k6eAd1	dobře
ochlazovaly	ochlazovat	k5eAaImAgFnP	ochlazovat
kouř	kouř	k1gInSc4	kouř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
potěchu	potěcha	k1gFnSc4	potěcha
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
koupit	koupit	k5eAaPmF	koupit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
možných	možný	k2eAgFnPc6d1	možná
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
tvarech	tvar	k1gInPc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
dýňovitý	dýňovitý	k2eAgInSc4d1	dýňovitý
tvar	tvar	k1gInSc4	tvar
z	z	k7c2	z
čirého	čirý	k2eAgNnSc2d1	čiré
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
průhledného	průhledný	k2eAgNnSc2d1	průhledné
skla	sklo	k1gNnSc2	sklo
–	–	k?	–
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
vidět	vidět	k5eAaImF	vidět
výška	výška	k1gFnSc1	výška
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
zabarvení	zabarvený	k2eAgMnPc1d1	zabarvený
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
menších	malý	k2eAgFnPc2d2	menší
váz	váza	k1gFnPc2	váza
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
rychlejšímu	rychlý	k2eAgInSc3d2	rychlejší
ohřevu	ohřev	k1gInSc3	ohřev
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
celého	celý	k2eAgInSc2d1	celý
požitku	požitek	k1gInSc2	požitek
z	z	k7c2	z
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vhodné	vhodný	k2eAgNnSc1d1	vhodné
vodu	voda	k1gFnSc4	voda
měnit	měnit	k5eAaImF	měnit
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
kouření	kouření	k1gNnSc6	kouření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tělo	tělo	k1gNnSc1	tělo
===	===	k?	===
</s>
</p>
<p>
<s>
Těla	tělo	k1gNnPc1	tělo
vodních	vodní	k2eAgFnPc2d1	vodní
dýmek	dýmka	k1gFnPc2	dýmka
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
mosazi	mosaz	k1gFnSc2	mosaz
<g/>
,	,	kIx,	,
nerez	nerez	k2eAgFnSc2d1	nerez
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
smokestemu	smokestem	k1gInSc2	smokestem
<g/>
.	.	kIx.	.
</s>
<s>
Smokestem	Smokest	k1gInSc7	Smokest
vede	vést	k5eAaImIp3nS	vést
kouř	kouř	k1gInSc1	kouř
z	z	k7c2	z
korunky	korunka	k1gFnSc2	korunka
do	do	k7c2	do
karafy	karafa	k1gFnSc2	karafa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
ponořená	ponořený	k2eAgFnSc1d1	ponořená
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kouř	kouř	k1gInSc1	kouř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
projde	projít	k5eAaPmIp3nS	projít
přes	přes	k7c4	přes
vodu	voda	k1gFnSc4	voda
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
veden	vést	k5eAaImNgInS	vést
skrz	skrz	k7c4	skrz
srdce	srdce	k1gNnSc4	srdce
dýmky	dýmka	k1gFnSc2	dýmka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
na	na	k7c4	na
karafu	karafa	k1gFnSc4	karafa
<g/>
,	,	kIx,	,
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
šlauchu	šlauchu	k?	šlauchu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičně	tradičně	k6eAd1	tradičně
bývá	bývat	k5eAaImIp3nS	bývat
tělo	tělo	k1gNnSc1	tělo
zdobeno	zdoben	k2eAgNnSc1d1	zdobeno
tepáním	tepání	k1gNnSc7	tepání
či	či	k8xC	či
korálky	korálek	k1gInPc7	korálek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
vykládáno	vykládán	k2eAgNnSc4d1	vykládáno
dřevem	dřevo	k1gNnSc7	dřevo
či	či	k8xC	či
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
dýmky	dýmka	k1gFnPc1	dýmka
pak	pak	k6eAd1	pak
naopak	naopak	k6eAd1	naopak
bývají	bývat	k5eAaImIp3nP	bývat
pojaty	pojmout	k5eAaPmNgFnP	pojmout
v	v	k7c6	v
jednodušším	jednoduchý	k2eAgInSc6d2	jednodušší
a	a	k8xC	a
méně	málo	k6eAd2	málo
zdobeném	zdobený	k2eAgInSc6d1	zdobený
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
funkčnost	funkčnost	k1gFnSc4	funkčnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šlauch	Šlauch	k?	Šlauch
===	===	k?	===
</s>
</p>
<p>
<s>
Šlauch	Šlauch	k?	Šlauch
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
hadice	hadice	k1gFnSc2	hadice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kuřák	kuřák	k1gMnSc1	kuřák
nasává	nasávat	k5eAaImIp3nS	nasávat
kouř	kouř	k1gInSc4	kouř
z	z	k7c2	z
dýmky	dýmka	k1gFnSc2	dýmka
<g/>
,	,	kIx,	,
a	a	k8xC	a
napojuje	napojovat	k5eAaImIp3nS	napojovat
se	se	k3xPyFc4	se
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
samotné	samotný	k2eAgFnSc2d1	samotná
hadice	hadice	k1gFnSc2	hadice
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
do	do	k7c2	do
dýmky	dýmka	k1gFnSc2	dýmka
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
si	se	k3xPyFc3	se
kuřák	kuřák	k1gInSc1	kuřák
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
šlauchu	šlauchu	k?	šlauchu
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
obtočený	obtočený	k2eAgInSc1d1	obtočený
kovovou	kovový	k2eAgFnSc7d1	kovová
pružinou	pružina	k1gFnSc7	pružina
a	a	k8xC	a
obalený	obalený	k2eAgInSc4d1	obalený
textilií	textilie	k1gFnSc7	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
šlauchy	šlauchy	k?	šlauchy
nejsou	být	k5eNaImIp3nP	být
proplachovatelné	proplachovatelný	k2eAgInPc1d1	proplachovatelný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
ihned	ihned	k6eAd1	ihned
vyměnit	vyměnit	k5eAaPmF	vyměnit
za	za	k7c4	za
kvalitnější	kvalitní	k2eAgFnPc4d2	kvalitnější
vymyvatelné	vymyvatelný	k2eAgFnPc4d1	vymyvatelný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
papírové	papírový	k2eAgInPc1d1	papírový
šlauchy	šlauchy	k?	šlauchy
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
reznout	reznout	k5eAaImF	reznout
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
až	až	k9	až
k	k	k7c3	k
vdechování	vdechování	k1gNnSc3	vdechování
částeček	částečka	k1gFnPc2	částečka
rzi	rez	k1gFnSc2	rez
<g/>
.	.	kIx.	.
</s>
<s>
Proplachovatelné	Proplachovatelný	k2eAgInPc1d1	Proplachovatelný
šlauchy	šlauchy	k?	šlauchy
bývají	bývat	k5eAaImIp3nP	bývat
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
z	z	k7c2	z
PVC	PVC	kA	PVC
<g/>
,	,	kIx,	,
silikonu	silikon	k1gInSc2	silikon
či	či	k8xC	či
velbloudí	velbloudí	k2eAgFnSc2d1	velbloudí
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Koncovky	koncovka	k1gFnPc1	koncovka
(	(	kIx(	(
<g/>
náustky	náustek	k1gInPc1	náustek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
z	z	k7c2	z
široké	široký	k2eAgFnSc2d1	široká
palety	paleta	k1gFnSc2	paleta
materiálů	materiál	k1gInPc2	materiál
od	od	k7c2	od
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
plastu	plast	k1gInSc2	plast
až	až	k9	až
po	po	k7c4	po
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
materiál	materiál	k1gInSc1	materiál
šlauchu	šlauchu	k?	šlauchu
má	mít	k5eAaImIp3nS	mít
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
částečky	částečka	k1gFnPc4	částečka
kouře	kouř	k1gInSc2	kouř
a	a	k8xC	a
jen	jen	k9	jen
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
šlauchů	šlauchů	k?	šlauchů
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
uvnitř	uvnitř	k6eAd1	uvnitř
čistit	čistit	k5eAaImF	čistit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Korunka	korunka	k1gFnSc1	korunka
===	===	k?	===
</s>
</p>
<p>
<s>
Korunky	korunka	k1gFnPc1	korunka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Phunnel	Phunnel	k1gInSc1	Phunnel
<g/>
,	,	kIx,	,
vortex	vortex	k1gInSc1	vortex
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgInSc1d1	klasický
egyptský	egyptský	k2eAgInSc1d1	egyptský
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
pár	pár	k4xCyI	pár
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
nasazena	nasadit	k5eAaPmNgFnS	nasadit
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
přes	přes	k7c4	přes
gumové	gumový	k2eAgNnSc4d1	gumové
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiné	jiný	k2eAgNnSc1d1	jiné
těsnění	těsnění	k1gNnSc1	těsnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
egyptský	egyptský	k2eAgInSc1d1	egyptský
kotlík	kotlík	k1gInSc1	kotlík
má	mít	k5eAaImIp3nS	mít
miskovitý	miskovitý	k2eAgInSc4d1	miskovitý
tvar	tvar	k1gInSc4	tvar
na	na	k7c4	na
tabák	tabák	k1gInSc4	tabák
a	a	k8xC	a
ve	v	k7c4	v
spod	spod	k1gInSc4	spod
má	mít	k5eAaImIp3nS	mít
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Phunnel	Phunnel	k1gInSc1	Phunnel
má	mít	k5eAaImIp3nS	mít
uprostřed	uprostřed	k6eAd1	uprostřed
komínek	komínek	k1gInSc4	komínek
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
dírou	díra	k1gFnSc7	díra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vortex	Vortex	k1gInSc1	Vortex
má	mít	k5eAaImIp3nS	mít
uprostřed	uprostřed	k6eAd1	uprostřed
komínek	komínek	k1gInSc4	komínek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
koncem	konec	k1gInSc7	konec
několik	několik	k4yIc4	několik
dírek	dírka	k1gFnPc2	dírka
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
proudění	proudění	k1gNnSc1	proudění
teplého	teplý	k2eAgInSc2d1	teplý
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Materiálem	materiál	k1gInSc7	materiál
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
keramická	keramický	k2eAgFnSc1d1	keramická
hlína	hlína	k1gFnSc1	hlína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
i	i	k8xC	i
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
glazurovaná	glazurovaný	k2eAgFnSc1d1	glazurovaná
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
šamot	šamot	k1gInSc1	šamot
<g/>
,	,	kIx,	,
silikon	silikon	k1gInSc1	silikon
<g/>
,	,	kIx,	,
sklo	sklo	k1gNnSc1	sklo
či	či	k8xC	či
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korunka	korunka	k1gFnSc1	korunka
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
alobalem	alobal	k1gInSc7	alobal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
proděraví	proděravit	k5eAaPmIp3nS	proděravit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
povrch	povrch	k1gInSc4	povrch
se	se	k3xPyFc4	se
pokládají	pokládat	k5eAaImIp3nP	pokládat
žhavé	žhavý	k2eAgInPc1d1	žhavý
uhlíky	uhlík	k1gInPc1	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
dát	dát	k5eAaPmF	dát
mezi	mezi	k7c4	mezi
uhlíky	uhlík	k1gInPc4	uhlík
a	a	k8xC	a
alobal	alobal	k1gInSc4	alobal
alobalovou	alobalový	k2eAgFnSc4d1	alobalová
spirálku	spirálka	k1gFnSc4	spirálka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
nezhasínají	zhasínat	k5eNaImIp3nP	zhasínat
uhlíky	uhlík	k1gInPc1	uhlík
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
se	se	k3xPyFc4	se
nepřepaluje	přepalovat	k5eNaImIp3nS	přepalovat
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
melasa	melasa	k1gFnSc1	melasa
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
se	se	k3xPyFc4	se
hustý	hustý	k2eAgInSc1d1	hustý
dým	dým	k1gInSc1	dým
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
putuje	putovat	k5eAaImIp3nS	putovat
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
šlauch	šlauch	k?	šlauch
až	až	k9	až
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využívají	využívat	k5eAaPmIp3nP	využívat
se	se	k3xPyFc4	se
i	i	k9	i
náhrady	náhrada	k1gFnSc2	náhrada
alobalu	alobal	k1gInSc2	alobal
(	(	kIx(	(
<g/>
HMS	HMS	kA	HMS
-	-	kIx~	-
Heat	Heat	k2eAgInSc1d1	Heat
Management	management	k1gInSc1	management
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgMnSc1d3	nejznámější
Kaloud	Kaloud	k1gMnSc1	Kaloud
lotus	lotus	k1gMnSc1	lotus
<g/>
,	,	kIx,	,
Badcha	Badcha	k1gMnSc1	Badcha
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tarbuš	tarbuš	k1gInSc4	tarbuš
===	===	k?	===
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
česky	česky	k6eAd1	česky
jako	jako	k8xC	jako
závětří	závětří	k1gNnSc6	závětří
<g/>
.	.	kIx.	.
</s>
<s>
Tarbuš	tarbuš	k1gInSc1	tarbuš
je	být	k5eAaImIp3nS	být
plechová	plechový	k2eAgFnSc1d1	plechová
krytka	krytka	k1gFnSc1	krytka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
teplu	teplo	k1gNnSc6	teplo
unikat	unikat	k5eAaImF	unikat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
jej	on	k3xPp3gInSc4	on
u	u	k7c2	u
korunky	korunka	k1gFnSc2	korunka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
ohřev	ohřev	k1gInSc1	ohřev
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
během	během	k7c2	během
počátečního	počáteční	k2eAgNnSc2d1	počáteční
roztahávání	roztahávání	k1gNnSc2	roztahávání
dýmky	dýmka	k1gFnSc2	dýmka
a	a	k8xC	a
když	když	k8xS	když
uhlíky	uhlík	k1gInPc1	uhlík
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
výhřevnost	výhřevnost	k1gFnSc4	výhřevnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kouření	kouření	k1gNnSc2	kouření
se	se	k3xPyFc4	se
tarbuš	tarbuš	k1gInSc1	tarbuš
používat	používat	k5eAaImF	používat
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
jelikož	jelikož	k8xS	jelikož
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nežádoucímu	žádoucí	k2eNgNnSc3d1	nežádoucí
připálení	připálení	k1gNnSc3	připálení
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tradice	tradice	k1gFnSc1	tradice
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
kouření	kouření	k1gNnSc1	kouření
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
výsadou	výsada	k1gFnSc7	výsada
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
vodních	vodní	k2eAgFnPc2d1	vodní
dýmek	dýmka	k1gFnPc2	dýmka
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zrovnoprávnění	zrovnoprávnění	k1gNnSc3	zrovnoprávnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vodní	vodní	k2eAgFnPc1d1	vodní
dýmky	dýmka	k1gFnPc1	dýmka
kouří	kouřit	k5eAaImIp3nP	kouřit
často	často	k6eAd1	často
v	v	k7c6	v
čajovnách	čajovna	k1gFnPc6	čajovna
a	a	k8xC	a
shishabarech	shishabar	k1gInPc6	shishabar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kouření	kouření	k1gNnSc3	kouření
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Léta	léto	k1gNnPc4	léto
Páně	páně	k2eAgFnSc1d1	páně
11.12	[number]	k4	11.12
<g/>
.2018	.2018	k4	.2018
přivezli	přivézt	k5eAaPmAgMnP	přivézt
Michal	Michal	k1gMnSc1	Michal
Zeman	Zeman	k1gMnSc1	Zeman
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Valenta	Valenta	k1gMnSc1	Valenta
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
shishu	shisha	k1gFnSc4	shisha
do	do	k7c2	do
Špindlerova	Špindlerův	k2eAgInSc2d1	Špindlerův
Mlýna	mlýn	k1gInSc2	mlýn
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
baru	bar	k1gInSc2	bar
"	"	kIx"	"
<g/>
Zuzu	Zuza	k1gFnSc4	Zuza
bar	bar	k1gInSc1	bar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
exemplář	exemplář	k1gInSc4	exemplář
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
započal	započnout	k5eAaPmAgInS	započnout
nový	nový	k2eAgInSc4d1	nový
věk	věk	k1gInSc4	věk
ve	v	k7c6	v
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
dýmka	dýmka	k1gFnSc1	dýmka
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
kouří	kouřit	k5eAaImIp3nS	kouřit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
čajovnách	čajovna	k1gFnPc6	čajovna
a	a	k8xC	a
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
počtu	počet	k1gInSc6	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
dýmku	dýmka	k1gFnSc4	dýmka
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
kouřit	kouřit	k5eAaImF	kouřit
i	i	k9	i
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
kouření	kouření	k1gNnSc2	kouření
dělá	dělat	k5eAaImIp3nS	dělat
určitý	určitý	k2eAgInSc1d1	určitý
rituál	rituál	k1gInSc1	rituál
<g/>
.	.	kIx.	.
<g/>
Minimální	minimální	k2eAgInSc1d1	minimální
věk	věk	k1gInSc1	věk
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
vodnice	vodnice	k1gFnSc2	vodnice
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
<g/>
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
bez	bez	k7c2	bez
nikotinu	nikotin	k1gInSc2	nikotin
<g/>
.	.	kIx.	.
</s>
<s>
Kuřáci	kuřák	k1gMnPc1	kuřák
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
častokrát	častokrát	k6eAd1	častokrát
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
šíša	šíša	k1gFnSc1	šíša
či	či	k8xC	či
nargilé	nargilé	k1gNnSc1	nargilé
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pije	pít	k5eAaImIp3nS	pít
čaj	čaj	k1gInSc1	čaj
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
slazený	slazený	k2eAgMnSc1d1	slazený
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ochucený	ochucený	k2eAgInSc1d1	ochucený
mátou	máta	k1gFnSc7	máta
(	(	kIx(	(
<g/>
touareg	touareg	k1gInSc1	touareg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gInSc1	jeho
piják	piják	k1gInSc1	piják
chuťové	chuťový	k2eAgInPc1d1	chuťový
pohárky	pohárek	k1gInPc1	pohárek
otupené	otupený	k2eAgInPc1d1	otupený
kouřem	kouř	k1gInSc7	kouř
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
to	ten	k3xDgNnSc1	ten
nepocítí	pocítit	k5eNaPmIp3nS	pocítit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ovšem	ovšem	k9	ovšem
člověk	člověk	k1gMnSc1	člověk
chce	chtít	k5eAaImIp3nS	chtít
vychutnat	vychutnat	k5eAaPmF	vychutnat
jemné	jemný	k2eAgInPc4d1	jemný
chuťové	chuťový	k2eAgInPc4d1	chuťový
odstíny	odstín	k1gInPc4	odstín
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
a	a	k8xC	a
s	s	k7c7	s
citem	cit	k1gInSc7	cit
připravených	připravený	k2eAgInPc2d1	připravený
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
požívání	požívání	k1gNnSc1	požívání
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jiného	jiný	k2eAgNnSc2d1	jiné
kuřiva	kuřivo	k1gNnSc2	kuřivo
<g/>
)	)	kIx)	)
na	na	k7c4	na
závadu	závada	k1gFnSc4	závada
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
čajomilec	čajomilec	k1gMnSc1	čajomilec
vychutnat	vychutnat	k5eAaPmF	vychutnat
druhý	druhý	k4xOgInSc4	druhý
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
nálev	nálev	k1gInSc4	nálev
třeba	třeba	k9	třeba
Pai	Pai	k1gMnSc1	Pai
Mu	on	k3xPp3gMnSc3	on
Tanu	tanout	k5eAaImIp1nS	tanout
<g/>
,	,	kIx,	,
Dračích	dračí	k2eAgNnPc2d1	dračí
očí	oko	k1gNnPc2	oko
nebo	nebo	k8xC	nebo
Mao	Mao	k1gFnPc2	Mao
Fengu	Feng	k1gInSc2	Feng
<g/>
,	,	kIx,	,
vadí	vadit	k5eAaImIp3nS	vadit
mu	on	k3xPp3gMnSc3	on
nejen	nejen	k6eAd1	nejen
<g/>
,	,	kIx,	,
kouří	kouřit	k5eAaImIp3nS	kouřit
<g/>
-li	i	k?	-li
sám	sám	k3xTgInSc4	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
zakouřené	zakouřený	k2eAgNnSc4d1	zakouřené
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgNnPc1d1	zdravotní
rizika	riziko	k1gNnPc1	riziko
kouření	kouření	k1gNnSc2	kouření
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přenos	přenos	k1gInSc1	přenos
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
===	===	k?	===
</s>
</p>
<p>
<s>
Sdílení	sdílení	k1gNnSc1	sdílení
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
více	hodně	k6eAd2	hodně
lidmi	člověk	k1gMnPc7	člověk
je	být	k5eAaImIp3nS	být
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
přenosu	přenos	k1gInSc2	přenos
infekčních	infekční	k2eAgFnPc2d1	infekční
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Zdokumentován	zdokumentovat	k5eAaPmNgInS	zdokumentovat
byl	být	k5eAaImAgInS	být
přenos	přenos	k1gInSc1	přenos
plicní	plicní	k2eAgFnSc2d1	plicní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
kuřáků	kuřák	k1gInPc2	kuřák
stejné	stejný	k2eAgFnSc2d1	stejná
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
riziko	riziko	k1gNnSc1	riziko
přenosu	přenos	k1gInSc2	přenos
chřipky	chřipka	k1gFnSc2	chřipka
<g/>
,	,	kIx,	,
oparů	opar	k1gInPc2	opar
nebo	nebo	k8xC	nebo
žloutenky	žloutenka	k1gFnSc2	žloutenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
chemikálie	chemikálie	k1gFnPc1	chemikálie
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
kouři	kouř	k1gInSc6	kouř
===	===	k?	===
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
diskutovaná	diskutovaný	k2eAgFnSc1d1	diskutovaná
je	být	k5eAaImIp3nS	být
škodlivost	škodlivost	k1gFnSc1	škodlivost
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
cigaretami	cigareta	k1gFnPc7	cigareta
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
se	se	k3xPyFc4	se
o	o	k7c6	o
objasnění	objasnění	k1gNnSc6	objasnění
situace	situace	k1gFnSc2	situace
postaral	postarat	k5eAaPmAgInS	postarat
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
tým	tým	k1gInSc1	tým
z	z	k7c2	z
Americké	americký	k2eAgFnSc2d1	americká
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Bejrútu	Bejrút	k1gInSc6	Bejrút
(	(	kIx(	(
<g/>
American	American	k1gMnSc1	American
University	universita	k1gFnSc2	universita
of	of	k?	of
Beirut	Beirut	k1gInSc1	Beirut
<g/>
/	/	kIx~	/
<g/>
AUB	AUB	kA	AUB
<g/>
)	)	kIx)	)
a	a	k8xC	a
University	universita	k1gFnSc2	universita
Svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Joseph	Joseph	k1gMnSc1	Joseph
University	universita	k1gFnSc2	universita
in	in	k?	in
Beirut	Beirut	k1gInSc1	Beirut
<g/>
/	/	kIx~	/
<g/>
USJ	USJ	kA	USJ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouř	kouř	k1gInSc1	kouř
z	z	k7c2	z
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
škodlivém	škodlivý	k2eAgInSc6d1	škodlivý
cigaretovém	cigaretový	k2eAgInSc6d1	cigaretový
kouři	kouř	k1gInSc6	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
popírají	popírat	k5eAaImIp3nP	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vodní	vodní	k2eAgFnSc1d1	vodní
dýmka	dýmka	k1gFnSc1	dýmka
byla	být	k5eAaImAgFnS	být
zdravotně	zdravotně	k6eAd1	zdravotně
nezávadná	závadný	k2eNgFnSc1d1	nezávadná
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
AUB	AUB	kA	AUB
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
v	v	k7c6	v
lednovém	lednový	k2eAgNnSc6d1	lednové
vydání	vydání	k1gNnSc6	vydání
Food	Food	k1gMnSc1	Food
and	and	k?	and
Chemical	Chemical	k1gMnSc1	Chemical
Toxicology	Toxicolog	k1gMnPc4	Toxicolog
<g/>
:	:	kIx,	:
dehet	dehet	k1gInSc1	dehet
<g/>
,	,	kIx,	,
nikotin	nikotin	k1gInSc1	nikotin
a	a	k8xC	a
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
kouři	kouř	k1gInSc6	kouř
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
<g/>
,	,	kIx,	,
vycházejícím	vycházející	k2eAgInSc7d1	vycházející
z	z	k7c2	z
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
kouřícího	kouřící	k2eAgNnSc2d1	kouřící
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc1d1	použit
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
melasový	melasový	k2eAgInSc1d1	melasový
tabák	tabák	k1gInSc1	tabák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
popsání	popsání	k1gNnSc2	popsání
průměrné	průměrný	k2eAgFnSc2d1	průměrná
škodlivosti	škodlivost	k1gFnSc2	škodlivost
kouření	kouření	k1gNnSc2	kouření
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
<g/>
:	:	kIx,	:
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
délce	délka	k1gFnSc3	délka
tahů	tah	k1gInPc2	tah
a	a	k8xC	a
na	na	k7c6	na
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
tahy	tah	k1gInPc7	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
–	–	k?	–
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
karcinogenní	karcinogenní	k2eAgInPc4d1	karcinogenní
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
a	a	k8xC	a
–	–	k?	–
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
karcinogenní	karcinogenní	k2eAgInPc4d1	karcinogenní
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
b	b	k?	b
–	–	k?	–
látka	látka	k1gFnSc1	látka
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
karcinogenní	karcinogenní	k2eAgInPc4d1	karcinogenní
</s>
</p>
<p>
<s>
3	[number]	k4	3
–	–	k?	–
látka	látka	k1gFnSc1	látka
není	být	k5eNaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c7	mezi
karcinogeny	karcinogen	k1gInPc7	karcinogen
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahromaděný	nahromaděný	k2eAgInSc1d1	nahromaděný
kouř	kouř	k1gInSc1	kouř
ze	z	k7c2	z
100	[number]	k4	100
tahů	tah	k1gInPc2	tah
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nikotin	nikotin	k1gInSc1	nikotin
<g/>
,	,	kIx,	,
dehet	dehet	k1gInSc1	dehet
a	a	k8xC	a
karcinogenní	karcinogenní	k2eAgInPc1d1	karcinogenní
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
v	v	k7c6	v
alarmujících	alarmující	k2eAgFnPc6d1	alarmující
hodnotách	hodnota	k1gFnPc6	hodnota
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
několikrát	několikrát	k6eAd1	několikrát
přesahujících	přesahující	k2eAgMnPc2d1	přesahující
hodnoty	hodnota	k1gFnPc1	hodnota
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
cigaretách	cigareta	k1gFnPc6	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
měřen	měřen	k2eAgInSc1d1	měřen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dehet	dehet	k1gInSc1	dehet
a	a	k8xC	a
nikotin	nikotin	k1gInSc1	nikotin
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
hlavní	hlavní	k2eAgFnPc4d1	hlavní
příčiny	příčina	k1gFnPc4	příčina
kuřáckých	kuřácký	k2eAgNnPc2d1	kuřácké
onemocnění	onemocnění	k1gNnPc2	onemocnění
(	(	kIx(	(
<g/>
rakovina	rakovina	k1gFnSc1	rakovina
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc2	onemocnění
srdce	srdce	k1gNnSc2	srdce
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dehet	dehet	k1gInSc1	dehet
v	v	k7c6	v
kouři	kouř	k1gInSc6	kouř
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
alarmujících	alarmující	k2eAgFnPc6d1	alarmující
hodnotách	hodnota	k1gFnPc6	hodnota
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
cigaretovým	cigaretový	k2eAgInSc7d1	cigaretový
kouřem	kouř	k1gInSc7	kouř
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
studie	studie	k1gFnSc2	studie
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Alan	Alan	k1gMnSc1	Alan
Shihadeh	Shihadeh	k1gMnSc1	Shihadeh
<g/>
,	,	kIx,	,
asistující	asistující	k2eAgMnSc1d1	asistující
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
AUB-ústředí	AUB-ústředý	k2eAgMnPc1d1	AUB-ústředý
mechanického	mechanický	k2eAgNnSc2d1	mechanické
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc1	výsledek
výzkumu	výzkum	k1gInSc2	výzkum
upřesňuje	upřesňovat	k5eAaImIp3nS	upřesňovat
<g/>
:	:	kIx,	:
V	v	k7c6	v
kouři	kouř	k1gInSc6	kouř
získaném	získaný	k2eAgInSc6d1	získaný
ze	z	k7c2	z
100	[number]	k4	100
tahů	tah	k1gInPc2	tah
z	z	k7c2	z
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
více	hodně	k6eAd2	hodně
dehtu	dehet	k1gInSc2	dehet
než	než	k8xS	než
ve	v	k7c6	v
20	[number]	k4	20
cigaretách	cigareta	k1gFnPc6	cigareta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cigaretový	cigaretový	k2eAgInSc1d1	cigaretový
dehet	dehet	k1gInSc1	dehet
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
teplotou	teplota	k1gFnSc7	teplota
spalování	spalování	k1gNnSc2	spalování
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kouření	kouření	k1gNnSc6	kouření
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teplota	teplota	k1gFnSc1	teplota
tabáku	tabák	k1gInSc2	tabák
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
-	-	kIx~	-
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
cigaret	cigareta	k1gFnPc2	cigareta
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
teplota	teplota	k1gFnSc1	teplota
tabaku	tabak	k1gInSc2	tabak
až	až	k9	až
900	[number]	k4	900
°	°	k?	°
<g/>
C.	C.	kA	C.
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
kouřových	kouřový	k2eAgInPc2d1	kouřový
kondenzátů	kondenzát	k1gInPc2	kondenzát
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dýmek	dýmka	k1gFnPc2	dýmka
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
prostou	prostý	k2eAgFnSc7d1	prostá
destilací	destilace	k1gFnSc7	destilace
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pyrolýzou	pyrolýza	k1gFnSc7	pyrolýza
(	(	kIx(	(
<g/>
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
<g/>
)	)	kIx)	)
a	a	k8xC	a
spalováním	spalování	k1gNnSc7	spalování
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
tabákových	tabákový	k2eAgInPc2d1	tabákový
kondenzátů	kondenzát	k1gInPc2	kondenzát
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvorba	tvorba	k1gFnSc1	tvorba
nádorů	nádor	k1gInPc2	nádor
a	a	k8xC	a
četnost	četnost	k1gFnSc4	četnost
vzniku	vznik	k1gInSc2	vznik
mutagenů	mutagen	k1gInPc2	mutagen
stoupá	stoupat	k5eAaImIp3nS	stoupat
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
pyrolýzy	pyrolýza	k1gFnSc2	pyrolýza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
USJ	USJ	kA	USJ
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Chawky	Chawka	k1gMnSc2	Chawka
Harfouch	Harfouch	k1gInSc1	Harfouch
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Negib	Negib	k1gMnSc1	Negib
Greahchan	Greahchan	k1gMnSc1	Greahchan
měřili	měřit	k5eAaImAgMnP	měřit
hodnotu	hodnota	k1gFnSc4	hodnota
nikotinu	nikotin	k1gInSc2	nikotin
a	a	k8xC	a
polycyklických	polycyklický	k2eAgInPc2d1	polycyklický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
kouři	kouř	k1gInSc6	kouř
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
při	při	k7c6	při
užití	užití	k1gNnSc6	užití
melasového	melasový	k2eAgInSc2d1	melasový
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
AUB	AUB	kA	AUB
<g/>
,	,	kIx,	,
z	z	k7c2	z
průzkumu	průzkum	k1gInSc2	průzkum
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
část	část	k1gFnSc4	část
nikotinu	nikotin	k1gInSc2	nikotin
zachytí	zachytit	k5eAaPmIp3nS	zachytit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezachytí	zachytit	k5eNaPmIp3nS	zachytit
rakovinotvorné	rakovinotvorný	k2eAgNnSc1d1	rakovinotvorné
benzo	benza	k1gFnSc5	benza
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
pyreny	pyren	k1gInPc1	pyren
<g/>
,	,	kIx,	,
klíčové	klíčový	k2eAgFnPc4d1	klíčová
složky	složka	k1gFnPc4	složka
dehtu	dehet	k1gInSc2	dehet
a	a	k8xC	a
silné	silný	k2eAgInPc1d1	silný
karcinogeny	karcinogen	k1gInPc1	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Harfouch	Harfouch	k1gMnSc1	Harfouch
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
výzkumů	výzkum	k1gInPc2	výzkum
a	a	k8xC	a
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
hladinu	hladina	k1gFnSc4	hladina
nikotinu	nikotin	k1gInSc2	nikotin
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
kuřáka	kuřák	k1gMnSc2	kuřák
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
a	a	k8xC	a
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Harfouch	Harfouch	k1gMnSc1	Harfouch
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kuřák	kuřák	k1gMnSc1	kuřák
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
vykouřit	vykouřit	k5eAaPmF	vykouřit
více	hodně	k6eAd2	hodně
tabáku	tabák	k1gInSc2	tabák
než	než	k8xS	než
kuřák	kuřák	k1gInSc4	kuřák
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
stejných	stejný	k2eAgFnPc2d1	stejná
hodnot	hodnota	k1gFnPc2	hodnota
nikotinu	nikotin	k1gInSc2	nikotin
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
vyššímu	vysoký	k2eAgInSc3d2	vyšší
množství	množství	k1gNnSc1	množství
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
studie	studie	k1gFnPc1	studie
USJ	USJ	kA	USJ
a	a	k8xC	a
AUB	AUB	kA	AUB
používaly	používat	k5eAaImAgInP	používat
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
způsoby	způsob	k1gInPc1	způsob
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
stejného	stejný	k2eAgInSc2d1	stejný
výsledku	výsledek	k1gInSc2	výsledek
<g/>
:	:	kIx,	:
Voda	voda	k1gFnSc1	voda
částečně	částečně	k6eAd1	částečně
zachytí	zachytit	k5eAaPmIp3nS	zachytit
nikotin	nikotin	k1gInSc1	nikotin
v	v	k7c6	v
kouři	kouř	k1gInSc6	kouř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
karcinogenní	karcinogenní	k2eAgFnPc4d1	karcinogenní
složky	složka	k1gFnPc4	složka
dehtu	dehet	k1gInSc2	dehet
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Samer	Samer	k1gMnSc1	Samer
Jabbour	Jabbour	k1gMnSc1	Jabbour
kardiolog	kardiolog	k1gMnSc1	kardiolog
<g/>
,	,	kIx,	,
praktický	praktický	k2eAgMnSc1d1	praktický
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
"	"	kIx"	"
<g/>
Protikuřáckého	protikuřácký	k2eAgInSc2d1	protikuřácký
programu	program	k1gInSc2	program
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Quit	Quit	k2eAgInSc1d1	Quit
smoking	smoking	k1gInSc1	smoking
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
na	na	k7c6	na
AUB	AUB	kA	AUB
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Důsledky	důsledek	k1gInPc1	důsledek
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
nikotinu	nikotin	k1gInSc2	nikotin
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
dýmce	dýmka	k1gFnSc6	dýmka
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g />
.	.	kIx.	.
</s>
<s>
opakované	opakovaný	k2eAgNnSc1d1	opakované
užívání	užívání	k1gNnSc1	užívání
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
závislosti	závislost	k1gFnSc3	závislost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
opakované	opakovaný	k2eAgNnSc1d1	opakované
užívání	užívání	k1gNnSc1	užívání
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
může	moct	k5eAaImIp3nS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
riziko	riziko	k1gNnSc4	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgInPc4	tento
výzkumy	výzkum	k1gInPc4	výzkum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nutno	nutno	k6eAd1	nutno
brát	brát	k5eAaImF	brát
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
čistě	čistě	k6eAd1	čistě
orientační	orientační	k2eAgFnPc1d1	orientační
-	-	kIx~	-
byly	být	k5eAaImAgFnP	být
dělány	dělat	k5eAaImNgInP	dělat
pro	pro	k7c4	pro
přesné	přesný	k2eAgFnPc4d1	přesná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
neznáme	neznat	k5eAaImIp1nP	neznat
typ	typ	k1gInSc4	typ
použitého	použitý	k2eAgInSc2d1	použitý
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
dýmky	dýmka	k1gFnSc2	dýmka
ani	ani	k8xC	ani
uhlíků	uhlík	k1gInPc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Kouření	kouření	k1gNnSc1	kouření
vodní	vodní	k2eAgFnSc2d1	vodní
dýmky	dýmka	k1gFnSc2	dýmka
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
zdraví	zdraví	k1gNnSc4	zdraví
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
čísla	číslo	k1gNnPc4	číslo
a	a	k8xC	a
data	datum	k1gNnPc4	datum
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
určení	určení	k1gNnSc3	určení
přesných	přesný	k2eAgInPc2d1	přesný
patologických	patologický	k2eAgInPc2d1	patologický
jevů	jev	k1gInPc2	jev
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
člověka	člověk	k1gMnSc2	člověk
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
tyto	tento	k3xDgInPc4	tento
výzkumy	výzkum	k1gInPc4	výzkum
provádět	provádět	k5eAaImF	provádět
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
scénáře	scénář	k1gInPc4	scénář
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
různých	různý	k2eAgInPc2d1	různý
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
uhlíků	uhlík	k1gInPc2	uhlík
a	a	k8xC	a
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bong	bongo	k1gNnPc2	bongo
</s>
</p>
<p>
<s>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodní	vodní	k2eAgFnSc1d1	vodní
dýmka	dýmka	k1gFnSc1	dýmka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šuta	šuta	k1gFnSc1	šuta
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šťovíček	Šťovíček	k1gMnSc1	Šťovíček
<g/>
:	:	kIx,	:
Vodní	vodní	k2eAgFnSc1d1	vodní
dýmka	dýmka	k1gFnSc1	dýmka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zrádnější	zrádný	k2eAgFnSc1d2	zrádnější
než	než	k8xS	než
cigarety	cigareta	k1gFnPc1	cigareta
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Zdraví	zdraví	k1gNnSc1	zdraví
"	"	kIx"	"
<g/>
v	v	k7c6	v
cajku	cajk	k1gInSc6	cajk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2018	[number]	k4	2018
</s>
</p>
