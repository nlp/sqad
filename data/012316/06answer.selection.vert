<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
dýmka	dýmka	k1gFnSc1	dýmka
<g/>
,	,	kIx,	,
slangově	slangově	k6eAd1	slangově
vodárna	vodárna	k1gFnSc1	vodárna
<g/>
,	,	kIx,	,
farec	farec	k1gInSc1	farec
<g/>
,	,	kIx,	,
vodnice	vodnice	k1gFnSc1	vodnice
<g/>
,	,	kIx,	,
šíša	šíša	k1gFnSc1	šíša
(	(	kIx(	(
<g/>
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
taktéž	taktéž	k?	taktéž
nargila	nargila	k1gFnSc1	nargila
<g/>
,	,	kIx,	,
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
hookah	hookaha	k1gFnPc2	hookaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
používané	používaný	k2eAgNnSc1d1	používané
ke	k	k7c3	k
kouření	kouření	k1gNnSc3	kouření
<g/>
.	.	kIx.	.
</s>
