<s>
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
(	(	kIx(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1
jako	jako	k9
ÚRN	ÚRN	kA
či	či	k8xC
URNA	urna	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
specifickou	specifický	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
zejména	zejména	k9
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
servisních	servisní	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>