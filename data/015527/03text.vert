<s>
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
</s>
<s>
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
(	(	kIx(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1
jako	jako	k9
ÚRN	ÚRN	kA
či	či	k8xC
URNA	urna	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
specifickou	specifický	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
zejména	zejména	k9
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
servisních	servisní	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
je	být	k5eAaImIp3nS
určen	určen	k2eAgInSc1d1
zejména	zejména	k9
k	k	k7c3
ozbrojeným	ozbrojený	k2eAgInPc3d1
zásahům	zásah	k1gInPc3
proti	proti	k7c3
teroristům	terorista	k1gMnPc3
<g/>
,	,	kIx,
při	při	k7c6
únosech	únos	k1gInPc6
osob	osoba	k1gFnPc2
a	a	k8xC
dopravních	dopravní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
zásazích	zásah	k1gInPc6
proti	proti	k7c3
nebezpečným	bezpečný	k2eNgMnPc3d1
pachatelům	pachatel	k1gMnPc3
či	či	k8xC
při	při	k7c6
zadržování	zadržování	k1gNnSc6
rukojmích	rukojmí	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Útvar	útvar	k1gInSc1
je	být	k5eAaImIp3nS
podřízen	podřídit	k5eAaPmNgInS
náměstkovi	náměstek	k1gMnSc3
policejního	policejní	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	s	k7c7
souhlasem	souhlas	k1gInSc7
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c4
vysílání	vysílání	k1gNnSc4
útvaru	útvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
konkrétním	konkrétní	k2eAgNnSc6d1
nasazení	nasazení	k1gNnSc6
pak	pak	k6eAd1
rozhoduje	rozhodovat	k5eAaImIp3nS
velitel	velitel	k1gMnSc1
oprávněný	oprávněný	k2eAgMnSc1d1
nařídit	nařídit	k5eAaPmF
provedení	provedení	k1gNnSc4
zákroku	zákrok	k1gInSc2
pod	pod	k7c7
jednotným	jednotný	k2eAgNnSc7d1
velením	velení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
jsou	být	k5eAaImIp3nP
vybíráni	vybírat	k5eAaImNgMnP
z	z	k7c2
řad	řada	k1gFnPc2
policistů	policista	k1gMnPc2
ve	v	k7c6
vícestupňovém	vícestupňový	k2eAgNnSc6d1
výběrovém	výběrový	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
a	a	k8xC
dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
podrobeni	podroben	k2eAgMnPc1d1
dalšímu	další	k2eAgInSc3d1
zvláštnímu	zvláštní	k2eAgInSc3d1
fyzickému	fyzický	k2eAgInSc3d1
a	a	k8xC
odbornému	odborný	k2eAgInSc3d1
výcviku	výcvik	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
určená	určený	k2eAgFnSc1d1
k	k	k7c3
provádění	provádění	k1gNnSc3
specifických	specifický	k2eAgInPc2d1
ozbrojených	ozbrojený	k2eAgInPc2d1
zásahů	zásah	k1gInPc2
proti	proti	k7c3
nebezpečným	bezpečný	k2eNgFnPc3d1
osobám	osoba	k1gFnPc3
původně	původně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
tehdejšího	tehdejší	k2eAgInSc2d1
Sboru	sbor	k1gInSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Útvar	útvar	k1gInSc1
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
ÚZU	úzus	k1gInSc2
<g/>
)	)	kIx)
SNB	SNB	kA
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
Odbor	odbor	k1gInSc1
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
OZU	OZU	kA
<g/>
)	)	kIx)
Správy	správa	k1gFnPc1
vojsk	vojsko	k1gNnPc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotka	jednotka	k1gFnSc1
původně	původně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
jako	jako	k9
reakce	reakce	k1gFnSc1
na	na	k7c4
vlnu	vlna	k1gFnSc4
terorismu	terorismus	k1gInSc2
a	a	k8xC
únosů	únos	k1gInPc2
letadel	letadlo	k1gNnPc2
ve	v	k7c6
světě	svět	k1gInSc6
i	i	k9
u	u	k7c2
nás	my	k3xPp1nPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
několika	několik	k4yIc2
reorganizací	reorganizace	k1gFnPc2
administrativně	administrativně	k6eAd1
zařazena	zařadit	k5eAaPmNgFnS
pod	pod	k7c7
různými	různý	k2eAgFnPc7d1
součástmi	součást	k1gFnPc7
SNB	SNB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
byla	být	k5eAaImAgFnS
jednotka	jednotka	k1gFnSc1
administrativně	administrativně	k6eAd1
vyčleněna	vyčleněn	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
policejní	policejní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
s	s	k7c7
celostátní	celostátní	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
jednotka	jednotka	k1gFnSc1
pojetím	pojetí	k1gNnSc7
výcviku	výcvik	k1gInSc2
a	a	k8xC
svou	svůj	k3xOyFgFnSc7
specializací	specializace	k1gFnSc7
postupně	postupně	k6eAd1
řadila	řadit	k5eAaImAgFnS
mezi	mezi	k7c4
podobné	podobný	k2eAgFnPc4d1
speciální	speciální	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
fungující	fungující	k2eAgFnPc4d1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útvar	útvar	k1gInSc1
se	se	k3xPyFc4
kupříkladu	kupříkladu	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
zatčení	zatčení	k1gNnSc4
a	a	k8xC
rozprášení	rozprášení	k1gNnSc4
Bulharského	bulharský	k2eAgInSc2d1
mafiánského	mafiánský	k2eAgInSc2d1
gangu	gang	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
obchodoval	obchodovat	k5eAaImAgMnS
s	s	k7c7
drogami	droga	k1gFnPc7
<g/>
,	,	kIx,
zbraněmi	zbraň	k1gFnPc7
a	a	k8xC
prostitucí	prostituce	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
kupř	kupř	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
útvar	útvar	k1gInSc1
zatkl	zatknout	k5eAaPmAgInS
bosse	boss	k1gMnSc4
Albánské	albánský	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
mafie	mafie	k1gFnSc2
hledaného	hledaný	k2eAgNnSc2d1
Interpolem	interpol	k1gInSc7
a	a	k8xC
Norskou	norský	k2eAgFnSc7d1
policií	policie	k1gFnSc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
např.	např.	kA
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
roce	rok	k1gInSc6
2000	#num#	k4
URNA	urna	k1gFnSc1
provedla	provést	k5eAaPmAgFnS
zatčení	zatčení	k1gNnSc4
Jiřího	Jiří	k1gMnSc2
Kajínka	Kajínek	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
utekl	utéct	k5eAaPmAgMnS
z	z	k7c2
Věznice	věznice	k1gFnSc2
Mírov	Mírov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
zajišťuje	zajišťovat	k5eAaImIp3nS
útvar	útvar	k1gInSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
ochranu	ochrana	k1gFnSc4
diplomatů	diplomat	k1gMnPc2
v	v	k7c6
Iráku	Irák	k1gInSc6
a	a	k8xC
Pákistánu	Pákistán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
URNA	urna	k1gFnSc1
udržuje	udržovat	k5eAaImIp3nS
pravidelné	pravidelný	k2eAgInPc4d1
kontakty	kontakt	k1gInPc4
např.	např.	kA
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1
22	#num#	k4
SAS	Sas	k1gMnSc1
<g/>
,	,	kIx,
francouzskou	francouzský	k2eAgFnSc4d1
GIGN	GIGN	kA
a	a	k8xC
RAID	raid	k1gInSc4
<g/>
,	,	kIx,
belgickou	belgický	k2eAgFnSc4d1
SIE	SIE	kA
<g/>
,	,	kIx,
italskou	italský	k2eAgFnSc4d1
NOCS	NOCS	kA
a	a	k8xC
GIS	gis	k1gNnSc7
<g/>
,	,	kIx,
švédskou	švédský	k2eAgFnSc7d1
CTU	CTU	kA
<g/>
,	,	kIx,
slovenskou	slovenský	k2eAgFnSc4d1
ÚOU	ÚOU	kA
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
speciálními	speciální	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
či	či	k8xC
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
tří	tři	k4xCgFnPc2
sekcí	sekce	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Sekce	sekce	k1gFnSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
–	–	k?
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
třemi	tři	k4xCgFnPc7
zásahovými	zásahový	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
,	,	kIx,
specialisté	specialista	k1gMnPc1
<g/>
,	,	kIx,
pyrotechnici	pyrotechnik	k1gMnPc1
<g/>
,	,	kIx,
výcviková	výcvikový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sekce	sekce	k1gFnSc1
speciálních	speciální	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
–	–	k?
jsou	být	k5eAaImIp3nP
v	v	k7c6
ní	on	k3xPp3gFnSc6
zařazeni	zařadit	k5eAaPmNgMnP
příslušníci	příslušník	k1gMnPc1
se	s	k7c7
zvláštními	zvláštní	k2eAgFnPc7d1
odbornostmi	odbornost	k1gFnPc7
jako	jako	k8xS,k8xC
např.	např.	kA
odstřelovači	odstřelovač	k1gMnPc1
<g/>
,	,	kIx,
spojaři	spojař	k1gMnPc1
<g/>
,	,	kIx,
dokumentaristé	dokumentarista	k1gMnPc1
<g/>
,	,	kIx,
vyjednavači	vyjednavač	k1gMnPc1
<g/>
,	,	kIx,
řidiči	řidič	k1gMnPc1
atd.	atd.	kA
</s>
<s>
Sekce	sekce	k1gFnSc1
administrativně-logistická	administrativně-logistický	k2eAgFnSc1d1
–	–	k?
zabezpečuje	zabezpečovat	k5eAaImIp3nS
organizační	organizační	k2eAgNnSc1d1
<g/>
,	,	kIx,
materiálové	materiálový	k2eAgNnSc1d1
a	a	k8xC
personální	personální	k2eAgNnSc1d1
zázemí	zázemí	k1gNnSc1
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
sekretariát	sekretariát	k1gInSc1
<g/>
,	,	kIx,
právní	právní	k2eAgInPc1d1
služby	služba	k1gFnPc4
a	a	k8xC
administrativní	administrativní	k2eAgMnPc4d1
pracovníky	pracovník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
celorepublikový	celorepublikový	k2eAgInSc1d1
útvar	útvar	k1gInSc1
má	mít	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc2
ředitele	ředitel	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jím	on	k3xPp3gMnSc7
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
JUDr.	JUDr.	kA
Libor	Libor	k1gMnSc1
Lochman	Lochman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výběrové	výběrový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
</s>
<s>
Ještě	ještě	k9
než	než	k8xS
se	se	k3xPyFc4
jedinec	jedinec	k1gMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
pro	pro	k7c4
službu	služba	k1gFnSc4
u	u	k7c2
ÚRN	ÚRN	kA
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
napřed	napřed	k6eAd1
splňovat	splňovat	k5eAaImF
základní	základní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
minimální	minimální	k2eAgInSc4d1
věk	věk	k1gInSc4
23	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
3	#num#	k4
roky	rok	k1gInPc1
služby	služba	k1gFnSc2
u	u	k7c2
policie	policie	k1gFnSc2
+	+	kIx~
odborné	odborný	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
a	a	k8xC
výcvik	výcvik	k1gInSc1
</s>
<s>
minimálně	minimálně	k6eAd1
středoškolské	středoškolský	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
</s>
<s>
dobrý	dobrý	k2eAgInSc1d1
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
a	a	k8xC
fyzická	fyzický	k2eAgFnSc1d1
kondice	kondice	k1gFnSc1
</s>
<s>
řidičský	řidičský	k2eAgInSc4d1
průkaz	průkaz	k1gInSc4
skupiny	skupina	k1gFnSc2
B	B	kA
</s>
<s>
plavec	plavec	k1gMnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jakmile	jakmile	k8xS
jedinec	jedinec	k1gMnSc1
splní	splnit	k5eAaPmIp3nS
základní	základní	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
zúčastnit	zúčastnit	k5eAaPmF
výběrového	výběrový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
3	#num#	k4
fází	fáze	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
fáze	fáze	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
fáze	fáze	k1gFnSc1
je	být	k5eAaImIp3nS
univerzální	univerzální	k2eAgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
prověřit	prověřit	k5eAaPmF
jak	jak	k8xS,k8xC
psychiku	psychika	k1gFnSc4
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
fyzickou	fyzický	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uchazeč	uchazeč	k1gMnSc1
musí	muset	k5eAaImIp3nS
projít	projít	k5eAaPmF
komplexním	komplexní	k2eAgNnSc7d1
lékařským	lékařský	k2eAgNnSc7d1
vyšetřením	vyšetření	k1gNnSc7
(	(	kIx(
<g/>
provádí	provádět	k5eAaImIp3nS
ZÚSS	ZÚSS	kA
MVČR	MVČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
vůbec	vůbec	k9
schopen	schopen	k2eAgMnSc1d1
zvládat	zvládat	k5eAaImF
nadměrnou	nadměrný	k2eAgFnSc4d1
fyzickou	fyzický	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
účastníka	účastník	k1gMnSc4
čeká	čekat	k5eAaImIp3nS
6	#num#	k4
až	až	k9
8	#num#	k4
hodin	hodina	k1gFnPc2
psychických	psychický	k2eAgInPc2d1
testů	test	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
prověří	prověřit	k5eAaPmIp3nP
veškeré	veškerý	k3xTgInPc1
aspekty	aspekt	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
odolnost	odolnost	k1gFnSc4
vůči	vůči	k7c3
stresu	stres	k1gInSc3
<g/>
,	,	kIx,
či	či	k8xC
schopnost	schopnost	k1gFnSc4
pracovat	pracovat	k5eAaImF
v	v	k7c6
týmu	tým	k1gInSc6
a	a	k8xC
testy	testa	k1gFnSc2
tělesné	tělesný	k2eAgFnSc2d1
zdatnosti	zdatnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Test	test	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
zdatnosti	zdatnost	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
60	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
5000	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
kliky	klika	k1gFnPc1
–	–	k?
maximální	maximální	k2eAgInSc4d1
počet	počet	k1gInSc4
za	za	k7c4
minutu	minuta	k1gFnSc4
</s>
<s>
shyby	shyb	k1gInPc1
na	na	k7c6
hrazdě	hrazda	k1gFnSc6
–	–	k?
maximální	maximální	k2eAgInSc4d1
počet	počet	k1gInSc4
</s>
<s>
sedy	sed	k1gInPc1
lehy	leh	k1gInPc1
–	–	k?
maximální	maximální	k2eAgInSc4d1
počet	počet	k1gInSc4
za	za	k7c4
2	#num#	k4
minuty	minuta	k1gFnPc4
</s>
<s>
plavání	plavání	k1gNnSc1
na	na	k7c4
400	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
šplh	šplh	k1gInSc1
na	na	k7c6
laně	lano	k1gNnSc6
4,5	4,5	k4
metru	metr	k1gInSc2
</s>
<s>
překážková	překážkový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
2	#num#	k4
kola	kolo	k1gNnSc2
/	/	kIx~
4	#num#	k4
minuty	minuta	k1gFnPc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
fáze	fáze	k1gFnSc1
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
první	první	k4xOgFnPc4
fáze	fáze	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
druhá	druhý	k4xOgFnSc1
fáze	fáze	k1gFnSc1
je	být	k5eAaImIp3nS
univerzální	univerzální	k2eAgFnSc1d1
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
zhodnotit	zhodnotit	k5eAaPmF
vliv	vliv	k1gInSc4
extrémní	extrémní	k2eAgFnSc2d1
fyzické	fyzický	k2eAgFnSc2d1
a	a	k8xC
psychické	psychický	k2eAgFnSc2d1
zátěže	zátěž	k1gFnSc2
při	při	k7c6
delším	dlouhý	k2eAgNnSc6d2
působení	působení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
šestidenní	šestidenní	k2eAgInSc4d1
pochod	pochod	k1gInSc4
s	s	k7c7
batohem	batoh	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
prověří	prověřit	k5eAaPmIp3nS
uchazečovu	uchazečův	k2eAgFnSc4d1
motivaci	motivace	k1gFnSc4
a	a	k8xC
schopnost	schopnost	k1gFnSc4
vydržet	vydržet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uchazeči	uchazeč	k1gMnPc1
během	během	k7c2
těchto	tento	k3xDgFnPc2
šesti	šest	k4xCc2
dnů	den	k1gInPc2
téměř	téměř	k6eAd1
vůbec	vůbec	k9
nespí	spát	k5eNaImIp3nS
a	a	k8xC
nejí	jíst	k5eNaImIp3nS
a	a	k8xC
jsou	být	k5eAaImIp3nP
nuceni	nutit	k5eAaImNgMnP
provádět	provádět	k5eAaImF
často	často	k6eAd1
důležitá	důležitý	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediné	jediné	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c4
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
se	se	k3xPyFc4
dává	dávat	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pitný	pitný	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakončení	zakončení	k1gNnSc1
druhé	druhý	k4xOgFnSc2
fáze	fáze	k1gFnSc2
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
u	u	k7c2
mostu	most	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yQgNnSc2,k3yRgNnSc2,k3yIgNnSc2
se	se	k3xPyFc4
uchazeči	uchazeč	k1gMnPc1
musejí	muset	k5eAaImIp3nP
zhoupnout	zhoupnout	k5eAaPmF
v	v	k7c6
postroji	postroj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
fáze	fáze	k1gFnSc1
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
výběrového	výběrový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
je	být	k5eAaImIp3nS
pohovor	pohovor	k1gInSc4
před	před	k7c7
komisí	komise	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
klade	klást	k5eAaImIp3nS
osobní	osobní	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
například	například	k6eAd1
zjistit	zjistit	k5eAaPmF
vaši	váš	k3xOp2gFnSc4
motivaci	motivace	k1gFnSc4
k	k	k7c3
nástupu	nástup	k1gInSc3
do	do	k7c2
Útvaru	útvar	k1gInSc2
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
absolvování	absolvování	k1gNnSc6
poslední	poslední	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
a	a	k8xC
výběru	výběr	k1gInSc2
čeká	čekat	k5eAaImIp3nS
adepta	adept	k1gMnSc4
výcvik	výcvik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výcvik	výcvik	k1gInSc1
a	a	k8xC
příprava	příprava	k1gFnSc1
</s>
<s>
Zaměření	zaměření	k1gNnSc1
výcviku	výcvik	k1gInSc2
</s>
<s>
Výcvik	výcvik	k1gInSc1
URNA	urna	k1gFnSc1
je	být	k5eAaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
splnil	splnit	k5eAaPmAgMnS
požadavky	požadavek	k1gInPc4
vycházející	vycházející	k2eAgInPc4d1
ze	z	k7c2
základních	základní	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
jednotky	jednotka	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
boje	boj	k1gInSc2
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
<g/>
,	,	kIx,
poskytování	poskytování	k1gNnSc3
služeb	služba	k1gFnPc2
pro	pro	k7c4
jiné	jiný	k2eAgInPc4d1
výkonné	výkonný	k2eAgInPc4d1
útvary	útvar	k1gInPc4
PČR	PČR	kA
<g/>
,	,	kIx,
zejména	zejména	k9
při	při	k7c6
zatýkání	zatýkání	k1gNnSc6
nebezpečných	bezpečný	k2eNgMnPc2d1
pachatelů	pachatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádrem	jádro	k1gNnSc7
výcviku	výcvik	k1gInSc2
jsou	být	k5eAaImIp3nP
taktické	taktický	k2eAgInPc1d1
postupy	postup	k1gInPc1
zásahů	zásah	k1gInPc2
na	na	k7c6
letadlo	letadlo	k1gNnSc4
<g/>
,	,	kIx,
budovy	budova	k1gFnPc4
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc1
<g/>
,	,	kIx,
autobusy	autobus	k1gInPc1
a	a	k8xC
metro	metro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
výcviku	výcvik	k1gInSc2
jsou	být	k5eAaImIp3nP
i	i	k9
postupy	postup	k1gInPc1
při	při	k7c6
zadržení	zadržení	k1gNnSc6
pachatelů	pachatel	k1gMnPc2
<g/>
,	,	kIx,
ochraně	ochrana	k1gFnSc3
VIP	VIP	kA
a	a	k8xC
ozbrojených	ozbrojený	k2eAgInPc6d1
doprovodech	doprovod	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
druhy	druh	k1gInPc1
výcviku	výcvik	k1gInSc2
–	–	k?
služební	služební	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
</s>
<s>
Takticko-speciální	takticko-speciální	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
:	:	kIx,
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
letadlo	letadlo	k1gNnSc1
<g/>
,	,	kIx,
automobil	automobil	k1gInSc1
<g/>
,	,	kIx,
autobus	autobus	k1gInSc1
aj.	aj.	kA
</s>
<s>
Střelecká	střelecký	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
</s>
<s>
Práce	práce	k1gFnSc1
ve	v	k7c6
výškách	výška	k1gFnPc6
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
(	(	kIx(
<g/>
sebeobrana	sebeobrana	k1gFnSc1
<g/>
,	,	kIx,
obecná	obecný	k2eAgFnSc1d1
tělesná	tělesný	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podpůrné	podpůrný	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
</s>
<s>
V	v	k7c6
podpůrné	podpůrný	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
je	být	k5eAaImIp3nS
zahrnuta	zahrnut	k2eAgFnSc1d1
<g/>
:	:	kIx,
spojovací	spojovací	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
výsadková	výsadkový	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
topografická	topografický	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
obsluha	obsluha	k1gFnSc1
speciální	speciální	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
psychologická	psychologický	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
proškolování	proškolování	k1gNnSc1
služebních	služební	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
a	a	k8xC
norem	norma	k1gFnPc2
<g/>
,	,	kIx,
základní	základní	k2eAgFnSc1d1
ženijní	ženijní	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
speciální	speciální	k2eAgFnSc1d1
ženijní	ženijní	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
,	,	kIx,
příprava	příprava	k1gFnSc1
odstřelovačů	odstřelovač	k1gMnPc2
<g/>
,	,	kIx,
příprava	příprava	k1gFnSc1
potápěčů	potápěč	k1gMnPc2
či	či	k8xC
příprava	příprava	k1gFnSc1
řidičů	řidič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
–	–	k?
Historie	historie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
–	–	k?
Nejznámější	známý	k2eAgInPc4d3
zásahy	zásah	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hledá	hledat	k5eAaImIp3nS
se	se	k3xPyFc4
nové	nový	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
vyslance	vyslanec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Pákistánu	Pákistán	k1gInSc2
míří	mířit	k5eAaImIp3nS
URNA	urna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
–	–	k?
Struktura	struktura	k1gFnSc1
<g/>
,	,	kIx,
výcvik	výcvik	k1gInSc1
a	a	k8xC
příprava	příprava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šéf	šéf	k1gMnSc1
URNA	urna	k1gFnSc1
<g/>
:	:	kIx,
Nemůžeme	moct	k5eNaImIp1nP
být	být	k5eAaImF
všude	všude	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
nejsou	být	k5eNaImIp3nP
ovce	ovce	k1gFnPc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
mít	mít	k5eAaImF
možnost	možnost	k1gFnSc4
bránit	bránit	k5eAaImF
se	se	k3xPyFc4
střelnou	střelný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-06-14	2016-06-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zpravodajství	zpravodajství	k1gNnSc4
–	–	k?
Policie	policie	k1gFnSc1
cvičila	cvičit	k5eAaImAgFnS
v	v	k7c6
Motole	Motol	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Prezident	prezident	k1gMnSc1
republiky	republika	k1gFnSc2
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
nové	nový	k2eAgMnPc4d1
generály	generál	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrad	hrad	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-10-28	2017-10-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
–	–	k?
Výběr	výběr	k1gInSc4
uchazečů	uchazeč	k1gMnPc2
o	o	k7c4
službu	služba	k1gFnSc4
v	v	k7c6
Útvaru	útvar	k1gInSc6
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výběrové	výběrový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útvar	útvar	k1gInSc1
rychlého	rychlý	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
Policie	policie	k1gFnSc2
ČR	ČR	kA
–	–	k?
fan	fana	k1gFnPc2
stránky	stránka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
STEHLÍK	Stehlík	k1gMnSc1
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
URNA	urna	k1gFnSc1
<g/>
:	:	kIx,
30	#num#	k4
let	léto	k1gNnPc2
policejní	policejní	k2eAgFnSc2d1
protiteroristické	protiteroristický	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohelnice	Mohelnice	k1gFnSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Vaňourek	Vaňourek	k1gMnSc1
<g/>
;	;	kIx,
V	v	k7c6
ráji	ráj	k1gInSc6
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
303	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904588	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86758	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
URNA	urna	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
URNA	urna	k1gFnSc1
PČR	PČR	kA
</s>
