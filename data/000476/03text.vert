<s>
Panamský	panamský	k2eAgInSc1d1	panamský
průplav	průplav	k1gInSc1	průplav
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Canal	Canal	k1gInSc1	Canal
de	de	k?	de
Panamá	Panamý	k2eAgFnSc1d1	Panamá
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Panama	panama	k2eAgInSc1d1	panama
Canal	Canal	k1gInSc1	Canal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgInSc1d1	námořní
průplav	průplav	k1gInSc1	průplav
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
skrz	skrz	k7c4	skrz
Panamskou	panamský	k2eAgFnSc4d1	Panamská
šíji	šíje	k1gFnSc4	šíje
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
konkrétněji	konkrétně	k6eAd2	konkrétně
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
Panamský	panamský	k2eAgInSc1d1	panamský
záliv	záliv	k1gInSc1	záliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
z	z	k7c2	z
karibské	karibský	k2eAgFnSc2d1	karibská
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
města	město	k1gNnSc2	město
Colón	colón	k1gInSc1	colón
<g/>
,	,	kIx,	,
tichomořský	tichomořský	k2eAgMnSc1d1	tichomořský
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
Panamá	Panamý	k2eAgFnSc1d1	Panamá
<g/>
.	.	kIx.	.
</s>
<s>
Panamský	panamský	k2eAgInSc1d1	panamský
průplav	průplav	k1gInSc1	průplav
byl	být	k5eAaImAgInS	být
budován	budovat	k5eAaImNgInS	budovat
Francouzi	Francouz	k1gMnSc3	Francouz
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1880	[number]	k4	1880
až	až	k9	až
1889	[number]	k4	1889
a	a	k8xC	a
následně	následně	k6eAd1	následně
Američany	Američan	k1gMnPc7	Američan
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901	[number]	k4	1901
až	až	k9	až
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byly	být	k5eAaImAgFnP	být
samotný	samotný	k2eAgInSc4d1	samotný
průplav	průplav	k1gInSc4	průplav
a	a	k8xC	a
šestimílový	šestimílový	k2eAgInSc4d1	šestimílový
pás	pás	k1gInSc4	pás
území	území	k1gNnSc2	území
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
jeho	jeho	k3xOp3gFnPc6	jeho
březích	březí	k2eAgInPc2d1	březí
výsostným	výsostný	k2eAgNnSc7d1	výsostné
územím	území	k1gNnSc7	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc1	průplav
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
81,6	[number]	k4	81,6
km	km	kA	km
a	a	k8xC	a
široký	široký	k2eAgInSc1d1	široký
150	[number]	k4	150
až	až	k8xS	až
305	[number]	k4	305
m.	m.	k?	m.
Před	před	k7c7	před
rozšířením	rozšíření	k1gNnSc7	rozšíření
měl	mít	k5eAaImAgInS	mít
průplav	průplav	k1gInSc4	průplav
tři	tři	k4xCgNnPc4	tři
zdymadla	zdymadlo	k1gNnPc4	zdymadlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
na	na	k7c6	na
tichomořské	tichomořský	k2eAgFnSc6d1	tichomořská
straně	strana	k1gFnSc6	strana
v	v	k7c4	v
Miraflores	Miraflores	k1gInSc4	Miraflores
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c4	v
Pedro	Pedro	k1gNnSc4	Pedro
Miguel	Miguela	k1gFnPc2	Miguela
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
stupeň	stupeň	k1gInSc1	stupeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
atlantické	atlantický	k2eAgFnSc6d1	Atlantická
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
zdymadlo	zdymadlo	k1gNnSc1	zdymadlo
Gatun	Gatuna	k1gFnPc2	Gatuna
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
stupni	stupeň	k1gInPc7	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
zdymadel	zdymadlo	k1gNnPc2	zdymadlo
napouštěna	napouštěn	k2eAgNnPc4d1	napouštěno
z	z	k7c2	z
Gatunského	Gatunský	k2eAgNnSc2d1	Gatunský
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yRgNnSc4	který
kanál	kanál	k1gInSc1	kanál
také	také	k9	také
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Gatunské	Gatunský	k2eAgNnSc1d1	Gatunský
jezero	jezero	k1gNnSc1	jezero
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Chagres	Chagresa	k1gFnPc2	Chagresa
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgNnSc1d1	další
přehradní	přehradní	k2eAgNnSc1d1	přehradní
jezero	jezero	k1gNnSc1	jezero
-	-	kIx~	-
Alajuela	Alajuela	k1gFnSc1	Alajuela
-	-	kIx~	-
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
odtok	odtok	k1gInSc1	odtok
během	během	k7c2	během
roku	rok	k1gInSc2	rok
reguluje	regulovat	k5eAaImIp3nS	regulovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
přítok	přítok	k1gInSc1	přítok
do	do	k7c2	do
Gatunského	Gatunský	k2eAgNnSc2d1	Gatunský
jezera	jezero	k1gNnSc2	jezero
co	co	k3yRnSc4	co
nejvíce	nejvíce	k6eAd1	nejvíce
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
byla	být	k5eAaImAgFnS	být
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
panamských	panamský	k2eAgInPc2d1	panamský
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
řeky	řeka	k1gFnSc2	řeka
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Chagres	Chagresa	k1gFnPc2	Chagresa
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc4	průplav
překonávají	překonávat	k5eAaImIp3nP	překonávat
dva	dva	k4xCgInPc4	dva
pevné	pevný	k2eAgInPc4d1	pevný
mosty	most	k1gInPc4	most
-	-	kIx~	-
Puente	Puent	k1gInSc5	Puent
de	de	k?	de
las	laso	k1gNnPc2	laso
Américas	Américas	k1gInSc1	Américas
a	a	k8xC	a
Puente	Puent	k1gMnSc5	Puent
Centenario	Centenaria	k1gMnSc5	Centenaria
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
kontinuitu	kontinuita	k1gFnSc4	kontinuita
dálnice	dálnice	k1gFnSc2	dálnice
Panamericana	Panamerican	k1gMnSc2	Panamerican
a	a	k8xC	a
spojují	spojovat	k5eAaImIp3nP	spojovat
Severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
probíhaly	probíhat	k5eAaImAgFnP	probíhat
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
podstatném	podstatný	k2eAgNnSc6d1	podstatné
rozšíření	rozšíření	k1gNnSc6	rozšíření
průplavu	průplav	k1gInSc2	průplav
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
proplutí	proplutí	k1gNnSc1	proplutí
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
tonáží	tonáž	k1gFnSc7	tonáž
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lodě	loď	k1gFnPc1	loď
mají	mít	k5eAaImIp3nP	mít
název	název	k1gInSc4	název
Post-Panamax	Post-Panamax	k1gInSc1	Post-Panamax
ships	ships	k1gInSc1	ships
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
provedení	provedení	k1gNnSc4	provedení
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
byly	být	k5eAaImAgInP	být
vyčísleny	vyčíslit	k5eAaPmNgInP	vyčíslit
částkou	částka	k1gFnSc7	částka
5,25	[number]	k4	5,25
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
byly	být	k5eAaImAgFnP	být
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
části	část	k1gFnPc1	část
kanálu	kanál	k1gInSc2	kanál
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
zdymadly	zdymadlo	k1gNnPc7	zdymadlo
uvedeny	uvést	k5eAaPmNgInP	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
devět	devět	k4xCc1	devět
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
a	a	k8xC	a
30	[number]	k4	30
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
proplula	proplout	k5eAaPmAgFnS	proplout
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
průplavem	průplav	k1gInSc7	průplav
-	-	kIx~	-
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
Karibiku	Karibik	k1gInSc2	Karibik
k	k	k7c3	k
Tichému	tichý	k2eAgInSc3d1	tichý
oceánu	oceán	k1gInSc3	oceán
-	-	kIx~	-
loď	loď	k1gFnSc1	loď
čínské	čínský	k2eAgFnSc2d1	čínská
společnosti	společnost	k1gFnSc2	společnost
Cosco	Cosco	k6eAd1	Cosco
Shipping	Shipping	k1gInSc4	Shipping
široká	široký	k2eAgNnPc4d1	široké
48,2	[number]	k4	48,2
metru	metro	k1gNnSc6	metro
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
9	[number]	k4	9
000	[number]	k4	000
standardních	standardní	k2eAgInPc2d1	standardní
kontejnerů	kontejner	k1gInPc2	kontejner
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkou	myšlenka	k1gFnSc7	myšlenka
propojení	propojení	k1gNnSc2	propojení
Atlantského	atlantský	k2eAgInSc2d1	atlantský
a	a	k8xC	a
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
španělští	španělský	k2eAgMnPc1d1	španělský
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
zabývat	zabývat	k5eAaImF	zabývat
již	již	k9	již
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
kolonizace	kolonizace	k1gFnSc2	kolonizace
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
variant	varianta	k1gFnPc2	varianta
a	a	k8xC	a
nápadů	nápad	k1gInPc2	nápad
vykrystalizovaly	vykrystalizovat	k5eAaPmAgFnP	vykrystalizovat
dvě	dva	k4xCgFnPc1	dva
možnosti	možnost	k1gFnPc1	možnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
rozvíjeny	rozvíjet	k5eAaImNgFnP	rozvíjet
-	-	kIx~	-
přes	přes	k7c4	přes
území	území	k1gNnSc4	území
dnešních	dnešní	k2eAgInPc2d1	dnešní
států	stát	k1gInPc2	stát
Panama	Panama	k1gFnSc1	Panama
a	a	k8xC	a
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
Nikaragujský	nikaragujský	k2eAgInSc1d1	nikaragujský
průplav	průplav	k1gInSc1	průplav
doposud	doposud	k6eAd1	doposud
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
konferenci	konference	k1gFnSc6	konference
Geografické	geografický	k2eAgFnSc2d1	geografická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
představil	představit	k5eAaPmAgMnS	představit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Lesseps	Lessepsa	k1gFnPc2	Lessepsa
projekt	projekt	k1gInSc4	projekt
zbudování	zbudování	k1gNnSc4	zbudování
průplavu	průplav	k1gInSc2	průplav
přes	přes	k7c4	přes
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
provincii	provincie	k1gFnSc4	provincie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Panamské	panamský	k2eAgFnSc2d1	Panamská
šíje	šíj	k1gFnSc2	šíj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
začali	začít	k5eAaPmAgMnP	začít
Francouzi	Francouz	k1gMnPc1	Francouz
budovat	budovat	k5eAaImF	budovat
kanál	kanál	k1gInSc4	kanál
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ručně	ručně	k6eAd1	ručně
mýtili	mýtit	k5eAaImAgMnP	mýtit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
průplav	průplav	k1gInSc4	průplav
<g/>
,	,	kIx,	,
mapovali	mapovat	k5eAaImAgMnP	mapovat
a	a	k8xC	a
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
budoucí	budoucí	k2eAgFnSc4d1	budoucí
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc1	budování
bylo	být	k5eAaImAgNnS	být
obtížnější	obtížný	k2eAgNnSc1d2	obtížnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
znepříjemňována	znepříjemňován	k2eAgFnSc1d1	znepříjemňován
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
malárie	malárie	k1gFnSc1	malárie
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
zimnice	zimnice	k1gFnSc1	zimnice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přenášeli	přenášet	k5eAaImAgMnP	přenášet
komáři	komár	k1gMnPc1	komár
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
Chagres	Chagresa	k1gFnPc2	Chagresa
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
přicházející	přicházející	k2eAgInPc4d1	přicházející
sesuvy	sesuv	k1gInPc4	sesuv
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
naplnila	naplnit	k5eAaPmAgFnS	naplnit
slova	slovo	k1gNnSc2	slovo
Adolpha	Adolph	k1gMnSc2	Adolph
Godiny	Godina	k1gMnSc2	Godina
de	de	k?	de
Lepinay	Lepinaa	k1gFnSc2	Lepinaa
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
projekt	projekt	k1gInSc1	projekt
zkrachoval	zkrachovat	k5eAaPmAgInS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
stál	stát	k5eAaImAgInS	stát
průplav	průplav	k1gInSc1	průplav
20	[number]	k4	20
000	[number]	k4	000
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
19	[number]	k4	19
tisíc	tisíc	k4xCgInPc2	tisíc
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Francouzům	Francouz	k1gMnPc3	Francouz
nepodařilo	podařit	k5eNaPmAgNnS	podařit
celý	celý	k2eAgInSc4d1	celý
průplav	průplav	k1gInSc4	průplav
vybudovat	vybudovat	k5eAaPmF	vybudovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
třetina	třetina	k1gFnSc1	třetina
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
budoucím	budoucí	k2eAgInSc6d1	budoucí
průplavu	průplav	k1gInSc6	průplav
hotova	hotov	k2eAgFnSc1d1	hotova
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétním	konkrétní	k2eAgInPc3d1	konkrétní
plánům	plán	k1gInPc3	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
průplavu	průplav	k1gInSc2	průplav
předcházela	předcházet	k5eAaImAgFnS	předcházet
stavba	stavba	k1gFnSc1	stavba
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
dokončené	dokončený	k2eAgInPc1d1	dokončený
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
mezi	mezi	k7c7	mezi
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
a	a	k8xC	a
USA	USA	kA	USA
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
Malarrino-Bidlackova	Malarrino-Bidlackův	k2eAgFnSc1d1	Malarrino-Bidlackův
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
si	se	k3xPyFc3	se
USA	USA	kA	USA
zajistily	zajistit	k5eAaPmAgFnP	zajistit
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
při	při	k7c6	při
eventuální	eventuální	k2eAgFnSc6d1	eventuální
stavbě	stavba	k1gFnSc6	stavba
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bankrotu	bankrot	k1gInSc6	bankrot
francouzké	francouzký	k2eAgFnSc2d1	francouzká
Compagnie	Compagnie	k1gFnSc2	Compagnie
Universelle	Universelle	k1gNnSc1	Universelle
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
stavitelem	stavitel	k1gMnSc7	stavitel
suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Lessepsem	Lesseps	k1gMnSc7	Lesseps
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
založili	založit	k5eAaPmAgMnP	založit
její	její	k3xOp3gMnPc1	její
majitelé	majitel	k1gMnPc1	majitel
společnost	společnost	k1gFnSc1	společnost
Compagnie	Compagnie	k1gFnSc1	Compagnie
Nouvelle	Nouvelle	k1gFnSc1	Nouvelle
du	du	k?	du
Canal	Canal	k1gInSc1	Canal
de	de	k?	de
Panama	Panama	k1gFnSc1	Panama
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
převedli	převést	k5eAaPmAgMnP	převést
aktiva	aktivum	k1gNnSc2	aktivum
Compagnie	Compagnie	k1gFnSc2	Compagnie
Universelle	Universelle	k1gNnSc2	Universelle
a	a	k8xC	a
50	[number]	k4	50
000	[number]	k4	000
akcií	akcie	k1gFnPc2	akcie
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
bezúplatně	bezúplatně	k6eAd1	bezúplatně
vládě	vláda	k1gFnSc3	vláda
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
právnímu	právní	k2eAgInSc3d1	právní
postihu	postih	k1gInSc3	postih
za	za	k7c4	za
krach	krach	k1gInSc4	krach
Compagnie	Compagnie	k1gFnSc2	Compagnie
Universelle	Universelle	k1gFnSc2	Universelle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
získala	získat	k5eAaPmAgFnS	získat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
společnost	společnost	k1gFnSc1	společnost
koncesi	koncese	k1gFnSc4	koncese
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
koupena	koupit	k5eAaPmNgFnS	koupit
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
poté	poté	k6eAd1	poté
zrušily	zrušit	k5eAaPmAgInP	zrušit
své	svůj	k3xOyFgInPc1	svůj
plány	plán	k1gInPc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
Nikaragujského	nikaragujský	k2eAgInSc2d1	nikaragujský
průplavu	průplav	k1gInSc2	průplav
a	a	k8xC	a
započaly	započnout	k5eAaPmAgFnP	započnout
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
průplavu	průplav	k1gInSc2	průplav
přes	přes	k7c4	přes
Panamu	Panama	k1gFnSc4	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Koupě	koupě	k1gFnSc1	koupě
společnosti	společnost	k1gFnSc2	společnost
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
následovala	následovat	k5eAaImAgFnS	následovat
po	po	k7c6	po
krachu	krach	k1gInSc6	krach
Hay-Herránovy	Hay-Herránův	k2eAgFnSc2d1	Hay-Herránův
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
kolumbijským	kolumbijský	k2eAgInSc7d1	kolumbijský
parlamentem	parlament	k1gInSc7	parlament
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
Hay-Bunau-Varillova	Hay-Bunau-Varillův	k2eAgFnSc1d1	Hay-Bunau-Varillův
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ustanoveními	ustanovení	k1gNnPc7	ustanovení
ústavy	ústava	k1gFnSc2	ústava
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
Panama	Panama	k1gFnSc1	Panama
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
výlučné	výlučný	k2eAgNnSc4d1	výlučné
postavení	postavení	k1gNnSc4	postavení
USA	USA	kA	USA
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
budovaného	budovaný	k2eAgInSc2d1	budovaný
průplavu	průplav	k1gInSc2	průplav
(	(	kIx(	(
<g/>
šestimílový	šestimílový	k2eAgInSc1d1	šestimílový
pás	pás	k1gInSc1	pás
území	území	k1gNnPc2	území
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
jeho	jeho	k3xOp3gNnPc2	jeho
březích	březí	k2eAgNnPc2d1	březí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
Panamské	panamský	k2eAgNnSc1d1	Panamské
průplavové	průplavový	k2eAgNnSc1d1	průplavový
pásmo	pásmo	k1gNnSc1	pásmo
jako	jako	k8xC	jako
výsostné	výsostný	k2eAgNnSc1d1	výsostné
území	území	k1gNnSc1	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
umožněna	umožněn	k2eAgFnSc1d1	umožněna
výstavba	výstavba	k1gFnSc1	výstavba
dvou	dva	k4xCgFnPc2	dva
amerických	americký	k2eAgFnPc2d1	americká
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zajištění	zajištění	k1gNnSc2	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
proplouvajících	proplouvající	k2eAgFnPc2d1	proplouvající
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
USA	USA	kA	USA
zavázaly	zavázat	k5eAaPmAgInP	zavázat
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vyplatily	vyplatit	k5eAaPmAgInP	vyplatit
Panamě	Panama	k1gFnSc3	Panama
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
poplatek	poplatek	k1gInSc1	poplatek
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
příslib	příslib	k1gInSc4	příslib
placení	placení	k1gNnSc2	placení
každoročního	každoroční	k2eAgInSc2d1	každoroční
poplatku	poplatek	k1gInSc2	poplatek
za	za	k7c4	za
používání	používání	k1gNnSc4	používání
průplavu	průplav	k1gInSc2	průplav
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
250	[number]	k4	250
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
James	James	k1gMnSc1	James
Carter	Carter	k1gMnSc1	Carter
a	a	k8xC	a
panamský	panamský	k2eAgMnSc1d1	panamský
prezident	prezident	k1gMnSc1	prezident
generál	generál	k1gMnSc1	generál
Omar	Omar	k1gMnSc1	Omar
Torrijos	Torrijos	k1gMnSc1	Torrijos
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stanovila	stanovit	k5eAaPmAgFnS	stanovit
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1999	[number]	k4	1999
jako	jako	k8xC	jako
den	den	k1gInSc4	den
předání	předání	k1gNnSc2	předání
Panamského	panamský	k2eAgNnSc2d1	Panamské
průplavového	průplavový	k2eAgNnSc2d1	průplavový
pásma	pásmo	k1gNnSc2	pásmo
státu	stát	k1gInSc2	stát
Panama	panama	k2eAgNnSc1d1	panama
jako	jako	k8xC	jako
neutrální	neutrální	k2eAgNnSc1d1	neutrální
území	území	k1gNnSc1	území
pod	pod	k7c7	pod
panamskou	panamský	k2eAgFnSc7d1	Panamská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
nadále	nadále	k6eAd1	nadále
garantují	garantovat	k5eAaBmIp3nP	garantovat
vnější	vnější	k2eAgFnSc4d1	vnější
obranu	obrana	k1gFnSc4	obrana
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
začali	začít	k5eAaPmAgMnP	začít
Američané	Američan	k1gMnPc1	Američan
budovat	budovat	k5eAaImF	budovat
Panamský	panamský	k2eAgInSc4d1	panamský
průplav	průplav	k1gInSc4	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
panovaly	panovat	k5eAaImAgFnP	panovat
špatné	špatný	k2eAgFnPc1d1	špatná
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
jako	jako	k8xS	jako
žlutá	žlutý	k2eAgFnSc1d1	žlutá
zimnice	zimnice	k1gFnSc1	zimnice
a	a	k8xC	a
malárie	malárie	k1gFnSc1	malárie
zužovaly	zužovat	k5eAaImAgFnP	zužovat
a	a	k8xC	a
zabíjely	zabíjet	k5eAaImAgFnP	zabíjet
jak	jak	k6eAd1	jak
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
inženýry	inženýr	k1gMnPc4	inženýr
a	a	k8xC	a
jiný	jiný	k2eAgInSc1d1	jiný
personál	personál	k1gInSc1	personál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
John	John	k1gMnSc1	John
Finley	Finlea	k1gFnSc2	Finlea
Wallace	Wallace	k1gFnSc2	Wallace
<g/>
.	.	kIx.	.
</s>
<s>
Opravoval	opravovat	k5eAaImAgMnS	opravovat
budovy	budova	k1gFnPc4	budova
po	po	k7c6	po
Francouzích	Francouzy	k1gInPc6	Francouzy
a	a	k8xC	a
přestavoval	přestavovat	k5eAaImAgInS	přestavovat
železnici	železnice	k1gFnSc4	železnice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
tu	tu	k6eAd1	tu
Francouzi	Francouz	k1gMnPc1	Francouz
zanechali	zanechat	k5eAaPmAgMnP	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
se	s	k7c7	s
zemními	zemní	k2eAgFnPc7d1	zemní
pracemi	práce	k1gFnPc7	práce
v	v	k7c4	v
Culebra	Culebr	k1gMnSc4	Culebr
Cut	Cut	k1gMnSc4	Cut
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
už	už	k9	už
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podal	podat	k5eAaPmAgMnS	podat
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
52	[number]	k4	52
<g/>
letý	letý	k2eAgMnSc1d1	letý
železniční	železniční	k2eAgMnSc1d1	železniční
inženýr	inženýr	k1gMnSc1	inženýr
John	John	k1gMnSc1	John
Frank	Frank	k1gMnSc1	Frank
Stevens	Stevens	k1gInSc4	Stevens
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
proslavil	proslavit	k5eAaPmAgMnS	proslavit
stavbou	stavba	k1gFnSc7	stavba
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
přes	přes	k7c4	přes
Skalnaté	skalnatý	k2eAgFnPc4d1	skalnatá
hory	hora	k1gFnPc4	hora
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Stevens	Stevensa	k1gFnPc2	Stevensa
zprvu	zprvu	k6eAd1	zprvu
zastavil	zastavit	k5eAaPmAgMnS	zastavit
zemní	zemní	k2eAgFnSc2d1	zemní
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
budovat	budovat	k5eAaImF	budovat
potřebné	potřebný	k2eAgNnSc4d1	potřebné
logistické	logistický	k2eAgNnSc4d1	logistické
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
zlepšení	zlepšení	k1gNnSc4	zlepšení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
i	i	k8xC	i
samotné	samotný	k2eAgMnPc4d1	samotný
inženýry	inženýr	k1gMnPc4	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
věcí	věc	k1gFnSc7	věc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
John	John	k1gMnSc1	John
Stevens	Stevens	k1gInSc4	Stevens
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
získání	získání	k1gNnSc1	získání
finančních	finanční	k2eAgInPc2d1	finanční
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaPmAgMnS	učinit
tak	tak	k9	tak
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
vojenského	vojenský	k2eAgMnSc2d1	vojenský
lékaře	lékař	k1gMnSc2	lékař
Williama	Williamum	k1gNnSc2	Williamum
Gorgase	Gorgas	k1gInSc6	Gorgas
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1854	[number]	k4	1854
v	v	k7c6	v
Mobile	mobile	k1gNnSc6	mobile
<g/>
,	,	kIx,	,
Alabama	Alabama	k1gFnSc1	Alabama
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
snažil	snažit	k5eAaImAgMnS	snažit
vymýtit	vymýtit	k5eAaPmF	vymýtit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Panamské	panamský	k2eAgFnSc2d1	Panamská
šíje	šíj	k1gFnSc2	šíj
žlutou	žlutý	k2eAgFnSc4d1	žlutá
zimnici	zimnice	k1gFnSc4	zimnice
a	a	k8xC	a
malárii	malárie	k1gFnSc4	malárie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neměl	mít	k5eNaImAgMnS	mít
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Asanace	asanace	k1gFnSc1	asanace
nemocí	nemoc	k1gFnPc2	nemoc
znamenala	znamenat	k5eAaImAgFnS	znamenat
především	především	k9	především
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
komárům	komár	k1gMnPc3	komár
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
sítě	síť	k1gFnPc1	síť
do	do	k7c2	do
oken	okno	k1gNnPc2	okno
stály	stát	k5eAaImAgInP	stát
90	[number]	k4	90
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vykuřovaly	vykuřovat	k5eAaImAgInP	vykuřovat
se	se	k3xPyFc4	se
domy	dům	k1gInPc7	dům
<g/>
,	,	kIx,	,
zaváděla	zavádět	k5eAaImAgFnS	zavádět
se	se	k3xPyFc4	se
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
,	,	kIx,	,
dláždily	dláždit	k5eAaImAgFnP	dláždit
se	se	k3xPyFc4	se
vozovky	vozovka	k1gFnPc1	vozovka
i	i	k9	i
chodníky	chodník	k1gInPc1	chodník
<g/>
,	,	kIx,	,
budovaly	budovat	k5eAaImAgFnP	budovat
se	se	k3xPyFc4	se
vodovody	vodovod	k1gInPc7	vodovod
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
stáčela	stáčet	k5eAaImAgFnS	stáčet
do	do	k7c2	do
lahví	lahev	k1gFnPc2	lahev
a	a	k8xC	a
vysoušely	vysoušet	k5eAaImAgFnP	vysoušet
se	se	k3xPyFc4	se
močály	močál	k1gInPc7	močál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
množení	množení	k1gNnSc1	množení
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Stevens	Stevensa	k1gFnPc2	Stevensa
změnil	změnit	k5eAaPmAgInS	změnit
původní	původní	k2eAgInSc1d1	původní
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
znamenal	znamenat	k5eAaImAgInS	znamenat
přehrazení	přehrazení	k1gNnSc4	přehrazení
řeky	řeka	k1gFnSc2	řeka
Chagres	Chagresa	k1gFnPc2	Chagresa
u	u	k7c2	u
Gatunu	Gatun	k1gInSc2	Gatun
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
Culebry	Culebra	k1gFnSc2	Culebra
bylo	být	k5eAaImAgNnS	být
posléze	posléze	k6eAd1	posléze
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
naplánován	naplánován	k2eAgInSc1d1	naplánován
systém	systém	k1gInSc1	systém
zdymadel	zdymadlo	k1gNnPc2	zdymadlo
napájený	napájený	k2eAgInSc1d1	napájený
z	z	k7c2	z
budoucího	budoucí	k2eAgNnSc2d1	budoucí
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
položena	položen	k2eAgFnSc1d1	položena
železnice	železnice	k1gFnSc1	železnice
k	k	k7c3	k
odvozu	odvoz	k1gInSc3	odvoz
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
přivezli	přivézt	k5eAaPmAgMnP	přivézt
výkonná	výkonný	k2eAgNnPc4d1	výkonné
parní	parní	k2eAgNnPc4d1	parní
rypadla	rypadlo	k1gNnPc4	rypadlo
<g/>
.	.	kIx.	.
</s>
<s>
Koleje	kolej	k1gFnPc1	kolej
byly	být	k5eAaImAgFnP	být
posunovány	posunovat	k5eAaImNgFnP	posunovat
nejprve	nejprve	k6eAd1	nejprve
lidskou	lidský	k2eAgFnSc7d1	lidská
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
je	on	k3xPp3gFnPc4	on
přesunovaly	přesunovat	k5eAaImAgInP	přesunovat
speciální	speciální	k2eAgInPc1d1	speciální
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Pracovalo	pracovat	k5eAaImAgNnS	pracovat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgInPc1d1	plochý
vagóny	vagón	k1gInPc1	vagón
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
postranicí	postranice	k1gFnSc7	postranice
vozily	vozit	k5eAaImAgFnP	vozit
zeminu	zemina	k1gFnSc4	zemina
na	na	k7c4	na
obrovské	obrovský	k2eAgFnPc4d1	obrovská
zemní	zemní	k2eAgFnPc4d1	zemní
hráze	hráz	k1gFnPc4	hráz
v	v	k7c6	v
Gatumu	Gatum	k1gInSc6	Gatum
a	a	k8xC	a
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
budovány	budován	k2eAgInPc1d1	budován
vlnolamy	vlnolam	k1gInPc1	vlnolam
<g/>
.	.	kIx.	.
</s>
<s>
Třítunový	třítunový	k2eAgInSc4d1	třítunový
pluh	pluh	k1gInSc4	pluh
poháněný	poháněný	k2eAgInSc4d1	poháněný
z	z	k7c2	z
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
efektivně	efektivně	k6eAd1	efektivně
vysypával	vysypávat	k5eAaImAgMnS	vysypávat
zeminu	zemina	k1gFnSc4	zemina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1905	[number]	k4	1905
oznámil	oznámit	k5eAaPmAgMnS	oznámit
hlavní	hlavní	k2eAgMnSc1d1	hlavní
lékař	lékař	k1gMnSc1	lékař
Gorgas	Gorgas	k1gMnSc1	Gorgas
<g/>
,	,	kIx,	,
že	že	k8xS	že
Panamské	panamský	k2eAgNnSc1d1	Panamské
pásmo	pásmo	k1gNnSc1	pásmo
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
žluté	žlutý	k2eAgFnSc2d1	žlutá
zimnice	zimnice	k1gFnSc2	zimnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
již	již	k6eAd1	již
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
24	[number]	k4	24
000	[number]	k4	000
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1906	[number]	k4	1906
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
provedl	provést	k5eAaPmAgInS	provést
třídenní	třídenní	k2eAgFnSc4d1	třídenní
inspekční	inspekční	k2eAgFnSc4d1	inspekční
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1907	[number]	k4	1907
dal	dát	k5eAaPmAgMnS	dát
John	John	k1gMnSc1	John
Stevens	Stevensa	k1gFnPc2	Stevensa
výpověď	výpověď	k1gFnSc1	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
příčinách	příčina	k1gFnPc6	příčina
jeho	jeho	k3xOp3gInSc2	jeho
odchodu	odchod	k1gInSc2	odchod
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
mnoho	mnoho	k4c4	mnoho
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nudil	nudit	k5eAaImAgMnS	nudit
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
vším	všecek	k3xTgNnSc7	všecek
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc4	jeho
stesk	stesk	k1gInSc4	stesk
po	po	k7c6	po
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
také	také	k9	také
odešel	odejít	k5eAaPmAgMnS	odejít
kvůli	kvůli	k7c3	kvůli
nějakému	nějaký	k3yIgInSc3	nějaký
skandálu	skandál	k1gInSc3	skandál
či	či	k8xC	či
kvůli	kvůli	k7c3	kvůli
prezidentu	prezident	k1gMnSc3	prezident
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
<g/>
,	,	kIx,	,
možná	možná	k9	možná
chtěl	chtít	k5eAaImAgInS	chtít
víc	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
již	již	k6eAd1	již
přepracován	přepracován	k2eAgMnSc1d1	přepracován
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
stresován	stresován	k2eAgMnSc1d1	stresován
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
Stevensově	Stevensův	k2eAgInSc6d1	Stevensův
odchodu	odchod	k1gInSc6	odchod
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
majora	major	k1gMnSc2	major
George	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
Goethalse	Goethalse	k1gFnSc2	Goethalse
hlavním	hlavní	k2eAgMnSc7d1	hlavní
inženýrem	inženýr	k1gMnSc7	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
dvěma	dva	k4xCgMnPc7	dva
armádními	armádní	k2eAgMnPc7d1	armádní
inženýry	inženýr	k1gMnPc7	inženýr
<g/>
,	,	kIx,	,
plukovníkem	plukovník	k1gMnSc7	plukovník
Davidem	David	k1gMnSc7	David
Gaillardem	gaillarde	k1gInSc7	gaillarde
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
řídit	řídit	k5eAaImF	řídit
zemní	zemní	k2eAgFnPc4d1	zemní
práce	práce	k1gFnPc4	práce
u	u	k7c2	u
Culebry	Culebra	k1gFnSc2	Culebra
<g/>
,	,	kIx,	,
a	a	k8xC	a
Wiliamem	Wiliam	k1gInSc7	Wiliam
Siebertem	Siebert	k1gMnSc7	Siebert
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
postavit	postavit	k5eAaPmF	postavit
gatunská	gatunský	k2eAgNnPc4d1	gatunský
zdymadla	zdymadlo	k1gNnPc4	zdymadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgFnPc1d3	nejtěžší
práce	práce	k1gFnPc1	práce
probíhaly	probíhat	k5eAaImAgFnP	probíhat
na	na	k7c6	na
překopu	překop	k1gInSc6	překop
v	v	k7c4	v
Culebra	Culebr	k1gMnSc4	Culebr
Cut	Cut	k1gMnSc4	Cut
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
bylo	být	k5eAaImAgNnS	být
odváženo	odvážit	k5eAaPmNgNnS	odvážit
200	[number]	k4	200
vlaků	vlak	k1gInPc2	vlak
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1913	[number]	k4	1913
se	se	k3xPyFc4	se
parní	parní	k2eAgInPc1d1	parní
bagry	bagr	k1gInPc1	bagr
střetly	střetnout	k5eAaPmAgFnP	střetnout
u	u	k7c2	u
Culeibry	Culeibra	k1gFnSc2	Culeibra
v	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
výkopové	výkopový	k2eAgFnSc6d1	výkopová
hloubce	hloubka	k1gFnSc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Zdymadla	zdymadlo	k1gNnPc1	zdymadlo
byla	být	k5eAaImAgNnP	být
zbudována	zbudovat	k5eAaPmNgNnP	zbudovat
z	z	k7c2	z
betonu	beton	k1gInSc2	beton
v	v	k7c6	v
segmentech	segment	k1gInPc6	segment
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
kubický	kubický	k2eAgInSc1d1	kubický
metr	metr	k1gInSc1	metr
betonu	beton	k1gInSc2	beton
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
zdymadel	zdymadlo	k1gNnPc2	zdymadlo
nalit	nalit	k2eAgMnSc1d1	nalit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Zdymadlová	zdymadlový	k2eAgNnPc4d1	zdymadlový
vrata	vrata	k1gNnPc4	vrata
jsou	být	k5eAaImIp3nP	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
elektrickými	elektrický	k2eAgInPc7d1	elektrický
motory	motor	k1gInPc7	motor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
zásobování	zásobování	k1gNnSc4	zásobování
elektřinou	elektřina	k1gFnSc7	elektřina
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
na	na	k7c6	na
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zkušební	zkušební	k2eAgFnSc1d1	zkušební
plavba	plavba	k1gFnSc1	plavba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zkušební	zkušební	k2eAgInSc1d1	zkušební
remorkér	remorkér	k1gInSc1	remorkér
Gatum	Gatum	k1gInSc1	Gatum
otestoval	otestovat	k5eAaPmAgInS	otestovat
funkčnost	funkčnost	k1gFnSc4	funkčnost
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
průplavu	průplav	k1gInSc2	průplav
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
a	a	k8xC	a
parník	parník	k1gInSc1	parník
Ankon	Ankona	k1gFnPc2	Ankona
přeplul	přeplout	k5eAaPmAgInS	přeplout
od	od	k7c2	od
Atlantského	atlantský	k2eAgMnSc2d1	atlantský
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
plného	plný	k2eAgInSc2d1	plný
provozu	provoz	k1gInSc2	provoz
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
průplav	průplav	k1gInSc1	průplav
uveden	uveden	k2eAgInSc1d1	uveden
až	až	k9	až
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zprovozňování	zprovozňování	k1gNnSc2	zprovozňování
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
okolí	okolí	k1gNnSc1	okolí
zdravější	zdravý	k2eAgNnSc1d2	zdravější
než	než	k8xS	než
území	území	k1gNnSc1	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
průplavu	průplav	k1gInSc2	průplav
stála	stát	k5eAaImAgFnS	stát
Američany	Američan	k1gMnPc4	Američan
352	[number]	k4	352
000	[number]	k4	000
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
investicí	investice	k1gFnSc7	investice
činily	činit	k5eAaImAgInP	činit
celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
639	[number]	k4	639
000	[number]	k4	000
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
7	[number]	k4	7
miliardám	miliarda	k4xCgFnPc3	miliarda
dnešních	dnešní	k2eAgMnPc2d1	dnešní
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
si	se	k3xPyFc3	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
5	[number]	k4	5
609	[number]	k4	609
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
4	[number]	k4	4
500	[number]	k4	500
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poplatky	poplatek	k1gInPc1	poplatek
za	za	k7c4	za
průjezd	průjezd	k1gInSc4	průjezd
jedné	jeden	k4xCgFnSc2	jeden
lodě	loď	k1gFnSc2	loď
průplavem	průplav	k1gInSc7	průplav
jsou	být	k5eAaImIp3nP	být
stanovovány	stanovovat	k5eAaImNgFnP	stanovovat
podle	podle	k7c2	podle
tonáže	tonáž	k1gFnSc2	tonáž
<g/>
.	.	kIx.	.
</s>
<s>
Rekordní	rekordní	k2eAgNnSc1d1	rekordní
mýtné	mýtné	k1gNnSc1	mýtné
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
331	[number]	k4	331
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
loď	loď	k1gFnSc1	loď
Disney	Disnea	k1gFnSc2	Disnea
Magic	Magice	k1gFnPc2	Magice
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgInSc4d3	nejmenší
poplatek	poplatek	k1gInSc4	poplatek
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
údajně	údajně	k6eAd1	údajně
Richard	Richard	k1gMnSc1	Richard
Halliburton	Halliburton	k1gInSc1	Halliburton
<g/>
,	,	kIx,	,
když	když	k8xS	když
plaveckým	plavecký	k2eAgInSc7d1	plavecký
stylem	styl	k1gInSc7	styl
"	"	kIx"	"
<g/>
ouško	ouško	k1gNnSc1	ouško
<g/>
"	"	kIx"	"
proplaval	proplavat	k5eAaPmAgInS	proplavat
kanálem	kanál	k1gInSc7	kanál
<g/>
,	,	kIx,	,
stálo	stát	k5eAaImAgNnS	stát
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
36	[number]	k4	36
centů	cent	k1gInPc2	cent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
výletní	výletní	k2eAgFnSc4d1	výletní
loď	loď	k1gFnSc4	loď
Queen	Quena	k1gFnPc2	Quena
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
II	II	kA	II
<g/>
.	.	kIx.	.
zaplacen	zaplatit	k5eAaPmNgInS	zaplatit
poplatek	poplatek	k1gInSc1	poplatek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
99	[number]	k4	99
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
převážející	převážející	k2eAgFnSc1d1	převážející
900	[number]	k4	900
automobilů	automobil	k1gInPc2	automobil
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
280	[number]	k4	280
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
kontejnerové	kontejnerový	k2eAgFnSc2d1	kontejnerová
lodě	loď	k1gFnSc2	loď
50	[number]	k4	50
000-250	[number]	k4	000-250
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc4	průplav
používají	používat	k5eAaImIp3nP	používat
početné	početný	k2eAgFnPc1d1	početná
různě	různě	k6eAd1	různě
velké	velký	k2eAgFnPc1d1	velká
výletní	výletní	k2eAgFnPc1d1	výletní
lodě	loď	k1gFnPc1	loď
(	(	kIx(	(
<g/>
cruise	cruise	k1gFnSc1	cruise
ships	shipsa	k1gFnPc2	shipsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
činí	činit	k5eAaImIp3nP	činit
poplatky	poplatek	k1gInPc1	poplatek
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
cca	cca	kA	cca
80	[number]	k4	80
000-300	[number]	k4	000-300
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Jachty	jachta	k1gFnPc1	jachta
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
malá	malý	k2eAgNnPc1d1	malé
plavidla	plavidlo	k1gNnPc1	plavidlo
platí	platit	k5eAaImIp3nP	platit
zhruba	zhruba	k6eAd1	zhruba
1300	[number]	k4	1300
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1977	[number]	k4	1977
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
prezidenti	prezident	k1gMnPc1	prezident
Panamy	Panama	k1gFnSc2	Panama
Omar	Omar	k1gMnSc1	Omar
Torrijos	Torrijosa	k1gFnPc2	Torrijosa
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Jimmy	Jimma	k1gFnSc2	Jimma
Carter	Cartra	k1gFnPc2	Cartra
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
postupně	postupně	k6eAd1	postupně
předat	předat	k5eAaPmF	předat
samotný	samotný	k2eAgInSc4d1	samotný
průplav	průplav	k1gInSc4	průplav
a	a	k8xC	a
pásmo	pásmo	k1gNnSc4	pásmo
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
pod	pod	k7c4	pod
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
státu	stát	k1gInSc2	stát
Panama	panama	k1gNnSc2	panama
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
přechodného	přechodný	k2eAgNnSc2d1	přechodné
období	období	k1gNnSc2	období
skončila	skončit	k5eAaPmAgFnS	skončit
dnem	dnem	k7c2	dnem
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
průplav	průplav	k1gInSc1	průplav
formálně	formálně	k6eAd1	formálně
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
plující	plující	k2eAgFnPc4d1	plující
pod	pod	k7c7	pod
vlajkami	vlajka	k1gFnPc7	vlajka
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
si	se	k3xPyFc3	se
však	však	k8xC	však
smluvně	smluvně	k6eAd1	smluvně
zajistily	zajistit	k5eAaPmAgFnP	zajistit
právo	právo	k1gNnSc4	právo
obrany	obrana	k1gFnSc2	obrana
neutrality	neutralita	k1gFnSc2	neutralita
průplavu	průplav	k1gInSc2	průplav
svými	svůj	k3xOyFgFnPc7	svůj
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
probíhaly	probíhat	k5eAaImAgFnP	probíhat
práce	práce	k1gFnPc1	práce
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
průplavu	průplav	k1gInSc2	průplav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
proplouvání	proplouvání	k1gNnSc4	proplouvání
kanálem	kanál	k1gInSc7	kanál
i	i	k8xC	i
lodím	loď	k1gFnPc3	loď
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
tonáží	tonáž	k1gFnSc7	tonáž
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Post-Panamax	Post-Panamax	k1gInSc1	Post-Panamax
ships	ships	k1gInSc1	ships
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
průplavu	průplav	k1gInSc2	průplav
bylo	být	k5eAaImAgNnS	být
odsouhlaseno	odsouhlasit	k5eAaPmNgNnS	odsouhlasit
panamským	panamský	k2eAgNnSc7d1	Panamské
referendem	referendum	k1gNnSc7	referendum
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
všech	všecek	k3xTgFnPc2	všecek
prací	práce	k1gFnPc2	práce
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
měly	mít	k5eAaImAgInP	mít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
5,25	[number]	k4	5,25
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
(	(	kIx(	(
<g/>
cca	cca	kA	cca
99,75	[number]	k4	99,75
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc1d1	funkční
původní	původní	k2eAgFnPc1d1	původní
plavební	plavební	k2eAgFnPc1d1	plavební
komory	komora	k1gFnPc1	komora
se	se	k3xPyFc4	se
dokončením	dokončení	k1gNnSc7	dokončení
projektu	projekt	k1gInSc2	projekt
získává	získávat	k5eAaImIp3nS	získávat
větší	veliký	k2eAgInSc4d2	veliký
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
správce	správce	k1gMnSc2	správce
průplavu	průplav	k1gInSc2	průplav
je	být	k5eAaImIp3nS	být
výhled	výhled	k1gInSc1	výhled
provozu	provoz	k1gInSc2	provoz
těchto	tento	k3xDgFnPc2	tento
původních	původní	k2eAgFnPc2d1	původní
plavebních	plavební	k2eAgFnPc2d1	plavební
komor	komora	k1gFnPc2	komora
časově	časově	k6eAd1	časově
neomezený	omezený	k2eNgInSc1d1	neomezený
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
projektantů	projektant	k1gMnPc2	projektant
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
nárůst	nárůst	k1gInSc1	nárůst
přepravy	přeprava	k1gFnSc2	přeprava
z	z	k7c2	z
280	[number]	k4	280
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
starým	starý	k2eAgInSc7d1	starý
průplavem	průplav	k1gInSc7	průplav
na	na	k7c4	na
510	[number]	k4	510
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2025	[number]	k4	2025
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
průplavem	průplav	k1gInSc7	průplav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgFnS	moct
maximální	maximální	k2eAgFnSc1d1	maximální
udržitelná	udržitelný	k2eAgFnSc1d1	udržitelná
kapacita	kapacita	k1gFnSc1	kapacita
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
byly	být	k5eAaImAgFnP	být
nově	nově	k6eAd1	nově
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
části	část	k1gFnPc1	část
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
po	po	k7c6	po
devítileté	devítiletý	k2eAgFnSc6d1	devítiletá
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
otevřeny	otevřen	k2eAgFnPc1d1	otevřena
pro	pro	k7c4	pro
komerční	komerční	k2eAgInSc4d1	komerční
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
