<s>
Panamský	panamský	k2eAgInSc1d1	panamský
průplav	průplav	k1gInSc1	průplav
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Canal	Canal	k1gInSc1	Canal
de	de	k?	de
Panamá	Panamý	k2eAgFnSc1d1	Panamá
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Panama	panama	k2eAgInSc1d1	panama
Canal	Canal	k1gInSc1	Canal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgInSc1d1	námořní
průplav	průplav	k1gInSc1	průplav
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
