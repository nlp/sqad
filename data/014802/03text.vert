<s>
Boyd	Boyd	k1gInSc1
County	Counta	k1gFnSc2
(	(	kIx(
<g/>
Kentucky	Kentucka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Boyd	Boyd	k1gInSc1
County	Counta	k1gFnSc2
Geografie	geografie	k1gFnSc2
</s>
<s>
Boyd	Boyd	k1gInSc1
County	Counta	k1gFnSc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Kentucky	Kentucka	k1gFnSc2
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Catlettsburg	Catlettsburg	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
okres	okres	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
38	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
82	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
419	#num#	k4
km²	km²	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC-	UTC-	k?
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
4	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
49	#num#	k4
542	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
118,2	118,2	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Kentucky	Kentucky	k6eAd1
Vznik	vznik	k1gInSc1
</s>
<s>
1860	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.boydcountyky.com	www.boydcountyky.com	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Boyd	Boyd	k1gInSc1
County	Counta	k1gFnSc2
je	být	k5eAaImIp3nS
okres	okres	k1gInSc1
v	v	k7c6
státu	stát	k1gInSc6
Kentucky	Kentucka	k1gFnSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
49	#num#	k4
542	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správním	správní	k2eAgNnSc7d1
městem	město	k1gNnSc7
okresu	okres	k1gInSc2
je	být	k5eAaImIp3nS
Catlettsburg	Catlettsburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
okresu	okres	k1gInSc2
činí	činit	k5eAaImIp3nS
419	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Boyd	Boyda	k1gFnPc2
County	Counta	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
státu	stát	k1gInSc2
Kentucky	Kentucka	k1gFnSc2
</s>
<s>
Adair	Adair	k1gMnSc1
•	•	k?
Allen	Allen	k1gMnSc1
•	•	k?
Anderson	Anderson	k1gMnSc1
•	•	k?
Ballard	Ballard	k1gMnSc1
•	•	k?
Barren	Barrno	k1gNnPc2
•	•	k?
Bath	Bath	k1gMnSc1
•	•	k?
Bell	bell	k1gInSc1
•	•	k?
Boone	Boon	k1gInSc5
•	•	k?
Bourbon	bourbon	k1gInSc1
•	•	k?
Boyd	Boyd	k1gInSc1
•	•	k?
Boyle	Boyle	k1gInSc1
•	•	k?
Bracken	Bracken	k1gInSc1
•	•	k?
Breathitt	Breathitt	k1gInSc1
•	•	k?
Breckinridge	Breckinridg	k1gFnSc2
•	•	k?
Bullitt	Bullitt	k1gMnSc1
•	•	k?
Butler	Butler	k1gMnSc1
•	•	k?
Caldwell	Caldwell	k1gMnSc1
•	•	k?
Calloway	Callowaa	k1gFnSc2
•	•	k?
Campbell	Campbell	k1gMnSc1
•	•	k?
Carlisle	Carlisle	k1gMnSc1
•	•	k?
Carroll	Carroll	k1gMnSc1
•	•	k?
Carter	Carter	k1gMnSc1
•	•	k?
Casey	Casea	k1gFnSc2
•	•	k?
Christian	Christian	k1gMnSc1
•	•	k?
Clark	Clark	k1gInSc1
•	•	k?
Clay	Claa	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Clinton	Clinton	k1gMnSc1
•	•	k?
Crittenden	Crittendna	k1gFnPc2
•	•	k?
Cumberland	Cumberland	k1gInSc1
•	•	k?
Daviess	Daviess	k1gInSc1
•	•	k?
Edmonson	Edmonson	k1gInSc1
•	•	k?
Elliott	Elliott	k1gInSc1
•	•	k?
Estill	Estill	k1gInSc1
•	•	k?
Fayette	Fayett	k1gInSc5
•	•	k?
Fleming	Fleming	k1gInSc1
•	•	k?
Floyd	Floyd	k1gInSc1
•	•	k?
Franklin	Franklin	k1gInSc1
•	•	k?
Fulton	Fulton	k1gInSc1
•	•	k?
Gallatin	Gallatina	k1gFnPc2
•	•	k?
Garrard	Garrard	k1gMnSc1
•	•	k?
Grant	grant	k1gInSc1
•	•	k?
Graves	Graves	k1gInSc1
•	•	k?
Grayson	Grayson	k1gInSc1
•	•	k?
Green	Green	k1gInSc1
•	•	k?
Greenup	Greenup	k1gInSc1
•	•	k?
Hancock	Hancock	k1gInSc1
•	•	k?
Hardin	Hardina	k1gFnPc2
•	•	k?
Harlan	Harlan	k1gMnSc1
•	•	k?
Harrison	Harrison	k1gMnSc1
•	•	k?
Hart	Hart	k1gMnSc1
•	•	k?
Henderson	Henderson	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Henry	Henry	k1gMnSc1
•	•	k?
Hickman	Hickman	k1gMnSc1
•	•	k?
Hopkins	Hopkins	k1gInSc1
•	•	k?
Jackson	Jackson	k1gMnSc1
•	•	k?
Jefferson	Jefferson	k1gMnSc1
•	•	k?
Jessamine	Jessamin	k1gInSc5
•	•	k?
Johnson	Johnson	k1gMnSc1
•	•	k?
Kenton	Kenton	k1gInSc1
•	•	k?
Knott	Knott	k1gInSc1
•	•	k?
Knox	Knox	k1gInSc1
•	•	k?
LaRue	LaRue	k1gInSc1
•	•	k?
Laurel	Laurel	k1gInSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
•	•	k?
Lee	Lea	k1gFnSc6
•	•	k?
Leslie	Leslie	k1gFnSc1
•	•	k?
Letcher	Letchra	k1gFnPc2
•	•	k?
Lewis	Lewis	k1gFnSc2
•	•	k?
Lincoln	Lincoln	k1gMnSc1
•	•	k?
Livingston	Livingston	k1gInSc1
•	•	k?
Logan	Logan	k1gInSc1
•	•	k?
Lyon	Lyon	k1gInSc1
•	•	k?
Madison	Madison	k1gInSc1
•	•	k?
Magoffin	Magoffin	k1gInSc1
•	•	k?
Marion	Marion	k1gInSc1
•	•	k?
Marshall	Marshall	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Martin	Martin	k1gMnSc1
•	•	k?
Mason	mason	k1gMnSc1
•	•	k?
McCracken	McCracken	k1gInSc1
•	•	k?
McCreary	McCreara	k1gFnSc2
•	•	k?
McLean	McLean	k1gMnSc1
•	•	k?
Meade	Mead	k1gInSc5
•	•	k?
Menifee	Menife	k1gFnSc2
•	•	k?
Mercer	Mercer	k1gMnSc1
•	•	k?
Metcalfe	Metcalf	k1gInSc5
•	•	k?
Monroe	Monroe	k1gFnSc1
•	•	k?
Montgomery	Montgomera	k1gFnSc2
•	•	k?
Morgan	morgan	k1gMnSc1
•	•	k?
Muhlenberg	Muhlenberg	k1gMnSc1
•	•	k?
Nelson	Nelson	k1gMnSc1
•	•	k?
Nicholas	Nicholas	k1gMnSc1
•	•	k?
Ohio	Ohio	k1gNnSc4
•	•	k?
Oldham	Oldham	k1gInSc1
•	•	k?
Owen	Owen	k1gInSc1
•	•	k?
Owsley	Owslea	k1gFnSc2
•	•	k?
Pendleton	Pendleton	k1gInSc1
•	•	k?
Perry	Perra	k1gFnSc2
•	•	k?
Pike	Pik	k1gFnSc2
•	•	k?
Powell	Powell	k1gMnSc1
•	•	k?
Pulaski	Pulask	k1gFnSc2
•	•	k?
Robertson	Robertson	k1gMnSc1
•	•	k?
Rockcastle	Rockcastle	k1gMnSc1
•	•	k?
Rowan	Rowan	k1gMnSc1
•	•	k?
Russell	Russell	k1gMnSc1
•	•	k?
Scott	Scott	k1gMnSc1
•	•	k?
Shelby	Shelba	k1gFnSc2
•	•	k?
Simpson	Simpson	k1gMnSc1
•	•	k?
Spencer	Spencer	k1gMnSc1
•	•	k?
Simpson	Simpson	k1gMnSc1
•	•	k?
Taylor	Taylor	k1gMnSc1
•	•	k?
Todd	Todd	k1gMnSc1
•	•	k?
Trigg	Trigg	k1gMnSc1
•	•	k?
Trimble	Trimble	k1gMnSc1
•	•	k?
Union	union	k1gInSc1
•	•	k?
Warren	Warrna	k1gFnPc2
•	•	k?
<g/>
Washington	Washington	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
•	•	k?
Webster	Webstrum	k1gNnPc2
•	•	k?
Whitley	Whitlea	k1gFnSc2
•	•	k?
Wolfe	Wolf	k1gMnSc5
•	•	k?
Woodford	Woodford	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80088496	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151269263	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80088496	#num#	k4
</s>
