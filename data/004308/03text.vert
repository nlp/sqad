<s>
Státní	státní	k2eAgInSc1d1	státní
symboly	symbol	k1gInPc7	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
stanoveny	stanovit	k5eAaPmNgInP	stanovit
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozměněném	pozměněný	k2eAgNnSc6d1	pozměněné
složení	složení	k1gNnSc6	složení
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
symboly	symbol	k1gInPc4	symbol
nově	nově	k6eAd1	nově
zařazena	zařazen	k2eAgFnSc1d1	zařazena
vlajka	vlajka	k1gFnSc1	vlajka
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
podoba	podoba	k1gFnSc1	podoba
české	český	k2eAgFnSc2d1	Česká
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
československou	československý	k2eAgFnSc7d1	Československá
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
Českou	český	k2eAgFnSc7d1	Česká
národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
stanoveny	stanovit	k5eAaPmNgInP	stanovit
v	v	k7c6	v
článku	článek	k1gInSc6	článek
14	[number]	k4	14
Ústavy	ústava	k1gFnSc2	ústava
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
popis	popis	k1gInSc1	popis
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zákon	zákon	k1gInSc4	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
neměla	mít	k5eNaImAgFnS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
komise	komise	k1gFnSc1	komise
expertů	expert	k1gMnPc2	expert
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
J.	J.	kA	J.
Šebánkem	Šebánek	k1gMnSc7	Šebánek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
stanovit	stanovit	k5eAaPmF	stanovit
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
zavést	zavést	k5eAaPmF	zavést
bíločervenou	bíločervený	k2eAgFnSc4d1	bíločervená
bikolóru	bikolóra	k1gFnSc4	bikolóra
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
znaku	znak	k1gInSc3	znak
Českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
připraveny	připravit	k5eAaPmNgInP	připravit
až	až	k6eAd1	až
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Stranické	stranický	k2eAgInPc1d1	stranický
i	i	k8xC	i
státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
tehdy	tehdy	k6eAd1	tehdy
návrhy	návrh	k1gInPc1	návrh
odložily	odložit	k5eAaPmAgInP	odložit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
nikdy	nikdy	k6eAd1	nikdy
vlastní	vlastní	k2eAgInSc4d1	vlastní
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
znacích	znak	k1gInPc6	znak
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
a	a	k8xC	a
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
předložil	předložit	k5eAaPmAgMnS	předložit
až	až	k6eAd1	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
prezident	prezident	k1gMnSc1	prezident
ČSSR	ČSSR	kA	ČSSR
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
přikázalo	přikázat	k5eAaPmAgNnS	přikázat
návrh	návrh	k1gInSc4	návrh
projednat	projednat	k5eAaPmF	projednat
ve	v	k7c6	v
výborech	výbor	k1gInPc6	výbor
ČNR	ČNR	kA	ČNR
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
komise	komise	k1gFnSc1	komise
předložila	předložit	k5eAaPmAgFnS	předložit
dva	dva	k4xCgInPc4	dva
návrhy	návrh	k1gInPc4	návrh
znaku	znak	k1gInSc2	znak
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgInS	být
J.	J.	kA	J.
Dolejš	Dolejš	k1gMnSc1	Dolejš
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
návrh	návrh	k1gInSc4	návrh
popisoval	popisovat	k5eAaImAgMnS	popisovat
i	i	k9	i
vlajku	vlajka	k1gFnSc4	vlajka
ČR	ČR	kA	ČR
jako	jako	k8xS	jako
bíločervenou	bíločervený	k2eAgFnSc4d1	bíločervená
bikolóru	bikolóra	k1gFnSc4	bikolóra
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
návrzích	návrh	k1gInPc6	návrh
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevovaly	objevovat	k5eAaImAgInP	objevovat
souběžně	souběžně	k6eAd1	souběžně
dva	dva	k4xCgInPc4	dva
znaky	znak	k1gInPc4	znak
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
malý	malý	k2eAgInSc1d1	malý
znak	znak	k1gInSc1	znak
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
hymna	hymna	k1gFnSc1	hymna
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
první	první	k4xOgInSc1	první
částí	část	k1gFnPc2	část
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
československé	československý	k2eAgFnSc2d1	Československá
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
grafik	grafika	k1gFnPc2	grafika
J.	J.	kA	J.
Rathouský	Rathouský	k1gMnSc1	Rathouský
předložil	předložit	k5eAaPmAgMnS	předložit
své	svůj	k3xOyFgInPc4	svůj
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
řešení	řešení	k1gNnSc4	řešení
obou	dva	k4xCgInPc2	dva
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
schválila	schválit	k5eAaPmAgFnS	schválit
ústavní	ústavní	k2eAgInSc4d1	ústavní
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
a	a	k8xC	a
dnem	den	k1gInSc7	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
začaly	začít	k5eAaPmAgInP	začít
státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
podobu	podoba	k1gFnSc4	podoba
znaku	znak	k1gInSc2	znak
namaloval	namalovat	k5eAaPmAgInS	namalovat
Joska	Joska	k1gMnSc1	Joska
Skalník	Skalník	k1gMnSc1	Skalník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
sice	sice	k8xC	sice
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
volebních	volební	k2eAgFnPc2d1	volební
místností	místnost	k1gFnPc2	místnost
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
jako	jako	k8xS	jako
československá	československý	k2eAgFnSc1d1	Československá
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
česká	český	k2eAgFnSc1d1	Česká
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
česká	český	k2eAgFnSc1d1	Česká
dvojbarevná	dvojbarevný	k2eAgFnSc1d1	dvojbarevná
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
a	a	k8xC	a
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
podobná	podobný	k2eAgFnSc1d1	podobná
polské	polský	k2eAgFnPc1d1	polská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
platnosti	platnost	k1gFnSc2	platnost
příliš	příliš	k6eAd1	příliš
nevžila	vžít	k5eNaPmAgFnS	vžít
a	a	k8xC	a
mnohde	mnohde	k6eAd1	mnohde
nebyla	být	k5eNaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
ani	ani	k8xC	ani
v	v	k7c6	v
povinných	povinný	k2eAgInPc6d1	povinný
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Státními	státní	k2eAgInPc7d1	státní
symboly	symbol	k1gInPc7	symbol
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
podoba	podoba	k1gFnSc1	podoba
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
současné	současný	k2eAgFnSc2d1	současná
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
státní	státní	k2eAgFnSc4d1	státní
pečeť	pečeť	k1gFnSc4	pečeť
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
Původní	původní	k2eAgFnSc1d1	původní
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgInPc2d1	široký
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
spodního	spodní	k2eAgNnSc2d1	spodní
červeného	červené	k1gNnSc2	červené
a	a	k8xC	a
vrchního	vrchní	k2eAgMnSc2d1	vrchní
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
při	při	k7c6	při
poměru	poměr	k1gInSc6	poměr
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
stanovil	stanovit	k5eAaPmAgInS	stanovit
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
lišila	lišit	k5eAaImAgFnS	lišit
pouze	pouze	k6eAd1	pouze
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nevžila	vžít	k5eNaPmAgFnS	vžít
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
většinou	většinou	k6eAd1	většinou
ani	ani	k8xC	ani
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
zákonem	zákon	k1gInSc7	zákon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
užívání	užívání	k1gNnSc4	užívání
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
stanovil	stanovit	k5eAaPmAgInS	stanovit
zákon	zákon	k1gInSc1	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
šest	šest	k4xCc1	šest
<g/>
:	:	kIx,	:
velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
státní	státní	k2eAgFnSc2d1	státní
barvy	barva	k1gFnSc2	barva
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
trikolóra	trikolóra	k1gFnSc1	trikolóra
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vlajka	vlajka	k1gFnSc1	vlajka
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
standarta	standarta	k1gFnSc1	standarta
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc1d1	státní
pečeť	pečeť	k1gFnSc1	pečeť
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
státní	státní	k2eAgFnSc1d1	státní
hymna	hymna	k1gFnSc1	hymna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
sloka	sloka	k1gFnSc1	sloka
<g/>
)	)	kIx)	)
Přijetí	přijetí	k1gNnSc1	přijetí
nové	nový	k2eAgFnSc2d1	nová
vlajky	vlajka	k1gFnSc2	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
vlajky	vlajka	k1gFnSc2	vlajka
ČSFR	ČSFR	kA	ČSFR
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dohodami	dohoda	k1gFnPc7	dohoda
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
politickou	politický	k2eAgFnSc7d1	politická
reprezentací	reprezentace	k1gFnSc7	reprezentace
<g/>
,	,	kIx,	,
zakotvenými	zakotvený	k2eAgInPc7d1	zakotvený
i	i	k9	i
v	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
3	[number]	k4	3
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
542	[number]	k4	542
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
ČSFR	ČSFR	kA	ČSFR
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
nesmějí	smát	k5eNaImIp3nP	smát
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
užívat	užívat	k5eAaImF	užívat
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
112	[number]	k4	112
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
na	na	k7c4	na
výslovné	výslovný	k2eAgFnPc4d1	výslovná
výjimky	výjimka	k1gFnPc4	výjimka
se	s	k7c7	s
z	z	k7c2	z
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
stávají	stávat	k5eAaImIp3nP	stávat
běžné	běžný	k2eAgInPc1d1	běžný
zákony	zákon	k1gInPc1	zákon
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
ústavní	ústavní	k2eAgInPc1d1	ústavní
zákony	zákon	k1gInPc1	zákon
platné	platný	k2eAgInPc1d1	platný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ke	k	k7c3	k
dni	den	k1gInSc3	den
účinnosti	účinnost	k1gFnSc2	účinnost
této	tento	k3xDgFnSc2	tento
Ústavy	ústava	k1gFnSc2	ústava
mají	mít	k5eAaImIp3nP	mít
sílu	síla	k1gFnSc4	síla
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
ČR	ČR	kA	ČR
následně	následně	k6eAd1	následně
byly	být	k5eAaImAgInP	být
upraveny	upravit	k5eAaPmNgInP	upravit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kolizních	kolizní	k2eAgNnPc2d1	kolizní
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
principu	princip	k1gInSc6	princip
"	"	kIx"	"
<g/>
lex	lex	k?	lex
posterior	posterior	k1gInSc1	posterior
derogat	derogat	k2eAgInSc1d1	derogat
priori	priori	k6eAd1	priori
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Tradiční	tradiční	k2eAgInPc4d1	tradiční
symboly	symbol	k1gInPc4	symbol
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
zakotveny	zakotvit	k5eAaPmNgInP	zakotvit
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
<g/>
:	:	kIx,	:
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
Národní	národní	k2eAgFnSc1d1	národní
strom	strom	k1gInSc1	strom
-	-	kIx~	-
lípa	lípa	k1gFnSc1	lípa
Kokarda	kokarda	k1gFnSc1	kokarda
heraldický	heraldický	k2eAgInSc4d1	heraldický
symbol	symbol	k1gInSc4	symbol
Kalicha	kalich	k1gInSc2	kalich
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
neoficiálních	oficiální	k2eNgInPc2d1	neoficiální
národních	národní	k2eAgInPc2d1	národní
symbolů	symbol	k1gInPc2	symbol
</s>
