<s>
Šachy	šach	k1gInPc1	šach
nebo	nebo	k8xC	nebo
šach	šach	k1gInSc1	šach
(	(	kIx(	(
<g/>
z	z	k7c2	z
perského	perský	k2eAgMnSc2d1	perský
šáh	šáh	k1gMnSc1	šáh
<g/>
,	,	kIx,	,
panovník	panovník	k1gMnSc1	panovník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
desková	deskový	k2eAgFnSc1d1	desková
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
dva	dva	k4xCgMnPc4	dva
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
soutěžní	soutěžní	k2eAgFnSc6d1	soutěžní
podobě	podoba	k1gFnSc6	podoba
zároveň	zároveň	k6eAd1	zároveň
považovaná	považovaný	k2eAgFnSc1d1	považovaná
i	i	k9	i
za	za	k7c4	za
odvětví	odvětví	k1gNnSc4	odvětví
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
forma	forma	k1gFnSc1	forma
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
úpravou	úprava	k1gFnSc7	úprava
perské	perský	k2eAgFnSc2d1	perská
šachové	šachový	k2eAgFnSc2d1	šachová
hry	hra	k1gFnSc2	hra
šatrandž	šatrandž	k6eAd1	šatrandž
<g/>
,	,	kIx,	,
následníka	následník	k1gMnSc4	následník
staré	starý	k2eAgFnSc2d1	stará
indické	indický	k2eAgFnSc2d1	indická
hry	hra	k1gFnSc2	hra
čaturanga	čaturanga	k1gFnSc1	čaturanga
<g/>
.	.	kIx.	.
</s>
<s>
Šachová	šachový	k2eAgFnSc1d1	šachová
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známá	známý	k2eAgFnSc1d1	známá
už	už	k6eAd1	už
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
šachovnici	šachovnice	k1gFnSc6	šachovnice
<g/>
,	,	kIx,	,
čtvercové	čtvercový	k2eAgFnSc6d1	čtvercová
desce	deska	k1gFnSc6	deska
rozdělené	rozdělený	k2eAgFnSc6d1	rozdělená
na	na	k7c4	na
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
8	[number]	k4	8
polí	pole	k1gNnPc2	pole
střídavě	střídavě	k6eAd1	střídavě
černých	černý	k2eAgMnPc2d1	černý
a	a	k8xC	a
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
hry	hra	k1gFnSc2	hra
celkem	celkem	k6eAd1	celkem
šestnáct	šestnáct	k4xCc4	šestnáct
kamenů	kámen	k1gInPc2	kámen
šesti	šest	k4xCc2	šest
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
dámu	dáma	k1gFnSc4	dáma
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
věžích	věž	k1gFnPc6	věž
<g/>
,	,	kIx,	,
střelcích	střelec	k1gMnPc6	střelec
a	a	k8xC	a
jezdcích	jezdec	k1gMnPc6	jezdec
a	a	k8xC	a
osm	osm	k4xCc1	osm
pěšců	pěšec	k1gMnPc2	pěšec
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
černý	černý	k2eAgMnSc1d1	černý
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
provádějí	provádět	k5eAaImIp3nP	provádět
tahy	tah	k1gInPc1	tah
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přesuny	přesun	k1gInPc1	přesun
kamenů	kámen	k1gInPc2	kámen
po	po	k7c6	po
šachovnici	šachovnice	k1gFnSc6	šachovnice
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
mat	mat	k2eAgNnSc1d1	mat
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc1	takový
napadení	napadení	k1gNnSc1	napadení
soupeřova	soupeřův	k2eAgMnSc2d1	soupeřův
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nelze	lze	k6eNd1	lze
odvrátit	odvrátit	k5eAaPmF	odvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
prvek	prvek	k1gInSc4	prvek
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
partii	partie	k1gFnSc4	partie
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
schopnosti	schopnost	k1gFnPc1	schopnost
a	a	k8xC	a
znalosti	znalost	k1gFnPc1	znalost
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
vznikala	vznikat	k5eAaImAgFnS	vznikat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
šachová	šachový	k2eAgFnSc1d1	šachová
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
charakter	charakter	k1gInSc4	charakter
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Kompoziční	kompoziční	k2eAgInSc1d1	kompoziční
šach	šach	k1gInSc1	šach
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
umělecký	umělecký	k2eAgInSc4d1	umělecký
aspekt	aspekt	k1gInSc4	aspekt
šachů	šach	k1gMnPc2	šach
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
a	a	k8xC	a
hraní	hraní	k1gNnSc1	hraní
šachů	šach	k1gInPc2	šach
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
jako	jako	k9	jako
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zlepšit	zlepšit	k5eAaPmF	zlepšit
kvalitu	kvalita	k1gFnSc4	kvalita
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
rozvinout	rozvinout	k5eAaPmF	rozvinout
příznivé	příznivý	k2eAgFnPc4d1	příznivá
charakterové	charakterový	k2eAgFnPc4d1	charakterová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
her	hra	k1gFnPc2	hra
světa	svět	k1gInSc2	svět
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
je	být	k5eAaImIp3nS	být
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Šachové	šachový	k2eAgInPc1d1	šachový
zápasy	zápas	k1gInPc1	zápas
a	a	k8xC	a
turnaje	turnaj	k1gInPc1	turnaj
se	se	k3xPyFc4	se
organizují	organizovat	k5eAaBmIp3nP	organizovat
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
šachový	šachový	k2eAgInSc1d1	šachový
sportovní	sportovní	k2eAgInSc1d1	sportovní
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
oficiální	oficiální	k2eAgMnSc1d1	oficiální
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Steinitz	Steinitz	k1gMnSc1	Steinitz
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgMnSc1	svůj
titul	titul	k1gInSc4	titul
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
současným	současný	k2eAgMnSc7d1	současný
následovníkem	následovník	k1gMnSc7	následovník
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
norský	norský	k2eAgMnSc1d1	norský
šachový	šachový	k2eAgMnSc1d1	šachový
velmistr	velmistr	k1gMnSc1	velmistr
Magnus	Magnus	k1gMnSc1	Magnus
Carlsen	Carlsno	k1gNnPc2	Carlsno
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
českým	český	k2eAgMnSc7d1	český
šachistou	šachista	k1gMnSc7	šachista
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
David	David	k1gMnSc1	David
Navara	Navara	k1gFnSc1	Navara
<g/>
,	,	kIx,	,
dalším	další	k2eAgNnSc7d1	další
je	on	k3xPp3gNnPc4	on
Viktor	Viktor	k1gMnSc1	Viktor
Láznička	Láznička	k1gMnSc1	Láznička
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
české	český	k2eAgFnPc4d1	Česká
šachistky	šachistka	k1gFnPc4	šachistka
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
Kristýna	Kristýna	k1gFnSc1	Kristýna
Havlíková	Havlíková	k1gFnSc1	Havlíková
a	a	k8xC	a
sestry	sestra	k1gFnSc2	sestra
Karolína	Karolína	k1gFnSc1	Karolína
Olšarová	Olšarová	k1gFnSc1	Olšarová
a	a	k8xC	a
Tereza	Tereza	k1gFnSc1	Tereza
Rodshtein	Rodshteina	k1gFnPc2	Rodshteina
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Olšarová	Olšarová	k1gFnSc1	Olšarová
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Kateřina	Kateřina	k1gFnSc1	Kateřina
Němcová	Němcová	k1gFnSc1	Němcová
již	již	k6eAd1	již
po	po	k7c6	po
emigraci	emigrace	k1gFnSc6	emigrace
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
používány	používán	k2eAgInPc1d1	používán
počítače	počítač	k1gInPc1	počítač
<g/>
;	;	kIx,	;
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
jejich	jejich	k3xOp3gInSc2	jejich
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
vytvoření	vytvoření	k1gNnSc1	vytvoření
šachového	šachový	k2eAgInSc2d1	šachový
programu	program	k1gInSc2	program
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
dobrý	dobrý	k2eAgInSc4d1	dobrý
test	test	k1gInSc4	test
schopnosti	schopnost	k1gFnSc2	schopnost
počítačů	počítač	k1gMnPc2	počítač
napodobit	napodobit	k5eAaPmF	napodobit
lidské	lidský	k2eAgNnSc4d1	lidské
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
nedokonalé	dokonalý	k2eNgInPc1d1	nedokonalý
programy	program	k1gInPc1	program
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zlepšovaly	zlepšovat	k5eAaImAgFnP	zlepšovat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
speciální	speciální	k2eAgInSc1d1	speciální
šachový	šachový	k2eAgInSc1d1	šachový
počítač	počítač	k1gInSc1	počítač
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
mistru	mistr	k1gMnSc3	mistr
světa	svět	k1gInSc2	svět
Garrimu	Garrim	k1gMnSc3	Garrim
Kasparovovi	Kasparova	k1gMnSc3	Kasparova
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
v	v	k7c6	v
roli	role	k1gFnSc6	role
sekundantů	sekundant	k1gMnPc2	sekundant
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
databáze	databáze	k1gFnSc2	databáze
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
pozic	pozice	k1gFnPc2	pozice
umožnily	umožnit	k5eAaPmAgFnP	umožnit
také	také	k9	také
zlepšení	zlepšení	k1gNnSc4	zlepšení
přípravy	příprava	k1gFnSc2	příprava
šachistů	šachista	k1gMnPc2	šachista
<g/>
;	;	kIx,	;
značné	značný	k2eAgFnSc3d1	značná
popularitě	popularita	k1gFnSc3	popularita
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
i	i	k9	i
hraní	hraní	k1gNnSc4	hraní
šachů	šach	k1gInPc2	šach
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pravidla	pravidlo	k1gNnSc2	pravidlo
šachů	šach	k1gMnPc2	šach
<g/>
.	.	kIx.	.
</s>
<s>
Šachovou	šachový	k2eAgFnSc4d1	šachová
soupravu	souprava	k1gFnSc4	souprava
tvoří	tvořit	k5eAaImIp3nP	tvořit
šachovnice	šachovnice	k1gFnPc1	šachovnice
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
sady	sada	k1gFnPc1	sada
kamenů	kámen	k1gInPc2	kámen
–	–	k?	–
světlých	světlý	k2eAgInPc2d1	světlý
(	(	kIx(	(
<g/>
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
"	"	kIx"	"
<g/>
bílé	bílé	k1gNnSc1	bílé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
tmavých	tmavý	k2eAgMnPc2d1	tmavý
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
černé	černý	k2eAgFnPc1d1	černá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šachovnice	šachovnice	k1gFnSc1	šachovnice
je	být	k5eAaImIp3nS	být
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
deska	deska	k1gFnSc1	deska
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
8	[number]	k4	8
polí	pole	k1gNnPc2	pole
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
světlých	světlý	k2eAgFnPc2d1	světlá
a	a	k8xC	a
tmavých	tmavý	k2eAgFnPc2d1	tmavá
(	(	kIx(	(
<g/>
šachovou	šachový	k2eAgFnSc7d1	šachová
terminologií	terminologie	k1gFnSc7	terminologie
"	"	kIx"	"
<g/>
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
černých	černý	k2eAgMnPc2d1	černý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
hráči	hráč	k1gMnPc7	hráč
na	na	k7c6	na
stole	stol	k1gInSc6	stol
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
bílé	bílý	k2eAgNnSc4d1	bílé
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
-li	i	k?	-li
šachovnice	šachovnice	k1gFnSc1	šachovnice
označení	označení	k1gNnSc2	označení
sloupců	sloupec	k1gInPc2	sloupec
(	(	kIx(	(
<g/>
A-H	A-H	k1gFnPc2	A-H
<g/>
)	)	kIx)	)
a	a	k8xC	a
řad	řada	k1gFnPc2	řada
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
používané	používaný	k2eAgFnSc2d1	používaná
při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
partie	partie	k1gFnSc2	partie
(	(	kIx(	(
<g/>
šachové	šachový	k2eAgFnSc6d1	šachová
notaci	notace	k1gFnSc6	notace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgNnSc1d1	základní
(	(	kIx(	(
<g/>
výchozí	výchozí	k2eAgNnSc4d1	výchozí
<g/>
)	)	kIx)	)
postavení	postavení	k1gNnSc4	postavení
bílých	bílý	k2eAgFnPc2d1	bílá
figur	figura	k1gFnPc2	figura
na	na	k7c4	na
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
řadě	řada	k1gFnSc3	řada
<g/>
,	,	kIx,	,
černých	černý	k2eAgFnPc2d1	černá
pak	pak	k6eAd1	pak
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
šachové	šachový	k2eAgFnSc2d1	šachová
partie	partie	k1gFnSc2	partie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nS	určit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
bílými	bílý	k2eAgInPc7d1	bílý
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
soupeř	soupeř	k1gMnSc1	soupeř
jako	jako	k8xS	jako
černý	černý	k2eAgMnSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Kameny	kámen	k1gInPc1	kámen
se	se	k3xPyFc4	se
postaví	postavit	k5eAaPmIp3nP	postavit
do	do	k7c2	do
výchozího	výchozí	k2eAgInSc2d1	výchozí
(	(	kIx(	(
<g/>
základního	základní	k2eAgNnSc2d1	základní
<g/>
)	)	kIx)	)
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
partii	partie	k1gFnSc4	partie
zahájí	zahájit	k5eAaPmIp3nS	zahájit
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
v	v	k7c6	v
tazích	tag	k1gInPc6	tag
pravidelně	pravidelně	k6eAd1	pravidelně
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
tahu	tah	k1gInSc2	tah
nemůže	moct	k5eNaImIp3nS	moct
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Tah	tah	k1gInSc1	tah
každého	každý	k3xTgMnSc4	každý
hráče	hráč	k1gMnSc4	hráč
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
přesunutí	přesunutí	k1gNnSc2	přesunutí
jednoho	jeden	k4xCgMnSc2	jeden
kamene	kámen	k1gInSc2	kámen
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
rošáda	rošáda	k1gFnSc1	rošáda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
přesune	přesunout	k5eAaPmIp3nS	přesunout
král	král	k1gMnSc1	král
i	i	k8xC	i
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žádným	žádný	k3yNgInSc7	žádný
tahem	tah	k1gInSc7	tah
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
kámen	kámen	k1gInSc1	kámen
přesunout	přesunout	k5eAaPmF	přesunout
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgInSc1d1	jiný
kámen	kámen	k1gInSc1	kámen
stejné	stejný	k2eAgFnSc2d1	stejná
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tah	tah	k1gInSc1	tah
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
s	s	k7c7	s
kamenem	kámen	k1gInSc7	kámen
soupeře	soupeř	k1gMnSc2	soupeř
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
braní	braní	k1gNnSc1	braní
<g/>
;	;	kIx,	;
soupeřův	soupeřův	k2eAgInSc1d1	soupeřův
kámen	kámen	k1gInSc1	kámen
je	být	k5eAaImIp3nS	být
takovým	takový	k3xDgInSc7	takový
tahem	tah	k1gInSc7	tah
odstraněn	odstranit	k5eAaPmNgInS	odstranit
ze	z	k7c2	z
šachovnice	šachovnice	k1gFnSc2	šachovnice
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
druh	druh	k1gInSc1	druh
kamenů	kámen	k1gInPc2	kámen
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgInPc4	všechen
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jezdce	jezdec	k1gMnSc2	jezdec
se	se	k3xPyFc4	se
posouvají	posouvat	k5eAaImIp3nP	posouvat
přímou	přímý	k2eAgFnSc7d1	přímá
čarou	čára	k1gFnSc7	čára
(	(	kIx(	(
<g/>
po	po	k7c6	po
řadách	řada	k1gFnPc6	řada
<g/>
,	,	kIx,	,
sloupcích	sloupec	k1gInPc6	sloupec
nebo	nebo	k8xC	nebo
diagonálách	diagonála	k1gFnPc6	diagonála
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmějí	smát	k5eNaImIp3nP	smát
žádný	žádný	k3yNgInSc4	žádný
jiný	jiný	k2eAgInSc4d1	jiný
kámen	kámen	k1gInSc4	kámen
"	"	kIx"	"
<g/>
přeskočit	přeskočit	k5eAaPmF	přeskočit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
pole	pole	k1gNnSc4	pole
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
i	i	k9	i
po	po	k7c6	po
diagonále	diagonála	k1gFnSc6	diagonála
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
způsobem	způsob	k1gInSc7	způsob
tahu	tah	k1gInSc2	tah
krále	král	k1gMnPc4	král
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
rošáda	rošáda	k1gFnSc1	rošáda
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
ještě	ještě	k6eAd1	ještě
nepohnuli	pohnout	k5eNaPmAgMnP	pohnout
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
přemístí	přemístit	k5eAaPmIp3nS	přemístit
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
věži	věž	k1gFnSc3	věž
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
přes	přes	k7c4	přes
krále	král	k1gMnPc4	král
na	na	k7c4	na
pole	pole	k1gFnPc4	pole
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
král	král	k1gMnSc1	král
právě	právě	k6eAd1	právě
přešel	přejít	k5eAaPmAgMnS	přejít
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
mezilehlá	mezilehlý	k2eAgNnPc1d1	mezilehlé
pole	pole	k1gNnPc1	pole
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
volná	volný	k2eAgFnSc1d1	volná
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
nesmí	smět	k5eNaImIp3nS	smět
stát	stát	k5eAaImF	stát
před	před	k7c7	před
rošádou	rošáda	k1gFnSc7	rošáda
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
přejít	přejít	k5eAaPmF	přejít
přes	přes	k7c4	přes
pole	pole	k1gNnSc4	pole
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Žádným	žádný	k3yNgInSc7	žádný
svým	svůj	k3xOyFgInSc7	svůj
tahem	tah	k1gInSc7	tah
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
nesmí	smět	k5eNaImIp3nS	smět
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
ohrožené	ohrožený	k2eAgNnSc4d1	ohrožené
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
tahu	tah	k1gInSc6	tah
mohl	moct	k5eAaImAgInS	moct
přesunout	přesunout	k5eAaPmF	přesunout
soupeřův	soupeřův	k2eAgInSc1d1	soupeřův
kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
krále	král	k1gMnSc4	král
brát	brát	k5eAaImF	brát
<g/>
.	.	kIx.	.
</s>
<s>
Dáma	dáma	k1gFnSc1	dáma
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
,	,	kIx,	,
řadách	řada	k1gFnPc6	řada
nebo	nebo	k8xC	nebo
diagonálách	diagonála	k1gFnPc6	diagonála
o	o	k7c4	o
libovolný	libovolný	k2eAgInSc4d1	libovolný
počet	počet	k1gInSc4	počet
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
řadách	řada	k1gFnPc6	řada
a	a	k8xC	a
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Střelec	Střelec	k1gMnSc1	Střelec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
diagonálách	diagonála	k1gFnPc6	diagonála
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ty	ten	k3xDgMnPc4	ten
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
šachovnici	šachovnice	k1gFnSc6	šachovnice
stejnou	stejný	k2eAgFnSc4d1	stejná
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
střelec	střelec	k1gMnSc1	střelec
nikdy	nikdy	k6eAd1	nikdy
nezmění	změnit	k5eNaPmIp3nS	změnit
barvu	barva	k1gFnSc4	barva
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
stojí	stát	k5eAaImIp3nS	stát
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
střelci	střelec	k1gMnSc6	střelec
bělopolném	bělopolný	k2eAgMnSc6d1	bělopolný
nebo	nebo	k8xC	nebo
černopolném	černopolný	k2eAgMnSc6d1	černopolný
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gInSc1	jezdec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
skoky	skok	k1gInPc7	skok
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
L	L	kA	L
(	(	kIx(	(
<g/>
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
rovně	rovně	k6eAd1	rovně
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jedno	jeden	k4xCgNnSc4	jeden
rovně	roveň	k1gFnPc4	roveň
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
stranou	strana	k1gFnSc7	strana
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
<g/>
-li	i	k?	-li
na	na	k7c6	na
mezilehlých	mezilehlý	k2eAgNnPc6d1	mezilehlé
polích	pole	k1gNnPc6	pole
nějaké	nějaký	k3yIgInPc4	nějaký
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gMnSc1	jezdec
tak	tak	k6eAd1	tak
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
skoku	skok	k1gInSc6	skok
změní	změnit	k5eAaPmIp3nS	změnit
barvu	barva	k1gFnSc4	barva
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
stojí	stát	k5eAaImIp3nS	stát
<g/>
:	:	kIx,	:
z	z	k7c2	z
bílého	bílý	k2eAgNnSc2d1	bílé
pole	pole	k1gNnSc2	pole
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
černé	černý	k2eAgFnPc4d1	černá
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Pěšec	pěšec	k1gMnSc1	pěšec
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
posunout	posunout	k5eAaPmF	posunout
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
pole	pole	k1gNnSc4	pole
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
neobsazené	obsazený	k2eNgNnSc1d1	neobsazené
(	(	kIx(	(
<g/>
ze	z	k7c2	z
základního	základní	k2eAgNnSc2d1	základní
postavení	postavení	k1gNnSc2	postavení
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
posunout	posunout	k5eAaPmF	posunout
i	i	k9	i
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgNnPc1	dva
prázdná	prázdné	k1gNnPc1	prázdné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
brát	brát	k5eAaImF	brát
soupeřův	soupeřův	k2eAgInSc4d1	soupeřův
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
úhlopříčně	úhlopříčně	k6eAd1	úhlopříčně
sousedícím	sousedící	k2eAgNnSc6d1	sousedící
poli	pole	k1gNnSc6	pole
před	před	k7c7	před
pěšcem	pěšec	k1gMnSc7	pěšec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pěšec	pěšec	k1gMnSc1	pěšec
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
soupeřův	soupeřův	k2eAgMnSc1d1	soupeřův
pěšec	pěšec	k1gMnSc1	pěšec
přeskočil	přeskočit	k5eAaPmAgMnS	přeskočit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
úvodní	úvodní	k2eAgFnSc2d1	úvodní
pozice	pozice	k1gFnSc2	pozice
postoupil	postoupit	k5eAaPmAgMnS	postoupit
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ho	on	k3xPp3gNnSc4	on
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgMnSc1	tento
pěšec	pěšec	k1gMnSc1	pěšec
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
soupeř	soupeř	k1gMnSc1	soupeř
postoupil	postoupit	k5eAaPmAgMnS	postoupit
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tah	tah	k1gInSc1	tah
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
braní	braní	k1gNnSc3	braní
mimochodem	mimochodem	k6eAd1	mimochodem
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
en	en	k?	en
passant	passant	k1gInSc1	passant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
jen	jen	k9	jen
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
soupeř	soupeř	k1gMnSc1	soupeř
svým	svůj	k3xOyFgMnSc7	svůj
pěšcem	pěšec	k1gMnSc7	pěšec
takto	takto	k6eAd1	takto
táhl	táhnout	k5eAaImAgMnS	táhnout
<g/>
.	.	kIx.	.
</s>
<s>
Pěšec	pěšec	k1gMnSc1	pěšec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
postoupil	postoupit	k5eAaPmAgMnS	postoupit
na	na	k7c4	na
poslední	poslední	k2eAgNnPc4d1	poslední
pole	pole	k1gNnPc4	pole
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
osmou	osma	k1gFnSc7	osma
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
první	první	k4xOgFnSc4	první
řadu	řada	k1gFnSc4	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
tahu	tah	k1gInSc6	tah
odstraněn	odstranit	k5eAaPmNgInS	odstranit
z	z	k7c2	z
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
poli	pole	k1gNnSc6	pole
dámou	dáma	k1gFnSc7	dáma
<g/>
,	,	kIx,	,
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
střelcem	střelec	k1gMnSc7	střelec
nebo	nebo	k8xC	nebo
jezdcem	jezdec	k1gMnSc7	jezdec
podle	podle	k7c2	podle
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
volby	volba	k1gFnSc2	volba
hráče	hráč	k1gMnSc2	hráč
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
proměna	proměna	k1gFnSc1	proměna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působnost	působnost	k1gFnSc1	působnost
proměněného	proměněný	k2eAgInSc2d1	proměněný
kamene	kámen	k1gInSc2	kámen
je	být	k5eAaImIp3nS	být
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
dát	dát	k5eAaPmF	dát
šach	šach	k1gInSc4	šach
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
matování	matování	k1gNnSc4	matování
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
proměně	proměna	k1gFnSc3	proměna
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hráč	hráč	k1gMnSc1	hráč
dvě	dva	k4xCgNnPc1	dva
i	i	k8xC	i
více	hodně	k6eAd2	hodně
dam	dáma	k1gFnPc2	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Králové	Král	k1gMnPc1	Král
<g/>
,	,	kIx,	,
dámy	dáma	k1gFnPc1	dáma
<g/>
,	,	kIx,	,
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
střelci	střelec	k1gMnPc1	střelec
a	a	k8xC	a
jezdci	jezdec	k1gMnPc1	jezdec
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
slovem	slovo	k1gNnSc7	slovo
figury	figura	k1gFnSc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
těžké	těžký	k2eAgFnSc2d1	těžká
figury	figura	k1gFnSc2	figura
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
dámy	dáma	k1gFnPc1	dáma
a	a	k8xC	a
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jezdci	jezdec	k1gMnPc1	jezdec
a	a	k8xC	a
střelci	střelec	k1gMnPc1	střelec
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
lehké	lehký	k2eAgFnPc1d1	lehká
figury	figura	k1gFnPc1	figura
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hráč	hráč	k1gMnSc1	hráč
nějakým	nějaký	k3yIgInSc7	nějaký
tahem	tah	k1gInSc7	tah
napadne	napadnout	k5eAaPmIp3nS	napadnout
soupeřova	soupeřův	k2eAgMnSc4d1	soupeřův
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
táhne	táhnout	k5eAaImIp3nS	táhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
příštím	příští	k2eAgInSc7d1	příští
tahem	tah	k1gInSc7	tah
mohl	moct	k5eAaImAgInS	moct
krále	král	k1gMnSc4	král
brát	brát	k5eAaImF	brát
<g/>
,	,	kIx,	,
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
soupeřovi	soupeř	k1gMnSc3	soupeř
dal	dát	k5eAaPmAgMnS	dát
šach	šach	k1gInSc4	šach
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
taková	takový	k3xDgFnSc1	takový
situace	situace	k1gFnSc1	situace
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
soupeř	soupeř	k1gMnSc1	soupeř
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
hrát	hrát	k5eAaImF	hrát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tuto	tento	k3xDgFnSc4	tento
hrozbu	hrozba	k1gFnSc4	hrozba
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
takový	takový	k3xDgInSc4	takový
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nP	by
hrozbu	hrozba	k1gFnSc4	hrozba
braní	braní	k1gNnSc2	braní
krále	král	k1gMnSc2	král
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
mat	mat	k1gInSc1	mat
<g/>
,	,	kIx,	,
konec	konec	k1gInSc1	konec
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
vítězství	vítězství	k1gNnSc4	vítězství
hráče	hráč	k1gMnSc2	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
takto	takto	k6eAd1	takto
soupeřova	soupeřův	k2eAgMnSc4d1	soupeřův
krále	král	k1gMnSc4	král
napadl	napadnout	k5eAaPmAgMnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
končí	končit	k5eAaImIp3nS	končit
vítězstvím	vítězství	k1gNnSc7	vítězství
také	také	k6eAd1	také
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
druhý	druhý	k4xOgMnSc1	druhý
hráč	hráč	k1gMnSc1	hráč
vzdá	vzdát	k5eAaPmIp3nS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
výsledek	výsledek	k1gInSc1	výsledek
<g/>
,	,	kIx,	,
remíza	remíza	k1gFnSc1	remíza
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
dohodou	dohoda	k1gFnSc7	dohoda
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
remízu	remíza	k1gFnSc4	remíza
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
ji	on	k3xPp3gFnSc4	on
přijme	přijmout	k5eAaPmIp3nS	přijmout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
patu	pat	k1gInSc2	pat
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
žádný	žádný	k3yNgInSc1	žádný
dovolený	dovolený	k2eAgInSc1d1	dovolený
tah	tah	k1gInSc1	tah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trojím	trojit	k5eAaImIp1nS	trojit
opakováním	opakování	k1gNnSc7	opakování
stejné	stejný	k2eAgFnSc2d1	stejná
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
redukcí	redukce	k1gFnSc7	redukce
počtu	počet	k1gInSc2	počet
kamenů	kámen	k1gInPc2	kámen
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbylými	zbylý	k2eAgInPc7d1	zbylý
kameny	kámen	k1gInPc7	kámen
už	už	k6eAd1	už
nelze	lze	k6eNd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
matu	mást	k5eAaImIp1nS	mást
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
jestliže	jestliže	k8xS	jestliže
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
provedli	provést	k5eAaPmAgMnP	provést
50	[number]	k4	50
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujících	následující	k2eAgInPc2d1	následující
tahů	tah	k1gInPc2	tah
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
přitom	přitom	k6eAd1	přitom
sebrali	sebrat	k5eAaPmAgMnP	sebrat
kámen	kámen	k1gInSc4	kámen
soupeře	soupeř	k1gMnSc2	soupeř
nebo	nebo	k8xC	nebo
táhli	táhnout	k5eAaImAgMnP	táhnout
pěšcem	pěšec	k1gMnSc7	pěšec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěžním	soutěžní	k2eAgInSc6d1	soutěžní
šachu	šach	k1gInSc6	šach
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
limitována	limitovat	k5eAaBmNgFnS	limitovat
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jenž	k3xRgNnSc4	jenž
měření	měření	k1gNnSc4	měření
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
šachové	šachový	k2eAgFnSc2d1	šachová
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
každému	každý	k3xTgMnSc3	každý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
měří	měřit	k5eAaImIp3nS	měřit
čas	čas	k1gInSc4	čas
samostatně	samostatně	k6eAd1	samostatně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tahu	tah	k1gInSc6	tah
a	a	k8xC	a
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
překročil	překročit	k5eAaPmAgMnS	překročit
stanovený	stanovený	k2eAgInSc4d1	stanovený
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
partii	partie	k1gFnSc4	partie
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
soupeř	soupeř	k1gMnSc1	soupeř
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k9	ještě
čas	čas	k1gInSc4	čas
nepřekročil	překročit	k5eNaPmAgMnS	překročit
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
šachovnici	šachovnice	k1gFnSc6	šachovnice
dostatek	dostatek	k1gInSc1	dostatek
sil	síla	k1gFnPc2	síla
umožňujících	umožňující	k2eAgFnPc2d1	umožňující
dát	dát	k5eAaPmF	dát
soupeři	soupeř	k1gMnPc7	soupeř
mat	mat	k6eAd1	mat
při	při	k7c6	při
nejhorší	zlý	k2eAgFnSc6d3	nejhorší
možné	možný	k2eAgFnSc6d1	možná
soupeřově	soupeřův	k2eAgFnSc6d1	soupeřova
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Časy	čas	k1gInPc1	čas
na	na	k7c4	na
soutěžní	soutěžní	k2eAgFnSc4d1	soutěžní
partii	partie	k1gFnSc4	partie
obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
i	i	k9	i
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
rapid	rapid	k1gInSc1	rapid
šach	šach	k1gInSc1	šach
(	(	kIx(	(
<g/>
10	[number]	k4	10
až	až	k9	až
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
na	na	k7c4	na
partii	partie	k1gFnSc4	partie
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
<g/>
)	)	kIx)	)
a	a	k8xC	a
bleskový	bleskový	k2eAgInSc1d1	bleskový
šach	šach	k1gInSc1	šach
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
na	na	k7c4	na
partii	partie	k1gFnSc4	partie
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
2014	[number]	k4	2014
se	se	k3xPyFc4	se
rapid	rapid	k1gInSc1	rapid
šach	šach	k1gInSc1	šach
hrál	hrát	k5eAaImAgInS	hrát
15	[number]	k4	15
až	až	k9	až
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
bleskový	bleskový	k2eAgInSc1d1	bleskový
šach	šach	k1gInSc1	šach
méně	málo	k6eAd2	málo
než	než	k8xS	než
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
korespondenční	korespondenční	k2eAgInSc1d1	korespondenční
šach	šach	k1gInSc1	šach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
telegrafu	telegraf	k1gInSc2	telegraf
nebo	nebo	k8xC	nebo
e-mailu	eail	k1gInSc2	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Hráčům	hráč	k1gMnPc3	hráč
se	se	k3xPyFc4	se
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
na	na	k7c4	na
tah	tah	k1gInSc4	tah
čas	čas	k1gInSc4	čas
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc4	počítač
i	i	k8xC	i
rady	rada	k1gFnPc4	rada
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Kompoziční	kompoziční	k2eAgInSc1d1	kompoziční
šach	šach	k1gInSc1	šach
(	(	kIx(	(
<g/>
též	též	k9	též
umělecký	umělecký	k2eAgInSc4d1	umělecký
šach	šach	k1gInSc4	šach
<g/>
,	,	kIx,	,
problémový	problémový	k2eAgInSc4d1	problémový
šach	šach	k1gInSc4	šach
či	či	k8xC	či
skladebný	skladebný	k2eAgInSc4d1	skladebný
šach	šach	k1gInSc4	šach
<g/>
)	)	kIx)	)
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
šachových	šachový	k2eAgFnPc2d1	šachová
kompozic	kompozice	k1gFnPc2	kompozice
(	(	kIx(	(
<g/>
též	též	k6eAd1	též
šachových	šachový	k2eAgInPc2d1	šachový
problémů	problém	k1gInPc2	problém
či	či	k8xC	či
skladeb	skladba	k1gFnPc2	skladba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
uměle	uměle	k6eAd1	uměle
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
určité	určitý	k2eAgNnSc4d1	určité
zadání	zadání	k1gNnSc4	zadání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
bílý	bílý	k1gMnSc1	bílý
táhne	táhnout	k5eAaImIp3nS	táhnout
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
mat	mat	k6eAd1	mat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
tazích	tag	k1gInPc6	tag
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skládání	skládání	k1gNnSc1	skládání
šachových	šachový	k2eAgInPc2d1	šachový
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
řadou	řada	k1gFnSc7	řada
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
úlohách	úloha	k1gFnPc6	úloha
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
atypické	atypický	k2eAgFnPc1d1	atypická
figury	figura	k1gFnPc1	figura
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nestandardní	standardní	k2eNgNnPc4d1	nestandardní
zadání	zadání	k1gNnPc4	zadání
nebo	nebo	k8xC	nebo
nestandardní	standardní	k2eNgNnPc4d1	nestandardní
doplňková	doplňkový	k2eAgNnPc4d1	doplňkové
pravidla	pravidlo	k1gNnPc4	pravidlo
apod.	apod.	kA	apod.
Takové	takový	k3xDgFnPc1	takový
úlohy	úloha	k1gFnPc1	úloha
jsou	být	k5eAaImIp3nP	být
souhrnně	souhrnně	k6eAd1	souhrnně
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
exošach	exošach	k1gInSc1	exošach
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
"	"	kIx"	"
<g/>
pohádkový	pohádkový	k2eAgInSc4d1	pohádkový
šach	šach	k1gInSc4	šach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kompoziční	kompoziční	k2eAgInSc1d1	kompoziční
šach	šach	k1gInSc1	šach
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
sportovním	sportovní	k2eAgNnPc3d1	sportovní
odvětvím	odvětví	k1gNnPc3	odvětví
<g/>
,	,	kIx,	,
soutěžit	soutěžit	k5eAaImF	soutěžit
lze	lze	k6eAd1	lze
jak	jak	k8xS	jak
ve	v	k7c6	v
skládání	skládání	k1gNnSc6	skládání
úloh	úloha	k1gFnPc2	úloha
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
řešení	řešení	k1gNnSc6	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnPc4d1	oficiální
pravidla	pravidlo	k1gNnPc4	pravidlo
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
šachová	šachový	k2eAgFnSc1d1	šachová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIDE	FIDE	kA	FIDE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgInPc2	tento
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
šachů	šach	k1gInPc2	šach
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
šachu	šach	k1gInSc2	šach
podobných	podobný	k2eAgFnPc2d1	podobná
her	hra	k1gFnPc2	hra
s	s	k7c7	s
odlišnými	odlišný	k2eAgNnPc7d1	odlišné
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
šachové	šachový	k2eAgFnSc2d1	šachová
varianty	varianta	k1gFnSc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
šachové	šachový	k2eAgFnSc2d1	šachová
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
šachů	šach	k1gMnPc2	šach
se	se	k3xPyFc4	se
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
několik	několik	k4yIc1	několik
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
prapůvod	prapůvod	k1gInSc1	prapůvod
šachové	šachový	k2eAgFnSc2d1	šachová
hry	hra	k1gFnSc2	hra
nejčastěji	často	k6eAd3	často
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
Guptovské	Guptovský	k2eAgFnSc2d1	Guptovská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
arabské	arabský	k2eAgNnSc4d1	arabské
<g/>
,	,	kIx,	,
perské	perský	k2eAgNnSc4d1	perské
i	i	k8xC	i
turecké	turecký	k2eAgNnSc4d1	turecké
označení	označení	k1gNnSc4	označení
šachů	šach	k1gMnPc2	šach
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
sanskrtského	sanskrtský	k2eAgInSc2d1	sanskrtský
čaturanga	čaturanga	k1gFnSc1	čaturanga
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
součásti	součást	k1gFnPc1	součást
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
a	a	k8xC	a
válečné	válečný	k2eAgInPc4d1	válečný
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
předobrazy	předobraz	k1gInPc4	předobraz
dnešních	dnešní	k2eAgMnPc2d1	dnešní
šachových	šachový	k2eAgMnPc2d1	šachový
pěšců	pěšec	k1gMnPc2	pěšec
<g/>
,	,	kIx,	,
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
střelců	střelec	k1gMnPc2	střelec
a	a	k8xC	a
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
modifikována	modifikovat	k5eAaBmNgFnS	modifikovat
a	a	k8xC	a
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
šatrandž	šatrandž	k1gFnSc4	šatrandž
<g/>
,	,	kIx,	,
nalézáme	nalézat	k5eAaImIp1nP	nalézat
první	první	k4xOgFnSc4	první
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
n.	n.	k?	n.
l.	l.	k?	l.
Za	za	k7c2	za
nejstarší	starý	k2eAgFnSc2d3	nejstarší
doložené	doložený	k2eAgFnSc2d1	doložená
šachové	šachový	k2eAgFnSc2d1	šachová
figurky	figurka	k1gFnSc2	figurka
byly	být	k5eAaImAgFnP	být
pokládané	pokládaný	k2eAgFnPc1d1	pokládaná
perské	perský	k2eAgFnPc1d1	perská
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
objevena	objeven	k2eAgFnSc1d1	objevena
šachová	šachový	k2eAgFnSc1d1	šachová
figurka	figurka	k1gFnSc1	figurka
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
už	už	k6eAd1	už
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
800	[number]	k4	800
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
s	s	k7c7	s
pozměněnými	pozměněný	k2eAgNnPc7d1	pozměněné
pravidly	pravidlo	k1gNnPc7	pravidlo
hrána	hrát	k5eAaImNgFnS	hrát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
siang-čchi	siang-čchi	k1gNnSc2	siang-čchi
(	(	kIx(	(
<g/>
též	též	k9	též
"	"	kIx"	"
<g/>
čínské	čínský	k2eAgInPc1d1	čínský
šachy	šach	k1gInPc1	šach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Persie	Persie	k1gFnSc1	Persie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmého	sedmý	k4xOgInSc2	sedmý
století	století	k1gNnSc2	století
dobyta	dobýt	k5eAaPmNgFnS	dobýt
muslimy	muslim	k1gMnPc7	muslim
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
se	se	k3xPyFc4	se
šatrandž	šatrandž	k6eAd1	šatrandž
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
muslimského	muslimský	k2eAgInSc2d1	muslimský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
počínaje	počínaje	k7c7	počínaje
devátým	devátý	k4xOgNnSc7	devátý
stoletím	století	k1gNnSc7	století
hra	hra	k1gFnSc1	hra
začala	začít	k5eAaPmAgFnS	začít
dostávat	dostávat	k5eAaImF	dostávat
jak	jak	k6eAd1	jak
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
přes	přes	k7c4	přes
muslimské	muslimský	k2eAgFnPc4d1	muslimská
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
východu	východ	k1gInSc2	východ
přes	přes	k7c4	přes
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
evropský	evropský	k2eAgInSc1d1	evropský
šachový	šachový	k2eAgInSc1d1	šachový
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
latinská	latinský	k2eAgFnSc1d1	Latinská
báseň	báseň	k1gFnSc1	báseň
Versus	versus	k7c1	versus
de	de	k?	de
scachis	scachis	k1gFnPc4	scachis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
997	[number]	k4	997
(	(	kIx(	(
<g/>
šatrandži	šatrandzat	k5eAaPmIp1nS	šatrandzat
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
říkalo	říkat	k5eAaImAgNnS	říkat
také	také	k6eAd1	také
šachy	šach	k1gInPc4	šach
–	–	k?	–
pozdější	pozdní	k2eAgFnSc1d2	pozdější
změna	změna	k1gFnSc1	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
na	na	k7c4	na
moderní	moderní	k2eAgInPc4d1	moderní
šachy	šach	k1gInPc4	šach
neznamenala	znamenat	k5eNaImAgFnS	znamenat
změnu	změna	k1gFnSc4	změna
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dnešní	dnešní	k2eAgFnSc1d1	dnešní
šachovnice	šachovnice	k1gFnSc1	šachovnice
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zvyk	zvyk	k1gInSc1	zvyk
zabarvovat	zabarvovat	k5eAaImF	zabarvovat
pole	pole	k1gNnSc4	pole
hrací	hrací	k2eAgFnSc2d1	hrací
desky	deska	k1gFnSc2	deska
střídavě	střídavě	k6eAd1	střídavě
tmavou	tmavý	k2eAgFnSc7d1	tmavá
a	a	k8xC	a
světlou	světlý	k2eAgFnSc7d1	světlá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
hru	hra	k1gFnSc4	hra
přinesli	přinést	k5eAaPmAgMnP	přinést
Maurové	Maurové	k?	Maurové
v	v	k7c6	v
desátém	desátý	k4xOgInSc6	desátý
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Alfonse	Alfons	k1gMnSc2	Alfons
X.	X.	kA	X.
Moudrého	moudrý	k2eAgMnSc2d1	moudrý
slavný	slavný	k2eAgInSc4d1	slavný
rukopis	rukopis	k1gInSc4	rukopis
Libro	libra	k1gFnSc5	libra
de	de	k?	de
los	los	k1gInSc1	los
juegos	juegos	k1gInSc1	juegos
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedle	vedle	k7c2	vedle
vrhcábů	vrhcáby	k1gInPc2	vrhcáby
a	a	k8xC	a
kostek	kostka	k1gFnPc2	kostka
popisuje	popisovat	k5eAaImIp3nS	popisovat
i	i	k9	i
šatrandž	šatrandž	k6eAd1	šatrandž
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
klade	klást	k5eAaImIp3nS	klást
vznik	vznik	k1gInSc4	vznik
šachu	šach	k1gInSc2	šach
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgInSc1d1	západní
šach	šach	k1gInSc1	šach
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
ze	z	k7c2	z
siang-čchi	siang-čchi	k6eAd1	siang-čchi
nebo	nebo	k8xC	nebo
nějakého	nějaký	k3yIgMnSc4	nějaký
předchůdce	předchůdce	k1gMnSc1	předchůdce
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kameny	kámen	k1gInPc1	kámen
v	v	k7c6	v
šatrandži	šatrandž	k1gFnSc6	šatrandž
měly	mít	k5eAaImAgFnP	mít
mnohem	mnohem	k6eAd1	mnohem
omezenější	omezený	k2eAgFnPc1d2	omezenější
možnosti	možnost	k1gFnPc1	možnost
pohybu	pohyb	k1gInSc2	pohyb
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
jejich	jejich	k3xOp3gMnPc1	jejich
moderní	moderní	k2eAgMnPc1d1	moderní
nástupci	nástupce	k1gMnPc1	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Alfil	Alfil	k1gMnSc1	Alfil
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
střelce	střelec	k1gMnSc2	střelec
<g/>
,	,	kIx,	,
skákal	skákat	k5eAaImAgMnS	skákat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
pole	pole	k1gNnPc4	pole
po	po	k7c6	po
diagonále	diagonál	k1gInSc6	diagonál
a	a	k8xC	a
předchůdce	předchůdce	k1gMnSc1	předchůdce
dámy	dáma	k1gFnSc2	dáma
vezír	vezír	k1gMnSc1	vezír
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
pole	pole	k1gNnSc4	pole
diagonálně	diagonálně	k6eAd1	diagonálně
<g/>
.	.	kIx.	.
</s>
<s>
Pěšci	pěšec	k1gMnPc1	pěšec
neměli	mít	k5eNaImAgMnP	mít
právo	právo	k1gNnSc4	právo
dvojkroku	dvojkrok	k1gInSc2	dvojkrok
z	z	k7c2	z
výchozího	výchozí	k2eAgNnSc2d1	výchozí
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
směli	smět	k5eAaImAgMnP	smět
proměnit	proměnit	k5eAaPmF	proměnit
pouze	pouze	k6eAd1	pouze
ve	v	k7c4	v
vezíry	vezír	k1gMnPc4	vezír
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
velice	velice	k6eAd1	velice
slabé	slabý	k2eAgFnPc1d1	slabá
figury	figura	k1gFnPc1	figura
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
odlišnosti	odlišnost	k1gFnPc1	odlišnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
neexistovala	existovat	k5eNaImAgFnS	existovat
rošáda	rošáda	k1gFnSc1	rošáda
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
začala	začít	k5eAaPmAgNnP	začít
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
až	až	k9	až
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1475	[number]	k4	1475
po	po	k7c6	po
několika	několik	k4yIc6	několik
velkých	velký	k2eAgFnPc6d1	velká
změnách	změna	k1gFnPc6	změna
hra	hra	k1gFnSc1	hra
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
tu	tu	k6eAd1	tu
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yQgFnSc6	jaký
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgNnPc4d1	známé
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnější	nápadní	k2eAgFnSc7d3	nápadní
změnou	změna	k1gFnSc7	změna
byla	být	k5eAaImAgFnS	být
podstatně	podstatně	k6eAd1	podstatně
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
bojová	bojový	k2eAgFnSc1d1	bojová
schopnost	schopnost	k1gFnSc1	schopnost
dámy	dáma	k1gFnSc2	dáma
<g/>
,	,	kIx,	,
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
našemu	náš	k3xOp1gInSc3	náš
šachu	šach	k1gInSc3	šach
říkalo	říkat	k5eAaImAgNnS	říkat
na	na	k7c6	na
odlišení	odlišení	k1gNnSc6	odlišení
od	od	k7c2	od
šatrandže	šatrandž	k1gFnSc2	šatrandž
"	"	kIx"	"
<g/>
královniny	královnin	k2eAgInPc1d1	královnin
šachy	šach	k1gInPc1	šach
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
šach	šach	k1gInSc1	šach
šílené	šílený	k2eAgFnSc2d1	šílená
královny	královna	k1gFnSc2	královna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
už	už	k6eAd1	už
shodovala	shodovat	k5eAaImAgFnS	shodovat
s	s	k7c7	s
dnešními	dnešní	k2eAgNnPc7d1	dnešní
pravidly	pravidlo	k1gNnPc7	pravidlo
vyjma	vyjma	k7c2	vyjma
některých	některý	k3yIgNnPc2	některý
ustanovení	ustanovení	k1gNnPc2	ustanovení
o	o	k7c6	o
konci	konec	k1gInSc6	konec
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
patu	pat	k1gInSc2	pat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
lišila	lišit	k5eAaImAgFnS	lišit
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
ustálena	ustálit	k5eAaPmNgNnP	ustálit
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
rozvoj	rozvoj	k1gInSc1	rozvoj
rané	raný	k2eAgFnSc2d1	raná
šachové	šachový	k2eAgFnSc2d1	šachová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
tištěná	tištěný	k2eAgFnSc1d1	tištěná
šachová	šachový	k2eAgFnSc1d1	šachová
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Repetición	Repetición	k1gInSc1	Repetición
de	de	k?	de
amores	amores	k1gInSc1	amores
y	y	k?	y
arte	arte	k1gInSc1	arte
de	de	k?	de
ajedrez	ajedrez	k1gInSc1	ajedrez
(	(	kIx(	(
<g/>
Opakování	opakování	k1gNnSc1	opakování
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
hry	hra	k1gFnSc2	hra
v	v	k7c4	v
šachy	šach	k1gMnPc4	šach
<g/>
)	)	kIx)	)
španělského	španělský	k2eAgMnSc4d1	španělský
kněze	kněz	k1gMnSc4	kněz
Luise	Luisa	k1gFnSc3	Luisa
Ramireze	Ramireze	k1gFnSc1	Ramireze
de	de	k?	de
Luceny	Lucena	k1gFnSc2	Lucena
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Salamance	Salamanka	k1gFnSc6	Salamanka
roku	rok	k1gInSc2	rok
1497	[number]	k4	1497
<g/>
.	.	kIx.	.
</s>
<s>
Lucena	Lucena	k1gFnSc1	Lucena
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
mistři	mistr	k1gMnPc1	mistr
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
Portugalec	Portugalec	k1gMnSc1	Portugalec
Pedro	Pedro	k1gNnSc4	Pedro
Damiano	Damiana	k1gFnSc5	Damiana
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Leonardo	Leonardo	k1gMnSc1	Leonardo
Di	Di	k1gFnSc2	Di
Bona	bona	k1gFnSc1	bona
<g/>
,	,	kIx,	,
Giulio	Giulia	k1gMnSc5	Giulia
Cesare	Cesar	k1gMnSc5	Cesar
Polerio	Poleria	k1gMnSc5	Poleria
a	a	k8xC	a
Gioacchino	Gioacchin	k2eAgNnSc4d1	Gioacchin
Greco	Greco	k1gNnSc4	Greco
nebo	nebo	k8xC	nebo
španělský	španělský	k2eAgMnSc1d1	španělský
biskup	biskup	k1gMnSc1	biskup
Ruy	Ruy	k1gFnSc2	Ruy
López	López	k1gMnSc1	López
de	de	k?	de
Segura	Segura	k1gFnSc1	Segura
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
základy	základ	k1gInPc4	základ
teorie	teorie	k1gFnSc2	teorie
zahájení	zahájení	k1gNnSc2	zahájení
jako	jako	k8xS	jako
italská	italský	k2eAgFnSc1d1	italská
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
královský	královský	k2eAgInSc1d1	královský
gambit	gambit	k1gInSc1	gambit
a	a	k8xC	a
španělská	španělský	k2eAgFnSc1d1	španělská
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
analyzovat	analyzovat	k5eAaImF	analyzovat
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
koncovky	koncovka	k1gFnPc4	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
evropského	evropský	k2eAgInSc2d1	evropský
šachového	šachový	k2eAgInSc2d1	šachový
života	život	k1gInSc2	život
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
jihoevropských	jihoevropský	k2eAgFnPc2d1	jihoevropská
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
francouzští	francouzský	k2eAgMnPc1d1	francouzský
šachoví	šachový	k2eAgMnPc1d1	šachový
mistři	mistr	k1gMnPc1	mistr
byli	být	k5eAaImAgMnP	být
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
François-André	François-Andrý	k2eAgFnSc2d1	François-Andrý
Danican	Danicana	k1gFnPc2	Danicana
Philidor	Philidor	k1gInSc1	Philidor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
objevil	objevit	k5eAaPmAgInS	objevit
důležitost	důležitost	k1gFnSc4	důležitost
pěšců	pěšec	k1gMnPc2	pěšec
pro	pro	k7c4	pro
šachovou	šachový	k2eAgFnSc4d1	šachová
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Louis-Charles	Louis-Charles	k1gMnSc1	Louis-Charles
Mahé	Mahá	k1gFnSc2	Mahá
de	de	k?	de
La	la	k1gNnSc1	la
Bourdonnais	Bourdonnais	k1gFnSc1	Bourdonnais
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
slavné	slavný	k2eAgFnSc2d1	slavná
série	série	k1gFnSc2	série
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
nejsilnějším	silný	k2eAgMnSc7d3	nejsilnější
britským	britský	k2eAgMnSc7d1	britský
mistrem	mistr	k1gMnSc7	mistr
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Alexanderem	Alexander	k1gMnSc7	Alexander
McDonnellem	McDonnell	k1gMnSc7	McDonnell
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc2d1	konaná
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Centry	centr	k1gInPc1	centr
šachového	šachový	k2eAgInSc2d1	šachový
života	život	k1gInSc2	život
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
kavárny	kavárna	k1gFnPc1	kavárna
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
evropských	evropský	k2eAgNnPc6d1	Evropské
městech	město	k1gNnPc6	město
jako	jako	k8xS	jako
Café	café	k1gNnSc2	café
de	de	k?	de
la	la	k1gNnSc2	la
Régence	Régenec	k1gMnSc2	Régenec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Simpson	Simpson	k1gInSc1	Simpson
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Divan	divan	k1gInSc1	divan
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
organizace	organizace	k1gFnSc1	organizace
šachového	šachový	k2eAgInSc2d1	šachový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
množství	množství	k1gNnSc1	množství
šachových	šachový	k2eAgInPc2d1	šachový
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
korespondenční	korespondenční	k2eAgInPc1d1	korespondenční
zápasy	zápas	k1gInPc1	zápas
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
kluby	klub	k1gInPc7	klub
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
hrál	hrát	k5eAaImAgInS	hrát
londýnský	londýnský	k2eAgInSc1d1	londýnský
šachový	šachový	k2eAgInSc1d1	šachový
klub	klub	k1gInSc1	klub
proti	proti	k7c3	proti
edinburskému	edinburský	k2eAgMnSc3d1	edinburský
<g/>
.	.	kIx.	.
</s>
<s>
Šachové	šachový	k2eAgInPc1d1	šachový
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
součástí	součást	k1gFnSc7	součást
periodik	periodikum	k1gNnPc2	periodikum
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gMnPc4	jejich
významné	významný	k2eAgMnPc4d1	významný
tvůrce	tvůrce	k1gMnPc4	tvůrce
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
počítali	počítat	k5eAaImAgMnP	počítat
například	například	k6eAd1	například
Bernhard	Bernhard	k1gMnSc1	Bernhard
Horwitz	Horwitz	k1gMnSc1	Horwitz
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kling	Kling	k1gMnSc1	Kling
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
Loyd	Loyd	k1gMnSc1	Loyd
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
vydání	vydání	k1gNnPc2	vydání
nejstaršího	starý	k2eAgInSc2d3	nejstarší
souhrnného	souhrnný	k2eAgInSc2d1	souhrnný
manuálu	manuál	k1gInSc2	manuál
šachové	šachový	k2eAgFnSc2d1	šachová
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
Handbuch	Handbuch	k1gMnSc1	Handbuch
des	des	k1gNnSc2	des
Schachspiels	Schachspiels	k1gInSc1	Schachspiels
(	(	kIx(	(
<g/>
Příručka	příručka	k1gFnSc1	příručka
hry	hra	k1gFnSc2	hra
šachové	šachový	k2eAgFnSc2d1	šachová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
němečtí	německý	k2eAgMnPc1d1	německý
šachoví	šachový	k2eAgMnPc1d1	šachový
mistři	mistr	k1gMnPc1	mistr
Paul	Paul	k1gMnSc1	Paul
Rudolf	Rudolf	k1gMnSc1	Rudolf
von	von	k1gInSc4	von
Bilguer	Bilguer	k1gMnSc1	Bilguer
a	a	k8xC	a
Tassilo	Tassilo	k1gFnSc1	Tassilo
von	von	k1gInSc1	von
Heydebrand	Heydebrand	k1gInSc1	Heydebrand
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Lasa	laso	k1gNnPc1	laso
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
moderní	moderní	k2eAgInSc1d1	moderní
šachový	šachový	k2eAgInSc1d1	šachový
turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
a	a	k8xC	a
překvapivě	překvapivě	k6eAd1	překvapivě
jej	on	k3xPp3gMnSc4	on
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
organizátor	organizátor	k1gMnSc1	organizátor
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
přední	přední	k2eAgMnSc1d1	přední
anglický	anglický	k2eAgMnSc1d1	anglický
hráč	hráč	k1gMnSc1	hráč
Howard	Howard	k1gMnSc1	Howard
Staunton	Staunton	k1gInSc4	Staunton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
málo	málo	k6eAd1	málo
známý	známý	k2eAgMnSc1d1	známý
Němec	Němec	k1gMnSc1	Němec
Adolf	Adolf	k1gMnSc1	Adolf
Anderssen	Anderssno	k1gNnPc2	Anderssno
<g/>
.	.	kIx.	.
</s>
<s>
Anderssen	Anderssen	k1gInSc1	Anderssen
byl	být	k5eAaImAgInS	být
oslavován	oslavován	k2eAgMnSc1d1	oslavován
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
šachista	šachista	k1gMnSc1	šachista
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
líbivý	líbivý	k2eAgMnSc1d1	líbivý
<g/>
,	,	kIx,	,
energický	energický	k2eAgMnSc1d1	energický
–	–	k?	–
ale	ale	k9	ale
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
strategicky	strategicky	k6eAd1	strategicky
mělký	mělký	k2eAgInSc1d1	mělký
–	–	k?	–
útočný	útočný	k2eAgInSc1d1	útočný
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
hojně	hojně	k6eAd1	hojně
napodobován	napodobovat	k5eAaImNgInS	napodobovat
<g/>
.	.	kIx.	.
</s>
<s>
Jiskřivé	jiskřivý	k2eAgFnPc1d1	jiskřivá
kombinace	kombinace	k1gFnPc1	kombinace
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Anderssenova	Anderssenův	k2eAgFnSc1d1	Anderssenův
Nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
partie	partie	k1gFnSc1	partie
nebo	nebo	k8xC	nebo
Morphyho	Morphy	k1gMnSc2	Morphy
Operní	operní	k2eAgFnSc1d1	operní
hra	hra	k1gFnSc1	hra
–	–	k?	–
obě	dva	k4xCgFnPc1	dva
krátké	krátký	k2eAgFnPc1d1	krátká
přátelské	přátelský	k2eAgFnPc1d1	přátelská
partie	partie	k1gFnPc1	partie
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
oběťmi	oběť	k1gFnPc7	oběť
–	–	k?	–
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
možný	možný	k2eAgInSc4d1	možný
vrchol	vrchol	k1gInSc4	vrchol
šachového	šachový	k2eAgNnSc2d1	šachové
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Hlubší	hluboký	k2eAgInSc1d2	hlubší
vhled	vhled	k1gInSc1	vhled
do	do	k7c2	do
povahy	povaha	k1gFnSc2	povaha
šachu	šach	k1gInSc2	šach
přišel	přijít	k5eAaPmAgMnS	přijít
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
mladšími	mladý	k2eAgMnPc7d2	mladší
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Američan	Američan	k1gMnSc1	Američan
Paul	Paul	k1gMnSc1	Paul
Morphy	Morpha	k1gFnPc4	Morpha
<g/>
,	,	kIx,	,
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
šachové	šachový	k2eAgNnSc4d1	šachové
zázračné	zázračný	k2eAgNnSc4d1	zázračné
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
krátké	krátký	k2eAgFnSc2d1	krátká
kariéry	kariéra	k1gFnSc2	kariéra
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1857	[number]	k4	1857
a	a	k8xC	a
1863	[number]	k4	1863
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
nad	nad	k7c7	nad
všemi	všecek	k3xTgMnPc7	všecek
důležitými	důležitý	k2eAgMnPc7d1	důležitý
konkurenty	konkurent	k1gMnPc7	konkurent
včetně	včetně	k7c2	včetně
Anderssena	Anderssen	k2eAgFnSc1d1	Anderssena
<g/>
.	.	kIx.	.
</s>
<s>
Morphyho	Morphyze	k6eAd1	Morphyze
úspěch	úspěch	k1gInSc1	úspěch
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
skvělého	skvělý	k2eAgNnSc2d1	skvělé
útočného	útočný	k2eAgNnSc2d1	útočné
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
zdravé	zdravý	k2eAgFnSc2d1	zdravá
strategie	strategie	k1gFnSc2	strategie
<g/>
;	;	kIx,	;
intuitivně	intuitivně	k6eAd1	intuitivně
chápal	chápat	k5eAaImAgMnS	chápat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
správně	správně	k6eAd1	správně
připravit	připravit	k5eAaPmF	připravit
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
později	pozdě	k6eAd2	pozdě
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
jiný	jiný	k2eAgMnSc1d1	jiný
silný	silný	k2eAgMnSc1d1	silný
mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
rodák	rodák	k1gMnSc1	rodák
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Steinitz	Steinitz	k1gMnSc1	Steinitz
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
svých	svůj	k3xOyFgInPc2	svůj
teoretických	teoretický	k2eAgInPc2d1	teoretický
úspěchů	úspěch	k1gInPc2	úspěch
Steinitz	Steinitza	k1gFnPc2	Steinitza
založil	založit	k5eAaPmAgInS	založit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
tradici	tradice	k1gFnSc4	tradice
šachového	šachový	k2eAgInSc2d1	šachový
sportu	sport	k1gInSc2	sport
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gInSc4	jeho
triumf	triumf	k1gInSc4	triumf
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
vůdčím	vůdčí	k2eAgMnSc7d1	vůdčí
německým	německý	k2eAgMnSc7d1	německý
mistrem	mistr	k1gMnSc7	mistr
Johannesem	Johannes	k1gMnSc7	Johannes
Zukertortem	Zukertort	k1gInSc7	Zukertort
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
první	první	k4xOgNnSc4	první
oficiální	oficiální	k2eAgNnSc4d1	oficiální
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
a	a	k8xC	a
Steinitz	Steinitz	k1gInSc1	Steinitz
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
korunu	koruna	k1gFnSc4	koruna
Steinitze	Steinitze	k1gFnSc2	Steinitze
připravil	připravit	k5eAaPmAgInS	připravit
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
mnohem	mnohem	k6eAd1	mnohem
mladší	mladý	k2eAgMnSc1d2	mladší
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
Emanuel	Emanuel	k1gMnSc1	Emanuel
Lasker	Lasker	k1gMnSc1	Lasker
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
titul	titul	k1gInSc1	titul
udržel	udržet	k5eAaPmAgInS	udržet
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zázračné	zázračný	k2eAgNnSc1d1	zázračné
dítě	dítě	k1gNnSc1	dítě
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
Raúl	Raúl	k1gMnSc1	Raúl
Capablanca	Capablanca	k1gMnSc1	Capablanca
(	(	kIx(	(
<g/>
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ukončil	ukončit	k5eAaPmAgInS	ukončit
dominanci	dominance	k1gFnSc4	dominance
německy	německy	k6eAd1	německy
hovořících	hovořící	k2eAgMnPc2d1	hovořící
šachových	šachový	k2eAgMnPc2d1	šachový
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
koncovky	koncovka	k1gFnPc4	koncovka
<g/>
;	;	kIx,	;
během	během	k7c2	během
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
partii	partie	k1gFnSc6	partie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
byl	být	k5eAaImAgMnS	být
rusko-francouzský	ruskorancouzský	k2eAgMnSc1d1	rusko-francouzský
velmistr	velmistr	k1gMnSc1	velmistr
Alexandr	Alexandr	k1gMnSc1	Alexandr
Aljechin	Aljechin	k1gMnSc1	Aljechin
<g/>
,	,	kIx,	,
silný	silný	k2eAgMnSc1d1	silný
útočný	útočný	k2eAgMnSc1d1	útočný
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
titul	titul	k1gInSc1	titul
zachoval	zachovat	k5eAaPmAgInS	zachovat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ztratil	ztratit	k5eAaPmAgMnS	ztratit
ho	on	k3xPp3gNnSc4	on
jen	jen	k6eAd1	jen
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Nizozemce	Nizozemec	k1gMnSc2	Nizozemec
Maxe	Max	k1gMnSc2	Max
Euweho	Euwe	k1gMnSc2	Euwe
<g/>
,	,	kIx,	,
v	v	k7c6	v
odvetném	odvetný	k2eAgInSc6d1	odvetný
zápase	zápas	k1gInSc6	zápas
ovšem	ovšem	k9	ovšem
Euweho	Euwe	k1gMnSc4	Euwe
porazil	porazit	k5eAaPmAgInS	porazit
a	a	k8xC	a
titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
prošla	projít	k5eAaPmAgFnS	projít
šachová	šachový	k2eAgFnSc1d1	šachová
teorie	teorie	k1gFnSc1	teorie
revolucí	revoluce	k1gFnSc7	revoluce
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
hypermodernismu	hypermodernismus	k1gInSc2	hypermodernismus
<g/>
,	,	kIx,	,
zastávaného	zastávaný	k2eAgInSc2d1	zastávaný
mistry	mistr	k1gMnPc7	mistr
jako	jako	k8xS	jako
Aaron	Aaron	k1gMnSc1	Aaron
Nimcovič	Nimcovič	k1gMnSc1	Nimcovič
či	či	k8xC	či
Richard	Richard	k1gMnSc1	Richard
Réti	Rét	k1gFnSc2	Rét
<g/>
.	.	kIx.	.
</s>
<s>
Hypermodernisté	hypermodernista	k1gMnPc1	hypermodernista
popírali	popírat	k5eAaImAgMnP	popírat
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
principy	princip	k1gInPc4	princip
klasické	klasický	k2eAgFnSc2d1	klasická
Steinitzovy	Steinitzův	k2eAgFnSc2d1	Steinitzova
a	a	k8xC	a
Tarraschovy	Tarraschův	k2eAgFnSc2d1	Tarraschův
poziční	poziční	k2eAgFnSc2d1	poziční
školy	škola	k1gFnSc2	škola
<g/>
:	:	kIx,	:
namísto	namísto	k7c2	namísto
přímého	přímý	k2eAgNnSc2d1	přímé
obsazování	obsazování	k1gNnSc2	obsazování
centra	centrum	k1gNnSc2	centrum
šachovnice	šachovnice	k1gFnSc2	šachovnice
pěšci	pěšec	k1gMnPc1	pěšec
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
centrum	centrum	k1gNnSc4	centrum
spíše	spíše	k9	spíše
pomocí	pomocí	k7c2	pomocí
jeho	jeho	k3xOp3gNnSc2	jeho
napadání	napadání	k1gNnSc2	napadání
figurami	figura	k1gFnPc7	figura
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožní	umožnit	k5eAaPmIp3nS	umožnit
snáze	snadno	k6eAd2	snadno
zaútočit	zaútočit	k5eAaPmF	zaútočit
proti	proti	k7c3	proti
soupeřovým	soupeřův	k2eAgMnPc3d1	soupeřův
pěšcům	pěšec	k1gMnPc3	pěšec
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc1	počet
každoročně	každoročně	k6eAd1	každoročně
konaných	konaný	k2eAgInPc2d1	konaný
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
turnajů	turnaj	k1gInPc2	turnaj
a	a	k8xC	a
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
formálně	formálně	k6eAd1	formálně
udělil	udělit	k5eAaPmAgInS	udělit
titul	titul	k1gInSc4	titul
šachový	šachový	k2eAgMnSc1d1	šachový
velmistr	velmistr	k1gMnSc1	velmistr
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
pěti	pět	k4xCc3	pět
finalistům	finalista	k1gMnPc3	finalista
turnaje	turnaj	k1gInPc4	turnaj
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
(	(	kIx(	(
<g/>
Lasker	Lasker	k1gMnSc1	Lasker
<g/>
,	,	kIx,	,
Capablanca	Capablanca	k1gMnSc1	Capablanca
<g/>
,	,	kIx,	,
Aljechin	Aljechin	k1gMnSc1	Aljechin
<g/>
,	,	kIx,	,
Tarrasch	Tarrasch	k1gMnSc1	Tarrasch
a	a	k8xC	a
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
později	pozdě	k6eAd2	pozdě
navázala	navázat	k5eAaPmAgFnS	navázat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
šachová	šachový	k2eAgFnSc1d1	šachová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
des	des	k1gNnSc2	des
Échecs	Échecsa	k1gFnPc2	Échecsa
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
FIDE	FIDE	kA	FIDE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
titul	titul	k1gInSc1	titul
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
držitelkou	držitelka	k1gFnSc7	držitelka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
česko-anglická	českonglický	k2eAgFnSc1d1	česko-anglická
šachistka	šachistka	k1gFnSc1	šachistka
Věra	Věra	k1gFnSc1	Věra
Menčíková	Menčíková	k1gFnSc1	Menčíková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Aljechina	Aljechina	k1gMnSc1	Aljechina
určil	určit	k5eAaPmAgMnS	určit
nového	nový	k2eAgMnSc4d1	nový
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
turnaj	turnaj	k1gInSc4	turnaj
elitních	elitní	k2eAgMnPc2d1	elitní
šachistů	šachista	k1gMnPc2	šachista
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
šachovou	šachový	k2eAgFnSc7d1	šachová
federací	federace	k1gFnSc7	federace
FIDE	FIDE	kA	FIDE
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zápolení	zápolený	k2eAgMnPc1d1	zápolený
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
řídila	řídit	k5eAaImAgFnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
Michail	Michail	k1gMnSc1	Michail
Botvinnik	Botvinnik	k1gMnSc1	Botvinnik
zahájil	zahájit	k5eAaPmAgMnS	zahájit
éru	éra	k1gFnSc4	éra
sovětské	sovětský	k2eAgFnSc2d1	sovětská
převahy	převaha	k1gFnSc2	převaha
v	v	k7c6	v
šachovém	šachový	k2eAgInSc6d1	šachový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	se	k3xPyFc4	se
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
stal	stát	k5eAaPmAgMnS	stát
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
nesovětský	sovětský	k2eNgMnSc1d1	nesovětský
šachista	šachista	k1gMnSc1	šachista
<g/>
,	,	kIx,	,
Američan	Američan	k1gMnSc1	Američan
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předválečný	předválečný	k2eAgInSc1d1	předválečný
neformální	formální	k2eNgInSc1d1	neformální
systém	systém	k1gInSc1	systém
bojů	boj	k1gInPc2	boj
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yInSc7	kdo
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzyvatel	vyzyvatel	k?	vyzyvatel
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
zajistit	zajistit	k5eAaPmF	zajistit
organizační	organizační	k2eAgInPc4d1	organizační
náklady	náklad	k1gInPc4	náklad
a	a	k8xC	a
cenový	cenový	k2eAgInSc4d1	cenový
fond	fond	k1gInSc4	fond
<g/>
.	.	kIx.	.
</s>
<s>
FIDE	FIDE	kA	FIDE
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
kvalifikačních	kvalifikační	k2eAgInPc2d1	kvalifikační
turnajů	turnaj	k1gInPc2	turnaj
a	a	k8xC	a
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgMnPc1d3	nejsilnější
šachisté	šachista	k1gMnPc1	šachista
světa	svět	k1gInSc2	svět
byli	být	k5eAaImAgMnP	být
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
do	do	k7c2	do
mezipásmových	mezipásmový	k2eAgInPc2d1	mezipásmový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
vítězi	vítěz	k1gMnPc7	vítěz
pásmových	pásmový	k2eAgInPc2d1	pásmový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
umístili	umístit	k5eAaPmAgMnP	umístit
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
mezipásmových	mezipásmový	k2eAgInPc6d1	mezipásmový
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
býval	bývat	k5eAaImAgInS	bývat
nejdříve	dříve	k6eAd3	dříve
dvoukolový	dvoukolový	k2eAgInSc1d1	dvoukolový
turnaj	turnaj	k1gInSc1	turnaj
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
řada	řada	k1gFnSc1	řada
zápasů	zápas	k1gInPc2	zápas
hraných	hraný	k2eAgInPc2d1	hraný
vylučovacím	vylučovací	k2eAgInSc7d1	vylučovací
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
turnaje	turnaj	k1gInSc2	turnaj
kandidátů	kandidát	k1gMnPc2	kandidát
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
právo	právo	k1gNnSc4	právo
vyzvat	vyzvat	k5eAaPmF	vyzvat
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
k	k	k7c3	k
zápasu	zápas	k1gInSc3	zápas
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
míval	mívat	k5eAaImAgMnS	mívat
zprvu	zprvu	k6eAd1	zprvu
právo	právo	k1gNnSc4	právo
odvety	odveta	k1gFnSc2	odveta
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
systém	systém	k1gInSc1	systém
fungoval	fungovat	k5eAaImAgInS	fungovat
s	s	k7c7	s
tříročním	tříroční	k2eAgInSc7d1	tříroční
cyklem	cyklus	k1gInSc7	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Botvinnik	Botvinnik	k1gMnSc1	Botvinnik
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
zápasů	zápas	k1gInPc2	zápas
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
a	a	k8xC	a
1954	[number]	k4	1954
ho	on	k3xPp3gMnSc4	on
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
ho	on	k3xPp3gInSc4	on
porazil	porazit	k5eAaPmAgMnS	porazit
Rus	Rus	k1gMnSc1	Rus
Vasilij	Vasilij	k1gFnSc2	Vasilij
Smyslov	Smyslov	k1gInSc1	Smyslov
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
vyváženým	vyvážený	k2eAgInSc7d1	vyvážený
šachovým	šachový	k2eAgInSc7d1	šachový
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Botvinnik	Botvinnik	k1gInSc1	Botvinnik
titul	titul	k1gInSc1	titul
za	za	k7c4	za
rok	rok	k1gInSc4	rok
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
získal	získat	k5eAaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Lotyš	Lotyš	k1gMnSc1	Lotyš
Michail	Michail	k1gMnSc1	Michail
Tal	Tal	k1gMnSc1	Tal
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
taktik	taktik	k1gMnSc1	taktik
a	a	k8xC	a
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Botvinnik	Botvinnik	k1gInSc1	Botvinnik
však	však	k9	však
Tala	Talus	k1gMnSc4	Talus
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
opět	opět	k6eAd1	opět
zdolal	zdolat	k5eAaPmAgMnS	zdolat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
FIDE	FIDE	kA	FIDE
právo	právo	k1gNnSc4	právo
odvety	odveta	k1gFnSc2	odveta
zrušila	zrušit	k5eAaPmAgFnS	zrušit
a	a	k8xC	a
příští	příští	k2eAgMnSc1d1	příští
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Armén	Armén	k1gMnSc1	Armén
Tigran	Tigrana	k1gFnPc2	Tigrana
Petrosjan	Petrosjan	k1gMnSc1	Petrosjan
<g/>
,	,	kIx,	,
génius	génius	k1gMnSc1	génius
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
silný	silný	k2eAgMnSc1d1	silný
poziční	poziční	k2eAgMnSc1d1	poziční
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
cykly	cyklus	k1gInPc4	cyklus
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
Rus	Rus	k1gMnSc1	Rus
Boris	Boris	k1gMnSc1	Boris
Spasskij	Spasskij	k1gMnSc1	Spasskij
(	(	kIx(	(
<g/>
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
univerzálním	univerzální	k2eAgMnSc7d1	univerzální
šachistou	šachista	k1gMnSc7	šachista
schopným	schopný	k2eAgMnPc3d1	schopný
vítězit	vítězit	k5eAaImF	vítězit
jak	jak	k8xS	jak
čistě	čistě	k6eAd1	čistě
pozičně	pozičně	k6eAd1	pozičně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
ostrého	ostrý	k2eAgInSc2d1	ostrý
taktického	taktický	k2eAgInSc2d1	taktický
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
první	první	k4xOgMnSc1	první
nesovětský	sovětský	k2eNgMnSc1d1	nesovětský
finalista	finalista	k1gMnSc1	finalista
od	od	k7c2	od
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Američan	Američan	k1gMnSc1	Američan
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
své	svůj	k3xOyFgMnPc4	svůj
oponenty	oponent	k1gMnPc4	oponent
v	v	k7c6	v
turnaji	turnaj	k1gInSc6	turnaj
kandidátů	kandidát	k1gMnPc2	kandidát
neslýchaným	slýchaný	k2eNgInSc7d1	neslýchaný
rozdílem	rozdíl	k1gInSc7	rozdíl
a	a	k8xC	a
jasně	jasně	k6eAd1	jasně
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
i	i	k9	i
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
však	však	k8xC	však
Fischer	Fischer	k1gMnSc1	Fischer
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
obhajovat	obhajovat	k5eAaImF	obhajovat
titul	titul	k1gInSc4	titul
proti	proti	k7c3	proti
vyzyvateli	vyzyvateli	k?	vyzyvateli
<g/>
,	,	kIx,	,
mladému	mladý	k2eAgMnSc3d1	mladý
sovětskému	sovětský	k2eAgMnSc3d1	sovětský
velmistru	velmistr	k1gMnSc3	velmistr
Anatoliji	Anatoliji	k1gMnSc3	Anatoliji
Karpovovi	Karpova	k1gMnSc3	Karpova
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
FIDE	FIDE	kA	FIDE
nesplnila	splnit	k5eNaPmAgFnS	splnit
jeho	jeho	k3xOp3gInPc4	jeho
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Karpov	Karpov	k1gInSc1	Karpov
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
Karpov	Karpovo	k1gNnPc2	Karpovo
dvakrát	dvakrát	k6eAd1	dvakrát
obhájil	obhájit	k5eAaPmAgInS	obhájit
proti	proti	k7c3	proti
Viktoru	Viktor	k1gMnSc6	Viktor
Korčnému	Korčné	k1gNnSc3	Korčné
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dominoval	dominovat	k5eAaImAgInS	dominovat
šachovému	šachový	k2eAgInSc3d1	šachový
životu	život	k1gInSc3	život
sérií	série	k1gFnPc2	série
skvělých	skvělý	k2eAgNnPc2d1	skvělé
turnajových	turnajový	k2eAgNnPc2d1	turnajové
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Karpovovu	Karpovův	k2eAgFnSc4d1	Karpovova
nadvládu	nadvláda	k1gFnSc4	nadvláda
nakonec	nakonec	k6eAd1	nakonec
skončil	skončit	k5eAaPmAgMnS	skončit
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
mladý	mladý	k1gMnSc1	mladý
Kavkazan	Kavkazan	k1gMnSc1	Kavkazan
Garri	Garr	k1gFnSc2	Garr
Kasparov	Kasparov	k1gInSc1	Kasparov
<g/>
.	.	kIx.	.
</s>
<s>
Karpov	Karpov	k1gInSc1	Karpov
a	a	k8xC	a
Kasparov	Kasparov	k1gInSc1	Kasparov
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1984	[number]	k4	1984
a	a	k8xC	a
1990	[number]	k4	1990
svedli	svést	k5eAaPmAgMnP	svést
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
zápasů	zápas	k1gInPc2	zápas
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Karpov	Karpov	k1gInSc1	Karpov
už	už	k6eAd1	už
ztracený	ztracený	k2eAgInSc4d1	ztracený
titul	titul	k1gInSc4	titul
nikdy	nikdy	k6eAd1	nikdy
nezískal	získat	k5eNaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Kasparov	Kasparov	k1gInSc1	Kasparov
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
světovým	světový	k2eAgMnSc7d1	světový
šachistou	šachista	k1gMnSc7	šachista
až	až	k8xS	až
do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
své	svůj	k3xOyFgFnSc2	svůj
závodní	závodní	k2eAgFnSc2d1	závodní
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
rekordu	rekord	k1gInSc2	rekord
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
jedničkou	jednička	k1gFnSc7	jednička
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
Kasparov	Kasparov	k1gInSc1	Kasparov
a	a	k8xC	a
Nigel	Nigel	k1gInSc1	Nigel
Short	Shorta	k1gFnPc2	Shorta
rozešli	rozejít	k5eAaPmAgMnP	rozejít
s	s	k7c7	s
FIDE	FIDE	kA	FIDE
<g/>
,	,	kIx,	,
zorganizovali	zorganizovat	k5eAaPmAgMnP	zorganizovat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
zápas	zápas	k1gInSc4	zápas
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
konkurenční	konkurenční	k2eAgFnSc4d1	konkurenční
Asociaci	asociace	k1gFnSc4	asociace
šachových	šachový	k2eAgMnPc2d1	šachový
profesionálů	profesionál	k1gMnPc2	profesionál
(	(	kIx(	(
<g/>
Professional	Professional	k1gMnSc1	Professional
Chess	Chessa	k1gFnPc2	Chessa
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
PCA	PCA	kA	PCA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
pak	pak	k6eAd1	pak
existovali	existovat	k5eAaImAgMnP	existovat
paralelně	paralelně	k6eAd1	paralelně
dva	dva	k4xCgMnPc1	dva
mistři	mistr	k1gMnPc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
šampionáty	šampionát	k1gInPc4	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
PCA	PCA	kA	PCA
(	(	kIx(	(
<g/>
či	či	k8xC	či
"	"	kIx"	"
<g/>
klasický	klasický	k2eAgMnSc1d1	klasický
<g/>
"	"	kIx"	"
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
steinitzovské	steinitzovský	k2eAgFnSc2d1	steinitzovský
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
vyzyvatelem	vyzyvatelem	k?	vyzyvatelem
zápas	zápas	k1gInSc1	zápas
(	(	kIx(	(
<g/>
řadu	řad	k1gInSc2	řad
mnoha	mnoho	k4c2	mnoho
šachových	šachový	k2eAgFnPc2d1	šachová
partií	partie	k1gFnPc2	partie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
FIDE	FIDE	kA	FIDE
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
získával	získávat	k5eAaImAgMnS	získávat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nového	nový	k2eAgInSc2d1	nový
formátu	formát	k1gInSc2	formát
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Kasparov	Kasparov	k1gInSc1	Kasparov
ztratil	ztratit	k5eAaPmAgInS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
klasický	klasický	k2eAgInSc4d1	klasický
titul	titul	k1gInSc4	titul
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
porazil	porazit	k5eAaPmAgMnS	porazit
Rus	Rus	k1gMnSc1	Rus
Vladimir	Vladimir	k1gMnSc1	Vladimir
Kramnik	Kramnik	k1gMnSc1	Kramnik
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
obou	dva	k4xCgNnPc2	dva
titulů	titul	k1gInPc2	titul
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kramnik	Kramnik	k1gMnSc1	Kramnik
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
konaném	konaný	k2eAgInSc6d1	konaný
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
FIDE	FIDE	kA	FIDE
porazil	porazit	k5eAaPmAgMnS	porazit
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
FIDE	FIDE	kA	FIDE
Veselina	Veselina	k1gFnSc1	Veselina
Topalova	Topalův	k2eAgFnSc1d1	Topalova
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jediným	jediný	k2eAgMnSc7d1	jediný
a	a	k8xC	a
nezpochybňovaným	zpochybňovaný	k2eNgMnSc7d1	nezpochybňovaný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
stal	stát	k5eAaPmAgMnS	stát
Ind	Ind	k1gMnSc1	Ind
Viswanathan	Viswanathan	k1gMnSc1	Viswanathan
Anand	Anand	k1gMnSc1	Anand
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
turnaje	turnaj	k1gInSc2	turnaj
osmi	osm	k4xCc2	osm
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
Nor	Nor	k1gMnSc1	Nor
Magnus	Magnus	k1gMnSc1	Magnus
Carlsen	Carlsen	k1gInSc4	Carlsen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
držitelem	držitel	k1gMnSc7	držitel
rekordu	rekord	k1gInSc2	rekord
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
ratingu	rating	k1gInSc2	rating
Elo	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
šachové	šachový	k2eAgFnSc6d1	šachová
hře	hra	k1gFnSc6	hra
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
latinsky	latinsky	k6eAd1	latinsky
psané	psaný	k2eAgFnSc6d1	psaná
Svatovojtěšské	svatovojtěšský	k2eAgFnSc6d1	Svatovojtěšská
legendě	legenda	k1gFnSc6	legenda
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
česky	česky	k6eAd1	česky
psaným	psaný	k2eAgNnSc7d1	psané
dílem	dílo	k1gNnSc7	dílo
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
hře	hra	k1gFnSc6	hra
jsou	být	k5eAaImIp3nP	být
Štítného	štítný	k2eAgMnSc4d1	štítný
Kniežky	Kniežek	k1gInPc7	Kniežek
o	o	k7c6	o
šašiech	šaši	k1gFnPc6	šaši
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
tehdejší	tehdejší	k2eAgNnPc4d1	tehdejší
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
šachový	šachový	k2eAgInSc1d1	šachový
život	život	k1gInSc1	život
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
šachová	šachový	k2eAgFnSc1d1	šachová
skladba	skladba	k1gFnSc1	skladba
<g/>
;	;	kIx,	;
úloháři	úlohář	k1gMnPc1	úlohář
zde	zde	k6eAd1	zde
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
českou	český	k2eAgFnSc4d1	Česká
školu	škola	k1gFnSc4	škola
úlohovou	úlohový	k2eAgFnSc4d1	úlohová
<g/>
,	,	kIx,	,
stylově	stylově	k6eAd1	stylově
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
staroněmecké	staroněmecký	k2eAgFnSc2d1	Staroněmecká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
šachovým	šachový	k2eAgInSc7d1	šachový
spolkem	spolek	k1gInSc7	spolek
byl	být	k5eAaImAgMnS	být
pražský	pražský	k2eAgMnSc1d1	pražský
Prager	Prager	k1gMnSc1	Prager
Schachklub	Schachklub	k1gMnSc1	Schachklub
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
sjezd	sjezd	k1gInSc1	sjezd
šachistů	šachista	k1gMnPc2	šachista
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
první	první	k4xOgInSc1	první
významnější	významný	k2eAgInSc1d2	významnější
český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
spolek	spolek	k1gInSc1	spolek
šachovní	šachovní	k2eAgInSc1d1	šachovní
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
také	také	k9	také
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
první	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
šachový	šachový	k2eAgInSc4d1	šachový
časopis	časopis	k1gInSc4	časopis
Šach-	Šach-	k1gFnSc1	Šach-
<g/>
Mat.	Mat.	k1gFnSc1	Mat.
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
českým	český	k2eAgMnSc7d1	český
šachistou	šachista	k1gMnSc7	šachista
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byl	být	k5eAaImAgMnS	být
Oldřich	Oldřich	k1gMnSc1	Oldřich
Duras	Duras	k1gMnSc1	Duras
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Čech	Čech	k1gMnSc1	Čech
velmistrem	velmistr	k1gMnSc7	velmistr
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
jednota	jednota	k1gFnSc1	jednota
českých	český	k2eAgMnPc2d1	český
šachistů	šachista	k1gMnPc2	šachista
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
sdružující	sdružující	k2eAgFnSc1d1	sdružující
pět	pět	k4xCc4	pět
klubů	klub	k1gInPc2	klub
s	s	k7c7	s
200	[number]	k4	200
členy	člen	k1gMnPc7	člen
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
turnaje	turnaj	k1gInPc1	turnaj
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
pořádat	pořádat	k5eAaImF	pořádat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
1907	[number]	k4	1907
a	a	k8xC	a
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
ČSR	ČSR	kA	ČSR
se	se	k3xPyFc4	se
šachový	šachový	k2eAgInSc1d1	šachový
život	život	k1gInSc1	život
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
řada	řada	k1gFnSc1	řada
velkých	velký	k2eAgInPc2d1	velký
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc4	Československo
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
například	například	k6eAd1	například
Richard	Richard	k1gMnSc1	Richard
Réti	Rét	k1gFnSc2	Rét
nebo	nebo	k8xC	nebo
Salo	Salo	k1gMnSc1	Salo
Flohr	Flohr	k1gMnSc1	Flohr
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
jednota	jednota	k1gFnSc1	jednota
českých	český	k2eAgMnPc2d1	český
šachistů	šachista	k1gMnPc2	šachista
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
stala	stát	k5eAaPmAgFnS	stát
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
šachové	šachový	k2eAgFnSc2d1	šachová
federace	federace	k1gFnSc2	federace
FIDE	FIDE	kA	FIDE
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
družstvo	družstvo	k1gNnSc1	družstvo
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
první	první	k4xOgFnSc7	první
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
neoficiální	oficiální	k2eNgFnSc4d1	neoficiální
šachovou	šachový	k2eAgFnSc4d1	šachová
olympiádu	olympiáda	k1gFnSc4	olympiáda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
a	a	k8xC	a
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
olympiáda	olympiáda	k1gFnSc1	olympiáda
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
U	u	k7c2	u
Nováků	Novák	k1gMnPc2	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Menčíková	Menčíková	k1gFnSc1	Menčíková
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
a	a	k8xC	a
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
ženskou	ženský	k2eAgFnSc7d1	ženská
mistryní	mistryně	k1gFnSc7	mistryně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
vychází	vycházet	k5eAaImIp3nS	vycházet
časopis	časopis	k1gInSc1	časopis
Československý	československý	k2eAgInSc1d1	československý
šach	šach	k1gInSc1	šach
(	(	kIx(	(
<g/>
za	za	k7c2	za
války	válka	k1gFnSc2	válka
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Šach	šach	k1gInSc1	šach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
převzetí	převzetí	k1gNnSc1	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
sice	sice	k8xC	sice
neznamenala	znamenat	k5eNaImAgFnS	znamenat
pro	pro	k7c4	pro
šachy	šach	k1gInPc4	šach
úplnou	úplný	k2eAgFnSc4d1	úplná
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
mnoho	mnoho	k4c1	mnoho
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
turnajů	turnaj	k1gInPc2	turnaj
a	a	k8xC	a
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
šach	šach	k1gMnSc1	šach
si	se	k3xPyFc3	se
udržoval	udržovat	k5eAaImAgMnS	udržovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
mnozí	mnohý	k2eAgMnPc1d1	mnohý
šachisté	šachista	k1gMnPc1	šachista
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
následkem	následkem	k7c2	následkem
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
Věra	Věra	k1gFnSc1	Věra
Menčíková	Menčíková	k1gFnSc1	Menčíková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zavražděni	zavražděn	k2eAgMnPc1d1	zavražděn
nacisty	nacista	k1gMnSc2	nacista
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Treybal	Treybal	k1gInSc1	Treybal
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
(	(	kIx(	(
<g/>
Luděk	Luděk	k1gMnSc1	Luděk
Pachman	Pachman	k1gMnSc1	Pachman
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Kaválek	Kaválek	k1gMnSc1	Kaválek
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Hort	Hort	k1gMnSc1	Hort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
československé	československý	k2eAgNnSc4d1	Československé
družstvo	družstvo	k1gNnSc4	družstvo
dvakrát	dvakrát	k6eAd1	dvakrát
vybojovalo	vybojovat	k5eAaPmAgNnS	vybojovat
titul	titul	k1gInSc4	titul
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
a	a	k8xC	a
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
korespondenční	korespondenční	k2eAgMnPc1d1	korespondenční
šachisté	šachista	k1gMnPc1	šachista
dvakrát	dvakrát	k6eAd1	dvakrát
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
svoji	svůj	k3xOyFgFnSc4	svůj
šachovou	šachový	k2eAgFnSc4d1	šachová
olympiádu	olympiáda	k1gFnSc4	olympiáda
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
a	a	k8xC	a
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
účastnil	účastnit	k5eAaImAgMnS	účastnit
turnaje	turnaj	k1gInPc4	turnaj
kandidátů	kandidát	k1gMnPc2	kandidát
o	o	k7c4	o
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
řídí	řídit	k5eAaImIp3nS	řídit
český	český	k2eAgInSc4d1	český
šachový	šachový	k2eAgInSc4d1	šachový
život	život	k1gInSc4	život
Šachový	šachový	k2eAgInSc1d1	šachový
svaz	svaz	k1gInSc1	svaz
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnějším	silný	k2eAgMnSc7d3	nejsilnější
českým	český	k2eAgMnSc7d1	český
šachistou	šachista	k1gMnSc7	šachista
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Navara	Navar	k1gMnSc2	Navar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2007	[number]	k4	2007
čtrnáctým	čtrnáctý	k4xOgMnSc7	čtrnáctý
hráčem	hráč	k1gMnSc7	hráč
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
českým	český	k2eAgMnSc7d1	český
šachistou	šachista	k1gMnSc7	šachista
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
100	[number]	k4	100
je	být	k5eAaImIp3nS	být
Viktor	Viktor	k1gMnSc1	Viktor
Láznička	Láznička	k1gMnSc1	Láznička
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
předchůdce	předchůdce	k1gMnSc1	předchůdce
šatrandž	šatrandž	k6eAd1	šatrandž
(	(	kIx(	(
<g/>
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
říkalo	říkat	k5eAaImAgNnS	říkat
rovněž	rovněž	k9	rovněž
šachy	šach	k1gInPc1	šach
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
renesanci	renesance	k1gFnSc6	renesance
součástí	součást	k1gFnPc2	součást
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
kultury	kultura	k1gFnSc2	kultura
<g/>
;	;	kIx,	;
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgInP	používat
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
válečné	válečný	k2eAgFnSc2d1	válečná
strategie	strategie	k1gFnSc2	strategie
a	a	k8xC	a
nazývány	nazývat	k5eAaImNgInP	nazývat
"	"	kIx"	"
<g/>
královskou	královský	k2eAgFnSc7d1	královská
hrou	hra	k1gFnSc7	hra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
šlechtou	šlechta	k1gFnSc7	šlechta
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
sedmera	sedmero	k1gNnSc2	sedmero
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
ovládnout	ovládnout	k5eAaPmF	ovládnout
každý	každý	k3xTgMnSc1	každý
dospívající	dospívající	k2eAgMnSc1d1	dospívající
rytíř	rytíř	k1gMnSc1	rytíř
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
plavání	plavání	k1gNnSc2	plavání
<g/>
,	,	kIx,	,
veršování	veršování	k1gNnSc2	veršování
<g/>
,	,	kIx,	,
jízdy	jízda	k1gFnPc4	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
střelby	střelba	k1gFnPc4	střelba
z	z	k7c2	z
kuše	kuše	k1gFnSc2	kuše
<g/>
,	,	kIx,	,
šermu	šerm	k1gInSc2	šerm
a	a	k8xC	a
lovu	lov	k1gInSc2	lov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nádherné	nádherný	k2eAgFnSc2d1	nádherná
šachové	šachový	k2eAgFnSc2d1	šachová
soupravy	souprava	k1gFnSc2	souprava
používané	používaný	k2eAgFnSc2d1	používaná
aristokracií	aristokracie	k1gFnSc7	aristokracie
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
už	už	k6eAd1	už
ztraceny	ztratit	k5eAaPmNgFnP	ztratit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc4	některý
ze	z	k7c2	z
zbývajících	zbývající	k2eAgInPc2d1	zbývající
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Šachové	šachový	k2eAgFnPc4d1	šachová
figurky	figurka	k1gFnPc4	figurka
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Lewis	Lewis	k1gFnSc2	Lewis
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
šachy	šach	k1gInPc1	šach
často	často	k6eAd1	často
používány	používán	k2eAgInPc1d1	používán
jako	jako	k8xC	jako
základna	základna	k1gFnSc1	základna
kázání	kázání	k1gNnSc2	kázání
o	o	k7c6	o
morálce	morálka	k1gFnSc6	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Liber	libra	k1gFnPc2	libra
de	de	k?	de
moribus	moribus	k1gInSc1	moribus
hominum	hominum	k1gInSc1	hominum
et	et	k?	et
officiis	officiis	k1gInSc1	officiis
nobilium	nobilium	k1gNnSc1	nobilium
sive	sivat	k5eAaPmIp3nS	sivat
super	super	k2eAgNnSc1d1	super
ludo	ludo	k1gNnSc1	ludo
scacchorum	scacchorum	k1gNnSc1	scacchorum
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
lidských	lidský	k2eAgInPc2d1	lidský
mravů	mrav	k1gInPc2	mrav
a	a	k8xC	a
povinností	povinnost	k1gFnPc2	povinnost
šlechty	šlechta	k1gFnSc2	šlechta
neboli	neboli	k8xC	neboli
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
v	v	k7c4	v
šachy	šach	k1gInPc4	šach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sepsaná	sepsaný	k2eAgNnPc4d1	sepsané
italským	italský	k2eAgInSc7d1	italský
dominikánem	dominikán	k1gMnSc7	dominikán
Jacobem	Jacob	k1gInSc7	Jacob
de	de	k?	de
Cessolis	Cessolis	k1gFnSc2	Cessolis
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
populární	populární	k2eAgFnSc1d1	populární
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
či	či	k8xC	či
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vydána	vydat	k5eAaPmNgFnS	vydat
tiskem	tisk	k1gInSc7	tisk
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1473	[number]	k4	1473
v	v	k7c6	v
Utrechtu	Utrecht	k1gInSc6	Utrecht
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
Tomáše	Tomáš	k1gMnSc4	Tomáš
Štítného	štítný	k2eAgMnSc2d1	štítný
ze	z	k7c2	z
Štítného	štítný	k2eAgInSc2d1	štítný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
půdorysu	půdorys	k1gInSc6	půdorys
napsal	napsat	k5eAaPmAgInS	napsat
své	svůj	k3xOyFgFnPc4	svůj
Kniežky	Kniežka	k1gFnPc4	Kniežka
o	o	k7c4	o
šašiech	šašiech	k1gInSc4	šašiech
<g/>
.	.	kIx.	.
</s>
<s>
Cessolis	Cessolis	k1gFnSc1	Cessolis
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
používají	používat	k5eAaImIp3nP	používat
šachové	šachový	k2eAgFnPc4d1	šachová
figurky	figurka	k1gFnPc4	figurka
jako	jako	k8xC	jako
metafory	metafora	k1gFnPc4	metafora
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
vrstvy	vrstva	k1gFnPc4	vrstva
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
jejich	jejich	k3xOp3gFnPc4	jejich
povinnosti	povinnost	k1gFnPc4	povinnost
z	z	k7c2	z
pravidel	pravidlo	k1gNnPc2	pravidlo
šachové	šachový	k2eAgFnSc2d1	šachová
hry	hra	k1gFnSc2	hra
nebo	nebo	k8xC	nebo
z	z	k7c2	z
vizuální	vizuální	k2eAgFnSc2d1	vizuální
podoby	podoba	k1gFnSc2	podoba
figur	figura	k1gFnPc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
mnohde	mnohde	k6eAd1	mnohde
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
autority	autorita	k1gFnSc2	autorita
šachy	šach	k1gInPc1	šach
zakazovaly	zakazovat	k5eAaImAgInP	zakazovat
jako	jako	k8xC	jako
projev	projev	k1gInSc1	projev
nemravnosti	nemravnost	k1gFnSc2	nemravnost
či	či	k8xC	či
hráčské	hráčský	k2eAgFnSc2d1	hráčská
vášně	vášeň	k1gFnSc2	vášeň
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
prý	prý	k9	prý
šachy	šach	k1gInPc4	šach
hrával	hrávat	k5eAaImAgMnS	hrávat
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
stal	stát	k5eAaPmAgMnS	stát
jejich	jejich	k3xOp3gMnSc7	jejich
odpůrcem	odpůrce	k1gMnSc7	odpůrce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
promarní	promarnit	k5eAaPmIp3nS	promarnit
spousta	spousta	k1gFnSc1	spousta
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
by	by	kYmCp3nS	by
člověk	člověk	k1gMnSc1	člověk
mohl	moct	k5eAaImAgMnS	moct
strávit	strávit	k5eAaPmF	strávit
v	v	k7c6	v
trpělivé	trpělivý	k2eAgFnSc6d1	trpělivá
práci	práce	k1gFnSc6	práce
nebo	nebo	k8xC	nebo
modlitbách	modlitba	k1gFnPc6	modlitba
<g/>
;	;	kIx,	;
rovněž	rovněž	k9	rovněž
mu	on	k3xPp3gNnSc3	on
vadilo	vadit	k5eAaImAgNnS	vadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šachy	šach	k1gInPc1	šach
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
špatné	špatný	k2eAgInPc4d1	špatný
pocity	pocit	k1gInPc4	pocit
vůči	vůči	k7c3	vůči
protivníkovi	protivník	k1gMnSc3	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hraní	k1gNnSc1	hraní
šachu	šach	k1gInSc2	šach
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
karetních	karetní	k2eAgFnPc2d1	karetní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
Chelčický	Chelčický	k2eAgMnSc1d1	Chelčický
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgInP	být
šachy	šach	k1gInPc1	šach
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
kleriků	klerik	k1gMnPc2	klerik
<g/>
,	,	kIx,	,
studentů	student	k1gMnPc2	student
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
a	a	k8xC	a
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
tak	tak	k9	tak
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
209	[number]	k4	209
<g/>
.	.	kIx.	.
píseň	píseň	k1gFnSc1	píseň
sbírky	sbírka	k1gFnSc2	sbírka
Carmina	Carmin	k2eAgMnSc2d1	Carmin
Burana	buran	k1gMnSc2	buran
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
začínající	začínající	k2eAgMnSc1d1	začínající
jmény	jméno	k1gNnPc7	jméno
šachových	šachový	k2eAgFnPc2d1	šachová
figur	figura	k1gFnPc2	figura
Roch	roch	k0	roch
<g/>
,	,	kIx,	,
pedites	pedites	k1gMnSc1	pedites
<g/>
,	,	kIx,	,
regina	regina	k1gMnSc1	regina
<g/>
...	...	k?	...
Odedávna	odedávna	k6eAd1	odedávna
se	se	k3xPyFc4	se
šachové	šachový	k2eAgInPc1d1	šachový
prvky	prvek	k1gInPc1	prvek
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dekorativnost	dekorativnost	k1gFnSc4	dekorativnost
také	také	k9	také
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
a	a	k8xC	a
užitém	užitý	k2eAgNnSc6d1	užité
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
šachovaná	šachovaný	k2eAgFnSc1d1	šachovaná
orlice	orlice	k1gFnSc1	orlice
na	na	k7c6	na
moravském	moravský	k2eAgInSc6d1	moravský
zemském	zemský	k2eAgInSc6d1	zemský
znaku	znak	k1gInSc6	znak
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
oblibě	obliba	k1gFnSc6	obliba
šachů	šach	k1gInPc2	šach
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
svědčí	svědčit	k5eAaImIp3nS	svědčit
také	také	k9	také
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přešel	přejít	k5eAaPmAgInS	přejít
výraz	výraz	k1gInSc1	výraz
šachovaný	šachovaný	k2eAgInSc1d1	šachovaný
jako	jako	k8xC	jako
vzor	vzor	k1gInSc1	vzor
látky	látka	k1gFnSc2	látka
do	do	k7c2	do
soukenické	soukenický	k2eAgFnSc2d1	Soukenická
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
šašek	šaška	k1gFnPc2	šaška
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
právě	právě	k9	právě
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
nosícího	nosící	k2eAgMnSc2d1	nosící
pestře	pestro	k6eAd1	pestro
šachované	šachovaný	k2eAgNnSc4d1	šachovaný
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Osvícenství	osvícenství	k1gNnSc1	osvícenství
se	se	k3xPyFc4	se
šachy	šach	k1gInPc1	šach
jevily	jevit	k5eAaImAgInP	jevit
především	především	k6eAd1	především
jako	jako	k8xC	jako
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k1gInSc4	Franklin
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
článku	článek	k1gInSc6	článek
The	The	k1gFnSc1	The
Morals	Morals	k1gInSc1	Morals
of	of	k?	of
Chess	Chess	k1gInSc1	Chess
(	(	kIx(	(
<g/>
Morálka	morálka	k1gFnSc1	morálka
šachů	šach	k1gInPc2	šach
<g/>
,	,	kIx,	,
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Šachová	šachový	k2eAgFnSc1d1	šachová
hra	hra	k1gFnSc1	hra
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
zábavou	zábava	k1gFnSc7	zábava
zahalečů	zahaleč	k1gMnPc2	zahaleč
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
nebo	nebo	k8xC	nebo
posílit	posílit	k5eAaPmF	posílit
několik	několik	k4yIc4	několik
velmi	velmi	k6eAd1	velmi
cenných	cenný	k2eAgFnPc2d1	cenná
schopností	schopnost	k1gFnPc2	schopnost
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stanou	stanout	k5eAaPmIp3nP	stanout
návyky	návyk	k1gInPc4	návyk
použitelnými	použitelný	k2eAgInPc7d1	použitelný
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
šachů	šach	k1gInPc2	šach
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
často	často	k6eAd1	často
máme	mít	k5eAaImIp1nP	mít
získávat	získávat	k5eAaImF	získávat
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
soupeřit	soupeřit	k5eAaImF	soupeřit
proti	proti	k7c3	proti
konkurentům	konkurent	k1gMnPc3	konkurent
a	a	k8xC	a
protivníkům	protivník	k1gMnPc3	protivník
a	a	k8xC	a
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nastává	nastávat	k5eAaImIp3nS	nastávat
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
dobrých	dobrý	k2eAgFnPc2d1	dobrá
i	i	k8xC	i
špatných	špatný	k2eAgFnPc2d1	špatná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
následky	následek	k1gInPc1	následek
lidské	lidský	k2eAgFnSc2d1	lidská
prozíravosti	prozíravost	k1gFnSc2	prozíravost
nebo	nebo	k8xC	nebo
jejího	její	k3xOp3gInSc2	její
nedostatku	nedostatek	k1gInSc2	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Hraním	hraní	k1gNnSc7	hraní
šachů	šach	k1gInPc2	šach
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
naučit	naučit	k5eAaPmF	naučit
<g/>
:	:	kIx,	:
Předvídavosti	předvídavost	k1gFnPc1	předvídavost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
hledí	hledět	k5eAaImIp3nP	hledět
do	do	k7c2	do
blízké	blízký	k2eAgFnSc2d1	blízká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
následky	následek	k1gInPc4	následek
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
může	moct	k5eAaImIp3nS	moct
přinést	přinést	k5eAaPmF	přinést
nějaká	nějaký	k3yIgFnSc1	nějaký
akce	akce	k1gFnSc1	akce
<g/>
...	...	k?	...
Pozornosti	pozornost	k1gFnPc1	pozornost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
celou	celý	k2eAgFnSc4d1	celá
šachovnici	šachovnice	k1gFnSc4	šachovnice
čili	čili	k8xC	čili
scénu	scéna	k1gFnSc4	scéna
akce	akce	k1gFnSc2	akce
<g/>
:	:	kIx,	:
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
poměr	poměr	k1gInSc1	poměr
několika	několik	k4yIc2	několik
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
situaci	situace	k1gFnSc4	situace
<g/>
...	...	k?	...
Opatrnosti	opatrnost	k1gFnSc2	opatrnost
<g/>
,	,	kIx,	,
nevykonávat	vykonávat	k5eNaImF	vykonávat
tahy	tah	k1gInPc4	tah
zbrkle	zbrkle	k6eAd1	zbrkle
<g/>
...	...	k?	...
S	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
nadějemi	naděje	k1gFnPc7	naděje
se	se	k3xPyFc4	se
šachům	šach	k1gMnPc3	šach
dnes	dnes	k6eAd1	dnes
učí	učit	k5eAaImIp3nS	učit
děti	dítě	k1gFnPc4	dítě
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
armádách	armáda	k1gFnPc6	armáda
jako	jako	k8xC	jako
mentální	mentální	k2eAgInSc4d1	mentální
trénink	trénink	k1gInSc4	trénink
pro	pro	k7c4	pro
kadety	kadet	k1gMnPc4	kadet
a	a	k8xC	a
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zobrazovány	zobrazován	k2eAgInPc1d1	zobrazován
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k9	jako
metafora	metafora	k1gFnSc1	metafora
boje	boj	k1gInSc2	boj
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
chladné	chladný	k2eAgFnSc2d1	chladná
logiky	logika	k1gFnSc2	logika
anebo	anebo	k8xC	anebo
–	–	k?	–
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
středověkých	středověký	k2eAgMnPc2d1	středověký
moralistů	moralista	k1gMnPc2	moralista
–	–	k?	–
jako	jako	k8xS	jako
alegorie	alegorie	k1gFnSc1	alegorie
života	život	k1gInSc2	život
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
šach	šach	k1gInSc1	šach
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
sahají	sahat	k5eAaImIp3nP	sahat
od	od	k7c2	od
Za	za	k7c7	za
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
Lewise	Lewise	k1gFnSc2	Lewise
Carrolla	Carrollo	k1gNnSc2	Carrollo
až	až	k9	až
po	po	k7c4	po
Královskou	královský	k2eAgFnSc4d1	královská
hru	hra	k1gFnSc4	hra
Stefana	Stefan	k1gMnSc4	Stefan
Zweiga	Zweig	k1gMnSc4	Zweig
či	či	k8xC	či
Lužinovu	Lužinův	k2eAgFnSc4d1	Lužinova
obranu	obrana	k1gFnSc4	obrana
Vladimira	Vladimir	k1gInSc2	Vladimir
Nabokova	Nabokův	k2eAgInSc2d1	Nabokův
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
skládal	skládat	k5eAaImAgInS	skládat
šachové	šachový	k2eAgFnPc4d1	šachová
úlohy	úloha	k1gFnPc4	úloha
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Poems	Poemsa	k1gFnPc2	Poemsa
and	and	k?	and
Problems	Problemsa	k1gFnPc2	Problemsa
je	být	k5eAaImIp3nS	být
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
básněmi	báseň	k1gFnPc7	báseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1	sedmý
pečeť	pečeť	k1gFnSc1	pečeť
Ingmara	Ingmar	k1gMnSc2	Ingmar
Bergmana	Bergman	k1gMnSc2	Bergman
proslula	proslout	k5eAaPmAgFnS	proslout
existenciální	existenciální	k2eAgFnSc7d1	existenciální
metaforou	metafora	k1gFnSc7	metafora
rytíře	rytíř	k1gMnSc2	rytíř
hrajícího	hrající	k2eAgMnSc2d1	hrající
šachy	šach	k1gInPc4	šach
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
hraje	hrát	k5eAaImIp3nS	hrát
čarodějné	čarodějný	k2eAgInPc4d1	čarodějný
šachy	šach	k1gInPc4	šach
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hrdinové	hrdina	k1gMnPc1	hrdina
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
třídimenzionálním	třídimenzionální	k2eAgInPc3d1	třídimenzionální
šachům	šach	k1gInPc3	šach
a	a	k8xC	a
šachy	šach	k1gInPc4	šach
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
letící	letící	k2eAgMnSc1d1	letící
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
v	v	k7c6	v
Kubrickově	Kubrickův	k2eAgInSc6d1	Kubrickův
filmu	film	k1gInSc6	film
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Šachová	šachový	k2eAgFnSc1d1	šachová
notace	notace	k1gFnSc1	notace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
několik	několik	k4yIc1	několik
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
průběh	průběh	k1gInSc4	průběh
šachové	šachový	k2eAgFnSc2d1	šachová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
(	(	kIx(	(
<g/>
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
)	)	kIx)	)
algebraická	algebraický	k2eAgFnSc1d1	algebraická
šachová	šachový	k2eAgFnSc1d1	šachová
notace	notace	k1gFnSc1	notace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
syrský	syrský	k2eAgMnSc1d1	syrský
šachový	šachový	k2eAgMnSc1d1	šachový
teoretik	teoretik	k1gMnSc1	teoretik
Philipp	Philipp	k1gMnSc1	Philipp
Stamma	Stamma	k1gFnSc1	Stamma
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Pole	pole	k1gFnSc2	pole
na	na	k7c6	na
šachovnici	šachovnice	k1gFnSc6	šachovnice
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
označují	označovat	k5eAaImIp3nP	označovat
průsečíkem	průsečík	k1gInSc7	průsečík
sloupce	sloupec	k1gInSc2	sloupec
(	(	kIx(	(
<g/>
označovaného	označovaný	k2eAgInSc2d1	označovaný
písmeny	písmeno	k1gNnPc7	písmeno
a	a	k8xC	a
až	až	k9	až
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgFnSc2d1	označovaná
čísly	číslo	k1gNnPc7	číslo
1	[number]	k4	1
až	až	k8xS	až
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
jednoho	jeden	k4xCgInSc2	jeden
tahu	tah	k1gInSc2	tah
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pořadového	pořadový	k2eAgNnSc2d1	pořadové
čísla	číslo	k1gNnSc2	číslo
tahu	tah	k1gInSc2	tah
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc4	označení
typu	typ	k1gInSc2	typ
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
bylo	být	k5eAaImAgNnS	být
taženo	táhnout	k5eAaImNgNnS	táhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
bylo	být	k5eAaImAgNnS	být
taženo	táhnout	k5eAaImNgNnS	táhnout
<g/>
.	.	kIx.	.
</s>
<s>
Tahy	tah	k1gInPc1	tah
se	se	k3xPyFc4	se
číslují	číslovat	k5eAaImIp3nP	číslovat
od	od	k7c2	od
jedničky	jednička	k1gFnSc2	jednička
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejné	stejný	k2eAgNnSc1d1	stejné
číslo	číslo	k1gNnSc1	číslo
označuje	označovat	k5eAaImIp3nS	označovat
tah	tah	k1gInSc4	tah
bílého	bílé	k1gNnSc2	bílé
i	i	k9	i
následující	následující	k2eAgInSc1d1	následující
tah	tah	k1gInSc1	tah
černého	černé	k1gNnSc2	černé
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
kámen	kámen	k1gInSc1	kámen
táhl	táhnout	k5eAaImAgInS	táhnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
ho	on	k3xPp3gMnSc4	on
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c7	za
označením	označení	k1gNnSc7	označení
druhu	druh	k1gInSc2	druh
kamene	kámen	k1gInSc2	kámen
napíše	napsat	k5eAaPmIp3nS	napsat
označení	označení	k1gNnSc1	označení
výchozího	výchozí	k2eAgInSc2d1	výchozí
sloupce	sloupec	k1gInSc2	sloupec
nebo	nebo	k8xC	nebo
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Figury	figura	k1gFnSc2	figura
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
počátečním	počáteční	k2eAgNnSc7d1	počáteční
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
:	:	kIx,	:
K	K	kA	K
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
D	D	kA	D
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
V	v	k7c4	v
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
S	s	k7c7	s
střelec	střelec	k1gMnSc1	střelec
a	a	k8xC	a
J	J	kA	J
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pěšců	pěšec	k1gMnPc2	pěšec
se	se	k3xPyFc4	se
však	však	k9	však
označení	označení	k1gNnSc1	označení
nepíše	psát	k5eNaImIp3nS	psát
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
v	v	k7c6	v
komentáři	komentář	k1gInSc6	komentář
či	či	k8xC	či
popisu	popis	k1gInSc6	popis
pozice	pozice	k1gFnSc2	pozice
potřeba	potřeba	k6eAd1	potřeba
označit	označit	k5eAaPmF	označit
pěšce	pěšec	k1gMnPc4	pěšec
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
malé	malý	k2eAgNnSc1d1	malé
písmeno	písmeno	k1gNnSc1	písmeno
p.	p.	k?	p.
Místo	místo	k7c2	místo
počátečních	počáteční	k2eAgNnPc2d1	počáteční
písem	písmo	k1gNnPc2	písmo
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
i	i	k9	i
grafické	grafický	k2eAgInPc4d1	grafický
symboly	symbol	k1gInPc4	symbol
figur	figura	k1gFnPc2	figura
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
braní	braní	k1gNnSc6	braní
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
označení	označení	k1gNnSc4	označení
figury	figura	k1gFnSc2	figura
a	a	k8xC	a
cílové	cílový	k2eAgFnSc2d1	cílová
pole	pole	k1gFnSc2	pole
vkládá	vkládat	k5eAaImIp3nS	vkládat
symbol	symbol	k1gInSc1	symbol
x	x	k?	x
<g/>
;	;	kIx,	;
bere	brát	k5eAaImIp3nS	brát
<g/>
-li	i	k?	-li
pěšec	pěšec	k1gMnSc1	pěšec
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
označení	označení	k1gNnSc2	označení
figury	figura	k1gFnSc2	figura
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
označení	označení	k1gNnSc1	označení
sloupce	sloupec	k1gInSc2	sloupec
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
pěšec	pěšec	k1gMnSc1	pěšec
stál	stát	k5eAaImAgMnS	stát
před	před	k7c7	před
braním	braní	k1gNnSc7	braní
<g/>
.	.	kIx.	.
</s>
<s>
Rošáda	rošáda	k1gFnSc1	rošáda
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
symbolem	symbol	k1gInSc7	symbol
0-0	[number]	k4	0-0
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
malá	malý	k2eAgFnSc1d1	malá
rošáda	rošáda	k1gFnSc1	rošáda
<g/>
,	,	kIx,	,
rošáda	rošáda	k1gFnSc1	rošáda
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
h	h	k?	h
<g/>
1	[number]	k4	1
nebo	nebo	k8xC	nebo
h	h	k?	h
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
0-0-0	[number]	k4	0-0-0
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
velká	velký	k2eAgFnSc1d1	velká
rošáda	rošáda	k1gFnSc1	rošáda
<g/>
,	,	kIx,	,
rošáda	rošáda	k1gFnSc1	rošáda
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
<g/>
1	[number]	k4	1
nebo	nebo	k8xC	nebo
a	a	k8xC	a
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
braní	braní	k1gNnSc6	braní
mimochodem	mimochodem	k6eAd1	mimochodem
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
cílové	cílový	k2eAgNnSc1d1	cílové
pole	pole	k1gNnSc1	pole
použije	použít	k5eAaPmIp3nS	použít
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
pěšec	pěšec	k1gMnSc1	pěšec
skutečně	skutečně	k6eAd1	skutečně
přesunul	přesunout	k5eAaPmAgMnS	přesunout
<g/>
;	;	kIx,	;
za	za	k7c4	za
tah	tah	k1gInSc4	tah
lze	lze	k6eAd1	lze
připsat	připsat	k5eAaPmF	připsat
e.	e.	k?	e.
p.	p.	k?	p.
(	(	kIx(	(
<g/>
en	en	k?	en
passant	passant	k1gInSc1	passant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tah	tah	k1gInSc1	tah
šachuje	šachovat	k5eAaImIp3nS	šachovat
soupeřova	soupeřův	k2eAgMnSc4d1	soupeřův
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
symbol	symbol	k1gInSc4	symbol
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tah	tah	k1gInSc4	tah
matem	mat	k1gInSc7	mat
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
symbol	symbol	k1gInSc1	symbol
++	++	k?	++
nebo	nebo	k8xC	nebo
#	#	kIx~	#
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
pěšce	pěšec	k1gMnSc2	pěšec
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
pole	pole	k1gNnSc4	pole
proměny	proměna	k1gFnSc2	proměna
připíše	připsat	k5eAaPmIp3nS	připsat
zkratka	zkratka	k1gFnSc1	zkratka
figury	figura	k1gFnSc2	figura
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
pěšec	pěšec	k1gMnSc1	pěšec
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
d	d	k?	d
<g/>
8	[number]	k4	8
<g/>
D.	D.	kA	D.
Nabídka	nabídka	k1gFnSc1	nabídka
remízy	remíza	k1gFnSc2	remíza
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
symbolem	symbol	k1gInSc7	symbol
=	=	kIx~	=
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
partie	partie	k1gFnSc2	partie
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
bílý	bílý	k1gMnSc1	bílý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1⁄	1⁄	k?	1⁄
<g/>
–	–	k?	–
<g/>
1⁄	1⁄	k?	1⁄
(	(	kIx(	(
<g/>
remíza	remíza	k1gFnSc1	remíza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
černý	černý	k2eAgMnSc1d1	černý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
*	*	kIx~	*
(	(	kIx(	(
<g/>
neznámý	známý	k2eNgMnSc1d1	neznámý
či	či	k8xC	či
neurčený	určený	k2eNgMnSc1d1	neurčený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
smí	smět	k5eAaImIp3nS	smět
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
komentáře	komentář	k1gInPc1	komentář
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vyjadřované	vyjadřovaný	k2eAgInPc1d1	vyjadřovaný
zavedenými	zavedený	k2eAgFnPc7d1	zavedená
značkami	značka	k1gFnPc7	značka
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgFnPc1d1	používána
značky	značka	k1gFnPc1	značka
<g/>
:	:	kIx,	:
!	!	kIx.	!
</s>
<s>
pro	pro	k7c4	pro
silný	silný	k2eAgInSc4d1	silný
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
!!	!!	k?	!!
pro	pro	k7c4	pro
velice	velice	k6eAd1	velice
silný	silný	k2eAgInSc4d1	silný
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
?	?	kIx.	?
</s>
<s>
pro	pro	k7c4	pro
slabý	slabý	k2eAgInSc4d1	slabý
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
??	??	k?	??
pro	pro	k7c4	pro
hrubou	hrubý	k2eAgFnSc4d1	hrubá
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
?!	?!	k?	?!
pro	pro	k7c4	pro
pochybný	pochybný	k2eAgInSc4d1	pochybný
nebo	nebo	k8xC	nebo
!?	!?	k?	!?
pro	pro	k7c4	pro
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
riskantní	riskantní	k2eAgInSc1d1	riskantní
tah	tah	k1gInSc1	tah
apod.	apod.	kA	apod.
Hráč	hráč	k1gMnSc1	hráč
zaznamenávající	zaznamenávající	k2eAgFnSc4d1	zaznamenávající
svoji	svůj	k3xOyFgFnSc4	svůj
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
partii	partie	k1gFnSc4	partie
ovšem	ovšem	k9	ovšem
nesmí	smět	k5eNaImIp3nS	smět
komentáře	komentář	k1gInPc4	komentář
k	k	k7c3	k
tahům	tah	k1gInPc3	tah
připisovat	připisovat	k5eAaImF	připisovat
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
partie	partie	k1gFnSc2	partie
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
notace	notace	k1gFnSc2	notace
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
pravidel	pravidlo	k1gNnPc2	pravidlo
FIDE	FIDE	kA	FIDE
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
zápisu	zápis	k1gInSc2	zápis
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
léčky	léčka	k1gFnSc2	léčka
zvané	zvaný	k2eAgFnSc2d1	zvaná
ovčácký	ovčácký	k2eAgInSc4d1	ovčácký
mat	mat	k1gInSc4	mat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ukázána	ukázat	k5eAaPmNgFnS	ukázat
na	na	k7c4	na
animaci	animace	k1gFnSc4	animace
vpravo	vpravo	k6eAd1	vpravo
<g/>
:	:	kIx,	:
e	e	k0	e
<g/>
4	[number]	k4	4
e	e	k0	e
<g/>
5	[number]	k4	5
Dh	Dh	k1gFnSc1	Dh
<g/>
5	[number]	k4	5
<g/>
?!	?!	k?	?!
Jc	Jc	k1gFnPc2	Jc
<g/>
6	[number]	k4	6
Sc	Sc	k1gFnPc2	Sc
<g/>
4	[number]	k4	4
Jf	Jf	k1gFnPc2	Jf
<g/>
6	[number]	k4	6
<g/>
??	??	k?	??
Dxf	Dxf	k1gFnSc1	Dxf
<g/>
7	[number]	k4	7
<g/>
#	#	kIx~	#
1-0	[number]	k4	1-0
Šachová	šachový	k2eAgFnSc1d1	šachová
strategie	strategie	k1gFnSc1	strategie
znamená	znamenat	k5eAaImIp3nS	znamenat
stanovování	stanovování	k1gNnSc4	stanovování
a	a	k8xC	a
dosahování	dosahování	k1gNnSc4	dosahování
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
cílů	cíl	k1gInPc2	cíl
během	během	k7c2	během
partie	partie	k1gFnSc1	partie
–	–	k?	–
například	například	k6eAd1	například
jak	jak	k6eAd1	jak
umístit	umístit	k5eAaPmF	umístit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kameny	kámen	k1gInPc4	kámen
–	–	k?	–
zatímco	zatímco	k8xS	zatímco
taktika	taktika	k1gFnSc1	taktika
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
bezprostřední	bezprostřední	k2eAgInPc4d1	bezprostřední
manévry	manévr	k1gInPc4	manévr
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
stránky	stránka	k1gFnPc1	stránka
šachového	šachový	k2eAgNnSc2d1	šachové
myšlení	myšlení	k1gNnSc2	myšlení
však	však	k9	však
nelze	lze	k6eNd1	lze
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
zcela	zcela	k6eAd1	zcela
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
strategických	strategický	k2eAgInPc2d1	strategický
cílů	cíl	k1gInPc2	cíl
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pomocí	pomocí	k7c2	pomocí
taktických	taktický	k2eAgInPc2d1	taktický
manévrů	manévr	k1gInPc2	manévr
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
taktické	taktický	k2eAgFnPc1d1	taktická
příležitosti	příležitost	k1gFnPc1	příležitost
jsou	být	k5eAaImIp3nP	být
připravovány	připravovat	k5eAaImNgFnP	připravovat
předchozí	předchozí	k2eAgFnSc7d1	předchozí
správnou	správný	k2eAgFnSc7d1	správná
strategií	strategie	k1gFnSc7	strategie
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
odlišných	odlišný	k2eAgFnPc2d1	odlišná
strategických	strategický	k2eAgFnPc2d1	strategická
a	a	k8xC	a
taktických	taktický	k2eAgFnPc2d1	taktická
charakteristik	charakteristika	k1gFnPc2	charakteristika
se	se	k3xPyFc4	se
šachová	šachový	k2eAgFnSc1d1	šachová
partie	partie	k1gFnSc1	partie
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
zahájení	zahájení	k1gNnSc1	zahájení
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
prvních	první	k4xOgFnPc2	první
10	[number]	k4	10
až	až	k9	až
25	[number]	k4	25
tahů	tah	k1gInPc2	tah
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgMnSc2	jenž
hráči	hráč	k1gMnPc1	hráč
rozestavují	rozestavovat	k5eAaImIp3nP	rozestavovat
své	svůj	k3xOyFgFnPc4	svůj
armády	armáda	k1gFnPc4	armáda
a	a	k8xC	a
připravují	připravovat	k5eAaImIp3nP	připravovat
scénu	scéna	k1gFnSc4	scéna
následující	následující	k2eAgFnSc2d1	následující
bitvy	bitva	k1gFnSc2	bitva
<g/>
;	;	kIx,	;
střední	střední	k2eAgFnSc1d1	střední
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
fáze	fáze	k1gFnSc1	fáze
partie	partie	k1gFnSc1	partie
<g/>
;	;	kIx,	;
a	a	k8xC	a
koncovka	koncovka	k1gFnSc1	koncovka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
zmizela	zmizet	k5eAaPmAgFnS	zmizet
většina	většina	k1gFnSc1	většina
kamenů	kámen	k1gInPc2	kámen
ze	z	k7c2	z
šachovnice	šachovnice	k1gFnSc2	šachovnice
a	a	k8xC	a
králové	králová	k1gFnSc2	králová
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
do	do	k7c2	do
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Strategie	strategie	k1gFnSc2	strategie
šachu	šach	k1gInSc2	šach
<g/>
.	.	kIx.	.
</s>
<s>
Šachová	šachový	k2eAgFnSc1d1	šachová
strategie	strategie	k1gFnSc1	strategie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
hodnocením	hodnocení	k1gNnSc7	hodnocení
šachových	šachový	k2eAgFnPc2d1	šachová
pozic	pozice	k1gFnPc2	pozice
a	a	k8xC	a
stanovováním	stanovování	k1gNnSc7	stanovování
cílů	cíl	k1gInPc2	cíl
a	a	k8xC	a
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
plánů	plán	k1gInPc2	plán
budoucí	budoucí	k2eAgFnSc2d1	budoucí
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotu	hodnota	k1gFnSc4	hodnota
pozice	pozice	k1gFnSc2	pozice
určuje	určovat	k5eAaImIp3nS	určovat
hodnota	hodnota	k1gFnSc1	hodnota
kamenů	kámen	k1gInPc2	kámen
na	na	k7c6	na
šachovnici	šachovnice	k1gFnSc6	šachovnice
<g/>
,	,	kIx,	,
pěšcová	pěšcový	k2eAgFnSc1d1	pěšcová
struktura	struktura	k1gFnSc1	struktura
pozice	pozice	k1gFnSc1	pozice
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
ovládání	ovládání	k1gNnSc1	ovládání
prostoru	prostor	k1gInSc2	prostor
šachovnice	šachovnice	k1gFnSc2	šachovnice
a	a	k8xC	a
zejména	zejména	k9	zejména
ovládání	ovládání	k1gNnSc1	ovládání
klíčových	klíčový	k2eAgNnPc2d1	klíčové
polí	pole	k1gNnPc2	pole
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
polí	pole	k1gFnPc2	pole
(	(	kIx(	(
<g/>
např.	např.	kA	např.
diagonál	diagonála	k1gFnPc2	diagonála
<g/>
,	,	kIx,	,
otevřených	otevřený	k2eAgInPc2d1	otevřený
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
černých	černý	k2eAgFnPc2d1	černá
či	či	k8xC	či
bílých	bílý	k2eAgFnPc2d1	bílá
polí	pole	k1gFnPc2	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejzákladnějším	základní	k2eAgInSc7d3	nejzákladnější
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
spočítat	spočítat	k5eAaPmF	spočítat
celkovou	celkový	k2eAgFnSc4d1	celková
hodnotu	hodnota	k1gFnSc4	hodnota
kamenů	kámen	k1gInPc2	kámen
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bodové	bodový	k2eAgFnPc1d1	bodová
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
;	;	kIx,	;
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pěšci	pěšec	k1gMnPc1	pěšec
počítají	počítat	k5eAaImIp3nP	počítat
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
bodu	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
střelci	střelec	k1gMnPc1	střelec
a	a	k8xC	a
jezdci	jezdec	k1gMnPc1	jezdec
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
věže	věž	k1gFnPc1	věž
po	po	k7c6	po
pěti	pět	k4xCc6	pět
bodech	bod	k1gInPc6	bod
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
hodnot	hodnota	k1gFnPc2	hodnota
mezi	mezi	k7c7	mezi
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
střelcem	střelec	k1gMnSc7	střelec
či	či	k8xC	či
jezdcem	jezdec	k1gMnSc7	jezdec
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
)	)	kIx)	)
a	a	k8xC	a
dámy	dáma	k1gFnPc1	dáma
kolem	kolem	k7c2	kolem
devíti	devět	k4xCc2	devět
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
krále	král	k1gMnSc2	král
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
neomezená	omezený	k2eNgFnSc1d1	neomezená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gInSc7	jeho
pádem	pád	k1gInSc7	pád
končí	končit	k5eAaImIp3nS	končit
partie	partie	k1gFnSc1	partie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
bojová	bojový	k2eAgFnSc1d1	bojová
hodnota	hodnota	k1gFnSc1	hodnota
v	v	k7c6	v
koncovce	koncovka	k1gFnSc6	koncovka
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
zhruba	zhruba	k6eAd1	zhruba
čtyřem	čtyři	k4xCgInPc3	čtyři
bodům	bod	k1gInPc3	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
základní	základní	k2eAgInPc1d1	základní
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
modifikují	modifikovat	k5eAaBmIp3nP	modifikovat
dalšími	další	k2eAgInPc7d1	další
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
umístění	umístění	k1gNnSc1	umístění
kamenů	kámen	k1gInPc2	kámen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
postoupivší	postoupivší	k2eAgMnPc1d1	postoupivší
pěšci	pěšec	k1gMnPc1	pěšec
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
cennější	cenný	k2eAgMnPc1d2	cennější
než	než	k8xS	než
pěšci	pěšec	k1gMnPc1	pěšec
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
postavení	postavení	k1gNnSc6	postavení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koordinace	koordinace	k1gFnSc1	koordinace
mezi	mezi	k7c7	mezi
kameny	kámen	k1gInPc7	kámen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
dvojice	dvojice	k1gFnSc1	dvojice
střelců	střelec	k1gMnPc2	střelec
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
obvykle	obvykle	k6eAd1	obvykle
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
dvojice	dvojice	k1gFnSc1	dvojice
střelec	střelec	k1gMnSc1	střelec
a	a	k8xC	a
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
)	)	kIx)	)
či	či	k8xC	či
typ	typ	k1gInSc1	typ
pozice	pozice	k1gFnSc2	pozice
(	(	kIx(	(
<g/>
jezdci	jezdec	k1gMnPc1	jezdec
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
lepší	dobrý	k2eAgInSc4d2	lepší
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
pozicích	pozice	k1gFnPc6	pozice
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
pěšci	pěšec	k1gMnPc7	pěšec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
střelci	střelec	k1gMnPc1	střelec
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
pozicích	pozice	k1gFnPc6	pozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
hodnocení	hodnocení	k1gNnSc6	hodnocení
šachové	šachový	k2eAgFnSc2d1	šachová
pozice	pozice	k1gFnSc2	pozice
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
pěšcová	pěšcový	k2eAgFnSc1d1	pěšcová
struktura	struktura	k1gFnSc1	struktura
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zvaná	zvaný	k2eAgFnSc1d1	zvaná
pěšcová	pěšcový	k2eAgFnSc1d1	pěšcová
kostra	kostra	k1gFnSc1	kostra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
konfigurace	konfigurace	k1gFnSc1	konfigurace
pěšců	pěšec	k1gMnPc2	pěšec
na	na	k7c6	na
šachovnici	šachovnice	k1gFnSc6	šachovnice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pěšci	pěšec	k1gMnPc1	pěšec
jsou	být	k5eAaImIp3nP	být
nejméně	málo	k6eAd3	málo
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pěšcová	pěšcový	k2eAgFnSc1d1	pěšcová
struktura	struktura	k1gFnSc1	struktura
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
strategickou	strategický	k2eAgFnSc4d1	strategická
povahu	povaha	k1gFnSc4	povaha
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Slabiny	slabina	k1gFnPc1	slabina
v	v	k7c6	v
pěšcové	pěšcový	k2eAgFnSc6d1	pěšcová
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
izolovaní	izolovaný	k2eAgMnPc1d1	izolovaný
pěšci	pěšec	k1gMnPc1	pěšec
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
pěšců	pěšec	k1gMnPc2	pěšec
vlastní	vlastní	k2eAgFnSc2d1	vlastní
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
sousedních	sousední	k2eAgInPc6d1	sousední
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdvojení	zdvojený	k2eAgMnPc1d1	zdvojený
pěšci	pěšec	k1gMnPc1	pěšec
(	(	kIx(	(
<g/>
dva	dva	k4xCgMnPc1	dva
pěšci	pěšec	k1gMnPc1	pěšec
stejné	stejný	k2eAgFnSc2d1	stejná
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
sloupci	sloupec	k1gInSc6	sloupec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
opoždění	opožděný	k2eAgMnPc1d1	opožděný
pěšci	pěšec	k1gMnPc1	pěšec
(	(	kIx(	(
<g/>
zaostávající	zaostávající	k2eAgMnPc1d1	zaostávající
za	za	k7c7	za
ostatními	ostatní	k2eAgMnPc7d1	ostatní
pěšci	pěšec	k1gMnPc7	pěšec
na	na	k7c6	na
sousedních	sousední	k2eAgInPc6d1	sousední
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
trvalé	trvalý	k2eAgFnSc2d1	trvalá
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc3	jejich
vytvoření	vytvoření	k1gNnSc3	vytvoření
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
kompenzovány	kompenzován	k2eAgInPc1d1	kompenzován
jinou	jiný	k2eAgFnSc7d1	jiná
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
možností	možnost	k1gFnSc7	možnost
zahájit	zahájit	k5eAaPmF	zahájit
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Šachová	šachový	k2eAgFnSc1d1	šachová
taktika	taktika	k1gFnSc1	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Šachová	šachový	k2eAgFnSc1d1	šachová
taktika	taktika	k1gFnSc1	taktika
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
krátkodobé	krátkodobý	k2eAgFnPc4d1	krátkodobá
akce	akce	k1gFnPc4	akce
–	–	k?	–
tak	tak	k9	tak
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
předem	předem	k6eAd1	předem
propočítány	propočítat	k5eAaPmNgInP	propočítat
lidským	lidský	k2eAgInSc7d1	lidský
hráčem	hráč	k1gMnSc7	hráč
nebo	nebo	k8xC	nebo
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
kalkulace	kalkulace	k1gFnSc2	kalkulace
přitom	přitom	k6eAd1	přitom
ovšem	ovšem	k9	ovšem
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hráčových	hráčův	k2eAgFnPc6d1	hráčova
schopnostech	schopnost	k1gFnPc6	schopnost
nebo	nebo	k8xC	nebo
rychlosti	rychlost	k1gFnSc2	rychlost
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
charakteru	charakter	k1gInSc6	charakter
pozice	pozice	k1gFnSc2	pozice
<g/>
:	:	kIx,	:
v	v	k7c6	v
klidných	klidný	k2eAgFnPc6d1	klidná
pozicích	pozice	k1gFnPc6	pozice
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
možnostmi	možnost	k1gFnPc7	možnost
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
není	být	k5eNaImIp3nS	být
hlubší	hluboký	k2eAgFnSc1d2	hlubší
kalkulace	kalkulace	k1gFnSc1	kalkulace
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
"	"	kIx"	"
<g/>
taktické	taktický	k2eAgFnPc4d1	taktická
<g/>
"	"	kIx"	"
pozice	pozice	k1gFnPc4	pozice
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
počtem	počet	k1gInSc7	počet
vynucených	vynucený	k2eAgFnPc2d1	vynucená
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
forsírovaných	forsírovaný	k2eAgFnPc2d1	forsírovaná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
variant	varianta	k1gFnPc2	varianta
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
propočítávat	propočítávat	k5eAaImF	propočítávat
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
posloupnosti	posloupnost	k1gFnPc4	posloupnost
tahů	tah	k1gInPc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Prosté	prostý	k2eAgFnPc1d1	prostá
jednotahové	jednotahový	k2eAgFnPc1d1	jednotahový
či	či	k8xC	či
dvoutahové	dvoutahový	k2eAgFnPc1d1	dvoutahový
taktické	taktický	k2eAgFnPc1d1	taktická
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hrozby	hrozba	k1gFnPc1	hrozba
<g/>
,	,	kIx,	,
výměny	výměna	k1gFnPc1	výměna
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
dvojité	dvojitý	k2eAgInPc4d1	dvojitý
útoky	útok	k1gInPc4	útok
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
kombinovány	kombinovat	k5eAaImNgFnP	kombinovat
do	do	k7c2	do
složitějších	složitý	k2eAgFnPc2d2	složitější
variant	varianta	k1gFnPc2	varianta
<g/>
,	,	kIx,	,
taktických	taktický	k2eAgInPc2d1	taktický
manévrů	manévr	k1gInPc2	manévr
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
či	či	k9wB	či
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
vynucených	vynucený	k2eAgFnPc2d1	vynucená
<g/>
.	.	kIx.	.
</s>
<s>
Teoretici	teoretik	k1gMnPc1	teoretik
popsali	popsat	k5eAaPmAgMnP	popsat
mnoho	mnoho	k4c1	mnoho
elementárních	elementární	k2eAgFnPc2d1	elementární
taktických	taktický	k2eAgFnPc2d1	taktická
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
typických	typický	k2eAgInPc2d1	typický
manévrů	manévr	k1gInPc2	manévr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vazbu	vazba	k1gFnSc4	vazba
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnPc1	omezení
pohybu	pohyb	k1gInSc2	pohyb
soupeřova	soupeřův	k2eAgInSc2d1	soupeřův
kamene	kámen	k1gInSc2	kámen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
odtah	odtah	k1gInSc1	odtah
by	by	kYmCp3nS	by
znamenal	znamenat	k5eAaImAgInS	znamenat
ohrožení	ohrožení	k1gNnSc4	ohrožení
jiného	jiný	k2eAgInSc2d1	jiný
soupeřova	soupeřův	k2eAgInSc2d1	soupeřův
kamene	kámen	k1gInSc2	kámen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vidličky	vidlička	k1gFnPc4	vidlička
(	(	kIx(	(
<g/>
dvojí	dvojit	k5eAaImIp3nS	dvojit
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedna	jeden	k4xCgFnSc1	jeden
figura	figura	k1gFnSc1	figura
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
současně	současně	k6eAd1	současně
dvě	dva	k4xCgFnPc4	dva
hrozby	hrozba	k1gFnPc4	hrozba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nelze	lze	k6eNd1	lze
odvrátit	odvrátit	k5eAaPmF	odvrátit
obě	dva	k4xCgFnPc4	dva
zároveň	zároveň	k6eAd1	zároveň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
(	(	kIx(	(
<g/>
tah	tah	k1gInSc1	tah
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
hráč	hráč	k1gMnSc1	hráč
soupeři	soupeř	k1gMnSc3	soupeř
vědomě	vědomě	k6eAd1	vědomě
nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc1	možnost
získat	získat	k5eAaPmF	získat
materiální	materiální	k2eAgFnSc4d1	materiální
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
chce	chtít	k5eAaImIp3nS	chtít
za	za	k7c4	za
obětovaný	obětovaný	k2eAgInSc4d1	obětovaný
materiál	materiál	k1gInSc4	materiál
získat	získat	k5eAaPmF	získat
výhodu	výhoda	k1gFnSc4	výhoda
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
útok	útok	k1gInSc1	útok
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Vynucená	vynucený	k2eAgFnSc1d1	vynucená
varianta	varianta	k1gFnSc1	varianta
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
obětí	oběť	k1gFnSc7	oběť
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
vyúsťující	vyúsťující	k2eAgInSc1d1	vyúsťující
v	v	k7c4	v
hmatatelný	hmatatelný	k2eAgInSc4d1	hmatatelný
zisk	zisk	k1gInSc4	zisk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
a	a	k8xC	a
překvapivé	překvapivý	k2eAgFnPc1d1	překvapivá
kombinace	kombinace	k1gFnPc1	kombinace
–	–	k?	–
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
z	z	k7c2	z
Nesmrtelné	smrtelný	k2eNgFnSc2d1	nesmrtelná
partie	partie	k1gFnSc2	partie
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
vnímány	vnímat	k5eAaImNgInP	vnímat
jako	jako	k9	jako
krásné	krásný	k2eAgFnPc1d1	krásná
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
obdivu	obdiv	k1gInSc2	obdiv
šachistů	šachista	k1gMnPc2	šachista
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
kombinací	kombinace	k1gFnPc2	kombinace
je	být	k5eAaImIp3nS	být
také	také	k9	také
běžným	běžný	k2eAgInSc7d1	běžný
typem	typ	k1gInSc7	typ
šachové	šachový	k2eAgFnSc2d1	šachová
úlohy	úloha	k1gFnSc2	úloha
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
schopností	schopnost	k1gFnPc2	schopnost
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
zná	znát	k5eAaImIp3nS	znát
pravidla	pravidlo	k1gNnSc2	pravidlo
šachů	šach	k1gMnPc2	šach
odhadem	odhad	k1gInSc7	odhad
605	[number]	k4	605
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
7,5	[number]	k4	7,5
milionů	milion	k4xCgInPc2	milion
je	být	k5eAaImIp3nS	být
členy	člen	k1gInPc4	člen
národních	národní	k2eAgFnPc2d1	národní
šachových	šachový	k2eAgFnPc2d1	šachová
federací	federace	k1gFnPc2	federace
<g/>
,	,	kIx,	,
existujících	existující	k2eAgInPc2d1	existující
ve	v	k7c6	v
160	[number]	k4	160
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
tak	tak	k9	tak
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
sporty	sport	k1gInPc4	sport
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Soudobý	soudobý	k2eAgMnSc1d1	soudobý
šach	šach	k1gMnSc1	šach
je	být	k5eAaImIp3nS	být
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
sport	sport	k1gInSc4	sport
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
i	i	k8xC	i
národními	národní	k2eAgFnPc7d1	národní
soutěžemi	soutěž	k1gFnPc7	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
i	i	k8xC	i
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
amatérskými	amatérský	k2eAgInPc7d1	amatérský
i	i	k8xC	i
profesionálními	profesionální	k2eAgInPc7d1	profesionální
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnSc2	svůj
národní	národní	k2eAgFnSc2d1	národní
šachové	šachový	k2eAgFnSc2d1	šachová
organizace	organizace	k1gFnSc2	organizace
jako	jako	k8xC	jako
například	například	k6eAd1	například
Šachový	šachový	k2eAgInSc1d1	šachový
svaz	svaz	k1gInSc1	svaz
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
šachová	šachový	k2eAgFnSc1d1	šachová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIDE	FIDE	kA	FIDE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
FIDE	FIDE	kA	FIDE
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
oficiální	oficiální	k2eAgNnPc4d1	oficiální
pravidla	pravidlo	k1gNnPc4	pravidlo
šachu	šach	k1gInSc2	šach
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
žebříčky	žebříček	k1gInPc4	žebříček
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
uděluje	udělovat	k5eAaImIp3nS	udělovat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
šachové	šachový	k2eAgInPc4d1	šachový
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
garantuje	garantovat	k5eAaBmIp3nS	garantovat
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
a	a	k8xC	a
šachové	šachový	k2eAgFnSc2d1	šachová
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sestavování	sestavování	k1gNnSc4	sestavování
žebříčků	žebříček	k1gInPc2	žebříček
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
používá	používat	k5eAaImIp3nS	používat
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
rating	rating	k1gInSc4	rating
Elo	Ela	k1gFnSc5	Ela
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Arpad	Arpad	k1gInSc1	Arpad
Elo	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
statistický	statistický	k2eAgInSc4d1	statistický
systém	systém	k1gInSc4	systém
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledek	výsledek	k1gInSc1	výsledek
partie	partie	k1gFnSc2	partie
je	být	k5eAaImIp3nS	být
náhodná	náhodný	k2eAgFnSc1d1	náhodná
veličina	veličina	k1gFnSc1	veličina
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
poměru	poměr	k1gInSc6	poměr
sil	síla	k1gFnPc2	síla
mezi	mezi	k7c7	mezi
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Elo	Ela	k1gFnSc5	Ela
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
partií	partie	k1gFnPc2	partie
v	v	k7c6	v
turnajích	turnaj	k1gInPc6	turnaj
zpětně	zpětně	k6eAd1	zpětně
odhadnout	odhadnout	k5eAaPmF	odhadnout
sílu	síla	k1gFnSc4	síla
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
přijat	přijat	k2eAgInSc1d1	přijat
Šachovou	šachový	k2eAgFnSc7d1	šachová
federací	federace	k1gFnSc7	federace
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
spravedlivější	spravedlivý	k2eAgMnSc1d2	spravedlivější
a	a	k8xC	a
přesnější	přesný	k2eAgInPc1d2	přesnější
než	než	k8xS	než
dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
ho	on	k3xPp3gInSc4	on
začala	začít	k5eAaPmAgFnS	začít
využívat	využívat	k5eAaPmF	využívat
FIDE	FIDE	kA	FIDE
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
sporty	sport	k1gInPc4	sport
než	než	k8xS	než
šachy	šach	k1gInPc4	šach
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
šachistům	šachista	k1gMnPc3	šachista
uděluje	udělovat	k5eAaImIp3nS	udělovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
šachová	šachový	k2eAgFnSc1d1	šachová
federace	federace	k1gFnSc1	federace
doživotní	doživotní	k2eAgFnSc1d1	doživotní
tituly	titul	k1gInPc7	titul
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnějším	prestižní	k2eAgInSc7d3	nejprestižnější
titulem	titul	k1gInSc7	titul
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
velmistr	velmistr	k1gMnSc1	velmistr
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
velmistr	velmistr	k1gMnSc1	velmistr
<g/>
,	,	kIx,	,
VM	VM	kA	VM
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
angličtiny	angličtina	k1gFnSc2	angličtina
IGM	IGM	kA	IGM
či	či	k8xC	či
GM	GM	kA	GM
<g/>
,	,	kIx,	,
International	International	k1gMnSc1	International
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
hráčům	hráč	k1gMnPc3	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
potřebného	potřebné	k1gNnSc2	potřebné
počtu	počet	k1gInSc2	počet
vynikajících	vynikající	k2eAgInPc2d1	vynikající
výkonů	výkon	k1gInPc2	výkon
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
turnajích	turnaj	k1gInPc6	turnaj
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
splnili	splnit	k5eAaPmAgMnP	splnit
normy	norma	k1gFnPc4	norma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
ratingu	rating	k1gInSc2	rating
Elo	Ela	k1gFnSc5	Ela
alespoň	alespoň	k9	alespoň
2500	[number]	k4	2500
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
tituly	titul	k1gInPc7	titul
s	s	k7c7	s
méně	málo	k6eAd2	málo
náročnými	náročný	k2eAgFnPc7d1	náročná
podmínkami	podmínka	k1gFnPc7	podmínka
jsou	být	k5eAaImIp3nP	být
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
FIDE	FIDE	kA	FIDE
a	a	k8xC	a
kandidát	kandidát	k1gMnSc1	kandidát
mistra	mistr	k1gMnSc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
paralelní	paralelní	k2eAgInPc4d1	paralelní
ženské	ženský	k2eAgInPc4d1	ženský
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ženská	ženský	k2eAgFnSc1d1	ženská
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
velmistryně	velmistryně	k1gFnSc1	velmistryně
(	(	kIx(	(
<g/>
WGM	WGM	kA	WGM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc2d1	národní
šachové	šachový	k2eAgFnSc2d1	šachová
federace	federace	k1gFnSc2	federace
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
svým	svůj	k3xOyFgMnPc3	svůj
členům	člen	k1gMnPc3	člen
udělovat	udělovat	k5eAaImF	udělovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
čestné	čestný	k2eAgInPc4d1	čestný
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
výkonnostní	výkonnostní	k2eAgFnPc4d1	výkonnostní
třídy	třída	k1gFnPc4	třída
podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
zvyklostí	zvyklost	k1gFnPc2	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
mistr	mistr	k1gMnSc1	mistr
/	/	kIx~	/
mistryně	mistryně	k1gFnSc1	mistryně
–	–	k?	–
kandidát	kandidát	k1gMnSc1	kandidát
mistra	mistr	k1gMnSc2	mistr
/	/	kIx~	/
kandidátka	kandidátka	k1gFnSc1	kandidátka
mistryně	mistryně	k1gFnSc1	mistryně
–	–	k?	–
I.	I.	kA	I.
výkonnostní	výkonnostní	k2eAgFnSc1d1	výkonnostní
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
VT	VT	kA	VT
<g/>
)	)	kIx)	)
–	–	k?	–
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
VT	VT	kA	VT
–	–	k?	–
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
VT	VT	kA	VT
–	–	k?	–
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
VT	VT	kA	VT
<g/>
.	.	kIx.	.
</s>
<s>
Šachy	šach	k1gInPc1	šach
jsou	být	k5eAaImIp3nP	být
zajímavé	zajímavý	k2eAgMnPc4d1	zajímavý
i	i	k9	i
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
stovky	stovka	k1gFnPc1	stovka
let	léto	k1gNnPc2	léto
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
mnohé	mnohý	k2eAgInPc1d1	mnohý
kombinatorické	kombinatorický	k2eAgInPc1d1	kombinatorický
a	a	k8xC	a
topologické	topologický	k2eAgInPc1d1	topologický
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojené	spojený	k2eAgInPc1d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Zermelo	Zermela	k1gFnSc5	Zermela
je	on	k3xPp3gNnPc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k9	jako
oporu	opora	k1gFnSc4	opora
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
herních	herní	k2eAgFnPc2d1	herní
strategií	strategie	k1gFnPc2	strategie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
předchůdkyň	předchůdkyně	k1gFnPc2	předchůdkyně
moderní	moderní	k2eAgFnSc2d1	moderní
teorie	teorie	k1gFnSc2	teorie
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
přípustných	přípustný	k2eAgFnPc2d1	přípustná
pozic	pozice	k1gFnPc2	pozice
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
mezi	mezi	k7c7	mezi
1043	[number]	k4	1043
a	a	k8xC	a
1050	[number]	k4	1050
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
složitost	složitost	k1gFnSc1	složitost
herního	herní	k2eAgInSc2d1	herní
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
10123	[number]	k4	10123
<g/>
,	,	kIx,	,
hodnota	hodnota	k1gFnSc1	hodnota
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
Shannonovo	Shannonův	k2eAgNnSc1d1	Shannonovo
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typické	typický	k2eAgFnSc6d1	typická
herní	herní	k2eAgFnSc6d1	herní
pozici	pozice	k1gFnSc6	pozice
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
třicet	třicet	k4xCc4	třicet
až	až	k9	až
čtyřicet	čtyřicet	k4xCc4	čtyřicet
možných	možný	k2eAgInPc2d1	možný
tahů	tah	k1gInPc2	tah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nemusí	muset	k5eNaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
žádné	žádný	k3yNgNnSc4	žádný
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
patu	pat	k1gInSc2	pat
nebo	nebo	k8xC	nebo
matu	mást	k5eAaImIp1nS	mást
<g/>
)	)	kIx)	)
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
jich	on	k3xPp3gNnPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
218	[number]	k4	218
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
matematickým	matematický	k2eAgInSc7d1	matematický
problémem	problém	k1gInSc7	problém
šachu	šach	k1gInSc2	šach
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
algoritmus	algoritmus	k1gInSc4	algoritmus
hrající	hrající	k2eAgInPc4d1	hrající
šachy	šach	k1gInPc4	šach
<g/>
.	.	kIx.	.
</s>
<s>
Idea	idea	k1gFnSc1	idea
stroje	stroj	k1gInSc2	stroj
hrajícího	hrající	k2eAgMnSc2d1	hrající
šachy	šach	k1gInPc4	šach
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
už	už	k9	už
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1769	[number]	k4	1769
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
automat	automat	k1gInSc1	automat
hrající	hrající	k2eAgInPc1d1	hrající
šachy	šach	k1gInPc1	šach
zvaný	zvaný	k2eAgInSc1d1	zvaný
Turek	turek	k1gInSc4	turek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
odhalen	odhalit	k5eAaPmNgInS	odhalit
jako	jako	k9	jako
podvod	podvod	k1gInSc1	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Vážné	vážný	k2eAgInPc1d1	vážný
pokusy	pokus	k1gInPc1	pokus
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
automatizační	automatizační	k2eAgFnSc6d1	automatizační
technice	technika	k1gFnSc6	technika
jako	jako	k9	jako
El	Ela	k1gFnPc2	Ela
Ajedrecista	Ajedrecista	k1gMnSc1	Ajedrecista
byly	být	k5eAaImAgInP	být
příliš	příliš	k6eAd1	příliš
složité	složitý	k2eAgInPc1d1	složitý
a	a	k8xC	a
omezené	omezený	k2eAgInPc1d1	omezený
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
užitečné	užitečný	k2eAgFnPc1d1	užitečná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
digitálních	digitální	k2eAgInPc2d1	digitální
počítačů	počítač	k1gInPc2	počítač
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
šachoví	šachový	k2eAgMnPc1d1	šachový
nadšenci	nadšenec	k1gMnPc1	nadšenec
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgInPc4d2	dokonalejší
šachové	šachový	k2eAgInPc4d1	šachový
programy	program	k1gInPc4	program
a	a	k8xC	a
šachové	šachový	k2eAgInPc4d1	šachový
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Průlomový	průlomový	k2eAgInSc1d1	průlomový
článek	článek	k1gInSc1	článek
o	o	k7c6	o
počítačovém	počítačový	k2eAgInSc6d1	počítačový
šachu	šach	k1gInSc6	šach
Programming	Programming	k1gInSc1	Programming
a	a	k8xC	a
Computer	computer	k1gInSc1	computer
for	forum	k1gNnPc2	forum
Playing	Playing	k1gInSc4	Playing
Chess	Chess	k1gInSc1	Chess
(	(	kIx(	(
<g/>
Programování	programování	k1gNnSc1	programování
počítačů	počítač	k1gInPc2	počítač
hrajících	hrající	k2eAgFnPc2d1	hrající
šachy	šach	k1gInPc4	šach
<g/>
)	)	kIx)	)
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Claude	Claud	k1gInSc5	Claud
Shannon	Shannona	k1gFnPc2	Shannona
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgMnSc1d1	ideální
začít	začít	k5eAaPmF	začít
se	se	k3xPyFc4	se
šachovým	šachový	k2eAgInSc7d1	šachový
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
definován	definovat	k5eAaBmNgInS	definovat
jak	jak	k8xS	jak
co	co	k9	co
do	do	k7c2	do
povolených	povolený	k2eAgFnPc2d1	povolená
operací	operace	k1gFnPc2	operace
(	(	kIx(	(
<g/>
tahy	tah	k1gInPc1	tah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
co	co	k3yRnSc4	co
do	do	k7c2	do
konečného	konečný	k2eAgInSc2d1	konečný
cíle	cíl	k1gInSc2	cíl
(	(	kIx(	(
<g/>
mat	mat	k6eAd1	mat
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
tak	tak	k9	tak
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
triviální	triviální	k2eAgMnSc1d1	triviální
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
příliš	příliš	k6eAd1	příliš
složitý	složitý	k2eAgInSc1d1	složitý
pro	pro	k7c4	pro
uspokojivé	uspokojivý	k2eAgNnSc4d1	uspokojivé
řešení	řešení	k1gNnSc4	řešení
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šachy	šach	k1gInPc1	šach
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
pro	pro	k7c4	pro
dobrou	dobrý	k2eAgFnSc4d1	dobrá
hru	hra	k1gFnSc4	hra
"	"	kIx"	"
<g/>
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
řešení	řešení	k1gNnSc1	řešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
nás	my	k3xPp1nPc2	my
buď	buď	k8xC	buď
donutí	donutit	k5eAaPmIp3nP	donutit
připustit	připustit	k5eAaPmF	připustit
existenci	existence	k1gFnSc4	existence
mechanizovaného	mechanizovaný	k2eAgNnSc2d1	mechanizované
myšlení	myšlení	k1gNnSc2	myšlení
anebo	anebo	k8xC	anebo
dále	daleko	k6eAd2	daleko
omezit	omezit	k5eAaPmF	omezit
náš	náš	k3xOp1gInSc4	náš
koncept	koncept	k1gInSc4	koncept
"	"	kIx"	"
<g/>
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
struktura	struktura	k1gFnSc1	struktura
šachu	šach	k1gInSc2	šach
dobře	dobře	k6eAd1	dobře
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
digitální	digitální	k2eAgFnSc3d1	digitální
povaze	povaha	k1gFnSc3	povaha
moderních	moderní	k2eAgInPc2d1	moderní
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
Asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
techniku	technika	k1gFnSc4	technika
(	(	kIx(	(
<g/>
Association	Association	k1gInSc1	Association
for	forum	k1gNnPc2	forum
Computing	Computing	k1gInSc1	Computing
Machinery	Machiner	k1gMnPc4	Machiner
<g/>
,	,	kIx,	,
ACM	ACM	kA	ACM
<g/>
)	)	kIx)	)
pořádala	pořádat	k5eAaImAgFnS	pořádat
v	v	k7c6	v
září	září	k1gNnSc6	září
1970	[number]	k4	1970
první	první	k4xOgInSc4	první
velký	velký	k2eAgInSc4d1	velký
šachový	šachový	k2eAgInSc4d1	šachový
turnaj	turnaj	k1gInSc4	turnaj
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
CHESS	CHESS	kA	CHESS
3.0	[number]	k4	3.0
<g/>
,	,	kIx,	,
šachový	šachový	k2eAgInSc1d1	šachový
program	program	k1gInSc1	program
z	z	k7c2	z
Northwestern	Northwesterna	k1gFnPc2	Northwesterna
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
nejdříve	dříve	k6eAd3	dříve
byly	být	k5eAaImAgInP	být
šachové	šachový	k2eAgInPc1d1	šachový
programy	program	k1gInPc1	program
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
pouhé	pouhý	k2eAgFnPc4d1	pouhá
kuriozity	kuriozita	k1gFnPc4	kuriozita
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Rybka	rybka	k1gFnSc1	rybka
nebo	nebo	k8xC	nebo
Hydra	hydra	k1gFnSc1	hydra
<g/>
,	,	kIx,	,
mimořádně	mimořádně	k6eAd1	mimořádně
silnými	silný	k2eAgMnPc7d1	silný
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prohrál	prohrát	k5eAaPmAgMnS	prohrát
Garri	Garr	k1gMnSc3	Garr
Kasparov	Kasparov	k1gInSc4	Kasparov
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
první	první	k4xOgMnSc1	první
hráč	hráč	k1gMnSc1	hráč
světového	světový	k2eAgInSc2d1	světový
šachového	šachový	k2eAgInSc2d1	šachový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
proti	proti	k7c3	proti
šachovému	šachový	k2eAgInSc3d1	šachový
počítači	počítač	k1gInSc3	počítač
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
neformálně	formálně	k6eNd1	formálně
nazývanému	nazývaný	k2eAgMnSc3d1	nazývaný
Deep	Deep	k1gInSc4	Deep
Blue	Blue	k1gNnSc3	Blue
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teorie	teorie	k1gFnSc2	teorie
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
šachové	šachový	k2eAgInPc1d1	šachový
programy	program	k1gInPc1	program
relativně	relativně	k6eAd1	relativně
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
:	:	kIx,	:
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
prozkoumávají	prozkoumávat	k5eAaImIp3nP	prozkoumávat
obrovské	obrovský	k2eAgInPc4d1	obrovský
počty	počet	k1gInPc4	počet
potenciálních	potenciální	k2eAgInPc2d1	potenciální
budoucích	budoucí	k2eAgInPc2d1	budoucí
tahů	tah	k1gInPc2	tah
obou	dva	k4xCgNnPc2	dva
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
na	na	k7c4	na
vznikající	vznikající	k2eAgFnPc4d1	vznikající
pozice	pozice	k1gFnPc4	pozice
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
ohodnocovací	ohodnocovací	k2eAgFnSc4d1	ohodnocovací
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
algoritmu	algoritmus	k1gInSc2	algoritmus
minimax	minimax	k1gInSc1	minimax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc4	počítač
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
databázemi	databáze	k1gFnPc7	databáze
mistrovských	mistrovský	k2eAgFnPc2d1	mistrovská
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
analytickou	analytický	k2eAgFnSc7d1	analytická
schopností	schopnost	k1gFnSc7	schopnost
dnes	dnes	k6eAd1	dnes
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
lidským	lidský	k2eAgMnPc3d1	lidský
hráčům	hráč	k1gMnPc3	hráč
se	se	k3xPyFc4	se
šachy	šach	k1gInPc7	šach
učit	učit	k5eAaImF	učit
a	a	k8xC	a
připravovat	připravovat	k5eAaImF	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Internetové	internetový	k2eAgInPc1d1	internetový
šachové	šachový	k2eAgInPc1d1	šachový
servery	server	k1gInPc1	server
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lidem	člověk	k1gMnPc3	člověk
nacházet	nacházet	k5eAaImF	nacházet
partnery	partner	k1gMnPc4	partner
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
počítačů	počítač	k1gMnPc2	počítač
a	a	k8xC	a
moderních	moderní	k2eAgInPc2d1	moderní
komunikačních	komunikační	k2eAgInPc2d1	komunikační
prostředků	prostředek	k1gInPc2	prostředek
také	také	k9	také
nastolila	nastolit	k5eAaPmAgFnS	nastolit
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
podvádění	podvádění	k1gNnSc2	podvádění
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
analogické	analogický	k2eAgInPc1d1	analogický
dopingu	doping	k1gInSc3	doping
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
byla	být	k5eAaImAgFnS	být
kontroverze	kontroverze	k1gFnSc1	kontroverze
během	během	k7c2	během
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
vysloveno	vysloven	k2eAgNnSc4d1	vysloveno
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
používá	používat	k5eAaImIp3nS	používat
nedovolenou	dovolený	k2eNgFnSc4d1	nedovolená
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
podporu	podpora	k1gFnSc4	podpora
na	na	k7c6	na
toaletě	toaleta	k1gFnSc6	toaleta
<g/>
.	.	kIx.	.
</s>
