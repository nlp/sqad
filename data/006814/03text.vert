<s>
Nokia	Nokia	kA	Nokia
(	(	kIx(	(
<g/>
NYSE	NYSE	kA	NYSE
<g/>
:	:	kIx,	:
NOK	NOK	kA	NOK
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nadnárodní	nadnárodní	k2eAgFnSc1d1	nadnárodní
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
finském	finský	k2eAgNnSc6d1	finské
Espoo	Espoo	k1gNnSc1	Espoo
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
Solutions	Solutions	k1gInSc1	Solutions
and	and	k?	and
Networks	Networks	k1gInSc1	Networks
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
producentem	producent	k1gMnSc7	producent
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
pro	pro	k7c4	pro
telekomunikační	telekomunikační	k2eAgFnPc4d1	telekomunikační
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
přes	přes	k7c4	přes
130	[number]	k4	130
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ve	v	k7c6	v
120	[number]	k4	120
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
hrála	hrát	k5eAaImAgFnS	hrát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byla	být	k5eAaImAgFnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgFnSc7d3	veliký
finskou	finský	k2eAgFnSc7d1	finská
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
Nokie	Nokie	k1gFnSc2	Nokie
na	na	k7c6	na
finském	finský	k2eAgNnSc6d1	finské
HDP	HDP	kA	HDP
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
až	až	k9	až
4	[number]	k4	4
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
klesl	klesnout	k5eAaPmAgInS	klesnout
pod	pod	k7c4	pod
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
společnost	společnost	k1gFnSc1	společnost
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
skoro	skoro	k6eAd1	skoro
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
finského	finský	k2eAgInSc2d1	finský
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
obrat	obrat	k5eAaPmF	obrat
vyšší	vysoký	k2eAgInSc4d2	vyšší
než	než	k8xS	než
celý	celý	k2eAgInSc4d1	celý
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnPc1	akcie
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
kótovány	kótovat	k5eAaBmNgFnP	kótovat
na	na	k7c6	na
burzách	burza	k1gFnPc6	burza
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
New	New	k1gFnSc3	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
46	[number]	k4	46
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
drží	držet	k5eAaImIp3nP	držet
investoři	investor	k1gMnPc1	investor
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
ani	ani	k8xC	ani
nepřímo	přímo	k6eNd1	přímo
kontrolována	kontrolovat	k5eAaImNgFnS	kontrolovat
jinou	jiný	k2eAgFnSc7d1	jiná
společností	společnost	k1gFnSc7	společnost
či	či	k8xC	či
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgMnSc7d3	veliký
akcionářem	akcionář	k1gMnSc7	akcionář
banka	banka	k1gFnSc1	banka
Morgan	morgan	k1gInSc4	morgan
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
,	,	kIx,	,
když	když	k8xS	když
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
8,3	[number]	k4	8,3
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
výrobcem	výrobce	k1gMnSc7	výrobce
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázala	dokázat	k5eNaPmAgFnS	dokázat
však	však	k9	však
zareagovat	zareagovat	k5eAaPmF	zareagovat
na	na	k7c4	na
nástup	nástup	k1gInSc4	nástup
nových	nový	k2eAgInPc2d1	nový
chytrých	chytrý	k2eAgInPc2d1	chytrý
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
světového	světový	k2eAgMnSc2d1	světový
výrobce	výrobce	k1gMnSc2	výrobce
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
postupně	postupně	k6eAd1	postupně
zcela	zcela	k6eAd1	zcela
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
firmy	firma	k1gFnSc2	firma
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vysoký	vysoký	k2eAgMnSc1d1	vysoký
představitel	představitel	k1gMnSc1	představitel
Microsoftu	Microsoft	k1gInSc2	Microsoft
Stephen	Stephen	k2eAgInSc4d1	Stephen
Elop	Elop	k1gInSc4	Elop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mobilní	mobilní	k2eAgInSc1d1	mobilní
divizi	divize	k1gFnSc3	divize
firmy	firma	k1gFnSc2	firma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
odprodal	odprodat	k5eAaPmAgMnS	odprodat
Microsoftu	Microsofta	k1gFnSc4	Microsofta
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
však	však	k9	však
provedla	provést	k5eAaPmAgFnS	provést
restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
telefonů	telefon	k1gInPc2	telefon
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Nokia	Nokia	kA	Nokia
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
,	,	kIx,	,
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
se	se	k3xPyFc4	se
však	však	k9	však
stará	starat	k5eAaImIp3nS	starat
společnost	společnost	k1gFnSc4	společnost
HMD	HMD	kA	HMD
Global	globat	k5eAaImAgMnS	globat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
licence	licence	k1gFnSc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
Fredrikem	Fredrik	k1gMnSc7	Fredrik
Idestamem	Idestam	k1gInSc7	Idestam
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nokia	Nokia	kA	Nokia
nedaleko	nedaleko	k7c2	nedaleko
Tampere	Tamper	k1gInSc5	Tamper
jako	jako	k9	jako
papírna	papírna	k1gFnSc1	papírna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
gumárenské	gumárenský	k2eAgInPc4d1	gumárenský
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
výrobu	výroba	k1gFnSc4	výroba
o	o	k7c4	o
telefonní	telefonní	k2eAgInPc4d1	telefonní
a	a	k8xC	a
telegrafní	telegrafní	k2eAgInPc4d1	telegrafní
kabely	kabel	k1gInPc4	kabel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
pronikla	proniknout	k5eAaPmAgFnS	proniknout
na	na	k7c4	na
trhy	trh	k1gInPc4	trh
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Své	své	k1gNnSc1	své
gumárenské	gumárenský	k2eAgFnSc2d1	gumárenská
divize	divize	k1gFnSc2	divize
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
značky	značka	k1gFnSc2	značka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
řeky	řeka	k1gFnSc2	řeka
Nokianvirta	Nokianvirt	k1gMnSc2	Nokianvirt
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
břehu	břeh	k1gInSc6	břeh
postavil	postavit	k5eAaPmAgMnS	postavit
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
zakladatel	zakladatel	k1gMnSc1	zakladatel
Fredrik	Fredrik	k1gMnSc1	Fredrik
Idestam	Idestam	k1gInSc4	Idestam
mlýn	mlýn	k1gInSc4	mlýn
na	na	k7c4	na
dřevní	dřevní	k2eAgFnSc4d1	dřevní
buničinu	buničina	k1gFnSc4	buničina
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
Nokia	Nokia	kA	Nokia
strategické	strategický	k2eAgNnSc1d1	strategické
partnerství	partnerství	k1gNnSc1	partnerství
s	s	k7c7	s
Microsoftem	Microsoft	k1gInSc7	Microsoft
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kterého	který	k3yRgInSc2	který
dostaly	dostat	k5eAaPmAgInP	dostat
všechny	všechen	k3xTgMnPc4	všechen
její	její	k3xOp3gMnPc4	její
smartphony	smartphon	k1gMnPc4	smartphon
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Nokia	Nokia	kA	Nokia
ukončila	ukončit	k5eAaPmAgFnS	ukončit
vývoj	vývoj	k1gInSc4	vývoj
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Symbian	Symbiana	k1gFnPc2	Symbiana
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
partnerství	partnerství	k1gNnSc1	partnerství
s	s	k7c7	s
Microsoftem	Microsoft	k1gInSc7	Microsoft
firmě	firma	k1gFnSc3	firma
příliš	příliš	k6eAd1	příliš
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
Nokia	Nokia	kA	Nokia
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
nucena	nucen	k2eAgFnSc1d1	nucena
prodat	prodat	k5eAaPmF	prodat
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Espoo	Espoo	k1gNnSc4	Espoo
za	za	k7c4	za
170	[number]	k4	170
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Nokia	Nokia	kA	Nokia
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgFnPc4d3	veliký
ztráty	ztráta	k1gFnPc4	ztráta
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nespokojenosti	nespokojenost	k1gFnPc1	nespokojenost
některých	některý	k3yIgMnPc2	některý
jejích	její	k3xOp3gMnPc2	její
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Nokie	Nokie	k1gFnSc2	Nokie
založena	založen	k2eAgFnSc1d1	založena
nová	nový	k2eAgFnSc1d1	nová
společnost	společnost	k1gFnSc1	společnost
Jolla	Jolla	k1gFnSc1	Jolla
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
Nokia	Nokia	kA	Nokia
prodala	prodat	k5eAaPmAgFnS	prodat
500	[number]	k4	500
patentů	patent	k1gInPc2	patent
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
22	[number]	k4	22
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
Vringo	Vringo	k1gNnSc1	Vringo
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
vynesou	vynést	k5eAaPmIp3nP	vynést
patenty	patent	k1gInPc1	patent
vyšší	vysoký	k2eAgFnSc4d2	vyšší
částku	částka	k1gFnSc4	částka
než	než	k8xS	než
za	za	k7c4	za
jakou	jaký	k3yIgFnSc4	jaký
je	být	k5eAaImIp3nS	být
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
Nokia	Nokia	kA	Nokia
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
prodání	prodání	k1gNnSc6	prodání
500	[number]	k4	500
patentů	patent	k1gInPc2	patent
si	se	k3xPyFc3	se
Nokia	Nokia	kA	Nokia
drží	držet	k5eAaImIp3nS	držet
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1	mobilní
divize	divize	k1gFnSc1	divize
Nokie	Nokie	k1gFnSc2	Nokie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
odkoupena	odkoupen	k2eAgFnSc1d1	odkoupena
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
za	za	k7c4	za
7	[number]	k4	7
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mobilní	mobilní	k2eAgFnSc7d1	mobilní
divizí	divize	k1gFnSc7	divize
a	a	k8xC	a
firemními	firemní	k2eAgFnPc7d1	firemní
službami	služba	k1gFnPc7	služba
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
licence	licence	k1gFnPc4	licence
na	na	k7c4	na
mobilní	mobilní	k2eAgInPc4d1	mobilní
patenty	patent	k1gInPc4	patent
Nokie	Nokie	k1gFnSc2	Nokie
<g/>
,	,	kIx,	,
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
ale	ale	k8xC	ale
nezískal	získat	k5eNaPmAgInS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Nokii	Nokie	k1gFnSc4	Nokie
tak	tak	k6eAd1	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
divize	divize	k1gFnSc1	divize
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
sítěmi	síť	k1gFnPc7	síť
a	a	k8xC	a
mapy	mapa	k1gFnPc1	mapa
Nokia	Nokia	kA	Nokia
HERE	HERE	kA	HERE
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
Nokia	Nokia	kA	Nokia
představila	představit	k5eAaPmAgFnS	představit
nový	nový	k2eAgInSc4d1	nový
tablet	tablet	k1gInSc4	tablet
s	s	k7c7	s
OS	OS	kA	OS
Windows	Windows	kA	Windows
RT	RT	kA	RT
–	–	k?	–
Lumia	Lumia	k1gFnSc1	Lumia
2520	[number]	k4	2520
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
šestipalcovým	šestipalcový	k2eAgInSc7d1	šestipalcový
Full	Full	k1gInSc1	Full
HD	HD	kA	HD
displejem	displej	k1gInSc7	displej
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
s	s	k7c7	s
aktualizacé	aktualizacé	k1gNnSc7	aktualizacé
GDR3	GDR3	k1gFnSc2	GDR3
–	–	k?	–
Lumii	Lumie	k1gFnSc4	Lumie
1320	[number]	k4	1320
a	a	k8xC	a
Lumii	Lumie	k1gFnSc4	Lumie
1520	[number]	k4	1520
Společnost	společnost	k1gFnSc1	společnost
Nokia	Nokia	kA	Nokia
se	se	k3xPyFc4	se
po	po	k7c6	po
odkoupení	odkoupení	k1gNnSc6	odkoupení
Microsoftem	Microsoft	k1gInSc7	Microsoft
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
odkoupená	odkoupený	k2eAgFnSc1d1	odkoupená
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
Corporation	Corporation	k1gInSc1	Corporation
za	za	k7c4	za
7,2	[number]	k4	7,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Nokia	Nokia	kA	Nokia
byla	být	k5eAaImAgFnS	být
přesněji	přesně	k6eAd2	přesně
odkoupena	odkoupit	k5eAaPmNgFnS	odkoupit
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
Microsoftu	Microsoft	k1gInSc2	Microsoft
-	-	kIx~	-
Microsoft	Microsoft	kA	Microsoft
Mobile	mobile	k1gNnSc1	mobile
Oy	Oy	k1gMnSc1	Oy
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tímto	tento	k3xDgInSc7	tento
přebírá	přebírat	k5eAaImIp3nS	přebírat
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
osobní	osobní	k2eAgInPc4d1	osobní
údaje	údaj	k1gInPc4	údaj
této	tento	k3xDgFnSc2	tento
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
tímto	tento	k3xDgInSc7	tento
nákupem	nákup	k1gInSc7	nákup
získal	získat	k5eAaPmAgMnS	získat
všechny	všechen	k3xTgInPc4	všechen
patenty	patent	k1gInPc4	patent
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovšem	ovšem	k9	ovšem
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Nokia	Nokia	kA	Nokia
Solutions	Solutions	k1gInSc1	Solutions
&	&	k?	&
Networks	Networks	k1gInSc1	Networks
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
nákupem	nákup	k1gInSc7	nákup
Microsoft	Microsoft	kA	Microsoft
dále	daleko	k6eAd2	daleko
získal	získat	k5eAaPmAgMnS	získat
mobilní	mobilní	k2eAgFnSc4d1	mobilní
divizi	divize	k1gFnSc4	divize
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
-	-	kIx~	-
její	její	k3xOp3gFnPc4	její
produktové	produktový	k2eAgFnPc4d1	produktová
řady	řada	k1gFnPc4	řada
Lumia	Lumium	k1gNnSc2	Lumium
<g/>
,	,	kIx,	,
Asha	Ashum	k1gNnSc2	Ashum
<g/>
,	,	kIx,	,
X	X	kA	X
a	a	k8xC	a
klasické	klasický	k2eAgInPc1d1	klasický
hloupé	hloupý	k2eAgInPc1d1	hloupý
telefony	telefon	k1gInPc1	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Microsoft	Microsoft	kA	Microsoft
napřed	napřed	k6eAd1	napřed
zrušila	zrušit	k5eAaPmAgFnS	zrušit
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
odkoupené	odkoupený	k2eAgFnPc4d1	odkoupená
produktové	produktový	k2eAgFnPc4d1	produktová
řady	řada	k1gFnPc4	řada
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
řady	řada	k1gFnSc2	řada
Lumia	Lumium	k1gNnSc2	Lumium
(	(	kIx(	(
<g/>
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
jen	jen	k9	jen
na	na	k7c6	na
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
<g/>
)	)	kIx)	)
a	a	k8xC	a
název	název	k1gInSc1	název
Nokia	Nokia	kA	Nokia
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
telefonů	telefon	k1gInPc2	telefon
úplně	úplně	k6eAd1	úplně
zmizet	zmizet	k5eAaPmF	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Microsoft	Microsoft	kA	Microsoft
sám	sám	k3xTgMnSc1	sám
jednal	jednat	k5eAaImAgMnS	jednat
o	o	k7c6	o
delším	dlouhý	k2eAgInSc6d2	delší
pronájmu	pronájem	k1gInSc6	pronájem
názvu	název	k1gInSc2	název
Nokia	Nokia	kA	Nokia
a	a	k8xC	a
přemýšlel	přemýšlet	k5eAaImAgInS	přemýšlet
nad	nad	k7c7	nad
vlastním	vlastní	k2eAgInSc7d1	vlastní
názvem	název	k1gInSc7	název
-	-	kIx~	-
NOKIA	Nokia	kA	Nokia
by	by	kYmCp3nS	by
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
koupi	koupě	k1gFnSc6	koupě
mobilní	mobilní	k2eAgFnSc2d1	mobilní
divize	divize	k1gFnSc2	divize
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
oznámil	oznámit	k5eAaPmAgMnS	oznámit
zrušení	zrušení	k1gNnSc4	zrušení
řady	řada	k1gFnSc2	řada
X	X	kA	X
s	s	k7c7	s
Androidem	android	k1gInSc7	android
<g/>
,	,	kIx,	,
řady	řada	k1gFnPc1	řada
Asha	Ash	k1gInSc2	Ash
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
klasických	klasický	k2eAgInPc2d1	klasický
hloupých	hloupý	k2eAgInPc2d1	hloupý
telefonů	telefon	k1gInPc2	telefon
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
též	též	k9	též
ukončen	ukončen	k2eAgMnSc1d1	ukončen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
po	po	k7c4	po
oznámení	oznámení	k1gNnSc4	oznámení
ukončení	ukončení	k1gNnSc1	ukončení
vývoje	vývoj	k1gInSc2	vývoj
klasických	klasický	k2eAgInPc2d1	klasický
hloupých	hloupý	k2eAgInPc2d1	hloupý
telefonů	telefon	k1gInPc2	telefon
Microsoft	Microsoft	kA	Microsoft
přece	přece	k9	přece
jenom	jenom	k9	jenom
bere	brát	k5eAaImIp3nS	brát
své	svůj	k3xOyFgInPc4	svůj
kroky	krok	k1gInPc4	krok
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
klasické	klasický	k2eAgInPc4d1	klasický
hloupé	hloupý	k2eAgInPc4d1	hloupý
telefony	telefon	k1gInPc4	telefon
nepřestává	přestávat	k5eNaImIp3nS	přestávat
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Microsoft	Microsoft	kA	Microsoft
prodal	prodat	k5eAaPmAgMnS	prodat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
název	název	k1gInSc4	název
Nokia	Nokia	kA	Nokia
společnosti	společnost	k1gFnSc2	společnost
HMD	HMD	kA	HMD
Global	globat	k5eAaImAgMnS	globat
oy	oy	k?	oy
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představila	představit	k5eAaPmAgFnS	představit
telefon	telefon	k1gInSc4	telefon
Nokia	Nokia	kA	Nokia
3310	[number]	k4	3310
a	a	k8xC	a
smartfony	smartfon	k1gInPc1	smartfon
Nokia	Nokia	kA	Nokia
3,5	[number]	k4	3,5
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
Nokie	Nokie	k1gFnSc2	Nokie
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
telekomunikačním	telekomunikační	k2eAgNnSc7d1	telekomunikační
sítím	sítí	k1gNnSc7	sítí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nyní	nyní	k6eAd1	nyní
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Huawei	Huawe	k1gFnSc2	Huawe
<g/>
,	,	kIx,	,
nejrychlejší	rychlý	k2eAgFnSc2d3	nejrychlejší
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
sítě	síť	k1gFnSc2	síť
společnosti	společnost	k1gFnSc2	společnost
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
sítě	síť	k1gFnSc2	síť
dodává	dodávat	k5eAaImIp3nS	dodávat
Huawei	Huawei	k1gNnSc1	Huawei
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
potom	potom	k8xC	potom
Nokia	Nokia	kA	Nokia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnPc1	část
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
mapové	mapový	k2eAgInPc4d1	mapový
podklady	podklad	k1gInPc4	podklad
HERE	HERE	kA	HERE
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
bezplatně	bezplatně	k6eAd1	bezplatně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
i	i	k9	i
exklusivně	exklusivně	k6eAd1	exklusivně
na	na	k7c6	na
smartphonech	smartphon	k1gInPc6	smartphon
Galaxy	Galax	k1gInPc1	Galax
od	od	k7c2	od
Samsungu	Samsung	k1gInSc2	Samsung
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
zůstal	zůstat	k5eAaPmAgMnS	zůstat
této	tento	k3xDgFnSc3	tento
části	část	k1gFnSc3	část
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Nokia	Nokia	kA	Nokia
Cinemagraph	Cinemagrapha	k1gFnPc2	Cinemagrapha
<g/>
,	,	kIx,	,
Refocus	Refocus	k1gInSc1	Refocus
aj.	aj.	kA	aj.
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vývojářem	vývojář	k1gMnSc7	vývojář
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
Nokia	Nokia	kA	Nokia
Collection	Collection	k1gInSc1	Collection
ve	v	k7c6	v
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
stal	stálit	k5eAaImRp2nS	stálit
Microsoft	Microsoft	kA	Microsoft
Mobile	mobile	k1gNnSc7	mobile
<g/>
.	.	kIx.	.
</s>
<s>
Björn	Björn	k1gMnSc1	Björn
Westerlund	Westerlund	k1gMnSc1	Westerlund
<g/>
:	:	kIx,	:
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
Kari	kari	k1gNnSc2	kari
Kairamo	Kairama	k1gFnSc5	Kairama
<g/>
:	:	kIx,	:
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
Simo	sima	k1gFnSc5	sima
Vuorilehto	Vuorilehto	k1gNnSc4	Vuorilehto
<g/>
:	:	kIx,	:
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
Jorma	Jorm	k1gMnSc2	Jorm
Ollila	Ollil	k1gMnSc2	Ollil
<g/>
:	:	kIx,	:
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
Olli-Pekka	Olli-Pekka	k1gMnSc1	Olli-Pekka
Kallasvuo	Kallasvuo	k1gMnSc1	Kallasvuo
<g/>
:	:	kIx,	:
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
Stephen	Stephen	k2eAgInSc1d1	Stephen
Elop	Elop	k1gInSc1	Elop
<g/>
:	:	kIx,	:
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
Rajeev	Rajeev	k1gFnSc1	Rajeev
Suri	Sur	k1gFnSc2	Sur
<g/>
:	:	kIx,	:
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
Telefony	telefon	k1gInPc1	telefon
Nokia	Nokia	kA	Nokia
vydané	vydaný	k2eAgInPc4d1	vydaný
před	před	k7c7	před
prosincem	prosinec	k1gInSc7	prosinec
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
výrobu	výrob	k1gInSc6	výrob
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgNnP	starat
buď	buď	k8xC	buď
samotná	samotný	k2eAgFnSc1d1	samotná
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgFnP	označovat
buď	buď	k8xC	buď
čtyřčíselným	čtyřčíselný	k2eAgInSc7d1	čtyřčíselný
kódem	kód	k1gInSc7	kód
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kombinací	kombinace	k1gFnSc7	kombinace
písmene	písmeno	k1gNnSc2	písmeno
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
Lumia	Lumia	k1gFnSc1	Lumia
měla	mít	k5eAaImAgFnS	mít
číselný	číselný	k2eAgInSc4d1	číselný
kód	kód	k1gInSc4	kód
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
tříčíselný	tříčíselný	k2eAgMnSc1d1	tříčíselný
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
telefony	telefon	k1gInPc1	telefon
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
HMD	HMD	kA	HMD
Global	globat	k5eAaImAgMnS	globat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
telefony	telefon	k1gInPc1	telefon
produkované	produkovaný	k2eAgInPc1d1	produkovaný
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
tříčíselným	tříčíselný	k2eAgInSc7d1	tříčíselný
kódem	kód	k1gInSc7	kód
<g/>
,	,	kIx,	,
chytré	chytrý	k2eAgInPc4d1	chytrý
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
Androidem	android	k1gInSc7	android
zatím	zatím	k6eAd1	zatím
nemají	mít	k5eNaImIp3nP	mít
stanovený	stanovený	k2eAgInSc4d1	stanovený
vzorec	vzorec	k1gInSc4	vzorec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
budou	být	k5eAaImBp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
názvy	název	k1gInPc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Basic	Basic	kA	Basic
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
základní	základní	k2eAgFnPc4d1	základní
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
monochromatický	monochromatický	k2eAgInSc1d1	monochromatický
displej	displej	k1gInSc1	displej
Basic	Basic	kA	Basic
expression	expression	k1gInSc1	expression
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
základní	základní	k2eAgInPc4d1	základní
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
designové	designový	k2eAgInPc4d1	designový
Expression	Expression	k1gInSc4	Expression
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
stylové	stylový	k2eAgInPc4d1	stylový
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
funkčně	funkčně	k6eAd1	funkčně
bývají	bývat	k5eAaImIp3nP	bývat
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
málokdy	málokdy	k6eAd1	málokdy
Active	Actiev	k1gFnSc2	Actiev
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
odolnější	odolný	k2eAgInPc1d2	odolnější
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
voděodolné	voděodolný	k2eAgInPc1d1	voděodolný
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	s	k7c7	s
speciálními	speciální	k2eAgFnPc7d1	speciální
funkcemi	funkce	k1gFnPc7	funkce
Classic	Classic	k1gMnSc1	Classic
businnes	businnes	k1gMnSc1	businnes
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
manažerské	manažerský	k2eAgInPc4d1	manažerský
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
funkcemi	funkce	k1gFnPc7	funkce
Fashion	Fashion	k1gInSc1	Fashion
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
telefony	telefon	k1gInPc1	telefon
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
s	s	k7c7	s
módním	módní	k2eAgInSc7d1	módní
vzhledem	vzhled	k1gInSc7	vzhled
Premium	Premium	k1gNnSc4	Premium
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
8	[number]	k4	8
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
luxusní	luxusní	k2eAgInPc4d1	luxusní
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
v	v	k7c6	v
limitovaných	limitovaný	k2eAgFnPc6d1	limitovaná
edicích	edice	k1gFnPc6	edice
Bussines	Bussinesa	k1gFnPc2	Bussinesa
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
xxx	xxx	k?	xxx
<g/>
)	)	kIx)	)
–	–	k?	–
komunikátory	komunikátor	k1gInPc1	komunikátor
plně	plně	k6eAd1	plně
nabité	nabitý	k2eAgFnPc1d1	nabitá
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
alfanumerickou	alfanumerický	k2eAgFnSc7d1	alfanumerická
klávesnicí	klávesnice	k1gFnSc7	klávesnice
C-Series	C-Series	k1gMnSc1	C-Series
(	(	kIx(	(
<g/>
Cxx	Cxx	k1gMnSc1	Cxx
<g/>
)	)	kIx)	)
–	–	k?	–
třída	třída	k1gFnSc1	třída
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k8xS	jako
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
<g />
.	.	kIx.	.
</s>
<s>
kompromis	kompromis	k1gInSc1	kompromis
mezi	mezi	k7c7	mezi
sériemi	série	k1gFnPc7	série
"	"	kIx"	"
<g/>
E	E	kA	E
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
"	"	kIx"	"
E-Series	E-Series	k1gMnSc1	E-Series
(	(	kIx(	(
<g/>
Exx	Exx	k1gMnSc1	Exx
<g/>
)	)	kIx)	)
–	–	k?	–
manažerské	manažerský	k2eAgInPc4d1	manažerský
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
smartphony	smartphon	k1gInPc4	smartphon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
alfanumerickou	alfanumerický	k2eAgFnSc7d1	alfanumerická
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgInPc4d1	vybavený
komunikačními	komunikační	k2eAgFnPc7d1	komunikační
bezdrátovými	bezdrátový	k2eAgFnPc7d1	bezdrátová
technologiemi	technologie	k1gFnPc7	technologie
N-Series	N-Seriesa	k1gFnPc2	N-Seriesa
(	(	kIx(	(
<g/>
Nxx	Nxx	k1gFnSc1	Nxx
<g/>
)	)	kIx)	)
–	–	k?	–
multimediální	multimediální	k2eAgInPc1d1	multimediální
telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
dobrými	dobrý	k2eAgInPc7d1	dobrý
fotoaparáty	fotoaparát	k1gInPc7	fotoaparát
a	a	k8xC	a
multimediálními	multimediální	k2eAgFnPc7d1	multimediální
funkcemi	funkce	k1gFnPc7	funkce
X-Series	X-Seriesa	k1gFnPc2	X-Seriesa
(	(	kIx(	(
<g/>
Xxx	Xxx	k1gFnSc1	Xxx
<g/>
)	)	kIx)	)
–	–	k?	–
hudební	hudební	k2eAgInPc1d1	hudební
telefony	telefon	k1gInPc1	telefon
XpresMusic	XpresMusice	k1gInPc2	XpresMusice
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nokia	Nokia	kA	Nokia
Lumia	Lumium	k1gNnSc2	Lumium
<g/>
.	.	kIx.	.
</s>
<s>
Lumia	Lumia	k1gFnSc1	Lumia
je	být	k5eAaImIp3nS	být
série	série	k1gFnPc4	série
telefonů	telefon	k1gInPc2	telefon
od	od	k7c2	od
Nokie	Nokie	k1gFnSc2	Nokie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgFnPc2d1	nová
a	a	k8xC	a
nezbytných	zbytný	k2eNgFnPc2d1	zbytný
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
řada	řada	k1gFnSc1	řada
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
nový	nový	k2eAgInSc4d1	nový
OS	OS	kA	OS
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7.5	[number]	k4	7.5
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
8.1	[number]	k4	8.1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
Lumia	Lumia	k1gFnSc1	Lumia
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
tablet	tablet	k1gInSc4	tablet
Lumia	Lumia	k1gFnSc1	Lumia
2520	[number]	k4	2520
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
může	moct	k5eAaImIp3nS	moct
řadit	řadit	k5eAaImF	řadit
i	i	k9	i
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
moderních	moderní	k2eAgInPc2d1	moderní
tabletů	tablet	k1gInPc2	tablet
s	s	k7c7	s
OS	OS	kA	OS
Windows	Windows	kA	Windows
RT	RT	kA	RT
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
o	o	k7c4	o
telefony	telefon	k1gInPc4	telefon
Lumia	Lumium	k1gNnSc2	Lumium
1320	[number]	k4	1320
a	a	k8xC	a
1520	[number]	k4	1520
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
Microsoft	Microsoft	kA	Microsoft
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
odkoupení	odkoupení	k1gNnSc4	odkoupení
Mobilní	mobilní	k2eAgFnSc2d1	mobilní
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Nokia	Nokia	kA	Nokia
za	za	k7c7	za
rozšířením	rozšíření	k1gNnSc7	rozšíření
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
a	a	k8xC	a
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
modely	model	k1gInPc4	model
nejmenuje	jmenovat	k5eNaImIp3nS	jmenovat
Nokia	Nokia	kA	Nokia
ale	ale	k8xC	ale
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
na	na	k7c4	na
Microsoft	Microsoft	kA	Microsoft
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Nokia	Nokia	kA	Nokia
začala	začít	k5eAaPmAgFnS	začít
prodávat	prodávat	k5eAaImF	prodávat
<g />
.	.	kIx.	.
</s>
<s>
mimo	mimo	k7c4	mimo
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
nový	nový	k2eAgInSc4d1	nový
Netbook	Netbook	k1gInSc4	Netbook
s	s	k7c7	s
Windows	Windows	kA	Windows
7	[number]	k4	7
Starter	Startra	k1gFnPc2	Startra
s	s	k7c7	s
názvem	název	k1gInSc7	název
Nokia	Nokia	kA	Nokia
Booklet	Booklet	k1gInSc4	Booklet
3	[number]	k4	3
<g/>
G.	G.	kA	G.
1	[number]	k4	1
<g/>
xx	xx	k?	xx
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
obsahující	obsahující	k2eAgInPc1d1	obsahující
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
nejnutnější	nutný	k2eAgNnSc4d3	nejnutnější
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
předního	přední	k2eAgInSc2d1	přední
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
bez	bez	k7c2	bez
větší	veliký	k2eAgFnSc2d2	veliký
podpory	podpora	k1gFnSc2	podpora
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
2	[number]	k4	2
<g/>
xx	xx	k?	xx
–	–	k?	–
Pokročilejší	pokročilý	k2eAgInPc4d2	pokročilejší
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
přední	přední	k2eAgFnSc7d1	přední
kamerou	kamera	k1gFnSc7	kamera
<g/>
,	,	kIx,	,
bleskem	blesk	k1gInSc7	blesk
<g/>
,	,	kIx,	,
větším	veliký	k2eAgInSc7d2	veliký
displejem	displej	k1gInSc7	displej
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc7d2	lepší
podporou	podpora	k1gFnSc7	podpora
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
větším	veliký	k2eAgInSc7d2	veliký
výkonem	výkon	k1gInSc7	výkon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
model	model	k1gInSc4	model
230	[number]	k4	230
zvládá	zvládat	k5eAaImIp3nS	zvládat
i	i	k9	i
přehrávání	přehrávání	k1gNnSc6	přehrávání
H	H	kA	H
<g/>
.264	.264	k4	.264
videa	video	k1gNnSc2	video
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
telefony	telefon	k1gInPc4	telefon
se	se	k3xPyFc4	se
výbavou	výbava	k1gFnSc7	výbava
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
nokia	nokia	k1gFnSc1	nokia
3310	[number]	k4	3310
Pro	pro	k7c4	pro
chytré	chytrý	k2eAgInPc4d1	chytrý
telefony	telefon	k1gInPc4	telefon
od	od	k7c2	od
HMD	HMD	kA	HMD
Global	globat	k5eAaImAgInS	globat
zatím	zatím	k6eAd1	zatím
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgFnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgFnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
výbava	výbava	k1gFnSc1	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Series	Series	k1gInSc1	Series
20	[number]	k4	20
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
monochromatickým	monochromatický	k2eAgInSc7d1	monochromatický
displejem	displej	k1gInSc7	displej
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
84	[number]	k4	84
<g/>
×	×	k?	×
<g/>
48	[number]	k4	48
px	px	k?	px
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
málo	málo	k6eAd1	málo
funkcemi	funkce	k1gFnPc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Series	Series	k1gInSc1	Series
30	[number]	k4	30
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
monochromatickým	monochromatický	k2eAgInSc7d1	monochromatický
nebo	nebo	k8xC	nebo
barevným	barevný	k2eAgInSc7d1	barevný
displejem	displej	k1gInSc7	displej
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
96	[number]	k4	96
<g/>
×	×	k?	×
<g/>
65	[number]	k4	65
px	px	k?	px
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
např.	např.	kA	např.
telefon	telefon	k1gInSc4	telefon
Nokia	Nokia	kA	Nokia
7110	[number]	k4	7110
nebo	nebo	k8xC	nebo
Nokia	Nokia	kA	Nokia
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Series	Series	k1gInSc1	Series
40	[number]	k4	40
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
displejem	displej	k1gInSc7	displej
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
208	[number]	k4	208
<g/>
×	×	k?	×
<g/>
208	[number]	k4	208
<g/>
,	,	kIx,	,
128	[number]	k4	128
<g/>
×	×	k?	×
<g/>
160	[number]	k4	160
px	px	k?	px
<g/>
,	,	kIx,	,
128	[number]	k4	128
<g/>
×	×	k?	×
<g/>
128	[number]	k4	128
px	px	k?	px
<g/>
,	,	kIx,	,
96	[number]	k4	96
<g/>
×	×	k?	×
<g/>
65	[number]	k4	65
px	px	k?	px
a	a	k8xC	a
poslední	poslední	k2eAgInPc4d1	poslední
modely	model	k1gInPc4	model
i	i	k9	i
320	[number]	k4	320
<g/>
×	×	k?	×
<g/>
240	[number]	k4	240
px	px	k?	px
<g/>
.	.	kIx.	.
</s>
<s>
Telefony	telefon	k1gInPc1	telefon
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
např.	např.	kA	např.
Nokia	Nokia	kA	Nokia
6230	[number]	k4	6230
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
6230	[number]	k4	6230
<g/>
i	i	k8xC	i
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
6100	[number]	k4	6100
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
8910	[number]	k4	8910
<g/>
i	i	k8xC	i
a	a	k8xC	a
Nokia	Nokia	kA	Nokia
7250	[number]	k4	7250
<g/>
i.	i.	k?	i.
Series	Series	k1gInSc1	Series
60	[number]	k4	60
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
displejem	displej	k1gInSc7	displej
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
176	[number]	k4	176
<g/>
×	×	k?	×
<g/>
208	[number]	k4	208
px	px	k?	px
až	až	k9	až
320	[number]	k4	320
<g/>
×	×	k?	×
<g/>
240	[number]	k4	240
px	px	k?	px
(	(	kIx(	(
<g/>
QVGA	QVGA	kA	QVGA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Symbian	Symbiana	k1gFnPc2	Symbiana
OS	OS	kA	OS
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
3	[number]	k4	3
<g/>
.	.	kIx.	.
edice	edice	k1gFnSc1	edice
S	s	k7c7	s
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
telefony	telefon	k1gInPc1	telefon
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
E	E	kA	E
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
N	N	kA	N
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
7650	[number]	k4	7650
a	a	k8xC	a
Nokia	Nokia	kA	Nokia
6600	[number]	k4	6600
<g/>
.	.	kIx.	.
</s>
<s>
Series	Series	k1gInSc1	Series
80	[number]	k4	80
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
displejem	displej	k1gInSc7	displej
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
640	[number]	k4	640
<g/>
×	×	k?	×
<g/>
200	[number]	k4	200
px	px	k?	px
a	a	k8xC	a
alfanumerickou	alfanumerický	k2eAgFnSc7d1	alfanumerická
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Symbian	Symbiana	k1gFnPc2	Symbiana
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
telefony	telefon	k1gInPc1	telefon
Nokia	Nokia	kA	Nokia
9300	[number]	k4	9300
<g/>
i	i	k8xC	i
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
9500	[number]	k4	9500
a	a	k8xC	a
Nokia	Nokia	kA	Nokia
9210	[number]	k4	9210
<g/>
i.	i.	k?	i.
Series	Series	k1gInSc1	Series
90	[number]	k4	90
–	–	k?	–
Telefony	telefon	k1gInPc1	telefon
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
dotykovým	dotykový	k2eAgInSc7d1	dotykový
displejem	displej	k1gInSc7	displej
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
640	[number]	k4	640
<g/>
×	×	k?	×
<g/>
320	[number]	k4	320
px	px	k?	px
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Symbian	Symbiana	k1gFnPc2	Symbiana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
telefon	telefon	k1gInSc4	telefon
Nokia	Nokia	kA	Nokia
7710	[number]	k4	7710
<g/>
.	.	kIx.	.
</s>
