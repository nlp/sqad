<s>
Tadeusz	Tadeusz	k1gMnSc1
Baird	Baird	k1gMnSc1
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Baird	Baird	k1gMnSc1
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1928	#num#	k4
Grodzisk	Grodzisko	k1gNnPc2
Mazowiecki	Mazowieck	k1gFnSc2
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1981	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
53	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Varšava	Varšava	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Powazkowský	Powazkowský	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
Žánry	žánr	k1gInPc1
</s>
<s>
klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
opera	opera	k1gFnSc1
a	a	k8xC
atonalita	atonalita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
filmové	filmový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Odznak	odznak	k1gInSc1
státního	státní	k2eAgNnSc2d1
ocenění	ocenění	k1gNnSc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
medaile	medaile	k1gFnSc2
Komise	komise	k1gFnSc2
pro	pro	k7c4
národní	národní	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
Web	web	k1gInSc4
</s>
<s>
www.baird.polmic.pl/index.php/pl/	www.baird.polmic.pl/index.php/pl/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Aleksander	Aleksander	k1gMnSc1
Baird	Baird	k1gMnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1928	#num#	k4
Grodzisk	Grodzisk	k1gInSc1
Mazowiecki	Mazowieck	k1gFnSc2
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1981	#num#	k4
Varšava	Varšava	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
polský	polský	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
studoval	studovat	k5eAaImAgMnS
u	u	k7c2
Bolesława	Bolesławum	k1gNnSc2
Woytowicze	Woytowicze	k1gFnSc2
a	a	k8xC
Kazimiera	Kazimier	k1gMnSc2
Sikorského	Sikorský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1947	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiu	studio	k1gNnSc6
u	u	k7c2
Piotra	Piotr	k1gMnSc2
Rytela	Rytel	k1gMnSc2
a	a	k8xC
Piotra	Piotr	k1gMnSc2
Perkowského	Perkowský	k1gMnSc2
na	na	k7c6
Státní	státní	k2eAgFnSc6d1
vyšší	vysoký	k2eAgFnSc6d2
hudební	hudební	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
studoval	studovat	k5eAaImAgMnS
hru	hra	k1gFnSc4
na	na	k7c4
klavír	klavír	k1gInSc4
u	u	k7c2
Tadeusze	Tadeusze	k1gFnSc2
Wituského	Wituský	k2eAgMnSc2d1
a	a	k8xC
muzikologii	muzikologie	k1gFnSc6
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
skladatel	skladatel	k1gMnSc1
debutoval	debutovat	k5eAaBmAgMnS
krátce	krátce	k6eAd1
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Kazimierzem	Kazimierz	k1gMnSc7
Serockým	Serocký	k1gMnSc7
a	a	k8xC
Janem	Jan	k1gMnSc7
Krenzem	Krenz	k1gMnSc7
založil	založit	k5eAaPmAgMnS
volné	volný	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
skladatelů	skladatel	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
si	se	k3xPyFc3
říkalo	říkat	k5eAaImAgNnS
Skupina	skupina	k1gFnSc1
49	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
kompozice	kompozice	k1gFnSc1
hudby	hudba	k1gFnSc2
v	v	k7c6
duchu	duch	k1gMnSc6
světového	světový	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
hudby	hudba	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
opozici	opozice	k1gFnSc6
vůči	vůči	k7c3
<g/>
,	,	kIx,
ve	v	k7c6
východním	východní	k2eAgInSc6d1
bloku	blok	k1gInSc6
vládnoucí	vládnoucí	k2eAgFnPc1d1
<g/>
,	,	kIx,
estetice	estetika	k1gFnSc3
socialistického	socialistický	k2eAgInSc2d1
realismu	realismus	k1gInSc2
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
tvůrců	tvůrce	k1gMnPc2
převratu	převrat	k1gInSc2
v	v	k7c6
polské	polský	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používal	používat	k5eAaImAgMnS
moderní	moderní	k2eAgFnSc2d1
kompoziční	kompoziční	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc4
z	z	k7c2
prvních	první	k4xOgInPc2
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
skladbách	skladba	k1gFnPc6
aplikoval	aplikovat	k5eAaBmAgInS
dodekafonii	dodekafonie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
iniciátorů	iniciátor	k1gMnPc2
a	a	k8xC
organizátorů	organizátor	k1gMnPc2
mezinárodního	mezinárodní	k2eAgInSc2d1
festivalu	festival	k1gInSc2
soudobé	soudobý	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
Varšavský	varšavský	k2eAgInSc4d1
podzim	podzim	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
konal	konat	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
vyučoval	vyučovat	k5eAaImAgMnS
skladbu	skladba	k1gFnSc4
na	na	k7c6
varšavské	varšavský	k2eAgFnSc6d1
Státní	státní	k2eAgFnSc6d1
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
profesorem	profesor	k1gMnSc7
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
vedoucím	vedoucí	k1gMnSc7
katedry	katedra	k1gFnSc2
skladby	skladba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
polské	polský	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
pro	pro	k7c4
soudobou	soudobý	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
(	(	kIx(
<g/>
ISMC	ISMC	kA
<g/>
/	/	kIx~
<g/>
SimC	SimC	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
členem	člen	k1gMnSc7
Akademie	akademie	k1gFnSc2
der	drát	k5eAaImRp2nS
Künste	Künst	k1gMnSc5
v	v	k7c6
Berlíně	Berlín	k1gInSc6
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Svazu	svaz	k1gInSc2
polských	polský	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
náhle	náhle	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1981	#num#	k4
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
následkem	následkem	k7c2
aneurysma	aneurysma	k1gNnSc2
mozkových	mozkový	k2eAgFnPc2d1
tepen	tepna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pohřben	pohřbít	k5eAaPmNgMnS
na	na	k7c6
Powązkowském	Powązkowský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Státní	státní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
záslužný	záslužný	k2eAgInSc1d1
kříž	kříž	k1gInSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
trbuně	trbuna	k1gFnSc6
skladatelů	skladatel	k1gMnPc2
UNESCO	UNESCO	kA
v	v	k7c6
Paříži	Paříž	k1gFnSc6
za	za	k7c4
Čtyři	čtyři	k4xCgInPc4
eseje	esej	k1gInPc4
pro	pro	k7c4
orchestr	orchestr	k1gInSc4
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
ministra	ministr	k1gMnSc2
kultury	kultura	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
trbuně	trbuna	k1gFnSc6
skladatelů	skladatel	k1gMnPc2
UNESCO	UNESCO	kA
v	v	k7c6
Paříži	Paříž	k1gFnSc6
za	za	k7c2
Variace	variace	k1gFnSc2
bez	bez	k7c2
tématu	téma	k1gNnSc2
pro	pro	k7c4
symfonický	symfonický	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hudební	hudební	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
města	město	k1gNnSc2
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
</s>
<s>
Státní	státní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
trbuně	trbuna	k1gFnSc6
skladatelů	skladatel	k1gMnPc2
UNESCO	UNESCO	kA
v	v	k7c6
Paříži	Paříž	k1gFnSc6
za	za	k7c4
Čtyři	čtyři	k4xCgInPc4
dialogy	dialog	k1gInPc4
pro	pro	k7c4
hoboj	hoboj	k1gFnSc4
a	a	k8xC
komorní	komorní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
Svazu	svaz	k1gInSc2
polských	polský	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
Sergeje	Sergej	k1gMnSc2
Kusewického	Kusewický	k2eAgInSc2d1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Státní	státní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
Nadace	nadace	k1gFnSc2
Alfreda	Alfred	k1gMnSc2
Jurzykowského	Jurzykowský	k1gMnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
práce	práce	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prix	Prix	k1gInSc1
Arthur	Arthur	k1gMnSc1
Honegger	Honegger	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čestné	čestný	k2eAgNnSc1d1
občanství	občanství	k1gNnSc1
Drezna	Drezna	k1gFnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
ministra	ministr	k1gMnSc2
kultury	kultura	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Medale	Medale	k6eAd1
Jeana	Jean	k1gMnSc2
Sibelia	Sibelius	k1gMnSc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Medaile	medaile	k1gFnSc1
Komise	komise	k1gFnSc1
národního	národní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
předsedy	předseda	k1gMnSc2
rady	rada	k1gMnSc2
ministrů	ministr	k1gMnPc2
1	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
práce	práce	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
,	,	kIx,
posmrtně	posmrtně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Hrob	hrob	k1gInSc1
skladatele	skladatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Komponoval	komponovat	k5eAaImAgInS
širokou	široký	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
symfonické	symfonický	k2eAgFnSc2d1
a	a	k8xC
komorní	komorní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnSc3d3
však	však	k9
jsou	být	k5eAaImIp3nP
vokální	vokální	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
inspirované	inspirovaný	k2eAgInPc1d1
poesií	poesie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
hudebního	hudební	k2eAgNnSc2d1
dramatu	drama	k1gNnSc2
Zítra	zítra	k6eAd1
komponovaného	komponovaný	k2eAgInSc2d1
na	na	k7c4
motivy	motiv	k1gInPc4
povídky	povídka	k1gFnSc2
Josepha	Josepha	k1gFnSc1
Conrada	Conrada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komponoval	komponovat	k5eAaImAgMnS
rovněž	rovněž	k9
hudbu	hudba	k1gFnSc4
pro	pro	k7c4
film	film	k1gInSc4
<g/>
,	,	kIx,
divadlo	divadlo	k1gNnSc4
a	a	k8xC
televizi	televize	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
seriální	seriální	k2eAgFnSc3d1
technice	technika	k1gFnSc3
vyrůstá	vyrůstat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
dílo	dílo	k1gNnSc4
z	z	k7c2
romantických	romantický	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Klavírní	klavírní	k2eAgFnPc1d1
skladby	skladba	k1gFnPc1
</s>
<s>
Sonatina	sonatina	k1gFnSc1
I.	I.	kA
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sonatina	sonatina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Malá	malý	k2eAgFnSc1d1
suita	suita	k1gFnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Komorní	komorní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
2	#num#	k4
capriccia	capriccio	k1gNnPc4
pro	pro	k7c4
klarinet	klarinet	k1gInSc4
a	a	k8xC
klavír	klavír	k1gInSc4
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
Unie	unie	k1gFnSc2
polských	polský	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
preludia	preludium	k1gNnPc4
pro	pro	k7c4
fagot	fagot	k1gInSc4
a	a	k8xC
klavír	klavír	k1gInSc4
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Divertimento	divertimento	k1gNnSc1
pro	pro	k7c4
flétnu	flétna	k1gFnSc4
<g/>
,	,	kIx,
klarinet	klarinet	k1gInSc1
<g/>
,	,	kIx,
hoboj	hoboj	k1gInSc1
a	a	k8xC
fagot	fagot	k1gInSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Smyčcový	smyčcový	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hra	hra	k1gFnSc1
pro	pro	k7c4
smyčcový	smyčcový	k2eAgInSc4d1
kvartet	kvartet	k1gInSc4
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Variace	variace	k1gFnPc1
ve	v	k7c6
formě	forma	k1gFnSc6
ronda	rondo	k1gNnSc2
pro	pro	k7c4
smyčcový	smyčcový	k2eAgInSc4d1
kvartet	kvartet	k1gInSc4
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Orchestrální	orchestrální	k2eAgFnPc1d1
skladby	skladba	k1gFnPc1
</s>
<s>
Sinfonietta	Sinfonietta	k1gFnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klavírní	klavírní	k2eAgInSc1d1
koncert	koncert	k1gInSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Symfonie	symfonie	k1gFnSc1
č.	č.	k?
1	#num#	k4
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
,	,	kIx,
Polaská	polaskat	k5eAaPmIp3nS
státní	státní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Colas	Colas	k1gMnSc1
Breugnon	Breugnon	k1gMnSc1
(	(	kIx(
<g/>
suita	suita	k1gFnSc1
ve	v	k7c6
starém	starý	k2eAgInSc6d1
stylu	styl	k1gInSc6
pro	pro	k7c4
smyčcový	smyčcový	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
a	a	k8xC
flétnu	flétna	k1gFnSc4
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Symfonie	symfonie	k1gFnSc1
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
quasi	quasi	k6eAd1
una	una	k?
fantasia	fantasia	k1gFnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Koncert	koncert	k1gInSc1
pro	pro	k7c4
orchestr	orchestr	k1gInSc4
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cassazione	Cassazion	k1gInSc5
per	pero	k1gNnPc2
orchestra	orchestra	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
esseje	essej	k1gInPc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
UNESCO	Unesco	k1gNnSc1
1959	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Variatace	Variatace	k1gFnSc1
bez	bez	k7c2
tématu	téma	k1gNnSc2
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Epiphany	Epiphan	k1gInPc1
Music	Musice	k1gFnPc2
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
dialogy	dialog	k1gInPc1
pro	pro	k7c4
hoboj	hoboj	k1gFnSc4
a	a	k8xC
komorní	komorní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
,	,	kIx,
Cena	cena	k1gFnSc1
UNESCO	Unesco	k1gNnSc1
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
noveletty	novelett	k1gInPc1
pro	pro	k7c4
komorní	komorní	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sinfonia	sinfonia	k1gFnSc1
Breve	breve	k1gNnSc2
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Symfonie	symfonie	k1gFnSc1
č.	č.	k?
3	#num#	k4
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Psychodrama	psychodrama	k1gNnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Elegia	Elegia	k1gFnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Koncert	koncert	k1gInSc1
pro	pro	k7c4
hoboj	hoboj	k1gFnSc4
a	a	k8xC
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Concerto	Concerta	k1gFnSc5
Lugubre	Lugubr	k1gInSc5
pro	pro	k7c4
violu	viola	k1gFnSc4
a	a	k8xC
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Scény	scéna	k1gFnPc4
pro	pro	k7c4
violoncello	violoncello	k1gNnSc4
<g/>
,	,	kIx,
harfu	harfa	k1gFnSc4
a	a	k8xC
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Canzona	Canzona	k1gFnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vokální	vokální	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
milostné	milostný	k2eAgInPc1d1
sonety	sonet	k1gInPc1
pro	pro	k7c4
baryton	baryton	k1gInSc4
a	a	k8xC
orchestr	orchestr	k1gInSc4
na	na	k7c4
slova	slovo	k1gNnPc4
Williama	William	k1gMnSc2
Shakespeara	Shakespeare	k1gMnSc2
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Exhortation	Exhortation	k1gInSc1
pro	pro	k7c4
recitátora	recitátor	k1gMnSc4
a	a	k8xC
orchestr	orchestr	k1gInSc1
na	na	k7c4
staré	starý	k2eAgInPc4d1
hebrejské	hebrejský	k2eAgInPc4d1
texty	text	k1gInPc4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Erotyki	Erotyki	k6eAd1
pro	pro	k7c4
soprán	soprán	k1gInSc4
a	a	k8xC
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
text	text	k1gInSc1
Małgorzata	Małgorzata	k1gFnSc1
Hilar	Hilar	k1gMnSc1
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
písní	píseň	k1gFnPc2
pro	pro	k7c4
mezzosoprán	mezzosoprán	k1gInSc4
a	a	k8xC
šest	šest	k4xCc4
náatrojů	náatroj	k1gInPc2
(	(	kIx(
<g/>
text	text	k1gInSc1
H.	H.	kA
Poświatowska	Poświatowsek	k1gMnSc2
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Státní	státní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Goetheho	Goetheze	k6eAd1
dopisy	dopis	k1gInPc1
(	(	kIx(
<g/>
kantáta	kantáta	k1gFnSc1
pro	pro	k7c4
baryton	baryton	k1gInSc4
<g/>
,	,	kIx,
smíšený	smíšený	k2eAgInSc1d1
sbor	sbor	k1gInSc1
a	a	k8xC
orchestr	orchestr	k1gInSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Głosy	Głosa	k1gFnPc1
z	z	k7c2
oddali	oddat	k5eAaPmAgMnP
(	(	kIx(
<g/>
Hlasy	hlas	k1gInPc1
zdaleka	zdaleka	k6eAd1
<g/>
)	)	kIx)
pro	pro	k7c4
baryton	baryton	k1gInSc4
a	a	k8xC
symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
(	(	kIx(
<g/>
text	text	k1gInSc1
J.	J.	kA
Iwaszkiewicz	Iwaszkiewicz	k1gInSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zítra	zítra	k6eAd1
<g/>
,	,	kIx,
hudební	hudební	k2eAgNnSc4d1
drama	drama	k1gNnSc4
<g/>
,	,	kIx,
libreto	libreto	k1gNnSc1
J.	J.	kA
S.	S.	kA
Sito	Sito	k1gMnSc1
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
Sztuka	Sztuk	k1gMnSc4
mlodych	mlodych	k1gMnSc1
(	(	kIx(
<g/>
dokument	dokument	k1gInSc1
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kierunek-	Kierunek-	k?
<g/>
0	#num#	k4
Nowa	Nowus	k1gMnSc2
Huta	Hutus	k1gMnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
dokument	dokument	k1gInSc1
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petla	Petla	k1gFnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kamienne	Kamienn	k1gMnSc5
niebo	nieba	k1gMnSc5
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lotna	Lotna	k1gFnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rok	rok	k1gInSc1
pierwszy	pierwsza	k1gFnSc2
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kwiecien	Kwiecien	k1gInSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ogniomistrz	Ogniomistrz	k1gMnSc1
Kalen	kalen	k2eAgMnSc1d1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Samson	Samson	k1gMnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ludzie	Ludzie	k1gFnSc1
z	z	k7c2
pociagu	pociag	k1gInSc2
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spóznieni	Spóznien	k2eAgMnPc1d1
przechodnie	przechodnie	k1gFnSc2
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pasazerka	Pasazerka	k1gFnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ich	Ich	k?
dzien	dzien	k1gInSc1
powszedni	powszednout	k5eAaPmRp2nS
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zerwany	Zerwana	k1gFnPc1
most	most	k1gInSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miedzy	Miedza	k1gFnPc1
brzegami	brzega	k1gFnPc7
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Agnieszka	Agnieszka	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nieznany	Nieznana	k1gFnPc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Naganiacz	Naganiacz	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wózek	Wózek	k1gMnSc1
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Czlowiek	Czlowiek	k6eAd1
z	z	k7c2
kwiatem	kwiat	k1gMnSc7
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Córeczka	Córeczka	k1gFnSc1
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Drewniany	Drewniana	k1gFnPc1
rózaniec	rózaniec	k1gInSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dzien	Dzien	k2eAgInSc4d1
ostatni	ostatnout	k5eAaPmRp2nS,k5eAaImRp2nS
<g/>
,	,	kIx,
dzien	dzien	k1gInSc4
pierwszy	pierwsza	k1gFnSc2
(	(	kIx(
<g/>
TV	TV	kA
seriál	seriál	k1gInSc1
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wizyta	Wizyta	k1gMnSc1
u	u	k7c2
królów	królów	k?
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Buty	Buta	k1gFnPc1
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miejsce	Miejska	k1gFnSc3
dla	dla	k?
jednego	jednego	k1gNnSc4
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Smierc	Smierc	k6eAd1
w	w	k?
srodkowym	srodkowym	k1gInSc4
pokoju	pokoju	k5eAaPmIp1nS
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Przerazliwe	Przerazliwe	k6eAd1
loze	loza	k1gFnSc3
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kiedy	Kiedy	k6eAd1
milosc	milosc	k6eAd1
byla	být	k5eAaImAgFnS
zbrodnia	zbrodnium	k1gNnPc4
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zbrodnia	Zbrodnium	k1gNnPc1
lorda	lord	k1gMnSc2
Artura	Artur	k1gMnSc2
Savile	Savila	k1gFnSc6
<g/>
'	'	kIx"
<g/>
a	a	k8xC
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gniewko	Gniewko	k6eAd1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
rybaka	rybak	k1gMnSc2
(	(	kIx(
<g/>
TV	TV	kA
miniserie	miniserie	k1gFnSc2
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c4
meline	melinout	k5eAaPmIp3nS
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
K.	K.	kA
Tarnawska-Kaczorowska	Tarnawska-Kaczorowska	k1gFnSc1
<g/>
,	,	kIx,
Świat	Świat	k1gInSc1
liryki	liryk	k1gFnSc2
wokalno-instrumentalnej	wokalno-instrumentalnat	k5eAaPmRp2nS,k5eAaImRp2nS
Tadeusza	Tadeusz	k1gMnSc4
Bairda	Bairda	k1gFnSc1
<g/>
,	,	kIx,
Kraków	Kraków	k1gFnSc1
1982	#num#	k4
</s>
<s>
K.	K.	kA
Tarnawska-Kaczorowska	Tarnawska-Kaczorowska	k1gFnSc1
<g/>
,	,	kIx,
Tadeusz	Tadeusz	k1gMnSc1
Baird	Baird	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glosy	glosa	k1gFnSc2
do	do	k7c2
biografii	biografie	k1gFnSc4
<g/>
,	,	kIx,
Kraków	Kraków	k1gFnSc1
1997	#num#	k4
</s>
<s>
T.	T.	kA
Baird	Baird	k1gMnSc1
<g/>
,	,	kIx,
I.	I.	kA
Grzenkowicz	Grzenkowicz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozmowy	Rozmowum	k1gNnPc7
<g/>
,	,	kIx,
szkice	szkice	k1gFnSc1
<g/>
,	,	kIx,
refleksje	refleksje	k1gFnSc1
<g/>
,	,	kIx,
Kraków	Kraków	k1gFnSc1
1998	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tadeusz	Tadeusza	k1gFnPc2
Baird	Bairdo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránka	stránka	k1gFnSc1
věnovaná	věnovaný	k2eAgFnSc1d1
skladateli	skladatel	k1gMnPc7
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Baird	Baird	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Baird	Baird	k1gMnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002150837	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
120312956	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8111	#num#	k4
6320	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80072505	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
32187433	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80072505	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
