<s>
Nightwish	Nightwish	k1gInSc1	Nightwish
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Tuomas	Tuomas	k1gInSc1	Tuomas
Holopainen	Holopainen	k2eAgInSc1d1	Holopainen
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Tarja	Tarj	k1gInSc2	Tarj
Turunen	Turunno	k1gNnPc2	Turunno
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Emppu	Empp	k1gInSc2	Empp
Vuorinen	Vuorinna	k1gFnPc2	Vuorinna
společně	společně	k6eAd1	společně
nahráli	nahrát	k5eAaPmAgMnP	nahrát
demo	demo	k2eAgMnPc1d1	demo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
"	"	kIx"	"
<g/>
Nightwish	Nightwish	k1gInSc4	Nightwish
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
i	i	k9	i
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
o	o	k7c4	o
bubeníka	bubeník	k1gMnSc4	bubeník
Juliuse	Juliuse	k1gFnSc1	Juliuse
"	"	kIx"	"
<g/>
Jukku	Jukka	k1gMnSc4	Jukka
<g/>
"	"	kIx"	"
Nevalainena	Nevalainen	k1gMnSc4	Nevalainen
a	a	k8xC	a
baskytaristu	baskytarista	k1gMnSc4	baskytarista
Samiho	Sami	k1gMnSc4	Sami
Vänsku	Vänsek	k1gInSc2	Vänsek
<g/>
.	.	kIx.	.
</s>
<s>
Samiho	Samize	k6eAd1	Samize
po	po	k7c6	po
pár	pár	k4xCyI	pár
letech	léto	k1gNnPc6	léto
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Marco	Marco	k6eAd1	Marco
Hietala	Hietal	k1gMnSc4	Hietal
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
Tarji	Tarj	k1gInSc6	Tarj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
vyhozena	vyhodit	k5eAaPmNgFnS	vyhodit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
oznámena	oznámen	k2eAgFnSc1d1	oznámena
nová	nový	k2eAgFnSc1d1	nová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
Švédka	Švédka	k1gFnSc1	Švédka
Anette	Anett	k1gInSc5	Anett
Olzon	Olzona	k1gFnPc2	Olzona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opustila	opustit	k5eAaPmAgFnS	opustit
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Floor	Floor	k1gMnSc1	Floor
Jansen	Jansen	k1gInSc1	Jansen
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c6	na
oficiálním	oficiální	k2eAgInSc6d1	oficiální
facebooku	facebook	k1gInSc6	facebook
skupiny	skupina	k1gFnSc2	skupina
bubeník	bubeník	k1gMnSc1	bubeník
Jukka	Jukka	k1gMnSc1	Jukka
Nevalainen	Nevalainen	k1gInSc4	Nevalainen
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
pauzu	pauza	k1gFnSc4	pauza
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
účinkování	účinkování	k1gNnSc6	účinkování
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
Endless	Endless	k1gInSc4	Endless
Forms	Forms	k1gInSc1	Forms
Most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
jej	on	k3xPp3gMnSc4	on
zastoupil	zastoupit	k5eAaPmAgInS	zastoupit
jeho	on	k3xPp3gMnSc4	on
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Kai	Kai	k1gMnSc2	Kai
Hahto	Hahto	k1gNnSc4	Hahto
<g/>
.	.	kIx.	.
</s>
<s>
Nightwish	Nightwish	k1gMnSc1	Nightwish
byl	být	k5eAaImAgMnS	být
myšlenkou	myšlenka	k1gFnSc7	myšlenka
Tuomase	Tuomasa	k1gFnSc3	Tuomasa
Holopainena	Holopaineno	k1gNnSc2	Holopaineno
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dokument	dokument	k1gInSc4	dokument
End	End	k1gMnSc2	End
of	of	k?	of
Innocence	Innocenec	k1gMnSc2	Innocenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
tato	tento	k3xDgFnSc1	tento
věc	věc	k1gFnSc1	věc
napadla	napadnout	k5eAaPmAgFnS	napadnout
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
strávené	strávený	k2eAgInPc1d1	strávený
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
u	u	k7c2	u
táboráku	táborák	k1gInSc2	táborák
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Holopainen	Holopainen	k1gInSc1	Holopainen
sezval	sezvat	k5eAaPmAgInS	sezvat
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
kytaristu	kytarista	k1gMnSc4	kytarista
Erna	Ernus	k1gMnSc4	Ernus
"	"	kIx"	"
<g/>
Emppu	Empp	k1gInSc2	Empp
<g/>
"	"	kIx"	"
Vuorinena	Vuorineno	k1gNnSc2	Vuorineno
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
znal	znát	k5eAaImAgInS	znát
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Tarju	Tarju	k1gFnSc2	Tarju
Turunen	Turunno	k1gNnPc2	Turunno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
oba	dva	k4xCgMnPc1	dva
chodili	chodit	k5eAaImAgMnP	chodit
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
svými	svůj	k3xOyFgFnPc7	svůj
úžasnými	úžasný	k2eAgFnPc7d1	úžasná
vokálními	vokální	k2eAgFnPc7d1	vokální
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
Tuomasových	Tuomasův	k2eAgInPc6d1	Tuomasův
experimentech	experiment	k1gInPc6	experiment
s	s	k7c7	s
klávesami	klávesa	k1gFnPc7	klávesa
<g/>
,	,	kIx,	,
akustickými	akustický	k2eAgFnPc7d1	akustická
kytarami	kytara	k1gFnPc7	kytara
a	a	k8xC	a
Tarjiným	Tarjin	k2eAgInSc7d1	Tarjin
operním	operní	k2eAgInSc7d1	operní
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
muzikanti	muzikant	k1gMnPc1	muzikant
mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
1996	[number]	k4	1996
nahráli	nahrát	k5eAaPmAgMnP	nahrát
akustické	akustický	k2eAgNnSc4d1	akustické
demo	demo	k2eAgNnSc4d1	demo
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgFnPc4	tři
písně	píseň	k1gFnPc4	píseň
<g/>
:	:	kIx,	:
Nightwish	Nightwish	k1gMnSc1	Nightwish
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
nazvala	nazvat	k5eAaPmAgFnS	nazvat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Forever	Forever	k1gMnSc1	Forever
Moments	Moments	k1gInSc1	Moments
a	a	k8xC	a
Etiäinen	Etiäinen	k1gInSc1	Etiäinen
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnSc7d1	prvotní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
dělat	dělat	k5eAaImF	dělat
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
hrát	hrát	k5eAaImF	hrát
při	při	k7c6	při
táboráku	táborák	k1gInSc6	táborák
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Tuomas	Tuomas	k1gMnSc1	Tuomas
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Tarjin	Tarjin	k2eAgInSc1d1	Tarjin
hlas	hlas	k1gInSc1	hlas
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
jako	jako	k8xC	jako
tento	tento	k3xDgInSc1	tento
moc	moc	k6eAd1	moc
mohutný	mohutný	k2eAgInSc1d1	mohutný
<g/>
,	,	kIx,	,
přidat	přidat	k5eAaPmF	přidat
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
metalové	metalový	k2eAgInPc4d1	metalový
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
přidal	přidat	k5eAaPmAgMnS	přidat
bubeník	bubeník	k1gMnSc1	bubeník
Jukka	Jukka	k1gMnSc1	Jukka
"	"	kIx"	"
<g/>
Julius	Julius	k1gMnSc1	Julius
<g/>
"	"	kIx"	"
Nevalainen	Nevalainen	k1gInSc1	Nevalainen
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
také	také	k9	také
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
zavřela	zavřít	k5eAaPmAgFnS	zavřít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahrála	nahrát	k5eAaBmAgFnS	nahrát
7	[number]	k4	7
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vylepšené	vylepšený	k2eAgFnSc2d1	vylepšená
verze	verze	k1gFnSc2	verze
demo-písně	demoísně	k6eAd1	demo-písně
Etiäinen	Etiäinen	k2eAgInSc1d1	Etiäinen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
songy	song	k1gInPc1	song
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
na	na	k7c6	na
limitované	limitovaný	k2eAgFnSc6d1	limitovaná
edici	edice	k1gFnSc6	edice
alba	album	k1gNnSc2	album
Angels	Angelsa	k1gFnPc2	Angelsa
Fall	Fall	k1gMnSc1	Fall
First	First	k1gMnSc1	First
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
toho	ten	k3xDgNnSc2	ten
samého	samý	k3xTgInSc2	samý
roku	rok	k1gInSc2	rok
podepsala	podepsat	k5eAaPmAgFnS	podepsat
kapela	kapela	k1gFnSc1	kapela
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
s	s	k7c7	s
finskou	finský	k2eAgFnSc7d1	finská
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
Spinefarm	Spinefarm	k1gInSc1	Spinefarm
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Angels	Angels	k1gInSc1	Angels
Fall	Fall	k1gInSc1	Fall
First	First	k1gInSc4	First
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
příčky	příčka	k1gFnSc2	příčka
ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
hitparádě	hitparáda	k1gFnSc6	hitparáda
prodejnosti	prodejnost	k1gFnSc2	prodejnost
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
vydaný	vydaný	k2eAgInSc4d1	vydaný
singl	singl	k1gInSc4	singl
The	The	k1gMnSc1	The
Carpenter	Carpenter	k1gMnSc1	Carpenter
byl	být	k5eAaImAgMnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
žebříčku	žebříček	k1gInSc6	žebříček
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
přijala	přijmout	k5eAaPmAgFnS	přijmout
Angels	Angels	k1gInSc4	Angels
Fall	Fall	k1gMnSc1	Fall
First	First	k1gMnSc1	First
rozdílně	rozdílně	k6eAd1	rozdílně
<g/>
.	.	kIx.	.
</s>
<s>
All	All	k?	All
Music	Music	k1gMnSc1	Music
Guide	Guid	k1gInSc5	Guid
ohodnotilo	ohodnotit	k5eAaPmAgNnS	ohodnotit
album	album	k1gNnSc1	album
2	[number]	k4	2
body	bod	k1gInPc7	bod
z	z	k7c2	z
5	[number]	k4	5
a	a	k8xC	a
zdroje	zdroj	k1gInPc4	zdroj
jako	jako	k9	jako
The	The	k1gMnPc3	The
Metal	metat	k5eAaImAgMnS	metat
Observer	Observer	k1gInSc4	Observer
prohlásily	prohlásit	k5eAaPmAgFnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
jejich	jejich	k3xOp3gNnSc1	jejich
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
mdlé	mdlý	k2eAgNnSc1d1	mdlé
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pozdější	pozdní	k2eAgFnSc7d2	pozdější
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c6	na
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
Metallum	Metallum	k1gInSc1	Metallum
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
recenzích	recenze	k1gFnPc6	recenze
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
album	album	k1gNnSc1	album
průměrného	průměrný	k2eAgNnSc2d1	průměrné
hodnocení	hodnocení	k1gNnSc2	hodnocení
91	[number]	k4	91
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1997	[number]	k4	1997
hráli	hrát	k5eAaImAgMnP	hrát
Nightwish	Nightwish	k1gInSc4	Nightwish
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
1997-1998	[number]	k4	1997-1998
hrála	hrát	k5eAaImAgFnS	hrát
kapela	kapela	k1gFnSc1	kapela
už	už	k6eAd1	už
jen	jen	k9	jen
sedmkrát	sedmkrát	k6eAd1	sedmkrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Nevalainen	Nevalainen	k1gInSc4	Nevalainen
a	a	k8xC	a
Vuorinen	Vuorinen	k1gInSc4	Vuorinen
museli	muset	k5eAaImAgMnP	muset
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
a	a	k8xC	a
Tarja	Tarja	k1gFnSc1	Tarja
Turunen	Turunna	k1gFnPc2	Turunna
ještě	ještě	k9	ještě
nedokončila	dokončit	k5eNaPmAgFnS	dokončit
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
přidal	přidat	k5eAaPmAgMnS	přidat
baskytarista	baskytarista	k1gMnSc1	baskytarista
Sami	sám	k3xTgMnPc1	sám
Vänskä	Vänskä	k1gMnPc1	Vänskä
<g/>
,	,	kIx,	,
Tuomasův	Tuomasův	k2eAgMnSc1d1	Tuomasův
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
natočení	natočení	k1gNnSc6	natočení
videa	video	k1gNnSc2	video
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
The	The	k1gFnSc2	The
Carpenter	Carpentra	k1gFnPc2	Carpentra
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
album	album	k1gNnSc4	album
Oceanborn	Oceanborn	k1gMnSc1	Oceanborn
<g/>
,	,	kIx,	,
následovníka	následovník	k1gMnSc2	následovník
Angels	Angelsa	k1gFnPc2	Angelsa
Fall	Fall	k1gMnSc1	Fall
First	First	k1gMnSc1	First
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
aranží	aranže	k1gFnPc2	aranže
a	a	k8xC	a
textů	text	k1gInPc2	text
bylo	být	k5eAaImAgNnS	být
Oceanborn	Oceanborn	k1gNnSc1	Oceanborn
propracovanější	propracovaný	k2eAgNnSc1d2	propracovanější
a	a	k8xC	a
progresivnější	progresivní	k2eAgNnSc1d2	progresivnější
než	než	k8xS	než
jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
samotných	samotný	k2eAgMnPc2d1	samotný
členů	člen	k1gMnPc2	člen
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
Oceanborn	Oceanborn	k1gInSc1	Oceanborn
technicky	technicky	k6eAd1	technicky
nejnáročnější	náročný	k2eAgNnSc4d3	nejnáročnější
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
natočili	natočit	k5eAaBmAgMnP	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
písních	píseň	k1gFnPc6	píseň
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
Tapio	Tapio	k1gMnSc1	Tapio
Wilska	Wilsek	k1gMnSc2	Wilsek
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Finntroll	Finntroll	k1gMnSc1	Finntroll
<g/>
)	)	kIx)	)
–	–	k?	–
Devil	Devil	k1gMnSc1	Devil
&	&	k?	&
The	The	k1gMnSc1	The
Deep	Deep	k1gMnSc1	Deep
Dark	Dark	k1gMnSc1	Dark
Ocean	Ocean	k1gMnSc1	Ocean
<g/>
,	,	kIx,	,
a	a	k8xC	a
The	The	k1gFnSc1	The
Pharaoh	Pharaoha	k1gFnPc2	Pharaoha
Sails	Sailsa	k1gFnPc2	Sailsa
to	ten	k3xDgNnSc4	ten
Orion	orion	k1gInSc1	orion
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finské	finský	k2eAgFnSc6d1	finská
hitparádě	hitparáda	k1gFnSc6	hitparáda
alb	album	k1gNnPc2	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Sacrament	Sacrament	k1gInSc1	Sacrament
of	of	k?	of
Wilderness	Wilderness	k1gInSc1	Wilderness
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgInS	skončit
okamžitě	okamžitě	k6eAd1	okamžitě
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
singlové	singlový	k2eAgFnSc6d1	singlová
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vydali	vydat	k5eAaPmAgMnP	vydat
<g/>
,	,	kIx,	,
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Walking	Walking	k1gInSc1	Walking
in	in	k?	in
the	the	k?	the
Air	Air	k1gFnSc2	Air
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
předělávka	předělávka	k1gFnSc1	předělávka
písně	píseň	k1gFnSc2	píseň
Howarda	Howard	k1gMnSc2	Howard
Blakea	Blakeus	k1gMnSc2	Blakeus
ze	z	k7c2	z
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
The	The	k1gMnSc1	The
Snowman	Snowman	k1gMnSc1	Snowman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vydali	vydat	k5eAaPmAgMnP	vydat
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
singl	singl	k1gInSc1	singl
Sleeping	Sleeping	k1gInSc4	Sleeping
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
Four	Four	k1gMnSc1	Four
Ballads	Balladsa	k1gFnPc2	Balladsa
of	of	k?	of
the	the	k?	the
Eclipse	Eclipse	k1gFnSc2	Eclipse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
prodalo	prodat	k5eAaPmAgNnS	prodat
15	[number]	k4	15
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Oceanborn	Oceanborna	k1gFnPc2	Oceanborna
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
stalo	stát	k5eAaPmAgNnS	stát
zlatým	zlatý	k2eAgInSc7d1	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Nightwish	Nightwish	k1gMnSc1	Nightwish
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Sleepwalker	Sleepwalker	k1gInSc4	Sleepwalker
účastnili	účastnit	k5eAaImAgMnP	účastnit
finské	finský	k2eAgFnPc4d1	finská
kvalifikace	kvalifikace	k1gFnPc4	kvalifikace
na	na	k7c4	na
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
<g/>
.	.	kIx.	.
</s>
<s>
Skončili	skončit	k5eAaPmAgMnP	skončit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
veřejné	veřejný	k2eAgNnSc4d1	veřejné
hlasování	hlasování	k1gNnSc4	hlasování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
porota	porota	k1gFnSc1	porota
to	ten	k3xDgNnSc4	ten
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
a	a	k8xC	a
Finsko	Finsko	k1gNnSc4	Finsko
tehdy	tehdy	k6eAd1	tehdy
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
Nina	Nina	k1gFnSc1	Nina
Å	Å	k?	Å
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
finská	finský	k2eAgFnSc1d1	finská
skupina	skupina	k1gFnSc1	skupina
Lordi	lord	k1gMnPc1	lord
celou	celý	k2eAgFnSc4d1	celá
Eurovizi	Eurovize	k1gFnSc4	Eurovize
<g/>
,	,	kIx,	,
ptali	ptat	k5eAaImAgMnP	ptat
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
Tuomase	Tuomas	k1gInSc6	Tuomas
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nightwish	Nightwish	k1gInSc4	Nightwish
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
lepší	dobrý	k2eAgInPc4d2	lepší
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
obě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
spadají	spadat	k5eAaPmIp3nP	spadat
pod	pod	k7c4	pod
metalový	metalový	k2eAgInSc4d1	metalový
žánr	žánr	k1gInSc4	žánr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuomas	Tuomas	k1gMnSc1	Tuomas
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
toho	ten	k3xDgMnSc4	ten
nelituje	litovat	k5eNaImIp3nS	litovat
<g/>
;	;	kIx,	;
Lordi	lord	k1gMnPc1	lord
jsou	být	k5eAaImIp3nP	být
prý	prý	k9	prý
skvělí	skvělý	k2eAgMnPc1d1	skvělý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pyšný	pyšný	k2eAgMnSc1d1	pyšný
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohli	moct	k5eAaImAgMnP	moct
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Finsko	Finsko	k1gNnSc1	Finsko
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
album	album	k1gNnSc4	album
Wishmaster	Wishmastra	k1gFnPc2	Wishmastra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
okamžitě	okamžitě	k6eAd1	okamžitě
vyletělo	vyletět	k5eAaPmAgNnS	vyletět
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
alb	album	k1gNnPc2	album
a	a	k8xC	a
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
tam	tam	k6eAd1	tam
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
oceněno	ocenit	k5eAaPmNgNnS	ocenit
jako	jako	k8xC	jako
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k6eAd1	navzdory
dlouho	dlouho	k6eAd1	dlouho
očekávaným	očekávaný	k2eAgNnSc7d1	očekávané
vydáním	vydání	k1gNnSc7	vydání
alb	alba	k1gFnPc2	alba
skupin	skupina	k1gFnPc2	skupina
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
či	či	k8xC	či
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
Wishmaster	Wishmaster	k1gInSc1	Wishmaster
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
časopise	časopis	k1gInSc6	časopis
Rock	rock	k1gInSc1	rock
Hard	Hard	k1gInSc1	Hard
titul	titul	k1gInSc4	titul
album	album	k1gNnSc4	album
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
nahráli	nahrát	k5eAaPmAgMnP	nahrát
předělávku	předělávka	k1gFnSc4	předělávka
písně	píseň	k1gFnSc2	píseň
Garyho	Gary	k1gMnSc2	Gary
Moora	Moor	k1gMnSc2	Moor
<g/>
,	,	kIx,	,
Over	Over	k1gInSc1	Over
the	the	k?	the
Hills	Hills	k1gInSc1	Hills
and	and	k?	and
Far	fara	k1gFnPc2	fara
Away	Awa	k2eAgFnPc4d1	Awa
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgFnPc7d1	další
novými	nový	k2eAgFnPc7d1	nová
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
upravenou	upravený	k2eAgFnSc7d1	upravená
verzí	verze	k1gFnSc7	verze
jedné	jeden	k4xCgFnSc2	jeden
starší	starší	k1gMnSc5	starší
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k9	jako
EP	EP	kA	EP
Over	Over	k1gInSc1	Over
the	the	k?	the
Hills	Hills	k1gInSc1	Hills
and	and	k?	and
Far	fara	k1gFnPc2	fara
Away	Awaa	k1gFnPc4	Awaa
<g/>
.	.	kIx.	.
</s>
<s>
Objevili	objevit	k5eAaPmAgMnP	objevit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
hosté	host	k1gMnPc1	host
jako	jako	k8xS	jako
Tony	Tony	k1gFnSc1	Tony
Kakko	Kakko	k1gNnSc4	Kakko
(	(	kIx(	(
<g/>
z	z	k7c2	z
power	powra	k1gFnPc2	powra
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Sonata	Sonata	k1gFnSc1	Sonata
Arctica	Arctica	k1gFnSc1	Arctica
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Tapio	Tapio	k1gNnSc1	Tapio
Wilska	Wilsk	k1gInSc2	Wilsk
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
také	také	k6eAd1	také
vydala	vydat	k5eAaPmAgFnS	vydat
VHS	VHS	kA	VHS
<g/>
,	,	kIx,	,
<g/>
DVD	DVD	kA	DVD
a	a	k8xC	a
CD	CD	kA	CD
s	s	k7c7	s
živým	živý	k2eAgInSc7d1	živý
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
nahraným	nahraný	k2eAgNnSc7d1	nahrané
při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
Tampere	Tamper	k1gInSc5	Tamper
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
jako	jako	k8xS	jako
From	From	k1gMnSc1	From
Wishes	Wishes	k1gMnSc1	Wishes
to	ten	k3xDgNnSc4	ten
Eternity	eternit	k1gInPc5	eternit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Tuomas	Tuomas	k1gMnSc1	Tuomas
basáka	basák	k1gMnSc2	basák
Samiho	Sami	k1gMnSc2	Sami
Vänsku	Vänsek	k1gInSc2	Vänsek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
Nightwish	Nightwish	k1gMnSc1	Nightwish
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
Marco	Marco	k1gMnSc1	Marco
Hietala	Hietal	k1gMnSc2	Hietal
(	(	kIx(	(
<g/>
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Tarot	Tarot	k1gInSc1	Tarot
<g/>
;	;	kIx,	;
bývalý	bývalý	k2eAgInSc1d1	bývalý
člen	člen	k1gInSc1	člen
Sinergy	Sinerg	k1gInPc1	Sinerg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgMnSc3	ten
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
Sinergy	Sinerg	k1gInPc4	Sinerg
<g/>
.	.	kIx.	.
</s>
<s>
Marco	Marco	k6eAd1	Marco
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
že	že	k9	že
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
zpívat	zpívat	k5eAaImF	zpívat
mužské	mužský	k2eAgInPc4d1	mužský
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Tuomas	Tuomas	k1gMnSc1	Tuomas
potom	potom	k6eAd1	potom
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
Samim	Samim	k1gMnSc1	Samim
stále	stále	k6eAd1	stále
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
už	už	k6eAd1	už
nesetkali	setkat	k5eNaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
už	už	k6eAd1	už
spolu	spolu	k6eAd1	spolu
nechtějí	chtít	k5eNaImIp3nP	chtít
dělat	dělat	k5eAaImF	dělat
žádný	žádný	k3yNgInSc4	žádný
další	další	k2eAgInSc4d1	další
projekt	projekt	k1gInSc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vydali	vydat	k5eAaPmAgMnP	vydat
Nightwish	Nightwish	k1gInSc4	Nightwish
album	album	k1gNnSc4	album
Century	Centura	k1gFnSc2	Centura
Child	Childa	k1gFnPc2	Childa
a	a	k8xC	a
singly	singl	k1gInPc4	singl
Ever	Evera	k1gFnPc2	Evera
Dream	Dream	k1gInSc1	Dream
a	a	k8xC	a
Bless	Bless	k1gInSc1	Bless
the	the	k?	the
Child	Child	k1gInSc1	Child
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
tímto	tento	k3xDgInSc7	tento
a	a	k8xC	a
předchozími	předchozí	k2eAgNnPc7d1	předchozí
alby	album	k1gNnPc7	album
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
Bless	Blessa	k1gFnPc2	Blessa
the	the	k?	the
Child	Child	k1gMnSc1	Child
<g/>
,	,	kIx,	,
Feel	Feel	k1gMnSc1	Feel
For	forum	k1gNnPc2	forum
You	You	k1gMnPc1	You
<g/>
,	,	kIx,	,
Ever	Ever	k1gMnSc1	Ever
Dream	Dream	k1gInSc1	Dream
a	a	k8xC	a
Beauty	Beaut	k2eAgInPc1d1	Beaut
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gInSc1	Beast
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
finský	finský	k2eAgInSc1d1	finský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
existující	existující	k2eAgInSc1d1	existující
pocit	pocit	k1gInSc1	pocit
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Trvalým	trvalý	k2eAgInSc7d1	trvalý
favoritem	favorit	k1gInSc7	favorit
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
nightwishácká	nightwishácký	k2eAgFnSc1d1	nightwishácký
<g/>
"	"	kIx"	"
verze	verze	k1gFnSc1	verze
písně	píseň	k1gFnSc2	píseň
The	The	k1gFnSc2	The
Phantom	Phantom	k1gInSc1	Phantom
of	of	k?	of
the	the	k?	the
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
ze	z	k7c2	z
slavného	slavný	k2eAgInSc2d1	slavný
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
muzikálu	muzikál	k1gInSc2	muzikál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Andrew	Andrew	k1gMnSc1	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webber	Webber	k1gMnSc1	Webber
<g/>
.	.	kIx.	.
</s>
<s>
Century	Centura	k1gFnPc4	Centura
Child	Childa	k1gFnPc2	Childa
získalo	získat	k5eAaPmAgNnS	získat
zlato	zlato	k1gNnSc4	zlato
už	už	k6eAd1	už
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
platinovým	platinový	k2eAgMnSc7d1	platinový
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
udělalo	udělat	k5eAaPmAgNnS	udělat
rekord	rekord	k1gInSc4	rekord
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
žebříčku	žebříček	k1gInSc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
<g/>
;	;	kIx,	;
ještě	ještě	k9	ještě
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
nebylo	být	k5eNaImAgNnS	být
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
prvním	první	k4xOgInSc7	první
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
Bless	Blessa	k1gFnPc2	Blessa
the	the	k?	the
Child	Childa	k1gFnPc2	Childa
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
druhé	druhý	k4xOgNnSc4	druhý
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
bez	bez	k7c2	bez
vydání	vydání	k1gNnSc2	vydání
singlu	singl	k1gInSc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
vybrána	vybrán	k2eAgFnSc1d1	vybrána
píseň	píseň	k1gFnSc1	píseň
End	End	k1gMnSc2	End
of	of	k?	of
All	All	k1gMnSc2	All
Hope	Hop	k1gMnSc2	Hop
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
finského	finský	k2eAgInSc2d1	finský
filmu	film	k1gInSc2	film
Kohtalon	Kohtalon	k1gInSc1	Kohtalon
Kirja	Kirja	k1gFnSc1	Kirja
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Kniha	kniha	k1gFnSc1	kniha
Zkázy	zkáza	k1gFnSc2	zkáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
své	svůj	k3xOyFgFnPc4	svůj
druhé	druhý	k4xOgFnPc4	druhý
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
End	End	k1gMnSc2	End
of	of	k?	of
Innocence	Innocenec	k1gMnSc2	Innocenec
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
kapely	kapela	k1gFnSc2	kapela
ústy	ústa	k1gNnPc7	ústa
Holopainena	Holopaineno	k1gNnSc2	Holopaineno
a	a	k8xC	a
Nevalainen	Nevalainna	k1gFnPc2	Nevalainna
<g/>
,	,	kIx,	,
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
části	část	k1gFnPc4	část
živých	živý	k2eAgInPc2d1	živý
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
stopáž	stopáž	k1gFnSc4	stopáž
atd.	atd.	kA	atd.
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
2003	[number]	k4	2003
se	se	k3xPyFc4	se
také	také	k9	také
Tarja	Tarja	k1gFnSc1	Tarja
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
,	,	kIx,	,
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
tu	tu	k6eAd1	tu
i	i	k9	i
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dohady	dohad	k1gInPc1	dohad
se	se	k3xPyFc4	se
však	však	k9	však
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
mylné	mylný	k2eAgFnPc1d1	mylná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skupina	skupina	k1gFnSc1	skupina
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
koncertech	koncert	k1gInPc6	koncert
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc1d1	další
rok	rok	k1gInSc4	rok
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
Once	Once	k1gNnSc1	Once
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
Nemo	Nemo	k6eAd1	Nemo
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc4	singl
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
pěti	pět	k4xCc6	pět
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nemo	Nemo	k6eAd1	Nemo
tedy	tedy	k9	tedy
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jejich	jejich	k3xOp3gNnSc2	jejich
doposud	doposud	k6eAd1	doposud
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
vydaným	vydaný	k2eAgInSc7d1	vydaný
singlem	singl	k1gInSc7	singl
<g/>
.	.	kIx.	.
</s>
<s>
Once	Once	k1gFnSc1	Once
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
devíti	devět	k4xCc6	devět
skladbách	skladba	k1gFnPc6	skladba
celého	celý	k2eAgInSc2d1	celý
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Century	Centura	k1gFnSc2	Centura
Child	Childa	k1gFnPc2	Childa
se	se	k3xPyFc4	se
Nightwish	Nightwish	k1gInSc1	Nightwish
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
tentokrát	tentokrát	k6eAd1	tentokrát
najít	najít	k5eAaPmF	najít
orchestr	orchestr	k1gInSc4	orchestr
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgMnS	být
vybrán	vybrán	k2eAgMnSc1d1	vybrán
London	London	k1gMnSc1	London
Session	Session	k1gInSc4	Session
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jejich	jejich	k3xOp3gNnSc1	jejich
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
píseň	píseň	k1gFnSc1	píseň
napsaná	napsaný	k2eAgFnSc1d1	napsaná
celá	celý	k2eAgFnSc1d1	celá
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
<g/>
,	,	kIx,	,
Kuolema	Kuolem	k1gMnSc2	Kuolem
Tekee	Teke	k1gMnSc2	Teke
Taiteilijan	Taiteilijan	k1gMnSc1	Taiteilijan
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Smrt	smrt	k1gFnSc1	smrt
dělá	dělat	k5eAaImIp3nS	dělat
umělce	umělec	k1gMnPc4	umělec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Once	Once	k1gNnSc1	Once
získalo	získat	k5eAaPmAgNnS	získat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
třikrát	třikrát	k6eAd1	třikrát
platinu	platina	k1gFnSc4	platina
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
platinu	platina	k1gFnSc4	platina
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zlatým	zlatý	k1gInSc7	zlatý
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
singly	singl	k1gInPc1	singl
byly	být	k5eAaImAgInP	být
<g/>
:	:	kIx,	:
Wish	Wish	k1gInSc1	Wish
I	i	k9	i
Had	had	k1gMnSc1	had
an	an	k?	an
Angel	angel	k1gMnSc1	angel
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
na	na	k7c6	na
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Alone	Alon	k1gInSc5	Alon
in	in	k?	in
the	the	k?	the
Dark	Dark	k1gInSc1	Dark
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kuolema	Kuolema	k1gFnSc1	Kuolema
Tekee	Tekee	k1gNnSc2	Tekee
Taiteilijan	Taiteilijany	k1gInPc2	Taiteilijany
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
vydaný	vydaný	k2eAgInSc1d1	vydaný
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc4	The
Siren	Sirna	k1gFnPc2	Sirna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
bylo	být	k5eAaImAgNnS	být
Once	Once	k1gNnSc1	Once
bylo	být	k5eAaImAgNnS	být
dobře	dobře	k6eAd1	dobře
přijato	přijmout	k5eAaPmNgNnS	přijmout
i	i	k9	i
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
mnoho	mnoho	k6eAd1	mnoho
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
recenzí	recenze	k1gFnPc2	recenze
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
na	na	k7c4	na
Metalfan	Metalfan	k1gInSc4	Metalfan
<g/>
.	.	kIx.	.
<g/>
nl	nl	k?	nl
nebo	nebo	k8xC	nebo
na	na	k7c4	na
RockReport	RockReport	k1gInSc4	RockReport
<g/>
.	.	kIx.	.
<g/>
be	be	k?	be
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Oceanborn	Oceanborna	k1gFnPc2	Oceanborna
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
alba	album	k1gNnSc2	album
kapele	kapela	k1gFnSc3	kapela
umožnil	umožnit	k5eAaPmAgInS	umožnit
jet	jet	k2eAgInSc1d1	jet
Once	Once	k1gInSc1	Once
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
nikdy	nikdy	k6eAd1	nikdy
nebyli	být	k5eNaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Nightwish	Nightwish	k1gInSc4	Nightwish
také	také	k9	také
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
Světového	světový	k2eAgInSc2d1	světový
Šampionátu	šampionát	k1gInSc2	šampionát
v	v	k7c6	v
Atletice	atletika	k1gFnSc6	atletika
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zvýraznili	zvýraznit	k5eAaPmAgMnP	zvýraznit
svou	svůj	k3xOyFgFnSc4	svůj
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nyní	nyní	k6eAd1	nyní
dostávala	dostávat	k5eAaImAgFnS	dostávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
vydali	vydat	k5eAaPmAgMnP	vydat
"	"	kIx"	"
<g/>
best	best	k1gInSc1	best
of	of	k?	of
<g/>
"	"	kIx"	"
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
celé	celá	k1gFnSc2	celá
jejich	jejich	k3xOp3gFnSc2	jejich
diskografie	diskografie	k1gFnSc2	diskografie
<g/>
.	.	kIx.	.
</s>
<s>
Kompilace	kompilace	k1gFnSc1	kompilace
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
Highest	Highest	k1gFnSc1	Highest
Hopes	Hopesa	k1gFnPc2	Hopesa
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
objevila	objevit	k5eAaPmAgFnS	objevit
živá	živý	k2eAgFnSc1d1	živá
předělávka	předělávka	k1gFnSc1	předělávka
songu	song	k1gInSc2	song
od	od	k7c2	od
Pink	pink	k2eAgInPc2d1	pink
Floydů	Floyd	k1gInPc2	Floyd
<g/>
,	,	kIx,	,
High	Higha	k1gFnPc2	Higha
Hopes	Hopesa	k1gFnPc2	Hopesa
(	(	kIx(	(
<g/>
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc4	první
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zpíval	zpívat	k5eAaImAgMnS	zpívat
Marco	Marco	k1gMnSc1	Marco
Hietala	Hietal	k1gMnSc2	Hietal
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
High	Higha	k1gFnPc2	Higha
Hopes	Hopesa	k1gFnPc2	Hopesa
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
albu	album	k1gNnSc6	album
i	i	k9	i
vylepšená	vylepšený	k2eAgFnSc1d1	vylepšená
verze	verze	k1gFnSc1	verze
Sleeping	Sleeping	k1gInSc1	Sleeping
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
z	z	k7c2	z
alba	album	k1gNnSc2	album
Oceanborn	Oceanborna	k1gFnPc2	Oceanborna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
i	i	k9	i
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
bylo	být	k5eAaImAgNnS	být
natočeno	natočen	k2eAgNnSc1d1	natočeno
nové	nový	k2eAgNnSc1d1	nové
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
středověká	středověký	k2eAgFnSc1d1	středověká
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
i	i	k8xC	i
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
verzi	verze	k1gFnSc6	verze
singlu	singl	k1gInSc2	singl
a	a	k8xC	a
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgFnSc2d1	samostatná
DVD	DVD	kA	DVD
vydané	vydaný	k2eAgFnSc2d1	vydaná
Spinefarm	Spinefarm	k1gInSc4	Spinefarm
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
DVD	DVD	kA	DVD
End	End	k1gMnSc2	End
of	of	k?	of
an	an	k?	an
Era	Era	k1gMnPc7	Era
(	(	kIx(	(
<g/>
vydaného	vydaný	k2eAgNnSc2d1	vydané
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
v	v	k7c4	v
Hartwall	Hartwall	k1gInSc4	Hartwall
aréně	aréna	k1gFnSc3	aréna
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ostatní	ostatní	k2eAgMnPc1d1	ostatní
čtyři	čtyři	k4xCgMnPc1	čtyři
členové	člen	k1gMnPc1	člen
Nightwish	Nightwish	k1gInSc4	Nightwish
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
pokračovat	pokračovat	k5eAaImF	pokračovat
bez	bez	k1gInSc4	bez
Tarji	Tarje	k1gFnSc4	Tarje
Turunen	Turunen	k2eAgInSc1d1	Turunen
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
vyznělo	vyznět	k5eAaImAgNnS	vyznět
z	z	k7c2	z
otevřeného	otevřený	k2eAgInSc2d1	otevřený
dopisu	dopis	k1gInSc2	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dal	dát	k5eAaPmAgInS	dát
Tarje	Tarj	k1gInPc4	Tarj
Tuomas	Tuomasa	k1gFnPc2	Tuomasa
Holopainen	Holopainna	k1gFnPc2	Holopainna
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
koncertu	koncert	k1gInSc2	koncert
a	a	k8xC	a
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
důvod	důvod	k1gInSc1	důvod
k	k	k7c3	k
vyhazovu	vyhazov	k1gInSc3	vyhazov
Tarji	Tarje	k1gFnSc4	Tarje
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
dopisu	dopis	k1gInSc2	dopis
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
argentinský	argentinský	k2eAgMnSc1d1	argentinský
podnikatel	podnikatel	k1gMnSc1	podnikatel
Marcelo	Marcela	k1gFnSc5	Marcela
Cabuli	Cabule	k1gFnSc4	Cabule
<g/>
,	,	kIx,	,
a	a	k8xC	a
komerční	komerční	k2eAgInPc4d1	komerční
zájmy	zájem	k1gInPc4	zájem
změnily	změnit	k5eAaPmAgFnP	změnit
její	její	k3xOp3gInSc4	její
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
fanoušci	fanoušek	k1gMnPc1	fanoušek
si	se	k3xPyFc3	se
jako	jako	k9	jako
znak	znak	k1gInSc1	znak
Nightwish	Nightwish	k1gMnSc1	Nightwish
představovali	představovat	k5eAaImAgMnP	představovat
Tarju	Tarju	k1gMnPc4	Tarju
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tarja	Tarja	k1gFnSc1	Tarja
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
stahuje	stahovat	k5eAaImIp3nS	stahovat
všechnu	všechen	k3xTgFnSc4	všechen
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
pryč	pryč	k6eAd1	pryč
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Tarja	Tarja	k6eAd1	Tarja
se	se	k3xPyFc4	se
k	k	k7c3	k
incidentu	incident	k1gInSc3	incident
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhazov	vyhazov	k1gInSc1	vyhazov
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgInS	být
šok	šok	k1gInSc1	šok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
než	než	k8xS	než
jí	on	k3xPp3gFnSc3	on
dali	dát	k5eAaPmAgMnP	dát
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
neupozornil	upozornit	k5eNaPmAgMnS	upozornit
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
osobní	osobní	k2eAgInPc1d1	osobní
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
byly	být	k5eAaImAgFnP	být
neoprávněné	oprávněný	k2eNgFnPc1d1	neoprávněná
a	a	k8xC	a
že	že	k8xS	že
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
článku	článek	k1gInSc2	článek
médiím	médium	k1gNnPc3	médium
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
zbytečně	zbytečně	k6eAd1	zbytečně
kruté	krutý	k2eAgNnSc1d1	kruté
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
pocity	pocit	k1gInPc4	pocit
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
otevřeném	otevřený	k2eAgInSc6d1	otevřený
dopise	dopis	k1gInSc6	dopis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejněn	k2eAgMnSc1d1	uveřejněn
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
osobní	osobní	k2eAgFnSc6d1	osobní
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
a	a	k8xC	a
skrz	skrz	k7c4	skrz
rozhovory	rozhovor	k1gInPc4	rozhovor
v	v	k7c6	v
TV	TV	kA	TV
<g/>
,	,	kIx,	,
časopisech	časopis	k1gInPc6	časopis
a	a	k8xC	a
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
Novou	nový	k2eAgFnSc4d1	nová
Éru	éra	k1gFnSc4	éra
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
historie	historie	k1gFnSc1	historie
skupiny	skupina	k1gFnSc2	skupina
od	od	k7c2	od
příchodu	příchod	k1gInSc2	příchod
nové	nový	k2eAgFnSc2d1	nová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Anette	Anett	k1gInSc5	Anett
Olzon	Olzon	k1gInSc1	Olzon
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
náhrady	náhrada	k1gFnSc2	náhrada
za	za	k7c4	za
Tarju	Tarju	k1gFnSc4	Tarju
Turunen	Turunna	k1gFnPc2	Turunna
jako	jako	k8xC	jako
ženské	ženský	k2eAgFnSc2d1	ženská
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
skupiny	skupina	k1gFnSc2	skupina
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
všem	všecek	k3xTgFnPc3	všecek
zpěvačkám	zpěvačka	k1gFnPc3	zpěvačka
<g/>
,	,	kIx,	,
zajímajícím	zajímající	k2eAgFnPc3d1	zajímající
se	se	k3xPyFc4	se
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
<g/>
,	,	kIx,	,
poslat	poslat	k5eAaPmF	poslat
kapele	kapela	k1gFnSc3	kapela
své	svůj	k3xOyFgFnSc2	svůj
demonahrávky	demonahrávka	k1gFnSc2	demonahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hledají	hledat	k5eAaImIp3nP	hledat
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
zpívat	zpívat	k5eAaImF	zpívat
dynamicky	dynamicky	k6eAd1	dynamicky
a	a	k8xC	a
všestranně	všestranně	k6eAd1	všestranně
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
k	k	k7c3	k
překvapení	překvapení	k1gNnSc3	překvapení
některých	některý	k3yIgMnPc2	některý
fanoušků	fanoušek	k1gMnPc2	fanoušek
speciálně	speciálně	k6eAd1	speciálně
neptala	ptat	k5eNaImAgFnS	ptat
po	po	k7c6	po
zpěvačkách	zpěvačka	k1gFnPc6	zpěvačka
trénovaných	trénovaný	k2eAgFnPc6d1	trénovaná
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
otevřeni	otevřen	k2eAgMnPc1d1	otevřen
každému	každý	k3xTgInSc3	každý
stylu	styl	k1gInSc3	styl
a	a	k8xC	a
hudební	hudební	k2eAgFnSc3d1	hudební
minulosti	minulost	k1gFnSc3	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
posílání	posílání	k1gNnSc1	posílání
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
kapela	kapela	k1gFnSc1	kapela
přes	přes	k7c4	přes
2000	[number]	k4	2000
demo	demo	k2eAgFnPc2d1	demo
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
eventuálně	eventuálně	k6eAd1	eventuálně
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Tarju	Tarju	k1gFnSc4	Tarju
jako	jako	k8xC	jako
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
dohady	dohad	k1gInPc4	dohad
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
kapela	kapela	k1gFnSc1	kapela
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
fanoušci	fanoušek	k1gMnPc1	fanoušek
nevěřili	věřit	k5eNaImAgMnP	věřit
žádným	žádný	k3yNgMnSc7	žádný
informacím	informace	k1gFnPc3	informace
ohledně	ohledně	k7c2	ohledně
nové	nový	k2eAgFnSc2d1	nová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
než	než	k8xS	než
těm	ten	k3xDgNnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zveřejní	zveřejnit	k5eAaPmIp3nS	zveřejnit
skupina	skupina	k1gFnSc1	skupina
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Identita	identita	k1gFnSc1	identita
nové	nový	k2eAgFnSc2d1	nová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
pouze	pouze	k6eAd1	pouze
den	den	k1gInSc4	den
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
prvního	první	k4xOgInSc2	první
singlu	singl	k1gInSc2	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Dark	Darka	k1gFnPc2	Darka
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
náhradnicí	náhradnice	k1gFnSc7	náhradnice
Turunen	Turunna	k1gFnPc2	Turunna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
35	[number]	k4	35
<g/>
letá	letý	k2eAgFnSc1d1	letá
Anette	Anett	k1gInSc5	Anett
Olzon	Olzon	k1gInSc1	Olzon
z	z	k7c2	z
Katrineholmu	Katrineholm	k1gInSc2	Katrineholm
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Identita	identita	k1gFnSc1	identita
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
ohlášena	ohlášet	k5eAaImNgFnS	ohlášet
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
bála	bát	k5eAaImAgFnS	bát
falešných	falešný	k2eAgFnPc2d1	falešná
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgFnP	začít
množit	množit	k5eAaImF	množit
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zavřela	zavřít	k5eAaPmAgFnS	zavřít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahrála	nahrát	k5eAaPmAgFnS	nahrát
své	svůj	k3xOyFgFnPc4	svůj
šesté	šestý	k4xOgNnSc1	šestý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
zatím	zatím	k6eAd1	zatím
bez	bez	k7c2	bez
nové	nový	k2eAgFnSc2d1	nová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
začal	začít	k5eAaPmAgInS	začít
nahráváním	nahrávání	k1gNnSc7	nahrávání
bicích	bicí	k2eAgNnPc2d1	bicí
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
baskytar	baskytara	k1gFnPc2	baskytara
a	a	k8xC	a
demo	demo	k2eAgFnPc2d1	demo
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
nahrávání	nahrávání	k1gNnSc1	nahrávání
orchestru	orchestr	k1gInSc2	orchestr
<g/>
/	/	kIx~	/
<g/>
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
nahrány	nahrát	k5eAaPmNgInP	nahrát
konečné	konečný	k2eAgInPc1d1	konečný
syntetizéry	syntetizér	k1gInPc1	syntetizér
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
píseň	píseň	k1gFnSc1	píseň
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgFnPc7	tři
ukázkami	ukázka	k1gFnPc7	ukázka
z	z	k7c2	z
nadcházejícího	nadcházející	k2eAgNnSc2d1	nadcházející
alba	album	k1gNnSc2	album
<g/>
:	:	kIx,	:
7	[number]	k4	7
Days	Daysa	k1gFnPc2	Daysa
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Wolves	Wolves	k1gInSc1	Wolves
<g/>
,	,	kIx,	,
Master	master	k1gMnSc1	master
Passion	Passion	k1gInSc4	Passion
Greed	Greed	k1gMnSc1	Greed
a	a	k8xC	a
Amaranth	Amaranth	k1gMnSc1	Amaranth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
singl	singl	k1gInSc1	singl
vydán	vydán	k2eAgInSc1d1	vydán
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
úniku	únik	k1gInSc3	únik
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
britskou	britský	k2eAgFnSc4d1	britská
stránku	stránka	k1gFnSc4	stránka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
už	už	k9	už
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
"	"	kIx"	"
<g/>
Dark	Dark	k1gInSc1	Dark
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
World	World	k1gMnSc1	World
Tour	Tour	k1gInSc1	Tour
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
připravovat	připravovat	k5eAaImF	připravovat
první	první	k4xOgInSc4	první
live	live	k1gInSc4	live
album	album	k1gNnSc4	album
s	s	k7c7	s
Anette	Anett	k1gInSc5	Anett
Made	Made	k1gNnSc3	Made
in	in	k?	in
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
(	(	kIx(	(
<g/>
and	and	k?	and
in	in	k?	in
various	various	k1gMnSc1	various
other	other	k1gMnSc1	other
places	places	k1gMnSc1	places
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
Dark	Darka	k1gFnPc2	Darka
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
a	a	k8xC	a
ze	z	k7c2	z
singlů	singl	k1gInPc2	singl
Amaranth	Amaranth	k1gInSc1	Amaranth
<g/>
,	,	kIx,	,
Bye	Bye	k1gFnSc1	Bye
Bye	Bye	k1gMnSc1	Bye
Beautiful	Beautiful	k1gInSc1	Beautiful
a	a	k8xC	a
The	The	k1gMnSc1	The
Islander	Islander	k1gMnSc1	Islander
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
CD	CD	kA	CD
zároveň	zároveň	k6eAd1	zároveň
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k8xC	i
DVD	DVD	kA	DVD
obsahující	obsahující	k2eAgInSc1d1	obsahující
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
z	z	k7c2	z
"	"	kIx"	"
<g/>
Dark	Darka	k1gFnPc2	Darka
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
World	World	k1gMnSc1	World
Tour	Tour	k1gInSc1	Tour
<g/>
"	"	kIx"	"
a	a	k8xC	a
3	[number]	k4	3
videoklipy	videoklip	k1gInPc1	videoklip
k	k	k7c3	k
singlům	singl	k1gInPc3	singl
Amaranth	Amarantha	k1gFnPc2	Amarantha
<g/>
,	,	kIx,	,
Bye	Bye	k1gFnSc1	Bye
Bye	Bye	k1gMnSc1	Bye
Beautiful	Beautiful	k1gInSc1	Beautiful
a	a	k8xC	a
The	The	k1gMnSc1	The
Islander	Islander	k1gMnSc1	Islander
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
v	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
časopise	časopis	k1gInSc6	časopis
Soundi	Sound	k1gMnPc1	Sound
hlavní	hlavní	k2eAgFnSc2d1	hlavní
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Tuomas	Tuomas	k1gMnSc1	Tuomas
Holopainen	Holopainen	k1gInSc4	Holopainen
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
skupiny	skupina	k1gFnSc2	skupina
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
šířit	šířit	k5eAaImF	šířit
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
ponese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Wind	Wind	k1gMnSc1	Wind
Embraced	Embraced	k1gMnSc1	Embraced
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tyto	tento	k3xDgFnPc1	tento
zprávy	zpráva	k1gFnPc1	zpráva
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Anette	Anett	k1gInSc5	Anett
Olzon	Olzon	k1gInSc4	Olzon
popřela	popřít	k5eAaPmAgFnS	popřít
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
Anette	Anett	k1gInSc5	Anett
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
napsala	napsat	k5eAaPmAgFnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
je	být	k5eAaImIp3nS	být
hotovo	hotov	k2eAgNnSc1d1	hotovo
devět	devět	k4xCc1	devět
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
sejde	sejít	k5eAaPmIp3nS	sejít
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
demo	demo	k2eAgFnPc2d1	demo
verzí	verze	k1gFnPc2	verze
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
ponese	ponést	k5eAaPmIp3nS	ponést
název	název	k1gInSc4	název
Imaginaerum	Imaginaerum	k1gInSc1	Imaginaerum
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
bude	být	k5eAaImBp3nS	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k9	i
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
vyšel	vyjít	k5eAaPmAgInS	vyjít
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
soundtrackem	soundtrack	k1gInSc7	soundtrack
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
filmu	film	k1gInSc3	film
a	a	k8xC	a
prolíná	prolínat	k5eAaImIp3nS	prolínat
se	se	k3xPyFc4	se
tak	tak	k9	tak
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
příběhem	příběh	k1gInSc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
starém	starý	k2eAgInSc6d1	starý
umírajícím	umírající	k2eAgInSc6d1	umírající
hudebním	hudební	k2eAgInSc6d1	hudební
skladateli	skladatel	k1gMnSc3	skladatel
a	a	k8xC	a
poetovi	poet	k1gMnSc3	poet
Thomasi	Thomas	k1gMnSc3	Thomas
Whitmanovi	Whitman	k1gMnSc3	Whitman
(	(	kIx(	(
<g/>
kombinace	kombinace	k1gFnSc1	kombinace
jmen	jméno	k1gNnPc2	jméno
Tuomase	Tuomasa	k1gFnSc3	Tuomasa
Holopainena	Holopainen	k1gMnSc2	Holopainen
a	a	k8xC	a
amerického	americký	k2eAgMnSc2d1	americký
basníka	basník	k1gMnSc2	basník
Walta	Walt	k1gMnSc2	Walt
Whitmana	Whitman	k1gMnSc2	Whitman
-	-	kIx~	-
Tuomasova	Tuomasův	k2eAgMnSc4d1	Tuomasův
oblíbeného	oblíbený	k2eAgMnSc4d1	oblíbený
autora	autor	k1gMnSc4	autor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
a	a	k8xC	a
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc1	film
režíruje	režírovat	k5eAaImIp3nS	režírovat
Stobe	Stob	k1gMnSc5	Stob
Harju	Harju	k1gMnSc5	Harju
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
již	již	k6eAd1	již
s	s	k7c7	s
Nightiwish	Nightiwisha	k1gFnPc2	Nightiwisha
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c6	na
klipu	klip	k1gInSc6	klip
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
The	The	k1gFnSc1	The
Islander	Islander	k1gMnSc1	Islander
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
skupina	skupina	k1gFnSc1	skupina
skrze	skrze	k?	skrze
svůj	svůj	k3xOyFgInSc4	svůj
profil	profil	k1gInSc4	profil
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
oznámila	oznámit	k5eAaPmAgFnS	oznámit
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
s	s	k7c7	s
Anette	Anett	k1gInSc5	Anett
o	o	k7c6	o
konci	konec	k1gInSc6	konec
jejího	její	k3xOp3gNnSc2	její
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tiskového	tiskový	k2eAgNnSc2d1	tiskové
prohlášení	prohlášení	k1gNnSc2	prohlášení
"	"	kIx"	"
<g/>
bylo	být	k5eAaImAgNnS	být
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
,	,	kIx,	,
že	že	k8xS	že
směřování	směřování	k1gNnSc1	směřování
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
s	s	k7c7	s
jejími	její	k3xOp3gFnPc7	její
potřebami	potřeba	k1gFnPc7	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
konflikt	konflikt	k1gInSc1	konflikt
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
turné	turné	k1gNnSc2	turné
Imaginaerum	Imaginaerum	k1gNnSc1	Imaginaerum
World	World	k1gInSc1	World
Tour	Tour	k1gMnSc1	Tour
Anette	Anett	k1gInSc5	Anett
zastoupila	zastoupit	k5eAaPmAgFnS	zastoupit
Nizozemka	Nizozemka	k1gFnSc1	Nizozemka
Floor	Floor	k1gMnSc1	Floor
Jansen	Jansen	k1gInSc1	Jansen
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
stala	stát	k5eAaPmAgFnS	stát
oficiální	oficiální	k2eAgFnSc7d1	oficiální
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
přijat	přijmout	k5eAaPmNgInS	přijmout
i	i	k9	i
Troy	Tro	k2eAgInPc1d1	Tro
Donockley	Donockley	k1gInPc1	Donockley
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
skladby	skladba	k1gFnSc2	skladba
Nightwish	Nightwish	k1gInSc1	Nightwish
na	na	k7c4	na
flétny	flétna	k1gFnPc4	flétna
a	a	k8xC	a
dudy	dudy	k1gFnPc4	dudy
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
toto	tento	k3xDgNnSc4	tento
oznámila	oznámit	k5eAaPmAgFnS	oznámit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
27	[number]	k4	27
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Argentině	Argentina	k1gFnSc3	Argentina
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vydatelství	vydatelství	k1gNnSc2	vydatelství
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
inspirováno	inspirován	k2eAgNnSc1d1	inspirováno
prací	práce	k1gFnSc7	práce
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
motiv	motiv	k1gInSc1	motiv
posloužil	posloužit	k5eAaPmAgInS	posloužit
slavný	slavný	k2eAgInSc4d1	slavný
Darwinův	Darwinův	k2eAgInSc4d1	Darwinův
citát	citát	k1gInSc4	citát
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
vlivné	vlivný	k2eAgFnSc2d1	vlivná
knihy	kniha	k1gFnSc2	kniha
–	–	k?	–
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pochází	pocházet	k5eAaImIp3nS	pocházet
citace	citace	k1gFnPc4	citace
"	"	kIx"	"
<g/>
Nekonečné	konečný	k2eNgFnPc4d1	nekonečná
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejkrásnější	krásný	k2eAgFnPc1d3	nejkrásnější
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
Darwin	Darwin	k1gMnSc1	Darwin
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
výstižně	výstižně	k6eAd1	výstižně
popsal	popsat	k5eAaPmAgMnS	popsat
vývoj	vývoj	k1gInSc4	vývoj
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tuomase	Tuomasa	k1gFnSc3	Tuomasa
Holopainena	Holopainena	k1gFnSc1	Holopainena
byla	být	k5eAaImAgFnS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
alba	alba	k1gFnSc1	alba
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Élan	Élan	k1gInSc1	Élan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
citát	citát	k1gInSc4	citát
Walta	Walt	k1gMnSc2	Walt
Whitmana	Whitman	k1gMnSc2	Whitman
<g/>
.	.	kIx.	.
</s>
<s>
Endless	Endless	k6eAd1	Endless
Forms	Forms	k1gInSc1	Forms
Most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc4	Beautiful
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
bubeník	bubeník	k1gMnSc1	bubeník
Jukka	Jukka	k1gMnSc1	Jukka
Nevalainen	Nevalainen	k1gInSc4	Nevalainen
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
Kaiem	Kaium	k1gNnSc7	Kaium
Hahto	Hahto	k1gNnSc1	Hahto
<g/>
.	.	kIx.	.
</s>
<s>
Tuomas	Tuomas	k1gInSc1	Tuomas
Holopainen	Holopainen	k1gInSc1	Holopainen
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
většiny	většina	k1gFnSc2	většina
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgFnSc4d3	veliký
inspiraci	inspirace	k1gFnSc4	inspirace
ke	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
písní	píseň	k1gFnPc2	píseň
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc3d1	filmová
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
jako	jako	k8xC	jako
Beauty	Beaut	k1gInPc1	Beaut
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gFnSc1	Beast
(	(	kIx(	(
<g/>
z	z	k7c2	z
alba	album	k1gNnSc2	album
Century	Centura	k1gFnSc2	Centura
Child	Child	k1gMnSc1	Child
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ghost	Ghost	k1gFnSc1	Ghost
Love	lov	k1gInSc5	lov
Score	Scor	k1gInSc5	Scor
(	(	kIx(	(
<g/>
z	z	k7c2	z
alba	album	k1gNnSc2	album
Once	Onc	k1gInSc2	Onc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
toho	ten	k3xDgNnSc2	ten
příkladem	příklad	k1gInSc7	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Holopainen	Holopainen	k2eAgMnSc1d1	Holopainen
také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmová	filmový	k2eAgFnSc1d1	filmová
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
například	například	k6eAd1	například
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Van	vana	k1gFnPc2	vana
Helsing	Helsing	k1gInSc1	Helsing
<g/>
,	,	kIx,	,
Krvavý	krvavý	k2eAgInSc1d1	krvavý
Příliv	příliv	k1gInSc1	příliv
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
napsal	napsat	k5eAaBmAgMnS	napsat
Hans	Hans	k1gMnSc1	Hans
Zimmer	Zimmer	k1gMnSc1	Zimmer
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
měli	mít	k5eAaImAgMnP	mít
také	také	k6eAd1	také
např.	např.	kA	např.
experimentální	experimentální	k2eAgFnSc7d1	experimentální
The	The	k1gFnSc7	The
3	[number]	k4	3
<g/>
rd	rd	k?	rd
and	and	k?	and
the	the	k?	the
Mortal	Mortal	k1gMnSc1	Mortal
či	či	k8xC	či
gothic	gothic	k1gMnSc1	gothic
metaloví	metalový	k2eAgMnPc1d1	metalový
Tiamat	Tiamat	k1gInSc4	Tiamat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgMnS	být
také	také	k9	také
Nightwish	Nightwish	k1gMnSc1	Nightwish
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
kapely	kapela	k1gFnPc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
Simons	Simons	k1gInSc1	Simons
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Epicy	Epica	k1gFnSc2	Epica
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
začala	začít	k5eAaPmAgFnS	začít
zpívat	zpívat	k5eAaImF	zpívat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Nightwish	Nightwish	k1gInSc1	Nightwish
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Visions	Visions	k1gInSc1	Visions
of	of	k?	of
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
Nicole	Nicole	k1gFnSc1	Nicole
Bogner	Bognra	k1gFnPc2	Bognra
<g/>
,	,	kIx,	,
uznala	uznat	k5eAaPmAgFnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nightwish	Nightwish	k1gInSc1	Nightwish
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byl	být	k5eAaImAgMnS	být
velkou	velká	k1gFnSc4	velká
inspirací	inspirace	k1gFnPc2	inspirace
na	na	k7c4	na
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Sander	Sandra	k1gFnPc2	Sandra
Gommans	Gommansa	k1gFnPc2	Gommansa
z	z	k7c2	z
After	Aftra	k1gFnPc2	Aftra
Forever	Forever	k1gMnSc1	Forever
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nightwish	Nightwish	k1gInSc1	Nightwish
"	"	kIx"	"
<g/>
nás	my	k3xPp1nPc2	my
budou	být	k5eAaImBp3nP	být
jistě	jistě	k9	jistě
inspirovat	inspirovat	k5eAaBmF	inspirovat
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
zpěvák	zpěvák	k1gMnSc1	zpěvák
power	power	k1gMnSc1	power
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Sonata	Sonat	k1gMnSc2	Sonat
Arctica	Arcticus	k1gMnSc2	Arcticus
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc5	Tony
Kakko	Kakka	k1gMnSc5	Kakka
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
několikrát	několikrát	k6eAd1	několikrát
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
Nightwish	Nightwish	k1gInSc1	Nightwish
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Správná	správný	k2eAgFnSc1d1	správná
definice	definice	k1gFnSc1	definice
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zčásti	zčásti	k6eAd1	zčásti
heavy	heav	k1gMnPc4	heav
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
symphonic	symphonice	k1gFnPc2	symphonice
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Symphonic	Symphonice	k1gFnPc2	Symphonice
metalová	metalový	k2eAgFnSc1d1	metalová
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nightwish	Nightwish	k1gInSc1	Nightwish
používají	používat	k5eAaImIp3nP	používat
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
styl	styl	k1gInSc4	styl
zpěvu	zpěv	k1gInSc2	zpěv
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
filmových	filmový	k2eAgFnPc2d1	filmová
melodií	melodie	k1gFnPc2	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Povznášející	povznášející	k2eAgInPc1d1	povznášející
fantasy	fantas	k1gInPc1	fantas
témata	téma	k1gNnPc1	téma
<g/>
,	,	kIx,	,
epická	epický	k2eAgFnSc1d1	epická
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgInPc1d1	vysoký
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytarová	kytarový	k2eAgNnPc1d1	kytarové
a	a	k8xC	a
klávesová	klávesový	k2eAgNnPc1d1	klávesové
sóla	sólo	k1gNnPc1	sólo
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
melodický	melodický	k2eAgInSc1d1	melodický
styl	styl	k1gInSc1	styl
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
finské	finský	k2eAgFnPc4d1	finská
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Sonatu	Sonat	k2eAgFnSc4d1	Sonata
Arcticu	Arctica	k1gFnSc4	Arctica
či	či	k8xC	či
Stratovarius	Stratovarius	k1gInSc4	Stratovarius
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
žánru	žánr	k1gInSc2	žánr
jako	jako	k8xS	jako
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
především	především	k9	především
velkou	velký	k2eAgFnSc7d1	velká
melodičností	melodičnost	k1gFnSc7	melodičnost
a	a	k8xC	a
vysoko	vysoko	k6eAd1	vysoko
položeným	položený	k2eAgInSc7d1	položený
<g/>
,	,	kIx,	,
čistým	čistý	k2eAgInSc7d1	čistý
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
poměrně	poměrně	k6eAd1	poměrně
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
objevují	objevovat	k5eAaImIp3nP	objevovat
sborově	sborově	k6eAd1	sborově
zpívané	zpívaný	k2eAgMnPc4d1	zpívaný
refrény-že	refrény-že	k6eAd1	refrény-že
skupina	skupina	k1gFnSc1	skupina
hraje	hrát	k5eAaImIp3nS	hrát
melodický	melodický	k2eAgInSc4d1	melodický
heavy	heav	k1gInPc7	heav
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
<g/>
řekli	říct	k5eAaPmAgMnP	říct
sami	sám	k3xTgMnPc1	sám
členové	člen	k1gMnPc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Tuomas	Tuomas	k1gInSc1	Tuomas
Holopainen	Holopainna	k1gFnPc2	Holopainna
jednou	jednou	k6eAd1	jednou
popsal	popsat	k5eAaPmAgMnS	popsat
hudbu	hudba	k1gFnSc4	hudba
Nightwish	Nightwish	k1gMnSc1	Nightwish
jako	jako	k9	jako
"	"	kIx"	"
<g/>
melodic	melodic	k1gMnSc1	melodic
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgMnS	metat
s	s	k7c7	s
ženskou	ženský	k2eAgFnSc7d1	ženská
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
éra	éra	k1gFnSc1	éra
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
albem	album	k1gNnSc7	album
Century	Centura	k1gFnSc2	Centura
Child	Childa	k1gFnPc2	Childa
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
na	na	k7c4	na
Once	Once	k1gFnSc4	Once
<g/>
.	.	kIx.	.
</s>
<s>
Kytary	kytara	k1gFnPc1	kytara
byly	být	k5eAaImAgFnP	být
více	hodně	k6eAd2	hodně
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zjednodušeny	zjednodušit	k5eAaPmNgFnP	zjednodušit
na	na	k7c4	na
nižší	nízký	k2eAgInSc4d2	nižší
rytmický	rytmický	k2eAgInSc4d1	rytmický
zvuk	zvuk	k1gInSc4	zvuk
blízký	blízký	k2eAgInSc4d1	blízký
spíše	spíše	k9	spíše
modernímu	moderní	k2eAgNnSc3d1	moderní
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
než	než	k8xS	než
starému	starý	k2eAgMnSc3d1	starý
melodickému	melodický	k2eAgMnSc3d1	melodický
power	power	k1gInSc1	power
metalovému	metalový	k2eAgInSc3d1	metalový
stylu	styl	k1gInSc3	styl
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInPc1d1	hlavní
Tarjiny	Tarjin	k2eAgInPc1d1	Tarjin
vokály	vokál	k1gInPc1	vokál
ztratily	ztratit	k5eAaPmAgInP	ztratit
něco	něco	k6eAd1	něco
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
operního	operní	k2eAgInSc2d1	operní
řízu	říz	k1gInSc2	říz
a	a	k8xC	a
přiblížily	přiblížit	k5eAaPmAgFnP	přiblížit
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
standardnímu	standardní	k2eAgInSc3d1	standardní
rockovému	rockový	k2eAgInSc3d1	rockový
ženskému	ženský	k2eAgInSc3d1	ženský
zpěvu	zpěv	k1gInSc3	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
zpěv	zpěv	k1gInSc1	zpěv
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nového	nový	k2eAgMnSc2d1	nový
basáka	basák	k1gMnSc2	basák
Marca	Marcum	k1gNnSc2	Marcum
Hietaly	Hietal	k1gMnPc4	Hietal
<g/>
,	,	kIx,	,
přidal	přidat	k5eAaPmAgMnS	přidat
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
písní	píseň	k1gFnPc2	píseň
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
tvrdší	tvrdý	k2eAgInSc4d2	tvrdší
rozměr	rozměr	k1gInSc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Zasněná	zasněný	k2eAgFnSc1d1	zasněná
fantasy	fantas	k1gInPc1	fantas
témata	téma	k1gNnPc4	téma
starších	starý	k2eAgFnPc2d2	starší
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
gothičtějšími	gothický	k2eAgInPc7d2	gothický
obrazy	obraz	k1gInPc7	obraz
děje	dít	k5eAaImIp3nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Tuomas	Tuomas	k1gInSc4	Tuomas
Holopainen	Holopainen	k2eAgInSc4d1	Holopainen
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
rozhovoru	rozhovor	k1gInSc6	rozhovor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
hodně	hodně	k6eAd1	hodně
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
albem	album	k1gNnSc7	album
posledním	poslední	k2eAgNnSc7d1	poslední
(	(	kIx(	(
<g/>
Once	Once	k1gNnSc7	Once
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
například	například	k6eAd1	například
zachová	zachovat	k5eAaPmIp3nS	zachovat
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
tvrdších	tvrdý	k2eAgFnPc2d2	tvrdší
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Wish	Wish	k1gMnSc1	Wish
I	i	k8xC	i
Had	had	k1gMnSc1	had
An	An	k1gMnSc1	An
Angel	angel	k1gMnSc1	angel
nebo	nebo	k8xC	nebo
Planet	planeta	k1gFnPc2	planeta
Hell	Hella	k1gFnPc2	Hella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
budou	být	k5eAaImBp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
jemnější	jemný	k2eAgFnSc2d2	jemnější
balady	balada	k1gFnSc2	balada
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mnoho	mnoho	k4c1	mnoho
hostujících	hostující	k2eAgMnPc2d1	hostující
umělců	umělec	k1gMnPc2	umělec
orchestrálních	orchestrální	k2eAgFnPc2d1	orchestrální
částí	část	k1gFnPc2	část
-	-	kIx~	-
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
rozhovoru	rozhovor	k1gInSc6	rozhovor
Tuomas	Tuomas	k1gMnSc1	Tuomas
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
Once	Onc	k1gInSc2	Onc
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
18	[number]	k4	18
sboristů	sborista	k1gMnPc2	sborista
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
to	ten	k3xDgNnSc1	ten
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Angels	Angels	k6eAd1	Angels
Fall	Fall	k1gMnSc1	Fall
First	First	k1gMnSc1	First
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Oceanborn	Oceanborna	k1gFnPc2	Oceanborna
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Wishmaster	Wishmastra	k1gFnPc2	Wishmastra
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Over	Over	k1gInSc1	Over
the	the	k?	the
Hills	Hills	k1gInSc1	Hills
and	and	k?	and
Far	fara	k1gFnPc2	fara
Away	Awaa	k1gFnSc2	Awaa
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Century	Centura	k1gFnSc2	Centura
Child	Child	k1gMnSc1	Child
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Once	Onc	k1gInSc2	Onc
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Dark	Dark	k1gInSc1	Dark
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Imaginaerum	Imaginaerum	k1gNnSc4	Imaginaerum
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Endless	Endless	k1gInSc1	Endless
Forms	Forms	k1gInSc1	Forms
Most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
From	From	k1gMnSc1	From
Wishes	Wishes	k1gMnSc1	Wishes
to	ten	k3xDgNnSc4	ten
Eternity	eternit	k1gInPc1	eternit
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
End	End	k1gFnSc1	End
of	of	k?	of
an	an	k?	an
Era	Era	k1gFnSc2	Era
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Made	Mad	k1gInSc2	Mad
in	in	k?	in
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
(	(	kIx(	(
<g/>
And	Anda	k1gFnPc2	Anda
in	in	k?	in
Various	Various	k1gMnSc1	Various
Other	Other	k1gMnSc1	Other
Places	Places	k1gMnSc1	Places
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Showtime	Showtim	k1gMnSc5	Showtim
<g/>
,	,	kIx,	,
Storytime	Storytim	k1gMnSc5	Storytim
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Vehicle	Vehicle	k1gInSc1	Vehicle
of	of	k?	of
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Carpenter	Carpenter	k1gMnSc1	Carpenter
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Sacrament	Sacrament	k1gInSc1	Sacrament
of	of	k?	of
Wilderness	Wilderness	k1gInSc1	Wilderness
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Walking	Walking	k1gInSc1	Walking
in	in	k?	in
the	the	k?	the
Air	Air	k1gFnSc2	Air
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Sleeping	Sleeping	k1gInSc1	Sleeping
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
Four	Four	k1gMnSc1	Four
Ballads	Balladsa	k1gFnPc2	Balladsa
of	of	k?	of
the	the	k?	the
Eclipse	Eclipse	k1gFnSc1	Eclipse
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Deep	Deep	k1gMnSc1	Deep
Silent	Silent	k1gMnSc1	Silent
Complete	Comple	k1gNnSc2	Comple
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Ever	Ever	k1gInSc1	Ever
Dream	Dream	k1gInSc1	Dream
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Bless	Bless	k1gInSc1	Bless
the	the	k?	the
Child	Child	k1gInSc1	Child
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Nemo	Nemo	k6eAd1	Nemo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Wish	Wish	k1gMnSc1	Wish
I	i	k8xC	i
Had	had	k1gMnSc1	had
an	an	k?	an
Angel	angel	k1gMnSc1	angel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Kuolema	Kuolema	k1gNnSc1	Kuolema
tekee	tekee	k1gFnPc2	tekee
taiteilijan	taiteilijana	k1gFnPc2	taiteilijana
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Siren	Siren	k1gInSc1	Siren
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Sleeping	Sleeping	k1gInSc1	Sleeping
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Eva	Eva	k1gFnSc1	Eva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Amaranth	Amaranth	k1gInSc1	Amaranth
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Erämaan	Erämaan	k1gInSc1	Erämaan
Viimeinen	Viimeinen	k1gInSc1	Viimeinen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Bye	Bye	k1gMnPc7	Bye
Bye	Bye	k1gFnSc2	Bye
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Islander	Islander	k1gMnSc1	Islander
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Storytime	Storytim	k1gMnSc5	Storytim
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Crow	Crow	k1gMnSc1	Crow
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Owl	Owl	k1gFnSc2	Owl
and	and	k?	and
the	the	k?	the
Dove	Dove	k1gInSc1	Dove
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Élan	Élan	k1gInSc1	Élan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Imaginaerum	Imaginaerum	k1gInSc1	Imaginaerum
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Imaginaerum	Imaginaerum	k1gNnSc1	Imaginaerum
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Score	Scor	k1gMnSc5	Scor
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
From	From	k1gMnSc1	From
Wishes	Wishes	k1gMnSc1	Wishes
to	ten	k3xDgNnSc4	ten
Eternity	eternit	k1gInPc1	eternit
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
videozáznam	videozáznam	k1gInSc1	videozáznam
koncertu	koncert	k1gInSc2	koncert
<g/>
)	)	kIx)	)
End	End	k1gFnSc1	End
of	of	k?	of
Innocence	Innocence	k1gFnSc1	Innocence
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
dokument	dokument	k1gInSc4	dokument
s	s	k7c7	s
videoklipy	videoklip	k1gInPc7	videoklip
<g/>
)	)	kIx)	)
Once	Once	k1gFnSc7	Once
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
audio	audio	k2eAgFnSc1d1	audio
+	+	kIx~	+
2	[number]	k4	2
videa	video	k1gNnSc2	video
<g/>
)	)	kIx)	)
End	End	k1gFnSc1	End
of	of	k?	of
an	an	k?	an
Era	Era	k1gFnSc2	Era
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
videozáznam	videozáznam	k1gInSc1	videozáznam
posledního	poslední	k2eAgInSc2d1	poslední
koncertu	koncert	k1gInSc2	koncert
s	s	k7c7	s
Tarjou	Tarja	k1gFnSc7	Tarja
+	+	kIx~	+
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
15	[number]	k4	15
dnech	den	k1gInPc6	den
před	před	k7c7	před
koncertem	koncert	k1gInSc7	koncert
<g/>
)	)	kIx)	)
Live	Live	k1gInSc1	Live
at	at	k?	at
Lowlands	Lowlands	k1gInSc1	Lowlands
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
asi	asi	k9	asi
první	první	k4xOgInSc1	první
videozáznam	videozáznam	k1gInSc1	videozáznam
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
"	"	kIx"	"
<g/>
všestrannou	všestranný	k2eAgFnSc7d1	všestranná
<g/>
"	"	kIx"	"
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Anette	Anett	k1gInSc5	Anett
Olzon	Olzon	k1gInSc1	Olzon
<g/>
)	)	kIx)	)
Made	Made	k1gInSc1	Made
in	in	k?	in
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
(	(	kIx(	(
<g/>
And	Anda	k1gFnPc2	Anda
in	in	k?	in
Various	Various	k1gMnSc1	Various
Other	Other	k1gMnSc1	Other
Places	Places	k1gMnSc1	Places
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dokument	dokument	k1gInSc1	dokument
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
Nightwish	Nightwisha	k1gFnPc2	Nightwisha
+	+	kIx~	+
videa	video	k1gNnSc2	video
ke	k	k7c3	k
třem	tři	k4xCgFnPc3	tři
<g />
.	.	kIx.	.
</s>
<s>
singlům	singl	k1gInPc3	singl
z	z	k7c2	z
DPP	DPP	kA	DPP
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Showtime	Showtim	k1gMnSc5	Showtim
<g/>
,	,	kIx,	,
Storytime	Storytim	k1gMnSc5	Storytim
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Highest	Highest	k1gMnSc1	Highest
Hopes	Hopes	k1gMnSc1	Hopes
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
Nightwish	Nightwish	k1gMnSc1	Nightwish
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Wishmastour	Wishmastoura	k1gFnPc2	Wishmastoura
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tales	Tales	k1gMnSc1	Tales
from	from	k1gMnSc1	from
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Elvenpath	Elvenpath	k1gInSc1	Elvenpath
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Bestwishes	Bestwishesa	k1gFnPc2	Bestwishesa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Highest	Highest	k1gMnSc1	Highest
Hopes	Hopes	k1gMnSc1	Hopes
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Nightwish	Nightwish	k1gMnSc1	Nightwish
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Sound	Sound	k1gMnSc1	Sound
of	of	k?	of
Nightwish	Nightwish	k1gMnSc1	Nightwish
Reborn	Reborn	k1gMnSc1	Reborn
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Lokikirja	Lokikirj	k1gInSc2	Lokikirj
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Decades	Decadesa	k1gFnPc2	Decadesa
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
Tuomas	Tuomas	k1gInSc4	Tuomas
Holopainen	Holopainen	k2eAgInSc4d1	Holopainen
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
Marco	Marco	k1gMnSc1	Marco
Hietala	Hietal	k1gMnSc2	Hietal
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Erno	Erno	k1gNnSc1	Erno
"	"	kIx"	"
<g/>
Emppu	Empp	k1gInSc2	Empp
<g/>
"	"	kIx"	"
Vuorinen	Vuorinen	k1gInSc1	Vuorinen
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Jukka	Jukka	k1gMnSc1	Jukka
Nevalainen	Nevalainen	k1gInSc1	Nevalainen
-	-	kIx~	-
bicí	bicí	k2eAgInSc1d1	bicí
Floor	Floor	k1gInSc1	Floor
Jansen	Jansen	k2eAgInSc1d1	Jansen
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Troy	Troa	k1gFnSc2	Troa
Donockley	Donockle	k2eAgInPc4d1	Donockle
-	-	kIx~	-
lidové	lidový	k2eAgInPc4d1	lidový
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
Tarja	Tarj	k1gInSc2	Tarj
Turunen	Turunen	k2eAgInSc4d1	Turunen
-	-	kIx~	-
zpěv	zpěv	k1gInSc4	zpěv
Anette	Anett	k1gInSc5	Anett
Olzon	Olzon	k1gInSc4	Olzon
-	-	kIx~	-
zpěv	zpěv	k1gInSc4	zpěv
Sami	sám	k3xTgMnPc1	sám
Vänskä	Vänskä	k1gMnPc1	Vänskä
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
</s>
