<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
Windsor	Windsor	k1gInSc1	Windsor
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc4	hrad
Windsor	Windsor	k1gInSc4	Windsor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Windsor	Windsor	k1gInSc1	Windsor
Castle	Castle	k1gFnSc2	Castle
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc4d3	veliký
doposud	doposud	k6eAd1	doposud
obývaný	obývaný	k2eAgInSc4d1	obývaný
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
