<p>
<s>
Windsor	Windsor	k1gInSc4	Windsor
je	být	k5eAaImIp3nS	být
malebné	malebný	k2eAgNnSc1d1	malebné
<g/>
,	,	kIx,	,
turisty	turist	k1gMnPc4	turist
hojně	hojně	k6eAd1	hojně
navštěvované	navštěvovaný	k2eAgNnSc1d1	navštěvované
městečko	městečko	k1gNnSc1	městečko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Berkshire	Berkshir	k1gInSc5	Berkshir
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
–	–	k?	–
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
Windsor	Windsor	k1gInSc1	Windsor
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc4	hrad
Windsor	Windsor	k1gInSc4	Windsor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Windsor	Windsor	k1gInSc1	Windsor
Castle	Castle	k1gFnSc2	Castle
<g/>
)	)	kIx)	)
–	–	k?	–
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc4d3	veliký
doposud	doposud	k6eAd1	doposud
obývaný	obývaný	k2eAgInSc4d1	obývaný
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
jiného	jiný	k2eAgNnSc2d1	jiné
známého	známý	k2eAgNnSc2d1	známé
městečka	městečko	k1gNnSc2	městečko
–	–	k?	–
Etonu	Eton	k1gInSc2	Eton
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
slavná	slavný	k2eAgFnSc1d1	slavná
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
Eton	Etona	k1gFnPc2	Etona
College	Colleg	k1gInSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Pouhé	pouhý	k2eAgFnPc1d1	pouhá
3	[number]	k4	3
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
leží	ležet	k5eAaImIp3nS	ležet
vesnice	vesnice	k1gFnSc1	vesnice
Old	Olda	k1gFnPc2	Olda
Windsor	Windsor	k1gInSc4	Windsor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
asi	asi	k9	asi
300	[number]	k4	300
let	léto	k1gNnPc2	léto
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
dnes	dnes	k6eAd1	dnes
známe	znát	k5eAaImIp1nP	znát
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
Windsor	Windsor	k1gInSc1	Windsor
nazýván	nazýván	k2eAgMnSc1d1	nazýván
New	New	k1gMnSc1	New
Windsor	Windsor	k1gInSc4	Windsor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
obou	dva	k4xCgInPc2	dva
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
nejranější	raný	k2eAgFnSc6d3	nejranější
historii	historie	k1gFnSc6	historie
místa	místo	k1gNnSc2	místo
mnoho	mnoho	k6eAd1	mnoho
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
mnoho	mnoho	k6eAd1	mnoho
let	let	k1gInSc4	let
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
kroniky	kronika	k1gFnPc1	kronika
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
splavná	splavný	k2eAgFnSc1d1	splavná
řeka	řeka	k1gFnSc1	řeka
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
strategicky	strategicky	k6eAd1	strategicky
umístěným	umístěný	k2eAgInSc7d1	umístěný
kopcem	kopec	k1gInSc7	kopec
přála	přát	k5eAaImAgFnS	přát
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
osídlení	osídlení	k1gNnSc6	osídlení
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dokládají	dokládat	k5eAaImIp3nP	dokládat
to	ten	k3xDgNnSc1	ten
četné	četný	k2eAgInPc1d1	četný
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
jako	jako	k8xC	jako
například	například	k6eAd1	například
paleolitické	paleolitický	k2eAgFnPc1d1	paleolitická
sekyrky	sekyrka	k1gFnPc1	sekyrka
<g/>
,	,	kIx,	,
neolitické	neolitický	k2eAgFnPc1d1	neolitická
pazourky	pazourka	k1gFnPc1	pazourka
nebo	nebo	k8xC	nebo
meče	meč	k1gInPc1	meč
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
a	a	k8xC	a
brož	brož	k1gFnSc4	brož
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatků	pozůstatek	k1gInPc2	pozůstatek
z	z	k7c2	z
období	období	k1gNnSc2	období
římské	římský	k2eAgFnSc2d1	římská
nadvlády	nadvláda	k1gFnSc2	nadvláda
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
existují	existovat	k5eAaImIp3nP	existovat
četné	četný	k2eAgInPc4d1	četný
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
oblasti	oblast	k1gFnSc2	oblast
anglosaskými	anglosaský	k2eAgInPc7d1	anglosaský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
již	již	k6eAd1	již
v	v	k7c6	v
anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
kronice	kronika	k1gFnSc6	kronika
z	z	k7c2	z
devátého	devátý	k4xOgNnSc2	devátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
osada	osada	k1gFnSc1	osada
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
od	od	k7c2	od
dnešního	dnešní	k2eAgNnSc2d1	dnešní
umístění	umístění	k1gNnSc2	umístění
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
městečko	městečko	k1gNnSc4	městečko
Old	Olda	k1gFnPc2	Olda
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
Normanů	Norman	k1gMnPc2	Norman
(	(	kIx(	(
<g/>
1066	[number]	k4	1066
<g/>
)	)	kIx)	)
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Viléma	Vilém	k1gMnSc2	Vilém
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1086	[number]	k4	1086
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
I.	I.	kA	I.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sídlo	sídlo	k1gNnSc1	sídlo
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
přemístilo	přemístit	k5eAaPmAgNnS	přemístit
tři	tři	k4xCgFnPc4	tři
míle	míle	k1gFnPc4	míle
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Domesday	Domesdaa	k1gFnSc2	Domesdaa
Book	Book	k1gInSc1	Book
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
jako	jako	k9	jako
'	'	kIx"	'
<g/>
Windsor	Windsor	k1gInSc1	Windsor
Castle	Castle	k1gFnSc2	Castle
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
dvanáctém	dvanáctý	k4xOgNnSc6	dvanáctý
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Old	Olda	k1gFnPc2	Olda
Windsor	Windsor	k1gInSc1	Windsor
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
trh	trh	k1gInSc1	trh
<g/>
,	,	kIx,	,
most	most	k1gInSc1	most
a	a	k8xC	a
nemocnice	nemocnice	k1gFnSc1	nemocnice
pro	pro	k7c4	pro
malomocné	malomocný	k1gMnPc4	malomocný
však	však	k8xC	však
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
až	až	k9	až
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
kvůli	kvůli	k7c3	kvůli
následnictví	následnictví	k1gNnSc3	následnictví
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
zhruba	zhruba	k6eAd1	zhruba
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1170	[number]	k4	1170
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
hradu	hrad	k1gInSc2	hrad
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
most	most	k1gInSc1	most
Windsor	Windsor	k1gInSc1	Windsor
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
most	most	k1gInSc1	most
na	na	k7c4	na
Temži	Temže	k1gFnSc4	Temže
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Staines	Staines	k1gMnSc1	Staines
a	a	k8xC	a
Reading	Reading	k1gInSc1	Reading
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
nebylo	být	k5eNaImAgNnS	být
budování	budování	k1gNnSc4	budování
mostů	most	k1gInPc2	most
zdaleka	zdaleka	k6eAd1	zdaleka
častým	častý	k2eAgInSc7d1	častý
jevem	jev	k1gInSc7	jev
a	a	k8xC	a
most	most	k1gInSc1	most
hrál	hrát	k5eAaImAgInS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
dopravní	dopravní	k2eAgFnSc6d1	dopravní
infrastruktuře	infrastruktura	k1gFnSc6	infrastruktura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
spojoval	spojovat	k5eAaImAgMnS	spojovat
Londýn	Londýn	k1gInSc4	Londýn
s	s	k7c7	s
městy	město	k1gNnPc7	město
Reading	Reading	k1gInSc1	Reading
a	a	k8xC	a
Winchester	Winchester	k1gInSc1	Winchester
a	a	k8xC	a
také	také	k9	také
odklonil	odklonit	k5eAaPmAgMnS	odklonit
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
jeho	jeho	k3xOp3gInSc4	jeho
rozmach	rozmach	k1gInSc4	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třináctém	třináctý	k4xOgNnSc6	třináctý
století	století	k1gNnSc6	století
získal	získat	k5eAaPmAgInS	získat
Windsor	Windsor	k1gInSc1	Windsor
status	status	k1gInSc4	status
obchodního	obchodní	k2eAgNnSc2d1	obchodní
města	město	k1gNnSc2	město
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgNnSc2	týž
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1332	[number]	k4	1332
se	se	k3xPyFc4	se
New	New	k1gMnSc1	New
Windsor	Windsor	k1gInSc4	Windsor
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
padesát	padesát	k4xCc4	padesát
nejbohatších	bohatý	k2eAgNnPc2d3	nejbohatší
měst	město	k1gNnPc2	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
prosperitu	prosperita	k1gFnSc4	prosperita
město	město	k1gNnSc4	město
vděčilo	vděčit	k5eAaImAgNnS	vděčit
především	především	k9	především
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Opakované	opakovaný	k2eAgFnPc1d1	opakovaná
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
hradu	hrad	k1gInSc2	hrad
přivedly	přivést	k5eAaPmAgFnP	přivést
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
obchodníky	obchodník	k1gMnPc4	obchodník
(	(	kIx(	(
<g/>
zlatníky	zlatník	k1gMnPc4	zlatník
<g/>
,	,	kIx,	,
obchodníky	obchodník	k1gMnPc4	obchodník
s	s	k7c7	s
vínem	víno	k1gNnSc7	víno
<g/>
,	,	kIx,	,
kořením	koření	k1gNnSc7	koření
a	a	k8xC	a
látkami	látka	k1gFnPc7	látka
<g/>
)	)	kIx)	)
a	a	k8xC	a
daly	dát	k5eAaPmAgFnP	dát
lidem	člověk	k1gMnPc3	člověk
z	z	k7c2	z
města	město	k1gNnSc2	město
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1	stavební
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Edwarda	Edward	k1gMnSc2	Edward
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
projektem	projekt	k1gInSc7	projekt
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
světské	světský	k2eAgFnSc2d1	světská
budovy	budova	k1gFnSc2	budova
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
utratil	utratit	k5eAaPmAgInS	utratit
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
hradu	hrad	k1gInSc2	hrad
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
než	než	k8xS	než
na	na	k7c6	na
jakémkoli	jakýkoli	k3yIgNnSc6	jakýkoli
jiném	jiný	k2eAgNnSc6d1	jiné
královském	královský	k2eAgNnSc6d1	královské
sídle	sídlo	k1gNnSc6	sídlo
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
přestavby	přestavba	k1gFnSc2	přestavba
Westminsterského	Westminsterský	k2eAgNnSc2d1	Westminsterské
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
drastickému	drastický	k2eAgNnSc3d1	drastické
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
až	až	k9	až
50	[number]	k4	50
<g/>
%	%	kIx~	%
snížení	snížení	k1gNnSc3	snížení
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
černého	černý	k2eAgInSc2d1	černý
moru	mor	k1gInSc2	mor
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
30	[number]	k4	30
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projekt	projekt	k1gInSc1	projekt
výstavby	výstavba	k1gFnSc2	výstavba
krále	král	k1gMnSc2	král
Edwarda	Edward	k1gMnSc2	Edward
III	III	kA	III
<g/>
.	.	kIx.	.
do	do	k7c2	do
města	město	k1gNnSc2	město
přilákal	přilákat	k5eAaPmAgInS	přilákat
spoustu	spousta	k1gFnSc4	spousta
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
populace	populace	k1gFnSc2	populace
města	město	k1gNnSc2	město
tak	tak	k6eAd1	tak
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
až	až	k9	až
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
<g/>
.	.	kIx.	.
</s>
<s>
Morové	morový	k2eAgFnPc1d1	morová
epidemie	epidemie	k1gFnPc1	epidemie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1361	[number]	k4	1361
-	-	kIx~	-
1372	[number]	k4	1372
způsobily	způsobit	k5eAaPmAgFnP	způsobit
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozmach	rozmach	k1gInSc4	rozmach
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
sjížděli	sjíždět	k5eAaImAgMnP	sjíždět
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
i	i	k8xC	i
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
štědře	štědro	k6eAd1	štědro
financované	financovaný	k2eAgInPc1d1	financovaný
z	z	k7c2	z
královské	královský	k2eAgFnSc2d1	královská
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
,	,	kIx,	,
vydělat	vydělat	k5eAaPmF	vydělat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1391	[number]	k4	1391
zde	zde	k6eAd1	zde
pracoval	pracovat	k5eAaImAgInS	pracovat
jako	jako	k8xC	jako
účetní	účetní	k1gMnPc1	účetní
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
hradu	hrad	k1gInSc6	hrad
Windsor	Windsor	k1gInSc4	Windsor
básník	básník	k1gMnSc1	básník
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
Chaucer	Chaucer	k1gMnSc1	Chaucer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
během	během	k7c2	během
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Windsor	Windsor	k1gInSc1	Windsor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
poutníků	poutník	k1gMnPc2	poutník
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Přicházeli	přicházet	k5eAaImAgMnP	přicházet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
dotknout	dotknout	k5eAaPmF	dotknout
královské	královský	k2eAgFnSc2d1	královská
svatyně	svatyně	k1gFnSc2	svatyně
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
úlomku	úlomek	k1gInSc2	úlomek
Svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
v	v	k7c6	v
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnSc6d1	postavená
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
1480	[number]	k4	1480
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poutníci	poutník	k1gMnPc1	poutník
přicházeli	přicházet	k5eAaImAgMnP	přicházet
s	s	k7c7	s
kapsami	kapsa	k1gFnPc7	kapsa
plnými	plný	k2eAgFnPc7d1	plná
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Windsoru	Windsor	k1gInSc6	Windsor
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k7c2	okolo
29	[number]	k4	29
hostinců	hostinec	k1gInPc2	hostinec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
příchozí	příchozí	k1gMnPc1	příchozí
mohli	moct	k5eAaImAgMnP	moct
ubytovat	ubytovat	k5eAaPmF	ubytovat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
prosperovat	prosperovat	k5eAaImF	prosperovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Londýňany	Londýňan	k1gMnPc4	Londýňan
bylo	být	k5eAaImAgNnS	být
druhým	druhý	k4xOgNnSc7	druhý
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
po	po	k7c4	po
Canterbury	Canterbura	k1gFnPc4	Canterbura
a	a	k8xC	a
svatyni	svatyně	k1gFnSc4	svatyně
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
Becketa	Becket	k1gMnSc2	Becket
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
pohřben	pohřben	k2eAgMnSc1d1	pohřben
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
po	po	k7c6	po
boku	bok	k1gInSc6	bok
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Janě	Jana	k1gFnSc3	Jana
Seymourové	Seymourový	k2eAgFnSc2d1	Seymourová
<g/>
,	,	kIx,	,
matky	matka	k1gFnSc2	matka
jeho	on	k3xPp3gMnSc2	on
jediného	jediný	k2eAgMnSc2d1	jediný
legitimního	legitimní	k2eAgMnSc2d1	legitimní
syna	syn	k1gMnSc2	syn
Edwarda	Edward	k1gMnSc2	Edward
(	(	kIx(	(
<g/>
Edward	Edward	k1gMnSc1	Edward
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Přibližně	přibližně	k6eAd1	přibližně
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Reformaci	reformace	k1gFnSc6	reformace
začalo	začít	k5eAaPmAgNnS	začít
město	město	k1gNnSc1	město
stagnovat	stagnovat	k5eAaImF	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
staromódní	staromódní	k2eAgNnPc4d1	staromódní
a	a	k8xC	a
svatyně	svatyně	k1gFnSc2	svatyně
mrtvých	mrtvý	k1gMnPc2	mrtvý
za	za	k7c4	za
návštěvní	návštěvní	k2eAgNnPc4d1	návštěvní
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
pověrčivé	pověrčivý	k2eAgNnSc4d1	pověrčivé
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
Windsoru	Windsor	k1gInSc6	Windsor
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
udělaných	udělaný	k2eAgFnPc6d1	udělaná
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
nechvalném	chvalný	k2eNgNnSc6d1	nechvalné
bydlení	bydlení	k1gNnSc6	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Windsoru	Windsor	k1gInSc6	Windsor
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
i	i	k9	i
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
hra	hra	k1gFnSc1	hra
Veselé	Veselé	k2eAgFnSc2d1	Veselé
paničky	panička	k1gFnSc2	panička
windsorské	windsorský	k2eAgFnSc2d1	Windsorská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hojně	hojně	k6eAd1	hojně
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
a	a	k8xC	a
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Windsor	Windsor	k1gInSc1	Windsor
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
královské	královský	k2eAgFnSc6d1	královská
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
center	centrum	k1gNnPc2	centrum
obchodu	obchod	k1gInSc2	obchod
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1649	[number]	k4	1649
popraven	popravit	k5eAaPmNgMnS	popravit
ve	v	k7c6	v
Whitehallu	Whitehall	k1gInSc6	Whitehall
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
bez	bez	k7c2	bez
obřadu	obřad	k1gInSc2	obřad
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
začal	začít	k5eAaPmAgInS	začít
hrad	hrad	k1gInSc4	hrad
Windsor	Windsor	k1gInSc4	Windsor
opět	opět	k6eAd1	opět
obývat	obývat	k5eAaImF	obývat
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Jiří	Jiří	k1gMnSc1	Jiří
III	III	kA	III
<g/>
..	..	k?	..
Odstartoval	odstartovat	k5eAaPmAgMnS	odstartovat
novou	nový	k2eAgFnSc4d1	nová
éru	éra	k1gFnSc4	éra
rozvoje	rozvoj	k1gInSc2	rozvoj
Windsoru	Windsor	k1gInSc2	Windsor
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
kasáren	kasárny	k1gFnPc2	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
však	však	k9	však
spojen	spojit	k5eAaPmNgInS	spojit
velký	velký	k2eAgInSc1d1	velký
příliv	příliv	k1gInSc1	příliv
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
problému	problém	k1gInSc3	problém
s	s	k7c7	s
prostitucí	prostituce	k1gFnSc7	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
a	a	k8xC	a
zavedení	zavedení	k1gNnSc4	zavedení
železnice	železnice	k1gFnSc2	železnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
znamenal	znamenat	k5eAaImAgMnS	znamenat
nejdramatičtější	dramatický	k2eAgFnPc4d3	nejdramatičtější
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ospalého	ospalý	k2eAgNnSc2d1	ospalé
středověkého	středověký	k2eAgNnSc2d1	středověké
městečka	městečko	k1gNnSc2	městečko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
středisko	středisko	k1gNnSc1	středisko
celého	celý	k2eAgNnSc2d1	celé
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
-	-	kIx~	-
mnoho	mnoho	k4c4	mnoho
korunovaných	korunovaný	k2eAgFnPc2d1	korunovaná
hlav	hlava	k1gFnPc2	hlava
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
přijelo	přijet	k5eAaPmAgNnS	přijet
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
Windsoru	Windsor	k1gInSc2	Windsor
navštívit	navštívit	k5eAaPmF	navštívit
královnu	královna	k1gFnSc4	královna
Viktorii	Viktoria	k1gFnSc3	Viktoria
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnSc2	snaha
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
středověký	středověký	k2eAgInSc4d1	středověký
ráz	ráz	k1gInSc4	ráz
města	město	k1gNnSc2	město
však	však	k9	však
bohužel	bohužel	k6eAd1	bohužel
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
destrukci	destrukce	k1gFnSc3	destrukce
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
včetně	včetně	k7c2	včetně
demolice	demolice	k1gFnSc2	demolice
starého	starý	k2eAgInSc2d1	starý
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1180	[number]	k4	1180
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
ulic	ulice	k1gFnPc2	ulice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
dnes	dnes	k6eAd1	dnes
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
Peascod	Peascod	k1gInSc1	Peascod
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Windsor	Windsor	k1gInSc1	Windsor
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Windsor	Windsor	k1gInSc4	Windsor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turismus	turismus	k1gInSc4	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Windsor	Windsor	k1gInSc1	Windsor
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
hlavně	hlavně	k9	hlavně
hradem	hrad	k1gInSc7	hrad
Windsor	Windsor	k1gInSc1	Windsor
Castle	Castle	k1gFnSc2	Castle
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
rezidencí	rezidence	k1gFnPc2	rezidence
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
také	také	k9	také
získal	získat	k5eAaPmAgMnS	získat
některé	některý	k3yIgFnPc4	některý
výsady	výsada	k1gFnPc4	výsada
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
výstavbu	výstavba	k1gFnSc4	výstavba
dvou	dva	k4xCgFnPc2	dva
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
několika	několik	k4yIc2	několik
hotelů	hotel	k1gInPc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
Legoland	Legoland	k1gInSc1	Legoland
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
vedle	vedle	k7c2	vedle
Windsorského	windsorský	k2eAgNnSc2d1	windsorské
safari	safari	k1gNnSc2	safari
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
denně	denně	k6eAd1	denně
křižují	křižovat	k5eAaImIp3nP	křižovat
Temži	Temže	k1gFnSc4	Temže
říční	říční	k2eAgInPc1d1	říční
čluny	člun	k1gInPc1	člun
s	s	k7c7	s
turisty	turist	k1gMnPc7	turist
<g/>
.	.	kIx.	.
</s>
<s>
Windsorské	windsorský	k2eAgNnSc1d1	windsorské
kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
nádherný	nádherný	k2eAgInSc1d1	nádherný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
Eton	Eton	k1gNnSc4	Eton
i	i	k8xC	i
údolí	údolí	k1gNnSc4	údolí
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Kolo	kolo	k1gNnSc1	kolo
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
Alexandra	Alexandr	k1gMnSc2	Alexandr
Gardens	Gardens	k1gInSc1	Gardens
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
smontováno	smontován	k2eAgNnSc1d1	smontováno
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
hradu	hrad	k1gInSc2	hrad
Windsor	Windsor	k1gInSc1	Windsor
je	být	k5eAaImIp3nS	být
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
pochováni	pochovat	k5eAaPmNgMnP	pochovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
scházejí	scházet	k5eAaImIp3nP	scházet
držitelé	držitel	k1gMnPc1	držitel
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
anglických	anglický	k2eAgMnPc2d1	anglický
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
a	a	k8xC	a
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
následuje	následovat	k5eAaImIp3nS	následovat
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
průvod	průvod	k1gInSc1	průvod
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
také	také	k9	také
členové	člen	k1gMnPc1	člen
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
uložen	uložit	k5eAaPmNgInS	uložit
velký	velký	k2eAgInSc1d1	velký
domek	domek	k1gInSc1	domek
pro	pro	k7c4	pro
panenky	panenka	k1gFnPc4	panenka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
si	se	k3xPyFc3	se
nechala	nechat	k5eAaPmAgFnS	nechat
vytvořit	vytvořit	k5eAaPmF	vytvořit
od	od	k7c2	od
Edwina	Edwin	k2eAgMnSc2d1	Edwin
Lutyense	Lutyens	k1gMnSc2	Lutyens
královna	královna	k1gFnSc1	královna
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
domek	domek	k1gInSc1	domek
pro	pro	k7c4	pro
panenky	panenka	k1gFnSc2	panenka
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
lákadel	lákadlo	k1gNnPc2	lákadlo
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Windsoru	Windsor	k1gInSc2	Windsor
se	se	k3xPyFc4	se
dostanete	dostat	k5eAaPmIp2nP	dostat
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
po	po	k7c6	po
sjezdu	sjezd	k1gInSc6	sjezd
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
pokračujete	pokračovat	k5eAaImIp2nP	pokračovat
5	[number]	k4	5
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
dvouproudou	dvouproudý	k2eAgFnSc7d1	dvouproudá
silnicí	silnice	k1gFnSc7	silnice
až	až	k6eAd1	až
do	do	k7c2	do
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
Windsor	Windsor	k1gInSc1	Windsor
&	&	k?	&
Eton	Eton	k1gInSc1	Eton
Central	Central	k1gMnSc2	Central
railway	railwaa	k1gMnSc2	railwaa
station	station	k1gInSc1	station
jezdí	jezdit	k5eAaImIp3nS	jezdit
pravidelně	pravidelně	k6eAd1	pravidelně
vlaky	vlak	k1gInPc4	vlak
do	do	k7c2	do
Slough	Slougha	k1gFnPc2	Slougha
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
dostanete	dostat	k5eAaPmIp2nP	dostat
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
Paddington	Paddington	k1gInSc1	Paddington
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
nebo	nebo	k8xC	nebo
západ	západ	k1gInSc1	západ
do	do	k7c2	do
Maidenhead	Maidenhead	k1gInSc4	Maidenhead
<g/>
,	,	kIx,	,
Readingu	Reading	k1gInSc2	Reading
až	až	k8xS	až
do	do	k7c2	do
Bristolu	Bristol	k1gInSc2	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
Windsor	Windsor	k1gInSc1	Windsor
&	&	k?	&
Eton	Eton	k1gInSc1	Eton
Riverside	Riversid	k1gInSc5	Riversid
station	station	k1gInSc1	station
jezdí	jezdit	k5eAaImIp3nP	jezdit
vlaky	vlak	k1gInPc1	vlak
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
London	London	k1gMnSc1	London
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
nádraží	nádraží	k1gNnPc1	nádraží
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
Windsoru	Windsor	k1gInSc2	Windsor
mohla	moct	k5eAaImAgFnS	moct
pohodlně	pohodlně	k6eAd1	pohodlně
dopravit	dopravit	k5eAaPmF	dopravit
královna	královna	k1gFnSc1	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Windsor	Windsor	k1gInSc1	Windsor
je	být	k5eAaImIp3nS	být
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
městem	město	k1gNnSc7	město
Eton	Etona	k1gFnPc2	Etona
(	(	kIx(	(
<g/>
které	který	k3yIgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
)	)	kIx)	)
mostem	most	k1gInSc7	most
Windsor	Windsor	k1gInSc4	Windsor
Bridge	Bridg	k1gInSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
původně	původně	k6eAd1	původně
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
veškerou	veškerý	k3xTgFnSc4	veškerý
dopravu	doprava	k1gFnSc4	doprava
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
jen	jen	k9	jen
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
krásnou	krásný	k2eAgFnSc4d1	krásná
procházku	procházka	k1gFnSc4	procházka
z	z	k7c2	z
Windsoru	Windsor	k1gInSc2	Windsor
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
High	High	k1gMnSc1	High
Street	Street	k1gMnSc1	Street
v	v	k7c6	v
Etonu	Eton	k1gInSc6	Eton
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
Great	Great	k2eAgInSc1d1	Great
Windsor	Windsor	k1gInSc1	Windsor
Park	park	k1gInSc1	park
a	a	k8xC	a
města	město	k1gNnSc2	město
Old	Olda	k1gFnPc2	Olda
Windsor	Windsor	k1gInSc1	Windsor
<g/>
,	,	kIx,	,
Egham	Egham	k1gInSc1	Egham
a	a	k8xC	a
Virginia	Virginium	k1gNnSc2	Virginium
Water	Watra	k1gFnPc2	Watra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Windsor	Windsor	k1gInSc1	Windsor
Castle	Castle	k1gFnSc2	Castle
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Windsor	Windsor	k1gInSc1	Windsor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Windsor	Windsor	k1gInSc4	Windsor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Windsoru	Windsor	k1gInSc2	Windsor
</s>
</p>
