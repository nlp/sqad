<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Strakonitz	Strakonitz	k1gInSc1	Strakonitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Otavy	Otava	k1gFnSc2	Otava
a	a	k8xC	a
Volyňky	Volyňka	k1gFnSc2	Volyňka
<g/>
,	,	kIx,	,
52	[number]	k4	52
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c7	mezi
390	[number]	k4	390
až	až	k9	až
430	[number]	k4	430
m.	m.	k?	m.
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
23	[number]	k4	23
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
3468	[number]	k4	3468
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
původně	původně	k6eAd1	původně
stály	stát	k5eAaImAgFnP	stát
čtyři	čtyři	k4xCgFnPc1	čtyři
vsi	ves	k1gFnPc1	ves
(	(	kIx(	(
<g/>
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
<g/>
,	,	kIx,	,
Žabokrty	Žabokrt	k1gInPc1	Žabokrt
a	a	k8xC	a
Lom	lom	k1gInSc1	lom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
v	v	k7c6	v
poddanské	poddanský	k2eAgFnSc6d1	poddanská
město	město	k1gNnSc1	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
Strakonic	Strakonice	k1gFnPc2	Strakonice
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
rod	rod	k1gInSc1	rod
Bavorů	Bavor	k1gMnPc2	Bavor
budovat	budovat	k5eAaImF	budovat
místní	místní	k2eAgInSc4d1	místní
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1243	[number]	k4	1243
daroval	darovat	k5eAaPmAgMnS	darovat
Bavor	Bavor	k1gMnSc1	Bavor
I.	I.	kA	I.
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Bolemilou	Bolemilý	k2eAgFnSc4d1	Bolemilý
polovinu	polovina	k1gFnSc4	polovina
strakonického	strakonický	k2eAgInSc2d1	strakonický
hradu	hrad	k1gInSc2	hrad
řádu	řád	k1gInSc2	řád
johanitů	johanita	k1gMnPc2	johanita
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zvaných	zvaný	k2eAgInPc2d1	zvaný
maltézští	maltézský	k2eAgMnPc1d1	maltézský
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
raně	raně	k6eAd1	raně
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
a	a	k8xC	a
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
prošel	projít	k5eAaPmAgInS	projít
rozmanitým	rozmanitý	k2eAgInSc7d1	rozmanitý
stavebním	stavební	k2eAgInSc7d1	stavební
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1367	[number]	k4	1367
byla	být	k5eAaImAgFnS	být
Strakonicím	Strakonice	k1gFnPc3	Strakonice
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
Bavorem	Bavor	k1gMnSc7	Bavor
IV	IV	kA	IV
<g/>
.	.	kIx.	.
městská	městský	k2eAgNnPc4d1	Městské
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1402	[number]	k4	1402
prodal	prodat	k5eAaPmAgInS	prodat
rod	rod	k1gInSc1	rod
Bavorů	Bavor	k1gMnPc2	Bavor
i	i	k8xC	i
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
strakonického	strakonický	k2eAgNnSc2d1	Strakonické
panství	panství	k1gNnSc2	panství
řádu	řád	k1gInSc2	řád
johanitů	johanita	k1gMnPc2	johanita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
strakonický	strakonický	k2eAgInSc1d1	strakonický
hrad	hrad	k1gInSc1	hrad
sídlem	sídlo	k1gNnSc7	sídlo
velkopřevora	velkopřevor	k1gMnSc2	velkopřevor
a	a	k8xC	a
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ze	z	k7c2	z
Strakonic	Strakonice	k1gFnPc2	Strakonice
spravovali	spravovat	k5eAaImAgMnP	spravovat
své	svůj	k3xOyFgFnPc4	svůj
komendy	komenda	k1gFnPc4	komenda
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
byly	být	k5eAaImAgFnP	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
středisek	středisko	k1gNnPc2	středisko
protihusitského	protihusitský	k2eAgInSc2d1	protihusitský
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
velkopřevor	velkopřevor	k1gMnSc1	velkopřevor
Colloredo	Colloredo	k1gNnSc4	Colloredo
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
-	-	kIx~	-
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
panství	panství	k1gNnPc2	panství
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
velkopřevora	velkopřevor	k1gMnSc2	velkopřevor
(	(	kIx(	(
<g/>
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
Varvažov	Varvažov	k1gInSc1	Varvažov
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Libchava	Libchava	k1gFnSc1	Libchava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
textilní	textilní	k2eAgFnSc1d1	textilní
továrna	továrna	k1gFnSc1	továrna
Fezko	Fezko	k1gNnSc1	Fezko
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
České	český	k2eAgFnSc2d1	Česká
zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
(	(	kIx(	(
<g/>
ČZ	ČZ	kA	ČZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
607	[number]	k4	607
domech	dům	k1gInPc6	dům
7723	[number]	k4	7723
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
4248	[number]	k4	4248
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
7581	[number]	k4	7581
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
44	[number]	k4	44
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
8	[number]	k4	8
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
6094	[number]	k4	6094
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
248	[number]	k4	248
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
387	[number]	k4	387
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
178	[number]	k4	178
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
902	[number]	k4	902
domech	dům	k1gInPc6	dům
9883	[number]	k4	9883
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
9599	[number]	k4	9599
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
124	[number]	k4	124
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
7936	[number]	k4	7936
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
886	[number]	k4	886
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
248	[number]	k4	248
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
169	[number]	k4	169
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
populace	populace	k1gFnSc2	populace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
do	do	k7c2	do
geomorfologického	geomorfologický	k2eAgNnSc2d1	Geomorfologické
členění	členění	k1gNnSc2	členění
se	se	k3xPyFc4	se
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
tří	tři	k4xCgInPc2	tři
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Strakonic	Strakonice	k1gFnPc2	Strakonice
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Českobudějovickou	českobudějovický	k2eAgFnSc4d1	českobudějovická
pánev	pánev	k1gFnSc4	pánev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Otavy	Otava	k1gFnSc2	Otava
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
úzkou	úzký	k2eAgFnSc7d1	úzká
sníženinou	sníženina	k1gFnSc7	sníženina
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Strakononická	Strakononický	k2eAgFnSc1d1	Strakononický
kotlina	kotlina	k1gFnSc1	kotlina
na	na	k7c4	na
západ	západ	k1gInSc4	západ
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
k	k	k7c3	k
Novosedlům	Novosedlo	k1gNnPc3	Novosedlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předměstí	předměstí	k1gNnSc1	předměstí
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
připojené	připojený	k2eAgFnSc2d1	připojená
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
dále	daleko	k6eAd2	daleko
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
Otavy	Otava	k1gFnSc2	Otava
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
představují	představovat	k5eAaImIp3nP	představovat
jižní	jižní	k2eAgInSc4d1	jižní
okraj	okraj	k1gInSc4	okraj
celku	celek	k1gInSc2	celek
Blatenská	blatenský	k2eAgFnSc1d1	Blatenská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
(	(	kIx(	(
<g/>
konkrétněji	konkrétně	k6eAd2	konkrétně
jeho	jeho	k3xOp3gFnSc4	jeho
podcelku	podcelka	k1gFnSc4	podcelka
Horažďovická	horažďovický	k2eAgFnSc1d1	horažďovická
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc6	okrsek
Radomyšlská	Radomyšlský	k2eAgFnSc1d1	Radomyšlská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
okrajem	okraj	k1gInSc7	okraj
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vypíná	vypínat	k5eAaImIp3nS	vypínat
vrch	vrch	k1gInSc1	vrch
Kuřidlo	kuřidlo	k1gNnSc1	kuřidlo
(	(	kIx(	(
<g/>
546	[number]	k4	546
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgFnPc1d1	okrajová
místní	místní	k2eAgFnPc1d1	místní
části	část	k1gFnPc1	část
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
Otavy	Otava	k1gFnSc2	Otava
<g/>
,	,	kIx,	,
příslušejí	příslušet	k5eAaImIp3nP	příslušet
k	k	k7c3	k
celku	celek	k1gInSc3	celek
Šumavské	šumavský	k2eAgNnSc1d1	Šumavské
podhůří	podhůří	k1gNnSc1	podhůří
(	(	kIx(	(
<g/>
podcelek	podcelek	k1gInSc1	podcelek
Bavorovská	Bavorovský	k2eAgFnSc1d1	Bavorovská
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
okrsek	okrsek	k1gInSc1	okrsek
Miloňovická	Miloňovický	k2eAgFnSc1d1	Miloňovický
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
též	též	k9	též
leží	ležet	k5eAaImIp3nS	ležet
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
vrch	vrch	k1gInSc1	vrch
Velká	velká	k1gFnSc1	velká
Kakada	Kakada	k?	Kakada
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
též	též	k9	též
zvaný	zvaný	k2eAgInSc1d1	zvaný
Srpská	Srpské	k1gNnPc4	Srpské
<g/>
,	,	kIx,	,
564	[number]	k4	564
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
klimatické	klimatický	k2eAgFnPc4d1	klimatická
regionalizace	regionalizace	k1gFnPc4	regionalizace
Česka	Česko	k1gNnSc2	Česko
dle	dle	k7c2	dle
Quitta	Quitt	k1gInSc2	Quitt
spadá	spadat	k5eAaImIp3nS	spadat
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
do	do	k7c2	do
mírně	mírně	k6eAd1	mírně
teplé	teplý	k2eAgFnSc2d1	teplá
oblasti	oblast	k1gFnSc2	oblast
třídy	třída	k1gFnSc2	třída
MT	MT	kA	MT
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
osm	osm	k4xCc1	osm
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
údaje	údaj	k1gInSc2	údaj
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
I	I	kA	I
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
)	)	kIx)	)
–	–	k?	–
13	[number]	k4	13
695	[number]	k4	695
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
II	II	kA	II
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Nové	Nové	k2eAgFnPc1d1	Nové
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
)	)	kIx)	)
–	–	k?	–
4871	[number]	k4	4871
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Dražejov	Dražejov	k1gInSc1	Dražejov
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Dražejov	Dražejov	k1gInSc1	Dražejov
u	u	k7c2	u
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
)	)	kIx)	)
–	–	k?	–
1352	[number]	k4	1352
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Hajská	Hajská	k1gFnSc1	Hajská
–	–	k?	–
49	[number]	k4	49
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Modlešovice	Modlešovice	k1gFnSc1	Modlešovice
–	–	k?	–
166	[number]	k4	166
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Přední	přední	k2eAgFnSc1d1	přední
Ptákovice	Ptákovice	k1gFnSc1	Ptákovice
–	–	k?	–
2429	[number]	k4	2429
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú	ú	k0	ú
Střela	střela	k1gFnSc1	střela
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
Střela	střela	k1gFnSc1	střela
–	–	k?	–
46	[number]	k4	46
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
Virt	Virta	k1gFnPc2	Virta
–	–	k?	–
82	[number]	k4	82
obyvatelV	obyvatelV	k?	obyvatelV
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obytná	obytný	k2eAgFnSc1d1	obytná
lokalita	lokalita	k1gFnSc1	lokalita
Habeš	Habeš	k1gFnSc1	Habeš
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
jí	on	k3xPp3gFnSc7	on
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
140	[number]	k4	140
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vrchy	vrch	k1gInPc1	vrch
Ryšová	Ryšová	k1gFnSc1	Ryšová
(	(	kIx(	(
<g/>
527	[number]	k4	527
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kuřidlo	kuřidlo	k1gNnSc4	kuřidlo
(	(	kIx(	(
<g/>
546	[number]	k4	546
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
přírodními	přírodní	k2eAgFnPc7d1	přírodní
rezervacemi	rezervace	k1gFnPc7	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Droužetice	Droužetika	k1gFnSc3	Droužetika
<g/>
,	,	kIx,	,
Mutěnice	Mutěnice	k1gFnSc1	Mutěnice
<g/>
,	,	kIx,	,
Rovná	rovný	k2eAgFnSc1d1	rovná
a	a	k8xC	a
Řepice	řepice	k1gFnSc1	řepice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Samospráva	samospráva	k1gFnSc1	samospráva
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
Pavel	Pavel	k1gMnSc1	Pavel
Vondrys	Vondrys	k1gInSc1	Vondrys
a	a	k8xC	a
místostarosty	místostarosta	k1gMnPc7	místostarosta
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
Pavel	Pavel	k1gMnSc1	Pavel
Pavel	Pavel	k1gMnSc1	Pavel
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Vlasák	Vlasák	k1gMnSc1	Vlasák
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Tomšovicová	Tomšovicový	k2eAgFnSc1d1	Tomšovicová
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ivana	Ivana	k1gFnSc1	Ivana
Říhová	Říhová	k1gFnSc1	Říhová
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Břetislav	Břetislav	k1gMnSc1	Břetislav
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
(	(	kIx(	(
<g/>
Strakonická	strakonický	k2eAgFnSc1d1	Strakonická
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
opět	opět	k6eAd1	opět
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
51,15	[number]	k4	51,15
%	%	kIx~	%
uskupení	uskupení	k1gNnSc2	uskupení
Strakonická	strakonický	k2eAgFnSc1d1	Strakonická
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
,	,	kIx,	,
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
však	však	k9	však
výsledek	výsledek	k1gInSc1	výsledek
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
a	a	k8xC	a
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
budou	být	k5eAaImBp3nP	být
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
a	a	k8xC	a
pečeť	pečeť	k1gFnSc1	pečeť
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajka	vlajka	k1gFnSc1	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
prapor	prapor	k1gInSc4	prapor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Odráží	odrážet	k5eAaImIp3nS	odrážet
historickou	historický	k2eAgFnSc4d1	historická
roli	role	k1gFnSc4	role
rodu	rod	k1gInSc2	rod
Bavorů	Bavor	k1gMnPc2	Bavor
dlouho	dlouho	k6eAd1	dlouho
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
Strakonicům	Strakonice	k1gFnPc3	Strakonice
(	(	kIx(	(
<g/>
1235	[number]	k4	1235
<g/>
–	–	k?	–
<g/>
1402	[number]	k4	1402
<g/>
)	)	kIx)	)
a	a	k8xC	a
řádu	řád	k1gInSc2	řád
johanitů	johanita	k1gMnPc2	johanita
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
řádu	řád	k1gInSc2	řád
maltézských	maltézský	k2eAgMnPc2d1	maltézský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
město	město	k1gNnSc1	město
patřilo	patřit	k5eAaImAgNnS	patřit
v	v	k7c6	v
letech	let	k1gInPc6	let
1402	[number]	k4	1402
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
List	list	k1gInSc1	list
praporu	prapor	k1gInSc2	prapor
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
pokosem	pokos	k1gInSc7	pokos
dělená	dělený	k2eAgFnSc1d1	dělená
pole	pole	k1gFnSc1	pole
<g/>
:	:	kIx,	:
horní	horní	k2eAgNnSc1d1	horní
červené	červené	k1gNnSc1	červené
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
maltézským	maltézský	k2eAgInSc7d1	maltézský
křížem	kříž	k1gInSc7	kříž
a	a	k8xC	a
dolní	dolní	k2eAgNnSc1d1	dolní
žluté	žlutý	k2eAgNnSc1d1	žluté
s	s	k7c7	s
kosmo	kosmo	k?	kosmo
položenou	položený	k2eAgFnSc7d1	položená
střelou	střela	k1gFnSc7	střela
z	z	k7c2	z
historické	historický	k2eAgFnSc2d1	historická
městské	městský	k2eAgFnSc2d1	městská
pečeti	pečeť	k1gFnSc2	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
k	k	k7c3	k
šířce	šířka	k1gFnSc3	šířka
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Znak	znak	k1gInSc4	znak
===	===	k?	===
</s>
</p>
<p>
<s>
Znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
Strakonicům	Strakonice	k1gFnPc3	Strakonice
udělen	udělen	k2eAgInSc1d1	udělen
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
)	)	kIx)	)
se	s	k7c7	s
symboly	symbol	k1gInPc7	symbol
Johanitů	johanita	k1gMnPc2	johanita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
modrém	modrý	k2eAgInSc6d1	modrý
štítě	štít	k1gInSc6	štít
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hradby	hradba	k1gFnPc1	hradba
s	s	k7c7	s
klíčovými	klíčový	k2eAgFnPc7d1	klíčová
střílnami	střílna	k1gFnPc7	střílna
<g/>
,	,	kIx,	,
brána	brát	k5eAaImNgFnS	brát
se	s	k7c7	s
zvednutou	zvednutý	k2eAgFnSc7d1	zvednutá
zlatou	zlatý	k2eAgFnSc7d1	zlatá
mříží	mříž	k1gFnSc7	mříž
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
je	být	k5eAaImIp3nS	být
cimbuří	cimbuří	k1gNnSc1	cimbuří
a	a	k8xC	a
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
okny	okno	k1gNnPc7	okno
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k7c2	uvnitř
oken	okno	k1gNnPc2	okno
jsou	být	k5eAaImIp3nP	být
kříže	kříž	k1gInPc1	kříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgInPc7	dva
cimbuřími	cimbuří	k1gNnPc7	cimbuří
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
střechou	střecha	k1gFnSc7	střecha
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
zlatými	zlatý	k2eAgFnPc7d1	zlatá
makovicemi	makovice	k1gFnPc7	makovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
věže	věž	k1gFnSc2	věž
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
červené	červený	k2eAgInPc4d1	červený
štítky	štítek	k1gInPc4	štítek
se	s	k7c7	s
zlatými	zlatý	k2eAgFnPc7d1	zlatá
šňůrkami	šňůrka	k1gFnPc7	šňůrka
<g/>
.	.	kIx.	.
</s>
<s>
Vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
štítku	štítek	k1gInSc6	štítek
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
štítku	štítek	k1gInSc6	štítek
je	být	k5eAaImIp3nS	být
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
Landštejnská	Landštejnský	k2eAgFnSc1d1	Landštejnský
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
zlatý	zlatý	k2eAgInSc4d1	zlatý
střed	střed	k1gInSc4	střed
a	a	k8xC	a
zelené	zelený	k2eAgInPc4d1	zelený
kališní	kališní	k2eAgInPc4d1	kališní
lístky	lístek	k1gInPc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růže	k1gFnSc1	růže
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
nejspíš	nejspíš	k9	nejspíš
omylem	omylem	k6eAd1	omylem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Landštejna	Landštejno	k1gNnSc2	Landštejno
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
řádu	řád	k1gInSc2	řád
Johanitů	johanita	k1gMnPc2	johanita
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
znaku	znak	k1gInSc2	znak
tam	tam	k6eAd1	tam
byli	být	k5eAaImAgMnP	být
Rožmberkové	Rožmberkové	k?	Rožmberkové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
malby	malba	k1gFnPc1	malba
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
máme	mít	k5eAaImIp1nP	mít
doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
růži	růž	k1gFnSc6	růž
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
štítku	štítek	k1gInSc6	štítek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
růže	růže	k1gFnSc1	růže
stále	stále	k6eAd1	stále
na	na	k7c6	na
strakonickém	strakonický	k2eAgInSc6d1	strakonický
znaku	znak	k1gInSc6	znak
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nemá	mít	k5eNaImIp3nS	mít
co	co	k3yInSc4	co
do	do	k7c2	do
činění	činění	k1gNnSc2	činění
se	s	k7c7	s
strakonickou	strakonický	k2eAgFnSc7d1	Strakonická
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
růže	růže	k1gFnSc1	růže
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
znak	znak	k1gInSc4	znak
z	z	k7c2	z
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pečeť	pečeť	k1gFnSc4	pečeť
===	===	k?	===
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
získaly	získat	k5eAaPmAgFnP	získat
i	i	k9	i
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
kdy	kdy	k6eAd1	kdy
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
velkopřevora	velkopřevor	k1gMnSc2	velkopřevor
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
<g/>
–	–	k?	–
<g/>
1542	[number]	k4	1542
<g/>
)	)	kIx)	)
udělil	udělit	k5eAaPmAgInS	udělit
městu	město	k1gNnSc3	město
privilegium	privilegium	k1gNnSc1	privilegium
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Také	také	k6eAd1	také
týmž	týž	k3xTgMnPc3	týž
obyvatelům	obyvatel	k1gMnPc3	obyvatel
v	v	k7c6	v
štědrosti	štědrost	k1gFnSc6	štědrost
naší	náš	k3xOp1gFnSc2	náš
královské	královský	k2eAgFnSc2d1	královská
tuto	tento	k3xDgFnSc4	tento
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
milost	milost	k1gFnSc4	milost
činiti	činit	k5eAaImF	činit
ráčíme	ráčit	k5eAaImIp1nP	ráčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
touž	týž	k3xTgFnSc7	týž
pečetí	pečeť	k1gFnSc7	pečeť
<g/>
,	,	kIx,	,
kteréž	kteréž	k?	kteréž
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
starodávna	starodávno	k1gNnSc2	starodávno
užívání	užívání	k1gNnSc2	užívání
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
a	a	k8xC	a
všelijakých	všelijaký	k3yIgFnPc6	všelijaký
potřebách	potřeba	k1gFnPc6	potřeba
obecních	obecní	k2eAgInPc2d1	obecní
červeným	červený	k2eAgInSc7d1	červený
voskem	vosk	k1gInSc7	vosk
pečetiti	pečetit	k5eAaImF	pečetit
mohli	moct	k5eAaImAgMnP	moct
a	a	k8xC	a
moc	moc	k6eAd1	moc
jměli	jměnout	k5eAaPmAgMnP	jměnout
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
citace	citace	k1gFnSc2	citace
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Strakonice	Strakonice	k1gFnPc4	Strakonice
získali	získat	k5eAaPmAgMnP	získat
pečeť	pečeť	k1gFnSc4	pečeť
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
privilegia	privilegium	k1gNnSc2	privilegium
velkopřevora	velkopřevor	k1gMnSc2	velkopřevor
Jana	Jan	k1gMnSc2	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
(	(	kIx(	(
<g/>
1511	[number]	k4	1511
<g/>
–	–	k?	–
<g/>
1532	[number]	k4	1532
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
totiž	totiž	k9	totiž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potvrzení	potvrzení	k1gNnSc3	potvrzení
a	a	k8xC	a
výraznému	výrazný	k2eAgNnSc3d1	výrazné
rozšíření	rozšíření	k1gNnSc3	rozšíření
stávajících	stávající	k2eAgFnPc2d1	stávající
městských	městský	k2eAgFnPc2d1	městská
výsad	výsada	k1gFnPc2	výsada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
má	mít	k5eAaImIp3nS	mít
tradici	tradice	k1gFnSc4	tradice
především	především	k9	především
průmysl	průmysl	k1gInSc4	průmysl
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
a	a	k8xC	a
pivovarnictví	pivovarnictví	k1gNnSc6	pivovarnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
zde	zde	k6eAd1	zde
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Fezko	Fezko	k1gNnSc1	Fezko
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
vyrábět	vyrábět	k5eAaImF	vyrábět
slavné	slavný	k2eAgInPc4d1	slavný
fezy	fez	k1gInPc4	fez
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vyráběly	vyrábět	k5eAaImAgInP	vyrábět
se	se	k3xPyFc4	se
také	také	k9	také
barety	baret	k1gInPc1	baret
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
pokrývky	pokrývka	k1gFnPc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
přikrývky	přikrývka	k1gFnPc4	přikrývka
<g/>
,	,	kIx,	,
pletené	pletený	k2eAgNnSc4d1	pletené
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
vlněné	vlněný	k2eAgFnSc2d1	vlněná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
hlavně	hlavně	k9	hlavně
autotextilie	autotextilie	k1gFnPc1	autotextilie
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Tonak	tonak	k1gInSc4	tonak
a.	a.	k?	a.
s.	s.	k?	s.
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
založila	založit	k5eAaPmAgFnS	založit
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
Fezko	Fezko	k1gNnSc4	Fezko
Slovakia	Slovakium	k1gNnSc2	Slovakium
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
ČZ	ČZ	kA	ČZ
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
jen	jen	k9	jen
pistole	pistole	k1gFnSc1	pistole
<g/>
,	,	kIx,	,
vzduchovky	vzduchovka	k1gFnPc1	vzduchovka
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
automatické	automatický	k2eAgFnPc1d1	automatická
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
dělá	dělat	k5eAaImIp3nS	dělat
firma	firma	k1gFnSc1	firma
jízdní	jízdní	k2eAgFnSc1d1	jízdní
kola	kola	k1gFnSc1	kola
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
i	i	k8xC	i
motocykly	motocykl	k1gInPc1	motocykl
(	(	kIx(	(
<g/>
též	též	k9	též
zn.	zn.	kA	zn.
Jawa	jawa	k1gFnSc1	jawa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
největší	veliký	k2eAgFnPc1d3	veliký
slávy	sláva	k1gFnPc1	sláva
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
podnik	podnik	k1gInSc1	podnik
znárodněn	znárodnit	k5eAaPmNgInS	znárodnit
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útlumu	útlum	k1gInSc3	útlum
výroby	výroba	k1gFnSc2	výroba
motocyklů	motocykl	k1gInPc2	motocykl
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
řetězů	řetěz	k1gInPc2	řetěz
<g/>
,	,	kIx,	,
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
odlitků	odlitek	k1gInPc2	odlitek
a	a	k8xC	a
obráběcích	obráběcí	k2eAgInPc2d1	obráběcí
strojů	stroj	k1gInPc2	stroj
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
autokomponentů	autokomponent	k1gMnPc2	autokomponent
–	–	k?	–
převodových	převodový	k2eAgFnPc2d1	převodová
skříní	skříň	k1gFnPc2	skříň
a	a	k8xC	a
turbodmychadel	turbodmychadlo	k1gNnPc2	turbodmychadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
získaly	získat	k5eAaPmAgFnP	získat
právo	právo	k1gNnSc4	právo
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc4	pivo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
právo	právo	k1gNnSc4	právo
várečné	várečné	k1gFnPc1	várečné
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1367	[number]	k4	1367
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vařilo	vařit	k5eAaImAgNnS	vařit
už	už	k6eAd1	už
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1308	[number]	k4	1308
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1649	[number]	k4	1649
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
vaří	vařit	k5eAaImIp3nS	vařit
piva	pivo	k1gNnSc2	pivo
značky	značka	k1gFnSc2	značka
Dudák	dudák	k1gMnSc1	dudák
(	(	kIx(	(
<g/>
DUDÁK	dudák	k1gMnSc1	dudák
Black	Black	k1gMnSc1	Black
tmavý	tmavý	k2eAgInSc1d1	tmavý
ležák	ležák	k1gInSc1	ležák
<g/>
,	,	kIx,	,
DUDÁK	dudák	k1gInSc1	dudák
Driver	driver	k1gInSc4	driver
nealkoholické	alkoholický	k2eNgNnSc1d1	nealkoholické
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
DUDÁK	dudák	k1gInSc1	dudák
Nektar	nektar	k1gInSc1	nektar
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
<g/>
,	,	kIx,	,
DUDÁK	dudák	k1gInSc1	dudák
Premium	Premium	k1gNnSc1	Premium
světlý	světlý	k2eAgMnSc1d1	světlý
ležák	ležák	k1gMnSc1	ležák
<g/>
,	,	kIx,	,
KLOSTERMANN	KLOSTERMANN	kA	KLOSTERMANN
polotmavý	polotmavý	k2eAgInSc1d1	polotmavý
ležák	ležák	k1gInSc1	ležák
</s>
</p>
<p>
<s>
KRÁL	Král	k1gMnSc1	Král
ŠUMAVY	Šumava	k1gFnSc2	Šumava
světlý	světlý	k2eAgMnSc1d1	světlý
ležák	ležák	k1gMnSc1	ležák
<g/>
,	,	kIx,	,
OTAVSKÝ	otavský	k2eAgInSc1d1	otavský
zlatý	zlatý	k1gInSc1	zlatý
<g/>
,	,	kIx,	,
PÁRTY	párty	k1gFnSc1	párty
soudek	soudek	k1gInSc1	soudek
<g/>
,	,	kIx,	,
ŠVANDA	švanda	k1gFnSc1	švanda
světlé	světlý	k2eAgNnSc4d1	světlé
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
VELKOPŘEVOR	velkopřevor	k1gMnSc1	velkopřevor
světlý	světlý	k2eAgInSc4d1	světlý
speciál	speciál	k1gInSc4	speciál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Punc	punc	k1gInSc1	punc
jedinečnosti	jedinečnost	k1gFnSc2	jedinečnost
tomuto	tento	k3xDgInSc3	tento
zlatému	zlatý	k2eAgInSc3d1	zlatý
moku	mok	k1gInSc3	mok
dodával	dodávat	k5eAaImAgMnS	dodávat
fakt	fakt	k9	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
strakonické	strakonický	k2eAgNnSc1d1	Strakonické
pivo	pivo	k1gNnSc1	pivo
Dudák	dudák	k1gInSc4	dudák
bylo	být	k5eAaImAgNnS	být
vařeno	vařen	k2eAgNnSc1d1	vařeno
ženou	žena	k1gFnSc7	žena
–	–	k?	–
paní	paní	k1gFnSc2	paní
sládkovou	sládkův	k2eAgFnSc7d1	sládkova
Dagmar	Dagmar	k1gFnSc7	Dagmar
Vlkovou	Vlkův	k2eAgFnSc7d1	Vlkova
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
sládkem	sládek	k1gMnSc7	sládek
pivovaru	pivovar	k1gInSc2	pivovar
je	být	k5eAaImIp3nS	být
ing.	ing.	kA	ing.
Dušan	Dušan	k1gMnSc1	Dušan
Krankus	Krankus	k1gMnSc1	Krankus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Firmy	firma	k1gFnPc1	firma
===	===	k?	===
</s>
</p>
<p>
<s>
Tonak	tonak	k1gInSc1	tonak
a.	a.	k?	a.
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Adient	Adient	k1gInSc1	Adient
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Fezko	Fezko	k1gNnSc1	Fezko
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ČZ	ČZ	kA	ČZ
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
Dura	Dura	k6eAd1	Dura
Automotive	Automotiv	k1gInSc5	Automotiv
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
</p>
<p>
<s>
Aluprogress	Aluprogress	k1gInSc1	Aluprogress
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Metal	metat	k5eAaImAgInS	metat
Progres	progres	k1gInSc1	progres
Strakonice	Strakonice	k1gFnPc1	Strakonice
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DUDÁK	dudák	k1gInSc1	dudák
-	-	kIx~	-
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
==	==	k?	==
</s>
</p>
<p>
<s>
Strakonická	strakonický	k2eAgFnSc1d1	Strakonická
nemocnice	nemocnice	k1gFnSc1	nemocnice
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
péči	péče	k1gFnSc4	péče
na	na	k7c6	na
314	[number]	k4	314
lůžkách	lůžko	k1gNnPc6	lůžko
v	v	k7c6	v
8	[number]	k4	8
lůžkových	lůžkový	k2eAgInPc6d1	lůžkový
odděleních	oddělení	k1gNnPc6	oddělení
a	a	k8xC	a
v	v	k7c6	v
58	[number]	k4	58
odborných	odborný	k2eAgFnPc6d1	odborná
ambulancích	ambulance	k1gFnPc6	ambulance
ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
<g/>
,	,	kIx,	,
Vodňanech	Vodňan	k1gMnPc6	Vodňan
a	a	k8xC	a
Blatné	blatný	k2eAgNnSc4d1	Blatné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
narodí	narodit	k5eAaPmIp3nS	narodit
kolem	kolem	k7c2	kolem
800	[number]	k4	800
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
hospitalizováno	hospitalizován	k2eAgNnSc4d1	hospitalizováno
cca	cca	kA	cca
15	[number]	k4	15
000	[number]	k4	000
pacientů	pacient	k1gMnPc2	pacient
a	a	k8xC	a
lékařský	lékařský	k2eAgInSc1d1	lékařský
personál	personál	k1gInSc1	personál
ošetří	ošetřit	k5eAaPmIp3nS	ošetřit
zhruba	zhruba	k6eAd1	zhruba
260	[number]	k4	260
000	[number]	k4	000
ambulantních	ambulantní	k2eAgMnPc2d1	ambulantní
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
přes	přes	k7c4	přes
500	[number]	k4	500
kmenových	kmenový	k2eAgMnPc2d1	kmenový
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strakonická	strakonický	k2eAgFnSc1d1	Strakonická
nemocnice	nemocnice	k1gFnSc1	nemocnice
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
prestižního	prestižní	k2eAgInSc2d1	prestižní
titulu	titul	k1gInSc2	titul
Baby	baba	k1gFnSc2	baba
Friendly	Friendly	k1gMnSc1	Friendly
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
certifikát	certifikát	k1gInSc4	certifikát
jakosti	jakost	k1gFnSc2	jakost
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
9001	[number]	k4	9001
<g/>
:	:	kIx,	:
<g/>
2001	[number]	k4	2001
a	a	k8xC	a
akreditaci	akreditace	k1gFnSc4	akreditace
podle	podle	k7c2	podle
SAK	sako	k1gNnPc2	sako
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Městské	městský	k2eAgNnSc1d1	Městské
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
===	===	k?	===
</s>
</p>
<p>
<s>
Informačním	informační	k2eAgInSc7d1	informační
centrum	centrum	k1gNnSc4	centrum
ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
pamětihodnostech	pamětihodnost	k1gFnPc6	pamětihodnost
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc6d1	přírodní
zajímavostech	zajímavost	k1gFnPc6	zajímavost
<g/>
,	,	kIx,	,
turistických	turistický	k2eAgInPc6d1	turistický
cílech	cíl	k1gInPc6	cíl
regionu	region	k1gInSc2	region
a	a	k8xC	a
jízdních	jízdní	k2eAgInPc6d1	jízdní
řádech	řád	k1gInPc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nabízí	nabízet	k5eAaImIp3nS	nabízet
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
stravování	stravování	k1gNnSc4	stravování
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgInPc4d1	sportovní
přehledy	přehled	k1gInPc4	přehled
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
foyer	foyer	k1gNnSc6	foyer
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
samoobslužný	samoobslužný	k2eAgInSc1d1	samoobslužný
platební	platební	k2eAgInSc1d1	platební
kiosek	kiosek	k1gInSc1	kiosek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
zakoupit	zakoupit	k5eAaPmF	zakoupit
vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
akce	akce	k1gFnPc4	akce
pořádané	pořádaný	k2eAgNnSc1d1	pořádané
Městským	městský	k2eAgNnSc7d1	Městské
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
jsou	být	k5eAaImIp3nP	být
připraveny	připravit	k5eAaPmNgInP	připravit
turistické	turistický	k2eAgInPc1d1	turistický
propagační	propagační	k2eAgInPc1d1	propagační
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
nabízí	nabízet	k5eAaImIp3nS	nabízet
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
pohlednic	pohlednice	k1gFnPc2	pohlednice
<g/>
,	,	kIx,	,
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
místních	místní	k2eAgMnPc2d1	místní
i	i	k8xC	i
regionálních	regionální	k2eAgMnPc2d1	regionální
průvodců	průvodce	k1gMnPc2	průvodce
a	a	k8xC	a
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
upomínkových	upomínkový	k2eAgInPc2d1	upomínkový
předmětů	předmět	k1gInPc2	předmět
ze	z	k7c2	z
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
4	[number]	k4	4
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Strážný	strážný	k1gMnSc1	strážný
<g/>
)	)	kIx)	)
a	a	k8xC	a
22	[number]	k4	22
(	(	kIx(	(
<g/>
Draženov	Draženov	k1gInSc1	Draženov
-	-	kIx~	-
Klatovy	Klatovy	k1gInPc1	Klatovy
-	-	kIx~	-
Vodňany	Vodňan	k1gMnPc7	Vodňan
-	-	kIx~	-
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
190	[number]	k4	190
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trať	trať	k1gFnSc1	trať
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
elektrizována	elektrizován	k2eAgFnSc1d1	elektrizována
střídavou	střídavý	k2eAgFnSc7d1	střídavá
napájecí	napájecí	k2eAgFnSc7d1	napájecí
soustavou	soustava	k1gFnSc7	soustava
25	[number]	k4	25
<g/>
kV	kV	k?	kV
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
trať	trať	k1gFnSc4	trať
198	[number]	k4	198
Strakonice	Strakonice	k1gFnPc4	Strakonice
–	–	k?	–
Volary	Volara	k1gFnSc2	Volara
(	(	kIx(	(
<g/>
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Strakonice	Strakonice	k1gFnPc1	Strakonice
-	-	kIx~	-
Vimperk	Vimperk	k1gInSc1	Vimperk
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
203	[number]	k4	203
Strakonice	Strakonice	k1gFnPc1	Strakonice
-	-	kIx~	-
Březnice	Březnice	k1gFnSc1	Březnice
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pět	pět	k4xCc1	pět
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
firmy	firma	k1gFnSc2	firma
ČSAD	ČSAD	kA	ČSAD
STTRANS	STTRANS	kA	STTRANS
a.	a.	k?	a.
s.	s.	k?	s.
Autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
meziměstské	meziměstský	k2eAgFnSc2d1	meziměstská
dopravy	doprava	k1gFnSc2	doprava
leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
autobusové	autobusový	k2eAgNnSc1d1	autobusové
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
a	a	k8xC	a
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
vnitrostátní	vnitrostátní	k2eAgNnSc1d1	vnitrostátní
civilní	civilní	k2eAgNnSc1d1	civilní
letiště	letiště	k1gNnSc1	letiště
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
přistávacími	přistávací	k2eAgFnPc7d1	přistávací
<g/>
/	/	kIx~	/
<g/>
vzletovými	vzletový	k2eAgFnPc7d1	vzletová
dráhami	dráha	k1gFnPc7	dráha
–	–	k?	–
Letiště	letiště	k1gNnSc2	letiště
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společnost	společnost	k1gFnSc1	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Školství	školství	k1gNnSc2	školství
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
deset	deset	k4xCc4	deset
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
Čtyřlístek	čtyřlístek	k1gInSc1	čtyřlístek
<g/>
,	,	kIx,	,
U	u	k7c2	u
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
pak	pak	k6eAd1	pak
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
Lidická	lidický	k2eAgFnSc1d1	Lidická
<g/>
,	,	kIx,	,
A.	A.	kA	A.
B.	B.	kA	B.
Svojsíka	Svojsík	k1gMnSc2	Svojsík
<g/>
,	,	kIx,	,
Lidická	lidický	k2eAgFnSc1d1	Lidická
<g/>
,	,	kIx,	,
Spojařů	spojař	k1gMnPc2	spojař
<g/>
,	,	kIx,	,
Stavbařů	stavbař	k1gMnPc2	stavbař
<g/>
,	,	kIx,	,
Školní	školní	k2eAgFnSc1d1	školní
a	a	k8xC	a
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
(	(	kIx(	(
<g/>
ZŠ	ZŠ	kA	ZŠ
Povážská	povážský	k2eAgFnSc1d1	Povážská
<g/>
,	,	kIx,	,
ZŠ	ZŠ	kA	ZŠ
F.	F.	kA	F.
L.	L.	kA	L.
Čelakovského	Čelakovský	k2eAgMnSc4d1	Čelakovský
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
na	na	k7c4	na
ulicích	ulice	k1gFnPc6	ulice
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc4	tři
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
–	–	k?	–
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
Euroškola	Euroškola	k1gFnSc1	Euroškola
Strakonice	Strakonice	k1gFnPc1	Strakonice
střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
VOŠ	VOŠ	kA	VOŠ
<g/>
,	,	kIx,	,
SPŠ	SPŠ	kA	SPŠ
a	a	k8xC	a
SOŠ	SOŠ	kA	SOŠ
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
Strakonice	Strakonice	k1gFnPc4	Strakonice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
dudáckým	dudácký	k2eAgInSc7d1	dudácký
festivalem	festival	k1gInSc7	festival
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
600	[number]	k4	600
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dudáci	dudák	k1gMnPc1	dudák
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
desítky	desítka	k1gFnPc1	desítka
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novoroční	novoroční	k2eAgInSc1d1	novoroční
ohňostroj	ohňostroj	k1gInSc1	ohňostroj
–	–	k?	–
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
již	již	k6eAd1	již
tradiční	tradiční	k2eAgInSc1d1	tradiční
ohňostroj	ohňostroj	k1gInSc1	ohňostroj
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Otavou	Otava	k1gFnSc7	Otava
</s>
</p>
<p>
<s>
Strakonické	strakonický	k2eAgNnSc1d1	Strakonické
vítání	vítání	k1gNnSc1	vítání
léta	léto	k1gNnSc2	léto
–	–	k?	–
otevření	otevření	k1gNnSc1	otevření
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
přivítání	přivítání	k1gNnSc2	přivítání
nové	nový	k2eAgFnSc2d1	nová
turistické	turistický	k2eAgFnSc2d1	turistická
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc4d1	bohatý
kulturní	kulturní	k2eAgInSc4d1	kulturní
i	i	k8xC	i
sportovní	sportovní	k2eAgInSc4d1	sportovní
program	program	k1gInSc4	program
</s>
</p>
<p>
<s>
Tattoo	Tattoo	k6eAd1	Tattoo
Jam	jam	k1gInSc1	jam
–	–	k?	–
tetovací	tetovací	k2eAgInSc1d1	tetovací
a	a	k8xC	a
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
šampionát	šampionát	k1gInSc1	šampionát
mažoretek	mažoretka	k1gFnPc2	mažoretka
ČR	ČR	kA	ČR
–	–	k?	–
mistrovství	mistrovství	k1gNnSc2	mistrovství
Čech	Čechy	k1gFnPc2	Čechy
-	-	kIx~	-
soutěž	soutěž	k1gFnSc1	soutěž
mažoretkových	mažoretkův	k2eAgFnPc2d1	mažoretkův
skupin	skupina	k1gFnPc2	skupina
</s>
</p>
<p>
<s>
Dance	Danka	k1gFnSc3	Danka
show	show	k1gFnSc1	show
–	–	k?	–
celorepubliková	celorepublikový	k2eAgFnSc1d1	celorepubliková
přehlídka	přehlídka	k1gFnSc1	přehlídka
tanečních	taneční	k2eAgInPc2d1	taneční
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
kostelů	kostel	k1gInPc2	kostel
–	–	k?	–
netradiční	tradiční	k2eNgFnSc1d1	netradiční
prohlídka	prohlídka	k1gFnSc1	prohlídka
sakrálních	sakrální	k2eAgFnPc2d1	sakrální
staveb	stavba	k1gFnPc2	stavba
</s>
</p>
<p>
<s>
Pivovarská	pivovarský	k2eAgFnSc1d1	Pivovarská
pouť	pouť	k1gFnSc1	pouť
–	–	k?	–
kulturní	kulturní	k2eAgInSc4d1	kulturní
zážitek	zážitek	k1gInSc4	zážitek
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
milovníky	milovník	k1gMnPc4	milovník
dobrého	dobré	k1gNnSc2	dobré
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
zahájen	zahájit	k5eAaPmNgInS	zahájit
neckiádou	neckiáda	k1gFnSc7	neckiáda
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Otavě	Otava	k1gFnSc6	Otava
</s>
</p>
<p>
<s>
Rumpálování	Rumpálování	k1gNnSc1	Rumpálování
–	–	k?	–
středověké	středověký	k2eAgFnSc2d1	středověká
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnSc2	ukázka
zapomenutých	zapomenutý	k2eAgFnPc2d1	zapomenutá
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
šermířská	šermířský	k2eAgNnPc1d1	šermířské
a	a	k8xC	a
kejklířská	kejklířský	k2eAgNnPc1d1	kejklířské
vystoupení	vystoupení	k1gNnPc1	vystoupení
</s>
</p>
<p>
<s>
Běh	běh	k1gInSc1	běh
městem	město	k1gNnSc7	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
sportovní	sportovní	k2eAgFnSc1d1	sportovní
připomínka	připomínka	k1gFnSc1	připomínka
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
věkové	věkový	k2eAgFnPc4d1	věková
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
trať	trať	k1gFnSc4	trať
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
3,3	[number]	k4	3,3
nebo	nebo	k8xC	nebo
6,6	[number]	k4	6,6
km	km	kA	km
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
nejen	nejen	k6eAd1	nejen
sobě	se	k3xPyFc3	se
–	–	k?	–
multižánrový	multižánrový	k2eAgInSc4d1	multižánrový
festival	festival	k1gInSc4	festival
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
přiblížit	přiblížit	k5eAaPmF	přiblížit
lidem	lid	k1gInSc7	lid
strakonickou	strakonický	k2eAgFnSc4d1	Strakonická
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
oživit	oživit	k5eAaPmF	oživit
veřejná	veřejný	k2eAgNnPc4d1	veřejné
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
propojit	propojit	k5eAaPmF	propojit
kulturní	kulturní	k2eAgFnSc4d1	kulturní
scénu	scéna	k1gFnSc4	scéna
s	s	k7c7	s
místními	místní	k2eAgFnPc7d1	místní
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
lidi	člověk	k1gMnPc4	člověk
spojit	spojit	k5eAaPmF	spojit
k	k	k7c3	k
sobě	se	k3xPyFc3	se
</s>
</p>
<p>
<s>
Václavská	václavský	k2eAgFnSc1d1	Václavská
pouť	pouť	k1gFnSc1	pouť
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
poutí	pouť	k1gFnPc2	pouť
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
pouťové	pouťový	k2eAgFnPc4d1	pouťová
atrakce	atrakce	k1gFnPc4	atrakce
<g/>
,	,	kIx,	,
stánkový	stánkový	k2eAgInSc4d1	stánkový
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc4d1	bohatý
kulturní	kulturní	k2eAgInSc4d1	kulturní
i	i	k8xC	i
sportovní	sportovní	k2eAgInSc4d1	sportovní
program	program	k1gInSc4	program
</s>
</p>
<p>
<s>
Sportem	sport	k1gInSc7	sport
proti	proti	k7c3	proti
rakovině	rakovina	k1gFnSc3	rakovina
–	–	k?	–
dříve	dříve	k6eAd2	dříve
Běh	běh	k1gInSc1	běh
Terryho	Terry	k1gMnSc2	Terry
Foxe	fox	k1gInSc5	fox
a	a	k8xC	a
Běh	běh	k1gInSc1	běh
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
zákeřné	zákeřný	k2eAgFnSc3d1	zákeřná
nemoci	nemoc	k1gFnSc3	nemoc
</s>
</p>
<p>
<s>
Běh	běh	k1gInSc1	běh
okolo	okolo	k7c2	okolo
Kuřidla	kuřidlo	k1gNnSc2	kuřidlo
–	–	k?	–
závod	závod	k1gInSc4	závod
v	v	k7c6	v
přespolním	přespolní	k2eAgInSc6d1	přespolní
běhu	běh	k1gInSc6	běh
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
věkové	věkový	k2eAgFnPc4d1	věková
kategorie	kategorie	k1gFnPc4	kategorie
</s>
</p>
<p>
<s>
Adventní	adventní	k2eAgInPc4d1	adventní
trhy	trh	k1gInPc4	trh
pod	pod	k7c7	pod
Rumpálem	rumpál	k1gInSc7	rumpál
–	–	k?	–
tradiční	tradiční	k2eAgNnSc1d1	tradiční
adventní	adventní	k2eAgNnSc1d1	adventní
rozjímání	rozjímání	k1gNnSc1	rozjímání
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
II	II	kA	II
<g/>
.	.	kIx.	.
hradního	hradní	k2eAgNnSc2d1	hradní
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
spojeno	spojit	k5eAaPmNgNnS	spojit
i	i	k9	i
s	s	k7c7	s
mikulášskou	mikulášský	k2eAgFnSc7d1	Mikulášská
nadílkou	nadílka	k1gFnSc7	nadílka
a	a	k8xC	a
peklem	peklo	k1gNnSc7	peklo
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
SK	Sk	kA	Sk
Strakonice	Strakonice	k1gFnPc1	Strakonice
1908	[number]	k4	1908
se	se	k3xPyFc4	se
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
střídavě	střídavě	k6eAd1	střídavě
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
mezi	mezi	k7c7	mezi
třetí	třetí	k4xOgFnSc7	třetí
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc1	divize
A	a	k9	a
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
soutěží	soutěž	k1gFnSc7	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
obsadil	obsadit	k5eAaPmAgMnS	obsadit
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
divizi	divize	k1gFnSc6	divize
A	A	kA	A
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
sezonu	sezona	k1gFnSc4	sezona
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
zajistil	zajistit	k5eAaPmAgMnS	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
ČFL	ČFL	kA	ČFL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
HC	HC	kA	HC
Strakonice	Strakonice	k1gFnPc1	Strakonice
si	se	k3xPyFc3	se
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
krajském	krajský	k2eAgInSc6d1	krajský
hokejovém	hokejový	k2eAgInSc6d1	hokejový
přeboru	přebor	k1gInSc6	přebor
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
právo	právo	k1gNnSc4	právo
postupu	postup	k1gInSc2	postup
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
národní	národní	k2eAgFnSc2d1	národní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
nevyužil	využít	k5eNaPmAgMnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
juniorů	junior	k1gMnPc2	junior
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
krajskou	krajský	k2eAgFnSc4d1	krajská
ligu	liga	k1gFnSc4	liga
juniorů	junior	k1gMnPc2	junior
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
ligy	liga	k1gFnSc2	liga
juniorů	junior	k1gMnPc2	junior
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
i	i	k9	i
dorostenci	dorostenec	k1gMnPc1	dorostenec
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
hrají	hrát	k5eAaImIp3nP	hrát
ligu	liga	k1gFnSc4	liga
staršího	starý	k2eAgInSc2d2	starší
dorostu	dorost	k1gInSc2	dorost
a	a	k8xC	a
zůstali	zůstat	k5eAaPmAgMnP	zůstat
i	i	k9	i
v	v	k7c6	v
krajské	krajský	k2eAgFnSc6d1	krajská
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
působí	působit	k5eAaImIp3nP	působit
také	také	k9	také
TJ	tj	kA	tj
ČZ	ČZ	kA	ČZ
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
volejbalistky	volejbalistka	k1gFnPc4	volejbalistka
a	a	k8xC	a
volejbalisty	volejbalista	k1gMnPc4	volejbalista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
krajský	krajský	k2eAgInSc4d1	krajský
přebor	přebor	k1gInSc4	přebor
II	II	kA	II
a	a	k8xC	a
okresní	okresní	k2eAgFnSc1d1	okresní
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
hrají	hrát	k5eAaImIp3nP	hrát
I.	I.	kA	I.
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
ročníku	ročník	k1gInSc6	ročník
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
vybojovaly	vybojovat	k5eAaPmAgFnP	vybojovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
JUDO	judo	k1gNnSc1	judo
1990	[number]	k4	1990
Strakonice	Strakonice	k1gFnPc1	Strakonice
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
oddílu	oddíl	k1gInSc2	oddíl
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Zelinka	Zelinka	k1gMnSc1	Zelinka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
Libor	Libor	k1gMnSc1	Libor
Pešek	Pešek	k1gMnSc1	Pešek
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
dan	dan	k?	dan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
klubu	klub	k1gInSc2	klub
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
účastní	účastnit	k5eAaImIp3nP	účastnit
kvalifikačních	kvalifikační	k2eAgInPc2d1	kvalifikační
turnajů	turnaj	k1gInPc2	turnaj
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
účastí	účast	k1gFnSc7	účast
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
kategoriích	kategorie	k1gFnPc6	kategorie
na	na	k7c6	na
přeborech	přebor	k1gInPc6	přebor
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
funguje	fungovat	k5eAaImIp3nS	fungovat
ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
florbalový	florbalový	k2eAgInSc1d1	florbalový
tým	tým	k1gInSc1	tým
FbC	FbC	k1gFnSc1	FbC
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
ligy	liga	k1gFnSc2	liga
florbalové	florbalový	k2eAgFnSc2d1	florbalová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
koná	konat	k5eAaImIp3nS	konat
Memoriál	memoriál	k1gInSc1	memoriál
Petra	Petr	k1gMnSc2	Petr
Adlera	Adler	k1gMnSc2	Adler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
působení	působení	k1gNnSc4	působení
bývalého	bývalý	k2eAgMnSc2d1	bývalý
hráče	hráč	k1gMnSc2	hráč
a	a	k8xC	a
trenéra	trenér	k1gMnSc2	trenér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
vážné	vážný	k2eAgFnPc4d1	vážná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HBC	HBC	kA	HBC
Strakonice	Strakonice	k1gFnPc1	Strakonice
1921	[number]	k4	1921
je	být	k5eAaImIp3nS	být
oddílem	oddíl	k1gInSc7	oddíl
házenkářů	házenkář	k1gMnPc2	házenkář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hrají	hrát	k5eAaImIp3nP	hrát
extraligu	extraliga	k1gFnSc4	extraliga
mužů	muž	k1gMnPc2	muž
-	-	kIx~	-
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
českou	český	k2eAgFnSc4d1	Česká
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
měly	mít	k5eAaImAgFnP	mít
Strakonice	Strakonice	k1gFnPc1	Strakonice
ragbyový	ragbyový	k2eAgInSc1d1	ragbyový
tým	tým	k1gInSc1	tým
RC	RC	kA	RC
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
společný	společný	k2eAgInSc4d1	společný
tým	tým	k1gInSc4	tým
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Pískem	Písek	k1gInSc7	Písek
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
soutěž	soutěž	k1gFnSc4	soutěž
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgNnSc1d1	vodní
pólo	pólo	k1gNnSc1	pólo
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
TJ	tj	kA	tj
Fezko	Fezko	k1gNnSc1	Fezko
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
po	po	k7c4	po
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zdejší	zdejší	k2eAgInSc1d1	zdejší
mužský	mužský	k2eAgInSc1d1	mužský
tým	tým	k1gInSc1	tým
stal	stát	k5eAaPmAgInS	stát
devětkrát	devětkrát	k6eAd1	devětkrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
mistrem	mistr	k1gMnSc7	mistr
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
české	český	k2eAgFnSc2d1	Česká
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Strakonický	strakonický	k2eAgInSc1d1	strakonický
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
založen	založit	k5eAaPmNgInS	založit
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Otavy	Otava	k1gFnSc2	Otava
a	a	k8xC	a
Volyňky	Volyňka	k1gFnSc2	Volyňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1243	[number]	k4	1243
daroval	darovat	k5eAaPmAgMnS	darovat
Bavorem	Bavor	k1gMnSc7	Bavor
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
část	část	k1gFnSc1	část
hradu	hrad	k1gInSc2	hrad
řádu	řád	k1gInSc2	řád
maltézských	maltézský	k2eAgMnPc2d1	maltézský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
ve	v	k7c6	v
stísněném	stísněný	k2eAgInSc6d1	stísněný
prostoru	prostor	k1gInSc6	prostor
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
komendu	komenda	k1gFnSc4	komenda
(	(	kIx(	(
<g/>
klášter	klášter	k1gInSc1	klášter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
raně	raně	k6eAd1	raně
gotické	gotický	k2eAgFnSc6d1	gotická
části	část	k1gFnSc6	část
kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
kapitulní	kapitulní	k2eAgFnSc7d1	kapitulní
síní	síň	k1gFnSc7	síň
a	a	k8xC	a
ambitem	ambit	k1gInSc7	ambit
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
řada	řada	k1gFnSc1	řada
stavebních	stavební	k2eAgInPc2d1	stavební
detailů	detail	k1gInPc2	detail
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
cenné	cenný	k2eAgFnPc4d1	cenná
fresky	freska	k1gFnPc4	freska
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1340	[number]	k4	1340
<g/>
)	)	kIx)	)
v	v	k7c6	v
ambitu	ambit	k1gInSc6	ambit
a	a	k8xC	a
na	na	k7c6	na
kruchtě	kruchta	k1gFnSc6	kruchta
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
věž	věž	k1gFnSc1	věž
Rumpál	rumpál	k1gInSc1	rumpál
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
věž	věž	k1gFnSc1	věž
Jelenka	Jelenka	k1gFnSc1	Jelenka
byla	být	k5eAaImAgFnS	být
přistavěna	přistavět	k5eAaPmNgFnS	přistavět
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
1714	[number]	k4	1714
<g/>
–	–	k?	–
<g/>
1721	[number]	k4	1721
barokní	barokní	k2eAgFnSc7d1	barokní
západní	západní	k2eAgFnSc7d1	západní
zámeckou	zámecký	k2eAgFnSc7d1	zámecká
přístavbou	přístavba	k1gFnSc7	přístavba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
hradu	hrad	k1gInSc2	hrad
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
prvky	prvek	k1gInPc4	prvek
několika	několik	k4yIc2	několik
architektonických	architektonický	k2eAgInPc2d1	architektonický
slohů	sloh	k1gInPc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
dobyt	dobyt	k2eAgInSc1d1	dobyt
a	a	k8xC	a
zpustošen	zpustošen	k2eAgInSc1d1	zpustošen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
areál	areál	k1gInSc1	areál
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
za	za	k7c7	za
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
prošel	projít	k5eAaPmAgInS	projít
nákladnou	nákladný	k2eAgFnSc7d1	nákladná
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
(	(	kIx(	(
<g/>
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
fondů	fond	k1gInPc2	fond
EU	EU	kA	EU
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Norských	norský	k2eAgInPc2d1	norský
fondů	fond	k1gInPc2	fond
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
areálu	areál	k1gInSc6	areál
sídlí	sídlet	k5eAaImIp3nS	sídlet
Muzeum	muzeum	k1gNnSc1	muzeum
středního	střední	k2eAgNnSc2d1	střední
Pootaví	Pootaví	k1gNnSc2	Pootaví
<g/>
,	,	kIx,	,
Šmidingerova	Šmidingerův	k2eAgFnSc1d1	Šmidingerova
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
opravené	opravený	k2eAgInPc1d1	opravený
sály	sál	k1gInPc1	sál
(	(	kIx(	(
<g/>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
Maltézský	maltézský	k2eAgInSc1d1	maltézský
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
sál	sál	k1gInSc1	sál
U	u	k7c2	u
Kata	kat	k1gMnSc2	kat
<g/>
)	)	kIx)	)
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
panského	panský	k2eAgInSc2d1	panský
pivovaru	pivovar	k1gInSc2	pivovar
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
představením	představení	k1gNnPc3	představení
a	a	k8xC	a
výstavám	výstava	k1gFnPc3	výstava
Městské	městský	k2eAgNnSc4d1	Městské
kulturní	kulturní	k2eAgNnSc4d1	kulturní
středisko	středisko	k1gNnSc4	středisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
uprostřed	uprostřed	k7c2	uprostřed
Palackého	Palacký	k1gMnSc2	Palacký
náměstí	náměstí	k1gNnSc2	náměstí
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1730	[number]	k4	1730
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
,	,	kIx,	,
též	též	k9	též
morový	morový	k2eAgInSc1d1	morový
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
Velkém	velký	k2eAgNnSc6d1	velké
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgInSc2d2	starší
sloupu	sloup	k1gInSc2	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1586	[number]	k4	1586
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1580	[number]	k4	1580
<g/>
–	–	k?	–
<g/>
1583	[number]	k4	1583
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
V.	V.	kA	V.
Vogarelliho	Vogarelli	k1gMnSc2	Vogarelli
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
průčelí	průčelí	k1gNnSc2	průčelí
upraveno	upravit	k5eAaPmNgNnS	upravit
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
.	.	kIx.	.
</s>
<s>
Jednolodní	jednolodní	k2eAgFnSc1d1	jednolodní
stavba	stavba	k1gFnSc1	stavba
se	s	k7c7	s
síťovou	síťový	k2eAgFnSc7d1	síťová
klenbou	klenba	k1gFnSc7	klenba
a	a	k8xC	a
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
blízko	blízko	k6eAd1	blízko
obou	dva	k4xCgNnPc2	dva
nádraží	nádraží	k1gNnPc2	nádraží
s	s	k7c7	s
barokními	barokní	k2eAgFnPc7d1	barokní
úpravami	úprava	k1gFnPc7	úprava
</s>
</p>
<p>
<s>
Poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
v	v	k7c6	v
části	část	k1gFnSc6	část
Podsrp	Podsrp	k1gInSc1	Podsrp
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
trojkřídlý	trojkřídlý	k2eAgInSc1d1	trojkřídlý
ambit	ambit	k1gInSc1	ambit
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
kostela	kostel	k1gInSc2	kostel
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jednopatrová	jednopatrový	k2eAgFnSc1d1	jednopatrová
fara	fara	k1gFnSc1	fara
s	s	k7c7	s
volutovými	volutový	k2eAgInPc7d1	volutový
štíty	štít	k1gInPc7	štít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
Velkém	velký	k2eAgNnSc6d1	velké
náměstí	náměstí	k1gNnSc6	náměstí
–	–	k?	–
zdobená	zdobený	k2eAgFnSc1d1	zdobená
sgrafity	sgrafito	k1gNnPc7	sgrafito
akademického	akademický	k2eAgMnSc2d1	akademický
malíře	malíř	k1gMnSc2	malíř
Josefa	Josef	k1gMnSc2	Josef
Bosáčka	bosáček	k1gMnSc2	bosáček
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
České	český	k2eAgFnSc2d1	Česká
spořitelny	spořitelna	k1gFnSc2	spořitelna
–	–	k?	–
sgrafitová	sgrafitový	k2eAgFnSc1d1	sgrafitová
výzdoba	výzdoba	k1gFnSc1	výzdoba
taktéž	taktéž	k?	taktéž
Josefa	Josef	k1gMnSc2	Josef
Bosáčka	bosáček	k1gMnSc2	bosáček
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Václava	Václav	k1gMnSc2	Václav
Malého	Malý	k1gMnSc2	Malý
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
Strakonic	Strakonice	k1gFnPc2	Strakonice
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Pracejovice	Pracejovice	k1gFnPc4	Pracejovice
</s>
</p>
<p>
<s>
Masné	masný	k2eAgInPc1d1	masný
krámy	krám	k1gInPc1	krám
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
142	[number]	k4	142
<g/>
)	)	kIx)	)
–	–	k?	–
původní	původní	k2eAgFnSc1d1	původní
středověká	středověký	k2eAgFnSc1d1	středověká
zástavba	zástavba	k1gFnSc1	zástavba
</s>
</p>
<p>
<s>
Domy	dům	k1gInPc1	dům
čp.	čp.	k?	čp.
44	[number]	k4	44
a	a	k8xC	a
45	[number]	k4	45
–	–	k?	–
tzv.	tzv.	kA	tzv.
Papežovy	Papežův	k2eAgInPc1d1	Papežův
domy	dům	k1gInPc1	dům
</s>
</p>
<p>
<s>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c6	na
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
47	[number]	k4	47
(	(	kIx(	(
<g/>
Velké	velká	k1gFnSc2	velká
nám.	nám.	k?	nám.
<g/>
)	)	kIx)	)
s	s	k7c7	s
reliéfem	reliéf	k1gInSc7	reliéf
profesora	profesor	k1gMnSc2	profesor
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
promluvil	promluvit	k5eAaPmAgMnS	promluvit
16	[number]	k4	16
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1891	[number]	k4	1891
svoji	svůj	k3xOyFgFnSc4	svůj
kandidátní	kandidátní	k2eAgFnSc4d1	kandidátní
řeč	řeč	k1gFnSc4	řeč
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
zvolením	zvolení	k1gNnSc7	zvolení
za	za	k7c4	za
poslance	poslanec	k1gMnPc4	poslanec
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Josefa	Josef	k1gMnSc2	Josef
Skupy	skupa	k1gFnSc2	skupa
na	na	k7c6	na
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
140	[number]	k4	140
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
hostinec	hostinec	k1gInSc1	hostinec
U	u	k7c2	u
Švehlů	Švehla	k1gMnPc2	Švehla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pozdně	pozdně	k6eAd1	pozdně
empírový	empírový	k2eAgInSc1d1	empírový
gloriet	gloriet	k1gInSc1	gloriet
–	–	k?	–
drobná	drobná	k1gFnSc1	drobná
stavba	stavba	k1gFnSc1	stavba
podobající	podobající	k2eAgFnSc1d1	podobající
se	se	k3xPyFc4	se
otevřenému	otevřený	k2eAgInSc3d1	otevřený
antickému	antický	k2eAgInSc3d1	antický
chrámu	chrám	k1gInSc3	chrám
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
umělém	umělý	k2eAgNnSc6d1	umělé
návrší	návrší	k1gNnSc6	návrší
v	v	k7c6	v
Rennerových	Rennerův	k2eAgInPc6d1	Rennerův
sadech	sad	k1gInPc6	sad
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
Antonína	Antonín	k1gMnSc2	Antonín
Rennera	Renner	k1gMnSc2	Renner
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Čelakovského	Čelakovský	k2eAgMnSc2d1	Čelakovský
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Břetislava	Břetislav	k1gMnSc2	Břetislav
Bendy	Benda	k1gMnSc2	Benda
</s>
</p>
<p>
<s>
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
–	–	k?	–
u	u	k7c2	u
bývalého	bývalý	k2eAgInSc2d1	bývalý
Siebertova	Siebertův	k2eAgInSc2d1	Siebertův
ústavu	ústav	k1gInSc2	ústav
pod	pod	k7c7	pod
areálem	areál	k1gInSc7	areál
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
domov	domov	k1gInSc4	domov
důchodců	důchodce	k1gMnPc2	důchodce
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Ocelová	ocelový	k2eAgFnSc1d1	ocelová
lávka	lávka	k1gFnSc1	lávka
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dudy	dudy	k1gFnPc1	dudy
–	–	k?	–
na	na	k7c6	na
kruhovém	kruhový	k2eAgInSc6d1	kruhový
objezdu	objezd	k1gInSc6	objezd
u	u	k7c2	u
Fezka	Fezko	k1gNnSc2	Fezko
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Ing.	ing.	kA	ing.
Pavel	Pavel	k1gMnSc1	Pavel
Pavel	Pavel	k1gMnSc1	Pavel
</s>
</p>
<p>
<s>
Stonehenge	Stonehenge	k1gFnSc1	Stonehenge
–	–	k?	–
replika	replika	k1gFnSc1	replika
brány	brána	k1gFnSc2	brána
pravěké	pravěký	k2eAgFnSc2d1	pravěká
svatyně	svatyně	k1gFnSc2	svatyně
<g/>
,	,	kIx,	,
také	také	k9	také
od	od	k7c2	od
Ing.	ing.	kA	ing.
Pavla	Pavel	k1gMnSc4	Pavel
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
Stonehenge	Stoneheng	k1gInSc2	Stoneheng
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
</s>
</p>
<p>
<s>
Memento	memento	k1gNnSc1	memento
strakonických	strakonický	k2eAgMnPc2d1	strakonický
Židů	Žid	k1gMnPc2	Žid
–	–	k?	–
podoba	podoba	k1gFnSc1	podoba
sedmiramenného	sedmiramenný	k2eAgInSc2d1	sedmiramenný
svícnu	svícen	k1gInSc2	svícen
menora	menora	k1gFnSc1	menora
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
doplněný	doplněný	k2eAgInSc1d1	doplněný
kamenem	kámen	k1gInSc7	kámen
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Otavy	Otava	k1gFnSc2	Otava
-	-	kIx~	-
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
Měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
pivovaru	pivovar	k1gInSc2	pivovar
nedaleko	nedaleko	k7c2	nedaleko
stavby	stavba	k1gFnSc2	stavba
nové	nový	k2eAgFnSc2d1	nová
lávky	lávka	k1gFnSc2	lávka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
hranicích	hranice	k1gFnPc6	hranice
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
či	či	k8xC	či
do	do	k7c2	do
nich	on	k3xPp3gNnPc2	on
alespoň	alespoň	k9	alespoň
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
následující	následující	k2eAgNnPc1d1	následující
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
PR	pr	k0	pr
Bažantnice	bažantnice	k1gFnPc4	bažantnice
u	u	k7c2	u
Pracejovic	Pracejovice	k1gFnPc2	Pracejovice
</s>
</p>
<p>
<s>
PR	pr	k0	pr
Kuřidlo	kuřidlo	k1gNnSc4	kuřidlo
</s>
</p>
<p>
<s>
PP	PP	kA	PP
Ryšovy	Ryšův	k2eAgFnPc1d1	Ryšův
</s>
</p>
<p>
<s>
PP	PP	kA	PP
Tůně	tůně	k1gFnSc1	tůně
u	u	k7c2	u
Hajské	Hajská	k1gFnSc2	Hajská
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
několik	několik	k4yIc1	několik
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Otavský	otavský	k2eAgInSc1d1	otavský
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Otavy	Otava	k1gFnSc2	Otava
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Podsrpenské	Podsrpenský	k2eAgFnPc1d1	Podsrpenská
lípy	lípa	k1gFnPc1	lípa
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
stromy	strom	k1gInPc1	strom
v	v	k7c6	v
Podsrpenské	Podsrpenský	k2eAgFnSc6d1	Podsrpenská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
z	z	k7c2	z
města	město	k1gNnSc2	město
asi	asi	k9	asi
150	[number]	k4	150
m	m	kA	m
za	za	k7c7	za
vodojemem	vodojem	k1gInSc7	vodojem
</s>
</p>
<p>
<s>
Švandova	Švandův	k2eAgFnSc1d1	Švandova
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Na	na	k7c6	na
Kalvárii	Kalvárie	k1gFnSc6	Kalvárie
<g/>
,	,	kIx,	,
někdejším	někdejší	k2eAgInSc6d1	někdejší
hřbitově	hřbitov	k1gInSc6	hřbitov
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Pracejovic	Pracejovice	k1gFnPc2	Pracejovice
</s>
</p>
<p>
<s>
Václavská	václavský	k2eAgFnSc1d1	Václavská
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Podsrpenské	Podsrpenský	k2eAgFnSc6d1	Podsrpenská
ulici	ulice	k1gFnSc6	ulice
poblíž	poblíž	k6eAd1	poblíž
hasičské	hasičský	k2eAgFnSc2d1	hasičská
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
===	===	k?	===
Zaniklé	zaniklý	k2eAgFnPc1d1	zaniklá
stavby	stavba	k1gFnPc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
západně	západně	k6eAd1	západně
od	od	k7c2	od
strakonického	strakonický	k2eAgInSc2d1	strakonický
hradu	hrad	k1gInSc2	hrad
stála	stát	k5eAaImAgFnS	stát
gotická	gotický	k2eAgFnSc1d1	gotická
kaple	kaple	k1gFnSc1	kaple
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
rozprostíral	rozprostírat	k5eAaImAgInS	rozprostírat
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
podle	podle	k7c2	podle
lidové	lidový	k2eAgFnSc2d1	lidová
pověsti	pověst	k1gFnSc2	pověst
byl	být	k5eAaImAgMnS	být
pochován	pochován	k2eAgMnSc1d1	pochován
legendární	legendární	k2eAgMnSc1d1	legendární
Švanda	Švanda	k1gMnSc1	Švanda
dudák	dudák	k1gMnSc1	dudák
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kapli	kaple	k1gFnSc3	kaple
vedla	vést	k5eAaImAgFnS	vést
Křížová	Křížová	k1gFnSc1	Křížová
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
MUDr.	MUDr.	kA	MUDr.
Jan	Jan	k1gMnSc1	Jan
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Cvrček	Cvrček	k1gMnSc1	Cvrček
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
internista	internista	k1gMnSc1	internista
na	na	k7c6	na
strakonické	strakonický	k2eAgFnSc6d1	Strakonická
poliklinice	poliklinika	k1gFnSc6	poliklinika
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
okresního	okresní	k2eAgInSc2d1	okresní
ústavu	ústav	k1gInSc2	ústav
národního	národní	k2eAgNnSc2d1	národní
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
přednosta	přednosta	k1gMnSc1	přednosta
oddělení	oddělení	k1gNnSc2	oddělení
klinické	klinický	k2eAgFnSc2d1	klinická
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
dokončil	dokončit	k5eAaPmAgInS	dokončit
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svým	svůj	k3xOyFgFnPc3	svůj
rodným	rodný	k2eAgFnPc3d1	rodná
Strakonicím	Strakonice	k1gFnPc3	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
středního	střední	k2eAgNnSc2d1	střední
Pootaví	Pootaví	k1gNnSc2	Pootaví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bude	být	k5eAaImBp3nS	být
hledat	hledat	k5eAaImF	hledat
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vydání	vydání	k1gNnSc3	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Cvrček	Cvrček	k1gMnSc1	Cvrček
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
s	s	k7c7	s
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
rozhlasem	rozhlas	k1gInSc7	rozhlas
a	a	k8xC	a
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
–	–	k?	–
<g/>
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
slavista	slavista	k1gMnSc1	slavista
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dvořák	Dvořák	k1gMnSc1	Dvořák
Šumavský	šumavský	k2eAgMnSc1d1	šumavský
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
Mnoho	mnoho	k4c4	mnoho
obrazů	obraz	k1gInPc2	obraz
namaloval	namalovat	k5eAaPmAgMnS	namalovat
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
podepisoval	podepisovat	k5eAaImAgInS	podepisovat
"	"	kIx"	"
<g/>
Šumavský	šumavský	k2eAgInSc1d1	šumavský
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Formánek	Formánek	k1gMnSc1	Formánek
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dudák	dudák	k1gMnSc1	dudák
a	a	k8xC	a
propagátor	propagátor	k1gMnSc1	propagátor
dudácké	dudácký	k2eAgFnSc2d1	dudácká
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
dudácké	dudácký	k2eAgFnSc2d1	dudácká
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
králem	král	k1gMnSc7	král
dudáků	dudák	k1gMnPc2	dudák
</s>
</p>
<p>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Gruber	Gruber	k1gMnSc1	Gruber
(	(	kIx(	(
<g/>
1683	[number]	k4	1683
<g/>
–	–	k?	–
<g/>
1737	[number]	k4	1737
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převor	převor	k1gMnSc1	převor
vyšebrodského	vyšebrodský	k2eAgInSc2d1	vyšebrodský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
hvězdář	hvězdář	k1gMnSc1	hvězdář
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Táňa	Táňa	k1gFnSc1	Táňa
Hodanová	Hodanová	k1gFnSc1	Hodanová
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
režisérka	režisérka	k1gFnSc1	režisérka
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Horký	Horký	k1gMnSc1	Horký
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hrubý	Hrubý	k1gMnSc1	Hrubý
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
dudák	dudák	k1gMnSc1	dudák
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
na	na	k7c6	na
LŠU	LŠU	kA	LŠU
Strakonice	Strakonice	k1gFnPc1	Strakonice
(	(	kIx(	(
<g/>
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
výuky	výuka	k1gFnSc2	výuka
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
dudy	dudy	k1gFnPc4	dudy
a	a	k8xC	a
vychoval	vychovat	k5eAaPmAgInS	vychovat
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
dudáků	dudák	k1gInPc2	dudák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Strakonické	strakonický	k2eAgFnSc2d1	Strakonická
dudácké	dudácký	k2eAgFnSc2d1	dudácká
muziky	muzika	k1gFnSc2	muzika
</s>
</p>
<p>
<s>
Valentin	Valentin	k1gMnSc1	Valentin
Kochan	kochan	k1gMnSc1	kochan
z	z	k7c2	z
Prachové	prachový	k2eAgFnSc2d1	prachová
(	(	kIx(	(
<g/>
1561	[number]	k4	1561
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
magistr	magistr	k1gMnSc1	magistr
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
zemský	zemský	k2eAgMnSc1d1	zemský
direktor	direktor	k1gMnSc1	direktor
<g/>
,	,	kIx,	,
měšťan	měšťan	k1gMnSc1	měšťan
<g/>
,	,	kIx,	,
písař	písař	k1gMnSc1	písař
a	a	k8xC	a
přední	přední	k2eAgMnPc1d1	přední
radní	radní	k1gMnPc1	radní
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
Pražského	pražský	k2eAgNnSc2d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
defenestraci	defenestrace	k1gFnSc6	defenestrace
(	(	kIx(	(
<g/>
svržení	svržení	k1gNnSc1	svržení
místodržících	místodržící	k1gMnPc2	místodržící
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
pražské	pražský	k2eAgFnSc2d1	Pražská
hradní	hradní	k2eAgFnSc2d1	hradní
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
)	)	kIx)	)
jej	on	k3xPp3gMnSc4	on
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
shromáždění	shromáždění	k1gNnSc1	shromáždění
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
30	[number]	k4	30
direktorů	direktor	k1gMnPc2	direktor
(	(	kIx(	(
<g/>
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
sťat	stnout	k5eAaPmNgInS	stnout
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1621	[number]	k4	1621
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Křížková	Křížková	k1gFnSc1	Křížková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Česlav	Česlat	k5eAaPmDgInS	Česlat
Augustin	Augustin	k1gMnSc1	Augustin
Ludikar	Ludikar	k1gInSc1	Ludikar
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
maltézských	maltézský	k2eAgMnPc2d1	maltézský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgInS	sepsat
první	první	k4xOgInSc4	první
česky	česky	k6eAd1	česky
psané	psaný	k2eAgFnPc1d1	psaná
dějiny	dějiny	k1gFnPc1	dějiny
maltézského	maltézský	k2eAgInSc2d1	maltézský
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Machoninová	Machoninový	k2eAgFnSc1d1	Machoninová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architektka	architektka	k1gFnSc1	architektka
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Mareš	Mareš	k1gMnSc1	Mareš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesionální	profesionální	k2eAgMnSc1d1	profesionální
diskžokej	diskžokej	k1gMnSc1	diskžokej
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
sběratel	sběratel	k1gMnSc1	sběratel
autogramů	autogram	k1gInPc2	autogram
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
30	[number]	k4	30
let	léto	k1gNnPc2	léto
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
diskotékách	diskotéka	k1gFnPc6	diskotéka
na	na	k7c6	na
Strakonicku	Strakonicko	k1gNnSc6	Strakonicko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
okres	okres	k1gInSc4	okres
Strakonice	Strakonice	k1gFnPc1	Strakonice
ve	v	k7c6	v
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Meitner	Meitner	k1gMnSc1	Meitner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
na	na	k7c6	na
Strakonicku	Strakonicko	k1gNnSc6	Strakonicko
trvale	trvale	k6eAd1	trvale
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
Strakonicemi	Strakonice	k1gFnPc7	Strakonice
jsou	být	k5eAaImIp3nP	být
inspirované	inspirovaný	k2eAgInPc4d1	inspirovaný
některé	některý	k3yIgInPc4	některý
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
a	a	k8xC	a
kresby	kresba	k1gFnPc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zejména	zejména	k6eAd1	zejména
motivy	motiv	k1gInPc4	motiv
strakonického	strakonický	k2eAgInSc2d1	strakonický
hradu	hrad	k1gInSc2	hrad
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
Rumpál	rumpál	k1gInSc4	rumpál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Mráz	Mráz	k1gMnSc1	Mráz
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basbarytonista	basbarytonista	k1gMnSc1	basbarytonista
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Oftalmius	Oftalmius	k1gInSc4	Oftalmius
Strakonický	strakonický	k2eAgInSc4d1	strakonický
z	z	k7c2	z
Oskořína	Oskořín	k1gMnSc2	Oskořín
(	(	kIx(	(
<g/>
1550	[number]	k4	1550
<g/>
–	–	k?	–
<g/>
cca	cca	kA	cca
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgMnSc1d1	soudní
a	a	k8xC	a
městský	městský	k2eAgMnSc1d1	městský
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Pavel	Pavel	k1gMnSc1	Pavel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgMnSc1d1	experimentální
archeolog	archeolog	k1gMnSc1	archeolog
</s>
</p>
<p>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Pondělíček	Pondělíček	k1gMnSc1	Pondělíček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
psycholog	psycholog	k1gMnSc1	psycholog
<g/>
,	,	kIx,	,
sexuolog	sexuolog	k1gMnSc1	sexuolog
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Režný	režný	k2eAgMnSc1d1	režný
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
výrazné	výrazný	k2eAgFnPc4d1	výrazná
osobnosti	osobnost	k1gFnPc4	osobnost
českého	český	k2eAgInSc2d1	český
národopisu	národopis	k1gInSc2	národopis
<g/>
,	,	kIx,	,
nestor	nestor	k1gMnSc1	nestor
strakonických	strakonický	k2eAgMnPc2d1	strakonický
dudáků	dudák	k1gMnPc2	dudák
</s>
</p>
<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Salák	Salák	k1gMnSc1	Salák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
KHL	KHL	kA	KHL
za	za	k7c4	za
tým	tým	k1gInSc4	tým
SKA	SKA	kA	SKA
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
účastníkem	účastník	k1gMnSc7	účastník
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
i	i	k8xC	i
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutkář	loutkář	k1gMnSc1	loutkář
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Škoda	Škoda	k1gMnSc1	Škoda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operetní	operetní	k2eAgMnSc1d1	operetní
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
sólista	sólista	k1gMnSc1	sólista
hudebního	hudební	k2eAgNnSc2d1	hudební
divadla	divadlo	k1gNnSc2	divadlo
Karlín	Karlín	k1gInSc1	Karlín
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Šmidinger	Šmidinger	k1gMnSc1	Šmidinger
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
–	–	k?	–
<g/>
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
strakonické	strakonický	k2eAgFnSc2d1	Strakonická
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
předlohou	předloha	k1gFnSc7	předloha
k	k	k7c3	k
Jiráskově	Jiráskův	k2eAgFnSc3d1	Jiráskova
postavě	postava	k1gFnSc3	postava
pátera	páter	k1gMnSc2	páter
Vrby	Vrba	k1gMnSc2	Vrba
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
F.	F.	kA	F.
L.	L.	kA	L.
Věk	věk	k1gInSc4	věk
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Troška	troška	k6eAd1	troška
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
trilogie	trilogie	k1gFnSc1	trilogie
"	"	kIx"	"
<g/>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
natáčena	natáčet	k5eAaImNgFnS	natáčet
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
O	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
Jasněnce	Jasněnka	k1gFnSc6	Jasněnka
a	a	k8xC	a
létajícím	létající	k2eAgInSc6d1	létající
ševci	švec	k1gMnPc1	švec
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
štěstí	štěstí	k1gNnSc2	štěstí
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Turek	Turek	k1gMnSc1	Turek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
hráč	hráč	k1gMnSc1	hráč
severoamerické	severoamerický	k2eAgFnSc6d1	severoamerická
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Držitel	držitel	k1gMnSc1	držitel
Jennings	Jennings	k1gInSc4	Jennings
Trophy	Tropha	k1gFnSc2	Tropha
za	za	k7c4	za
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
a	a	k8xC	a
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hokejky	hokejka	k1gFnSc2	hokejka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Beck	Beck	k1gMnSc1	Beck
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
designér	designér	k1gMnSc1	designér
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Bad	Bad	k?	Bad
Salzungen	Salzungen	k1gInSc1	Salzungen
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Durynsko	Durynsko	k1gNnSc1	Durynsko
–	–	k?	–
Stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Calderdale	Calderdale	k6eAd1	Calderdale
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
–	–	k?	–
Stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Lengnau	Lengnau	k6eAd1	Lengnau
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Kanton	Kanton	k1gInSc1	Kanton
Bern	Bern	k1gInSc1	Bern
–	–	k?	–
Stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-239-9816-0	[number]	k4	978-80-239-9816-0
</s>
</p>
<p>
<s>
Vlastivědný	vlastivědný	k2eAgInSc1d1	vlastivědný
sborník	sborník	k1gInSc1	sborník
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl-	díl-	k?	díl-
,	,	kIx,	,
město	město	k1gNnSc1	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-238-7889-1	[number]	k4	80-238-7889-1
</s>
</p>
<p>
<s>
Vlastivědný	vlastivědný	k2eAgInSc1d1	vlastivědný
sborník	sborník	k1gInSc1	sborník
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-239-1640-8	[number]	k4	80-239-1640-8
</s>
</p>
<p>
<s>
Vlastivědný	vlastivědný	k2eAgInSc1d1	vlastivědný
sborník	sborník	k1gInSc1	sborník
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-239-1640-8	[number]	k4	80-239-1640-8
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
Simona	Simona	k1gFnSc1	Simona
Kotlárová	Kotlárový	k2eAgFnSc1d1	Kotlárová
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-86829-59-3	[number]	k4	978-80-86829-59-3
</s>
</p>
<p>
<s>
Obecní	obecní	k2eAgFnSc1d1	obecní
kronika	kronika	k1gFnSc1	kronika
1916-	[number]	k4	1916-
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Simona	Simona	k1gFnSc1	Simona
Kotlárová	Kotlárový	k2eAgFnSc1d1	Kotlárová
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-239-6208-6	[number]	k4	80-239-6208-6
</s>
</p>
<p>
<s>
Bavorové	Bavorové	k?	Bavorové
erbu	erb	k1gInSc2	erb
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
Simona	Simona	k1gFnSc1	Simona
Kotlárová	Kotlárový	k2eAgFnSc1d1	Kotlárová
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86829-04-9	[number]	k4	80-86829-04-9
</s>
</p>
<p>
<s>
Páni	pan	k1gMnPc1	pan
ze	z	k7c2	z
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7422-034-0	[number]	k4	978-80-7422-034-0
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
řeči	řeč	k1gFnPc1	řeč
<g/>
,	,	kIx,	,
vážné-	vážné-	k?	vážné-
nevážné-	nevážné-	k?	nevážné-
darebné	darebný	k2eAgFnSc2d1	darebná
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Režný	režný	k2eAgMnSc1d1	režný
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-904285-0-8	[number]	k4	978-80-904285-0-8
</s>
</p>
<p>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
dudáků	dudák	k1gMnPc2	dudák
na	na	k7c6	na
Prácheňsku	Prácheňsko	k1gNnSc6	Prácheňsko
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Režný	režný	k2eAgMnSc1d1	režný
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-239-3276-4	[number]	k4	80-239-3276-4
</s>
</p>
<p>
<s>
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
Strakonicka	Strakonicko	k1gNnSc2	Strakonicko
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Hankovec	Hankovec	k1gMnSc1	Hankovec
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-239-6370-8	[number]	k4	80-239-6370-8
</s>
</p>
<p>
<s>
Zapomenuté	zapomenutý	k2eAgFnPc1d1	zapomenutá
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Tyla	tyla	k1gFnSc1	tyla
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
na	na	k7c6	na
starých	starý	k2eAgFnPc6d1	stará
pohlednicích	pohlednice	k1gFnPc6	pohlednice
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Tyla	tyla	k1gFnSc1	tyla
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
dobových	dobový	k2eAgFnPc2d1	dobová
pohlednic	pohlednice	k1gFnPc2	pohlednice
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Tyla	tyla	k1gFnSc1	tyla
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Zmizelí	zmizelý	k2eAgMnPc1d1	zmizelý
a	a	k8xC	a
nalezení	nalezený	k2eAgMnPc1d1	nalezený
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Sekyrka	sekyrka	k1gFnSc1	sekyrka
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-254-3715-5	[number]	k4	978-80-254-3715-5
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
podnikání	podnikání	k1gNnSc2	podnikání
na	na	k7c6	na
Strakonicku	Strakonicko	k1gNnSc6	Strakonicko
<g/>
,	,	kIx,	,
Městské	městský	k2eAgFnSc2d1	městská
knihy	kniha	k1gFnSc2	kniha
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-902919-7-X	[number]	k4	80-902919-7-X
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
Strakonicku	Strakonicko	k1gNnSc6	Strakonicko
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
I.	I.	kA	I.
-	-	kIx~	-
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Debnar	Debnar	k1gMnSc1	Debnar
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gFnPc4	náš
noviny	novina	k1gFnPc4	novina
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
46	[number]	k4	46
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
:	:	kIx,	:
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
osudy	osud	k1gInPc1	osud
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Cvrček	Cvrček	k1gMnSc1	Cvrček
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Strakonicko	Strakonicko	k1gNnSc1	Strakonicko
v	v	k7c6	v
pověstech	pověst	k1gFnPc6	pověst
a	a	k8xC	a
bájích	báj	k1gFnPc6	báj
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Pecen	pecen	k1gInSc1	pecen
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Strakonice	Strakonice	k1gFnPc1	Strakonice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Strakonice	Strakonice	k1gFnPc4	Strakonice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
www.strakonice.eu	www.strakonice.eu	k6eAd1	www.strakonice.eu
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
www.mu-st.cz	www.mut.cz	k1gInSc1	www.mu-st.cz
-	-	kIx~	-
Stránky	stránka	k1gFnSc2	stránka
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
</s>
</p>
<p>
<s>
www.hradstrakonice.cz	www.hradstrakonice.cz	k1gInSc1	www.hradstrakonice.cz
-	-	kIx~	-
Stránky	stránka	k1gFnSc2	stránka
strakonického	strakonický	k2eAgInSc2d1	strakonický
hradu	hrad	k1gInSc2	hrad
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
-	-	kIx~	-
facebookový	facebookový	k2eAgInSc1d1	facebookový
profil	profil	k1gInSc1	profil
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
-	-	kIx~	-
facebookový	facebookový	k2eAgInSc4d1	facebookový
profil	profil	k1gInSc4	profil
programu	program	k1gInSc2	program
Zdravé	zdravý	k2eAgNnSc4d1	zdravé
město	město	k1gNnSc4	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
</s>
</p>
