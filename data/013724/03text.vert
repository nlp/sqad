<s>
Dějiny	dějiny	k1gFnPc1
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Uruguayské	uruguayský	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Uruguaye	Uruguay	k1gFnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
dějiny	dějiny	k1gFnPc1
a	a	k8xC
historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
společností	společnost	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Uruguaye	Uruguay	k1gFnSc2
od	od	k7c2
počátku	počátek	k1gInSc2
přítomnosti	přítomnost	k1gFnSc2
lidské	lidský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
11	#num#	k4
000	#num#	k4
až	až	k8xS
8000	#num#	k4
lety	let	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přes	přes	k7c4
období	období	k1gNnSc4
španělské	španělský	k2eAgFnSc2d1
koloniální	koloniální	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
(	(	kIx(
<g/>
začátek	začátek	k1gInSc1
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
dosažení	dosažení	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
(	(	kIx(
<g/>
1828	#num#	k4
<g/>
)	)	kIx)
až	až	k6eAd1
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
na	na	k7c6
jihoamerickém	jihoamerický	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
</s>
<s>
Prehistorie	prehistorie	k1gFnPc1
a	a	k8xC
dějiny	dějiny	k1gFnPc1
domorodého	domorodý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
Podle	podle	k7c2
existujících	existující	k2eAgInPc2d1
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
se	se	k3xPyFc4
první	první	k4xOgMnPc1
lidé	člověk	k1gMnPc1
na	na	k7c6
území	území	k1gNnSc6
Uruguaye	Uruguay	k1gFnSc2
objevili	objevit	k5eAaPmAgMnP
asi	asi	k9
před	před	k7c7
11	#num#	k4
000	#num#	k4
až	až	k9
8000	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
společenství	společenství	k1gNnPc4
lovců	lovec	k1gMnPc2
a	a	k8xC
sběračů	sběrač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
před	před	k7c7
2000	#num#	k4
lety	léto	k1gNnPc7
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
objevily	objevit	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
odlišné	odlišný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
domorodého	domorodý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
:	:	kIx,
Chaná	Chaná	k1gFnSc1
<g/>
,	,	kIx,
Guarání	Guarání	k1gNnSc1
a	a	k8xC
Charrúa	Charrúa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
zmínění	zmíněný	k2eAgMnPc1d1
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
žili	žít	k5eAaImAgMnP
na	na	k7c6
území	území	k1gNnSc6
Uruguaye	Uruguay	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
příchodu	příchod	k1gInSc2
španělských	španělský	k2eAgMnPc2d1
kolonizátorů	kolonizátor	k1gMnPc2
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chaná	Chanat	k5eAaPmIp3nS
se	se	k3xPyFc4
zabývali	zabývat	k5eAaImAgMnP
především	především	k6eAd1
rybolovem	rybolov	k1gInSc7
a	a	k8xC
lovem	lov	k1gInSc7
a	a	k8xC
žili	žít	k5eAaImAgMnP
nomádským	nomádský	k2eAgInSc7d1
životem	život	k1gInSc7
v	v	k7c6
malých	malý	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jejich	jejich	k3xOp3gInSc6
životním	životní	k2eAgInSc6d1
stylu	styl	k1gInSc6
a	a	k8xC
kultuře	kultura	k1gFnSc6
víme	vědět	k5eAaImIp1nP
dnes	dnes	k6eAd1
relativně	relativně	k6eAd1
málo	málo	k6eAd1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
Chaná	Chaná	k1gFnSc1
zmizeli	zmizet	k5eAaPmAgMnP
záhy	záhy	k6eAd1
po	po	k7c6
španělské	španělský	k2eAgFnSc6d1
kolonizaci	kolonizace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Guaraní	Guaraný	k2eAgMnPc1d1
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
oblasti	oblast	k1gFnSc2
Uruguaye	Uruguay	k1gFnSc2
asi	asi	k9
200	#num#	k4
let	léto	k1gNnPc2
před	před	k7c4
Španěly	Španěl	k1gMnPc4
a	a	k8xC
zabývali	zabývat	k5eAaImAgMnP
se	s	k7c7
zemědělstvím	zemědělství	k1gNnSc7
a	a	k8xC
lovem	lov	k1gInSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
ani	ani	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
nepřežili	přežít	k5eNaPmAgMnP
první	první	k4xOgFnSc4
vlnu	vlna	k1gFnSc4
kolonizace	kolonizace	k1gFnSc2
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1
domorodá	domorodý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Charrúa	Charrú	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Uruguaye	Uruguay	k1gFnSc2
udržela	udržet	k5eAaPmAgFnS
dlouho	dlouho	k6eAd1
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
někteří	některý	k3yIgMnPc1
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
sehráli	sehrát	k5eAaPmAgMnP
roli	role	k1gFnSc4
při	při	k7c6
bojích	boj	k1gInPc6
o	o	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Charrúa	Charrúa	k1gFnSc1
byla	být	k5eAaImAgFnS
zabita	zabít	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
při	při	k7c6
masakru	masakr	k1gInSc6
v	v	k7c4
Salsipuedes	Salsipuedes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charrúové	Charrúus	k1gMnPc1
byli	být	k5eAaImAgMnP
výbornými	výborný	k2eAgMnPc7d1
lovci	lovec	k1gMnPc7
a	a	k8xC
bojovníky	bojovník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
i	i	k9
jinde	jinde	k6eAd1
na	na	k7c6
jihoamerickém	jihoamerický	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
španělská	španělský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
negativní	negativní	k2eAgFnSc1d1
dopad	dopad	k1gInSc4
na	na	k7c4
domorodé	domorodý	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odhadů	odhad	k1gInPc2
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
na	na	k7c6
počátku	počátek	k1gInSc6
kolonizace	kolonizace	k1gFnSc2
asi	asi	k9
15	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
domorodých	domorodý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
asi	asi	k9
300	#num#	k4
let	léto	k1gNnPc2
jich	on	k3xPp3gMnPc2
ale	ale	k9
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
asi	asi	k9
500	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Setkání	setkání	k1gNnSc1
s	s	k7c7
evropským	evropský	k2eAgMnSc7d1
kolonizátorem	kolonizátor	k1gMnSc7
také	také	k6eAd1
značně	značně	k6eAd1
změnilo	změnit	k5eAaPmAgNnS
životní	životní	k2eAgInSc4d1
styl	styl	k1gInSc4
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
uruguayské	uruguayský	k2eAgFnSc2d1
pampy	pampa	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
Evropané	Evropan	k1gMnPc1
například	například	k6eAd1
uvedli	uvést	k5eAaPmAgMnP
hovězí	hovězí	k2eAgInSc4d1
dobytek	dobytek	k1gInSc4
a	a	k8xC
koně	kůň	k1gMnPc1
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
o	o	k7c4
kulturní	kulturní	k2eAgFnSc4d1
asimilaci	asimilace	k1gFnSc4
domorodců	domorodec	k1gMnPc2
pomocí	pomocí	k7c2
konverzí	konverze	k1gFnPc2
na	na	k7c4
křesťanství	křesťanství	k1gNnSc4
atp.	atp.	kA
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1
a	a	k8xC
koloniální	koloniální	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
na	na	k7c6
Východním	východní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
</s>
<s>
Oblast	oblast	k1gFnSc1
takzvaného	takzvaný	k2eAgInSc2d1
východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
(	(	kIx(
<g/>
Banda	banda	k1gFnSc1
oriental	oriental	k1gMnSc1
<g/>
)	)	kIx)
ležela	ležet	k5eAaImAgFnS
po	po	k7c4
většinu	většina	k1gFnSc4
koloniálního	koloniální	k2eAgNnSc2d1
období	období	k1gNnSc2
na	na	k7c6
periférii	periférie	k1gFnSc6
španělského	španělský	k2eAgNnSc2d1
koloniálního	koloniální	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
až	až	k9
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
ve	v	k7c6
stínu	stín	k1gInSc6
významnějšího	významný	k2eAgInSc2d2
přístavu	přístav	k1gInSc2
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
významným	významný	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
ovlivňující	ovlivňující	k2eAgFnSc4d1
periferní	periferní	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
oblasti	oblast	k1gFnSc2
byla	být	k5eAaImAgFnS
blízkost	blízkost	k1gFnSc1
konkurenčního	konkurenční	k2eAgNnSc2d1
portugalského	portugalský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
patřilo	patřit	k5eAaImAgNnS
území	území	k1gNnSc4
pod	pod	k7c4
správu	správa	k1gFnSc4
Místokrálovství	Místokrálovství	k1gNnSc2
Peru	Peru	k1gNnSc2
a	a	k8xC
audiencie	audiencie	k1gFnSc2
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1776	#num#	k4
pod	pod	k7c4
nově	nově	k6eAd1
zřízené	zřízený	k2eAgNnSc4d1
místokrálovství	místokrálovství	k1gNnSc4
Río	Río	k1gFnSc3
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnPc1
španělští	španělský	k2eAgMnPc1d1
kolonizátoři	kolonizátor	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
Uruguaye	Uruguay	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1516	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
španělské	španělský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
Soriano	Soriana	k1gFnSc5
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
při	při	k7c6
řece	řeka	k1gFnSc6
Río	Río	k1gFnSc2
Negro	Negro	k1gNnSc1
a	a	k8xC
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
už	už	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
operovali	operovat	k5eAaImAgMnP
také	také	k9
Portugalci	Portugalec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
na	na	k7c6
území	území	k1gNnSc6
Uruguaye	Uruguay	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
většinu	většina	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
úrodná	úrodný	k2eAgFnSc1d1
pampa	pampa	k1gFnSc1
<g/>
,	,	kIx,
uveden	uveden	k2eAgInSc1d1
také	také	k9
hovězí	hovězí	k2eAgInSc1d1
dobytek	dobytek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
zintenzivnila	zintenzivnit	k5eAaPmAgFnS
španělská	španělský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
na	na	k7c6
východním	východní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
obavám	obava	k1gFnPc3
z	z	k7c2
rozšíření	rozšíření	k1gNnSc2
vlivu	vliv	k1gInSc2
Portugalců	Portugalec	k1gMnPc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
ale	ale	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
celkově	celkově	k6eAd1
skromné	skromný	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
lokalizované	lokalizovaný	k2eAgNnSc1d1
spíše	spíše	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
severu	sever	k1gInSc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnešní	dnešní	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
město	město	k1gNnSc1
Montevideo	Montevideo	k1gNnSc4
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1726	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
otevřeného	otevřený	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
mezi	mezi	k7c7
Portugalskem	Portugalsko	k1gNnSc7
a	a	k8xC
Španělskem	Španělsko	k1gNnSc7
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
územím	území	k1gNnSc7
dnešní	dnešní	k2eAgFnSc2d1
Uruguaye	Uruguay	k1gFnSc2
respektive	respektive	k9
Východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montevideo	Montevideo	k1gNnSc1
tak	tak	k6eAd1
mělo	mít	k5eAaImAgNnS
mít	mít	k5eAaImF
strategickou	strategický	k2eAgFnSc4d1
obrannou	obranný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
proti	proti	k7c3
portugalským	portugalský	k2eAgInPc3d1
útokům	útok	k1gInPc3
ze	z	k7c2
severu	sever	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
význam	význam	k1gInSc1
byl	být	k5eAaImAgInS
ještě	ještě	k9
umocněn	umocněn	k2eAgInSc1d1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
hlavní	hlavní	k2eAgFnSc7d1
španělskou	španělský	k2eAgFnSc7d1
námořní	námořní	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
pro	pro	k7c4
oblast	oblast	k1gFnSc4
Jižního	jižní	k2eAgInSc2d1
Pacifiku	Pacifik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
obchodní	obchodní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
Montevidea	Montevideo	k1gNnSc2
<g/>
,	,	kIx,
posílily	posílit	k5eAaPmAgFnP
konkurenční	konkurenční	k2eAgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
Montevideem	Montevideo	k1gNnSc7
a	a	k8xC
na	na	k7c6
druhém	druhý	k4xOgInSc6
břehu	břeh	k1gInSc6
ležícím	ležící	k2eAgMnSc6d1
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
Idealizovaný	idealizovaný	k2eAgInSc1d1
portrét	portrét	k1gInSc1
José	Josá	k1gFnSc2
Gervasia	Gervasium	k1gNnSc2
Artigase	Artigasa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
od	od	k7c2
Juana	Juan	k1gMnSc2
Manuela	Manuela	k1gFnSc1
Blanese	Blanese	k1gFnSc1
(	(	kIx(
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Během	během	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
autonomistické	autonomistický	k2eAgFnPc1d1
nálady	nálada	k1gFnPc1
mezi	mezi	k7c7
kreolskými	kreolský	k2eAgFnPc7d1
elitami	elita	k1gFnPc7
španělského	španělský	k2eAgNnSc2d1
koloniálního	koloniální	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
stále	stále	k6eAd1
zřejmější	zřejmý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělská	španělský	k2eAgFnSc1d1
metropole	metropole	k1gFnSc1
přitom	přitom	k6eAd1
stále	stále	k6eAd1
slábla	slábnout	k5eAaImAgFnS
<g/>
,	,	kIx,
ekonomicky	ekonomicky	k6eAd1
i	i	k9
politicky	politicky	k6eAd1
zaostávala	zaostávat	k5eAaImAgFnS
za	za	k7c4
Británii	Británie	k1gFnSc4
a	a	k8xC
Francii	Francie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britské	britský	k2eAgInPc4d1
koloniální	koloniální	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zasahovaly	zasahovat	k5eAaImAgFnP
už	už	k6eAd1
mnohem	mnohem	k6eAd1
dále	daleko	k6eAd2
než	než	k8xS
jenom	jenom	k6eAd1
do	do	k7c2
oblasti	oblast	k1gFnSc2
Karibiku	Karibik	k1gInSc2
a	a	k8xC
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
<g/>
,	,	kIx,
především	především	k9
strategicky	strategicky	k6eAd1
a	a	k8xC
obchodně	obchodně	k6eAd1
<g/>
,	,	kIx,
do	do	k7c2
oblasti	oblast	k1gFnSc2
jižního	jižní	k2eAgInSc2d1
Pacifiku	Pacifik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významná	významný	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
samozřejmě	samozřejmě	k6eAd1
kreolská	kreolský	k2eAgFnSc1d1
odezva	odezva	k1gFnSc1
americké	americký	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
(	(	kIx(
<g/>
1776	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
revoluce	revoluce	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
(	(	kIx(
<g/>
1789	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
za	za	k7c4
následek	následek	k1gInSc4
oslabení	oslabení	k1gNnSc4
monarchie	monarchie	k1gFnSc2
ve	v	k7c6
vlastním	vlastní	k2eAgNnSc6d1
Španělsku	Španělsko	k1gNnSc6
po	po	k7c6
francouzské	francouzský	k2eAgFnSc6d1
invazi	invaze	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
místokrálovství	místokrálovství	k1gNnPc2
Río	Río	k1gMnSc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
bylo	být	k5eAaImAgNnS
tedy	tedy	k9
ovlivněno	ovlivnit	k5eAaPmNgNnS
děním	dění	k1gNnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
především	především	k9
abdikací	abdikace	k1gFnSc7
španělského	španělský	k2eAgMnSc4d1
panovníka	panovník	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
strategickým	strategický	k2eAgNnSc7d1
soupeřením	soupeření	k1gNnSc7
Francie	Francie	k1gFnSc2
a	a	k8xC
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
v	v	k7c6
případě	případ	k1gInSc6
východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
,	,	kIx,
anglickou	anglický	k2eAgFnSc4d1
okupaci	okupace	k1gFnSc4
Montevidea	Montevideo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1807	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestabilní	stabilní	k2eNgFnSc1d1
situace	situace	k1gFnSc1
také	také	k6eAd1
zostřila	zostřit	k5eAaPmAgFnS
spory	spor	k1gInPc4
mezi	mezi	k7c7
Montevideem	Montevideo	k1gNnSc7
a	a	k8xC
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montevideo	Montevideo	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
podpořilo	podpořit	k5eAaPmAgNnS
autoritu	autorita	k1gFnSc4
krále	král	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
tamější	tamější	k2eAgFnSc1d1
junta	junta	k1gFnSc1
zastávala	zastávat	k5eAaImAgFnS
roajalistické	roajalistický	k2eAgFnPc4d1
<g/>
,	,	kIx,
protifrancouzské	protifrancouzský	k2eAgFnPc4d1
a	a	k8xC
konečně	konečně	k6eAd1
také	také	k9
proti-buenosaireské	proti-buenosaireský	k2eAgInPc4d1
postoje	postoj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
bašta	bašta	k1gFnSc1
roajalistů	roajalista	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Airesa	k1gFnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
svolána	svolán	k2eAgFnSc1d1
junta	junta	k1gFnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stala	stát	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
laplatských	laplatský	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
od	od	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nicméně	nicméně	k8xC
neznamenalo	znamenat	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
neexistovaly	existovat	k5eNaImAgFnP
vlastenecké	vlastenecký	k2eAgFnPc1d1
nálady	nálada	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Roajalistická	roajalistický	k2eAgFnSc1d1
junta	junta	k1gFnSc1
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1811	#num#	k4
značně	značně	k6eAd1
otřesena	otřást	k5eAaPmNgFnS
odchodem	odchod	k1gInSc7
vlivného	vlivný	k2eAgNnSc2d1
José	José	k1gNnSc2
Gervasia	Gervasium	k1gNnSc2
Artigase	Artigasa	k1gFnSc3
do	do	k7c2
Buenos	Buenosa	k1gFnPc2
Aires	Airesa	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgInS
k	k	k7c3
místní	místní	k2eAgFnSc3d1
květnové	květnový	k2eAgFnSc3d1
revoluci	revoluce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artigas	Artigas	k1gInSc1
pocházel	pocházet	k5eAaImAgInS
z	z	k7c2
významné	významný	k2eAgFnSc2d1
montevidejské	montevidejský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
a	a	k8xC
měl	mít	k5eAaImAgInS
značnou	značný	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
hlavně	hlavně	k9
na	na	k7c6
venkově	venkov	k1gInSc6
mezi	mezi	k7c7
uruguayskými	uruguayský	k2eAgInPc7d1
gauči	gauč	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
se	se	k3xPyFc4
kolem	kolem	k7c2
něho	on	k3xPp3gNnSc2
sjednotila	sjednotit	k5eAaPmAgFnS
několikatisícová	několikatisícový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
venkovanů	venkovan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Artigas	Artigas	k1gInSc1
z	z	k7c2
počátku	počátek	k1gInSc2
uznával	uznávat	k5eAaImAgInS
svrchovanost	svrchovanost	k1gFnSc4
buenosaireské	buenosaireský	k2eAgFnSc2d1
junty	junta	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
záhy	záhy	k6eAd1
se	se	k3xPyFc4
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
členy	člen	k1gMnPc7
junty	junta	k1gFnSc2
začaly	začít	k5eAaPmAgFnP
objevovat	objevovat	k5eAaImF
vážné	vážný	k2eAgInPc4d1
názorové	názorový	k2eAgInPc4d1
konflikty	konflikt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
buenosaireská	buenosaireský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
si	se	k3xPyFc3
stále	stále	k6eAd1
pohrávala	pohrávat	k5eAaImAgFnS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
určité	určitý	k2eAgFnSc2d1
formy	forma	k1gFnSc2
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
Artigas	Artigas	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
stoupenci	stoupenec	k1gMnPc1
byli	být	k5eAaImAgMnP
pro	pro	k7c4
nezávislost	nezávislost	k1gFnSc4
La	la	k1gNnSc2
Platy	plat	k1gInPc1
ve	v	k7c6
formě	forma	k1gFnSc6
federace	federace	k1gFnSc2
nebo	nebo	k8xC
konfederace	konfederace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
měla	mít	k5eAaImAgFnS
podporu	podpora	k1gFnSc4
nejenom	nejenom	k6eAd1
mezi	mezi	k7c7
Artigasovými	Artigasův	k2eAgMnPc7d1
stoupenci	stoupenec	k1gMnPc7
z	z	k7c2
Východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
rovněž	rovněž	k9
v	v	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
napříč	napříč	k7c7
La	la	k1gNnSc7
Platou	Platá	k1gFnSc4
<g/>
,	,	kIx,
především	především	k9
v	v	k7c6
Entre	Entr	k1gInSc5
Ríos	Ríos	k1gInSc1
<g/>
,	,	kIx,
Corrientes	Corrientes	k1gMnSc1
<g/>
,	,	kIx,
Santa	Santa	k1gMnSc1
Fé	Fé	k1gMnSc1
<g/>
,	,	kIx,
Misiones	Misiones	k1gMnSc1
a	a	k8xC
Cordobě	Cordoba	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
spojoval	spojovat	k5eAaImAgInS
odpor	odpor	k1gInSc1
proti	proti	k7c3
nadvládě	nadvláda	k1gFnSc3
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spor	spor	k1gInSc1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
otevřené	otevřený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
Artigas	Artigas	k1gInSc1
přinesl	přinést	k5eAaPmAgInS
do	do	k7c2
Buenos	Buenosa	k1gFnPc2
Aires	Airesa	k1gFnPc2
tzv.	tzv.	kA
Instrukci	instrukce	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
roku	rok	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
montevidejské	montevidejský	k2eAgNnSc1d1
cabildo	cabildo	k1gNnSc1
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
myšlence	myšlenka	k1gFnSc3
nezávislosti	nezávislost	k1gFnSc2
a	a	k8xC
konfederace	konfederace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
proti	proti	k7c3
sobě	se	k3xPyFc3
stáli	stát	k5eAaImAgMnP
Buenos	Buenos	k1gInSc4
Aires	Airesa	k1gFnPc2
a	a	k8xC
Artigas	Artigasa	k1gFnPc2
<g/>
,	,	kIx,
podpořený	podpořený	k2eAgInSc1d1
provinciemi	provincie	k1gFnPc7
Entre	Entr	k1gInSc5
Ríos	Ríos	k1gInSc1
<g/>
,	,	kIx,
Corrientes	Corrientes	k1gMnSc1
a	a	k8xC
Santa	Santa	k1gMnSc1
Fé	Fé	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artigas	Artigas	k1gInSc1
záhy	záhy	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
uruguayský	uruguayský	k2eAgInSc1d1
venkov	venkov	k1gInSc1
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
odsud	odsud	k6eAd1
zemi	zem	k1gFnSc4
nově	nově	k6eAd1
reorganizovat	reorganizovat	k5eAaBmF
pomocí	pomocí	k7c2
konfiskací	konfiskace	k1gFnPc2
půdy	půda	k1gFnSc2
těch	ten	k3xDgFnPc2
z	z	k7c2
vlastníků	vlastník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
přidali	přidat	k5eAaPmAgMnP
k	k	k7c3
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půdu	půda	k1gFnSc4
potom	potom	k6eAd1
rozděloval	rozdělovat	k5eAaImAgMnS
mezi	mezi	k7c4
nejchudší	chudý	k2eAgMnPc4d3
ať	ať	k8xC,k8xS
kreoly	kreol	k1gMnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
bývalé	bývalý	k2eAgMnPc4d1
otroky	otrok	k1gMnPc4
a	a	k8xC
Indiány	Indián	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
rovnostářským	rovnostářský	k2eAgInSc7d1
přístupem	přístup	k1gInSc7
si	se	k3xPyFc3
ale	ale	k8xC
nenaklonil	naklonit	k5eNaPmAgMnS
cabildo	cabildo	k1gNnSc4
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Artigasova	Artigasův	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
a	a	k8xC
vojenská	vojenský	k2eAgFnSc1d1
dominance	dominance	k1gFnSc1
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
příchodem	příchod	k1gInSc7
Portugalců	Portugalec	k1gMnPc2
ze	z	k7c2
severu	sever	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1816	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artigas	Artigas	k1gMnSc1
tak	tak	k9
musel	muset	k5eAaImAgMnS
bojovat	bojovat	k5eAaImF
na	na	k7c6
dvou	dva	k4xCgFnPc6
frontách	fronta	k1gFnPc6
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
ze	z	k7c2
severu	sever	k1gInSc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
profesionálněji	profesionálně	k6eAd2
organizovanou	organizovaný	k2eAgFnSc4d1
10	#num#	k4
000	#num#	k4
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc6
porážkách	porážka	k1gFnPc6
byla	být	k5eAaImAgFnS
Artigasova	Artigasův	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
de	de	k?
facto	facto	k1gNnSc4
rozprášena	rozprášen	k2eAgFnSc1d1
a	a	k8xC
Artigas	Artigas	k1gInSc1
mohl	moct	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
1820	#num#	k4
působit	působit	k5eAaImF
Portugalcům	Portugalec	k1gMnPc3
jenom	jenom	k9
menší	malý	k2eAgFnPc4d2
škody	škoda	k1gFnPc4
pomocí	pomocí	k7c2
nájezdů	nájezd	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
podstatě	podstata	k1gFnSc6
ocitl	ocitnout	k5eAaPmAgInS
pod	pod	k7c7
portugalskou	portugalský	k2eAgFnSc7d1
okupací	okupace	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
včetně	včetně	k7c2
Montevidea	Montevideo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Artigas	Artigas	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1820	#num#	k4
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
paraguajského	paraguajský	k2eAgInSc2d1
exilu	exil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Paradoxně	paradoxně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
momentě	moment	k1gInSc6
Artigasovy	Artigasův	k2eAgFnSc2d1
porážky	porážka	k1gFnSc2
k	k	k7c3
vítězství	vítězství	k1gNnSc3
federalistů	federalista	k1gMnPc2
v	v	k7c6
La	la	k1gNnSc6
Platě	platit	k5eAaImSgInS
(	(	kIx(
<g/>
respektive	respektive	k9
Argentině	Argentina	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
úspěšně	úspěšně	k6eAd1
porazili	porazit	k5eAaPmAgMnP
zastánce	zastánce	k1gMnSc4
centralismu	centralismus	k1gInSc2
a	a	k8xC
monarchismu	monarchismus	k1gInSc2
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Airesa	k1gFnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Cepady	Cepada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
1821	#num#	k4
Východní	východní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
oddělen	oddělen	k2eAgInSc4d1
od	od	k7c2
La	la	k1gNnPc2
Platy	plat	k1gInPc7
a	a	k8xC
připojen	připojit	k5eAaPmNgInS
k	k	k7c3
portugalské	portugalský	k2eAgFnSc3d1
Brazílii	Brazílie	k1gFnSc3
pod	pod	k7c7
názvem	název	k1gInSc7
Provincia	Provincia	k1gFnSc1
Cisplatina	Cisplatina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
sliby	slib	k1gInPc4
zavedli	zavést	k5eAaPmAgMnP
Portugalci	Portugalec	k1gMnPc1
v	v	k7c6
provincii	provincie	k1gFnSc6
vojenský	vojenský	k2eAgInSc1d1
režim	režim	k1gInSc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
netěšila	těšit	k5eNaImAgFnS
popularitě	popularita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
1825	#num#	k4
malá	malý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Airesa	k1gFnPc2
žijících	žijící	k2eAgMnPc2d1
Uruguayců	Uruguayec	k1gMnPc2
uspořádala	uspořádat	k5eAaPmAgFnS
dobrodružnou	dobrodružný	k2eAgFnSc4d1
"	"	kIx"
<g/>
kapesní	kapesní	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dostala	dostat	k5eAaPmAgFnS
název	název	k1gInSc4
"	"	kIx"
<g/>
Výprava	výprava	k1gFnSc1
33	#num#	k4
Uruguayců	Uruguayec	k1gMnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
se	se	k3xPyFc4
našlo	najít	k5eAaPmAgNnS
také	také	k9
několik	několik	k4yIc1
bývalých	bývalý	k2eAgMnPc2d1
Artigasových	Artigasův	k2eAgMnPc2d1
spolubojovníků	spolubojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
malá	malý	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
se	se	k3xPyFc4
vylodila	vylodit	k5eAaPmAgFnS
na	na	k7c6
Východním	východní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
a	a	k8xC
záhy	záhy	k6eAd1
se	se	k3xPyFc4
kolem	kolem	k7c2
sebe	se	k3xPyFc2
shromáždila	shromáždit	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
podporovatelů	podporovatel	k1gMnPc2
z	z	k7c2
řad	řada	k1gFnPc2
místního	místní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
oblehla	oblehnout	k5eAaPmAgFnS
Montevideo	Montevideo	k1gNnSc4
<g/>
,	,	kIx,
kontrolované	kontrolovaný	k2eAgFnPc4d1
Portugalci	Portugalec	k1gMnSc3
respektive	respektive	k9
Brazilci	Brazilec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1826	#num#	k4
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
samostatnost	samostatnost	k1gFnSc1
a	a	k8xC
připojení	připojení	k1gNnSc1
Východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
k	k	k7c3
provinciím	provincie	k1gFnPc3
La	la	k1gNnSc2
Platy	plat	k1gInPc1
(	(	kIx(
<g/>
respektive	respektive	k9
k	k	k7c3
Argentině	Argentina	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
války	válka	k1gFnSc2
Brazilcům	Brazilec	k1gMnPc3
ze	z	k7c2
strany	strana	k1gFnSc2
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Eskalace	eskalace	k1gFnSc1
konkurenčního	konkurenční	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
mezi	mezi	k7c7
Brazílií	Brazílie	k1gFnSc7
a	a	k8xC
Argentinou	Argentina	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
jevila	jevit	k5eAaImAgFnS
stále	stále	k6eAd1
větším	veliký	k2eAgInSc7d2
problémem	problém	k1gInSc7
pro	pro	k7c4
Velkou	velký	k2eAgFnSc4d1
Británii	Británie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
La	la	k1gNnSc4
Platy	plat	k1gInPc1
strategické	strategický	k2eAgInPc1d1
a	a	k8xC
obchodní	obchodní	k2eAgInPc1d1
zájmy	zájem	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gInSc6
zájmu	zájem	k1gInSc6
nebylo	být	k5eNaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
region	region	k1gInSc1
neustále	neustále	k6eAd1
destabilizován	destabilizovat	k5eAaBmNgInS
rivalitou	rivalita	k1gFnSc7
mezi	mezi	k7c7
Brazílii	Brazílie	k1gFnSc3
a	a	k8xC
Argentinou	Argentina	k1gFnSc7
<g/>
,	,	kIx,
proto	proto	k8xC
Británie	Británie	k1gFnSc1
přistoupila	přistoupit	k5eAaPmAgFnS
na	na	k7c4
myšlenku	myšlenka	k1gFnSc4
vytvoření	vytvoření	k1gNnSc2
nezávislé	závislý	k2eNgFnSc2d1
nárazníkové	nárazníkový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1828	#num#	k4
donutila	donutit	k5eAaPmAgFnS
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
k	k	k7c3
podepsání	podepsání	k1gNnSc3
mírové	mírový	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
Argentina	Argentina	k1gFnSc1
i	i	k8xC
Brazílie	Brazílie	k1gFnPc1
uznaly	uznat	k5eAaPmAgFnP
nezávislost	nezávislost	k1gFnSc4
Východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
<g/>
,	,	kIx,
respektive	respektive	k9
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výchozí	výchozí	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
nového	nový	k2eAgInSc2d1
státu	stát	k1gInSc2
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
se	se	k3xPyFc4
ocitla	ocitnout	k5eAaPmAgFnS
po	po	k7c6
dosažení	dosažení	k1gNnSc6
nezávislosti	nezávislost	k1gFnSc2
v	v	k7c6
poměrně	poměrně	k6eAd1
bizarní	bizarní	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
měla	mít	k5eAaImAgFnS
rozlohu	rozloha	k1gFnSc4
187	#num#	k4
000	#num#	k4
čtverečních	čtvereční	k2eAgInPc2d1
km	km	kA
<g/>
,	,	kIx,
ale	ale	k8xC
žilo	žít	k5eAaImAgNnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
asi	asi	k9
jen	jen	k9
75	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
koncentrovala	koncentrovat	k5eAaBmAgFnS
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imigrace	imigrace	k1gFnSc1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
hlavním	hlavní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
k	k	k7c3
zvýšení	zvýšení	k1gNnSc3
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
poměr	poměr	k1gInSc1
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
přistěhovalců	přistěhovalec	k1gMnPc2
během	během	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měnil	měnit	k5eAaImAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
cizinců	cizinec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
znesnadňovala	znesnadňovat	k5eAaImAgFnS
proces	proces	k1gInSc4
budování	budování	k1gNnSc2
národní	národní	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
imigrace	imigrace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
uruguayském	uruguayský	k2eAgInSc6d1
případě	případ	k1gInSc6
dříve	dříve	k6eAd2
než	než	k8xS
do	do	k7c2
Argentiny	Argentina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
byla	být	k5eAaImAgFnS
ekonomicky	ekonomicky	k6eAd1
značně	značně	k6eAd1
podlomená	podlomený	k2eAgFnSc1d1
dlouhou	dlouhý	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
a	a	k8xC
také	také	k9
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
během	během	k7c2
poměrně	poměrně	k6eAd1
krátké	krátký	k2eAgFnSc2d1
doby	doba	k1gFnSc2
vystřídalo	vystřídat	k5eAaPmAgNnS
několik	několik	k4yIc1
režimů	režim	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
znovu	znovu	k6eAd1
a	a	k8xC
znovu	znovu	k6eAd1
přerozdělovaly	přerozdělovat	k5eAaImAgFnP
místní	místní	k2eAgFnSc4d1
úrodnou	úrodný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
trend	trend	k1gInSc1
nicméně	nicméně	k8xC
pokračoval	pokračovat	k5eAaImAgInS
i	i	k9
během	během	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různí	různý	k2eAgMnPc1d1
političtí	politický	k2eAgMnPc1d1
lídři	lídr	k1gMnPc1
měli	mít	k5eAaImAgMnP
tendence	tendence	k1gFnPc4
přidělovat	přidělovat	k5eAaImF
půdu	půda	k1gFnSc4
svým	svůj	k3xOyFgFnPc3
rodinám	rodina	k1gFnPc3
nebo	nebo	k8xC
podporovatelům	podporovatel	k1gMnPc3
podle	podle	k7c2
v	v	k7c6
podstatě	podstata	k1gFnSc6
nepotického	potický	k2eNgInSc2d1
klíče	klíč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgFnSc1d1
nestabilita	nestabilita	k1gFnSc1
byla	být	k5eAaImAgFnS
ještě	ještě	k9
umocněna	umocnit	k5eAaPmNgFnS
častými	častý	k2eAgFnPc7d1
vzpourami	vzpoura	k1gFnPc7
a	a	k8xC
konflikty	konflikt	k1gInPc7
uvnitř	uvnitř	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
příjmů	příjem	k1gInPc2
zůstávalo	zůstávat	k5eAaImAgNnS
hovězí	hovězí	k2eAgNnSc1d1
solené	solený	k2eAgNnSc1d1
maso	maso	k1gNnSc1
a	a	k8xC
kůže	kůže	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
problémem	problém	k1gInSc7
nového	nový	k2eAgInSc2d1
státu	stát	k1gInSc2
byla	být	k5eAaImAgFnS
nutnost	nutnost	k1gFnSc1
vybudování	vybudování	k1gNnSc4
aparátu	aparát	k1gInSc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
podstatě	podstata	k1gFnSc6
na	na	k7c6
zelené	zelený	k2eAgFnSc6d1
louce	louka	k1gFnSc6
a	a	k8xC
relativní	relativní	k2eAgInSc4d1
nedostatek	nedostatek	k1gInSc4
vzdělaných	vzdělaný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
by	by	kYmCp3nS
se	se	k3xPyFc4
této	tento	k3xDgFnSc2
role	role	k1gFnSc2
ujali	ujmout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situaci	situace	k1gFnSc4
země	zem	k1gFnSc2
neprospěl	prospět	k5eNaPmAgInS
ani	ani	k8xC
vysoký	vysoký	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
dluh	dluh	k1gInSc4
a	a	k8xC
obecně	obecně	k6eAd1
špatná	špatný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
ve	v	k7c6
státních	státní	k2eAgFnPc6d1
financích	finance	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Vnitropolitický	vnitropolitický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
první	první	k4xOgFnSc4
ústavu	ústava	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
a	a	k8xC
v	v	k7c6
prvních	první	k4xOgNnPc6
desetiletích	desetiletí	k1gNnPc6
zemi	zem	k1gFnSc6
dominovali	dominovat	k5eAaImAgMnP
bývalí	bývalý	k2eAgMnPc1d1
Artigasovi	Artigasův	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
prezidentem	prezident	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
generál	generál	k1gMnSc1
José	Josá	k1gFnSc2
Fructuoso	Fructuosa	k1gFnSc5
Rivera	Rivero	k1gNnSc2
a	a	k8xC
druhým	druhý	k4xOgMnSc7
generál	generál	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Oribe	Orib	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Latentní	latentní	k2eAgFnSc1d1
rivalita	rivalita	k1gFnSc1
mezi	mezi	k7c7
oběma	dva	k4xCgMnPc7
generály	generál	k1gMnPc7
vygenerovala	vygenerovat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1838	#num#	k4
v	v	k7c4
otevřený	otevřený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Guerra	Guerra	k1gMnSc1
Grande	grand	k1gMnSc5
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
trvala	trvat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1851	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
sporu	spor	k1gInSc2
potom	potom	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
letité	letitý	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
Uruguaye	Uruguay	k1gFnSc2
do	do	k7c2
dvou	dva	k4xCgFnPc2
politických	politický	k2eAgFnPc2d1
frakcí	frakce	k1gFnPc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
stran	strana	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
blancos	blancos	k1gInSc1
(	(	kIx(
<g/>
bílí	bílý	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
frakce	frakce	k1gFnSc1
kolem	kolem	k7c2
generála	generál	k1gMnSc2
Oribeho	Oribe	k1gMnSc2
<g/>
,	,	kIx,
opírající	opírající	k2eAgFnSc7d1
se	se	k3xPyFc4
o	o	k7c4
podporu	podpora	k1gFnSc4
venkova	venkov	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
colorados	colorados	k1gInSc1
(	(	kIx(
<g/>
barevní	barevný	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
frakce	frakce	k1gFnSc1
kolem	kolem	k7c2
generála	generál	k1gMnSc2
Rivery	Rivera	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
podporu	podpora	k1gFnSc4
především	především	k6eAd1
ve	v	k7c6
městech	město	k1gNnPc6
(	(	kIx(
<g/>
respektive	respektive	k9
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
tato	tento	k3xDgFnSc1
interpretace	interpretace	k1gFnSc1
zapomíná	zapomínat	k5eAaImIp3nS
na	na	k7c4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
v	v	k7c6
řadách	řada	k1gFnPc6
colorados	coloradosa	k1gFnPc2
byla	být	k5eAaImAgFnS
řada	řada	k1gFnSc1
venkovských	venkovský	k2eAgMnPc2d1
velkostatkářů	velkostatkář	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
mentalitou	mentalita	k1gFnSc7
zakotvení	zakotvení	k1gNnPc2
na	na	k7c6
venkově	venkov	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c4
většinu	většina	k1gFnSc4
roku	rok	k1gInSc2
žili	žít	k5eAaImAgMnP
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehledné	přehledný	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
Uruguaye	Uruguay	k1gFnSc2
do	do	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
před	před	k7c7
asi	asi	k9
11	#num#	k4
až	až	k9
8000	#num#	k4
let	léto	k1gNnPc2
<g/>
:	:	kIx,
první	první	k4xOgFnSc2
doložené	doložená	k1gFnSc2
lidské	lidský	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
</s>
<s>
asi	asi	k9
před	před	k7c7
2000	#num#	k4
lety	let	k1gInPc7
<g/>
:	:	kIx,
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Chaná	Chaná	k1gFnSc1
a	a	k8xC
Charruá	Charruá	k1gFnSc1
</s>
<s>
kolem	kolem	k7c2
1300	#num#	k4
<g/>
:	:	kIx,
příchod	příchod	k1gInSc1
Guaraní	Guaraní	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
1492	#num#	k4
<g/>
:	:	kIx,
cesta	cesta	k1gFnSc1
Kryštofa	Kryštof	k1gMnSc2
Kolumba	Kolumbus	k1gMnSc2
a	a	k8xC
objevení	objevení	k1gNnSc1
nového	nový	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
</s>
<s>
1520	#num#	k4
<g/>
:	:	kIx,
Magã	Magã	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
poprvé	poprvé	k6eAd1
mapuje	mapovat	k5eAaImIp3nS
oblast	oblast	k1gFnSc4
pobřeží	pobřeží	k1gNnSc2
Río	Río	k1gFnPc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
</s>
<s>
1527	#num#	k4
<g/>
:	:	kIx,
založení	založení	k1gNnSc6
první	první	k4xOgFnSc2
osady	osada	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
budoucí	budoucí	k2eAgFnSc2d1
Uruguaye	Uruguay	k1gFnSc2
Sancti	Sancť	k1gFnSc2
Spiritus	spiritus	k1gInSc1
(	(	kIx(
<g/>
zničena	zničen	k2eAgFnSc1d1
1529	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1536	#num#	k4
<g/>
:	:	kIx,
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Río	Río	k1gFnSc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
založena	založen	k2eAgFnSc1d1
osada	osada	k1gFnSc1
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
(	(	kIx(
<g/>
opuštěna	opuštěn	k2eAgFnSc1d1
1541	#num#	k4
a	a	k8xC
obnovena	obnoven	k2eAgFnSc1d1
1580	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1607	#num#	k4
<g/>
:	:	kIx,
výprava	výprava	k1gFnSc1
na	na	k7c4
východní	východní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
a	a	k8xC
vysazení	vysazení	k1gNnSc4
koní	kůň	k1gMnPc2
a	a	k8xC
hovězího	hovězí	k2eAgInSc2d1
dobytka	dobytek	k1gInSc2
do	do	k7c2
zdejší	zdejší	k2eAgFnSc2d1
pampy	pampa	k1gFnSc2
</s>
<s>
1625	#num#	k4
<g/>
:	:	kIx,
založení	založení	k1gNnSc6
první	první	k4xOgFnSc2
trvalé	trvalý	k2eAgFnSc2d1
osady	osada	k1gFnSc2
na	na	k7c4
území	území	k1gNnSc4
Uruguaye	Uruguay	k1gFnSc2
Santo	Sant	k2eAgNnSc4d1
Domingo	Domingo	k1gNnSc4
de	de	k?
Soriana	Soriana	k1gFnSc1
</s>
<s>
1726	#num#	k4
<g/>
:	:	kIx,
založení	založení	k1gNnSc6
města	město	k1gNnSc2
Montevideo	Montevideo	k1gNnSc4
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Río	Río	k1gFnSc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
</s>
<s>
1776	#num#	k4
<g/>
:	:	kIx,
vznik	vznik	k1gInSc1
Místokrálovství	Místokrálovství	k1gNnSc1
Río	Río	k1gFnPc2
de	de	k?
la	la	k1gNnSc1
Plata	plato	k1gNnSc2
</s>
<s>
1807	#num#	k4
<g/>
:	:	kIx,
Angličané	Angličan	k1gMnPc1
okupují	okupovat	k5eAaBmIp3nP
Montevideo	Montevideo	k1gNnSc4
(	(	kIx(
<g/>
únor	únor	k1gInSc1
–	–	k?
září	září	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
1808	#num#	k4
<g/>
:	:	kIx,
roajalistická	roajalistický	k2eAgFnSc1d1
junta	junta	k1gFnSc1
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
(	(	kIx(
<g/>
do	do	k7c2
1814	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1811	#num#	k4
<g/>
:	:	kIx,
J.G.	J.G.	k1gMnSc1
Artigas	Artigas	k1gMnSc1
přešel	přejít	k5eAaPmAgMnS
k	k	k7c3
juntě	junta	k1gFnSc3
v	v	k7c4
Buenos	Buenos	k1gInSc4
Aires	Airesa	k1gFnPc2
</s>
<s>
1813	#num#	k4
<g/>
:	:	kIx,
XIII	XIII	kA
<g/>
.	.	kIx.
instrukce	instrukce	k1gFnSc1
a	a	k8xC
roztržka	roztržka	k1gFnSc1
Artigase	Artigasa	k1gFnSc3
s	s	k7c7
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
</s>
<s>
1825	#num#	k4
<g/>
:	:	kIx,
výprava	výprava	k1gFnSc1
33	#num#	k4
Uruguyajců	Uruguyajce	k1gMnPc2
a	a	k8xC
vyhlášení	vyhlášení	k1gNnSc2
nezávislosti	nezávislost	k1gFnSc2
Východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
připojení	připojení	k1gNnSc2
k	k	k7c3
Provinciím	provincie	k1gFnPc3
La	la	k1gNnSc2
Plata	plato	k1gNnPc1
</s>
<s>
1828	#num#	k4
<g/>
:	:	kIx,
uznání	uznání	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
Východního	východní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
dohody	dohoda	k1gFnSc2
Argentiny	Argentina	k1gFnSc2
a	a	k8xC
Brazílie	Brazílie	k1gFnSc2
a	a	k8xC
za	za	k7c2
intervence	intervence	k1gFnSc2
Británie	Británie	k1gFnSc2
</s>
<s>
1830	#num#	k4
<g/>
:	:	kIx,
první	první	k4xOgFnSc1
ústava	ústava	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
1839	#num#	k4
<g/>
-	-	kIx~
<g/>
1851	#num#	k4
<g/>
:	:	kIx,
Guerra	Guerr	k1gInSc2
Grande	grand	k1gMnSc5
<g/>
,	,	kIx,
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
frakcemi	frakce	k1gFnPc7
<g/>
,	,	kIx,
vznik	vznik	k1gInSc4
colorados	coloradosa	k1gFnPc2
a	a	k8xC
blancos	blancosa	k1gFnPc2
</s>
<s>
1849	#num#	k4
<g/>
:	:	kIx,
založení	založení	k1gNnSc2
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Montevideu	Montevideo	k1gNnSc6
</s>
<s>
1860	#num#	k4
<g/>
:	:	kIx,
pokus	pokus	k1gInSc1
prezidenta	prezident	k1gMnSc2
Berry	Berra	k1gMnSc2
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
)	)	kIx)
o	o	k7c4
zrušení	zrušení	k1gNnSc4
obou	dva	k4xCgFnPc2
soupeřících	soupeřící	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
1865	#num#	k4
<g/>
:	:	kIx,
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
o	o	k7c6
Trojité	trojitý	k2eAgFnSc6d1
alianci	aliance	k1gFnSc6
(	(	kIx(
<g/>
Argentina-Brazílie-Uruguay	Argentina-Brazílie-Uruguaa	k1gFnSc2
<g/>
)	)	kIx)
ve	v	k7c6
válce	válka	k1gFnSc6
proti	proti	k7c3
Paraguayi	Paraguay	k1gFnSc3
</s>
<s>
1865	#num#	k4
<g/>
-	-	kIx~
<g/>
1870	#num#	k4
<g/>
:	:	kIx,
válka	válka	k1gFnSc1
proti	proti	k7c3
Paraguayi	Paraguay	k1gFnSc3
</s>
<s>
1868	#num#	k4
<g/>
:	:	kIx,
zavraždění	zavraždění	k1gNnSc1
dvou	dva	k4xCgMnPc2
politických	politický	k2eAgMnPc2d1
rivalů	rival	k1gMnPc2
Berry	Berra	k1gFnSc2
a	a	k8xC
Florese	Florese	k1gFnSc2
</s>
<s>
1869	#num#	k4
<g/>
:	:	kIx,
otevření	otevření	k1gNnSc6
první	první	k4xOgFnSc2
železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
(	(	kIx(
<g/>
20	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
1872	#num#	k4
<g/>
:	:	kIx,
blancos	blancosa	k1gFnPc2
získali	získat	k5eAaPmAgMnP
na	na	k7c6
základě	základ	k1gInSc6
politické	politický	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
podíl	podíl	k1gInSc1
na	na	k7c6
moci	moc	k1gFnSc6
</s>
<s>
1876	#num#	k4
<g/>
:	:	kIx,
dovršení	dovršení	k1gNnSc6
centralizace	centralizace	k1gFnSc2
státu	stát	k1gInSc2
</s>
<s>
1882	#num#	k4
<g/>
-	-	kIx~
<g/>
1886	#num#	k4
<g/>
:	:	kIx,
polodiktatura	polodiktatura	k1gFnSc1
prezidenta	prezident	k1gMnSc2
Santose	Santosa	k1gFnSc6
</s>
<s>
1890	#num#	k4
<g/>
:	:	kIx,
plný	plný	k2eAgInSc4d1
návrat	návrat	k1gInSc4
k	k	k7c3
civilní	civilní	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
</s>
<s>
1897	#num#	k4
<g/>
:	:	kIx,
poslední	poslední	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
:	:	kIx,
vzpoura	vzpoura	k1gFnSc1
Aparacia	Aparacia	k1gFnSc1
Saravii	Saravium	k1gNnPc7
(	(	kIx(
<g/>
do	do	k7c2
1904	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Blancos	Blancos	k1gMnSc1
a	a	k8xC
Colorados	Colorados	k1gMnSc1
</s>
<s>
Politické	politický	k2eAgNnSc1d1
a	a	k8xC
vojenské	vojenský	k2eAgNnSc1d1
soupeření	soupeření	k1gNnSc1
mezi	mezi	k7c4
blancos	blancos	k1gInSc4
a	a	k8xC
colorados	colorados	k1gInSc4
charakterizovalo	charakterizovat	k5eAaBmAgNnS
vnitropolitickou	vnitropolitický	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
Uruguaye	Uruguay	k1gFnSc2
téměř	téměř	k6eAd1
po	po	k7c6
celé	celá	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
ale	ale	k8xC
také	také	k9
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
poněkud	poněkud	k6eAd1
zjednodušeně	zjednodušeně	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
blancos	blancos	k1gInSc4
(	(	kIx(
<g/>
respektive	respektive	k9
Partido	Partida	k1gFnSc5
blanco	blanco	k2eAgInPc1d1
<g/>
)	)	kIx)
charakterizovat	charakterizovat	k5eAaBmF
jako	jako	k9
stoupence	stoupenec	k1gMnSc4
politického	politický	k2eAgInSc2d1
konzervativismu	konzervativismus	k1gInSc2
<g/>
,	,	kIx,
vyznávající	vyznávající	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
a	a	k8xC
tradiční	tradiční	k2eAgFnSc2d1
autority	autorita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
colorados	coloradosa	k1gFnPc2
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
colorado	colorada	k1gFnSc5
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
spíše	spíše	k9
vyznavači	vyznavač	k1gMnPc1
liberalismu	liberalismus	k1gInSc2
<g/>
,	,	kIx,
Sarmientova	Sarmientův	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
vzor	vzor	k1gInSc4
považovali	považovat	k5eAaImAgMnP
Evropu	Evropa	k1gFnSc4
a	a	k8xC
hájili	hájit	k5eAaImAgMnP
zájmy	zájem	k1gInPc4
imigrantů	imigrant	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1839	#num#	k4
<g/>
–	–	k?
<g/>
1851	#num#	k4
<g/>
)	)	kIx)
táhnoucí	táhnoucí	k2eAgFnSc2d1
se	se	k3xPyFc4
dvanáct	dvanáct	k4xCc4
let	let	k1gInSc4
oslabovala	oslabovat	k5eAaImAgFnS
vnitřní	vnitřní	k2eAgInSc4d1
život	život	k1gInSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
také	také	k9
ohrožením	ohrožení	k1gNnSc7
pro	pro	k7c4
její	její	k3xOp3gFnSc4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasahovali	zasahovat	k5eAaImAgMnP
do	do	k7c2
ji	on	k3xPp3gFnSc4
vojensky	vojensky	k6eAd1
nebo	nebo	k8xC
diplomaticky	diplomaticky	k6eAd1
vedle	vedle	k7c2
Rosasovy	Rosasův	k2eAgFnSc2d1
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
také	také	k9
Brazílie	Brazílie	k1gFnSc2
a	a	k8xC
konečně	konečně	k6eAd1
také	také	k9
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
a	a	k8xC
Francie	Francie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
zásah	zásah	k1gInSc1
Brazílie	Brazílie	k1gFnSc2
do	do	k7c2
konfliktu	konflikt	k1gInSc2
vedl	vést	k5eAaImAgMnS
k	k	k7c3
podepsání	podepsání	k1gNnSc3
příměří	příměří	k1gNnSc2
a	a	k8xC
k	k	k7c3
revizi	revize	k1gFnSc3
hranic	hranice	k1gFnPc2
a	a	k8xC
k	k	k7c3
umístění	umístění	k1gNnSc3
brazilských	brazilský	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
Uruguaye	Uruguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokusy	pokus	k1gInPc1
o	o	k7c4
stabilizaci	stabilizace	k1gFnSc4
Uruguaye	Uruguay	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
volby	volba	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
proti	proti	k7c3
sobě	se	k3xPyFc3
stáli	stát	k5eAaImAgMnP
caudillové	caudill	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
odpůrci	odpůrce	k1gMnPc1
napříč	napříč	k6eAd1
obou	dva	k4xCgFnPc2
politických	politický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentem	prezident	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvzdělanějších	vzdělaný	k2eAgMnPc2d3
Uruguayců	Uruguayec	k1gMnPc2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
Juan	Juan	k1gMnSc1
Francisco	Francisco	k1gMnSc1
Giró	Giró	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
programem	program	k1gInSc7
politiky	politika	k1gFnSc2
usmíření	usmíření	k1gNnSc2
a	a	k8xC
stabilizace	stabilizace	k1gFnSc2
země	zem	k1gFnPc4
zničené	zničený	k2eAgFnPc4d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giró	Giró	k1gFnPc2
stál	stát	k5eAaImAgInS
před	před	k7c7
velmi	velmi	k6eAd1
obtížným	obtížný	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
obnovení	obnovení	k1gNnSc2
autority	autorita	k1gFnSc2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
nebyl	být	k5eNaImAgInS
schopen	schopen	k2eAgMnSc1d1
zaručit	zaručit	k5eAaPmF
základní	základní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
svým	svůj	k3xOyFgMnPc3
obyvatelům	obyvatel	k1gMnPc3
a	a	k8xC
v	v	k7c6
němž	jenž	k3xRgInSc6
bylo	být	k5eAaImAgNnS
právo	právo	k1gNnSc4
v	v	k7c6
rukou	ruka	k1gFnPc6
silných	silný	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
respektive	respektive	k9
caudillů	caudill	k1gMnPc2
<g/>
,	,	kIx,
majících	mající	k2eAgMnPc2d1
na	na	k7c6
své	svůj	k3xOyFgFnSc6
straně	strana	k1gFnSc6
soukromé	soukromý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
byl	být	k5eAaImAgInS
Girův	Girův	k2eAgInSc1d1
pokus	pokus	k1gInSc1
odsouzen	odsoudit	k5eAaPmNgInS,k5eAaImNgInS
k	k	k7c3
neúspěchu	neúspěch	k1gInSc3
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1853	#num#	k4
se	se	k3xPyFc4
vlády	vláda	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
ujal	ujmout	k5eAaPmAgInS
triumvirát	triumvirát	k1gInSc4
generálů	generál	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
další	další	k2eAgFnSc3d1
destabilizaci	destabilizace	k1gFnSc3
a	a	k8xC
další	další	k2eAgFnSc3d1
intervenci	intervence	k1gFnSc3
Brazilců	Brazilec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálové	generálová	k1gFnSc6
se	se	k3xPyFc4
zemi	zem	k1gFnSc6
snažili	snažit	k5eAaImAgMnP
integrovat	integrovat	k5eAaBmF
pod	pod	k7c7
heslem	heslo	k1gNnSc7
nadstranické	nadstranický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
respektive	respektive	k9
založením	založení	k1gNnSc7
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
napříště	napříště	k6eAd1
sjednocovala	sjednocovat	k5eAaImAgFnS
celý	celý	k2eAgInSc4d1
uruguayský	uruguayský	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
úspěšný	úspěšný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
zvolení	zvolení	k1gNnSc1
dalšího	další	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Gabriela	Gabriel	k1gMnSc2
Antonia	Antonio	k1gMnSc2
Pereiry	Pereira	k1gMnSc2
nevedlo	vést	k5eNaImAgNnS
k	k	k7c3
vnitřnímu	vnitřní	k2eAgInSc3d1
míru	mír	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Pereirovi	Pereir	k1gMnSc6
vystoupil	vystoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1858	#num#	k4
jeho	jeho	k3xOp3gFnSc4
neúspěšný	úspěšný	k2eNgMnSc1d1
protikandidát	protikandidát	k1gMnSc1
generál	generál	k1gMnSc1
César	César	k1gMnSc1
Díaz	Díaz	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vojenské	vojenský	k2eAgFnSc3d1
srážce	srážka	k1gFnSc3
u	u	k7c2
Quiteros	Quiterosa	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
Pereirovo	Pereirův	k2eAgNnSc1d1
vládní	vládní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
Díaze	Díaze	k1gFnSc2
porazilo	porazit	k5eAaPmAgNnS
a	a	k8xC
povstalci	povstalec	k1gMnPc1
pak	pak	k6eAd1
byli	být	k5eAaImAgMnP
popraveni	popravit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Bernardo	Bernardo	k1gNnSc1
Prudencio	Prudencio	k1gMnSc1
Berro	Berro	k1gNnSc4
nepřinesl	přinést	k5eNaPmAgInS
zemi	zem	k1gFnSc4
kýženou	kýžený	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
konsolidaci	konsolidace	k1gFnSc4
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
neúspěšný	úspěšný	k2eNgInSc4d1
puč	puč	k1gInSc4
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
zemi	zem	k1gFnSc6
zavedl	zavést	k5eAaPmAgInS
do	do	k7c2
války	válka	k1gFnSc2
proti	proti	k7c3
Paraguyi	Paraguy	k1gInSc3
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
takzvané	takzvaný	k2eAgFnSc2d1
Trojité	trojitý	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1865	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Berro	Berro	k1gNnSc4
byl	být	k5eAaImAgMnS
po	po	k7c6
prozrazení	prozrazení	k1gNnSc6
konspirace	konspirace	k1gFnSc2
zabit	zabít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
v	v	k7c6
rukou	ruka	k1gFnPc6
vojáků	voják	k1gMnPc2
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
pokračováním	pokračování	k1gNnSc7
vnitropolitické	vnitropolitický	k2eAgFnSc2d1
nestability	nestabilita	k1gFnSc2
<g/>
,	,	kIx,
soupeření	soupeření	k1gNnSc2
a	a	k8xC
slabého	slabý	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
další	další	k2eAgFnSc4d1
konsolidaci	konsolidace	k1gFnSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Lorenza	Lorenza	k?
Battleho	Battle	k1gMnSc2
<g/>
,	,	kIx,
přešla	přejít	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
do	do	k7c2
rukou	ruka	k1gFnPc2
principistů	principista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
aplikovat	aplikovat	k5eAaBmF
v	v	k7c6
zemi	zem	k1gFnSc6
se	s	k7c7
slabým	slabý	k2eAgInSc7d1
státem	stát	k1gInSc7
myšlenky	myšlenka	k1gFnSc2
absolutního	absolutní	k2eAgInSc2d1
liberalismu	liberalismus	k1gInSc2
importovaného	importovaný	k2eAgInSc2d1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pokus	pokus	k1gInSc4
skončil	skončit	k5eAaPmAgMnS
neúspěchem	neúspěch	k1gInSc7
a	a	k8xC
vedl	vést	k5eAaImAgMnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
převzetí	převzetí	k1gNnSc3
moci	moc	k1gFnSc2
vojáky	voják	k1gMnPc7
(	(	kIx(
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1879	#num#	k4
prezidentem	prezident	k1gMnSc7
stal	stát	k5eAaPmAgMnS
plukovník	plukovník	k1gMnSc1
Lorenzo	Lorenza	k1gFnSc5
Latorre	Latorr	k1gInSc5
<g/>
,	,	kIx,
zahájila	zahájit	k5eAaPmAgFnS
centrální	centrální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
strategii	strategie	k1gFnSc4
tvrdé	tvrdá	k1gFnSc2
ruky	ruka	k1gFnSc2
vůči	vůči	k7c3
separatistickým	separatistický	k2eAgInPc3d1
caudillům	caudill	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
strategie	strategie	k1gFnPc4
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c4
uvádění	uvádění	k1gNnSc4
a	a	k8xC
podpory	podpora	k1gFnPc4
nových	nový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
kapitalistických	kapitalistický	k2eAgInPc2d1
principů	princip	k1gInPc2
na	na	k7c4
venkov	venkov	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
tradiční	tradiční	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
moci	moc	k1gFnSc2
caudillismu	caudillismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caudillové	Caudillové	k2eAgFnSc1d1
tak	tak	k6eAd1
byli	být	k5eAaImAgMnP
postupně	postupně	k6eAd1
eliminováni	eliminovat	k5eAaBmNgMnP
na	na	k7c6
základě	základ	k1gInSc6
ekonomické	ekonomický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odchodu	odchod	k1gInSc6
Latorreho	Latorre	k1gMnSc2
(	(	kIx(
<g/>
1880	#num#	k4
<g/>
)	)	kIx)
vládl	vládnout	k5eAaImAgMnS
zemi	zem	k1gFnSc4
další	další	k2eAgMnSc1d1
voják	voják	k1gMnSc1
Máximo	Máxima	k1gFnSc5
Santos	Santos	k1gMnSc1
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Santos	Santos	k1gMnSc1
se	se	k3xPyFc4
projevil	projevit	k5eAaPmAgMnS
jako	jako	k9
výborný	výborný	k2eAgMnSc1d1
vyjednavač	vyjednavač	k1gMnSc1
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
získat	získat	k5eAaPmF
podporu	podpora	k1gFnSc4
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
colorados	coloradosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
tato	tento	k3xDgFnSc1
aliance	aliance	k1gFnSc1
mezi	mezi	k7c7
armádou	armáda	k1gFnSc7
a	a	k8xC
colorados	colorados	k1gInSc4
přinesla	přinést	k5eAaPmAgFnS
Uruguayi	Uruguay	k1gFnSc3
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
relativní	relativní	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kontextu	kontext	k1gInSc6
tohoto	tento	k3xDgInSc2
vývoje	vývoj	k1gInSc2
se	se	k3xPyFc4
uruguayská	uruguayský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
značně	značně	k6eAd1
proměňovala	proměňovat	k5eAaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
díky	díky	k7c3
přílivu	příliv	k1gInSc2
emigrantů	emigrant	k1gMnPc2
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
znamenali	znamenat	k5eAaImAgMnP
posilu	posila	k1gFnSc4
pro	pro	k7c4
ekonomiku	ekonomika	k1gFnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
přeorientace	přeorientace	k1gFnPc1
z	z	k7c2
produkce	produkce	k1gFnSc2
soleného	solený	k2eAgNnSc2d1
hovězího	hovězí	k1gNnSc2
masa	maso	k1gNnSc2
na	na	k7c4
produkci	produkce	k1gFnSc4
ovčí	ovčí	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gInSc1
export	export	k1gInSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
zavedení	zavedení	k1gNnSc1
produkce	produkce	k1gFnSc2
konzerv	konzerva	k1gFnPc2
hovězího	hovězí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
představovaly	představovat	k5eAaImAgInP
významné	významný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitá	důležitý	k2eAgFnSc1d1
byla	být	k5eAaImAgNnP
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
ostatně	ostatně	k6eAd1
v	v	k7c6
celém	celý	k2eAgInSc6d1
regionu	region	k1gInSc6
<g/>
,	,	kIx,
přítomnost	přítomnost	k1gFnSc1
britského	britský	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnPc4
například	například	k6eAd1
investoval	investovat	k5eAaBmAgMnS
do	do	k7c2
stavby	stavba	k1gFnSc2
uruguayských	uruguayský	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
do	do	k7c2
městských	městský	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
a	a	k8xC
do	do	k7c2
pojišťovnictví	pojišťovnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc4
a	a	k8xC
význam	význam	k1gInSc4
Británie	Británie	k1gFnSc2
tak	tak	k9
byl	být	k5eAaImAgInS
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
poloviny	polovina	k1gFnPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
klíčový	klíčový	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
k	k	k7c3
moci	moc	k1gFnSc3
vrátila	vrátit	k5eAaPmAgFnS
opět	opět	k6eAd1
civilní	civilní	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
a	a	k8xC
prezidentem	prezident	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zástupce	zástupce	k1gMnSc1
colorados	colorados	k1gMnSc1
Julio	Julio	k1gMnSc1
Herrera	Herrera	k1gFnSc1
y	y	k?
Obes	Obes	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
si	se	k3xPyFc3
ale	ale	k8xC
svojí	svůj	k3xOyFgFnSc7
politikou	politika	k1gFnSc7
záhy	záhy	k6eAd1
znepřátelil	znepřátelit	k5eAaPmAgInS
nejen	nejen	k6eAd1
opoziční	opoziční	k2eAgInSc1d1
blancos	blancos	k1gInSc1
a	a	k8xC
montevidejské	montevidejský	k2eAgNnSc1d1
podnikale	podnikal	k1gMnSc5
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
významnou	významný	k2eAgFnSc4d1
část	část	k1gFnSc4
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
colorados	coloradosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
příčin	příčina	k1gFnPc2
Herrerovy	Herrerův	k2eAgFnSc2d1
nepopularity	nepopularita	k1gFnSc2
byl	být	k5eAaImAgInS
krach	krach	k1gInSc1
státních	státní	k2eAgFnPc2d1
financí	finance	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomická	ekonomický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1890	#num#	k4
je	být	k5eAaImIp3nS
také	také	k9
považována	považován	k2eAgFnSc1d1
za	za	k7c4
krizi	krize	k1gFnSc4
doposud	doposud	k6eAd1
dominantního	dominantní	k2eAgInSc2d1
primitivního	primitivní	k2eAgInSc2d1
liberalismu	liberalismus	k1gInSc2
a	a	k8xC
postupný	postupný	k2eAgInSc4d1
příklon	příklon	k1gInSc4
k	k	k7c3
politice	politika	k1gFnSc3
budoucích	budoucí	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
intervencí	intervence	k1gFnPc2
do	do	k7c2
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgMnSc1d1
v	v	k7c6
řadě	řada	k1gFnSc6
vnitropolitických	vnitropolitický	k2eAgFnPc2d1
krizí	krize	k1gFnPc2
přerostla	přerůst	k5eAaPmAgFnS
v	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1897	#num#	k4
v	v	k7c4
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
zahájenou	zahájený	k2eAgFnSc4d1
tzv.	tzv.	kA
bílou	bílý	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
však	však	k9
neměla	mít	k5eNaImAgFnS
dlouhého	dlouhý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
a	a	k8xC
už	už	k6eAd1
v	v	k7c6
září	září	k1gNnSc6
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
Juanu	Juan	k1gMnSc3
Lidolfovi	Lidolf	k1gMnSc3
Cuestasovi	Cuestas	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
uzavřít	uzavřít	k5eAaPmF
politický	politický	k2eAgInSc4d1
kompromis	kompromis	k1gInSc4
mezi	mezi	k7c4
blancos	blancos	k1gInSc4
a	a	k8xC
colorados	colorados	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
provést	provést	k5eAaPmF
státní	státní	k2eAgInSc4d1
převrat	převrat	k1gInSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
mu	on	k3xPp3gMnSc3
umožnil	umožnit	k5eAaPmAgInS
provést	provést	k5eAaPmF
některé	některý	k3yIgFnPc4
reformy	reforma	k1gFnPc4
<g/>
,	,	kIx,
požadované	požadovaný	k2eAgFnPc4d1
ze	z	k7c2
strany	strana	k1gFnSc2
blancos	blancosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Cuestasova	Cuestasův	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
nebyla	být	k5eNaImAgFnS
pevná	pevný	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
venkově	venkov	k1gInSc6
mu	on	k3xPp3gMnSc3
vyrostl	vyrůst	k5eAaPmAgMnS
silný	silný	k2eAgMnSc1d1
oponent	oponent	k1gMnSc1
v	v	k7c6
caudillovi	caudill	k1gMnSc6
Aparicii	Aparicie	k1gFnSc4
Saraviavovi	Saraviava	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
specifická	specifický	k2eAgFnSc1d1
forma	forma	k1gFnSc1
dvojvládí	dvojvládí	k1gNnSc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
část	část	k1gFnSc1
departemanů	departeman	k1gMnPc2
pod	pod	k7c7
vládou	vláda	k1gFnSc7
blancos	blancosa	k1gFnPc2
a	a	k8xC
Saravii	Saravie	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
část	část	k1gFnSc1
pod	pod	k7c7
vládou	vláda	k1gFnSc7
centrální	centrální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
prezident	prezident	k1gMnSc1
José	Josý	k2eAgFnSc2d1
Batlle	Batlle	k1gFnSc2
y	y	k?
Ordóñ	Ordóñ	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
tomuto	tento	k3xDgMnSc3
rozdělení	rozdělení	k1gNnSc4
moci	moc	k1gFnSc2
učinit	učinit	k5eAaPmF,k5eAaImF
přítrž	přítrž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
se	se	k3xPyFc4
Battle	Battle	k1gFnSc2
střetl	střetnout	k5eAaPmAgInS
s	s	k7c7
odporem	odpor	k1gInSc7
blancos	blancosa	k1gFnPc2
a	a	k8xC
Saravii	Saravie	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Masoller	Masoller	k1gInSc1
byl	být	k5eAaImAgMnS
Saravia	Saravius	k1gMnSc4
poražen	porazit	k5eAaPmNgMnS
a	a	k8xC
zabit	zabít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
také	také	k9
symbolicky	symbolicky	k6eAd1
skončila	skončit	k5eAaPmAgFnS
dlouhá	dlouhý	k2eAgFnSc1d1
etapa	etapa	k1gFnSc1
dějin	dějiny	k1gFnPc2
caudillismu	caudillismus	k1gInSc2
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Prezident	prezident	k1gMnSc1
José	Josý	k2eAgFnSc2d1
Batlle	Batlle	k1gFnSc2
y	y	k?
Ordóñ	Ordóñ	k1gMnSc1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
kolem	kolem	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měla	mít	k5eAaImAgFnS
Uruguay	Uruguay	k1gFnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
ekonomického	ekonomický	k2eAgInSc2d1
boomu	boom	k1gInSc2
a	a	k8xC
několika	několik	k4yIc2
vln	vlna	k1gFnPc2
emigrantů	emigrant	k1gMnPc2
již	již	k6eAd1
celkem	celkem	k6eAd1
1	#num#	k4
114	#num#	k4
0000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
oproti	oproti	k7c3
asi	asi	k9
120	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
výrazný	výrazný	k2eAgInSc4d1
nárůst	nárůst	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Země	země	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
také	také	k9
významnými	významný	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
spojenými	spojený	k2eAgFnPc7d1
s	s	k7c7
industrializaci	industrializace	k1gFnSc4
<g/>
,	,	kIx,
zaváděním	zavádění	k1gNnSc7
nových	nový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
nových	nový	k2eAgNnPc2d1
výrobních	výrobní	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
sociálních	sociální	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
těchto	tento	k3xDgFnPc2
změn	změna	k1gFnPc2
byl	být	k5eAaImAgInS
vznik	vznik	k1gInSc1
městského	městský	k2eAgNnSc2d1
dělnictva	dělnictvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
už	už	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
politizovat	politizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
formovat	formovat	k5eAaImF
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
socialistická	socialistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nastal	nastat	k5eAaPmAgInS
soumrak	soumrak	k1gInSc1
staré	starý	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
elit	elita	k1gFnPc2
a	a	k8xC
ke	k	k7c3
slovu	slovo	k1gNnSc3
se	se	k3xPyFc4
hlásila	hlásit	k5eAaImAgFnS
nová	nový	k2eAgFnSc1d1
forma	forma	k1gFnSc1
masové	masový	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
masovou	masový	k2eAgFnSc7d1
participací	participace	k1gFnSc7
a	a	k8xC
větší	veliký	k2eAgFnSc7d2
demokratizací	demokratizace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batllismus	Batllismus	k1gInSc1
v	v	k7c6
letech	let	k1gInPc6
1903	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
</s>
<s>
José	Josý	k2eAgFnPc1d1
Batlle	Batlle	k1gFnPc1
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
Uruguaye	Uruguay	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1903	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
a	a	k8xC
1911	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
<g/>
,	,	kIx,
si	se	k3xPyFc3
tento	tento	k3xDgInSc1
trend	trend	k1gInSc1
zcela	zcela	k6eAd1
jasně	jasně	k6eAd1
uvědomoval	uvědomovat	k5eAaImAgMnS
a	a	k8xC
pochopil	pochopit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
úspěchu	úspěch	k1gInSc2
může	moct	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
získá	získat	k5eAaPmIp3nS
podporu	podpora	k1gFnSc4
mezi	mezi	k7c7
širokými	široký	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
–	–	k?
především	především	k9
dělnictvem	dělnictvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
Batlleovi	Batlleus	k1gMnSc3
nové	nový	k2eAgFnSc2d1
reformní	reformní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
reforma	reforma	k1gFnSc1
pracovního	pracovní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byla	být	k5eAaImAgFnS
osmihodinová	osmihodinový	k2eAgFnSc1d1
pracovní	pracovní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
a	a	k8xC
povinný	povinný	k2eAgInSc4d1
nedělní	nedělní	k2eAgInSc4d1
odpočinek	odpočinek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
byl	být	k5eAaImAgInS
předložen	předložit	k5eAaPmNgInS
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
ale	ale	k8xC
trvalo	trvat	k5eAaImAgNnS
celých	celý	k2eAgNnPc2d1
deset	deset	k4xCc1
let	léto	k1gNnPc2
než	než	k8xS
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
podařilo	podařit	k5eAaPmAgNnS
prosadit	prosadit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
opozici	opozice	k1gFnSc6
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
stály	stát	k5eAaImAgFnP
především	především	k9
bohaté	bohatý	k2eAgFnPc1d1
podnikatelské	podnikatelský	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batllemu	Batllem	k1gInSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
navázat	navázat	k5eAaPmF
úspěšnou	úspěšný	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
odbory	odbor	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
nepokusil	pokusit	k5eNaPmAgInS
o	o	k7c4
korporativní	korporativní	k2eAgFnSc4d1
integraci	integrace	k1gFnSc4
odborů	odbor	k1gInPc2
do	do	k7c2
mocenského	mocenský	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
batllismus	batllismus	k1gInSc1
odlišovalo	odlišovat	k5eAaImAgNnS
od	od	k7c2
jiných	jiný	k2eAgMnPc2d1
latinskoamerických	latinskoamerický	k2eAgMnPc2d1
reformismů	reformismus	k1gInPc2
(	(	kIx(
<g/>
především	především	k9
pak	pak	k6eAd1
od	od	k7c2
pozdějšího	pozdní	k2eAgInSc2d2
perónismu	perónismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
poměrně	poměrně	k6eAd1
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
Batllemu	Batllemo	k1gNnSc3
v	v	k7c6
prvním	první	k4xOgNnSc6
prezidentském	prezidentský	k2eAgNnSc6d1
období	období	k1gNnSc6
podařilo	podařit	k5eAaPmAgNnS
přikročit	přikročit	k5eAaPmF
k	k	k7c3
dalších	další	k2eAgFnPc6d1
řadě	řada	k1gFnSc3
modernizačních	modernizační	k2eAgInPc2d1
kroků	krok	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
sekularizace	sekularizace	k1gFnSc2
společnosti	společnost	k1gFnSc2
a	a	k8xC
základního	základní	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
posílení	posílení	k1gNnSc2
demokratických	demokratický	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
vládnutí	vládnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batlleho	Batlle	k1gMnSc2
mandát	mandát	k1gInSc4
skočil	skočit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
s	s	k7c7
bezprecedentní	bezprecedentní	k2eAgFnSc7d1
lidovou	lidový	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
čtyřleté	čtyřletý	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
se	se	k3xPyFc4
Batlle	Batlle	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
do	do	k7c2
prezidentského	prezidentský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
s	s	k7c7
radikálním	radikální	k2eAgInSc7d1
plánem	plán	k1gInSc7
na	na	k7c4
další	další	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
však	však	k9
nebyly	být	k5eNaImAgFnP
po	po	k7c6
chuti	chuť	k1gFnSc6
především	především	k9
některým	některý	k3yIgMnPc3
podnikatelům	podnikatel	k1gMnPc3
<g/>
,	,	kIx,
velkostatkářům	velkostatkář	k1gMnPc3
a	a	k8xC
zahraničnímu	zahraniční	k2eAgInSc3d1
kapitálu	kapitál	k1gInSc3
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
moc	moc	k6eAd1
chtěl	chtít	k5eAaImAgInS
Batlle	Batlle	k1gInSc1
omezit	omezit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batlleho	Batlleha	k1gFnSc5
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
dosáhnout	dosáhnout	k5eAaPmF
větší	veliký	k2eAgFnSc3d2
ekonomické	ekonomický	k2eAgFnSc3d1
nezávislosti	nezávislost	k1gFnSc3
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
zavedení	zavedení	k1gNnSc1
kolektivní	kolektivní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batlle	Batlle	k1gInSc1
se	se	k3xPyFc4
také	také	k9
snažil	snažit	k5eAaImAgMnS
posílit	posílit	k5eAaPmF
ekonomickou	ekonomický	k2eAgFnSc4d1
a	a	k8xC
sociální	sociální	k2eAgFnSc4d1
roli	role	k1gFnSc4
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistoupil	přistoupit	k5eAaPmAgMnS
ke	k	k7c3
znárodnění	znárodnění	k1gNnSc3
některých	některý	k3yIgFnPc2
bank	banka	k1gFnPc2
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
konkurovat	konkurovat	k5eAaImF
zahraničnímu	zahraniční	k2eAgInSc3d1
kapitálu	kapitál	k1gInSc3
ve	v	k7c6
stavbě	stavba	k1gFnSc6
nových	nový	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
paralelně	paralelně	k6eAd1
s	s	k7c7
těmi	ten	k3xDgMnPc7
britskými	britský	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Battle	Battle	k1gFnSc1
vytvořil	vytvořit	k5eAaPmAgInS
také	také	k6eAd1
Státní	státní	k2eAgFnSc4d1
spořitelnu	spořitelna	k1gFnSc4
a	a	k8xC
nebo	nebo	k8xC
Státní	státní	k2eAgInPc1d1
elektrické	elektrický	k2eAgInPc1d1
závody	závod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
kroky	krok	k1gInPc7
v	v	k7c6
zavedení	zavedení	k1gNnSc6
nové	nový	k2eAgFnSc2d1
mrazírenské	mrazírenský	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
zpracování	zpracování	k1gNnSc1
hovězího	hovězí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
založením	založení	k1gNnSc7
Uruguayských	uruguayský	k2eAgFnPc2d1
mrazíren	mrazírna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batlleho	Batlleha	k1gFnSc5
druhé	druhý	k4xOgNnSc1
prezidentské	prezidentský	k2eAgNnSc1d1
období	období	k1gNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
zcela	zcela	k6eAd1
úspěšné	úspěšný	k2eAgNnSc1d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
dlouhodobé	dlouhodobý	k2eAgFnPc4d1
udržitelnosti	udržitelnost	k1gFnPc4
jeho	jeho	k3xOp3gFnPc2
některých	některý	k3yIgFnPc2
reforem	reforma	k1gFnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
Uruguayské	uruguayský	k2eAgFnPc1d1
mrazírny	mrazírna	k1gFnPc1
se	se	k3xPyFc4
záhy	záhy	k6eAd1
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
rukou	ruka	k1gFnPc2
zahraničního	zahraniční	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
sociálně-ekonomická	sociálně-ekonomický	k2eAgFnSc1d1
politika	politika	k1gFnSc1
zaměřovala	zaměřovat	k5eAaImAgFnS
na	na	k7c4
městské	městský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
venkov	venkov	k1gInSc1
zůstával	zůstávat	k5eAaImAgInS
mimo	mimo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
potom	potom	k6eAd1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
politickému	politický	k2eAgNnSc3d1
i	i	k8xC
sociálnímu	sociální	k2eAgNnSc3d1
rozdělení	rozdělení	k1gNnSc3
mezi	mezi	k7c7
venkovem	venkov	k1gInSc7
(	(	kIx(
<g/>
tradiční	tradiční	k2eAgFnSc7d1
baštou	bašta	k1gFnSc7
blancos	blancosa	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
projevovat	projevovat	k5eAaImF
také	také	k9
na	na	k7c6
postupném	postupný	k2eAgInSc6d1
propadu	propad	k1gInSc6
obchodu	obchod	k1gInSc2
s	s	k7c7
hovězím	hovězí	k2eAgNnSc7d1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
stále	stále	k6eAd1
zmenšoval	zmenšovat	k5eAaImAgInS
díky	díky	k7c3
nedostatku	nedostatek	k1gInSc3
technologických	technologický	k2eAgFnPc2d1
inovací	inovace	k1gFnPc2
a	a	k8xC
nepříznivé	příznivý	k2eNgFnSc3d1
situaci	situace	k1gFnSc3
na	na	k7c6
světových	světový	k2eAgInPc6d1
trzích	trh	k1gInPc6
a	a	k8xC
v	v	k7c6
podstatě	podstata	k1gFnSc6
měl	mít	k5eAaImAgMnS
dopady	dopad	k1gInPc7
v	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
na	na	k7c4
uruguayský	uruguayský	k2eAgInSc4d1
venkov	venkov	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
širším	široký	k2eAgMnSc6d2
na	na	k7c4
celou	celý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Významným	významný	k2eAgInSc7d1
vývozním	vývozní	k2eAgInSc7d1
artiklem	artikl	k1gInSc7
ale	ale	k8xC
zůstávala	zůstávat	k5eAaImAgFnS
uruguayská	uruguayský	k2eAgFnSc1d1
ovčí	ovčí	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
Přehledné	přehledný	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
1903	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
Batlle	Batlle	k1gFnSc2
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
počátek	počátek	k1gInSc1
uruguayského	uruguayský	k2eAgInSc2d1
reformismu	reformismus	k1gInSc2
<g/>
/	/	kIx~
<g/>
batllismu	batllismus	k1gInSc2
</s>
<s>
1906	#num#	k4
<g/>
:	:	kIx,
návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
o	o	k7c4
osmihodinové	osmihodinový	k2eAgFnPc4d1
pracovní	pracovní	k2eAgFnPc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
přijat	přijat	k2eAgInSc1d1
1916	#num#	k4
</s>
<s>
1910	#num#	k4
<g/>
:	:	kIx,
demokratizační	demokratizační	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
volebního	volební	k2eAgInSc2d1
zákona	zákon	k1gInSc2
(	(	kIx(
<g/>
lepší	dobrý	k2eAgNnSc1d2
zastoupení	zastoupení	k1gNnSc1
pro	pro	k7c4
menšiny	menšina	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
1917	#num#	k4
<g/>
:	:	kIx,
nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
rozděluje	rozdělovat	k5eAaImIp3nS
exekutivní	exekutivní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
mezi	mezi	k7c4
prezidenta	prezident	k1gMnSc4
a	a	k8xC
Národní	národní	k2eAgFnSc4d1
radu	rada	k1gFnSc4
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
pro	pro	k7c4
ženy	žena	k1gFnPc4
</s>
<s>
1933	#num#	k4
<g/>
:	:	kIx,
prezident	prezident	k1gMnSc1
Terra	Terra	k1gMnSc1
realizuje	realizovat	k5eAaBmIp3nS
puč	puč	k1gInSc4
</s>
<s>
1934	#num#	k4
<g/>
:	:	kIx,
reforma	reforma	k1gFnSc1
ústavy	ústava	k1gFnSc2
</s>
<s>
1942	#num#	k4
<g/>
:	:	kIx,
nenásilný	násilný	k2eNgInSc1d1
převrat	převrat	k1gInSc1
prezidenta	prezident	k1gMnSc2
Baldomira	Baldomir	k1gMnSc2
nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
a	a	k8xC
neutralita	neutralita	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
1945	#num#	k4
<g/>
:	:	kIx,
Uruguay	Uruguay	k1gFnSc1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
Druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
na	na	k7c6
straně	strana	k1gFnSc6
Spojenců	spojenec	k1gMnPc2
</s>
<s>
1949	#num#	k4
<g/>
:	:	kIx,
odprodej	odprodej	k1gInSc4
železnic	železnice	k1gFnPc2
do	do	k7c2
rukou	ruka	k1gFnPc2
státu	stát	k1gInSc2
</s>
<s>
1952	#num#	k4
<g/>
:	:	kIx,
nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
zavádí	zavádět	k5eAaImIp3nS
kolektivní	kolektivní	k2eAgInSc4d1
způsob	způsob	k1gInSc4
vlády	vláda	k1gFnSc2
Národní	národní	k2eAgFnSc4d1
vládní	vládní	k2eAgFnSc4d1
radu	rada	k1gFnSc4
</s>
<s>
1958	#num#	k4
<g/>
:	:	kIx,
blancos	blancosa	k1gFnPc2
poprvé	poprvé	k6eAd1
ve	v	k7c6
vládě	vláda	k1gFnSc6
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1963	#num#	k4
<g/>
:	:	kIx,
vznik	vznik	k1gInSc1
levicových	levicový	k2eAgMnPc2d1
radikálů	radikál	k1gMnPc2
Tupamaros	Tupamarosa	k1gFnPc2
</s>
<s>
1966	#num#	k4
<g/>
:	:	kIx,
návrat	návrat	k1gInSc1
colorados	colorados	k1gInSc1
k	k	k7c3
moci	moc	k1gFnSc3
a	a	k8xC
návrat	návrat	k1gInSc4
k	k	k7c3
prezidentskému	prezidentský	k2eAgInSc3d1
systému	systém	k1gInSc3
</s>
<s>
1971	#num#	k4
<g/>
:	:	kIx,
vznik	vznik	k1gInSc1
levicové	levicový	k2eAgFnSc2d1
Široké	Široké	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
(	(	kIx(
<g/>
Frente	Frent	k1gInSc5
Amplia	Amplius	k1gMnSc2
<g/>
,	,	kIx,
FA	fa	kA
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
nástup	nástup	k1gInSc1
Bordaberry	Bordaberra	k1gFnSc2
do	do	k7c2
úřadu	úřad	k1gInSc2
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
příprava	příprava	k1gFnSc1
vojenské	vojenský	k2eAgFnSc2d1
diktatury	diktatura	k1gFnSc2
</s>
<s>
1973	#num#	k4
<g/>
:	:	kIx,
rozpuštěn	rozpuštěn	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
likvidace	likvidace	k1gFnSc1
městské	městský	k2eAgFnSc2d1
guerilly	guerilla	k1gFnSc2
Tupamaros	Tupamarosa	k1gFnPc2
</s>
<s>
1976	#num#	k4
<g/>
:	:	kIx,
Bordaberra	Bordaberr	k1gInSc2
odstaven	odstaven	k2eAgMnSc1d1
<g/>
,	,	kIx,
vláda	vláda	k1gFnSc1
v	v	k7c6
rukou	ruka	k1gFnPc6
generálů	generál	k1gMnPc2
</s>
<s>
1980	#num#	k4
<g/>
:	:	kIx,
odmítnutí	odmítnutí	k1gNnSc3
vlády	vláda	k1gFnSc2
vojáků	voják	k1gMnPc2
v	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
hlasování	hlasování	k1gNnSc6
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
dohoda	dohoda	k1gFnSc1
z	z	k7c2
Námořního	námořní	k2eAgInSc2d1
klubu	klub	k1gInSc2
a	a	k8xC
návrat	návrat	k1gInSc4
k	k	k7c3
civilní	civilní	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
</s>
<s>
1985	#num#	k4
<g/>
:	:	kIx,
prezidentem	prezident	k1gMnSc7
J.	J.	kA
<g/>
M.	M.	kA
Sanguinetti	Sanguinett	k1gMnPc1
(	(	kIx(
<g/>
colorados	colorados	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
:	:	kIx,
referendem	referendum	k1gNnSc7
schválen	schválen	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c4
amnestii	amnestie	k1gFnSc4
vojáků	voják	k1gMnPc2
</s>
<s>
1991	#num#	k4
<g/>
:	:	kIx,
založení	založení	k1gNnSc2
MERCOSURu	MERCOSURus	k1gInSc2
</s>
<s>
1995	#num#	k4
<g/>
:	:	kIx,
vládní	vládní	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
colorados	coloradosa	k1gFnPc2
a	a	k8xC
blancos	blancosa	k1gFnPc2
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
reforma	reforma	k1gFnSc1
ústavy	ústava	k1gFnSc2
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
zřízení	zřízení	k1gNnSc4
Komise	komise	k1gFnSc2
pro	pro	k7c4
mír	mír	k1gInSc4
dokumentující	dokumentující	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
vojenské	vojenský	k2eAgFnSc2d1
junty	junta	k1gFnSc2
v	v	k7c6
U.	U.	kA
</s>
<s>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
hospodářská	hospodářský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
prezidentem	prezident	k1gMnSc7
kandidát	kandidát	k1gMnSc1
Široké	Široké	k2eAgInPc4d1
fronty	front	k1gInPc4
Tabaré	Tabarý	k2eAgInPc4d1
Vázquez	Vázquez	k1gInSc4
</s>
<s>
2010	#num#	k4
<g/>
:	:	kIx,
prezidentem	prezident	k1gMnSc7
kandidát	kandidát	k1gMnSc1
Široké	Široké	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
José	Josá	k1gFnSc2
Mujica	Mujic	k1gInSc2
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
ve	v	k7c6
stínu	stín	k1gInSc6
batllismu	batllismus	k1gInSc2
1915	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
</s>
<s>
Batlle	Batlle	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
vlivnou	vlivný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
figurou	figura	k1gFnSc7
Uruguaye	Uruguay	k1gFnSc2
i	i	k8xC
po	po	k7c6
svém	svůj	k3xOyFgInSc6
odchodu	odchod	k1gInSc6
z	z	k7c2
funkce	funkce	k1gFnSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
po	po	k7c6
odchodu	odchod	k1gInSc6
z	z	k7c2
funkce	funkce	k1gFnSc2
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
prosadit	prosadit	k5eAaPmF
svojí	svůj	k3xOyFgFnSc7
reformu	reforma	k1gFnSc4
demokratického	demokratický	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
země	zem	k1gFnSc2
směrem	směr	k1gInSc7
ke	k	k7c3
kolektivní	kolektivní	k2eAgFnSc3d1
demokracii	demokracie	k1gFnSc3
podle	podle	k7c2
evropského	evropský	k2eAgInSc2d1
(	(	kIx(
<g/>
respektive	respektive	k9
švýcarského	švýcarský	k2eAgMnSc2d1
<g/>
)	)	kIx)
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
návrhem	návrh	k1gInSc7
ale	ale	k8xC
utrpěl	utrpět	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
volební	volební	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
odporu	odpor	k1gInSc3
uruguayského	uruguayský	k2eAgInSc2d1
venkova	venkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
poté	poté	k6eAd1
Batlle	Batlle	k1gInSc1
zůstával	zůstávat	k5eAaImAgInS
politicky	politicky	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
a	a	k8xC
šikovným	šikovný	k2eAgNnSc7d1
politickým	politický	k2eAgNnSc7d1
manévrováním	manévrování	k1gNnSc7
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
vést	vést	k5eAaImF
úspěšný	úspěšný	k2eAgInSc4d1
dialog	dialog	k1gInSc4
mezi	mezi	k7c7
jeho	jeho	k3xOp3gFnSc7
stranou	strana	k1gFnSc7
a	a	k8xC
blancos	blancosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
tak	tak	k9
nesla	nést	k5eAaImAgFnS
Batlleho	Batlle	k1gMnSc4
rukopis	rukopis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
ústavy	ústava	k1gFnSc2
byly	být	k5eAaImAgFnP
upraveny	upraven	k2eAgFnPc4d1
pravomoce	pravomoc	k1gFnPc4
prezidenta	prezident	k1gMnSc2
a	a	k8xC
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
rozšířeno	rozšířen	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
a	a	k8xC
nebo	nebo	k8xC
ustanovena	ustanoven	k2eAgFnSc1d1
odluka	odluka	k1gFnSc1
státu	stát	k1gInSc2
od	od	k7c2
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústava	ústava	k1gFnSc1
také	také	k6eAd1
upravila	upravit	k5eAaPmAgFnS
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
vládou	vláda	k1gFnSc7
a	a	k8xC
opozicí	opozice	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
dávala	dávat	k5eAaImAgFnS
široké	široký	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
lokálním	lokální	k2eAgFnPc3d1
administrativám	administrativa	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
ústava	ústava	k1gFnSc1
v	v	k7c6
referendu	referendum	k1gNnSc6
přijata	přijat	k2eAgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
Batlle	Batlle	k1gNnSc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
žurnalistice	žurnalistika	k1gFnSc3
a	a	k8xC
politické	politický	k2eAgFnSc3d1
osvětě	osvěta	k1gFnSc3
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
nichž	jenž	k3xRgFnPc2
si	se	k3xPyFc3
udržoval	udržovat	k5eAaImAgInS
vliv	vliv	k1gInSc4
na	na	k7c4
politické	politický	k2eAgNnSc4d1
dění	dění	k1gNnSc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dařilo	dařit	k5eAaImAgNnS
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batlleho	Batlle	k1gMnSc2
reformismus	reformismus	k1gInSc4
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
hodnocen	hodnotit	k5eAaImNgInS
ambivalentně	ambivalentně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
argentinským	argentinský	k2eAgInSc7d1
reformismem	reformismus	k1gInSc7
(	(	kIx(
<g/>
=	=	kIx~
politika	politika	k1gFnSc1
Radikální	radikální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
H.	H.	kA
Yrigoyena	Yrigoyena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
batllismus	batllismus	k1gInSc1
projevem	projev	k1gInSc7
oficialismu	oficialismus	k1gInSc2
(	(	kIx(
<g/>
vycházel	vycházet	k5eAaImAgMnS
z	z	k7c2
liberální	liberální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
colorados	coloradosa	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
u	u	k7c2
moci	moc	k1gFnSc2
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
mnohém	mnohé	k1gNnSc6
umírněnější	umírněný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
připomínáno	připomínán	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Batlle	Batlle	k1gInSc1
byl	být	k5eAaImAgInS
politická	politický	k2eAgFnSc1d1
osobnost	osobnost	k1gFnSc1
tíhnoucí	tíhnoucí	k2eAgFnSc1d1
k	k	k7c3
autoritářství	autoritářství	k1gNnSc3
a	a	k8xC
netoleranci	netolerance	k1gFnSc3
vůči	vůči	k7c3
politickým	politický	k2eAgMnPc3d1
oponentům	oponent	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
mezi	mezi	k7c7
roky	rok	k1gInPc7
1930	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
</s>
<s>
Také	také	k9
ekonomika	ekonomika	k1gFnSc1
Uruguaye	Uruguay	k1gFnSc2
byla	být	k5eAaImAgFnS
poškozena	poškodit	k5eAaPmNgFnS
důsledky	důsledek	k1gInPc4
světové	světový	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1929	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc1
po	po	k7c6
letech	léto	k1gNnPc6
relativního	relativní	k2eAgInSc2d1
růstu	růst	k1gInSc2
před	před	k7c7
a	a	k8xC
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
hospodářský	hospodářský	k2eAgInSc1d1
boom	boom	k1gInSc1
dával	dávat	k5eAaImAgInS
zemi	zem	k1gFnSc3
relativní	relativní	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
a	a	k8xC
také	také	k9
finanční	finanční	k2eAgInSc4d1
prostor	prostor	k1gInSc4
pro	pro	k7c4
Batlleho	Batlle	k1gMnSc4
reformy	reforma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
se	s	k7c7
prezidentem	prezident	k1gMnSc7
stal	stát	k5eAaPmAgMnS
Gabriel	Gabriel	k1gMnSc1
Terra	Terra	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
v	v	k7c6
zemi	zem	k1gFnSc6
zavedl	zavést	k5eAaPmAgInS
polodiktátorský	polodiktátorský	k2eAgInSc1d1
režim	režim	k1gInSc1
s	s	k7c7
podporou	podpora	k1gFnSc7
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terra	Terr	k1gInSc2
následně	následně	k6eAd1
rozpustil	rozpustit	k5eAaPmAgInS
parlament	parlament	k1gInSc1
<g/>
,	,	kIx,
uplatňoval	uplatňovat	k5eAaImAgInS
politiku	politika	k1gFnSc4
potlačování	potlačování	k1gNnSc2
svobody	svoboda	k1gFnSc2
slova	slovo	k1gNnSc2
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
prosazovat	prosazovat	k5eAaImF
některé	některý	k3yIgInPc4
protibatllistické	protibatllistický	k2eAgInPc4d1
kroky	krok	k1gInPc4
směrem	směr	k1gInSc7
k	k	k7c3
větší	veliký	k2eAgFnSc3d2
liberalizaci	liberalizace	k1gFnSc3
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
umenšení	umenšení	k1gNnSc2
role	role	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
také	také	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
přeorientování	přeorientování	k1gNnSc3
uruguayské	uruguayský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
na	na	k7c4
stále	stále	k6eAd1
silnější	silný	k2eAgInPc4d2
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitropoliticky	vnitropoliticky	k6eAd1
byla	být	k5eAaImAgFnS
nejvýznamnější	významný	k2eAgFnSc1d3
nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1934	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
znamenala	znamenat	k5eAaImAgFnS
plný	plný	k2eAgInSc4d1
návrat	návrat	k1gInSc4
k	k	k7c3
prezidentskému	prezidentský	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terra	Terr	k1gInSc2
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
země	zem	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Alfredo	Alfredo	k1gNnSc4
Baldomiro	Baldomiro	k1gNnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
dokončil	dokončit	k5eAaPmAgInS
návrat	návrat	k1gInSc4
k	k	k7c3
civilní	civilní	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
Uruguay	Uruguay	k1gFnSc1
přerušila	přerušit	k5eAaPmAgFnS
vztahy	vztah	k1gInPc4
se	s	k7c7
zeměmi	zem	k1gFnPc7
Osy	osa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
se	se	k3xPyFc4
ale	ale	k9
snažila	snažit	k5eAaImAgFnS
držet	držet	k5eAaImF
mimo	mimo	k7c4
válečný	válečný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
a	a	k8xC
vyhlásila	vyhlásit	k5eAaPmAgFnS
neutralitu	neutralita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
přítomnost	přítomnost	k1gFnSc4
německé	německý	k2eAgFnSc2d1
bitevní	bitevní	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Admiral	Admiral	k1gFnPc1
Graf	graf	k1gInSc4
Spee	Spe	k1gFnSc2
přivedla	přivést	k5eAaPmAgFnS
Uruguay	Uruguay	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
diplomatické	diplomatický	k2eAgFnSc2d1
ofenzivy	ofenziva	k1gFnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
USA	USA	kA
i	i	k8xC
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
se	se	k3xPyFc4
Uruguay	Uruguay	k1gFnSc1
připojila	připojit	k5eAaPmAgFnS
na	na	k7c4
stranu	strana	k1gFnSc4
Spojenců	spojenec	k1gMnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
vyhlásila	vyhlásit	k5eAaPmAgFnS
válku	válka	k1gFnSc4
zemím	zem	k1gFnPc3
Osy	osa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
rovněž	rovněž	k9
znamenala	znamenat	k5eAaImAgFnS
ekonomické	ekonomický	k2eAgNnSc4d1
oživení	oživení	k1gNnSc4
uruguayského	uruguayský	k2eAgInSc2d1
exportu	export	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Poválečný	poválečný	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
vytvářel	vytvářet	k5eAaImAgInS
prostor	prostor	k1gInSc4
pro	pro	k7c4
pokračování	pokračování	k1gNnSc4
politiky	politika	k1gFnSc2
sociálního	sociální	k2eAgInSc2d1
reformismu	reformismus	k1gInSc2
Batlleho	Batlle	k1gMnSc2
střihu	střih	k1gInSc2
(	(	kIx(
<g/>
či	či	k8xC
neobatllismu	neobatllismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
se	s	k7c7
prezidentem	prezident	k1gMnSc7
stal	stát	k5eAaPmAgMnS
Batlleho	Batlle	k1gMnSc2
synovec	synovec	k1gMnSc1
Luis	Luisa	k1gFnPc2
Conrado	Conrada	k1gFnSc5
Batlle	Batll	k1gMnPc4
Berres	Berres	k1gInSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
přivedl	přivést	k5eAaPmAgInS
batllismus	batllismus	k1gInSc1
znovu	znovu	k6eAd1
do	do	k7c2
centra	centrum	k1gNnSc2
politiky	politika	k1gFnSc2
colorados	coloradosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příznivou	příznivý	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
politiky	politika	k1gFnSc2
posilování	posilování	k1gNnSc2
ekonomické	ekonomický	k2eAgFnSc2d1
role	role	k1gFnSc2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
jeden	jeden	k4xCgMnSc1
z	z	k7c2
pilířů	pilíř	k1gInPc2
batllismu	batllismus	k1gInSc2
<g/>
)	)	kIx)
představoval	představovat	k5eAaImAgInS
ústup	ústup	k1gInSc1
britského	britský	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
spojený	spojený	k2eAgInSc1d1
s	s	k7c7
nacionalizací	nacionalizace	k1gFnSc7
doposud	doposud	k6eAd1
britských	britský	k2eAgInPc2d1
podniků	podnik	k1gInPc2
–	–	k?
především	především	k9
železnice	železnice	k1gFnSc2
<g/>
,	,	kIx,
tramvají	tramvaj	k1gFnPc2
nebo	nebo	k8xC
vodáren	vodárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poválečný	poválečný	k2eAgInSc1d1
reformismus	reformismus	k1gInSc1
Batlle	Batlle	k1gFnSc2
Berresova	Berresův	k2eAgInSc2d1
stylu	styl	k1gInSc2
se	se	k3xPyFc4
zaměřil	zaměřit	k5eAaPmAgMnS
především	především	k9
na	na	k7c4
industrializaci	industrializace	k1gFnSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
nicméně	nicméně	k8xC
dotována	dotován	k2eAgFnSc1d1
především	především	k9
z	z	k7c2
příjmů	příjem	k1gInPc2
ze	z	k7c2
zemědělství	zemědělství	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
budování	budování	k1gNnSc6
sociálního	sociální	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
byl	být	k5eAaImAgMnS
v	v	k7c6
rukou	ruka	k1gFnPc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legislativním	legislativní	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
této	tento	k3xDgFnSc2
vlny	vlna	k1gFnSc2
reforem	reforma	k1gFnPc2
byla	být	k5eAaImAgFnS
také	také	k9
nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
posilovala	posilovat	k5eAaImAgFnS
kolektivní	kolektivní	k2eAgInPc4d1
demokratické	demokratický	k2eAgInPc4d1
mechanismy	mechanismus	k1gInPc4
a	a	k8xC
v	v	k7c6
podstatě	podstata	k1gFnSc6
zrušila	zrušit	k5eAaPmAgFnS
prezidentský	prezidentský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desetiletí	desetiletí	k1gNnSc1
růstu	růst	k1gInSc2
a	a	k8xC
politické	politický	k2eAgFnSc2d1
stability	stabilita	k1gFnSc2
mezi	mezi	k7c7
roky	rok	k1gInPc7
1945	#num#	k4
až	až	k9
1955	#num#	k4
daly	dát	k5eAaPmAgFnP
Uruguayi	Uruguay	k1gFnSc6
symbolický	symbolický	k2eAgInSc1d1
název	název	k1gInSc1
"	"	kIx"
<g/>
Americké	americký	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
v	v	k7c6
zemi	zem	k1gFnSc6
začaly	začít	k5eAaPmAgFnP
projevovat	projevovat	k5eAaImF
dopady	dopad	k1gInPc4
hospodářského	hospodářský	k2eAgNnSc2d1
oživení	oživení	k1gNnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
propadu	propad	k1gInSc3
zájmu	zájem	k1gInSc2
o	o	k7c4
uruguayský	uruguayský	k2eAgInSc4d1
export	export	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
v	v	k7c6
hospodářství	hospodářství	k1gNnSc6
se	se	k3xPyFc4
rychle	rychle	k6eAd1
přenesla	přenést	k5eAaPmAgFnS
do	do	k7c2
politické	politický	k2eAgFnSc2d1
sféry	sféra	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
vedla	vést	k5eAaImAgFnS
k	k	k7c3
volební	volební	k2eAgFnSc3d1
porážce	porážka	k1gFnSc3
colorados	coloradosa	k1gFnPc2
a	a	k8xC
nástupu	nástup	k1gInSc2
první	první	k4xOgFnSc2
vlády	vláda	k1gFnSc2
blancos	blancosa	k1gFnPc2
(	(	kIx(
<g/>
Národní	národní	k2eAgFnSc2d1
bílé	bílý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
:	:	kIx,
Partido	Partida	k1gFnSc5
Nacionalista-Blanco	Nacionalista-Blanco	k1gNnSc4
<g/>
)	)	kIx)
po	po	k7c4
téměř	téměř	k6eAd1
sto	sto	k4xCgNnSc4
letech	léto	k1gNnPc6
nadvlády	nadvláda	k1gFnSc2
colorados	coloradosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Krize	krize	k1gFnSc1
a	a	k8xC
cesta	cesta	k1gFnSc1
k	k	k7c3
vojenské	vojenský	k2eAgFnSc3d1
diktatuře	diktatura	k1gFnSc3
1958	#num#	k4
až	až	k9
1972	#num#	k4
</s>
<s>
Konec	konec	k1gInSc1
politického	politický	k2eAgInSc2d1
monopolu	monopol	k1gInSc2
colorados	colorados	k1gInSc1
a	a	k8xC
nástup	nástup	k1gInSc1
celkem	celkem	k6eAd1
dvou	dva	k4xCgFnPc2
vlád	vláda	k1gFnPc2
blancos	blancos	k1gInSc4
sice	sice	k8xC
znamenal	znamenat	k5eAaImAgInS
revoluční	revoluční	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
dosavadní	dosavadní	k2eAgFnSc6d1
uruguayské	uruguayský	k2eAgFnSc6d1
politice	politika	k1gFnSc6
<g/>
,	,	kIx,
<g/>
ale	ale	k8xC
nevedl	vést	k5eNaImAgMnS
k	k	k7c3
vyřešení	vyřešení	k1gNnSc3
prohlubující	prohlubující	k2eAgFnSc2d1
se	se	k3xPyFc4
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blancos	Blancos	k1gInSc4
přišli	přijít	k5eAaPmAgMnP
k	k	k7c3
moci	moc	k1gFnSc3
s	s	k7c7
programem	program	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
dával	dávat	k5eAaImAgInS
přednost	přednost	k1gFnSc4
liberální	liberální	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
ekonomické	ekonomický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
umenšila	umenšit	k5eAaPmAgFnS
ekonomickou	ekonomický	k2eAgFnSc4d1
roli	role	k1gFnSc4
státu	stát	k1gInSc2
a	a	k8xC
umožnila	umožnit	k5eAaPmAgFnS
oživit	oživit	k5eAaPmF
ekonomiku	ekonomika	k1gFnSc4
především	především	k9
na	na	k7c6
uruguayském	uruguayský	k2eAgInSc6d1
venkově	venkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
realizace	realizace	k1gFnSc1
tohoto	tento	k3xDgInSc2
plánu	plán	k1gInSc2
byla	být	k5eAaImAgFnS
více	hodně	k6eAd2
než	než	k8xS
problematická	problematický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uruguayský	uruguayský	k2eAgInSc1d1
stát	stát	k1gInSc1
nemohl	moct	k5eNaImAgInS
být	být	k5eAaImF
ze	z	k7c2
dne	den	k1gInSc2
na	na	k7c4
den	den	k1gInSc4
reformován	reformován	k2eAgInSc4d1
<g/>
,	,	kIx,
a	a	k8xC
každá	každý	k3xTgFnSc1
reforma	reforma	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
ohrožení	ohrožení	k1gNnSc3
sociálního	sociální	k2eAgInSc2d1
smíru	smír	k1gInSc2
a	a	k8xC
pauperizaci	pauperizace	k1gFnSc4
středních	střední	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
ani	ani	k8xC
soukromý	soukromý	k2eAgInSc1d1
sektor	sektor	k1gInSc1
neměl	mít	k5eNaImAgInS
dostatek	dostatek	k1gInSc4
ekonomické	ekonomický	k2eAgFnSc2d1
síly	síla	k1gFnSc2
aby	aby	kYmCp3nS
převzal	převzít	k5eAaPmAgInS
dosavadní	dosavadní	k2eAgFnSc4d1
roli	role	k1gFnSc4
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
blancos	blancos	k1gInSc1
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
snížit	snížit	k5eAaPmF
deficit	deficit	k1gInSc4
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
a	a	k8xC
zkrotit	zkrotit	k5eAaPmF
inflaci	inflace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
vlády	vláda	k1gFnSc2
blancos	blancosa	k1gFnPc2
se	se	k3xPyFc4
krize	krize	k1gFnSc1
přenesla	přenést	k5eAaPmAgFnS
do	do	k7c2
bankovního	bankovní	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
<g/>
,	,	kIx,
vedoucí	vedoucí	k2eAgNnPc4d1
k	k	k7c3
sanaci	sanace	k1gFnSc3
krachující	krachující	k2eAgFnSc2d1
Transatlantické	transatlantický	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Montevideiská	Montevideiský	k2eAgFnSc1d1
policie	policie	k1gFnSc1
hledá	hledat	k5eAaImIp3nS
Tupamaros	Tupamarosa	k1gFnPc2
v	v	k7c6
městské	městský	k2eAgFnSc6d1
kanalizaci	kanalizace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odklon	odklon	k1gInSc1
od	od	k7c2
batllismu	batllismus	k1gInSc2
a	a	k8xC
neúspěch	neúspěch	k1gInSc1
blancos	blancosa	k1gFnPc2
ukázal	ukázat	k5eAaPmAgInS
na	na	k7c4
hloubku	hloubka	k1gFnSc4
ekonomické	ekonomický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
a	a	k8xC
na	na	k7c4
nedostatek	nedostatek	k1gInSc4
celonárodní	celonárodní	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
řešení	řešení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
vyhráli	vyhrát	k5eAaPmAgMnP
colorados	coloradosa	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
se	se	k3xPyFc4
Uruguay	Uruguay	k1gFnSc1
v	v	k7c6
referendu	referendum	k1gNnSc6
vrátila	vrátit	k5eAaPmAgFnS
k	k	k7c3
prezidentskému	prezidentský	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
kolektivní	kolektivní	k2eAgInSc1d1
systém	systém	k1gInSc1
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
podle	podle	k7c2
názoru	názor	k1gInSc2
většiny	většina	k1gFnSc2
neosvědčil	osvědčit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uruguay	Uruguay	k1gFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
obrátit	obrátit	k5eAaPmF
na	na	k7c4
pomoc	pomoc	k1gFnSc4
MMF	MMF	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
zavedení	zavedení	k1gNnSc4
drastických	drastický	k2eAgInPc2d1
hospodářských	hospodářský	k2eAgInPc2d1
kroků	krok	k1gInPc2
–	–	k?
mezi	mezi	k7c7
jinými	jiná	k1gFnPc7
devalvace	devalvace	k1gFnSc1
a	a	k8xC
otevření	otevření	k1gNnSc1
uruguayského	uruguayský	k2eAgInSc2d1
trhu	trh	k1gInSc2
zahraniční	zahraniční	k2eAgFnSc3d1
konkurenci	konkurence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
dominantních	dominantní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
nebyla	být	k5eNaImAgFnS
schopna	schopen	k2eAgFnSc1d1
přinést	přinést	k5eAaPmF
trvalý	trvalý	k2eAgInSc4d1
recept	recept	k1gInSc4
na	na	k7c4
řešení	řešení	k1gNnPc4
hospodářských	hospodářský	k2eAgInPc2d1
a	a	k8xC
sociálních	sociální	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
dávala	dávat	k5eAaImAgFnS
prostor	prostor	k1gInSc4
pro	pro	k7c4
alternativní	alternativní	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
recepty	recept	k1gInPc4
a	a	k8xC
především	především	k6eAd1
posilovala	posilovat	k5eAaImAgFnS
levici	levice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politika	politika	k1gFnSc1
MMF	MMF	kA
se	se	k3xPyFc4
nesetkávala	setkávat	k5eNaImAgFnS
s	s	k7c7
podporou	podpora	k1gFnSc7
mezi	mezi	k7c7
dělnictvem	dělnictvo	k1gNnSc7
a	a	k8xC
odbory	odbor	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
organizovaly	organizovat	k5eAaBmAgFnP
časté	častý	k2eAgFnPc1d1
stávky	stávka	k1gFnPc1
a	a	k8xC
demonstrace	demonstrace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radikalizovalo	radikalizovat	k5eAaBmAgNnS
se	se	k3xPyFc4
také	také	k9
studentské	studentský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
atmosféře	atmosféra	k1gFnSc6
se	se	k3xPyFc4
zradikalizovalo	zradikalizovat	k5eAaPmAgNnS
levicové	levicový	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
Tupamaros	Tupamarosa	k1gFnPc2
<g/>
,	,	kIx,
založené	založený	k2eAgInPc4d1
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tupamaros	Tupamarosa	k1gFnPc2
obdivovali	obdivovat	k5eAaImAgMnP
kubánské	kubánský	k2eAgInPc4d1
revoluční	revoluční	k2eAgInPc4d1
vzory	vzor	k1gInPc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
užívat	užívat	k5eAaImF
novou	nový	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
revolučního	revoluční	k2eAgInSc2d1
boje	boj	k1gInSc2
–	–	k?
městskou	městský	k2eAgFnSc4d1
guerillu	guerilla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
potom	potom	k8xC
Tupamaros	Tupamarosa	k1gFnPc2
změnili	změnit	k5eAaPmAgMnP
taktiku	taktika	k1gFnSc4
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
vyvolat	vyvolat	k5eAaPmF
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
jim	on	k3xPp3gMnPc3
pomohla	pomoct	k5eAaPmAgFnS
svrhnout	svrhnout	k5eAaPmF
stávající	stávající	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násilné	násilný	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
boje	boj	k1gInSc2
(	(	kIx(
<g/>
únosy	únos	k1gInPc4
<g/>
,	,	kIx,
loupeže	loupež	k1gFnPc4
v	v	k7c6
bankách	banka	k1gFnPc6
<g/>
,	,	kIx,
přepadání	přepadání	k1gNnSc1
kasin	kasino	k1gNnPc2
<g/>
)	)	kIx)
Tupamaros	Tupamarosa	k1gFnPc2
vyvolaly	vyvolat	k5eAaPmAgFnP
násilnou	násilný	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
státu	stát	k1gInSc2
respektive	respektive	k9
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
začala	začít	k5eAaPmAgFnS
během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
sehrávat	sehrávat	k5eAaImF
stále	stále	k6eAd1
významnější	významný	k2eAgFnSc4d2
roli	role	k1gFnSc4
a	a	k8xC
podílela	podílet	k5eAaImAgFnS
se	se	k3xPyFc4
také	také	k9
na	na	k7c4
násilním	násilnit	k5eAaImIp1nS
potlačení	potlačení	k1gNnSc3
stávkového	stávkový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
na	na	k7c6
konci	konec	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
nový	nový	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
<g/>
,	,	kIx,
složený	složený	k2eAgInSc4d1
z	z	k7c2
převážně	převážně	k6eAd1
levicových	levicový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
Široká	široký	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
(	(	kIx(
<g/>
Frente	Frent	k1gMnSc5
Amplio	Amplia	k1gMnSc5
<g/>
,	,	kIx,
FA	fa	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
přišla	přijít	k5eAaPmAgFnS
s	s	k7c7
programem	program	k1gInSc7
znárodnění	znárodnění	k1gNnSc1
<g/>
,	,	kIx,
prosazení	prosazení	k1gNnSc1
agrární	agrární	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
a	a	k8xC
kontroly	kontrola	k1gFnSc2
exportu	export	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnPc1
sice	sice	k8xC
vyhráli	vyhrát	k5eAaPmAgMnP
colorados	colorados	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Široká	široký	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
získala	získat	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
18	#num#	k4
<g/>
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
ohrozila	ohrozit	k5eAaPmAgFnS
tak	tak	k6eAd1
dosavadní	dosavadní	k2eAgFnSc4d1
hegemonii	hegemonie	k1gFnSc4
colorados	coloradosa	k1gFnPc2
a	a	k8xC
blancos	blancosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
colorados	coloradosa	k1gFnPc2
potom	potom	k6eAd1
sílil	sílit	k5eAaImAgMnS
vliv	vliv	k1gInSc4
armády	armáda	k1gFnSc2
a	a	k8xC
prezidentem	prezident	k1gMnSc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
stal	stát	k5eAaPmAgMnS
Juan	Juan	k1gMnSc1
María	María	k1gMnSc1
Bordaberra	Bordaberra	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
měl	mít	k5eAaImAgInS
podporu	podpora	k1gFnSc4
armády	armáda	k1gFnSc2
a	a	k8xC
frakce	frakce	k1gFnSc2
autoritativního	autoritativní	k2eAgMnSc2d1
exprezidenta	exprezident	k1gMnSc2
Jorge	Jorg	k1gMnSc2
Batlle	Batll	k1gMnSc2
Pancheca	Panchecus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
1972	#num#	k4
až	až	k9
1984	#num#	k4
</s>
<s>
Nástup	nástup	k1gInSc1
nového	nový	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
a	a	k8xC
první	první	k4xOgInSc4
rok	rok	k1gInSc4
vlády	vláda	k1gFnSc2
colorados	colorados	k1gInSc1
měly	mít	k5eAaImAgFnP
nicméně	nicméně	k8xC
násilný	násilný	k2eAgInSc1d1
kontext	kontext	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
Tupamaros	Tupamarosa	k1gFnPc2
zintenzivněli	zintenzivnět	k5eAaPmAgMnP,k5eAaImAgMnP
svojí	svojit	k5eAaImIp3nP
guerrilovou	guerrilový	k2eAgFnSc4d1
válku	válka	k1gFnSc4
proti	proti	k7c3
uruguayskému	uruguayský	k2eAgInSc3d1
státu	stát	k1gInSc3
a	a	k8xC
establishmentu	establishment	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovědí	odpověď	k1gFnPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
koncepce	koncepce	k1gFnSc1
vnitřní	vnitřní	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
guerra	guerra	k1gFnSc1
interna	interna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
armádní	armádní	k2eAgFnPc1d1
špičky	špička	k1gFnPc1
překřtily	překřtít	k5eAaPmAgFnP
svou	svůj	k3xOyFgFnSc4
strategii	strategie	k1gFnSc4
vůči	vůči	k7c3
Tupamaros	Tupamarosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologicky	ideologicky	k6eAd1
byla	být	k5eAaImAgFnS
vnitřní	vnitřní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
ospravedlněna	ospravedlnit	k5eAaPmNgFnS
nejen	nejen	k6eAd1
potřebou	potřeba	k1gFnSc7
nastolení	nastolení	k1gNnSc2
pořádku	pořádek	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
nebezpečím	nebezpečí	k1gNnSc7
komunismu	komunismus	k1gInSc2
a	a	k8xC
souvisela	souviset	k5eAaImAgFnS
s	s	k7c7
celkovou	celkový	k2eAgFnSc7d1
atmosférou	atmosféra	k1gFnSc7
Studené	Studené	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ofenzívu	ofenzíva	k1gFnSc4
doprovázelo	doprovázet	k5eAaImAgNnS
potlačování	potlačování	k1gNnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
mučení	mučení	k1gNnSc1
i	i	k8xC
masakry	masakr	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Úspěšná	úspěšný	k2eAgFnSc1d1
armádní	armádní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
proti	proti	k7c3
Tupamaros	Tupamarosa	k1gFnPc2
posílala	posílat	k5eAaImAgFnS
její	její	k3xOp3gFnSc4
další	další	k2eAgFnSc4d1
politizaci	politizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
1973	#num#	k4
vypukl	vypuknout	k5eAaPmAgInS
také	také	k9
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
vojenský	vojenský	k2eAgInSc4d1
puč	puč	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokus	pokus	k1gInSc1
prezidenta	prezident	k1gMnSc2
Bordaberry	Bordaberra	k1gMnSc2
se	se	k3xPyFc4
vojákům	voják	k1gMnPc3
s	s	k7c7
podporou	podpora	k1gFnSc7
národa	národ	k1gInSc2
postavit	postavit	k5eAaPmF
vyzněl	vyznít	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c4
prázdno	prázdno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1973	#num#	k4
rezignovaný	rezignovaný	k2eAgInSc1d1
Bordaberra	Bordaberr	k1gMnSc4
rozpustil	rozpustit	k5eAaPmAgInS
parlament	parlament	k1gInSc1
a	a	k8xC
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
ujali	ujmout	k5eAaPmAgMnP
moci	moct	k5eAaImF
s	s	k7c7
politickým	politický	k2eAgNnSc7d1
heslem	heslo	k1gNnSc7
nastolení	nastolení	k1gNnSc2
pořádku	pořádek	k1gInSc6
<g/>
.	.	kIx.
<g/>
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
nastolen	nastolit	k5eAaPmNgInS
za	za	k7c4
cenu	cena	k1gFnSc4
porušování	porušování	k1gNnSc2
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
nastolením	nastolení	k1gNnSc7
diktatury	diktatura	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
paralyzovala	paralyzovat	k5eAaBmAgFnS
politické	politický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
a	a	k8xC
zlikvidovala	zlikvidovat	k5eAaPmAgFnS
mimo	mimo	k7c4
guerilly	guerilla	k1gFnPc4
také	také	k6eAd1
odbory	odbor	k1gInPc1
<g/>
,	,	kIx,
svobodu	svoboda	k1gFnSc4
slova	slovo	k1gNnSc2
a	a	k8xC
nezávislost	nezávislost	k1gFnSc4
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
užívali	užívat	k5eAaImAgMnP
"	"	kIx"
<g/>
osvědčené	osvědčený	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
"	"	kIx"
sousedních	sousední	k2eAgFnPc2d1
diktatur	diktatura	k1gFnPc2
(	(	kIx(
<g/>
Chile	Chile	k1gNnSc1
a	a	k8xC
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
bylo	být	k5eAaImAgNnS
mučení	mučení	k1gNnSc1
při	při	k7c6
výsleších	výslech	k1gInPc6
a	a	k8xC
mizení	mizení	k1gNnSc1
nepohodlných	pohodlný	k2eNgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Bordaberra	Bordaberra	k1gMnSc1
vystoupil	vystoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
plánem	plán	k1gInSc7
na	na	k7c4
reformu	reforma	k1gFnSc4
politického	politický	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
plánu	plán	k1gInSc2
chtěl	chtít	k5eAaImAgMnS
Bordaberra	Bordaberra	k1gMnSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
nahradit	nahradit	k5eAaPmF
starou	starý	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
garnituru	garnitura	k1gFnSc4
a	a	k8xC
politické	politický	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
novou	nový	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
plnou	plný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
armády	armáda	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
by	by	kYmCp3nS
dále	daleko	k6eAd2
zabezpečovala	zabezpečovat	k5eAaImAgFnS
další	další	k2eAgInSc4d1
vliv	vliv	k1gInSc4
armády	armáda	k1gFnSc2
v	v	k7c6
politice	politika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc4
byl	být	k5eAaImAgMnS
nicméně	nicméně	k8xC
vojáky	voják	k1gMnPc4
odmítnut	odmítnut	k2eAgInSc4d1
a	a	k8xC
Bordaberra	Bordaberr	k1gInSc2
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
ve	v	k7c6
funkci	funkce	k1gFnSc6
prezidenta	prezident	k1gMnSc2
Albertem	Albert	k1gMnSc7
Demichellim	Demichellima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
nicméně	nicméně	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
podepsat	podepsat	k5eAaPmF
dekret	dekret	k1gInSc4
o	o	k7c4
pozastavení	pozastavení	k1gNnSc4
činnosti	činnost	k1gFnSc2
všech	všecek	k3xTgFnPc2
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgInS
záhy	záhy	k6eAd1
nahrazen	nahradit	k5eAaPmNgInS
Aparecio	Aparecio	k6eAd1
Mendézem	Mendéz	k1gInSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
podepsal	podepsat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
dekretem	dekret	k1gInSc7
ztratila	ztratit	k5eAaPmAgFnS
vojenská	vojenský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
svou	svůj	k3xOyFgFnSc4
dosavadní	dosavadní	k2eAgFnSc4d1
image	image	k1gFnSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
(	(	kIx(
<g/>
především	především	k9
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
se	se	k3xPyFc4
junta	junta	k1gFnSc1
pokusila	pokusit	k5eAaPmAgFnS
o	o	k7c4
legitimizování	legitimizování	k1gNnSc4
svého	svůj	k3xOyFgInSc2
režimu	režim	k1gInSc2
pomocí	pomocí	k7c2
plebiscitu	plebiscit	k1gInSc2
o	o	k7c6
novém	nový	k2eAgInSc6d1
návrhu	návrh	k1gInSc6
ústavy	ústava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
překvapení	překvapení	k1gNnSc3
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
ale	ale	k9
více	hodně	k6eAd2
než	než	k8xS
57	#num#	k4
<g/>
%	%	kIx~
voličů	volič	k1gMnPc2
vyslovilo	vyslovit	k5eAaPmAgNnS
proti	proti	k7c3
a	a	k8xC
režim	režim	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
do	do	k7c2
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
očividně	očividně	k6eAd1
neměl	mít	k5eNaImAgMnS
lidovou	lidový	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
(	(	kIx(
<g/>
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
počítal	počítat	k5eAaImAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
nutnost	nutnost	k1gFnSc1
hledání	hledání	k1gNnSc2
politického	politický	k2eAgInSc2d1
kompromisu	kompromis	k1gInSc2
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
podtržená	podtržený	k2eAgNnPc4d1
kontextem	kontext	k1gInSc7
hospodářských	hospodářský	k2eAgInPc2d1
neúspěchů	neúspěch	k1gInPc2
vojenské	vojenský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1981	#num#	k4
se	se	k3xPyFc4
úřadu	úřad	k1gInSc6
prezidenta	prezident	k1gMnSc2
Uruguaye	Uruguay	k1gFnSc2
ujal	ujmout	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Gregorio	Gregorio	k1gMnSc1
Álvarez	Álvarez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Přechod	přechod	k1gInSc1
Uruguaye	Uruguay	k1gFnSc2
k	k	k7c3
demokracii	demokracie	k1gFnSc3
trval	trvat	k5eAaImAgInS
celkem	celkem	k6eAd1
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
během	během	k7c2
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
vojáci	voják	k1gMnPc1
postupně	postupně	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
neochotně	ochotně	k6eNd1
<g/>
,	,	kIx,
uvolňovali	uvolňovat	k5eAaImAgMnP
své	svůj	k3xOyFgFnPc4
mocenské	mocenský	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
,	,	kIx,
umožnili	umožnit	k5eAaPmAgMnP
návrat	návrat	k1gInSc4
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
do	do	k7c2
politického	politický	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
uspořádání	uspořádání	k1gNnSc2
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
v	v	k7c6
listopadu	listopad	k1gInSc6
1984	#num#	k4
bez	bez	k7c2
účasti	účast	k1gFnSc2
nejvýznamnějších	významný	k2eAgMnPc2d3
lídrů	lídr	k1gMnPc2
blancos	blancos	k1gMnSc1
<g/>
,	,	kIx,
colorados	colorados	k1gMnSc1
a	a	k8xC
levicové	levicový	k2eAgFnPc1d1
Široké	Široké	k2eAgFnPc1d1
fronty	fronta	k1gFnPc1
(	(	kIx(
<g/>
Frente	Frent	k1gMnSc5
Amplio	Amplia	k1gMnSc5
<g/>
,	,	kIx,
FA	fa	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
byli	být	k5eAaImAgMnP
končícím	končící	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
režimem	režim	k1gInSc7
preventivně	preventivně	k6eAd1
uvězněni	uvěznit	k5eAaPmNgMnP
a	a	k8xC
propuštěni	propustit	k5eAaPmNgMnP
až	až	k6eAd1
po	po	k7c6
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnSc2
vyhráli	vyhrát	k5eAaPmAgMnP
colorados	colorados	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
umístili	umístit	k5eAaPmAgMnP
blancos	blancos	k1gMnSc1
a	a	k8xC
třetí	třetí	k4xOgMnSc1
s	s	k7c7
22	#num#	k4
<g/>
%	%	kIx~
Široká	široký	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současná	současný	k2eAgFnSc1d1
Uruguay	Uruguay	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
se	se	k3xPyFc4
prezidentem	prezident	k1gMnSc7
republiky	republika	k1gFnSc2
stal	stát	k5eAaPmAgMnS
colorado	colorada	k1gFnSc5
Julio	Julio	k1gNnSc1
María	Marí	k2eAgFnSc1d1
Sanguinetti	Sanguinetti	k1gNnSc7
<g/>
,	,	kIx,
před	před	k7c7
jehož	jehož	k3xOyRp3gFnSc7
vládou	vláda	k1gFnSc7
stála	stát	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
politických	politický	k2eAgInPc2d1
a	a	k8xC
hospodářských	hospodářský	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
politiky	politika	k1gFnSc2
šlo	jít	k5eAaImAgNnS
například	například	k6eAd1
o	o	k7c4
neochotu	neochota	k1gFnSc4
generálů	generál	k1gMnPc2
dopustit	dopustit	k5eAaPmF
soudní	soudní	k2eAgInPc4d1
procesy	proces	k1gInPc4
spojené	spojený	k2eAgInPc4d1
se	s	k7c7
státním	státní	k2eAgInSc7d1
terorismem	terorismus	k1gInSc7
během	během	k7c2
jejich	jejich	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
problémem	problém	k1gInSc7
byla	být	k5eAaImAgFnS
otázka	otázka	k1gFnSc1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
jako	jako	k8xC,k8xS
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
tzn.	tzn.	kA
budoucí	budoucí	k2eAgFnSc1d1
regulace	regulace	k1gFnSc1
nebo	nebo	k8xC
platforma	platforma	k1gFnSc1
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
armádou	armáda	k1gFnSc7
a	a	k8xC
vládou	vláda	k1gFnSc7
a	a	k8xC
role	role	k1gFnSc1
armády	armáda	k1gFnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
a	a	k8xC
politice	politika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
byla	být	k5eAaImAgFnS
nová	nový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
oddaná	oddaný	k2eAgFnSc1d1
myšlence	myšlenka	k1gFnSc3
úsporných	úsporný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
ale	ale	k8xC
zohledňovaly	zohledňovat	k5eAaImAgFnP
otázky	otázka	k1gFnPc1
sociální	sociální	k2eAgFnSc2d1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
privátní	privátní	k2eAgInSc4d1
sektor	sektor	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
odporu	odpor	k1gInSc2
k	k	k7c3
některým	některý	k3yIgFnPc3
státním	státní	k2eAgFnPc3d1
intervencím	intervence	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Součástí	součást	k1gFnSc7
uzavřeného	uzavřený	k2eAgInSc2d1
kompromisu	kompromis	k1gInSc2
z	z	k7c2
Námořního	námořní	k2eAgInSc2d1
klubu	klub	k1gInSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
amnestie	amnestie	k1gFnSc1
pro	pro	k7c4
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgInS
zákon	zákon	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
projednáván	projednávat	k5eAaImNgInS
<g/>
,	,	kIx,
zvednula	zvednout	k5eAaPmAgFnS
se	se	k3xPyFc4
proti	proti	k7c3
němu	on	k3xPp3gNnSc3
vlna	vlna	k1gFnSc1
nevole	nevole	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
v	v	k7c6
plebiscitu	plebiscit	k1gInSc6
schválen	schválit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnestie	amnestie	k1gFnSc1
se	se	k3xPyFc4
týkala	týkat	k5eAaImAgFnS
také	také	k9
Tupamaros	Tupamarosa	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
posléze	posléze	k6eAd1
zlegalizováni	zlegalizován	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uruguay	Uruguay	k1gFnSc1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
preferovala	preferovat	k5eAaImAgFnS
cestu	cesta	k1gFnSc4
usmíření	usmíření	k1gNnSc2
a	a	k8xC
odpuštění	odpuštění	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
garantovat	garantovat	k5eAaBmF
politickou	politický	k2eAgFnSc4d1
stabilitu	stabilita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
status	status	k1gInSc4
quo	quo	k?
byl	být	k5eAaImAgInS
porušen	porušit	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
prezidentem	prezident	k1gMnSc7
Jorge	Jorg	k1gFnSc2
Batllem	Batll	k1gMnSc7
(	(	kIx(
<g/>
colorados	colorados	k1gMnSc1
<g/>
)	)	kIx)
založena	založen	k2eAgFnSc1d1
„	„	k?
<g/>
Komise	komise	k1gFnSc1
pro	pro	k7c4
mír	mír	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
Comisión	Comisión	k1gInSc4
para	para	k2eAgNnSc2d1
la	la	k1gNnSc2
Paz	Paz	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
zabývala	zabývat	k5eAaImAgFnS
mapováním	mapování	k1gNnSc7
a	a	k8xC
shromažďováním	shromažďování	k1gNnSc7
svědectví	svědectví	k1gNnSc2
o	o	k7c6
zločinech	zločin	k1gInPc6
vojenské	vojenský	k2eAgFnSc2d1
junty	junta	k1gFnSc2
mezi	mezi	k7c7
léty	léto	k1gNnPc7
1973	#num#	k4
až	až	k8xS
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Uruguaye	Uruguay	k1gFnSc2
Jorge	Jorg	k1gMnSc2
Batlle	Batll	k1gMnSc2
s	s	k7c7
bývalým	bývalý	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
USA	USA	kA
Georgem	Georg	k1gMnSc7
H.	H.	kA
W.	W.	kA
Bushem	Bush	k1gMnSc7
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
návštěvě	návštěva	k1gFnSc6
Uruguaye	Uruguay	k1gFnSc2
</s>
<s>
V	v	k7c6
dalších	další	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
se	s	k7c7
prezidentem	prezident	k1gMnSc7
stal	stát	k5eAaPmAgMnS
politik	politik	k1gMnSc1
blancos	blancos	k1gMnSc1
Luis	Luisa	k1gFnPc2
Alberto	Alberta	k1gFnSc5
Lacalle	Lacalle	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
levicové	levicový	k2eAgFnSc2d1
Široké	Široké	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
(	(	kIx(
<g/>
Frente	Frent	k1gMnSc5
Amplio	Amplia	k1gMnSc5
<g/>
,	,	kIx,
FA	fa	kA
<g/>
)	)	kIx)
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
přinutil	přinutit	k5eAaPmAgInS
oba	dva	k4xCgMnPc4
tradiční	tradiční	k2eAgMnPc4d1
rivaly	rival	k1gMnPc4
blancos	blancosa	k1gFnPc2
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
Nacional	Nacional	k1gMnPc5
<g/>
)	)	kIx)
a	a	k8xC
colorados	colorados	k1gMnSc1
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
Colorado	Colorado	k1gNnSc1
<g/>
)	)	kIx)
ke	k	k7c3
koaliční	koaliční	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejná	stejný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
se	se	k3xPyFc4
opakovala	opakovat	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
prezidentského	prezidentský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
vrátil	vrátit	k5eAaPmAgInS
Sanguinetti	Sanguinetti	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgInSc1d1
monopol	monopol	k1gInSc1
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
politických	politický	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
byl	být	k5eAaImAgInS
nakonec	nakonec	k6eAd1
přerušen	přerušit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
prezidentem	prezident	k1gMnSc7
poprvé	poprvé	k6eAd1
zvolen	zvolen	k2eAgMnSc1d1
kandidát	kandidát	k1gMnSc1
Široké	Široké	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
Tabaré	Tabarý	k2eAgFnSc2d1
Vázquez	Vázquez	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
ho	on	k3xPp3gMnSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
nahradil	nahradit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
stranický	stranický	k2eAgMnSc1d1
kolega	kolega	k1gMnSc1
José	José	k1gNnSc2
Mujica	Mujica	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
guerrillero	guerrillero	k1gNnSc1
a	a	k8xC
člen	člen	k1gInSc1
Tupamaros	Tupamarosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příklon	příklon	k1gInSc4
k	k	k7c3
levici	levice	k1gFnSc3
(	(	kIx(
<g/>
respektive	respektive	k9
středolevici	středolevice	k1gFnSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
celkově	celkově	k6eAd1
špatnou	špatný	k2eAgFnSc7d1
ekonomickou	ekonomický	k2eAgFnSc7d1
a	a	k8xC
sociální	sociální	k2eAgFnSc7d1
situací	situace	k1gFnSc7
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
ekonomika	ekonomika	k1gFnSc1
v	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
po	po	k7c6
návratu	návrat	k1gInSc6
k	k	k7c3
demokracii	demokracie	k1gFnSc3
rostla	růst	k5eAaImAgFnS
<g/>
,	,	kIx,
země	země	k1gFnSc1
zdědila	zdědit	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
problémů	problém	k1gInPc2
včetně	včetně	k7c2
vysokého	vysoký	k2eAgNnSc2d1
zahraničního	zahraniční	k2eAgNnSc2d1
zadlužení	zadlužení	k1gNnSc2
a	a	k8xC
inflace	inflace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezprostřední	bezprostřední	k2eAgInPc1d1
dopady	dopad	k1gInPc1
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
postihly	postihnout	k5eAaPmAgInP
oba	dva	k4xCgMnPc1
sousedy	soused	k1gMnPc7
Uruguaye	Uruguay	k1gFnSc2
<g/>
,	,	kIx,
Brazílii	Brazílie	k1gFnSc6
a	a	k8xC
především	především	k9
Argentinu	Argentina	k1gFnSc4
<g/>
,	,	kIx,
hrály	hrát	k5eAaImAgInP
taktéž	taktéž	k?
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
odklonu	odklon	k1gInSc6
voličů	volič	k1gMnPc2
od	od	k7c2
tradičních	tradiční	k2eAgFnPc2d1
stran	strana	k1gFnPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
zpochybnila	zpochybnit	k5eAaPmAgFnS
důvěru	důvěra	k1gFnSc4
voličů	volič	k1gInPc2
v	v	k7c4
neoliberální	neoliberální	k2eAgMnPc4d1
politiky	politik	k1gMnPc4
privatizace	privatizace	k1gFnSc2
a	a	k8xC
deregulace	deregulace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
hlavní	hlavní	k2eAgInPc1d1
ekonomickou	ekonomický	k2eAgFnSc4d1
strategii	strategie	k1gFnSc4
v	v	k7c6
celém	celý	k2eAgInSc6d1
regionu	region	k1gInSc6
i	i	k8xC
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
vypukla	vypuknout	k5eAaPmAgFnS
v	v	k7c6
Uruguayi	Uruguay	k1gFnSc6
také	také	k9
vážná	vážný	k2eAgFnSc1d1
krize	krize	k1gFnSc1
bankovního	bankovní	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
krizi	krize	k1gFnSc4
v	v	k7c6
Argentině	Argentina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nárůstu	nárůst	k1gInSc3
nezaměstnanosti	nezaměstnanost	k1gFnSc2
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
20	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
devalvaci	devalvace	k1gFnSc4
uruguayského	uruguayský	k2eAgNnSc2d1
pesa	peso	k1gNnSc2
a	a	k8xC
k	k	k7c3
propadu	propad	k1gInSc3
reálných	reálný	k2eAgFnPc2d1
mezd	mzda	k1gFnPc2
<g/>
,	,	kIx,
mající	mající	k2eAgInPc4d1
negativní	negativní	k2eAgInPc4d1
sociální	sociální	k2eAgInPc4d1
dopady	dopad	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Situaci	situace	k1gFnSc4
ještě	ještě	k6eAd1
zhoršila	zhoršit	k5eAaPmAgFnS
epidemie	epidemie	k1gFnSc1
slintavky	slintavka	k1gFnSc2
a	a	k8xC
kulhavky	kulhavka	k1gFnSc2
mezi	mezi	k7c7
uruguayským	uruguayský	k2eAgInSc7d1
hovězím	hovězí	k2eAgInSc7d1
dobytkem	dobytek	k1gInSc7
<g/>
,	,	kIx,
významnou	významný	k2eAgFnSc7d1
exportní	exportní	k2eAgFnSc7d1
surovinou	surovina	k1gFnSc7
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
Široké	Široké	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
se	se	k3xPyFc4
proto	proto	k8xC
musela	muset	k5eAaImAgFnS
zaměřit	zaměřit	k5eAaPmF
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
na	na	k7c4
zmírnění	zmírnění	k1gNnSc4
sociálních	sociální	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
krize	krize	k1gFnSc2
(	(	kIx(
<g/>
nezaměstnanost	nezaměstnanost	k1gFnSc4
a	a	k8xC
chybějící	chybějící	k2eAgInPc4d1
byty	byt	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
ekonomického	ekonomický	k2eAgNnSc2d1
oživení	oživení	k1gNnSc2
a	a	k8xC
splácení	splácení	k1gNnSc2
zahraničního	zahraniční	k2eAgInSc2d1
dluhu	dluh	k1gInSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
je	být	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
Uruguaye	Uruguay	k1gFnSc2
opět	opět	k6eAd1
Tabaré	Tabarý	k2eAgInPc1d1
Vázquez	Vázquez	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
tento	tento	k3xDgInSc1
úřad	úřad	k1gInSc1
zastával	zastávat	k5eAaImAgInS
již	již	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
2005	#num#	k4
až	až	k9
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jermyn	Jermyn	k1gMnSc1
<g/>
,	,	kIx,
Leslie-Wong	Leslie-Wong	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Winnie	Winnie	k1gFnSc1
<g/>
,	,	kIx,
Uruguay	Uruguay	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gMnSc1
Cavendish	Cavendish	k1gMnSc1
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chalupa	Chalupa	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NLN	NLN	kA
Praha	Praha	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
279	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Winn	Winn	k1gInSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
:	:	kIx,
British	British	k1gMnSc1
Informal	Informal	k1gMnSc1
Empire	empir	k1gInSc5
in	in	k?
Uruguay	Uruguay	k1gFnSc1
in	in	k?
the	the	k?
Nineteen	Nineteen	k1gInSc4
Century	Centura	k1gFnSc2
<g/>
,	,	kIx,
in	in	k?
Past	past	k1gFnSc1
and	and	k?
Present	Present	k1gInSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
;	;	kIx,
73	#num#	k4
<g/>
:	:	kIx,
100	#num#	k4
<g/>
-	-	kIx~
<g/>
126	#num#	k4
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc1
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
301	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Blakewell	Blakewell	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Peter	Peter	k1gMnSc1
John	John	k1gMnSc1
<g/>
,	,	kIx,
History	Histor	k1gInPc1
of	of	k?
Latin	latina	k1gFnPc2
America	America	k1gFnSc1
<g/>
:	:	kIx,
c.	c.	k?
1450	#num#	k4
to	ten	k3xDgNnSc1
the	the	k?
present	present	k1gInSc1
<g/>
,	,	kIx,
Wiley	Wiley	k1gInPc1
Blackwell	Blackwella	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
447	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Více	hodně	k6eAd2
např.	např.	kA
Drake	Drake	k1gNnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
W.	W.	kA
<g/>
,	,	kIx,
Between	Between	k1gInSc1
Tyranny	Tyranna	k1gFnSc2
and	and	k?
Anarchy	Anarcha	k1gFnSc2
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Democracy	Democraca	k1gFnSc2
in	in	k?
Latin	Latin	k1gMnSc1
America	America	k1gMnSc1
<g/>
,	,	kIx,
1800	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanford	Stanford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanford	Stanford	k1gInSc1
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
126	#num#	k4
<g/>
–	–	k?
<g/>
162	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Yaffé	Yaffá	k1gFnSc2
<g/>
,	,	kIx,
Jaime	Jaim	k1gMnSc5
<g/>
,	,	kIx,
Ideas	Ideas	k1gInSc1
<g/>
,	,	kIx,
Programa	Programa	k1gNnSc1
y	y	k?
Politica	Politica	k1gMnSc1
Economica	Economica	k1gMnSc1
del	del	k?
Batllismo	Batllisma	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uruguay	Uruguay	k1gFnSc1
1911	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
,	,	kIx,
pdf	pdf	k?
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bertola	Bertola	k1gFnSc1
<g/>
,	,	kIx,
Luis	Luisa	k1gFnPc2
<g/>
,	,	kIx,
An	An	k1gMnSc1
Overview	Overview	k1gFnSc2
of	of	k?
the	the	k?
Economic	Economic	k1gMnSc1
History	Histor	k1gMnPc4
of	of	k?
Uruguay	Uruguay	k1gFnSc1
since	since	k1gFnSc1
the	the	k?
1870	#num#	k4
<g/>
s	s	k7c7
<g/>
,	,	kIx,
Universidad	Universidad	k1gInSc1
de	de	k?
Uruguay	Uruguay	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
přístupné	přístupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
s.	s.	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc4
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
317	#num#	k4
<g/>
–	–	k?
<g/>
318	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc4
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
341	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
Konečná	konečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Komise	komise	k1gFnSc2
míru	mír	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
23.7	23.7	k4
<g/>
.2010	.2010	k4
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc4
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
<g/>
348	#num#	k4
<g/>
–	–	k?
<g/>
349	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gillespie	Gillespie	k1gFnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
<g/>
,	,	kIx,
Negotiating	Negotiating	k1gInSc1
Democracy	Democraca	k1gFnSc2
<g/>
:	:	kIx,
Politicians	Politicians	k1gInSc1
and	and	k?
Generals	Generals	k1gInSc1
in	in	k?
Uruguay	Uruguay	k1gFnSc1
<g/>
,	,	kIx,
Cambrigde	Cambrigd	k1gMnSc5
Un	Un	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
1991	#num#	k4
<g/>
,	,	kIx,
<g/>
s.	s.	k?
<g/>
217	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
OSN	OSN	kA
<g/>
:	:	kIx,
Statistické	statistický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
o	o	k7c6
Uruguayi	Uruguay	k1gFnSc6
23.7	23.7	k4
<g/>
.2010	.2010	k4
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GILLESPIE	GILLESPIE	kA
<g/>
,	,	kIx,
CHARLES	Charles	k1gMnSc1
<g/>
,	,	kIx,
Negotiating	Negotiating	k1gInSc1
Democracy	Democraca	k1gFnSc2
<g/>
:	:	kIx,
Politicians	Politicians	k1gInSc1
and	and	k?
Generals	Generals	k1gInSc1
in	in	k?
Uruguay	Uruguay	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
Un	Un	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
40152	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CHALUPA	Chalupa	k1gMnSc1
<g/>
,	,	kIx,
JIŘÍ	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
Argentiny	Argentina	k1gFnSc2
<g/>
,	,	kIx,
Uruguaye	Uruguay	k1gFnSc2
a	a	k8xC
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NLN	NLN	kA
Praha	Praha	k1gFnSc1
1999	#num#	k4
<g/>
.	.	kIx.
<g/>
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
323	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JERMYN	JERMYN	kA
LESLIE	LESLIE	kA
<g/>
,	,	kIx,
WONG	WONG	kA
<g/>
,	,	kIx,
WINNIE	WINNIE	kA
<g/>
,	,	kIx,
Uruguay	Uruguay	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marshall	Marshall	k1gMnSc1
Cavendish	Cavendish	k1gMnSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7614	#num#	k4
<g/>
-	-	kIx~
<g/>
4482	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Argentiny	Argentina	k1gFnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dějiny	dějiny	k1gFnPc1
Uruguaye	Uruguay	k1gFnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dějiny	dějiny	k1gFnPc1
amerických	americký	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Země	zem	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
</s>
<s>
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Země	zem	k1gFnSc2
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
</s>
<s>
Belize	Belize	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Kostarika	Kostarika	k1gFnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Panama	Panama	k1gFnSc1
•	•	k?
Salvador	Salvador	k1gInSc4
Země	zem	k1gFnSc2
Karibské	karibský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gInSc1
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Haiti	Haiti	k1gNnSc2
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kuba	Kuba	k1gMnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
Země	zem	k1gFnSc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
</s>
<s>
Argentina	Argentina	k1gFnSc1
•	•	k?
Bolívie	Bolívie	k1gFnSc2
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
Paraguay	Paraguay	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Surinam	Surinam	k1gInSc1
•	•	k?
Uruguay	Uruguay	k1gFnSc1
•	•	k?
Venezuela	Venezuela	k1gFnSc1
Teritoria	teritorium	k1gNnPc1
<g/>
,	,	kIx,
koloniea	kolonie	k2eAgNnPc1d1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Americké	americký	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Anguilla	Anguilla	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Aruba	Aruba	k1gMnSc1
(	(	kIx(
<g/>
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Bermudy	Bermudy	k1gFnPc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Curaçao	curaçao	k1gNnSc4
(	(	kIx(
<g/>
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Falklandy	Falklanda	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Francouzská	francouzský	k2eAgFnSc1d1
Guyana	Guyana	k1gFnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Grónsko	Grónsko	k1gNnSc4
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
Guadeloupe	Guadeloupe	k1gMnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Karibské	karibský	k2eAgNnSc1d1
Nizozemsko	Nizozemsko	k1gNnSc1
(	(	kIx(
<g/>
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Martinik	Martinik	k1gMnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Montserrat	Montserrat	k1gInSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Portoriko	Portoriko	k1gNnSc4
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Saint	Saint	k1gInSc1
Pierre	Pierr	k1gInSc5
a	a	k8xC
Miquelon	Miquelon	k1gInSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Saint-Martin	Saint-Martin	k1gMnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Saint-Barthelémy	Saint-Barthelém	k1gInPc4
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Sint	Sint	k1gMnSc1
Maarten	Maarten	k2eAgMnSc1d1
(	(	kIx(
<g/>
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Turks	Turks	k1gInSc1
a	a	k8xC
Caicos	Caicos	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
</s>
