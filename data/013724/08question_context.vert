<s desamb="1">
Asi	asi	k9
před	před	k7c7
2000	#num#	k4
lety	léto	k1gNnPc7
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
objevily	objevit	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
odlišné	odlišný	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
domorodého	domorodý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
:	:	kIx,
Chaná	Chaná	k1gFnSc1
<g/>
,	,	kIx,
Guarání	Guarání	k1gNnSc1
a	a	k8xC
Charrúa	Charrúa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Chaná	Chanat	k5eAaPmIp3nS
se	se	k3xPyFc4
zabývali	zabývat	k5eAaImAgMnP
především	především	k6eAd1
rybolovem	rybolov	k1gInSc7
a	a	k8xC
lovem	lov	k1gInSc7
a	a	k8xC
žili	žít	k5eAaImAgMnP
nomádským	nomádský	k2eAgInSc7d1
životem	život	k1gInSc7
v	v	k7c6
malých	malý	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
existujících	existující	k2eAgInPc2d1
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
se	se	k3xPyFc4
první	první	k4xOgMnPc1
lidé	člověk	k1gMnPc1
na	na	k7c6
území	území	k1gNnSc6
Uruguaye	Uruguay	k1gFnSc2
objevili	objevit	k5eAaPmAgMnP
asi	asi	k9
před	před	k7c7
11	#num#	k4
000	#num#	k4
až	až	k9
8000	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>