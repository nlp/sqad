<s>
Mlhovinová	Mlhovinový	k2eAgFnSc1d1	Mlhovinový
hypotéza	hypotéza	k1gFnSc1	hypotéza
(	(	kIx(	(
<g/>
též	též	k9	též
nebulární	nebulární	k2eAgFnSc1d1	nebulární
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
hypotézou	hypotéza	k1gFnSc7	hypotéza
vysvětlující	vysvětlující	k2eAgInSc4d1	vysvětlující
vznik	vznik	k1gInSc4	vznik
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
planetárních	planetární	k2eAgFnPc2d1	planetární
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
