<p>
<s>
Mlhovinová	Mlhovinový	k2eAgFnSc1d1	Mlhovinový
hypotéza	hypotéza	k1gFnSc1	hypotéza
(	(	kIx(	(
<g/>
též	též	k9	též
nebulární	nebulární	k2eAgFnSc1d1	nebulární
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
hypotézou	hypotéza	k1gFnSc7	hypotéza
vysvětlující	vysvětlující	k2eAgInSc4d1	vysvětlující
vznik	vznik	k1gInSc4	vznik
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
planetárních	planetární	k2eAgFnPc2d1	planetární
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgNnP	být
navržena	navrhnout	k5eAaPmNgNnP	navrhnout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1734	[number]	k4	1734
švédským	švédský	k2eAgMnSc7d1	švédský
vědcem	vědec	k1gMnSc7	vědec
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Swedenborgem	Swedenborg	k1gMnSc7	Swedenborg
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vznik	vznik	k1gInSc4	vznik
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
rotujících	rotující	k2eAgFnPc2d1	rotující
soustav	soustava	k1gFnPc2	soustava
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
formulace	formulace	k1gFnSc2	formulace
hypotéza	hypotéza	k1gFnSc1	hypotéza
prodělala	prodělat	k5eAaPmAgFnS	prodělat
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
planetární	planetární	k2eAgFnPc1d1	planetární
soustavy	soustava	k1gFnPc1	soustava
vznikají	vznikat	k5eAaImIp3nP	vznikat
kolapsem	kolaps	k1gInSc7	kolaps
obrovských	obrovský	k2eAgFnPc2d1	obrovská
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
mračen	mračna	k1gFnPc2	mračna
složených	složený	k2eAgFnPc2d1	složená
především	především	k9	především
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
mračna	mračno	k1gNnPc1	mračno
jsou	být	k5eAaImIp3nP	být
gravitačně	gravitačně	k6eAd1	gravitačně
velmi	velmi	k6eAd1	velmi
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
hmota	hmota	k1gFnSc1	hmota
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
obsažená	obsažený	k2eAgFnSc1d1	obsažená
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
do	do	k7c2	do
hustších	hustý	k2eAgInPc2d2	hustší
chomáčů	chomáč	k1gInPc2	chomáč
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dále	daleko	k6eAd2	daleko
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
gravitačním	gravitační	k2eAgInSc6d1	gravitační
kolapsu	kolaps	k1gInSc6	kolaps
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc1d1	složitý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
během	během	k7c2	během
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
plynný	plynný	k2eAgInSc1d1	plynný
protoplanetární	protoplanetární	k2eAgInSc1d1	protoplanetární
disk	disk	k1gInSc1	disk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
mohou	moct	k5eAaImIp3nP	moct
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
vzniknout	vzniknout	k5eAaPmF	vzniknout
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
planetárních	planetární	k2eAgInPc2d1	planetární
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zřejmě	zřejmě	k6eAd1	zřejmě
přirozeným	přirozený	k2eAgInSc7d1	přirozený
důsledkem	důsledek	k1gInSc7	důsledek
tvorby	tvorba	k1gFnSc2	tvorba
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
hvězdy	hvězda	k1gFnSc2	hvězda
velikosti	velikost	k1gFnSc2	velikost
Slunce	slunce	k1gNnSc2	slunce
obvykle	obvykle	k6eAd1	obvykle
trvá	trvat	k5eAaImIp3nS	trvat
kolem	kolem	k7c2	kolem
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Protoplanetární	Protoplanetární	k2eAgInSc1d1	Protoplanetární
disk	disk	k1gInSc1	disk
je	být	k5eAaImIp3nS	být
akreční	akreční	k2eAgInSc4d1	akreční
disk	disk	k1gInSc4	disk
kolem	kolem	k7c2	kolem
mladé	mladý	k2eAgFnSc2d1	mladá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dále	daleko	k6eAd2	daleko
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
hmotou	hmota	k1gFnSc7	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátečních	počáteční	k2eAgFnPc6d1	počáteční
fázích	fáze	k1gFnPc6	fáze
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
horký	horký	k2eAgMnSc1d1	horký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
chladne	chladnout	k5eAaImIp3nS	chladnout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
centrální	centrální	k2eAgFnSc1d1	centrální
hvězda	hvězda	k1gFnSc1	hvězda
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Tauri	k1gNnSc7	Tauri
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akrečním	akreční	k2eAgInSc6d1	akreční
disku	disk	k1gInSc6	disk
již	již	k6eAd1	již
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
malá	malý	k2eAgNnPc4d1	malé
prachová	prachový	k2eAgNnPc4d1	prachové
zrna	zrno	k1gNnPc4	zrno
tvořená	tvořený	k2eAgFnSc1d1	tvořená
minerály	minerál	k1gInPc7	minerál
a	a	k8xC	a
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
spojovat	spojovat	k5eAaImF	spojovat
až	až	k9	až
do	do	k7c2	do
1	[number]	k4	1
kilometr	kilometr	k1gInSc1	kilometr
velkých	velký	k2eAgFnPc2d1	velká
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
disk	disk	k1gInSc4	disk
dostatečně	dostatečně	k6eAd1	dostatečně
hmotný	hmotný	k2eAgInSc4d1	hmotný
<g/>
,	,	kIx,	,
akrece	akrece	k1gFnSc1	akrece
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
poměrně	poměrně	k6eAd1	poměrně
překotným	překotný	k2eAgInSc7d1	překotný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
během	běh	k1gInSc7	běh
100	[number]	k4	100
000	[number]	k4	000
až	až	k9	až
300	[number]	k4	300
000	[number]	k4	000
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
protoplanety	protoplanet	k1gInPc1	protoplanet
velikosti	velikost	k1gFnSc2	velikost
Měsíce	měsíc	k1gInSc2	měsíc
až	až	k8xS	až
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hvězdy	hvězda	k1gFnSc2	hvězda
tato	tento	k3xDgNnPc1	tento
planetární	planetární	k2eAgNnPc1d1	planetární
embrya	embryo	k1gNnPc1	embryo
prochází	procházet	k5eAaImIp3nP	procházet
fází	fáze	k1gFnSc7	fáze
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
až	až	k6eAd1	až
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Tvorba	tvorba	k1gFnSc1	tvorba
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
je	být	k5eAaImIp3nS	být
komplikovanější	komplikovaný	k2eAgInSc1d2	komplikovanější
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
za	za	k7c7	za
tzv.	tzv.	kA	tzv.
sněžnou	sněžný	k2eAgFnSc7d1	sněžná
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
protoplanety	protoplanet	k1gInPc1	protoplanet
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
především	především	k9	především
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
zmrzlých	zmrzlý	k2eAgInPc2d1	zmrzlý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
několikrát	několikrát	k6eAd1	několikrát
hmotnější	hmotný	k2eAgInPc1d2	hmotnější
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
proces	proces	k1gInSc1	proces
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
embryí	embryo	k1gNnPc2	embryo
mohou	moct	k5eAaImIp3nP	moct
nakonec	nakonec	k6eAd1	nakonec
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hmotnosti	hmotnost	k1gFnSc3	hmotnost
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
Zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
hraniční	hraniční	k2eAgFnSc1d1	hraniční
hodnota	hodnota	k1gFnSc1	hodnota
umožňující	umožňující	k2eAgNnSc1d1	umožňující
zachycování	zachycování	k1gNnSc1	zachycování
plynu	plyn	k1gInSc2	plyn
sestávajícího	sestávající	k2eAgInSc2d1	sestávající
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Akumulace	akumulace	k1gFnSc1	akumulace
plynu	plyn	k1gInSc2	plyn
kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgNnSc2	tento
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
pomalý	pomalý	k2eAgInSc1d1	pomalý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
však	však	k9	však
protoplaneta	protoplaneta	k1gFnSc1	protoplaneta
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
hmotnosti	hmotnost	k1gFnSc2	hmotnost
asi	asi	k9	asi
30	[number]	k4	30
Zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
nabere	nabrat	k5eAaPmIp3nS	nabrat
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
a	a	k8xC	a
překotnosti	překotnost	k1gFnSc6	překotnost
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
velikosti	velikost	k1gFnSc2	velikost
Jupiteru	Jupiter	k1gInSc2	Jupiter
či	či	k8xC	či
Saturnu	Saturn	k1gInSc2	Saturn
zřejmě	zřejmě	k6eAd1	zřejmě
dokáží	dokázat	k5eAaPmIp3nP	dokázat
nashromáždit	nashromáždit	k5eAaPmF	nashromáždit
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
zbývající	zbývající	k2eAgFnSc4d1	zbývající
hmotu	hmota	k1gFnSc4	hmota
během	během	k7c2	během
pouhých	pouhý	k2eAgNnPc2d1	pouhé
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Akrece	akrece	k1gFnSc1	akrece
končí	končit	k5eAaImIp3nS	končit
teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
až	až	k9	až
je	být	k5eAaImIp3nS	být
veškerý	veškerý	k3xTgInSc4	veškerý
plyn	plyn	k1gInSc4	plyn
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vyčerpán	vyčerpat	k5eAaPmNgMnS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
planety	planeta	k1gFnPc1	planeta
potom	potom	k6eAd1	potom
mohou	moct	k5eAaImIp3nP	moct
migrovat	migrovat	k5eAaImF	migrovat
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Ledoví	ledový	k2eAgMnPc1d1	ledový
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Uran	Uran	k1gInSc4	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc4	Neptun
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nedostatečně	dostatečně	k6eNd1	dostatečně
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
tvořit	tvořit	k5eAaImF	tvořit
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
v	v	k7c6	v
disku	disk	k1gInSc6	disk
nebyl	být	k5eNaImAgInS	být
dostatek	dostatek	k1gInSc1	dostatek
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Mlhovinová	Mlhovinový	k2eAgFnSc1d1	Mlhovinový
hypotéza	hypotéza	k1gFnSc1	hypotéza
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
formulována	formulován	k2eAgFnSc1d1	formulována
roku	rok	k1gInSc2	rok
1734	[number]	k4	1734
švédským	švédský	k2eAgMnSc7d1	švédský
vědcem	vědec	k1gMnSc7	vědec
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Swedenborgem	Swedenborg	k1gMnSc7	Swedenborg
<g/>
.	.	kIx.	.
</s>
<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
myšlenkou	myšlenka	k1gFnSc7	myšlenka
podrobně	podrobně	k6eAd1	podrobně
obeznámen	obeznámit	k5eAaPmNgMnS	obeznámit
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
dále	daleko	k6eAd2	daleko
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
pomalu	pomalu	k6eAd1	pomalu
rotující	rotující	k2eAgFnSc1d1	rotující
plynná	plynný	k2eAgFnSc1d1	plynná
mračna	mračna	k1gFnSc1	mračna
(	(	kIx(	(
<g/>
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
)	)	kIx)	)
vlivem	vliv	k1gInSc7	vliv
gravitace	gravitace	k1gFnSc2	gravitace
kolabují	kolabovat	k5eAaImIp3nP	kolabovat
a	a	k8xC	a
zplošťují	zplošťovat	k5eAaImIp3nP	zplošťovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nakonec	nakonec	k6eAd1	nakonec
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
model	model	k1gInSc1	model
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
také	také	k9	také
roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
Pierre	Pierr	k1gInSc5	Pierr
Simon	Simon	k1gMnSc1	Simon
de	de	k?	de
Laplace	Laplace	k1gFnSc2	Laplace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
model	model	k1gInSc4	model
popisoval	popisovat	k5eAaImAgMnS	popisovat
smršťující	smršťující	k2eAgMnSc1d1	smršťující
se	se	k3xPyFc4	se
a	a	k8xC	a
chladnoucí	chladnoucí	k2eAgInSc1d1	chladnoucí
protosolární	protosolární	k2eAgInSc1d1	protosolární
mrak	mrak	k1gInSc1	mrak
–	–	k?	–
sluneční	sluneční	k2eAgFnSc4d1	sluneční
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mlhovina	mlhovina	k1gFnSc1	mlhovina
smršťovala	smršťovat	k5eAaImAgFnS	smršťovat
<g/>
,	,	kIx,	,
zplošťovala	zplošťovat	k5eAaImAgFnS	zplošťovat
se	se	k3xPyFc4	se
a	a	k8xC	a
tvořily	tvořit	k5eAaImAgFnP	tvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
prstence	prstenec	k1gInPc4	prstenec
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dále	daleko	k6eAd2	daleko
kolabovaly	kolabovat	k5eAaImAgFnP	kolabovat
až	až	k8xS	až
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Laplaceův	Laplaceův	k2eAgInSc1d1	Laplaceův
model	model	k1gInSc1	model
převládal	převládat	k5eAaImAgInS	převládat
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
mezi	mezi	k7c7	mezi
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
planetami	planeta	k1gFnPc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
99,86	[number]	k4	99,86
%	%	kIx~	%
hmoty	hmota	k1gFnPc1	hmota
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
činí	činit	k5eAaImIp3nS	činit
jen	jen	k9	jen
1	[number]	k4	1
%	%	kIx~	%
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
celé	celý	k2eAgFnSc2d1	celá
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
pak	pak	k6eAd1	pak
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
ostatní	ostatní	k2eAgNnPc4d1	ostatní
obíhající	obíhající	k2eAgNnPc4d1	obíhající
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
Laplaceova	Laplaceův	k2eAgFnSc1d1	Laplaceova
hypotéza	hypotéza	k1gFnSc1	hypotéza
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
nijak	nijak	k6eAd1	nijak
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
opuštěna	opuštěn	k2eAgFnSc1d1	opuštěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neúspěch	neúspěch	k1gInSc1	neúspěch
Laplaceova	Laplaceův	k2eAgInSc2d1	Laplaceův
modelu	model	k1gInSc2	model
stimuloval	stimulovat	k5eAaImAgMnS	stimulovat
vědce	vědec	k1gMnPc4	vědec
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokusili	pokusit	k5eAaPmAgMnP	pokusit
nějak	nějak	k6eAd1	nějak
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
mnoho	mnoho	k4c1	mnoho
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
planetesimální	planetesimální	k2eAgFnSc1d1	planetesimální
teorie	teorie	k1gFnSc1	teorie
Thomase	Thomas	k1gMnSc2	Thomas
Chamberlina	Chamberlin	k2eAgMnSc2d1	Chamberlin
a	a	k8xC	a
Foresta	Forest	k1gMnSc2	Forest
Moultona	Moulton	k1gMnSc2	Moulton
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slapový	slapový	k2eAgInSc1d1	slapový
model	model	k1gInSc1	model
Jamese	Jamese	k1gFnSc2	Jamese
Jeanse	Jeanse	k1gFnSc2	Jeanse
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akreční	akreční	k2eAgInSc1d1	akreční
model	model	k1gInSc1	model
Otto	Otto	k1gMnSc1	Otto
Šmidta	Šmidta	k1gMnSc1	Šmidta
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protoplanetární	protoplanetární	k2eAgFnSc1d1	protoplanetární
teorie	teorie	k1gFnSc1	teorie
Williama	William	k1gMnSc2	William
McCrea	McCreus	k1gMnSc2	McCreus
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
teorie	teorie	k1gFnSc1	teorie
zachycení	zachycení	k1gNnSc2	zachycení
Michaela	Michael	k1gMnSc2	Michael
M.	M.	kA	M.
Woolfsona	Woolfson	k1gMnSc2	Woolfson
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Andrew	Andrew	k1gFnSc1	Andrew
Prentice	Prentice	k1gFnSc2	Prentice
opět	opět	k6eAd1	opět
vzkřísil	vzkřísit	k5eAaPmAgMnS	vzkřísit
původní	původní	k2eAgFnPc4d1	původní
Laplaceovy	Laplaceův	k2eAgFnPc4d1	Laplaceova
myšlenky	myšlenka	k1gFnPc4	myšlenka
tvorby	tvorba	k1gFnSc2	tvorba
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
tzv.	tzv.	kA	tzv.
moderní	moderní	k2eAgFnSc4d1	moderní
Laplaceovu	Laplaceův	k2eAgFnSc4d1	Laplaceova
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
pokusů	pokus	k1gInPc2	pokus
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
široce	široko	k6eAd1	široko
akceptovaná	akceptovaný	k2eAgFnSc1d1	akceptovaná
teorie	teorie	k1gFnSc1	teorie
tvorby	tvorba	k1gFnSc2	tvorba
planet	planeta	k1gFnPc2	planeta
ze	z	k7c2	z
sluneční	sluneční	k2eAgFnSc2d1	sluneční
mlhoviny	mlhovina	k1gFnSc2	mlhovina
označovaná	označovaný	k2eAgNnPc4d1	označované
anglickým	anglický	k2eAgInSc7d1	anglický
výrazem	výraz	k1gInSc7	výraz
Solar	Solara	k1gFnPc2	Solara
Nebular	Nebulara	k1gFnPc2	Nebulara
Disk	disk	k1gInSc1	disk
Model	model	k1gInSc1	model
(	(	kIx(	(
<g/>
SNDM	SNDM	kA	SNDM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
v	v	k7c6	v
pracích	práce	k1gFnPc6	práce
sovětského	sovětský	k2eAgMnSc2d1	sovětský
astronoma	astronom	k1gMnSc2	astronom
Viktora	Viktor	k1gMnSc2	Viktor
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Safronova	Safronův	k2eAgMnSc2d1	Safronův
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Evolucija	Evolucij	k2eAgFnSc1d1	Evolucij
doplanětnogo	doplanětnogo	k6eAd1	doplanětnogo
oblaka	oblaka	k1gNnPc4	oblaka
i	i	k8xC	i
obrazovanie	obrazovanie	k1gFnPc4	obrazovanie
Zemlji	Zemlje	k1gFnSc4	Zemlje
i	i	k8xC	i
planět	planět	k5eAaImF	planět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
přeložena	přeložit	k5eAaPmNgFnS	přeložit
i	i	k9	i
do	do	k7c2	do
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
odbornou	odborný	k2eAgFnSc4d1	odborná
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
byly	být	k5eAaImAgFnP	být
formulované	formulovaný	k2eAgInPc1d1	formulovaný
všechny	všechen	k3xTgInPc1	všechen
hlavní	hlavní	k2eAgInPc1d1	hlavní
problémy	problém	k1gInPc1	problém
procesu	proces	k1gInSc2	proces
tvorby	tvorba	k1gFnSc2	tvorba
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
autor	autor	k1gMnSc1	autor
dokázal	dokázat	k5eAaPmAgInS	dokázat
i	i	k9	i
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Safronovovy	Safronovův	k2eAgFnPc1d1	Safronovův
myšlenky	myšlenka	k1gFnPc1	myšlenka
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pracích	prak	k1gInPc6	prak
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
zejména	zejména	k9	zejména
George	George	k1gInSc1	George
Wetherill	Wetherilla	k1gFnPc2	Wetherilla
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
teorie	teorie	k1gFnSc1	teorie
původně	původně	k6eAd1	původně
týkala	týkat	k5eAaImAgFnS	týkat
pouze	pouze	k6eAd1	pouze
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
ji	on	k3xPp3gFnSc4	on
vědci	vědec	k1gMnPc1	vědec
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
platnou	platný	k2eAgFnSc4d1	platná
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
planetárních	planetární	k2eAgFnPc2d1	planetární
soustav	soustava	k1gFnPc2	soustava
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
mlhovinového	mlhovinový	k2eAgInSc2d1	mlhovinový
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěchy	úspěch	k1gInPc1	úspěch
===	===	k?	===
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
hvězd	hvězda	k1gFnPc2	hvězda
během	během	k7c2	během
jejich	jejich	k3xOp3gNnSc2	jejich
utváření	utváření	k1gNnSc2	utváření
vzniká	vznikat	k5eAaImIp3nS	vznikat
akreční	akreční	k2eAgInSc4d1	akreční
disk	disk	k1gInSc4	disk
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gInSc4	on
nalézt	nalézt	k5eAaBmF	nalézt
kolem	kolem	k7c2	kolem
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
starých	starý	k2eAgFnPc2d1	stará
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
byl	být	k5eAaImAgInS	být
kromě	kromě	k7c2	kromě
různých	různý	k2eAgFnPc2d1	různá
teoretických	teoretický	k2eAgFnPc2d1	teoretická
úvah	úvaha	k1gFnPc2	úvaha
podpořen	podpořit	k5eAaPmNgInS	podpořit
zejména	zejména	k9	zejména
objevem	objev	k1gInSc7	objev
prachoplynových	prachoplynův	k2eAgInPc2d1	prachoplynův
disků	disk	k1gInPc2	disk
kolem	kolem	k7c2	kolem
protohvězd	protohvězda	k1gFnPc2	protohvězda
a	a	k8xC	a
hvězd	hvězda	k1gFnPc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Tauri	k1gNnSc7	Tauri
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gNnPc2	jejich
pozorování	pozorování	k1gNnPc2	pozorování
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
prachová	prachový	k2eAgNnPc1d1	prachové
zrna	zrno	k1gNnPc1	zrno
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	on	k3xPp3gInPc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
nabývají	nabývat	k5eAaImIp3nP	nabývat
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
tisíců	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
tělíska	tělísko	k1gNnSc2	tělísko
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
asi	asi	k9	asi
1	[number]	k4	1
centimetru	centimetr	k1gInSc2	centimetr
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k9	již
astrofyzikové	astrofyzik	k1gMnPc1	astrofyzik
také	také	k9	také
rozumí	rozumět	k5eAaImIp3nP	rozumět
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
1	[number]	k4	1
km	km	kA	km
velké	velký	k2eAgFnSc2d1	velká
planetesimály	planetesimála	k1gFnSc2	planetesimála
dorostou	dorůst	k5eAaPmIp3nP	dorůst
do	do	k7c2	do
těles	těleso	k1gNnPc2	těleso
o	o	k7c6	o
průměrech	průměr	k1gInPc6	průměr
kolem	kolem	k7c2	kolem
1000	[number]	k4	1000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
planetesimál	planetesimál	k1gInSc4	planetesimál
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Akrece	akrece	k1gFnSc1	akrece
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
bývá	bývat	k5eAaImIp3nS	bývat
zpočátku	zpočátku	k6eAd1	zpočátku
velmi	velmi	k6eAd1	velmi
překotná	překotný	k2eAgFnSc1d1	překotná
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
jejich	jejich	k3xOp3gInSc1	jejich
růst	růst	k1gInSc1	růst
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
a	a	k8xC	a
konečným	konečný	k2eAgInSc7d1	konečný
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
značně	značně	k6eAd1	značně
závisí	záviset	k5eAaImIp3nP	záviset
i	i	k9	i
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
simulace	simulace	k1gFnPc1	simulace
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnPc1d1	další
srážky	srážka	k1gFnPc1	srážka
protoplanet	protoplanet	k5eAaImF	protoplanet
ve	v	k7c6	v
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
částech	část	k1gFnPc6	část
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
několika	několik	k4yIc2	několik
těles	těleso	k1gNnPc2	těleso
velikosti	velikost	k1gFnSc2	velikost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
zdá	zdát	k5eAaImIp3nS	zdát
prakticky	prakticky	k6eAd1	prakticky
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Problémy	problém	k1gInPc4	problém
===	===	k?	===
</s>
</p>
<p>
<s>
Studium	studium	k1gNnSc1	studium
akrečních	akreční	k2eAgInPc2d1	akreční
disků	disk	k1gInPc2	disk
naráží	narážet	k5eAaPmIp3nS	narážet
také	také	k9	také
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
v	v	k7c6	v
protohvězdu	protohvězd	k1gInSc6	protohvězd
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
moment	moment	k1gInSc1	moment
hybnosti	hybnost	k1gFnSc2	hybnost
se	se	k3xPyFc4	se
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
vzdálenější	vzdálený	k2eAgFnPc4d2	vzdálenější
části	část	k1gFnPc4	část
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesný	přesný	k2eAgInSc1d1	přesný
mechanismus	mechanismus	k1gInSc1	mechanismus
tohoto	tento	k3xDgInSc2	tento
přenosu	přenos	k1gInSc2	přenos
není	být	k5eNaImIp3nS	být
stále	stále	k6eAd1	stále
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vymizení	vymizení	k1gNnSc3	vymizení
disků	disk	k1gInPc2	disk
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
velkým	velký	k2eAgInSc7d1	velký
nevyřešeným	vyřešený	k2eNgInSc7d1	nevyřešený
problémem	problém	k1gInSc7	problém
mlhovinové	mlhovinový	k2eAgFnSc2d1	mlhovinový
hypotézy	hypotéza	k1gFnSc2	hypotéza
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jak	jak	k8xC	jak
se	s	k7c7	s
1	[number]	k4	1
cm	cm	kA	cm
velká	velký	k2eAgNnPc1d1	velké
tělíska	tělísko	k1gNnPc1	tělísko
pospojují	pospojovat	k5eAaPmIp3nP	pospojovat
do	do	k7c2	do
1	[number]	k4	1
km	km	kA	km
velkých	velká	k1gFnPc2	velká
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
by	by	kYmCp3nP	by
zřejmě	zřejmě	k6eAd1	zřejmě
rovněž	rovněž	k9	rovněž
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
také	také	k9	také
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
kolem	kolem	k7c2	kolem
některých	některý	k3yIgFnPc2	některý
hvězd	hvězda	k1gFnPc2	hvězda
obíhají	obíhat	k5eAaImIp3nP	obíhat
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kolem	kolem	k7c2	kolem
jiných	jiný	k2eAgFnPc2d1	jiná
jen	jen	k9	jen
prachové	prachový	k2eAgInPc1d1	prachový
prstence	prstenec	k1gInPc1	prstenec
<g/>
.	.	kIx.	.
<g/>
Astrofyzikové	astrofyzik	k1gMnPc1	astrofyzik
zatím	zatím	k6eAd1	zatím
zcela	zcela	k6eAd1	zcela
úplně	úplně	k6eAd1	úplně
nerozumí	rozumět	k5eNaImIp3nS	rozumět
ani	ani	k8xC	ani
vzniku	vznik	k1gInSc2	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
teorie	teorie	k1gFnPc1	teorie
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnPc1	jejich
jádra	jádro	k1gNnPc1	jádro
narostou	narůst	k5eAaPmIp3nP	narůst
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
sobě	se	k3xPyFc3	se
stihla	stihnout	k5eAaPmAgFnS	stihnout
ještě	ještě	k6eAd1	ještě
připoutat	připoutat	k5eAaPmF	připoutat
tak	tak	k9	tak
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
plynu	plyn	k1gInSc2	plyn
z	z	k7c2	z
rychle	rychle	k6eAd1	rychle
mizejícího	mizející	k2eAgInSc2d1	mizející
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
doby	doba	k1gFnSc2	doba
života	život	k1gInSc2	život
těchto	tento	k3xDgInPc2	tento
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
činí	činit	k5eAaImIp3nS	činit
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc3	vytvoření
takových	takový	k3xDgNnPc2	takový
jader	jádro	k1gNnPc2	jádro
příliš	příliš	k6eAd1	příliš
krátká	krátký	k2eAgNnPc1d1	krátké
<g/>
.	.	kIx.	.
<g/>
Problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
také	také	k9	také
migrace	migrace	k1gFnSc1	migrace
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
výpočtů	výpočet	k1gInPc2	výpočet
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
jejich	jejich	k3xOp3gFnSc1	jejich
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
disku	disk	k1gInSc6	disk
mohla	moct	k5eAaImAgFnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
přemístění	přemístění	k1gNnSc3	přemístění
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k7c2	dovnitř
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
migrace	migrace	k1gFnSc1	migrace
nebyla	být	k5eNaImAgFnS	být
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
zastavena	zastaven	k2eAgFnSc1d1	zastavena
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
by	by	kYmCp3nP	by
své	svůj	k3xOyFgFnPc4	svůj
velikosti	velikost	k1gFnPc4	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
protoplanetárních	protoplanetární	k2eAgInPc2d1	protoplanetární
disků	disk	k1gInPc2	disk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Protohvězdy	Protohvězd	k1gInPc4	Protohvězd
===	===	k?	===
</s>
</p>
<p>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
uvnitř	uvnitř	k7c2	uvnitř
obrovských	obrovský	k2eAgFnPc2d1	obrovská
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
mračen	mračna	k1gFnPc2	mračna
sestávajících	sestávající	k2eAgInPc2d1	sestávající
z	z	k7c2	z
chladného	chladný	k2eAgInSc2d1	chladný
molekulového	molekulový	k2eAgInSc2d1	molekulový
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mívají	mívat	k5eAaImIp3nP	mívat
průměr	průměr	k1gInSc4	průměr
kolem	kolem	k7c2	kolem
20	[number]	k4	20
parseků	parsec	k1gInPc2	parsec
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hmotnost	hmotnost	k1gFnSc1	hmotnost
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
mohou	moct	k5eAaImIp3nP	moct
tato	tento	k3xDgFnSc1	tento
mračna	mračna	k1gFnSc1	mračna
začít	začít	k5eAaPmF	začít
kolabovat	kolabovat	k5eAaImF	kolabovat
a	a	k8xC	a
rozpadat	rozpadat	k5eAaPmF	rozpadat
se	se	k3xPyFc4	se
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
fragmentů	fragment	k1gInPc2	fragment
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nP	tvořit
malá	malý	k2eAgNnPc1d1	malé
hustá	hustý	k2eAgNnPc1d1	husté
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
dále	daleko	k6eAd2	daleko
kolabují	kolabovat	k5eAaImIp3nP	kolabovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnosti	hmotnost	k1gFnPc4	hmotnost
těchto	tento	k3xDgNnPc2	tento
jader	jádro	k1gNnPc2	jádro
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
malého	malý	k2eAgInSc2d1	malý
zlomku	zlomek	k1gInSc2	zlomek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
až	až	k9	až
po	po	k7c4	po
několik	několik	k4yIc4	několik
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
protostelární	protostelární	k2eAgMnPc1d1	protostelární
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
Slunce	slunce	k1gNnSc2	slunce
protosolární	protosolární	k2eAgFnSc2d1	protosolární
<g/>
)	)	kIx)	)
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
průměr	průměr	k1gInSc1	průměr
bývá	bývat	k5eAaImIp3nS	bývat
od	od	k7c2	od
0,01	[number]	k4	0,01
do	do	k7c2	do
0,1	[number]	k4	0,1
parseku	parsec	k1gInSc2	parsec
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
000	[number]	k4	000
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
počtu	počet	k1gInSc2	počet
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
molekul	molekula	k1gFnPc2	molekula
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
přibližně	přibližně	k6eAd1	přibližně
mezi	mezi	k7c7	mezi
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
cm	cm	kA	cm
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
kolapsu	kolaps	k1gInSc2	kolaps
protostelární	protostelární	k2eAgFnSc2d1	protostelární
mlhoviny	mlhovina	k1gFnSc2	mlhovina
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Slunce	slunce	k1gNnSc2	slunce
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
rotující	rotující	k2eAgFnSc1d1	rotující
mlhovina	mlhovina	k1gFnSc1	mlhovina
má	mít	k5eAaImIp3nS	mít
nějaký	nějaký	k3yIgInSc4	nějaký
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
moment	moment	k1gInSc1	moment
hybnosti	hybnost	k1gFnSc2	hybnost
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
již	již	k9	již
nestlačující	stlačující	k2eNgNnSc1d1	stlačující
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
zlomek	zlomek	k1gInSc1	zlomek
hmoty	hmota	k1gFnSc2	hmota
původní	původní	k2eAgFnSc2d1	původní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
zárodkem	zárodek	k1gInSc7	zárodek
budoucí	budoucí	k2eAgFnSc2d1	budoucí
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
kolaps	kolaps	k1gInSc1	kolaps
mlhoviny	mlhovina	k1gFnSc2	mlhovina
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
vlivem	vlivem	k7c2	vlivem
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
hybnosti	hybnost	k1gFnSc2	hybnost
se	se	k3xPyFc4	se
rotace	rotace	k1gFnSc1	rotace
zmenšující	zmenšující	k2eAgFnSc2d1	zmenšující
se	se	k3xPyFc4	se
plynné	plynný	k2eAgFnSc2d1	plynná
obálky	obálka	k1gFnSc2	obálka
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
částečně	částečně	k6eAd1	částečně
plynu	plynout	k5eAaImIp1nS	plynout
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dále	daleko	k6eAd2	daleko
padal	padat	k5eAaImAgInS	padat
na	na	k7c4	na
centrální	centrální	k2eAgNnSc4d1	centrální
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
vytlačován	vytlačovat	k5eAaImNgInS	vytlačovat
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
podél	podél	k7c2	podél
jeho	jeho	k3xOp3gFnSc2	jeho
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
roviny	rovina	k1gFnSc2	rovina
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tak	tak	k6eAd1	tak
akreční	akreční	k2eAgInSc4d1	akreční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
nakonec	nakonec	k6eAd1	nakonec
stane	stanout	k5eAaPmIp3nS	stanout
mladá	mladý	k2eAgFnSc1d1	mladá
horká	horký	k2eAgFnSc1d1	horká
protohvězda	protohvězda	k1gFnSc1	protohvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
diskem	disk	k1gInSc7	disk
zahalena	zahalen	k2eAgFnSc1d1	zahalena
v	v	k7c6	v
obálce	obálka	k1gFnSc6	obálka
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
padá	padat	k5eAaImIp3nS	padat
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
přímo	přímo	k6eAd1	přímo
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Obálka	obálka	k1gFnSc1	obálka
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
neprůhledná	průhledný	k2eNgFnSc1d1	neprůhledná
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokonce	dokonce	k9	dokonce
i	i	k9	i
záření	záření	k1gNnSc1	záření
milimetrových	milimetrový	k2eAgFnPc2d1	milimetrová
délek	délka	k1gFnPc2	délka
uniká	unikat	k5eAaImIp3nS	unikat
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
vnitřku	vnitřek	k1gInSc2	vnitřek
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
velmi	velmi	k6eAd1	velmi
jasně	jasně	k6eAd1	jasně
září	zářit	k5eAaImIp3nS	zářit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
milimetrových	milimetrový	k2eAgFnPc6d1	milimetrová
a	a	k8xC	a
submilimetrových	submilimetrový	k2eAgFnPc6d1	submilimetrová
délkách	délka	k1gFnPc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
protohvězdy	protohvězd	k1gInPc1	protohvězd
třídy	třída	k1gFnSc2	třída
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
provázen	provázet	k5eAaImNgInS	provázet
bipolárními	bipolární	k2eAgInPc7d1	bipolární
výtrysky	výtrysk	k1gInPc7	výtrysk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
směřují	směřovat	k5eAaImIp3nP	směřovat
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
podél	podél	k7c2	podél
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
výtrysky	výtrysk	k1gInPc1	výtrysk
lze	lze	k6eAd1	lze
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
rodícími	rodící	k2eAgFnPc7d1	rodící
se	se	k3xPyFc4	se
hvězdami	hvězda	k1gFnPc7	hvězda
pozorovat	pozorovat	k5eAaImF	pozorovat
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
protohvězd	protohvězd	k1gMnSc1	protohvězd
třídy	třída	k1gFnSc2	třída
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
–	–	k?	–
protohvězda	protohvězda	k1gFnSc1	protohvězda
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
může	moct	k5eAaImIp3nS	moct
zářit	zářit	k5eAaImF	zářit
až	až	k9	až
100	[number]	k4	100
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
jejich	jejich	k3xOp3gFnSc2	jejich
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgInSc1d1	gravitační
kolaps	kolaps	k1gInSc1	kolaps
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
v	v	k7c6	v
protohvězdě	protohvězda	k1gFnSc6	protohvězda
ještě	ještě	k6eAd1	ještě
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
vodíkové	vodíkový	k2eAgFnSc3d1	vodíková
fúzi	fúze	k1gFnSc3	fúze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plyn	plyn	k1gInSc1	plyn
obklopující	obklopující	k2eAgFnSc4d1	obklopující
protohvězdu	protohvězda	k1gFnSc4	protohvězda
dále	daleko	k6eAd2	daleko
padá	padat	k5eAaImIp3nS	padat
na	na	k7c4	na
protoplanetární	protoplanetární	k2eAgInSc4d1	protoplanetární
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
oblaku	oblak	k1gInSc2	oblak
již	již	k6eAd1	již
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k6eAd1	jen
tenká	tenký	k2eAgFnSc1d1	tenká
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
průhledná	průhledný	k2eAgFnSc1d1	průhledná
obálka	obálka	k1gFnSc1	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Mladou	mladý	k2eAgFnSc4d1	mladá
hvězdu	hvězda	k1gFnSc4	hvězda
již	již	k9	již
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
přímo	přímo	k6eAd1	přímo
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
světelného	světelný	k2eAgNnSc2d1	světelné
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
protohvězdě	protohvězda	k1gFnSc6	protohvězda
zažehnuta	zažehnut	k2eAgFnSc1d1	zažehnuta
fúze	fúze	k1gFnSc1	fúze
deuteria	deuterium	k1gNnSc2	deuterium
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
také	také	k9	také
běžného	běžný	k2eAgInSc2d1	běžný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rodí	rodit	k5eAaImIp3nS	rodit
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
po	po	k7c6	po
počátku	počátek	k1gInSc6	počátek
kolapsu	kolaps	k1gInSc2	kolaps
původní	původní	k2eAgFnSc2d1	původní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc4d1	vnější
vzhled	vzhled	k1gInSc4	vzhled
mladé	mladý	k2eAgFnSc2d1	mladá
hvězdy	hvězda	k1gFnSc2	hvězda
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
protohvězdě	protohvězda	k1gFnSc3	protohvězda
třídy	třída	k1gFnSc2	třída
I	I	kA	I
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
mladá	mladý	k2eAgFnSc1d1	mladá
hvězda	hvězda	k1gFnSc1	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Tauri	k1gNnSc7	Tauri
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
konečné	konečný	k2eAgFnSc2d1	konečná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
:	:	kIx,	:
hmotnost	hmotnost	k1gFnSc1	hmotnost
disku	disk	k1gInSc2	disk
a	a	k8xC	a
zbývající	zbývající	k2eAgFnSc2d1	zbývající
obálky	obálka	k1gFnSc2	obálka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
již	již	k6eAd1	již
jen	jen	k9	jen
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
již	již	k9	již
obálka	obálka	k1gFnSc1	obálka
úplně	úplně	k6eAd1	úplně
mizí	mizet	k5eAaImIp3nS	mizet
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc4	veškerý
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Protohvězda	Protohvězda	k1gFnSc1	Protohvězda
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
klasickou	klasický	k2eAgFnSc7d1	klasická
hvězdou	hvězda	k1gFnSc7	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Tauri	k1gNnSc7	Tauri
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c4	po
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
disku	disk	k1gInSc2	disk
kolem	kolem	k7c2	kolem
klasické	klasický	k2eAgFnSc2d1	klasická
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taure	k1gFnSc4	Taure
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
akrece	akrece	k1gFnSc1	akrece
postupuje	postupovat	k5eAaImIp3nS	postupovat
přibližnou	přibližný	k2eAgFnSc7d1	přibližná
rychlostí	rychlost	k1gFnSc7	rychlost
10	[number]	k4	10
<g/>
miliontiny	miliontina	k1gFnPc1	miliontina
až	až	k8xS	až
miliardtiny	miliardtina	k1gFnPc1	miliardtina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
bipolární	bipolární	k2eAgInPc4d1	bipolární
výtrysky	výtrysk	k1gInPc4	výtrysk
<g/>
.	.	kIx.	.
</s>
<s>
Akrece	akrece	k1gFnSc1	akrece
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
všechny	všechen	k3xTgFnPc4	všechen
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
klasických	klasický	k2eAgFnPc2d1	klasická
hvězd	hvězda	k1gFnPc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taur	k1gFnSc2	Taur
<g/>
:	:	kIx,	:
neobvykle	obvykle	k6eNd1	obvykle
silné	silný	k2eAgFnPc4d1	silná
emisní	emisní	k2eAgFnPc4d1	emisní
čáry	čára	k1gFnPc4	čára
<g/>
,	,	kIx,	,
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
aktivitu	aktivita	k1gFnSc4	aktivita
a	a	k8xC	a
výtrysky	výtrysk	k1gInPc4	výtrysk
<g/>
.	.	kIx.	.
</s>
<s>
Emisní	emisní	k2eAgFnPc1d1	emisní
čáry	čára	k1gFnPc1	čára
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
když	když	k8xS	když
plyn	plyn	k1gInSc1	plyn
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jejích	její	k3xOp3gInPc2	její
magnetických	magnetický	k2eAgInPc2d1	magnetický
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Výtrysky	výtrysk	k1gInPc1	výtrysk
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
akrece	akrece	k1gFnSc2	akrece
a	a	k8xC	a
odnášejí	odnášet	k5eAaImIp3nP	odnášet
přebytečný	přebytečný	k2eAgInSc4d1	přebytečný
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
fáze	fáze	k1gFnSc1	fáze
hvězd	hvězda	k1gFnPc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taure	k1gFnSc4	Taure
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Protoplanetární	Protoplanetární	k2eAgInSc4d1	Protoplanetární
disk	disk	k1gInSc4	disk
nakonec	nakonec	k6eAd1	nakonec
zcela	zcela	k6eAd1	zcela
vymizí	vymizet	k5eAaPmIp3nP	vymizet
vlivem	vliv	k1gInSc7	vliv
akrece	akrece	k1gFnSc2	akrece
<g/>
,	,	kIx,	,
formování	formování	k1gNnSc2	formování
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
výtrysků	výtrysk	k1gInPc2	výtrysk
a	a	k8xC	a
fotoevaporace	fotoevaporace	k1gFnSc2	fotoevaporace
způsobené	způsobený	k2eAgFnSc2d1	způsobená
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
zářením	záření	k1gNnSc7	záření
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
blízkých	blízký	k2eAgFnPc2d1	blízká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
hvězda	hvězda	k1gFnSc1	hvězda
tak	tak	k6eAd1	tak
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
čárově	čárově	k6eAd1	čárově
slabé	slabý	k2eAgFnSc2d1	slabá
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taure	k1gFnSc4	Taure
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
běžnou	běžný	k2eAgFnSc4d1	běžná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gNnSc4	náš
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Protoplanetární	Protoplanetární	k2eAgInSc4d1	Protoplanetární
disk	disk	k1gInSc4	disk
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
začít	začít	k5eAaPmF	začít
rodit	rodit	k5eAaImF	rodit
nová	nový	k2eAgFnSc1d1	nová
planetární	planetární	k2eAgFnSc1d1	planetární
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Protoplanetární	Protoplanetární	k2eAgInPc1d1	Protoplanetární
disky	disk	k1gInPc1	disk
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
kolem	kolem	k7c2	kolem
poměrně	poměrně	k6eAd1	poměrně
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
mladých	mladý	k2eAgFnPc6d1	mladá
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
již	již	k6eAd1	již
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
fázích	fáze	k1gFnPc6	fáze
tvorby	tvorba	k1gFnSc2	tvorba
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpočátku	zpočátku	k6eAd1	zpočátku
nejsou	být	k5eNaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
neprůhledné	průhledný	k2eNgFnSc3d1	neprůhledná
plynné	plynný	k2eAgFnSc3d1	plynná
obálce	obálka	k1gFnSc3	obálka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gInPc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
,	,	kIx,	,
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Disk	disk	k1gInSc1	disk
obklopující	obklopující	k2eAgInSc1d1	obklopující
protohvězdu	protohvězda	k1gFnSc4	protohvězda
třídy	třída	k1gFnSc2	třída
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
masivní	masivní	k2eAgInSc4d1	masivní
a	a	k8xC	a
horký	horký	k2eAgInSc4d1	horký
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
akreční	akreční	k2eAgInSc4d1	akreční
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
protohvězdu	protohvězda	k1gFnSc4	protohvězda
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
středu	střed	k1gInSc6	střed
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
5	[number]	k4	5
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
až	až	k9	až
400	[number]	k4	400
kelvinů	kelvin	k1gInPc2	kelvin
a	a	k8xC	a
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
1	[number]	k4	1
AU	au	k0	au
dokonce	dokonce	k9	dokonce
1000	[number]	k4	1000
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
ho	on	k3xPp3gNnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
rozpadající	rozpadající	k2eAgFnSc1d1	rozpadající
se	se	k3xPyFc4	se
turbulence	turbulence	k1gFnSc1	turbulence
a	a	k8xC	a
dopadající	dopadající	k2eAgInSc1d1	dopadající
plyn	plyn	k1gInSc1	plyn
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
prchavých	prchavý	k2eAgFnPc2d1	prchavá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
kamenné	kamenný	k2eAgInPc1d1	kamenný
materiály	materiál	k1gInPc1	materiál
se	se	k3xPyFc4	se
vypaří	vypařit	k5eAaPmIp3nP	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
zde	zde	k6eAd1	zde
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
pouze	pouze	k6eAd1	pouze
žáruvzdorné	žáruvzdorný	k2eAgInPc1d1	žáruvzdorný
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
může	moct	k5eAaImIp3nS	moct
přežít	přežít	k5eAaPmF	přežít
jen	jen	k9	jen
ve	v	k7c6	v
vnějších	vnější	k2eAgFnPc6d1	vnější
částech	část	k1gFnPc6	část
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
<g/>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
problémy	problém	k1gInPc7	problém
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
akrečních	akreční	k2eAgInPc2d1	akreční
disků	disk	k1gInPc2	disk
jsou	být	k5eAaImIp3nP	být
vznik	vznik	k1gInSc4	vznik
turbulencí	turbulence	k1gFnPc2	turbulence
a	a	k8xC	a
mechanismy	mechanismus	k1gInPc1	mechanismus
odpovědné	odpovědný	k2eAgInPc1d1	odpovědný
za	za	k7c4	za
vysokou	vysoký	k2eAgFnSc4d1	vysoká
efektivní	efektivní	k2eAgFnSc4d1	efektivní
viskozitu	viskozita	k1gFnSc4	viskozita
<g/>
.	.	kIx.	.
</s>
<s>
Turbulentní	turbulentní	k2eAgFnSc1d1	turbulentní
viskozita	viskozita	k1gFnSc1	viskozita
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
příčinou	příčina	k1gFnSc7	příčina
přenosu	přenos	k1gInSc2	přenos
hmoty	hmota	k1gFnSc2	hmota
na	na	k7c4	na
centrální	centrální	k2eAgFnSc4d1	centrální
protohvězdu	protohvězda	k1gFnSc4	protohvězda
a	a	k8xC	a
momentu	moment	k1gInSc3	moment
hybnosti	hybnost	k1gFnSc2	hybnost
do	do	k7c2	do
vnějších	vnější	k2eAgFnPc2d1	vnější
oblastí	oblast	k1gFnPc2	oblast
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
pokračující	pokračující	k2eAgFnSc4d1	pokračující
akreci	akrece	k1gFnSc4	akrece
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
centrální	centrální	k2eAgFnSc1d1	centrální
protohvězda	protohvězda	k1gFnSc1	protohvězda
může	moct	k5eAaImIp3nS	moct
přijímat	přijímat	k5eAaImF	přijímat
další	další	k2eAgInSc4d1	další
plyn	plyn	k1gInSc4	plyn
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ztratí	ztratit	k5eAaPmIp3nS	ztratit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odnesen	odnést	k5eAaPmNgMnS	odnést
malou	malý	k2eAgFnSc7d1	malá
částí	část	k1gFnSc7	část
plynu	plyn	k1gInSc2	plyn
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
růst	růst	k1gInSc1	růst
protohvězdy	protohvězda	k1gFnSc2	protohvězda
i	i	k8xC	i
průměru	průměr	k1gInSc2	průměr
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
moment	moment	k1gInSc1	moment
hybnosti	hybnost	k1gFnSc2	hybnost
mlhoviny	mlhovina	k1gFnSc2	mlhovina
dostatečně	dostatečně	k6eAd1	dostatečně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
1000	[number]	k4	1000
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
běžně	běžně	k6eAd1	běžně
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
velké	velký	k2eAgInPc4d1	velký
disky	disk	k1gInPc4	disk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
vzniku	vznik	k1gInSc3	vznik
nových	nový	k2eAgFnPc2d1	nová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Velká	velký	k2eAgFnSc1d1	velká
mlhovina	mlhovina	k1gFnSc1	mlhovina
v	v	k7c6	v
Orionu	orion	k1gInSc6	orion
<g/>
.	.	kIx.	.
<g/>
Akreční	Akreční	k2eAgInSc4d1	Akreční
disk	disk	k1gInSc4	disk
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
kolem	kolem	k7c2	kolem
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
hvězda	hvězda	k1gFnSc1	hvězda
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
fáze	fáze	k1gFnPc4	fáze
klasické	klasický	k2eAgFnSc2d1	klasická
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taur	k1gFnPc1	Taur
<g/>
,	,	kIx,	,
disk	disk	k1gInSc1	disk
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
ztenčovat	ztenčovat	k5eAaImF	ztenčovat
a	a	k8xC	a
chladnout	chladnout	k5eAaImF	chladnout
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
prchavé	prchavý	k2eAgInPc1d1	prchavý
materiály	materiál	k1gInPc1	materiál
začnou	začít	k5eAaPmIp3nP	začít
kondenzovat	kondenzovat	k5eAaImF	kondenzovat
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
blíže	blíž	k1gFnSc2	blíž
jeho	jeho	k3xOp3gFnSc4	jeho
středu	středa	k1gFnSc4	středa
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
prachová	prachový	k2eAgNnPc4d1	prachové
zrnka	zrnko	k1gNnPc4	zrnko
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
μ	μ	k?	μ
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
krystalické	krystalický	k2eAgInPc4d1	krystalický
křemičitany	křemičitan	k1gInPc4	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Transport	transport	k1gInSc1	transport
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
vnějších	vnější	k2eAgFnPc2d1	vnější
částí	část	k1gFnPc2	část
disku	disk	k1gInSc2	disk
může	moct	k5eAaImIp3nS	moct
promíchat	promíchat	k5eAaPmF	promíchat
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgNnPc4d1	vytvořené
prachová	prachový	k2eAgNnPc4d1	prachové
zrna	zrno	k1gNnPc4	zrno
s	s	k7c7	s
těmi	ten	k3xDgFnPc7	ten
staršími	starý	k2eAgFnPc7d2	starší
<g/>
,	,	kIx,	,
obsahujícími	obsahující	k2eAgFnPc7d1	obsahující
různé	různý	k2eAgInPc4d1	různý
organické	organický	k2eAgInPc4d1	organický
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
prchavé	prchavý	k2eAgFnPc4d1	prchavá
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
promíchání	promíchání	k1gNnSc1	promíchání
by	by	kYmCp3nS	by
vysvětlilo	vysvětlit	k5eAaPmAgNnS	vysvětlit
některé	některý	k3yIgFnPc4	některý
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
přítomnost	přítomnost	k1gFnSc1	přítomnost
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
prachu	prach	k1gInSc2	prach
ve	v	k7c6	v
starých	starý	k2eAgInPc6d1	starý
meteoritech	meteorit	k1gInPc6	meteorit
a	a	k8xC	a
žáruvzdorných	žáruvzdorný	k2eAgInPc2d1	žáruvzdorný
materiálů	materiál	k1gInPc2	materiál
v	v	k7c6	v
kometách	kometa	k1gFnPc6	kometa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
hustém	hustý	k2eAgNnSc6d1	husté
prostředí	prostředí	k1gNnSc6	prostředí
disku	disk	k1gInSc2	disk
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
lepit	lepit	k5eAaImF	lepit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
větších	veliký	k2eAgNnPc2d2	veliký
tělísek	tělísko	k1gNnPc2	tělísko
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
shlukování	shlukování	k1gNnSc2	shlukování
prachu	prach	k1gInSc2	prach
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
záření	záření	k1gNnSc2	záření
vycházejícího	vycházející	k2eAgMnSc2d1	vycházející
z	z	k7c2	z
mladých	mladý	k2eAgInPc2d1	mladý
disků	disk	k1gInPc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
hromadění	hromadění	k1gNnSc1	hromadění
pak	pak	k6eAd1	pak
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
planetesimál	planetesimál	k1gInSc1	planetesimál
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
km	km	kA	km
nebo	nebo	k8xC	nebo
i	i	k9	i
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
planetesimály	planetesimál	k1gInPc1	planetesimál
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
základními	základní	k2eAgInPc7d1	základní
stavebními	stavební	k2eAgInPc7d1	stavební
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
proces	proces	k1gInSc1	proces
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
planetesimál	planetesimála	k1gFnPc2	planetesimála
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
nevyřešeným	vyřešený	k2eNgInSc7d1	nevyřešený
problémem	problém	k1gInSc7	problém
fyziky	fyzika	k1gFnSc2	fyzika
akrečních	akreční	k2eAgInPc2d1	akreční
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
větších	veliký	k2eAgFnPc6d2	veliký
velikostech	velikost	k1gFnPc6	velikost
již	již	k9	již
prosté	prostý	k2eAgNnSc4d1	prosté
shlukování	shlukování	k1gNnSc4	shlukování
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
účinné	účinný	k2eAgNnSc1d1	účinné
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
oblibu	obliba	k1gFnSc4	obliba
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
hypotéza	hypotéza	k1gFnSc1	hypotéza
gravitační	gravitační	k2eAgFnSc2d1	gravitační
nestability	nestabilita	k1gFnSc2	nestabilita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
tělíska	tělísko	k1gNnPc1	tělísko
o	o	k7c6	o
rozměru	rozměr	k1gInSc6	rozměr
několika	několik	k4yIc2	několik
centimetrů	centimetr	k1gInPc2	centimetr
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgNnPc1d2	veliký
pomalu	pomalu	k6eAd1	pomalu
usazují	usazovat	k5eAaImIp3nP	usazovat
poblíž	poblíž	k7c2	poblíž
střední	střední	k2eAgFnSc2d1	střední
roviny	rovina	k1gFnSc2	rovina
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc7d1	tenká
–	–	k?	–
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
km	km	kA	km
silnou	silný	k2eAgFnSc7d1	silná
–	–	k?	–
a	a	k8xC	a
hustou	hustý	k2eAgFnSc4d1	hustá
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
gravitačně	gravitačně	k6eAd1	gravitačně
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
rozpadat	rozpadat	k5eAaPmF	rozpadat
do	do	k7c2	do
početných	početný	k2eAgInPc2d1	početný
shluků	shluk	k1gInPc2	shluk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
potom	potom	k6eAd1	potom
gravitačně	gravitačně	k6eAd1	gravitačně
kolabují	kolabovat	k5eAaImIp3nP	kolabovat
do	do	k7c2	do
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
.	.	kIx.	.
<g/>
Vznik	vznik	k1gInSc1	vznik
planet	planeta	k1gFnPc2	planeta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
spuštěn	spuštěn	k2eAgInSc1d1	spuštěn
gravitační	gravitační	k2eAgFnSc7d1	gravitační
nestabilitou	nestabilita	k1gFnSc7	nestabilita
uvnitř	uvnitř	k7c2	uvnitř
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
rozpadu	rozpad	k1gInSc3	rozpad
do	do	k7c2	do
shluků	shluk	k1gInPc2	shluk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
husté	hustý	k2eAgInPc1d1	hustý
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
začít	začít	k5eAaPmF	začít
gravitačně	gravitačně	k6eAd1	gravitačně
kolabovat	kolabovat	k5eAaImF	kolabovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
vzniku	vznik	k1gInSc3	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
kolem	kolem	k7c2	kolem
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
však	však	k9	však
možný	možný	k2eAgMnSc1d1	možný
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
masivních	masivní	k2eAgInPc2d1	masivní
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
0,3	[number]	k4	0,3
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc4d1	typický
disk	disk	k1gInSc4	disk
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
pouze	pouze	k6eAd1	pouze
0,01	[number]	k4	0,01
až	až	k9	až
0,03	[number]	k4	0,03
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
masivní	masivní	k2eAgInPc1d1	masivní
disky	disk	k1gInPc1	disk
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
vzniku	vznik	k1gInSc2	vznik
planet	planeta	k1gFnPc2	planeta
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
definitivním	definitivní	k2eAgInSc6d1	definitivní
rozpadu	rozpad	k1gInSc6	rozpad
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
několik	několik	k4yIc1	několik
různých	různý	k2eAgInPc2d1	různý
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
disku	disk	k1gInSc2	disk
končí	končit	k5eAaImIp3nS	končit
vlivem	vliv	k1gInSc7	vliv
akrece	akrece	k1gFnSc2	akrece
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
hvězdě	hvězda	k1gFnSc6	hvězda
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
je	být	k5eAaImIp3nS	být
vystřelena	vystřelit	k5eAaPmNgFnS	vystřelit
bipolárními	bipolární	k2eAgInPc7d1	bipolární
výtrysky	výtrysk	k1gInPc7	výtrysk
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
vypaří	vypařit	k5eAaPmIp3nS	vypařit
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
silného	silný	k2eAgNnSc2d1	silné
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taur	k1gFnSc2	Taur
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
blízkých	blízký	k2eAgFnPc2d1	blízká
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
disku	disk	k1gInSc2	disk
buď	buď	k8xC	buď
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
odmrští	odmrštit	k5eAaPmIp3nS	odmrštit
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
planety	planeta	k1gFnPc1	planeta
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
zase	zase	k9	zase
vytlačí	vytlačit	k5eAaPmIp3nP	vytlačit
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
vítr	vítr	k1gInSc4	vítr
vanoucí	vanoucí	k2eAgInSc4d1	vanoucí
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
co	co	k9	co
nakonec	nakonec	k6eAd1	nakonec
zbude	zbýt	k5eAaPmIp3nS	zbýt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
planetární	planetární	k2eAgInSc1d1	planetární
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
proces	proces	k1gInSc1	proces
tvorby	tvorba	k1gFnSc2	tvorba
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
prachový	prachový	k2eAgInSc4d1	prachový
disk	disk	k1gInSc4	disk
bez	bez	k7c2	bez
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
planetesimály	planetesimál	k1gInPc1	planetesimál
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
početné	početný	k2eAgFnPc1d1	početná
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
napříč	napříč	k7c7	napříč
protoplanetárním	protoplanetární	k2eAgInSc7d1	protoplanetární
diskem	disk	k1gInSc7	disk
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
mohou	moct	k5eAaImIp3nP	moct
přežít	přežít	k5eAaPmF	přežít
dobu	doba	k1gFnSc4	doba
tvorby	tvorba	k1gFnSc2	tvorba
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Planetky	planetka	k1gFnPc1	planetka
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
takovými	takový	k3xDgFnPc7	takový
pozůstalými	pozůstalý	k2eAgFnPc7d1	pozůstalá
planetesimálami	planetesimála	k1gFnPc7	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
obrousily	obrousit	k5eAaPmAgFnP	obrousit
do	do	k7c2	do
menších	malý	k2eAgInPc2d2	menší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Komety	kometa	k1gFnPc1	kometa
jsou	být	k5eAaImIp3nP	být
zase	zase	k9	zase
planetesimálami	planetesimála	k1gFnPc7	planetesimála
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
oblastí	oblast	k1gFnPc2	oblast
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Meteority	meteorit	k1gInPc1	meteorit
jsou	být	k5eAaImIp3nP	být
kousky	kousek	k1gInPc4	kousek
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
vědcům	vědec	k1gMnPc3	vědec
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
meteoritů	meteorit	k1gInPc2	meteorit
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
kousky	kousek	k1gInPc1	kousek
malých	malý	k2eAgFnPc2d1	malá
roztříštěných	roztříštěný	k2eAgFnPc2d1	roztříštěná
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neprošly	projít	k5eNaPmAgFnP	projít
žádnou	žádný	k3yNgFnSc4	žádný
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
diferenciací	diferenciace	k1gFnSc7	diferenciace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgInPc1d1	jiný
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
planetesimál	planetesimála	k1gFnPc2	planetesimála
mnohem	mnohem	k6eAd1	mnohem
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
planet	planeta	k1gFnPc2	planeta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Terestrické	terestrický	k2eAgFnSc2d1	terestrická
planety	planeta	k1gFnSc2	planeta
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
vyplývajícího	vyplývající	k2eAgInSc2d1	vyplývající
z	z	k7c2	z
mlhovinové	mlhovinový	k2eAgFnSc2d1	mlhovinový
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
terestrické	terestrický	k2eAgFnPc1d1	terestrická
planety	planeta	k1gFnPc1	planeta
tvoří	tvořit	k5eAaImIp3nP	tvořit
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
ohraničené	ohraničený	k2eAgInPc4d1	ohraničený
tzv.	tzv.	kA	tzv.
sněžnou	sněžný	k2eAgFnSc7d1	sněžná
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
před	před	k7c7	před
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohly	moct	k5eAaImAgFnP	moct
tvořit	tvořit	k5eAaImF	tvořit
částice	částice	k1gFnPc1	částice
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
prchavých	prchavý	k2eAgFnPc2d1	prchavá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
pouze	pouze	k6eAd1	pouze
různé	různý	k2eAgInPc1d1	různý
kamenné	kamenný	k2eAgInPc1d1	kamenný
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
utvářejí	utvářet	k5eAaImIp3nP	utvářet
kamenné	kamenný	k2eAgFnPc4d1	kamenná
planetesimály	planetesimála	k1gFnPc4	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
hvězd	hvězda	k1gFnPc2	hvězda
velikosti	velikost	k1gFnSc2	velikost
Slunce	slunce	k1gNnSc2	slunce
takové	takový	k3xDgFnSc2	takový
podmínky	podmínka	k1gFnSc2	podmínka
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
částech	část	k1gFnPc6	část
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
3	[number]	k4	3
až	až	k8xS	až
4	[number]	k4	4
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
malé	malý	k2eAgFnPc1d1	malá
planetesimály	planetesimála	k1gFnPc1	planetesimála
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
fáze	fáze	k1gFnSc1	fáze
velmi	velmi	k6eAd1	velmi
překotné	překotný	k2eAgFnSc2d1	překotná
akrece	akrece	k1gFnSc2	akrece
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
tělesa	těleso	k1gNnSc2	těleso
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
nabývají	nabývat	k5eAaImIp3nP	nabývat
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
větší	veliký	k2eAgNnPc1d2	veliký
tělesa	těleso	k1gNnPc1	těleso
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
svou	svůj	k3xOyFgFnSc4	svůj
hmotnost	hmotnost	k1gFnSc4	hmotnost
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
těles	těleso	k1gNnPc2	těleso
menších	malý	k2eAgNnPc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
když	když	k8xS	když
velká	velký	k2eAgNnPc1d1	velké
tělesa	těleso	k1gNnPc1	těleso
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
velikosti	velikost	k1gFnSc3	velikost
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
zpomalení	zpomalení	k1gNnSc1	zpomalení
akrece	akrece	k1gFnSc2	akrece
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
rušením	rušení	k1gNnSc7	rušení
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
planetesimál	planetesimála	k1gFnPc2	planetesimála
již	již	k6eAd1	již
vytvořenými	vytvořený	k2eAgFnPc7d1	vytvořená
velkými	velký	k2eAgNnPc7d1	velké
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
větší	veliký	k2eAgNnPc1d2	veliký
tělesa	těleso	k1gNnPc1	těleso
růst	růst	k1gInSc1	růst
těles	těleso	k1gNnPc2	těleso
menších	malý	k2eAgMnPc2d2	menší
zcela	zcela	k6eAd1	zcela
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
planetesimály	planetesimála	k1gFnSc2	planetesimála
přejdou	přejít	k5eAaPmIp3nP	přejít
do	do	k7c2	do
uspořádaného	uspořádaný	k2eAgInSc2d1	uspořádaný
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
oligarchického	oligarchický	k2eAgInSc2d1	oligarchický
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
dominancí	dominance	k1gFnSc7	dominance
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
větších	veliký	k2eAgFnPc2d2	veliký
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
oligarchů	oligarch	k1gMnPc2	oligarch
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
srážejí	srážet	k5eAaImIp3nP	srážet
a	a	k8xC	a
následně	následně	k6eAd1	následně
splývají	splývat	k5eAaImIp3nP	splývat
menší	malý	k2eAgFnPc4d2	menší
planetesimály	planetesimála	k1gFnPc4	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tyto	tento	k3xDgMnPc4	tento
oligarchy	oligarch	k1gMnPc4	oligarch
už	už	k6eAd1	už
žádné	žádný	k3yNgNnSc4	žádný
jiné	jiný	k2eAgNnSc4d1	jiné
těleso	těleso	k1gNnSc4	těleso
nemůže	moct	k5eNaImIp3nS	moct
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
planetesimál	planetesimála	k1gFnPc2	planetesimála
jsou	být	k5eAaImIp3nP	být
oligarchové	oligarch	k1gMnPc1	oligarch
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
drženi	držen	k2eAgMnPc1d1	držen
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
desetinásobku	desetinásobek	k1gInSc2	desetinásobek
poloměru	poloměr	k1gInSc2	poloměr
tzv.	tzv.	kA	tzv.
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
okruhu	okruh	k1gInSc2	okruh
převládajícího	převládající	k2eAgInSc2d1	převládající
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstřednost	výstřednost	k1gFnSc1	výstřednost
a	a	k8xC	a
sklon	sklon	k1gInSc1	sklon
jejich	jejich	k3xOp3gFnPc2	jejich
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Oligarchové	oligarch	k1gMnPc1	oligarch
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
akreci	akrece	k1gFnSc6	akrece
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nejsou	být	k5eNaImIp3nP	být
z	z	k7c2	z
disku	disk	k1gInSc2	disk
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
vyčerpány	vyčerpán	k2eAgFnPc1d1	vyčerpána
všechny	všechen	k3xTgFnPc1	všechen
planetesimály	planetesimála	k1gFnPc1	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
někteří	některý	k3yIgMnPc1	některý
sousední	sousední	k2eAgMnPc1d1	sousední
oligarchové	oligarch	k1gMnPc1	oligarch
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
navzájem	navzájem	k6eAd1	navzájem
srazit	srazit	k5eAaPmF	srazit
a	a	k8xC	a
splynout	splynout	k5eAaPmF	splynout
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
konečná	konečný	k2eAgFnSc1d1	konečná
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
omezená	omezený	k2eAgFnSc1d1	omezená
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
hustotě	hustota	k1gFnSc6	hustota
planetesimál	planetesimál	k1gInSc1	planetesimál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
terestrické	terestrický	k2eAgFnPc4d1	terestrická
planety	planeta	k1gFnPc4	planeta
činí	činit	k5eAaImIp3nS	činit
0,1	[number]	k4	0,1
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
hmotnost	hmotnost	k1gFnSc1	hmotnost
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
této	tento	k3xDgFnSc2	tento
etapy	etapa	k1gFnSc2	etapa
je	být	k5eAaImIp3nS	být
zformováno	zformovat	k5eAaPmNgNnS	zformovat
asi	asi	k9	asi
100	[number]	k4	100
protoplanet	protoplanet	k1gInSc1	protoplanet
velikosti	velikost	k1gFnSc2	velikost
Měsíce	měsíc	k1gInSc2	měsíc
až	až	k8xS	až
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozprostřeny	rozprostřen	k2eAgInPc1d1	rozprostřen
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gNnPc7	on
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
pěti	pět	k4xCc3	pět
až	až	k8xS	až
desetinásobku	desetinásobek	k1gInSc6	desetinásobek
poloměru	poloměr	k1gInSc2	poloměr
jejich	jejich	k3xOp3gFnSc2	jejich
Hillovy	Hillův	k2eAgFnSc2d1	Hillova
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mezerách	mezera	k1gFnPc6	mezera
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděleny	oddělit	k5eAaPmNgInP	oddělit
prstenci	prstenec	k1gInPc7	prstenec
ze	z	k7c2	z
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fáze	fáze	k1gFnSc1	fáze
trvá	trvat	k5eAaImIp3nS	trvat
zřejmě	zřejmě	k6eAd1	zřejmě
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgFnSc7d1	poslední
fází	fáze	k1gFnSc7	fáze
tvorby	tvorba	k1gFnSc2	tvorba
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
je	být	k5eAaImIp3nS	být
etapa	etapa	k1gFnSc1	etapa
jejich	jejich	k3xOp3gFnPc2	jejich
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
planetesimál	planetesimála	k1gFnPc2	planetesimála
a	a	k8xC	a
protoplanety	protoplanet	k1gInPc4	protoplanet
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
natolik	natolik	k6eAd1	natolik
hmotné	hmotný	k2eAgFnPc1d1	hmotná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
začínají	začínat	k5eAaImIp3nP	začínat
gravitačně	gravitačně	k6eAd1	gravitačně
rušit	rušit	k5eAaImF	rušit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
uvede	uvést	k5eAaPmIp3nS	uvést
jejich	jejich	k3xOp3gFnPc4	jejich
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
v	v	k7c4	v
chaos	chaos	k1gInSc4	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
protoplanety	protoplanet	k1gInPc4	protoplanet
vytlačí	vytlačit	k5eAaPmIp3nP	vytlačit
zbývající	zbývající	k2eAgInPc4d1	zbývající
planetesimály	planetesimál	k1gInPc4	planetesimál
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
srazí	srazit	k5eAaPmIp3nS	srazit
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
10	[number]	k4	10
až	až	k9	až
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezeného	omezený	k2eAgInSc2d1	omezený
počtu	počet	k1gInSc2	počet
těles	těleso	k1gNnPc2	těleso
velikosti	velikost	k1gFnSc2	velikost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Simulace	simulace	k1gFnPc1	simulace
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
přeživších	přeživší	k2eAgFnPc2d1	přeživší
planet	planeta	k1gFnPc2	planeta
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
mezi	mezi	k7c7	mezi
2	[number]	k4	2
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
jsou	být	k5eAaImIp3nP	být
takovými	takový	k3xDgFnPc7	takový
planetami	planeta	k1gFnPc7	planeta
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vytvoření	vytvoření	k1gNnSc3	vytvoření
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k1gFnSc1	potřeba
splynutí	splynutí	k1gNnSc2	splynutí
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zhruba	zhruba	k6eAd1	zhruba
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
jich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
vymrštěn	vymrštit	k5eAaPmNgInS	vymrštit
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
hlavním	hlavní	k2eAgInSc6d1	hlavní
pásu	pás	k1gInSc6	pás
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nesly	nést	k5eAaImAgInP	nést
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
daly	dát	k5eAaPmAgFnP	dát
tak	tak	k6eAd1	tak
vzniknout	vzniknout	k5eAaPmF	vzniknout
pozemským	pozemský	k2eAgInPc3d1	pozemský
oceánům	oceán	k1gInPc3	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Mars	Mars	k1gInSc1	Mars
a	a	k8xC	a
Merkur	Merkur	k1gInSc1	Merkur
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
protoplanety	protoplanet	k1gInPc4	protoplanet
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
tuto	tento	k3xDgFnSc4	tento
chaotickou	chaotický	k2eAgFnSc4d1	chaotická
dobu	doba	k1gFnSc4	doba
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
terestrické	terestrický	k2eAgFnPc1d1	terestrická
planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
usadí	usadit	k5eAaPmIp3nS	usadit
na	na	k7c4	na
víceméně	víceméně	k9	víceméně
stabilních	stabilní	k2eAgFnPc6d1	stabilní
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsou	být	k5eAaImIp3nP	být
planetární	planetární	k2eAgInPc4d1	planetární
systémy	systém	k1gInPc4	systém
obvykle	obvykle	k6eAd1	obvykle
obsazené	obsazený	k2eAgInPc4d1	obsazený
až	až	k9	až
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
únosnosti	únosnost	k1gFnSc2	únosnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
nestability	nestabilita	k1gFnSc2	nestabilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
===	===	k?	===
</s>
</p>
<p>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
proces	proces	k1gInSc1	proces
vzniku	vznik	k1gInSc2	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
planetology	planetolog	k1gMnPc4	planetolog
stále	stále	k6eAd1	stále
nevyřešený	vyřešený	k2eNgInSc4d1	nevyřešený
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mlhovinového	mlhovinový	k2eAgInSc2d1	mlhovinový
modelu	model	k1gInSc2	model
vzniku	vznik	k1gInSc2	vznik
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
existují	existovat	k5eAaImIp3nP	existovat
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
teorie	teorie	k1gFnPc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
masivních	masivní	k2eAgInPc6d1	masivní
protoplanetárních	protoplanetární	k2eAgInPc6d1	protoplanetární
discích	disk	k1gInPc6	disk
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
shlukování	shlukování	k1gNnSc2	shlukování
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
nestabilita	nestabilita	k1gFnSc1	nestabilita
takového	takový	k3xDgInSc2	takový
disku	disk	k1gInSc2	disk
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
i	i	k9	i
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
tzv.	tzv.	kA	tzv.
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
řazeni	řadit	k5eAaImNgMnP	řadit
mezi	mezi	k7c4	mezi
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Slibnější	slibní	k2eAgFnSc7d2	slibní
možností	možnost	k1gFnSc7	možnost
se	se	k3xPyFc4	se
však	však	k9	však
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
jiný	jiný	k2eAgInSc4d1	jiný
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vznik	vznik	k1gInSc4	vznik
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
i	i	k9	i
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
málo	málo	k6eAd1	málo
hmotných	hmotný	k2eAgInPc6d1	hmotný
discích	disk	k1gInPc6	disk
(	(	kIx(	(
<g/>
dosahujících	dosahující	k2eAgFnPc2d1	dosahující
i	i	k9	i
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,1	[number]	k4	0,1
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
modelu	model	k1gInSc6	model
probíhá	probíhat	k5eAaImIp3nS	probíhat
tvorba	tvorba	k1gFnSc1	tvorba
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
fázích	fáze	k1gFnPc6	fáze
<g/>
:	:	kIx,	:
nejprve	nejprve	k6eAd1	nejprve
vzniká	vznikat	k5eAaImIp3nS	vznikat
akrecí	akrece	k1gFnSc7	akrece
jádro	jádro	k1gNnSc4	jádro
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
Zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
nabaluje	nabalovat	k5eAaImIp3nS	nabalovat
plyn	plyn	k1gInSc1	plyn
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
<g/>
Tvorba	tvorba	k1gFnSc1	tvorba
jader	jádro	k1gNnPc2	jádro
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
probíhá	probíhat	k5eAaImIp3nS	probíhat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
u	u	k7c2	u
planet	planeta	k1gFnPc2	planeta
terestrických	terestrický	k2eAgInPc2d1	terestrický
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
procházejí	procházet	k5eAaImIp3nP	procházet
obdobím	období	k1gNnPc3	období
překotného	překotný	k2eAgInSc2d1	překotný
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
navazuje	navazovat	k5eAaImIp3nS	navazovat
pomalejší	pomalý	k2eAgInSc1d2	pomalejší
růst	růst	k1gInSc1	růst
oligarchický	oligarchický	k2eAgInSc1d1	oligarchický
<g/>
.	.	kIx.	.
</s>
<s>
Nenastává	nastávat	k5eNaImIp3nS	nastávat
zde	zde	k6eAd1	zde
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
období	období	k1gNnSc3	období
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
vnějších	vnější	k2eAgFnPc6d1	vnější
částech	část	k1gFnPc6	část
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
srážky	srážka	k1gFnPc1	srážka
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
složení	složení	k1gNnSc1	složení
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
za	za	k7c7	za
sněžnou	sněžný	k2eAgFnSc7d1	sněžná
čárou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
kamenné	kamenný	k2eAgInPc4d1	kamenný
materiály	materiál	k1gInPc4	materiál
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
hmotnost	hmotnost	k1gFnSc1	hmotnost
planetesimál	planetesimála	k1gFnPc2	planetesimála
asi	asi	k9	asi
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
discích	disk	k1gInPc6	disk
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
možnou	možný	k2eAgFnSc7d1	možná
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ještě	ještě	k6eAd1	ještě
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tvorbu	tvorba	k1gFnSc4	tvorba
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
5	[number]	k4	5
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Jupiteru	Jupiter	k1gInSc2	Jupiter
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgFnP	moct
během	během	k7c2	během
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgInPc4	který
již	již	k6eAd1	již
plynné	plynný	k2eAgInPc4d1	plynný
disky	disk	k1gInPc4	disk
kolem	kolem	k7c2	kolem
hvězd	hvězda	k1gFnPc2	hvězda
velikosti	velikost	k1gFnSc2	velikost
Slunce	slunce	k1gNnSc2	slunce
zanikají	zanikat	k5eAaImIp3nP	zanikat
<g/>
)	)	kIx)	)
vzniknout	vzniknout	k5eAaPmF	vzniknout
pouze	pouze	k6eAd1	pouze
planetární	planetární	k2eAgNnPc1d1	planetární
jádra	jádro	k1gNnPc1	jádro
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
Zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nP	by
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
nepostačovalo	postačovat	k5eNaImAgNnS	postačovat
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
desetinásobné	desetinásobný	k2eAgNnSc1d1	desetinásobné
zvětšení	zvětšení	k1gNnSc1	zvětšení
potřebné	potřebný	k2eAgFnSc2d1	potřebná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
migrace	migrace	k1gFnSc1	migrace
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
srážet	srážet	k5eAaImF	srážet
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
planetesimál	planetesimát	k5eAaPmAgMnS	planetesimát
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
také	také	k9	také
stupňování	stupňování	k1gNnSc4	stupňování
akrece	akrece	k1gFnSc2	akrece
vlivem	vlivem	k7c2	vlivem
odporu	odpor	k1gInSc2	odpor
plynu	plyn	k1gInSc2	plyn
obklopujícího	obklopující	k2eAgInSc2d1	obklopující
planetární	planetární	k2eAgFnSc4d1	planetární
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgInPc2	tento
faktorů	faktor	k1gInPc2	faktor
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vznik	vznik	k1gInSc4	vznik
jader	jádro	k1gNnPc2	jádro
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
nebo	nebo	k8xC	nebo
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
planet	planeta	k1gFnPc2	planeta
jako	jako	k8xS	jako
Uran	Uran	k1gInSc1	Uran
nebo	nebo	k8xC	nebo
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
složitější	složitý	k2eAgMnSc1d2	složitější
a	a	k8xC	a
astronomové	astronom	k1gMnPc1	astronom
jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
ve	v	k7c6	v
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
20	[number]	k4	20
až	až	k8xS	až
30	[number]	k4	30
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
od	od	k7c2	od
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
proto	proto	k6eAd1	proto
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
někde	někde	k6eAd1	někde
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
Saturnem	Saturn	k1gMnSc7	Saturn
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
migrovaly	migrovat	k5eAaImAgInP	migrovat
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
současné	současný	k2eAgFnPc4d1	současná
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
jednou	jeden	k4xCgFnSc7	jeden
planetární	planetární	k2eAgFnSc7d1	planetární
jádra	jádro	k1gNnSc2	jádro
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
hmotnosti	hmotnost	k1gFnPc4	hmotnost
(	(	kIx(	(
<g/>
asi	asi	k9	asi
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
násobku	násobek	k1gInSc2	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nabalovat	nabalovat	k5eAaImF	nabalovat
okolní	okolní	k2eAgInSc4d1	okolní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pomalý	pomalý	k2eAgInSc4d1	pomalý
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
milionech	milion	k4xCgInPc6	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
jejich	jejich	k3xOp3gFnSc1	jejich
hmotnost	hmotnost	k1gFnSc1	hmotnost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
30	[number]	k4	30
<g/>
násobku	násobek	k1gInSc2	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dramaticky	dramaticky	k6eAd1	dramaticky
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgMnPc2d1	zbývající
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
planety	planeta	k1gFnSc2	planeta
nasbíraly	nasbírat	k5eAaPmAgInP	nasbírat
již	již	k6eAd1	již
jen	jen	k9	jen
během	během	k7c2	během
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
akrece	akrece	k1gFnSc1	akrece
plynu	plyn	k1gInSc2	plyn
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
veškerý	veškerý	k3xTgInSc4	veškerý
plyn	plyn	k1gInSc4	plyn
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vyčerpán	vyčerpán	k2eAgMnSc1d1	vyčerpán
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
vzniká	vznikat	k5eAaImIp3nS	vznikat
prázdná	prázdný	k2eAgFnSc1d1	prázdná
mezera	mezera	k1gFnSc1	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
Uran	Uran	k1gMnSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gMnSc1	Neptun
jádra	jádro	k1gNnSc2	jádro
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ustrnula	ustrnout	k5eAaPmAgFnS	ustrnout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
tvořit	tvořit	k5eAaImF	tvořit
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
v	v	k7c6	v
disku	disk	k1gInSc6	disk
nezbýval	zbývat	k5eNaImAgMnS	zbývat
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
fázi	fáze	k1gFnSc6	fáze
překotné	překotný	k2eAgFnSc2d1	překotná
akrece	akrece	k1gFnSc2	akrece
plynu	plyn	k1gInSc2	plyn
následuje	následovat	k5eAaImIp3nS	následovat
období	období	k1gNnSc4	období
migrace	migrace	k1gFnSc2	migrace
nově	nově	k6eAd1	nově
vytvořených	vytvořený	k2eAgMnPc2d1	vytvořený
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
planety	planeta	k1gFnPc1	planeta
mohou	moct	k5eAaImIp3nP	moct
ještě	ještě	k6eAd1	ještě
pomalu	pomalu	k6eAd1	pomalu
nabalovat	nabalovat	k5eAaImF	nabalovat
další	další	k2eAgInSc4d1	další
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
planety	planeta	k1gFnPc1	planeta
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
plyn	plyn	k1gInSc4	plyn
nabalují	nabalovat	k5eAaImIp3nP	nabalovat
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
disku	disk	k1gInSc6	disk
mezera	mezera	k1gFnSc1	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
působením	působení	k1gNnSc7	působení
planety	planeta	k1gFnSc2	planeta
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
prázdné	prázdný	k2eAgFnSc6d1	prázdná
mezeře	mezera	k1gFnSc6	mezera
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
zbývajícího	zbývající	k2eAgInSc2d1	zbývající
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
disk	disk	k1gInSc1	disk
zcela	zcela	k6eAd1	zcela
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
když	když	k8xS	když
planeta	planeta	k1gFnSc1	planeta
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
jeho	jeho	k3xOp3gInPc4	jeho
okraje	okraj	k1gInPc4	okraj
<g/>
,	,	kIx,	,
migrace	migrace	k1gFnSc1	migrace
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
končí	končit	k5eAaImIp3nS	končit
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
okraji	okraj	k1gInSc6	okraj
disku	disk	k1gInSc2	disk
svou	svůj	k3xOyFgFnSc4	svůj
pouť	pouť	k1gFnSc4	pouť
tzv.	tzv.	kA	tzv.
horké	horký	k2eAgInPc1d1	horký
Jupitery	Jupiter	k1gInPc1	Jupiter
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
velikosti	velikost	k1gFnSc2	velikost
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
chladných	chladný	k2eAgFnPc2d1	chladná
oblastí	oblast	k1gFnPc2	oblast
planetární	planetární	k2eAgFnSc2d1	planetární
soustavy	soustava	k1gFnSc2	soustava
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
ležící	ležící	k2eAgFnPc1d1	ležící
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
však	však	k9	však
tým	tým	k1gInSc1	tým
astronomů	astronom	k1gMnPc2	astronom
vedený	vedený	k2eAgInSc1d1	vedený
Andrew	Andrew	k1gMnSc7	Andrew
Cameronem	Cameron	k1gMnSc7	Cameron
a	a	k8xC	a
Didier	Didier	k1gInSc4	Didier
Quelozem	Queloz	k1gInSc7	Queloz
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
jím	jíst	k5eAaImIp1nS	jíst
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
vzorku	vzorek	k1gInSc2	vzorek
27	[number]	k4	27
horkých	horký	k2eAgInPc2d1	horký
Jupiterů	Jupiter	k1gInPc2	Jupiter
jich	on	k3xPp3gMnPc2	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
dráhy	dráha	k1gFnPc4	dráha
výrazně	výrazně	k6eAd1	výrazně
nakloněny	naklonit	k5eAaPmNgFnP	naklonit
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
pohyb	pohyb	k1gInSc4	pohyb
šesti	šest	k4xCc2	šest
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
retrográdní	retrográdní	k2eAgMnSc1d1	retrográdní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
rotuje	rotovat	k5eAaImIp3nS	rotovat
centrální	centrální	k2eAgFnSc1d1	centrální
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
dráhy	dráha	k1gFnPc1	dráha
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
migrací	migrace	k1gFnSc7	migrace
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
gravitační	gravitační	k2eAgFnSc7d1	gravitační
interakcí	interakce	k1gFnSc7	interakce
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
prachoplynovým	prachoplynův	k2eAgInSc7d1	prachoplynův
diskem	disk	k1gInSc7	disk
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
exoplanety	exoplanet	k1gInPc1	exoplanet
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
navedeny	navést	k5eAaPmNgInP	navést
vlivem	vlivem	k7c2	vlivem
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
poruch	porucha	k1gFnPc2	porucha
způsobených	způsobený	k2eAgFnPc2d1	způsobená
jinou	jiný	k2eAgFnSc7d1	jiná
planetou	planeta	k1gFnSc7	planeta
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
blízkou	blízký	k2eAgFnSc7d1	blízká
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
proces	proces	k1gInSc1	proces
by	by	kYmCp3nP	by
patrně	patrně	k6eAd1	patrně
trval	trvat	k5eAaImAgInS	trvat
ještě	ještě	k9	ještě
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dosud	dosud	k6eAd1	dosud
uvažované	uvažovaný	k2eAgInPc1d1	uvažovaný
způsoby	způsob	k1gInPc1	způsob
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
až	až	k8xS	až
stovky	stovka	k1gFnPc4	stovka
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
menší	malý	k2eAgFnPc1d2	menší
planety	planeta	k1gFnPc1	planeta
by	by	kYmCp3nP	by
během	během	k7c2	během
něho	on	k3xPp3gInSc2	on
byly	být	k5eAaImAgInP	být
zřejmě	zřejmě	k6eAd1	zřejmě
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
zcela	zcela	k6eAd1	zcela
vymeteny	vymeten	k2eAgFnPc1d1	vymetena
<g/>
.	.	kIx.	.
<g/>
Obří	obří	k2eAgFnPc1d1	obří
planety	planeta	k1gFnPc1	planeta
mohou	moct	k5eAaImIp3nP	moct
významně	významně	k6eAd1	významně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
i	i	k9	i
tvorbu	tvorba	k1gFnSc4	tvorba
planet	planeta	k1gFnPc2	planeta
terestrických	terestrický	k2eAgInPc2d1	terestrický
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
obrů	obr	k1gMnPc2	obr
totiž	totiž	k9	totiž
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
výstřednost	výstřednost	k1gFnSc4	výstřednost
a	a	k8xC	a
sklon	sklon	k1gInSc4	sklon
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
planetesimál	planetesimála	k1gFnPc2	planetesimála
a	a	k8xC	a
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
v	v	k7c6	v
terestrické	terestrický	k2eAgFnSc6d1	terestrická
oblasti	oblast	k1gFnSc6	oblast
planetární	planetární	k2eAgFnSc2d1	planetární
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
do	do	k7c2	do
4	[number]	k4	4
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
příliš	příliš	k6eAd1	příliš
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
by	by	kYmCp3nP	by
akreci	akrece	k1gFnSc3	akrece
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
planet	planeta	k1gFnPc2	planeta
zpomalit	zpomalit	k5eAaPmF	zpomalit
nebo	nebo	k8xC	nebo
i	i	k9	i
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
však	však	k9	však
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
fáze	fáze	k1gFnSc2	fáze
oligarchického	oligarchický	k2eAgInSc2d1	oligarchický
růstu	růst	k1gInSc2	růst
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
srážky	srážka	k1gFnPc1	srážka
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nabudou	nabýt	k5eAaPmIp3nP	nabýt
mnohem	mnohem	k6eAd1	mnohem
dramatičtější	dramatický	k2eAgFnPc1d2	dramatičtější
podoby	podoba	k1gFnPc1	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
sníží	snížit	k5eAaPmIp3nS	snížit
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
vlivem	vlivem	k7c2	vlivem
terestrické	terestrický	k2eAgFnSc2d1	terestrická
planety	planeta	k1gFnSc2	planeta
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
blíže	blízce	k6eAd2	blízce
centrální	centrální	k2eAgFnSc3d1	centrální
hvězdě	hvězda	k1gFnSc3	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
však	však	k9	však
byl	být	k5eAaImAgInS	být
vliv	vliv	k1gInSc1	vliv
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
byl	být	k5eAaImAgInS	být
vliv	vliv	k1gInSc1	vliv
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
omezen	omezit	k5eAaPmNgMnS	omezit
jejich	jejich	k3xOp3gMnSc7	jejich
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
<g/>
Oblast	oblast	k1gFnSc1	oblast
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
výstřednost	výstřednost	k1gFnSc4	výstřednost
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
protoplanet	protoplaneta	k1gFnPc2	protoplaneta
vzrůst	vzrůst	k1gInSc1	vzrůst
tak	tak	k6eAd1	tak
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
až	až	k9	až
do	do	k7c2	do
těsné	těsný	k2eAgFnSc2d1	těsná
blízkostí	blízkost	k1gFnPc2	blízkost
některého	některý	k3yIgMnSc2	některý
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobným	pravděpodobný	k2eAgInSc7d1	pravděpodobný
důsledkem	důsledek	k1gInSc7	důsledek
takového	takový	k3xDgNnSc2	takový
setkání	setkání	k1gNnSc2	setkání
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
vystřelení	vystřelení	k1gNnSc1	vystřelení
tělesa	těleso	k1gNnSc2	těleso
ven	ven	k6eAd1	ven
z	z	k7c2	z
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
nezbudou	zbýt	k5eNaPmIp3nP	zbýt
žádné	žádný	k3yNgInPc4	žádný
protoplanety	protoplanet	k1gInPc4	protoplanet
<g/>
,	,	kIx,	,
nevytvoří	vytvořit	k5eNaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ani	ani	k8xC	ani
žádné	žádný	k3yNgFnSc2	žádný
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
účinkem	účinek	k1gInSc7	účinek
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zůstane	zůstat	k5eAaPmIp3nS	zůstat
určitý	určitý	k2eAgInSc1d1	určitý
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
planetesimál	planetesimát	k5eAaPmAgInS	planetesimát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
sami	sám	k3xTgMnPc1	sám
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
protoplanet	protoplanet	k1gInSc1	protoplanet
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
sami	sám	k3xTgMnPc1	sám
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
vyčistit	vyčistit	k5eAaPmF	vyčistit
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
všech	všecek	k3xTgFnPc2	všecek
přeživších	přeživší	k2eAgFnPc2d1	přeživší
planetesimál	planetesimála	k1gFnPc2	planetesimála
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
protoplanety	protoplanet	k1gInPc1	protoplanet
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
obřími	obří	k2eAgFnPc7d1	obří
planetami	planeta	k1gFnPc7	planeta
stihly	stihnout	k5eAaPmAgFnP	stihnout
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
vymrštěním	vymrštění	k1gNnSc7	vymrštění
99	[number]	k4	99
%	%	kIx~	%
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vymést	vymést	k5eAaPmF	vymést
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
pás	pás	k1gInSc4	pás
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
pásu	pás	k1gInSc3	pás
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nebular	Nebular	k1gInSc1	Nebular
hypothesis	hypothesis	k1gFnSc2	hypothesis
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
</s>
</p>
<p>
<s>
Mlhovina	mlhovina	k1gFnSc1	mlhovina
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
</s>
</p>
<p>
<s>
Scénář	scénář	k1gInSc1	scénář
skákajícího	skákající	k2eAgMnSc2d1	skákající
Jupitera	Jupiter	k1gMnSc2	Jupiter
</s>
</p>
