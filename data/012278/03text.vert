<p>
<s>
Pieta	pieta	k1gFnSc1	pieta
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc4	zobrazení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
klíně	klín	k1gInSc6	klín
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
Krista	Kristus	k1gMnSc2	Kristus
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
snětí	snětí	k1gNnSc6	snětí
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
horizontální	horizontální	k2eAgFnSc1d1	horizontální
nebo	nebo	k8xC	nebo
vertikální	vertikální	k2eAgFnSc1d1	vertikální
<g/>
)	)	kIx)	)
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
Oplakávání	oplakávání	k1gNnSc2	oplakávání
izolováním	izolování	k1gNnSc7	izolování
obou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ikonografie	ikonografie	k1gFnSc2	ikonografie
==	==	k?	==
</s>
</p>
<p>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
Piety	pieta	k1gFnSc2	pieta
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ikonografického	ikonografický	k2eAgInSc2d1	ikonografický
cyklu	cyklus	k1gInSc2	cyklus
výjevů	výjev	k1gInPc2	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
cyklu	cyklus	k1gInSc2	cyklus
výjevů	výjev	k1gInPc2	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Kristova	Kristův	k2eAgInSc2d1	Kristův
ikonografického	ikonografický	k2eAgInSc2d1	ikonografický
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Pašijového	pašijový	k2eAgInSc2d1	pašijový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
části	část	k1gFnPc1	část
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
jeho	jeho	k3xOp3gNnPc2	jeho
Ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c4	mezi
výjevy	výjev	k1gInPc4	výjev
Snímání	snímání	k1gNnSc2	snímání
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
Kladení	kladení	k1gNnSc2	kladení
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
původně	původně	k6eAd1	původně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
Oplakávání	oplakávání	k1gNnSc2	oplakávání
(	(	kIx(	(
<g/>
lamentace	lamentace	k1gFnSc1	lamentace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
tradici	tradice	k1gFnSc6	tradice
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
standardním	standardní	k2eAgFnPc3d1	standardní
zobrazování	zobrazování	k1gNnSc2	zobrazování
tohoto	tento	k3xDgInSc2	tento
výjevu	výjev	k1gInSc2	výjev
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
Snímání	snímání	k1gNnSc6	snímání
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazněním	zdůraznění	k1gNnSc7	zdůraznění
motivu	motiv	k1gInSc2	motiv
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
postupným	postupný	k2eAgNnSc7d1	postupné
vyloučením	vyloučení	k1gNnSc7	vyloučení
ostatních	ostatní	k2eAgFnPc2d1	ostatní
postav	postava	k1gFnPc2	postava
se	se	k3xPyFc4	se
z	z	k7c2	z
motivu	motiv	k1gInSc2	motiv
Oplakávání	oplakávání	k1gNnSc2	oplakávání
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
motiv	motiv	k1gInSc4	motiv
Piety	pieta	k1gFnSc2	pieta
<g/>
.	.	kIx.	.
</s>
<s>
Pieta	pieta	k1gFnSc1	pieta
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Oplakávání	oplakávání	k1gNnSc4	oplakávání
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
zastavení	zastavení	k1gNnSc3	zastavení
křížové	křížový	k2eAgFnSc2d1	křížová
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
také	také	k9	také
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
bolestí	bolest	k1gFnPc2	bolest
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pieta	pieta	k1gFnSc1	pieta
v	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
==	==	k?	==
</s>
</p>
<p>
<s>
Pieta	pieta	k1gFnSc1	pieta
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sousoší	sousoší	k1gNnSc1	sousoší
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
časté	častý	k2eAgFnPc1d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Piety	pieta	k1gFnSc2	pieta
je	být	k5eAaImIp3nS	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
motivem	motiv	k1gInSc7	motiv
středoevropského	středoevropský	k2eAgNnSc2d1	středoevropské
gotického	gotický	k2eAgNnSc2d1	gotické
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sousoší	sousoší	k1gNnSc1	sousoší
Piety	pieta	k1gFnSc2	pieta
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
dochovaným	dochovaný	k2eAgInPc3d1	dochovaný
dílům	díl	k1gInPc3	díl
patří	patřit	k5eAaImIp3nS	patřit
piety	pieta	k1gFnPc1	pieta
z	z	k7c2	z
Radolfzellu	Radolfzell	k1gInSc2	Radolfzell
a	a	k8xC	a
Meersburgu	Meersburg	k1gInSc2	Meersburg
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Gotické	gotický	k2eAgFnPc1d1	gotická
piety	pieta	k1gFnPc1	pieta
prošly	projít	k5eAaPmAgFnP	projít
výrazným	výrazný	k2eAgInSc7d1	výrazný
výrazovým	výrazový	k2eAgInSc7d1	výrazový
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
jej	on	k3xPp3gInSc4	on
dělíme	dělit	k5eAaImIp1nP	dělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
rozlišení	rozlišení	k1gNnSc1	rozlišení
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
těla	tělo	k1gNnSc2	tělo
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc4d1	používán
alternativní	alternativní	k2eAgInPc4d1	alternativní
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Heroické	heroický	k2eAgFnSc2d1	heroická
(	(	kIx(	(
<g/>
mystické	mystický	k2eAgFnSc2d1	mystická
<g/>
,	,	kIx,	,
vertikální	vertikální	k2eAgFnSc2d1	vertikální
<g/>
)	)	kIx)	)
piety	pieta	k1gFnSc2	pieta
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
vertikální	vertikální	k2eAgNnSc4d1	vertikální
umístění	umístění	k1gNnSc4	umístění
těla	tělo	k1gNnSc2	tělo
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
velká	velká	k1gFnSc1	velká
až	až	k6eAd1	až
nadživotní	nadživotní	k2eAgFnSc1d1	nadživotní
velikost	velikost	k1gFnSc1	velikost
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Horizontální	horizontální	k2eAgFnSc2d1	horizontální
piety	pieta	k1gFnSc2	pieta
===	===	k?	===
</s>
</p>
<p>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
přísnějším	přísný	k2eAgInSc7d2	přísnější
skladebným	skladebný	k2eAgInSc7d1	skladebný
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
zdrženlivostí	zdrženlivost	k1gFnSc7	zdrženlivost
výrazu	výraz	k1gInSc2	výraz
a	a	k8xC	a
větší	veliký	k2eAgFnSc7d2	veliký
plastičností	plastičnost	k1gFnSc7	plastičnost
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
Krista	Kristus	k1gMnSc2	Kristus
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
téměř	téměř	k6eAd1	téměř
horizontálně	horizontálně	k6eAd1	horizontálně
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
otočená	otočený	k2eAgFnSc1d1	otočená
k	k	k7c3	k
divákovi	divák	k1gMnSc3	divák
a	a	k8xC	a
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
horizontální	horizontální	k2eAgFnSc6d1	horizontální
poloze	poloha	k1gFnSc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Kolena	koleno	k1gNnPc1	koleno
jsou	být	k5eAaImIp3nP	být
zalomena	zalomen	k2eAgNnPc1d1	zalomen
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
<g/>
,	,	kIx,	,
ruce	ruka	k1gFnPc4	ruka
složené	složený	k2eAgFnPc4d1	složená
podélně	podélně	k6eAd1	podélně
<g/>
,	,	kIx,	,
levá	levá	k1gFnSc1	levá
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
pravé	pravý	k2eAgNnSc4d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
sedí	sedit	k5eAaImIp3nS	sedit
vzpřímeně	vzpřímeně	k6eAd1	vzpřímeně
<g/>
,	,	kIx,	,
trup	trup	k1gInSc1	trup
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
nepatrně	nepatrně	k6eAd1	nepatrně
vykloněný	vykloněný	k2eAgMnSc1d1	vykloněný
<g/>
.	.	kIx.	.
</s>
<s>
Expresivita	expresivita	k1gFnSc1	expresivita
výrazu	výraz	k1gInSc2	výraz
je	být	k5eAaImIp3nS	být
potlačená	potlačený	k2eAgFnSc1d1	potlačená
<g/>
,	,	kIx,	,
důležitější	důležitý	k2eAgFnSc1d2	důležitější
je	být	k5eAaImIp3nS	být
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Piety	pieta	k1gFnPc4	pieta
krásného	krásný	k2eAgInSc2d1	krásný
slohu	sloh	k1gInSc2	sloh
(	(	kIx(	(
<g/>
diagonální	diagonální	k2eAgMnSc1d1	diagonální
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Krásný	krásný	k2eAgInSc1d1	krásný
sloh	sloh	k1gInSc1	sloh
se	se	k3xPyFc4	se
v	v	k7c6	v
gotickém	gotický	k2eAgNnSc6d1	gotické
sochařství	sochařství	k1gNnSc6	sochařství
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1390	[number]	k4	1390
a	a	k8xC	a
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sochy	socha	k1gFnPc4	socha
tohoto	tento	k3xDgInSc2	tento
slohu	sloh	k1gInSc2	sloh
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
rytmický	rytmický	k2eAgInSc1d1	rytmický
pohyb	pohyb	k1gInSc1	pohyb
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
idealizace	idealizace	k1gFnSc2	idealizace
vzhledu	vzhled	k1gInSc2	vzhled
(	(	kIx(	(
<g/>
rozvoj	rozvoj	k1gInSc1	rozvoj
smyslové	smyslový	k2eAgFnSc2d1	smyslová
krásy	krása	k1gFnSc2	krása
jako	jako	k8xS	jako
projevu	projev	k1gInSc2	projev
božské	božský	k2eAgFnSc2d1	božská
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Piety	pieta	k1gFnPc1	pieta
krásného	krásný	k2eAgInSc2d1	krásný
slohu	sloh	k1gInSc2	sloh
bývají	bývat	k5eAaImIp3nP	bývat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
diagonální	diagonální	k2eAgInSc1d1	diagonální
-	-	kIx~	-
tělo	tělo	k1gNnSc1	tělo
Krista	Kristus	k1gMnSc2	Kristus
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Mariině	Mariin	k2eAgInSc6d1	Mariin
klíně	klín	k1gInSc6	klín
šikmo	šikmo	k6eAd1	šikmo
<g/>
,	,	kIx,	,
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
výrazně	výrazně	k6eAd1	výrazně
zakloněnou	zakloněný	k2eAgFnSc7d1	zakloněná
a	a	k8xC	a
jen	jen	k6eAd1	jen
zčásti	zčásti	k6eAd1	zčásti
pootočenou	pootočený	k2eAgFnSc4d1	pootočená
k	k	k7c3	k
divákovi	divák	k1gMnSc3	divák
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mírněji	mírně	k6eAd2	mírně
ohnutými	ohnutý	k2eAgNnPc7d1	ohnuté
koleny	koleno	k1gNnPc7	koleno
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
celkový	celkový	k2eAgInSc4d1	celkový
zvlněný	zvlněný	k2eAgInSc4d1	zvlněný
dynamický	dynamický	k2eAgInSc4d1	dynamický
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Ruce	ruka	k1gFnPc1	ruka
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
překřížené	překřížený	k2eAgFnPc1d1	překřížená
<g/>
,	,	kIx,	,
pravá	pravá	k1gFnSc1	pravá
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
levé	levý	k2eAgNnSc4d1	levé
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
výrazněji	výrazně	k6eAd2	výrazně
odkloněná	odkloněný	k2eAgFnSc1d1	odkloněná
<g/>
.	.	kIx.	.
</s>
<s>
Výrazy	výraz	k1gInPc1	výraz
obličeje	obličej	k1gInSc2	obličej
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
dramatičtější	dramatický	k2eAgFnPc1d2	dramatičtější
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc1d1	plná
citů	cit	k1gInPc2	cit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Renesanční	renesanční	k2eAgFnSc2d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc2d1	barokní
piety	pieta	k1gFnSc2	pieta
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
nejkrásnější	krásný	k2eAgFnSc4d3	nejkrásnější
pietu	pieta	k1gFnSc4	pieta
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
pieta	pieta	k1gFnSc1	pieta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
renesanční	renesanční	k2eAgMnSc1d1	renesanční
sochař	sochař	k1gMnSc1	sochař
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarroti	Buonarrot	k1gMnPc1	Buonarrot
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Piety	pieta	k1gFnSc2	pieta
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
==	==	k?	==
</s>
</p>
<p>
<s>
Piety	pieta	k1gFnPc1	pieta
nejsou	být	k5eNaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
převládá	převládat	k5eAaImIp3nS	převládat
spíše	spíše	k9	spíše
šířeji	šířej	k1gInSc3	šířej
zachycené	zachycený	k2eAgNnSc1d1	zachycené
Oplakávání	oplakávání	k1gNnSc1	oplakávání
či	či	k8xC	či
kombinace	kombinace	k1gFnSc1	kombinace
výjevu	výjev	k1gInSc2	výjev
se	s	k7c7	s
Snímáním	snímání	k1gNnSc7	snímání
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Piety	pieta	k1gFnSc2	pieta
milostné	milostný	k2eAgFnSc2d1	milostná
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Piety	pieta	k1gFnSc2	pieta
milostné	milostný	k2eAgFnPc1d1	milostná
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
Pieta	pieta	k1gFnSc1	pieta
Bohosudovská	Bohosudovský	k2eAgFnSc1d1	Bohosudovská
-	-	kIx~	-
Bohosudov	Bohosudovo	k1gNnPc2	Bohosudovo
</s>
</p>
<p>
<s>
Pieta	pieta	k1gFnSc1	pieta
Jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
-	-	kIx~	-
Jihlava	Jihlava	k1gFnSc1	Jihlava
</s>
</p>
<p>
<s>
Pieta	pieta	k1gFnSc1	pieta
Přeštická	přeštický	k2eAgFnSc1d1	Přeštická
-	-	kIx~	-
Přeštice	Přeštice	k1gFnPc4	Přeštice
</s>
</p>
<p>
<s>
Pieta	pieta	k1gFnSc1	pieta
Svatojakubská	svatojakubský	k2eAgFnSc1d1	Svatojakubská
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Přenesený	přenesený	k2eAgInSc4d1	přenesený
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obecné	obecný	k2eAgNnSc4d1	obecné
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
zbožnou	zbožný	k2eAgFnSc4d1	zbožná
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
uctivý	uctivý	k2eAgInSc4d1	uctivý
ohled	ohled	k1gInSc4	ohled
<g/>
,	,	kIx,	,
nepředstírané	předstíraný	k2eNgNnSc4d1	nepředstírané
uctívání	uctívání	k1gNnSc4	uctívání
apod.	apod.	kA	apod.
</s>
</p>
