<s>
Pieta	pieta	k1gFnSc1	pieta
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc4	zobrazení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
klíně	klín	k1gInSc6	klín
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
Krista	Kristus	k1gMnSc2	Kristus
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
snětí	snětí	k1gNnSc6	snětí
z	z	k7c2	z
kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
horizontální	horizontální	k2eAgFnSc1d1	horizontální
nebo	nebo	k8xC	nebo
vertikální	vertikální	k2eAgFnSc1d1	vertikální
<g/>
)	)	kIx)	)
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
Oplakávání	oplakávání	k1gNnSc2	oplakávání
izolováním	izolování	k1gNnSc7	izolování
obou	dva	k4xCgFnPc2	dva
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
