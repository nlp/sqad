<s>
Struma	struma	k1gFnSc1	struma
popř.	popř.	kA	popř.
Strymon	Strymon	k1gNnSc1	Strymon
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
С	С	k?	С
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Σ	Σ	k?	Σ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
(	(	kIx(	(
<g/>
oblasti	oblast	k1gFnSc2	oblast
Blagoevgradská	Blagoevgradský	k2eAgFnSc1d1	Blagoevgradský
<g/>
,	,	kIx,	,
Pernická	Pernický	k2eAgFnSc1d1	Pernická
<g/>
,	,	kIx,	,
Kjustendilská	Kjustendilský	k2eAgFnSc1d1	Kjustendilský
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Řecka	Řecko	k1gNnSc2	Řecko
(	(	kIx(	(
<g/>
kraje	kraj	k1gInSc2	kraj
Střední	střední	k2eAgFnSc2d1	střední
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc2d1	východní
Makedonie	Makedonie	k1gFnSc2	Makedonie
a	a	k8xC	a
Thrákie	Thrákie	k1gFnSc2	Thrákie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
415	[number]	k4	415
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
290	[number]	k4	290
km	km	kA	km
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
činí	činit	k5eAaImIp3nS	činit
17	[number]	k4	17
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
svahu	svah	k1gInSc6	svah
masivu	masiv	k1gInSc2	masiv
Vitoša	Vitoš	k1gInSc2	Vitoš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
horský	horský	k2eAgInSc4d1	horský
tok	tok	k1gInSc4	tok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
střídá	střídat	k5eAaImIp3nS	střídat
hluboké	hluboký	k2eAgInPc4d1	hluboký
a	a	k8xC	a
úzké	úzký	k2eAgInPc4d1	úzký
soutěsky	soutěsk	k1gInPc4	soutěsk
s	s	k7c7	s
mezihorskými	mezihorský	k2eAgFnPc7d1	Mezihorská
kotlinami	kotlina	k1gFnPc7	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Řecka	Řecko	k1gNnSc2	Řecko
pak	pak	k6eAd1	pak
protéká	protékat	k5eAaImIp3nS	protékat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
údolí	údolí	k1gNnSc6	údolí
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
Strymonikos	Strymonikosa	k1gFnPc2	Strymonikosa
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
průtokům	průtok	k1gInPc3	průtok
dochází	docházet	k5eAaImIp3nS	docházet
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
června	červen	k1gInSc2	červen
a	a	k8xC	a
k	k	k7c3	k
nejnižším	nízký	k2eAgNnPc3d3	nejnižší
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
poblíž	poblíž	k6eAd1	poblíž
bulharsko-řecké	bulharsko-řecký	k2eAgFnSc2d1	bulharsko-řecký
hranice	hranice	k1gFnSc2	hranice
činí	činit	k5eAaImIp3nS	činit
80	[number]	k4	80
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
maximální	maximální	k2eAgInSc4d1	maximální
500	[number]	k4	500
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
vybudována	vybudován	k2eAgFnSc1d1	vybudována
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
Studena	studeno	k1gNnSc2	studeno
a	a	k8xC	a
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
byly	být	k5eAaImAgInP	být
taktéž	taktéž	k?	taktéž
vybudovány	vybudován	k2eAgInPc4d1	vybudován
na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
přítocích	přítok	k1gInPc6	přítok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
leží	ležet	k5eAaImIp3nP	ležet
města	město	k1gNnPc1	město
Kjustendil	Kjustendil	k1gMnSc1	Kjustendil
<g/>
,	,	kIx,	,
Blagoevgrad	Blagoevgrad	k1gInSc1	Blagoevgrad
(	(	kIx(	(
<g/>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Seres	Seres	k1gInSc1	Seres
(	(	kIx(	(
<g/>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
С	С	k?	С
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Struma	struma	k1gFnSc1	struma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
