<s>
Červen	červen	k1gInSc1	červen
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgInSc4	šestý
měsíc	měsíc	k1gInSc4	měsíc
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
léto	léto	k1gNnSc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
červen	červen	k1gInSc1	červen
je	být	k5eAaImIp3nS	být
odvozováno	odvozovat	k5eAaImNgNnS	odvozovat
rozmanitě	rozmanitě	k6eAd1	rozmanitě
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Rakowiecki	Rakowieck	k1gFnSc2	Rakowieck
a	a	k8xC	a
Leška	Lešek	k1gInSc2	Lešek
viděli	vidět	k5eAaImAgMnP	vidět
jeho	on	k3xPp3gInSc4	on
původ	původ	k1gInSc4	původ
v	v	k7c6	v
červenání	červenání	k1gNnSc6	červenání
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
jahod	jahoda	k1gFnPc2	jahoda
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Partl	Partl	k1gMnSc1	Partl
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
slovo	slovo	k1gNnSc4	slovo
od	od	k7c2	od
červenosti	červenost	k1gFnSc2	červenost
vůbec	vůbec	k9	vůbec
nebo	nebo	k8xC	nebo
od	od	k7c2	od
červů	červ	k1gMnPc2	červ
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
dělají	dělat	k5eAaImIp3nP	dělat
škody	škoda	k1gFnPc4	škoda
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
štěpích	štěpí	k1gNnPc6	štěpí
a	a	k8xC	a
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
jméno	jméno	k1gNnSc4	jméno
měsíce	měsíc	k1gInSc2	měsíc
od	od	k7c2	od
sbírání	sbírání	k1gNnSc2	sbírání
červce	červec	k1gMnSc2	červec
<g/>
,	,	kIx,	,
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kteréhož	kteréhož	k?	kteréhož
se	se	k3xPyFc4	se
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Chladný	chladný	k2eAgInSc1d1	chladný
květen	květen	k1gInSc1	květen
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
vlažný	vlažný	k2eAgInSc1d1	vlažný
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
sýpky	sýpka	k1gFnPc4	sýpka
<g/>
,	,	kIx,	,
sudy	sud	k1gInPc4	sud
blažný	blažný	k2eAgInSc1d1	blažný
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
<g/>
-li	i	k?	-li
více	hodně	k6eAd2	hodně
sucho	sucho	k1gNnSc1	sucho
než	než	k8xS	než
mokro	mokro	k1gNnSc1	mokro
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
,	,	kIx,	,
urodí	urodit	k5eAaPmIp3nS	urodit
se	se	k3xPyFc4	se
hojnost	hojnost	k1gFnSc1	hojnost
dobrého	dobrý	k2eAgNnSc2d1	dobré
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
nedá	dát	k5eNaPmIp3nS	dát
do	do	k7c2	do
klasu	klas	k1gInSc2	klas
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
nažene	nahnat	k5eAaPmIp3nS	nahnat
v	v	k7c6	v
času	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
červen	červen	k1gInSc4	červen
teplem	teplo	k1gNnSc7	teplo
září	zář	k1gFnPc2	zář
<g/>
,	,	kIx,	,
takový	takový	k3xDgInSc1	takový
bude	být	k5eAaImBp3nS	být
i	i	k9	i
měsíc	měsíc	k1gInSc1	měsíc
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Jaký	jaký	k3yRgInSc1	jaký
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
takový	takový	k3xDgInSc4	takový
i	i	k8xC	i
prosinec	prosinec	k1gInSc4	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
červen	červen	k1gInSc1	červen
mírný	mírný	k2eAgInSc1d1	mírný
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
mráz	mráz	k1gInSc1	mráz
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
stálý	stálý	k2eAgInSc1d1	stálý
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
<g/>
.	.	kIx.	.
</s>
<s>
Jaká	jaký	k3yIgNnPc1	jaký
parna	parno	k1gNnPc1	parno
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
dostaví	dostavit	k5eAaPmIp3nS	dostavit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
i	i	k9	i
prosincové	prosincový	k2eAgInPc1d1	prosincový
mraky	mrak	k1gInPc1	mrak
postaví	postavit	k5eAaPmIp3nP	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
červený	červený	k2eAgInSc1d1	červený
jako	jako	k8xS	jako
z	z	k7c2	z
růže	růž	k1gFnSc2	růž
květ	květ	k1gInSc4	květ
Červnové	červnový	k2eAgNnSc1d1	červnové
večerní	večerní	k2eAgNnSc1d1	večerní
hřmění	hřmění	k1gNnSc1	hřmění
-	-	kIx~	-
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
raků	rak	k1gMnPc2	rak
nadělení	nadělení	k1gNnSc2	nadělení
<g/>
.	.	kIx.	.
</s>
<s>
Hřímá	hřímat	k5eAaImIp3nS	hřímat
<g/>
-li	i	k?	-li
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
zvede	zvést	k5eAaPmIp3nS	zvést
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k2eAgMnSc1d1	červen
studený	studený	k2eAgMnSc1d1	studený
-	-	kIx~	-
sedlák	sedlák	k1gMnSc1	sedlák
krčí	krčit	k5eAaImIp3nP	krčit
rameny	rameno	k1gNnPc7	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
severní	severní	k2eAgInPc1d1	severní
větry	vítr	k1gInPc1	vítr
vějí	vát	k5eAaImIp3nP	vát
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
se	se	k3xPyFc4	se
bouřky	bouřka	k1gFnPc1	bouřka
opozdějí	opozdět	k5eAaImIp3nP	opozdět
<g/>
.	.	kIx.	.
</s>
<s>
Netřeba	netřeba	k6eAd1	netřeba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
o	o	k7c4	o
déšť	déšť	k1gInSc4	déšť
prositi	prosit	k5eAaImF	prosit
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
začne	začít	k5eAaPmIp3nS	začít
kositi	kosit	k5eAaImF	kosit
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
červen	červen	k1gInSc1	červen
mokrý	mokrý	k2eAgInSc1d1	mokrý
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc1	obilí
pak	pak	k6eAd1	pak
málo	málo	k6eAd1	málo
rodívá	rodívat	k5eAaImIp3nS	rodívat
<g/>
.	.	kIx.	.
</s>
<s>
Pláče	pláč	k1gInPc1	pláč
<g/>
-li	i	k?	-li
červen	červen	k1gInSc1	červen
a	a	k8xC	a
neoschne	oschnout	k5eNaPmIp3nS	oschnout
žitko	žitko	k1gNnSc1	žitko
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajících	zajíc	k1gMnPc6	zajíc
<g/>
,	,	kIx,	,
koroptvích	koroptví	k2eAgMnPc6d1	koroptví
budem	budem	k?	budem
mít	mít	k5eAaImF	mít
řídko	řídko	k6eAd1	řídko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
deštivo	deštivo	k6eAd1	deštivo
a	a	k8xC	a
chladno	chladno	k6eAd1	chladno
způsobí	způsobit	k5eAaPmIp3nS	způsobit
rok	rok	k1gInSc1	rok
neúrodný	úrodný	k2eNgInSc1d1	neúrodný
snadno	snadno	k6eAd1	snadno
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
mokrý	mokrý	k2eAgInSc1d1	mokrý
a	a	k8xC	a
studený	studený	k2eAgMnSc1d1	studený
-	-	kIx~	-
bývají	bývat	k5eAaImIp3nP	bývat
žně	žeň	k1gFnPc4	žeň
vždy	vždy	k6eAd1	vždy
zkaženy	zkažen	k2eAgFnPc4d1	zkažena
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
hrom	hrom	k1gInSc1	hrom
ozývá	ozývat	k5eAaImIp3nS	ozývat
<g/>
,	,	kIx,	,
kalné	kalný	k2eAgNnSc1d1	kalné
léto	léto	k1gNnSc1	léto
potom	potom	k6eAd1	potom
přicházívá	přicházívat	k5eAaImIp3nS	přicházívat
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
MDD	MDD	kA	MDD
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
červen	červen	k1gInSc1	červen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
červen	červen	k1gInSc1	červen
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
