<p>
<s>
Tchoř	tchoř	k1gMnSc1	tchoř
tmavý	tmavý	k2eAgMnSc1d1	tmavý
(	(	kIx(	(
<g/>
Mustela	Mustela	k1gFnSc1	Mustela
putorius	putorius	k1gMnSc1	putorius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
zvaný	zvaný	k2eAgInSc1d1	zvaný
kuna	kuna	k1gFnSc1	kuna
tchoř	tchoř	k1gMnSc1	tchoř
je	být	k5eAaImIp3nS	být
šelma	šelma	k1gMnSc1	šelma
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgMnPc2d1	lasicovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Domestikací	domestikace	k1gFnSc7	domestikace
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
tchoře	tchoř	k1gMnSc2	tchoř
tmavého	tmavý	k2eAgNnSc2d1	tmavé
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
fretka	fretka	k1gFnSc1	fretka
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Velikostí	velikost	k1gFnSc7	velikost
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
kunu	kuna	k1gFnSc4	kuna
a	a	k8xC	a
lasici	lasice	k1gFnSc4	lasice
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
menší	malý	k2eAgFnSc4d2	menší
šelmu	šelma	k1gFnSc4	šelma
<g/>
,	,	kIx,	,
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
od	od	k7c2	od
33	[number]	k4	33
do	do	k7c2	do
44	[number]	k4	44
cm	cm	kA	cm
<g/>
,	,	kIx,	,
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
500	[number]	k4	500
do	do	k7c2	do
1200	[number]	k4	1200
g	g	kA	g
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
samec	samec	k1gInSc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnSc1d1	svrchní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
tmavohnědá	tmavohnědý	k2eAgFnSc1d1	tmavohnědá
s	s	k7c7	s
prosvítající	prosvítající	k2eAgFnSc7d1	prosvítající
podsadou	podsada	k1gFnSc7	podsada
medové	medový	k2eAgFnSc2d1	medová
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
čenichu	čenich	k1gInSc2	čenich
a	a	k8xC	a
okraje	okraj	k1gInPc1	okraj
boltců	boltec	k1gInPc2	boltec
jsou	být	k5eAaImIp3nP	být
bělavé	bělavý	k2eAgFnPc1d1	bělavá
<g/>
.	.	kIx.	.
<g/>
Myslivci	Myslivec	k1gMnPc1	Myslivec
jej	on	k3xPp3gMnSc4	on
kdysi	kdysi	k6eAd1	kdysi
nazývali	nazývat	k5eAaImAgMnP	nazývat
tchoř	tchoř	k1gMnSc1	tchoř
žabař	žabař	k1gMnSc1	žabař
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
světlejší	světlý	k2eAgMnSc1d2	světlejší
tchoř	tchoř	k1gMnSc1	tchoř
stepní	stepní	k2eAgFnSc2d1	stepní
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
tchořem	tchoř	k1gMnSc7	tchoř
mlynářem	mlynář	k1gMnSc7	mlynář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
až	až	k6eAd1	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
2000	[number]	k4	2000
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
Žije	žít	k5eAaImIp3nS	žít
u	u	k7c2	u
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
lesů	les	k1gInPc2	les
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
)	)	kIx)	)
i	i	k9	i
poblíž	poblíž	k7c2	poblíž
lidských	lidský	k2eAgFnPc2d1	lidská
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
úbytku	úbytek	k1gInSc2	úbytek
vhodných	vhodný	k2eAgInPc2d1	vhodný
biotopů	biotop	k1gInPc2	biotop
(	(	kIx(	(
<g/>
vlhká	vlhký	k2eAgNnPc1d1	vlhké
a	a	k8xC	a
podmáčená	podmáčený	k2eAgNnPc1d1	podmáčené
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
už	už	k6eAd1	už
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bionomie	bionomie	k1gFnSc2	bionomie
==	==	k?	==
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
v	v	k7c6	v
hromadách	hromada	k1gFnPc6	hromada
kamení	kamení	k1gNnSc2	kamení
či	či	k8xC	či
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
kůlny	kůlna	k1gFnPc4	kůlna
a	a	k8xC	a
stodoly	stodola	k1gFnPc4	stodola
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
osidluje	osidlovat	k5eAaImIp3nS	osidlovat
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
nory	nora	k1gFnPc4	nora
králíků	králík	k1gMnPc2	králík
nebo	nebo	k8xC	nebo
jezevců	jezevec	k1gMnPc2	jezevec
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
loví	lovit	k5eAaImIp3nP	lovit
zejména	zejména	k9	zejména
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
běhá	běhat	k5eAaImIp3nS	běhat
<g/>
,	,	kIx,	,
lezení	lezení	k1gNnSc1	lezení
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
ani	ani	k8xC	ani
plavání	plavání	k1gNnSc2	plavání
mu	on	k3xPp3gMnSc3	on
nečiní	činit	k5eNaImIp3nS	činit
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
<g/>
Tchoř	tchoř	k1gMnSc1	tchoř
je	být	k5eAaImIp3nS	být
teritoriální	teritoriální	k2eAgMnSc1d1	teritoriální
<g/>
;	;	kIx,	;
samci	samec	k1gMnPc1	samec
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
stálý	stálý	k2eAgInSc4d1	stálý
revír	revír	k1gInSc4	revír
o	o	k7c6	o
výměře	výměra	k1gFnSc6	výměra
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
km2	km2	k4	km2
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
brání	bránit	k5eAaImIp3nS	bránit
před	před	k7c7	před
jinými	jiný	k2eAgInPc7d1	jiný
samci	samec	k1gInPc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
samice	samice	k1gFnPc1	samice
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
revíry	revír	k1gInPc4	revír
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
menší	malý	k2eAgInPc1d2	menší
než	než	k8xS	než
revíry	revír	k1gInPc1	revír
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
i	i	k9	i
překrývat	překrývat	k5eAaImF	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
teritoria	teritorium	k1gNnSc2	teritorium
si	se	k3xPyFc3	se
zvířata	zvíře	k1gNnPc4	zvíře
značí	značit	k5eAaImIp3nS	značit
výměškem	výměšek	k1gInSc7	výměšek
análních	anální	k2eAgFnPc2d1	anální
pachových	pachový	k2eAgFnPc2d1	pachová
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
výměšek	výměšek	k1gInSc4	výměšek
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
zejména	zejména	k9	zejména
ptáky	pták	k1gMnPc4	pták
(	(	kIx(	(
<g/>
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
bažanta	bažant	k1gMnSc2	bažant
<g/>
)	)	kIx)	)
a	a	k8xC	a
drobné	drobný	k2eAgMnPc4d1	drobný
savce	savec	k1gMnPc4	savec
(	(	kIx(	(
<g/>
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
ježka	ježek	k1gMnSc2	ježek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
větší	veliký	k2eAgInSc4d2	veliký
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
pak	pak	k6eAd1	pak
žáby	žába	k1gFnSc2	žába
a	a	k8xC	a
ryby	ryba	k1gFnSc2	ryba
(	(	kIx(	(
<g/>
za	za	k7c7	za
kořistí	kořist	k1gFnSc7	kořist
se	se	k3xPyFc4	se
umí	umět	k5eAaImIp3nS	umět
i	i	k9	i
potápět	potápět	k5eAaImF	potápět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
živou	živý	k2eAgFnSc4d1	živá
potravu	potrava	k1gFnSc4	potrava
před	před	k7c7	před
mršinami	mršina	k1gFnPc7	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
plody	plod	k1gInPc4	plod
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
páření	páření	k1gNnSc3	páření
dochází	docházet	k5eAaImIp3nS	docházet
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
námluv	námluva	k1gFnPc2	námluva
jsou	být	k5eAaImIp3nP	být
tchoři	tchoř	k1gMnPc1	tchoř
hlasově	hlasově	k6eAd1	hlasově
silně	silně	k6eAd1	silně
aktivní	aktivní	k2eAgMnPc1d1	aktivní
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
skolení	skolení	k1gNnSc1	skolení
a	a	k8xC	a
křičení	křičení	k1gNnSc1	křičení
se	se	k3xPyFc4	se
rozléhá	rozléhat	k5eAaImIp3nS	rozléhat
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
samotném	samotný	k2eAgNnSc6d1	samotné
páření	páření	k1gNnSc6	páření
drží	držet	k5eAaImIp3nS	držet
sameček	sameček	k1gMnSc1	sameček
samičku	samička	k1gFnSc4	samička
zuby	zub	k1gInPc1	zub
za	za	k7c4	za
šíji	šíje	k1gFnSc4	šíje
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samička	samička	k1gFnSc1	samička
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nehybná	hybný	k2eNgFnSc1d1	nehybná
<g/>
.	.	kIx.	.
<g/>
Samice	samice	k1gFnSc1	samice
tchoře	tchoř	k1gMnSc2	tchoř
bývá	bývat	k5eAaImIp3nS	bývat
březí	březí	k1gNnSc4	březí
41	[number]	k4	41
až	až	k9	až
42	[number]	k4	42
dní	den	k1gInPc2	den
a	a	k8xC	a
pak	pak	k6eAd1	pak
rodí	rodit	k5eAaImIp3nP	rodit
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
tři	tři	k4xCgInPc4	tři
až	až	k6eAd1	až
sedm	sedm	k4xCc4	sedm
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vážících	vážící	k2eAgFnPc6d1	vážící
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
g.	g.	k?	g.
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
vrhu	vrh	k1gInSc6	vrh
samicí	samice	k1gFnSc7	samice
pečlivě	pečlivě	k6eAd1	pečlivě
olízána	olízán	k2eAgFnSc1d1	olízán
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
hedvábnou	hedvábný	k2eAgFnSc4d1	hedvábná
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
týdnech	týden	k1gInPc6	týden
na	na	k7c6	na
příslušných	příslušný	k2eAgNnPc6d1	příslušné
místech	místo	k1gNnPc6	místo
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Rodí	rodit	k5eAaImIp3nS	rodit
se	se	k3xPyFc4	se
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
otevírají	otevírat	k5eAaImIp3nP	otevírat
asi	asi	k9	asi
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
měsíc	měsíc	k1gInSc1	měsíc
trvá	trvat	k5eAaImIp3nS	trvat
též	též	k9	též
doba	doba	k1gFnSc1	doba
kojení	kojení	k1gNnSc2	kojení
<g/>
.	.	kIx.	.
</s>
<s>
Samostatně	samostatně	k6eAd1	samostatně
žrát	žrát	k5eAaImF	žrát
umí	umět	k5eAaImIp3nS	umět
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc4	mládě
pohybově	pohybově	k6eAd1	pohybově
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
usměrňuje	usměrňovat	k5eAaImIp3nS	usměrňovat
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
je	být	k5eAaImIp3nS	být
stiskem	stisk	k1gInSc7	stisk
za	za	k7c4	za
šíji	šíje	k1gFnSc4	šíje
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
na	na	k7c6	na
výchově	výchova	k1gFnSc6	výchova
dále	daleko	k6eAd2	daleko
nepodílí	podílet	k5eNaImIp3nS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
zcela	zcela	k6eAd1	zcela
samostatná	samostatný	k2eAgNnPc1d1	samostatné
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc1	hmotnost
dospělého	dospělý	k2eAgMnSc2d1	dospělý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepřátelé	nepřítel	k1gMnPc5	nepřítel
==	==	k?	==
</s>
</p>
<p>
<s>
Tchoř	tchoř	k1gMnSc1	tchoř
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
stává	stávat	k5eAaImIp3nS	stávat
obětí	oběť	k1gFnSc7	oběť
větších	veliký	k2eAgMnPc2d2	veliký
predátorů	predátor	k1gMnPc2	predátor
jako	jako	k8xS	jako
lišky	liška	k1gFnSc2	liška
<g/>
,	,	kIx,	,
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
,	,	kIx,	,
dravců	dravec	k1gMnPc2	dravec
a	a	k8xC	a
sov	sova	k1gFnPc2	sova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
i	i	k8xC	i
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
kožešině	kožešina	k1gFnSc3	kožešina
každoročně	každoročně	k6eAd1	každoročně
hyne	hynout	k5eAaImIp3nS	hynout
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
tchořů	tchoř	k1gMnPc2	tchoř
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
pak	pak	k6eAd1	pak
následkem	následkem	k7c2	následkem
ztráty	ztráta	k1gFnSc2	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tchoř	tchoř	k1gMnSc1	tchoř
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tchoř	tchoř	k1gMnSc1	tchoř
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc4	taxon
Mustela	Mustela	k1gFnSc1	Mustela
putorius	putorius	k1gInSc1	putorius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
