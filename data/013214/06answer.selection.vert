<s>
Loví	lovit	k5eAaImIp3nS
zejména	zejména	k9
ptáky	pták	k1gMnPc4
(	(	kIx(
<g/>
do	do	k7c2
velikosti	velikost	k1gFnSc2
bažanta	bažant	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
drobné	drobný	k2eAgMnPc4d1
savce	savec	k1gMnPc4
(	(	kIx(
<g/>
do	do	k7c2
velikosti	velikost	k1gFnSc2
ježka	ježek	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
větší	veliký	k2eAgInSc4d2
hmyz	hmyz	k1gInSc4
<g/>
,	,	kIx,
u	u	k7c2
vody	voda	k1gFnSc2
pak	pak	k6eAd1
žáby	žába	k1gFnSc2
a	a	k8xC
ryby	ryba	k1gFnSc2
(	(	kIx(
<g/>
za	za	k7c7
kořistí	kořist	k1gFnSc7
se	se	k3xPyFc4
umí	umět	k5eAaImIp3nS
i	i	k9
potápět	potápět	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>