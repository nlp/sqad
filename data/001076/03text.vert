<s>
Lionel	Lionel	k1gMnSc1	Lionel
Andrés	Andrés	k1gInSc4	Andrés
Messi	Mess	k1gMnSc3	Mess
Cuccitini	Cuccitin	k2eAgMnPc1d1	Cuccitin
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1987	[number]	k4	1987
Rosario	Rosario	k1gMnSc1	Rosario
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
argentinský	argentinský	k2eAgMnSc1d1	argentinský
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
za	za	k7c4	za
katalánský	katalánský	k2eAgInSc4d1	katalánský
klub	klub	k1gInSc4	klub
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
a	a	k8xC	a
nosí	nosit	k5eAaImIp3nS	nosit
číslo	číslo	k1gNnSc1	číslo
dresu	dres	k1gInSc2	dres
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Stylem	styl	k1gInSc7	styl
hry	hra	k1gFnSc2	hra
bývá	bývat	k5eAaImIp3nS	bývat
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
slavnému	slavný	k2eAgMnSc3d1	slavný
argentinskému	argentinský	k2eAgMnSc3d1	argentinský
fotbalistovi	fotbalista	k1gMnSc3	fotbalista
Diego	Diego	k1gMnSc1	Diego
Maradonovi	Maradon	k1gMnSc3	Maradon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
lize	liga	k1gFnSc6	liga
rekordních	rekordní	k2eAgInPc2d1	rekordní
50	[number]	k4	50
gólů	gól	k1gInPc2	gól
a	a	k8xC	a
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soutěžích	soutěž	k1gFnPc6	soutěž
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
též	též	k9	též
rekordních	rekordní	k2eAgInPc2d1	rekordní
73	[number]	k4	73
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Vstřelil	vstřelit	k5eAaPmAgInS	vstřelit
nejvíce	nejvíce	k6eAd1	nejvíce
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
utkání	utkání	k1gNnSc6	utkání
hlavní	hlavní	k2eAgFnSc2d1	hlavní
fáze	fáze	k1gFnSc2	fáze
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
v	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
osmifinále	osmifinále	k1gNnSc2	osmifinále
LM	LM	kA	LM
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
proti	proti	k7c3	proti
Bayeru	Bayer	k1gMnSc3	Bayer
Leverkusen	Leverkusen	k2eAgMnSc1d1	Leverkusen
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
5	[number]	k4	5
gólů	gól	k1gInPc2	gól
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
dál	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g />
.	.	kIx.	.
</s>
<s>
<g/>
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
statistice	statistika	k1gFnSc6	statistika
vyrovnal	vyrovnat	k5eAaPmAgMnS	vyrovnat
Brazilec	Brazilec	k1gMnSc1	Brazilec
Luiz	Luiz	k1gMnSc1	Luiz
Adriano	Adriana	k1gFnSc5	Adriana
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
86	[number]	k4	86
<g/>
.	.	kIx.	.
branky	branka	k1gFnSc2	branka
překonal	překonat	k5eAaPmAgMnS	překonat
o	o	k7c4	o
gól	gól	k1gInSc4	gól
předchozí	předchozí	k2eAgInSc4d1	předchozí
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
německého	německý	k2eAgMnSc2d1	německý
útočníka	útočník	k1gMnSc2	útočník
Gerda	Gerd	k1gMnSc2	Gerd
Müllera	Müller	k1gMnSc2	Müller
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
gólů	gól	k1gInPc2	gól
nastřílených	nastřílený	k2eAgInPc2d1	nastřílený
za	za	k7c4	za
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
toho	ten	k3xDgMnSc4	ten
v	v	k7c4	v
utkání	utkání	k1gNnSc4	utkání
La	la	k1gNnSc2	la
Ligy	liga	k1gFnSc2	liga
proti	proti	k7c3	proti
Betisu	Betis	k1gInSc3	Betis
Sevilla	Sevilla	k1gFnSc1	Sevilla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
74	[number]	k4	74
gólů	gól	k1gInPc2	gól
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
dalších	další	k2eAgNnPc2d1	další
12	[number]	k4	12
za	za	k7c4	za
argentinskou	argentinský	k2eAgFnSc4d1	Argentinská
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Rekord	rekord	k1gInSc1	rekord
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
zambijský	zambijský	k2eAgInSc1d1	zambijský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
brazilský	brazilský	k2eAgInSc1d1	brazilský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
CR	cr	k0	cr
Flamengo	flamengo	k1gNnSc1	flamengo
<g/>
.	.	kIx.	.
</s>
<s>
Zambijský	zambijský	k2eAgInSc1d1	zambijský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
svaz	svaz	k1gInSc1	svaz
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zambijský	zambijský	k2eAgMnSc1d1	zambijský
hráč	hráč	k1gMnSc1	hráč
Godfrey	Godfrea	k1gFnSc2	Godfrea
Chitalu	Chital	k1gMnSc3	Chital
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
údajně	údajně	k6eAd1	údajně
připsal	připsat	k5eAaPmAgInS	připsat
107	[number]	k4	107
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Flamengo	flamengo	k1gNnSc1	flamengo
zase	zase	k9	zase
upozorňovalo	upozorňovat	k5eAaImAgNnS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
hráč	hráč	k1gMnSc1	hráč
Zico	Zico	k6eAd1	Zico
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
celkem	celkem	k6eAd1	celkem
89	[number]	k4	89
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
FIFA	FIFA	kA	FIFA
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
góly	gól	k1gInPc1	gól
zambijského	zambijský	k2eAgMnSc2d1	zambijský
fotbalisty	fotbalista	k1gMnSc2	fotbalista
nelze	lze	k6eNd1	lze
ověřit	ověřit	k5eAaPmF	ověřit
a	a	k8xC	a
tak	tak	k6eAd1	tak
Messiho	Messi	k1gMnSc2	Messi
rekord	rekord	k1gInSc1	rekord
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
Messi	Messe	k1gFnSc4	Messe
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
91	[number]	k4	91
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
překonal	překonat	k5eAaPmAgMnS	překonat
tak	tak	k6eAd1	tak
předchozí	předchozí	k2eAgInSc4d1	předchozí
rekord	rekord	k1gInSc4	rekord
Gerda	Gerd	k1gMnSc2	Gerd
Müllera	Müller	k1gMnSc2	Müller
o	o	k7c4	o
6	[number]	k4	6
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
Messi	Messe	k1gFnSc4	Messe
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
překonal	překonat	k5eAaPmAgInS	překonat
rekord	rekord	k1gInSc1	rekord
historicky	historicky	k6eAd1	historicky
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
střelce	střelec	k1gMnSc2	střelec
Barcelony	Barcelona	k1gFnSc2	Barcelona
Césara	César	k1gMnSc2	César
Rodrígueze	Rodrígueze	k1gFnSc2	Rodrígueze
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
nastřílel	nastřílet	k5eAaPmAgMnS	nastřílet
190	[number]	k4	190
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Lionel	Lionel	k1gMnSc1	Lionel
Messi	Messe	k1gFnSc4	Messe
docílil	docílit	k5eAaPmAgMnS	docílit
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Seville	Sevilla	k1gFnSc3	Sevilla
191	[number]	k4	191
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někteří	některý	k3yIgMnPc1	některý
statistikové	statistik	k1gMnPc1	statistik
uvádí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
za	za	k7c2	za
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
dokonce	dokonce	k9	dokonce
192	[number]	k4	192
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
hattrick	hattrick	k1gInSc4	hattrick
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Sevilla	Sevilla	k1gFnSc1	Sevilla
FC	FC	kA	FC
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
docílil	docílit	k5eAaPmAgInS	docílit
253	[number]	k4	253
<g/>
.	.	kIx.	.
branky	branka	k1gFnSc2	branka
v	v	k7c6	v
Primera	primera	k1gFnSc1	primera
División	División	k1gMnSc1	División
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
španělské	španělský	k2eAgFnSc2d1	španělská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
překonal	překonat	k5eAaPmAgMnS	překonat
251	[number]	k4	251
gólů	gól	k1gInPc2	gól
Telmo	Telma	k1gFnSc5	Telma
Zarry	Zarro	k1gNnPc7	Zarro
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
kvalifikační	kvalifikační	k2eAgFnSc4d1	kvalifikační
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překonal	překonat	k5eAaPmAgMnS	překonat
71	[number]	k4	71
gólů	gól	k1gInPc2	gól
Raúla	Raúl	k1gMnSc2	Raúl
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
překonal	překonat	k5eAaPmAgMnS	překonat
hranici	hranice	k1gFnSc4	hranice
400	[number]	k4	400
gólů	gól	k1gInPc2	gól
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
zápasech	zápas	k1gInPc6	zápas
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
přípravných	přípravný	k2eAgNnPc2d1	přípravné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Grandoli	Grandole	k1gFnSc4	Grandole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trénoval	trénovat	k5eAaImAgMnS	trénovat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
začal	začít	k5eAaPmAgMnS	začít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
CA	ca	kA	ca
Newell	Newell	k1gMnSc1	Newell
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc3	boy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
diagnostikován	diagnostikován	k2eAgInSc4d1	diagnostikován
nedostatek	nedostatek	k1gInSc4	nedostatek
růstových	růstový	k2eAgInPc2d1	růstový
hormonů	hormon	k1gInPc2	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
CA	ca	kA	ca
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Messiho	Messi	k1gMnSc4	Messi
rodiče	rodič	k1gMnSc4	rodič
tehdy	tehdy	k6eAd1	tehdy
neměli	mít	k5eNaImAgMnP	mít
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
platili	platit	k5eAaImAgMnP	platit
synovu	synův	k2eAgFnSc4d1	synova
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Carles	Carles	k1gMnSc1	Carles
Rexach	Rexach	k1gMnSc1	Rexach
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
týmu	tým	k1gInSc2	tým
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Messimu	Messima	k1gFnSc4	Messima
kontrakt	kontrakt	k1gInSc1	kontrakt
včetně	včetně	k7c2	včetně
zaplacení	zaplacení	k1gNnSc2	zaplacení
celé	celý	k2eAgFnSc2d1	celá
léčby	léčba	k1gFnSc2	léčba
a	a	k8xC	a
přestěhování	přestěhování	k1gNnSc2	přestěhování
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
měl	mít	k5eAaImAgMnS	mít
Messi	Messe	k1gFnSc4	Messe
svůj	svůj	k3xOyFgInSc4	svůj
oficiální	oficiální	k2eAgInSc4d1	oficiální
debutový	debutový	k2eAgInSc4d1	debutový
zápas	zápas	k1gInSc4	zápas
za	za	k7c4	za
A	A	kA	A
tým	tým	k1gInSc4	tým
Barcelony	Barcelona	k1gFnSc2	Barcelona
proti	proti	k7c3	proti
RCD	RCD	kA	RCD
Espanyol	Espanyol	k1gInSc1	Espanyol
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
třetím	třetí	k4xOgMnSc7	třetí
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
za	za	k7c4	za
Barcelonu	Barcelona	k1gFnSc4	Barcelona
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
tehdy	tehdy	k6eAd1	tehdy
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
7	[number]	k4	7
dnů	den	k1gInPc2	den
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
střelcem	střelec	k1gMnSc7	střelec
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
srovnáván	srovnávat	k5eAaImNgInS	srovnávat
s	s	k7c7	s
Diegem	Dieg	k1gInSc7	Dieg
Maradonou	Maradona	k1gFnSc7	Maradona
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
fotbalistů	fotbalista	k1gMnPc2	fotbalista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
podepsal	podepsat	k5eAaPmAgInS	podepsat
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
Barcelonou	Barcelona	k1gFnSc7	Barcelona
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
s	s	k7c7	s
klauzulí	klauzule	k1gFnSc7	klauzule
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
případném	případný	k2eAgInSc6d1	případný
odchodu	odchod	k1gInSc6	odchod
musí	muset	k5eAaImIp3nS	muset
dostat	dostat	k5eAaPmF	dostat
Barcelona	Barcelona	k1gFnSc1	Barcelona
odstupné	odstupné	k1gNnSc1	odstupné
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
Barcelona	Barcelona	k1gFnSc1	Barcelona
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
neuvolní	uvolnit	k5eNaPmIp3nP	uvolnit
Messiho	Messi	k1gMnSc4	Messi
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jí	jíst	k5eAaImIp3nS	jíst
nějaký	nějaký	k3yIgInSc1	nějaký
tým	tým	k1gInSc1	tým
nedá	dát	k5eNaPmIp3nS	dát
6	[number]	k4	6
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
240	[number]	k4	240
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
eur	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
rozpočet	rozpočet	k1gInSc4	rozpočet
Barcelony	Barcelona	k1gFnSc2	Barcelona
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
nového	nový	k2eAgInSc2d1	nový
kontraktu	kontrakt	k1gInSc2	kontrakt
<g/>
,	,	kIx,	,
oznámila	oznámit	k5eAaPmAgFnS	oznámit
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
hráčem	hráč	k1gMnSc7	hráč
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
vyšší	vysoký	k2eAgInSc4d2	vyšší
plat	plat	k1gInSc4	plat
a	a	k8xC	a
také	také	k9	také
působení	působení	k1gNnSc3	působení
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
přijal	přijmout	k5eAaPmAgMnS	přijmout
španělské	španělský	k2eAgNnSc4d1	španělské
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
splňovat	splňovat	k5eAaImF	splňovat
tak	tak	k6eAd1	tak
kvótu	kvóta	k1gFnSc4	kvóta
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
získal	získat	k5eAaPmAgInS	získat
rovněž	rovněž	k9	rovněž
ocenění	ocenění	k1gNnSc4	ocenění
Golden	Goldna	k1gFnPc2	Goldna
Boy	boy	k1gMnSc1	boy
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uděluje	udělovat	k5eAaImIp3nS	udělovat
italský	italský	k2eAgInSc1d1	italský
sportovní	sportovní	k2eAgInSc1d1	sportovní
deník	deník	k1gInSc1	deník
Tuttosport	Tuttosport	k1gInSc1	Tuttosport
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
evropskými	evropský	k2eAgFnPc7d1	Evropská
novinami	novina	k1gFnPc7	novina
vítězi	vítěz	k1gMnSc3	vítěz
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
působícího	působící	k2eAgNnSc2d1	působící
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
temperament	temperament	k1gInSc1	temperament
pomohl	pomoct	k5eAaPmAgInS	pomoct
Barceloně	Barcelona	k1gFnSc3	Barcelona
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
nad	nad	k7c4	nad
Chelsea	Chelseum	k1gNnPc4	Chelseum
nebo	nebo	k8xC	nebo
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc1	Madrid
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klubová	klubový	k2eAgFnSc1d1	klubová
sezóna	sezóna	k1gFnSc1	sezóna
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
skončila	skončit	k5eAaPmAgFnS	skončit
předčasně	předčasně	k6eAd1	předčasně
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
přetrhal	přetrhat	k5eAaPmAgInS	přetrhat
stehenní	stehenní	k2eAgInSc4d1	stehenní
sval	sval	k1gInSc4	sval
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
noze	noha	k1gFnSc6	noha
během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
zápasu	zápas	k1gInSc2	zápas
s	s	k7c7	s
Chelsea	Chelseum	k1gNnPc1	Chelseum
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
oznámily	oznámit	k5eAaPmAgFnP	oznámit
italské	italský	k2eAgFnPc1d1	italská
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Interu	Inter	k1gInSc2	Inter
Milán	Milán	k1gInSc1	Milán
hodlá	hodlat	k5eAaImIp3nS	hodlat
Messiho	Messi	k1gMnSc4	Messi
koupit	koupit	k5eAaPmF	koupit
za	za	k7c4	za
71	[number]	k4	71
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Přestup	přestup	k1gInSc1	přestup
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nekonal	konat	k5eNaImAgInS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
listopadu	listopad	k1gInSc2	listopad
laboroval	laborovat	k5eAaImAgMnS	laborovat
se	s	k7c7	s
zlomenou	zlomený	k2eAgFnSc7d1	zlomená
zanartní	zanartní	k2eAgFnSc7d1	zanartní
kůstkou	kůstka	k1gFnSc7	kůstka
<g/>
,	,	kIx,	,
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
napodobil	napodobit	k5eAaPmAgMnS	napodobit
svého	svůj	k3xOyFgMnSc2	svůj
slavnějšího	slavný	k2eAgMnSc2d2	slavnější
krajana	krajan	k1gMnSc2	krajan
Diega	Dieg	k1gMnSc2	Dieg
Maradonu	Maradon	k1gInSc2	Maradon
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
domácího	domácí	k2eAgInSc2d1	domácí
poháru	pohár	k1gInSc2	pohár
proti	proti	k7c3	proti
Getafe	Getaf	k1gInSc5	Getaf
CF	CF	kA	CF
obehrál	obehrát	k5eAaPmAgInS	obehrát
pět	pět	k4xCc4	pět
hráčů	hráč	k1gMnPc2	hráč
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
,	,	kIx,	,
brankáře	brankář	k1gMnSc2	brankář
a	a	k8xC	a
rozvlnil	rozvlnit	k5eAaPmAgMnS	rozvlnit
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Maradonu	Maradona	k1gFnSc4	Maradona
napodobil	napodobit	k5eAaPmAgInS	napodobit
i	i	k9	i
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
ligového	ligový	k2eAgInSc2d1	ligový
ročníku	ročník	k1gInSc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Espanyolu	Espanyol	k1gInSc3	Espanyol
Barcelona	Barcelona	k1gFnSc1	Barcelona
Messi	Messe	k1gFnSc3	Messe
evidentně	evidentně	k6eAd1	evidentně
dopravil	dopravit	k5eAaPmAgMnS	dopravit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
míč	míč	k1gInSc4	míč
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Maradona	Maradona	k1gFnSc1	Maradona
na	na	k7c4	na
MS	MS	kA	MS
1986	[number]	k4	1986
proti	proti	k7c3	proti
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
ukončil	ukončit	k5eAaPmAgInS	ukončit
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
14	[number]	k4	14
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
23	[number]	k4	23
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
polovina	polovina	k1gFnSc1	polovina
sezony	sezona	k1gFnSc2	sezona
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
velice	velice	k6eAd1	velice
vydařená	vydařený	k2eAgFnSc1d1	vydařená
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
FIFPro	FIFPro	k1gNnSc4	FIFPro
World	Worlda	k1gFnPc2	Worlda
XI	XI	kA	XI
Player	Player	k1gMnSc1	Player
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
online	onlinout	k5eAaPmIp3nS	onlinout
průzkumu	průzkum	k1gInSc3	průzkum
deníku	deník	k1gInSc2	deník
Marca	Marc	k1gInSc2	Marc
ho	on	k3xPp3gMnSc2	on
lidé	člověk	k1gMnPc1	člověk
77	[number]	k4	77
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
si	se	k3xPyFc3	se
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
proti	proti	k7c3	proti
skotskému	skotský	k2eAgMnSc3d1	skotský
Celticu	Celtic	k1gMnSc3	Celtic
přivodil	přivodit	k5eAaPmAgInS	přivodit
svalové	svalový	k2eAgNnSc4d1	svalové
poranění	poranění	k1gNnSc4	poranění
stehna	stehno	k1gNnSc2	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
poranění	poranění	k1gNnSc2	poranění
jej	on	k3xPp3gMnSc4	on
zbrzdil	zbrzdit	k5eAaPmAgInS	zbrzdit
již	již	k6eAd1	již
poněkolikáté	poněkolikáté	k6eAd1	poněkolikáté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
za	za	k7c4	za
katalánský	katalánský	k2eAgInSc4d1	katalánský
klub	klub	k1gInSc4	klub
40	[number]	k4	40
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
krát	krát	k6eAd1	krát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vsítit	vsítit	k5eAaPmF	vsítit
a	a	k8xC	a
13	[number]	k4	13
<g/>
krát	krát	k6eAd1	krát
přihrát	přihrát	k5eAaPmF	přihrát
na	na	k7c4	na
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
semifinále	semifinále	k1gNnSc2	semifinále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
prohry	prohra	k1gFnSc2	prohra
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
s	s	k7c7	s
Bayernem	Bayern	k1gInSc7	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
utkání	utkání	k1gNnSc2	utkání
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nebyl	být	k5eNaImAgInS	být
zdravotně	zdravotně	k6eAd1	zdravotně
stoprocentně	stoprocentně	k6eAd1	stoprocentně
fit	fit	k2eAgFnSc1d1	fit
<g/>
.	.	kIx.	.
</s>
<s>
Taktika	taktik	k1gMnSc4	taktik
soupeře	soupeř	k1gMnSc4	soupeř
ale	ale	k8xC	ale
dokázala	dokázat	k5eAaPmAgFnS	dokázat
jeho	jeho	k3xOp3gFnPc4	jeho
přednosti	přednost	k1gFnPc4	přednost
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
dokonale	dokonale	k6eAd1	dokonale
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
33	[number]	k4	33
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
La	la	k1gNnSc2	la
Ligy	liga	k1gFnSc2	liga
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
gólem	gól	k1gInSc7	gól
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
remíze	remíza	k1gFnSc6	remíza
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Bilbaem	Bilbaum	k1gNnSc7	Bilbaum
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
skončila	skončit	k5eAaPmAgFnS	skončit
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
Bayernem	Bayern	k1gInSc7	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
Messi	Messe	k1gFnSc4	Messe
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
Aktualizováno	aktualizován	k2eAgNnSc1d1	Aktualizováno
<g/>
:	:	kIx,	:
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
reprezentace	reprezentace	k1gFnSc2	reprezentace
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
na	na	k7c4	na
přátelský	přátelský	k2eAgInSc4d1	přátelský
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
Paraguayi	Paraguay	k1gFnSc3	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
U20	U20	k1gFnSc2	U20
2005	[number]	k4	2005
konaném	konaný	k2eAgNnSc6d1	konané
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Argentina	Argentina	k1gFnSc1	Argentina
Nigérii	Nigérie	k1gFnSc4	Nigérie
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc3	Messe
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
střelcem	střelec	k1gMnSc7	střelec
<g/>
,	,	kIx,	,
vsítil	vsítit	k5eAaPmAgMnS	vsítit
celkově	celkově	k6eAd1	celkově
šest	šest	k4xCc4	šest
branek	branka	k1gFnPc2	branka
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
argentinského	argentinský	k2eAgInSc2d1	argentinský
olympijského	olympijský	k2eAgInSc2d1	olympijský
výběru	výběr	k1gInSc2	výběr
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
LOH	LOH	kA	LOH
2008	[number]	k4	2008
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
získal	získat	k5eAaPmAgInS	získat
zlato	zlato	k1gNnSc4	zlato
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
nad	nad	k7c7	nad
Nigérií	Nigérie	k1gFnSc7	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
hrát	hrát	k5eAaImF	hrát
za	za	k7c4	za
španělský	španělský	k2eAgInSc4d1	španělský
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
ale	ale	k9	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
nastupovat	nastupovat	k5eAaImF	nastupovat
za	za	k7c4	za
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
jej	on	k3xPp3gMnSc4	on
povolal	povolat	k5eAaPmAgMnS	povolat
do	do	k7c2	do
A-týmu	Aým	k1gInSc2	A-tým
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
reprezentace	reprezentace	k1gFnSc2	reprezentace
trenér	trenér	k1gMnSc1	trenér
José	Josá	k1gFnSc2	Josá
Pekerman	Pekerman	k1gMnSc1	Pekerman
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
v	v	k7c6	v
přátelském	přátelský	k2eAgInSc6d1	přátelský
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
šel	jít	k5eAaImAgMnS	jít
v	v	k7c6	v
63	[number]	k4	63
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
strávil	strávit	k5eAaPmAgMnS	strávit
pouze	pouze	k6eAd1	pouze
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
Markus	Markus	k1gMnSc1	Markus
Merk	merk	k1gInSc4	merk
jej	on	k3xPp3gInSc4	on
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
za	za	k7c4	za
strkanici	strkanice	k1gFnSc4	strkanice
s	s	k7c7	s
maďarským	maďarský	k2eAgMnSc7d1	maďarský
obráncem	obránce	k1gMnSc7	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
rozčílilo	rozčílit	k5eAaPmAgNnS	rozčílit
mnoho	mnoho	k4c1	mnoho
hráčů	hráč	k1gMnPc2	hráč
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
také	také	k9	také
Diega	Diega	k1gFnSc1	Diega
Maradonu	Maradon	k1gInSc2	Maradon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sám	sám	k3xTgMnSc1	sám
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyloučení	vyloučení	k1gNnSc1	vyloučení
bylo	být	k5eAaImAgNnS	být
neregulérní	regulérní	k2eNgNnSc1d1	neregulérní
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
oslabení	oslabení	k1gNnSc6	oslabení
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
Lionel	Lionel	k1gInSc1	Lionel
Messi	Messe	k1gFnSc4	Messe
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
hattrick	hattrick	k1gInSc4	hattrick
v	v	k7c6	v
argentinském	argentinský	k2eAgInSc6d1	argentinský
dresu	dres	k1gInSc6	dres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
domácímu	domácí	k2eAgNnSc3d1	domácí
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
88	[number]	k4	88
<g/>
.	.	kIx.	.
a	a	k8xC	a
92	[number]	k4	92
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
tak	tak	k6eAd1	tak
výhru	výhra	k1gFnSc4	výhra
Argentiny	Argentina	k1gFnSc2	Argentina
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
hattrick	hattrick	k1gInSc4	hattrick
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
s	s	k7c7	s
tradičním	tradiční	k2eAgMnSc7d1	tradiční
kontinentálním	kontinentální	k2eAgMnSc7d1	kontinentální
rivalem	rival	k1gMnSc7	rival
Brazílií	Brazílie	k1gFnPc2	Brazílie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
na	na	k7c6	na
neutrální	neutrální	k2eAgFnSc6d1	neutrální
půdě	půda	k1gFnSc6	půda
v	v	k7c4	v
East	East	k2eAgInSc4d1	East
Rutherford	Rutherford	k1gInSc4	Rutherford
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
napínavém	napínavý	k2eAgInSc6d1	napínavý
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
skóre	skóre	k1gNnSc1	skóre
přelévalo	přelévat	k5eAaImAgNnS	přelévat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
Lionel	Lionel	k1gInSc1	Lionel
Messi	Messe	k1gFnSc4	Messe
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
argentinských	argentinský	k2eAgInPc2d1	argentinský
gólů	gól	k1gInPc2	gól
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgNnPc1	tři
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
pokořil	pokořit	k5eAaPmAgInS	pokořit
Brazílii	Brazílie	k1gFnSc3	Brazílie
poměrem	poměr	k1gInSc7	poměr
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc1	zranění
z	z	k7c2	z
března	březno	k1gNnSc2	březno
mu	on	k3xPp3gMnSc3	on
značně	značně	k6eAd1	značně
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS	zkomplikovat
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
MS	MS	kA	MS
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jej	on	k3xPp3gMnSc4	on
trenér	trenér	k1gMnSc1	trenér
nominoval	nominovat	k5eAaBmAgMnS	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
skupiny	skupina	k1gFnSc2	skupina
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Maxi	Max	k1gMnSc3	Max
Rodrígueze	Rodrígueze	k1gFnSc2	Rodrígueze
a	a	k8xC	a
zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
a	a	k8xC	a
Černé	Černá	k1gFnSc3	Černá
Hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Argentina	Argentina	k1gFnSc1	Argentina
porazila	porazit	k5eAaPmAgFnS	porazit
soupeře	soupeř	k1gMnPc4	soupeř
vysoko	vysoko	k6eAd1	vysoko
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
Messi	Messe	k1gFnSc4	Messe
také	také	k6eAd1	také
přispěl	přispět	k5eAaPmAgMnS	přispět
gólem	gól	k1gInSc7	gól
a	a	k8xC	a
asistencí	asistence	k1gFnSc7	asistence
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
učinil	učinit	k5eAaPmAgMnS	učinit
na	na	k7c6	na
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nejmladšího	mladý	k2eAgMnSc2d3	nejmladší
střelce	střelec	k1gMnSc2	střelec
a	a	k8xC	a
asistenta	asistent	k1gMnSc2	asistent
a	a	k8xC	a
šestého	šestý	k4xOgMnSc2	šestý
nejmladšího	mladý	k2eAgMnSc2d3	nejmladší
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvnim	prvnim	k6eAd1	prvnim
zápase	zápas	k1gInSc6	zápas
Argentina	Argentina	k1gFnSc1	Argentina
porazila	porazit	k5eAaPmAgFnS	porazit
Pobřeží	pobřeží	k1gNnSc4	pobřeží
Slonoviny	slonovina	k1gFnSc2	slonovina
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
remizovali	remizovat	k5eAaPmAgMnP	remizovat
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
nad	nad	k7c7	nad
kterým	který	k3yIgMnSc7	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
po	po	k7c4	po
prodloužení	prodloužení	k1gNnSc4	prodloužení
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
Argentinci	Argentinec	k1gMnPc1	Argentinec
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Copa	Copus	k1gMnSc4	Copus
América	Améric	k1gInSc2	Améric
2007	[number]	k4	2007
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
Argentina	Argentina	k1gFnSc1	Argentina
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
porazila	porazit	k5eAaPmAgFnS	porazit
USA	USA	kA	USA
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
Kolumbii	Kolumbie	k1gFnSc4	Kolumbie
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
Paraguay	Paraguay	k1gFnSc1	Paraguay
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
porazila	porazit	k5eAaPmAgFnS	porazit
Peru	Peru	k1gNnSc7	Peru
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Messi	Messe	k1gFnSc4	Messe
dal	dát	k5eAaPmAgMnS	dát
druhou	druhý	k4xOgFnSc4	druhý
branku	branka	k1gFnSc4	branka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
nad	nad	k7c7	nad
Mexikem	Mexiko	k1gNnSc7	Mexiko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
i	i	k9	i
tentokrát	tentokrát	k6eAd1	tentokrát
Messi	Messe	k1gFnSc4	Messe
skóroval	skórovat	k5eAaBmAgMnS	skórovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
prohráli	prohrát	k5eAaPmAgMnP	prohrát
Argentinci	Argentinec	k1gMnPc1	Argentinec
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
z	z	k7c2	z
Brazílií	Brazílie	k1gFnPc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
též	též	k9	též
<g/>
,	,	kIx,	,
Argentinci	Argentinec	k1gMnPc1	Argentinec
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
Diego	Diego	k6eAd1	Diego
Maradonou	Maradona	k1gFnSc7	Maradona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvnim	prvnim	k6eAd1	prvnim
zápase	zápas	k1gInSc6	zápas
Argentina	Argentina	k1gFnSc1	Argentina
porazila	porazit	k5eAaPmAgFnS	porazit
Nigérii	Nigérie	k1gFnSc4	Nigérie
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
přemohli	přemoct	k5eAaPmAgMnP	přemoct
Jížní	Jížní	k2eAgFnSc4d1	Jížní
Koreu	Korea	k1gFnSc4	Korea
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
třetim	třetim	k1gInSc4	třetim
zápase	zápas	k1gInSc6	zápas
porazili	porazit	k5eAaPmAgMnP	porazit
Řecko	Řecko	k1gNnSc4	Řecko
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
vyřadili	vyřadit	k5eAaPmAgMnP	vyřadit
Mexiko	Mexiko	k1gNnSc4	Mexiko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Německu	Německo	k1gNnSc6	Německo
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Copa	Copus	k1gMnSc4	Copus
América	Améric	k1gInSc2	Améric
2011	[number]	k4	2011
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
Messi	Messe	k1gFnSc3	Messe
příliš	příliš	k6eAd1	příliš
nezářil	zářit	k5eNaImAgInS	zářit
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
remizovala	remizovat	k5eAaPmAgFnS	remizovat
s	s	k7c7	s
Bolívií	Bolívie	k1gFnSc7	Bolívie
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dalšim	dalšim	k6eAd1	dalšim
zápase	zápas	k1gInSc6	zápas
remizovala	remizovat	k5eAaPmAgFnS	remizovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
porazila	porazit	k5eAaPmAgFnS	porazit
Kostariku	Kostarika	k1gFnSc4	Kostarika
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Uruguayí	Uruguay	k1gFnSc7	Uruguay
(	(	kIx(	(
<g/>
vítězem	vítěz	k1gMnSc7	vítěz
turnaje	turnaj	k1gInSc2	turnaj
<g/>
)	)	kIx)	)
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
roztřelu	roztřel	k1gInSc6	roztřel
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
F	F	kA	F
táhl	táhnout	k5eAaImAgMnS	táhnout
Argentinu	Argentina	k1gFnSc4	Argentina
za	za	k7c7	za
postupem	postup	k1gInSc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Skóroval	skórovat	k5eAaBmAgMnS	skórovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
třech	tři	k4xCgInPc6	tři
zápasech	zápas	k1gInPc6	zápas
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Bosně	Bosna	k1gFnSc3	Bosna
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc3	Írán
i	i	k8xC	i
Nigérii	Nigérie	k1gFnSc3	Nigérie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
zápasu	zápas	k1gInSc2	zápas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
obsadila	obsadit	k5eAaPmAgFnS	obsadit
s	s	k7c7	s
devíti	devět	k4xCc7	devět
body	bod	k1gInPc7	bod
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Bosnou	Bosna	k1gFnSc7	Bosna
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Messi	Messe	k1gFnSc4	Messe
přispěl	přispět	k5eAaPmAgInS	přispět
vítězným	vítězný	k2eAgInSc7d1	vítězný
gólem	gól	k1gInSc7	gól
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Messi	Messe	k1gFnSc4	Messe
opět	opět	k6eAd1	opět
přispěl	přispět	k5eAaPmAgInS	přispět
vítězným	vítězný	k2eAgInSc7d1	vítězný
gólem	gól	k1gInSc7	gól
a	a	k8xC	a
s	s	k7c7	s
Nigérií	Nigérie	k1gFnSc7	Nigérie
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Messi	Messe	k1gFnSc4	Messe
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
brány	brána	k1gFnSc2	brána
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
týmem	tým	k1gInSc7	tým
postoupil	postoupit	k5eAaPmAgInS	postoupit
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
Argentina	Argentina	k1gFnSc1	Argentina
porazila	porazit	k5eAaPmAgFnS	porazit
po	po	k7c4	po
prodloužení	prodloužení	k1gNnSc4	prodloužení
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Argentina	Argentina	k1gFnSc1	Argentina
porazila	porazit	k5eAaPmAgFnS	porazit
Belgii	Belgie	k1gFnSc4	Belgie
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c6	na
penalty	penalty	k1gNnSc6	penalty
<g/>
,	,	kIx,	,
Messi	Messe	k1gFnSc6	Messe
svůj	svůj	k3xOyFgInSc4	svůj
pokus	pokus	k1gInSc4	pokus
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
musel	muset	k5eAaImAgMnS	muset
přijmout	přijmout	k5eAaPmF	přijmout
porážku	porážka	k1gFnSc4	porážka
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
šampionátu	šampionát	k1gInSc2	šampionát
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
-	-	kIx~	-
Adidas	Adidas	k1gInSc1	Adidas
Golden	Goldna	k1gFnPc2	Goldna
Ball	Balla	k1gFnPc2	Balla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Copa	Copus	k1gMnSc4	Copus
América	Améric	k1gInSc2	Améric
2015	[number]	k4	2015
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
Argentina	Argentina	k1gFnSc1	Argentina
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
remizovala	remizovat	k5eAaPmAgFnS	remizovat
s	s	k7c7	s
Paraguayí	Paraguay	k1gFnSc7	Paraguay
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
Messi	Mess	k1gMnSc3	Mess
přispěl	přispět	k5eAaPmAgMnS	přispět
druhou	druhý	k4xOgFnSc7	druhý
brankou	branka	k1gFnSc7	branka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
porazila	porazit	k5eAaPmAgFnS	porazit
Uruguay	Uruguay	k1gFnSc1	Uruguay
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
Jamajku	Jamajka	k1gFnSc4	Jamajka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
porazili	porazit	k5eAaPmAgMnP	porazit
Argentinci	Argentinec	k1gMnPc1	Argentinec
Kolumbii	Kolumbie	k1gFnSc4	Kolumbie
v	v	k7c6	v
penaltovém	penaltový	k2eAgInSc6d1	penaltový
rozstřelu	rozstřel	k1gInSc6	rozstřel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
poráží	porážet	k5eAaImIp3nS	porážet
Paraguay	Paraguay	k1gFnSc1	Paraguay
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
s	s	k7c7	s
Chile	Chile	k1gNnSc7	Chile
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
penalty	penalta	k1gFnPc1	penalta
a	a	k8xC	a
Argentina	Argentina	k1gFnSc1	Argentina
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Messimu	Messimat	k5eAaPmIp1nS	Messimat
opět	opět	k6eAd1	opět
unikl	uniknout	k5eAaPmAgMnS	uniknout
titul	titul	k1gInSc4	titul
s	s	k7c7	s
argentinskou	argentinský	k2eAgFnSc7d1	Argentinská
reprezentací	reprezentace	k1gFnSc7	reprezentace
na	na	k7c6	na
výnamném	výnamný	k2eAgInSc6d1	výnamný
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
Copa	Cop	k2eAgFnSc1d1	Copa
América	América	k1gFnSc1	América
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Argentina	Argentina	k1gFnSc1	Argentina
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
reprezentační	reprezentační	k2eAgFnSc4d1	reprezentační
kariéru	kariéra	k1gFnSc4	kariéra
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
opětovně	opětovně	k6eAd1	opětovně
skončil	skončit	k5eAaPmAgMnS	skončit
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
(	(	kIx(	(
<g/>
ziskem	zisk	k1gInSc7	zisk
titulu	titul	k1gInSc2	titul
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
však	však	k9	však
vzal	vzít	k5eAaPmAgMnS	vzít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
touze	touha	k1gFnSc6	touha
opět	opět	k6eAd1	opět
obléci	obléct	k5eAaPmF	obléct
národní	národní	k2eAgInSc4d1	národní
dres	dres	k1gInSc4	dres
<g/>
.	.	kIx.	.
</s>
<s>
Góly	gól	k1gInPc1	gól
Lionela	Lionel	k1gMnSc2	Lionel
Messiho	Messi	k1gMnSc2	Messi
za	za	k7c4	za
argentinskou	argentinský	k2eAgFnSc4d1	Argentinská
reprezentaci	reprezentace	k1gFnSc4	reprezentace
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
Góly	gól	k1gInPc1	gól
Lionela	Lionel	k1gMnSc2	Lionel
Messiho	Messi	k1gMnSc2	Messi
za	za	k7c4	za
A-mužstvo	Aužstvo	k1gNnSc4	A-mužstvo
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
reprezentace	reprezentace	k1gFnSc2	reprezentace
Messi	Messe	k1gFnSc4	Messe
byl	být	k5eAaImAgMnS	být
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
Macarenou	Macarena	k1gFnSc7	Macarena
Lemos	Lemosa	k1gFnPc2	Lemosa
<g/>
,	,	kIx,	,
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
Rosaria	rosarium	k1gNnSc2	rosarium
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Macarenu	Macaren	k1gMnSc3	Macaren
představil	představit	k5eAaPmAgMnS	představit
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Messi	Messe	k1gFnSc4	Messe
vrátil	vrátit	k5eAaPmAgMnS	vrátit
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
MS	MS	kA	MS
2006	[number]	k4	2006
léčit	léčit	k5eAaImF	léčit
své	svůj	k3xOyFgNnSc4	svůj
zranění	zranění	k1gNnSc4	zranění
do	do	k7c2	do
Rosaria	rosarium	k1gNnSc2	rosarium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
spojován	spojovat	k5eAaImNgMnS	spojovat
s	s	k7c7	s
argentinskou	argentinský	k2eAgFnSc7d1	Argentinská
modelkou	modelka	k1gFnSc7	modelka
Lucianou	Luciana	k1gFnSc7	Luciana
Salazar	Salazar	k1gInSc4	Salazar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Messi	Messe	k1gFnSc6	Messe
pro	pro	k7c4	pro
TV	TV	kA	TV
stanici	stanice	k1gFnSc4	stanice
Canal	Canal	k1gInSc1	Canal
33	[number]	k4	33
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
uvolněný	uvolněný	k2eAgMnSc1d1	uvolněný
a	a	k8xC	a
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgMnS	být
viděn	vidět	k5eAaImNgMnS	vidět
s	s	k7c7	s
Antonellou	Antonella	k1gFnSc7	Antonella
Roccuzzo	Roccuzza	k1gFnSc5	Roccuzza
na	na	k7c6	na
karnevalu	karneval	k1gInSc6	karneval
v	v	k7c4	v
Sitges	Sitges	k1gInSc4	Sitges
po	po	k7c6	po
derby	derby	k1gNnSc6	derby
mezi	mezi	k7c7	mezi
Barcelonou	Barcelona	k1gFnSc7	Barcelona
a	a	k8xC	a
Espanyolem	Espanyol	k1gInSc7	Espanyol
<g/>
.	.	kIx.	.
</s>
<s>
Roccuzzo	Roccuzza	k1gFnSc5	Roccuzza
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Rosaria	rosarium	k1gNnSc2	rosarium
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
dal	dát	k5eAaPmAgMnS	dát
Messi	Mess	k1gMnSc3	Mess
gól	gól	k1gInSc4	gól
na	na	k7c4	na
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
mezi	mezi	k7c7	mezi
Argentinou	Argentina	k1gFnSc7	Argentina
a	a	k8xC	a
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
na	na	k7c4	na
MS	MS	kA	MS
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
23	[number]	k4	23
<g/>
.	.	kIx.	.
gólu	gól	k1gInSc2	gól
v	v	k7c6	v
argentinské	argentinský	k2eAgFnSc6d1	Argentinská
reprezentaci	reprezentace	k1gFnSc6	reprezentace
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
balon	balon	k1gInSc4	balon
pod	pod	k7c4	pod
dres	dres	k1gInSc4	dres
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
mělo	mít	k5eAaImAgNnS	mít
narodit	narodit	k5eAaPmF	narodit
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
se	se	k3xPyFc4	se
pak	pak	k9	pak
dvojice	dvojice	k1gFnSc1	dvojice
očekávaného	očekávaný	k2eAgMnSc2d1	očekávaný
syna	syn	k1gMnSc2	syn
dočkala	dočkat	k5eAaPmAgFnS	dočkat
a	a	k8xC	a
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
jej	on	k3xPp3gNnSc4	on
Thiago	Thiago	k1gNnSc4	Thiago
Messi	Mess	k1gMnSc3	Mess
Roccuzzo	Roccuzza	k1gFnSc5	Roccuzza
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc4	Messe
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
bratrance	bratranec	k1gMnPc4	bratranec
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgInSc4d1	hrající
fotbal	fotbal	k1gInSc4	fotbal
<g/>
:	:	kIx,	:
Maxe	Max	k1gMnSc2	Max
a	a	k8xC	a
Emanuela	Emanuel	k1gMnSc2	Emanuel
Biancucchi	Biancucch	k1gFnSc2	Biancucch
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
za	za	k7c4	za
brazilskou	brazilský	k2eAgFnSc4d1	brazilská
Bahiu	Bahius	k1gMnSc6	Bahius
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
založil	založit	k5eAaPmAgMnS	založit
Messi	Messe	k1gFnSc4	Messe
nadaci	nadace	k1gFnSc4	nadace
"	"	kIx"	"
<g/>
Leo	Leo	k1gMnSc1	Leo
Messi	Messe	k1gFnSc4	Messe
Foundation	Foundation	k1gInSc1	Foundation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgInSc4d1	podporující
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
vzdělání	vzdělání	k1gNnSc3	vzdělání
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
péči	péče	k1gFnSc3	péče
pro	pro	k7c4	pro
ohrožené	ohrožený	k2eAgFnPc4d1	ohrožená
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
Messiho	Messi	k1gMnSc4	Messi
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
problémy	problém	k1gInPc4	problém
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
jeho	jeho	k3xOp3gFnSc1	jeho
nadace	nadace	k1gFnSc1	nadace
argentinským	argentinský	k2eAgFnPc3d1	Argentinská
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
porucha	porucha	k1gFnSc1	porucha
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
transport	transport	k1gInSc1	transport
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc2	zajištění
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
a	a	k8xC	a
plné	plný	k2eAgNnSc4d1	plné
hrazení	hrazení	k1gNnSc4	hrazení
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Messiho	Messize	k6eAd1	Messize
nadace	nadace	k1gFnSc1	nadace
také	také	k9	také
podporuje	podporovat	k5eAaImIp3nS	podporovat
jeho	jeho	k3xOp3gFnPc4	jeho
vlastní	vlastní	k2eAgFnPc4d1	vlastní
fundraisingové	fundraisingový	k2eAgFnPc4d1	fundraisingová
aktivity	aktivita	k1gFnPc4	aktivita
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Herbalife	Herbalif	k1gInSc5	Herbalif
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
Messi	Mess	k1gMnSc3	Mess
UNICEFem	UNICEFem	k1gInSc4	UNICEFem
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
vyslancem	vyslanec	k1gMnSc7	vyslanec
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Messiho	Messize	k6eAd1	Messize
aktivity	aktivita	k1gFnSc2	aktivita
jako	jako	k8xC	jako
vyslance	vyslanec	k1gMnSc2	vyslanec
UNICEFu	UNICEFus	k1gInSc2	UNICEFus
byly	být	k5eAaImAgFnP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
dětských	dětský	k2eAgNnPc2d1	dětské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc4	Messe
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
plně	plně	k6eAd1	plně
podporován	podporovat	k5eAaImNgMnS	podporovat
Barcelonou	Barcelona	k1gFnSc7	Barcelona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
také	také	k9	také
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
UNICEFem	UNICEF	k1gInSc7	UNICEF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
časopisem	časopis	k1gInSc7	časopis
France	Franc	k1gMnPc4	Franc
Football	Football	k1gMnSc1	Football
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
žebříčku	žebříček	k1gInSc2	žebříček
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
fotbalistů	fotbalista	k1gMnPc2	fotbalista
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
před	před	k7c4	před
Davida	David	k1gMnSc4	David
Beckhama	Beckham	k1gMnSc4	Beckham
a	a	k8xC	a
Cristiana	Cristian	k1gMnSc4	Cristian
Ronalda	Ronald	k1gMnSc4	Ronald
<g/>
,	,	kIx,	,
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
33	[number]	k4	33
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
z	z	k7c2	z
platu	plat	k1gInSc2	plat
<g/>
,	,	kIx,	,
bonusů	bonus	k1gInPc2	bonus
a	a	k8xC	a
mimofotbalových	mimofotbalův	k2eAgInPc2d1	mimofotbalův
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
čisté	čistý	k2eAgNnSc1d1	čisté
jmění	jmění	k1gNnSc1	jmění
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
110	[number]	k4	110
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc4	Messe
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
her	hra	k1gFnPc2	hra
Pro	pro	k7c4	pro
Evolution	Evolution	k1gInSc4	Evolution
Soccer	Soccer	k1gInSc4	Soccer
2009	[number]	k4	2009
a	a	k8xC	a
Pro	pro	k7c4	pro
Evolution	Evolution	k1gInSc4	Evolution
Soccer	Soccer	k1gInSc4	Soccer
2011	[number]	k4	2011
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
zapojen	zapojen	k2eAgMnSc1d1	zapojen
do	do	k7c2	do
propagačních	propagační	k2eAgFnPc2d1	propagační
kampaní	kampaň	k1gFnPc2	kampaň
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc4	Messe
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Fernando	Fernanda	k1gFnSc5	Fernanda
Torresem	Torres	k1gMnSc7	Torres
tváří	tvář	k1gFnPc2	tvář
Pro	pro	k7c4	pro
Evolution	Evolution	k1gInSc4	Evolution
Soccer	Soccer	k1gInSc1	Soccer
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
traileru	trailer	k1gInSc6	trailer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Messi	Messe	k1gFnSc4	Messe
stal	stát	k5eAaPmAgMnS	stát
novou	nový	k2eAgFnSc7d1	nová
tváří	tvář	k1gFnSc7	tvář
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
hry	hra	k1gFnSc2	hra
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc4	Messe
je	být	k5eAaImIp3nS	být
sponzorován	sponzorován	k2eAgInSc1d1	sponzorován
německou	německý	k2eAgFnSc7d1	německá
značkou	značka	k1gFnSc7	značka
Adidas	Adidasa	k1gFnPc2	Adidasa
a	a	k8xC	a
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
televizních	televizní	k2eAgFnPc6d1	televizní
reklamách	reklama	k1gFnPc6	reklama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
Messi	Messe	k1gFnSc4	Messe
také	také	k6eAd1	také
podepsal	podepsat	k5eAaPmAgMnS	podepsat
tříletou	tříletý	k2eAgFnSc4d1	tříletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Herbalife	Herbalif	k1gInSc5	Herbalif
<g/>
.	.	kIx.	.
</s>
<s>
Messi	Messe	k1gFnSc4	Messe
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
do	do	k7c2	do
rubriky	rubrika	k1gFnSc2	rubrika
Time	Tim	k1gInSc2	Tim
100	[number]	k4	100
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
žebříček	žebříček	k1gInSc4	žebříček
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
lidí	člověk	k1gMnPc2	člověk
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
spustil	spustit	k5eAaPmAgInS	spustit
Messi	Messe	k1gFnSc4	Messe
svou	svůj	k3xOyFgFnSc4	svůj
oficiální	oficiální	k2eAgFnSc4d1	oficiální
Facebookovou	Facebookový	k2eAgFnSc4d1	Facebooková
stránku	stránka	k1gFnSc4	stránka
a	a	k8xC	a
během	během	k7c2	během
pár	pár	k4xCyI	pár
hodin	hodina	k1gFnPc2	hodina
měl	mít	k5eAaImAgMnS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
jeho	jeho	k3xOp3gFnSc1	jeho
stránka	stránka	k1gFnSc1	stránka
přes	přes	k7c4	přes
33	[number]	k4	33
milionů	milion	k4xCgInPc2	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
mu	on	k3xPp3gMnSc3	on
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
Jorgem	Jorg	k1gMnSc7	Jorg
Horaciem	Horacium	k1gNnSc7	Horacium
Messim	Messim	k1gMnSc1	Messim
vyměřil	vyměřit	k5eAaPmAgMnS	vyměřit
španělský	španělský	k2eAgInSc4d1	španělský
soud	soud	k1gInSc4	soud
trest	trest	k1gInSc1	trest
vězení	vězení	k1gNnSc1	vězení
21	[number]	k4	21
měsíců	měsíc	k1gInPc2	měsíc
podmíněně	podmíněně	k6eAd1	podmíněně
za	za	k7c2	za
krácení	krácení	k1gNnSc2	krácení
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
daňovým	daňový	k2eAgInPc3d1	daňový
podvodům	podvod	k1gInPc3	podvod
mělo	mít	k5eAaImAgNnS	mít
docházet	docházet	k5eAaImF	docházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Primera	primera	k1gFnSc1	primera
división	división	k1gMnSc1	división
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Copa	Copa	k1gMnSc1	Copa
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
španělského	španělský	k2eAgInSc2d1	španělský
superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
evropského	evropský	k2eAgNnSc2d1	Evropské
<g />
.	.	kIx.	.
</s>
<s>
Superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
vítěz	vítěz	k1gMnSc1	vítěz
MS	MS	kA	MS
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
MS	MS	kA	MS
U20	U20	k1gFnSc2	U20
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
na	na	k7c4	na
MS	MS	kA	MS
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
1	[number]	k4	1
<g/>
×	×	k?	×
stříbro	stříbro	k1gNnSc1	stříbro
na	na	k7c4	na
MS	MS	kA	MS
2014	[number]	k4	2014
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
1	[number]	k4	1
<g/>
×	×	k?	×
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
×	×	k?	×
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
FIFA	FIFA	kA	FIFA
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
×	×	k?	×
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kopačka	kopačka	k1gFnSc1	kopačka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
UEFA	UEFA	kA	UEFA
Club	club	k1gInSc1	club
Footballer	Footballer	k1gInSc1	Footballer
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
Cena	cena	k1gFnSc1	cena
UEFA	UEFA	kA	UEFA
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
španělské	španělský	k2eAgFnSc2d1	španělská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
×	×	k?	×
fotbalista	fotbalista	k1gMnSc1	fotbalista
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
Cena	cena	k1gFnSc1	cena
Golden	Goldna	k1gFnPc2	Goldna
Boy	boa	k1gFnSc2	boa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
×	×	k?	×
Cena	cena	k1gFnSc1	cena
Bravo	bravo	k1gMnSc1	bravo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
×	×	k?	×
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
fotbalista	fotbalista	k1gMnSc1	fotbalista
španělské	španělský	k2eAgFnSc2d1	španělská
ligy	liga	k1gFnSc2	liga
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
×	×	k?	×
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
Zvolen	Zvolen	k1gInSc1	Zvolen
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Šampión	šampión	k1gMnSc1	šampión
šampiónů	šampión	k1gMnPc2	šampión
<g/>
)	)	kIx)	)
deníkem	deník	k1gInSc7	deník
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Equipe	Equip	k1gMnSc5	Equip
<g/>
.	.	kIx.	.
</s>
<s>
Zvolen	Zvolen	k1gInSc1	Zvolen
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
MS	MS	kA	MS
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgMnS	získat
Adidas	Adidas	k1gMnSc1	Adidas
Golden	Goldna	k1gFnPc2	Goldna
Ball	Ball	k1gMnSc1	Ball
<g/>
)	)	kIx)	)
</s>
