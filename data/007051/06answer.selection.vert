<s>
Komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
též	též	k9	též
komorový	komorový	k2eAgInSc4d1	komorový
mok	mok	k1gInSc4	mok
<g/>
,	,	kIx,	,
nitrooční	nitrooční	k2eAgFnSc1d1	nitrooční
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
tělní	tělní	k2eAgFnSc1d1	tělní
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
čočkou	čočka	k1gFnSc7	čočka
a	a	k8xC	a
rohovkou	rohovka	k1gFnSc7	rohovka
<g/>
.	.	kIx.	.
</s>
