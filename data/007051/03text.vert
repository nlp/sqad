<s>
Komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
též	též	k9	též
komorový	komorový	k2eAgInSc4d1	komorový
mok	mok	k1gInSc4	mok
<g/>
,	,	kIx,	,
nitrooční	nitrooční	k2eAgFnSc1d1	nitrooční
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
tělní	tělní	k2eAgFnSc1d1	tělní
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
čočkou	čočka	k1gFnSc7	čočka
a	a	k8xC	a
rohovkou	rohovka	k1gFnSc7	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
Funkcí	funkce	k1gFnSc7	funkce
komorové	komorový	k2eAgFnSc2d1	komorová
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
zachovávat	zachovávat	k5eAaImF	zachovávat
nitrooční	nitrooční	k2eAgInSc4d1	nitrooční
tlak	tlak	k1gInSc4	tlak
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
vyživovat	vyživovat	k5eAaImF	vyživovat
okolní	okolní	k2eAgFnPc4d1	okolní
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
,	,	kIx,	,
odvádět	odvádět	k5eAaImF	odvádět
odpadní	odpadní	k2eAgInPc4d1	odpadní
produkty	produkt	k1gInPc4	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
a	a	k8xC	a
transportovat	transportovat	k5eAaBmF	transportovat
vitamín	vitamín	k1gInSc4	vitamín
C	C	kA	C
do	do	k7c2	do
rohovky	rohovka	k1gFnSc2	rohovka
(	(	kIx(	(
<g/>
antioxidant	antioxidant	k1gInSc1	antioxidant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
imunoglobuliny	imunoglobulin	k2eAgInPc4d1	imunoglobulin
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
imunitní	imunitní	k2eAgFnSc6d1	imunitní
odpovědi	odpověď	k1gFnSc6	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
z	z	k7c2	z
99	[number]	k4	99
<g/>
%	%	kIx~	%
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgInPc4	některý
ionty	ion	k1gInPc4	ion
<g/>
,	,	kIx,	,
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc4	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
glukózu	glukóza	k1gFnSc4	glukóza
a	a	k8xC	a
kyselinu	kyselina	k1gFnSc4	kyselina
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
či	či	k8xC	či
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
komorovou	komorový	k2eAgFnSc7d1	komorová
vodou	voda	k1gFnSc7	voda
souvisí	souviset	k5eAaImIp3nS	souviset
porucha	porucha	k1gFnSc1	porucha
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
glaukom	glaukom	k1gInSc1	glaukom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Aqueous	Aqueous	k1gMnSc1	Aqueous
humour	humour	k1gMnSc1	humour
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
